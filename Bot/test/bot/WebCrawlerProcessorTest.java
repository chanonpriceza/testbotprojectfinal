package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import bean.ProductDataBean;
import bean.ProductUrlBean;
import bean.ReportBean;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;
import product.processor.ParserCrawler;
import product.processor.ProductUrlReader;
import unittest.MockHTMLParser;
import unittest.MockObjectHolder;
import unittest.MockHTMLParser.commandSet;
import web.crawler.WebCrawlerProcessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ParserCrawler.class)
@PowerMockIgnore("javax.management.*")
public class WebCrawlerProcessorTest {
	
	@InjectMocks WebCrawlerProcessor webcrawlerProcessor;
	@Mock DatabaseManager db;
	@Mock ProductUrlReader produtUrlReader; 
	@InjectMocks ProductUrlBean urlBean;
	@InjectMocks ProductUrlBean urlBean1;
	@InjectMocks ProductUrlBean urlBean2;
	@InjectMocks ProductUrlBean urlBean3;
	@InjectMocks ProductUrlBean urlBean4;
	@InjectMocks ProductUrlDB productUrlDB;
	@InjectMocks ProductDataDB productDataDB;
	@InjectMocks SendDataDB sendDataDB;
	@InjectMocks ReportBean report;
	@Mock ReportManager reportManager;
	@Mock CountDownLatch mockLatch;
	
	
	CountDownLatch latch;
	ExecutorService parserThread;
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ParserCrawler.class);
		BaseConfig.THREAD_NUM = 1;
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = db;
		MockHTMLParser.command = commandSet.NORMAL;
		MockHTMLParser.count = 0;
		BaseConfig.CONF_WCE_SLEEP_TIME = 0;
		MockObjectHolder.mockReportManager = reportManager;
		MockObjectHolder.productUrlReader = produtUrlReader;
		BaseConfig.CONF_PARSER_CLASS = "unittest.MockHTMLParser";
		urlBean.setUrl("http://www.soccersuck.com/");
		when(db.getProductUrlDB()).thenReturn(productUrlDB);
		when(db.getProductDataDB()).thenReturn(productDataDB);
		when(db.getSendDataDB()).thenReturn(sendDataDB);
		when(reportManager.getReport()).thenReturn(report);
		
		
		urlBean1.setUrl("1");
		urlBean2.setUrl("2");
		urlBean3.setUrl("3");
		urlBean4.setUrl("4");
		
	}
	
	//กรณีที่ รันได้ปกติ
	@Test
	public void web_crawler_processor_test_one_thread_normal() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		webcrawlerProcessor.setProperties(latch);
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null);;
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		parserThread.shutdown();
		latch.await();
		PowerMockito.verifyStatic(ParserCrawler.class);
		assertEquals(result.get(),true);
		ParserCrawler.process(Mockito.any(ProductDataBean[].class),Mockito.eq(urlBean));
		
	}
	
	//กรณีีที่ call readData(); แล้วเกิด SQL Exception
	@Test
	public void web_crawler_processor_test_one_thread_SQL_exception() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		webcrawlerProcessor.setProperties(latch);
		when(produtUrlReader.readData()).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		parserThread.shutdown();
		latch.await();
		assertEquals(result.get(),false);
		
	}
	
	//กรณีีที่ call ตรง finally แลเวเกิด Exception
	@Test
	public void web_crawler_processor_test_one_countDownLatch_exception() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		webcrawlerProcessor.setProperties(mockLatch);
		when(produtUrlReader.readData()).thenReturn(null);
		Mockito.doThrow(new MockitoException("UnitTest Error")).when(mockLatch).countDown();
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		parserThread.shutdown();
		assertEquals(result.get(),false);
		
	}
	
	//กรณีีที่ setProperties แล้ว CONF_URL_CRAWLER_CLASS new Instance เกิด ClassNotFound
	@Test
	public void web_crawler_processor_test_set_propertie_found_class_not_found() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		BaseConfig.CONF_PARSER_CLASS = "NOT FOUND";
		try {
			webcrawlerProcessor.setProperties(latch);
			parserThread.submit(webcrawlerProcessor);
			parserThread.shutdown();
		}catch(ClassNotFoundException e) {
			assertEquals(e.toString(),"java.lang.ClassNotFoundException: NOT FOUND");
		}
		
	}
	
	//กรณีที่ crawl ไม่พบ dataBeanList
	@Test
	public void web_crawler_processor_test_one_thread_cannot_parse_databean_normal() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		webcrawlerProcessor.setProperties(latch);
		MockHTMLParser.command = commandSet.NULL;
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null);
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		parserThread.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		PowerMockito.verifyStatic(ParserCrawler.class);
		ParserCrawler.markNoResult(Mockito.eq(urlBean));
		
	}

	//กรณีที่ crawl แล้ว sleepTIme > 0
	@Test
	public void web_crawler_processor_test_one_thread_sleep_normal() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		BaseConfig.CONF_WCE_SLEEP_TIME = 5000;
		webcrawlerProcessor.setProperties(latch);
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null);;
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		parserThread.shutdown();
		latch.await();
		PowerMockito.verifyStatic(ParserCrawler.class);
		assertEquals(result.get(),true);
		ParserCrawler.process(Mockito.any(ProductDataBean[].class),Mockito.eq(urlBean));
	}
	
	//กรณีที่ crawl พบ InterruptedException	TRUE	//
	@Test
	public void web_crawler_processor_test_one_thread_Interupted_exception() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		BaseConfig.CONF_WCE_SLEEP_TIME = 5000;
		webcrawlerProcessor.setProperties(latch);
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null);
		Future<Boolean> result = parserThread.submit(webcrawlerProcessor);
		Thread.sleep(1000);
		result.cancel(true);
		parserThread.shutdown();
		latch.await();
		assertEquals(result.isCancelled(),true);

	}
	
	//กรณีที่ รันได้ปกติ หลาย Thread
	@Test
	public void web_crawler_processor_test_many_all_thread_normal() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		List<Future<Boolean>> futureList = new ArrayList<>();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		webcrawlerProcessor.setProperties(latch);
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null);
		
		for(int i = 0;i < BaseConfig.THREAD_NUM ;i++) {
				futureList.add(parserThread.submit(webcrawlerProcessor));
		}
		
		parserThread.shutdown();
		latch.await();
		assertEquals(processFutureList(futureList), true);
		
	}
	
	//กรณีรันได้ ปกติบาง thread
	@Test
	public void web_crawler_processor_test_many_some_thread_normal() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		List<Future<Boolean>> futureList = new ArrayList<>();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(produtUrlReader.readData()).thenReturn(urlBean).thenReturn(null).thenThrow(new MockitoException("Mockito Exception")).thenThrow(new MockitoException("Mockito Exception"));
		
		for(int i = 0;i < BaseConfig.THREAD_NUM ;i++) {
				webcrawlerProcessor.setProperties(latch);
				Thread.sleep(1000);
				futureList.add(parserThread.submit(webcrawlerProcessor));
		}
		
		parserThread.shutdown();
		latch.await();
		assertEquals(futureList.get(0).get(),true);
		assertEquals(futureList.get(1).get(),false);
		assertEquals(futureList.get(2).get(),false);
		assertEquals(processFutureList(futureList), false);
		
	}
	
	//กรณีรันได้ error ทุก thread
	@Test
	public void web_crawler_processor_test_many_all_thread_fail() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		when(produtUrlReader.readData()).thenThrow(new MockitoException("Mockito Exception")); 
		List<Future<Boolean>> futureList = new ArrayList<>();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		
		for(int i = 0;i < BaseConfig.THREAD_NUM ;i++) {
			webcrawlerProcessor.setProperties(latch);
			futureList.add(parserThread.submit(webcrawlerProcessor));
		}
		
		parserThread.shutdown();
		latch.await();
		assertEquals(futureList.get(0).get(),false);
		assertEquals(futureList.get(1).get(),false);
		assertEquals(futureList.get(2).get(),false);
		assertEquals(processFutureList(futureList), false);
		
	}

	private boolean processFutureList(List<Future<Boolean>> flist) throws Exception {
		boolean result = true;
		
		for(Future<Boolean> f:flist) {//
			result = result && f.get();
		}
		
		return result;
	}
}
