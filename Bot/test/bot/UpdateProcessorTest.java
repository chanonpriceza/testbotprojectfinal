package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;

import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import bean.ReportBean;
import db.ProductDataDB;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import feed.CSVFeedCrawler;
import feed.JSONFeedCrawler;
import feed.TypeFeedCrawlerInterface.FEED_TYPE;
import feed.UpdateProcessor;
import feed.XMLFeedCrawler;
import manager.ReportManager;
import unittest.MockFeedCrawler;
import unittest.MockObjectHolder;

public class UpdateProcessorTest {
	
	private static final String NAME= "Test product";
	private static final String URL= "http://www.priceza.com/priceza";
	private static final String PIC_URL= "http://www.priceza.com/testPic.jpg";
	private static final String KEYWORD= "Test keyword";
	private static final String DESC= "Test Desc";
	private static final String REAL_PRODUCT_ID="test-real";
	private static final int CAT= 1;
	private static final double PRICE = 100.0;
	private static final double BASE_PRICE = 150.0;
	
	@Mock CountDownLatch mockLatch;
	@Mock DatabaseManager databaseManager;
	private MockFeedCrawler mockFeed;
	@Mock
	private XMLFeedCrawler xmlFeedCrawler;
	@Mock
	private CSVFeedCrawler csvFeedCrawler;
	@Mock
	private JSONFeedCrawler jsonFeedCrawler;
	@Mock
	private UrlPatternDB urlPatternDB;
	@Mock
	private ReportManager reportManager;
	
	@Mock
	private ProductDataDB productDataDB ;
	@InjectMocks
	private ProductDataBean pdb;
	@Mock
	private  ReportBean  report;
	
	CountDownLatch latch;
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		MockObjectHolder.csvFeedCrawler = csvFeedCrawler;
		MockObjectHolder.jsonFeedCrawler = jsonFeedCrawler;
		MockObjectHolder.xmlFeedCrawler =  xmlFeedCrawler;
		BaseConfig.THREAD_NUM = 1;
		when(databaseManager.getUrlPatternDB()).thenReturn(urlPatternDB);
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		when(reportManager.getReport()).thenReturn(report);
		pdb.setName(NAME);
		pdb.setBasePrice(BASE_PRICE);
		pdb.setUrl(URL);
		pdb.setUrlForUpdate(URL);
		pdb.setPrice(PRICE);
		pdb.setPictureUrl(PIC_URL);
		pdb.setCategoryId(CAT);
		pdb.setKeyword(KEYWORD);
		pdb.setDescription(DESC);
		pdb.setRealProductId(REAL_PRODUCT_ID);
		pdb.setStatus(STATUS.W.toString());
	
	}
	
//	@Test
//	public void updateprocessor_normal_feedtype_xml_beanisnull() throws Exception {	
//		ProductDataBean pdBean = new ProductDataBean();
//		pdBean.setName("");
//		pdBean.setBasePrice(BASE_PRICE);
//		pdBean.setUrl(URL);
//		pdBean.setUrlForUpdate(URL);
//		pdBean.setPrice(PRICE);
//		pdBean.setPictureUrl(PIC_URL);
//		pdBean.setCategoryId(CAT);
//		pdBean.setKeyword(KEYWORD);
//		pdBean.setDescription(DESC);
//		pdBean.setRealProductId(REAL_PRODUCT_ID);
//		pdBean.setStatus(STATUS.W.toString());
//		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
//		UpdateProcessor updateProcessor = new UpdateProcessor();
//		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
//		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
//		mockFeed = new MockFeedCrawler();
//
//		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdBean).thenReturn(null);
//		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdBean.getName())).thenReturn(pdBean);
//		updateProcessor.setProperties(mockFeed, latch);
//		
//		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
//		Future<Boolean> exFuture = executors.submit(updateProcessor);
//		latch.await();
//		executors.shutdown();
//		assertEquals(exFuture.get(),true);
//	}
	
	@Test
	public void updateprocessor_normal_feedtype_xml_many_thread() throws Exception {	
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(pdb).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, latch);
		BaseConfig.THREAD_NUM = 3;
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	@Test
	public void updateprocessor_normal_feedtype_xml() throws Exception {	
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, latch);
		
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	@Test
	public void updateprocessor_normal_feedtype_csv() throws Exception {	
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.CSV;
		mockFeed = new MockFeedCrawler();
		when(csvFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, latch);
		
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	@Test
	public void updateprocessor_normal_feedtype_json() throws Exception {	
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.JSON;
		mockFeed = new MockFeedCrawler();
		when(jsonFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, latch);
		
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	@Test
	public void updateprocessor_normal_feedtype_is_null_have_exception() throws Exception {	
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = null;
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, latch);
		
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	@Test
	public void updateprocessor_feedtype_SQLexception() throws Exception {	
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(null);
		updateProcessor.setProperties(mockFeed, latch);
		
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	@Test
	public void updateprocessor_feedtype_countDownlatch_error() throws Exception {	
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		when(productDataDB.updateStatusAndErrorUpdateCount(Mockito.anyInt(),Mockito.eq(STATUS.C), Mockito.anyInt())).thenReturn(1);
		UpdateProcessor updateProcessor = new UpdateProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);      
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		mockFeed = new MockFeedCrawler();
		Mockito.doThrow(new MockitoException("UnitTest Error")).when(mockLatch).countDown();
		when(xmlFeedCrawler.readData(mockFeed)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, pdb.getName())).thenReturn(pdb);
		updateProcessor.setProperties(mockFeed, mockLatch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(updateProcessor);
		mockLatch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	
}
