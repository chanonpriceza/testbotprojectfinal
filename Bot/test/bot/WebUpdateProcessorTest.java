package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;

import bean.ProductDataBean;
import bean.ReportBean;
import db.MerchantDB;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;
import product.processor.ProductReader;
import unittest.MockObjectHolder;
import web.crawler.WebUpdateProcessor;

public class WebUpdateProcessorTest {
	private final String name = "Test Product";
	private final String url = "http://test.com?productId=1";
	private final String status= "W";
	private final String keyword= "keyword";
	private final String desc= "Test Description";
	private final int cat = 1;
	private final double basePrice = 169.00;
	private final double price = 69.00;
	private final int merchantId= 1;
	private final int id= 999;
	

	@Mock DatabaseManager databaseManager;
	@Mock ProductReader productReader;
	@Mock MerchantDB merchantDB ;
	@Mock ProductDataDB productDataDB;
	@Mock ProductUrlDB productUrlDB ;
	@Mock SendDataDB  sendDataDB;
	@Mock ReportBean reportBean ;
	@Mock ReportManager reportManager;
	@Mock CountDownLatch mockLatch;
	
	@InjectMocks WebUpdateProcessor webUpdate;
	@InjectMocks WebUpdateProcessor errorUpdate;
	@InjectMocks ProductDataBean pdb;
	
	CountDownLatch latch;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		reportBean.setErrorCount(0);
		pdb.setName(name);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(url);
		pdb.setPrice(price);
		pdb.setBasePrice(basePrice);
		pdb.setDescription(desc);
		pdb.setCategoryId(cat);
		pdb.setStatus(status);
		pdb.setErrorUpdateCount(1);
		pdb.setKeyword(keyword);
		pdb.setMerchantId(1);
		pdb.setId(id);
		
		BaseConfig.CONF_PARSER_CLASS = "unittest.MockParserClass";
		MockObjectHolder.isMock = true;
		MockObjectHolder.productReader = productReader;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		BaseConfig.MERCHANT_ID = merchantId;
		BaseConfig.THREAD_NUM = 1;
		BaseConfig.CONF_DELETE_LIMIT = 10000;
		BaseConfig.CONF_FORCE_DELETE = false;
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		when(databaseManager.getProductUrlDB()).thenReturn(productUrlDB);
		when(databaseManager.getSendDataDB()).thenReturn(sendDataDB);
		when(reportManager.getReport()).thenReturn(reportBean);
		when(productReader.readData()).thenReturn(pdb).thenReturn(null);
		when(reportBean.getDelete()).thenReturn(0);
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = 10;
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_a_no_update() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_aexpire() throws Exception {
		pdb.setUrlForUpdate("http://test.com?productId=2");
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_error_link() throws Exception {
		pdb.setUrlForUpdate("http://test.com?productId=3");
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_sql_error() throws Exception {
		pdb.setUrlForUpdate("http://test.com?productId=3");
		when(productReader.readData()).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),false);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_other_error() throws Exception {
		pdb.setUrlForUpdate("http://test.com?productId=3");
		when(productReader.readData()).thenAnswer(invocation -> { throw new Exception("Exception");}).thenReturn(null); 
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),false);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_countDown_error() throws Exception {
		pdb.setUrlForUpdate("http://test.com?productId=1");
		Mockito.doThrow(new MockitoException("UnitTest Error")).when(mockLatch).countDown();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(mockLatch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		assertEquals(result.get(),false);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_pdb_null() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		pdb.setUrlForUpdate("null");
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_update_url_when_null() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		pdb.setUrlForUpdate("http://test.com?productId=4");
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_markError_forceDelete() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		BaseConfig.CONF_FORCE_DELETE = true;
		pdb.setUrlForUpdate("http://test.com?productId=3");
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_errorcount_more_than_deleteLimit() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = 5;
		pdb.setUrlForUpdate("http://test.com?productId=5");
		pdb.setErrorUpdateCount(6);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_normal_not_pass_deleteLimit() throws Exception {
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(reportBean.getDelete()).thenReturn(6);
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = 5;
		BaseConfig.CONF_DELETE_LIMIT = 6;
		pdb.setUrlForUpdate("http://test.com?productId=5");
		pdb.setErrorUpdateCount(6);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> result = executor.submit(webUpdate);
		executor.shutdown();
		latch.await();
		assertEquals(result.get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_many_thread_normal() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		List<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			results.add(executor.submit(webUpdate));
		}
		executor.shutdown();
		latch.await();
		assertEquals(results.get(0).get(),true);
		assertEquals(results.get(1).get(),true);
		assertEquals(results.get(2).get(),true);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_many_thread_some() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(productReader.readData()).thenReturn(pdb).thenReturn(null).thenReturn(pdb).thenReturn(null).thenThrow(new MockitoException("Thread error"));
		List<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			Thread.sleep(1000);
			results.add(executor.submit(webUpdate));
			
		}
		executor.shutdown();
		latch.await();
		assertEquals(results.get(0).get(),true);
		assertEquals(results.get(1).get(),true);
		assertEquals(results.get(2).get(),false);
		
	}
	
	@Test
	public void WebUpdateProcessorTest_many_thread_error_all() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		webUpdate.setProperties(latch);
		ExecutorService executor = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(productReader.readData()).thenThrow(new MockitoException("Thread error"));
		List<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			results.add(executor.submit(webUpdate));
			
		}
		Thread.sleep(1000);
		executor.shutdown();
		latch.await();
		assertEquals(results.get(0).get(),false);
		assertEquals(results.get(1).get(),false);
		assertEquals(results.get(2).get(),false);
		
	}
}
