package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import db.ProductDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import product.processor.ProductReader;
import unittest.MockObjectHolder;

public class ProductReaderTest {
	@Mock DatabaseManager databaseManager;
	@Mock ProductDataDB productDataDB;
	@Mock ProductReader productReader;
	@InjectMocks ProductDataBean productDataBean;
	int merchantId = 1;
	private int OFFSET = 1000;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock=true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		BaseConfig.MERCHANT_ID = merchantId;
	}
	
	@Test
	public void getByStatusLimit_isEmpty() throws Exception {
		ProductReader productReader = new ProductReader();
		assertEquals(null, productReader.readData());
	}
	
	@Test
	public void getByStatusLimit_Exception() throws Exception {
		ProductReader productReader = new ProductReader();
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenAnswer(invocation -> { throw new SQLException("Exception");}).thenReturn(null);
		assertEquals(null, productReader.readData());
	}
	
	@Test
	public void getByStatusLimit_size_lessthan() throws Exception {
		ProductReader productReader = new ProductReader();
		ProductDataBean productDataBean = new ProductDataBean();
		List<ProductDataBean> productsListBean = new ArrayList<ProductDataBean>();
		productDataBean.setId(1);
		productDataBean.setMerchantId(BaseConfig.MERCHANT_ID);
		productDataBean.setCategoryId(100);
		productDataBean.setDescription("test");
		productDataBean.setDynamicField("test");
		productDataBean.setErrorUpdateCount(1);
		productDataBean.setExpire(false);
		productDataBean.setHttpStatus("200");
		productDataBean.setKeyword("test");
		productDataBean.setName("test");
		productDataBean.setPictureData("test");
		productDataBean.setPictureUrl("test");
		productDataBean.setPrice(100);
		productDataBean.setBasePrice(120);
		productDataBean.setRealProductId("100");
		productDataBean.setStatus("test");
		productDataBean.setUpc("test");
		productDataBean.setUrl("test");
		productDataBean.setUrlForUpdate("test");
		productsListBean.add(productDataBean);
	
		
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductDataBean productDataBeanReturn = productReader.readData();
		assertEquals(productDataBeanReturn.getMerchantId(), merchantId);
	}
	@Test
	public void getByStatusLimit_size_greaterthan() throws Exception {
		ProductReader productReader = new ProductReader();
		ProductDataBean productDataBean = new ProductDataBean();
		List<ProductDataBean> productsListBean = new ArrayList<ProductDataBean>();
		for (int i = 0; i < OFFSET; i++) {
			productDataBean.setId(i);
			productDataBean.setMerchantId(BaseConfig.MERCHANT_ID);
			productDataBean.setCategoryId(100);
			productDataBean.setDescription("test");
			productDataBean.setDynamicField("test");
			productDataBean.setErrorUpdateCount(1);
			productDataBean.setExpire(false);
			productDataBean.setHttpStatus("200");
			productDataBean.setKeyword("test");
			productDataBean.setName("test");
			productDataBean.setPictureData("test");
			productDataBean.setPictureUrl("test");
			productDataBean.setPrice(100);
			productDataBean.setBasePrice(120);
			productDataBean.setRealProductId("100");
			productDataBean.setStatus("test");
			productDataBean.setUpc("test");
			productDataBean.setUrl("test");
			productDataBean.setUrlForUpdate("test");
			productsListBean.add(productDataBean);
		}
		
		
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductDataBean productDataBeanReturn = productReader.readData();
		assertEquals(productDataBeanReturn.getMerchantId(), BaseConfig.MERCHANT_ID);
	}

	
	@Test
	public void getByStatusLimit_changeCheckout_isTrue() throws Exception {
		ProductReader productReader = new ProductReader();
		ProductDataBean productDataBean = new ProductDataBean();
		List<ProductDataBean> productsListBean = new ArrayList<ProductDataBean>();
		productDataBean.setId(1);
		productDataBean.setMerchantId(BaseConfig.MERCHANT_ID);
		productDataBean.setCategoryId(100);
		productDataBean.setDescription("test");
		productDataBean.setDynamicField("test");
		productDataBean.setErrorUpdateCount(1);
		productDataBean.setExpire(false);
		productDataBean.setHttpStatus("200");
		productDataBean.setKeyword("test");
		productDataBean.setName("test");
		productDataBean.setPictureData("test");
		productDataBean.setPictureUrl("test");
		productDataBean.setPrice(100);
		productDataBean.setBasePrice(120);
		productDataBean.setRealProductId("100");
		productDataBean.setStatus("test");
		productDataBean.setUpc("test");
		productDataBean.setUrl("test");
		productDataBean.setUrlForUpdate("test");
		productsListBean.add(productDataBean);
		
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductDataBean productDataBeanReturn = productReader.readData();
		assertEquals(productDataBeanReturn.getMerchantId(), BaseConfig.MERCHANT_ID);
		
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductDataBean productDataBeanReturn2 = productReader.readData();
		assertEquals(productDataBeanReturn2, null);
	}

}
