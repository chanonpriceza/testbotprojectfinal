package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bean.ProductUrlBean;
import bean.ProductUrlBean.STATUS;
import db.ProductUrlDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import product.processor.ProductUrlReader;
import unittest.MockObjectHolder;

public class ProductUrlReaderTest {
	@Mock DatabaseManager databaseManager;
	@Mock ProductUrlDB producturlDB;
	@Mock ProductUrlReader productUrlReader;
	int merchantId = 1;
	private int OFFSET = 1000;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock=true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		when(databaseManager.getProductUrlDB()).thenReturn(producturlDB);
		BaseConfig.MERCHANT_ID = merchantId;
	}
	
	@Test
	public void getByStatusLimit_isEmpty() throws Exception {
		ProductUrlReader productReader = new ProductUrlReader();
		assertEquals(null, productReader.readData());
	}
	
	@Test
	public void getByStatusLimit_Exception() throws Exception {
		ProductUrlReader productReader = new ProductUrlReader();
		when(producturlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenAnswer(invocation -> { throw new SQLException("Exception");}).thenReturn(null);
		assertEquals(null, productReader.readData());
	}
	
	@Test
	public void getByStatusLimit_size_lessthan() throws Exception {
		ProductUrlReader productUrlReader = new ProductUrlReader();
		ProductUrlBean productUrlBean = new ProductUrlBean();
		List<ProductUrlBean> productsListBean = new ArrayList<ProductUrlBean>();
		productUrlBean.setCategoryId(1);
		productUrlBean.setKeyword("test");
		productUrlBean.setMerchantId(BaseConfig.MERCHANT_ID);
		productUrlBean.setState("test");
		productUrlBean.setStatus("test");
		productUrlBean.setUrl("test");
		productsListBean.add(productUrlBean);
	
		
		when(producturlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductUrlBean productDataBeanReturn = productUrlReader.readData();
		assertEquals(productDataBeanReturn.getMerchantId(), merchantId);
		assertEquals(productDataBeanReturn.getKeyword(), "test");
	}
	
	@Test
	public void getByStatusLimit_size_greaterthan() throws Exception {
		ProductUrlReader productUrlReader = new ProductUrlReader();
		ProductUrlBean productUrlBean = new ProductUrlBean();
		List<ProductUrlBean> productsListBean = new ArrayList<ProductUrlBean>();
		
		for (int i = 0; i < OFFSET; i++) {
			productUrlBean.setCategoryId(i);
			productUrlBean.setKeyword("test");
			productUrlBean.setMerchantId(BaseConfig.MERCHANT_ID);
			productUrlBean.setState("test");
			productUrlBean.setStatus("test");
			productUrlBean.setUrl("test");
			productsListBean.add(productUrlBean);
		}
		when(producturlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductUrlBean productDataBeanReturn = productUrlReader.readData();
		assertEquals(productDataBeanReturn.getMerchantId(), merchantId);
		assertEquals(productDataBeanReturn.getKeyword(), "test");
	}

	
	@Test
	public void getByStatusLimit_changeCheckout_isTrue() throws Exception {
		ProductUrlBean productUrlBean = new ProductUrlBean();
		List<ProductUrlBean> productsListBean = new ArrayList<ProductUrlBean>();
		
		productUrlBean.setCategoryId(1);
		productUrlBean.setKeyword("test");
		productUrlBean.setMerchantId(BaseConfig.MERCHANT_ID);
		productUrlBean.setState("test");
		productUrlBean.setStatus("test");
		productUrlBean.setUrl("test");
		productsListBean.add(productUrlBean);
		
		when(producturlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductUrlBean productDataBeanReturn = ProductUrlReader.getInstance().readData();
		assertEquals(productDataBeanReturn.getMerchantId(), BaseConfig.MERCHANT_ID);
		
		when(producturlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET)).thenReturn(productsListBean);
		ProductUrlBean productDataBeanReturn2 = ProductUrlReader.getInstance().readData();
		assertEquals(productDataBeanReturn2, null);
	}

}
