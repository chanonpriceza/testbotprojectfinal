package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import db.ProductDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;
import product.processor.ParserUpdate;
import unittest.MockImageParser;
import unittest.MockObjectHolder;
import utils.BotUtil;

public class ParserUpdateTest {

	@Mock DatabaseManager databaseManager;
	@Mock ReportManager reportManager;
	@Mock ProductDataDB productDataDB;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		BaseConfig.IMAGE_PARSER = new MockImageParser();
	}
	// ############################# Analyzeproduct 1 parameterbean ###################################
	@Test
	public void analyzeproduct_parameterbean_is_null() throws SQLException {
		assertEquals(ParserUpdate.analyzeProduct(null),null);
	}
		
	@Test
	public void analyzeproduct_parameterbean_is_empty() throws SQLException {
		ProductDataBean productDataBean = new ProductDataBean();
		assertEquals(ParserUpdate.analyzeProduct(productDataBean),null);
	}
	
	@Test
	public void baseValidate_return_false() throws SQLException {
		// name,price,url is null
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName(null);
		productDataBean.setPrice(0);
		productDataBean.setUrl(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, productDataBean.getName())).thenReturn(productDataBean);
		assertEquals(ParserUpdate.baseValidate(productDataBean),false);
		
		// name,price,url is allempty
		ProductDataBean productDataBeanOne = new ProductDataBean();
		productDataBeanOne.setName("");
		productDataBeanOne.setPrice(0);
		productDataBeanOne.setUrl("");
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, productDataBeanOne.getName())).thenReturn(productDataBean);
		assertEquals(ParserUpdate.baseValidate(productDataBeanOne),false);
		
		// name is empty but price,url is have value
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		productDataBeanTwo.setName("");
		productDataBeanTwo.setPrice(2);
		productDataBeanTwo.setUrl("https://test.com");
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID, productDataBeanTwo.getName())).thenReturn(productDataBean);
		assertEquals(ParserUpdate.baseValidate(productDataBeanTwo),false);
	
	}

	@Test
	public void baseValidate_return_true() throws SQLException {
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/");
		productDataBean.setPictureUrl("https:/www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		assertEquals(ParserUpdate.baseValidate(productDataBean),true);
		
		ProductDataBean productDataBeanOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(10.0);
		productDataBeanOne.setUrl("https://test/");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription("test");
		productDataBeanOne.setKeyword("test");
		assertEquals(ParserUpdate.baseValidate(productDataBeanOne),true);
		
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(10.0);
		productDataBeanTwo.setUrl("https://test/");
		productDataBeanTwo.setPictureUrl(null);
		assertEquals(ParserUpdate.baseValidate(productDataBeanTwo),true);
		
	}
	

	// ############################# Analyzeproduct 2 parameterbean ###################################
	@Test
	public void analyzeproduct_parameter2bean_is_null() throws SQLException {
		assertEquals(ParserUpdate.analyzeProduct(null,null),null);
	}
		
	@Test
	public void analyzeproduct_parameter2bean_is_empty() throws SQLException {
		ProductDataBean productDataBean = new ProductDataBean();
		assertEquals(ParserUpdate.analyzeProduct(productDataBean,productDataBean),null);
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_check_bean_status() throws SQLException {
		// bean is null
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBean,null),null);
		
		// status is C
		ProductDataBean productDataBeanOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(10.0);
		productDataBeanOne.setUrl("https://test/");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription("test");
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setStatus(STATUS.C.toString());
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanOne),null);
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_bean_case_urlempty_return_null() throws SQLException {
		
		
		//#################################################################
		//#----------------  Bean DB -------------------------
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrlForUpdate("");
		//----------------------------------------------------
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB),null);
		//#################################################################

	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_bean_case_urlnull_return_null() throws SQLException {
		
		
		//#################################################################
		//#----------------  Bean DB -------------------------
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrlForUpdate(null);
		//----------------------------------------------------
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB),null);
		//#################################################################
		
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_bean_case_name_is_notduplicate() throws SQLException {
		BotUtil.LOCALE = "TH";
		
		//#################################################################
		//#----------------  Bean DB -------------------------
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("testha");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB),null);
		//#################################################################
		
		//#################################################################
		//#----------------  Bean DB -------------------------
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(0);
		productDataBeanOne.setUrl("https://test/");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription("test");
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("testha");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne),null);
		//#################################################################
		
//		have config BaseConfig.CONF_IGNORE_NAME_CHANGE
		//#################################################################
		//#----------------  Bean DB -------------------------
		BaseConfig.CONF_IGNORE_NAME_CHANGE = true;
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(0);
		productDataBeanTwo.setUrl("https://test/");
		productDataBeanTwo.setPictureUrl("www.test/");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("testha");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//############################  Result  ###########################
		assertEquals(ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo),null);
		//#################################################################
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_config_CONF_USER_OLD_PRODUCT_URL() throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = true;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getName(), "test");
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		assertEquals(resultProductDataBean.getKeyword(), "test");
		assertEquals(resultProductDataBean.getUrlForUpdate(), "https://test/");
		//#################################################################
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_nothave_config_CONF_USER_OLD_PRODUCT_URL() throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getName(), "test");
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		assertEquals(resultProductDataBean.getKeyword(), "test");
		assertEquals(resultProductDataBean.getUrlForUpdate(), "https://test/");
		//#################################################################
	} 
	
	
	
	@Test
	public void analyzeproduct_parameter2bean_is_urlold_and_urlnew_is_duplicate_and_nothave_config() throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getName(), "test");
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		assertEquals(resultProductDataBean.getKeyword(), "test");
		assertEquals(resultProductDataBean.getUrlForUpdate(), "https://test/");
		//#################################################################
	} 
	
	
	
	@Test
	public void analyzeproduct_parameter2bean_is_urlold_and_urlnew_is_notduplicate_and_have_config() throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getName(), "test");
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		assertEquals(resultProductDataBean.getKeyword(), "test");
		assertEquals(resultProductDataBean.getUrlForUpdate(), "https://test/");
		//#################################################################
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_urlold_and_urlnew_is_notduplicate_and_nothave_config() throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		//############################  Result  ###########################
		assertEquals(parserResultBean, null);
		//#################################################################
	} 
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_config_CONF_CURRENCY_RATE_greaterthan_0 () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 10;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_config_CONF_CURRENCY_RATE_lessthan_0 () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 10;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_notconfig_CONF_CURRENCY_RATE_greaterthan_0 () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_notconfig_CONF_CURRENCY_RATE_lessthan_0 () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(1.0);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 10.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newprice_greaterthan_CONTACT_PRICE () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(BotUtil.CONTACT_PRICE+1);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 9999999.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newprice_lessthan_CONTACT_PRICE () throws SQLException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(BotUtil.CONTACT_PRICE-1);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 9999999.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newprice_equals_CONTACT_PRICE () throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(BotUtil.CONTACT_PRICE);
		productDataBean.setBasePrice(10.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 9999999.0,12.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newbaseprice_greaterthan_CONTACT_0_and_newbaseprice_greaterthan_newprice () throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(300.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 200.0,12.0);
		assertEquals(resultProductDataBean.getBasePrice(), 300.0,0.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newbaseprice_greaterthan_CONTACT_0_and_newbaseprice_lessthan_newprice () throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(100.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 200.0,0.0);
		assertEquals(resultProductDataBean.getBasePrice(), 200.0,0.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newbaseprice_lessthan_CONTACT_0_and_newbaseprice_greaterthan_newprice () throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 0.0,0.0);
		assertEquals(resultProductDataBean.getBasePrice(), -1.0,0.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_newbaseprice_lessthan_CONTACT_0_and_newbaseprice_lessthan_newprice () throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription("test");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getPrice(), 0.0,0.0);
		assertEquals(resultProductDataBean.getBasePrice(), -1.0,0.0);
		//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_new_and_old_Description() throws SQLException {
		// ############################	null ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBean.getDescription(), null);
		//#################################################################
		
		
		
		// ############################	empty ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setBasePrice(-1);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription(null);
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setPictureUrl("www.test/");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setDescription("");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBeanOne.getDescription(), null);
		//#################################################################
		
		
		// ############################	have value ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setBasePrice(-1);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setPictureUrl("www.test/");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setPictureUrl("www.test/");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setDescription("test");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  ###########################
		assertEquals(resultProductDataBeanTwo.getDescription(), "test");
		//#################################################################
	
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_dec_duplicate_and_have_config_CONF_ENABLE_UPDATE_DESC() throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setDescription("duplicate and have config");
		
		
		//#----------------  Bean DB -------------------------
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setDescription("duplicate and have config");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_ENABLE_UPDATE_DESC = true;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getDescription(), "duplicate and have config");
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_dec_duplicate_and_nohave_config_CONF_ENABLE_UPDATE_DESC() throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setDescription("duplicate des and have config");
		
		
		//#----------------  old Bean DB -------------------------
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setDescription("duplicate des and have config");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_ENABLE_UPDATE_DESC = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getDescription(), "duplicate des and have config");
		
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_dec_notduplicate_and_have_config_CONF_ENABLE_UPDATE_DESC() throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setDescription("notduplicate dec and have config");
		
		
		//#----------------  old Bean DB -------------------------
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setDescription("notduplicate dec and have configgg");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_ENABLE_UPDATE_DESC = true;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getDescription(), "notduplicate dec and have config");
		
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_dec_notduplicate_and_nohave_config_CONF_ENABLE_UPDATE_DESC() throws SQLException {
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setDescription("notduplicate dec and have config");
		
		
		//#----------------  old Bean DB -------------------------
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setDescription("notduplicate dec and have configgg");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_ENABLE_UPDATE_DESC = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getDescription(), "notduplicate dec and have configgg");
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_new_and_old_RealProductId() throws SQLException {
		// ############################	null ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId(null);
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getRealProductId(), null);
		//#################################################################
		
		
		
		// ############################	empty ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setBasePrice(-1);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription(null);
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setRealProductId("");
		
		
		//#---------------- old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setPictureUrl("www.test/");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setDescription("");
		productDataBeanfromDBOne.setRealProductId("");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanOne.getRealProductId(), null);
		//#################################################################
		
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setBasePrice(-1);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setPictureUrl("www.test/");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setRealProductId(null);
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setPictureUrl("www.test/");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setDescription("test");
		productDataBeanfromDBTwo.setRealProductId("test");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanTwo.getRealProductId(), "test");
		//#################################################################
	
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setBasePrice(-1);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setPictureUrl("www.test/");
		productDataBeanThree.setDescription("test");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setRealProductId("test");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setPictureUrl("www.test/");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setDescription("test");
		productDataBeanfromDBThree.setRealProductId(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanThree.getRealProductId(), "test");
		//#################################################################
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_new_and_old_UPC() throws SQLException {
		// ############################	null ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getUpc(), null);
		//#################################################################
		
		
		
		// ############################	empty ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setBasePrice(-1);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription(null);
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setRealProductId("test");
		productDataBeanOne.setUpc("");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setPictureUrl("www.test/");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setDescription("");
		productDataBeanfromDBOne.setRealProductId("test");
		productDataBeanfromDBOne.setUpc("");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanOne.getUpc(), null);
		//#################################################################
		
		//#################################################################
		//#----------------  old Bean DB -------------------------
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setBasePrice(-1);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setPictureUrl("www.test/");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setRealProductId(null);
		productDataBeanTwo.setUpc(null);
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setPictureUrl("www.test/");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setDescription("test");
		productDataBeanfromDBTwo.setRealProductId("test");
		productDataBeanfromDBTwo.setUpc("test");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanTwo.getUpc(), "test");
		//#################################################################
	
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setBasePrice(-1);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setPictureUrl("www.test/");
		productDataBeanThree.setDescription("test");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setRealProductId("test");
		productDataBeanThree.setUpc("test");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setPictureUrl("www.test/");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setDescription("test");
		productDataBeanfromDBThree.setRealProductId(null);
		productDataBeanfromDBThree.setUpc(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanThree.getUpc(), "test");
		//#################################################################
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_new_and_old_Keyword() throws SQLException {
		// ############################	null ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setPictureUrl("www.test/");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		productDataBean.setKeyword(null);
		//#----------------  Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		productDataBeanfromDB.setKeyword(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getKeyword(), null);
		//#################################################################
		
		
		
		// ############################	empty ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setBasePrice(-1);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setPictureUrl("www.test/");
		productDataBeanOne.setDescription(null);
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setRealProductId("test");
		productDataBeanOne.setUpc("");
		productDataBeanOne.setKeyword("");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setPictureUrl("www.test/");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setDescription("");
		productDataBeanfromDBOne.setRealProductId("test");
		productDataBeanfromDBOne.setUpc("");
		productDataBeanfromDBOne.setKeyword("");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanOne.getKeyword(), null);
		//#################################################################
		
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setBasePrice(-1);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setPictureUrl("www.test/");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setRealProductId(null);
		productDataBeanTwo.setUpc(null);
		productDataBeanTwo.setKeyword(null);
		
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setPictureUrl("www.test/");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setDescription("test");
		productDataBeanfromDBTwo.setRealProductId("test");
		productDataBeanfromDBTwo.setUpc("test");
		productDataBeanfromDBTwo.setKeyword("test");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanTwo.getKeyword(), "test");
		//#################################################################
	
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setBasePrice(-1);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setPictureUrl("www.test/");
		productDataBeanThree.setDescription("test");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setRealProductId("test");
		productDataBeanThree.setUpc("test");
		productDataBeanThree.setKeyword("test");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setPictureUrl("www.test/");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setDescription("test");
		productDataBeanfromDBThree.setRealProductId(null);
		productDataBeanfromDBThree.setUpc(null);
		productDataBeanThree.setKeyword(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanThree.getKeyword(), null);
		//#################################################################
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_config_CONF_FORCE_UPDATE_CATEGORY_and_CONF_FORCE_UPDATE_KEYWORD() throws SQLException, ParseException {

		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setKeyword("test");
		productDataBean.setCategoryId(1);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setKeyword("test");
		productDataBeanfromDB.setCategoryId(1);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = true;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = true;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		String jsonResult = resultProductDataBean.getDynamicField();
		assertEquals(null, jsonResult);
		assertEquals("test", resultProductDataBean.getKeyword());
		assertEquals(1, resultProductDataBean.getCategoryId());
		//#################################################################	
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setKeyword(null);
		productDataBeanOne.setCategoryId(10);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setKeyword(null);
		productDataBeanfromDBOne.setCategoryId(5);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = true;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		String jsonResultOne = resultProductDataBeanOne.getDynamicField();
		JSONObject jsonObj = (JSONObject) new JSONParser().parse(jsonResultOne);
		String oldCategory = (String) jsonObj.get("oldCategory");
		assertEquals(oldCategory, "5");
		assertEquals(null, resultProductDataBeanOne.getKeyword());
		assertEquals(10, resultProductDataBeanOne.getCategoryId());
		//#################################################################	
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setKeyword("tytryrt");
		productDataBeanTwo.setCategoryId(100);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setKeyword("testtt");
		productDataBeanfromDBTwo.setCategoryId(100);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = true;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		String jsonResultTwo = resultProductDataBeanTwo.getDynamicField();
		assertEquals(jsonResultTwo, null);
		assertEquals("tytryrt", resultProductDataBeanTwo.getKeyword());
		assertEquals(100, resultProductDataBeanTwo.getCategoryId());
		//#################################################################	
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setKeyword("tytryrt");
		productDataBeanThree.setCategoryId(10);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setKeyword("testtt");
		productDataBeanfromDBThree.setCategoryId(5);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = true;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		String jsonResultThree = resultProductDataBeanThree.getDynamicField();
		JSONObject jsonObjThree  = (JSONObject) new JSONParser().parse(jsonResultThree);
		String oldCategoryThree = (String) jsonObjThree.get("oldCategory");
		assertEquals(oldCategoryThree, "5");
		//#################################################################	
		
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanFour = new ProductDataBean();
		ProductDataBean productDataBeanfromDBFour = new ProductDataBean();
		productDataBeanFour.setName("test");
		productDataBeanFour.setPrice(200.0);
		productDataBeanFour.setUrl("https://test/1");
		productDataBeanFour.setUrlForUpdate("https://test/");
		productDataBeanFour.setKeyword("test");
		productDataBeanFour.setKeyword("tytryrt");
		productDataBeanFour.setCategoryId(0);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBFour.setName("test");
		productDataBeanfromDBFour.setStatus(STATUS.W.toString());
		productDataBeanfromDBFour.setUrl("https://test/2");
		productDataBeanfromDBFour.setUrlForUpdate("https://test/");
		productDataBeanfromDBFour.setKeyword("testtt");
		productDataBeanfromDBFour.setCategoryId(0);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = true;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanFour = ParserUpdate.analyzeProduct(productDataBeanFour,productDataBeanfromDBFour);
		ProductDataBean resultProductDataBeanFour = parserResultBeanFour.getPdb();
		//############################  Result  #####################################
		String jsonResultFour = resultProductDataBeanFour.getDynamicField();
		assertEquals(null, jsonResultFour);
		//#################################################################	

	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_have_noconfig_CONF_FORCE_UPDATE_CATEGORY_and_CONF_FORCE_UPDATE_KEYWORD() throws SQLException, ParseException {
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setKeyword(null);
		productDataBeanOne.setCategoryId(1);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setKeyword(null);
		productDataBeanfromDBOne.setCategoryId(0);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		String jsonResultOne = resultProductDataBeanOne.getDynamicField();
		JSONObject jsonObjOne = (JSONObject) new JSONParser().parse(jsonResultOne);
		String oldCategoryOne = (String) jsonObjOne.get("oldCategory");
		assertEquals(1, resultProductDataBeanOne.getCategoryId());
		assertEquals(null, resultProductDataBeanOne.getKeyword());
		assertEquals(oldCategoryOne, "0");
		//#################################################################	
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setUrl("https://test/1");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setKeyword(null);
		productDataBean.setCategoryId(-1);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setKeyword(null);
		productDataBeanfromDB.setCategoryId(0);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		String jsonResult = resultProductDataBean.getDynamicField();
		assertEquals(null, jsonResult);
		assertEquals(0,resultProductDataBean.getCategoryId());
		assertEquals(null,resultProductDataBean.getKeyword());
		//#################################################################	
		

		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setCategoryId(0);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setKeyword(null);
		productDataBeanfromDBTwo.setCategoryId(1);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		String jsonResultTwo = resultProductDataBeanTwo.getDynamicField();
		assertEquals(null, jsonResultTwo);
		assertEquals(1,resultProductDataBeanTwo.getCategoryId());
		assertEquals("test",resultProductDataBeanTwo.getKeyword());
		//#################################################################	

		//#################################################################
		//#---------------- new Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setKeyword(null);
		productDataBeanThree.setCategoryId(1);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setKeyword("test");
		productDataBeanfromDBThree.setCategoryId(1);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		String jsonResultThree = resultProductDataBeanThree.getDynamicField();
		assertEquals(null, jsonResultThree);
		assertEquals(1,resultProductDataBeanThree.getCategoryId());
		assertEquals("test",resultProductDataBeanThree.getKeyword());
		//#################################################################	
		//#################################################################
		//#---------------- new Bean DB ------------------------- 
		ProductDataBean productDataBeanFour = new ProductDataBean();
		ProductDataBean productDataBeanfromDBFour = new ProductDataBean();
		productDataBeanFour.setName("test");
		productDataBeanFour.setPrice(200.0);
		productDataBeanFour.setUrl("https://test/1");
		productDataBeanFour.setUrlForUpdate("https://test/");
		productDataBeanFour.setKeyword("test");
		productDataBeanFour.setCategoryId(1);
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBFour.setName("test");
		productDataBeanfromDBFour.setStatus(STATUS.W.toString());
		productDataBeanfromDBFour.setUrl("https://test/2");
		productDataBeanfromDBFour.setUrlForUpdate("https://test/");
		productDataBeanfromDBFour.setKeyword("test");
		productDataBeanfromDBFour.setCategoryId(1);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		ParserResultBean parserResultBeanFour = ParserUpdate.analyzeProduct(productDataBeanFour,productDataBeanfromDBFour);
		ProductDataBean resultProductDataBeanFour = parserResultBeanFour.getPdb();
		//############################  Result  #####################################
		String jsonResultFour = resultProductDataBeanFour.getDynamicField();
		assertEquals(null, jsonResultFour);
		assertEquals(1,resultProductDataBeanFour.getCategoryId());
		assertEquals("test",resultProductDataBeanFour.getKeyword());
		//#################################################################	
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_new_and_old_Picture() throws SQLException {
		// ############################	null ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		productDataBean.setKeyword(null);
		productDataBean.setPictureUrl(null);
		//#----------------  Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/2");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		productDataBeanfromDB.setKeyword(null);
		productDataBeanfromDB.setPictureUrl(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getPictureUrl(), null);
		//#################################################################
		
		
		
		// ############################	empty ############################
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanOne = new ProductDataBean();
		ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
		productDataBeanOne.setName("test");
		productDataBeanOne.setPrice(200.0);
		productDataBeanOne.setBasePrice(-1);
		productDataBeanOne.setUrl("https://test/1");
		productDataBeanOne.setDescription(null);
		productDataBeanOne.setKeyword("test");
		productDataBeanOne.setUrlForUpdate("https://test/");
		productDataBeanOne.setRealProductId("test");
		productDataBeanOne.setUpc("");
		productDataBeanOne.setKeyword("");
		productDataBeanOne.setPictureUrl("");
	
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBOne.setName("test");
		productDataBeanfromDBOne.setStatus(STATUS.W.toString());
		productDataBeanfromDBOne.setUrl("https://test/2");
		productDataBeanfromDBOne.setPictureUrl("www.test/");
		productDataBeanfromDBOne.setUrlForUpdate("https://test/");
		productDataBeanfromDBOne.setDescription("");
		productDataBeanfromDBOne.setRealProductId("test");
		productDataBeanfromDBOne.setUpc("");
		productDataBeanfromDBOne.setKeyword("");
		productDataBeanfromDBOne.setPictureUrl("");
		
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
		ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanOne.getPictureUrl(), null);
		//#################################################################
		
		
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanTwo = new ProductDataBean();
		ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
		productDataBeanTwo.setName("test");
		productDataBeanTwo.setPrice(200.0);
		productDataBeanTwo.setBasePrice(-1);
		productDataBeanTwo.setUrl("https://test/1");
		productDataBeanTwo.setDescription("test");
		productDataBeanTwo.setKeyword("test");
		productDataBeanTwo.setUrlForUpdate("https://test/");
		productDataBeanTwo.setRealProductId(null);
		productDataBeanTwo.setUpc(null);
		productDataBeanTwo.setKeyword(null);
		productDataBeanTwo.setPictureUrl(null);
		
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBTwo.setName("test");
		productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
		productDataBeanfromDBTwo.setUrl("https://test/2");
		productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
		productDataBeanfromDBTwo.setDescription("test");
		productDataBeanfromDBTwo.setRealProductId("test");
		productDataBeanfromDBTwo.setUpc("test");
		productDataBeanfromDBTwo.setKeyword("test");
		productDataBeanfromDBTwo.setPictureUrl("https://test/");
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
		ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanTwo.getPictureUrl(), "https://test/");
		//#################################################################
	
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBeanThree = new ProductDataBean();
		ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
		productDataBeanThree.setName("test");
		productDataBeanThree.setPrice(200.0);
		productDataBeanThree.setBasePrice(-1);
		productDataBeanThree.setUrl("https://test/1");
		productDataBeanThree.setDescription("test");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setUrlForUpdate("https://test/");
		productDataBeanThree.setRealProductId("test");
		productDataBeanThree.setUpc("test");
		productDataBeanThree.setKeyword("test");
		productDataBeanThree.setPictureUrl("https://test/");
		
		
		//#----------------  old Bean DB -------------------------
		productDataBeanfromDBThree.setName("test");
		productDataBeanfromDBThree.setStatus(STATUS.W.toString());
		productDataBeanfromDBThree.setUrl("https://test/2");
		productDataBeanfromDBThree.setUrlForUpdate("https://test/");
		productDataBeanfromDBThree.setDescription("test");
		productDataBeanfromDBThree.setRealProductId(null);
		productDataBeanfromDBThree.setUpc(null);
		productDataBeanfromDBThree.setKeyword(null);
		productDataBeanfromDBThree.setPictureUrl(null);
		//----------------------------------------------------
		//#################################################################
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
		ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBeanThree.getPictureUrl(), null);
		//#################################################################
		
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_CONF_ENABLE_UPDATE_PIC_CONF_FORCE_UPDATE_IMAGE_allconfig_is_true() throws SQLException {
		BaseConfig.CONF_ENABLE_UPDATE_PIC = true;
		BaseConfig.CONF_FORCE_UPDATE_IMAGE = true;
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		productDataBean.setKeyword(null);
		productDataBean.setPictureUrl("http://www.priceza.com/test.jpg");
		//#----------------  Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		productDataBeanfromDB.setKeyword(null);
		productDataBeanfromDB.setPictureUrl(null);
		//----------------------------------------------------
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getPictureUrl(), "http://www.priceza.com/test.jpg");
		//#################################################################
		
		
		//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanOne = new ProductDataBean();
			ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
			productDataBeanOne.setName("test");
			productDataBeanOne.setPrice(200.0);
			productDataBeanOne.setBasePrice(-1);
			productDataBeanOne.setUrl("https://test/1");
			productDataBeanOne.setDescription(null);
			productDataBeanOne.setKeyword("test");
			productDataBeanOne.setUrlForUpdate("https://test/");
			productDataBeanOne.setRealProductId("test");
			productDataBeanOne.setUpc(null);
			productDataBeanOne.setKeyword(null);
			productDataBeanOne.setPictureUrl(null);
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBOne.setName("test");
			productDataBeanfromDBOne.setStatus(STATUS.W.toString());
			productDataBeanfromDBOne.setUrl("https://test/1");
			productDataBeanfromDBOne.setPictureUrl("www.test/");
			productDataBeanfromDBOne.setDescription(null);
			productDataBeanfromDBOne.setUrlForUpdate("https://test/");
			productDataBeanfromDBOne.setRealProductId("test");
			productDataBeanfromDBOne.setUpc(null);
			productDataBeanfromDBOne.setKeyword(null);
			productDataBeanfromDBOne.setPictureUrl("http://www.priceza.com/test.jpg");
			//----------------------------------------------------
			ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
			ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanOne.getPictureUrl(), "http://www.priceza.com/test.jpg");
			//#################################################################
			
			//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanTwo = new ProductDataBean();
			ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
			productDataBeanTwo.setName("test");
			productDataBeanTwo.setPrice(200.0);
			productDataBeanTwo.setBasePrice(-1);
			productDataBeanTwo.setUrl("https://test/1");
			productDataBeanTwo.setDescription(null);
			productDataBeanTwo.setKeyword("test");
			productDataBeanTwo.setUrlForUpdate("https://test/");
			productDataBeanTwo.setRealProductId("test");
			productDataBeanTwo.setUpc(null);
			productDataBeanTwo.setKeyword(null);
			productDataBeanTwo.setPictureUrl("https://test/");
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBTwo.setName("test");
			productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
			productDataBeanfromDBTwo.setUrl("https://test/1");
			productDataBeanfromDBTwo.setPictureUrl("www.test/");
			productDataBeanfromDBTwo.setDescription(null);
			productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
			productDataBeanfromDBTwo.setRealProductId("test");
			productDataBeanfromDBTwo.setUpc(null);
			productDataBeanfromDBTwo.setKeyword(null);
			productDataBeanfromDBTwo.setPictureUrl(null);
			//----------------------------------------------------
			ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
			ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanTwo.getPictureUrl(), null);
			//#################################################################
			
			//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanThree = new ProductDataBean();
			ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
			productDataBeanThree.setName("test");
			productDataBeanThree.setPrice(200.0);
			productDataBeanThree.setBasePrice(-1);
			productDataBeanThree.setUrl("https://test/1");
			productDataBeanThree.setDescription(null);
			productDataBeanThree.setKeyword("test");
			productDataBeanThree.setUrlForUpdate("https://test/");
			productDataBeanThree.setRealProductId("test");
			productDataBeanThree.setUpc(null);
			productDataBeanThree.setKeyword(null);
			productDataBeanThree.setPictureUrl("https://test/");
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBThree.setName("test");
			productDataBeanfromDBThree.setStatus(STATUS.W.toString());
			productDataBeanfromDBThree.setUrl("https://test/1");
			productDataBeanfromDBThree.setPictureUrl("www.test/");
			productDataBeanfromDBThree.setDescription(null);
			productDataBeanfromDBThree.setUrlForUpdate("https://test/");
			productDataBeanfromDBThree.setRealProductId("test");
			productDataBeanfromDBThree.setUpc(null);
			productDataBeanfromDBThree.setKeyword(null);
			productDataBeanfromDBThree.setPictureUrl("http://www.priceza.com/test.jpg");
			//----------------------------------------------------
			ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
			ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanThree.getPictureUrl(), "http://www.priceza.com/test.jpg");
			//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_CONF_ENABLE_UPDATE_PIC_CONF_FORCE_UPDATE_IMAGE_allconfig_is_false() throws SQLException {
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		BaseConfig.CONF_FORCE_UPDATE_IMAGE = false;
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		productDataBean.setKeyword(null);
		productDataBean.setPictureUrl("http://www.priceza.com/test.jpg");
		//#----------------  Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		productDataBeanfromDB.setKeyword(null);
		productDataBeanfromDB.setPictureUrl(null);
		//----------------------------------------------------
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getPictureUrl(), "http://www.priceza.com/test.jpg");
		//#################################################################
		
		
		//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanOne = new ProductDataBean();
			ProductDataBean productDataBeanfromDBOne = new ProductDataBean();
			productDataBeanOne.setName("test");
			productDataBeanOne.setPrice(200.0);
			productDataBeanOne.setBasePrice(-1);
			productDataBeanOne.setUrl("https://test/1");
			productDataBeanOne.setDescription(null);
			productDataBeanOne.setKeyword("test");
			productDataBeanOne.setUrlForUpdate("https://test/");
			productDataBeanOne.setRealProductId("test");
			productDataBeanOne.setUpc(null);
			productDataBeanOne.setKeyword(null);
			productDataBeanOne.setPictureUrl(null);
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBOne.setName("test");
			productDataBeanfromDBOne.setStatus(STATUS.W.toString());
			productDataBeanfromDBOne.setUrl("https://test/1");
			productDataBeanfromDBOne.setPictureUrl("www.test/");
			productDataBeanfromDBOne.setDescription(null);
			productDataBeanfromDBOne.setUrlForUpdate("https://test/");
			productDataBeanfromDBOne.setRealProductId("test");
			productDataBeanfromDBOne.setUpc(null);
			productDataBeanfromDBOne.setKeyword(null);
			productDataBeanfromDBOne.setPictureUrl("http://www.priceza.com/test.jpg");
			//----------------------------------------------------
			ParserResultBean parserResultBeanOne = ParserUpdate.analyzeProduct(productDataBeanOne,productDataBeanfromDBOne);
			ProductDataBean resultProductDataBeanOne = parserResultBeanOne.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanOne.getPictureUrl(), "http://www.priceza.com/test.jpg");
			//#################################################################
			
			//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanTwo = new ProductDataBean();
			ProductDataBean productDataBeanfromDBTwo = new ProductDataBean();
			productDataBeanTwo.setName("test");
			productDataBeanTwo.setPrice(200.0);
			productDataBeanTwo.setBasePrice(-1);
			productDataBeanTwo.setUrl("https://test/1");
			productDataBeanTwo.setDescription(null);
			productDataBeanTwo.setKeyword("test");
			productDataBeanTwo.setUrlForUpdate("https://test/");
			productDataBeanTwo.setRealProductId("test");
			productDataBeanTwo.setUpc(null);
			productDataBeanTwo.setKeyword(null);
			productDataBeanTwo.setPictureUrl("https://test/");
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBTwo.setName("test");
			productDataBeanfromDBTwo.setStatus(STATUS.W.toString());
			productDataBeanfromDBTwo.setUrl("https://test/1");
			productDataBeanfromDBTwo.setPictureUrl("www.test/");
			productDataBeanfromDBTwo.setDescription(null);
			productDataBeanfromDBTwo.setUrlForUpdate("https://test/");
			productDataBeanfromDBTwo.setRealProductId("test");
			productDataBeanfromDBTwo.setUpc(null);
			productDataBeanfromDBTwo.setKeyword(null);
			productDataBeanfromDBTwo.setPictureUrl(null);
			//----------------------------------------------------
			ParserResultBean parserResultBeanTwo = ParserUpdate.analyzeProduct(productDataBeanTwo,productDataBeanfromDBTwo);
			ProductDataBean resultProductDataBeanTwo = parserResultBeanTwo.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanTwo.getPictureUrl(), null);
			//#################################################################
			
			//#################################################################
			//#----------------  Bean DB ------------------------- 
			ProductDataBean productDataBeanThree = new ProductDataBean();
			ProductDataBean productDataBeanfromDBThree = new ProductDataBean();
			productDataBeanThree.setName("test");
			productDataBeanThree.setPrice(200.0);
			productDataBeanThree.setBasePrice(-1);
			productDataBeanThree.setUrl("https://test/1");
			productDataBeanThree.setDescription(null);
			productDataBeanThree.setKeyword("test");
			productDataBeanThree.setUrlForUpdate("https://test/");
			productDataBeanThree.setRealProductId("test");
			productDataBeanThree.setUpc(null);
			productDataBeanThree.setKeyword(null);
			productDataBeanThree.setPictureUrl("https://test/");
			//#----------------  Bean DB -------------------------
			productDataBeanfromDBThree.setName("test");
			productDataBeanfromDBThree.setStatus(STATUS.W.toString());
			productDataBeanfromDBThree.setUrl("https://test/1");
			productDataBeanfromDBThree.setPictureUrl("www.test/");
			productDataBeanfromDBThree.setDescription(null);
			productDataBeanfromDBThree.setUrlForUpdate("https://test/");
			productDataBeanfromDBThree.setRealProductId("test");
			productDataBeanfromDBThree.setUpc(null);
			productDataBeanfromDBThree.setKeyword(null);
			productDataBeanfromDBThree.setPictureUrl("http://www.priceza.com/test.jpg");
			//----------------------------------------------------
			ParserResultBean parserResultBeanThree = ParserUpdate.analyzeProduct(productDataBeanThree,productDataBeanfromDBThree);
			ProductDataBean resultProductDataBeanThree = parserResultBeanThree.getPdb();
			//############################  Result  #####################################
			assertEquals(resultProductDataBeanThree.getPictureUrl(), "http://www.priceza.com/test.jpg");
			//#################################################################
	}
	
	@Test
	public void analyzeproduct_parameter2bean_is_expire() throws SQLException {
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		BaseConfig.CONF_FORCE_UPDATE_IMAGE = false;
		//#################################################################
		//#----------------  Bean DB ------------------------- 
		ProductDataBean productDataBean = new ProductDataBean();
		ProductDataBean productDataBeanfromDB = new ProductDataBean();
		productDataBean.setName("test");
		productDataBean.setPrice(200.0);
		productDataBean.setBasePrice(-1);
		productDataBean.setUrl("https://test/1");
		productDataBean.setDescription(null);
		productDataBean.setKeyword("test");
		productDataBean.setUrlForUpdate("https://test/");
		productDataBean.setRealProductId("test");
		productDataBean.setUpc(null);
		productDataBean.setKeyword(null);
		productDataBean.setPictureUrl("http://www.priceza.com/test.jpg");
		productDataBean.setExpire(true);
	
		//#----------------  Bean DB -------------------------
		productDataBeanfromDB.setName("test");
		productDataBeanfromDB.setStatus(STATUS.W.toString());
		productDataBeanfromDB.setUrl("https://test/1");
		productDataBeanfromDB.setPictureUrl("www.test/");
		productDataBeanfromDB.setDescription(null);
		productDataBeanfromDB.setUrlForUpdate("https://test/");
		productDataBeanfromDB.setRealProductId("test");
		productDataBeanfromDB.setUpc(null);
		productDataBeanfromDB.setKeyword(null);
		productDataBeanfromDB.setPictureUrl(null);
		productDataBeanfromDB.setExpire(true);
		//----------------------------------------------------
		ParserResultBean parserResultBean = ParserUpdate.analyzeProduct(productDataBean,productDataBeanfromDB);
		ProductDataBean resultProductDataBean = parserResultBean.getPdb();
		//############################  Result  #####################################
		assertEquals(resultProductDataBean.getPictureUrl(), "http://www.priceza.com/test.jpg");
		//#################################################################
	
	}
	
	@Test
	public void checkPrice_have_config_CONF_UPDATE_PRICE_PERCENT_greaterthan_0() {
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 100;
		assertEquals(false, ParserUpdate.checkPrice(null, 0, 0));
		assertEquals(true, ParserUpdate.checkPrice(null, 500, 100));
		assertEquals(true, ParserUpdate.checkPrice(null, 5000, 10000));
		assertEquals(false, ParserUpdate.checkPrice(new ProductDataBean(), 10, 500));
		assertEquals(false, ParserUpdate.checkPrice(new ProductDataBean(), 10, BotUtil.CREDIT_BASE_PRICE-10));
		assertEquals(true, ParserUpdate.checkPrice(new ProductDataBean(), 10, BotUtil.CREDIT_BASE_PRICE+10));
		assertEquals(true, ParserUpdate.checkPrice(new ProductDataBean(), BotUtil.CREDIT_BASE_PRICE-10, 500));
		assertEquals(true, ParserUpdate.checkPrice(new ProductDataBean(), BotUtil.CREDIT_BASE_PRICE+10, 500));
	}
	
	@Test
	public void checkPrice_have_config_CONF_UPDATE_PRICE_PERCENT_lessthan_0() {
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = -1;
		assertEquals(false, ParserUpdate.checkPrice(null, 0, 0));
		assertEquals(true, ParserUpdate.checkPrice(null, 500, 100));
		assertEquals(true, ParserUpdate.checkPrice(null, 5000, 10000));
		assertEquals(true, ParserUpdate.checkPrice(new ProductDataBean(), 10, 500));
	}
	
	@Test
	public void checkBasePrice_have_config_CONF_UPDATE_PRICE_PERCENT_greaterthan_0() {
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		assertEquals(true, ParserUpdate.checkBasePrice(null, 5000, 10000,1000));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, 10000,1000));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 10000,1000));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 1,-1));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 100, 1,1));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 500,10000));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 100, 100,1));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, -500,10000));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 500,BotUtil.CONTACT_PRICE+10));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 9999999,10000));

	}
	
	@Test
	public void checkBasePrice_have_config_CONF_UPDATE_PRICE_PERCENT_lessthan_0() {
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = false;
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, 10000,1000));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, 1,-1));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 0, 9999999,10000));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, 1,BotUtil.CONTACT_PRICE));
		assertEquals(false, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, 1,BotUtil.CONTACT_PRICE-1));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, BotUtil.CREDIT_BASE_PRICE-10,BotUtil.CONTACT_PRICE));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, BotUtil.CREDIT_BASE_PRICE+10,BotUtil.CONTACT_PRICE));
		assertEquals(true, ParserUpdate.checkBasePrice(new ProductDataBean(), 5000, BotUtil.CREDIT_BASE_PRICE+10,BotUtil.CONTACT_PRICE-1));
	}
}
