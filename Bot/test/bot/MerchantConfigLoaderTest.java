package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import bean.MerchantConfigBean;
import bean.MerchantConfigBean.FIELD;
import bean.ParserConfigBean;
import db.MerchantConfigDB;
import db.ParserConfigDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import feed.TypeFeedCrawlerInterface;
import loader.MerchantConfigLoader;
import unittest.MockObjectHolder;

public class  MerchantConfigLoaderTest {
	@Mock DatabaseManager databaseManager;
	@Mock MerchantConfigDB merchantConfigDB;
	@Mock ParserConfigDB parserConfigDB;
	
	@InjectMocks MerchantConfigBean merchantConfigBean;
	@InjectMocks ParserConfigBean parserConfigBean;
	@InjectMocks Properties prop;
	int merchantId = 1;
	List<MerchantConfigBean> merchantConfigBeanList = new ArrayList<MerchantConfigBean>();
	List<MerchantConfigBean> merchantConfigListEmpty = new ArrayList<MerchantConfigBean>();
	List<ParserConfigBean> parserConfigBeanList = new ArrayList<ParserConfigBean>();
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		when(databaseManager.getMerchantConfigDB()).thenReturn(merchantConfigDB);
		when(databaseManager.getParserConfigDB()).thenReturn(parserConfigDB);
		
		BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       = 24 ;        
		BaseConfig.CONF_MAX_DEPTH                   = 5 ;         
		BaseConfig.CONF_PARSER_CLASS                = null ;      
		BaseConfig.CONF_INIT_CRAWLER_CLASS          = null ;      
		BaseConfig.CONF_URL_CRAWLER_CLASS           = null ;      
		BaseConfig.CONF_FEED_CRAWLER_CLASS          = null ;      
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        = 7 ;         
		BaseConfig.CONF_FEED_CRAWLER_CLASS          = null ;      
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM       = 1 ;         
		BaseConfig.CONF_DUE_PARSER_THREAD_NUM       = 1 ;         
		BaseConfig.CONF_ENABLE_COOKIES              = false ;     
		BaseConfig.CONF_IGNORE_HTTP_ERROR           = false ;     
		BaseConfig.CONF_SKIP_ENCODE_URL             = false ;     
		BaseConfig.CONF_FORCE_DELETE                = false ;     
		BaseConfig.CONF_ENABLE_UPDATE_PIC           = false ;     
		BaseConfig.CONF_DUE_SLEEP_TIME              = 0 ;         
		BaseConfig.CONF_WCE_SLEEP_TIME              = 0 ;         
		BaseConfig.CONF_USER_OLD_PRODUCT_URL        = false ;     
		BaseConfig.CONF_IMAGE_PARSER_CLASS          = null ;      
		BaseConfig.CONF_ENABLE_LARGE_IMAGE          = true ;      
		BaseConfig.CONF_FORCE_UPDATE_IMAGE          = false ;     
		BaseConfig.CONF_ENABLE_UPDATE_DESC          = false ;     
		BaseConfig.CONF_ENABLE_SMALL_IMAGE          = false ;     
		BaseConfig.CONF_UPDATE_PRICE_PERCENT        = 0 ;         
		BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     = false ;     
		BaseConfig.CONF_DELETE_LIMIT                = 200 ;       
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD        = false ;     
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY       = false ;     
		BaseConfig.CONF_FORCE_UPDATE_LIMIT          = false ;     
		BaseConfig.CONF_DISBLE_PARSER_COOKIES       = false ;     
		BaseConfig.CONF_DISABLE_DUE_CONTINUE        = false ;     
		BaseConfig.CONF_PARSER_CHARSET              = "UTF-8" ;   
		BaseConfig.CONF_ENABLE_UPDATE_URL           = false ;     
		BaseConfig.CONF_FEED_CRAWLER_PARAM          = null ;      
		BaseConfig.CONF_WCE_PARSER_CLASS            = null ;      
		BaseConfig.CONF_DUE_PARSER_CLASS            = null ;      
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    = false ;     
		BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       = false ;     
		BaseConfig.CONF_MAX_RUNTIME                 = 24 ;        
		BaseConfig.CONF_FEED_USERNAME               = null ;      
		BaseConfig.CONF_FEED_PASSWORD               = null ;      
		BaseConfig.CONF_MERCHANT_NAME               = null ;      
		BaseConfig.CONF_CURRENCY_RATE               = 1.0 ; 
		BaseConfig.CONF_ENABLE_PROXY                = false ;     
		BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      = null ;      
		BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     = null ;      
		BaseConfig.FEED_TYPE_CONF                   = null ;      
		
	}

	
	@Test
	public void loadMerchantConfig_list_is_return_empty() {
		try {
			when(merchantConfigDB.findMerchantConfig(1)).thenReturn(merchantConfigListEmpty);
			MerchantConfigLoader.loadMerchantConfig();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void loadMerchantConfig_is_blank_have_feed_xml_mega_tag () {
		try {
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField("test");
			merchantConfigBean.setValue("test");
			merchantConfigBeanList.add(merchantConfigBean);
			
			parserConfigBean.setId(1);
			parserConfigBean.setMerchantId(1);
			parserConfigBean.setDepth(1);
			parserConfigBean.setFieldType("test");
			parserConfigBean.setFilterType("test");
			parserConfigBean.setValue("test");
			parserConfigBeanList.add(parserConfigBean);
			
			
			BaseConfig.MERCHANT_ID = 1;
			BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = "";
			when(merchantConfigDB.findMerchantConfig(1)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(1)).thenReturn(parserConfigBeanList);
			MerchantConfigLoader.loadMerchantConfig();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void loadMerchantConfig_BaseConfig_FEED_TYPE_CONF_have_feedType() {
		try {
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField("test");
			merchantConfigBean.setValue("test");
			
			merchantConfigBean.setId(2);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField(FIELD.feedType.toString());
			merchantConfigBean.setValue("xml");
			merchantConfigBeanList.add(merchantConfigBean);
			
			parserConfigBean.setId(1);
			parserConfigBean.setMerchantId(1);
			parserConfigBean.setDepth(1);
			parserConfigBean.setFieldType("test");
			parserConfigBean.setFilterType("test");
			parserConfigBean.setValue("test");
			parserConfigBeanList.add(parserConfigBean);
			
			BaseConfig.MERCHANT_ID = 1;
			BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = "product";
			
			when(merchantConfigDB.findMerchantConfig(1)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(1)).thenReturn(parserConfigBeanList);
			MerchantConfigLoader.loadMerchantConfig();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void loadMerchantConfig_BaseConfig_FEED_TYPE_CONF_Exception() {
		try {
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField("test");
			merchantConfigBean.setValue("test");
			
			parserConfigBean.setId(1);
			parserConfigBean.setMerchantId(1);
			parserConfigBean.setDepth(1);
			parserConfigBean.setFieldType("test");
			parserConfigBean.setFilterType("test");
			parserConfigBean.setValue("test");
			parserConfigBeanList.add(parserConfigBean);
			
			BaseConfig.MERCHANT_ID = 1;
			BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = "product";
			when(merchantConfigDB.findMerchantConfig(1)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(1)).thenReturn(parserConfigBeanList);
			MerchantConfigLoader.loadMerchantConfig();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_wce_runtimetype_is_allconfig_is_true_and_parserclass_is_not_DefaultFeedHTMLParser () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "WCE";
			BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.wceClearProductUrl, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateKeyword, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateCategory, "false", BaseConfig.MERCHANT_ID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_wce_runtimetype_is_allconfig_is_true_and_parserclass_is_DefaultFeedHTMLParser () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "WCE";
			BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.wceClearProductUrl, "false", BaseConfig.MERCHANT_ID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_wce_runtimetype_is_allconfig_is_false_and_parserclass_is_not_DefaultFeedHTMLParser () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "WCE";
			BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_wce_runtimetype_is_allconfig_is_false_and_parserclass_is__DefaultFeedHTMLParser () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "WCE";
			BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void updateMerchantConfig_due_runtimetype_is_allconfig_is_true_and_parserclass_is_DefaultFeedHTMLParser_and_feedcrawlerclass_is_null () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceDelete, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateImage, "false", BaseConfig.MERCHANT_ID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_true_and_parserclass_is_not_DefaultFeedHTMLParser_and_feedcrawlerclass_is_null () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceDelete, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateImage, "false", BaseConfig.MERCHANT_ID);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_true_and_parserclass_is_DefaultFeedHTMLParser_and_feedcrawlerclass_is_not_null () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = "test";
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceDelete, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateImage, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateKeyword, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateCategory, "false", BaseConfig.MERCHANT_ID);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void updateMerchantConfig_due_runtimetype_is_empty() {
		try {
			BaseConfig.RUNTIME_TYPE = "";
			MerchantConfigLoader.updateMerchantConfig();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_true_and_parserclass_is_not_DefaultFeedHTMLParser_and_feedcrawlerclass_is_not_null () {
		try {
			Boolean setBoolean = true;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = "test";
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceDelete, "false", BaseConfig.MERCHANT_ID);
			Mockito.verify(merchantConfigDB).updateConfigValue(FIELD.forceUpdateImage, "false", BaseConfig.MERCHANT_ID);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_false_and_parserclass_is_DefaultFeedHTMLParser_and_feedcrawlerclass_is_null () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_false_and_parserclass_is_not_DefaultFeedHTMLParser_and_feedcrawlerclass_is_null () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_false_and_parserclass_is_DefaultFeedHTMLParser_and_feedcrawlerclass_is_not_null () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = "test";
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
			MerchantConfigLoader.updateMerchantConfig();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void updateMerchantConfig_due_runtimetype_is_all_config_is_false_and_parserclass_is_not_DefaultFeedHTMLParser_and_feedcrawlerclass_is_not_null () {
		try {
			Boolean setBoolean = false;
			BaseConfig.RUNTIME_TYPE = "DUE";
			BaseConfig.CONF_FORCE_DELETE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_IMAGE = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_KEYWORD = setBoolean;
			BaseConfig.CONF_FORCE_UPDATE_CATEGORY = setBoolean;
			BaseConfig.CONF_FEED_CRAWLER_CLASS = "test";
			BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.test";
			MerchantConfigLoader.updateMerchantConfig();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private List<MerchantConfigBean> mockMerchantConfigList(String value){
        List<MerchantConfigBean> merchantConfigBeanList = new ArrayList<>();
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.hourRate.toString())                        ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.maxDepth.toString())                        ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.parserClass.toString())                     ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.initCrawlerClass.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.urlCrawlerClass.toString())                 ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedCrawlerClass.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.productUpdateLimit.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedCrawlerClass.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.wceParserThreadNum.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.dueParserThreadNum.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableCookies.toString())                   ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.ignoreHttpError.toString())                 ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.skipEncodeURL.toString())                   ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.forceDelete.toString())                     ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableUpdatePic.toString())                 ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.dueSleepTime.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.wceSleepTime.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.userOldProductUrl.toString())               ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.imageParserClass.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableLargeImage.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.forceUpdateImage.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableUpdateDesc.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableSmallImage.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.updatePricePercent.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableVeryLargeImage.toString())        	; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.deleteLimit.toString())                     ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.forceUpdateKeyword.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.forceUpdateCategory.toString())             ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.forceUpdateLimit.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.disableParserCookies.toString())        	; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.disableDUEContinue.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.parserCharset.toString())                   ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableUpdateUrl.toString())                 ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedCrawlerParam.toString())                ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.wceParserClass.toString())                  ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.dueParserClass.toString())                  ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableUpdateBasePrice.toString())        	; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.wceClearProductUrl.toString())              ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.maxRuntime.toString())                      ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedUsername.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedPassword.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.merchantName.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.currencyRate.toString())                    ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.enableProxy.toString())                     ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedXmlMegaTag.toString())                  ; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        merchantConfigBean = new MerchantConfigBean(); merchantConfigBean.setId(1); merchantConfigBean.setMerchantId(merchantId); merchantConfigBean.setField(FIELD.feedType.toString())				  		; merchantConfigBean.setValue(value); merchantConfigBeanList.add(merchantConfigBean);
        return merchantConfigBeanList;
}

	@Test
	public void loadMerchantConfig_all_null(){
		String value = null;
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , null );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , null );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , null );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , true );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "UTF-8" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , null );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , null );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , null );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , null );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , null );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , null );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 1.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , null );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , null );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
			
			
		} catch (Exception e) {
		}
	}
	
	@Test
	public void loadMerchantConfig_all_emptyvalue(){
		String value = "";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
		
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , null );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , null );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , null );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , null );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , true );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "UTF-8" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , null );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , null );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , null );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , null );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , null );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , null );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 1.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , null );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , null );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Test
	public void loadMerchantConfig_all_boolean_true(){
		String value = "true";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
		
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , "true" );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , "true" );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , "true" );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "true" );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "true" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , true );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , true );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , true );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , true );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , true );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , true );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , "true" );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , true );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , true );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , true );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , true );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , true );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , true );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , true );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , true );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , true );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , true );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "true" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , true );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , "true" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , "true" );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , "true" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , true );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , true );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , "true" );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , "true" );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , "true" );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 1.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , true );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , "<true>" );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , "</true>" );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	@Test
	public void loadMerchantConfig_all_boolean_false(){
		String value = "false";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
		
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , "false" );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , "false" );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , "false" );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "false" );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "false" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , "false" );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "false" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , "false" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , "false" );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , "false" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , "false" );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , "false" );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , "false" );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 1.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , "<false>" );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , "</false>" );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test
	public void loadMerchantConfig_all_string_havetext(){
		String value = "text";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
		
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , "text" );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , "text" );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , "text" );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "text" );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "text" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , "text" );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "text" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , "text" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , "text" );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , "text" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , "text" );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , "text" );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , "text" );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 1.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , "<text>" );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , "</text>" );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test
	public void loadMerchantConfig_all_int(){
		String value = "444";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();
		
			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 444 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 444 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , "444" );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , "444" );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , "444" );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "444" );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 444 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "444" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 444 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 444 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 444 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 444 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , "444" );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 444 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 444 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "444" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , "444" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , "444" );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , "444" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 444 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , "444" );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , "444" );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , "444" );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 444.0 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , "<444>" );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , "</444>" );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
		
	}
	
	@Test
	public void loadMerchantConfig_all_double(){
		String value = "999.98";
		try {
			BaseConfig.MERCHANT_ID = merchantId;

			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(mockMerchantConfigList(value));
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);

		
			MerchantConfigLoader.loadMerchantConfig();

			assertEquals(BaseConfig.CONF_BOT_QUALITY_HOUR_RATE       , 24 );
			assertEquals(BaseConfig.CONF_MAX_DEPTH                   , 5 );
			assertEquals(BaseConfig.CONF_PARSER_CLASS                , "999.98" );
			assertEquals(BaseConfig.CONF_INIT_CRAWLER_CLASS          , "999.98" );
			assertEquals(BaseConfig.CONF_URL_CRAWLER_CLASS           , "999.98" );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "999.98" );
			assertEquals(BaseConfig.CONF_PRODUCT_UPADTE_LIMIT        , 7 );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_CLASS          , "999.98" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_DUE_PARSER_THREAD_NUM       , 1 );
			assertEquals(BaseConfig.CONF_ENABLE_COOKIES              , false );
			assertEquals(BaseConfig.CONF_IGNORE_HTTP_ERROR           , false );
			assertEquals(BaseConfig.CONF_SKIP_ENCODE_URL             , false );
			assertEquals(BaseConfig.CONF_FORCE_DELETE                , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_PIC           , false );
			assertEquals(BaseConfig.CONF_DUE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_WCE_SLEEP_TIME              , 0 );
			assertEquals(BaseConfig.CONF_USER_OLD_PRODUCT_URL        , false );
			assertEquals(BaseConfig.CONF_IMAGE_PARSER_CLASS          , "999.98" );
			assertEquals(BaseConfig.CONF_ENABLE_LARGE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_IMAGE          , false );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_DESC          , false );
			assertEquals(BaseConfig.CONF_ENABLE_SMALL_IMAGE          , false );
			assertEquals(BaseConfig.CONF_UPDATE_PRICE_PERCENT        , 0 );
			assertEquals(BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE     , false );
			assertEquals(BaseConfig.CONF_DELETE_LIMIT                , 200 );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_KEYWORD        , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_CATEGORY       , false );
			assertEquals(BaseConfig.CONF_FORCE_UPDATE_LIMIT          , false );
			assertEquals(BaseConfig.CONF_DISBLE_PARSER_COOKIES       , false );
			assertEquals(BaseConfig.CONF_DISABLE_DUE_CONTINUE        , false );
			assertEquals(BaseConfig.CONF_PARSER_CHARSET              , "999.98" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_URL           , false );
			assertEquals(BaseConfig.CONF_FEED_CRAWLER_PARAM          , "999.98" );
			assertEquals(BaseConfig.CONF_WCE_PARSER_CLASS            , "999.98" );
			assertEquals(BaseConfig.CONF_DUE_PARSER_CLASS            , "999.98" );
			assertEquals(BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE    , false );
			assertEquals(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL       , false );
			assertEquals(BaseConfig.CONF_MAX_RUNTIME                 , 24 );
			assertEquals(BaseConfig.CONF_FEED_USERNAME               , "999.98" );
			assertEquals(BaseConfig.CONF_FEED_PASSWORD               , "999.98" );
			assertEquals(BaseConfig.CONF_MERCHANT_NAME               , "999.98" );
			assertEquals(BaseConfig.CONF_CURRENCY_RATE               , 999.98 , 1.0 );
			assertEquals(BaseConfig.CONF_ENABLE_PROXY                , false );
			assertEquals(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG      , "<999.98>" );
			assertEquals(BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG     , "</999.98>" );
			assertEquals(BaseConfig.FEED_TYPE_CONF                   , null );
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
		
	@Test
	public void loadMerchantConfig_feedtype_enum_csv(){
		try {
			BaseConfig.MERCHANT_ID = 1;
			
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField(FIELD.feedType.toString());
			merchantConfigBean.setValue("csv");
			merchantConfigBeanList.add(merchantConfigBean);
			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);
			
			MerchantConfigLoader.loadMerchantConfig();
			assertEquals(BaseConfig.FEED_TYPE_CONF, TypeFeedCrawlerInterface.FEED_TYPE.CSV );
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test
	public void loadMerchantConfig_feedtype_enum_xml(){
		try {
			BaseConfig.MERCHANT_ID = 1;
			
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField(FIELD.feedType.toString());
			merchantConfigBean.setValue("xml");
			merchantConfigBeanList.add(merchantConfigBean);
			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);
			
			MerchantConfigLoader.loadMerchantConfig();
			assertEquals(BaseConfig.FEED_TYPE_CONF, TypeFeedCrawlerInterface.FEED_TYPE.XML );
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	@Test
	public void loadMerchantConfig_feedtype_enum_json(){
		try {
			BaseConfig.MERCHANT_ID = 1;
			
			merchantConfigBean.setId(1);
			merchantConfigBean.setMerchantId(1);
			merchantConfigBean.setField(FIELD.feedType.toString());
			merchantConfigBean.setValue("json");
			merchantConfigBeanList.add(merchantConfigBean);
			
			when(merchantConfigDB.findMerchantConfig(merchantId)).thenReturn(merchantConfigBeanList);
			when(parserConfigDB.findParserConfig(merchantId)).thenReturn(parserConfigBeanList);
			
		
			MerchantConfigLoader.loadMerchantConfig();
			assertEquals(BaseConfig.FEED_TYPE_CONF, TypeFeedCrawlerInterface.FEED_TYPE.JSON );
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
	
}
