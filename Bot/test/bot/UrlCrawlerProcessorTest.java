package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;

import bean.PatternResultBean;
import bean.ProductUrlBean;
import bean.WorkloadBean;
import db.ProductUrlDB;
import db.WorkloadDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.UrlPatternManager;
import product.processor.ProductUrlReader;
import product.processor.WorkloadReader;
import unittest.MockInitCrawlerClass;
import unittest.MockObjectHolder;
import unittest.MockURLCrawlerClass;
import web.crawler.UrlCrawlerProcessor;

public class UrlCrawlerProcessorTest {
	
	private final static int MERCHANT_ID = 69;
	private final static int CATEGORY = 6969;
	private final static String KEYWORD = "Test";
	private final static String URL = "http://www.soccersuck.com";
	private final static String STATUS = "C";
	
	@Mock ProductUrlReader produtUrlReader; 
	@Mock DatabaseManager db;
	@Mock WorkloadReader workLoadReader;
	@Mock UrlPatternManager urlPattern;
	@Mock ProductUrlDB productURLDB;
	@Mock WorkloadDB  workLoadDB;
	@Mock CountDownLatch mockLatch;
	
	@InjectMocks UrlCrawlerProcessor urlCrawlerProcessor;
	@InjectMocks WorkloadBean workloadBean;
	@InjectMocks PatternResultBean patternResultBean;
	@InjectMocks PatternResultBean falseResultPattern;
	
	
	CountDownLatch normalLatch;
	ExecutorService workloadThread;

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		BaseConfig.THREAD_NUM = 1;
		MockObjectHolder.isMock = true;
		BaseConfig.MERCHANT_ID = MERCHANT_ID;
		MockObjectHolder.mockDatabaseManager = db;
		BaseConfig.CONF_WCE_SLEEP_TIME = 0;
		BaseConfig.CONF_MAX_DEPTH = 40;
		MockObjectHolder.productUrlReader = produtUrlReader;
		MockObjectHolder.workloadReader = workLoadReader;
		MockObjectHolder.urlPatternManager = urlPattern;
		BaseConfig.CONF_URL_CRAWLER_CLASS = "unittest.MockURLCrawlerClass";
		when(db.getWorkloadDB()).thenReturn(workLoadDB);
		when(db.getProductUrlDB()).thenReturn(productURLDB);
		MockURLCrawlerClass.command = "";
		MockInitCrawlerClass.command = "";
		BaseConfig.CONF_INIT_CRAWLER_CLASS = null;
		
	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		assertEquals(result.get(), true);
		verify(workLoadDB).insert(eq(MERCHANT_ID), eq(URL), eq(1), eq(CATEGORY), eq(KEYWORD), eq("W"));

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal_class_NotFound() throws Exception {

		boolean result = false;

		try {
			BaseConfig.CONF_URL_CRAWLER_CLASS = "ERROR_CRAWLER";
			workloadBean.setCategoryId(CATEGORY);
			workloadBean.setDepth(0);
			workloadBean.setKeyword(KEYWORD);
			workloadBean.setUrl(URL);
			workloadBean.setMerchantId(MERCHANT_ID);
			workloadBean.setStatus(STATUS);
			workloadBean.setRequestUrl(URL);

			patternResultBean.setCategoryId(CATEGORY);
			patternResultBean.setKeyword(KEYWORD);
			patternResultBean.setPass(true);

			falseResultPattern.setPass(false);

			normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
			workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
			urlCrawlerProcessor.setProperties(normalLatch);
		} catch (ClassNotFoundException e) {
			result = true;
		}
		assertEquals(result, true);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal_sql_exception() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), false);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal_trio_exception() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenAnswer(invocation -> { throw new ClassNotFoundException("1 Exception");}).thenAnswer(invocation -> { throw new InstantiationException("2 Exception");}).thenAnswer(invocation -> { throw new  IllegalAccessException("3 Exception");}).thenReturn(null); 
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), false);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal_other_exception() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenAnswer(invocation -> { throw new Exception("other Exception");}).thenReturn(null); 
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), false);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_normal_countDownLatch_exception() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Mockito.doThrow(new MockitoException("CountDownLatch Error")).when(mockLatch).countDown();
		urlCrawlerProcessor.setProperties(mockLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		normalLatch.countDown();
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), false);

	}
	
	@Test
	public void urlCrawlerProcees_error_crawl() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenThrow(new MockitoException("Insert Error"));
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProcees_normal_sleep_time() throws Exception {
		BaseConfig.CONF_WCE_SLEEP_TIME = 1000;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProcees_sleep_time_interupt_exception() throws Exception {
		boolean resultBoolean = false;
		
		BaseConfig.CONF_WCE_SLEEP_TIME = 3000;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		Thread.sleep(2000);
		result.cancel(true);
		try {
			assertEquals(result.get(), true);
		}catch (CancellationException e) {
			resultBoolean = true;
		}
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.isCancelled(), true);
		assertEquals(resultBoolean, true);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_more_than_max_depth() throws Exception {
		BaseConfig.CONF_MAX_DEPTH = 1;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(1);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);
		verify(workLoadDB).updateStatus(MERCHANT_ID , URL,bean.WorkloadBean.STATUS.C);

	}
	
	@Test
	public void urlCrawlerProceesor_normal_skip_encode() throws Exception {
		BaseConfig.CONF_SKIP_ENCODE_URL = true;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProceesor_normal_encode_error() throws Exception {
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProceesor_normal_not_foundInternalLink_not_macth_and_not_continue_or_drop() throws Exception {
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(workloadBean).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern).thenReturn(patternResultBean).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(falseResultPattern);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean).thenReturn(falseResultPattern).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean).thenReturn(patternResultBean).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProceesor_category_not_normal() throws Exception {
		workloadBean.setCategoryId(-1);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);


		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}
	
	@Test
	public void urlCrawlerProceesor_one_thread_picture_link() throws Exception {

		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);
		MockURLCrawlerClass.command = "pic";
		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch("http://www.testPicture.com/test.jpg")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop("http://www.testPicture.com/test.jpg")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);

	}

	 @Test
	public void urlCrawlerProceesor_one_thread_normal_init_crawler() throws Exception {

		BaseConfig.CONF_INIT_CRAWLER_CLASS = "unittest.MockInitCrawlerClass";
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		assertEquals(result.get(), true);
		workloadThread.shutdown();
		normalLatch.await();
		verify(workLoadDB).insert(eq(MERCHANT_ID), eq(URL), eq(1), eq(CATEGORY), eq(KEYWORD), eq("W"));

	}

	 @Test
	public void urlCrawlerProceesor_one_thread_normal_initCrawler_null() throws Exception {
		BaseConfig.CONF_INIT_CRAWLER_CLASS = "unittest.MockInitCrawlerClass";
		MockInitCrawlerClass.command = "null";
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), false);

	}

	@Test
	public void urlCrawlerProceesor_one_thread_normal_initCrawler_continueProcess_false() throws Exception {
		BaseConfig.CONF_INIT_CRAWLER_CLASS = "unittest.MockInitCrawlerClass";
		MockInitCrawlerClass.command = "continueProcessFalse";
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		Future<Boolean> result = workloadThread.submit(urlCrawlerProcessor);
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(result.get(), true);
		verify(workLoadDB).updateStatus(MERCHANT_ID, URL, bean.WorkloadBean.STATUS.C);
	}
	
	@Test
	public void urlCrawlerProceesor_many_thread_normal_complete() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null).thenReturn(workloadBean).thenReturn(null).thenReturn(workloadBean).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		List<Future<Boolean>> resultList = new ArrayList<Future<Boolean>>();
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			resultList.add(workloadThread.submit(urlCrawlerProcessor));
		}
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(resultList.get(0).get(), true);
		assertEquals(resultList.get(1).get(), true);
		assertEquals(resultList.get(2).get(), true);
		verify(workLoadDB,Mockito.times(3)).insert(eq(MERCHANT_ID), eq(URL), eq(1), eq(CATEGORY), eq(KEYWORD), eq("W"));

	}
	
	@Test
	public void urlCrawlerProceesor_many_thread_normal_some() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		when(workLoadReader.readData()).thenReturn(workloadBean).thenReturn(null).thenReturn(workloadBean).thenReturn(null).thenThrow(new MockitoException("Thread 3 Error")).thenReturn(null);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		List<Future<Boolean>> resultList = new ArrayList<Future<Boolean>>();
		
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			Thread.sleep(3000);
			resultList.add(workloadThread.submit(urlCrawlerProcessor));
		}
		workloadThread.shutdown();
		normalLatch.await();

		assertEquals(resultList.get(0).get(), true);
		assertEquals(resultList.get(1).get(), true);
		assertEquals(resultList.get(2).get(), false);
	}
	
	@Test
	public void urlCrawlerProceesor_many_thread_error() throws Exception {
		BaseConfig.THREAD_NUM = 3;
		workloadBean.setCategoryId(CATEGORY);
		workloadBean.setDepth(0);
		workloadBean.setKeyword(KEYWORD);
		workloadBean.setUrl(URL);
		workloadBean.setMerchantId(MERCHANT_ID);
		workloadBean.setStatus(STATUS);
		workloadBean.setRequestUrl(URL);

		patternResultBean.setCategoryId(CATEGORY);
		patternResultBean.setKeyword(KEYWORD);
		patternResultBean.setPass(true);

		falseResultPattern.setPass(false);

		normalLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
		workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		urlCrawlerProcessor.setProperties(normalLatch);
		when(urlPattern.isPatternContinue(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl())).thenReturn(patternResultBean);
		when(urlPattern.isPatternMatch(workloadBean.getUrl() + "?product_id=1")).thenReturn(patternResultBean);
		when(urlPattern.isPatternDrop(workloadBean.getUrl() + "?product_id=1")).thenReturn(falseResultPattern);
		when(productURLDB.insertProductUrl(any(ProductUrlBean.class))).thenReturn(1);
		when(urlPattern.processRemoveQuery(workloadBean.getUrl() + "?product_id=1")).thenReturn(URL);
		List<Future<Boolean>> resultList = new ArrayList<Future<Boolean>>();
		when(workLoadReader.readData()).thenThrow(new MockitoException("Thread Error")).thenReturn(null).thenThrow(new MockitoException("Thread Error")).thenReturn(null).thenThrow(new MockitoException("Thread Error")).thenReturn(null);
		for(int i = 0;i<BaseConfig.THREAD_NUM;i++) {
			Thread.sleep(3000);
			resultList.add(workloadThread.submit(urlCrawlerProcessor));
		}
		workloadThread.shutdown();
		normalLatch.await();
		assertEquals(resultList.get(0).get(), false);
		assertEquals(resultList.get(1).get(), false);
		assertEquals(resultList.get(2).get(), false);

	}
}
