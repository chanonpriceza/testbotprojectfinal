package bot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ProductUrlBean;
import bean.ReportBean;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;
import product.processor.ParserCrawler;
import unittest.MockImageParser;
import unittest.MockObjectHolder;
import utils.BotUtil;

public class ParserCrawlerTest {
	
	private static final String URL = "http://example.com";
	private static final int MERCHANT_ID = 1;
	private static final String KEYWORD = "test";
	private static final int CAT = 14;
	private static final String STATE = "S";
	private static final String STATUS = "C";
	private static final String NAME = "TEST";
	private static final int PRICE = 99;
	private static final int BASE_PRICE = 200;
	private static final String DESC = "TEST";
	private static final String PIC_URL = "http://example.com/test.jpg";
	
	
	@Mock
	private static DatabaseManager databaseManager;
	@Mock
	private static ProductUrlDB productUrlDB;
	@Mock
	private static ProductDataDB productDataDB ;
	@Mock
	private static SendDataDB sendDataDB;
	@Mock
	private static ReportManager reportManager;

	@InjectMocks
	private static ReportBean  report;
	
	private static ProductDataBean[] dataBeanList;
	@InjectMocks
	private static ProductUrlBean urlBean;
	@InjectMocks
	private static ProductDataBean pdb;
	@InjectMocks
	private static ProductDataBean oldBean;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		when(databaseManager.getSendDataDB()).thenReturn(sendDataDB);
		when(databaseManager.getProductUrlDB()).thenReturn(productUrlDB);
		when(reportManager.getReport()).thenReturn(report);
		
		dataBeanList = new ProductDataBean[1];
		pdb.setName(NAME);
		pdb.setPrice(PRICE);
		pdb.setCategoryId(CAT);
		pdb.setUrl(URL);
		pdb.setUrlForUpdate(URL);
		pdb.setDescription(DESC);
		pdb.setPictureUrl(PIC_URL);
		pdb.setKeyword(KEYWORD);
		pdb.setStatus(STATUS);
		pdb.setExpire(false);
		pdb.setBasePrice(0);
		pdb.setRealProductId(null);
		pdb.setUpc(null);
		
		oldBean.setName(NAME);
		oldBean.setPrice(PRICE);
		oldBean.setCategoryId(CAT);
		oldBean.setUrl(URL);
		oldBean.setUrlForUpdate(URL);
		oldBean.setDescription(DESC);
		oldBean.setPictureUrl(PIC_URL);
		oldBean.setKeyword(KEYWORD);
		oldBean.setStatus(STATUS);
		oldBean.setExpire(false);
		oldBean.setBasePrice(0);
		oldBean.setRealProductId(null);
		oldBean.setUpc(null);
		
		dataBeanList[0] = pdb;
		urlBean.setMerchantId(MERCHANT_ID);
		urlBean.setKeyword(KEYWORD);
		urlBean.setCategoryId(CAT);
		urlBean.setState(STATE);
		urlBean.setStatus(STATUS);
		urlBean.setUrl(URL);
		
		
		BaseConfig.MERCHANT_ID = MERCHANT_ID;
		BaseConfig.CONF_CURRENCY_RATE = 1;
		BaseConfig.CONF_ENABLE_UPDATE_URL = false;
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 0;
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = false;
		BaseConfig.CONF_ENABLE_UPDATE_DESC = false;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY =false; 
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD=false;
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		BaseConfig.IMAGE_PARSER = new MockImageParser();
	}
	
	@Test
	public void analyzeProductList_test_normal() throws SQLException {
		when(productDataDB.insertProductData(Mockito.any())).thenReturn(1);
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		assertNotNull(result);
		assertEquals(result.size(),1);
	}
	
	@Test
	public void analyzeProductList_resultBean_is_null() throws SQLException {
		List<ParserResultBean> resultUrlBeanNull = ParserCrawler.analyzeProductList(dataBeanList,null);
		List<ParserResultBean> result2DataBeanNull =ParserCrawler.analyzeProductList(null,urlBean);
		List<ParserResultBean> bothNull = ParserCrawler.analyzeProductList(null,null);
		assertNull(resultUrlBeanNull);
		assertNull(result2DataBeanNull);
		assertNull(bothNull);
	}
	
	@Test
	public void analyzeProductList_test_expire() throws SQLException {
		pdb.setExpire(true);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		assertNotNull(result);
		ParserResultBean ps = result.get(0);
		assertEquals(ps.isExpire(),true);
	}
	
	@Test
	public void analyzeProductList_name_price_not_pass() throws SQLException {
		pdb.setName("");
		pdb.setPrice(0);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		assertEquals(result.size(),0);
	}
	
	@Test
	public void analyzeProductList_url_empty() throws SQLException {
		pdb.setUrl(null);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		assertEquals(ps.getPdb().getUrl(),urlBean.getUrl());
	}
	
	@Test
	public void analyzeProductList_urlForUpdate_empty() throws SQLException {
		pdb.setUrlForUpdate(null);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		assertEquals(ps.getPdb().getUrl(),pdb.getUrl());
	}
	
	@Test
	public void analyzeProductList_currency_multiple() throws SQLException {
		//----------------------------รัน analyzeProductList รันแล้ว currency != 1.0 แล้วbase price != 0-----------------------
		BaseConfig.CONF_CURRENCY_RATE = 2.0;
		pdb.setBasePrice(BASE_PRICE);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		double price = PRICE*BaseConfig.CONF_CURRENCY_RATE;
		assertEquals(pdb.getPrice(), price,0);
		assertEquals(pdb.getBasePrice(), BASE_PRICE*BaseConfig.CONF_CURRENCY_RATE,0);
		//--------------------------------Case ที่ BasePrice = 0---------------------------
		pdb.setBasePrice(0);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result2 = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps2 = result2.get(0);
		ProductDataBean pdb2 = ps2.getPdb();
		assertEquals(pdb2.getBasePrice(), 0,0);
	}
	
	@Test
	public void analyzeProductList_price_more_than_contract_price() throws SQLException {
		double priceMore = 999999999999999999.0;
		pdb.setPrice(priceMore);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		assertEquals(pdb.getPrice(),BotUtil.CONTACT_PRICE,0);
	}
	
	@Test
	public void analyzeProductList_basePrice_less_than_price() throws SQLException {
		pdb.setBasePrice(15);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		assertEquals(pdb.getBasePrice(),PRICE,0);
	}
	
	@Test
	public void analyzeProductList_cat_change() throws SQLException {
		pdb.setCategoryId(0);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		assertEquals(pdb.getCategoryId(),CAT);
		//---------------------- Cat อันเก่า !=0 -----------------------------
		pdb.setCategoryId(99);
		dataBeanList[0] = pdb;
		List<ParserResultBean> result2 = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps2 = result2.get(0);
		ProductDataBean pdb2 = ps2.getPdb();
		assertEquals(pdb2.getCategoryId(),99);
	}
	
	@Test
	public void analyzeProductList_keyword_change() throws SQLException {
		pdb.setKeyword("");
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		assertEquals(pdb.getKeyword(),KEYWORD);
	}
	
	@Test
	public void analyzeProductList_base_validate_imageUrl_not_pass() throws SQLException {
		pdb.setPictureUrl("Test");;
		dataBeanList[0] = pdb;
		List<ParserResultBean> result = ParserCrawler.analyzeProductList(dataBeanList, urlBean);
		ParserResultBean ps = result.get(0);
		ProductDataBean pdb = ps.getPdb();
		assertNull(pdb.getPictureUrl());
	}
	
	@Test
	public void analyzeExistProduct_normal() throws SQLException {
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		assertEquals(result.isExist(),true);
	}
	
	@Test
	public void analyzeExistProduct_url_change() throws SQLException {
		pdb.setUrl("http://priceza.com");
		BaseConfig.CONF_ENABLE_UPDATE_URL = true;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		assertEquals(result.getPdb().getUrl(),"http://priceza.com");
	}
	
	@Test
	public void analyzeExistProduct_url_change_without_use_old_url() throws SQLException {
		pdb.setUrl("http://priceza.com");
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		assertEquals(result.isDuplicate(),true);
		//-------------------------- Config CONF_USER_OLD_PRODUCT_URL เป็น true ------------------------------------------------
		pdb.setUrl("http://priceza.com");
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = true;
		ParserResultBean resultCase2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(resultCase2);
		assertEquals(resultCase2.isDuplicate(),false);
	}
	
	@Test
	public void analyzeExistProduct_price_more_than_contract_price() throws SQLException {
		pdb.setPrice(9999999999.0);
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		ProductDataBean pdb =  result.getPdb();
		assertEquals(pdb.getPrice(),BotUtil.CONTACT_PRICE,0);
	}
	
	@Test
	public void analyzeExistProduct_update_price_percent() throws SQLException {
		pdb.setPrice(120);
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 50;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		ProductDataBean pdb =  result.getPdb();
		assertEquals(pdb.getPrice(),120,0);
		assertEquals(result.isUpdate(), true);
		//---------------------- ราคา ใหม่ ราคามากกว่า Credit Base Price -----------------
		pdb.setPrice(120000000.0);
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 50;
		ParserResultBean resultCase2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(resultCase2);
		ProductDataBean pdbCase2 =  resultCase2.getPdb();
		assertEquals(pdbCase2.getPrice(),BotUtil.CONTACT_PRICE,0);
		assertEquals(resultCase2.isUpdate(), true);
		//--------------------------ราคา เก่า ราคามากกว่า Credit Base Price -----------------
		pdb.setPrice(120);
		oldBean.setPrice(12000000.0);
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 50;
		ParserResultBean resultCase3 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(resultCase3);
		ProductDataBean pdbCase3 =  resultCase3.getPdb();
		assertEquals(pdbCase3.getPrice(),120,0);
		assertEquals(resultCase3.isUpdate(), true);
		//------------------------------ case update ราคา ค่าเกิน percent ที่ตั้งไว้---------------
		pdb.setPrice(1200);
		oldBean.setPrice(PRICE);
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 50;
		ParserResultBean result4 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result4);
		ProductDataBean pdb4 =  result4.getPdb();
		assertNull(pdb4);
		
	}
	
	@Test
	public void analyzeExistProduct_update_basePrice() throws SQLException {
		pdb.setBasePrice(120);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getBasePrice(),120,0);
		assertEquals(result.isUpdate(), true);
		//-----------------------------------------------------------------------------------
		pdb.setBasePrice(120);
		pdb.setPrice(999999999999999999.0);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb2 =  result2.getPdb();
		assertNotNull(result2);
		assertNotNull(pdb2);
		//--------------------------ConfigBasePrice เป็น false----------------------------------
		pdb.setBasePrice(120);
		pdb.setPrice(PRICE);
		oldBean.setBasePrice(BASE_PRICE);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = false;
		ParserResultBean resultCase3 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdbCase3 =  resultCase3.getPdb();
		assertNotNull(resultCase3);
		assertNull(pdbCase3);
		
	}
	//if(basePrice > 0 && (newPrice == BotUtil.CONTACT_PRICE || newBasePrice > BotUtil.CREDIT_BASE_PRICE)
	
	@Test
	public void analyzeExistProduct_change_basePrice_if_price_over_limit() throws SQLException {
		pdb.setBasePrice(BotUtil.CREDIT_BASE_PRICE+1000);
		pdb.setPrice(BotUtil.CONTACT_PRICE);
		oldBean.setBasePrice(BASE_PRICE);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getBasePrice(),0,0);
		assertEquals(result.isUpdate(), true);
		//--------------------------------------------------------------------------
		pdb.setBasePrice(BotUtil.CREDIT_BASE_PRICE+1000);
		oldBean.setBasePrice(BASE_PRICE);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb2 =  result2.getPdb();
		assertNotNull(result2);
		assertEquals(pdb2.getBasePrice(),0,0);
		assertEquals(result2.isUpdate(), true);
		//-------------------------------------------------------------------------------
		pdb.setBasePrice(0);
		pdb.setPrice(0);
		oldBean.setBasePrice(0);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result3 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb3 =  result3.getPdb();
		assertNotNull(result3);
		assertEquals(pdb3.getBasePrice(),0,0);
		//-------------------------------------------------------------------------------
		pdb.setBasePrice(BotUtil.CREDIT_BASE_PRICE+1000);
		oldBean.setBasePrice(BASE_PRICE);
		pdb.setPrice(0);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = true;
		ParserResultBean result4 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb4 =  result4.getPdb();
		assertNotNull(result4);
		assertEquals(pdb4.getBasePrice(),0,0);
		assertEquals(result4.isUpdate(), true);
	}
	
	@Test
	public void analyzeExistProduct_update_description() throws SQLException {
		pdb.setDescription("change");
		oldBean.setDescription(null);
		BaseConfig.CONF_ENABLE_UPDATE_DESC = true;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getDescription(), "change");
		assertEquals(result.isUpdate(), true);
		//---------------------------------------------------------------------
		pdb.setDescription(null);
		oldBean.setDescription(DESC);
		BaseConfig.CONF_ENABLE_UPDATE_DESC = true;
		ParserResultBean result3 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb3 =  result3.getPdb();
		assertNotNull(result3);
		assertEquals(pdb3.getDescription(),null);
		//---------------------------------------------------
		pdb.setDescription(null);
		oldBean.setDescription(null);
		BaseConfig.CONF_ENABLE_UPDATE_DESC = true;
		ParserResultBean result4 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb4 =  result4.getPdb();
		assertNotNull(result4);
		assertNull(pdb4);
	}
	
	@Test
	public void analyzeExistProduct_check_updateCat_and_keyword() throws SQLException {
		ProductDataBean newPdb = pdb;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY =true; 
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD=true;
		newPdb.setKeyword("k");
		ParserResultBean result = ParserCrawler.analyzeExistProduct(newPdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getKeyword(),"k");
		//--------------------------------------------------------------------------
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY =false; 
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD=true;
		newPdb.setKeyword(DESC);
		oldBean.setKeyword(DESC);
		newPdb.setCategoryId(6969);
		ParserResultBean result2 = ParserCrawler.analyzeExistProduct(newPdb, oldBean);
		ProductDataBean pdb2 =  result2.getPdb();
		assertNotNull(result2);
		assertNotNull(pdb2.getDynamicField());
		//-----------------------------------------------------------------------------
		newPdb.setCategoryId(0);
		oldBean.setCategoryId(0);
		ParserResultBean result3 = ParserCrawler.analyzeExistProduct(newPdb, oldBean);
		ProductDataBean pdb3 =  result3.getPdb();
		assertNotNull(result3);
		assertNull(pdb3);
		//-----------------------------------------------------------------------------
		newPdb.setCategoryId(0);
		oldBean.setCategoryId(100);
		ParserResultBean result4 = ParserCrawler.analyzeExistProduct(newPdb, oldBean);
		ProductDataBean pdb4 =  result4.getPdb();
		assertNotNull(result4);
		assertNull(pdb4);
		
	}
	
	@Test
	public void analyzeExistProduct_update_realProductId() throws SQLException {
		pdb.setRealProductId("newRealProductID");
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getRealProductId(),"newRealProductID");
		//----------------------------------------------------------------------
		pdb.setRealProductId("newRealProductID");
		oldBean.setRealProductId("newRealProductID2");
		ParserResultBean result2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb2 =  result2.getPdb();
		assertNotNull(result2);
		assertNull(pdb2);
	
	}
	
	@Test
	public void analyzeExistProduct_update_upc() throws SQLException {
		pdb.setUpc("newUpc");
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getUpc(),"newUpc");
		//--------------------------------------------------------------------------
		pdb.setUpc("newUpc");
		oldBean.setUpc("newUpcx");
		ParserResultBean result2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb2 =  result2.getPdb();
		assertNotNull(result2);
		assertNull(pdb2);
	
	}
	
	@Test
	public void analyzeExistProduct_update_pic() throws SQLException {
		String newPicurl = "http://www.priceza.com/x.jpg";
		pdb.setPictureUrl(newPicurl);
		BaseConfig.CONF_ENABLE_UPDATE_PIC = true;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdb =  result.getPdb();
		assertNotNull(result);
		assertEquals(pdb.getPictureUrl(), newPicurl);
		//--------------------------Case ที่ PICTURE URLเก่าไม่มีค่าแล้ว อันใหม่ มีค่า-------------------------------------------------
		pdb.setPictureUrl(newPicurl);
		oldBean.setPictureUrl(null);
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		ParserResultBean resultCase2 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdbCase2 =  resultCase2.getPdb();
		assertNotNull(resultCase2);
		assertEquals(pdbCase2.getPictureUrl(), newPicurl);
		//---------------------------- Case ที่ PICTURE URL เป็น NULL ทั้งคู่ ---------------------------------------------------
		pdb.setPictureUrl(null);
		oldBean.setPictureUrl(null);
		BaseConfig.CONF_ENABLE_UPDATE_PIC = true;
		ParserResultBean resultCase3 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdbCase3 =  resultCase3.getPdb();
		assertNotNull(resultCase3);
		assertNull(pdbCase3);
		//---------------------------- Case ที่ PICTURE URL มีเปลี่ยน แต่ Config เป็น false---------------------------------------------------
		pdb.setPictureUrl(newPicurl);
		oldBean.setPictureUrl(PIC_URL);
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		ParserResultBean resultCase4 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdbCase4 =  resultCase4.getPdb();
		assertNotNull(resultCase4);
		assertNull(pdbCase4);
		//-------------------------------Case ที่ เป็น URL ใหม่มีค่า NULL และ อันเก่า ไม่ NULL-------------------------------------------------
		pdb.setPictureUrl(null);
		oldBean.setPictureUrl(PIC_URL);
		BaseConfig.CONF_ENABLE_UPDATE_PIC = true;
		ParserResultBean resultCase5 = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		ProductDataBean pdbCase5 =  resultCase5.getPdb();
		assertNotNull(resultCase5);
		assertNull(pdbCase5);
	
	}
	
	@Test
	public void analyzeExistProduct_update_url_not_match() throws SQLException {
		pdb.setUrlForUpdate("http://www.priceza.com/");
		ParserResultBean result = ParserCrawler.analyzeExistProduct(pdb, oldBean);
		assertNotNull(result);
		assertTrue(result.isDuplicate());
	
	}
	
	@Test
	public void analyzeExistProduct_check_pdb() throws SQLException {
		ProductDataBean dummyNewPdb = pdb;
		ProductDataBean dummyOldBean = oldBean;
		dummyNewPdb = null;
		dummyOldBean = null;
		ParserResultBean result = ParserCrawler.analyzeExistProduct(dummyNewPdb, dummyOldBean);
		assertNotNull(result);
		assertNull(result.getPdb());
		//--------------------------------Case ที่ Null ข้างเดียว---------------------------
		dummyNewPdb = pdb;
		dummyOldBean = oldBean;
		dummyNewPdb = null;
		ParserResultBean resultCase2 = ParserCrawler.analyzeExistProduct(dummyNewPdb, dummyOldBean);
		assertNotNull(resultCase2);
		assertNull(resultCase2.getPdb());
		//---------------------------------------------------------------------------------
		dummyNewPdb = pdb;
		dummyOldBean = oldBean;
		dummyOldBean = null;
		ParserResultBean resultCase3 = ParserCrawler.analyzeExistProduct(dummyNewPdb, dummyOldBean);
		assertNotNull(resultCase3);
		assertNull(resultCase3.getPdb());
	
	}
	
	@Test
	public void process_normal() throws SQLException {
		boolean result = ParserCrawler.process(dataBeanList, urlBean);
		assertEquals(result, true);
	}
	
	//@Test
	public void process_error() throws SQLException {
		when(productDataDB.getProduct(Mockito.eq(BaseConfig.MERCHANT_ID),Mockito.anyString())).thenThrow(new MockitoException("Error"));
		boolean result = ParserCrawler.process(dataBeanList, urlBean);
		assertEquals(result, false);
	}
	
	@Test
	public void parse_image_normal() throws SQLException {
		ParserResultBean resultParse = ParserCrawler.analyzeProductList(dataBeanList, urlBean).get(0);
		resultParse.getPdb().setPictureUrl("http://www.priceza.com/test.jpg");
		ParserResultBean result = ParserCrawler.parseImage(resultParse);
		assertNotNull(result.getImageByte());
		//------------------------------------ case url is null
		ParserResultBean resultParse2= ParserCrawler.analyzeProductList(dataBeanList, urlBean).get(0);
		resultParse2.getPdb().setPictureUrl(null);
		ParserResultBean result2 = ParserCrawler.parseImage(resultParse2);
		assertNull(result2.getImageByte());
		//--------------------------------------- resultBean = null ---------------------------------------
		ParserResultBean resultParse3= null;
		ParserResultBean result3 = ParserCrawler.parseImage(resultParse3);
		assertNull(result3);
		//----------------------------------------pdb = null--------------------------------------------
		ParserResultBean resultParse4= ParserCrawler.analyzeProductList(dataBeanList, urlBean).get(0);
		resultParse4.setPdb(null);
		ParserResultBean result4 = ParserCrawler.parseImage(resultParse4);
		assertNull(result4.getImageByte());
		//------------------------------ดึงรูปไม่ได้-----------------------------------------------
		ParserResultBean resultParse5 = ParserCrawler.analyzeProductList(dataBeanList, urlBean).get(0);
		resultParse5.getPdb().setPictureUrl("http://www.priceza.com/ะ.jpg");
		ParserResultBean result5 = ParserCrawler.parseImage(resultParse5);
		assertNull(result5.getImageByte());
	}

}
