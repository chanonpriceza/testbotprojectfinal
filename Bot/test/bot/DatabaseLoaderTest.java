package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Properties;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import bean.BotConfigBean;
import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;
import db.BotConfigDB;
import db.MerchantDB;
import db.ProcessDB;
import db.manager.DatabaseFactory;
import db.manager.DatabaseUtil;
import engine.BaseConfig;
import loader.DatabaseLoader;
import manager.CommandManager;
import unittest.MockObjectHolder;


public class DatabaseLoaderTest {
	
	@Mock Properties p;
	@Mock QueryRunner q;
	@Mock BotConfigBean configBean;
	
	@InjectMocks MerchantBean merchantBean;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		DatabaseLoader.setDatabaseLoader(q);
		BaseConfig.RUNTIME_TYPE = "WCE";
		merchantBean.setId(999);
		p.setProperty("ConfigDBURL","");
		p.setProperty("ConfigDBDriverName","");
		p.setProperty("ConfigDBUserName","");
		p.setProperty("ConfigDBPassword","");
		BaseConfig.CONF_PROP = p;
		merchantBean.setName("UnitTest");
		BaseConfig.BOT_FAMILY = null;
		merchantBean.setDataServer("Test-Server");
		MockObjectHolder.isMock=true;
		DatabaseFactory.clearDataSource();
		DatabaseLoader.closeDBConnection();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_complete_loadDBConnectionTest() throws Exception {
		String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, "1,3");
		String command = "%"+CommandManager.PRIMARY_WCE+"%";
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
		when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()),Mockito.eq(command),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
		when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq( merchantBean.getDataServer()),Mockito.eq(1))).thenReturn(configBean);
		assertEquals(DatabaseLoader.loadDBConnection(),true);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_complete_node_test_loadDBConnectionTest() throws Exception {
		String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, "1,3");
		String command = "%"+CommandManager.PRIMARY_WCE+"%";
		merchantBean.setActive(3);
		merchantBean.setDataServer("Bot-verify0");
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
		when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()),Mockito.eq(command),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
		when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq( merchantBean.getDataServer()),Mockito.eq(1))).thenReturn(configBean);
		assertEquals(DatabaseLoader.loadDBConnection(),true);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_error_not_found_bot_config_loadDBConnectionTest()  {
			try {
				String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, "1,3");
				String command = "%"+CommandManager.PRIMARY_WCE+"%";
				when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
				when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()),Mockito.eq(command),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
				when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq("not"),Mockito.eq(1))).thenReturn(configBean);
				DatabaseLoader.loadDBConnection();
			} catch (Exception  e) {
				assertEquals(e.getMessage(),"Not Found Bot Database Config : Test-Server");
			}

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void due_complete_loadDBConnectionTest() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, "1");
		String command = "%"+CommandManager.PRIMARY_DUE+"%";
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
		when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()),Mockito.eq(command),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
		when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq( merchantBean.getDataServer()),Mockito.eq(1))).thenReturn(configBean);
		assertEquals(DatabaseLoader.loadDBConnection(),true);

	}
	
	@Test
	public void notFound_loadDBConnectionTest() throws Exception {
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(99);
		assertEquals(DatabaseLoader.loadDBConnection(),false);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_complete_botFamily_loadDBConnectionTest() throws Exception {
		BaseConfig.BOT_FAMILY = "true";
		String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_FAMILY_QUEUE, "1,3");
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
		when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(BaseConfig.BOT_FAMILY),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
		when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq( merchantBean.getDataServer()),Mockito.eq(1))).thenReturn(configBean);
		assertEquals(DatabaseLoader.loadDBConnection(),true);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void due_complete_botFamily_loadDBConnectionTest() throws Exception {
		BaseConfig.BOT_FAMILY = "true";
		BaseConfig.RUNTIME_TYPE = "DUE";
		String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_FAMILY_QUEUE, "1");
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
		when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(BaseConfig.BOT_FAMILY),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
		when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq( merchantBean.getDataServer()),Mockito.eq(1))).thenReturn(configBean);
		assertEquals(DatabaseLoader.loadDBConnection(),true);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void due_error_not_found_bot_config_loadDBConnectionTest()  {
			try {
				BaseConfig.RUNTIME_TYPE = "DUE";
				String queryString = DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, "1");
				String command = "%"+CommandManager.PRIMARY_DUE+"%";
				when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(1);
				when(q.query(Mockito.eq(queryString),Mockito.any(BeanHandler.class),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()),Mockito.eq(command),Mockito.eq(STATE.BOT_FINISH.toString()),Mockito.eq(STATUS.FINISH.toString()))).thenReturn(merchantBean);
				when(q.query(Mockito.eq(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE),Mockito.any(BeanHandler.class),Mockito.eq("not"),Mockito.eq(1))).thenReturn(configBean);
				DatabaseLoader.loadDBConnection();
			} catch (Exception e) {
				assertEquals(e.getMessage(),"Not Found Bot Database Config : Test-Server");
			}

	}
	
	@Test
	public void due_notFound_loadDBConnectionTest() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		when(q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available")).thenReturn(99);
		assertEquals(DatabaseLoader.loadDBConnection(),false);
		
	}


}
