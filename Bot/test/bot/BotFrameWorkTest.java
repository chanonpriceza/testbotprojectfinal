package bot;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import engine.WCEApp;

public class BotFrameWorkTest {
	
	private final String TEST_CONFIG_PATH = "config/test/testBotFamilyConfig.properties";
	private final String BOT_FAMILY_CONFIG_PATH  = "config/test/testBotFamilyConfig.properties";

	@InjectMocks
	private  WCEApp wce ;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

	}
	
	@Test
	public void BotFramework_onInitialize_config_is_null(){
		
		assertEquals(wce.onInitialize(null),false);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_length0(){
		
		assertEquals(wce.onInitialize(new String[0]),false);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_isBlank(){
		
		assertEquals(wce.onInitialize(new String[] {""}),false);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_isNotBlank(){

		assertEquals(wce.onInitialize(new String[] {TEST_CONFIG_PATH}),true);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_isFileNotFound(){

		assertEquals(wce.onInitialize(new String[] {"x"}),false);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_isNotBlank_With_BotFamily(){

		assertEquals(wce.onInitialize(new String[] {TEST_CONFIG_PATH,BOT_FAMILY_CONFIG_PATH}),true);	
		
	}
	
	@Test
	public void BotFramework_onInitialize_config_isNotBlank_With_BotFamily_Blank(){

		assertEquals(wce.onInitialize(new String[] {TEST_CONFIG_PATH,""}),true);	
		
	}
	
}