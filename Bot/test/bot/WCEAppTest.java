package bot;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.sql.DataSource;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import bean.BotConfigBean;
import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;
import bean.UrlPatternBean;
import db.MerchantConfigDB;
import db.MerchantDB;
import db.ParserConfigDB;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.ReportDB;
import db.UrlPatternDB;
import db.WorkloadDB;
import db.manager.DatabaseFactory;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import engine.WCEApp;
import loader.DatabaseLoader;
import manager.CommandManager;
import manager.ReportManager;
import unittest.MockObjectHolder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DatabaseLoader.class)
@PowerMockIgnore("javax.management.*")
public class WCEAppTest { 
	
	@Mock DatabaseLoader databaseLoader;
	@Mock DatabaseFactory databaseFactory;
	@Mock DataSource ds;
	@Mock Logger logger;
	@Mock DatabaseManager databaseManager;
	@Mock MerchantDB merchantDB;
	@Mock ReportDB reportDB;
	@Mock MerchantConfigDB merchantConfigDB;
	@Mock ParserConfigDB parserConfigDB;
	@Mock WorkloadDB workloadDB;
	@Mock ProductUrlDB productUrlDB;
	@Mock ProductDataDB productdataDB;
	@Mock UrlPatternDB urlPatternDB;
	@Mock CommandManager commandManager;
	@Mock ExecutorService parserThread;
	@Mock ExecutorService workloadThread;
	@Mock ReportManager reportManager;
	@SuppressWarnings("rawtypes")
	@Mock Future	parserThreadFuture;	
	@Mock Future<Boolean> workloadThreadFuture;
	
	@InjectMocks  WCEApp wce ;
	@InjectMocks BotConfigBean botconfigBean;
	@InjectMocks MerchantBean merchantBean;
	@InjectMocks UrlPatternBean urlPatternBean;
	List<UrlPatternBean> patternList = new ArrayList<UrlPatternBean>();;
	final static String MOCK_FEED_CRAWLER = "unittest.MockFeedCrawler";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.commandManager = commandManager;
		MockObjectHolder.mockLogger = logger;
		MockObjectHolder.mockReportManager = reportManager;
		MockObjectHolder.threadSleepTime = 0;
		MockObjectHolder.countDownLatchWebCrawlerProcessorCommand = null;
		MockObjectHolder.countDownLatchUrlCrawlerProcesserCommand = null;

		PowerMockito.mockStatic(DatabaseLoader.class);
		
		BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
		BaseConfig.CONF_PARSER_CLASS = null;
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 0;
		
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(databaseManager.getReportDB()).thenReturn(reportDB);
		when(databaseManager.getMerchantConfigDB()).thenReturn(merchantConfigDB);
		when(databaseManager.getParserConfigDB()).thenReturn(parserConfigDB);
		when(databaseManager.getWorkloadDB()).thenReturn(workloadDB);
		when(databaseManager.getProductUrlDB()).thenReturn(productUrlDB);
		when(databaseManager.getProductDataDB()).thenReturn(productdataDB);
		when(databaseManager.getUrlPatternDB()).thenReturn(urlPatternDB);
	}

	// start : ทุกอย่างปกติ, โหลด config ได้, ไม่มีร้านค้าให้รัน, return true
	@Test
	public void wce_complete_not_found_merchant() throws Exception {
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(false);
		assertEquals(runWce(), true);

	}
	
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_FEED_CRAWLER_CLASS มีค่า, 1 Thread, mainProcess จบก่อน, checkProcess จบตาม
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/

	@Test
	public void wce_feed_complete_one_thread() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_FEED_CRAWLER_CLASS = MOCK_FEED_CRAWLER;
		BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), true);		
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_FEED_CRAWLER_CLASS มีค่า, n Thread, mainProcess จบก่อน, checkProcess จบตาม
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	
	@Test
	public void wce_feed_complete_many_thread() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 5;
		BaseConfig.CONF_FEED_CRAWLER_CLASS = MOCK_FEED_CRAWLER;
		BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), true);		
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_FEED_CRAWLER_CLASS มีค่า, n Thread, checkProcess จบก่อน, mainProcess ถูก kill
		merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	@Test
	public void wce_feed_complete_many_thread_main_process_kill() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 5;
		BaseConfig.CONF_FEED_CRAWLER_CLASS = MOCK_FEED_CRAWLER;
		BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		MockObjectHolder.threadSleepTime = 60000;
		when(commandManager.checkCommand(CommandManager.KILL_WCE)).thenReturn(true);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);
		
		
		assertEquals(runWce(), true);	
		Mockito.verify(commandManager).checkCommand(CommandManager.KILL_WCE);
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_PARSER_CLASS มีค่า, 1 Thread, mainProcess จบก่อน, checkProcess จบตาม
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_complete_one_thread() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), true);		
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_PARSER_CLASS มีค่า, n Thread, mainProcess จบก่อน, checkProcess จบตาม
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_complete_many_thread() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 10;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), true);		
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	//start : เช็ค workloadProcess & parserProcess ดูว่า Cancel เรียบร้อยแล้วหรือเปล่า
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_PARSER_CLASS มีค่า, n Thread, checkProcess จบก่อน, mainProcess ถูก kill
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_complete_many__thread_main_process_web_crawler_kill() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 5;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		MockObjectHolder.threadSleepTime = 6000;
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		MockObjectHolder.countDownLatchWebCrawlerProcessorCommand = "Sleep";
		when(commandManager.checkCommand(CommandManager.KILL_WCE)).thenReturn(true);
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);
		assertEquals(runWce(), true);		

		Mockito.verify(commandManager).checkCommand(CommandManager.KILL_WCE);
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	//start : เช็ค workloadProcess & parserProcess ดูว่า Cancel เรียบร้อยแล้วหรือเปล่า
	/*"start : ทุกอย่างปกติ, BaseConfig.CONF_PARSER_CLASS มีค่า, n Thread, checkProcess จบก่อน, mainProcess ถูก kill
	merchantDB อัพเดทถูก, reportDB อัพเดทถูก, return true"*/
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_complete_many__thread_main_process_url_craweler_kill() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 5;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		MockObjectHolder.threadSleepTime = 6000;
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		MockObjectHolder.countDownLatchUrlCrawlerProcesserCommand = "Sleep";
		
		
		when(commandManager.checkCommand(CommandManager.KILL_WCE)).thenReturn(true);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);
		
		assertEquals(runWce(), false);		
		Mockito.verify(commandManager).checkCommand(CommandManager.KILL_WCE);
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	//start : DatabaseLoader.loadDBConnection() เกิด Exception
	@Test
	public void wce_databaseLoader_error() throws Exception {
		BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenThrow(new MockitoException("DatabaseLoader Error"));
		assertEquals(runWce(), false);

	}
	//start : BaseConfig.CONF_FEED_CRAWLER_CLASS เป็นคลาสที่ไม่มีอยู่จริง หรือเกิด Class cast exception
	@Test
	public void wce_feed_class_not_found() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_FEED_CRAWLER_CLASS = "feed.crawler.UnitTest";
		BaseConfig.CONF_PARSER_CLASS = "com.ha.bot.parser.DefaultFeedHTMLParser";
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), false);		
	}
	
	//	//start : UrlCrawlerProcessor setProperties เกิด Exception
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_url_crawler_processor_set_propertie_error() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		MockObjectHolder.countDownLatchUrlCrawlerProcesserCommand = "Error";
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), false);		
	}
	
	//start : WebCrawlerProcessor setProperties เกิด Exception
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_web_parser_processor_set_propertie_error() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		MockObjectHolder.countDownLatchWebCrawlerProcessorCommand = "Error";
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);

		assertEquals(runWce(), false);		
	}
	
	//start : เช็ค clearStatus ว่าตรงตามเงื่อนไขหรือเปล่า (2 เคส)
	@SuppressWarnings("unchecked")
	@Test
	public void wce_parser_complete_chekClearStatus() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);
		BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = true;

		assertEquals(runWce(), true);		
		Mockito.verify(reportManager).finishReport();
		Mockito.verify(productUrlDB).clearProductUrlByStatus(BaseConfig.MERCHANT_ID,new String[]{ bean.ProductUrlBean.STATUS.W.toString(), bean.ProductUrlBean.STATUS.R.toString(), bean.ProductUrlBean.STATUS.E.toString(), bean.ProductUrlBean.STATUS.C.toString()});
		Mockito.verify(merchantDB).updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.FINISH);
		Mockito.verify(merchantDB).updateNextStart(ArgumentMatchers.eq(BaseConfig.MERCHANT_ID), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(STATE.BOT_FINISH), ArgumentMatchers.eq(STATUS.FINISH));
	}
	
	//start : checkProcess เช็คกรณีที่เกิด Exception
	@SuppressWarnings("unchecked")
	@Test
	public void wce_check_process_exception() throws Exception {
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_PARSER_CLASS = "web.parser.filter.UnitTestHTMLParser";
		when(commandManager.checkCommand(CommandManager.KILL_WCE)).thenThrow(new MockitoException("Check Process Error"));
		PowerMockito.when(DatabaseLoader.loadDBConnection()).thenReturn(true);
		MockObjectHolder.countDownLatchWebCrawlerProcessorCommand = "Sleep";
		when(parserThread.submit(ArgumentMatchers.any(Runnable.class))).thenReturn(parserThreadFuture);
		when(workloadThread.submit(ArgumentMatchers.any(Callable.class))).thenReturn(workloadThreadFuture);
		when(merchantDB.getMerchantById(100)).thenReturn(merchantBean);
		MockObjectHolder.threadSleepTime = 60000;

		assertEquals(runWce(), false);		
	}
	
	public boolean runWce() throws Exception {
		Thread.sleep(10000);
		String[] config = new String[] {"D:/config/fakeWce.properties"};
		wce = new WCEApp(config);
		MockObjectHolder.addExecutors("wce_parserThread", parserThread);
		MockObjectHolder.addExecutors("wce_workloadThread",workloadThread);
		BaseConfig.NEXT_START = new Date();
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockLogger = logger;
		merchantBean.setName("UnitTest");
		merchantBean.setActive(1);
		merchantBean.setCommand("PRIMARY_WCE");
		merchantBean.setId(1);
		merchantBean.setDataServer("test-dataserver");
		
		BaseConfig.MERCHANT_ID = 1;
		
		urlPatternBean.setAction("test");
		urlPatternBean.setCategoryId(1);
		urlPatternBean.setCondition("test");
		urlPatternBean.setGroup(0);
		urlPatternBean.setId(1);
		urlPatternBean.setKeyword("test");
		urlPatternBean.setMerchantId(1);
		urlPatternBean.setValue("test");
		patternList.add(urlPatternBean);
		


		boolean result = wce.start();
		return result;
		
	}
	
	
}