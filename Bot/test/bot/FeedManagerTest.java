package bot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.ProductDataBean;
import bean.ReportBean;
import bean.UrlPatternBean;
import db.MerchantDB;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;
import product.processor.ProductReader;
import unittest.MockFeedManager;
import unittest.MockObjectHolder;
import unittest.mockParserProcessor;

public class FeedManagerTest {
	
	private static final String URL = "http://example.com";
	private static final String KEYWORD = "test";
	private static final int CAT = 14;
	private static final String STATUS = "C";
	private static final String NAME = "TEST";
	private static final int PRICE = 99;
	private static final String DESC = "TEST";
	private static final String PIC_URL = "http://example.com/test.jpg";
	
	private static final String URL_PATTERN_ACTION = "FEED";
	private static final String URL_PATTERN_VALUE = "test";
	private static final int URL_PATTERN_CATEGORY = 1;
	private static final String URL_PATTERN_KEYWORD = "test";
	
	
	@Mock DatabaseManager databaseManager;
	@Mock ProductReader productReader;
	@Mock MerchantDB merchantDB ;
	@Mock ProductDataDB productDataDB;
	@Mock ProductUrlDB productUrlDB ;
	@Mock UrlPatternDB urlPatternDB;
	@Mock SendDataDB  sendDataDB;
	@Mock ReportBean reportBean ;
	@Mock ReportManager reportManager;
	@Mock CountDownLatch mockLatch;
	
	@InjectMocks ProductDataBean pdb;
	@InjectMocks UrlPatternBean urlPattern;
	
	MockFeedManager feedCrawler;
	CountDownLatch latch;
	List<ProductDataBean> pdbList =  new ArrayList<ProductDataBean>();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		List<UrlPatternBean> urlPatterns = new ArrayList<UrlPatternBean>();
		urlPattern.setCategoryId(URL_PATTERN_CATEGORY);
		urlPattern.setAction(URL_PATTERN_ACTION);
		urlPattern.setKeyword(URL_PATTERN_KEYWORD);
		urlPattern.setValue(URL_PATTERN_VALUE);
		
		urlPatterns.add(urlPattern);
		
		pdb.setName(NAME);
		pdb.setPrice(PRICE);
		pdb.setCategoryId(CAT);
		pdb.setUrl(URL);
		pdb.setUrlForUpdate(URL);
		pdb.setDescription(DESC);
		pdb.setPictureUrl(PIC_URL);
		pdb.setKeyword(KEYWORD);
		pdb.setStatus(STATUS);
		pdb.setExpire(false);
		pdb.setBasePrice(0);
		pdb.setRealProductId(null);
		pdb.setUpc(null);
		
		MockObjectHolder.isMock = true;
		MockObjectHolder.productReader = productReader;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		
		BaseConfig.THREAD_NUM = 1;
		BaseConfig.CONF_DELETE_LIMIT = 10000;
		BaseConfig.CONF_FORCE_DELETE = false;
		BaseConfig.FEED_STORE_PATH = "test/mockData/feedFolder";
		BaseConfig.RUNTIME_TYPE = "WCE";
		BaseConfig.FEED_PROCESSOR = "feed.ParserProcessor";
		BaseConfig.CONF_FEED_USERNAME = "bot";
		BaseConfig.CONF_FEED_PASSWORD = "pzad4r7u";
		BaseConfig.CONF_FORCE_DELETE = false;
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = 6;
		BaseConfig.CONF_DELETE_LIMIT = 20;
		
		when(reportManager.getReport()).thenReturn(reportBean);
		when(databaseManager.getUrlPatternDB()).thenReturn(urlPatternDB);
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		when(databaseManager.getSendDataDB()).thenReturn(sendDataDB);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID,"FEED")).thenReturn(urlPatterns);
		when(productDataDB.getByMerchantStatus(BaseConfig.MERCHANT_ID,bean.ProductDataBean.STATUS.W,200000)).thenReturn(new ArrayList<Integer>());
		pdbList =  new ArrayList<ProductDataBean>();
		pdbList.add(pdb);
		when(productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID,bean.ProductDataBean.STATUS.W,1000)).thenReturn(pdbList);
		
		MockFeedManager.command = "";
		mockParserProcessor.command = "";
	}
	
	@Test
	public void feed_manager_test_normal() throws Exception {
		
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_test_normal_due() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_test_normal_due_with_force_delete_config() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		BaseConfig.CONF_FORCE_DELETE = true;
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_test_normal_due_delete_over_product_update_limit() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		BaseConfig.CONF_DELETE_LIMIT = 1;
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_test_normal_due_delete_over_delete_limit() throws Exception {
		BaseConfig.RUNTIME_TYPE = "DUE";
		BaseConfig.CONF_FORCE_DELETE = true;
		BaseConfig.CONF_DELETE_LIMIT = 1;
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_getKeySet_test_normal() throws Exception {
		feedCrawler = new MockFeedManager();
		Set<String> result = feedCrawler.getCategoryKeySet();
		assertNotNull(result);
		assertEquals(result.size(),1);
		List<String> convertSet = new ArrayList<String>(result);
		assertEquals(convertSet.get(0),URL_PATTERN_VALUE);
	}
	
	@Test
	public void feed_manager_getKeySet_test_not_found() throws Exception {
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID,"FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		feedCrawler = new MockFeedManager();
		Set<String> result = feedCrawler.getCategoryKeySet();
		assertNotNull(result);
		assertEquals(result.size(),0);
	}
	
	@Test
	public void feed_manager_getCat_normal() throws Exception {
		feedCrawler = new MockFeedManager();
		String[] result = feedCrawler.getCategory(URL_PATTERN_VALUE);
		assertNotNull(result);
		assertEquals(result.length,2);
		assertEquals(result[0],"1");
		assertEquals(result[1],"test");
	}
	
	@Test
	public void feed_manager_getCat_when_input_eq_null() throws Exception {
		feedCrawler = new MockFeedManager();
		String[] result = feedCrawler.getCategory(null);
		assertNull(result);
	}
	
	@Test
	public void feed_manager_setCategoryMapping_findUrlPattern_null() throws Exception {
		try {
			when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID,"FEED")).thenReturn(null);
			feedCrawler = new MockFeedManager();
		}catch (Exception e) {
			boolean result  = (""+e).contains("NullPointer");
			assertEquals(result, true);
		}
	}
	
	@Test
	public void feed_manager_setCategoryMapping_findUrlPattern_is_empty_list() throws Exception {
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID,"FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_setCategoryMapping_findUrlPattern_is_value_is_blank() throws Exception {
		
		List<UrlPatternBean> urlPatterns = new ArrayList<UrlPatternBean>();
		urlPattern.setCategoryId(URL_PATTERN_CATEGORY);
		urlPattern.setAction(URL_PATTERN_ACTION);
		urlPattern.setKeyword(URL_PATTERN_KEYWORD);
		urlPattern.setValue(null);
		
		urlPatterns.add(urlPattern);
		
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID,"FEED")).thenReturn(urlPatterns);
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_clear_feed_store_path_not_correct_path() throws Exception {
		BaseConfig.FEED_STORE_PATH = "test/mokData/feedFolder";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, false);
		
	}
	
	@Test
	public void feed_manager_feed_file_is_null() throws Exception {
		MockFeedManager.command = "null";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, false);
		
	}
	
	@Test
	public void feed_manager_feed_file_is_empty_set() throws Exception {
		MockFeedManager.command = "empty";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_read_feed_thread_num_less_than_1() throws Exception {
		BaseConfig.THREAD_NUM = 0;
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, false);
		
	}

	@Test
	public void feed_manager_read_feed_cant_find_feed_processor() throws Exception {
		BaseConfig.FEED_PROCESSOR = "feed.testProcessor";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, false);
		
	}
	
	@Test
	public void feed_manager_read_feed_InstallatioException() throws Exception {
		BaseConfig.FEED_PROCESSOR = "unittest.mockParserProcessor";
		mockParserProcessor.command = "InstantiationException";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_read_feed_IllegalAccessException() throws Exception {
		BaseConfig.FEED_PROCESSOR = "unittest.mockParserProcessor";
		mockParserProcessor.command = "IllegalAccessException";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_read_feed_ClassNotFoundException() throws Exception {
		BaseConfig.FEED_PROCESSOR = "unittest.mockParserProcessor";
		mockParserProcessor.command = "ClassNotFoundException";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_feed_file_load_feed_IOException() throws Exception {
		MockFeedManager.command = "error";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);
		
	}
	
	@Test
	public void feed_manager_read_feed_InteruptException() throws Exception {
		BaseConfig.FEED_PROCESSOR = "unittest.mockParserProcessor";
		MockFeedManager.command = "Interupt";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, true);	
	}
	
	@Test
	public void feed_manager_read_feed_Exception() throws Exception {
		when(merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.FINISH)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");});
		BaseConfig.FEED_PROCESSOR = "unittest.mockParserProcessor";
		MockFeedManager.command = "Interupt";
		feedCrawler = new MockFeedManager();
		boolean result = feedCrawler.isSuccess();
		assertEquals(result, false);	
	}
	
}
