package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import db.WorkloadDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import product.processor.WorkloadReader;
import unittest.MockObjectHolder;

public class WorkloadReaderTest {
	@Mock DatabaseManager databaseManager;
	@Mock WorkloadDB workloadDB;
	@Mock WorkloadReader workLoadReader;
	int merchantId = 1;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock=true;
		BaseConfig.MERCHANT_ACTIVE = 1;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		when(databaseManager.getWorkloadDB()).thenReturn(workloadDB);
		BaseConfig.MERCHANT_ID = merchantId;

	}
	
//	return loadData เป็น false เพราะ getMinDepth  และ  getByStatus ได้ empty 
	@Test
	public void readData_nothave_workloadBean_getMinDepth_and_getByStatus_is_Empty() {
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBean.setMerchantId(merchantId);
			workloadBean.setUrl("test");
			workloadBean.setCategoryId(1);
			workloadBean.setDepth(1);
			workloadBean.setKeyword("test");
			workloadBean.setRequestUrl("test");
			workloadBean.setStatus("test");
			workloadBeanList.add(workloadBean);

			when(workloadDB.getMinDepth(BaseConfig.MERCHANT_ID, 100)).thenReturn(new ArrayList<WorkloadBean>());
			when(workloadDB.getByStatus(BaseConfig.MERCHANT_ID, STATUS.R)).thenReturn(new ArrayList<WorkloadBean>());
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn, null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	 return loadData เป็น true เพราะ getMinDepth ได้ not empty / 
	@Test
	public void readData_nothave_workloadBean_getMinDepth_isNotEmpty() {
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBean.setMerchantId(merchantId);
			workloadBean.setUrl("test");
			workloadBean.setCategoryId(1);
			workloadBean.setDepth(1);
			workloadBean.setKeyword("test");
			workloadBean.setRequestUrl("test");
			workloadBean.setStatus("test");
			workloadBeanList.add(workloadBean);
			
			when(workloadDB.getMinDepth(BaseConfig.MERCHANT_ID, 100)).thenReturn(workloadBeanList).thenReturn(new ArrayList<WorkloadBean>());
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			
			assertEquals(workloadBeanReturn.getMerchantId(), merchantId);
			assertEquals(workloadBeanReturn.getCategoryId(), 1);
			assertEquals(workloadBeanReturn.getDepth(), 1);
			assertEquals(workloadBeanReturn.getKeyword(), "test");
			assertEquals(workloadBeanReturn.getRequestUrl(), "test");
			assertEquals(workloadBeanReturn.getStatus(), "test");
			assertEquals(workloadBeanReturn.getUrl(), "test");
			assertEquals(workloadBeanReturn.getStatus(), "test");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void readData_finish_isTrue() {
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBean.setMerchantId(merchantId);
			workloadBean.setUrl("test");
			workloadBean.setCategoryId(1);
			workloadBean.setDepth(1);
			workloadBean.setKeyword("test");
			workloadBean.setRequestUrl("test");
			workloadBean.setStatus("test");
			workloadBeanList.add(workloadBean);
			
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn, null);

			WorkloadBean workloadBeanReturn2 = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn2, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	- return loadData เป็น false เพราะ getMinDepth ได้ empty แต่ getByStatus  not empty ให้  workloadBeanList ตัวสุดท้ายเป็น null จะ เกิด  Exception  และ return null  
	@Test
	public void readData_not_have_workloadBean_getMinDepth_isEmpty_but_getbyStatus_isnotEmpty_lastlist_is_null() {

			try {
				WorkloadReader workloadReaderInstance = new WorkloadReader();
				WorkloadBean workloadBean = new WorkloadBean();
				List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
				workloadBean.setMerchantId(merchantId);
				workloadBean.setUrl("test");
				workloadBean.setCategoryId(1);
				workloadBean.setDepth(1);
				workloadBean.setKeyword("test");
				workloadBean.setRequestUrl("test");
				workloadBean.setStatus("test");
				workloadBeanList = new ArrayList<WorkloadBean>();
				workloadBeanList.add(workloadBean);
						
				when(workloadDB.getByStatus(BaseConfig.MERCHANT_ID, STATUS.R)).thenReturn(workloadBeanList).thenReturn(null);
				WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
				assertEquals(workloadBeanReturn, null);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
//	- return loadData เป็น false เพราะ getMinDepth ได้ empty แต่ getByStatus  not empty ให้  workloadBeanList ตัวสุดท้ายเป็น  empty จะ เกิด  Exception  และ return null  
	@Test
	public void readData_not_have_workloadBean_getMinDepth_isEmpty_but_getbyStatus_isnotEmpty_lastlist_is_empty() {
		
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBean.setMerchantId(merchantId);
			workloadBean.setUrl("test");
			workloadBean.setCategoryId(1);
			workloadBean.setDepth(1);
			workloadBean.setKeyword("test");
			workloadBean.setRequestUrl("test");
			workloadBean.setStatus("test");
			workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBeanList.add(workloadBean);
			
			when(workloadDB.getByStatus(BaseConfig.MERCHANT_ID, STATUS.R)).thenReturn(workloadBeanList).thenReturn(new ArrayList<WorkloadBean>());
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn, null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
//	return loadData เป็น false เพราะ getMinDepth หรือ  getByStatus  เป็น null
	@Test
	public void readData_not_have_workloadBean_getByStatus_Exception() throws SQLException {

			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			when(workloadDB.getMinDepth(BaseConfig.MERCHANT_ID, 100)).thenAnswer(invocation -> { throw new SQLException("Exception");}).thenReturn(null);
			workloadBean = workloadReaderInstance.readData();
			assertEquals(workloadBean, null);
		
	}
	
//	return loadData เป็น false เพราะ getMinDepth หรือ  getByStatus  เป็น null
	@Test
	public void readData_not_have_workloadBean_getByStatus_SQLException() {
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			when(workloadDB.getMinDepth(BaseConfig.MERCHANT_ID, 100)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null);
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn, null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void readData_not_have_workloadBean_and_Finish_istrue() {
		
		try {
			WorkloadReader workloadReaderInstance = new WorkloadReader();
			WorkloadBean workloadBean = new WorkloadBean();
			List<WorkloadBean> workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBean.setMerchantId(merchantId);
			workloadBean.setUrl("test");
			workloadBean.setCategoryId(1);
			workloadBean.setDepth(1);
			workloadBean.setKeyword("test");
			workloadBean.setRequestUrl("test");
			workloadBean.setStatus("test");
			workloadBeanList = new ArrayList<WorkloadBean>();
			workloadBeanList.add(workloadBean);
			
			BaseConfig.MERCHANT_ID = merchantId;
			when(workloadDB.getByStatus(BaseConfig.MERCHANT_ID, STATUS.R)).thenReturn(workloadBeanList).thenReturn(new ArrayList<WorkloadBean>());
			WorkloadBean workloadBeanReturn = workloadReaderInstance.readData();
			assertEquals(workloadBeanReturn, null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}