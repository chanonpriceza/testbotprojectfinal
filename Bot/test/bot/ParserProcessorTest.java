package bot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;

import bean.ProductDataBean;
import bean.ReportBean;
import bean.SendDataBean;
import bean.UrlPatternBean;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import feed.CSVFeedCrawler;
import feed.JSONFeedCrawler;
import feed.ParserProcessor;
import feed.TypeFeedCrawlerInterface.FEED_TYPE;
import feed.XMLFeedCrawler;
import manager.ReportManager;
import unittest.MockFeedCrawler;
import unittest.MockImageParser;
import unittest.MockObjectHolder;
import utils.BotUtil;

public class ParserProcessorTest {
	private static final String NAME= "Test product";
	private static final String URL= "http://www.priceza.com/priceza";
	private static final String PIC_URL= "http://www.priceza.com/testPic.jpg";
	private static final String KEYWORD= "Test keyword";
	private static final String DESC= "Test Desc";
	private static final String REAL_PRODUCT_ID="test-real";
	//private static final String UPC="test-upc";
	private static final int CAT= 1;
	private static final double PRICE = 100.0;
	private static final double BASE_PRICE = 150.0;
	
	
	@Mock
	private DatabaseManager databaseManager;
	@Mock
	private ProductUrlDB productUrlDB;
	@Mock
	private ProductDataDB productDataDB ;
	@Mock
	private SendDataDB sendDataDB;
	@Mock
	private ReportManager reportManager;
	@Mock
	private UrlPatternDB urlPatternDB;
	@Mock
	private XMLFeedCrawler xmlFeedCrawler;
	@Mock
	private CSVFeedCrawler csvFeedCrawler;
	@Mock
	private JSONFeedCrawler jsonFeedCrawler;
	@Mock
	private CountDownLatch mockLatch;
	
	@InjectMocks
	private ProductDataBean pdb;
	@InjectMocks
	private  ReportBean  report;
	
	
	private CountDownLatch latch;
	private MockFeedCrawler mockFeed;
	
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock = true;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.mockReportManager = reportManager;
		MockObjectHolder.csvFeedCrawler = csvFeedCrawler;
		MockObjectHolder.jsonFeedCrawler = jsonFeedCrawler;
		MockObjectHolder.xmlFeedCrawler =  xmlFeedCrawler;
		
		
		when(databaseManager.getProductDataDB()).thenReturn(productDataDB);
		when(databaseManager.getSendDataDB()).thenReturn(sendDataDB);
		when(databaseManager.getProductUrlDB()).thenReturn(productUrlDB);
		when(databaseManager.getUrlPatternDB()).thenReturn(urlPatternDB);
		when(reportManager.getReport()).thenReturn(report);
		
		BotUtil.LOCALE = "TH";
		BaseConfig.MERCHANT_ID = 1;
		BaseConfig.THREAD_NUM = 1;
		BaseConfig.IMAGE_PARSER = new MockImageParser();
		BaseConfig.CONF_CURRENCY_RATE = 1;
		BaseConfig.CONF_IGNORE_NAME_CHANGE = false;
		
		pdb.setName(NAME);
		pdb.setBasePrice(BASE_PRICE);
		pdb.setUrl(URL);
		pdb.setUrlForUpdate(URL);
		pdb.setPrice(PRICE);
		pdb.setPictureUrl(PIC_URL);
		pdb.setCategoryId(CAT);
		pdb.setKeyword(KEYWORD);
		pdb.setDescription(DESC);
		pdb.setRealProductId(REAL_PRODUCT_ID);
	}
	
	@Test
	public void parserProcessor_normal_xml() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	@Test
	public void parserProcessor_normal_csv() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.CSV;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(csvFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
	}
	
	@Test
	public void parserProcessor_normal_json() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.JSON;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(jsonFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//รันแล้้วเกิด Exception แล้วเช็ค กรณีที่รันแล้ว feedCrawler ไม่พบ
	@Test
	public void parserProcessor_normal_FEDD_TYPE_null() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = null;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	//-------------------------analyzeProduct name is blank----------------------------------
	@Test
	public void parserProcessor_normal_product_name_blank() throws Exception {
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		ParserProcessor parserProcessor = new ParserProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		pdb.setName("");
		pdb.setUrl("");
		pdb.setPrice(0);
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct url is blank
	@Test
	public void parserProcessor_normal_url_is_blank() throws Exception {
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		ParserProcessor parserProcessor = new ParserProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		pdb.setName("Name");
		pdb.setUrl("");
		pdb.setPrice(100);
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct price < 1
	@Test
	public void parserProcessor_normal_product_price_is_zero() throws Exception {
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		ParserProcessor parserProcessor = new ParserProcessor();
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		pdb.setName("Name");
		pdb.setUrl("url");
		pdb.setPrice(0);
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct isDuplicate
	@Test
	public void parserProcessor_normal_duplicate() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		pdb.setName("Duplicate Product");
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.getProduct(BaseConfig.MERCHANT_ID,"Duplicate Product")).thenReturn(pdb);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct currency_rate != 1.0
	@Test
	public void parserProcessor_normal_currency_rate_multiple() throws Exception {
		BaseConfig.CONF_CURRENCY_RATE = 2.0;
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct currency_rate != 1.0 and No BasePrice
	@Test
	public void parserProcessor_normal_currency_rate_multiple_no_basePrice() throws Exception {
		BaseConfig.CONF_CURRENCY_RATE = 2.0;
		pdb.setBasePrice(0);
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct price > contract price
	@Test
	public void parserProcessor_normal_price_more_than_contractPrice() throws Exception {
		pdb.setPrice(9999999999999999999999.0);
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct urlforupdate is blank
	@Test
	public void parserProcessor_normal_no_url_for_update() throws Exception {
		pdb.setUrlForUpdate("");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct Blank pictureUrl
	@Test
	public void parserProcessor_normal_pic_url_blank() throws Exception {
		pdb.setPictureUrl("");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct notBlank pic Data notBlank
	@Test
	public void parserProcessor_normal_pic_data_not_blank() throws Exception {
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	//analyzeProduct insert >0
	@Test
	public void parserProcessor_normal_insert_eq_zero() throws Exception {
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(0);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).insertProductData(pdb);
		assertEquals(exFuture.get(),true);
	}
	
	//รันแล้วเกิด SQL Exception
	@Test
	public void parserProcessor_SQLexception() throws Exception {
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		when(productDataDB.insertProductData(pdb)).thenReturn(0);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	//รันแล้วเกิิด Exception ตอนปิด CountDownLatch
	@Test
	public void parserProcessor_countDownlatch_error() throws Exception {
		pdb.setPictureUrl("http://www.priceza.com/test.jpg");
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;

		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		Mockito.doThrow(new MockitoException("UnitTest Error")).when(mockLatch).countDown();
		when(xmlFeedCrawler.readData(mockFeed)).thenAnswer(invocation -> { throw new SQLException("SQL Exception");}).thenReturn(null); 
		when(productDataDB.insertProductData(pdb)).thenReturn(0);
		parserProcessor.setProperties(mockFeed, mockLatch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		mockLatch.await();
		executors.shutdown();
		assertEquals(exFuture.get(),false);
	}
	
	@Test
	public void parserProcessor_config_ignore_name() throws Exception {
		ParserProcessor parserProcessor = new ParserProcessor();
		BaseConfig.CONF_IGNORE_NAME_CHANGE = true;
		BaseConfig.FEED_TYPE_CONF = FEED_TYPE.XML;
		latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		when(urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED")).thenReturn(new ArrayList<UrlPatternBean>());
		mockFeed = new MockFeedCrawler();
		when(xmlFeedCrawler.readData(mockFeed)).thenReturn(pdb).thenReturn(null);
		when(productDataDB.insertProductData(pdb)).thenReturn(1);
		parserProcessor.setProperties(mockFeed, latch);
		ExecutorService executors = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		Future<Boolean> exFuture = executors.submit(parserProcessor);
		latch.await();
		executors.shutdown();
		Mockito.verify(productDataDB).getProductByUrlForUpdate(BaseConfig.MERCHANT_ID,pdb.getUrl());
		Mockito.verify(productDataDB).insertProductData(pdb);
		Mockito.verify(sendDataDB).insertSendData(Mockito.any(SendDataBean.class));
		assertEquals(exFuture.get(),true);
	}
	
	
}
