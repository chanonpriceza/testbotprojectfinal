package bot;

import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.dbutils.QueryRunner;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;
import db.MerchantDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.MerchantManager;
import unittest.MockObjectHolder;


public class MerchantManagerTest {
	
	SimpleDateFormat fmt =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Mock DatabaseManager databaseManager;
	@Mock QueryRunner q;
	@Mock MerchantDB merchantDB;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockObjectHolder.isMock=true;
		BaseConfig.MERCHANT_ACTIVE = 1;
		MockObjectHolder.mockDatabaseManager = databaseManager;
		MockObjectHolder.queryRunnerDB = q;
		BaseConfig.MERCHANT_ID = 1000;
		BaseConfig.NEXT_START = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");

	}
	
	@Test
	public void active3_updateNextStart() throws Exception {
		BaseConfig.MERCHANT_ACTIVE = 3;
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		Date newDate = new Date();
		MerchantManager.updateNextStart();
		newDate = removeMilliSecond(newDate);
		Mockito.verify(merchantDB).updateActiveNextStart(Mockito.eq(BaseConfig.MERCHANT_ID),Mockito.eq(4),Mockito.eq(newDate), Mockito.eq(STATE.BOT_FINISH), Mockito.eq(STATUS.FINISH));	
	}
	
	@Test
	public void updateNextStart() throws Exception {
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		Date newDate = new Date();
		MerchantManager.updateNextStart();
		newDate = removeMilliSecond(newDate);
		Mockito.verify(merchantDB).updateNextStart(Mockito.eq(BaseConfig.MERCHANT_ID),Mockito.eq(newDate),Mockito.eq(STATE.BOT_FINISH),Mockito.eq(STATUS.FINISH));
	}
	
	@Test
	public void updateDateEqualCurrent_updateNextStart() throws Exception {
		BaseConfig.NEXT_START = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-12-12 00:00:00");
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		Date newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-12-13 00:00:00");
		MerchantManager.updateNextStart();
		Mockito.verify(merchantDB).updateNextStart(Mockito.eq(BaseConfig.MERCHANT_ID),Mockito.eq(newDate),Mockito.eq(STATE.BOT_FINISH),Mockito.eq(STATUS.FINISH));
		
	}
	
	private Date removeMilliSecond(Date d) throws Exception{
		Date newDate = new Date();
		String resultTest = fmt.format(newDate);
		newDate = fmt.parse(resultTest);
		return newDate;
	}
}
