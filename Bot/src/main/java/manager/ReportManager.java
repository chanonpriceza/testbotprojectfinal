package manager;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.sql.SQLException;

import bean.ReportBean;
import db.ProductDataDB;
import db.ReportDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;

public class ReportManager {
	private ReportBean report;
	private ProductDataDB productdataDB;
	private ReportDB reportDB;
	
	private static class SingletonHelper {
        private static final ReportManager INSTANCE = new ReportManager();
    }
	
	public static ReportManager getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.mockReportManager;
		}
		return SingletonHelper.INSTANCE;
	}
	
	public ReportManager() {
		productdataDB = DatabaseManager.getInstance().getProductDataDB();
		reportDB = DatabaseManager.getInstance().getReportDB();
		report = new ReportBean();
		report.setMerchantId(BaseConfig.MERCHANT_ID);
		report.setName(BaseConfig.MERCHANT_NAME);
		report.setPackageType(BaseConfig.PACKAGE_TYPE);
		report.setType(BaseConfig.RUNTIME_TYPE);
		report.setStatus(ReportBean.STATUS.RUNNING.toString());
		report.setDefineDate(BaseConfig.NEXT_START);
	}
	
	public void countOldProduct() throws SQLException {
		int oldProduct = productdataDB.countProductByMerchant(report.getMerchantId());
		report.setCountOld(oldProduct);
	}
	
	public void countNewProduct() {
		report.setCountNew(report.getCountOld() + report.getAdd());
	}
	
	public void insertReport() throws SQLException {
		int id = reportDB.insertReport(report);
		report.setId(id);
	}
	
	public void updateReport() throws SQLException {
		reportDB.updateReport(report, ReportBean.STATUS.RUNNING);
	}
	
	public void markFail() throws SQLException {
		reportDB.markLastReport(BaseConfig.MERCHANT_ID, BaseConfig.RUNTIME_TYPE, ReportBean.STATUS.RUNNING, ReportBean.STATUS.FAILED);
	}

	public ReportBean getReport() {
		return report;
	}
	
	public void startReport() throws SQLException {
		markFail();
		insertReport();
	}
	
	public void finishReport() throws SQLException {
		if(isBlank(report.getStatus()) || report.getStatus().equals(ReportBean.STATUS.RUNNING.toString()))
			markStatus(ReportBean.STATUS.PASSED);
		updateReport();
	}
	
	public void markStatus(ReportBean.STATUS status) {
		report.setStatus(status.toString());
	}
}
