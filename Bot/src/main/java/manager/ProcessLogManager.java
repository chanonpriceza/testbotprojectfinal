package manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import engine.BaseConfig;

public class ProcessLogManager {
	
	private static final Logger logger = LogManager.getRootLogger();
	private static final int	MAX_PRINT_NUM = 10;
	
	private static class SingletonHelper {
        private static final ProcessLogManager INSTANCE = new ProcessLogManager();
    }
	
	public enum ERRORSTATUS {
		URLFORUPDATECHANE,DUPLICATENAME,NAMEISBLANK,URLFORUPDATEISBLANK,
		URLCHANGE,URLISBLANK,UPDATECAT,IMAGEERRORPARSE,EXPIREPRODUCT
	}
	
	public enum EXCEPTION{
		NULLPOINTER,
	}
	
	public static ProcessLogManager getInstance(){
		return SingletonHelper.INSTANCE;
	}
	
	//----------------- ERROR PARSE CASE ----------------------------------
	
	private Map<String,String> urlForUpdateChange;
	private Map<String,String> duplicateName;
	private Map<String,String> nameIsBlank;
	private Map<String,String> urlForUpdateIsBlank;
	private Map<String,String> urlChange;
	private Map<String,String> urlIsBlank;
	private Map<String,String> priceZero;
	private Map<String,String> nameIsChange;
	private Map<String,String> cantFindOldBean;
	
	//--------------------- Count Processor -------------------------------
	
	private int countUrlForUpdateChange; //
	private int countDuplicateName; //
	private int countNameIsBlank; //
	private int countUrlForUpdateIsBlank; //
	private int countCounturlChange;
	private int countUrlIsBlank;
	private int countPriceZero;
	private int countNameIsChange;
	private int countCantFindOldBean;
	
	//----------------- Exception CASE -------------------------------------
	
	private Map<String,Exception> exception;
	
	public ProcessLogManager() {
		
		urlForUpdateChange 	= new HashMap<String, String>();
		duplicateName	    = new HashMap<String, String>();
		nameIsBlank			= new HashMap<String, String>();
		urlForUpdateIsBlank = new HashMap<String, String>();
		urlChange 			= new HashMap<String, String>();
		urlIsBlank 			= new HashMap<String, String>();
		exception			= new HashMap<String, Exception>();
		priceZero			= new HashMap<String, String>();
		nameIsChange		= new HashMap<String, String>();
		cantFindOldBean		= new HashMap<String, String>();
		
	}
	
	public void printLog() {
		if(!BaseConfig.CONF_DISABLE_LOG) {
			
			logger.info("Enable Process Log");
			
			if(urlForUpdateChange.size() > 0) {
				printHeader("urlForUpdateChange");
				printDetail(urlForUpdateChange,countUrlForUpdateChange);
			}
			if(duplicateName.size() > 0) {
				printHeader("duplicateName");
				printDetail(duplicateName,countDuplicateName);
			}
			if(nameIsBlank.size() > 0) {
				printHeader("nameIsBlank");
				printDetail(nameIsBlank,countNameIsBlank);
			}
			if(urlForUpdateIsBlank.size() > 0) {
				printHeader("urlForUpdateIsBlank");
				printDetail(urlForUpdateIsBlank,countUrlForUpdateIsBlank);
			}
			if(urlChange.size() > 0) {
				printHeader("urlChange");
				printDetail(urlChange,countCounturlChange);
			}
			if(urlIsBlank.size() > 0) {
				printHeader("urlIsBlank");
				printDetail(urlIsBlank,countUrlIsBlank);
			}

			if(priceZero.size() > 0) {
				printHeader("priceZero");
				printDetail(priceZero,countPriceZero);
			}
			if(nameIsChange.size() > 0) {
				printHeader("nameIsChange");
				printDetail(nameIsChange,countNameIsChange);
			}
			if(cantFindOldBean.size() > 0) {
				printHeader("cantFindOldBean");
				printDetail(cantFindOldBean,countCantFindOldBean);
			}
			if(exception.size() > 0) {
				printHeader("Exception");
				printExceptionDetail(exception);
			}
		}
	}
	
	public synchronized  void  addUrlForUpdateChange(String oldData,String newData) {
		if(urlForUpdateChange.size() < MAX_PRINT_NUM) 
			urlForUpdateChange.put(oldData,newData);
		countUrlForUpdateChange++;
	}

	public synchronized void addDuplicateName(String oldData,String newData) {
		if(duplicateName.size() < MAX_PRINT_NUM) 
			duplicateName.put(oldData,newData);
		countDuplicateName++;
		
	}

	public synchronized void addNameIsBlank(String oldData,String newData) {
		if(nameIsBlank.size() < MAX_PRINT_NUM) 
			nameIsBlank.put(oldData,newData);
		countNameIsBlank++;
	}

	public synchronized void addUrlForUpdateIsBlank(String oldData,String newData) {
		if(urlForUpdateIsBlank.size() < MAX_PRINT_NUM) 
			urlForUpdateIsBlank.put(oldData,newData);
		countUrlForUpdateIsBlank++;
	}

	public synchronized void addUrlChange(String oldData,String newData) {
		if(urlChange.size() < MAX_PRINT_NUM) 
			urlChange.put(oldData,newData);
		countCounturlChange++;
	}

	public synchronized void addUrlIsBlank(String oldData,String newData) {
		if(urlIsBlank.size() < MAX_PRINT_NUM) 
			urlIsBlank.put(oldData,newData);
		countUrlIsBlank++;
	}
	
	public synchronized void addException(String type,Exception e) {
		if(exception.size() < MAX_PRINT_NUM) 
			exception.put(type,e);
	}
	
	public synchronized void addPriceZero(String oldData,String newData) {
		if(priceZero.size() < MAX_PRINT_NUM) 
			priceZero.put(oldData,newData);
		countPriceZero++;
	}
	
	public synchronized void addNameIsChange(String oldData,String newData) {
		if(nameIsChange.size() < MAX_PRINT_NUM) 
			nameIsChange.put(oldData,newData);
		countNameIsChange++;
	}
	
	public synchronized void addCantFindOldBean(String oldData,String newData) {
		if(cantFindOldBean.size() < MAX_PRINT_NUM) 
			cantFindOldBean.put(oldData,newData);
		countCantFindOldBean++;
	}
	
	
	private  static void printHeader(String header) {
		
		logger.info("----------------------------------------------------------------------------");
		logger.info("\t\t\t\t\t\t\t"+header);
		logger.info("----------------------------------------------------------------------------");
	}
	
	private  static void printDetail(Map<String,String> map,int count) {
		int i = 1;
		for(Entry<String,String> entry:map.entrySet()) {
			logger.info(i+" Previous Data: "+entry.getKey()+" Update Data: "+entry.getValue());
			i++;
		}
		logger.info("total : "+count);
	}
	
	private static void printExceptionDetail(Map<String,Exception> map) {
		int i = 1;
		for(Entry<String,Exception> entry:map.entrySet()) {
			/*String exceptions = "";
			for(int r = 0;r<entry.getValue().getStackTrace().length;r++) {
				StackTraceElement e = entry.getValue().getStackTrace()[r];
				if(r >= 3) 
					break;
				exceptions += "Class name :"+e.getClassName()+" Line: "+e.getLineNumber()+" , ";
			}*/
			logger.info(i+" Exception Type: "+entry.getKey(),entry.getValue());
			i++;
		}
	}
	
	
}
