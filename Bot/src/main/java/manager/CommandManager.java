package manager;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.sql.SQLException;

import bean.MerchantBean;
import db.MerchantDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;

public class CommandManager {

	public static final String PRIMARY_WCE = "PRIMARY_WCE";
	public static final String PRIMARY_DUE = "PRIMARY_DUE";
	public static final String KILL_WCE = "KILL_WCE";
	public static final String KILL_DUE = "KILL_DUE";
	public static final String DELETE_ALL = "DELETE_ALL";
	public static final String DELETE_NOT_MAP = "DELETE_NOT_MAP";
	
	private MerchantBean merchantBean;
	private MerchantDB merchantDB;
	
	private static class SingletonHelper {
        private static final CommandManager INSTANCE = new CommandManager();
    }
	
	public static CommandManager getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.commandManager;
		}
		return SingletonHelper.INSTANCE;
	}
	
	public CommandManager() {
		 merchantDB = DatabaseManager.getInstance().getMerchantDB();
	}
	
	public void clearCommand(String command, boolean needCheck) throws SQLException {
		if(needCheck) {
			if(!checkCommand(command)) {
				return;
			}
		}
		clearCommand(command);
	}

	public void clearCommand(String command) throws SQLException {
		if(merchantBean == null)
			return;

		merchantDB.clearCommand(BaseConfig.MERCHANT_ID, command+",");
	}
	
	public boolean checkCommand(String command) throws SQLException {
		updateMerchantBean();
		if(merchantBean != null) {
			if(!isBlank(merchantBean.getCommand()) && merchantBean.getCommand().contains(command)) {
				return true;
			}
		}
		return false;
	}
	
	private synchronized void updateMerchantBean() throws SQLException{
		merchantBean = merchantDB.getMerchantById(BaseConfig.MERCHANT_ID);
	}
	
	
}