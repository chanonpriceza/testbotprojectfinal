package manager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.zip.GZIPInputStream;

import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import bean.ConfigBean;
import db.ConfigDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import utils.BotUtil;
import utils.DateTimeUtil;
import utils.HTTPUtil;

@SuppressWarnings("unused")
public class MonitoringManager {
	
	private ConfigDB configDB;
	private long countOld, countNew, countSuccess, countError;
	private long wceWorkloadCount, wceWorkloadCountSuccess, wceWorkloadCountFailed;
	private long wceProductUrlCount, wceProductUrlCountSuccess, wceProductUrlCountFailed;
	private long wceAdd, wceDuplicate;
	
	private boolean isWeb, isFeed, isExceed,isCheckApprove;
	
	private static final Logger logger = LogManager.getRootLogger();
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private final String CONF_MONITORING_SERVICE_URL_PREFIX = "monitoring.service.url.";
	
	private static class SingletonHelper {
        private static final MonitoringManager INSTANCE = new MonitoringManager();
    }
	
	public static MonitoringManager getInstance(){
		return SingletonHelper.INSTANCE;
	}
	
	public MonitoringManager() {
		configDB = DatabaseManager.getInstance().getConfigDB();
	}
	
	@SuppressWarnings("unchecked")
	public void calculate() {
		try {
			gatherAllStat();
			
			ConfigBean configBean = configDB.getByName(CONF_MONITORING_SERVICE_URL_PREFIX + BotUtil.LOCALE.toLowerCase());
			if(configBean != null) {
				
				String[] analyzeResult = null;
				
				// check 
				if(analyzeResult == null) analyzeResult = checkIsApprove();
				if(analyzeResult == null) analyzeResult = checkExceed();
				if(analyzeResult == null) analyzeResult = checkWorkload();
				if(analyzeResult == null) analyzeResult = checkProductUrl();
				if(analyzeResult == null) analyzeResult = checkCurrentProduct();
				if(analyzeResult == null) analyzeResult = checkParser();
				if(analyzeResult == null) analyzeResult = checkFeed();
				
				// everything is ok
				if(analyzeResult == null) analyzeResult = new String[] {"Stable", "This merchant work correctly."};
				
				JSONObject dataObj = new JSONObject();
				dataObj.put("type"                  	  , BaseConfig.RUNTIME_TYPE);
				dataObj.put("merchantId"                  , BaseConfig.MERCHANT_ID);
				dataObj.put("analyzeDate"                 , DateTimeUtil.generateStringDateTime(new Date()));
				dataObj.put("analyzeResult"               , analyzeResult[0]);
				dataObj.put("analyzeDetail"               , analyzeResult[1]);
				
				// send result to service
				String url = configBean.getConfigValue();
				String[] postResult = sendMernitorData(url, dataObj.toJSONString());
				if(postResult != null && postResult.length == 2) {
					logger.info("Send monitoring service, response code " + postResult[1] + " : " + postResult[0]);
				} else {
					logger.info("No response from monitoring backend service.");
				}
			} else {
				logger.info("No config for monitoring service.");
			}
		} catch(SQLException e) {
			logger.error(e);
			ProcessLogManager.getInstance().addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		}
	}
	
	private void gatherAllStat() {
		isWeb                       = ReportManager.getInstance().getReport().isWeb();
		isFeed                      = ReportManager.getInstance().getReport().isFeed();
		isExceed 					= ReportManager.getInstance().getReport().isExceed();
		countOld                    = ReportManager.getInstance().getReport().getCountOld();
		countNew                    = ReportManager.getInstance().getReport().getCountNew();
		countSuccess                = ReportManager.getInstance().getReport().getSuccessCount();
		countError                  = ReportManager.getInstance().getReport().getErrorCount();
		wceWorkloadCount 			= ReportManager.getInstance().getReport().getWceWorkloadCount(); 
		wceWorkloadCountSuccess 	= ReportManager.getInstance().getReport().getWceWorkloadCountSuccess(); 
		wceWorkloadCountFailed 		= ReportManager.getInstance().getReport().getWceWorkloadCountFailed(); 
		wceProductUrlCount 			= ReportManager.getInstance().getReport().getWceProductUrlCount();
		wceProductUrlCountSuccess	= ReportManager.getInstance().getReport().getWceProductUrlCountSuccess();
		wceProductUrlCountFailed 	= ReportManager.getInstance().getReport().getWceProductUrlCountFailed();
	}
	
	// #####################################################################
	// ############### CHECK METHOD ########################################
	// #####################################################################
	
	private String[] checkExceed() {
		if(BaseConfig.RUNTIME_TYPE.equals("DUE")) {
			if(isExceed)
				return new String[] {"ExceedDeleteLimit", "Exceed delete limit = " + BaseConfig.CONF_DELETE_LIMIT};
		}
		return null;
	}
	
	private String[] checkWorkload() {
		if(BaseConfig.RUNTIME_TYPE.equals("WCE") && isWeb) {
			if(wceWorkloadCount == 0) 
				return new String[] {"NoUrlPattern", "Web, Cannot load urlPattern, Cannot continue."};
			if(wceWorkloadCountSuccess == 0)
				// add process to collect error cause, later
				return new String[] {"WorkloadFailedAll", "Web, Workload fail all."};
		}
		return null;
	}
	
	private String[] checkIsApprove() {
		if(BaseConfig.MERCHANT_ACTIVE==3)
			return new String[] {"isCheckApprove", "Check Approve Alert"};
		return null;
	}
	
	private String[] checkProductUrl() {
		if(BaseConfig.RUNTIME_TYPE.equals("WCE") && isWeb) {
			if(countOld == 0 && wceProductUrlCount == 0)
				return new String[] {"CannotMatch", "Web, Dont have product remain, Cannot match the new one." };
			if(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL && wceProductUrlCount == 0)
				return new String[] {"CannotMatch", "Web, Dont have product remain, Cannot match the new one." };
			if(wceProductUrlCount > 0 && wceProductUrlCountSuccess == 0)
				// add process to collect error cause, later
				return new String[] {"CannotParse", "Web, Get new product urls, error all." };
		}
		return null;
	}
	
	private String[] checkCurrentProduct() {
		if(BaseConfig.RUNTIME_TYPE.equals("DUE")) {
			if(countOld == 0 && countSuccess == 0)
				return new String[] {"NotFoundProduct", "Not found product to update." };
		}
		return null;
	}
	
	private String[] checkParser() {
		// for check parser work correctly ?
		return null;
	}
	
	private String[] checkFeed() {
		// for check feed download successfully ? file exist ? file size on server is not 0 byte ?
		return null;
	}
	
	public  String[] sendMernitorData(String url,String json) {
		HttpURLConnection conn = null;
		boolean success = false;
		String charset = "UTF-8";
		String putNotificationURL = url;
		try {
			URI bulder = new URIBuilder(putNotificationURL).addParameter("monitorBean",json).build();
			URL u = new URL(bulder.toString());
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(true);	
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write(json.getBytes());
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null,String.valueOf(status)};
			 
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = charset == null ? 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
								new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
					BufferedReader brd = new BufferedReader(isr);) {
				
					StringBuilder rtn = new StringBuilder();
					String line = null;
					while ((line = brd.readLine()) != null)
						rtn.append(line);
					return new String[]{rtn.toString(), String.valueOf(status)};
				}
			}catch (Exception e) {
				e.printStackTrace();
				success = false;
			}finally {
				if(!success)
					consumeInputStreamQuietly(conn.getErrorStream());
				if(conn != null)
					conn.disconnect();
			}
			return null;
		}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
