package manager;

import java.text.SimpleDateFormat;
import java.util.Date;

import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import utils.DateTimeUtil;

public class MerchantManager {
	
	private static SimpleDateFormat fmt =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static void updateNextStart() throws Exception {
		int hourRate  =   BaseConfig.CONF_BOT_QUALITY_HOUR_RATE;
		
		if(BaseConfig.RUNTIME_TYPE.equals("WCE") && (BaseConfig.CONF_BOT_WCE_HOUR_RATE != BaseConfig.CONF_BOT_QUALITY_HOUR_RATE) && BaseConfig.CONF_BOT_WCE_HOUR_RATE != 24) {
			hourRate = BaseConfig.CONF_BOT_WCE_HOUR_RATE;
		}
		if(BaseConfig.RUNTIME_TYPE.equals("DUE") && (BaseConfig.CONF_BOT_DUE_HOUR_RATE != BaseConfig.CONF_BOT_QUALITY_HOUR_RATE) && BaseConfig.CONF_BOT_DUE_HOUR_RATE != 24)	{
			 hourRate = BaseConfig.CONF_BOT_DUE_HOUR_RATE;
		}
		
		Date nextStart = DateTimeUtil.generateDateHourOffset(BaseConfig.NEXT_START, hourRate);
		Date now = fmt.parse(fmt.format(new Date()));
		
		if(nextStart.after(DateTimeUtil.generateDateHourOffset(now,hourRate)))
			nextStart = DateTimeUtil.generateDateHourOffset(now,hourRate);
		if(nextStart.before(now))
			nextStart = DateTimeUtil.generateDateHourOffset(now,hourRate);
		
		if(BaseConfig.MERCHANT_ACTIVE == 3) 
			DatabaseManager.getInstance().getMerchantDB().updateActiveNextStart(BaseConfig.MERCHANT_ID, 4, nextStart, STATE.BOT_FINISH, STATUS.FINISH);
		else
			DatabaseManager.getInstance().getMerchantDB().updateNextStart(BaseConfig.MERCHANT_ID, nextStart, STATE.BOT_FINISH, STATUS.FINISH);
	}
}
