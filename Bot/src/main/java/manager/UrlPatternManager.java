package manager;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import bean.PatternBean;
import bean.PatternResultBean;
import bean.UrlPatternBean;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;
import web.crawler.bot.URLUtility;

public class UrlPatternManager {

	private Vector<Vector<PatternBean>> allPatternMatch;
	private Vector<Vector<PatternBean>> allPatternContinue;
	private Vector<Vector<PatternBean>> allPatternDrop;
	private Vector<Vector<PatternBean>> allPatternRemove;	
	private Vector<Vector<PatternBean>> allPatternStartUrl;	
	private Vector<Vector<PatternBean>> allPatternFeed;
	
	private static class SingletonHelper {
        private static final UrlPatternManager INSTANCE = new UrlPatternManager(BaseConfig.MERCHANT_ID);
    }
	
	public static UrlPatternManager getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.urlPatternManager;
		}
		return SingletonHelper.INSTANCE;
	}
	
	public UrlPatternManager(int merchantId) { // default constructor for easy use, get DB from DatabaseManager
		this(DatabaseManager.getInstance().getUrlPatternDB(), merchantId);
	}
	
	public UrlPatternManager(UrlPatternDB urlPatternDB, int merchantId) { // constructor for dependency-injection
		allPatternMatch = new Vector<Vector<PatternBean>>();
		allPatternContinue = new Vector<Vector<PatternBean>>();
		allPatternDrop = new Vector<Vector<PatternBean>>();
		allPatternRemove = new Vector<Vector<PatternBean>>();
		allPatternStartUrl = new Vector<Vector<PatternBean>>();
		allPatternFeed = new Vector<Vector<PatternBean>>();
				
		try {
			List<UrlPatternBean> urlPatternList = urlPatternDB.findUrlPattern(merchantId);
			
			HashMap<Integer, Vector<PatternBean>> groupM = new HashMap<Integer, Vector<PatternBean>>();
			HashMap<Integer, Vector<PatternBean>> groupC = new HashMap<Integer, Vector<PatternBean>>();
			HashMap<Integer, Vector<PatternBean>> groupD = new HashMap<Integer, Vector<PatternBean>>();
			HashMap<Integer, Vector<PatternBean>> groupR = new HashMap<Integer, Vector<PatternBean>>();
			HashMap<Integer, Vector<PatternBean>> groupS = new HashMap<Integer, Vector<PatternBean>>();
			HashMap<Integer, Vector<PatternBean>> groupF = new HashMap<Integer, Vector<PatternBean>>();
			
			for (UrlPatternBean urlPatternBean : urlPatternList) {
				
				String condition = urlPatternBean.getCondition();
				String value = urlPatternBean.getValue();
				String action = urlPatternBean.getAction();
				int categoryId = urlPatternBean.getCategoryId();
				String keyword = urlPatternBean.getKeyword();
				int group = urlPatternBean.getGroup();

				if (condition == null) condition = "";
				if (value == null) value = "";
				if (action == null) action = "";
					
				PatternBean pattern = new PatternBean();
				pattern.setCondition(condition);
				pattern.setValue(value);
				pattern.setCategoryId(categoryId);
				pattern.setKeyword(keyword);
				
				HashMap<Integer, Vector<PatternBean>> currentGroup = null;
				Vector<Vector<PatternBean>> currentVector = null;
				if (action.equalsIgnoreCase("MATCH")) {						
					currentGroup = groupM;
					currentVector = allPatternMatch;
				} else if (action.equalsIgnoreCase("CONTINUE")) {
					currentGroup = groupC;
					currentVector = allPatternContinue;
				} else if (action.equalsIgnoreCase("DROP")) {
					currentGroup = groupD;
					currentVector = allPatternDrop;
				} else if (action.equalsIgnoreCase("REMOVE")) {
					currentGroup = groupR;
					currentVector = allPatternRemove;
				} else if (action.equalsIgnoreCase("STARTURL")) {
					currentGroup = groupS;
					currentVector = allPatternStartUrl;
				} else if (action.equalsIgnoreCase("FEED")) {
					currentGroup = groupF;
					currentVector = allPatternFeed;
				}
				
				if(currentGroup == null){						
					System.out.println("found invalid action "+action);
					continue;
				}
					
				if(group == 0) {
					Vector<PatternBean> v = new Vector<PatternBean>();
					v.add(pattern);
					currentVector.add(v);
					continue;
				}
				
				Vector<PatternBean> v = currentGroup.get(group);
				if(v == null) {
					v = new Vector<PatternBean>();
					v.add(pattern);			
					currentGroup.put(group, v);
				} else {
					v.add(pattern);
				}
			}
			
			if (groupM.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupM.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternMatch.add(vector);
				}
			}
			if (groupC.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupC.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternContinue.add(vector);
				}
			}
			if (groupD.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupD.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternDrop.add(vector);
				}
			}
			if (groupR.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupR.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternRemove.add(vector);
				}
			}
			if (groupS.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupS.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternStartUrl.add(vector);
				}
			}
			if (groupF.size() > 0) {
				Collection<Vector<PatternBean>> collection = groupF.values();
				for (Vector<PatternBean> vector : collection) {
					allPatternFeed.add(vector);
				}
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getFirstStartUrl() {		
		for (Vector<PatternBean> patternVector : allPatternStartUrl) {
			for (PatternBean patternBean : patternVector) {			
				return patternBean.getValue();				
			}
		}		
		return null;
	}

	public PatternResultBean isPatternMatch(String url) {
		return checkURLPatternMatch(url, allPatternMatch);
	}

	public PatternResultBean isPatternContinue(String url) {
		return checkURLPatternMatch(url, allPatternContinue);
	}

	public PatternResultBean isPatternDrop(String url) {
		return checkURLPatternMatch(url, allPatternDrop);
	}

	public String processRemoveQuery(String url) {
		if (allPatternRemove.size() == 0)
			return url;
		for (Vector<PatternBean> patternVector : allPatternRemove) {
			for (PatternBean patternBean : patternVector) {					
				if (patternBean.getCondition().equalsIgnoreCase("haveQuery")
						&& checkURLhaveQuery(url, patternBean.getValue())) {
					url = URLUtility.removeQueryString(url, patternBean.getValue());
				}
			}
		}
		return url;
	}

	public PatternResultBean checkURLPatternMatch(String url, Vector<Vector<PatternBean>> allPattern) {

		PatternResultBean rtn = new PatternResultBean();
		
		if (allPattern.size() == 0) {
			return rtn;
		}
				
		for (Vector<PatternBean> patternVector : allPattern) {			
			boolean t = true;
			for (PatternBean patternBean : patternVector) {				
				if (patternBean.getCondition().equalsIgnoreCase("haveString")) {
					if (checkURLhaveString(url, patternBean.getValue())) {								
						t = true;
					} else {
						t = false;
						break;
					}
				} else if (patternBean.getCondition().equalsIgnoreCase("haveQuery")) {
					if (checkURLhaveQuery(url, patternBean.getValue())) {
						t = true;
					} else {
						t = false;
						break;
					}
				} else if (patternBean.getCondition().equalsIgnoreCase("endsWith")) {
					if (checkURLEndsWith(url, patternBean.getValue())) {
						t = true;
					} else {
						t = false;
						break;
					}
				} else if (patternBean.getCondition().equalsIgnoreCase("equals")) {
					if (checkURLEquals(url, patternBean.getValue())) {
						t = true;
					} else {
						t = false;
						break;
					}
				} else {
					t = false;
					break;
				}
			}

			if (t == true) {				
				rtn.setPass(true);	
				for (PatternBean patternBean : patternVector) {				
					if(patternBean.getCategoryId() > 0) {
						rtn.setCategoryId(patternBean.getCategoryId());
						break;
					}			
				}
				for (PatternBean patternBean : patternVector) {				
					if(patternBean.getKeyword() != null && patternBean.getKeyword().trim().length() != 0) {
						rtn.setKeyword(patternBean.getKeyword());
						break;
					}			
				}
				
				break;
			}
		}
		return rtn;
	}

	public boolean checkURLhaveString(String url, String test) {
		return (url.toUpperCase().indexOf(test.toUpperCase()) == -1) ? false
				: true;
	}

	public boolean checkURLEndsWith(String url, String test) {
		return (url.toUpperCase().endsWith(test.toUpperCase())) ? true : false;
	}

	public boolean checkURLEquals(String url, String test) {
		return (url.equalsIgnoreCase(test)) ? true : false;
	}

	public boolean checkURLhaveQuery(String url, String test) {
		int eqSign = test.indexOf("=");
		if (eqSign == -1) {
			return URLUtility.haveQueryString(url, test, "");
		} else {
			return URLUtility.haveQueryString(url, test.substring(0, eqSign),
					test.substring(eqSign + 1, test.length()));
		}
	}
	
	
	public Vector<Vector<PatternBean>> getAllPatternMatch() {
		return allPatternMatch;
	}

	public Vector<Vector<PatternBean>> getAllPatternContinue() {
		return allPatternContinue;
	}

	public Vector<Vector<PatternBean>> getAllPatternDrop() {
		return allPatternDrop;
	}

	public Vector<Vector<PatternBean>> getAllPatternRemove() {
		return allPatternRemove;
	}

	public Vector<Vector<PatternBean>> getAllPatternStartUrl() {
		return allPatternStartUrl;
	}

	public Vector<Vector<PatternBean>> getAllPatternFeed() {
		return allPatternFeed;
	}

}