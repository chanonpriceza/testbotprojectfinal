package feed;

import bean.ProductDataBean;

public interface TypeFeedCrawlerInterface {
	public enum FEED_TYPE {
		XML, CSV, JSON
	}
	
	ProductDataBean readData(CallBack callback);
	boolean readFile();
	void close();
}
