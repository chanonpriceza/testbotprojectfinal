package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LooksiCJFeedCrawler extends FeedManager{

	public LooksiCJFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.semtrack.de/e?i=22d311eb0d7166e2faea528c7915fd4e15f10548"
		};
	}

	StringBuilder tmp;
	
	public ProductDataBean parse(String xml) {
		String productName = FilterUtil.getStringBetween(xml, "<Product_Name><![CDATA[", "]]></Product_Name>");		
		String productPrice = FilterUtil.getStringBetween(xml, "<Price><![CDATA[", "]]></Price>");
		String productSalePrice = FilterUtil.getStringBetween(xml, "<Special_Price><![CDATA[", "]]></Special_Price>");
		String productUrl = FilterUtil.getStringBetween(xml, "<URL><![CDATA[", "]]></URL>");
		String productImageUrl = FilterUtil.getStringBetween(xml, "<Image_URL><![CDATA[", "]]></Image_URL>");
		//String productCatId = FilterUtil.getStringBetween(xml, "<advertisercategory>", "</advertisercategory>");
		// Feed Change Cat from advertisercategory to keywords 
		String productCatId = FilterUtil.getStringBetween(xml, "<Category><![CDATA[", "]]></Category>");
		String productDesc = FilterUtil.getStringBetween(xml, "<Description><![CDATA[", "]]></Description>");
		String productKeyword = FilterUtil.getStringBetween(xml, "<Category><![CDATA[", "]]></Category>");
		String productBrand = FilterUtil.getStringBetween(xml, "<Brand><![CDATA[", "]]></Brand>");
		String productColor = FilterUtil.getStringBetween(xml, "<COLOR><![CDATA[", "]]></COLOR>");
		String productGender = FilterUtil.getStringBetween(xml, "<GENDER><![CDATA[", "]]></GENDER>");
		
		
		String productId = FilterUtil.getStringBetween(xml, "<SKU><![CDATA[", "]]></SKU>");
		productUrl = productUrl.replace("&amp;", "&");
//		productUrl = removeDeeplinkUTM(productUrl);
		productImageUrl = productImageUrl.replace("&amp;", "&");
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 || productId.trim().length() == 0) {
			return null;			
		}
		
		ProductDataBean pdb = new ProductDataBean();	
		
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		productBrand = StringEscapeUtils.unescapeHtml4(productBrand);	
		productColor = StringEscapeUtils.unescapeHtml4(productColor);	
		productGender = StringEscapeUtils.unescapeHtml4(productGender);	
		
		if(productBrand.trim().length() != 0) {
			pdb.setName(productBrand.trim() + " " + productName.trim() + " "+ productGender.trim()  + " (" + productColor.trim() + ")"+ " (" + productId.trim() + ")");			
		} else {
			pdb.setName(productName.trim() + " "+ productGender.trim()  + " (" + productColor.trim() + ")"+ " (" + productId.trim() + ")");	
		}
		
		pdb.setRealProductId(productId.trim());
		
		if(productSalePrice.trim().length() != 0) {
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productSalePrice)));
		} else {
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		}
		
		if (productImageUrl.length() != 0) {
			productImageUrl = FilterUtil.getStringAfter(productImageUrl, ":quality(90)/", "");
			pdb.setPictureUrl(productImageUrl);
		}
		if (productUrl.length() != 0) {	
			productUrl = productUrl.replace("{affiliate_id}", "2913");
			productUrl = FilterUtil.getStringBefore(productUrl, "?utm", productUrl);
			pdb.setUrl(productUrl);
		}
		
		pdb.setUrlForUpdate(productId.trim());
		
		productCatId = productCatId.replace("&gt;", ">");
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));	
			pdb.setKeyword(catMap[1]);
		} else {
			pdb.setCategoryId(0);
			
			if(productKeyword.length() > 0 && !productKeyword.toLowerCase().contains("default")){
				productKeyword = productKeyword.replace("&gt;", " ");
				pdb.setKeyword(productKeyword);
			}
		}
		
		if(productDesc.length() > 0){
			pdb.setDescription(productDesc);
		}
				
		return pdb;
		
	}
	
	
}
