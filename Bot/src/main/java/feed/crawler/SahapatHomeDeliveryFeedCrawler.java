package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SahapatHomeDeliveryFeedCrawler extends FeedManager {
	
	private static String REQUEST_URL = "https://ws.sahapat.com/b2b/v4/Product/list";
	private static JSONParser parser = new JSONParser();

	public SahapatHomeDeliveryFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		List<String> feedFile = new ArrayList<String>();
		int fileNum = 0;
		
		for(String x:getCategoryKeySet()) {
			String cat = "";
			String keyword = "";
			cat = getCategory(x)[0];
			keyword = getCategory(x)[1];
			
			downloadPostRequestOptimezeJSON(x,BaseConfig.FEED_STORE_PATH+"/"+"70016_"+fileNum,cat,keyword);
			feedFile.add(BaseConfig.FEED_STORE_PATH+"/"+"70016_"+fileNum);
			fileNum++;
		}
		
		BaseConfig.FEED_FILE = feedFile.stream().toArray(String[]::new);
	}
	
	@SuppressWarnings("unchecked")
	public static boolean downloadPostRequestOptimezeJSON(String data,String fileName,String cat,String keyword) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(REQUEST_URL);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setInstanceFollowRedirects(true);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("Content-Type","application/json");
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
			outStream.writeBytes(data);
			outStream.flush();
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return false;
			
			try (InputStream is = conn.getInputStream();InputStreamReader isReader = new InputStreamReader(is)){
				JSONObject json = (JSONObject) parser.parse(isReader)  ;
				JSONArray array = (JSONArray)json.get("result");
				for(Object ob:array) {
					JSONObject castOb = (JSONObject) ob;
					castOb.put("cat",cat);
					castOb.put("keyword",keyword);
					castOb.remove("stocks");
					castOb.remove("characteristic");
					castOb.remove("showpagedtl");
					castOb.remove("brands");
					castOb.remove("statuses");
					castOb.remove("categories");
					castOb.remove("productmast");
				}
				String str = array.toJSONString();
				InputStream writeFileInputStream = new ByteArrayInputStream(str.getBytes());
				Files.copy(writeFileInputStream,new File(fileName).toPath(),StandardCopyOption.REPLACE_EXISTING);
			}catch (Exception e) {
				logger.error("Error",e);
			}

		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return false;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		
		String name = String.valueOf(data.get("productname"));
		String price = String.valueOf(data.get("special_price"));
		String desc = String.valueOf(data.get("description"));
		String image = String.valueOf(data.get("image"));
		String id = String.valueOf(data.get("productcode"));
		String basePrice = String.valueOf(data.get("price"));
		String url = "https://www.sahapatdelivery.com/product/"+id;
		String cat =  String.valueOf(data.get("cat"));
		String keyword =  String.valueOf(data.get("keyword"));
		String tmp = "";
		ProductDataBean pdb = new ProductDataBean();
		
		if(StringUtils.isNotBlank(id))
			tmp = " ("+id+")";
		
		pdb.setName(name+tmp);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl(url);
		pdb.setDescription(desc);
		pdb.setPictureUrl(image);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setCategoryId(BotUtil.stringToInt(cat, 0));
		pdb.setKeyword(keyword);
		
		
		
		return pdb;
	}

}
