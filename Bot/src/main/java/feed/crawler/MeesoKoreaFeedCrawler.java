package feed.crawler;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.zip.GZIPInputStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class MeesoKoreaFeedCrawler extends FeedManager {
	
	private static JSONParser parser = new JSONParser();

	public MeesoKoreaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		try {
		JSONArray array = new JSONArray();
		for(String x:getCategoryKeySet()) {
			String[] reposnse = httpPostRequestWithStatus("{\"permalink\":\"{data}\"}".replace("{data}",x));
			if(reposnse==null||reposnse.length==0||reposnse[0]==null)
				continue;
			JSONObject obj =(JSONObject) parser.parse(reposnse[0]);
			JSONObject cut = (JSONObject) obj.get("product");
			cut.remove("tags");
			cut.put("cat",x);
			array.add(cut);
		}
		 InputStream inputStream = new ByteArrayInputStream(array.toJSONString().getBytes(Charset.forName("UTF-8")));
		 Files.copy(inputStream,new File(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".json").toPath(),StandardCopyOption.REPLACE_EXISTING);
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error",e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".json"};
	}
	
	public static String[] httpPostRequestWithStatus(String data) {
		String charset = "UTF-8";
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL("https://api2.ketshopweb.com/api/v1/product/productDetail");
			conn = (HttpURLConnection)  u.openConnection();
			conn.setRequestMethod("POST");
			conn.setInstanceFollowRedirects(true);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("Accept","application/json, text/plain, */*");
			conn.setRequestProperty("accept-encoding","gzip, deflate, br");
			conn.setRequestProperty("content-type","application/json");
			conn.setRequestProperty("origin","https://meesokorea.com");
			conn.setRequestProperty("referer","https://meesokorea.com");
			conn.setRequestProperty("lang","lang1");
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
	 		conn.setDoOutput(true);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(data.getBytes("UTF-8"));
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
			
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String name = String.valueOf(data.get("title_lang1"));
		String price = String.valueOf(data.get("price"));
		String desc = String.valueOf(data.get("short_desc_lang1"));
		String url = String.valueOf(data.get("permalink_lang1"));
		String id = String.valueOf(data.get("id"));
		String image = String.valueOf(data.get("feature_img"));
		String cat  = String.valueOf(data.get("cat")); 
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setUrl("https://meesokorea.com/%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%AD%E0%B8%B5%E0%B8%A2%E0%B8%94%E0%B8%AA%E0%B8%B4%E0%B8%99%E0%B8%84%E0%B9%89%E0%B8%B2/"+url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl("https://meesokorea.com/"+image);
		
		String[] catMap = getCategory(cat);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);		
		}else {
			pdb.setCategoryId(0);
		}
		//130103	meeso รองพื้น

		return pdb;
	}

}
