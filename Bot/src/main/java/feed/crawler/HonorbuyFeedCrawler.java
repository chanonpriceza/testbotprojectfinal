package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;

import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HonorbuyFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	private boolean nextPage = false;
	
	public HonorbuyFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for (String linkUrl : keySet) {
			try {
				int page = 0;
				nextPage = true;
				while(!Thread.currentThread().isInterrupted() && nextPage){
					page++;
					String REQUEST_URL = linkUrl + page;
					String[] response1 = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
					if(response1 == null || response1.length != 2) continue;
					
					String pdHtml = response1[0];
					if(StringUtils.isBlank(pdHtml)) continue;
							
					pdHtml = FilterUtil.getStringBetween(pdHtml,"<div id=\\\"product_list\\\" class=\\\"products_block\\\">", "<div class=\\\"all-pagination pagination span6\\\" id=\\\"pagination\\\" >");
					List<String> pdHtmlCut = FilterUtil.getAllStringBetween(pdHtml,"<div class=\\\"product_desc\\\"><a href=\\\"", "\\\"");
					for (String linkproduct : pdHtmlCut) {
						String tmplink = linkproduct.replace("\\", "");
						if(tmplink.contains("www.honorbuy.com")){
							String[] response2 = HTTPUtil.httpRequestWithStatus(tmplink, "UTF-8", false);
							if(response2 == null || response2.length != 2) continue;
							
							String data = response2[0];
							if(StringUtils.isBlank(data)) continue;
							
							parseHtml(data, linkUrl);							
						}
					}
					if(page >= 50){
						nextPage = false;
					}
				}
			} catch (Exception e) {
				logger.error(e);
				continue;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public void parseHtml(String html, String linkUrl) {
		String[] map = getCategory(linkUrl);
		String category = map[0];
		String keyword = map[1];
		
		String name = getPdName(html);
		String description = getPdDescription(html);
		String price = getPdPrice(html);
		String basePrice = getPdBaseprice(html);
		String picurl = getPdPicUrl(html);
		String pdUrl = getPdUrl(html);
		String pdId = getPdId(html);

		if (pdUrl.isEmpty() || name.isEmpty() || price.isEmpty()) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
		pdb.setDescription(description);
		pdb.setUrl(pdUrl);
		pdb.setUrlForUpdate(pdUrl);
		pdb.setCategoryId(Integer.parseInt(category));
		pdb.setPictureUrl(picurl);
		pdb.setKeyword(keyword);
		pdb.setRealProductId(pdId);
		
//		System.out.println("name  : "+ pdb.getName());
//		System.out.println("price  : "+pdb.getPrice());
//		System.out.println("basePrice  : "+pdb.getBasePrice());
//		System.out.println("description  : "+pdb.getDescription());
//		System.out.println("url  : "+pdb.getUrl());
//		System.out.println("picurl  : "+pdb.getPictureUrl());
//		System.out.println("picurl  : "+pdb.getPictureUrl());
//		System.out.println("category  : "+pdb.getCategoryId());
//		System.out.println("keyword  : "+pdb.getKeyword());
//		System.out.println("RealProductId  : "+pdb.getRealProductId());
//		System.out.println("count  : "+count);
//		System.out.println("------------------------------------------------------------");

		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}

	private String getPdDescription(String html) {
		String description = FilterUtil.getStringBetween(html, "<div id=\"short_description_content\" class=\"rte align_justify\">", "<p class=\"buttons_bottom_block\">");
		description = FilterUtil.toPlainTextString(description);
		return description;
	}

	private String getPdBaseprice(String html) {
		double USD_to_THB = 31.257;
		String basePrice ="";
		basePrice = FilterUtil.getStringBetween(html, "<span id=\"old_price_display\">", "</span>");
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		if(StringUtils.isNoneBlank(basePrice)){
			basePrice = Double.toString(Math.round(Double.parseDouble(basePrice)*USD_to_THB));		
		}
		return basePrice;
	}

	private String getPdId(String html) {
		String pdId ="";
		pdId = FilterUtil.getStringBetween(html, "name=\"id_product\" value=\"", "\"");
		return pdId;
	}

	private String getPdUrl(String html) {
		String url ="";
		url = FilterUtil.getStringBetween(html, "<a id=\"resetImages\" href=\"", "\"");
		return url;
	}

	private String getPdPicUrl(String html) {
		String productPictureUrl = "";
		productPictureUrl = FilterUtil.getStringBetween(html, "<div id=\"image-block\">", "</span>");
		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
		return productPictureUrl;
	}

	private String getPdName(String html) {
		String name = FilterUtil.getStringBetween(html, "<h1 class=\"title_detailproduct\"><span>", "</span>");
		name = FilterUtil.toPlainTextString(name);
		return name;
	}

	private String getPdPrice(String html) {
		double USD_to_THB = 31.257;
		String price = "";
		price = FilterUtil.getStringBetween(html, "<span id=\"our_price_display\">", "</span>");
		price = FilterUtil.removeCharNotPrice(price);
		if(StringUtils.isNoneBlank(price)){
			price = Double.toString(Math.round(Double.parseDouble(price)*USD_to_THB));		
		}
		if(price.equals("")){
			price = BotUtil.CONTACT_PRICE_STR;
		}
		return price;
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
