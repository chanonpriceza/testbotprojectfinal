package feed.crawler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BeautriumFeedCrawler extends FeedManager {

	public BeautriumFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return new String[] {"https://api.thebeautrium.com:8020/feed/priceza/product.csv"};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
						
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			URL u = null;
			HttpURLConnection conn = null;
			try {
				u = new URL(feed);
				conn = (HttpURLConnection)u.openConnection();
				String userpass = BaseConfig.CONF_FEED_USERNAME + ":" + BaseConfig.CONF_FEED_PASSWORD;
				String basicAuth = "Basic " + new String(Base64.encodeBase64(userpass.getBytes()));
				conn.setRequestProperty ("Authorization", basicAuth);
				conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36");
				conn.setInstanceFollowRedirects(true);
			} catch (Exception e1) {
				logger.error(e1,e1);
			}
			try (InputStream in = conn.getInputStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
				processLog.addException(e.toString()+"-"+this.getClass().getSimpleName(),e);
			}
		}
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {

		String id= FilterUtil.toPlainTextString(data.get("id"));
		String dummyId = "";
		
		if(StringUtils.isNotBlank(id)) {
			dummyId = " ("+id+")";
		}
		
		String name 	= FilterUtil.toPlainTextString(data.get("title"));
		String desc		= data.get("description");
		String expire	= FilterUtil.toPlainTextString(data.get("availability"));
		String url 		= FilterUtil.toPlainTextString(data.get("link"));
		String price 	= FilterUtil.toPlainTextString(data.get("sale_price"));
		String basePrice= FilterUtil.toPlainTextString(data.get("price"));
		String imageUrl = FilterUtil.toPlainTextString(data.get("image_link"));
		String cat		= FilterUtil.toPlainTextString(data.get("product_type"));
		
		price = FilterUtil.removeCharNotPrice(price);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		
		desc = FilterUtil.toPlainTextString(desc);
		
		double realBasePrice = FilterUtil.convertPriceStr(basePrice);
		double realPrice = FilterUtil.convertPriceStr(price) != 0 ? FilterUtil.convertPriceStr(price) : realBasePrice;
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+dummyId);
		pdb.setPrice(realPrice);
		pdb.setBasePrice(realBasePrice);
		pdb.setDescription(desc);
		pdb.setUrl(url);
		pdb.setPictureUrl(imageUrl);
		pdb.setRealProductId(id);
		
		if(!"in stock".equals(expire))
			pdb.setExpire(true);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}

		return pdb;
	}
	
}
