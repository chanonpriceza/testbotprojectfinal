package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ModernToolsFeedCrawler extends FeedManager {
	
	private static final String FEED_REUQEST_URL = "https://api.srinakornchai.com/product-service/productSearch/hierarchyMenuSearch";
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	
	private static final Logger logger = LogManager.getRootLogger();

	private static boolean haveResult = false;
	
	public ModernToolsFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for(String cat : keySet) {
			String data = "";
			int page = 0;
			try {	
				do {	
					String inputGen = "------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"supplierId\"\r\n" + 
							"\r\n" + 
							"1\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"locale\"\r\n" + 
							"\r\n" + 
							"th_TH\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"menuId\"\r\n" + 
							"\r\n" + 
							"{cat}\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"hId1\"\r\n" + 
							"\r\n" + 
							"\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"hId2\"\r\n" + 
							"\r\n" + 
							"\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"hId3\"\r\n" + 
							"\r\n" + 
							"\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"hId4\"\r\n" + 
							"\r\n" + 
							"\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"hId5\"\r\n" + 
							"\r\n" + 
							"\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"priceLevel\"\r\n" + 
							"\r\n" + 
							"11\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"sorting\"\r\n" + 
							"\r\n" + 
							"SORTING_RATING\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"pageSize\"\r\n" + 
							"\r\n" + 
							"36\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh\r\n" + 
							"Content-Disposition: form-data; name=\"page\"\r\n" + 
							"\r\n" + 
							"{page}\r\n" + 
							"------WebKitFormBoundarylgnjY1J5zDDHYTxh--\r\n" + 
							"hierarchyMenuSearch	";
					
					data = postRequest(FEED_REUQEST_URL,inputGen.replace("{page}",String.valueOf(page)).replace("{cat}",cat), "UTF-8");
	
					if(data==null||data.length()==0) {
						logger.info("Page " + page + " is null");
						break;
					}
					
					JSONObject jsonObj = (JSONObject)new JSONParser().parse("{"+data);
					JSONArray jsArr = (JSONArray) jsonObj.get("content");
					if(jsArr==null||jsArr.size()==0) {
						logger.info("Page " + page + " is size 0");
						break;
					}
					logger.info("Page " + page + " is size " + jsArr.size());
					for(Object x : jsArr) {
						JSONObject extractData = (JSONObject)x;
						parseProductData(extractData,cat);
					}
					page++;
				}while(!Thread.currentThread().isInterrupted() && StringUtils.isNotBlank(data));
			}catch (Exception e) {
				logger.error(e);
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}
	
	private void parseProductData(JSONObject obj,String cat) {
		
		String name = (String)obj.get("productName");
		String price = String.valueOf(obj.get("specialPrice"));
		String basePrice = String.valueOf(obj.get("price"));
		String id = (String)obj.get("productCode");
		String pic = (String)obj.get("thumbnailUrl");
		
		price = StringUtils.isBlank(price)||"null".equals(price)?basePrice:price;
	
		if(StringUtils.isBlank(name)||StringUtils.isBlank(price)||StringUtils.isBlank(id)) {
		
			return;
		}
		
		price = formatPrice.format(Double.parseDouble(FilterUtil.removeCharNotPrice(StringUtils.defaultIfBlank(price, "0"))));
		basePrice = formatPrice.format(Double.parseDouble(FilterUtil.removeCharNotPrice(StringUtils.defaultIfBlank(basePrice, "0"))));
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl("https://www.moderntools.in.th/products/"+id);
		pdb.setRealProductId(id);
		pdb.setPictureUrl(pic);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}

		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private static String postRequest( String url,String data,String charSet) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;	
	 	
	 	try {
			
	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);												
	 	    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=----WebKitFormBoundarylgnjY1J5zDDHYTxh");
	 	    conn.setRequestProperty("Accept", "*/*");
	 	    conn.setRequestProperty("Host","api.srinakornchai.com");
	 	    conn.setRequestProperty("User-Agent","Mozilla/5.0 (compatible; Rigor/1.0.0; http://rigor.com)");
	 	    conn.setChunkedStreamingMode(2000);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(data.getBytes());
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();					
	 		if(conn.getResponseCode()!=200) 
	 			return null;							
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}
}
