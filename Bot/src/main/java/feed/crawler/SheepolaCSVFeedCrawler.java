package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SheepolaCSVFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public SheepolaCSVFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.sheepola.com/rss/priceza/feed.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		try{
			
			String id = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(0)));
			String name = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(1)));
			String price = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(2)));
			String desc = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(3)));
			String category = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(4)))+","+StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(5)));
			String url  = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(8)));
			String image = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(9)));
			String basePrice = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(10)));
			String upc = StringEscapeUtils.escapeCsv(FilterUtil.toPlainTextString(data.get(11)));
	
			
			if( StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
				return null;
			}
			ProductDataBean pdb = new ProductDataBean();

			if(Double.parseDouble(price) > Double.parseDouble(basePrice)){
				String swapPrice = price;
				price = basePrice;
				basePrice = swapPrice; 
			}
			
			pdb.setName(name + "  ("+id+")");	
			pdb.setPrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
			url = url.replace("http://", "https://");
			pdb.setUrl(url);
			pdb.setRealProductId(id);
			
			if(!image.isEmpty()){
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
				pdb.setBasePrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice))));
			}
			
			if(StringUtils.isNotBlank(desc)){
				desc = StringEscapeUtils.unescapeHtml4(desc);
				pdb.setDescription(desc);
			}
			if(StringUtils.isNotBlank(upc)){
				upc = StringEscapeUtils.unescapeHtml4(upc);
				pdb.setUpc(upc);			}
						
			String[] mapping = getCategory(category);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}
			
			return pdb;
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
	
}
