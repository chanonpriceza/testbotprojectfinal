package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateJDCentralFeedCrawler extends FeedManager {
	
	private static String detail 			= "https://p.jd.co.th/prices/mgets?skuIds={ID_LIST}&language=th_TH";
	private static String main 				= "https://jshop-rest.jd.co.th/shop/product/new/search?venderId={SHOPID}&siteFlag=th&pageSize=60&currency=THB&pageNo={PAGE}";
	private static String mainWithCat 		= "https://jshop-rest.jd.co.th/shop/product/new/search?venderId={SHOPID}&siteFlag=th&pageSize=60&currency=THB&pageNo={PAGE}&shopCategoryId={cat}";
	private static JSONParser parser		= new  JSONParser();
	private static String JDCentralId		= "";
	private static int fixCat				= 0 ; 
	private static Map<String,String> catMap = new HashMap<String, String>();
	
	public TemplateJDCentralFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		String[] spilt = BaseConfig.CONF_FEED_CRAWLER_PARAM.split(",");
		if(spilt!=null&&spilt.length==2) {
			JDCentralId = spilt[0];
			fixCat = BotUtil.stringToInt(spilt[1],0);
		}else
			JDCentralId = BaseConfig.CONF_FEED_CRAWLER_PARAM;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		for(String cat:getCategoryKeySet()) {
			for (int i = 0;i<100;i++) {
				try (InputStream in = new URL(mainWithCat.replace("{SHOPID}",JDCentralId).replace("{PAGE}",String.valueOf(i)).replace("{cat}", cat)).openStream()) {
					InputStreamReader reader = new InputStreamReader(in);
					JSONObject obj = (JSONObject)parser.parse(reader);
					obj = (JSONObject) obj.get("productResult");
					JSONArray arr = (JSONArray) obj.get("list");
					if(arr==null||arr.size()==0) {
						break;
					}
					for(Object o:arr) {
						JSONObject cast = (JSONObject) o;
						String id = String.valueOf(cast.get("wareid"));
						catMap.put(id, cat);
					}
				}catch (Exception e) {
					logger.error("Error",e);
				}
			}
		}
		
		for (int i = 0;i<100;i++) {
			try (InputStream in = new URL(main.replace("{SHOPID}",JDCentralId).replace("{PAGE}",String.valueOf(i))).openStream()) {
				InputStreamReader reader = new InputStreamReader(in);
				JSONObject obj = (JSONObject)parser.parse(reader);
				obj = (JSONObject) obj.get("productResult");
				JSONArray arr = (JSONArray) obj.get("list");
				if(arr==null||arr.size()==0) {
					break;
				}
				JSONArray cut = new JSONArray();
				for(Object o:arr) {
					JSONObject cast = (JSONObject) o;
					String id = String.valueOf(cast.get("wareid"));
					cast.remove("subProductList");
					if(catMap.containsKey(id)) {
						String catString  = catMap.get(id);
						String[] mapping = getCategory(catString);
						if(mapping != null) {
							cast.put("categoryId",BotUtil.stringToInt(mapping[0], 0));
							cast.put("keyword",mapping[1]);				
						} else {
							if(fixCat!=0)
								cast.put("categoryId",fixCat);
							else
								cast.put("categoryId",0);
						}
					}
					cut.add(cast);
				}
				InputStream is = new ByteArrayInputStream(cut.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING) ;
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
			
		}
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
	}

	@Override
	public ProductDataBean parse(JSONObject data) {
		String name 	= String.valueOf(data.get("warename"));
		String id 		= String.valueOf(data.get("wareid"));
		String image 	= String.valueOf(data.get("imageurl"));
		int cat			= BotUtil.stringToInt(String.valueOf(data.get("categoryId")),0);	
		String keyword	= String.valueOf(data.get("keyword"));	
		keyword = keyword.equals("null")?null:keyword;
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ("+id+")");
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setUrl("https://item.jd.co.th/"+id+".html");
		pdb.setPictureUrl("https://img10.jd.co.th/n0/"+image+"!q70.jpg");
		pdb = getPrice(pdb);
		if(cat==0&&fixCat!=0)
			pdb.setCategoryId(fixCat);
		else
			pdb.setCategoryId(cat);
		pdb.setKeyword(keyword);
		

		return pdb;
	}
	
	private static ProductDataBean getPrice(ProductDataBean pdb) {
		try {
			URL u =  new URL(detail.replace("{ID_LIST}",String.valueOf(pdb.getRealProductId())));
			InputStreamReader reader = new InputStreamReader(u.openStream());
			JSONArray arr = (JSONArray) parser.parse(reader);
			if(arr.size()>0) {
				JSONObject one = (JSONObject) arr.get(0);
				String price = String.valueOf(one.get("p"));
				String oprice = String.valueOf(one.get("op"));
				
				pdb.setPrice(FilterUtil.convertPriceStr(price));
				pdb.setBasePrice(FilterUtil.convertPriceStr(oprice));
				
			}
		}catch (Exception e) {
			logger.error("Error",e);
		}
		return pdb;
	}
	

}
