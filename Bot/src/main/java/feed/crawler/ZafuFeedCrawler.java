package feed.crawler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class ZafuFeedCrawler extends FeedManager {

	public ZafuFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			FTPUtil.downloadFileWithLogin("http://datatransfer.cj.com/datatransfer/files/4114763/outgoing/productcatalog/237446/Zaful-ZF_CJ_datafeed-shopping.xml.zip",BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".zip","4114763","5Rb68J_m");
			extractFile(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".zip");
		} catch (Exception e) {
			logger.error("Error",e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".xml"};
	}
	
	
	public static void extractFile(String fileName) {

        byte[] buffer = new byte[2048];
        Path outDir = Paths.get(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".xml");
        String zipFileName = fileName;

        try (FileInputStream fis = new FileInputStream(zipFileName);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ZipInputStream stream = new ZipInputStream(bis)){

            @SuppressWarnings("unused")
			ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {


                try (FileOutputStream fos = new FileOutputStream(outDir.toFile());
                        BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

                    int len;
                    while ((len = stream.read(buffer)) > 0) {
                        bos.write(buffer, 0, len);
                    }
                }
                
            }
        }catch (Exception e) {
        	logger.error("Error",e);
		}
	
	}
	
	@Override
	public ProductDataBean parse(String data) {
		String productName 			= FilterUtil.getStringBetween(data, "<title>", "</title>").trim();
		String productDesc 			= FilterUtil.getStringBetween(data, "<description>", "</description>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(data, "<image_link>", "</image_link>").trim();
		String productUrl 			= FilterUtil.getStringBetween(data, "<link>", "</link>").trim();
		String productPrice 		= FilterUtil.getStringBetween(data, "<sale_price>", "</sale_price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(data, "<price>", "</price>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(data, "<id>", "</id>").trim();
		String brand				= FilterUtil.getStringBetween(data, "<brand>", "</brand>").trim();
		String size					= FilterUtil.getStringBetween(data, "<size>", "</size>").trim();
		String color				= FilterUtil.getStringBetween(data, "<color>", "</color>").trim();
		String cat					= FilterUtil.getStringBetween(data, "<product_type>", "</product_type>").trim();
		String expire	= FilterUtil.getStringBetween(data, "<identifier_exists>", "</identifier_exists>").trim();
		
		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice));
		
		if("yes".equals(expire)) {
			logger.info("Expire "+productRealProductId);
			return null;
		}
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		
		
		if(StringUtils.isBlank(size)) {
			size = "";
		}
		String tmp = "";
		if(StringUtils.isNotBlank(productRealProductId)) {
			tmp = "("+productRealProductId+")";
		}
		
		String sizetmp = "";
		if(StringUtils.isNotBlank(size)) {
			sizetmp = " - "+size;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(brand+" "+productName+" "+color+sizetmp+" "+tmp);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productRealProductId);
		pdb.setPrice(p);		
		pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));

		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			return null;
		}
		
		
		return pdb;
	}
	

}
