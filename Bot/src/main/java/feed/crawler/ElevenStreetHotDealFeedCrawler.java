package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ElevenStreetHotDealFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public ElevenStreetHotDealFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Map<String, Integer> linkMap = new HashMap<String, Integer>();
		linkMap.put("http://www.11street.co.th/browsing/shockingDeals/getHotItemProductJson.do", 350);

		for(Entry<String, Integer> rec : linkMap.entrySet()){
			String linkUrl = rec.getKey();
			int maxDepth = rec.getValue();
			for(int i = 1; i <= maxDepth; i++){
				String linkPage = linkUrl;
				linkPage = linkUrl + "?page=" + i;
				try{
					String[] response = HTTPUtil.httpRequestWithStatus(linkPage, "UTF-8", false);
					if(response == null || response.length != 2) break;
					
					String json = response[0];
					if(StringUtils.isNotBlank(json)){
						parseJson(json);
					}
				}catch(Exception e){
					logger.error(e);						
				}

			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	 }	
	
     private void parseJson(String data) throws  ParseException {

    	JSONArray jarr = (JSONArray)new JSONParser().parse(data);	
    	
    	for (Object object : jarr) {
			
    		JSONObject jObj = (JSONObject)object;
			
			String productName = (String)jObj.get("dealProductName");		
			String productPrice = (String)jObj.get("displayFinalDiscountedPrice");
			String productBasePrice = (String)jObj.get("displayOriginalPrice");
			String productUrl = (String)jObj.get("dealProductUrl");
			String productImageUrl = (String)jObj.get("productImg");
			String productId = jObj.get("dealProductNumber").toString();

	
			if (productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0) {
				return;
			}
			ProductDataBean pdb = new ProductDataBean();
			productName = StringEscapeUtils.unescapeHtml4(FilterUtil.toPlainTextString(productName.trim()));
			productUrl = productUrl.replace("&amp;", "&");
			productImageUrl = productImageUrl.replace("&amp;", "&");
	
			pdb.setName(productName + " (Hot Deal)");
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
			
			String linkPattern = "http://www.11street.co.th/connect/GatewayAction/Xsite.do?tid=1400013015&prdNo=";
			String prdNo = FilterUtil.getStringAfterLastIndex(productUrl, "-", "");
			if(StringUtils.isNoneBlank(prdNo)) {
				linkPattern += prdNo;
				pdb.setUrl(linkPattern);
			} else {
				pdb.setUrl(productUrl);
			}
			pdb.setUrlForUpdate(productUrl);
	
			if (StringUtils.isNotBlank(productImageUrl)) {
				pdb.setPictureUrl(productImageUrl);
			}
	
			if (StringUtils.isNotBlank(productId)) {
				pdb.setRealProductId(productId);
			}
			
			String[] response = HTTPUtil.httpRequestWithStatus(productUrl, "UTF-8", false);
			if(response != null && response.length == 2) {
				String html = response[0];
				if (StringUtils.isNotBlank(html)) {
					String productDesc = getPdDescription(html);
					pdb.setDescription(productDesc);
				}
			}
			
			pdb.setCategoryId(160255);
	
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
			
    	}
	}
      
	private String getPdDescription(String html){
		String desc = FilterUtil.getStringBetween(html, "<div class=\"detail_view\" itemprop=\"description\">", "</table>");
		desc = FilterUtil.toPlainTextString(desc);
		if(desc.isEmpty()){
			return "";
		}
		return desc;
	}


	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
