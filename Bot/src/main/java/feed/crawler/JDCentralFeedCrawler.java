package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class JDCentralFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public JDCentralFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://tdc.jd.co.th/getFeedXml.do"
			};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed+" at:"+new Date());
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				editFileReplaceNewLine(fileName);
			}catch (ClosedByInterruptException e) {
				logger.error(e);
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		logger.info("Finish : Load : Feed at:"+new Date());
	}
	
	private void editFileReplaceNewLine(String fileName) {
		String newFileName = BaseConfig.FEED_STORE_PATH + "/editedFile.xml";
		boolean haveResult = false;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(BaseConfig.FEED_STORE_PATH +"/"+fileName),"UTF-8"), 10);
			 PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/editedFile.xml"),  false))){
			
			String text = "";
			while((text = br.readLine()) != null) {
				text = text.replaceAll("\r\n", "");
				pwr.print(text.toString().replaceAll("</item>","</item>\n"));
				haveResult = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {newFileName};
	}
	
	public ProductDataBean parse(String xml){
		
//	    <item>
//	        <g:id>117772</g:id>
//	        <g:title>ลอรีอัล ปารีส คัลเลอร์ ริช  เฉด 235 พลัม แมนเนควิน 3.7 กรัม</g:title>
//	        <g:description>ลอรีอัล ปารีส คัลเลอร์ ริช  เฉด 235 พลัม แมนเนควิน 3.7 กรัม สีม่วง</g:description>
//	        <g:link>https://item.jd.co.th/117772.html</g:link>
//	        <g:image_link>https://img.jd.co.th/n0/s340x340_jfs/t22/258/788238/63968/6445634b/5a8bc13cNf88d91a7.jpg</g:image_link>
//	        <g:availability>in stock</g:availability>
//	        <g:price>398 THB</g:price>
//	        <g:sale_price>175 THB</g:sale_price>
//	        <g:unit_pricing_measure>0.1 kg</g:unit_pricing_measure>
//	        <g:google_product_category>469</g:google_product_category>
//	        <g:product_type>สุขภาพและความงาม &gt; เครื่องสำอาง &gt; ริมฝีปาก</g:product_type>
//	        <g:brand>L'Oréal Paris(ลอรีอัล ปารีส)</g:brand>
//	        <g:condition>new</g:condition>
//	        <g:adult>no</g:adult>
//	        <g:color></g:color>
//	        <g:gender>unisex</g:gender>
//	        <g:size></g:size>
//	    </item>
		
		
//		<item>
//		<g:extra_deeplink_url_ios>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/3417908</g:extra_deeplink_url_ios>
//		<g:gtin></g:gtin>
//		<g:image_link>https://img.jd.co.th/n0/s340x340_jfs/t16/139/715792151/170165/c72083bb/5d19adfdNf9f3a799.jpeg</g:image_link>
//		<g:color></g:color>
//		<g:gender>unisex</g:gender>
//		<g:link>https://www.jd.co.th/product/_3417908.html</g:link>
//		<g:description>For Toyota Camry 2018 2019 Multi-Functions Car Tail Light Led Rear Fog Lamp Bumper Light Auto Bulb Brake Light Reflector I213951</g:description>
//		<g:extra_deeplink_url_andriod>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/3417908</g:extra_deeplink_url_andriod>
//		<g:availability>in stock</g:availability>
//		<g:title>For Toyota Camry 2018 2019 Multi-Functions Car Tail Light Led Rear Fog Lamp Bumper Light Auto Bulb Brake Light Reflector</g:title>
//		<g:sale_price>504 THB</g:sale_price>
//		<g:condition>new</g:condition>
//		<g:product_type>ยานยนต์ &amp;gt; อะไหล่ยนต์และอุปกรณ์ตกแต่ง &amp;gt; ไฟรถยนต์และอุปกรณ์ตกแต่ง</g:product_type>
//		<g:size></g:size>
//		<g:price>630 THB</g:price>
//		<g:vendor_id>14952</g:vendor_id>
//		<g:code_of_vendor></g:code_of_vendor>
//		<g:mobile_link>https://m.jd.co.th/product/3417908.html</g:mobile_link>
//		<g:id>3417908</g:id>
//		<g:unit_pricing_measure>0.183 kg</g:unit_pricing_measure>
//		<g:adult>no</g:adult>
//		<g:brand>UpperX</g:brand>
//		<g:google_product_category>888</g:google_product_category>
//		</item>
//		<item>

	    
		String productName 		= FilterUtil.getStringBetween(xml, "<g:title>", "</g:title>").trim();		
		String productBrand		= FilterUtil.getStringBetween(xml, "<g:brand>", "</g:brand>").trim();		
		String productColor		= FilterUtil.getStringBetween(xml, "<g:color>", "</g:color>").trim();		
		String productPrice 	= FilterUtil.getStringBetween(xml, "<g:price>", "</g:price>").trim();
		String productSalePrice	= FilterUtil.getStringBetween(xml, "<g:sale_price>", "</g:sale_price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<g:link>", "</g:link>").trim();
		String productImageUrl 	= FilterUtil.getStringBetween(xml, "<g:image_link>", "</g:image_link>");
		String productCatName 	= FilterUtil.getStringBetween(xml, "<g:product_type>", "</g:product_type>");
		String productCatId 	= FilterUtil.getStringBetween(xml, "<g:google_product_category>", "</g:google_product_category>");
		String productDesc 		= FilterUtil.getStringBetween(xml, "<g:description>", "</g:description>").trim();
		String productId 		= FilterUtil.getStringBetween(xml, "<g:id>", "</g:id>").trim();	
		String status	 		= FilterUtil.getStringBetween(xml, "<g:availability>", "</g:availability>").trim();	
		
		if(!status.equals("in stock")){
			return null;
		}
		
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;		
		}
				
		if(StringUtils.isBlank(productSalePrice)) {
			productSalePrice = productPrice;
			productPrice = "";
		}
		
		productName += " -";
		
		if(StringUtils.isNotBlank(productBrand)) {
			productName += " " + productBrand;
		}
		
		if(StringUtils.isNotBlank(productColor)) {
			productName += " " + productColor;
		}
		
		if(StringUtils.isNotBlank(productId)) {
			productName += " (" + productId + ")";
		}
		
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		
//		if(productName.contains("[Global]") || productName.contains("【Global】") || (productName.startsWith("Global")) || (productName.contains("test product"))) {
//			return null;	
//		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productSalePrice)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (productDesc.length() != 0) {					
			pdb.setDescription(productDesc);
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		pdb.setUrlForUpdate(productUrl);
		
		productUrl = productUrl + "||https://jdcentral.onelink.me/SJGK?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_dp=openjdthai%3A%2F%2Fdeeplink%2Fcategory%2Fjump%2FM_sourceFrom%2Fsx_thai%2Fdes%2FproductDetail%2FskuId%2F******&af_reengagement_window=30d&clickid=(xxxxx)&af_ios_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html&af_android_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html";
		productUrl = productUrl.replace("******", productId);
		
		pdb.setUrl(productUrl);
		
		String[] catMap = getCategory(StringEscapeUtils.unescapeHtml4(productCatName).replace("&gt;",">"));
		String[] catMap2 = getCategory(StringEscapeUtils.unescapeHtml4(productCatId).replace("&gt;",">"));
		String[] catMap3 = getCategory(StringEscapeUtils.unescapeHtml4(productBrand).replace("&gt;",">"));
		
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}
		
		else if(catMap2 != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap2[0], 0));
			pdb.setKeyword(catMap2[1]);			
		}
		
		else if(catMap3 != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap3[0], 0));
			pdb.setKeyword(catMap3[1]);			
		}
		
		return pdb;
	}
}
