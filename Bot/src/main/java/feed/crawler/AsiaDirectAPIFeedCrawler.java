package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AsiaDirectAPIFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	private static final int maxConcurrent = 1;
	private ExecutorService executor = null;
	
	private boolean haveResult = false;
	private List<asiadirectThread> targetList = null;
	private String tmp = "";
	private int count = 0;
	
	public AsiaDirectAPIFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}

	@Override
	public void loadFeed() {
		try {
			targetList = new ArrayList<>();
			
			String linkSpec = "https://www.asiadirect.co.th/adbapi/api/serviceapi/getModelcar.php?g=priceza";
			executor = Executors.newFixedThreadPool(maxConcurrent);
			
			logger.info("linkSpec : " + linkSpec);
			
			specRequest(linkSpec, "UTF-8", false, 1000);
			
			executor.invokeAll(targetList);
			executor.shutdown();
			if(executor.awaitTermination(120, TimeUnit.HOURS)){
				logger.info("Done All.");
			}else{
				logger.info("Time out.");
				executor.shutdownNow();
			}

			if(haveResult)
				BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
			
		} catch (InterruptedException e) {
			logger.error(e);
		}
	}
	
	private void specRequest(String url, String charset, boolean redirectEnable, int charBufferSize) {
		URL u = null;
	  	HttpURLConnection conn = null;
		try {
			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.setConnectTimeout(600000);
			conn.setReadTimeout(600000);
			
			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = new InputStreamReader(is, charset);) {
    				char[] buffer = new char[charBufferSize];
    				while(!Thread.currentThread().isInterrupted()){
    					int rsz = isr.read(buffer, 0, buffer.length);
    					if (rsz <= 0) {
    						break;
    					}
    					processLine(new String(buffer, 0, rsz));
    			    }
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if(conn != null) { conn.disconnect(); }
		}
	}
	
	private void processLine(String line) {
		tmp += line;
		List<String> objList = FilterUtil.getAllStringBetween(tmp, "{", "}");
		if(objList != null) {
			for (String spec : objList) {
				asiadirectThread t = new asiadirectThread(spec);
				targetList.add(t);
				count++;
				if(targetList.size() % 100 == 0){
					try {
						logger.info("Asiadirect fetch feed : " + count);
						executor.invokeAll(targetList);
						targetList.clear();
					} catch(InterruptedException e) {
						logger.error(e);
					}
				}
			}
			tmp = FilterUtil.getStringAfterLastIndex(tmp, "}", tmp);
		}
		
	}
	
	private class asiadirectThread implements Callable<Void> {

		private String dataRow;

		asiadirectThread(String data) {
			dataRow = "{" + data + "}";
		}

		public Void call() {
			try{
				
				JSONObject jObj = (JSONObject) new JSONParser().parse(dataRow);
				String year  = (String) String.valueOf(jObj.get("year"));  
				String brand = (String) String.valueOf(jObj.get("make"));
				String model = (String) String.valueOf(jObj.get("model"));  
				String type  = (String) String.valueOf(jObj.get("class"));  
				String cc	 = (String) String.valueOf(jObj.get("cc"));  
				
//				year = URLEncoder.encode(year, "UTF-8");
				brand = BotUtil.encodeURL(brand);
				model = BotUtil.encodeURL(model);
				type = URLEncoder.encode(type, "UTF-8");
				cc = cc.replace("cc", "");
//				cc = URLEncoder.encode(cc, "UTF-8");
				
				String requestUrl = "https://www.asiadirect.co.th/adbapi/api/serviceapi/priceza.php?year={YEAR}&make={BRAND}&model={MODEL}&class={TYPE}&cc={CC}";
				
				requestUrl = requestUrl.replace("{BRAND}", brand);
				requestUrl = requestUrl.replace("{MODEL}", model);
				requestUrl = requestUrl.replace("{YEAR}", year);
				requestUrl = requestUrl.replace("{TYPE}", type);
				requestUrl = requestUrl.replace("{CC}", cc);
				
				String[] response = HTTPUtil.httpRequestWithStatus(requestUrl, "UTF-8", false);
				if(response == null || response.length != 2) return null;
				
				String result = response[0];
				if(StringUtils.isNotBlank(result)){
					if(result.indexOf("<product>") > -1){
						parseXML(result, String.valueOf(jObj.get("make")));
					}
				}else{
					logger.info("no response : " + requestUrl);
				}

			}catch(Exception e){
				logger.error(e);
			}
			return null;
		}
		
		public void parseXML(String data, String pricezaModel) throws Exception {
			
//			<products>
//				<product>
//					<broker>Asiadirect</broker>
//					<insurer>วิริยะประกันภัย</insurer>
//					<title>Single Rate เก๋ง Group ซ่อมอู่  5</title>
//					<type>1</type>
//					<premium>15700</premium>
//					<brand>Honda</brand>
//					<model>Jazz</model>
//					<cc>1500cc</cc>
//					<year>2016</year>
//					<url>https://www.asiadirect.co.th/adbserviceapi.php?page=Priceza&class=1&make=HONDA&model=JAZZ&year=2016&cc=1500cc&insurercode=VBI&planid=341&premium=15700&discount=1300&taxamount=17000.00&costid=5137</url>
//					<compulsory_insurance>ไม่มี</compulsory_insurance>
//					<sum_insured>450000</sum_insured>
//					<fire_damage>1</fire_damage>
//					<deductible>0</deductible>
//					<repair>อู่</repair>
//					<flood_damage>450000</flood_damage>
//					<bodily_injury_liability>1000000</bodily_injury_liability>
//					<bodily_injury_liability_Accident>10000000</bodily_injury_liability_Accident>
//					<property_damage_liability>5000000</property_damage_liability>
//					<fire_damage>0</fire_damage>
//					<theft>450000</theft>
//					<prtsonal_injury_protection>200000</prtsonal_injury_protection>
//					<medical_payments>200000</medical_payments>
//					<bail_bond>200000</bail_bond>
//					<bodily_injury_liability>1000000</bodily_injury_liability>
//					<bodily_injury_liability_Accident>10000000</bodily_injury_liability_Accident>
//					<property_damage_liability>5000000</property_damage_liability>
//					<fire_damage>0</fire_damage>
//					<theft>450000</theft>
//					<prtsonal_injury_protection>200000</prtsonal_injury_protection>
//					<medical_payments>200000</medical_payments>
//					<bail_bond>200000</bail_bond>
//					<flood_damage>450000</flood_damage>
//				</product>
//				<product>...</product>
//				<product>...</product>
//				<product>...</product>
//			</product>
			
			List<String> productList = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
			if(productList != null && productList.size() > 0){
				for (String product : productList) {
					
					product = StringEscapeUtils.unescapeHtml4(product);
					
					String type 		= FilterUtil.getStringBetween(product, "<type>", "</type>");                                                         // -ชั้นประกัน
					String insurer 		= FilterUtil.getStringBetween(product, "<insurer>", "</insurer>");                                                   // -บริษัทประกัน
					String brand 		= FilterUtil.getStringBetween(product, "<brand>", "</brand>").replace("-", " ");                                     // -ยี่ห้อ
					String model 		= FilterUtil.getStringBetween(product, "<model>", "</model>").replace("-", " ");                                     // -รุ่น
					String year 		= FilterUtil.getStringBetween(product, "<year>", "</year>");                                                         // -ปี
					String title 		= FilterUtil.getStringBetween(product, "<title>", "</title>").replace("-", " ");                                     // -แผน
					                                                                                                                                            
					String subModel		= pricezaModel.replace("-", " ");                                                                                    // -รุ่นย่อย
					String cc			= FilterUtil.getStringBetween(product, "<cc>", "</cc>");                                                     // -ซ่อม
					String repair		= FilterUtil.getStringBetween(product, "<repair>", "</repair>");                                                     // -ซ่อม
					String od			= FilterUtil.getStringBetween(product, "<sum_insured>", "</sum_insured>");                                           // -ทุนประกัน
					String oddd			= FilterUtil.getStringBetween(product, "<deductible>", "</deductible>");                                             // -ค่าเสียหายส่วนแรก
					String carAct		= FilterUtil.getStringBetween(product, "<compulsory_insurance>", "</compulsory_insurance>");                         // -พรบ.
					String fire			= FilterUtil.getStringBetween(product, "<fire_damage>", "</fire_damage>");                                           // -คุ้มครองไฟไหม้            
					String theft		= FilterUtil.getStringBetween(product, "<theft>", "</theft>");                                                       // -คุ้มครองโจรกรรม           
					String flood		= FilterUtil.getStringBetween(product, "<flood_damage>", "</flood_damage>");                                         // -คุ้มครองน้ำท่วม           
					String bi			= FilterUtil.getStringBetween(product, "<bodily_injury_liability>", "</bodily_injury_liability>");                   // -ชีวิตบุคคลภายนอกต่อคน     
					String bt			= FilterUtil.getStringBetween(product, "<bodily_injury_liability_Accident>", "</bodily_injury_liability_Accident>"); // -ชีวิตบุคคลภายนอกต่อครั้ง  
					String tp			= FilterUtil.getStringBetween(product, "<property_damage_liability>", "</property_damage_liability>");               // -ทรัพย์สินบุคคลภายนอก      
					String pa			= FilterUtil.getStringBetween(product, "<prtsonal_injury_protection>", "</prtsonal_injury_protection>");             // -อุบัติเหตุส่วนบุคคล       
					String me			= FilterUtil.getStringBetween(product, "<medical_payments>", "</medical_payments>");                                 // -ค่ารักษาพยาบาล            
					String bb			= FilterUtil.getStringBetween(product, "<bail_bond>", "</bail_bond>");                                               // -ประกันตัวผู้ขับขี่        
					
					String productUrl 	= FilterUtil.getStringBetween(product, "<url>", "</url>");
					String productPrice = FilterUtil.removeCharNotPrice(FilterUtil.getStringBetween(product, "<premium>", "</premium>"));
					
					if(StringUtils.isBlank(productPrice) || StringUtils.isBlank(productUrl)){
						return;
					}
					
					if(StringUtils.isNotBlank(cc)) {
						cc = cc.replace("cc", "");
						String txtCC = new DecimalFormat("##.0").format(Double.parseDouble(cc) / 1000) + "cc";
						subModel = subModel + " " + txtCC;
					}
					
					String uniqueText = "";
					uniqueText += type;
					uniqueText += insurer;
					uniqueText += brand;
					uniqueText += model;
					uniqueText += subModel;
					uniqueText += year;
					uniqueText += title;
					uniqueText += repair;
					uniqueText += carAct;
					uniqueText += od;
					uniqueText = toMD5(uniqueText);
					
					productPrice = formatPrice.format(Double.parseDouble(FilterUtil.removeCharNotPrice(productPrice)));
					
//					ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
					String productName = "";
					productName += "ประกันชั้น " + type;
					productName += " - " + insurer;
					productName += " - " + brand;
					productName += " - " + model;
					productName += " - " + ((StringUtils.isNotBlank(subModel))? subModel : "ไม่ระบุรุ่นย่อย");
					productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
					productName += " (" + uniqueText.substring(0,10) + ")";
					productName = productName.replace(",", "");
					productName = productName.replaceAll("\\s+", " ");                                                  
					productName = productName.replace("( ", "(");                                                       
					productName = productName.replace(" )", ")");                                                       
					productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
					productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")"); 
					
//					( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
					String productDesc = "";
					productDesc += ((StringUtils.isNotBlank(title))? title : "ไม่ระบุแผน" ); 
					productDesc += " - " + ((StringUtils.isNotBlank(repair))? "ซ่อม"+repair : "ไม่ระบุอู่ซ่อม");
					productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
					productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
					productDesc += " - " + ((StringUtils.isBlank(carAct) || carAct.equals("ไม่มี")) ? "ไม่รวมพรบ." : "รวมพรบ.");
					productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
					productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
					productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood : "ไม่ระบุ" ); 
					productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
					productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
					productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
					productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
					productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );         
					productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
					productDesc = productDesc.replaceAll("\\s+", " ");                                               
					productDesc = productDesc.replace("( ", "(");                                                    
					productDesc = productDesc.replace(" )", ")");                                                    
					productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
					productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
					productDesc = productDesc.replace(",", "");
					
					String productImage = "";
					if(insurer.equalsIgnoreCase("สินทรัพย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5567.jpg"; 
					if(insurer.equalsIgnoreCase("เอเชียประกันภัย 1950"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5570.jpg"; 
					if(insurer.equalsIgnoreCase("อลิอันซ์ ซี.พี. ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
					if(insurer.equalsIgnoreCase("อลิอันซ์ ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
					if(insurer.equalsIgnoreCase("กรุงเทพประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5558.jpg"; 
					if(insurer.equalsIgnoreCase("ทิพยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5565.jpg"; 
					if(insurer.equalsIgnoreCase("กรุงไทยพานิชประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5571.jpg"; 
					if(insurer.equalsIgnoreCase("เคเอสเคประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5559.jpg"; 
					if(insurer.equalsIgnoreCase("แอลเอ็มจีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5572.jpg"; 
					if(insurer.equalsIgnoreCase("เมืองไทยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5551.jpg"; 
					if(insurer.equalsIgnoreCase("นวกิจประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5569.jpg"; 
					if(insurer.equalsIgnoreCase("อาคเนย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5552.jpg"; 
					if(insurer.equalsIgnoreCase("สินมั่นคงประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5562.jpg"; 
					if(insurer.equalsIgnoreCase("ประกันคุ้มภัย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5554.jpg"; 
					if(insurer.equalsIgnoreCase("ไทยประกันภัย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5575.jpg"; 
					if(insurer.equalsIgnoreCase("ไทยไพบูลย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9559.jpg"; 
					if(insurer.equalsIgnoreCase("ประกันภัยไทยวิวัฒน์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5574.jpg"; 
					if(insurer.equalsIgnoreCase("วิริยะประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg"; 
					if(insurer.equalsIgnoreCase("อลิอันซ์ โกลบอล แอสซิสแทนซ์"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9605.jpg"; 
					if(insurer.equalsIgnoreCase("ไอโออิ กรุงเทพ ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5568.jpg"; 
					if(insurer.equalsIgnoreCase("แอกซ่าประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5557.jpg"; 
					if(insurer.equalsIgnoreCase("บูพา"))						productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9606.jpg"; 
					if(insurer.equalsIgnoreCase("จรัญประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9558.jpg"; 
					if(insurer.equalsIgnoreCase("ซิกน่าประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9604.jpg"; 
					if(insurer.equalsIgnoreCase("เจ้าพระยาประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5560.jpg"; 
					if(insurer.equalsIgnoreCase("เทเวศประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5563.jpg"; 
					if(insurer.equalsIgnoreCase("เอราวัณประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9569.jpg"; 
					if(insurer.equalsIgnoreCase("อินทรประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9572.jpg"; 
					if(insurer.equalsIgnoreCase("กมลประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9576.jpg"; 
					if(insurer.equalsIgnoreCase("เอ็มเอสไอจีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5564.jpg"; 
					if(insurer.equalsIgnoreCase("นำสินประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5573.jpg"; 
					if(insurer.equalsIgnoreCase("เอไอจี ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9435.jpg"; 
					if(insurer.equalsIgnoreCase("ฟีนิกซ์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9573.jpg"; 
					if(insurer.equalsIgnoreCase("รู้ใจ รับประกันโดย KPI กรุงไทยพานิช"))	productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9419.jpg"; 
					if(insurer.equalsIgnoreCase("ศรีอยุธยา เจนเนอรัล ประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9565.jpg"; 
					if(insurer.equalsIgnoreCase("สามัคคีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5553.jpg"; 
					if(insurer.equalsIgnoreCase("ธนชาตประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5556.jpg"; 
					if(insurer.equalsIgnoreCase("โตเกียวมารีนศรีเมืองประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9601.jpg"; 
					if(insurer.equalsIgnoreCase("ไทยเศรษฐกิจประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5566.jpg"; 
					if(insurer.equalsIgnoreCase("ไทยพัฒนาประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9556.jpg"; 
					if(insurer.equalsIgnoreCase("ไทยศรีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9561.jpg"; 
					if(insurer.equalsIgnoreCase("มิตรแท้ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9566.jpg"; 
					if(insurer.equalsIgnoreCase("พุทธธรรมประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9564.jpg"; 
					if(insurer.equalsIgnoreCase("บางกอกสหประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9568.jpg"; 
					if(insurer.equalsIgnoreCase("สหนิรภัยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9580.jpg"; 
					if(insurer.equalsIgnoreCase("สหมงคลประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9567.jpg"; 
					
					// https://priceza.atlassian.net/browse/WEB-6692
					if(insurer.equalsIgnoreCase("เจ้าพระยาประกันภัย")) {
						return;
					}
						
					
					ProductDataBean pdb = new ProductDataBean();
					pdb.setName(productName);
					pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
					pdb.setPictureUrl(productImage);
					pdb.setDescription(productDesc);
					pdb.setUrl(productUrl);
					pdb.setUrlForUpdate(uniqueText);
					pdb.setCategoryId(250101);
					pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;

					String result = mockResult(pdb);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
							pwr.println(result);
							haveResult = true;
						}catch(Exception e) {
							logger.error(e);
						}
					}
					
				}
			}
				
		}
		
		private String toMD5(String text){
			MessageDigest m = null;
			try {
				m = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				logger.error(e);
				return "";
			}
			m.reset();
			m.update(text.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			return hashtext;
		}

		private String mockResult(ProductDataBean pdb) {
			StringBuilder result = new StringBuilder();
			
			result.append("<product>");
			if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
			if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
			if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
			if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
			if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
			if(pdb.getPrice() != 0) 							result.append("<price>"+String.format("%.0f", pdb.getPrice())+"</price>");
			if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+String.format("%.0f", pdb.getBasePrice())+"</basePrice>");
			if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
			if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
			if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
			if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
			result.append("</product>");
			
			return result.toString();
		}

	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
