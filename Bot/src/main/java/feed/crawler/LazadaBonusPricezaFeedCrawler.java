package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LazadaBonusPricezaFeedCrawler extends FeedManager{

	public LazadaBonusPricezaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
			"http://52.74.190.12/th/new_priceza_bonus_priceza_th_marketing_feed.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		
		// 0      sku_id
		// 1      lazada_sku
		// 2      product_name
		// 3      product_id
		// 4      description
		// 5      is_purchasable
		// 6      price
		// 7      current_price
		// 8      discount_percentage
		// 9      currency
		// 10     brand_name
		// 11     venture_category1_name_en
		// 12     rating_avg_value
		// 13     number_of_reviews
		// 14     is_free_shipping
		// 15     product_big_img
		// 16     product_medium_img
		// 17     product_small_img
		// 18     image_url_2
		// 19     image_url_3
		// 20     image_url_4
		// 21     image_url_5
		// 22     product_url
		// 23     seller_name
		// 24     seller_rating
		// 25     business_area
		// 26     business_type
		// 27     is_flash_sale
		// 28     is_hot_deals
		// 29     total_orders_last_2weeks
		// 30     total_orders_last_4weeks
		// 31     pv_last_90_days
		// 32     availability
		// 33     has_bonus_commission
		// 34     bonus_commission_rate
		// 35     bonus_tracking_link
		
		String product_name             = (String) String.valueOf((Object) data.get(2));                      
		String description              = (String) String.valueOf((Object) data.get(5));                      
		String simple_sku               = (String) String.valueOf((Object) data.get(0));                    
		String deeplink                 = (String) String.valueOf((Object) data.get(35));                  
		String brand                    = (String) String.valueOf((Object) data.get(10));               
		String sale_price               = (String) String.valueOf((Object) data.get(6));                    
		String discounted_price         = (String) String.valueOf((Object) data.get(7));                          
		String picture_url              = (String) String.valueOf((Object) data.get(16));                     
		
		if( StringUtils.isBlank(product_name) || StringUtils.isBlank(sale_price) ){
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		String name = product_name + " (BP" + simple_sku + ")";
		pdb.setName(BotUtil.limitString(name, 200));
		pdb.setUrlForUpdate(simple_sku);
		pdb.setUrl(deeplink);
		pdb.setRealProductId(simple_sku);
		
		if (!discounted_price.isEmpty()) {
			double curPrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(discounted_price));
			if(curPrice == 0){
				return null;
			}else{
				pdb.setPrice(curPrice);
			}
		}
		
		if (!sale_price.isEmpty()) {
			double curBasePrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(sale_price));
			if(curBasePrice != 0 && curBasePrice > pdb.getPrice()){
				pdb.setBasePrice(curBasePrice);
			}
		}
		
		if (!picture_url.isEmpty() && picture_url.startsWith("http") && BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			pdb.setPictureUrl(picture_url);
		}

		description = FilterUtil.toPlainTextString(description);
		if (StringUtils.isNotBlank(brand)) {
			description = description + " แบรนด์  " + brand;
		}

		pdb.setDescription(description);
		
		return pdb;
	}
	
}
