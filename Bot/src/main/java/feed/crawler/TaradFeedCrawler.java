package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import product.processor.ProductFilter;
import utils.BotUtil;
import utils.FilterUtil;

public class TaradFeedCrawler extends FeedManager {
	
	private static int countBrand = 0;
	private static int totalBrand = 0;
	private static boolean killed ;
	
	public TaradFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			totalBrand = getCategoryKeySet().size();
			int FixThreadPool = BotUtil.stringToInt(BaseConfig.CONF_FEED_CRAWLER_PARAM, 1);
			logger.info("Thread Num: "+FixThreadPool);
			ExecutorService ex = Executors.newFixedThreadPool(FixThreadPool);
			for(String cat:getCategoryKeySet()) {
				ex.execute(new TaradFeedCrawlerRunner(cat));
			}
			ex.shutdown();
			if(ex.awaitTermination(120, TimeUnit.HOURS)){
				logger.info("Done All.");
			}else{
				logger.info("Time out.");
				ex.shutdownNow();
			}
			
		}catch (InterruptedException e) {
			killed = true;
			logger.info("Killed");
		}
		catch (Exception e) {
			logger.info(e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		logger.info("Load is Complete !!");
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private class TaradFeedCrawlerRunner implements Runnable{
		private static final String urlTemplate = "https://www.taradplaza.com/category/ajax_list_product?next_page={page}&mall_category_id={cat}";
		private String cat;
		private JSONParser jsonParser = new JSONParser();
		
		public TaradFeedCrawlerRunner(String inputCat) {
			this.cat = inputCat;
		}
		
		@Override
		public void run() {
			countBrand++;
			logger.info("Current Cat : "+cat+" Count cat:" + countBrand + " totalBrand: " + totalBrand);
			
			boolean isHaveResult = false;
			int page = 1;
			do {
				isHaveResult = false;
				InputStreamReader isr;
				String url = urlTemplate.replace("{page}",String.valueOf(page)).replace("{cat}",cat);
				URLConnection conn = null;
				try {
					URL u = new URL(url);
					conn = u.openConnection();
					conn.connect();
					conn.setReadTimeout(10000);
				}catch (Exception e) {
					logger.error(e,e);
				}
				try (InputStream in = conn.getInputStream()) {
					isr = new InputStreamReader(in,"UTF-8");
					JSONObject obj = (JSONObject) jsonParser.parse(isr);
					isHaveResult = (String.valueOf(obj.get("status"))).equals("success");
					
					if(!isHaveResult) {
						logger.info("request url -----> "+url + ", isHaveResult = " + isHaveResult);
						return;	
					}
					
					JSONArray arr = (JSONArray) obj.get("list_product");
					logger.info("request url -----> "+url + ", list_product.size() = " + arr.size());
					for(Object i:arr) {
						if(i instanceof JSONObject) {
							JSONObject objectCast = (JSONObject) i;
							parseJSON(objectCast);
						}
					}
					
					
				}catch (Exception e) {
					logger.error(e,e);
				}
				page++;
			}while(isHaveResult && !killed);
			
		}
		
		private void parseJSON(JSONObject item) {
			
			String suffix = "";
			String shop_name= "";
			String name 		= String.valueOf(item.get("spd_title_th"));
			if(name.contains("LRT224 "))
				System.out.println("5555");
			String price 		= String.valueOf(item.get("net_price"));
			String url			= "https://www.taradplaza.com/product/";
			String basePrice	= String.valueOf(item.get("total_price"));
			String id			= String.valueOf(item.get("spd_id_pk"));
			String picUrl		= "";
			JSONArray image 	= (JSONArray)item.get("images");
			JSONObject shop_info = (JSONObject) item.get("shop_info");
			
			if(shop_info!=null)
				shop_name = String.valueOf(shop_info.get("shop_url"));
			
			JSONObject obj 		= image.size()> 0 ?(JSONObject)image.get(0):null;

			if(obj!=null) {
				picUrl = "https://img.trd.cm/tools/r/409/480/{shopinfo}/img-lib/"+String.valueOf(obj.get("spp_picture_big"));
				picUrl = picUrl.replace("{shopinfo}",shop_name);
			}else{
				return;
			}
			
			if("0".equals(price)&&"0".equals(basePrice))
				price = BotUtil.CONTACT_PRICE_STR;
			
			if(StringUtils.isNotBlank(id)) {
				suffix = " ("+id+")";
				url = url+id;
			}else {
				logger.info("Data not complete name :"+name);
				return;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			if(ProductFilter.checkCOVIDInstantDelete(name))
	        	return;
			pdb.setName(name+suffix);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
			pdb.setPictureUrl(picUrl);
			pdb.setRealProductId(id);
			pdb.setUrl(url);
			
			String[] mapping = getCategory(cat);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}
			
			if(!mockResult(pdb))
				logger.info("Error Mock ---> "+id);
			
		}
		
		private boolean mockResult(ProductDataBean pdb){
			
			String id = pdb.getRealProductId();
			String name = pdb.getName();
			String url = pdb.getUrl();
			String image = pdb.getPictureUrl();
			double price = pdb.getPrice();
			String desc = pdb.getDescription();
			int cat = pdb.getCategoryId();
			double basePrice = pdb.getBasePrice();
			boolean expire = pdb.isExpire();
			String keyword = pdb.getKeyword();
			String urlForUpdate = pdb.getUrlForUpdate();
			
			desc = FilterUtil.toPlainTextString(desc);

			if(StringUtils.isBlank(url)) return false;
			if(StringUtils.isBlank(name)) return false;
			if(price==0) return false;
			if(expire) return false;
			
			StringBuilder result = new StringBuilder();
			result.append("<product>");
			if(StringUtils.isNotBlank(name)) 			result.append("<name>"+name+"</name>");
			if(StringUtils.isNotBlank(desc)) 			result.append("<desc>"+desc+"</desc>");
			if(price!=0) 								result.append("<price>"+price+"</price>");
			if(basePrice!=0) 							result.append("<basePrice>"+basePrice+"</basePrice>");
			if(StringUtils.isNotBlank(image)) 			result.append("<pictureUrl>"+image+"</pictureUrl>");
			if(StringUtils.isNotBlank(url)) 			result.append("<url>"+url+"</url>");
			if(StringUtils.isNotBlank(urlForUpdate))	result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
			if(StringUtils.isNotBlank(id)) 				result.append("<realProductId>"+id+"</realProductId>");
			if(cat!=0) 									result.append("<categoryId>"+cat+"</categoryId>");
			if(StringUtils.isNotBlank(keyword))	 		result.append("<keyword>"+keyword+"</keyword>");
			if(cat!=0) 									result.append("<cat>"+cat+"</cat>");
			result.append("</product>");
			
			if(StringUtils.isNotBlank(result)) {
				if(StringUtils.isNotBlank(result)) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
						pwr.println(result);
					}catch(Exception e) {
						logger.error("Error",e);
						return false;
					}
				}
			}
			return true;
			
		}
		
	}

}
