package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class HeaddaddyFeedCrawler extends FeedManager{

	public HeaddaddyFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.headdaddy.com/index.php/home/process/feed_priceza"
		};
	}
	
	public ProductDataBean parse(String data){
	
		String productName = FilterUtil.getStringBetween(data, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(data, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(data, "<url>", "</url>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(data, "<image_url>", "</image_url>").trim();
		String realProductId = FilterUtil.getStringBetween(data, "<id>", "</id>").trim();
		String productCatId = FilterUtil.getStringBetween(data, "<category_id>", "</category_id>").trim();
		String productDescription = FilterUtil.getStringBetween(data, "<description>", "</description>").trim();
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 ) {
			return null;		
		}
		productName = StringEscapeUtils.unescapeHtml4(productName);
		if(realProductId.length() > 0){
			productName += "("+realProductId+")";
		}		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (productUrl.length() != 0) {
			pdb.setUrl(productUrl);			
			pdb.setUrlForUpdate(productUrl);
		}
		
		if (productPictureUrl.length() != 0) {
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(realProductId.length() != 0){
			pdb.setRealProductId(realProductId);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			return null;
		}
		
		if(!productDescription.isEmpty()){
			pdb.setDescription(FilterUtil.toPlainTextString(productDescription));
		}
		
		return pdb;
	}

}
