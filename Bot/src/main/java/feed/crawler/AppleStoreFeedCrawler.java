package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AppleStoreFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public AppleStoreFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://feeds.performancehorizon.com/priceza/301136/AppleTH.PHGXML.xml"
		};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			String[] result = HTTPUtil.httpRequestWithStatus(feed, "UTF-8", true);
			if(result != null && result.length == 2) {
				if(StringUtils.isNotBlank(result[0])) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + fileName),  true));){
						pwr.println(result[0]);
					}catch(Exception e) {
						logger.error(e);
					}
				}
			} else {
				logger.info("Feed | Cannot Load | " + feed);
			}
		}
	}
	
	public ProductDataBean parse(String xml) {
		
		//	<item>
		//    <sku>MKXQ2ZA/A</sku>
		//    <mpn>MKXQ2ZA/A</mpn>
		//    <upc>888462508391</upc>
		//    <product_name>เคสซิลิโคน iPhone 6 Plus / 6s Plus - สีส้ม</product_name>
		//    <product_summary>ทั้ง iPhone และเคสต่างได้รับการออกแบบมาอย่างประณีตบรรจงจากนักออกแบบของ Apple ทีมเดียวกัน เคสซิลิโคนนี้จึงแนบกระชับกับปุ่มปรับระดับเสียง ปุ่มพัก/เปิด และส่วนโค้งของ iPhone 6s Plus และ iPhone 6 Plus ได้อย่างลงตัวโดยไม่ได้ทำให้รู้สึกว่าหนาขึ้นแต่อย่างใด ส่วนภายในบุด้วยไมโครไฟเบอร์อันอ่อนนุ่มที่ช่วยปกป้อง iPhone ของคุณได้เป็นอย่างดี  อีกทั้งพื้นผิวซิลิโคนด้านนอกก็ให้สัมผัสที่เนียนนุ่ม และยังให้ความรู้สึกดีเยี่ยมเมื่ออยู่ในมือคุณอีกด้วย เคสซิลิโคน iPhone 6s Plus</product_summary>
		//    <product_feature>ทั้ง iPhone และเคสต่างได้รับการออกแบบมาอย่างประณีตบรรจงจากนักออกแบบของ Apple ทีมเดียวกัน เคสซิลิโคนนี้จึงแนบกระชับกับปุ่มปรับระดับเสียง ปุ่มพัก/เปิด และส่วนโค้งของ iPhone 6s Plus และ iPhone 6 Plus ได้อย่างลงตัวโดยไม่ได้ทำให้รู้สึกว่าหนาขึ้นแต่อย่างใด ส่วนภายในบุด้วยไมโครไฟเบอร์อันอ่อนนุ่มที่ช่วยปกป้อง iPhone ของคุณได้เป็นอย่างดี อีกทั้งพื้นผิวซิลิโคนด้านนอกก็ให้สัมผัสที่เนียนนุ่ม และยังให้ความรู้สึกดีเยี่ยมเมื่ออยู่ในมือคุณอีกด้วย</product_feature>
		//    <brand>Apple</brand>
		//    <color>สีส้ม</color>
		//    <price>1590.00</price>
		//    <price_localized>฿ 1590.00</price_localized>
		//    <condition>new</condition>
		//    <product_url>https://aos.prf.hn/click/camref:11loky/creativeref:290010/destination:http://www.apple.com/th/shop/go/product/MKXQ2</product_url>
		//    <image_url>https://store.storeimages.cdn-apple.com/8755/as-images.apple.com/is/image/AppleInc/aos/published/images/M/KX/MKXQ2/MKXQ2?wid=600&amp;hei=600&amp;.v=0.png</image_url>
		//    <free_shipping>true</free_shipping>
		//    <category>acc : apple_branded</category>
		//    <product_type>acc : apple_branded</product_type>
		//    <availability>in stock</availability>
		//    <shipping_cost>0.00</shipping_cost>
		//  </item>
		
		String productName = FilterUtil.getStringBetween(xml, "<product_name>", "</product_name>").trim();
		String productBrand = FilterUtil.getStringBetween(xml, "<brand>", "</brand>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<product_url>", "</product_url>").trim();
		String productImageUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>");
		String productCatId = FilterUtil.getStringBetween(xml, " <category>", "</category>");
		String productDesc = FilterUtil.getStringBetween(xml, "<product_summary>", "</product_summary>").trim();
		String productId = FilterUtil.getStringBetween(xml, "<sku>", "</sku>").trim();

		if (StringUtils.isBlank(productPrice) || StringUtils.isBlank(productName) || StringUtils.isBlank(productUrl)  || StringUtils.isBlank(productId)) {
			return null;
		}

		String suffix = "";
		productName += " -";

		if (StringUtils.isNotBlank(productBrand)) {
			suffix += productBrand;
		}

		if (StringUtils.isNotBlank(productId)) {
			suffix += " - " + productId;
		}
		productName += " (" + suffix + ")";

		productName = StringEscapeUtils.unescapeHtml4(productName);
		productUrl = productUrl.replace("https://aos.prf.hn/click/camref:11loky/creativeref:290010/destination:", "");

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));

		if (productDesc.length() != 0) {
			pdb.setDescription(productDesc);
		}

		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}

		if (productUrl.length() != 0) {
			pdb.setUrl(productUrl);
		}
		String[] catMap = getCategory(StringEscapeUtils.unescapeHtml4(productCatId));
		if (catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}
		pdb.setRealProductId(productId);

		return pdb;
	}

}
