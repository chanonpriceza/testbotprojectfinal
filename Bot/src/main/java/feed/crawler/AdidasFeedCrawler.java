package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AdidasFeedCrawler extends FeedManager {

	public AdidasFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"http://feeds.performancehorizon.com/involve-asia/1011l705/489f273e573dc0fd54dbc669fe274095.xml"};
	}
	
	@Override
	public ProductDataBean parse(String data) {
		Document doc = Jsoup.parse(data, "", Parser.xmlParser());
		String name = doc.select("title").html();
		String price = doc.select("g|sale_price").html();
		String basePrice = doc.select("g|price").html();
		String id = doc.select("g|id").html();
		String desc = doc.select("description").html();
		
		String url = doc.select("link").text();
		String brand = doc.select("g|brand").html();
		String color = doc.select("g|color").html();
		String image = doc.select("g|image_link").html();
		String cat = doc.select("g|product_type").html();
		desc = FilterUtil.toPlainTextString(desc);
		url = FilterUtil.getStringAfter(url,"destination:",url);
		image = FilterUtil.getStringAfter(image,"destination:",image);

		url = StringEscapeUtils.unescapeHtml4(url);
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		
		if("null".equals(desc)) {
			desc  = null;
		}
		String tmp = "";
		
		if(StringUtils.isNotBlank(id)) 
			tmp = " ("+id+")";
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(brand+" "+color+" "+name+tmp);
		pdb.setPrice(p);
		pdb.setDescription(desc);
		pdb.setBasePrice(bp);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}

}
