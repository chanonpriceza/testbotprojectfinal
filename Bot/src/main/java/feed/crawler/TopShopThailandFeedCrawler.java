package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TopShopThailandFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private static final int pageOffset = 20; 
	private static final String pageDomain = "http://th.topshop.com";
	private static final String pageMediaDomain = "http://media.topshop.com/wcsstore/TopShopTH/";
	private boolean nextPage = false;
	private boolean haveResult = false;
	
	public TopShopThailandFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for(String REQUEST_URL : keySet){
			int page = 0;
			nextPage = true;
			String result = "";
			while(!Thread.currentThread().isInterrupted() && nextPage){
				page = page + pageOffset;
				nextPage = false;
				String[] response = HTTPUtil.httpRequestWithStatus(REQUEST_URL.replace("{NO}", Integer.toString(page)).replace("{NRPP}", Integer.toString(pageOffset)), "UTF-8", false);
				if(response == null || response.length != 2) break;
				if(result.equals(response[0])) {
					break;
				}
				result = response[0];
				if(StringUtils.isNotBlank(result)){
					String catId="", keyword="";
					String[] catMap = getCategory(REQUEST_URL);
					if(catMap != null && catMap.length == 2) {
						catId = catMap[0];
						keyword = catMap[1];
					}
					try {
						parseJson(result, catId, keyword);
					} catch (Exception e) {
						logger.error(e);
					}
				} else {
					break;
				}
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void parseJson(String data, String pzcat, String pzkeyword) throws Exception {
		JSONObject jData = (JSONObject)new JSONParser().parse(data);
		
		Object result = jData.get("results");
		if(result == null) return; 
		JSONObject jResult = (JSONObject)new JSONParser().parse(result.toString());
		JSONArray jContents = (JSONArray)jResult.get("contents");
		if(jContents == null || jContents.size() == 0) return;
		for (Object content : jContents) {
			JSONObject jContentObj = (JSONObject)content;
			JSONArray jRecords = (JSONArray)jContentObj.get("records");
			if(jRecords == null || jRecords.size() == 0) return;
			
			for (Object obj : jRecords) {
				
//				"productUrl": "/en/tsth/product/clothing-1226642/cup-detail-short-sleeve-t-shirt-6625813?bi=20&ps=20",
//				"was1Price": 0,
//				"fromIndicator": false,
//				"seoProductToken": "cup-detail-short-sleeve-t-shirt-6625813",
//				"name": "Cup Detail Short Sleeve T-Shirt",
//				"colour": "BLACK",
//				"nowPrice": 990,
//				"outfitImage": "images/catalog/TS04T06LBLK_3col_M_1.jpg",
//				"id": "28528115",
//				"partNumber": "TS04T06LBLK",
//				"numRecords": 0,
//				"b_hasImage": "N",
//				"shortDescription": "Cup Detail Short Sleeve T-Shirt",
//				"longDescription": "This fitted black t-shirt has been reworked with fashion-forward cup detailing to flattering your figure. In stretch cotton fabric, this cropped piece is styled with a crew neckline and short sleeves. It's perfect for pairing with your high-waisted separates. 93% Cotton, 7% Elastane. Machine wash.",
//				"keyword": "04T06LBLK",
//				"b_thumbnailImageSuffixes": "",
//				"thumbnail": "images/catalog/04T06LBLK_thumb.jpg",
//				"productImage": "images/catalog/TS04T06LBLK_3col_F_1.jpg",
//				"EmailBackInStock": "N",
//				"thumbnailImageSuffixes": "_",
//				"catentType": "ProductBean",
//				"isCurrent": false,
//				"seoCommonToken": "en/tsth/product",
//				"ce3ThumbnailSuffixes": "F_1.jpg|M_1.jpg|M_2.jpg|M_3.jpg",
//				"sizeGuide": "Conversions"
					
//				ชื่อสินค้า : "name" + "colour" + ("partNumber") เช่น Embroidered Floral Bardot Top WHITE (TS13N13LWHT)
//				Desc : "longDescription"
					
				JSONObject jObj = (JSONObject)obj;
				Double price 		= (Double)jObj.get("nowPrice");
				String url 			= (String)jObj.get("productUrl");
				String picture 		= (String)jObj.get("productImage");
				
				String description 	= (String)jObj.get("longDescription");
				String name 		= (String)jObj.get("name");
				String colour		= (String)jObj.get("colour");
				String partNumber	= (String)jObj.get("partNumber");
				
				String productName 	= name;
				if(StringUtils.isNotBlank(colour)) productName += " " + colour;
				if(StringUtils.isNotBlank(partNumber)) productName += " (" + partNumber + ")";
				
				if(StringUtils.isBlank(url) || StringUtils.isBlank(productName) ||price == 0){
					continue;
				}
				
				ProductDataBean pdb = new ProductDataBean();
				pdb.setName(FilterUtil.toPlainTextString(productName));
				pdb.setPrice(price);
				pdb.setDescription(FilterUtil.toPlainTextString(description));
				pdb.setPictureUrl(pageMediaDomain + picture);
				pdb.setUrl(pageDomain + url);
				pdb.setUrlForUpdate(pageDomain + url);
				pdb.setCategoryId(StringUtils.isNumeric(pzcat)?Integer.parseInt(pzcat):0);
				pdb.setKeyword(pzkeyword);
				
				String mockData = mockResult(pdb);
				if(StringUtils.isNotBlank(mockData)) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println(mockData);
						haveResult = true;
						nextPage = true;
					}catch(Exception e) {
						logger.error(e);
					}
				}
			}
		}
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	

	
}
