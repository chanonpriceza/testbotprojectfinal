package feed.crawler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BPMuscleFeedCrawler extends FeedManager {
	
	private static JSONParser jsonParser = new JSONParser();
	private static int maxpage = 1;
	private static String currentCat = "";

	private static final Logger logger = LogManager.getRootLogger();
	
	public BPMuscleFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : getCategoryKeySet()) {
			currentCat = feed;
			logger.info("Feed : Load : " + feed);
			for(int i = 1;i<=maxpage;i++) {
				String url = feed;
				url = url.replace("{page}",String.valueOf(i));
				Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + "dataHolder.json").toPath();
				try (InputStream in = new URL(url).openStream()) {
					Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				}catch (ClosedByInterruptException e) {
					logger.error("Closed By Kill Commad");
					return;
				}catch (IOException e) {
					e.printStackTrace();
					logger.error(e);
					//processLog.addException(e.toString()+"-"+this.getClass().getSimpleName(),e);
				}
				mockFeed();
			}
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void mockFeed() {
		readMockFeed();
	}
	
	private void readMockFeed() {
		 try (FileReader reader = new FileReader(BaseConfig.FEED_STORE_PATH + "/" + "dataHolder.json"))
	        {
	            //Read JSON file
	            Object obj = jsonParser.parse(reader);
	 
	            JSONObject objParse = (JSONObject) obj;
	            if(objParse.toJSONString().contains("Product not found."))
	            	return;
	            JSONArray  array = (JSONArray)objParse.get("data");     
	            maxpage = StringUtils.isNumeric(String.valueOf(objParse.get("total_page")))?Integer.parseInt(String.valueOf(objParse.get("total_page"))):0;
	            
	            for(Object o:array) {
	            	JSONObject ob = (JSONObject) o;
	            	parseJSON(ob);
	            }
	         
	 
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
	}
	
	static NumberFormat formatter = new DecimalFormat("#0.00");  
	
	private void parseJSON(JSONObject obj) {
		
		String name = String.valueOf(obj.get("pro_name"));
		String desc = String.valueOf(obj.get("pro_keywords"));
		String url = "https://bpmuscle.com/รายละเอียดสินค้า?productName="+name;
		String id = String.valueOf(obj.get("pro_barcode"));
		String discount = String.valueOf(obj.get("pro_discount"));
		double discountPer = 0;
		if(StringUtils.isNotBlank(discount))
			discountPer = (FilterUtil.convertPriceStr(discount))*0.01;
		
		String price = String.valueOf(obj.get("pro_price"));
		String img = String.valueOf(obj.get("pro_img_file"));
		img = "https://bpmuscle.com//admin/uploads/products/"+img;
		if(StringUtils.isBlank(name)||StringUtils.isBlank(price)||StringUtils.isBlank(id)) {
			logger.info("Data not complete "+name+" "+price+" "+id);
		}

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ("+id+")");
		
		if(discountPer > 0) {
			pdb.setPrice(FilterUtil.convertPriceStr(formatter.format((FilterUtil.convertPriceStr(price))*(1-discountPer))));
			pdb.setBasePrice(FilterUtil.convertPriceStr(price));
		}else {
			pdb.setPrice(FilterUtil.convertPriceStr(price));
		}
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setPictureUrl(img);
		pdb.setDescription(desc);
		pdb.setUrlForUpdate(id);
		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		mockResult(pdb);
		
		
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
