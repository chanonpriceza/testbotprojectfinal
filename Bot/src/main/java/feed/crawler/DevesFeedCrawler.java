package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DevesFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private HashMap<String, String> titlePriceMap =  new HashMap<String, String>();
	private boolean haveResult = false;
	
	public DevesFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> links = getCategoryKeySet();
		for (String link : links) {
			String[] response = HTTPUtil.httpRequestWithStatus(link, "UTF-8", false);
			if(response == null || response.length != 2 ) continue;
			
			String data = response[0];
			if(StringUtils.isBlank(data)) continue;
			
			parseData(data, link);
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	
	public void parseData(String html, String url){

		String category = "0";
		String keyword = "";
		
		String[] dbMap = getCategory(url);
		if(dbMap != null && dbMap.length >= 2) {
			category = dbMap[0];
			keyword = dbMap[1];
		}
		
		String desc = StringEscapeUtils.unescapeHtml4(getPdDescription(html));
		String name = getPdName(html);
		String price = getPdPrice(html,keyword);
		
		if(url.isEmpty() || name.isEmpty() || price.isEmpty()) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		if("ประกันภัยรถยนต์".equals(keyword)){
			if(BotUtil.CONTACT_PRICE_STR.equals(price)){
				name = "ประกันภัยรถยนต์ "+ name.trim() +" เทเวศประกันภัย  ทุนประกัน ";
			}else{
				if(titlePriceMap != null && titlePriceMap.size() > 0){
					String priceType = titlePriceMap.get(price);
					name = "ประกันภัยรถยนต์ "+ name.trim() +" เทเวศประกันภัย  ทุนประกัน  "+priceType;
				}else{
					name = "ประกันภัยรถยนต์ "+ name.trim() +" เทเวศประกันภัย  ทุนประกัน  "+price;
				}
				
			}
			
		}else{
			if(titlePriceMap != null && titlePriceMap.size() > 0){
				String type = titlePriceMap.get(price);
				name = name.trim()+" "+ type +" เทเวศประกันภัย"; 
			}
		}
		
		name = StringEscapeUtils.unescapeHtml4(name);
		
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		pdb.setUrl(url);
		pdb.setCategoryId(Integer.parseInt(category));
		pdb.setPictureUrl("http://www.deves.co.th/Content/img/header-card.png");
		
		if(desc != null){
			pdb.setDescription(desc);
		}
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	private String getPdName(String html){
		String name = FilterUtil.getStringBetween(html, "<div id=\"page-header\">", "</h3>");
		name = FilterUtil.getStringAfterLastIndex(name, "<a href=\"#\">", name);
		name = FilterUtil.toPlainTextString(name);
		if(name.isEmpty()){
			return null;
		}
		return name;
	}
	
	private String getPdPrice(String html, String keyword){
        List<String> allPriceStr = new ArrayList<String>();
 
		String price = "";
		String rawPrice = FilterUtil.getStringBetween(html, "<div id=\"page-header\">", "<div id=\"footer\">");
		price = FilterUtil.getStringBetween(rawPrice, "<tbody>", "</tbody>");
		
	
		if(StringUtils.isNotBlank(price)){
			String curPrice = price;
			curPrice = curPrice.replaceAll("&nbsp;", "");
			price = FilterUtil.getStringBetween(curPrice, "<tr>", "บาท/ปี</td>");
			
			if(StringUtils.isNotBlank(price) && price.matches(".*\\d.*")){
				allPriceStr = FilterUtil.getAllStringBetween(curPrice, "<tr>", "บาท/ปี</td>");
			}else{
				price = FilterUtil.getStringBetween(curPrice, "บาท</td>", "บาท/ปี</td>");
				if(StringUtils.isNotBlank(price) && price.matches(".*\\d.*")){
					allPriceStr = FilterUtil.getAllStringBetween(curPrice, "บาท</td>", "บาท/ปี</td>");
				}else{
					price = FilterUtil.getStringBetween(curPrice, "<tr>", "ปี</td>");
					if(StringUtils.isNotBlank(price) && price.matches(".*\\d.*")){
						allPriceStr = FilterUtil.getAllStringBetween(curPrice, "<tr>", "ปี</td>");
					}
					
				}
			}
			price = getLowPrice(allPriceStr, keyword);
			
		}else{
			allPriceStr = FilterUtil.getAllStringBetween(rawPrice, "<li>", "</li>");
			price = getLowPrice(allPriceStr, keyword);
			if(StringUtils.isBlank(price)){
				price = FilterUtil.getStringBetween(rawPrice, "<span>", "</span>");
				allPriceStr = FilterUtil.getAllStringBetween(price, "<p>", "</p>");
				price = getLowPrice(allPriceStr, keyword);
			}
		
		}
		
		if(price.isEmpty()){
			price = BotUtil.CONTACT_PRICE_STR;
		}
		return price;
	}
	
	private String getPdDescription(String html){
		String desc = FilterUtil.getStringBetween(html, "<div id=\"content-box\">", "<div id=\"footer\">");
		desc = FilterUtil.toPlainTextString(desc);
		if(desc.isEmpty()){
			return null;
		}
		return desc;
	}
	
	private String getLowPrice(List<String> allPriceStr, String keyword){
		String price = "";
		titlePriceMap  = new HashMap<String, String>();
	    List<Integer> allPrice = new ArrayList<Integer>();
		if(allPriceStr != null && allPriceStr.size() > 0){
			for(String pricestr : allPriceStr){
				pricestr = FilterUtil.toPlainTextString(pricestr);
				String priceTitle =StringEscapeUtils.unescapeHtml4(pricestr);
				if(priceTitle.contains("ทุน")){
					priceTitle = FilterUtil.getStringBetween(priceTitle, "ทุน", "บาท");
				}else if(keyword.contains("พ.ร.บ.")){
					priceTitle = FilterUtil.getStringBeforeLastIndex(priceTitle, "ราคา", priceTitle).trim();
				}
				
				
				pricestr = FilterUtil.getStringAfterLastIndex(pricestr, "ราคา", pricestr);
				pricestr = FilterUtil.removeCharNotPrice(pricestr);
				if(StringUtils.isNumeric(pricestr.trim())){
					allPrice.add(Integer.parseInt(pricestr));
					if(StringUtils.isNotBlank(priceTitle)){
						if(!keyword.contains("พ.ร.บ.")){
							priceTitle = FilterUtil.removeCharNotPrice(priceTitle);
						}
						titlePriceMap.put(pricestr.trim(), priceTitle.trim());
					}
				}
			}
			
			if(allPrice != null && allPrice.size() > 0){
				Collections.sort(allPrice);
				price = allPrice.get(0).toString();
			}
			
		}
		return price;
		
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
