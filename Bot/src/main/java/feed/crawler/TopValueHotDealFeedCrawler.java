package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TopValueHotDealFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public TopValueHotDealFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	
	@Override
	public void loadFeed() {
		String[] response = HTTPUtil.httpRequestWithStatus("https://www.topvalue.com/flash-sale.html", "UTF-8", false); 
		if(response == null || response.length != 2) return;
		String html = response[0];
		if(StringUtils.isBlank(html)) return;
		html = FilterUtil.getStringBetween(html, "<div class=\"blog_flashsale_inner category-products responsive-list list-actdeals\">", "<div class=\"footer-container\">");
		List<String> urlList = FilterUtil.getAllStringBetween(html, "href=\"", "\"");
		for(String url : urlList){
			if(!url.contains(".html") && !url.contains("/product/")){
				continue;
			}
			if(url.contains("/product/")){
				url = FilterUtil.getStringBefore(url, "/s/", url) + "/";
			}
			
			if(!url.startsWith("https://")){
				url = "https://www.topvalue.com/" + url;
			}
			
			try{
				String[] htmlResponse = HTTPUtil.httpRequestWithStatus(url, "UTF-8", false);
				if(htmlResponse == null || htmlResponse.length != 2) continue;
					
				String pdHtml = htmlResponse[0];
				mergeProductData(pdHtml, url);
			}catch(Exception e){
				continue;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}	
	public void mergeProductData(String html, String url){
		String name = getPdName(html);
		String price = getPdPrice(html);
		if(url.isEmpty() || name.isEmpty() || price.isEmpty()) {
			return;
		}
		String bPrice = getPdBasePrice(html);
		String desc = getPdDescription(html);
		String pic = getPictureUrl(html);
		
		ProductDataBean pdb = new ProductDataBean();
		name = StringEscapeUtils.unescapeHtml4(name);	

		pdb.setName(name + " (Hot Deal)");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		pdb.setUrl(url);
		pdb.setCategoryId(160255);		
		
		if(bPrice != null){
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(bPrice)));
		}
		if(desc != null){
			pdb.setDescription(desc);
		}
		if(pic != null){
			pdb.setPictureUrl(pic);
		}
		
		String mockData = mockResult(pdb);
		if(StringUtils.isNotBlank(mockData)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(mockData);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}
	
	private String getPdName(String html){
		String name = FilterUtil.getStringBetween(html, "<span class=\"h1\">", "</span>");
		name = name.replace("&nbsp;", "").trim();
		name = FilterUtil.toPlainTextString(name);
		if(name.isEmpty()){
			return null;
		}
		return name;
	}
	
	private String getPdPrice(String html){
		String	price = FilterUtil.getStringBetween(html, "<p class=\"special-price\">", "</p>");
		if(price.isEmpty()){
			price = FilterUtil.getStringBetween(html, "<span class=\"special-price\">", "</p>");
		}
		if (StringUtils.isNotBlank(price)) {
			price = FilterUtil.getStringBetween(price, "<span class=\"price\"  id=\"product-price", "</span>");
			price = FilterUtil.getStringAfter(price, ">", price);
			
		}else{
			price = FilterUtil.getStringBetween(html, "<span class=\"price\">", "บาท");
		}
		price = FilterUtil.toPlainTextString(price);
		price = FilterUtil.removeCharNotPrice(price);
	    

		if(price.isEmpty()){
			return null;
		}
		return price;
	}
	
	private String getPdBasePrice(String html){
		String price = FilterUtil.getStringBetween(html, "<span class=\"price\" id=\"old-price", "</span>");
		if(!price.isEmpty()){
			price = "<span class=\"price\" id=\"old-price" + price;
		}
		price = FilterUtil.toPlainTextString(price);
		price = FilterUtil.removeCharNotPrice(price);
		if(price.isEmpty()){
			return null;
		}
		return price;
	}
	
	private String getPdDescription(String html){
		String desc = FilterUtil.getStringBetween(html, "</h2><div class=\"std\">", "<div class=\"footer-container\">");
		desc = desc.replace("</span>", " ").trim();
		desc = FilterUtil.toPlainTextString(desc);
		if(desc.isEmpty()){
			return null;
		}
		return desc;
	}
	
	private String getPictureUrl(String html){
		String pic = FilterUtil.getStringBetween(html, "<meta itemprop=\"image\" content=\"", "\"");
		pic = FilterUtil.toPlainTextString(pic);
		if(pic.isEmpty()){
			return null;
		}
		return pic;
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
