package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SBDesignSquareFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public SBDesignSquareFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for (String menuId : keySet) {
			int page=0;
			boolean isNext = true;
			
			while(!Thread.currentThread().isInterrupted() && isNext) {
				String url = "https://www.sbdesignsquare.com/ajax/products/departmentFilter";
				String param = "";
				param += "{\"menuIds\":";				param += "["+menuId+"]";
				param += ",\"styGroupIds\":";			param += "[]";
				param += ",\"md1Nodes\":";				param += "[]";
				param += ",\"md5Nodes\":";				param += "[]";
				param += ",\"colsIds\":";				param += "[]";
				param += ",\"prices\":";				param += "[]";
				param += ",\"isCondoFit\":";			param += "false";
				param += ",\"isCleaf\":";				param += "false";
				param += ",\"isInstallment\":";			param += "false";
				param += ",\"page\":";					param += page;
				param += ",\"size\":";					param += "12";
				param += ",\"sortingOption\":";			param += "\"RECOMMEND\"";
				param += ",\"searchTexts\":";			param += "[]}";
				
				String result = postJsonRequest(url,param,"UTF-8",true);
				if(StringUtils.isBlank(result)) break;
				
				if(result.indexOf("\"rel\":\"next\"") == -1) isNext = false;
				
				try {
					parseJson(result, menuId);
				} catch (ParseException e) {
					logger.error(e);
				}
				
				page++;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void parseJson(String data, String menuId) throws ParseException {

		JSONObject jData = (JSONObject)new JSONParser().parse(data);
		JSONArray jArr = (JSONArray)jData.get("content");
		for (Object obj : jArr) {
			JSONObject jObj = (JSONObject)obj;
			JSONObject jObjPrice = (JSONObject)jObj.get("pricelists");
			JSONObject jObjPic = (JSONObject)jObj.get("a4Picture");
			
			String productName = (String) jObj.get("ptitle");
			String productId = (String) jObj.get("matnr");
			double productPrice = ((Number)jObjPrice.get("specialPrice")).doubleValue();
			double productBasePrice = ((Number)jObjPrice.get("normalPrice")).doubleValue();
			String productDesc = (String) jObj.get("exDetailTh");
			String productPicture = "";
			if(jObjPic != null) {
				productPicture = (String) jObjPic.get("url");
				if(StringUtils.isNoneBlank(productPicture)) {
					if(!productPicture.contains("http")) {
						productPicture = productPicture.replace("//", "http://");
					}
				}
			}
			
			String productSlug = (String) jObj.get("slug");
			String productUrl = "https://www.sbdesignsquare.com/th/products/" + productId + "/" + productSlug;
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(productName + " (" + productId + ")");
			pdb.setPrice(productPrice);
			pdb.setBasePrice(productBasePrice);
			pdb.setDescription(productDesc);
			pdb.setPictureUrl(productPicture);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);

			String[] catMap = getCategory(menuId);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
	}
	
	public static String postJsonRequest(String url, String data, String charset, boolean redirectEnable) {

		DataOutputStream outStream = null;
		URL u = null;
		HttpURLConnection conn = null;
		BufferedReader brd = null;
		InputStreamReader isr = null;
		try {

			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");				
			conn.setRequestProperty("Content-Length", "" + data.length());
			conn.setInstanceFollowRedirects(redirectEnable);
			outStream = new DataOutputStream(conn.getOutputStream());

			outStream.writeBytes(data);
			outStream.flush();
			outStream.close();

			isr = new InputStreamReader(conn.getInputStream(), charset);
			brd = new BufferedReader(isr);
			StringBuilder rtn = new StringBuilder(1000);
			String line = "";
			while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
				rtn.append(line);
			}

			return rtn.toString();
		} catch (MalformedURLException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		} finally {
			if (outStream != null) { try { outStream.close(); } catch (IOException e1) {	} }
			if (brd != null) { try { brd.close(); } catch (IOException e1) { } }
			if (isr != null) { try { isr.close(); } catch (IOException e1) { } }
			conn.disconnect();
		}

		return null;
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
