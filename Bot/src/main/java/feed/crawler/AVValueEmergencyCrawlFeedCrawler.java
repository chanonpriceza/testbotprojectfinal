package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class AVValueEmergencyCrawlFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public AVValueEmergencyCrawlFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){

		String name 		= data.get(0);
		String price 		= data.get(1);
		String basePrice 	= data.get(2);
		String url 			= data.get(3);
		String categoryId 	= data.get(4);
		String keyword 		= data.get(5);
		String description 	= data.get(6);
		String pictureUrl 	= data.get(7);
		
		if( StringUtils.isBlank(name)){
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl(url);
		pdb.setUrlForUpdate(url);
		pdb.setDescription(StringUtils.defaultString(description));
		pdb.setPictureUrl(pictureUrl);
		pdb.setKeyword(keyword);
		pdb.setCategoryId(Integer.parseInt(categoryId));
		
		return pdb;
	}
	
}
