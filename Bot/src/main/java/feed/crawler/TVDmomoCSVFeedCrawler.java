package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TVDmomoCSVFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public TVDmomoCSVFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://marketing.acommerce.asia/marketing/ultimate/OneFeed/Brand/TVDMomo/TVDMomoFBFeed.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		try {

//			0	id,
//			1	title,
//			2	description,
//			3	price,
//			4	sale_price,
//			5	sale_price_effective_date,
//			6	link,
//			7	image_link,
//			8	product_type,
//			9	availability, 					→ out of stock / in stock
//			10	condition,
//			11	brand

			String id 			= data.get(0);
			String title 		= data.get(1);
			String description 	= data.get(2);
			String price 		= data.get(3);
			String sale_price 	= data.get(4);
			String link 		= data.get(6);
			String image_link 	= data.get(7);
			String product_type = data.get(8);
			String availability = data.get(9);
			
			if (StringUtils.isNotBlank(availability) && !availability.equals("in stock")) {
				return null;
			}

			if (StringUtils.isBlank(title) || StringUtils.isBlank(link) || StringUtils.isBlank(sale_price)) {
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(title + " (" + id + ")");
			pdb.setDescription(description);
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(sale_price)));
			pdb.setBasePrice(FilterUtil.convertPriceStr(price));
			pdb.setPictureUrl(image_link);
			pdb.setUrl(link);
			pdb.setUrlForUpdate(id);
			pdb.setRealProductId(id);
			
			String[] mapping = getCategory(product_type);
			if (mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);
			} else {
				pdb.setCategoryId(0);
			}
			
			return pdb;
			
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}
	
}
