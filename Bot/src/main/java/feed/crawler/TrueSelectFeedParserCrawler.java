package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class TrueSelectFeedParserCrawler  extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	
	public TrueSelectFeedParserCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
		URL u = new URL("http://web-api.true-shopping.com/api/v1/priceza/get_product");
		HttpURLConnection http = (HttpURLConnection)u.openConnection();
		http.setRequestProperty("User-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36");
		http.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		InputStream in = http.getInputStream();
		InputStreamReader isr = new InputStreamReader(in);
		JSONParser parser = new JSONParser();
			JSONObject jsOrr = ((JSONObject)parser.parse(isr));
			JSONObject data = (JSONObject)jsOrr.get("data");
			JSONArray jsArr = (JSONArray)data.get("product");
			for(Object j:jsArr) {
				JSONObject jsObj = (JSONObject) j;
				parseJSON(jsObj);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	private void parseJSON(JSONObject jsObj) {
		//System.out.println(jsObj);
		String name = (String) jsObj.get("name");
		String price = (String) jsObj.get("price");
		String des = (String) jsObj.get("description");
		String url = (String) jsObj.get("url");
		String catid = (String) jsObj.get("category_id");
		String base_price = (String) jsObj.get("base_price");
		String id = (String) jsObj.get("id");
		String pictureurl = (String) jsObj.get("image_url");
		
		if(StringUtils.isBlank(name)||StringUtils.isBlank(price)||StringUtils.isBlank(url)||StringUtils.isBlank(id)) {
			//System.out.println("not found main attribute");
			return ;
		}
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name + "("+id+")");
		pdb.setUrl(url);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setRealProductId(id);
		
		
		if(StringUtils.isNotBlank(base_price)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(base_price));
		}
		if(StringUtils.isNotBlank(des)) {
			pdb.setDescription(des);
			
		}if(StringUtils.isNotBlank(pictureurl)) {
			pdb.setPictureUrl(pictureurl);
		}
		String[] mapping = getCategory(catid);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			logger.info("0 cat:"+catid);
			pdb.setCategoryId(0);
		}
		
		String mockData = mockResult(pdb);
		if(StringUtils.isNotBlank(mockData)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(mockData);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}	
	
}
