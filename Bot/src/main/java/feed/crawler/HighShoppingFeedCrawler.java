package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HighShoppingFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private Set<String> x = new HashSet<>();
	private boolean haveResult = false;
	
	public HighShoppingFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		x = new HashSet<>();
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> allCat = getCategoryKeySet();
		for (String cat : allCat) {
			int page = 1;
			String result= "";
			
			do {
			
				String url = "https://www.highshopping.com/category/categoryView.do?categoryCode="+cat+"&orderType=2&rowsPerPage=100&viewType=0&keywordSearch=";
				String data = "currentPage="+page;

				String[] response = HTTPUtil.httpPostRequestWithStatus(url, data, "UTF-8", true, false);
				if(response == null || response.length != 2) break;
				
				result = response[0];
				if(StringUtils.isBlank(data)) break;
				
				List<String> goodsList = FilterUtil.getAllStringBetween(result,"<div class=\"list-item\">","<!-- //.list-item -->");
				
				if(goodsList==null || goodsList.size()==0) {
					break;
				}
			
				long startCount = x.size();
				for(String cutData:goodsList) {
					if(cutData.contains("<div class=\"cell goods\">"))continue;
					parseHTML(cutData,cat);
				}
				long finishCount = x.size();
				
				if(startCount == finishCount) break;
				
				haveResult = true;
				page ++;
				
			}while(!Thread.currentThread().isInterrupted() && StringUtils.isNotBlank(result));
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	
	private void parseHTML(String html,String cat) {
		String name = FilterUtil.getStringBetween(html,"<h3 class=\"title\">","</h3>");
		name = FilterUtil.getStringBetween(name,";\">","<");
		String price = FilterUtil.getStringBetween(html,"<p class=\"meta sales\">","</p>");
		price = FilterUtil.removeCharNotPrice(price);
		String basePrice = FilterUtil.getStringBetween(html,"<p class=\"meta price\">","</p>");
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		String picturUrl = FilterUtil.getStringBetween(html,"<img src=\"","\"");
		String id = FilterUtil.getStringBetween(html,"javascript:goodsDetail('","'");
		String url = "http://www.highshopping.com/display/goods/"+id+"?goods_code="+id;

		if(StringUtils.isBlank(name) || StringUtils.isBlank(price) || StringUtils.isBlank(basePrice) || StringUtils.isBlank(picturUrl) || StringUtils.isBlank(url)) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setPictureUrl(picturUrl);
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		
		if(x.contains(url+price)) {
			logger.error("duplicate " + url + " |price " + price);
			return;
		}else {
			x.add(url+price);
		}
		
		String[] catMap = getCategory(cat);
		
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}

		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
