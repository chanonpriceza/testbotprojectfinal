package feed.crawler;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class DirectAsiaFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public DirectAsiaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){

//		0		   Product_id                             1
//		1          broker                                 Direct Asia
//		2          insurer                                Direct Asia
//		3          title                                  Value Plan Type 1
//		4          type                                   1
//		5          premium                                15023.87
//		6          Make                                   Chevrolet
//		7          Model                                  Captiva
//		8          sub_model                              Captiva 2.0CC
//		9          Engine                                 2.0CC
//		10         year                                   2009
//		11         URL ของสินค้านั้นๆ (ถ้ามี)
//		12         compulsory_insurance                   Y
//		13         sum_insured                            370000
//		14         fire_damage                            370000
//		15         theft                                  370000
//		16         flood_damage                           370000
//		17         deductible                             0
//		18         repair                                 Dealer
//		19         bodily_injury_liability                10000000
//		20         bodily_injury_liability/Accident       10000000
//		21         property_damage_liability              5000000
//		22         personal_injury_protection             100000
//		23         bail_bond                              50000
//		24         medical_payments                       100000
		
		if(data == null || data.size() < 25) {
			return null;
		}

		String id								= String.valueOf(data.get(0)).replace("-", "_");
		
		String poType							= String.valueOf(data.get(4)).replace("-", "_");
		String insName							= String.valueOf(data.get(2)).replace("-", "_");
		String brand							= String.valueOf(data.get(6)).replace("-", "_");
		String model							= String.valueOf(data.get(7)).replace("-", "_");
		String modelDes							= String.valueOf(data.get(8)).replace("-", "_");
		String year								= String.valueOf(data.get(10)).replace("-", "_");
		
		String packName							= String.valueOf(data.get(3)).replace("-", "_");
		String fix								= String.valueOf(data.get(18)).replace("-", "_");
		String od								= String.valueOf(data.get(13)).replace("-", "_");
		String oddd								= String.valueOf(data.get(17)).replace("-", "_");
		String compulsory						= String.valueOf(data.get(12)).replace("-", "_");
		String fire								= String.valueOf(data.get(14)).replace("-", "_");
		String theft							= String.valueOf(data.get(15)).replace("-", "_");
		String flood							= String.valueOf(data.get(16)).replace("-", "_");
		String bi								= String.valueOf(data.get(19)).replace("-", "_");
		String bt								= String.valueOf(data.get(20)).replace("-", "_");
		String tp								= String.valueOf(data.get(21)).replace("-", "_");
		String pa								= String.valueOf(data.get(22)).replace("-", "_");
		String bb								= String.valueOf(data.get(23)).replace("-", "_");
		String me								= String.valueOf(data.get(24)).replace("-", "_");
		
		String price							= String.valueOf(data.get(5)).replace("-", "_");
		
		fix = (fix.equals("Dealer"))? "ห้าง" : "อู่";
		compulsory = (compulsory.equals("Y"))? "รวมพรบ." : "ไม่รวมพรบ.";
		
		String productName = "";
		String productDesc = "";
		String productPrice = price;
		String productPic = "https://i.ibb.co/Hpy27vs/direct-asia-broker.jpg";
		String productUrl = "https://www.priceza.com/merchant/direct-asia";
		
		String uniqueText = "";
		uniqueText = toMD5(id);
		
//		ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
		productName += "ประกันชั้น " + poType.trim();
		productName += " - " + insName;
		productName += " - " + brand;
		productName += " - " + model;
		productName += " - " + ((StringUtils.isNotBlank(modelDes))? modelDes : "ไม่ระบุรุ่นย่อย"); 
		productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
		productName += " (" + uniqueText.substring(0,10) + ")";
		productName = productName.replace(",", "");
		productName = productName.replaceAll("\\s+", " ");                                                  
		productName = productName.replace("( ", "(");                                                       
		productName = productName.replace(" )", ")");                                                       
		productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
		productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")"); 
		
//		( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
		productDesc += ((StringUtils.isNotBlank(packName))? packName : "ไม่ระบุแผน" );
		productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(fix))? fix : " ไม่ระบุอู่/ห้าง");
		productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od) || od.equals("000")|| od.equals("0"))?"ไม่ระบุ":od);
		productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd)|| oddd.equals("000")|| oddd.equals("0"))?"ไม่ระบุ": oddd);
		productDesc += " - " + ((StringUtils.isBlank(compulsory) || compulsory.equals("no"))?"ไม่รวมพรบ.":"รวมพรบ. ");
		productDesc += " - คุ้มครองไฟไหม้  " + ((StringUtils.isBlank(fire) || fire.equals("000")|| fire.equals("0"))?"ไม่ระบุ":fire);
		productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isBlank(theft)|| theft.equals("000")|| theft.equals("0"))?"ไม่ระบุ":theft);
		productDesc += " - คุ้มครองน้ำท่วม  " + ((StringUtils.isBlank(flood)|| flood.equals("000")|| flood.equals("0"))?"ไม่ระบุ":flood);
		productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isBlank(bi)|| bi.equals("000")|| bi.equals("0"))?"ไม่ระบุ":bi);
		productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isBlank(bt)|| bt.equals("000")|| bt.equals("0"))?"ไม่ระบุ":bt);
		productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isBlank(tp)|| tp.equals("000")|| tp.equals("0"))?"ไม่ระบุ":tp);
		productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isBlank(pa)|| pa.equals("000")|| pa.equals("0"))?"ไม่ระบุ":pa);
		productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isBlank(bb)|| bb.equals("000")|| bb.equals("0"))?"ไม่ระบุ":bb);
		productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isBlank(me)|| me.equals("000")|| me.equals("0"))?"ไม่ระบุ":me);
		productDesc = productDesc.replaceAll("\\s+", " ");                                               
		productDesc = productDesc.replace("( ", "(");                                                    
		productDesc = productDesc.replace(" )", ")");                                                    
		productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
		productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
		productDesc = productDesc.replace(",", "");
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		pdb.setPictureUrl(productPic);
		pdb.setDescription(productDesc);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(uniqueText);
		pdb.setCategoryId(250101);
		pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;

		return pdb;
	
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
}
