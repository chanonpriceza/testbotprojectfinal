package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class ReebonzFeedCrawler extends FeedManager {


	public ReebonzFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
 
	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	
	
	public ProductDataBean parse(String data){
		Document doc 		= Jsoup.parse(data);
		String  name		= doc.select("title").html();
		String  price		= doc.select("g|sale_price").html();
		String  baseprice	= doc.select("g|price").html();
		String  url			= FilterUtil.getStringBetween(data,"<link>","</link>");
		String  id			= doc.select("g|mpn").html();
		String  pic			= doc.select("g|image_link").html();
		String  desc		= doc.select("description").html();
		String  cat			= doc.select("g|google_product_category").html();
		String  sex			= doc.select("g|gender").html();
		String  condition	= doc.select("g|condition").html();
		String  color		= doc.select("g|colour").html();
		String conditionTmp = "";
		String catTmp       = "";
		cat = StringEscapeUtils.unescapeHtml4(cat);
		price = FilterUtil.removeCharNotPrice(price);
		baseprice = FilterUtil.removeCharNotPrice(baseprice);
		double temp_price 	= 0;
		
		if("used".equals(condition)) 
			conditionTmp = "[มือสอง]";
		else 
			conditionTmp = "[new]";
		
		double price_d = FilterUtil.convertPriceStr(price);
		double base_d = FilterUtil.convertPriceStr(baseprice);
		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
		
		catTmp = sex+"|"+cat;
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(conditionTmp+" "+name+" "+" "+color+" ("+id+")");
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPrice(price_d);
		pdb.setBasePrice(base_d);
		pdb.setDescription(desc);
		pdb.setUrl(url);
		pdb.setPictureUrl(pic);
		String[] mapping = getCategory(catTmp);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		}
		
		
		return pdb;
	
	}
	
}
