package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class NaiinFeedCrawler extends FeedManager{

	public NaiinFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://api.naiin.com/Product/ListProduct"};
	}
	
	public ProductDataBean parse(JSONObject jObj) {
				
		String productName 		= (String)jObj.get("name").toString();
		String productPrice 	= (String)jObj.get("price").toString();
		String productBasePrice = (String)jObj.get("base_price").toString();
		String productDesc 		= (String)jObj.get("description").toString();
		String productImageUrl 	= (String)jObj.get("image_url").toString();
		String productUrl 		= (String)jObj.get("url").toString();
		String productCatId 	= (String)jObj.get("category_id").toString();
		String productId 		= (String)jObj.get("id").toString();
		String productUpc 		= (String)jObj.get("upc").toString();
		
		productUrl = productUrl.trim();
		
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;
		}
				
		ProductDataBean pdb = new ProductDataBean();
		productName = StringEscapeUtils.unescapeHtml4(FilterUtil.toPlainTextString(productName.trim()));
		
			String[] catMap = getCategory(productCatId);
			if(catMap != null) {		
				int subCategory = BotUtil.stringToInt(catMap[0], 0);
//				if(subCategory==231008||subCategory==230240)
//					return null;
				pdb.setCategoryId(subCategory);
				pdb.setKeyword(catMap[1]);			
				if(subCategory != 0 && !productName.startsWith("หนังสือ")){
					int cat = subCategory - (subCategory % 10000);
					if(cat == 230000){
						productName = "หนังสือ" + productName;
					}
				}
			}else{
				pdb.setCategoryId(0);
			}
		
		pdb.setName(productName);
		productUrl = productUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace("&amp;", "&");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrl);
		
		if (productDesc.length() != 0) {
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			pdb.setDescription(FilterUtil.toPlainTextString(productDesc));
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		if (productUpc.length() != 0) {
			pdb.setUpc(productUpc);
		}

		return pdb;
		
	}
	
}
