package feed.crawler;

import java.sql.SQLException;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LazadaAdsenseFeedCrawler extends FeedManager{

	private static long CRAWL_LIMIT = 30000000;
	private static boolean SHOW_EXCEED_LOG = false;
    
	public LazadaAdsenseFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public void clearFeed() throws SQLException {
		logger.info("Skip clear feed");
		return ;
	}

	@Override
	public void loadFeed() {
		logger.info("Skip load feed");
		return ;
	}



	@Override
	public String[] setFeedFile() {
		if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_CRAWLER_PARAM) && NumberUtils.isCreatable(BaseConfig.CONF_FEED_CRAWLER_PARAM))
			CRAWL_LIMIT = NumberUtils.toLong(BaseConfig.CONF_FEED_CRAWLER_PARAM);
		
		logger.info("CRAWL_LIMIT : " + CRAWL_LIMIT);
		
		return new String[] {
//				"http://52.74.190.12/sale_upload/Main_Audio.tsv",
//				"http://52.74.190.12/sale_upload/Main_Bags_and_Travel.tsv",
//				"http://52.74.190.12/sale_upload/Main_Beauty.tsv",
//				"http://52.74.190.12/sale_upload/Main_Bedding_Bath.tsv",
//				"http://52.74.190.12/sale_upload/Main_Cameras_Drones.tsv",
//				"http://52.74.190.12/sale_upload/Main_Computer_Laptops.tsv",
//				"http://52.74.190.12/sale_upload/Main_Data_Storage.tsv",
//				"http://52.74.190.12/sale_upload/Main_Digital_Goods.tsv",
//				"http://52.74.190.12/sale_upload/Main_Digital_Utilities.tsv",
//				"http://52.74.190.12/sale_upload/Main_Free_Sample_FlexiCombo.tsv",
//				"http://52.74.190.12/sale_upload/Main_Furniture_Organization.tsv",
//				"http://52.74.190.12/sale_upload/Main_Groceries.tsv",
//				"http://52.74.190.12/sale_upload/Main_Health.tsv",
//				"http://52.74.190.12/sale_upload/Main_Household_Supplies.tsv",
//				"http://52.74.190.12/sale_upload/Main_Kitchen_Dining.tsv",
//				"http://52.74.190.12/sale_upload/Main_Large_Appliances.tsv",
//				"http://52.74.190.12/sale_upload/Main_Laundry_Cleaning_Equipment.tsv",
//				"http://52.74.190.12/sale_upload/Main_Lighting_Decor.tsv",
//				"http://52.74.190.12/sale_upload/Main_Media_Music_Books.tsv",
//				"http://52.74.190.12/sale_upload/Main_Mens_Shoes_and_Clothing.tsv",
				"http://52.74.190.12/sale_upload/Main_Mobiles_Tablets.tsv",
				"http://52.74.190.12/sale_upload/Main_Monitors_Printers.tsv",
				"http://52.74.190.12/sale_upload/Main_Mother_Baby.tsv",
				"http://52.74.190.12/sale_upload/Main_Motors.tsv",
				"http://52.74.190.12/sale_upload/Main_Outdoor_Garden.tsv",
				"http://52.74.190.12/sale_upload/Main_Pet_Supplies.tsv",
				"http://52.74.190.12/sale_upload/Main_Service_Product.tsv",
				"http://52.74.190.12/sale_upload/Main_Services.tsv",
				"http://52.74.190.12/sale_upload/Main_Small_Appliances.tsv",
				"http://52.74.190.12/sale_upload/Main_Smart_Devices.tsv",
				"http://52.74.190.12/sale_upload/Main_Sports_Outdoors.tsv",
				"http://52.74.190.12/sale_upload/Main_Stationery_Craft.tsv",
				"http://52.74.190.12/sale_upload/Main_Televisions_Videos.tsv",
				"http://52.74.190.12/sale_upload/Main_Tools_Home_Improvement.tsv",
				"http://52.74.190.12/sale_upload/Main_Toys_Games.tsv",
				"http://52.74.190.12/sale_upload/Main_Watches_Sunglasses_Jewelley.tsv",
				"http://52.74.190.12/sale_upload/Main_Womens_Shoes_and_Clothing.tsv",
		};
	}
	
	public ProductDataBean parse(CSVRecord data) {
		
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			if((ReportManager.getInstance().getReport().getCountOld() + ReportManager.getInstance().getReport().getAdd()) > CRAWL_LIMIT) {
				if(!SHOW_EXCEED_LOG) {
					SHOW_EXCEED_LOG = true;
					logger.info("WCE Exceed Log, ignore all after this.");
					logger.info("CountOld : " + ReportManager.getInstance().getReport().getCountOld() + ", CountAdd : " + ReportManager.getInstance().getReport().getAdd());
				}
				return null;
			}
		}
		
		//      lazada_sku                         898278514_TH-1812130565
		//      product_id                         898278514
		//      sku_id                             1812130565
		//      product_name                       แอร์ฟรีกลางวัน พลังงานแสงอาทิตย์
		//      description                        แอร์ฟรีกลางวัน พลังงานแสงอาทิตย์แอร์ฟรีกลางวันใช้ตู้ควบคุมแอร์แผงโซล่าเซลล์ 4 แผง
		//      currency                           THB
		//      current_price                      28900
		//      price                              34900
		//      discount_percentage                17.19
		//      seller_name                        PSmartITplus
		//      brand_name                         No Brand
		//  	product_small_img                  https://th-live.slatic.net/p/d3a898cfd314e715fd814eb528b02366.jpg
		//      product_medium_img                 https://th-live.slatic.net/p/d3a898cfd314e715fd814eb528b02366.jpg
		//      product_big_img                    https://th-live.slatic.net/p/d3a898cfd314e715fd814eb528b02366.jpg
		//      image_url_2                        https://th-live.slatic.net/p/77c62896272c28206dbe5dbfeaaffee2.jpg
		//      image_url_3                        https://th-live.slatic.net/p/05cadfdf1619bdce2f5d82e2461f93f9.jpg
		//      image_url_4                        https://th-live.slatic.net/p/5af7aff99a0fc6a8911aaf6123f80a50.jpg
		//      image_url_5
		//      has_bonus_commission               1
		//      is_purchasable                     1
		//      is_flash_sale                      0
		//      is_hot_deals                       0
		//      seller_rating                      91
		//      bonus_commission_rate              0.04
		//      product_url                        https://www.lazada.co.th/products/-i898278514-s1812130565.html
		//		cps_tracking_link                  https://c.lazada.co.th/t/c.Ovw?url=https%3A%2F%2Fwww.lazada.co.th%2Fproducts%2F-i898278514-s1812130565.html
		//      bonus_link                         https://c.lazada.co.th/t/c.0aFNUr?url=https%3A%2F%2Fwww.lazada.co.th%2Fproducts%2F-i898278514-s1812130565.html
		
		String realProductId	= data.get("sku_id");
		String productName 		= data.get("product_name");
		String productPrice		= data.get("current_price");
		String productBasePrice = data.get("price");
		
		String brand_name		= data.get("brand_name");
		String productDesc		= data.get("description");
		
		String imageUrl 		= "";
		if(StringUtils.isBlank(imageUrl)) imageUrl = data.get("product_big_img");
		if(StringUtils.isBlank(imageUrl)) imageUrl = data.get("product_medium_img");
		if(StringUtils.isBlank(imageUrl)) imageUrl = data.get("product_small_img");
		
		String productUrl		= "";
		if(StringUtils.isBlank(productUrl)) productUrl = data.get("bonus_link");
		if(StringUtils.isBlank(productUrl)) productUrl = (StringUtils.isBlank(data.get("cps_tracking_link"))? "" : data.get("cps_tracking_link").replace("https://c.lazada.co.th/t/c.Ovw?url=", "https://c.lazada.co.th/t/c.PVm?url=") + "&pztrack");
		
		if(StringUtils.isBlank(productUrl)
				|| !productUrl.startsWith("http") 
				|| StringUtils.isBlank(productName) 
				|| StringUtils.isBlank(realProductId) 
				|| (StringUtils.isBlank(productBasePrice) && StringUtils.isBlank(productPrice))){
			return null;
		}
		
		if(productName.contains("�")){
			return null;
		}
		
		productName = FilterUtil.removeEmoji(productName);
		productDesc = FilterUtil.removeEmoji(productDesc);
		
		ProductDataBean pdb = new ProductDataBean();
		String name = productName + " (" + realProductId + ")";
		pdb.setName(BotUtil.limitString(name, 200));
		pdb.setDescription(productDesc);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(realProductId);
		pdb.setRealProductId(realProductId);
		
		if (StringUtils.isNotBlank(productBasePrice)) {
			double basePrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice));
			if(basePrice > 0) pdb.setBasePrice(basePrice);
		}

		if (StringUtils.isNotBlank(productPrice)) {
			double price = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice));
			if(price > 0) pdb.setPrice(price);
		}
		
		if (!imageUrl.isEmpty() && imageUrl.startsWith("http") && BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			if(imageUrl.contains("th-live.slatic.net") && (imageUrl.contains(".jpg") || imageUrl.contains(".png") || imageUrl.contains(".jpeg"))) {
				pdb.setPictureUrl(imageUrl);
			}
		}

		String[] catMap = getCategory(FilterUtil.getStringBefore(BaseConfig.FEED_READING, ".", BaseConfig.FEED_READING) + "|" + brand_name);
		if (catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		} else { 
			catMap = getCategory(FilterUtil.getStringBefore(BaseConfig.FEED_READING, ".", BaseConfig.FEED_READING));
			if(catMap != null) {
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);
			}
					
		}
		
		return pdb;
	}
	
}
