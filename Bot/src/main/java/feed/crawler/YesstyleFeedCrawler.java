package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.helper.StringUtil;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class YesstyleFeedCrawler extends FeedManager {

//	private final String filePath = "5038.csv";
	private static final Logger logger = LogManager.getRootLogger();
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	
	public YesstyleFeedCrawler() throws Exception {
		super();
		
	}
	
	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String id = data.get(0);
		String name = data.get(1);
		String price = data.get(2);
		String description = data.get(3);
		String prod_cat = data.get(5);
		String producturl = data.get(6);
		String image_url = data.get(7);
		String base_price = data.get(8);

		
		if(StringUtil.isBlank(name)||StringUtil.isBlank(producturl)||StringUtil.isBlank(price)||StringUtil.isBlank(id)||StringUtil.isBlank(image_url)) {
			logger.info(data+" not complete");
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setUrl(producturl);
		pdb.setPictureUrl(image_url);
		
		if(StringUtils.isNotBlank(base_price)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(base_price));
			
		}
		
		if(StringUtils.isNotBlank(description)) {
			pdb.setDescription(description);
		}
		
		String[] mapping = getCategory(prod_cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}
	

	
	

}
