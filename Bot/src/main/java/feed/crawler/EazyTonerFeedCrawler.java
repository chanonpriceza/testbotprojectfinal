package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class EazyTonerFeedCrawler extends FeedManager{

//	http://www.quicksupply.co.th/productfeed
//	UTF-16LE
	
	public EazyTonerFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://www.quicksupply.co.th/productfeed"
			};
	}
	
	public ProductDataBean parse(String xml) {

		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId = FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
		String productDescription = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
	
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty() || realProductId.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();
		productName = unEscape(productName);
		pdb.setName(productName + " ("+ realProductId +")");
		if(productPrice.equals("0") || productPrice.equals("n/a")){
			pdb.setPrice(BotUtil.CONTACT_PRICE);
		}else{
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		}
		productUrl = unEscape(productUrl);
		pdb.setUrl(productUrl);	
		
		if (!productPictureUrl.isEmpty()) {
			productPictureUrl = unEscape(productPictureUrl);	
			pdb.setPictureUrl(productPictureUrl);
		}

		pdb.setRealProductId(realProductId);
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		
		if (!productDescription.isEmpty()) {
			productDescription = unEscape(productDescription);
			pdb.setDescription(productDescription);
		}
		
		return pdb;
		
	}
	
	private String unEscape(String value){
		value = value.replace("&amp;", "&");
		value = StringEscapeUtils.unescapeHtml4(value);
		return	value;
	}

}
