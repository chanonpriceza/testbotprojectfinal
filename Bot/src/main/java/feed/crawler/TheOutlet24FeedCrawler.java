package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TheOutlet24FeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	private String tmp = "";
	
	public TheOutlet24FeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		feedRequest("http://marketing.acommerce.asia/marketing/theoutlet24/productFeed/outlet24PricezaFeed.xml", "UTF-8", false, 1000);
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	private void processLine(String line) {
		
		line = StringEscapeUtils.unescapeHtml4(line);
		tmp += " " + line;				
		List<String> product = FilterUtil.getAllStringBetween(tmp, "<product id", "</product>");
		if(product != null && product.size() > 0) {	
			tmp = FilterUtil.getStringAfterLastIndex(tmp, "</product>", "");	 	
			for (String s : product) {
				processProductData(s);
			}
		}
	}

	public void processProductData(String xml){
		
		String productId = FilterUtil.getStringBetween			(xml, "\"","\"").trim();
		String productBrand = FilterUtil.getStringBetween		(xml, "<brand>", "</brand>").trim();
		String productName = FilterUtil.getStringBetween		(xml, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween		(xml, "<sale_price>", "</sale_price>").trim();
		String productBasePrice = FilterUtil.getStringBetween	(xml, "<price>", "</price>").trim();
		String productPictureUrl = FilterUtil.getStringBetween	(xml, "<image_url>", "</image_url>").trim();
		String productDesc = FilterUtil.getStringBetween		(xml, "<description>", "</description>").trim();
		String productCatId = FilterUtil.getStringBetween		(xml, "<categoryid>", "</categoryid>").trim();
		String productUrl = FilterUtil.getStringBetween			(xml, "<url>", "</url>").trim();
		if(StringUtils.isBlank(productPrice)) {
			productPrice = productBasePrice;
		}
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return;		
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		productName = productBrand + " " + productName + " (" + productId + ") ";
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productName = FilterUtil.toPlainTextString(productName);
		
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));	
		if(!productPrice.equals(productBasePrice)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));					
		}
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrl);
		
		if (!productPictureUrl.isEmpty()) {	
//			productPictureUrl = productPictureUrl.replace("http://", "https://");
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!productDesc.isEmpty()){
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			pdb.setDescription(productDesc);
		}

		String[] catMap = getCategory(productCatId);
		
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}

	private void feedRequest(String url, String charset, boolean redirectEnable, int charBufferSize) {
		URL u = null;
	  	HttpURLConnection conn = null;
		try {
			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			
			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = new InputStreamReader(is, charset);) {
    				char[] buffer = new char[charBufferSize];
    				while(!Thread.currentThread().isInterrupted()){
    					int rsz = isr.read(buffer, 0, buffer.length);
    					if (rsz <= 0) {
    						break;
    					}
    					processLine(new String(buffer, 0, rsz));
    			    }
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if(conn != null) { conn.disconnect(); }
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}


}
