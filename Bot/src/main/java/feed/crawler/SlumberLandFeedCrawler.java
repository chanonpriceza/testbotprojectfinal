package feed.crawler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class SlumberLandFeedCrawler extends FeedManager {
	
	private static final Logger logger = LogManager.getRootLogger();
	private boolean haveResult ;
	private String currentCat = "";
	private static JSONParser jsonParser = new JSONParser();
	
	public SlumberLandFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
	 	for(String cat:getCategoryKeySet()) {
	 							 //https://digioneapi.com/v1/firesale/products?lang_code=th&status=1&offset=192&limit=12&category=&collection=&search=&include=currency%2Ccurrency&order=count_view%2Cdesc&brand=&filter=&price=&sale=&new_product=&currency=THB&number_format=decimal&chanel=
			String requestURl = "https://digioneapi.com/v1/firesale/products?lang_code=th&status=1&offset={page}&limit=12&category={cate}&collection=&search=&include=currency%2Ccurrency&order=count_view%2Cdesc&brand=&filter=&price=&sale=&new_product=&currency=THB&number_format=decimal&chanel=";
			int page = 0;
			currentCat=cat;
			if("space".equals(cat)) {
				cat = "";
			}
			do {
				haveResult = false;
				String url = requestURl.replace("{cate}",cat);
				url = url.replace("{cate}",cat);
				url = url.replace("{page}",String.valueOf(page*12));

				try {
					feedRequest(url,"UTF-8",true);
				}catch (Exception e) {
					logger.error(e);
				}
				try {
					mockFeed();
				} catch (IOException e) {
					logger.error(e);
				}
				page++;
			}while(haveResult);
			
	 	}
		logger.info("Set Feed Path");
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void mockFeed() throws IOException {

		String feed = BaseConfig.FEED_STORE_PATH + "/"+"dataHolder.json";
	    try (FileReader reader = new FileReader(feed))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
 
            JSONArray list = (JSONArray) obj;
            for(Object x:list) {
            	parseData(((JSONObject)x).toJSONString());
            }
            reader.close();
            //Iterate over employee array 
        } catch (FileNotFoundException e) {
        	logger.error(e);
        } catch (IOException e) {
        	e.printStackTrace();
        } catch (ParseException e) {
        	e.printStackTrace();
        }
	    
	}
	
	private void parseData(String result) throws ParseException {
		JSONObject ob = (JSONObject)new JSONParser().parse(result);
		String name = String.valueOf(ob.get("title"));
		String id = String.valueOf(ob.get("code"));
		String desc = String.valueOf(ob.get("comfort_layer_th"));
		String price = String.valueOf(ob.get("price"));
		String slug = String.valueOf(ob.get("slug"));
		String image = String.valueOf(ob.get("image_c"));
		String url = "https://slumberland.co.th/th/product/"+slug;
		if(StringUtils.isBlank(name)||StringUtils.isBlank(slug)||StringUtils.isBlank(price+"")) {
			logger.info("Data not complete "+id+" "+name+" "+currentCat+" "+slug+" "+price+"");
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		if(StringUtils.isNotBlank(id)) 
			id = " ("+id+")";
		
		pdb.setName("Slumberland  "+name+id);
		if(FilterUtil.convertPriceStr(price)==0) {
			price = BotUtil.CONTACT_PRICE_STR;
		}
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(slug);
		if(desc!=null&&!desc.equals("null")) {
			pdb.setDescription(desc);
		}
		pdb.setPictureUrl(image);
		
		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		mockResult(pdb);
		
	}
	
	private void feedRequest(String url, String charset, boolean redirectEnable) throws InterruptedException {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36");
	 		conn.setRequestProperty("Authorization","REF eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZWYiOiI0NzBVIn0.NUe6W7A9Kqd7VzomcJneliGp6URuy9asDTacF7dAyQM");
	 		conn.setRequestProperty("Referer","https://slumberland.co.th/th/category/special-mattress");
	 		conn.setRequestProperty("Origin","https://slumberland.co.th");
	 		conn.setRequestProperty("Sec-Fetch-Mode","cors");
	 		conn.setRequestProperty("cookie","__cfduid=d310eeb8eb33b597bb2768bbe15c13b9b1567131465; PHPSESSID=r8la9l1ju8au8l0tubnvtfgt36; _ga=GA1.3.584613064.1567131466; _gid=GA1.3.206825261.1567131466; _gat=1; slumberland_panda_development=VPaVJgNimcOa6o%2BC3oNKZst1NY4FTDGUOlj3DQTcS2A7acVPYTEDl%2FQlVhCz4JrmzSIUze7l7dy5loC4xru%2Fd%2BahCP8lJc%2FcnAKYE4wCE2WB%2BgfdYxfk%2FWftsQ5mewN1R35KIvGxdsOkcFBNz53TFKz5g0XmOvyUzQhdz%2BdE1lFcKz0mbB%2BsbMZ8mSSle2D13sv09lYu9Rve9e8x95zTFASLpHv68eJWypJtpxGmNPPUiUL7wc83d1h%2FuwqjfEgO1RuXsscE79pQtBp8%2FvJ5vUJvtyF%2Fqf5VdBI292UB5iOICP%2FJi8iQ968ZLHeTDGjzkKlsVjuDz%2B%2BKYViVMlFuYR7QaZ2b9pOANOCvvG4vkwawPMiMsKml1Ttr%2FWSLhblzoDqhog55bI6rULeg1RKK12DsP0TgmaEp5Htg8EZgCFk%3D; _fbp=fb.2.1567131466135.926907484");
	 		
			
			conn.setConnectTimeout(180000);
			conn.setReadTimeout(180000);
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" +"dataHolder.json").toPath();
			if(conn.getResponseCode() == 200 || conn.getResponseCode() == 204) {
    			try (InputStream is = conn.getInputStream()){
    				Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);	
		    	}catch(IOException e){
		    		logger.error(e);
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				logger.info("Error status : " + conn.getResponseCode() + " - " + url);
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		logger.info("Error "+e.getMessage()+" url: "+url);
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) 			result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) 			result.append("<desc>"+desc+"</desc>");
		if(price!=0) 								result.append("<price>"+price+"</price>");
		if(basePrice!=0) 							result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) 			result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) 			result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(urlForUpdate))	result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(StringUtils.isNotBlank(id)) 				result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) 									result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword))	 		result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) 									result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.info("Line 134 "+e.getMessage());
				}
			}
		}
		return result.toString();
		
	}
	
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
