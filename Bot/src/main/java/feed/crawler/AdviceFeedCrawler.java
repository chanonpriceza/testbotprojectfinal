package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AdviceFeedCrawler extends FeedManager{
	
	public AdviceFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.advice.co.th/home/process/feed_priceza_advice"
		};
	}

	public ProductDataBean parse(String xml) {
		
		String productName 		= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice 	= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDesc 		= FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId 	= FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId 	= FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
		String holderReal = "";
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		pdb.setUrl(productUrl);
		
		if (!productPictureUrl.isEmpty()) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
			holderReal = " ("+realProductId+")";
		}
		
		pdb.setName(productName+holderReal);
		
		if(!productDesc.isEmpty()){
			pdb.setDescription(productDesc);
		}
		
		pdb.setUrlForUpdate(realProductId);

		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 10805));
			pdb.setKeyword(catMap[1]);			
		}else {
			return null;
		}

		return pdb;
		
	}
	
}
