package feed.crawler;

import org.apache.commons.csv.CSVRecord;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class KichaFeedCrawler extends FeedManager {
	
	public KichaFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		String FEED_URL = "http://27.254.82.243:8081/7030.csv";
		String FILE_NAME = "7030.csv";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		FTPUtil.downloadFileWithLogin(FEED_URL,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME, FEED_USER, FEED_PASS);
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME};
		

	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String id = data.get(0);
		String name = data.get(1);
		String desc = data.get(3);
		String price = data.get(2);
		String bprice = data.get(8);
		String url = data.get(6);
		String image = data.get(7);
		String category = data.get(4);
		
		id = FilterUtil.toPlainTextString(id);
		name = FilterUtil.toPlainTextString(name);
		desc = FilterUtil.toPlainTextString(desc);
		price = FilterUtil.toPlainTextString(price);
		bprice = FilterUtil.toPlainTextString(bprice);
		url = FilterUtil.toPlainTextString(url);
		url = FilterUtil.toPlainTextString(url);
		image = FilterUtil.toPlainTextString(image);
		
		double p = FilterUtil.convertPriceStr(price);
		double b = FilterUtil.convertPriceStr(bprice);

		ProductDataBean pdb = new ProductDataBean();
		
		if(p == 0 && b == 0) {
			p = BotUtil.CONTACT_PRICE;
		}else if(p == 0 && b > 0) {
			double t = p;
			p = b;
			b = t;
		}

		
		price = FilterUtil.removeCharNotPrice(price);
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(p);
		pdb.setBasePrice(b);
		pdb.setDescription(desc);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		//pdb.setExpire(true);
		return pdb;
	}
	
	

	
}
