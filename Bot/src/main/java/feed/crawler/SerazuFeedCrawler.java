package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SerazuFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	private int failCount = 0;
	
	public SerazuFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		int page = 1;
		while(!Thread.currentThread().isInterrupted() && failCount < 2){
			String[] response = HTTPUtil.httpRequestWithStatus("https://serazu.com/web/site/rss?page=" + page, "UTF-8", false);
			if(response == null || response.length != 2) {
				failCount++;
				page++;
				continue;
			}

			String data = response[0];
			if(StringUtils.isBlank(data)) {
				failCount++;
				page++;
				continue;
			}
			
			parseData(data);
			page++;
			
			if(page >= 100){
				break;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public void parseData(String data) {
		List<String> products = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
		if(products == null || products.size() == 0) {
			failCount++;
			return;
		}
		
		for (String product : products) {
			String productName 			= FilterUtil.getStringBetween(product, "<name>","</name>").trim();
			String productPrice 		= FilterUtil.getStringBetween(product, "<price>","</price>").trim();
			String productUrl 			= FilterUtil.getStringBetween(product, "<url>","</url>").trim();
			String productPictureUrl 	= FilterUtil.getStringBetween(product, "<image_url>","</image_url>").trim();
			String productCatId 		= FilterUtil.getStringBetween(product, "<category_id>","</category_id>").trim();
			String productDescription 	= FilterUtil.getStringBetween(product, "<description>","</description>").trim();
			String upc			 		= FilterUtil.getStringBetween(product, "<barcode>","</barcode>").trim();
			String productCatName 		= FilterUtil.getStringBetween(product,"<category_name>","</category_name>");
			
			boolean isBook 				= true;
			if(StringUtils.isNotBlank(productCatName)){
				isBook = productCatName.contains("หนังสือ");
			}
			
			if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 ) {
				return;		
			}
			
			productName = StringEscapeUtils.unescapeHtml4(productName);
			productUrl = productUrl.replace("&amp;", "&");
			productPictureUrl = productPictureUrl.replace("&amp;", "&");				
			
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(productName+" ("+upc+")");
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
			
			if (productUrl.length() != 0) {
				pdb.setUrl(productUrl);			
				pdb.setUrlForUpdate(productUrl);
			}
			
			if (productPictureUrl.length() != 0) {
				productPictureUrl = BotUtil.encodeURL(productPictureUrl);
				pdb.setPictureUrl(productPictureUrl);
			}
			
			if(upc.length() != 0 && isBook){
				pdb.setUpc(upc);
			}
			
			String[] catMap = getCategory(productCatId);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);
			}else{
				pdb.setCategoryId(0);
			}
			
			pdb.setDescription(productDescription);
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
			
		}
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
