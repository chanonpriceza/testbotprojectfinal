package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class TemplateCentralFeedCrawler extends FeedManager {
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
	static String full = "{\"operationName\":\"Search\",\"variables\":{\"locale\":\"th\",\"pagination\":{\"size\":50,\"offset\":{page}},\"time\":1588177636399,\"filter\":{\"inFilters\":[{\"filterBy\":\"VISIBILITY\",\"filterValues\":[\"2\",\"4\"]},{\"filterBy\":\"BRAND_NAME\",\"filterValues\":\"{brand}\"},{\"filterBy\":\"CATEGORY\",\"filterValues\":[\"{cat}\"]}]},\"keyword\":null},\"query\":\"query Search($locale: String!, $keyword: String, $sort: SortCS, $pagination: PaginationCS!, $filter: FilterCS) {\\n  search(store: \\\"cds\\\", locale: $locale, keyword: $keyword, pagination: $pagination, filter: $filter, sort: $sort) {\\n    total\\n    products {\\n      id\\n      sku\\n      name\\n      status\\n      visibility\\n      type_id\\n      created_at\\n      updated_at\\n      brand_name\\n      brand_url\\n      image\\n      url_key\\n      is_in_stock\\n      price\\n      special_price\\n      special_from_date\\n      special_to_date\\n      discount_amount\\n      new_tag\\n      only_at_tag\\n      promo_tag\\n      online_exclusive_tag\\n      recommended\\n      best_sellers\\n      collection\\n      min_sale_qty\\n      max_sale_qty\\n      preorder_shipping_date\\n      product_type\\n      flash_deal\\n      flash_deal_qty\\n      flash_deal_sold_qty\\n      flash_deal_from\\n      flash_deal_to\\n      marketplace_product_type_option\\n      marketplace_seller_option\\n      color_group_name\\n      size\\n      overlay {\\n        image\\n        status\\n        mobile_status\\n        start_date\\n        end_date\\n        __typename\\n      }\\n      breadcrumbs {\\n        category_id\\n        level\\n        name\\n        url\\n        __typename\\n      }\\n      categories {\\n        id\\n        name\\n        parent_id\\n        url_path\\n        is_gtm\\n        __typename\\n      }\\n      configurable_product_links\\n      configurable_products {\\n        id\\n        sku\\n        name\\n        status\\n        visibility\\n        type_id\\n        created_at\\n        updated_at\\n        brand_name\\n        brand_url\\n        image\\n        url_key\\n        is_in_stock\\n        price\\n        special_price\\n        special_from_date\\n        special_to_date\\n        discount_amount\\n        new_tag\\n        only_at_tag\\n        promo_tag\\n        online_exclusive_tag\\n        recommended\\n        best_sellers\\n        collection\\n        min_sale_qty\\n        max_sale_qty\\n        preorder_shipping_date\\n        product_type\\n        flash_deal\\n        flash_deal_qty\\n        flash_deal_sold_qty\\n        flash_deal_from\\n        flash_deal_to\\n        marketplace_product_type_option\\n        marketplace_seller_option\\n        size\\n        color_group_name\\n        overlay {\\n          image\\n          status\\n          mobile_status\\n          start_date\\n          end_date\\n          __typename\\n        }\\n        breadcrumbs {\\n          category_id\\n          level\\n          name\\n          url\\n          __typename\\n        }\\n        categories {\\n          id\\n          name\\n          parent_id\\n          url_path\\n          is_gtm\\n          __typename\\n        }\\n        __typename\\n      }\\n      configurable_product_options {\\n        attribute_id\\n        label\\n        values {\\n          value_index\\n          extension_attributes {\\n            frontend_type\\n            frontend_value\\n            label\\n            products\\n            __typename\\n          }\\n          __typename\\n        }\\n        __typename\\n      }\\n      __typename\\n    }\\n    aggregations {\\n      field\\n      value\\n      buckets {\\n        key\\n        doc_count\\n        __typename\\n      }\\n      __typename\\n    }\\n    category_aggregations {\\n      id\\n      name\\n      level\\n      parent_id\\n      url_path\\n      doc_count\\n      url_key\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}";
	private static String brand = "";
	private static String cat = "";
	public TemplateCentralFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			for(String x:getCategoryKeySet()) {
				String param = x ;
				String[] paramArray = param.split(",");
				if(paramArray==null||paramArray.length!=2) {
					logger.error("Param incomplete !!!!!!!!!!!!!!!");
				}else {
					brand = paramArray[0]; 
					cat =   paramArray[1];
				}
				for(int page = 0; page <5;page++) {
					String[] mainPage = httpPostRequestWithStatus("https://coreapi.central.co.th/graphql",full.replace("{page}",page*50+"").replace("{brand}", brand).replace("{cat}",cat),"UTF-8", true, true);
					if(mainPage!=null) {
						JSONObject obj = (JSONObject)new JSONParser().parse(mainPage[0]);
						obj  = (JSONObject) obj.get("data");
						obj  = (JSONObject) obj.get("search");
						JSONArray arr = (JSONArray) obj.get("products");
						if(arr.size()==0)
							break;
						for(Object a:arr) {
							JSONObject o = (JSONObject)a;
							String u = String.valueOf(o.get("url_key"));
							Document  doc = Jsoup.parse(HTTPUtil.httpRequestWithStatus("https://www.central.co.th/th/"+u,"UTF-8",true)[0]);
							mock(doc,x);
						}
					}
				}
			}
		}catch (Exception e) {
			logger.error(e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	

	public void mock(Document doc,String cat) {
		String id = FilterUtil.getStringBetween(doc.toString(),"\"sku\":\"","\"");
		String name = doc.select("a._1FlZa").html();
		String desc =doc.select("meta[property=og:description]").attr("content");
		String price = FilterUtil.getStringBetween(doc.toString(),"\"special_price\":\"","\"");
		String baseprice = FilterUtil.getStringBetween(doc.toString(),"\"price\":",",");
		String url = doc.select("meta[property=og:url]").attr("content");
		String image = doc.select("meta[property=og:image]").attr("content");
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(id);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(baseprice));
		pdb.setDescription(desc);
		pdb.setPictureUrl(image);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		mockResult(pdb);
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(urlForUpdate)) result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public static String[] httpPostRequestWithStatus(String url, String data, String charset, boolean redirectEnable, boolean isJsonRequestParam) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			conn.setRequestProperty("origin", "https://www.central.co.th");
			conn.setRequestProperty("Content-Length", "" + data.length());

			
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write(data.getBytes());
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
			
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
