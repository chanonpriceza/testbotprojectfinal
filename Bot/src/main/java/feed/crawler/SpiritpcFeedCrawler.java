package feed.crawler;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SpiritpcFeedCrawler extends FeedManager {
	
	JSONParser parser = new JSONParser();

	public SpiritpcFeedCrawler() throws Exception {
		super();
		
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://spiritpc.com/api/searchengine/products?engine=priceza"};
	}
	

	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String id = String.valueOf(data.get("id"));
		String name = String.valueOf(data.get("name"));
		String desc = String.valueOf(data.get("description"));
		String price = String.valueOf(data.get("price"));
		String baseprice = String.valueOf(data.get("base_price"));
		String url = String.valueOf(data.get("url"));
		String image = String.valueOf(data.get("image_url"));
		String cat = String.valueOf(data.get("category_id"));
		desc = FilterUtil.toPlainTextString(desc);
		if("null".equals(desc)) {
			desc  = null;
		}
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setBasePrice(FilterUtil.convertPriceStr(baseprice));
		pdb.setPictureUrl("https://www."+image);
		pdb.setUrl("https://www."+url);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}

}
