package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TheToolmartFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public TheToolmartFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		String url = "https://toolmartgo.com/api/front/feed/{current_page}";
		long current_page = 1;
		long all_page = 1;
		
		while(!Thread.currentThread().isInterrupted() && current_page <= all_page) {
			String requestUrl = url;
			requestUrl = requestUrl.replace("{current_page}", String.valueOf(current_page));
			String[] responseMain = HTTPUtil.httpRequestWithStatus(requestUrl, "UTF-8", false);
			if(responseMain == null || responseMain.length != 2) continue;
			
			String jsonStr = responseMain[0];
			if(StringUtils.isBlank(jsonStr)) continue;
			
			if(StringUtils.isNotBlank(jsonStr)) {
				JSONObject jsonObj = null;
				try {
					jsonObj = (JSONObject) new JSONParser().parse(jsonStr);
				} catch (ParseException e) {
					logger.error(e);
				}
				if(!jsonObj.isEmpty()) {
					JSONObject jsonData = (JSONObject) jsonObj.get("data");
					JSONArray jsonArr = (JSONArray) jsonData.get("feed_link");
					
					all_page = (long) jsonData.get("all_page");
					for(Object json : jsonArr) {
						String str = json.toString();
						String[] responseInfo = HTTPUtil.httpRequestWithStatus(str, "UTF-8", false);
						if(responseInfo == null || responseInfo.length != 2) continue;
						
						String productStr = responseInfo[0];
						if(StringUtils.isNotBlank(productStr)) {
							try {
								parseJson(productStr);
							} catch (ParseException e) {
								logger.error(e);
							}							
						}
					}					
				}
			}
			current_page++;
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	 }
	
	private void parseJson(String data) throws  ParseException {
		JSONObject jsonObj = (JSONObject) new JSONParser().parse(data);
		JSONObject jsonData = (JSONObject) jsonObj.get("data");
		JSONObject jsonProduct = (JSONObject) jsonData.get("product");
		
		String productName = (String) jsonProduct.get("product_name");	
		String productPrice = (String) jsonProduct.get("product_price");
		String productBasePrice = (String)jsonProduct.get("product_discount_price");
		String productUrl = (String) jsonData.get("url");
		String productImageUrl = (String) jsonData.get("image");
		String productId = String.valueOf((long)jsonProduct.get("id"));
		String productDesc = (String) jsonProduct.get("product_s_desc");
		String sku = (String) jsonProduct.get("product_sku");
		JSONArray catArr = (JSONArray) jsonProduct.get("category");

		if (StringUtils.isBlank(productName) || StringUtils.isBlank(productPrice) || StringUtils.isBlank(productBasePrice) 
				|| StringUtils.isBlank(productId) || StringUtils.isBlank(productUrl) || catArr.isEmpty()) {
			return;
		}
		ProductDataBean pdb = new ProductDataBean();
		productName = productName.trim() + " (" + productId + ")";
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		pdb.setName(productName);
		if(productBasePrice.equals("0.00")) {
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		} else {
			pdb.setBasePrice(FilterUtil.convertPriceStr(productPrice));
			pdb.setPrice(FilterUtil.convertPriceStr(productBasePrice));
		}
		
		pdb.setRealProductId(productId);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId);
		
		if(StringUtils.isNotBlank(sku)) {
			pdb.setUpc(sku);
		}
		
		if (StringUtils.isNotBlank(productImageUrl)) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		if(StringUtils.isNotBlank(productDesc)) {
			pdb.setDescription(productDesc);
		}
		StringBuilder productCatId = new StringBuilder();
		for(Object cat : catArr) {
			JSONObject obj = (JSONObject) cat;
			if(obj.containsKey("id")) {
				productCatId.append("|" + obj.get("id"));
			}
		}
		productCatId.deleteCharAt(0);
		String[] catMap = getCategory(productCatId.toString());
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		} else {
			pdb.setCategoryId(0);
		}
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
    	
	}
	

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	
}
