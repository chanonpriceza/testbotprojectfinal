package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class IndexLivingMallFeedCrawler extends FeedManager{

	private boolean haveResult;
    private static final Logger logger = LogManager.getRootLogger();
	
	public IndexLivingMallFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				""
		};
	}
	@Override
	public void loadFeed() {
		for (String key : getCategoryKeySet()) {
			
			int page = 1;
			
			String data =  "" ;
			boolean isContinue = true;
			String oldData = "";
			do {
				logger.info(key);
				data = feedRequest("https://www.indexlivingmall.com/active/catalog/item_page/?","page="+page+"&total_page=999&cate_id="+key+"&sort=");
				if(data==null)
					break;
				if(data.equals(oldData)) {
					break;
				}
				oldData = data;
				Document doc = Jsoup.parse(data);
				Elements result = doc.select("div.product-item");
				if(result.size()==0) {
					break;
				}
				for (Element cut : result) {
					parseHTML(cut, key);
				}
				if(result.size()!=32) {
					isContinue=false;
				}
				page++;
				
			}while(!Thread.currentThread().isInterrupted() && !StringUtils.isBlank(data)&&isContinue);
		}
		if (haveResult)
			BaseConfig.FEED_FILE = new String[] { BaseConfig.FEED_STORE_PATH + "/mockFeed.xml" };
	
	}
	protected static String feedRequest(String url,String priceAttr) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;
	 			
	 	
	 	try {

	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setRequestProperty("Content-type","application/x-www-form-urlencoded; charset=UTF-8");
	 		conn.setRequestProperty("User-Agent"," Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36");
	 		conn.setDoOutput(true);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(priceAttr.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					logger.error(e);
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private void parseHTML(Element data,String cat) {
		String name = data.select("p.item-name").attr("productname");
		String price = data.select("p.item-name").attr("productprice");
		String url = data.select("div.product-det").select("a").attr("href");
		String pic = FilterUtil.getStringAfterLastIndex((data.select("div.lists_img")).html(),"src=", (data.select("div.lists_img").select("img")).html());
		pic = FilterUtil.getStringBetween(pic,"\"", "\"");
		String basePrice = data.select("p.old-price").html();
		String id = data.select("p.item-name").attr("productid");
		
		if(StringUtils.isBlank(name)||StringUtils.isBlank(url)||StringUtils.isBlank(price)) {
			return;
		}
		name = FilterUtil.toPlainTextString(name);
		price = FilterUtil.toPlainTextString(price);
		name = FilterUtil.toPlainTextString(name);
		url = FilterUtil.toPlainTextString(url);
		basePrice = FilterUtil.toPlainTextString(basePrice);
		
		price = FilterUtil.removeCharNotPrice(price);
		basePrice= FilterUtil.removeCharNotPrice(basePrice);
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setPictureUrl(pic);
		pdb.setCategoryId(StringUtils.isNumeric(cat)?Integer.parseInt(cat):0);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		mockResult(pdb);

	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
}
