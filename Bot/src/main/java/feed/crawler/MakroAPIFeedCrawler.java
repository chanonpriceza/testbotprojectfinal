package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class MakroAPIFeedCrawler extends FeedManager {
	
	private static final Logger logger = LogManager.getRootLogger();
	private final static String REQUEST_URL = "https://ocs-prod-api.makroclick.com/next-product/public/api/product/search";
	private final static JSONParser jsonParser = new JSONParser();
	private int count = 0;
	private static String  currentCat = "";
	
	boolean haveResult = false;
	
	public MakroAPIFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			for(String cat:getCategoryKeySet()) {
				currentCat = cat;
				int page = 1;
				do {
					try {
						haveResult = false;
						count = 0;
						String response = postRequest(cat,String.valueOf(page));
						if(!response.startsWith("{\"content")) {
							response = response.replace("content", "{\"content");
						}
						JSONObject jsonObject = (JSONObject)jsonParser.parse(response);
						JSONArray  jsonArray = (JSONArray)jsonObject.get("content");
						for(Object obj:jsonArray) {
							parseJSON(obj);	
						}
						logger.info("Cat: "+cat+" Page: "+page+" Count: "+count);
						page ++;
					} catch(Exception e) {
						logger.error(cat, e);
					}
				}while(haveResult);
			}
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		}catch (Exception e) {
			logger.info(e);
		}
	}

	
	protected static String postRequest(String cat,String page) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;
	 	String param = "{\"locale\":\"th_TH\",\"minPrice\":null,\"maxPrice\":null,\"menuId\":"+cat+",\"hierarchies\":[],\"customerType\":\"MKC\",\"page\":"+page+",\"pageSize\":32,\"sorting\":\"SORTING_LAST_UPDATE\",\"reloadPrice\":true}";
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36";
		
	 	try {
	 		u = new URL(REQUEST_URL);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 		conn.setRequestProperty("Content-Type", "application/json; charset=utf8");
	 	    conn.setRequestProperty("Accept", "application/json, text/plain, */*");
	 	    conn.setRequestProperty("User-Agent", userAgent);
	 	    

	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(param.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		int code = conn.getResponseCode() ;
	 		if(code != 200) {
	 			return null;
	 		}
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while ((line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while ((line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					e.printStackTrace();
					//logger.error(e);
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}
	
	private void parseJSON(Object response) {
		
		JSONObject parseJSON = (JSONObject) response;
		String name = String.valueOf(parseJSON.get("productName"));
		String price = String.valueOf(parseJSON.get("inVatSpecialPrice"));
		String basePrice = String.valueOf(parseJSON.get("inVatPrice"));
		String id = String.valueOf(parseJSON.get("productCode"));
		String image = String.valueOf(parseJSON.get("image"));
		String url = "";
		
		if(StringUtils.isBlank(id) || StringUtils.isBlank(price) || StringUtils.isBlank(name) ) {
			logger.info("Data not complete "+id+" "+name);
			return;	
		}
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		url = "https://www.makroclick.com/th/products/"+id;
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setRealProductId(id);
		pdb.setPictureUrl(image);
		pdb.setUrlForUpdate(id);
		pdb.setUrl(url);
		
		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		mockResult(pdb);
		
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
					count++;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
