package feed.crawler;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateShopeeDistributedFeedCrawlerWithId extends TemplateShopeeDistributedFeedCrawler {

	public TemplateShopeeDistributedFeedCrawlerWithId() throws Exception {
		super();
	}
	
	@Override
	protected void parseJSON(String data) throws UnsupportedEncodingException, ClassNotFoundException, SQLException, ParseException {
		parseJSON(data,null,null);
	}
	
	private void parseJSON(String data,String cat,String keyword){
		try{
			
		JSONObject allObj = (JSONObject) new JSONParser().parse(data);
		JSONArray jArr = (JSONArray) allObj.get("items");
		
		for (Object object : jArr) {
			
			JSONObject jObj 		= (JSONObject) object;
			String productId 		= jObj.get("itemid").toString();
			String productName 		= (String) String.valueOf(jObj.get("name"));
			String productPrice 	= (String) String.valueOf(jObj.get("price"));
			String basePrice 		= (String) String.valueOf(jObj.get("price_before_discount"));
			String productImageUrl 	= (String) String.valueOf(jObj.get("image"));
			String productDesc 		= (String) String.valueOf(jObj.get("description")).trim();
			String productUrl 		= "";
			String productNameUrl 	= "";
			
			productName = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(productName).trim());
			productNameUrl = productName.replaceAll(" ",  "-").replaceAll("%", "");
			
			String linkName = productNameUrl + "-i." + shopId + "." + productId;
			productUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
			productImageUrl = productImageUrl.replace("&amp;", "&");
			
			if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(productName)) {
				continue;
			}
			ProductDataBean pdb = new ProductDataBean();

			pdb.setName(productName+"("+productId+")");
			if(StringUtils.isNotBlank(productPrice) && NumberUtils.isCreatable(productPrice)) {
				if(Double.parseDouble(productPrice) != 0) {
					pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)) / 100000);
				}else {
					pdb.setPrice(BotUtil.CONTACT_PRICE);
					error = true;
				}
			}else {
				pdb.setPrice(BotUtil.CONTACT_PRICE);
				error = true;
			}
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(shopId + "." + productId);
			pdb.setRealProductId(productId);

			if (StringUtils.isNotBlank(basePrice)) {
				pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)) / 100000);
			}

			if (StringUtils.isNotBlank(productImageUrl)) {
				pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
			}

			if (StringUtils.isNotBlank(productDesc)&&!"null".equals(productDesc)) {
				productDesc = StringEscapeUtils.unescapeHtml4(productDesc).trim();
				productDesc = FilterUtil.toPlainTextString(productDesc);
				pdb.setDescription(productDesc);
			}
			if(cat==null&&keyword==null){
				pdb.setCategoryId(0);
				productList.add(pdb);
			}else{
				pdb.setCategoryId(Integer.parseInt(cat));
				pdb.setKeyword(keyword);
				productListWithCat.add(pdb);
			}
			nextPage = true;
		}
		}catch (Exception e) {
		}
	}
	
}
