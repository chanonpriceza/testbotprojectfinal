package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;


public class NewbalanceFeedCrawler extends FeedManager	 {

	private static final Logger logger = LogManager.getRootLogger();
	private static boolean haveResult = false;
	private boolean next = true;

	public NewbalanceFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> allKey = getCategoryKeySet();
		if(allKey != null && allKey.size() > 0) {
			for (String linkUrl : allKey) {
				String[] catMap = getCategory(linkUrl);
				int category = 0;
				String keyword = "";
				if(catMap != null){
					category = BotUtil.stringToInt(catMap[0], 0);
					keyword = catMap[1];
				}
				
				int maxDepth = 24;
				int start = 0;
				
				String linkPage = linkUrl;
				String BeforelinkPage = "";
				String AfterlinkPage = "";
				next = true;

				while (next) {
					next = false;
					String ajax = "";
					try {
						if (linkPage.contains("&sz=") && linkPage.contains("&start=")) {
							BeforelinkPage = FilterUtil.getStringBefore(linkPage, "&sz=", "");
							AfterlinkPage = FilterUtil.getStringAfter(linkPage, "&pref", "");
							linkPage = BeforelinkPage + "&sz=" + maxDepth + "&start=" + start + "&pref" + AfterlinkPage;
							String[] httpResult = HTTPUtil.httpRequestWithStatus(linkPage, "UTF-8", false);
							if(httpResult != null && httpResult.length != 2) {
								break;
							} else {
								ajax = httpResult[0];
								if (StringUtils.isNotBlank(ajax)) {
									logger.info("Craw " + linkPage);
									parseAjax(ajax, category, keyword);
									if (ajax.contains("product product-tile")) {
										start = maxDepth;
										maxDepth = maxDepth + 24;
										next = true;
									}

								}
							}
						} else {
							String[] httpResult = HTTPUtil.httpRequestWithStatus(linkPage, "UTF-8", false);
							if(httpResult != null && httpResult.length != 2) {
								break;
							} else {
								ajax = httpResult[0];
								if (StringUtils.isNotBlank(ajax)) {
									logger.info("Craw " + linkPage);
									parseAjax(ajax, category, keyword);
								}
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}

	private void parseAjax(String html, int category, String keyword) throws ParseException {
		//System.out.println(html);
		List<String> urlList = FilterUtil.getAllStringBetween(html,"data-quickview-url=\"", "\"");
		if (urlList != null && urlList.size() > 0) {
			for (String url : urlList) {
				//System.out.println(url);
				url = url.replace("&amp;", "&").trim();
				if (url.length() == 0 || !url.startsWith("http")) {
					continue;
				}
				try {
					String[] result = HTTPUtil.httpRequestWithStatus(url, "UTF-8", true);
					if(result == null || result.length != 2) {
						return;
					}
					
					String productHTML = result[0];
					if (StringUtils.isNotBlank(productHTML)) {
						String name = getProductName(productHTML);
						String price = getProductPrice(productHTML);
 						String pictureUrl = getProductPictureUrl(productHTML);
						String description = getProductDescription(productHTML);
						String realProductId = getRealProductId(productHTML);
						if (name.length() == 0 || price.length() == 0 || url.length() == 0 || !url.startsWith("https") ||realProductId.length()==0) {
							continue;
						}
						
						ProductDataBean pdb = new ProductDataBean();
						pdb.setName(name+" ("+realProductId+")");
						pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
						pdb.setUrl(url);
						pdb.setRealProductId(realProductId);
						pdb.setPictureUrl(pictureUrl);
						pdb.setKeyword(keyword);
						pdb.setCategoryId(category);

						if (description != null) {
							pdb.setDescription(description);
						}

						String mockResult = mockResult(pdb);
						if(StringUtils.isNotBlank(mockResult)) {
							try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
								pwr.println(mockResult);
								haveResult = true;
							}catch(Exception e) {
								logger.error(e);
							}
						}
						
					}
				} catch (Exception e) {
				}
			}
		}
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	public String getProductName(String html) {

		String productName = "";

		if (html.length() >= 1) {
			productName = FilterUtil.getStringBetween(html, "<meta property=\"og:title\" content=\"", "\"");
			if (!StringUtils.contains(productName, "New Balance"))
				productName = "New Balance " + productName;
		}

		return productName;
	}

	public String getProductPrice(String html) {

		String productPrice = "";
		if (html.length() > 0) {
			productPrice = FilterUtil.getStringAfterLastIndex(html,"<script type=\"application/ld+json\">", html);
			productPrice = FilterUtil.getStringBetween(productPrice, "{", "</script>");
			productPrice = FilterUtil.getStringBetween(productPrice, "\"price\":\"", "\"");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if (!productPrice.equals("")) {
				productPrice = Double.toString(Math.round(Double.parseDouble(productPrice)));
			}
			if (productPrice.equals("0")) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return productPrice;
	}

	public String getProductDescription(String html) {

		String productDesc = "";
		if (html.length() > 0) {
			productDesc = FilterUtil.getStringBetween(html, "<meta property=\"og:description\" content=\"", "\"");
		}

		return productDesc;
	}

	public String getProductPictureUrl(String html) {

		String productPictureUrl = "";
		if (html.length() > 0) {
			productPictureUrl = FilterUtil.getStringBetween(html, "meta name=\"twitter:image\" content=\"", "\"");
		}

		return productPictureUrl;
	}
	
	public String getRealProductId(String html) {

		String realProductId = "";
		if (html.length() > 0) {
			realProductId = FilterUtil.getStringAfterLastIndex(html,"<script type=\"application/ld+json\">", html);
			realProductId = FilterUtil.getStringBetween(realProductId, "{", "</script>");
			realProductId = FilterUtil.getStringBetween(realProductId, "\"productID\":\"", "\"");
			realProductId = FilterUtil.removeCharNotPrice(realProductId);
		}

		return realProductId;
	}
	
	public String getColor(String html) {
		String color = "";
		if (html.length() > 0) {
			color = FilterUtil.getStringBetween(html,"data-currentvariant=\"{&quot;color&quot;:&quot;","&");
		}
		return color;
	}
	
}
