package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TopsShopOnlineFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private int page = 1;
	private boolean next = true;
	private boolean haveResult = false;
	
	public TopsShopOnlineFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> cat_id = getCategoryKeySet();
		for (String CatId : cat_id) 
		{
			page = 1;
			next = true;
			while(!Thread.currentThread().isInterrupted() && next){
				String url = "https://www.tops.co.th/api/products?field=category.category_id%2C"+CatId +"%2Ceq&filters=%7B%7D&page_number="+page+"&page_size=20&sort=name%2Casc";
				String data = jsonRequest("GET",url,"","UTF-8",true);
				if(StringUtils.isNotBlank(data)) {
					try {
						parseJson(data,CatId);
						page++;

					} catch (ParseException e) {
						logger.error(e);
					} catch (UnsupportedEncodingException e) {
						logger.error(e);
					}
				}else{
					next = false;
				}
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	 public static String jsonRequest(String method, String url, String data, String charset, boolean redirectEnable) {

		  URL u = null;
		  HttpURLConnection conn = null;

		  try {

		   u = new URL(url);
		   conn = (HttpURLConnection) u.openConnection();
		   conn.setRequestMethod(method);
		   conn.setDoInput(true);
		   conn.setDoOutput(true);
		   conn.setUseCaches(false);
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
		   conn.addRequestProperty("x-website-id","9");
		   conn.addRequestProperty("x-store-code","tops_sa_432_th");
		   conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
		   conn.addRequestProperty("Referer", "https://www.tops.co.th/th/");
		   conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
		   conn.setInstanceFollowRedirects(redirectEnable);

		   StringBuilder rtn = new StringBuilder(1000);
		   try (InputStream is = conn.getInputStream(); InputStreamReader isr = new InputStreamReader(is, "UTF-8"); BufferedReader brd = new BufferedReader(isr);) {
				String line = "";
				while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
					rtn.append(line);
					return rtn.toString();
			}
			} catch (IOException e) {
				BotUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}

		  } catch (MalformedURLException e) {
			  logger.error(e);
		  } catch (IOException e) {
			  logger.error(e);
		  } 

		  return null;
		 }
	 
	private void parseJson(String data , String cat_id) throws UnsupportedEncodingException, ParseException{
		JSONObject jData = (JSONObject)new JSONParser().parse(data);
		Object result = jData.get("products");
		if(result == null){
			next = false;
			return;
		}	
		JSONObject jResult = (JSONObject)new JSONParser().parse(result.toString());
		JSONArray jarr = (JSONArray)jResult.get("items");
		if(jarr == null || jarr.size() == 0){
			next = false;
			return;
		}
		
		for(Object json : jarr)
		{	
			JSONObject jObj = (JSONObject)json;
			String productName 		= (String)jObj.get("name").toString();
			String productPrice 	= (String)jObj.get("price").toString();
			String productBasePrice = "";
			if(jObj.get("special_price")!=null){
				 productBasePrice = (String)jObj.get("special_price").toString();					
			}
			String productImageUrl = "";
			if(jObj.get("image")!=null){
				productImageUrl = (String)jObj.get("image").toString();					
			} 
			String productUrl 		= (String)jObj.get("url").toString();
			String productId 		= (String)jObj.get("sku").toString();
			
			JSONArray productCatIdJson 	= (JSONArray)jObj.get("category_ids");

			productUrl = productUrl.trim();
			
			if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 ) {
				return;			
			}
					
			ProductDataBean pdb = new ProductDataBean();

			productUrl = "https://www.tops.co.th/th"+productUrl;
			productImageUrl = "https://d2t5tgzzxhb6v2.cloudfront.net/media//catalog/product"+productImageUrl;

			String productCatId ="";
			
			for (Object object : productCatIdJson) {
				if(object.toString().equals(cat_id)){
					productCatId = object.toString();
				}	
			}
			
			String[] catMap = getCategory(productCatId);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}else{
				pdb.setCategoryId(0);
			}
			pdb.setName(productName);	
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));			
			pdb.setBasePrice(FilterUtil.convertPriceStr(productBasePrice));
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);
			pdb.setRealProductId(productId);
			
			if (productImageUrl.length() != 0) {
				pdb.setPictureUrl(productImageUrl);
			}
			
			String mockData = mockResult(pdb);
			if(StringUtils.isNotBlank(mockData)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(mockData);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
