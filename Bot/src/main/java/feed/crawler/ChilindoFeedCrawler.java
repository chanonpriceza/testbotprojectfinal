package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;

import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChilindoFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public ChilindoFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		boolean haveResult = false;
		int page = 1;
		while(!Thread.currentThread().isInterrupted()){
			String REQUEST_URL = "https://beta.chilindo.com/th/priceza/?page="+page;
			
			String[] response = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
			if(response == null || response.length != 2) break;
			
			String data = response[0];
			if (StringUtils.isBlank(data)) { 
				break;
			}
			
			List<String> pdList = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
			if(pdList != null && pdList.size() > 0){
				for (String pd : pdList) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println("<product>"+pd+"</product>");
					}catch(Exception e) {
						logger.error(e);
					}
				}
				haveResult = true;
			}else {
				break;
			}
			page++;
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	public ProductDataBean parse(String data) {

		String productId = FilterUtil.getStringBetween			(data, "<id>", "</id>").trim();
		String productName = FilterUtil.getStringBetween		(data, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween		(data, "<price>", "</price>").trim();
		String productDescription = FilterUtil.getStringBetween	(data, "<description>", "</description>").trim();
//		String productCatId = FilterUtil.getStringBetween		(data, "<category_id>", "</category_id>").trim();
		String productCatName = FilterUtil.getStringBetween		(data, "<category_name>", "</category_name>").trim();
		String productUrl = FilterUtil.getStringBetween			(data, "<url>", "</url>").trim();
		String productPictureUrl = FilterUtil.getStringBetween	(data, "<image_url>", "</image_url>").trim();
//		String productKeyword = FilterUtil.getStringBetween		(data, "<keyword>", "</keyword>").trim();
		
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}

		if(StringUtils.isNotBlank(productCatName)) {
			productCatName = StringEscapeUtils.unescapeHtml4(productCatName);
			productCatName = FilterUtil.getStringBefore(productCatName, ">", productCatName);
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productName = FilterUtil.toPlainTextString(productName);
		productName = productName + " ("+ productId +")";
		
		productUrl = productUrl.replace("&amp;", "&");
		
		productPictureUrl = productPictureUrl.replace("&amp;", "&");				
		productPictureUrl = productPictureUrl.replace("http:", "https:");
		
		pdb.setName(productName);
		pdb.setPrice(BotUtil.BID_PRICE);		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrl);
		pdb.setRealProductId(productId);
		
		if (!productPictureUrl.isEmpty()) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!productDescription.isEmpty()){
			pdb.setDescription(productDescription);
		}
		
		if("05-125".equals(productId.trim())) {
			logger.info("Delete!!!!!!!!!");
			return null;
		}
		
		String[] catMap = getCategory(productCatName.trim());
		
		if("NON SALES ITEM".equals(productCatName.trim())) 
			return null;
		
		if(catMap != null&&!"NON SALES ITEM".equals(productCatName.trim())) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			logger.info(productCatName+" "+productId);
			pdb.setCategoryId(0);
		}
		
		return pdb;
		
	}
	
}
