package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class LazadaFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private Set<String> skuListSet = null;
	private Set<String> catLocalSet = null;
	private Set<String> catSpecificSet = null;
	private Set<String> catFilterPrice3000Set = null;
	private Set<String> catFilterPrice5000Set = null;
	private Set<String> nonCategoryMappingSet = null;
	private Set<String> nonCrawlCategorySet = null;
	private Set<String> blockSku = null;
	private Set<String> blockSeller = null;
	private Map<String, Boolean> blockCat = null;
	private Map<String, Object[]> catCountMap = null;
	
	private String[] HEADER;
	private String HEADER_LINE;
	private int HEADER_SIZE;
	private long CRAWL_COUNT;
	private long CRAWL_LIMIT;
	private long CRAWL_SKU, CRAWL_LOCAL, CRAWL_LAZMALL, CRAWL_SPECIFIC, CRAWL_FILTER_PRICE_3000, CRAWL_FILTER_PRICE_5000;
	private long countLocal, countCrossBorder;
	
	private long countRecordContentError=0;
	private long countRecordNotCrawl=0;
	
	public LazadaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://52.74.190.12/th/priceza_main_th_marketing_feed.csv.gz",
				"http://52.74.190.12/th/priceza_lazmall_th_marketing_feed.csv.gz"
		};
	}

	@Override
	public void loadFeed() throws RuntimeException {

		setConfigByFile();
		setBlockCategory();
		setBlockSeller();
		
		nonCategoryMappingSet = new HashSet<>();
		nonCrawlCategorySet = new HashSet<>();
		catCountMap = new HashMap<>();
		
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			File file = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName);
			Path path = file.toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error(e);
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
			
			try {
				long fileSize = file.length();
				if(feed.contains("priceza_main_th_marketing_feed") && fileSize < (14 * 1004857600)) { //1004857600 bytes = 1 GB.
					logger.error("File smaller than expect. Only " + fileSize + " byte, stop program.");
					throw new RuntimeException("File smaller than expect. Only " + fileSize + " byte, stop program.");
				}
				if(!checkHeader(file)) {
					logger.error("Header Error, stop program.");
					throw new RuntimeException("Header Error, stop program.");
				}
			} catch(RuntimeException e) {
				throw e;
			} catch(Exception e) {
				logger.error(e);
			}
		}
		
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			
			CRAWL_LIMIT = 29000000;
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_CRAWLER_PARAM) && NumberUtils.isCreatable(BaseConfig.CONF_FEED_CRAWLER_PARAM))
				CRAWL_LIMIT = NumberUtils.toLong(BaseConfig.CONF_FEED_CRAWLER_PARAM);

			String skuFile = BaseConfig.FEED_STORE_PATH + "/crawl_skuFile.csv";
			String lazMallFile = BaseConfig.FEED_STORE_PATH + "/crawl_lazMallFile.csv";
			String catLocalFile = BaseConfig.FEED_STORE_PATH + "/crawl_catLocalFile.csv";
			String catSpecificFile = BaseConfig.FEED_STORE_PATH + "/crawl_catSpecificFile.csv";
			String catFilterPrice3000File = BaseConfig.FEED_STORE_PATH + "/crawl_catFilterPrice3000File.csv";
			String catFilterPrice5000File = BaseConfig.FEED_STORE_PATH + "/crawl_catFilterPrice5000File.csv";
			String nonCategoryFile = BaseConfig.FEED_STORE_PATH + "/list_nonCategoryFile.csv";
			String nonCrawlCategoryFile = BaseConfig.FEED_STORE_PATH + "/list_nonCrawlCategoryFile.csv";
			String mapCategoryDetailFile = BaseConfig.FEED_STORE_PATH + "/list_mapCategoryDetailFile.csv";
			
			try (
					PrintWriter pwr_sku = new PrintWriter(new FileOutputStream(new File(skuFile),  true));
					PrintWriter pwr_local = new PrintWriter(new FileOutputStream(new File(catLocalFile),  true));
					PrintWriter pwr_lazMall = new PrintWriter(new FileOutputStream(new File(lazMallFile),  true));
					PrintWriter pwr_specific = new PrintWriter(new FileOutputStream(new File(catSpecificFile),  true));
					PrintWriter pwr_filterPrice3000 = new PrintWriter(new FileOutputStream(new File(catFilterPrice3000File),  true));
					PrintWriter pwr_filterPrice5000 = new PrintWriter(new FileOutputStream(new File(catFilterPrice5000File),  true));
					PrintWriter pwr_noncat = new PrintWriter(new FileOutputStream(new File(nonCategoryFile),  true));
					PrintWriter pwr_catNotCrawl = new PrintWriter(new FileOutputStream(new File(nonCrawlCategoryFile),  true));
					PrintWriter pwr_catCount = new PrintWriter(new FileOutputStream(new File(mapCategoryDetailFile),  true));
				){
				
				pwr_sku.println(HEADER_LINE);
				pwr_local.println(HEADER_LINE);
				pwr_lazMall.println(HEADER_LINE);
				pwr_specific.println(HEADER_LINE);
				pwr_filterPrice3000.println(HEADER_LINE);
				pwr_filterPrice5000.println(HEADER_LINE);
				pwr_catCount.println("LazadaCatLv1|LazadaCatLv2|LazadaCatLv3|LazadaCatLv4|LazadaCatLv5^countProduct^isPatternMap^isSpecificMap^isLocalMap^isFilterPrice3000Map^isFilterPrice5000Map");
				
				Map<String, PrintWriter> writerMap = new HashMap<>();
				writerMap.put("sku", pwr_sku);
				writerMap.put("local", pwr_local);
				writerMap.put("lazMall", pwr_lazMall);
				writerMap.put("specific", pwr_specific);
				writerMap.put("filterPrice3000", pwr_filterPrice3000);
				writerMap.put("filterPrice5000", pwr_filterPrice5000);
				
				CRAWL_COUNT = 0;
				CRAWL_SKU = 0; 
				CRAWL_LOCAL = 0; 
				CRAWL_LAZMALL = 0; 
				CRAWL_SPECIFIC = 0; 
				CRAWL_FILTER_PRICE_3000 = 0; 
				CRAWL_FILTER_PRICE_5000 = 0;
				
				long lineCount = 0;
				boolean firstLine = true;
				for (String feed : BaseConfig.FEED_FILE) {
					String fileName = BotUtil.getFileNameURL(feed);
					try(BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(BaseConfig.FEED_STORE_PATH + "/" + fileName)), "UTF-8"))){
						String line = null;
						while ((line = br.readLine()) != null) { 
							lineCount++;
							if(lineCount % 1000000 == 0)
								logger.info("verifyByFilterWCE " + lineCount + ", error " + countRecordContentError + ", skip " + countRecordNotCrawl);
							if(firstLine) {
								firstLine = false;
								continue;
							}
							verifyByFilterWCE(line, writerMap, feed);
							
							if(Thread.currentThread().isInterrupted())
								throw new InterruptedException();
						}
					}catch(InterruptedException e) {
						throw e;
					}catch(Exception e) {
						logger.error(e);
					}
				}
				
				logger.info("all lineCount = "        + lineCount);
				logger.info("all crawlCount = "       + CRAWL_COUNT);
				logger.info("crawlSKU = "             + CRAWL_SKU);
				logger.info("crawlLocal = "           + CRAWL_LOCAL);
				logger.info("crawlLazMall = "         + CRAWL_LAZMALL);
				logger.info("crawlSpecific = "        + CRAWL_SPECIFIC);
				logger.info("crawlFilterPrice3000 = " + CRAWL_FILTER_PRICE_3000);
				logger.info("crawlFilterPrice5000 = " + CRAWL_FILTER_PRICE_5000);
				
				logger.info("countLocal = "           + countLocal);
				logger.info("countCrossBorder = "     + countCrossBorder);
				
				logger.info("countRecordContentError = " + countRecordContentError);
				logger.info("countRecordNotCrawl = "  + countRecordNotCrawl);
				
				if(nonCategoryMappingSet != null && nonCategoryMappingSet.size() > 0)
					for (String cat : nonCategoryMappingSet) 
						pwr_noncat.println(cat);
				
				if(nonCrawlCategorySet != null && nonCrawlCategorySet.size() > 0)
					for (String cat : nonCrawlCategorySet) 
						pwr_catNotCrawl.println(cat);
				
				if(catCountMap != null && catCountMap.size() > 0) {
					for(Entry<String, Object[]> catMapDetail : catCountMap.entrySet()) {
						pwr_catCount.println(catMapDetail.getKey() + "^" + catMapDetail.getValue()[0] + "^" + catMapDetail.getValue()[1] + "^" + catMapDetail.getValue()[2] + "^" + catMapDetail.getValue()[3] + "^" + catMapDetail.getValue()[4] + "^" + catMapDetail.getValue()[5]);
					}
				}
				
				if(skuListSet != null && skuListSet.size() > 0)
					try(PrintWriter pwr_remain_sku = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "lazada-remain-sku.csv"),  true));){
						for (String cat : skuListSet) 
							pwr_remain_sku.println(cat);
					}
			
				List<String> finalCrawlList = new ArrayList<>();
				if(CRAWL_SKU > 0) finalCrawlList.add(skuFile);
				if(CRAWL_LOCAL > 0) finalCrawlList.add(catLocalFile);
				if(CRAWL_LAZMALL > 0) finalCrawlList.add(lazMallFile);
				if(CRAWL_SPECIFIC > 0) finalCrawlList.add(catSpecificFile);
				if(CRAWL_FILTER_PRICE_3000 > 0) finalCrawlList.add(catFilterPrice3000File);
				if(CRAWL_FILTER_PRICE_5000 > 0) finalCrawlList.add(catFilterPrice5000File);
				BaseConfig.FEED_FILE = (String[]) finalCrawlList.toArray(new String[finalCrawlList.size()]);
				Arrays.asList(BaseConfig.FEED_FILE).stream().forEach(s -> logger.info("Summary File to crawl : " + s));
			} catch(Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	private void setConfigByFile() {
		String FEED_URL = "";
		String FILE_NAME = "";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		
		//sku file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_sku.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_skuFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		skuListSet = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0});
		
		//cat local file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_nocb.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_catLocalFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catLocalSet = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2});
		
		//cat specific file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_specific_category.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_catSpecificFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catSpecificSet = getDataFromOfflineFile(FILE_NAME, "\\|", new Integer[] {0,1,2,3,4});

		//cat filter price 3000 file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_filter_price_3000.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_catFilterPrice3000.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catFilterPrice3000Set = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2});

		//cat filter price 5000 file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_filter_price_5000.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_catFilterPrice5000.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catFilterPrice5000Set = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2});
		
		//block sku
		FEED_URL = "http://27.254.82.243:8081/314_block_sku.csv";
		FILE_NAME = BaseConfig.FEED_STORE_PATH + "/config_block_sku.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		blockSku = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0});
		
	}
	
	private void setBlockCategory() {
		blockCat = new HashMap<String, Boolean>();
		blockCat.put("Health & Beauty|Sexual Wellness|Sensual Toys", false);
		blockCat.put("Health & Beauty|Sexual Wellness|Condoms", false);
		blockCat.put("Health & Beauty|Sexual Wellness|Lubricants", false);
		blockCat.put("Mobiles & Tablets|Mobile Accessories|Phone Cases", true);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test normal Product", false);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test sample", false);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test service", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test normal Product", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test sample", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test service", false);
	}
	
	private void setBlockSeller() {
		blockSeller = new HashSet<>();
		blockSeller.add("MusicMove");
	}
	
	private LinkedHashSet<String> getDataFromOfflineFile(String file, String delimiter, Integer[] index){
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))){
			LinkedHashSet<String> resultSet = new LinkedHashSet<>();
			String line = null;
			int duplicateCount = 0;
			while ((line = br.readLine()) != null) { 
					String[] data = line.split(delimiter);
					String result = "";
					for (Integer i : index) {
						if(i >= data.length) 
							continue;
						
						result += "|";
						result += data[i];
					}
					if(StringUtils.isNotBlank(result)) {
						result = result.substring(1);
						if(resultSet.contains(result)) {
							duplicateCount++;
						}else {
							resultSet.add(result);
						}
					}
				if(Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
			}
			logger.info(file + " done, " + resultSet.size() + " records, " + duplicateCount + " duplicated.");
			return resultSet;
		}catch(Exception e) {
			logger.error("Error from getDataFromOfflineFile : " + file + " -> " + e);
		}
		return new LinkedHashSet<>();
	}
	
	@SuppressWarnings("resource")
	private void verifyByFilterWCE(String line, Map<String, PrintWriter> writerMap, String feedName) {
		try {
			StringReader lineReader = new StringReader(line);
			CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL.withHeader(HEADER));
			for (CSVRecord csvRecord : record) {
				
				if (csvRecord == null || csvRecord.size() != HEADER_SIZE) {
					countRecordContentError++;
					continue;
				}
					
				String id = csvRecord.get("simple_sku");
				String cat1 = csvRecord.get("level_1");
				String cat2 = csvRecord.get("level_2");
				String cat3 = csvRecord.get("level_3");
				String cat4 = csvRecord.get("level_4");
				String cat5 = csvRecord.get("level_5");
				String oldSku = StringUtils.defaultIfBlank(csvRecord.get("old_sku_id"), "");
				String shippingType = StringUtils.defaultIfBlank(csvRecord.get("cross_border"), "");
				String discountPrice = StringUtils.defaultIfBlank(csvRecord.get("discounted_price"), "0");
				double price = (NumberUtils.isCreatable(discountPrice))? Double.parseDouble(discountPrice) : 0;
				
				String allCat = cat1;
				if(StringUtils.isNotBlank(cat2)) allCat += "|" + cat2;
				if(StringUtils.isNotBlank(cat3)) allCat += "|" + cat3;
				if(StringUtils.isNotBlank(cat4)) allCat += "|" + cat4;
				if(StringUtils.isNotBlank(cat5)) allCat += "|" + cat5;
				allCat = allCat.replace("\"", "");
				
				String catLevel3 = cat1;
				if(StringUtils.isNotBlank(cat2)) catLevel3 += "|" + cat2;
				if(StringUtils.isNotBlank(cat3)) catLevel3 += "|" + cat3;
				
				verifyCat(allCat, catLevel3);
				
				if(shippingType.equals("0")) {
					countLocal++;
				} else {
					countCrossBorder++;
				}
				
				if(getCategory(allCat) == null)
					if(!nonCategoryMappingSet.contains(allCat))
						nonCategoryMappingSet.add(allCat);
				
				Boolean typeBlock = blockCat.get(allCat);
				if (allCat.contains("SellerCenter") || (typeBlock != null && (!typeBlock || (typeBlock && shippingType.equals("1"))))) {
					countRecordNotCrawl++;
					return;
				}
				
				if(blockSku.contains(oldSku)) {
					countRecordNotCrawl++;
					return;
				}
				
				String crawlType = "";
				if(skuListSet.contains(id)) {
					skuListSet.remove(id);
					crawlType = "sku";
					CRAWL_SKU++;
				} else if(feedName.contains("priceza_lazmall_th")) {
					crawlType = "lazMall";
					CRAWL_LAZMALL++;
				} else if((catFilterPrice5000Set.contains(allCat) || catFilterPrice5000Set.contains(catLevel3)) && price >= 5000) {
					crawlType = "filterPrice5000";
					CRAWL_FILTER_PRICE_5000++;
				} else if((catFilterPrice3000Set.contains(allCat) || catFilterPrice3000Set.contains(catLevel3)) && price >= 3000) {
					crawlType = "filterPrice3000";
					CRAWL_FILTER_PRICE_3000++;
				} else if((catLocalSet.contains(allCat) || catLocalSet.contains(catLevel3)) && shippingType.equals("0")) {
					crawlType = "local";
					CRAWL_LOCAL++;
				} else if(catSpecificSet.contains(allCat) && (CRAWL_COUNT < CRAWL_LIMIT)) {
					crawlType = "specific";
					CRAWL_SPECIFIC++;
				} else {
					if(!nonCrawlCategorySet.contains(allCat))
						nonCrawlCategorySet.add(allCat);
				}
				
				if(StringUtils.isNotBlank(crawlType)) {
					writeLineToFile(line, crawlType, writerMap);
					CRAWL_COUNT++;
				} else {
					countRecordNotCrawl++;
				}
			}
		}catch(IllegalStateException | IllegalArgumentException e) {
			countRecordContentError++;
		}catch(Exception e) {
			logger.error(e + "\n" + line);
		}
	}
	
	private void verifyCat(String allCat, String catLevel3) {
		Object[] catMapDetail = catCountMap.getOrDefault(allCat, new Object[] {(long) 0, (long) 0, false, false, false, false, false});
		long countProduct = (long) catMapDetail[0];
		long catPatternMap = (long) catMapDetail[1];
		boolean isSpecificMap = (boolean) catMapDetail[2];
		boolean isLocalMap = (boolean) catMapDetail[3];
		boolean isFilterPrice3000Map = (boolean) catMapDetail[4];
		boolean isFilterPrice5000Map = (boolean) catMapDetail[5];
		
		countProduct++;
		
		//check isPatternMap all level
		String[] catMap = getCategory(allCat);
		if (catMap != null) 
			catPatternMap = Long.parseLong(catMap[0]);
		
		//check isSpecificMap all level
		if (catSpecificSet.contains(allCat)) 
			isSpecificMap = true;
		
		//check isLocalMap only level 3
		if (catLocalSet.contains(catLevel3)) 
			isLocalMap = true;
		
		//check isFilterPrice3000Map only level 3
		if (catFilterPrice3000Set.contains(catLevel3)) 
			isFilterPrice3000Map = true;
		
		//check isFilterPrice5000Map only level 3
		if (catFilterPrice5000Set.contains(catLevel3)) 
			isFilterPrice5000Map = true;
		
		catMapDetail = new Object[] {countProduct, catPatternMap, isSpecificMap, isLocalMap, isFilterPrice3000Map, isFilterPrice5000Map};
		catCountMap.put(allCat, catMapDetail);
		
	}
	
	private void writeLineToFile(String line, String crawlType, Map<String, PrintWriter> writerMap) {
		PrintWriter pw = writerMap.get(crawlType);
		if(pw != null) {
			line = editDataLine(crawlType, line);
			pw.println(line);
		}
	}
	
	private boolean checkHeader(File file) throws Exception {
		try(FileInputStream fis = new FileInputStream(file);
				GZIPInputStream gzis = new GZIPInputStream(fis);
				InputStreamReader isr = new InputStreamReader(gzis, "UTF-8");
				BufferedReader br = new BufferedReader(isr)){
			
			String firstLine = br.readLine();
			if(firstLine != null) {
				HEADER = firstLine.split(",");
				HEADER_LINE = firstLine;
				HEADER_SIZE = HEADER.length;
				return true;
			}
		} catch(Exception e) {
			logger.error(e);
		}
		return false;
	}
	
	private String editDataLine(String crawlType, String line) {
		if(crawlType.equals("lazMall")) {
			line = line.replaceFirst("\"\"\",", " LazMall\"\"\",");
			if(!line.contains("LazMall")) line = line.replaceFirst("\"\",", " LazMall\"\",");
			if(!line.contains("LazMall")) line = line.replaceFirst("\",", " LazMall\",");
			if(!line.contains("LazMall")) line = line.replaceFirst(",", " LazMall,");
		}
		return line;
	}
	
	public ProductDataBean parse(CSVRecord data) {
		
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			if((ReportManager.getInstance().getReport().getCountOld() + ReportManager.getInstance().getReport().getAdd()) > CRAWL_LIMIT) {
				return null;
			}
		}
		
		if (data == null || data.size() != HEADER_SIZE)
			return null;
		
		String id = data.get("simple_sku");
		String pdName = data.get("product_name");
		String imageUrl = data.get("picture_url");
		String price = data.get("sale_price");
		String discountPrice = data.get("discounted_price");
		String cat1 = data.get("level_1");
		String cat2 = data.get("level_2");
		String cat3 = data.get("level_3");
		String cat4 = data.get("level_4");
		String cat5 = data.get("level_5");
		String brand = data.get("brand");
		String appLink = data.get("deeplink");
		String shippingType = data.get("cross_border");
		String availability = data.get("availability");
		String oldSku = StringUtils.defaultIfBlank(data.get("old_sku_id"), "");
		String sellerName = StringUtils.defaultIfBlank(data.get("seller_name"), "");
		
		if(availability.equals("0")) {
			return null;
		}
		
		if(StringUtils.isBlank(appLink)
				|| !appLink.startsWith("http") 
				|| StringUtils.isBlank(pdName) 
				|| StringUtils.isBlank(id) 
				|| StringUtils.isBlank(cat1)
				|| (StringUtils.isBlank(discountPrice) && StringUtils.isBlank(price))){
			return null;
		}
		
		if(pdName.contains("�")){
			return null;
		}
		
		imageUrl = imageUrl.replace("&amp;", "&");
		
		String productCat = cat1;
		if(StringUtils.isNotBlank(cat2)) productCat += "|" + cat2;
		if(StringUtils.isNotBlank(cat3)) productCat += "|" + cat3;
		if(StringUtils.isNotBlank(cat4)) productCat += "|" + cat4;
		if(StringUtils.isNotBlank(cat5)) productCat += "|" + cat5;
		productCat = productCat.replace("\"", "");
		
		Boolean typeBlock = blockCat.get(productCat);
		if (productCat.contains("SellerCenter") || (typeBlock != null && (!typeBlock || (typeBlock && shippingType.equals("1"))))){
			return null;
		}
		
		if(blockSku.contains(oldSku)) {
			return null;
		}
		
		if(blockSeller.contains(sellerName)) {
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		String[] catMap = getCategory(productCat);
		if (catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}
		
		String name = pdName + " (" + id + ")";
		pdb.setName(BotUtil.limitString(name, 200));
		pdb.setUrlForUpdate(id);
		pdb.setUrl(appLink);
		pdb.setRealProductId(id);
		
		if (!discountPrice.isEmpty()) {
			double curPrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(discountPrice));
			if(curPrice == 0){
				return null;
			}else{
				pdb.setPrice(curPrice);
			}
		}
		
		if (!price.isEmpty()) {
			double curBasePrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
			if(curBasePrice != 0 && curBasePrice > pdb.getPrice()){
				pdb.setBasePrice(curBasePrice);
			}
		}
		
		if (!imageUrl.isEmpty() && imageUrl.startsWith("http") && BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			pdb.setPictureUrl(imageUrl);
		}

		String description = shippingType.equals("1") ? "Shipping:Import" : "Shipping:Local";
		description += " " + name;
		if (StringUtils.isNotBlank(brand)) {
			description = description + " แบรนด์  " + brand;
		}

		if (StringUtils.isNotBlank(cat1)) {
			description = description + " ประเภท " + cat1;
		}
		if (StringUtils.isNotBlank(cat2)) {
			if (StringUtils.isNotBlank(cat1)) {
				description = description + " " + cat2;
			} else {
				description = description + " ประเภท  " + cat2;
			}
		}
		
		pdb.setDescription(description);
		
		return pdb;
	}
	
}
