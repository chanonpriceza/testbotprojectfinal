package feed.crawler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class StubhubFeedCrawler extends FeedManager {

	private static final Logger logger = LogManager.getRootLogger();
	
	public StubhubFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"http://pf.tradedoubler.com/export/export?myFeed=14721050662854334&myFormat=1472105066285433"};
	}
	
	public ProductDataBean parse(String xml) {
		
		try {
			String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();		
			String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
			String productUrl = FilterUtil.getStringBetween(xml, "<productUrl>", "</productUrl>").trim();
			String productImageUrl = FilterUtil.getStringBetween(xml, "<imageUrl>", "</imageUrl>");
			String productCatId = FilterUtil.getStringBetween(xml, "<TDCategoryID>", "</TDCategoryID>");
			String productCatName = FilterUtil.getStringBetween(xml, "<TDCategoryName>", "</TDCategoryName>");
			String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
			String productDate = FilterUtil.getStringBetween(xml, "<date>", "</date>").trim();	
			String productCountry = FilterUtil.getStringBetween(xml, "<country>", "</country>").trim();	
			String productVenue = FilterUtil.getStringBetween(xml, "<venue>", "</venue>").trim();	
			String productCity = FilterUtil.getStringBetween(xml, "<city>", "</city>").trim();	
			
			if(StringUtils.isBlank(productName) && StringUtils.isBlank(productUrl)) {
				return null;			
			}
			
			if(StringUtils.isBlank(productPrice) || Double.parseDouble(productPrice ) == 0){
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
					
			ProductDataBean pdb = new ProductDataBean();
			
			if(StringUtils.isNotBlank(productDate)){
				SimpleDateFormat  sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				Date datetime = sf.parse(productDate);
				sf = new SimpleDateFormat("EEEEที่ d MMMM yyyy เวลา HH:mm น.", new Locale ( "th", "TH" ));
				productDate = sf.format(datetime);
		    }
		
			productName = "บัตร "+ productName +" "+productDate+" "+productCountry;
			pdb.setName(StringEscapeUtils.unescapeHtml4(productName));
			pdb.setPrice(Math.round(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice))));
			
			if (productDesc.length() != 0) {
				String newDesc = "";
				newDesc = productCatName +" "+productVenue+" "+" "+productCity+" "+ " "+productCountry+ " "+ productDesc;
				pdb.setDescription(StringEscapeUtils.unescapeHtml4(newDesc));
			}
			
			if (productImageUrl.length() != 0) {
				pdb.setPictureUrl(productImageUrl);
			}
			if (productUrl.length() != 0) {					
				pdb.setUrl(productUrl);
				pdb.setUrlForUpdate(productUrl);
			}
			
			String[] catMap = getCategory(productCatId);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}else{
				pdb.setCategoryId(160254);
			}
			
			return pdb;
		} catch(Exception e) {
			logger.error(e);
		}
		
		return null;
	}
	
}
