package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SCGOnlineStoreFeedCrawler extends FeedManager{

	public SCGOnlineStoreFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://azecomsa99.blob.core.windows.net/production/priceza/product.json"
		};
	}

	public ProductDataBean parse(JSONObject jObj) {
			
		String productName 		= (String)jObj.get("name").toString();
		String productPrice 	= (String)jObj.get("price").toString();
		String productDesc 		= (String)jObj.get("description").toString();
		String productImageUrl 	= (String)jObj.get("image_url").toString();
		String productUrl 		= (String)jObj.get("url").toString();
		String productCatId 	= (String)jObj.get("category_id").toString();
		String productId 		= (String)jObj.get("id").toString();
		
		
		productUrl = productUrl.trim();
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;
		}
				
		productName = StringEscapeUtils.unescapeHtml4(FilterUtil.toPlainTextString(productName.trim()));
		productName = "SCG " + productName;
		ProductDataBean pdb = new ProductDataBean();
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		
		productName = productName + " (" + productId + ")";
		pdb.setName(productName);
		productUrl = StringEscapeUtils.unescapeHtml4(productUrl);
		productImageUrl = StringEscapeUtils.unescapeHtml4(productImageUrl);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		productPrice = FilterUtil.getStringBefore(productPrice, ".", productPrice);
		if(productPrice.equals("0")) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		pdb.setRealProductId(productId);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId);
		
		if (productDesc.length() != 0) {
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			pdb.setDescription(FilterUtil.toPlainTextString(productDesc));
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		return pdb;
	}
	

}

