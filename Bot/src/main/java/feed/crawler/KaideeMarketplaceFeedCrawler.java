package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class KaideeMarketplaceFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private static boolean haveResult = false;
	private static int pageResult;
	
	private static Set<String> banList = new HashSet<String>();
	
	static {
		banList.add("25948");
	}
	
	public KaideeMarketplaceFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}

	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for(String link : keySet){
			
			String[] catMap = getCategory(link);
			
			int page = 1;
			int limitPage = 300;
			if(catMap != null && catMap.length == 2 && catMap[0].equals("400201")) {
				limitPage = 9999999;
			}

			do {
				
				pageResult = 0;
				
				String cate_id = FilterUtil.getStringBetween(link, "kaidee.com/c", "-");
				if(cate_id.contains("a")) {
					cate_id = FilterUtil.getStringBefore(cate_id, "a", cate_id);
				}
				
				String data = "{\"attribute\":[],\"page\":"+page+",\"limit\":12,\"cate_id\":"+cate_id+",\"province_id\":0,\"district_id\":0}";
				String response = postRequest("https://api.kaidee.com/0.8/ads/search", data);
				parseAPI(response, getCategory(link));
				
				logger.info("crawl" + link + " |page " + page + " |result " + pageResult);
				page++;

			}while(!Thread.currentThread().isInterrupted() && pageResult>0 && page<limitPage);
		}
		
		if(haveResult) {
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		}
	}

	private void parseAPI(String data, String[] catMap) {
		if(StringUtils.isNotBlank(data)) {
			try {
				JSONObject jsonObj = (JSONObject) new JSONParser().parse(data);
				if(jsonObj == null) return;
				
				JSONArray jsonArr = (JSONArray) jsonObj.get("items");
				if(jsonArr == null || jsonArr.size() == 0) return;
				
				for (Object obj : jsonArr) {
					JSONObject obj1 = (JSONObject) obj;
					if(obj1 == null) continue;
					
					JSONObject obj2 = (JSONObject) obj1.get("ad");
					if(obj2 == null) continue;
					
					String id = StringUtils.defaultString(String.valueOf((Object) obj2.get("legacy_id")));
					String title = StringUtils.defaultString(String.valueOf((Object) obj2.get("title")));
					String description = StringUtils.defaultString(String.valueOf((Object) obj2.get("description")));
					String price = StringUtils.defaultString(String.valueOf((Object) obj2.get("price")));
					String condition = "";
					String pic = "";
					JSONObject memeber = (JSONObject) obj2.get("member");
					if(memeber!=null) {
						String memeberId = StringUtils.defaultString(String.valueOf((Object) memeber.get("id")),"");
						memeberId = memeberId.trim();
						if(banList.contains(memeberId)) {						
							logger.info("skip id ban merchant :"+memeberId+" productId :"+id);
							continue;
						}
					}
					
					JSONArray imageArr = (JSONArray) obj2.get("images");
					if(imageArr != null && imageArr.size() > 0) {
						for (Object imageObj : imageArr) {
							try {
								String image = String.valueOf(((JSONObject) ((JSONObject) ((JSONObject) imageObj).get("sizes")).get("large")).get("link"));
								if(StringUtils.isNotBlank(image)) {
									pic = image;
									break;
								}
							} catch(Exception e) {
								// nothing
							}
						}
					}
					
					JSONObject conditionObj = (JSONObject) obj2.get("condition");
					if(conditionObj != null) {
						condition = StringUtils.defaultString(String.valueOf((Object) conditionObj.get("name_th")));
					}
					
					if(StringUtils.isNotBlank(condition) && (condition.equals("มือหนึ่ง") || condition.equals("มือสอง"))) {
						title = title + " (" + condition + ")";
					}
					
					if(title == null || id == null || price == null) continue;
					if(title.equals("null") || id.equals("null") || price.equals("null")) continue;
					
					title = title + " (" + id + ")";
					
					ProductDataBean resultBean = new ProductDataBean();
					resultBean.setPrice(FilterUtil.convertPriceStr(price));
					resultBean.setName(FilterUtil.removeSpace(title));
					description = removeIllegaCharactor(description,new String[] {"th","eng"});				
					resultBean.setDescription(FilterUtil.removeSpace(description));
					resultBean.setUrl("https://www.kaidee.com/product-" + id);
					resultBean.setPictureUrl(pic);
					resultBean.setUrlForUpdate(id);
					if(catMap != null && catMap.length == 2) {
						resultBean.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
						resultBean.setKeyword(catMap[1]);
					}
					
					String result = mockResult(resultBean);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
							pwr.println(result);
							haveResult = true;
							pageResult++;
						}catch(Exception e) {
							logger.error(e);
						}
					}
					
				}
				
			} catch(Exception e) {
				logger.error(e);
			}
		}
	}
	
	private String postRequest(String url, String requestParam) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	try {
	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(false);
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 	
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(requestParam.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		isr = new InputStreamReader(conn.getInputStream());
	 		brd = new BufferedReader(isr);
	 		StringBuilder rtn = new StringBuilder(5000);
	 		String line = "";
	 		while ((line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 		return rtn.toString();
	 	} catch (Exception e) {
	 		logger.error(e);
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
		return null;
	 	
	}	
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	public static String removeIllegaCharactor(String inputString, String[] allowList) {
		StringBuilder build = new StringBuilder();
		for (char splitChar : inputString.toCharArray()) {
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							build.append(splitChar);
						}
					}
				}
		}
		return build.toString();
	}
	
	private static final Map<String, Map<Integer, Integer>> languageCharacterMap = createLanguageCharacterMap();
    private static Map<String, Map<Integer, Integer>> createLanguageCharacterMap(){
    	Map<String, Map<Integer, Integer>> maps = new HashMap<>();
    	maps.put("th", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0E00, 0x0E5B);
			}
		});
    	maps.put("eng", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0041, 0x005A);
				put(0x0061, 0x007A);
				put(0x0030, 0x0039);
			}
		});
		maps.put("viet", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0xc0, 0xc3);
				put(0xc8, 0xca);
				put(0xcc, 0xcd);
				put(0xd2, 0xd5);
				put(0xd9, 0xda);
				put(0xdd, 0xdd);
				put(0xe0, 0xe3);
				put(0xe8, 0xea);
				put(0xec, 0xed);
				put(0xf2, 0xf5);
				put(0xf9, 0xfa);
				put(0xfd, 0xfd);
				put(0x102, 0x103);
				put(0x110, 0x111);
				put(0x128, 0x129);
				put(0x168, 0x169);
				put(0x1a0, 0x1a1);
				put(0x1af, 0x1b0);
				put(0x1ea0, 0x1ef9);
			}
		});
		maps.put("chinese", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x4E00, 0x9FD5);
			}
		});
    	return maps;
    }
    
    public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
		if ((i >= minValueInclusive && i <= maxValueInclusive))
			return true;
		else
			return false;
	}

}
