package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LennovoFeedCrawler extends FeedManager {

	public LennovoFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"http://ftp.gbi-lenovo.com/data/GBI_Google_Merchant_Lenovo_th_TH.xml"};
	}
	
	@Override
	public ProductDataBean parse(String data) {
		Document doc = Jsoup.parse(data,"",Parser.xmlParser());
		String name = parseCData(doc.select("title").html());
		String price = parseCData(doc.select("g|sale_price").html());
		String basePrice = parseCData(doc.select("g|price").html());
		String id = parseCData(doc.select("g|id").html());
		String desc = parseCData(doc.select("description").html());
		String url  = parseCData(doc.select("link").html());
		String image = parseCData(doc.select("g|image_link").html());
		
		String cat = parseCData(doc.select("g|product_type").html());
		cat = StringEscapeUtils.unescapeHtml4(cat);
		desc = FilterUtil.toPlainTextString(desc);

		url = StringEscapeUtils.unescapeHtml4(url);
		cat = StringEscapeUtils.unescapeHtml4(cat);
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		
		if("null".equals(desc)) {
			desc  = null;
		}
		String tmp = "";
		
		if(StringUtils.isNotBlank(id)) 
			tmp = " ("+id+")";
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		
		ProductDataBean pdb = new ProductDataBean();

		pdb.setName(name+tmp);
		pdb.setPrice(p);
		pdb.setDescription(desc);
		pdb.setBasePrice(bp);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		String[] catMap = getCategory(cat);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0);
		}
		return pdb;
	}
	
	private String parseCData(String value){
		if(StringUtils.isNotBlank(value)){
			value = value.replace("<![CDATA[", "").replace("]]>", "");
			value = FilterUtil.toPlainTextString(value);
		}
		return value;
	}

}
