package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class KingPowerFeedCrawler extends FeedManager {

	public KingPowerFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://kpc-prod-resources.s3-ap-southeast-1.amazonaws.com/seo-products/ProductFeedGoogleTh.xml"};
	}
	

	

	@Override
	public ProductDataBean parse(String data) {
		
		String productName = FilterUtil.getStringBetween(data, "<title>", "</title>");		
		String basePrice = FilterUtil.getStringBetween(data, "<g:price>", "</g:price>");
		String productUrl = FilterUtil.getStringBetween(data, "<link>", "</link>").replace("?utm_source=InvolveAsia", "");
		String productImageUrl = FilterUtil.getStringBetween(data, "<g:image_link>", "</g:image_link>");
		String productCatId = FilterUtil.getStringBetween(data, "<g:product_type>", "</g:product_type>");
		String productDesc = FilterUtil.getStringBetween(data, "<g:description>", "</g:description>");
		String productExpire = FilterUtil.getStringBetween(data, "<g:availability>", "</g:availability>");
		String productId = FilterUtil.getStringBetween(data, "<g:id>", "</g:id>");
		String check = FilterUtil.getStringBetween(data, "<g:custom_label_0>", "</g:custom_label_0>");
		String productPrice = FilterUtil.getStringBetween(data, "<g:sale_price>", "</g:sale_price>");
		if(!check.equals("Delivery")) {
			return null;
		}
		
		
		if(productExpire.equals("out of stock") || productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 || productId.trim().length() == 0) {
			return null;			
		}
		
		double price_d = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice));
		double base_d = FilterUtil.convertPriceStr((FilterUtil.removeCharNotPrice(basePrice)));
		double temp_price = 0;

		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
						
		ProductDataBean pdb = new ProductDataBean();	
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		pdb.setName(productName.trim()+" ("+productId+")");			
		pdb.setUrlForUpdate(productId);
		
		if(productId.trim().length() > 0) {
			pdb.setRealProductId(productId.trim());
		}

		pdb.setPrice(price_d);
		pdb.setBasePrice(base_d);
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);
		}
		
		pdb.setUrlForUpdate(productUrl);
		
		String[] catMap = getCategory(StringEscapeUtils.unescapeHtml4(productCatId));
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));	
			pdb.setKeyword(catMap[1]);
		}else {
			logger.info("Non cat: "+productCatId);
		}

		if(productDesc.length() > 0){
			pdb.setDescription(productDesc);
		}
		
		return pdb;
	}
	
	

}
