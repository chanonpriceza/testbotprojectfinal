package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class PomeloFashionFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	
	public PomeloFashionFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		int size = 20;
		Set<String> keySet = getCategoryKeySet();
		for (String url : keySet) {
			int start = 0 ;
			boolean next = true;
			while(!Thread.currentThread().isInterrupted() && next){
				String reqUrl = url.replace("XXX", String.valueOf(start));
				String[] response = httpRequestWithStatus(reqUrl, "UTF-8", false);
				if(response == null || response.length != 2) continue;
				//accept-language: th
				String data = response[0];
				if(StringUtils.isBlank(data)) continue;
				
				JSONArray jarr = new JSONArray();
				
				try {
					JSONParser parser = new JSONParser();
					JSONObject jObj = (JSONObject) parser.parse(data);
					if(jObj != null && jObj.size() > 0) {
						jarr = (JSONArray) jObj.get("products");
						parseJson(jarr, url);
					}
					
					if(jarr.size() < size){
						next = false;
					}
					
					if(jarr != null && jarr.size() > 0) {
						logger.info(jarr.size() + " data from " + reqUrl);
					}else {
						logger.info("nodata from " + reqUrl);
					}
					
					
					start += size;
					
				} catch (ParseException | UnsupportedEncodingException | ClassNotFoundException | SQLException e) {
					logger.error(e);
				}
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	@SuppressWarnings("unchecked")
	private void parseJson(JSONArray jarr, String sourceUrl) throws UnsupportedEncodingException, ClassNotFoundException, SQLException, ParseException {
		for (Object object : jarr) {
			JSONObject jObj = (JSONObject)object;
			String expire = String.valueOf(jObj.get("is_back_in_stock"));
			if("0".equalsIgnoreCase(expire)) {
				//continue;
			}
			long id = Long.parseLong(String.valueOf(jObj.get("id_product")));
			String name = (String)jObj.get("name");		
			String man = String.valueOf(jObj.getOrDefault("manufacturer_name",""));		
			String hyphen_name = (String)jObj.get("hyphen_name");	
			String category = (String)jObj.get("category_link_rewrite");	
			String img = (String) jObj.get("cover_img_v2");	
			
			double price = Double.parseDouble(String.valueOf(jObj.get("discounted_price")));
			double bPrice = Double.parseDouble(String.valueOf(jObj.get("price")));
			boolean exp = (boolean)jObj.get("is_sold_out");		
			
			if( exp || StringUtils.isBlank(name) || StringUtils.isBlank(hyphen_name) 
					|| StringUtils.isBlank(category) || id == 0 || price == 0.0) {
				continue;				
			}		
			ProductDataBean pdb = new ProductDataBean();	
			
			name = StringEscapeUtils.unescapeHtml4(name).trim();
			name = name +" "+man+ " (" + id + ")";
			pdb.setName(name);
			pdb.setRealProductId(String.valueOf(id));
		
			pdb.setPrice(price);
			if(price != bPrice){
				pdb.setBasePrice(bPrice);
			}
			String productUrl = "https://www.pomelofashion.com/th/th/"+ category+"/"+id+"-"+ hyphen_name + ".html";
			if (StringUtils.isNotBlank(productUrl) && productUrl.length() != 0) {	
				productUrl = BotUtil.encodeURL(productUrl);
				pdb.setUrl(productUrl);
				pdb.setUrlForUpdate(String.valueOf(id));
			}
			if (StringUtils.isNotBlank(img) && img.length() != 0) {
				pdb.setPictureUrl("https://pomelofashion.imgix.net/"+img);
			}
			
			String[] catMap = getCategory(sourceUrl);
			if(catMap != null && catMap.length > 0) {
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);
			}
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("accept-language","th");
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
