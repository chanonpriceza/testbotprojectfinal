package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.HTMLParser;
import web.parser.filter.TemplateLazadaHTMLParser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LazadaDailyDealFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public LazadaDailyDealFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		// addition from urlPattern
		try {
			Set<String> keySet = getCategoryKeySet();
			HTMLParser webParser = (HTMLParser)Class.forName("web.parser.filter.TemplateLazadaJSIgnoreMerchantCheckHTMLParser").getDeclaredConstructor().newInstance();
			for (String url : keySet) {
				ProductDataBean[] dataBeanList = webParser.parse(url);
				if(dataBeanList != null && dataBeanList.length > 0) {
					for (ProductDataBean pdb : dataBeanList) {
						String result = mockResult(pdb);
						if(StringUtils.isNotBlank(result)) {
							try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
								pwr.println(result);
								haveResult = true;
							}catch(Exception e) {
								logger.error(e);
							}
						}
					}
				}
			}
		}catch(Exception e) {
			logger.error(e);
		}
		
		Map<String, Integer> linkMap = new HashMap<String, Integer>();
		linkMap.put("https://www.lazada.co.th/hot-top-deals/", 30);
		linkMap.put("https://www.lazada.co.th/stock-sale/", 70);
		linkMap.put("https://www.lazada.co.th/special-promotion/", 20);
		linkMap.put("https://www.lazada.co.th/shop-blenders/?tefal-official-store1&from=wangpu", 1);
		linkMap.put("https://www.lazada.co.th/eglips-thailand/?langFlag=th&q=All-Products&from=wangpu&pageTypeId=2", 5);

		for(Entry<String, Integer> rec : linkMap.entrySet()){
			String linkUrl = rec.getKey();
			int maxDepth = rec.getValue();
			for(int i = 1; i <= maxDepth; i++){
				String linkPage = linkUrl;
				if(i > 1){
					if(linkUrl.contains("?")) {
						linkPage = linkUrl + "&page=" + i;
					}else {
						linkPage = linkUrl + "?page=" + i;
					}
				}
				try{
					String[] response = HTTPUtil.httpRequestWithStatus(linkPage, "UTF-8", false);
					if(response == null || response.length != 2) break;
					
					String html = response[0];
					if(StringUtils.isBlank(html)) break;
					
					List<String> jsonList = FilterUtil.getAllStringBetween(html, "<script type=\"application/ld+json\">", "</script>");
					if(jsonList != null && jsonList.size() > 0) {
						for (String jsonStr : jsonList) {
							if(StringUtils.isNotBlank(jsonStr)) {
								parseJson(jsonStr);
							}
						}
					}
					
					Thread.sleep(5000);
				}catch(Exception e){
					logger.error(e);
					continue;
				}
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}	
	
	public void parseJson(String jsonStr){
		String name="", price="", pic="", url="";
		String availability="";

		try {
			JSONObject jMainObj = (JSONObject) new JSONParser().parse(jsonStr);
			if(jMainObj != null) {
				JSONArray jMainArr = (JSONArray) jMainObj.get("itemListElement");
				if(jMainArr != null && jMainArr.size() > 0) {
					for (Object object : jMainArr) {
						try {
							JSONObject jObj 	= (JSONObject) object;
							JSONObject offerObj	= (JSONObject) jObj.get("offers");
							name				= (String) jObj.get("name");
							pic 				= (String) jObj.get("image");
							url					= (String) jObj.get("url");
							price 				= (String) offerObj.get("price");
							availability 		= (String) offerObj.get("availability");
							
							if(StringUtils.isNotBlank(availability) && availability.toLowerCase().indexOf("instock") > -1) {
								if(StringUtils.isBlank(name) || StringUtils.isBlank(url) || StringUtils.isBlank(price)) {
									continue;
								}
								
								ProductDataBean pdb = new ProductDataBean();
								name = StringEscapeUtils.unescapeHtml4(name).trim();	

								pdb.setName(name + " (Hot Deal)");
								pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
								
								pdb.setUrlForUpdate(url);
								
								url = FilterUtil.getStringBefore(url, "?", url);
								pdb.setUrl("https://c.lazada.co.th/t/c.PVm?url="+ URLEncoder.encode(TemplateLazadaHTMLParser.generateURL(url), Charset.defaultCharset()));
								
								pdb.setCategoryId(160255);
								if(StringUtils.isNotBlank(pic)){
									pdb.setPictureUrl(pic);
								}
								
								String result = mockResult(pdb);
								if(StringUtils.isNotBlank(result)) {
									try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
										pwr.println(result);
										haveResult = true;
									}catch(Exception e) {
										logger.error(e);
									}
								}
							}
						}catch(Exception e) {
							logger.error(e);
						}
					}
				}
			}
		}catch(Exception e) {
			logger.error(e);
		}
		
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
