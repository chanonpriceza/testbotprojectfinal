package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ShopeeKeywordAPIFeedCrawler extends FeedManager {
	
//	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
	private static String mainParam = "https://shopee.co.th/api/v2/search_items/?by=relevancy&keyword={keyword}&limit=50&newest={page}&order=desc&page_type=search&version=2";
	protected static String GET_PRODUCT_LINK = "https://shopee.co.th/api/v2/item/get?itemid={itemid}&shopid={shopId}";
	protected static JSONParser parser = new JSONParser();
	private static String currentCat = "";

	
	public ShopeeKeywordAPIFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			for(String key:getCategoryKeySet()) {
				currentCat = key;
				for(int page = 0; page <20 ;page++) {
					String keyword = key;
					JSONParser paser = new JSONParser();
					URL u =  new URL(mainParam.replace("{page}",String.valueOf(page*50)).replace("{keyword}",keyword));
					InputStreamReader in = new InputStreamReader(u.openStream());
					JSONObject obj = (JSONObject) paser.parse(in);
					getUrl(obj);
				}
			}
		}catch (Exception e) {
			logger.error("Error",e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	protected void getProductDetail(String productId,String shopId) throws Exception {
		
		String req = GET_PRODUCT_LINK.replace("{itemid}", productId).replace("{shopId}", shopId);
		String[] response = HTTPUtil.httpRequestWithStatus(req, "UTF-8", true);
		
		if(response==null||response.length==0) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		if(response[0]==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		JSONObject obj = (JSONObject) parser.parse(response[0]);
		
		obj = (JSONObject) obj.get("item");
		
		if(obj==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		String price = String.valueOf(obj.get("price"));
		String basePrice = String.valueOf(obj.get("price_before_discount"));
		String name = String.valueOf(obj.get("name"));
		String productImageUrl =  String.valueOf(obj.get("image"));
		String productUrl 		= "";
		double priceDouble = FilterUtil.convertPriceStr(price) / 100000;
		double basePriceDouble = FilterUtil.convertPriceStr(basePrice) / 100000;
		productImageUrl = productImageUrl.replace("&amp;", "&");
		name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim()).trim();
		
		productUrl = ""
				+ "https://shopee.co.th/universal-link/product/" + shopId + "/" + productId
				+ "?smtt=9"
				+ "&deep_and_deferred=1"
				+ "&pid=priceza_int"
				+ "&c=datafeed"
				+ "&af_click_lookback=7d"
				+ "&is_retargeting=true"
				+ "&af_reengagement_window=7d"
				+ "&af_installpostback=false"
				+ "&clickid=(xxxxx)"
				+ "&utm_source=priceza"
				+ "&utm_medium=affiliates"
				+ "&utm_campaign=datafeed"
				;
		
		if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(name)) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+productId+")");
		pdb.setPrice(priceDouble);
		pdb.setBasePrice(basePriceDouble);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(shopId + "." + productId);
		pdb.setRealProductId(productId);
		
		if (StringUtils.isNotBlank(productImageUrl)) {
			pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
		}
		

		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} 
		else
			pdb.setCategoryId(0);
		
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
			}catch(Exception e) {
				logger.error("Error",e);
			}
		}
		
	}

	public String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	protected String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getRealProductId()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	private boolean getUrl(JSONObject data) {
		boolean isCon = false;
		
		try{		
			if(data==null)
				return false;
			JSONArray jArr = (JSONArray) data.get("items");
			if(jArr==null)
				return false;
			
			for (Object object : jArr) {
				JSONObject jObj 		= (JSONObject) object;
				String productId 		= jObj.get("itemid").toString();
				String shopId 			= jObj.get("shopid").toString();
				getProductDetail(productId,shopId);
				
				isCon= true;
			}
				
		}catch (Exception e) {
			logger.error("Error",e);
			isCon = false;
		}
		
		return isCon;
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}




}
