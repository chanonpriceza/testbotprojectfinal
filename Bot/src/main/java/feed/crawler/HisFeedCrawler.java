package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class HisFeedCrawler extends FeedManager {

	public HisFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://external.his.in.th/product/feed"};
	}
	
	@Override
	public ProductDataBean parse(String data) {

		String name = FilterUtil.getStringBetween(data,"<name>","</name>");
		String id = FilterUtil.getStringBetween(data,"<id>","</id>");
		String url = FilterUtil.getStringBetween(data,"<url>","</url>");
		String image = FilterUtil.getStringBetween(data,"<image_url>","</image_url>");
		String cat = FilterUtil.getStringBetween(data,"<category_id>","</category_id>");
		String desc = FilterUtil.getStringBetween(data,"<description>","</description>");
		String basePrice = FilterUtil.getStringBetween(data,"<base_price>","</base_price>");
		String price = FilterUtil.getStringBetween(data,"<price>","</price>");
		String id_temp = (StringUtils.isNotBlank(id))?" ("+id+")":"";
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+id_temp);
		pdb.setRealProductId(id);
		pdb.setUrl(url);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setDescription(desc);
		pdb.setPictureUrl(image);
		pdb.setUrlForUpdate(id);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
		
	}
}
