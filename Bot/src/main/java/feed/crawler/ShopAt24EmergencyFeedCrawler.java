package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class ShopAt24EmergencyFeedCrawler extends FeedManager {

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public ShopAt24EmergencyFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
				if(fileName.indexOf("2002_shopat24_emergency.csv") > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){

		String name          = data.get(0);
		String description   = data.get(1);
		String price         = data.get(2);
		String base_price    = data.get(3);
		String image_url     = data.get(4);
		String url           = data.get(5);
		String realProductId = data.get(6);
		String subcat        = data.get(7);
		String keyword       = data.get(8);
		
		if( StringUtils.isBlank(name) || name.contains("Hot Deal")){
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name);
		pdb.setDescription(StringUtils.defaultString(description));
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(base_price));
		pdb.setPictureUrl(image_url);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(realProductId);
		pdb.setRealProductId(realProductId);
		pdb.setCategoryId(BotUtil.stringToInt(subcat, 0));	
		pdb.setKeyword(keyword);

		return pdb;
	}
	
}
