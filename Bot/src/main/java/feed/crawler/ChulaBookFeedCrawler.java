package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ChulaBookFeedCrawler extends FeedManager {
	
	private static String mainUrl = "https://api.chulabook.com/products?type=book";
	private static String main= "";
	private static String sub= "";
	private static String cat= "";
	private static String page= "";
	private static JSONParser parser = new JSONParser();

	public ChulaBookFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	static int maxPage = 35;
	
	@Override
	public void loadFeed() {
		try {
			for(String x:getCategoryKeySet()) {
				List<String> split = Arrays.asList(x.split("\\|"));
				if(split==null||split.size()==0)
						return;
				for(int i = 1;i < maxPage; i++) {
				main= split.size()>0 ? split.get(0):"";
				sub= split.size()>1 ?split.get(1):"";
				cat= split.size()>2 ?  split.get(2):"";
				page= i+"";
				Map<String,String> params= new LinkedHashMap<String,String>();
				params.put("main_category",main);
				params.put("sub_category",sub);
				params.put("category",cat);
				params.put("page",page);
				String u = genUrlParam(params);
				URL url = new URL(u);
				HttpURLConnection h = (HttpURLConnection)url.openConnection();
				h.setRequestProperty("User-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
				
				JSONObject extract = (JSONObject)parser.parse(new InputStreamReader(h.getInputStream()));
				JSONArray rows = (JSONArray)extract.get("rows");
				if(rows==null||rows.size()==0)
					break;
				JSONArray optimize = optimizeJSON(rows,x);
				Files.copy(new ByteArrayInputStream(optimize.toJSONString().getBytes()),new File(BaseConfig.FEED_STORE_PATH+"/"+Math.random()*999+"-"+i).toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
	}
	
	private String genUrlParam(Map<String,String> params) {
		String result = mainUrl;
		for(Entry<String,String> x:params.entrySet()) {
			if(StringUtils.isNotBlank(x.getValue())) {
				try {
					URI bulder = new URIBuilder(result).addParameter(x.getKey(),x.getValue()).build();
					result = bulder.toString();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private JSONArray optimizeJSON(JSONArray array,String cat) {
		array.stream().forEach(x->((JSONObject)x).put("cat",cat));
		return array;
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String name = String.valueOf(data.get("name"));
		String price = String.valueOf(data.get("price"));
		String url = "https://www.chulabook.com/th/product-details/"+String.valueOf(data.get("id"));
		String desc = String.valueOf(data.get("author"));
		String img = "https://api.chulabook.com/images/pid-"+String.valueOf(data.get("id"))+".jpg";
		//String base = String.valueOf(data.get(""));
		String cat = String.valueOf(data.get("cat"));
		String id = String.valueOf(data.get("barcode"));
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" "+"("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(img);
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		return pdb;

	}

}
