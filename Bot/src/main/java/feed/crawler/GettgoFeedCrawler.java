package feed.crawler;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FTPUtil;
import utils.FilterUtil;

public class GettgoFeedCrawler extends FeedManager {

	public GettgoFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public void loadFeed() {
		
		
		String FEED_URL = "http://27.254.82.243:8081/300673.csv";
		String FILE_NAME = "300673.csv";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		FTPUtil.downloadFileWithLogin(FEED_URL,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME, FEED_USER, FEED_PASS);
		String FEED_URL2 = "http://27.254.82.243:8081/300673_2.csv";
		String FILE_NAME2 = "300673_2.csv";
		String FEED_USER2 = "bot";
		String FEED_PASS2 = "pzad4r7u";
		FTPUtil.downloadFileWithLogin(FEED_URL2,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME2, FEED_USER2, FEED_PASS2);
		
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME2};
	
	}
	

	

	
	public ProductDataBean parse(CSVRecord data){
		
//		product_id=0, 
//		broker=1, 
//		insurer=2, 
//		title=3, 
//		type=4, 
//		premium=5, 
//		previous_price=6, 
//		brand=7, 
//		Model=8, 
//		sub_model=9, 
//		cc=10, 
//		year=11, 
//		URL ของสินค้านั้นๆ (ถ้ามี)=12, 
//		compulsory_insurance=13, 
//		sum_insured=14, 
//		fire_damage=15, 
//		theft=16, 
//		flood_damage=17, 
//		deductible=18, 
//		repair=19, 
//		bodily_injury_liability=20, 
//		bodily_injury_liability/Accident=21, 
//		property_damage_liability=22, 
//		personal_injury_protection=23, 
//		bail_bond=24, 
//		medical_payments=25

		
		try{
			//ประกันชั้น 1 - วิริยะประกันภัย - Toyota - Yaris 1.2cc - ไม่ระบุรุ่นย่อย - ปี 2018 (121e083970)
			String poType = data.get(4).toLowerCase().replace("type", "").trim().replace("-", " ").replace(",", "");                // -ชั้นประกัน
			String insName = data.get(2).trim().replace("-", " ");                                       // -บริษัทประกัน
			//(ชั้นประกัน) - (บริษัทประกัน) - (brand) - (model+cc (ตัว cc ที่เค้าส่งมา หาร1000 ต่อด้วย cc ทศนิยม 1ตำแหน่ง))  - (sub_model) - (year) - __
			String brand = data.get(7).trim().replace("-", " ");                                                                    // -ยี่ห้อรถ
			String model = data.get(8).trim().replace("-", " ");                                                                    // -รุ่นรถ
			String subModel = data.get(9).trim().replace("-", " ");                                                                 // -รุ่นย่อย
			String year = data.get(11).trim().replace("-", " ");                                                                     // -ปีรถ
			String od = data.get(14).trim().replace("-", " ");                                                                       // -ทุนประกัน
			String oddd = data.get(18).trim().replace("-", " ");                                                                    // -ค่าเสียหายส่วนแรก
			String garage = data.get(19).trim().replace("-", " ");                                                                  // -ซ่อม
			garage = StringUtils.isNotBlank(garage)?garage.contains("อู่")?"ซ่อมอู่":"ซ่อมศูนย์":"ไม่ระบุอู่ซ่อม";
			String packageName = data.get(3).trim().replace("-", " ");                                                            // -แผน
			String carAct = data.get(13).trim().replace("-", " ").replace(",", "");	    
			String cc = data.get(10).trim().replace("-", " ");     
			double ccCal = FilterUtil.convertPriceStr(cc);
			if(ccCal!=0) {
				ccCal = ccCal/1000;
				cc = String.valueOf(ccCal)+"cc";
			}
			
			String fire = data.get(15).trim().replace("-", " ");           										                    // -คุ้มครองไฟไหม้
			String theft = data.get(16).trim().replace("-", " ");          										                    // -คุ้มครองโจรกรรม
			String flood = data.get(17).trim().replace("-", " ");          										                    // -คุ้มครองน้ำท่วม
			String bi = data.get(20).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อคน
			String bt = data.get(21).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อครั้ง
			String tp = data.get(22).trim().replace("-", " ");             										                    // -ทรัพย์สินบุคคลภายนอก
			String pa = data.get(23).trim().replace("-", " ");             										                    // -อุบัติเหตุส่วนบุคคล
			String bb = data.get(24).trim().replace("-", " ");             										                    // -ประกันตัวผู้ขับขี่
			String me = data.get(25).trim().replace("-", " ");             										                    // -ค่ารักษาพยาบาล
			
			String id = data.get(0).trim();
			String productPrice = data.get(5).trim();
			String basePrice = data.get(6).trim();
			String productUrl = data.get(12).trim();
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
			
			String uniqueText = "";
			uniqueText += id;
			uniqueText = toMD5(id);
			
//			ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
			String productName = "";
			productName += "ประกันชั้น " + poType;
			productName += " - " + insName;
			productName += " - " + brand;
			productName += " - " + model+" "+cc;
			productName += " - " + ((StringUtils.isNotBlank(subModel) && !subModel.equals("0"))? subModel : "ไม่ระบุรุ่นย่อย");
			productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
			productName += " (" + uniqueText.substring(0,10) + ")";
			productName = productName.replace(",", "");
			productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
			productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
			productName = productName.replaceAll("\\s+", " ");                                                  
			productName = productName.replace("( ", "(");                                                       
			productName = productName.replace(" )", ")");    
 			productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
			productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");                            
			                             
//			( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
			String productDesc = "";
			productDesc += ((StringUtils.isNotBlank(packageName))? packageName : "ไม่ระบุแผน" ); 
			productDesc += " - " + (StringUtils.isNotBlank(garage)?garage:"ไม่ระบุอู่ซ่อม");
			productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("No"))? "ไม่ระบุ" : od ));
			productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
			productDesc += " - " + ((StringUtils.isNotBlank(carAct) && (carAct.equals("Yes"))) ? "รวมพรบ." : "ไม่รวมพรบ.");
			productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
			productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
			productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood : "ไม่ระบุ" ); 
			productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
			productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
			productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
			productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
			productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );          
			productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
			productDesc = productDesc.replaceAll("\\s+", " ");                                               
			productDesc = productDesc.replace("( ", "(");                                                    
			productDesc = productDesc.replace(" )", ")");                                                    
			productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
			productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
			productDesc = productDesc.replace(".00", "");
			productDesc = productDesc.replace(",", "");
			
			DecimalFormat formatPrice = new DecimalFormat("###");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			productPrice = formatPrice.format(Double.parseDouble(productPrice));
			
			String productUrlForUpdate = uniqueText;
			productUrl = productUrl.replaceAll(" ", "-");
			if(productUrl.startsWith("www")) {
				productUrl = "https://" + productUrl;
			}
			
			String productImage = getImageByInsName(insName);
			
			if(StringUtils.isBlank(productPrice) || StringUtils.isBlank(productUrl)){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
			pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
			pdb.setPictureUrl(productImage);
			pdb.setDescription(productDesc);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrlForUpdate);
			pdb.setCategoryId(250101);
			pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		
		return null;
		
	}

	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	private String getImageByInsName(String insName) {
		String result = null;
		if(insName.equals("เมืองไทยประกันภัย")) result = "https://halobe.files.wordpress.com/2017/08/insure-company-5551.jpg";
		if(insName.equals("ประกันภัยไทยวิวัฒน์")) result = "https://halobe.files.wordpress.com/2017/08/insure-company-5574.jpg";
		if(insName.equals("วิริยะประกันภัย")) result = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg";
		if(insName.equals("วิริยะประกันภัย")) result = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg";
		if(insName.equals("ธนชาตประกันภัย"))	result = "https://halobe.files.wordpress.com/2017/08/insure-company-5556.jpg"; 
		if(insName.equals("MSIG"))	result = "https://halobe.files.wordpress.com/2017/08/insure-company-5564.jpg"; 
		return result;
	}
}
