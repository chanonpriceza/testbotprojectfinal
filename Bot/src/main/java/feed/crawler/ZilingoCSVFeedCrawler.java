package feed.crawler;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ZilingoCSVFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public ZilingoCSVFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		String feedRequestUrl = "https://88b5bdde-6e4e-4e3f-b588-22dec51d9dab.zilingo.com/api/v1/productFeedURL?provider=FACEBOOK_BUSINESS&region=THA&s3dirname=zilingo-products-feed-v1";
		String url = httpRequestGetFinalUrl(feedRequestUrl,"priceza","lham1nk234pHSzsd",true,"UTF-8", true);
		return new String[] {url};
	}
	
	public  String httpRequestGetFinalUrl(String url, String user, String password, boolean forceGzip, String charset, boolean redirectEnable) {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			if(StringUtils.isNotBlank(user)){
				String userpass = user + ":" + password;
	  			String basicAuth = "Basic " + new String(Base64.encodeBase64(userpass.getBytes()));
	  			conn.setRequestProperty ("Authorization", basicAuth);
				conn.setInstanceFollowRedirects(true);
			}
			
			conn.getResponseMessage();

			return conn.getURL().toString();
			
    	} catch (Exception e) {
    		logger.error(e);
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}
	
	public ProductDataBean parse(CSVRecord data){
		try{

			//3. ชื่อสินค้า title + (color) + (id) เช่น
			String id = data.get(0);
			String name = data.get(6);
			String price = data.get(8);
			String desc = data.get(3);
			String category = data.get(11);
			String image = data.get(4);
			String url  = data.get(5);
			String basePrice = data.get(7);
			String color = data.get(10);
			
			if( StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
				return null;
			}
			if(category.contains("Sexual Care")){
				return null;
			}
			ProductDataBean pdb = new ProductDataBean();

			name = name+" ("+color+")"+" ("+id+")";
			pdb.setName(name);	
			pdb.setPrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
			url = url.replace("http://", "https://");
			pdb.setUrl(url);
			pdb.setRealProductId(id);
			
			if(!image.isEmpty()){
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
				pdb.setBasePrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice))));
			}
			
			if(StringUtils.isNotBlank(desc)){
				desc = StringEscapeUtils.unescapeHtml4(desc);
				pdb.setDescription(desc);
			}
						
			String[] mapping = getCategory(category);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}

			return pdb;
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
	
}
