package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import product.processor.ProductFilter;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LnwmallFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	
	public LnwmallFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
		
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for (String link : keySet) {
			int offset = 0;
			boolean isContinue = true;
			while(!Thread.currentThread().isInterrupted() && isContinue && offset <= 10000) {
				String REQUEST_URL = link.replace("{offset}", String.valueOf(offset));
				logger.info(REQUEST_URL);
				String[] response = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
				if(response == null || response.length != 2) break;
				
				String data = response[0];
				if(StringUtils.isBlank(data)) break;
				
				try {
					isContinue = parseJson(data, link);
				} catch (UnsupportedEncodingException e) {
					logger.error(e);
				} catch (ParseException e) {
					logger.error(e);
				}	
				offset += 100;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	@SuppressWarnings("unchecked")
	private boolean parseJson(String data, String catNo) throws UnsupportedEncodingException, ParseException {
		
		JSONObject jData = (JSONObject)new JSONParser().parse(data);
		JSONArray jarr = (JSONArray) jData.get("products");

		int pdCnt = 0;
		if(jarr == null || jarr.size() == 0){
			return false;
		}
		
		for (Object obj : jarr) {
			pdCnt++;
			
			JSONObject jObj = (JSONObject)obj;
			
			String productName = String.valueOf(jObj.getOrDefault("product_name",""));
			if(ProductFilter.checkCOVIDInstantDelete(productName))
	        	return false;
			String productPrice = String.valueOf(jObj.getOrDefault("product_min_price",""));
			String productUrl = "https://www.lnwmall.com/product/";
			String productImageUrl = String.valueOf( jObj.getOrDefault("product_images",""));    
			String productId =  String.valueOf(jObj.getOrDefault("product_uuid",""));
			String sellerId = String.valueOf(jObj.getOrDefault("seller_account_id",""));
			
			if(StringUtils.isNotBlank(sellerId) && sellerId.equals("487405")) {
				continue;
			}
			if(StringUtils.isBlank(productUrl) || StringUtils.isBlank(productName) || StringUtils.isBlank(productPrice) || StringUtils.isBlank(productId)) {
				continue;
			}		
			ProductDataBean pdb = new ProductDataBean();
			
			productName = StringEscapeUtils.unescapeHtml4(productName);	
			productName = productName.trim();
			productName += " (" + productId + ")";
			pdb.setName(productName);
			pdb.setPrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice))));
			productUrl += productId;
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productId);
			
			if (StringUtils.isNoneBlank(productImageUrl)) {
				productImageUrl = productImageUrl.replace("&amp;", "&");
				productImageUrl = FilterUtil.getStringBefore(productImageUrl, ",", productImageUrl);
				pdb.setPictureUrl(productImageUrl);
			}
			
			String[] catMap = getCategory(catNo);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}else{
				pdb.setCategoryId(0);
			}
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
			
		}
		if(pdCnt < 40) {
			return false;
		}
		return true;
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}	
}
