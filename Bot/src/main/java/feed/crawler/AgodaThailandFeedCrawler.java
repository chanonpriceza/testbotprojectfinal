package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AgodaThailandFeedCrawler extends FeedManager {
	
	private static boolean haveResult = true;
	private static final Logger logger = LogManager.getRootLogger();
	
	public AgodaThailandFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@SuppressWarnings("resource")
	@Override
	public void loadFeed() {
		
		String zipName = "56A3C1A2-0531-49F3-8720-D7D4B1410E41_TH.zip";
		String csvName = "56A3C1A2-0531-49F3-8720-D7D4B1410E41_TH.csv";
		String feedLink = "http://xml.agoda.com/hoteldatafiles/56A3C1A2-0531-49F3-8720-D7D4B1410E41_TH.zip";
		
		Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + zipName).toPath();
		try (InputStream in = new URL(feedLink).openStream()) {
			logger.info("Feed : Load : " + feedLink);
			Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
		}catch (ClosedByInterruptException e) {
			logger.error(e);
			return;
		}catch (IOException e) {
			logger.error(e);
		}
		
		try(FileInputStream fis = new FileInputStream(BaseConfig.FEED_STORE_PATH + "/" + zipName);
			ZipInputStream zis = new ZipInputStream(fis)){
			ZipEntry entry = null;
			while(!Thread.currentThread().isInterrupted() && (entry = zis.getNextEntry()) != null) {
//				System.out.println(entry.getName());
				if(entry.getName().equals(csvName)) {
					try(InputStreamReader isr = new InputStreamReader(zis);
				 	BufferedReader br = new BufferedReader(isr)) {
						String line = null;
						boolean isFirstRow = true;
						while (!Thread.currentThread().isInterrupted() && (line = br.readLine()) != null) {		
							try{
								StringReader lineReader = new StringReader(line);
								CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
								for (CSVRecord csvRecord : record) {
							
									if(isFirstRow){
										isFirstRow = false;
										continue;
									}
									try{
										parseCSV(csvRecord);
									}catch(Exception e){
										continue;
									}
								}
							}catch(Exception e){
								logger.warn("Can't parse line data : " + line);
							}
						}	
					}
					break;
				}
			}
		}catch(Exception e) {
			logger.error(e);
		}

		if (haveResult) 
			BaseConfig.FEED_FILE = new String[] { BaseConfig.FEED_STORE_PATH + "/mockFeed.xml" };
		
		
	}
	
	private static int itemCount = 0;
	private static int successCount = 0;
	private static int recordWrongLength = 0;
	private static int recordContentFail = 0;
	private static int noCatCount = 0;
	
	
	private void parseCSV(CSVRecord data) {
		try{
			itemCount++;
			if(successCount > 0 && successCount % 5000  == 0) {
				logger.info("process "+successCount);
			}
			
			if(itemCount%50000==0) {
				logger.info("itemCount: "+itemCount+" successCount: "+successCount+" "
						+ "recordWrongLength: "+recordWrongLength+" recordContentFail: "+recordContentFail+""
						+ " noCatCount: "+recordContentFail+" noCatCount: "+noCatCount);
			}
			
			if(data == null || data.size() < 39) {
				recordWrongLength++;
				return;
			}		
			
			String hotel_name = data.get(5);
			String city = data.get(11);
			String country = data.get(13);
			String desc = data.get(30);
			
			String price = data.get(31);
			
			String pictureUrl = data.get(25);
			
			String productUrl = data.get(18);
			String city_id = data.get(34);
			String hotel_translated_name = data.get(7);
			
			if( StringUtils.isBlank(productUrl) || StringUtils.isBlank(price) ){
				recordContentFail++;	
				return;
			}
	
			ProductDataBean pdb = new ProductDataBean();

			String name = "";
			name  = hotel_translated_name+" "+city+", "+country+" ("+hotel_name+")";
			String url = productUrl;
			
			
			pdb.setName(name);
			pdb.setDescription(desc);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			pdb.setPictureUrl(pictureUrl);
			pdb.setUrl(url);
			pdb.setUrlForUpdate(url);
			
			String[] mapping = getCategory(city_id);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
				noCatCount++;
			}
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		
		}catch(Exception e){
			logger.error(e);
		}
	}


	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
