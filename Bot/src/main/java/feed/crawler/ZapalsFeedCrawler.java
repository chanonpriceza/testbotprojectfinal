package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ZapalsFeedCrawler extends FeedManager{

	public ZapalsFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.zapals.com/feeds/ia_product_us.xml"
		};
	}
	
	public ProductDataBean parse(String html) {
	
		final double USD_TO_THB = 34.37;
		html = html.replace(" xmlns:g=\"http://base.google.com/ns/1.0\"", "");
		String productName = FilterUtil.getStringBetween(html, "<g:title>", "</g:title>");		
		String productPrice = FilterUtil.getStringBetween(html, "<g:price>", "</g:price>");
		String productUrl = FilterUtil.getStringBetween(html, "<g:link>", "</g:link>").replace("?utm_source=InvolveAsia", "");
		String productImageUrl = FilterUtil.getStringBetween(html, "<g:image_link>", "</g:image_link>");
		String productCatId = FilterUtil.getStringBetween(html, "<g:google_product_category>", "</g:google_product_category>");
		String productDesc = FilterUtil.getStringBetween(html, "<g:description>", "</g:description>");
		String productExpire = FilterUtil.getStringBetween(html, "<g:availability>", "</g:availability>");
		String productId = FilterUtil.getStringBetween(html, "<g:id>", "</g:id>");
		
		if(productExpire.equals("out of stock") || productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 || productId.trim().length() == 0) {
			return null;			
		}
						
		ProductDataBean pdb = new ProductDataBean();	
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		pdb.setName(productName.trim()+" ("+productId+")");			
		
		if(productId.trim().length() > 0) {
			pdb.setRealProductId(productId.trim());
		}

		pdb.setPrice(Math.round(USD_TO_THB * FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice))));
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);
		}
		
		pdb.setUrlForUpdate(productUrl);
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));	
			pdb.setKeyword(catMap[1]);
		} 

		if(productDesc.length() > 0){
			pdb.setDescription(productDesc);
		}
		
		return pdb;
		
	}
		
}
