package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;

import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShopeeFlashSaleFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private final int limit = 100;
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
	private boolean nextPage = true;
	private boolean error = false;
	private boolean haveResult = false;
	
	public ShopeeFlashSaleFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		crawlByProductList();
		crawlByWebPage();
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void crawlByProductList() {
		
		Set<String> urlInPattern = getCategoryKeySet();
		if(urlInPattern != null && urlInPattern.size() > 0) {
			for (String url : urlInPattern) {
				if(url.contains("-i.") || url.contains("/product/")) {
					
					String shopId = "";
					String itemId = "";
					if(url.contains("/universal-link/product/")) {
						String tmp = FilterUtil.getStringBetween(url, "/universal-link/product/", "?");
						shopId = FilterUtil.getStringBefore(tmp, "/", "");
						itemId = FilterUtil.getStringAfter(tmp, "/", "");
					} else if (url.contains("/universal-link/")){
						String tmp = FilterUtil.getStringBetween(url, "-i.", "?");
						shopId = FilterUtil.getStringBefore(tmp, ".", "");
						itemId = FilterUtil.getStringAfter(tmp, ".", "");
					} else if (url.contains("/product/")) {
						String tmp = FilterUtil.getStringAfter(url, "/product/", "");
						shopId = FilterUtil.getStringBefore(tmp, "/", "");
						itemId = FilterUtil.getStringAfter(tmp, "/", "");
						itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
						itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
						itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
					} else {
						String tmp = FilterUtil.getStringAfter(url, "-i.", "");
						shopId = FilterUtil.getStringBefore(tmp, ".", "");
						itemId = FilterUtil.getStringAfter(tmp, ".", "");
						itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
						itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
						itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
					}
					
					if(StringUtils.isNotBlank(shopId) && StringUtils.isNotBlank(itemId)) {
						String newUrl = "https://shopee.co.th/api/v2/item/get?itemid=" + itemId + "&shopid=" + shopId;
						String[] response = HTTPUtil.httpRequestWithStatus(newUrl, "UTF-8", true);
						if(response == null || response.length != 2) continue;
						try {
							String json = response[0];
							
							JSONObject obj = (JSONObject) new JSONParser().parse(json);
							if(obj == null) continue;
							
							JSONObject itemObj = (JSONObject) obj.get("item");
							if(itemObj == null) continue;
							
							String tempPrice = StringUtils.defaultString(String.valueOf(itemObj.get("price")), "");
							String resultName = StringUtils.defaultString(String.valueOf(itemObj.get("name")), "");
//							String resultDesc = StringUtils.defaultString(String.valueOf(itemObj.get("description")), "");
							String resultImage = StringUtils.defaultString(String.valueOf(itemObj.get("image")), "");
							String resultPrice = "0";

							if(NumberUtils.isCreatable(tempPrice)) {
								long priceFull = Long.parseLong(tempPrice);
								long priceReal = priceFull / 100000;
								if(priceReal > 0) resultPrice = String.valueOf(priceReal);
							}
							
							resultName = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(resultName).trim());
							
							String resultUrl = ""
									+ "https://shopee.co.th/universal-link/product/" + shopId + "/" + itemId
									+ "?smtt=9"
									+ "&deep_and_deferred=1"
									+ "&pid=priceza_int"
									+ "&c=datafeed"
									+ "&af_click_lookback=7d"
									+ "&is_retargeting=true"
									+ "&af_reengagement_window=7d"
									+ "&af_installpostback=false"
									+ "&clickid=(xxxxx)"
									+ "&utm_source=priceza"
									+ "&utm_medium=affiliates"
									+ "&utm_campaign=datafeed"
									;
							
							String suffix = "";
							if(StringUtils.isNotBlank(itemId)) {
								suffix = " ("+itemId+")";
							}
							
							ProductDataBean resultBean = new ProductDataBean();
							resultBean.setPrice(FilterUtil.convertPriceStr(resultPrice));
							resultBean.setName(resultName+suffix);
							resultBean.setRealProductId(itemId);
//							resultBean.setDescription(StringEscapeUtils.escapeHtml4(resultDesc));
							resultBean.setUrl(resultUrl);
							resultBean.setUrlForUpdate(shopId + "." + itemId);
							resultBean.setPictureUrl("https://cf.shopee.co.th/file/" + resultImage);
							
							String result = mockResult(resultBean);
							if(StringUtils.isNotBlank(result)) {
								try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
									pwr.println(result);
									haveResult = true;
								}catch(Exception e) {
									logger.error(e);
								}
							}
							
						} catch(Exception e) {
							logger.error(e);
						}
					}
				}
			}
		}
	}
	
	private void crawlByWebPage() {
		String flashSaleUrl = "https://shopee.co.th/api/v2/flash_sale/get_items?offset=0&limit=100&filter_soldout=true";
		String[] responseMain = HTTPUtil.httpRequestWithStatus(flashSaleUrl, "UTF-8", false);
		if(responseMain == null || responseMain.length != 2) return;
		String productData = responseMain[0];
		if(StringUtils.isBlank(productData)) return;
		String promotionId = getPromotionId(productData);
		String cookies = getCookies();
		int page = 0;
		do {
			nextPage = false;
			String requestUrl = "https://shopee.co.th/api/v2/flash_sale/get_items?offset=" + page * limit + "&limit=" + limit + "&promotionid=" + promotionId;
			String data = httpRequest(requestUrl, page, cookies, promotionId);
			parseJSON(data);
			page++;

		} while (!Thread.currentThread().isInterrupted() && nextPage && !error);
	}
	
	
	private String getPromotionId(String data) {
		String promotionId = "";
		try {
			JSONObject allObj = (JSONObject) new JSONParser().parse(data);
			allObj = (JSONObject) allObj.get("data");
			JSONArray jArr = (JSONArray) allObj.get("items");
			for (Object object : jArr) {
				JSONObject jObj = (JSONObject) object;
				promotionId = jObj.get("promotionid").toString();
				if (StringUtils.isNotBlank(promotionId)) {
					break;
				}
			}
		} catch (Exception e) {
		}
		return promotionId;
	}

	private String httpRequest(String url, int page, String cookie, String promotionId) {
		URL u = null;
		HttpURLConnection conn = null;

		try {

			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setRequestProperty("cookie", cookie);
			conn.setRequestProperty("referer", "https://shopee.co.th/flash_sale?offset=" + page * limit + "&promotionId=" + promotionId);
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);

			if (conn.getResponseCode() == 200) {
				try (InputStream is = conn.getInputStream();
						InputStreamReader isr = new InputStreamReader(is);
						BufferedReader brd = new BufferedReader(isr);) {

					StringBuilder rtn = new StringBuilder(5000);
					String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						rtn.append(line);
					}

					return rtn.toString();
				} catch (IOException e) {
					logger.error(e);
					BotUtil.consumeInputStreamQuietly(conn.getErrorStream());
				}
			} else {
				BotUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

	private void parseJSON(String data) {
		try {

			JSONObject allObj = (JSONObject) new JSONParser().parse(data);
			allObj = (JSONObject) allObj.get("data");
			JSONArray jArr = (JSONArray) allObj.get("items");

			for (Object object : jArr) {

				JSONObject jObj = (JSONObject) object;
				String productId = jObj.get("itemid").toString();
				String productName = (String) String.valueOf(jObj.get("name"));
				String productPrice = (String) String.valueOf(jObj.get("price"));
				String basePrice = (String) String.valueOf(jObj.get("price_before_discount"));
				String productImageUrl = (String) String.valueOf(jObj.get("image"));
				String dataShopId = (String) String.valueOf(jObj.get("shopid"));
				String productDesc = (String) String.valueOf(jObj.get("description")).trim();
				String productNameUrl = "";

				productName = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(productName).trim());
				productNameUrl = productName.replaceAll("%", "");
				productName = productName + " (" + productId + ")";

				String linkName = productNameUrl + "-i." + dataShopId + "." + productId;
				String resultUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
				
				productImageUrl = productImageUrl.replace("&amp;", "&");

				if (StringUtils.isBlank(resultUrl) || StringUtils.isBlank(productName)) {
					continue;
				}
				ProductDataBean pdb = new ProductDataBean();
				

				pdb.setName(productName);
				if (StringUtils.isNotBlank(productPrice) && NumberUtils.isCreatable(productPrice)) {
					if (Double.parseDouble(productPrice) != 0) {
						pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)) / 100000);
					} else {
						pdb.setPrice(BotUtil.CONTACT_PRICE);
						error = true;
					}
				} else {
					pdb.setPrice(BotUtil.CONTACT_PRICE);
					error = true;
				}
				pdb.setUrl(resultUrl);
				pdb.setUrlForUpdate(dataShopId + "." + productId);
				pdb.setRealProductId(productId);

				if (StringUtils.isNotBlank(basePrice)) {
					pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)) / 100000);
				}

				if (StringUtils.isNotBlank(productImageUrl)) {
					pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
				}

				if (StringUtils.isNotBlank(productDesc) && !"null".equals(productDesc)) {
					productDesc = StringEscapeUtils.unescapeHtml4(productDesc).trim();
					productDesc = FilterUtil.toPlainTextString(productDesc);
					pdb.setDescription(productDesc);
				}
				
				String result = mockResult(pdb);
				if(StringUtils.isNotBlank(result)) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println(result);
						haveResult = true;
					}catch(Exception e) {
						logger.error(e);
					}
				}
				
				nextPage = true;
			}
		} catch (Exception e) {
		}
	}

	private String getCookies() {
		String cookie = null;

		URL u = null;
		HttpURLConnection conn = null;

		try {
//			u = new URL("https://shopee.co.th/shop/" + shopId + "/");
			u = new URL("https://shopee.co.th/api/v1/account_info/");
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(1800000);
			conn.setReadTimeout(1800000);

			cookie = conn.getHeaderField("Set-Cookie");

		} catch (Exception e) {
			logger.error(e);
		}

		return cookie;
	}

	// remove non Basic Multilingual Plane (Unicode)
	public String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty())
			return null;		
		
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
