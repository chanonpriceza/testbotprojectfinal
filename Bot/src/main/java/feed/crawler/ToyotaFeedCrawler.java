package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ToyotaFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private boolean haveResult = false;
	
	public ToyotaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}

	@Override
	public void loadFeed() {
		String[] response = HTTPUtil.httpRequestWithStatus("https://www.xn--m3caabp7cdc6lmgb0g7a.com/%E0%B8%A3%E0%B8%B8%E0%B9%88%E0%B8%99%E0%B8%A3%E0%B8%96%E0%B9%82%E0%B8%95%E0%B9%82%E0%B8%A2%E0%B8%95%E0%B9%89%E0%B8%B2-home.page", "UTF-8", false);
		if(response == null || response.length != 2) 
			return;
		String data = response[0];
		if(StringUtils.isBlank(data)) 
			return;
		parseHTML(data);
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	public void parseHTML(String data){
		List<String> cars = FilterUtil.getAllStringBetween(data,"<td class=\"car\" style=\"text-align: center;\">","</td>");
		for(String car:cars){
			String name = "";
			
			if(StringUtils.isBlank(name)){
				name =  FilterUtil.getStringBetween(car, "<h2 style=\"text-align: center;\">", "</h2>");
			}
			
			if(StringUtils.isBlank(name)){
				name =  FilterUtil.getStringBetween(car, "<h2>", "</h2>");
			}

			name = FilterUtil.toPlainTextString(name);
			name = FilterUtil.removeSpace(name);
			name = name.replaceAll("&nbsp;","");
			String price = FilterUtil.getStringBetween(car,"เริ่มต้น","บาท");
			if(StringUtils.isBlank(price)){
				price = FilterUtil.getStringBetween(car,"เรื่มต้น","บาท");
			}
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
			String url = FilterUtil.getStringBetween(car,"<a href=\"","\"");
			if(url.contains("/page/")){
				url = url.replaceAll(".html",".page");
				url = url.replaceAll("/page/","");
				url = "http://www.รถยนต์โตโยต้า.com/"+url;
			}
			String image = FilterUtil.getStringBetween(car,"src=\"","\"");
			if(!image.contains("http://toyotacar.justmakeweb.com/")){
				image = "http://www.xn--m3caabp7cdc6lmgb0g7a.com/"+image;
			}
			if(StringUtils.isBlank(name)||StringUtils.isBlank(price)||StringUtils.isBlank(url)||StringUtils.isBlank(image)){
				continue;
			}
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(name);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			pdb.setUrl(url);
			pdb.setCategoryId(70101);
			pdb.setPictureUrl(image);
			
			String mockData = mockResult(pdb);
			if(StringUtils.isNotBlank(mockData)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(mockData);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}	
	
}
