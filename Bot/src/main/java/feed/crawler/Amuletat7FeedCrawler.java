package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Amuletat7FeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public Amuletat7FeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		String fileName = "mockFeed.xml";
		List<String> allFile = new ArrayList<>();
		Set<String> allKey = getCategoryKeySet();
		for(String key : allKey){
			boolean haveResult = false;
			int page = 0;
			String[] data = new String[2];
			String rtn = "";
			String beforData= "";
			String currentCat = key;
			
			int pageMax = 0;
			do{
				haveResult = false;
				beforData = rtn;
				data = HTTPUtil.httpRequestWithStatus(key,"UTF-8",false);
				
				rtn = data[0];
				rtn = StringEscapeUtils.unescapeJava(rtn);
				pageMax = (BotUtil.stringToInt(FilterUtil.removeCharNotPrice(FilterUtil.getStringBetween(rtn,"<span class=\"how-many\">","</span>")),0)/90)+1;
				rtn = FilterUtil.getStringBetween(rtn,"<div class=\"row product_grid outer \">","<div class=\"col-md-8 col-xs-12 hidden-print\">");
				List<String> productListString = FilterUtil.getAllStringBetween(rtn,"<div class=\"product-item col-xs-6 col-sm-4 xxs-padding-left-right-5\">","</strong>");
				
				for(String productString:productListString){
					productString += "</strong>";
					String result = mockResult(productString,currentCat);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + fileName),  true));){
							pwr.println(result);
							haveResult = true;
						}catch(Exception e) {
							logger.error(e);
						}
					}
				}
				if(haveResult)
					allFile.add(BaseConfig.FEED_STORE_PATH + "/" + fileName);
				if(beforData.equals(rtn)||StringUtils.isBlank(rtn))
					break;
				page++;
			}while(!Thread.currentThread().isInterrupted() && rtn!=null && page<pageMax);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/" + fileName};

	}

	private String mockResult(String input,String cat){

		String name = FilterUtil.getStringBetween(input,"<a title=\"","\"");
		String url = FilterUtil.getStringBetween(input,"href=\"","\"");
		String image = FilterUtil.getStringBetween(input,"data-src=\"","\"");
		String price = FilterUtil.getStringBetween(input," <strong>","</strong>");
		String desc = FilterUtil.getStringBetween(input,"<div class=\"description\">","</div>");
		String id 	= FilterUtil.getStringBetween(input,"productId=","&");
		price = FilterUtil.removeCharNotPrice(price);
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(StringUtils.isBlank(price)) return null;
		name += " ("+id+")";
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(StringUtils.isNotBlank(price)) result.append("<price>"+price+"</price>");
		if(StringUtils.isNotBlank(image)) result.append("<image>"+image+"</image>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(cat)) result.append("<cat>"+cat+"</cat>");
		if(StringUtils.isNotBlank(id)) result.append("<readProductId>"+id+"</readProductId>");
		result.append("</product>");
		
		return result.toString();
		
	}

	public ProductDataBean parse(String xml) {
		
		String productName 		= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice 	= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDesc 		= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image>", "</image>").trim();
		String id				 = FilterUtil.getStringBetween(xml, "<readProductId>", "</readProductId>").trim();
		String productCatId 	= FilterUtil.getStringBetween(xml, "<cat>", "</cat>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		pdb.setUrl("https://www.amulet24.com"+productUrl);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		
		if (!productPictureUrl.isEmpty()) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!productDesc.isEmpty()){
			pdb.setDescription(productDesc);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}

		return pdb;
		
	}

}
