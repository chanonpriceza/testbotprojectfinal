package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LineShoppingJSONReader extends FeedManager {

	private static final Logger logger = LogManager.getRootLogger();
	
	private static final String accessKey = "AKIARHI7IWM4T4JIP775";
	private static final String secretKey = "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";
	private static final String bucketName = "lineshopping-feed";
	private static Map<String, String> storeInfoMap;
	private static List<String> allFile;
	private static JSONParser jsonParser;
	private static AmazonS3 s3client;
	private static boolean haveResult;
	private static int countPare = 0;
	private static JSONParser parser = new JSONParser();
	
	public LineShoppingJSONReader() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		AWSCredentials credentials = new BasicAWSCredentials(accessKey,secretKey);
		s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_SOUTHEAST_1).build();
		jsonParser = new JSONParser();
		storeInfoMap = new HashMap<>();
		
		allFile = new ArrayList<>();
		String bucketName = "lineshopping-feed";
		ObjectListing objectListing = s3client.listObjects(bucketName);
		for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
		    String fileName = os.getKey();
		    if(fileName.contains("prod/product/") || fileName.contains("prod/store/")) {
		    	allFile.add(fileName);
		    }
		}
		
	return null;
	}
	
	@Override
	public void clearFeed() throws SQLException {
		
	}
	
	@Override
	public void loadFeed() {
		
		// LOAD ALL
		for (String feed : allFile) {
			String fileName = FilterUtil.getStringAfterLastIndex(feed, "/", feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			try {
				S3Object s3object = s3client.getObject(bucketName, feed);
				S3ObjectInputStream inputStream = s3object.getObjectContent();
				FileUtils.copyInputStreamToFile(inputStream, new File(destination));
			} catch(Exception e) {
				logger.error(e);
			}
		}
		
		// READ STORE INFO FIRST
		for (String feed : allFile) {
			if(feed.contains("prod/store/")) {
				String fileName = FilterUtil.getStringAfterLastIndex(feed, "/", feed);
				String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
				try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(destination),"UTF-8"))){
					String tmp = "";
					char[] chars = new char[1024];
					while(br.read(chars) > 0) {
						tmp += String.valueOf(chars);
						List<String> jsonList = FilterUtil.getAllStringBetween(tmp, "{", "}");
						if(jsonList != null && jsonList.size() > 0) {
							for (String jsonTxt : jsonList) {
								parseStoreJSON("{"+jsonTxt+"}");
							}
							tmp = FilterUtil.getStringAfterLastIndex(tmp, "}", tmp);
						}
					}
				} catch(Exception e) {
					logger.error(e);
				}
				logger.info("Done store info from " + fileName);
			}
		}
		
		// READ PRODUCT INFO AND WRITE MOCK FEED
		for (String feed : allFile) {
			if(feed.contains("prod/product/")) {
				String fileName = FilterUtil.getStringAfterLastIndex(feed, "/", feed);
				String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
				try {
					JSONArray ob = (JSONArray)parser.parse(new InputStreamReader(new FileInputStream(destination),"UTF-8"));
					PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));
					int count=0, ojbCount=0;
					for(Object o:ob) {
						count ++;
						parseProductJSON(String.valueOf(o), pwr);
					}
					logger.info("LineCount = " + count);
					logger.info("ObjectCount = " + ojbCount);
				} catch(Exception e) {
					logger.error(e);
				}
				logger.info("Done mock file for " + fileName);
			}
		}
		
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}
	
	private void parseStoreJSON(String jsonTxt) {
		try {
			JSONObject json = (JSONObject) jsonParser.parse(jsonTxt);
			if(json == null) return;
			
			String shopId	= (String) json.get("shop_id");
			String shopName	= (String) json.get("shop_name");
			
			if(StringUtils.isNotBlank(shopId) && StringUtils.isNotBlank(shopName)) {
				storeInfoMap.put(shopId, shopName);
			}
			
		} catch(Exception e) {
			logger.error(e);
		}
	}
	
	private void parseProductJSON(String jsonTxt, PrintWriter pwr) {
		try {
			JSONObject json = (JSONObject) jsonParser.parse(jsonTxt);
			if(json == null)
				return;
			
			String id		= StringUtils.defaultIfBlank((String) json.get("id"), "");
			String name		= StringUtils.defaultIfBlank((String) json.get("name"), "");
			Object iprice	= (Object) json.get("price");
			Object ibPrice	= (Object) json.get("base_price");
			String desc		= StringUtils.defaultIfBlank((String) json.get("description"), "");
			String subcat	= StringUtils.defaultIfBlank((String) json.get("subcategory_id"), "0");
			String url		= StringUtils.defaultIfBlank((String) json.get("url"), "");
			String image	= StringUtils.defaultIfBlank((String) json.get("image_url"), "");
			String shopId	= StringUtils.defaultIfBlank((String) json.get("shop_id"), "");
			
			
			Double price = Double.parseDouble(String.valueOf(iprice));
			Double bPrice = Double.parseDouble(String.valueOf(ibPrice));
			
			String finalName = "";
			finalName = FilterUtil.toPlainTextString(name);
			if(storeInfoMap.get(shopId) != null) {
				finalName = finalName + " by " + storeInfoMap.get(shopId);
			}
			finalName = FilterUtil.removeEmoji(finalName);
			finalName = finalName + " (" + id + ")";
			
			String finalDesc = "";
			finalDesc = FilterUtil.toPlainTextString(desc);
			finalDesc = finalDesc.replaceAll("\r\n", " ");
			finalDesc = finalDesc.replaceAll("\\s+", " ");
			finalDesc = FilterUtil.removeEmoji(finalDesc);
			finalDesc = FilterUtil.limitString(finalDesc, 200);
			finalDesc = finalDesc + " [" + shopId + "-" + subcat + "]";
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(finalName);
			pdb.setPrice(price);
			pdb.setBasePrice(bPrice);
			pdb.setDescription(finalDesc);
			pdb.setPictureUrl(image);
			pdb.setUrl(url);
			pdb.setUrlForUpdate(id);
			pdb.setRealProductId(id);
			
			String[] catMap = getCategory(subcat);
			if(catMap != null && catMap.length > 0) {
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);
			}
			
			String mockData = mockResult(pdb);
			if(StringUtils.isNotBlank(mockData)) {
				pwr.println(mockData);
				haveResult = true;
			}
			
		} catch(Exception e) {
			logger.error(e);
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	static int count = 0;
	public ProductDataBean parse(String xml) {
		countPare++;
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();
		if(!productDesc.contains("17980-"))
			return null;
		if(productRealProductId.contains("318765700") || productRealProductId.contains("318765739")) {
			logger.error("Skip!!");
			return null;	
		}
		//logger.info(countPare+" "+productRealProductId);

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		return pdb;
	}
	
}
