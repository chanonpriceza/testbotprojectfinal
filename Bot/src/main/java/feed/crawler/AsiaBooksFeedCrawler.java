package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AsiaBooksFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private static boolean goNextPage = true;
	private static boolean haveResult = false;
	
	public AsiaBooksFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		int page = 1, nonData = 0;
		while(!Thread.currentThread().isInterrupted() && goNextPage){
			goNextPage = false;
			String url = "https://www.asiabooks.com/AsiaBookProductFeed?record=50&page=" + page;
			try{
				if(nonData == 0){
					logger.info("Craw " + url);
				}
				String[] result = HTTPUtil.httpRequestWithStatusIgnoreCookies(url, "UTF-8", false);
				if(result == null || result.length != 2) {
					if(nonData < 10) {
						nonData++;
						goNextPage = true;
						logger.info("Retry Craw" + nonData + "  " +url);
						Thread.sleep(3000);
						continue;
					}else {
						break;
					}
				} else {
					processResult(result[1]);
				}
			}catch(Exception e){
				logger.info("Error Craw " + url);
				logger.error(e);
			}
			page++;
			if(page >= 5000){
				break;
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void processResult(String data) {
		List<String> product = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
		if(product != null && product.size() > 0) {	
			for (String s : product) {
				processProductData(s);
			}
		}
	}
	
	private void processProductData(String xml){
		
//		  <product>
//		    <isbn><![CDATA[9780007557288]]></isbn>
//		    <name><![CDATA[SMITH OF WOOTTON MAJOR]]></name>
//		    <description><![CDATA[A charming new pocket edition of one of Tolkien's major pieces of short fiction, and his only finished work dating from after publication of The Lord of the Rings. What began as a preface to ]]></description>
//		    <retailprice><![CDATA[450]]></retailprice>
//		    <price><![CDATA[450]]></price>
//		    <producturl><![CDATA[https://www.asiabooks.com/smith-of-wootton-major-4590.html]]></producturl>
//		    <imageurl><![CDATA[https://www.asiabooks.com/media/catalog/product//9/7/9780007557288A_2.png]]></imageurl>
//		    <categoryid1><![CDATA[Book]]></categoryid1>
//		  </product>
		
		xml = removeCDATATag(xml);
		
		String isbn = FilterUtil.getStringBetween(xml, "<isbn>", "</isbn>").trim();
		String name = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();		
		String description = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String price = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();		
		String url = FilterUtil.getStringBetween(xml, "<producturl>", "</producturl>").trim();
		String img = FilterUtil.getStringBetween(xml, "<imageurl>", "</imageurl>").trim();
		String catId = FilterUtil.getStringBetween(xml, "<categoryid1>", "</categoryid1>").trim();
	
		if(url.isEmpty() || name.isEmpty() || price.isEmpty()) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();

		name = StringEscapeUtils.unescapeHtml4(name);
		
		pdb.setName(name);
		pdb.setDescription(description);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		pdb.setUrl(url);			
		pdb.setUrlForUpdate(url);
		
		if (!img.isEmpty()) {
			img = img.replace("&amp;", "&");
			pdb.setPictureUrl(img);
		}
		
		
		String[] catMap = getCategory(catId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[1], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}

		if(StringUtils.isNumeric(isbn) && isbn.length() == 13){
			pdb.setRealProductId(isbn);
			pdb.setUpc(isbn);
		}
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}
	

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String removeCDATATag(String value){
		if(StringUtils.isNotBlank(value)){
			value = value.replace("<![CDATA[", "").replace("]]>", "");
			value = FilterUtil.toPlainTextString(value);
		}
		return value;
	}
	
}
