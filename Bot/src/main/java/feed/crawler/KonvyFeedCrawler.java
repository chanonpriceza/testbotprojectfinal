package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KonvyFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public KonvyFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.konvy.com/pricezaFeed/product.php"
		};
	}
	
	
	public ProductDataBean parse(CSVRecord data){
		try{

			String id = data.get(0);
			String name = data.get(1);
			String price = data.get(2);
			String desc = data.get(3);			
			String catId = data.get(4);			
			String url  = data.get(6);
			String image = data.get(7);
			String bPrice = data.get(8);
			String status = data.get(9);
		
			if( status.contains("sold out") ||StringUtils.isBlank(name) ||  StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			
			pdb.setName(name);
			price = FilterUtil.removeCharNotPrice(price);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			
			if(StringUtils.isNotBlank(bPrice) && !bPrice.equals(price) ){
				bPrice = FilterUtil.removeCharNotPrice(bPrice);
				pdb.setBasePrice(FilterUtil.convertPriceStr(bPrice));
			}
			
			pdb.setUrl(url);
			
			if(!image.isEmpty()){
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(desc)){
				pdb.setDescription(desc);
			}
			
			if(StringUtils.isNotBlank(id)){
				pdb.setRealProductId(id);
			}
			
			String[] mapping = getCategory(catId);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} 

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}

}
