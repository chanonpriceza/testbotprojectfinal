package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class GearBestFeedCrawler extends FeedManager  {

	public GearBestFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		runURL();
		return linkList.stream().toArray(String[]::new);
	}
	
	@Override
	public void loadFeed() {
		String[] redirect = new String[BaseConfig.FEED_FILE.length];
		for (int i = 0;i<BaseConfig.FEED_FILE.length;i++) {
			String fileName = BotUtil.getFileNameURL(String.valueOf(i));
			logger.info("Feed : Load : " + BaseConfig.FEED_FILE[i]);
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName+i).toPath();
			redirect[i] = BaseConfig.FEED_STORE_PATH + "/" + fileName+i;
			try (InputStream in = new URL(BaseConfig.FEED_FILE[i]).openStream()) {
				surgeryJSONData(in,path);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
				processLog.addException(e.toString()+"-"+"GearBest",e);
			}
		}
		BaseConfig.FEED_FILE = redirect;
	}
	
	private static Set<String> nonCat = new HashSet<String>();
	
	@Override
	public ProductDataBean parse(JSONObject data) {

		String name = String.valueOf(data.get("product_title"));
		String price = String.valueOf(data.get("sale_price"));
		String url = String.valueOf(data.get("product_url"));
		String image = String.valueOf(data.get("image_url"));
		String cat = String.valueOf(data.get("category"));
		String base_price = String.valueOf(data.get("orig_price"));
		String productId = FilterUtil.getStringBetween(url,"pp_",".html");
		String idTmp = "";
		double price_r = FilterUtil.convertPriceStr(price);
		double baseprice_r = FilterUtil.convertPriceStr(base_price);
		double tmp = 0;
		
		
		if(baseprice_r < price_r) {
			tmp = price_r;
			price_r = baseprice_r;
			baseprice_r = tmp;
		}
		
		if(price_r == 0)
			price_r = baseprice_r;
		
		if(StringUtils.isNotBlank(productId)) {
			idTmp = " ("+productId+")";
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+idTmp);
		pdb.setPrice(price_r);
		pdb.setBasePrice(baseprice_r);
		pdb.setUrl(url);
		pdb.setPictureUrl(image);
		pdb.setUrlForUpdate(productId);
		
		String[] catMap = getCategory(cat);
		
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			if(!nonCat.contains(cat)) { 
				nonCat.add(cat);
				logger.info("Non cat: "+cat);
			}else 
				logger.info("Non cat: "+cat);
			pdb.setCategoryId(0);
		}

		return pdb;
	}
	
	private static final String mainAPI = "https://affiliate.gearbest.com/api/promotions/list-product-creative?api_key=[APIKEY]&time=[TIME]&type=[TYPE]&page=[PAGE]&sign=[SIGN]&lkid=[LKID]";
	private static final String mainSign = "[APISECRET][PARAM][APISECRET]";
	private static final String apiKey = "1366135";
	private static final String apiSecret = "Ue5pzHlHPDQ";
	private static final String lkid = "78496144";
	private static Set<String> linkList = new HashSet<String>();
	private static int max_page = 0;
	private static JSONParser json = new JSONParser();
	
	private void surgeryJSONData(InputStream in,Path p) throws Exception{
		InputStreamReader reader = new InputStreamReader(in);
		JSONObject o = (JSONObject)json.parse(reader);
		o = (JSONObject) o.get("data");
		JSONArray array = (JSONArray) o.get("items");
		InputStream createInputStream = new ByteArrayInputStream(array.toJSONString().getBytes(Charset.forName("UTF-8")));
		Files.copy(createInputStream, p,StandardCopyOption.REPLACE_EXISTING);
	}
	
	
	public void runURL() {
		try {
			
			String genMaxPage = createLink(1);
			InputStreamReader reader = new InputStreamReader(new URL(genMaxPage).openStream());
			JSONObject o = (JSONObject)json.parse(reader);
			o = (JSONObject) o.get("data");
			max_page = BotUtil.stringToInt(String.valueOf(o.get("total_pages")),0);
			 
			for(int i =1 ;i <= max_page;i++) {
				linkList.add(createLink(i));
			}
			
		}catch (Exception e) {
			logger.error("Error",e);
		}
		
	}
	
	
	
	private String createLink(int page) {

		String time = String.valueOf(Calendar.getInstance().getTimeInMillis());
		String sign = "";
		
		
		String mainParam = "api_key"+apiKey+"lkid"+lkid+"page"+page+"time"+time+"type1";
		sign = mainSign
				.replace("[APISECRET]", apiSecret)
				.replace("[PARAM]", mainParam)
				;
		sign = toMD5(sign);
		sign = sign.toUpperCase();
		String link = mainAPI
				.replace("[APIKEY]", apiKey)
				.replace("[TIME]", time)
				.replace("[TYPE]", "1")
				.replace("[PAGE]", String.valueOf(page))
				.replace("[SIGN]", sign)
				.replace("[LKID]", lkid)
				;
		
		return link;
		
	}
	
	private  String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	

	


	
	
}


