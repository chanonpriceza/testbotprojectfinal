package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import utils.HTTPUtil;

public class JDCentralFlashSaleFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	
	public JDCentralFlashSaleFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://api.jd.co.th/client.action?functionId=pcBabelGenericChannel&client=pc&clientVersion=1.0.0&screen=1250*2208&lang=th_TH&area=&body=%7B%22activityId%22:%222URjovcCvfvAsi7oZxgrpFfQHmKW%22,%22pageNum%22:%22-1%22,%22innerAnchor%22:%22%22,%22innerExtId%22:%22%22,%22hideTopFoot%22:%22%22,%22innerLink%22:%22%22,%22userIp%22:%220.0.0.0%22,%22mitemAddrId%22:%22%22,%22geo%22:%7B%22lng%22:%22%22,%22lat%22:%22%22%7D,%22flt%22:%22%22,%22pin%22:%22%22,%22showCount%22:%220%22,%22mtm_source%22:%22%22,%22mtm_subsource%22:%22%22,%22USER_FLAG_CHECK%22:%22%22%7D"
		};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			if(feed.startsWith("https://api.jd.co.th/client.action?")){
				String[] response = HTTPUtil.httpRequestWithStatus(feed, "UTF-8", false);
				if(response == null || response.length != 2) continue;
				parseAPI(response[0]);
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void parseAPI(String data) {
		
		try {
			
			JSONObject obj = (JSONObject) new JSONParser().parse(data);
			if(obj == null) return;
			
			JSONArray floorArr = (JSONArray) obj.get("floorList");
			if(floorArr == null || floorArr.size() == 0) return;
			
			for (Object floor : floorArr) {
				
				JSONObject floorObj = (JSONObject) floor;
				
				JSONArray waresListArr = (JSONArray) floorObj.get("waresList");
				if(waresListArr == null || waresListArr.size() == 0) continue;
				
				for (Object wares : waresListArr) {
					
					JSONObject waresObj = (JSONObject) wares; 
					
					JSONArray groupInfoListArr = (JSONArray) waresObj.get("groupInfoList");
					if(groupInfoListArr == null || groupInfoListArr.size() == 0) continue;
					
					for (Object groupInfo : groupInfoListArr) {
						
						JSONObject groupInfoObj = (JSONObject) groupInfo;
						
						String name = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("name")), null);
						String pPrice = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("pPrice")), null);
						String pcpPrice = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("pcpPrice")), null);
						String skuId = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("skuId")), null);
						String pictureUrl = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("pictureUrl")), null);
						
						if(StringUtils.isBlank(name) || StringUtils.isBlank(pPrice) || StringUtils.isBlank(skuId)) continue;
						String url = "https://item.jd.co.th/" + skuId + ".html";
//						String picture = "https://img.jd.co.th/babelglobal/s366x366_" + pictureUrl;
						String picture = "https://img.jd.co.th/n0/s340x340_" + pictureUrl;

						String timeBegin = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("timeBegin")), null);
						String timeEnd = StringUtils.defaultIfBlank(String.valueOf((Object) groupInfoObj.get("timeEnd")), null);

						if(StringUtils.isNotBlank(timeBegin) && StringUtils.isNotBlank(timeEnd)) {
							Date date1, date2, date3;
							date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timeBegin);
							date2 = new Date();
							date3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timeEnd);
							if((date1.compareTo(date2) == -1) && (date2.compareTo(date3) == -1)) {
								ProductDataBean pdb = new ProductDataBean();
								pdb.setName(name + " (" + skuId + ")");
								pdb.setPrice(FilterUtil.convertPriceStr(pPrice));
								pdb.setBasePrice(FilterUtil.convertPriceStr(pcpPrice));
								pdb.setRealProductId(skuId);
								pdb.setUrl(url);
								pdb.setUrlForUpdate(skuId);
								pdb.setPictureUrl(picture);
								pdb.setCategoryId(500405);
								
								String result = mockResult(pdb);
								if(StringUtils.isNotBlank(result)) {
									try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
										pwr.println(result);
										haveResult = true;
									}catch(Exception e) {
										logger.error(e);
									}
								}
							}
						}
					}
				}
			}
			
			
			
		} catch(Exception e) {
			logger.error(e);
		}
		
	}
	

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty())
			return null;		
		
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
