package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.SocketFactory;

public class TemplateShopeeItemApiFeedCrawler extends FeedManager {
	
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
	protected static String shopId;
	protected static int fixcat;
	protected static String fixKeyword;
	private static int page = 0;
	private static final int PAGE_OFFSET = 100;
	private static String cookie;
	protected static String GET_PRODUCT_LINK = "https://shopee.co.th/api/v2/item/get?itemid={itemid}&shopid={shopId}";
	protected static JSONParser parser = new JSONParser();
	protected static Map<String,String[]> mapData = new HashMap<String,String[]>();


	public TemplateShopeeItemApiFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		shopId = BaseConfig.CONF_FEED_CRAWLER_PARAM;
		String[] inputData = shopId.split(",");
		
		if(inputData.length>=2) {
			shopId = inputData[0];
			fixcat = BotUtil.stringToInt(inputData[1],0);
		}else if(StringUtils.isNotBlank(BaseConfig.CONF_FIX_CATEGORY)) {
			fixcat =  BotUtil.stringToInt(BaseConfig.CONF_FIX_CATEGORY,0);
		}
		
		if (StringUtils.isBlank(shopId)) {
			return;
		}
		
		if(StringUtils.isNotBlank(BaseConfig.CONF_FIX_KEYWORD))
			fixKeyword = BaseConfig.CONF_FIX_KEYWORD;
		
		cookie = getCookies(shopId);
		if(StringUtils.isBlank(cookie)) return;
		
		getProductWithCat();
		parseProductNormal();
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}
	
	
	
	private void getProductWithCat(){
		boolean nextPage = false;
		int page = 0;
		
		if (!StringUtils.isBlank(shopId)) {
			if(StringUtils.isBlank(cookie)) return;
			
			Set<String> keySet = getCategoryKeySet();
			for (String catId : keySet) {
				page = 0;
				String[] catMap = getCategory(catId);
				if(catMap == null || catMap.length != 2) continue;
				String pzCatId = catMap[0];
				if(!NumberUtils.isCreatable(pzCatId)) continue;
				
				boolean isFirstParamName = true;
				
				do {
					nextPage = false;
					
					enableUseProxy();
					if(isFirstParamName) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&shop_categoryids=" + catId;
						String data = httpRequestWithStatusIgnoreCookies2(url,page)[0];
						
						try {
							nextPage = mapCat(data,catMap);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
					}
					
					if(nextPage ==false) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&original_categoryid=" + catId;
						
						String data = httpRequestWithStatusIgnoreCookies2(url,page)[0];
						
						try {
							nextPage = mapCat(data,catMap);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
					}
					
					if(nextPage) page++ ;
					
				}while(!Thread.currentThread().isInterrupted() && nextPage);
				
			}
			
		}else {
			logger.error("Cant't get shop id.");
			return ;
		}
		
	}
	
	private boolean mapCat(String data,String[] cat) {
		boolean isCon = false;
		
		try{
			if(data.contains("\"bundle_deal_id\":,"))
				data = data.replace("\"bundle_deal_id\":,","");
			JSONObject allObj = (JSONObject) parser.parse(data);
			if(allObj==null)
				return false;
			JSONArray jArr = (JSONArray) allObj.get("items");
			if(jArr==null||jArr.size()==0)
				return false;
			
			for (Object object : jArr) {
				JSONObject jObj 		= (JSONObject) object;
				String productId 		= jObj.get("itemid").toString();
				mapData.put(productId, cat);
				isCon= true;
			}
				
		}catch (Exception e) {
			logger.error("Error",e);
			isCon = false;
		}
			
		return isCon;
			
		
	}
	
	
	private String getCookies(String shopId) {
		String cookie = null;
		
		URL u = null;
		HttpURLConnection conn = null;

		try {
//			u = new URL("https://shopee.co.th/shop/" + shopId + "/");
			u = new URL("https://shopee.co.th/api/v1/account_info/");
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(1800000);
			conn.setReadTimeout(1800000);
			
			cookie = conn.getHeaderField("Set-Cookie");
			
		}catch(Exception e) {
			logger.error("Error",e);
		}
		
		return cookie;
	}
	
	private void parseProductNormal() {
		boolean isContinue = true;
		do {
			String url = "https://shopee.co.th/api/v2/search_items/"
					+ "?by=ctime"
					+ "&order=desc"
					+ "&newest=" + page * PAGE_OFFSET
					+ "&limit="+PAGE_OFFSET
					+ "&skip_price_adjust=false"
					+ "&page_type=shop"
					+ "&match_id=" + shopId;
			getCookies(shopId);

			String data = httpRequestWithStatusIgnoreCookies2(url,page)[0];
			if(StringUtils.isBlank(data)){ break;}
			try {
				isContinue = getUrl(data);
			}catch(Exception e) {
				logger.error("Error",e);
			}
			page++ ;
		}while(isContinue&&page < 100);
	}
		
	private boolean getUrl(String data) {
		boolean isCon = false;
		
		try{
			if(data.contains("\"bundle_deal_id\":,"))
					data = data.replace("\"bundle_deal_id\":,","");
			
			JSONObject allObj = (JSONObject) parser.parse(data);
			if(allObj==null)
				return false;
			JSONArray jArr = (JSONArray) allObj.get("items");
			if(jArr==null||jArr.size()==0)
				return false;
			
			for (Object object : jArr) {
				JSONObject jObj 		= (JSONObject) object;
				String productId 		= jObj.get("itemid").toString();
				getProductDetail(productId);
				
				isCon= true;
			}
				
		}catch (Exception e) {
			logger.error("Error",e);
			isCon = false;
		}
		
		return isCon;
		
	}
	
	protected void getProductDetail(String productId) throws Exception {
		getProductDetail(productId,null);
	}
	
	protected void getProductDetail(String productId,String cat) throws Exception {
		
		String req = GET_PRODUCT_LINK.replace("{itemid}", productId).replace("{shopId}", shopId);
		String[] response = HTTPUtil.httpRequestWithStatus(req, "UTF-8", true);
		
		if(response==null||response.length==0) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		if(response[0]==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		JSONObject obj = (JSONObject) parser.parse(response[0]);
		
		obj = (JSONObject) obj.get("item");
		
		if(obj==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		String price = String.valueOf(obj.get("price"));
		//String description = String.valueOf(obj.get("description"));
		String basePrice = String.valueOf(obj.get("price_before_discount"));
		String name = String.valueOf(obj.get("name"));
		String productImageUrl =  String.valueOf(obj.get("image"));
		String productUrl 		= "";
		String productNameUrl 	= "";
		double priceDouble = FilterUtil.convertPriceStr(price) / 100000;
		double basePriceDouble = FilterUtil.convertPriceStr(basePrice) / 100000;
		productImageUrl = productImageUrl.replace("&amp;", "&");
		name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim()).trim();
		productNameUrl = name.replaceAll("%", "");
		String linkName = productNameUrl + "-i." + shopId + "." + productId;
		
		productUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
		//name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim());
		
		if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(name)) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+productId+")");
		pdb.setPrice(priceDouble);
		pdb.setBasePrice(basePriceDouble);
		//pdb.setDescription(description);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(shopId + "." + productId);
		pdb.setRealProductId(productId);
		
		if (StringUtils.isNotBlank(productImageUrl)) {
			pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
		}
		

		String[] mapping = mapData.get(productId);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			if(fixcat!=0) 
				pdb.setCategoryId(fixcat);
			else
			pdb.setCategoryId(0);
		}
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
			}catch(Exception e) {
				logger.error("Error",e);
			}
		}
	
		
		
	}



	protected String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getRealProductId()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	public String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
		proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("148.251.73.161", 60000));
		Authenticator authenticator = new Authenticator() {

	        public PasswordAuthentication getPasswordAuthentication() {
	            return (new PasswordAuthentication("priceza",
	                    "49sO7t2jgQ".toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	}
	 
	private final static int DEFAULT_TIMEOUT = 10000;
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private final static Proxy PROXY_HTTP = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_DOMAIN, PROXY_PORT));
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	static Proxy  proxy = null;
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	
	public static String[] httpRequestWithStatusIgnoreCookies2(String url,int page) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", USER_AGENT);
		httpGet.addHeader("cookie", cookie);
		httpGet.addHeader("referer", "https://shopee.co.th/shop/"+shopId+"/search/?page="+page+"&sortBy=ctime");
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is,"UTF-8");
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while((line = brd.readLine()) != null)
    					rtn.append(line);
    				return new String[]{rtn.toString(), String.valueOf(status)};
    			}
    		}else{
				return new String[]{null, String.valueOf(status)};
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpGet != null)
				httpGet.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    }
		

}
