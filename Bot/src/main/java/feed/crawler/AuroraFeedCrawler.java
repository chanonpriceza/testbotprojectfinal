package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;

import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AuroraFeedCrawler extends TemplateShopeeItemApiFeedCrawler {
	
	public AuroraFeedCrawler() throws Exception {
		super();
	}

	@Override
	protected void getProductDetail(String productId,String cat) throws Exception {
		
		String req = GET_PRODUCT_LINK.replace("{itemid}", productId).replace("{shopId}", shopId);
		String[] response = HTTPUtil.httpRequestWithStatus(req, "UTF-8", true);
		
		if(response==null||response.length==0) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		if(response[0]==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		JSONObject obj = (JSONObject) parser.parse(response[0]);
		
		obj = (JSONObject) obj.get("item");
		
		if(obj==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		String price = String.valueOf(obj.get("price"));
		String basePrice = String.valueOf(obj.get("price_before_discount"));
		String name = String.valueOf(obj.get("name"));
		String productImageUrl =  String.valueOf(obj.get("image"));
		String productUrl 		= "";
		double priceDouble = FilterUtil.convertPriceStr(price) / 100000;
		double basePriceDouble = FilterUtil.convertPriceStr(basePrice) / 100000;
		productImageUrl = productImageUrl.replace("&amp;", "&");
		name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim()).trim();
		
		productUrl = ""
				+ "https://shopee.co.th/universal-link/product/" + shopId + "/" + productId
				+ "?smtt=9"
				+ "&deep_and_deferred=1"
				+ "&pid=priceza_int"
				+ "&c=datafeed"
				+ "&af_click_lookback=7d"
				+ "&is_retargeting=true"
				+ "&af_reengagement_window=7d"
				+ "&af_installpostback=false"
				+ "&clickid=(xxxxx)"
				+ "&utm_source=priceza"
				+ "&utm_medium=affiliates"
				+ "&utm_campaign=datafeed"
				;
		
		if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(name)) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+productId+")");
		pdb.setPrice(priceDouble);
		pdb.setBasePrice(basePriceDouble);
		//pdb.setDescription(description);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(shopId + "." + productId);
		pdb.setRealProductId(productId);
		
		if (StringUtils.isNotBlank(productImageUrl)) {
			pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
		}
		

		String[] mapping = mapData.get(productId);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(name + " " + mapping[1]);				
		} else {
			if(fixcat!=0) 
				pdb.setCategoryId(fixcat);
			else
			pdb.setCategoryId(0);
		}
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
			}catch(Exception e) {
				logger.error("Error",e);
			}
		}
		
	}

}
