package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class One2CarFeedCrawler extends FeedManager{

	public One2CarFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.one2car.com/priceza/criteo.xml.gz"
		};
	}
	
	public ProductDataBean parse(String xml) {
	    
//		<AD>
//		    <ID><![CDATA[4484658]]></ID>
//		    <TITLE><![CDATA[2013 Isuzu D-Max 2.5 Hi-Lander Z Pickup]]></TITLE>
//		    <URL><![CDATA[https://www.one2car.com/for-sale/isuzu-d-max-hi-lander-z-กรุงเทพและปริมณฑล-หลักสี่-สะพานใหม่-แจ้งวัฒนะ/4484658]]></URL>
//		    <CONTENT><![CDATA["อุปกรณ์เสริมและอุปกรณ์ตกแต่ง
//		     แอร์ เครื่องเล่น วิทยุ cd mp3 พวงมาลัยพาวเวอร์ เซ็นทรัลล็อค กระจกไฟฟ้า ล้อแม๊กซ์ ไมล์ดิจิตอล full option
//		``one2car แนะนำให้ ผู้ซื้อติดต่อผู้ขายเพื่อสอบถามข้อมูลรถยนต์ที่ถูกต้อง ก่อนตัดสินใจเลือกซื้อรถ เพื่อประโยชน์สูงสุดแก่ผู้ซื้อ นะคะ"
//		]]></CONTENT>
//		    <MAKE><![CDATA[Isuzu]]></MAKE>
//		    <MODEL><![CDATA[D-Max]]></MODEL>
//		    <YEAR><![CDATA[2013]]></YEAR>
//		    <PRICE currency="THB"><![CDATA[589000]]></PRICE>
//		    <CITY><![CDATA[กรุงเทพและปริมณฑล]]></CITY>
//		    <REGION><![CDATA[กรุงเทพและปริมณฑล]]></REGION>
//		    <MILEAGE><![CDATA[97500]]></MILEAGE>
//		    <PICTURES>
//		        <PICTURE>
//		            <PICTURE_URL><![CDATA[https://img4.icarcdn.com/8564844/main-s_used-car-one2car-isuzu-d-max-hi-lander-z-pickup-thailand_8564844_LraW0Fxqi9MbkQ5tGasA0e.JPG?smia=xTM]]></PICTURE_URL>
//		            <PICTURE_TITLE><![CDATA[main image]]></PICTURE_TITLE>
//		        </PICTURE>
//		    </PICTURES>
//		    <DATE><![CDATA[18/07/2018]]></DATE>
//		    <TIME><![CDATA[12:06]]></TIME>
//		    <EXPIRATION_DATE><![CDATA[29/04/2018]]></EXPIRATION_DATE>
//		    <IS_NEW>0</IS_NEW>
//		</AD>
	
		String productId = removeCDATATag(FilterUtil.getStringBetween(xml, "<ID>", "</ID>"));
		String productName = removeCDATATag(FilterUtil.getStringBetween(xml, "<TITLE>", "</TITLE>"));
		String productPrice = removeCDATATag(FilterUtil.getStringBetween(xml, "<PRICE currency=\"THB\">", "</PRICE>"));
		String productUrl = removeCDATATag(FilterUtil.getStringBetween(xml, "<URL>", "</URL>"));
		String productImageUrl = removeCDATATag(FilterUtil.getStringBetween(xml, "<PICTURE_URL>", "</PICTURE_URL>"));
		String productDesc = removeCDATATag(FilterUtil.getStringBetween(xml, "<CONTENT>", "</CONTENT>"));
		
		String city = removeCDATATag(FilterUtil.getStringBetween(xml, "<CITY>", "</CITY>"));
		String isNew = removeCDATATag(FilterUtil.getStringBetween(xml, "<IS_NEW>", "</IS_NEW>"));
		
		String isNewTxt = (isNew.equals("0"))?"(รถมือสอง)":"(รถใหม่)";
		
		productUrl = productUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace("&amp;", "&");
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 || productId.trim().length() == 0) {
			return null;			
		}
						
		ProductDataBean pdb = new ProductDataBean();	
		
		if(productName.trim().length() != 0) {
			pdb.setName(productName + " - " + city + " " + isNewTxt + " (" + productId + ")");
		}
		
		if(productPrice.trim().length() != 0) {
			String price = FilterUtil.removeCharNotPrice(productPrice);
			if(price.equals("1")){
				price = BotUtil.CONTACT_PRICE_STR;
			}
			pdb.setPrice(FilterUtil.convertPriceStr(price));
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);
		}

		String[] catMap = getCategory(isNew);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		
		if(productDesc.length() > 0){
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			pdb.setDescription(productDesc);
		}
				
		return pdb;
		
	}
	
	private String removeCDATATag(String value){
		if(StringUtils.isNotBlank(value)){
			value = value.replace("<![CDATA[", "").replace("]]>", "");
			value = FilterUtil.toPlainTextString(value);
		}
		return value;
	}
	
}
