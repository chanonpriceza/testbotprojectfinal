package feed.crawler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class CentralFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	boolean first = false;
	boolean haveResult = false;
	private static Map<String,String[]> lowerCaseCat = new HashMap<String, String[]>();
	
	public CentralFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
							  
		return new String[] {"https://cds-feed-production.s3.amazonaws.com/cds-feed/cds_product_feed_fb.tsv"};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			if(Thread.currentThread().isInterrupted()) {
				break;
			}
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error(e);
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		
		for(String cat:getCategoryKeySet()) {
			lowerCaseCat.put(cat.toLowerCase(),getCategory(cat));
		}
		
		mockFeed();
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void mockFeed() {
		try {
			String url = BaseConfig.FEED_FILE[0];
			String feed = BaseConfig.FEED_STORE_PATH + "/" + StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			File file = new File(feed);
			FileInputStream ins = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(ins,BaseConfig.CONF_PARSER_CHARSET);
			@SuppressWarnings("resource")
			final CSVParser parser = new CSVParser(isr, CSVFormat.TDF);
			for (final CSVRecord record : parser) {
				if(Thread.currentThread().isInterrupted()) {
					break;
				}
				try {
					parseCSV(record);
				} catch (Exception e) {
					logger.error(e);
					continue;
				}
			}
		
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void parseCSV(CSVRecord data) throws IOException, InterruptedException {
		String id  = FilterUtil.toPlainTextString(data.get(0));
		String name = FilterUtil.toPlainTextString(data.get(5));
		String expire = FilterUtil.toPlainTextString(data.get(4));
		String description = FilterUtil.toPlainTextString(data.get(6));
		String image = FilterUtil.toPlainTextString(data.get(7));
		String  brand = FilterUtil.toPlainTextString(data.get(8));
		String  url = FilterUtil.toPlainTextString(data.get(17));
		String cat2 =  FilterUtil.toPlainTextString(data.get(1));
		String  price = data.get(9);
		String  sale_price = data.get(10);
		String  category = data.get(2);
		
		if(StringUtils.isBlank(name)) {
			return;
		}

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(brand+" "+name+" ("+id+")");
		
		if(pdb.getName().contains("AESOP"))
			return;

		sale_price = FilterUtil.removeCharNotPrice(sale_price);
		price = FilterUtil.removeCharNotPrice(price);
		pdb.setPrice(FilterUtil.convertPriceStr(sale_price));
		pdb.setUrlForUpdate(id);
		
		if(StringUtils.isNotBlank(description)) {
			pdb.setDescription(description);
		}
		
		if(StringUtils.isNotBlank(image)) {
			pdb.setPictureUrl(image);
		}
		
		if(StringUtils.isNotBlank(id)) {
			pdb.setRealProductId(id);
		}
		
		if(StringUtils.isNotBlank(price)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(price));
		}
		
		if(StringUtils.isNotBlank(expire)&&expire.equals("out of stock")) {
			pdb.setExpire(true);
		}
		
		String utmCampaign = "-";
		category = category.replaceAll("\u00a0"," ");

		String[] mapping = getCategory(FilterUtil.toPlainTextString((category)));
		String[] mappingLowerCase = lowerCaseCat.get(FilterUtil.toPlainTextString((category.toLowerCase().trim())));
		String[] mapping2 = getCategory(FilterUtil.toPlainTextString((cat2)));
		
		
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			
			String keyword = mapping[1];
			String[] keyData = keyword.split("\\|");
			
			if(keyData != null && keyData.length >= 1) {
				String key = StringUtils.defaultIfBlank(keyData[0], null);
				pdb.setKeyword(key);				
			}
			
			if(keyData != null && keyData.length >= 2) {
				utmCampaign = StringUtils.defaultIfBlank(keyData[1], "");
			}
			
		}else if(mappingLowerCase!=null) {
			pdb.setCategoryId(BotUtil.stringToInt(mappingLowerCase[0], 0));
			
			String keyword = mappingLowerCase[1];
			String[] keyData = keyword.split("\\|");
			
			if(keyData != null && keyData.length >= 1) {
				String key = StringUtils.defaultIfBlank(keyData[0], null);
				pdb.setKeyword(key);				
			}
			
			if(keyData != null && keyData.length >= 2) {
				utmCampaign = StringUtils.defaultIfBlank(keyData[1], "");
			}
			
		}else if(mapping2 != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping2[0], 0));
			
			String keyword = mapping2[1];
			String[] keyData = keyword.split("\\|");
			
			if(keyData != null && keyData.length >= 1) {
				String key = StringUtils.defaultIfBlank(keyData[0], null);
				pdb.setKeyword(key);		
			}
			
			if(keyData != null && keyData.length >= 2) {
				utmCampaign = StringUtils.defaultIfBlank(keyData[1], "");
			}
			
		}else {
			pdb.setCategoryId(0);
		}
		
		if(StringUtils.isNotBlank(url)) {
			if(url.contains("?")) {
				url += "&utm_campaign=" + utmCampaign ;
			}else {
				url += "?utm_campaign=" + utmCampaign ; 
			}
		}
		
		if(url.startsWith("?")) {
			return;
		}
		
		pdb.setUrl(url);
		mockResult(pdb);

	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(StringUtils.isNotBlank(urlForUpdate))result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	
	
}
