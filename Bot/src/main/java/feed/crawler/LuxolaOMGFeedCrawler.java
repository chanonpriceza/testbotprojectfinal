package feed.crawler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class LuxolaOMGFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public LuxolaOMGFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public void loadFeed() {
		if("offline".equals(BaseConfig.CONF_FEED_CRAWLER_PARAM)) {
			logger.info("Feed Offline Mode");
			String FEED_URL = "http://27.254.82.243:8081/434.xml";
			String FILE_NAME = "434.xml";
			String FEED_USER = "bot";
			String FEED_PASS = "pzad4r7u";
			
			FTPUtil.downloadFileWithLogin(FEED_URL,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME, FEED_USER, FEED_PASS);
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME};
		}else {
			for (String feed : BaseConfig.FEED_FILE) {
				if(feed.isEmpty()) {
					logger.error("Feed | Cannot Load | " + feed);
					continue;
				}
				logger.info("Feed : Load : " + feed);
				
				if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
					Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
				
				Path path = new File(BaseConfig.FEED_STORE_PATH + "/" +"434.xml").toPath();
				try (InputStream in = new URL(feed).openStream()) {
					Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				}catch (ClosedByInterruptException e) {
					logger.error(e);
					return;
				}catch (IOException e) {
					e.printStackTrace();
					logger.error(e);
				}
				BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/" +"434.xml"};
			}
		}
		
	}

	@Override
	public String[] setFeedFile() {
		
		return new String[] {"http://feeds.omgpm.com/GetFeed/?aid=622948&feedid=370&Format=xml"};
	}
	
	public Map<String, String> catchTagXML(String html){
		String productName = FilterUtil.getStringBetween(html, "<ProductName>", "</ProductName>").trim();
		String productPrice = FilterUtil.getStringBetween(html, "<ProductPrice>", "</ProductPrice>").trim();
		String productUrl = FilterUtil.getStringBetween(html, "<ProductURL>", "</ProductURL>").trim();
		
		String lImage = FilterUtil.getStringBetween(html, "<ProductImageLargeURL>", "</ProductImageLargeURL>").trim();
		String mImage = FilterUtil.getStringBetween(html, "<ProductImageMediumURL>", "</ProductImageMediumURL>").trim();
		String sImage = FilterUtil.getStringBetween(html, "<ProductImageSmallURL>", "</ProductImageSmallURL>").trim();
		
		String productPictureUrl = "";
		if(StringUtils.isNotEmpty(lImage)){
			productPictureUrl = lImage;
		}else if(StringUtils.isNotEmpty(mImage)){
			productPictureUrl = mImage;
		}else if(StringUtils.isNotEmpty(sImage)){
			productPictureUrl = sImage;
		}
		
		String realProductId = FilterUtil.getStringBetween(html, "<ProductID>", "</ProductID>").trim();
		String productCatName = FilterUtil.getStringBetween(html, "<CategoryPathAsString>", "</CategoryPathAsString>").trim();
		String productDescription = FilterUtil.getStringBetween(html, "<ProductDescription>", "</ProductDescription>").trim();
		
		
		Map<String ,String> m = new HashMap<String, String>();
		m.put("productName", productName);
		m.put("productPrice", productPrice);
		m.put("productUrl", productUrl);
		m.put("productPictureUrl", productPictureUrl);
		m.put("realProductId", realProductId);
		m.put("productCatId", productCatName);
		//m.put("productCatName", productCatName);
		m.put("productDescription", productDescription);
		
		return m;
		
	}
	
//	public ProductDataBean mergeProductData(Map<String, String> pdMap){
	public ProductDataBean parse(String html) {
		String brand 				= FilterUtil.getStringBetween(html, "<Brand>", "</Brand>").trim();
		String productName 			= FilterUtil.getStringBetween(html, "<ProductName>", "</ProductName>").trim();
		String productPrice 		= FilterUtil.getStringBetween(html, "<ProductPrice>", "</ProductPrice>").trim();
		String productUrl 			= FilterUtil.getStringBetween(html, "<ProductURL>", "</ProductURL>").trim();
		String realProductId 		= FilterUtil.getStringBetween(html, "<ProductSKU>", "</ProductSKU>").trim();
		String productCatId 		= FilterUtil.getStringBetween(html, "<CategoryPathAsString>", "</CategoryPathAsString>").trim();
		String productDescription 	= FilterUtil.getStringBetween(html, "<ProductDescription>", "</ProductDescription>").trim();

		String lImage = FilterUtil.getStringBetween(html, "<ProductImageLargeURL>", "</ProductImageLargeURL>").trim();
		String mImage = FilterUtil.getStringBetween(html, "<ProductImageMediumURL>", "</ProductImageMediumURL>").trim();
		String sImage = FilterUtil.getStringBetween(html, "<ProductImageSmallURL>", "</ProductImageSmallURL>").trim();
		String productPictureUrl = "";
		if(StringUtils.isNotEmpty(lImage)){
			productPictureUrl = lImage;
		}else if(StringUtils.isNotEmpty(mImage)){
			productPictureUrl = mImage;
		}else if(StringUtils.isNotEmpty(sImage)){
			productPictureUrl = sImage;
		}
		
		if(productUrl.trim().isEmpty() || productName.trim().isEmpty() || productPrice.trim().isEmpty()) {
			return null;			
		}
		
		ProductDataBean pdb = new ProductDataBean();	
		
		productName = StringEscapeUtils.unescapeHtml4(productName).trim();
		if(realProductId.length() > 0){
			productName = brand+" "+productName+" (" + realProductId + ")";
			pdb.setRealProductId(realProductId);
		}
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
				
		if (productDescription.length() != 0) {
			pdb.setDescription(productDescription);
		}
		if (productPictureUrl.length() != 0) {
			productPictureUrl = StringEscapeUtils.unescapeHtml4(productPictureUrl).trim();
			productPictureUrl = productPictureUrl.replace(" ", "").replace("%20", "");
			pdb.setPictureUrl(productPictureUrl);
		}
		if (productUrl.length() != 0) {
			productUrl = StringEscapeUtils.unescapeHtml4(productUrl).trim();
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(FilterUtil.getStringAfter(productUrl, "&r=", productUrl));
		}
		
		productCatId = StringEscapeUtils.unescapeHtml4(productCatId);
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0); // Default category
		}

		return pdb;
	}
	
}
