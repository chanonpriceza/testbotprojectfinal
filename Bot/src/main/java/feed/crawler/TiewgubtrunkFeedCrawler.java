package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;
import web.parser.image.IgnoreSSL;
import web.parser.image.Limit2MBImageParser;

public class TiewgubtrunkFeedCrawler extends FeedManager {
	
	public TiewgubtrunkFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		BaseConfig.IMAGE_PARSER =  new Limit2MBImageParser();
		String FEED_URL = "http://27.254.82.243:8081/300778.csv";
		String FILE_NAME = "300778.csv";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		FTPUtil.downloadFileWithLogin(FEED_URL,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME, FEED_USER, FEED_PASS);
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME};
		BaseConfig.IMAGE_PARSER = new IgnoreSSL();
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {

		String id = data.get(0);
		String name = data.get(1);
		String desc = data.get(2);
		String price = data.get(3);
		String bprice = data.get(4);
		String url = data.get(6);
		String image = data.get(5);
		String category = data.get(7);

		ProductDataBean pdb = new ProductDataBean();
		
		if(StringUtils.isBlank(name) || StringUtils.isBlank(url) || StringUtils.isBlank(price) ||StringUtils.isBlank(id)) {
			return null;
		}
		price = FilterUtil.removeCharNotPrice(price);
		pdb.setName(name+" ("+id+")");
		pdb.setBasePrice(FilterUtil.convertPriceStr(bprice));
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setRealProductId(id);

		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		//pdb.setExpire(true);
		return pdb;
	}
}
