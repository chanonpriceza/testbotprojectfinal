package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ChimchardFeedCrawler extends FeedManager {
	
	private static JSONParser parser = new JSONParser();
	private static String currentCat = "";

	public ChimchardFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			for(String cat:getCategoryKeySet()) {
				try {
					String[] x = HTTPUtil.httpRequestWithStatus(cat,"UTF-8",true);
					String html = surgeryHTML(x[0]);
					currentCat = cat;
					JSONArray obj = (JSONArray)parser.parse(html);
					JSONArray surgey = surgeyJSON(obj);
					Files.copy(new ByteArrayInputStream(surgey.toJSONString().getBytes()),new File(BaseConfig.FEED_STORE_PATH+"/"+Math.random()+".json").toPath(),StandardCopyOption.REPLACE_EXISTING);
				}catch (Exception e) {
					logger.error("error",e);
				}
			}
		}catch (Exception e) {
			logger.error("error",e);
		}
		
		BaseConfig.FEED_FILE = new File(BaseConfig.FEED_STORE_PATH).list();
	}
	
	private String surgeryHTML(String html) {
		Document jsoup = Jsoup.parse(html);
		Elements e = jsoup.select("data");
		String result = FilterUtil.getStringBetween(e.toString(),"store_list  = [", "data");
		result = FilterUtil.getStringBeforeLastIndex(result,"\"><", result);
		result = StringEscapeUtils.unescapeHtml4(result);
		return "["+result;
	}
	
	@SuppressWarnings("unchecked")
	private JSONArray surgeyJSON(JSONArray o) {
		JSONArray result = new JSONArray();
		for(Object ox:o) {
			JSONObject cast = (JSONObject) ox;
			JSONObject add = new JSONObject();
			String name  = "";
			String id = "";
			String profilePic = "";
			if(currentCat.contains("restaurant")) {
				 name = String.valueOf(cast.get("restaurantName"));
				 id = String.valueOf(cast.get("id_restaurant"));
				 profilePic =  String.valueOf(cast.get("profilePic"));
			}
			else {
				 name = String.valueOf(cast.get("name"));
				 id = String.valueOf(cast.get("id"));
				 profilePic =  String.valueOf(cast.get("image"));
			}
			add.put("name",name);
			String area = String.valueOf(cast.get("area"));
			add.put("area",area);
			add.put("cat",currentCat);
			add.put("profilePic",profilePic);
			add.put("id",id);
			JSONObject pro = (JSONObject)cast.get("promotion");
			if(pro!=null) {
				String promotion = String.valueOf(pro.get("name"));
				add.put("promotion",promotion);
			}
			 pro = (JSONObject)cast.get("offer");
			if(pro!=null) {
					String promotion = String.valueOf(pro.get("name"));
					add.put("promotion",promotion);
			}
			result.add(add);

		}
		return result;
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String name = String.valueOf(data.get("name"));
		String promotion = String.valueOf(data.get("promotion"));
		String area = String.valueOf(data.get("area"));
		String pic = String.valueOf(data.get("profilePic"));
		String cat = String.valueOf(data.get("cat"));
		String id = String.valueOf(data.get("id"));
		
		if(promotion==null||promotion.equals("null")) {
			promotion = "";
		}else {
			promotion = "-"+promotion;
		}
		
		if(area==null||area.equals("null")) {
			area = "";
		}else {
			area = "-"+area;
		}
		
		if(pic==null||pic.equals("null")) {
			pic = null;
		}
		
		if(name==null||name.equals("null")) {
			name = "";
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+promotion+area);
		pdb.setPrice(BotUtil.CONTACT_PRICE);
		pdb.setUrl("https://www.chimcard.com/th/chimcardreview/page/"+id);
		pdb.setPictureUrl(pic);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
			
		
		return  pdb;
	}

}
