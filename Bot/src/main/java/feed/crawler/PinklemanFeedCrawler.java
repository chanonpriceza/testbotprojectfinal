package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class PinklemanFeedCrawler extends FeedManager {
	private static JSONParser parser = new JSONParser();
	private static String mainBody = "{\"view\":\"grid\",\"cate_id\":\"{cat}\",\"search\":\"\",\"page\":{page},\"perpage\":\"21\",\"tags\":[],\"sort\":\"3\"}";
	private static String mainUrl = "https://api2.ketshopweb.com/api/v1/product/searchProductIndex";
	private static String currentCat = "";
	
	public PinklemanFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		try {
			for (String cat:getCategoryKeySet()) {
				currentCat = cat;
				for(int i = 1 ;i <= 30;i++) {
					JSONObject obj = requestJSON(cat,i+"");
					JSONArray array =(JSONArray) obj.get("data");
					int count = BotUtil.stringToInt(String.valueOf(obj.get("count")), 0);
					if(i*20>=count&&i>1)
						break;
					if(array==null)
						break;
					JSONArray result = new JSONArray();
					for(Object o:array) {
						JSONObject ox = (JSONObject)o;
						result.add(surgeyJSON(ox));
					}
					InputStream is = new ByteArrayInputStream(result.toString().getBytes());
					Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+cat+"-"+BaseConfig.MERCHANT_ID+"-"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
			}
			BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
		}catch (Exception e) {
			logger.error("Error",e);
		}
	}
	
	private JSONObject requestJSON(String cat,String page) {
		URL u = null;
		HttpURLConnection conn = null;
		JSONObject obj = new JSONObject();
		String data = mainBody.replace("{page}",page).replace("{cat}", cat);
		try {
			u = new URL(mainUrl);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);			
			conn.setRequestProperty("referer", "https://pinklemononline.com/");				
			conn.setRequestProperty("Accept", "*/*");				
			conn.setRequestProperty("User-Agent", "PostmanRuntime/7.26.3");				
			conn.setRequestProperty("content-type", "application/json");				
			
			
			conn.setRequestProperty("Content-Length", "" + data.length());
			conn.setInstanceFollowRedirects(true);
			
			try(DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());){
				outStream.writeBytes(data);
				outStream.flush();
				try (
						InputStream is = conn.getInputStream();
						InputStreamReader isr = new InputStreamReader(is, "UTF-8");){
					obj = (JSONObject)parser.parse(isr);
				}catch (Exception e) {
					logger.error("Error",e);
				}
			}catch (Exception e) {
				logger.error("Error",e);
			}
		}catch (Exception e) {
			logger.error("Error",e);
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject surgeyJSON(JSONObject o) {
		JSONObject result = new JSONObject();
		result.put("id",String.valueOf(o.get("id")));
		result.put("sku",String.valueOf(o.get("sku")));
		result.put("name",String.valueOf(o.get("title_lang1")));
		result.put("img",String.valueOf(o.get("feature_img")));
		result.put("url",String.valueOf(o.get("permalink_lang1")));
		result.put("price",String.valueOf(o.get("price")));
		result.put("basePrice",String.valueOf(o.get("temp_price")));
		result.put("cat",String.valueOf(currentCat));

		return result;
		
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		
		String  name = String.valueOf(data.get("name"));
		String  id = String.valueOf(data.get("id"));
		String  price = String.valueOf(data.get("price"));
		String  basePrice = String.valueOf(data.get("basePrice"));
		String  url = String.valueOf(data.get("url"));
		url = url.replace("(","%28");
		url = url.replace(")","%29");
		String  image = String.valueOf(data.get("img"));
		String  cat = String.valueOf(data.get("cat"));
		price =FilterUtil.removeCharNotPrice(price);
		basePrice =FilterUtil.removeCharNotPrice(basePrice);
		
		double priceReal = FilterUtil.convertPriceStr(price); //0
		double baseReal = FilterUtil.convertPriceStr(basePrice);//100
		
		if(priceReal==0) {
			double tmp = priceReal;
			priceReal = baseReal;
			baseReal = tmp;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl("https://pinklemononline.com/รายละเอียดสินค้า/"+url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl("https://pinklemononline.com/"+image);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
				
				
		return pdb;
	}
	

}
