package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class TopValueFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public TopValueFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
			"https://topvalue.com/media/amasty/feed/priceza.xml"	
		};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			String[] result = HTTPUtil.httpRequestWithStatus(feed, "UTF-8", true);
			if(result != null && result.length == 2) {
				if(StringUtils.isNotBlank(result[0])) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + fileName),  true));){
						pwr.println(result[0]);
					}catch(Exception e) {
						logger.error(e);
					}
				}
			} else {
				logger.info("Feed | Cannot Load | " + feed);
			}
		}
	}

	public ProductDataBean parse(String xml) {
		
		String productName = removeCDATATag(FilterUtil.getStringBetween(xml, "<name>", "</name>")).trim();
		String productPrice = removeCDATATag(FilterUtil.getStringBetween(xml, "<price>", "</price>")).trim();
		String productUrl = removeCDATATag(FilterUtil.getStringBetween(xml, "<url>", "</url>")).trim();
		String productPictureUrl = removeCDATATag(FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>")).trim();
		String realProductId = removeCDATATag(FilterUtil.getStringBetween(xml, "<id>", "</id>")).trim();
		String productCatId = removeCDATATag(FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>")).trim();
		//String productCatName = removeCDATATag(FilterUtil.getStringBetween(xml, "<category_name>", "</category_name>")).trim();
		String productDescription = removeCDATATag(FilterUtil.getStringBetween(xml, "<description>", "</description>")).trim();
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 ) {
			return null;		
		}
		
		//String mixCat = productCatId;
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productDescription = StringEscapeUtils.unescapeHtml4(productDescription);
		productPictureUrl = StringEscapeUtils.unescapeHtml4(productPictureUrl);
		productPictureUrl = productPictureUrl.replace("&amp;", "&");
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		productUrl = productUrl.replace("&amp;", "&");
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName+" "+"("+realProductId+")");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);			
			pdb.setUrlForUpdate(FilterUtil.getStringAfter(productUrl, "TARGETURL=", productUrl));
		}
		
		if (productPictureUrl.length() != 0) {
			if(productPictureUrl.startsWith("http:")){
				productPictureUrl = productPictureUrl.replace("http:", "https:");
			}
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(realProductId.length() != 0){
			pdb.setRealProductId(realProductId);
		}
		
		pdb.setUrlForUpdate(realProductId);
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		
		if(productDescription.length() != 0){
			pdb.setDescription(productDescription);
		}
		
		return pdb;
		
	}
	
	private String removeCDATATag(String value){
		if(StringUtils.isNotBlank(value)){
			value = value.replace("<![CDATA[", "").replace("]]>", "");
			value = FilterUtil.toPlainTextString(value).trim();
		}
		return value;
	}

}
