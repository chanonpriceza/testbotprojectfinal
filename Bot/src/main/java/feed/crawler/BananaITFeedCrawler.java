package feed.crawler;


import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BananaITFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public BananaITFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://media.bnn.in.th/feed/acommerce/products.csv"};
	}
		
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String id = data.get(0).trim();
		String name = data.get(1).trim();
		String desc = data.get(2).trim();
		String image = data.get(3).trim();
		String url  = data.get(4).trim();
		String price = data.get(6).trim();
		String expire = data.get(8).trim();
		String product_type =data.get(9).trim();
		String basePrice = data.get(5).trim();

		if(url.isEmpty() || name.isEmpty() || price.isEmpty() || id.isEmpty()) {
			logger.info("Data not complete :"+url+" "+name+" "+price+" "+id);
			return null;		
		}
		
		if(!"in stock".equals(expire)) {
			logger.info("expire :"+url+" "+name+" "+price+" "+id);
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(name+" ("+id+")");

		if(StringUtils.isNotBlank(desc)) 		
			pdb.setDescription(desc);
		
		if(StringUtils.isNotBlank(image)) 	
			pdb.setPictureUrl(image);
		
		pdb.setUrl(url);
		
		if(url.contains("/expire/")) {
			pdb.setExpire(true);
			return pdb;
		}
		pdb.setUrlForUpdate(id);
		
		String[] splitCat = new String[0];
		if(product_type.contains("|")) {
			splitCat = product_type.split("\\|");
			if(splitCat!=null&&splitCat.length>0) 
				product_type = splitCat[0];
		}
		
		product_type = StringEscapeUtils.unescapeXml(product_type);
		product_type = FilterUtil.toPlainTextString(product_type);
		product_type = FilterUtil.removeSpace(product_type);


		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));		
		
		if(StringUtils.isNotBlank(basePrice) && !basePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
		
		if(StringUtils.isNotBlank(id)) 
			pdb.setRealProductId(id);

		String[] mapping = getCategory(product_type);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			logger.info(product_type);
			pdb.setCategoryId(0);
		}
		

		
		return pdb;
		
	}
	
	
}
