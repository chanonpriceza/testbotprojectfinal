package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class JayMartFeedCrawler extends FeedManager{

	public JayMartFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.jaymartstore.com/rss/priceza/index.asp"
		};
	}
	
	public ProductDataBean parse(String xml){
		String productName = FilterUtil.getStringBetween		(xml, "<name>", "</name>").trim();
		String productUrl = FilterUtil.getStringBetween			(xml, "<url>", "</url>").trim();
		String productPictureUrl = FilterUtil.getStringBetween	(xml, "<image_url>", "</image_url>").trim();
		String productPrice = FilterUtil.getStringBetween		(xml, "<price>", "</price>").trim();
		String productBasePrice = FilterUtil.getStringBetween	(xml, "<base_price>", "</base_price>").trim();
		String productDesc = FilterUtil.getStringBetween		(xml, "<description>", "</description>").trim();
		String productCatName = FilterUtil.getStringBetween		(xml, "<category_id>", "</category_id>").trim();
		String productId = FilterUtil.getStringBetween			(xml, "<id>", "</id>").trim();
		productUrl = productUrl.replace("http://","https://");
		
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		ProductDataBean pdb = new ProductDataBean();
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productName = FilterUtil.toPlainTextString(productName);
		if(!productId.isEmpty()){
			productName = productName + " ("+productId+")";
			pdb.setRealProductId(productId);
		}
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		if(productBasePrice != null && !productBasePrice.isEmpty()){
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));		
		}
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId);
		
		if (!productPictureUrl.isEmpty()) {	
			if(!productPictureUrl.startsWith("http")){
				productPictureUrl = "https://www.jaymartstore.com/"+productPictureUrl;
			}
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!productDesc.isEmpty()){
			pdb.setDescription(productDesc);
		}


		String[] catMap = getCategory(productCatName);
		
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}else{
			pdb.setCategoryId(0);
		}
		return pdb;
		
	}
	
}
