package feed.crawler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class GNCLiveWellFeedCrawler extends FeedManager {

	public GNCLiveWellFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			FTPUtil.sftpGetDowloadFile("/outgoing/productcatalog/227339/GNC-GNC_Shopping_Feed-shopping.xml.zip","datatransfer.cj.com", 22,"4860367","vbuMfQe$",BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".zip");
			extractFile(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".zip");
		} catch (IOException e) {
			logger.error("Error",e);
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".xml"};
	}
	
	public static void extractFile(String fileName) {

        byte[] buffer = new byte[2048];
        Path outDir = Paths.get(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+".xml");
        String zipFileName = fileName;

        try (FileInputStream fis = new FileInputStream(zipFileName);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ZipInputStream stream = new ZipInputStream(bis)){

            @SuppressWarnings("unused")
			ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {


                try (FileOutputStream fos = new FileOutputStream(outDir.toFile());
                        BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

                    int len;
                    while ((len = stream.read(buffer)) > 0) {
                        bos.write(buffer, 0, len);
                    }
                }
                
            }
        }catch (Exception e) {
        	e.printStackTrace();
		}
	
	}
	
	@Override
	public ProductDataBean parse(String data) {
//		ชื่อสินค้า			
//		brand + title + size + id
//		เช่น 		
//		GNC Mega Men® Sport 90 Capsules (201512)
//		Kaged Muscle® C-Hcl - Lemon Lime (217522)
//		Kaged Muscle® C-Hcl - Lemon Lime - (217522)			
//					
//		ดึงสินค้า เฉพาะ identifier_exists มีค่า yes
//
//
//		Map pattern จาก google_product_category
		//ดึงสินค้า เฉพาะ identifier_exists มีค่า yes
		
//		<entry>
//		<program_name>GNC</program_name>
//		<program_url>http://www.gnc.com</program_url>
//		<catalog_name>GNC Shopping Feed</catalog_name>
//		<last_updated>2019-08-18T05:06:17.639-07:00</last_updated>
//		<id>201512</id>
//		<title>Mega Men® Sport</title>
//		<description>Clinically Studied Multivitamin Performance Muscle Function Antioxidants More Antioxidant Power Than Ever And Over 30 Clinically Studied Ingredients In Every Formula - All In Smaller, Easier-To-Swallow Pills. Men&#39;s Health Contains A Clinically Studied Men&#39;s Multivitamin Shown To Work Better Than A Basic Multivitamin. Helps Support Colon And Prostate Health.  Performance Features Beta-Alanine, A High-Efficiency Muscle Ph Buffer, And Carnitine, Which Helps Use Fatty Acids As Energy.  Muscle Function Features Branched Chain Amino Acids To Fuel Muscles. Also Supports Muscles By Providing Essential Electrolytes That May Be Lost During Intense Exercise, Including Calcium, Which Plays A Role In Mediating Muscle Contraction.  Antioxidants Packed With Antioxidants To Help Fight Free Radicals That Can Be Produced During Your Workout.  Not All Multivitamins Are Created Equal. When It Comes To Something As Important As Your Health, You Want A High Quality Multivitamin. Not Only Is Every Gnc Mega Men® And Women’S Ultra Mega® Multivitamin Complex Created With The Vitamins And Minerals You Need To Live Well, They’Re Also Clinically Proven To Make You Feel Better.† Here’S Four More Reasons To Choose Gnc Mega Men® And Women’S Ultra Mega® Multivitamins.</description>
//		<link>http://www.jdoqocy.com/click-8481208-13586606?url=https%3A%2F%2Fwww.gnc.com%2Fvitamins_supplements%2F201512.html</link>
//		<impression_url>http://www.ftjcfx.com/image-8481208-13586606</impression_url>
//		<image_link>https://www.gnc.com/dw/image/v2/BBLB_PRD/on/demandware.static/-/Sites-master-catalog-gnc/default/dw7151b905/hi-res/201512_1.jpg?sw=2000&amp;sh=2000&amp;sm=fit</image_link>
//		<availability>in stock</availability>
//		<price>22.99 USD</price>
//		<sale_price>22.99 USD</sale_price>
//		<google_product_category>525</google_product_category>
//		<google_product_category_name>Health &amp; Beauty &gt; Health Care &gt; Fitness &amp; Nutrition &gt; Vitamins &amp; Supplements</google_product_category_name>
//		<product_type>Vitamins &amp; Supplements &gt; Multivitamins &gt; Multivitamins for Men &gt; Mega Men Multivitamins</product_type>
//		<brand>GNC</brand>
//		<gtin>048107154905</gtin>
//		<mpn>201512</mpn>
//		<identifier_exists>yes</identifier_exists>
//		<condition>new</condition>
//		<gender>unisex</gender>
//		<size>90 Capsules</size>
//		<custom_label_1>LIA_flag</custom_label_1>
//		<custom_label_2>In Store</custom_label_2>
//		<custom_label_4>1st Party</custom_label_4>
//		<promotion_id>august19_bogo50</promotion_id>
//		</entry>
		
		String productName 			= FilterUtil.getStringBetween(data, "<title>", "</title>").trim();
		String productDesc 			= FilterUtil.getStringBetween(data, "<description>", "</description>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(data, "<image_link>", "</image_link>").trim();
		String productUrl 			= FilterUtil.getStringBetween(data, "<link>", "</link>").trim();
		String productPrice 		= FilterUtil.getStringBetween(data, "<sale_price>", "</sale_price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(data, "<price>", "</price>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(data, "<id>", "</id>").trim();
		String brand				= FilterUtil.getStringBetween(data, "<brand>", "</brand>").trim();
		String size					= FilterUtil.getStringBetween(data, "<size>", "</size>").trim();
		String cat					= FilterUtil.getStringBetween(data, "<google_product_category>", "</google_product_category>").trim();
		String expire	= FilterUtil.getStringBetween(data, "<identifier_exists>", "</identifier_exists>").trim();
		
		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		
		if(!"yes".equals(expire)) {
			logger.info("Expire "+productRealProductId);
			return null;
		}
		
		
		if(StringUtils.isBlank(size)) {
			size = "";
		}
		String tmp = "";
		if(StringUtils.isNotBlank(productRealProductId)) {
			tmp = "("+productRealProductId+")";
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(brand+" "+productName+" "+size+" "+tmp);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productRealProductId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));

		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		
		return pdb;
	}

}
