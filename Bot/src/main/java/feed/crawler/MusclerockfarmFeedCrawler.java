package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class MusclerockfarmFeedCrawler extends FeedManager{

	public MusclerockfarmFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://musclerockfarm.com/feed"
		};
	}
	
	public ProductDataBean parse(String html) {
		String productName = FilterUtil.getStringBetween(html, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(html, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(html, "<url>", "</url>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(html, "<image_url>", "</image_url>").trim();
		String realProductId = FilterUtil.getStringBetween(html, "<id>", "</id>").trim();
		String productCatId = FilterUtil.getStringBetween(html, "<category_id>", "</category_id>").trim();
		String productDescription = FilterUtil.getStringBetween(html, "<description>", "</description>").trim();
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 
				|| !productUrl.startsWith("http")) {
			return null;		
		}
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productUrl = productUrl.replace("&amp;", "&");
		productPictureUrl = productPictureUrl.replace("&amp;", "&");				
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);			
			pdb.setUrlForUpdate(FilterUtil.getStringAfter(productUrl, "TARGETURL=", productUrl));
		}
		
		if (productPictureUrl.length() != 0) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(realProductId.length() != 0){
			pdb.setRealProductId(realProductId);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}
		
		if(StringUtils.isNotBlank(productDescription)) {
			productDescription = StringEscapeUtils.unescapeHtml4(productDescription);
			productDescription = FilterUtil.toPlainTextString(productDescription);
			pdb.setDescription(productDescription);
		}
		
		return pdb;
	}
	

}


