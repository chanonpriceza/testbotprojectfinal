package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MetroDealFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	private boolean next = true;
	private int page = 1;

	public MetroDealFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}

	@Override
	public void loadFeed() {

		Set<String> cat_id = getCategoryKeySet();
		for (String id : cat_id) {
			page = 1;
			next = true;
			while (!Thread.currentThread().isInterrupted() && next) {
				String url = "https://th.metrodeal.com/web_api/?request=loadDeals&page=" + page + "&category_id=" + id + "&by_category=false";
				String data = jsonRequest("GET", url, "", "UTF-8", false);

				if (StringUtils.isNotBlank(data)) {
					try {
						parseJson(data);
					} catch (Exception e) {
					}
				} else {
					next = false;
				}
				page++;
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public static String jsonRequest(String method, String url, String data, String charset, boolean redirectEnable) {

		DataOutputStream outStream = null;
		URL u = null;
		HttpURLConnection conn = null;
		BufferedReader brd = null;
		InputStreamReader isr = null;
		try {

			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod(method);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Referer", "http//th.metrodeal.com/category/restaurants/Buffets");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			conn.setInstanceFollowRedirects(redirectEnable);
			outStream = new DataOutputStream(conn.getOutputStream());

			outStream.writeBytes(data);
			outStream.flush();
			outStream.close();

			isr = new InputStreamReader(conn.getInputStream(), charset);
			brd = new BufferedReader(isr);
			StringBuilder rtn = new StringBuilder(1000);
			String line = "";
			while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
				rtn.append(line);
			}

			return rtn.toString();
		} catch (Exception e) {
		} finally {
			if (outStream != null) { try { outStream.close(); } catch (IOException e1) { } }
			if (brd != null) { try { brd.close(); } catch (IOException e1) { } }
			if (isr != null) { try { isr.close(); } catch (IOException e1) { } }
			conn.disconnect();
		}

		return null;
	}

	private void parseJson(String data) throws UnsupportedEncodingException, ParseException {

		/*
		 * "ID": "3011600781014", "name":
		 * "Frixion Pen ปากกาลบได้ 0.5mm แบบกด - หมึกสีแดง - Minnie Dot", "price":
		 * "185", "category_id": "101176", "category_name": "ปากกา", "description":
		 * "ปากกา", "url":
		 * "http://www.pchome.co.th/pro/content/show/3011600781014/?utm_source=priceza&utm_campaign=priceza",
		 * "image_url":
		 * "http://cimg.pchome.co.th/prods/007/810/3011600781014_b.jpg?uid=151c09072c917c4c939b026bfe516e4b",
		 * "base_price": "", "upc": "", "keyword": ""
		 */

		JSONObject jResult = (JSONObject) new JSONParser().parse(data);

		JSONArray jarr = (JSONArray) jResult.get("deals");
		if (jarr == null || jarr.size() == 0) {
			next = false;
			return;
		}
		for (Object json : jarr) {
			JSONObject jObj = (JSONObject) json;
			String productName = (String) jObj.get("title").toString();
			String productPrice = (String) jObj.get("price").toString();
			String productBasePrice = (String) jObj.get("value").toString();
			String productImageUrl = (String) jObj.get("image").toString();
			String productUrl = (String) jObj.get("link").toString();
			String productCatId = (String) jObj.get("deal_category_id").toString();
			String productId = (String) jObj.get("deal_id").toString();

			productUrl = productUrl.trim();

			if (productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0) {
				return;
			}

			ProductDataBean pdb = new ProductDataBean();

			productName = StringEscapeUtils.unescapeHtml4(FilterUtil.toPlainTextString(productName.trim()));
			productUrl = productUrl.replace("\\", "");
			productImageUrl = productImageUrl.replace("\\", "");
			productPrice = StringEscapeUtils.unescapeHtml4(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			productBasePrice = StringEscapeUtils.unescapeHtml4(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);

			String[] catMap = getCategory(productCatId);
			if (catMap != null) {
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);
			} else {
				pdb.setCategoryId(0);
			}

			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
			pdb.setBasePrice(FilterUtil.convertPriceStr(productBasePrice));
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);
			pdb.setRealProductId(productId);

			if (productImageUrl.length() != 0) {
				pdb.setPictureUrl(productImageUrl);
			}

			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}

		if ((long) page >= (Long) jResult.get("max_page")) {
			next = false;
		}

	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	

}
