package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import feed.crawler.init.SilkspanAPIFeedCrawlerInit01;
import feed.crawler.init.SilkspanAPIFeedCrawlerInit02;
import feed.crawler.init.SilkspanAPIFeedCrawlerInit03;
import feed.crawler.init.SilkspanAPIFeedCrawlerInit04;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class SilkspanAPIFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	private static final int maxConcurrent = 4;
	private static boolean haveResult = false;
	
	public SilkspanAPIFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}

	@Override
	public void loadFeed() {
		
		int count = 0;
		List<silkspanThread> processList = new ArrayList<>();
		try {
		ExecutorService executor = Executors.newFixedThreadPool(maxConcurrent);
		for(int i = 0; i<SilkspanAPIFeedCrawlerInit01.paramList.length; i++) {
			String year = SilkspanAPIFeedCrawlerInit01.paramList[i][0];
			String brand = SilkspanAPIFeedCrawlerInit01.paramList[i][1];
			String model = SilkspanAPIFeedCrawlerInit01.paramList[i][2];
			silkspanThread s = new silkspanThread(year, brand, model);
			count++;
			processList.add(s);
			if(processList.size()%100==0){
					executor.invokeAll(processList);
					processList.clear();
					logger.info("Silkspan Fecth feed: "+count);
			}
		}
		for(int i = 0; i<SilkspanAPIFeedCrawlerInit02.paramList.length; i++) {
			String year = SilkspanAPIFeedCrawlerInit02.paramList[i][0];
			String brand = SilkspanAPIFeedCrawlerInit02.paramList[i][1];
			String model = SilkspanAPIFeedCrawlerInit02.paramList[i][2];
			silkspanThread s = new silkspanThread(year, brand, model);
			count++;
			processList.add(s);
			if(processList.size()%100==0){
					executor.invokeAll(processList);
					processList.clear();
					logger.info("Silkspan Fecth feed: "+count);
			}
		}
		for(int i = 0; i<SilkspanAPIFeedCrawlerInit03.paramList.length; i++) {
			String year = SilkspanAPIFeedCrawlerInit03.paramList[i][0];
			String brand = SilkspanAPIFeedCrawlerInit03.paramList[i][1];
			String model = SilkspanAPIFeedCrawlerInit03.paramList[i][2];
			silkspanThread s = new silkspanThread(year, brand, model);
			count++;
			processList.add(s);
			if(processList.size()%100==0){
					executor.invokeAll(processList);
					processList.clear();
					logger.info("Silkspan Fecth feed: "+count);

			}
		}
		for(int i = 0; i<SilkspanAPIFeedCrawlerInit04.paramList.length; i++) {
			String year = SilkspanAPIFeedCrawlerInit04.paramList[i][0];
			String brand = SilkspanAPIFeedCrawlerInit04.paramList[i][1];
			String model = SilkspanAPIFeedCrawlerInit04.paramList[i][2];
			silkspanThread s = new silkspanThread(year, brand, model);
			count++;
			processList.add(s);
			if(processList.size()%100==0){
					executor.invokeAll(processList);
					processList.clear();
					logger.info("Silkspan Fecth feed : "+count);
			}	
		}
		executor.invokeAll(processList);
		executor.shutdown();
		if(executor.awaitTermination(120, TimeUnit.HOURS)){
			logger.info("Done All.");
		}else{
			logger.info("Time out.");
			executor.shutdownNow();
		}
		} catch (InterruptedException e) {
			logger.error(e);
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	

	private class silkspanThread implements Callable<Void> {

		private String tYear;
		private String tBrand;
		private String tModel;

		silkspanThread(String inputYear, String inputBrand, String inputModel) {
			tYear = inputYear;
			tBrand = inputBrand;
			tModel = inputModel;
		}
		
		private String toMD5(String text){
			MessageDigest m = null;
			try {
				m = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				logger.error(e);
				return "";
			}
			m.reset();
			m.update(text.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			return hashtext;
		}
		
		public void parseJSON(String data) throws IOException, InterruptedException {
			try {
				int productCount = 0;
				JSONArray jArr = (JSONArray) new JSONParser().parse(data);
				if(jArr != null && jArr.size() > 0) {
					logger.info("found " + jArr.size() + " object (" + tYear + ", " + tBrand + ", " + tModel + ")");
					for (Object obj : jArr) {
						
						JSONObject jObj = (JSONObject)obj;
						String id 			  = (String)jObj.get("product_id");
						String poType 		  = (String)jObj.get("type");
						String insName 		  = (String)jObj.get("insurer");
						String brand 		  = (String)jObj.get("brand");
						String model 		  = (String)jObj.get("model");
						String subModel 	  = (String)jObj.get("sub_model");
						String year 		  = (String)jObj.get("year");
						String od 			  = (String)jObj.get("sum_insured");
						String oddd 		  = (String)jObj.get("deductible");
						String garage 		  = (String)jObj.get("repair");
						String packageName 	  = (String)jObj.get("title");
						String carAct 		  = "ไม่รวมพรบ.";
						                
						String fire 		  = (String)jObj.get("fire_damage");
						String theft 		  = (String)jObj.get("theft");
						String flood 		  = (String)jObj.get("flood_damage");
						String bi 			  = (String)jObj.get("bodily_injury_liability");
						String bt 			  = (String)jObj.get("bodily_injury_liability/Accident");
						String tp 			  = (String)jObj.get("property_damage_liability");
						String pa 			  = (String)jObj.get("personal_injury_protection");
						String bb 			  = (String)jObj.get("bail_bond");
						String me 			  = (String)jObj.get("medical_payments");

						String productPrice   = (String)jObj.get("premium");
						String productUrl 	  = (String)jObj.get("URL");
						
						if(StringUtils.isBlank(productPrice)){
							logger.info("no price : " + id + " (" + tYear + ", " + tBrand + ", " + tModel + ")");
							continue;
						}
						
						if(StringUtils.isBlank(productUrl)) {
							logger.info("no url : " + id + " (" + tYear + ", " + tBrand + ", " + tModel + ")");
							continue;
						}
						
						String subModelFromUrl = FilterUtil.getStringAfter(FilterUtil.getStringBetween(productUrl.toUpperCase(), "&MODEL=", "&"), model.toUpperCase(), "");
						
						if(StringUtils.isNotBlank(subModelFromUrl)){
							subModel = subModelFromUrl.trim().replace("-", " ");
						}else {
							logger.info("no subModelFrom Url : " + id + " (" + tYear + ", " + tBrand + ", " + tModel + ")");
						}
						
						String uniqueText = "";
						uniqueText += id;
						uniqueText += poType;
						uniqueText += insName;
						uniqueText += brand;
						uniqueText += model;
						uniqueText += subModel;
						uniqueText += year;
						uniqueText += packageName;
						uniqueText += garage;
//						uniqueText += carAct;
						uniqueText = toMD5(uniqueText);
						
//						ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
						String productName = "";
						productName += "ประกันชั้น " + poType.replace("-", " ").replace("ป", "");
						productName += " - " + insName.replace("-", " ");
						productName += " - " + brand.replace("-", " ");
						productName += " - " + model.replace("-", " ");
						productName += " - " + ((StringUtils.isNotBlank(subModel))? subModel.replace("-", " ") : "ไม่ระบุรุ่นย่อย");
						productName += " - ปี " + ((StringUtils.isNotBlank(year))? year.replace("-", " ") : "ไม่ระบุ" );
						productName += " (" + uniqueText.substring(0,10) + ")";
						productName = productName.replace(",", "");
						productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
						productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
						productName = productName.replaceAll("\\s+", " ");                                                  
						productName = productName.replace("( ", "(");                                                       
						productName = productName.replace(" )", ")");                                                       
						productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
						productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");                            
						                             
//						( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
						String productDesc = "";
						productDesc += ((StringUtils.isNotBlank(packageName))? packageName.replace("-", " ") : "ไม่ระบุแผน" );
						productDesc += " - " + ((StringUtils.isNotBlank(garage))? garage.replace("-", " "): "ไม่ระบุอู่ซ่อม");
						productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od.replace(",", "")));
						productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd.replace(",", "") ));
						productDesc += " - " + (StringUtils.isNotBlank(carAct) && carAct.equals("มี") ? "รวมพรบ." : "ไม่รวมพรบ.");
						productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire.replace(",", "") : "ไม่ระบุ" );
						productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft.replace(",", "") : "ไม่ระบุ" );
						productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood.replace(",", "") : "ไม่ระบุ" ); 
						productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi.replace(",", "") : "ไม่ระบุ" );   
						productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt.replace(",", "") : "ไม่ระบุ" );   
						productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp.replace(",", "") : "ไม่ระบุ" );    
						productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa.replace(",", "") : "ไม่ระบุ" );       
						productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb.replace(",", "") : "ไม่ระบุ" );         
						productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me.replace(",", "") : "ไม่ระบุ" ); 
						productDesc = productDesc.replaceAll("\\s+", " ");                                               
						productDesc = productDesc.replace("( ", "(");                                                    
						productDesc = productDesc.replace(" )", ")");                                                    
						productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
						productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
						productDesc = productDesc.replace(",", "");
						
						productPrice = FilterUtil.removeCharNotPrice(productPrice);
						
						String productUrlForUpdate = uniqueText;
						
						String productImage = "https://halobe.files.wordpress.com/2018/06/silkspan.jpg";
						
						ProductDataBean pdb = new ProductDataBean();
						pdb.setName(productName);
						pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
						pdb.setPictureUrl(productImage);
						pdb.setDescription(productDesc);
						pdb.setUrl(productUrl);
						pdb.setUrlForUpdate(productUrlForUpdate);
						pdb.setCategoryId(250101);
						pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");

						productCount++;
						
						String result = mockResult(pdb);
						if(StringUtils.isNotBlank(result)) {
							try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
								pwr.println(result);
								haveResult = true;
							}catch(Exception e) {
								logger.error(e);
							}
						}
					}
				}else {
					logger.info("object size 0 (" + tYear + ", " + tBrand + ", " + tModel + ")");
				}
				logger.info("Finish " + tYear + ", " + tBrand + ", " + tModel + " : " + productCount);
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
		@Override
		public Void call() throws Exception {
			try {
				
				// production
				String apiUrl = "https://www.silkspan.com:82/inspackage/getpackagepriceza?caryear="+tYear+"&carbrand="+tBrand+"&carmodel="+tModel;
				
				// uat
				// String apiUrl = "http://www.silkspan.com:81/dev/inspackage/getpackagepriceza?caryear="+tYear+"&carbrand="+tBrand+"&carmodel="+tModel;
				
				apiUrl = BotUtil.encodeURL(apiUrl);
				String[] resData = HTTPUtil.httpRequestWithStatusIgnoreCookies(apiUrl, "UTF-8", true);
				if(resData != null && resData.length == 2) {
					if(StringUtils.isNotBlank(resData[0]) && resData[1].equals("200")) {
						parseJSON(resData[0]);
					}else if (!resData[1].equals("200")){
						logger.info("response code "+resData[1]+" (" + tYear + ", " + tBrand + ", " + tModel + ")");
					}else if (StringUtils.isBlank(resData[0])) {
						logger.info("no response data (" + tYear + ", " + tBrand + ", " + tModel + ")");
					}
				}
			}catch(Exception e) {
				logger.error(e);
			}
			return null;
		}
		
		

	}
	
		private String mockResult(ProductDataBean pdb) {
			StringBuilder result = new StringBuilder();
			
			result.append("<product>");
			if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
			if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
			if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
			if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
			if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
			if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
			if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
			if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
			if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
			if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
			if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
			result.append("</product>");
			
			return result.toString();
		}

		public ProductDataBean parse(String xml) {
			
			String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
			String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
			String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
			String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
			String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
			String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
			String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
			String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
			String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
			String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
			String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

			if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
				return null;		
			}
			
			ProductDataBean pdb = new ProductDataBean();		
			pdb.setName(productName);
			
			if(StringUtils.isNotBlank(productDesc)) 		
				pdb.setDescription(productDesc);
			
			if(StringUtils.isNotBlank(productPictureUrl)) 	
				pdb.setPictureUrl(productPictureUrl);
			
			pdb.setUrl(productUrl);

			if(StringUtils.isNotBlank(productUrlForUpdate)) 
				pdb.setUrlForUpdate(productUrlForUpdate);
			else 											
				pdb.setUrlForUpdate(productUrl);
			
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
			
			if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
				pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
			
			if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
				pdb.setCategoryId(Integer.parseInt(productCatId));
			
			if(StringUtils.isNotBlank(productKeyword))
				pdb.setKeyword(productKeyword);
			
			if(StringUtils.isNotBlank(productRealProductId)) 
				pdb.setRealProductId(productRealProductId);
			
			if(StringUtils.isNotBlank(productUpc))
				pdb.setUpc(productUpc);
			
			return pdb;
		}

}

