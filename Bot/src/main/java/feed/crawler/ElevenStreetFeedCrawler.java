package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import product.processor.ProductFilter;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.SocketFactory;

public class ElevenStreetFeedCrawler extends FeedManager {
	private static final Logger logger = LogManager.getRootLogger();
	boolean haveResult = false;
	private static final String FILE_NAME = "dataHolder.json";
	private static String currentCat =  "";
	
	public ElevenStreetFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@SuppressWarnings("serial")
	Set<String> BLOCK_CAT_SET = new HashSet<String>() {{
	    add("7144");
	    add("7729");
	    add("7730");
	    add("7731");
	    add("8517");
	    add("7732");
	    add("8518");
	}};
	
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "priceza.shader.io";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

	@Override
	public void loadFeed() {
		if(BaseConfig.CONF_ENABLE_PROXY) {
			logger.info("Enable proxy !!!!!!!!!!!");
			enableUseProxy();
		}
		
    	SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
    	
		
		for(String feed:getCategoryKeySet()) {
			if(Thread.currentThread().isInterrupted())
				return;
			currentCat = feed;
			int page = 1;
			haveResult = false;
				do {
					haveResult = false;
					if(feed.isEmpty()) {
						logger.error("Feed | Cannot Load | " + feed);
						continue;
					}
					String replaceString = feed.replace("{page}",String.valueOf(page));
					HttpGet httpPost = new HttpGet(replaceString);
					httpPost.setConfig(HTTP_LOCAL_CONFIG);
					HttpEntity entity = null;
					try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
				    		CloseableHttpResponse response = httpclient.execute(httpPost);){
							entity = response.getEntity();
							InputStream in = entity.getContent();
							logger.info("url: "+replaceString);	
							Path path = new File(BaseConfig.FEED_STORE_PATH + "/" +"dataHolder.json").toPath();
							Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
					}catch (ClosedByInterruptException e) {
						logger.error(e);
						return;
					}catch (IOException e) {
						logger.error(e);
					}finally {
			    		if(httpPost != null)
							httpPost.releaseConnection();
			    		EntityUtils.consumeQuietly(entity);
			    	}			

					mockData();
					page++;
					if(haveResult) {
						BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
					}
				}while(haveResult);
		}
	}
	
	private void mockData() {
		try {
			String feed = BaseConfig.FEED_STORE_PATH + "/" +FILE_NAME;
			 JSONParser jsonParser = new JSONParser();
			   try (FileReader reader = new FileReader(feed))
		        {
		            Object obj = jsonParser.parse(reader); 
		            JSONObject employeeList = (JSONObject) obj;
		    		String xml = String.valueOf(employeeList.get("template"));
		    		Document doc = Jsoup.parse(xml);
		    		Elements eList = doc.select("li.item_list");
		    		for(Element e:eList) {
		    			parseData(e.html());
		    		}
		 
		        }
		
		
		}catch (Exception e) {
			logger.info(e);
		}
		
	}
	
	private void parseData(String xml) {	
		String productName = FilterUtil.getStringBetween(xml,"<meta itemprop=\"name\" content=\"", "\"").trim();		
		String productPrice = FilterUtil.getStringBetween(xml,"<meta itemprop=\"price\" content=\"", "\"").trim();
		String productUrl = FilterUtil.getStringBetween(xml,"href=\"", "\"").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml,"src=\"", "\"").trim();
		String productId = productUrl;
		
		String[] splitUrl = productId.split("-");
		productId = splitUrl[splitUrl.length-1];
		if(productId.contains("?")) {
			productId = FilterUtil.getStringBefore(productId,"?","");
		}
		
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 || productId.trim().length() == 0) {
			logger.info("Not complete Product ID: "+productId);
			return;		
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		productUrl = productUrl.replace("&amp;", "&");
		productPictureUrl = productPictureUrl.replace("&amp;", "&");	
		productName += " (" + productId + ")";
		pdb.setName(productName);
		if(ProductFilter.checkCOVIDInstantDelete(productName))
        	return;
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		pdb.setRealProductId(productId);
		productUrl = FilterUtil.getStringBefore(productUrl,"?callerId",productUrl);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId);
		
		if (productPictureUrl.length() != 0) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
	
		String[] catMap = getCategory(currentCat);
		if(catMap != null && catMap.length > 0) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));	
			pdb.setKeyword(catMap[1]);
		}else {
				pdb.setCategoryId(0);
				
		}
		mockResult(pdb);
	}
	
	private String mockResult(ProductDataBean pdb){
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) {return null;}
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(urlForUpdate)) result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
