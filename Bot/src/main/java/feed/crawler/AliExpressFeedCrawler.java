package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AliExpressFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private int page = 1;
	private boolean next = true;
	private boolean haveResult = false;
	
	public AliExpressFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		Set<String> keySet = getCategoryKeySet();
		
		for(String catId : keySet){
			String[] catMap = getCategory(catId);
			
			if(catMap == null || catMap.length != 2) 
				continue;
			
			String pzCatId = catMap[0];
			String keyword = catMap[1];
			page = 1;
			next = true;
			while(!Thread.currentThread().isInterrupted() && next){
				String targetUrl = "http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.listPromotionProduct/50023"
						+ "?fields=productId,productTitle,productUrl,imageUrl,salePrice"
						+ "&categoryId="+ catId
						+ "&commissionRateFrom=0.01"
//						+ "&commissionRateFrom=0.1"
						+ "&volumeFrom=0"
						+ "&startCreditScore=0"
						+ "&pageSize=40"
						+ "&pageNo=" + page;

				String[] response = HTTPUtil.httpRequestWithStatus(targetUrl, "UTF-8", true);
				if(response == null || response.length != 2) break;
				
				String data = response[0];
				if(StringUtils.isNotBlank(data)) {
					try {
						parseJson(data, pzCatId, keyword);
						if(page > 10000){
							break;
						}
					} catch (ParseException e) {
						logger.error(e);
					}
				}
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void parseJson(String data, String pzCatId, String keyword) throws ParseException {
		
		JSONObject jData = (JSONObject)new JSONParser().parse(data);
		Long code = (Long)jData.get("errorCode");
		Object result = jData.get("result");
		if(code != 20010000 || result == null){
			next = false;
			return;
		}
		
		JSONObject jResult = (JSONObject)new JSONParser().parse(result.toString());
		JSONArray jarr = (JSONArray)jResult.get("products");
		if(jarr == null || jarr.size() == 0){
			next = false;
			return;
		}
		
		page++;
		
		List<String> urlList = new ArrayList<String>();
		List<ProductDataBean> productList = new ArrayList<ProductDataBean>();
		
		for (Object obj : jarr) {
			JSONObject jObj = (JSONObject)obj;
			
			String url = String.valueOf(jObj.get("productUrl"));
			String name = String.valueOf(jObj.get("productTitle"));
			String price = String.valueOf(jObj.get("salePrice"));
			String id =  String.valueOf(jObj.get("productId"));
			url = url.trim();
			name = name.trim();
			
			if(url.isEmpty() || name.isEmpty() || price.isEmpty() || !url.contains(".com/item/")) {
				continue;
			}
			
			name = StringEscapeUtils.unescapeHtml4(name);
			try {
				String urlName = URLEncoder.encode(name, "UTF-8");
				url = url.replace("&amp;", "&");
				url = url.replace(".com/item//", ".com/item/"+ urlName + "/");
				
			} catch (UnsupportedEncodingException e1) {
				continue;
			}

			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(name);
			pdb.setUrl(url);
			pdb.setUrlForUpdate(url);
			pdb.setRealProductId(id);
			pdb.setUrlForUpdate(id);
			
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
			
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			
			String img = (String)jObj.get("imageUrl");
			if(!img.isEmpty()){
				img = img.replace("&amp;", "&");
				pdb.setPictureUrl(img);
			}

			pdb.setCategoryId(BotUtil.stringToInt(pzCatId, 0));
			pdb.setKeyword(keyword);
			
			urlList.add(pdb.getUrl());
			productList.add(pdb);
			
		}
		
		
		Map<String,String> linkTrackMap = new HashMap<String, String >();
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")){
			linkTrackMap = parseJsonMapping(urlList);
		}
		
		for(ProductDataBean pdb : productList){
			
			if(pdb == null || linkTrackMap == null) continue;
			
			if(BaseConfig.RUNTIME_TYPE.equals("WCE")){
				String trackUrl = linkTrackMap.get(pdb.getUrl());
				if(StringUtils.isBlank(trackUrl)){
					System.out.println("cannot get track link : " + pdb.getUrl());
					continue;
				}
				pdb.setUrl(trackUrl);
			}
			
			String mockData = mockResult(pdb);
			if(StringUtils.isNotBlank(mockData)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(mockData);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		
		
	}
	
	private Map<String, String> parseJsonMapping(List<String> urlList) {
		
		Map<String,String> linkTrackMap = new HashMap<String,String>();
		if(urlList != null && urlList.size() > 0){
			
			StringBuilder allUrl = new StringBuilder();
			for (String url : urlList) {
				allUrl.append(",");
				allUrl.append(url);
			}
			
			String targetUrl = "http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.getPromotionLinks/50023?fields=promotionUrl&trackingId=aliexpressth&urls=" + allUrl.toString().substring(1);
			String[] response = HTTPUtil.httpRequestWithStatus(targetUrl, "UTF-8", true);
			if(response == null || response.length != 2) return null;
			
			String data = response[0];
			if(StringUtils.isNotBlank(data)) {
				try{
					
					JSONObject jData = (JSONObject)new JSONParser().parse(data);
					Long code = (Long)jData.get("errorCode");
					Object result = jData.get("result");
					if(code != 20010000 || result == null){
						next = false;
						return null;
					}
					
					JSONObject jResult = (JSONObject)new JSONParser().parse(result.toString());
					JSONArray jarr = (JSONArray)jResult.get("promotionUrls");
					if(jarr == null || jarr.size() == 0){
						return null;
					}
					
					for (Object obj : jarr) {
						JSONObject jObj = (JSONObject)obj;
						String promotionUrl = (String)jObj.get("promotionUrl");
						String url = (String)jObj.get("url");
						if(StringUtils.isNotBlank(promotionUrl) && StringUtils.isNotBlank(url)){
							linkTrackMap.put(url, promotionUrl);
						}
					}
					
					
				}catch(Exception e){
					logger.error(e);
				}
			}
		}
		return linkTrackMap;
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getRealProductId()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

}
