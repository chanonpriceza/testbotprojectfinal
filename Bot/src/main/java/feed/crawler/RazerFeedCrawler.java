package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class RazerFeedCrawler extends FeedManager {

	public RazerFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://www.feedoptimise.com/sy/feed/5e3936ceb7242ca1b28e70cd7725c3d7.csv?campaign=TH-Channels&channel=Google-Shopping-CSV"};
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String name = data.get(1);
		String url = data.get(3);
		String image = data.get(7);
		String desc = data.get(6);
		String id = data.get(0);
		String cat3 = StringUtils.isNotBlank(data.get(18))?data.get(18):"";
		String price = data.get(5);
		String basePrice = data.get(4);
		price = FilterUtil.removeCharNotPrice(price);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		
		double temp_price = 0;
		double price_d = FilterUtil.convertPriceStr(price);
		double base_d = FilterUtil.convertPriceStr(basePrice);
		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
		
		if(price_d%1!=0)
			return null;
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" "+" ("+id+")");
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPrice(price_d);
		pdb.setBasePrice(base_d);
		pdb.setDescription(desc);
		pdb.setUrl(url);
		pdb.setPictureUrl(image);
		String[] mapping = getCategory(cat3);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		}
		
		
		return pdb;
	}
	
	

}
