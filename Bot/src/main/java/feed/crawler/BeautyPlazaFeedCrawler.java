package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BeautyPlazaFeedCrawler extends FeedManager{

	public BeautyPlazaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://beautyplazaonline.com/index.php/beautyplaza_api/product"
		};
	}

	public ProductDataBean parse(String xml) {
		
		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice = FilterUtil.getStringBetween(xml, "<base_price>", "</base_price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId = FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
		productCatId = productCatId.replace("&amp;", "&");
		String productUpc = FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty() || realProductId.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName + " (" + realProductId + ")");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals(productPrice)){
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		}
		if(StringUtils.isNotBlank(productUpc)) {
			pdb.setUpc(productUpc);
		}
		pdb.setUrl(productUrl);
		
		if (!productPictureUrl.isEmpty()) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
		}
		
		if(!productDesc.isEmpty()){
			pdb.setDescription(productDesc);
		}

		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}

		return pdb;
		
	}

}
