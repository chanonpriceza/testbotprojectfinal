package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BigCShoppingOnlineFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public BigCShoppingOnlineFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.bigc.co.th/coreapi/feed/priceza_products.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		try{

//			0 id
//			1 name
//			2 price
//			3 description
//			4 category_id
//			5 category_name
//			6 url
//			7 image_url
//			8 base_price
//			9 upc
			
			String name 		= data.get(1);
			String price 		= data.get(2);
			String basePrice	= data.get(8);
			String desc 		= data.get(3);
			String image 		= data.get(7);
			String id 			= data.get(0);
			String url  		= data.get(6);
			String category 	= data.get(4);
			String upc			= data.get(9);
			
			if(StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || !url.startsWith("http") || (StringUtils.isBlank(price) && StringUtils.isBlank(basePrice))){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();

			if(StringUtils.isNotBlank(id)){
				pdb.setRealProductId(id);
			}
			
			if(StringUtils.isNotBlank(upc)){
				pdb.setUpc(upc);
			}
			
			name = FilterUtil.removeNonData(name);
			name = StringEscapeUtils.unescapeHtml4(name);
			name = FilterUtil.toPlainTextString(name);
			if(StringUtils.isNotBlank(id)) {
				name += " (" + id + ")";
			}
			pdb.setName(name);
			
			if(StringUtils.isNotBlank(price)){
				pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			}else{
				if(StringUtils.isNotBlank(basePrice)){
					pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
				}
			}

			if(StringUtils.isNotBlank(basePrice) && StringUtils.isNotBlank(price) && !basePrice.equals(price)){
				pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
			}
			
			url = url.replace("&amp;", "&");
			url = url.replace(" ", "%20");
			url = url.replace("http:", "https:");
			pdb.setUrl(url);
			pdb.setUrlForUpdate(url);
			
			if(!image.isEmpty() && image.startsWith("http")){
				image = image.replace("&amp;", "&");
				image = image.replace("http:", "https:");
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(desc)){
				desc = desc.replace("\\s+", " ");
				desc = StringEscapeUtils.unescapeHtml4(desc);
				desc = FilterUtil.toPlainTextString(desc);
				desc = FilterUtil.removeNonData(desc);
				pdb.setDescription(desc);
			}
			
			String[] mapping = getCategory(category);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}

			return pdb;

		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
	
}
