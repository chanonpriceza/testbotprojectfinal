package feed.crawler;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class PropertyFitFeedCrawler extends FeedManager {
	
	private static String currentCat = "";
	private boolean haveResult;
	private static final Logger logger = LogManager.getRootLogger();
	private final static String request_url = "https://propertyfit.co.th/wp-json/myhome/v1/estates?currency=any";
	private final static String  FILE_NAME = "dataHolder.json";
	
	public PropertyFitFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : getCategoryKeySet()) {
			haveResult = false;
			int page = 1;
			do {
				haveResult = false;
				currentCat = feed;
				feedRequest(request_url,"UTF-8", false,page,feed);
				mockData();
				page += 1;
				if(haveResult)
					BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
			}while(haveResult);
		}
		
	}
	
	private void feedRequest(String url, String charset, boolean redirectEnable,int page,String cat) {
		URL u = null;
    	HttpURLConnection conn = null;
    	String param  = "data%5Bproperty-type%5D%5Bslug%5D=property-type&data%5Bproperty-type%5D%5BbaseSlug%5D=property_type&data%5Bproperty-type%5D%5Bkey%5D=property-type&data%5Bproperty-type%5D%5Bunits%5D=&data%5Bproperty-type%5D%5Bcompare%5D=%3D&data%5Bproperty-type%5D%5Bvalues%5D%5B0%5D%5Bname%5D={cat}&data%5Bproperty-type%5D%5Bvalues%5D%5B0%5D%5Bvalue%5D={catLower}&page={page}&limit=30&sortBy=newest&currency=any";
    	param = param.replace("{page}",page+"");
    	param = param.replace("{catLower}",cat.toLowerCase());
    	param = param.replace("{cat}", cat);
    	logger.info(param);
    	
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("accept","application/json, text/plain, */*");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
			conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		
	 		wr.write(param.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		conn.connect();
	 		
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" +FILE_NAME).toPath();
			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream()){
    					Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);	
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				logger.info("Error status : " + conn.getResponseCode() + " - " + url);
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		logger.info(e);
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return ;
	}
	
	private void mockData() {
		try {
			String feed = BaseConfig.FEED_STORE_PATH + "/" +FILE_NAME;
			 JSONParser jsonParser = new JSONParser();
			   try (FileReader reader = new FileReader(feed))
		        {
		            Object obj = jsonParser.parse(reader); 
		            JSONObject allJSON = (JSONObject) obj;
		            JSONArray jsonArray = (JSONArray)allJSON.get("results");
		        	logger.info(jsonArray.size());
		            for(Object j:jsonArray) {
		            	JSONObject castJSON = (JSONObject) j;
		            	parseData(castJSON);
		            }
		 
		        }
		
		
		}catch (Exception e) {
			logger.info(e);
		}
		
	}
	
	private void parseData(JSONObject json) {	
		Map<String,JSONArray> attrMap  = new HashMap<String, JSONArray>();
		
		String id = String.valueOf(json.get("id")).trim();
		String name = String.valueOf(json.get("name")).trim();
		String desc = "";
		JSONArray attr = (JSONArray)json.get("attributes");
		for(Object a:attr) {
			JSONObject castJSON = (JSONObject) a ;
			String attrName = String.valueOf(castJSON.get("name"));
			JSONArray attrValue = (JSONArray) castJSON.get("values");			
			if(name==null) {
				continue;
			}
			attrMap.put(attrName,attrValue);
		}
		JSONArray priceArray = (JSONArray)(json.get("price"));
		String price = (parseJSON(priceArray,"price")).trim();
		String url  = String.valueOf(json.get("link")).trim();
		String image = String.valueOf(json.get("image")).trim();
		String Bedrooms = parseJSON(attrMap.get("Bedrooms"),"name");
		String size = 	parseJSON(attrMap.get("Property size"),"name");
		String type = parseJSON(attrMap.get("Property type"),"name");
		String offerType = parseJSON(attrMap.get("Offer type"),"name");
		String Bathrooms = parseJSON(attrMap.get("Bathrooms"),"name");
		String Floor = parseJSON(attrMap.get("Floor"),"name");
		String address = String.valueOf(json.get("address")).trim();
		
		desc = "Bedrooms: "+Bedrooms+" Bathrooms: "+Bathrooms+" Floor: "+Floor+" Address: "+address;
		
		if( StringUtils.isBlank(name) ||StringUtils.isBlank(url) || StringUtils.isBlank(price) || StringUtils.isBlank(id)){
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" "+type+" "+size+" "+" "+offerType+" ("+id+")");	

		pdb.setPrice((FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);

		if(!image.isEmpty()){
			pdb.setPictureUrl(image);
		}
		
//		if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
//			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
//		}
		
		if(StringUtils.isNotBlank(desc)){
			pdb.setDescription(desc);
		}
		
		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		mockResult(pdb);
		
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(urlForUpdate)) result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		if(!productPrice.contains("E")) {
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		}else {
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));		
		}
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String parseJSON(JSONArray jsArray,String input) {
		if(jsArray != null && jsArray.size() > 0) {
			JSONObject jsObj = (JSONObject )jsArray.get(0);
			String result = String.valueOf(jsObj.get(input));
			return result;
		}
		return null;
	}
	

}
