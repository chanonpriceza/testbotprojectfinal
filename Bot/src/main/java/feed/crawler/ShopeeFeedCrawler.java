package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ShopeeFeedCrawler extends FeedManager{

	private Set<String> mallShopSet = new HashSet<>();
	private static long CRAWL_LIMIT = 23000000;
	
	private static Map<String, Object[]> catDetailMap;
	private static int allCount=0, crawlCount=0;
	
	public ShopeeFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		
		catDetailMap = new HashMap<>();
		
		if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_CRAWLER_PARAM) && NumberUtils.isCreatable(BaseConfig.CONF_FEED_CRAWLER_PARAM))
			CRAWL_LIMIT = NumberUtils.toLong(BaseConfig.CONF_FEED_CRAWLER_PARAM);
		
		mallShopSet = new HashSet<>();
		
		mallShopSet.add("57904479");    // https://shopee.co.th/daisothailand
		mallShopSet.add("21100884");    // https://shopee.co.th/all_bright
		mallShopSet.add("21612020");    // https://shopee.co.th/bbia_thailand
		mallShopSet.add("172201120");   // https://shopee.co.th/chivitdbyscgofficialshop
		mallShopSet.add("28084747");    // https://shopee.co.th/fracora_thailand
		mallShopSet.add("86793753");    // https://shopee.co.th/glowmoriofficial
		mallShopSet.add("57766351");    // https://shopee.co.th/jason_leena
		mallShopSet.add("60910726");    // https://shopee.co.th/hmckbaby
		mallShopSet.add("35237068");    // https://shopee.co.th/ymdshops
		
		try {
			String[] mallApi = HTTPUtil.httpRequestWithStatus("https://shopee.co.th/api/v2/brand_lists/get", "UTF-8", false);
			if(mallApi != null && mallApi.length == 2) {
				String data = mallApi[0];
				JSONObject jsonObj = (JSONObject) new JSONParser().parse(data);
				JSONObject dataObj = (JSONObject) jsonObj.get("data");
				Set<?> shopIdList = dataObj.keySet();
				if(shopIdList != null && shopIdList.size() > 0) {
					for (Object index : shopIdList) {
						JSONObject shopObj = (JSONObject) dataObj.get(String.valueOf(index));
						String shopId = (String) String.valueOf(shopObj.get("shopid"));
						mallShopSet.add(shopId);
					}
					logger.info("Finish list mall shopId, " + mallShopSet.size() + " items found.");
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return new String[] {
				"http://52.74.190.12/shopee/priceza_th_1.csv",
				"http://52.74.190.12/shopee/priceza_th_2.csv",
				"http://52.74.190.12/shopee/priceza_th_3.csv",
				"http://52.74.190.12/shopee/priceza_th_4.csv",
				"http://52.74.190.12/shopee/priceza_th_5.csv",
				"http://52.74.190.12/shopee/priceza_th_6.csv",
				"http://52.74.190.12/shopee/priceza_th_7.csv",
				"http://52.74.190.12/shopee/priceza_th_8.csv",
				"http://52.74.190.12/shopee/priceza_th_9.csv",
				"http://52.74.190.12/shopee/priceza_th_10.csv",
				"http://52.74.190.12/shopee/priceza_th_11.csv",
				"http://52.74.190.12/shopee/priceza_th_12.csv",
				"http://52.74.190.12/shopee/priceza_th_13.csv",
				"http://52.74.190.12/shopee/priceza_th_14.csv",
				"http://52.74.190.12/shopee/priceza_th_15.csv",
				"http://52.74.190.12/shopee/priceza_th_16.csv",
				"http://52.74.190.12/shopee/priceza_th_17.csv",
				"http://52.74.190.12/shopee/priceza_th_18.csv",
				"http://52.74.190.12/shopee/priceza_th_19.csv",
				"http://52.74.190.12/shopee/priceza_th_20.csv",
				"http://52.74.190.12/shopee/priceza_th_21.csv",
				"http://52.74.190.12/shopee/priceza_th_22.csv",
				"http://52.74.190.12/shopee/priceza_th_23.csv",
				"http://52.74.190.12/shopee/priceza_th_24.csv",
				"http://52.74.190.12/shopee/priceza_th_25.csv",
				"http://52.74.190.12/shopee/priceza_th_26.csv",
				"http://52.74.190.12/shopee/priceza_th_27.csv",
				"http://52.74.190.12/shopee/priceza_th_28.csv",
				"http://52.74.190.12/shopee/priceza_th_29.csv",
				"http://52.74.190.12/shopee/priceza_th_30.csv",
				"http://52.74.190.12/shopee/priceza_th_31.csv",
				"http://52.74.190.12/shopee/priceza_th_32.csv",
				"http://52.74.190.12/shopee/priceza_th_33.csv",
				"http://52.74.190.12/shopee/priceza_th_34.csv",
				"http://52.74.190.12/shopee/priceza_th_35.csv",
				"http://52.74.190.12/shopee/priceza_th_36.csv",
				"http://52.74.190.12/shopee/priceza_th_37.csv",
				"http://52.74.190.12/shopee/priceza_th_38.csv",
				"http://52.74.190.12/shopee/priceza_th_39.csv",
				"http://52.74.190.12/shopee/priceza_th_40.csv",
				"http://52.74.190.12/shopee/priceza_th_41.csv",
				"http://52.74.190.12/shopee/priceza_th_42.csv",
				"http://52.74.190.12/shopee/priceza_th_43.csv",
				"http://52.74.190.12/shopee/priceza_th_44.csv",
				"http://52.74.190.12/shopee/priceza_th_45.csv",
				"http://52.74.190.12/shopee/priceza_th_46.csv",
				"http://52.74.190.12/shopee/priceza_th_47.csv",
				"http://52.74.190.12/shopee/priceza_th_48.csv",
				"http://52.74.190.12/shopee/priceza_th_49.csv",
				"http://52.74.190.12/shopee/priceza_th_50.csv",
				"http://52.74.190.12/shopee/priceza_th_51.csv",
				"http://52.74.190.12/shopee/priceza_th_52.csv",
				"http://52.74.190.12/shopee/priceza_th_53.csv",
				"http://52.74.190.12/shopee/priceza_th_54.csv",
				"http://52.74.190.12/shopee/priceza_th_55.csv",
				"http://52.74.190.12/shopee/priceza_th_56.csv",
				"http://52.74.190.12/shopee/priceza_th_57.csv",
				"http://52.74.190.12/shopee/priceza_th_58.csv",
				"http://52.74.190.12/shopee/priceza_th_59.csv",
				"http://52.74.190.12/shopee/priceza_th_60.csv",
				"http://52.74.190.12/shopee/priceza_th_61.csv",
				"http://52.74.190.12/shopee/priceza_th_62.csv",
				"http://52.74.190.12/shopee/priceza_th_63.csv",
				"http://52.74.190.12/shopee/priceza_th_64.csv",
				"http://52.74.190.12/shopee/priceza_th_65.csv",
				"http://52.74.190.12/shopee/priceza_th_66.csv",
				"http://52.74.190.12/shopee/priceza_th_67.csv",
				"http://52.74.190.12/shopee/priceza_th_68.csv",
				"http://52.74.190.12/shopee/priceza_th_69.csv",
				"http://52.74.190.12/shopee/priceza_th_70.csv",
				"http://52.74.190.12/shopee/priceza_th_71.csv",
				"http://52.74.190.12/shopee/priceza_th_72.csv",
				"http://52.74.190.12/shopee/priceza_th_73.csv",
				"http://52.74.190.12/shopee/priceza_th_74.csv",
				"http://52.74.190.12/shopee/priceza_th_75.csv",
				"http://52.74.190.12/shopee/priceza_th_76.csv",
				"http://52.74.190.12/shopee/priceza_th_77.csv",
				"http://52.74.190.12/shopee/priceza_th_78.csv",
				"http://52.74.190.12/shopee/priceza_th_79.csv",
				"http://52.74.190.12/shopee/priceza_th_80.csv",
				"http://52.74.190.12/shopee/priceza_th_81.csv",
				"http://52.74.190.12/shopee/priceza_th_82.csv",
				"http://52.74.190.12/shopee/priceza_th_83.csv",
				"http://52.74.190.12/shopee/priceza_th_84.csv",
				"http://52.74.190.12/shopee/priceza_th_85.csv",
				"http://52.74.190.12/shopee/priceza_th_86.csv",
				"http://52.74.190.12/shopee/priceza_th_87.csv",
				"http://52.74.190.12/shopee/priceza_th_88.csv",
				"http://52.74.190.12/shopee/priceza_th_89.csv",
				"http://52.74.190.12/shopee/priceza_th_90.csv",
				"http://52.74.190.12/shopee/priceza_th_91.csv",
				"http://52.74.190.12/shopee/priceza_th_92.csv",
				"http://52.74.190.12/shopee/priceza_th_93.csv",
				"http://52.74.190.12/shopee/priceza_th_94.csv",
				"http://52.74.190.12/shopee/priceza_th_95.csv",
				"http://52.74.190.12/shopee/priceza_th_96.csv",
				"http://52.74.190.12/shopee/priceza_th_97.csv",
				"http://52.74.190.12/shopee/priceza_th_98.csv",
				"http://52.74.190.12/shopee/priceza_th_99.csv",
				"http://52.74.190.12/shopee/priceza_th_100.csv",
				"http://52.74.190.12/shopee/priceza_th_101.csv",
				"http://52.74.190.12/shopee/priceza_th_102.csv",
				"http://52.74.190.12/shopee/priceza_th_103.csv",
				"http://52.74.190.12/shopee/priceza_th_104.csv",
				"http://52.74.190.12/shopee/priceza_th_105.csv",
		};
	}
	
	public ProductDataBean parse(CSVRecord data) {	
		
		boolean isCrawl = true;
		boolean isMall = false;
		boolean isCb = false;
		int pzCatMapResult = 0;
		allCount++;
		
//		0	additional_image_link
//		1	brand
//		2	condition
//		3	product_description
//		4	id
//		5	is_cb
//		6	is_lpg
//		7	link
//		8	onelink_url
//		9	price
//		10	sale_price
//		11	shop_name
//		12	shop_penalty
//		13	stock
//		14	title
//		15	image_link
//		16	category
//		17	category_id
		
		String productName 			= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(14)), "");
		String productPrice 		= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(9 )), "");
		String productSalePrice 	= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(10)), "");
		String productUrl 			= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(7 )), "");
		String productImageUrl 		= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(15)), "");
		String productCatId			= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(17)), "");
		String productCatName		= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(16)), "");
		String productDesc 			= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(3)), "");
		String productId 			= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(4)), "");
		String statusCb				= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(5)), "");
		String status 				= (String) StringUtils.defaultIfBlank(String.valueOf((Object) data.get(13)), "");
		
		if(StringUtils.isBlank(productUrl) || StringUtils.isBlank(productName) || 
		    StringUtils.isBlank(productPrice) || StringUtils.isBlank(productId) || 
		    (StringUtils.isNotBlank(status) && status.equals("0"))) {
			isCrawl = false;
		}
		
		String shopId = FilterUtil.getStringBetween(productUrl, "/universal-link/product/", "/");
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			if(!mallShopSet.contains(shopId)) {
				if((ReportManager.getInstance().getReport().getCountOld() + ReportManager.getInstance().getReport().getAdd()) > CRAWL_LIMIT) {
					isCrawl = false;
				}
			}
		}
		
		if(mallShopSet.contains(shopId)) 
			isMall = true;
		
		if(statusCb.equals("1"))
			isCb = true;
		
		productUrl = StringEscapeUtils.unescapeHtml4(productUrl);
		productImageUrl = StringEscapeUtils.unescapeHtml4(productImageUrl);
						
		ProductDataBean pdb = new ProductDataBean();
		
		productName = BotUtil.removeNonUnicodeBMP(productName);
	
		pdb.setName(productName.trim() + " (" + productId.trim() + ")");
		pdb.setRealProductId(productId.trim());
		
		if(productSalePrice.length() > 0){
			productSalePrice = FilterUtil.removeCharNotPrice(productSalePrice);
			pdb.setPrice(FilterUtil.convertPriceStr(productSalePrice));
		}
		
		if(StringUtils.isNotBlank(productPrice) && !productPrice.equals(productSalePrice)){
			pdb.setBasePrice(FilterUtil.convertPriceStr(productPrice));
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId.trim());
		if(productDesc.length() > 0){
			productDesc = BotUtil.removeNonUnicodeBMP(productDesc);
			pdb.setDescription(productDesc);
		}

		String[] catMap = getCategory(productCatId);
		if(catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pzCatMapResult = (BotUtil.stringToInt(catMap[0], 0));
			if(StringUtils.isNotBlank(catMap[1]) && catMap[1].contains("|")) {
				String[] keywordArgument = catMap[1].split("\\|");
				if(keywordArgument != null && keywordArgument.length > 0) {
					for (String ka : keywordArgument) {
						try {
							if(ka.contains("max-")) {
								long filterPrice = Long.parseLong(FilterUtil.getStringAfter(ka, "max-", BotUtil.CONTACT_PRICE_STR));
								if(pdb.getPrice() > filterPrice) isCrawl = false;
							}else if(ka.contains("min-")) {	
								long filterPrice = Long.parseLong(FilterUtil.getStringAfter(ka, "min-", "0"));
								if(pdb.getPrice() < filterPrice) isCrawl = false;
							}else {
								pdb.setKeyword(ka);
							}
						}catch(NumberFormatException e) {
							pdb.setKeyword(catMap[1]);
						}
					}
				}
			}else {
				pdb.setKeyword(catMap[1]);
			}
		}else{
			pdb.setCategoryId(0);
		} 
		
		updateCatDetail(productCatId, productCatName, isCrawl, isMall, isCb, pzCatMapResult);
		if(isCrawl) {
			crawlCount++;
			return pdb;
		} else {
			return null;		
		}
	}

	
	private void updateCatDetail(String productCatId, String productCatName, boolean isCrawl, boolean isMall, boolean isCb, int pzCatMapResult ) {
		Object[] catMapDetail = catDetailMap.getOrDefault(productCatId, new Object[] {(String) "", (long) 0, (long) 0, (long) 0, (long) 0, (long) 0, (long) 0,(long) 0});
		long countAll = (long) catMapDetail[1];
		long countAllCrawl = (long) catMapDetail[2];
		long countMallAll = (long) catMapDetail[3];
		long countMallCrawl = (long) catMapDetail[4];
		long countCbAll = (long) catMapDetail[5];
		long countCbCrawl = (long) catMapDetail[6];
		
		countAll++;
		if(isCrawl)
			countAllCrawl++;
		
		if(isMall) {
			countMallAll++;
			if(isCrawl)
				countMallCrawl++;
		}
		
		if(isCb) {
			countCbAll++;
			if(isCrawl)
				countCbCrawl++;
		}
		
		catMapDetail = new Object[] {productCatName, countAll, countAllCrawl, countMallAll, countMallCrawl, countCbAll, countCbCrawl, pzCatMapResult};
		catDetailMap.put(productCatId, catMapDetail);
		
	}
	
	@Override
	public void finishRunning() throws SQLException {
		logger.info(String.format("%-12s = %d", "allCount", allCount));
		logger.info(String.format("%-12s = %d", "crawlCount", crawlCount));
		
		if(catDetailMap != null && catDetailMap.size() > 0) {
			String catDetailFilePath = BaseConfig.FEED_STORE_PATH + "/catDetailFile.xml";
			try(PrintWriter pwr_catCount = new PrintWriter(new FileOutputStream(new File(catDetailFilePath),  false));){
				pwr_catCount.println("catId^catName^allCount^crawlCount^mallCount^mallCrawlCount^cbCount^cbCrawlCount^pzCatMappingTo");
				for(Entry<String, Object[]> catMapDetail : catDetailMap.entrySet()) {
					pwr_catCount.println(catMapDetail.getKey() + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[0]), "") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[1]), "1") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[2]), "1") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[3]), "0") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[4]), "0") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[5]), "0") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[6]), "0") + 
							"^" + StringUtils.defaultIfBlank(String.valueOf(catMapDetail.getValue()[7]), "0"));
				}
			} catch(Exception e) {
				logger.error(e);
			}
		}
		
		super.finishRunning();
	}
	
}