package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class MakeWebEasyFeedCrawler extends FeedManager {
	
	private static final Logger logger = LogManager.getRootLogger(); 
	private static String hostName = null;
	private boolean haveResult = false;
	private static int count = 0;
	
	public MakeWebEasyFeedCrawler() throws Exception {
		
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		String html = "";
		for(String startUrl:getCategoryKeySet()) {
			hostName = genHostName(startUrl);
			if(Thread.currentThread().isInterrupted()) {
				logger.info("Killed !!");
				return;
			}
			int page = 1;
			do {
				
				String[] response = HTTPUtil.httpRequestWithStatusIgnoreCookies(startUrl+"&p="+page,"UTF-8",true);
				
				if(response == null ||response.length < 2||!"200".equals(response[1])) {
					break;
				}
				
				html = response[0];
				Document doc = Jsoup.parse(html);
				Elements e = doc.select("div.productWidget");
				html = e.html();
				
				if(StringUtils.isBlank(html) || html.contains("ไม่พบสินค้า")) {
					break;
				}
				
				logger.info("request url: --> "+startUrl+"&p="+page);
				parseHTML(html,startUrl);
				count++;
				page++;
				
			}while(html!=null);
			
			if(haveResult)
				BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		}
		logger.info("workLoad count :"+count);
	}
	
	private void parseHTML(String html,String cat) {
		Document productList = Jsoup.parse(html);
		Elements cut = productList.select("div.thumbnail.rounded");
		for(Element p:cut) {
			Document doc = Jsoup.parse(p.html());
			
			Elements nameSelector = doc.select("div.productName");
			Elements productIntro = doc.select("div.productIntro");
			Elements urlSelector = doc.select("div.productName");
			Elements priceSelector = doc.select("span.NmPrice");
			Elements basePriceSelector = doc.select("span.SpePrice");
			
			String name = nameSelector.html();
			name = FilterUtil.getStringBetween(name,"<h4>","</h4>");
			String url = urlSelector.html();
			String info = productIntro.html();
			if(StringUtils.isBlank(info)) {
				info = FilterUtil.toPlainTextString(info);
				name = name+" "+info;
			}
			url = FilterUtil.getStringBetween(url,"href=\"","\"");
			url = url.startsWith("http")? url:hostName+url;
			String price = priceSelector.html();
			price = FilterUtil.removeCharNotPrice(price);
			String basePrice = basePriceSelector.html();
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
			String holderPrice = basePrice;
			if(StringUtils.isNotBlank(basePrice)) {
				basePrice = price;
				price = holderPrice;
			}
			String picUrl =  FilterUtil.getStringBetween(doc.html(),"data-src=\"","\"");
//			if(StringUtils.isBlank(picUrl)) {
//				logger.info("Non pic html: ------------>"+html);
//				logger.info("Non pic ------------>"+picUrl);
//			}
			//logger.info("pic : --------->"+picUrl);
			if(StringUtils.isBlank(price)) {
				price = BotUtil.CONTACT_PRICE_STR;
			}
			
			if(StringUtils.isBlank(name)||StringUtils.isBlank(url)||StringUtils.isBlank(price)) {
				logger.info("Data not complete url: "+url+" name: "+name);
			}	
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(name);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
			pdb.setPictureUrl(picUrl);
			pdb.setUrl(url);
			
			String[] mapping = getCategory(cat);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}
			
			String mockData = mockResult(pdb);
			if(StringUtils.isNotBlank(mockData)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					haveResult = true;
					pwr.println(mockData);
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();
	
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);
	
		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}	
	
	private String genHostName(String url){
		boolean isHttps = url.startsWith("https");
		URI uri;
		try {
			uri = new URI(url);
		    String domain = uri.getHost();
		    domain = isHttps?"https://"+domain:"http://"+domain;
		    return  domain;
		} catch (URISyntaxException e) {
			logger.info(e);
			return null;
		}
	}
		
}
