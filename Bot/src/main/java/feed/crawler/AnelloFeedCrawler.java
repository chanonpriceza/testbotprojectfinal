package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AnelloFeedCrawler extends FeedManager{

	public AnelloFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://s3-ap-southeast-1.amazonaws.com/prod.google-merchant/anello_vw_product_feeding_data_en.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		
		String id = data.get(0);
		String name = data.get(1);
		String desc = data.get(2);
		String url  = data.get(3);
		String image = data.get(4);
		String expire = data.get(5);
		String basePrice = data.get(6);
		String price = data.get(7);
		String category = data.get(9);
		String color = data.get(15);

		if("out of stock".equals(expire)){
			return null;
		}
		
		if( StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
			return null;
		}

		ProductDataBean pdb = new ProductDataBean();
		name = name + "-" + color + "("+id+")";
		pdb.setName(name);	
		pdb.setPrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		
		if(!image.isEmpty()){
			pdb.setPictureUrl(image);
		}
		
		if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
			pdb.setBasePrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice))));
		}
		
		if(StringUtils.isNotBlank(desc)){
			desc = StringEscapeUtils.unescapeHtml4(desc);
			pdb.setDescription(desc);
		}
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}

}
