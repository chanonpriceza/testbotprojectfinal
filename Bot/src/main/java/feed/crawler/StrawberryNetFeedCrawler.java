package feed.crawler;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class StrawberryNetFeedCrawler extends FeedManager{

	public StrawberryNetFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://www3.strawberrynet.com/productfeed/priceza/priceza_th.xml"
		};
	}
	
	public ProductDataBean parse(String xml){
		
		String productName = FilterUtil.getStringBetween(xml, "<product-name>", "</product-name>").trim();		
		String productPrice = FilterUtil.getStringBetween(xml, "<product-price>", "</product-price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<product-url>", "</product-url>").trim();
		String productImageUrl = FilterUtil.getStringBetween(xml, "<product-image-url>", "</product-image-url>");
		String productCatId = FilterUtil.getStringBetween(xml, "<product-sub-cat>", "</product-sub-cat>");
		String productDesc = FilterUtil.getStringBetween(xml, "<product-desc>", "</product-desc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;
		}
		
		productUrl = productUrl.replace("&amp;", "&").replace(" ", "");
		productImageUrl = productImageUrl.replace("&amp;", "&").replace(" ", "");
		productCatId = productCatId.replace("&amp;", "&");
		
		String productId = productUrl.replaceAll(".*ProdId=(\\d*)&?.*", "$1");
		
		ProductDataBean pdb = new ProductDataBean();
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productName += " (" + productId + ")";
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));	
				
		if (!productDesc.isEmpty()) {
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			productDesc = FilterUtil.toPlainTextString(productDesc);
			pdb.setDescription(productDesc);
		}
		if (!productImageUrl.isEmpty()) {
			pdb.setPictureUrl(productImageUrl);
		}		
		pdb.setUrl(productUrl);
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			if(BaseConfig.RUNTIME_TYPE.equals("WCE")){
				return null;
			}
		}
		
		return pdb;
	}
	
}
