package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class RalunarFeedCrawler extends FeedManager {

	public RalunarFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://ralunar-pro.s3-ap-southeast-1.amazonaws.com/priceza-product-feed/ralunar-product-feed.csv"};
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {

		String name = data.get("title");
		String price = data.get("sale_price");
		String basePrice = data.get("price");
		String id = data.get("id");
		String desc = data.get("description");
		String url  = data.get("link");
		String image = data.get("image_link");
		String cat = data.get("product_type");
		String gender = data.get("gender");
		String expire = data.get("availability");
		if(expire.contains("out of stock")) {
			return null;
		}
		cat = StringEscapeUtils.unescapeHtml4(cat);
		desc = FilterUtil.toPlainTextString(desc);

		url = StringEscapeUtils.unescapeHtml4(url);
		cat = StringEscapeUtils.unescapeHtml4(cat);
		cat = cat+"|"+gender;
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		
		if("null".equals(desc)) {
			desc  = null;
		}
		String tmp = "";
		
		if(StringUtils.isNotBlank(id)) 
			tmp = " ("+id+")";
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		ProductDataBean pdb = new ProductDataBean();

		pdb.setName(name+tmp);
		pdb.setPrice(p);
		pdb.setDescription(desc);
		pdb.setBasePrice(bp);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		String[] catMap = getCategory(cat);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0);
		}
		return pdb;
	}

}
