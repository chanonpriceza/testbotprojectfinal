package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class HonestBeeFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public HonestBeeFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){
		try{

//			0 Products ID
//			1 Products Name
//			2 Products Name En
//			3 Products Price
//			4 Products Normal Price
//			5 Products Description
//			6 Products Categories Name
//			7 Products URL Product Page En
//			8 Products Image URL
			
			String id = data.get(0);
			String name = data.get(1);
			String nameEn = data.get(2);
			String price = data.get(3);
			String basePrice = data.get(4);
			String desc = data.get(5);
			String category = data.get(6);
			String url  = data.get(7);
			String image = data.get(8);
			
			if(!StringUtils.isNumeric(id) || StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || !url.startsWith("http") || StringUtils.isBlank(price)){
				return null;
			}
			ProductDataBean pdb = new ProductDataBean();
			
			if(id != null && !id.isEmpty()) {
				pdb.setRealProductId(id);
			}
			
			name = name + " (" + nameEn + ")";
			name = StringEscapeUtils.unescapeHtml4(name);
			name = FilterUtil.toPlainTextString(name);
			pdb.setName(name);
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			if(!price.equals(basePrice)){
				pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
			}

			url = url.replace("&amp;", "&");
			url = url.replace("/en/", "/th/");
			pdb.setUrl(url);
			
			if(!image.isEmpty() && image.startsWith("http")){
				image = image.replace("&amp;", "&");
				pdb.setPictureUrl(image);
			}
			
			desc = category + "-" + name + " " + nameEn;
			
			if(StringUtils.isNotBlank(desc)){
				desc = desc.replace("\\s+", " ");
				desc = StringEscapeUtils.unescapeHtml4(desc);
				desc = FilterUtil.toPlainTextString(desc);
				desc = FilterUtil.removeNonData(desc);
				pdb.setDescription(desc);
			}
			
			String[] mapping = getCategory(category);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);	
				if(BotUtil.stringToInt(mapping[0],0) == 170405){
					pdb.setPictureUrl("https://hdskasl.files.wordpress.com/2017/01/s4tsxm.png");
				}
			} else {
				pdb.setCategoryId(0);
			}

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
}
