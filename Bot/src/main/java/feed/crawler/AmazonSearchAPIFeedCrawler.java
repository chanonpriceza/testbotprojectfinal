package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;

import org.apache.commons.codec.binary.Base64;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit01;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit02;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit03;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit04;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit05;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit06;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit07;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit08;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit09;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit10;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit11;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit12;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit13;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit14;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit15;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit16;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit17;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit18;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit19;
import feed.crawler.init.AmazonSearchAPIFeedCrawlerInit20;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AmazonSearchAPIFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private static final String UTF8_CHARSET = "UTF-8";
	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
	private static final String REQUEST_URI = "/onca/xml";
	private static final String REQUEST_METHOD = "GET";
	private String endpoint = "webservices.amazon.com"; // must be lowercase
	private String awsAccessKeyId = "AKIAIMCPQY45DVN3WDTA";
	private String awsSecretKey = "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";
	private SecretKeySpec secretKeySpec = null;
	private Mac mac = null;
	private boolean haveResult = false;
	
	public AmazonSearchAPIFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	public void startCrawlProduct() {
		
//		String searchIndex 	= "Books";
//		String keyword 		= "Bound Series Paperback";
//		String catId 		= "230204";
//		String keywordPZ 	= "Bound Series Paperback";
//		crawlProduct(searchIndex,keyword,catId,keywordPZ);
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit01.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit01.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit01.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit01.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit01.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit02.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit02.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit02.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit02.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit02.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit03.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit03.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit03.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit03.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit03.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit04.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit04.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit04.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit04.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit04.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit05.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit05.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit05.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit05.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit05.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit06.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit06.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit06.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit06.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit06.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit07.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit07.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit07.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit07.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit07.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit08.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit08.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit08.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit08.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit08.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit09.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit09.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit09.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit09.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit09.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit10.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit10.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit10.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit10.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit10.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit11.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit11.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit11.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit11.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit11.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit12.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit12.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit12.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit12.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit12.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit13.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit13.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit13.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit13.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit13.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit14.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit14.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit14.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit14.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit14.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit15.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit15.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit15.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit15.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit15.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit16.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit16.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit16.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit16.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit16.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit17.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit17.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit17.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit17.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit17.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit18.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit18.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit18.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit18.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit18.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit19.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit19.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit19.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit19.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit19.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}
		
		for (int i = 0; i < AmazonSearchAPIFeedCrawlerInit20.KEYWORD.length; i++) {
			String searchIndex = AmazonSearchAPIFeedCrawlerInit20.KEYWORD[i][0];
			String keyword = AmazonSearchAPIFeedCrawlerInit20.KEYWORD[i][1];
			String catId = AmazonSearchAPIFeedCrawlerInit20.KEYWORD[i][2];
			String keywordPZ = AmazonSearchAPIFeedCrawlerInit20.KEYWORD[i][3];
			crawlProduct(searchIndex,keyword,catId,keywordPZ);
		}

		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	private void crawlProduct(String searchIndex, String keyword, String catId, String keywordPZ){
		try {
			for(int page = 1; page <= 10; page++){
				String url = sign(searchIndex, keyword, page);
				String[] response = HTTPUtil.httpRequestWithStatus(url, "UTF-8", true);
				if(response == null || response.length != 2) return;
				String data = response[0];
				if(StringUtils.isNotBlank(data)) {
					List<String> itemList = FilterUtil.getAllStringBetween(data, "<Item>", "</Item>");
					if(itemList != null && itemList.size() > 0){
						for (String itemData : itemList) {
							try {
								parseXML(searchIndex,itemData,catId,keywordPZ);
							} catch (Exception e) {
								logger.error(e);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	private void parseXML(String searchIndex, String data, String catId, String keyword){
		if(StringUtils.isBlank(data) || StringUtils.isBlank(catId) || StringUtils.isBlank(keyword)){
			return;
		}
		
		String name 		= FilterUtil.getStringBetween(data, "<Title>", "</Title>");
		String desc 		= FilterUtil.getStringBetween(data, "<Content>", "</Content>");
		String price 		= FilterUtil.getStringBetween(data, "<ListPrice>", "</ListPrice>");
		String picture 		= FilterUtil.getStringBetween(data, "<LargeImage>", "</LargeImage>");
		String url	 		= FilterUtil.getStringBetween(data, "<DetailPageURL>", "</DetailPageURL>");
		String isbn 		= FilterUtil.getStringBetween(data, "<ISBN>", "</ISBN>");
		
		if(StringUtils.isBlank(name) || StringUtils.isBlank(price) || StringUtils.isBlank(url)){
			return;
		}
		
		if(searchIndex.equals("Books")){
			name = name + " (" + FilterUtil.toPlainTextString(isbn) + ")" ;
		}
		
		price = FilterUtil.getStringBetween(price, "<FormattedPrice>", "</FormattedPrice>");
		price = FilterUtil.toPlainTextString(price);
		price = FilterUtil.removeCharNotPrice(price);
		
		picture = FilterUtil.getStringBetween(picture, "<URL>","</URL>");
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(FilterUtil.toPlainTextString(StringEscapeUtils.unescapeHtml4(name)));
		pdb.setDescription(FilterUtil.toPlainTextString(StringEscapeUtils.unescapeHtml4(desc)));
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setPictureUrl(FilterUtil.toPlainTextString(picture));
		pdb.setUrlForUpdate(FilterUtil.toPlainTextString(url));
		pdb.setUrl(FilterUtil.toPlainTextString(url));
		pdb.setCategoryId(Integer.parseInt(catId));
		pdb.setKeyword(keyword);
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	public void SignedRequestsHelper()
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] secretyKeyBytes = awsSecretKey.getBytes(UTF8_CHARSET);
		secretKeySpec = new SecretKeySpec(secretyKeyBytes, HMAC_SHA256_ALGORITHM);
		mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(secretKeySpec);
	}

	public String sign(String searchIndex, String keyword, int page)throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		SignedRequestsHelper();
		
		Map<String, String> params = new HashMap<String, String>();
		
		//String paramss = "&Operation=ItemSearch&Keywords=the%20hunger%20games";
		params.put("AWSAccessKeyId", awsAccessKeyId);
		params.put("AssociateTag", "priceza09-20");
		//params.put("Operation", "BrowseNodeLookup");
		//params.put("BrowseNodeId", "20");
		//params.put("IdType", "SKU");
		
		//params.put("ResponseGroup", "Images,ItemAttributes,Offers,RelatedItems");
		params.put("Operation", "ItemSearch");
		//params.put("BrowseNode", "8604904011");
		//params.put("Condition", "All");
		params.put("ResponseGroup", "Medium,ItemAttributes");
		params.put("SearchIndex", searchIndex);
		params.put("Keywords", keyword);
		//params.put("VariationPage", "1");
		
		params.put("ItemPage", Integer.toString(page));
		params.put("Availability", "Available");

		//params.put("Availability", "Available");
		//params.put("Title", "Harry%20Potter");
		params.put("Timestamp", timestamp());

		SortedMap<String, String> sortedParamMap = new TreeMap<String, String>(params);
		String canonicalQS = canonicalize(sortedParamMap);
		String toSign = REQUEST_METHOD + "\n" + endpoint + "\n" + REQUEST_URI + "\n" + canonicalQS;

		String hmac = hmac(toSign);
		String sig = percentEncodeRfc3986(hmac);
		String url = "http://" + endpoint + REQUEST_URI + "?" + canonicalQS + "&Signature=" + sig;

		return url;
	}

	private String hmac(String stringToSign) {
		String signature = null;
		byte[] data;
		byte[] rawHmac;
		try {
			data = stringToSign.getBytes(UTF8_CHARSET);
			rawHmac = mac.doFinal(data);
			signature = new String(Base64.encodeBase64(rawHmac));
		} catch (UnsupportedEncodingException e) {throw new RuntimeException(UTF8_CHARSET + " is unsupported!", e);
		}
		return signature;
	}

	private String timestamp() {
		String timestamp = null;
		Calendar cal = Calendar.getInstance();

		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
		timestamp = dfm.format(cal.getTime());
		return timestamp;
	}

	private String canonicalize(SortedMap<String, String> sortedParamMap) {
		if (sortedParamMap.isEmpty()) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		Iterator<Map.Entry<String, String>> iter = sortedParamMap.entrySet().iterator();
		while (!Thread.currentThread().isInterrupted() && iter.hasNext()) {
			Map.Entry<String, String> kvpair = iter.next();
			buffer.append(percentEncodeRfc3986(kvpair.getKey()));
			buffer.append("=");
			buffer.append(percentEncodeRfc3986(kvpair.getValue()));
			if (iter.hasNext()) {
				buffer.append("&");
			}
		}
		String canonical = buffer.toString();
		return canonical;
	}

	private String percentEncodeRfc3986(String s) {
		String out;
		try {
			out = URLEncoder.encode(s, UTF8_CHARSET).replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
		} catch (UnsupportedEncodingException e) {
			out = s;
		}
		return out;
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

	
}
