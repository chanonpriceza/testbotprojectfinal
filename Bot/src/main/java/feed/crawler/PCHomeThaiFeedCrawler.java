package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;
import web.parser.image.ImageParser;

public class PCHomeThaiFeedCrawler extends FeedManager{

	public PCHomeThaiFeedCrawler() throws Exception {
		super();
	}
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale/";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";

	@Override
	public void loadFeed() {
		BaseConfig.IMAGE_PARSER = new ImageParser();
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	public ProductDataBean parse(CSVRecord csvRecord) {
		/*
		 	0id
			1name
			2price
			3description
			4category_id
			5category_name
			6url
			7image_url
			8base_price
			9upc
			10keyword
		 */
		
		String productName 		= String.valueOf(csvRecord.get(1));	
		String productPrice 	= String.valueOf(csvRecord.get(2));
		String productBasePrice = String.valueOf(csvRecord.get(8));	
		String productDesc 		= String.valueOf(csvRecord.get(3));
		String productImageUrl 	= String.valueOf(csvRecord.get(7));   
		String productUrl 		= String.valueOf(csvRecord.get(6));
		String productCatId 	= String.valueOf(csvRecord.get(4));   
		String productId 		= String.valueOf(csvRecord.get(0));
		
		productUrl = productUrl.trim();
		
		if(productUrl.length() == 0|| !productUrl.startsWith("http") || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;			
		}
				
		ProductDataBean pdb = new ProductDataBean();
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0);
		}
		
		productName = StringEscapeUtils.unescapeHtml4(FilterUtil.toPlainTextString(productName.trim()));
		productName += " (" + productId +")";
		pdb.setName(productName);
		
		productUrl = productUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace("http://","https://");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productId);
		pdb.setRealProductId(productId);
		
		if (productDesc.length() != 0) {
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			pdb.setDescription(FilterUtil.toPlainTextString(productDesc));
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		return pdb;
		
	}
	
}
