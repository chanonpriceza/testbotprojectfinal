package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import web.parser.image.IgnoreSSL;

public class TiresbidFeedCrawler extends FeedManager {
	
	static String url = "https://tiresbid.com/extensions/devsource/dev.sugarfree/dealer/api.php?type=filter_product&search_type=car&car_name=Alfa%20Romeo&car_series=147&car_model=3dr%20Sedan%202.0L%20(2001)&per_page=10&page={page}&filter=&no_zero=1";
	
	static JSONParser parser = new JSONParser();
	
	public TiresbidFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		for(int i = 1;i<=10;i++) {
			try {		
				JSONObject obj = (JSONObject) parser.parse(new InputStreamReader(new URL(url.replace("{page}",i+"")).openStream()));
				JSONArray array = (JSONArray)obj.get("data_lists");
				if(array==null||array.size()==0) {
					break;
				}
				JSONArray result = extractJSON(array);
				InputStream is = new ByteArrayInputStream(result.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+BaseConfig.MERCHANT_ID+"-"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING);
				
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("Error",e);
			}
		}
		BaseConfig.IMAGE_PARSER = new IgnoreSSL();
		logger.info("Ignore!!");
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
		
	}
	
	@SuppressWarnings("unchecked")
	private JSONArray extractJSON(JSONArray arr) {
		JSONArray result = new JSONArray();
		for(Object a:arr) {
			JSONObject obj = new JSONObject();
			JSONObject cast = (JSONObject) a;
			obj.put("product_id",cast.get("product_id"));
			obj.put("price",cast.get("price"));
			obj.put("cost_price",cast.get("cost_price"));
			obj.put("title",cast.get("title"));
			obj.put("product_id",cast.get("product_id"));
			JSONArray data = (JSONArray)cast.get("product_data");
			JSONArray image = (JSONArray)(((JSONObject) data.get(0)).get("product_image"));
			String img_url = "";
			if(image!=null&&image.size()>0) {
				JSONObject imageobj = (JSONObject)image.get(0);
				img_url = String.valueOf(imageobj.get("image_url"));
			}
			obj.put("img_url",img_url);
			result.add(obj);
		}
		return result;
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String name = String.valueOf(data.get("title"));
		String id = String.valueOf(data.get("product_id"));
		String price = String.valueOf(data.get("price"));
		String img = String.valueOf(data.get("img_url"));
		String url = ("https://tiresbid.com/product-detail/"+id);
		String basePrice = String.valueOf(data.get("cost_price"));
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ยางรถยนต์ ("+id+")");
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setUrl(url);
		pdb.setPictureUrl(img);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setCategoryId(70105);
		pdb.setKeyword("tires ยางรถยนต์");
		return pdb;
	}
	
}
