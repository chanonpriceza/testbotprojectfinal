package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ThaiInkFeedCrawler extends FeedManager{

	public ThaiInkFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://www.thaiink.net/feed-priceza/product"
		};
	}
	
	public ProductDataBean parse(String xml) {

		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice = FilterUtil.getStringBetween(xml, "<base_price>", "</base_price>").trim();
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productImageUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String productCatId = FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
		String realProductId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		
		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;			
		}
		
		double price = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice));
		
		ProductDataBean pdb = new ProductDataBean();	
		
		if (StringUtils.isNotBlank(realProductId)) {
			productName += " ("+realProductId+")";
			pdb.setRealProductId(realProductId);
		}
		pdb.setName(productName);
		pdb.setPrice(price);	

		if(!productBasePrice.isEmpty()){
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			pdb.setBasePrice(FilterUtil.convertPriceStr(productBasePrice));	
		}
		if (!productImageUrl.isEmpty()) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		if (!productUrl.isEmpty()) {		
			pdb.setUrl(productUrl);
		}
		
		if (!productDesc.isEmpty()) {
			productDesc = FilterUtil.toPlainTextString(productDesc);
			pdb.setDescription(productDesc);
		}
		
		if(!productCatId.isEmpty()){
			String[] catMap = getCategory(productCatId);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}
		}
		
		return pdb;
		
	}
	
}
