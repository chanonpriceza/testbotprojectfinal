package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateJDCentralDistributedFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public TemplateJDCentralDistributedFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://tdc.jd.co.th/getFeedXml.do"
			};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed+" at:"+new Date());
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				editFileReplaceNewLine(fileName);
			}catch (ClosedByInterruptException e) {
				logger.error(e);
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		logger.info("Finish : Load : Feed at:"+new Date());
	}
	
	private void editFileReplaceNewLine(String fileName) {
		String newFileName = BaseConfig.FEED_STORE_PATH + "/editedFile.xml";
		boolean haveResult = false;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(BaseConfig.FEED_STORE_PATH +"/"+fileName),"UTF-8"), 10);
			 PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/editedFile.xml"),  false))){
			
			String text = "";
			while((text = br.readLine()) != null) {
				text = text.replaceAll("\r\n", "");
				pwr.print(text.toString().replaceAll("</item>","</item>\n"));
				haveResult = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {newFileName};
	}
	
	public ProductDataBean parse(String xml){
		
//		<item>
//			<g:extra_deeplink_url_ios>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/4194545</g:extra_deeplink_url_ios>
//			<g:gtin/>
//			<g:image_link>https://img.jd.co.th/n0/s340x340_jfs/t1/230/967437063/923442/fb449ccd/5dd4c5f2N5b4764e4.png</g:image_link>
//			<g:color/>
//			<g:gender>unisex</g:gender>
//			<g:link>https://www.jd.co.th/product/eaudio-e01-pro_4194545.html</g:link>
//			<g:description>Eaudio E01 Pro USB C</g:description>
//			<g:extra_deeplink_url_andriod>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/4194545</g:extra_deeplink_url_andriod>
//			<g:availability>in stock</g:availability>
//			<g:title>Eaudio E01 Pro</g:title>
//			<g:sale_price>890 THB</g:sale_price>
//			<g:condition>new</g:condition>
//			<g:product_type>อุปกรณ์อิเล็กทรอนิกส์ &amp;gt; เครื่องเล่นเสียงและวีดีโอ &amp;gt; เครื่องเสียงและโฮมเธียร์เตอร์</g:product_type>
//			<g:size/>
//			<g:price>890 THB</g:price>
//			<g:vendor_id>16287</g:vendor_id>
//			<g:code_of_vendor/>
//			<g:mobile_link>https://m.jd.co.th/product/4194545.html</g:mobile_link>
//			<g:id>4194545</g:id>
//			<g:unit_pricing_measure>0.1 kg</g:unit_pricing_measure>
//			<g:adult>no</g:adult>
//			<g:brand>E-Audio</g:brand>
//			<g:google_product_category>222</g:google_product_category>
//		</item>
	    
		String productName 		= FilterUtil.getStringBetween(xml, "<g:title>", "</g:title>").trim();		
		String productBrand		= FilterUtil.getStringBetween(xml, "<g:brand>", "</g:brand>").trim();		
		String productColor		= FilterUtil.getStringBetween(xml, "<g:color>", "</g:color>").trim();		
		String productPrice 	= FilterUtil.getStringBetween(xml, "<g:price>", "</g:price>").trim();
		String productSalePrice	= FilterUtil.getStringBetween(xml, "<g:sale_price>", "</g:sale_price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<g:link>", "</g:link>").trim();
		String productImageUrl 	= FilterUtil.getStringBetween(xml, "<g:image_link>", "</g:image_link>");
		String productCatId 	= FilterUtil.getStringBetween(xml, "<g:product_type>", "</g:product_type>");
		String productDesc 		= FilterUtil.getStringBetween(xml, "<g:description>", "</g:description>").trim();
		String productId 		= FilterUtil.getStringBetween(xml, "<g:id>", "</g:id>").trim();	
		String status	 		= FilterUtil.getStringBetween(xml, "<g:availability>", "</g:availability>").trim();
		
		String vendor_id	 	= FilterUtil.getStringBetween(xml, "<g:vendor_id>", "</g:vendor_id>").trim();	
		
		if(!status.equals("in stock")){
			return null;
		}
		
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;		
		}
		
		if(StringUtils.isBlank(vendor_id) || !vendor_id.equals(BaseConfig.CONF_FEED_CRAWLER_PARAM)) {
			return null;
		}
		
		if(StringUtils.isBlank(productSalePrice)) {
			productSalePrice = productPrice;
			productPrice = "";
		}
		
		productName += " -";
		
		if(StringUtils.isNotBlank(productBrand)) {
			productName += " " + productBrand;
		}
		
		if(StringUtils.isNotBlank(productColor)) {
			productName += " " + productColor;
		}
		
		if(StringUtils.isNotBlank(productId)) {
			productName += " (" + productId + ")";
		}
		
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		
//		if(productName.contains("[Global]") || productName.contains("【Global】") || (productName.startsWith("Global")) || (productName.contains("test product"))) {
//			return null;	
//		}
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productSalePrice)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (productDesc.length() != 0) {					
			pdb.setDescription(productDesc);
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		pdb.setUrlForUpdate(productUrl);
		
		productUrl = productUrl + "||https://jdcentral.onelink.me/SJGK?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_dp=openjdthai%3A%2F%2Fdeeplink%2Fcategory%2Fjump%2FM_sourceFrom%2Fsx_thai%2Fdes%2FproductDetail%2FskuId%2F******&af_reengagement_window=30d&clickid=(xxxxx)&af_ios_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html&af_android_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html";
		productUrl = productUrl.replace("******", productId);
		
		pdb.setUrl(productUrl);
		
		String[] catMap = getCategory(StringEscapeUtils.unescapeHtml4(productCatId).replace("&gt;",">"));
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}
		
		return pdb;
	}
}
