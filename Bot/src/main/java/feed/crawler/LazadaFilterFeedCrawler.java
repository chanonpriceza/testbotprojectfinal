package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class LazadaFilterFeedCrawler extends FeedManager{

	private static long CRAWL_LIMIT = 30000000;
	private static boolean SHOW_EXCEED_LOG = false;

    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public LazadaFilterFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_CRAWLER_PARAM) && NumberUtils.isCreatable(BaseConfig.CONF_FEED_CRAWLER_PARAM))
			CRAWL_LIMIT = NumberUtils.toLong(BaseConfig.CONF_FEED_CRAWLER_PARAM);
		
		logger.info("CRAWL_LIMIT : " + CRAWL_LIMIT);
		
		return new String[] {
				"http://27.254.82.243:8081/314-lazada/crawl_catFilterPrice3000File.csv.tar.gz",
				"http://27.254.82.243:8081/314-lazada/crawl_catFilterPrice5000File.csv.tar.gz",
				"http://27.254.82.243:8081/314-lazada/crawl_catLocalFile.csv.tar.gz",
				"http://27.254.82.243:8081/314-lazada/crawl_catSpecificFile.csv.tar.gz",
				"http://27.254.82.243:8081/314-lazada/crawl_lazMallFile.csv.tar.gz",
				"http://27.254.82.243:8081/314-lazada/crawl_skuFile.csv.tar.gz",
		};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data) {
		
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			if((ReportManager.getInstance().getReport().getCountOld() + ReportManager.getInstance().getReport().getAdd()) > CRAWL_LIMIT) {
				if(!SHOW_EXCEED_LOG) {
					SHOW_EXCEED_LOG = true;
					logger.info("WCE Exceed Log, ignore all after this.");
					logger.info("CountOld : " + ReportManager.getInstance().getReport().getCountOld() + ", CountAdd : " + ReportManager.getInstance().getReport().getAdd());
				}
				return null;
			}
		}
		
//		System.out.println(data.toString());
		
		// 0      product_name
		// 1      simple_sku
		// 2      old_sku_id
		// 3      deeplink
		// 4      brand
		// 5      sale_price
		// 6      discounted_price
		// 7      discount_percentage
		// 8      availability
		// 9      cross_border
		// 10     picture_url
		// 11     image_url_2
		// 12     image_url_3
		// 13     image_url_4
		// 14     image_url_5
		// 15     level_1
		// 16     level_2
		// 17     level_3
		// 18     level_4
		// 19     level_5
		// 20     is_slash_it
		// 21     is_hot_deals
		// 22     is_crazy_flash_sale
		// 23     seller_name
		
		
		String id 				= data.get(1);
		String pdName 			= data.get(0);
		String imageUrl 		= data.get(10);
		String price 			= data.get(5);
		String discountPrice 	= data.get(6);
		String cat1 			= data.get(15);
		String cat2 			= data.get(16);
		String cat3 			= data.get(17);
		String cat4 			= data.get(18);
		String cat5 			= data.get(19);
		String brand 			= data.get(4);
		String appLink 			= data.get(3);
		String shippingType 	= data.get(9);
		String availability 	= data.get(8);
//		String oldSku 			= StringUtils.defaultIfBlank(data.get(2), "");
//		String sellerName 		= StringUtils.defaultIfBlank(data.get(23), "");
		
		if(availability.equals("0")) {
			return null;
		}
		
		if(StringUtils.isBlank(appLink)
				|| !appLink.startsWith("http") 
				|| StringUtils.isBlank(pdName) 
				|| StringUtils.isBlank(id) 
				|| StringUtils.isBlank(cat1)
				|| (StringUtils.isBlank(discountPrice) && StringUtils.isBlank(price))){
			return null;
		}
		
		if(pdName.contains("�")){
			return null;
		}
		
		imageUrl = imageUrl.replace("&amp;", "&");
		
		String productCat = cat1;
		if(StringUtils.isNotBlank(cat2)) productCat += "|" + cat2;
		if(StringUtils.isNotBlank(cat3)) productCat += "|" + cat3;
		if(StringUtils.isNotBlank(cat4)) productCat += "|" + cat4;
		if(StringUtils.isNotBlank(cat5)) productCat += "|" + cat5;
		productCat = productCat.replace("\"", "");
		
		ProductDataBean pdb = new ProductDataBean();
		
		String[] catMap = getCategory(productCat);
		if (catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}
		
		String name = pdName + " (" + id + ")";
		pdb.setName(BotUtil.limitString(name, 200));
		pdb.setUrlForUpdate(id);
		pdb.setUrl(appLink);
		pdb.setRealProductId(id);
		
		if (!discountPrice.isEmpty()) {
			double curPrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(discountPrice));
			if(curPrice == 0){
				return null;
			}else{
				pdb.setPrice(curPrice);
			}
		}
		
		if (!price.isEmpty()) {
			double curBasePrice = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
			if(curBasePrice != 0 && curBasePrice > pdb.getPrice()){
				pdb.setBasePrice(curBasePrice);
			}
		}
		
		if (!imageUrl.isEmpty() && imageUrl.startsWith("http") && BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			pdb.setPictureUrl(imageUrl);
		}

		String description = shippingType.equals("1") ? "Shipping:Import" : "Shipping:Local";
		description += " " + name;
		if (StringUtils.isNotBlank(brand)) {
			description = description + " แบรนด์  " + brand;
		}

		if (StringUtils.isNotBlank(cat1)) {
			description = description + " ประเภท " + cat1;
		}
		if (StringUtils.isNotBlank(cat2)) {
			if (StringUtils.isNotBlank(cat1)) {
				description = description + " " + cat2;
			} else {
				description = description + " ประเภท  " + cat2;
			}
		}
		
		pdb.setDescription(description);
		
		if(pdb.getName().contains("หน้ากาก") || pdb.getName().contains("PREMIUM PRODUCT") ) {
			return null;
		}
		
		return pdb;
	}
	
}
