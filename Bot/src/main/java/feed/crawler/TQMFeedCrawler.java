package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;


public class TQMFeedCrawler extends FeedManager {
	
	public enum TYPE {
		GetCarBrand,GetCarModel,GetCarSubModel,GetInsure	
	}
	
	private static final Logger logger = LogManager.getRootLogger();
	
	private final static String[] YEAR_INSURE_OFFSET = new String[] {"1","2","20","3","30"};
	private final static String TQM_CAR_REQUEST_URL = "https://www.tqm.co.th/service/insure_service/service_car.php?wsdl";
	private final static String TQM_INSURE_REQUEST_URL = "https://www.tqm.co.th/service/insure_service/service_insure.php?wsdl";
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	private static JSONArray brandArray = new JSONArray();
	private boolean haveResult = false;
	private static int count = 0;
	private static int countBrand = 0;
	private static int totalBrand = 0;
	
	public TQMFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public String[] setFeedFile() {
		logger.info("MaxRuntime :"+BaseConfig.CONF_MAX_RUNTIME);
		try {
			JSONParser jsonParser = new JSONParser();
			String param = genJSONObject("Username,priceza","Password,Pi21_Z4@x");
			String body =  genParam(TYPE.GetCarBrand.toString(),param);
			String response = feedRequest(body);
			response = StringEscapeUtils.unescapeXml(response);
			JSONObject jObj = (JSONObject) jsonParser.parse(getResult(response));
			brandArray = (JSONArray)jObj.get("Result");
//			JSONArray brandArray2 = (JSONArray)jObj.get("Result");
//			for(Object b:brandArray2.subList(4,5)) {
//				brandArray.add(b);
//			}
		} catch (ParseException e) {
			logger.info(e);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		try {
			ExecutorService ex = Executors.newFixedThreadPool(7);
			for(Object brand:brandArray) {
				if(((JSONObject) brand).get("Description").equals("Aston Martin")) {
					continue;
				}
				if(Thread.currentThread().isInterrupted()) {
					return;
				}
				totalBrand = brandArray.size();
				JSONObject brandJSON = (JSONObject) brand;
				ex.execute(new tqmFeedLoader(brandJSON));
			}
			
			logger.info("Finish Load Feed !!!!!!!");
			
			ex.shutdown();
			if(ex.awaitTermination(10, TimeUnit.MINUTES)){
				logger.info("Done All.");
			}else{
				logger.info("Time out.");
				ex.shutdownNow();
			}
			if(haveResult)
				BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		}catch (InterruptedException e) {
			logger.info("Killed");
			return;
		}catch (Exception e) {
			logger.info(e);
		}
	}
	
	private void mockJSON(JSONObject obj) {
//		{
//			"Cc": "2.2",
//			"descriptionGetModel": "Brera",
//			"Vat": "168.84",
//			"CoverAcc": "50000",
//			"CoverLegal": "50000",
//			"ProductId": "14859",
//			"CoverFlooding": "N",
//			"Cover3RD": "300000",
//			"CompanyName": "ประกันภัยไทยวิวัฒน์",
//			"Duty": "10.00",
//			"VehicleKey": "ALFA13AB",
//			"NetAmount": "2580.84",
//			"CoverMed": "50000",
//			"CoverDeduct": "-",
//			"CarUsage": "110",
//			"descriptionGetSubModel": "Manual \/ 2.2 \/ (CBU) \/ 2dr \/ ราคา 4,690,000",
//			"CoverAccNum": "5",
//			"CoverAccPass": "50000",
//			"descriptionGetBrand": "Alfa Romeo",
//			"CarCheck": "N",
//			"GarageTotal": "1710",
//			"Cover3RDTime": "10000000",
//			"PromotionCodeSupplier": "TVI3110",
//			"Terrorism": "N",
//			"PromotionCode": "TVI 3110",
//			"Amount": "2402.00",
//			"MakeCode": "ALFA",
//			"ShowDeductNoPaid": "ND",
//			"CoverLostFire": "0",
//			"Cover": "0",
//			"Garage": "ซ่อมอู่",
//			"Year": "2006",
//			"CoverAccPassNum": "5",
//			"FamilyCode": "BRERA",
//			"Level": "3",
//			"CompleteOnline": "N",
//			"CompanyCode": "TVI",
//			"Cover3RDAsset": "1000000"
//		}

		String id 			= String.valueOf(obj.get("ProductId"));                                  					  // - รหัสสินค้า
		String insurer 		= String.valueOf(obj.get("CompanyName"));                                        			  // - บริษัทประกัน
		String type			= String.valueOf(obj.get("Level"));                                           				  // - ชั้นประกัน
		String title		= insurer+type;      
		//String vechicleKey  = String.valueOf(obj.get("VehicleKey"));           
		// - ชื่อสินค้า
		String brand		= String.valueOf(obj.get("descriptionGetBrand"));                                             // - ยี่ห้อรถ
		String model		= String.valueOf(obj.get("descriptionGetModel"));                                             // - รุ่นรถ
		
		String subModel		= String.valueOf(obj.get("descriptionGetSubModel"));                                          // - รุ่นย่อย
		subModel = FilterUtil.getStringBefore(subModel,"ราคา", model);
		subModel = subModel.replaceAll("/","");
		String year			= String.valueOf(obj.get("Year"));                                            				 // - ปีรถ
		String repair		= String.valueOf(obj.get("Garage"));                                   				         // - อู่ซ่อม


		String fire			= String.valueOf((Object)obj.get("CoverLostFire"));                       					 // - คุ้มครองไฟไหม้
		String theft		= String.valueOf((Object)obj.get("CoverLostFire"));                             			 // - คุ้มครองโจรกรรม
		String flood		= String.valueOf((Object)obj.get("CoverFlooding"));                     					 // - คุ้มครองน้ำท่วม
		String bi			= String.valueOf((Object)obj.get("Cover3RD"));           									 // - ชีวิตบุคคลภายนอก ต่อคน
		String bt			= String.valueOf((Object)obj.get("Cover3RDTime"));  										 // - ชีวิตบุคคลภายนอก ต่อครั้ง
		String tp			= String.valueOf((Object)obj.get("Cover3RDAsset"));         								 // - ทรัพย์สินบุคคลภายนอก
		String pa			= String.valueOf((Object)obj.get("CoverAcc"));        										 // - อุบัติเหตุส่วนบุคคล
		String bb			= String.valueOf((Object)obj.get("CoverLegal"));                   					         // - ประกันตัวผู้ขับขี่
		String me			= String.valueOf((Object)obj.get("CoverAccPass"));                 						 	 // - ค่ารักษาพยาบาล 
		String cc			= String.valueOf((Object)obj.get("Cc"));                 						 	 		 // - Cc
		
		String premium		= String.valueOf((Object)obj.get("NetAmount"));                       					     // - เบี้ยประกัน
		String od			= String.valueOf((Object)obj.get("Cover"));                      							 // - ทุนประกัน
		String oddd			= String.valueOf((Object)obj.get("CoverDeduct"));

		if(type.contains("0")) {
			type = type.replace("0","+");
		}
		
		cc = cc+"cc";
		
		String uniqueText = "";
		uniqueText += id;
		uniqueText += type;
		uniqueText += insurer;
		uniqueText += brand;
		uniqueText += model;
		uniqueText += subModel;
		uniqueText += year;
		uniqueText += title;
		uniqueText += repair;
		uniqueText += od;
		uniqueText = toMD5(uniqueText);
		
//		ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
		String productName = "";
		productName += "ประกันชั้น " + type;
		productName += " - " + insurer.replace("-", " ");
		productName += " - " + brand.replace("-", " ");
		productName += " - " + model.replace("-", " ")+" "+cc;
		productName += " - " + ((StringUtils.isNotBlank(subModel))? subModel.replace("-", " ") : "ทุกรุ่น");
		productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
		productName += " (" + uniqueText.substring(0,10) + ")";
		productName = productName.replace(",", "");
		productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
		productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
		productName = productName.replaceAll("\\s+", " ");                                                  
		productName = productName.replace("( ", "(");                                                       
		productName = productName.replace(" )", ")");                                                       
		productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
		productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");   		
		

		String productDesc = "";
		productDesc += insurer+" ประกันชั้น " + type;;
		productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(repair))? repair : " ไม่ระบุอู่/ห้าง");
		productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
		productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : (oddd.equals("-")) ? "ไม่ระบุ":oddd ));
		productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
		productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
		productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0") && !flood.equals("N"))? flood : "ไม่ระบุ" ); 
		productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
		productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
		productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
		productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
		productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );         
		productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
		productDesc = productDesc.replaceAll("\\s+", " ");                                               
		productDesc = productDesc.replace("( ", "(");                                                    
		productDesc = productDesc.replace(" )", ")");                                                    
		productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
		productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
		productDesc = productDesc.replace("ซ่อมซ่อม", "ซ่อม");                         
		productDesc = productDesc.replace(",", "");
		
		
		
		String productPrice =  formatPrice.format(Double.parseDouble(FilterUtil.removeCharNotPrice(premium)));
		
		String productUrl = "https://www.tqm.co.th/";
		String productUrlForUpdate = uniqueText;
		String productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-55651.jpg";
		if(insurer.equalsIgnoreCase("สินทรัพย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5567.jpg"; 
		if(insurer.equalsIgnoreCase("เอเชียประกันภัย 1950"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5570.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ ซี.พี. ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.equalsIgnoreCase("กรุงเทพประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5558.jpg"; 
		if(insurer.equalsIgnoreCase("ทิพยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5565.jpg"; 
		if(insurer.equalsIgnoreCase("กรุงไทยพานิชประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5571.jpg"; 
		if(insurer.equalsIgnoreCase("เคเอสเคประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5559.jpg"; 
		if(insurer.equalsIgnoreCase("แอลเอ็มจีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5572.jpg"; 
		if(insurer.equalsIgnoreCase("เมืองไทยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5551.jpg"; 
		if(insurer.equalsIgnoreCase("นวกิจประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5569.jpg"; 
		if(insurer.equalsIgnoreCase("อาคเนย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5552.jpg"; 
		if(insurer.equalsIgnoreCase("สินมั่นคงประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5562.jpg"; 
		if(insurer.equalsIgnoreCase("ประกันคุ้มภัย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5554.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยประกันภัย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5575.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยไพบูลย์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9559.jpg"; 
		if(insurer.equalsIgnoreCase("ประกันภัยไทยวิวัฒน์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5574.jpg"; 
		if(insurer.equalsIgnoreCase("วิริยะประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ โกลบอล แอสซิสแทนซ์"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9605.jpg"; 
		if(insurer.equalsIgnoreCase("ไอโออิ กรุงเทพ ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5568.jpg"; 
		if(insurer.equalsIgnoreCase("แอกซ่าประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5557.jpg"; 
		if(insurer.equalsIgnoreCase("บูพา"))						productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9606.jpg"; 
		if(insurer.equalsIgnoreCase("จรัญประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9558.jpg"; 
		if(insurer.equalsIgnoreCase("ซิกน่าประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9604.jpg"; 
		if(insurer.equalsIgnoreCase("เจ้าพระยาประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5560.jpg"; 
		if(insurer.equalsIgnoreCase("เทเวศประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5563.jpg"; 
		if(insurer.equalsIgnoreCase("เอราวัณประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9569.jpg"; 
		if(insurer.equalsIgnoreCase("อินทรประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9572.jpg"; 
		if(insurer.equalsIgnoreCase("กมลประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9576.jpg"; 
		if(insurer.equalsIgnoreCase("เอ็มเอสไอจีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5564.jpg"; 
		if(insurer.equalsIgnoreCase("นำสินประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5573.jpg"; 
		if(insurer.equalsIgnoreCase("เอไอจี ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9435.jpg"; 
		if(insurer.equalsIgnoreCase("ฟีนิกซ์ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9573.jpg"; 
		if(insurer.equalsIgnoreCase("รู้ใจ รับประกันโดย KPI กรุงไทยพานิช"))	productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9419.jpg"; 
		if(insurer.equalsIgnoreCase("ศรีอยุธยา เจนเนอรัล ประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9565.jpg"; 
		if(insurer.equalsIgnoreCase("สามัคคีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5553.jpg"; 
		if(insurer.equalsIgnoreCase("ธนชาตประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5556.jpg"; 
		if(insurer.equalsIgnoreCase("โตเกียวมารีนศรีเมืองประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9601.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยเศรษฐกิจประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5566.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยพัฒนาประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9556.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยศรีประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9561.jpg"; 
		if(insurer.equalsIgnoreCase("มิตรแท้ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9566.jpg"; 
		if(insurer.equalsIgnoreCase("พุทธธรรมประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9564.jpg"; 
		if(insurer.equalsIgnoreCase("บางกอกสหประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9568.jpg"; 
		if(insurer.equalsIgnoreCase("สหนิรภัยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9580.jpg"; 
		if(insurer.equalsIgnoreCase("สหมงคลประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9567.jpg"; 
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		pdb.setPictureUrl(productImage);
		pdb.setDescription(productDesc);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrlForUpdate);
		pdb.setCategoryId(250101);
		pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;
		
		if(count%10000==0) {
			logger.info("Process progess at :"+count);
		}
		
		String mockData = mockResult(pdb);
		if(StringUtils.isNotBlank(mockData)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(mockData);
				count++;
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

	
	private static String feedRequest(String body) {
		return feedRequest(body,TQM_CAR_REQUEST_URL);
	}
	
	private static String feedRequest(String body,String url) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;
	 			
	 	
	 	try {

	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setRequestProperty("Content-Type","text/xml");
	 		conn.setDoOutput(true);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(body.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					logger.error(e);
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}
	
	private String genParam(String genType,String param,String genTypeUrl) {
	
		String  mainString =  "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + 
			"    <Body>\r\n" + 
			"        <"+genType+" xmlns=\"http://www.tqm.co.th/soap/"+genTypeUrl+"\">\r\n" + 
			"           <parameter>"+param+"</parameter>\r\n" + 
			"        </"+genType+">\r\n" + 
			"    </Body>\r\n" + 
			"</Envelope>";
		return mainString;
		
	}
	
	private String genParam(String genType,String param) {
		return genParam(genType,param,"GetCarDetail");
	}
	
	@SuppressWarnings("unchecked")
	private String genJSONObject(String... params) {
		JSONObject jsonObj = new JSONObject();
		for(String p:params) {
			String[] param = p.split(",");
			if(param==null||param.length!=2)
				continue;
			String key = param[0];
			String value=  param[1];
			jsonObj.put(key,value);
		}
		return jsonObj.toJSONString();
	}
	
	private String getResult(String result) {
		String parseResult = FilterUtil.getStringBetween(result,"<return xsi:type=\"xsd:string\">","</return>");
		return parseResult;
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	private class tqmFeedLoader implements Runnable {
		
		private JSONObject brand;
		public tqmFeedLoader(JSONObject jsonObject) {
			brand = jsonObject;
		}
		
		JSONParser jsonParser = new JSONParser();

		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			try {
				countBrand++;
				logger.info("Current Brand: " + ((JSONObject) brand).get("Description") + " Count Brand:" + countBrand
						+ " totalBrand: " + totalBrand);
				String param = "";
				String body = "";
				String response = "";
				JSONObject jObj = null;

				JSONObject brandJSON = (JSONObject) brand;
				String makeCode = String.valueOf(brandJSON.get("MakeCode"));
				String descriptionGetBrand = String.valueOf(brandJSON.get("Description"));
				param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", "MakeCode," + makeCode);
				body = genParam(TYPE.GetCarModel.toString(), param);
				logger.info(Thread.currentThread().getName() + " Request GetCarModel " + makeCode);
				response = feedRequest(body);
				response = StringEscapeUtils.unescapeXml(response);
				JSONArray modelArray = null;
				jObj = (JSONObject) jsonParser.parse(getResult(response));
				modelArray = (JSONArray) jObj.get("Result");

				for (Object model : modelArray) {

					JSONObject modelJSON = (JSONObject) model;
					String descriptionGetModel = String.valueOf(modelJSON.get("Description"));
					String LatestYear = String.valueOf(modelJSON.get("LatestYear"));
					String StartYear = String.valueOf(modelJSON.get("StartYear"));
					String familyCode = String.valueOf(modelJSON.get("FamilyCode"));
					String carUsage = String.valueOf(modelJSON.get("CarUsage"));
					int lastYear = BotUtil.stringToInt(LatestYear, 2019);
					int startYear = BotUtil.stringToInt(StartYear, 2003);

					for (int y = startYear; y <= lastYear; y++) {
						param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", "MakeCode," + makeCode,
								"FamilyCode," + familyCode, "Year," + y);
						body = genParam(TYPE.GetCarSubModel.toString(), param);
						logger.info(
								Thread.currentThread().getName() + " Request GetCarSubModel " + familyCode + " " + y);
						response = feedRequest(body);
						response = StringEscapeUtils.unescapeXml(response);
						jObj = (JSONObject) jsonParser.parse(getResult(response));
						if ("No Result".equals(jObj.get("Result"))) {
							continue;
						}

						JSONArray cars = (JSONArray) jObj.get("Result");

						for (Object c : cars) {

							JSONObject car = (JSONObject) c;

							String descriptionGetSubModel = String.valueOf(car.get("Description"));
							String cc = String.valueOf(car.get("Cc"));
							String year = String.valueOf(car.get("Year"));

							String vehicleKey = String.valueOf(car.get("VehicleKey"));
							for (String insureLevel : YEAR_INSURE_OFFSET) {
								param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", "MakeCode," + makeCode,
										"FamilyCode," + familyCode, "Year," + year, "InsureLevel," + insureLevel,
										"Cc," + cc, "VehicleKey," + vehicleKey, "CarUsage," + carUsage, "CoverMin," + 0,
										"CoverMax," + 9999999);
								logger.info(Thread.currentThread().getName() + " Request insure "
										+ descriptionGetSubModel + " " + year + " " + insureLevel);
								body = genParam(TYPE.GetInsure.toString(), param, "GetInsureDetail");
								response = feedRequest(body, TQM_INSURE_REQUEST_URL);
								response = StringEscapeUtils.unescapeXml(response);
//								if(Thread.currentThread().getName().equals("")) {
//								logger.info(Thread.currentThread().getName() + " Finish "
//										+ descriptionGetSubModel + " " + year + " " + insureLevel);
//								}
								jObj = (JSONObject) jsonParser.parse(getResult(response));

								if (jObj == null || "No Result".equals(jObj)) {

									continue;
								}

								if (jObj.get("Result") == null || jObj.get("Result").equals("No Result")) {
									continue;
								}

								JSONArray jsonArray = (JSONArray) jObj.get("Result");

								for (Object isure : jsonArray) {
									JSONObject insureJSON = (JSONObject) isure;
									if (insureJSON.get("Level") == null
											|| String.valueOf(insureJSON.get("Level")).equals("9"))
										continue;
									insureJSON.put("MakeCode", makeCode);
									insureJSON.put("FamilyCode", familyCode);
									insureJSON.put("Year", year);
									insureJSON.put("Cc", cc);
									insureJSON.put("VehicleKey", vehicleKey);
									insureJSON.put("CarUsage", carUsage);
									insureJSON.put("descriptionGetBrand", descriptionGetBrand);
									insureJSON.put("descriptionGetModel", descriptionGetModel);
									insureJSON.put("descriptionGetSubModel", descriptionGetSubModel);
									mockJSON(insureJSON);
								}

							}
						}
					}
				}

			} catch (Exception e) {
				logger.info(e);
			}

		}
	}
}

