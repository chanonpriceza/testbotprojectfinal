package feed.crawler;

import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LovelyWholesaleFeedCrawler extends FeedManager {

	public LovelyWholesaleFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"http://erp.lovelywholesale.com/lovelyErpAdm2015/google_feed_xml.php?token=siaxkIJDLSsjd2324xsasdrmxjsa138945012&sort=0&country=UnitedStates&is_new=0"};
	}
	
	
	@Override
	public ProductDataBean parse(String data) {
		Document doc = Jsoup.parse(data,"",Parser.xmlParser());
		String name = doc.select("title").html();
		String price = doc.select("g|sale_price").html();
		String basePrice = doc.select("g|price").html();
		String id = doc.select("g|id").html();
		String desc = doc.select("description").html();
		String color = doc.select("g|color").html();
		String size = doc.select("g|size").html();
		String url  = doc.select("link").html();
		String image = doc.select("g|image_link").html();
		String cat = doc.select("g|product_type").html();
		cat = StringEscapeUtils.unescapeHtml4(cat);
		desc = FilterUtil.toPlainTextString(desc);

		url = StringEscapeUtils.unescapeHtml4(url);
		cat = StringEscapeUtils.unescapeHtml4(cat);
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		
		if("null".equals(desc)) {
			desc  = null;
		}
		String tmp = "";
		
		if(StringUtils.isNotBlank(id)) 
			tmp = " ("+id+")";
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		
		ProductDataBean pdb = new ProductDataBean();

		pdb.setName(name+" "+color+" "+size+tmp);
		pdb.setPrice(p);
		pdb.setDescription(desc);
		pdb.setBasePrice(bp);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		pdb.setUrlForUpdate(id);
		pdb.setRealProductId(id);
		String[] catMap = getCategory(cat);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0);
		}
		return pdb;
	}

}
