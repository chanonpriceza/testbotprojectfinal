package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class DigitalDCFeedCrawler extends FeedManager{

	public DigitalDCFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://www.thedigitaldc.com/feedproduct.aspx"
			};
	}

	public ProductDataBean parse(String xml){
		
		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice = FilterUtil.getStringBetween(xml, "<base_price>", "</base_price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId = FilterUtil.getStringBetween(xml, "<category_name>", "</category_name>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty() || realProductId.isEmpty()) {
			return null;		
		}
		

		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName + " (" + realProductId + ")");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals(productPrice)){
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		}
		productUrl = productUrl.replace("&amp;", "&");
		productUrl = StringEscapeUtils.unescapeHtml4(productUrl);
		pdb.setUrl(productUrl);
		if (!productPictureUrl.isEmpty()) {	
			productPictureUrl = productPictureUrl.replace("&amp;", "&");
			productPictureUrl = StringEscapeUtils.unescapeHtml4(productPictureUrl);
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
		}
		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
		productDesc = FilterUtil.toPlainTextString(productDesc);
		if(!productDesc.isEmpty()){
			pdb.setDescription(productDesc);
		}
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		}

		return pdb;
		
	}
	
}
