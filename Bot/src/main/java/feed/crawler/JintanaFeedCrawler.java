package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class JintanaFeedCrawler extends FeedManager{
	private static final Logger logger = LogManager.getRootLogger();
	public JintanaFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public String[] setFeedFile() {
		return new String[] {
				"http://www.jintana.com/feed.php"
		};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String[] result = HTTPUtil.httpRequestWithStatus(feed, "UTF-8", true);
			if(result != null && result.length == 2) {
				if(StringUtils.isNotBlank(result[0])) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + fileName),  true));){
						pwr.println(result[0]);
					}catch(Exception e) {
						logger.error(e);
					}
				}
			} else {
				logger.info("Feed | Cannot Load | " + feed);
			}
		}
	}


	public ProductDataBean parse(String xml) {

		String productName 		= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice 	= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDescription 		= FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId 	= FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId 	= FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
	
		
		if(productUrl.trim().isEmpty() || productName.trim().isEmpty() || productPrice.trim().isEmpty()) {
			return null;		
		}
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productUrl = productUrl.replace("&amp;", "&");
		productPictureUrl = productPictureUrl.replace("&amp;", "&");				
		

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));

		pdb.setUrl(productUrl);			
		pdb.setUrlForUpdate(productUrl);
		pdb.setPictureUrl(productPictureUrl);
		

		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
			int pIdLength = realProductId.length();
			if(pIdLength > 5 || pIdLength == 4){
				pdb.setUpc(realProductId);
			}
		}
		
		productCatId = productCatId.replaceFirst("^0+(?!$)", "");
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}
		
		pdb.setDescription(productDescription);
		
		return pdb;
		
	}
}
