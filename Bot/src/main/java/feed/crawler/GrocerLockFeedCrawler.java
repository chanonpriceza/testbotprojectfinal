package feed.crawler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class GrocerLockFeedCrawler extends FeedManager{

	public GrocerLockFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	} 

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.grocerlock.com/media/upload/price/product.xml"
		};
//		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			
			try{
				URL u = new URL(feed);
				HttpURLConnection conn = (HttpURLConnection) u.openConnection();
				conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36");
				InputStream in = conn.getInputStream();
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e,e);
				processLog.addException(e.toString()+"-"+this.getClass().getSimpleName(),e);
			}
		}
	}
	
	public ProductDataBean parse(String xml) {
		String productName 		= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productPrice 	= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl 		= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productDescription 		= FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();
		String realProductId 	= FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();
		String productCatId 	= FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>").trim();
	
		if(productName.contains("(เลิก)")||productName.contains("เลิกผลิต"))
			return null;
		
		if(productUrl.trim().isEmpty() || productName.trim().isEmpty() || productPrice.trim().isEmpty()) {
			return null;		
		}
		
		productName = StringEscapeUtils.unescapeHtml4(productName);
		productUrl = productUrl.replace("&amp;", "&");
		productPictureUrl = productPictureUrl.replace("&amp;", "&");				
		

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		if (!productUrl.isEmpty()) {
			if (!productUrl.startsWith("https://")) {
				productUrl = "https://www."+productUrl;
			}
			pdb.setUrl(productUrl);			
			pdb.setUrlForUpdate(productUrl);
		}
		
		if (!productPictureUrl.isEmpty()) {	
			if (!productPictureUrl.startsWith("https://")) {
				productPictureUrl = "https://www."+productPictureUrl;
			}
			pdb.setPictureUrl(productPictureUrl);
		}

		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
			int pIdLength = realProductId.length();
			if(pIdLength > 5 || pIdLength == 4){
				pdb.setUpc(realProductId);
			}
		}
		
		productCatId = productCatId.replaceFirst("^0+(?!$)", "");
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}
		
		pdb.setDescription(productDescription);
		
		return pdb;
		
	}
	
}
