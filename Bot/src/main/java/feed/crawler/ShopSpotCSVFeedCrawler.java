package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShopSpotCSVFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	public ShopSpotCSVFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://s3-ap-southeast-1.amazonaws.com/shopspot-product-feed/priceza_feed.csv"
		};
	}
	
	public ProductDataBean parse(CSVRecord data){
		try{
			
			String id = data.get(0);
			String name = data.get(1);
			String price = data.get(2);
			String desc = data.get(3);
			String image = data.get(6);
			String url  = data.get(7);
		
			String cat1 = data.get(4);
			String cat2 = data.get(5);
			String productCat = cat1 + "|" + cat2;
			if( StringUtils.isBlank(name) || StringUtils.isBlank(cat1) || StringUtils.isBlank(cat2) || StringUtils.isBlank(url) 
					|| StringUtils.isBlank(price)  || StringUtils.isBlank(id)){
				return null;
			}
			ProductDataBean pdb = new ProductDataBean();

	
			pdb.setName(removeNonUnicodeBMP(name) + " (" + id + ")");	
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			pdb.setUrl("https://"+url);
			pdb.setRealProductId(id);
			
			if(!image.isEmpty()){
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(desc)){
				desc = StringEscapeUtils.unescapeHtml4(desc);
				pdb.setDescription(removeNonUnicodeBMP(desc));
			}
			
			String[] catMap = getCategory(productCat);
			if(catMap != null) {			
				pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
				pdb.setKeyword(catMap[1]);			
			}else{
				pdb.setCategoryId(0);
			}

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
	
	public static String removeNonUnicodeBMP(String str) {    
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();
		
		while(!Thread.currentThread().isInterrupted() && offset < length) {
		   int codepoint = str.codePointAt(offset);
		   
		   if(codepoint < 65536) {
			   bmpString.append(str.charAt(offset));
		   }
		   
		   offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
}

