package feed.crawler;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class HomeProFeedCrawler extends FeedManager{

	public HomeProFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		String date = "";
		Calendar c = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
		date = format1.format(c.getTime());
		System.out.println(date);
		return new String[] {"https://static.homepro.co.th/feed/product_th.csv?r="+date};
	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {


		String name 		=data.get("title"); 
		String id 			=data.get("id"); 
		String url			=data.get("link"); 
		String image		=data.get("image_link"); 
		String cat 			=data.get("product_type"); 
		String desc 		=data.get("description"); 
		String basePrice	=data.get("price"); 
		String price 		=data.get("sale_price");
		
		double temp_price = 0;
		String id_temp = (StringUtils.isNotBlank(id))?" ("+id+")":"";
		
		double price_d = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double base_d = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+id_temp);
		pdb.setRealProductId(id);
		pdb.setUrl(url);
		pdb.setPrice(price_d);
		pdb.setDescription(desc);
		pdb.setBasePrice(base_d);
		pdb.setPictureUrl(image);
		pdb.setUrlForUpdate(id);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}

		return pdb;
	}
	
}
