package feed.crawler;

import java.net.URLDecoder;

import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class LightInTheBoxFeedCrawler extends FeedManager{

	public LightInTheBoxFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.tagserve.asia/productCatalogServlet?MID=134&PID=199&AID=275&SID=400&&&&&COLUMNS=C100,C101,C102,C105,C106,C108,C109,C110,C103,C104,C111,C112,C113,C114,C115,C152,C117,C118,C119,C120,C153,C121,C123,C124,C125,C126,C122,C127,C128,C129,C130,C131,C132,C133,C134,C135,C136,C137,C138,C139,C140,C141,C142,C143,C144,C145,C146,C147,C148,C149,C150,C151,C154,C155,C156,C157,C158,C159,C160,C161&FORMAT=xml"
		};
	}

	public ProductDataBean parse(String xml) {
		
		String productName = FilterUtil.getStringBetween(xml, "<product_name>", "</product_name>");		
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>");
		String productUrl = FilterUtil.getStringBetween(xml, "<landing_url>", "</landing_url>");
		String productImageUrl = FilterUtil.getStringBetween(xml, "<picture_thumbnail_url>", "</picture_thumbnail_url>");
		String productCatId = FilterUtil.getStringBetween(xml, "<product_category_id>", "</product_category_id>");
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String realProductId = FilterUtil.getStringBetween(xml, "<tag_product_id>", "</tag_product_id>").trim();
		
		productUrl = productUrl.replace(" ", "");
		productUrl = productUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace(" ","");
		productImageUrl = productImageUrl.replace("&amp;", "&");
		
		if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 ) {
			return null;			
		}
		
		ProductDataBean pdb = new ProductDataBean();	
		
		productName = StringEscapeUtils.unescapeHtml4(productName).trim();
		if(realProductId.length() > 0){
			productName += " (" + realProductId + ")";
		}
		pdb.setName(productName);
		
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
				
		if (productDesc.length() != 0) {
			pdb.setDescription(productDesc);
		}
		if (productImageUrl.length() != 0) {
			productImageUrl = FilterUtil.getStringAfter(productImageUrl, "IMGURL=", "");
			productImageUrl = FilterUtil.getStringBefore(productImageUrl, "</picture_thumbnail_url>", productImageUrl);
			try{                                                           
				productImageUrl = URLDecoder.decode(productImageUrl, "UTF-8");
				productImageUrl = productImageUrl.replace(" ", "");						
				pdb.setPictureUrl(productImageUrl);
			}catch(Exception e){}			
		}
		if (productUrl.length() != 0) {
			productUrl = productUrl.replace(" ", "");
			productUrl = FilterUtil.getStringBefore(productUrl, "</landing_url>", productUrl);
			productUrl = FilterUtil.getStringBefore(productUrl, "?p.utm", productUrl);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(FilterUtil.getStringAfter(productUrl, "TARGETURL=", productUrl));
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}
	
}
