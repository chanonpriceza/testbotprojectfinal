package feed.crawler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class MotherHoodFeedCrawler extends FeedManager{

	public MotherHoodFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://www.motherhood.co.th/job/buildFeed_All.php"};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try {
				URL u = new URL(feed);
				HttpURLConnection http = (HttpURLConnection)u.openConnection();
				http.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
				http.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
				Files.copy(http.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		}
	
	@Override
	public ProductDataBean parse(String data) {
		String productName = FilterUtil.getStringBetween(data, "<title>", "</title>").trim();		
		String productPrice = FilterUtil.getStringBetween(data, "<discounted_Price>", "</discounted_Price>");
		String productBasePrice = FilterUtil.getStringBetween(data, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(data, "<URL>", "</URL>").trim();
		productUrl = productUrl.replace("]]>","").replace("<![CDATA[","");
		String sku = FilterUtil.getStringAfterLastIndex(productUrl,"/", productUrl);
		sku = FilterUtil.getStringBefore(sku,"-", "");
		String productImageUrl = FilterUtil.getStringBetween(data, "<Images>", "</Images>");
		String productDesc = FilterUtil.getStringBetween(data, "<description>", "</description>");
		productDesc = productDesc.replace("]]>","").replace("<![CDATA[","");
		String cat = FilterUtil.getStringBetween(data, "<category>", "</category>");
		cat = StringEscapeUtils.unescapeHtml4(cat);
		cat = FilterUtil.removeSpace(cat);
		if(StringUtils.isBlank(sku))
			return null;
		
		String tmp = " ("+sku+")";

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName+tmp);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		pdb.setBasePrice(FilterUtil.convertPriceStr(productBasePrice));
		pdb.setUrl(productUrl);
		pdb.setRealProductId(sku);
		pdb.setUrlForUpdate(sku);
		pdb.setPictureUrl(productImageUrl);
		pdb.setDescription(productDesc);
		
		String[] mapping = getCategory(cat);
		
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		return pdb;
	}

}
