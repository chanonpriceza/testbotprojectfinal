package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class CharlesAndKeithFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public CharlesAndKeithFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.tagserve.asia/productCatalogServlet?MID=150&PID=230&AID=275&SID=400&&&&&COLUMNS=C100,C101,C102,C105,C106,C108,C109,C110,C103,C104,C111,C112,C113,C114,C115,C152,C117,C118,C119,C120,C153,C121,C123,C124,C125,C126,C122,C127,C128,C129,C130,C131,C132,C133,C134,C135,C136,C137,C138,C139,C140,C141,C142,C143,C144,C145,C146,C147,C148,C149,C150,C151,C154,C155,C156,C157,C158,C159,C160,C161&FORMAT=csv&DELIMITER=comma"
		};
	}

	public ProductDataBean parse(CSVRecord data) {
		try{
			
			String product_id = data.get(0);
			String product_code = data.get(1);
			String name = data.get(2);
			String description = data.get(3);
			String image = data.get(4);			
			String url = data.get(5);
			String price = data.get(11);
			String basePrice  = data.get(7);
			String cat  = data.get(19);
			
			if( StringUtils.isBlank(name) || StringUtils.isBlank(product_code) || StringUtils.isBlank(url) || StringUtils.isBlank(basePrice) || !url.startsWith("http")){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			String prdName = "CHARLES & KEITH " + name + " (" + product_code + ") " ;
			pdb.setName(prdName);
			if(StringUtils.isBlank(price)){
				price = basePrice;
				basePrice = "";
			}
			price = FilterUtil.removeCharNotPrice(price);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			if(!basePrice.isEmpty()){
				basePrice = FilterUtil.removeCharNotPrice(basePrice);
				pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
			}
			
			url = url.replace("TARGETURL=http:", "TARGETURL=https:");
			
			pdb.setUrl(url);
			pdb.setUrlForUpdate(product_id);
			
			if(!image.isEmpty()){
				pdb.setPictureUrl(image);
			}
			
			if(StringUtils.isNotBlank(description)){
				pdb.setDescription(description);
			}
			String[] mapping = getCategory(cat);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);
			} 
			return pdb;
		}catch(Exception e){
			logger.error(e);
		}
		return null;
	}
		
}
