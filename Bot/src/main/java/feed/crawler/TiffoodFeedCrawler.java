package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import utils.HTTPUtil;

public class TiffoodFeedCrawler extends FeedManager {

	public TiffoodFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Document doc = Jsoup.parse(HTTPUtil.httpRequestWithStatus("https://www.tiffood.com/17160258/products","UTF-8",true)[0]);
		Elements e = doc.select("div.ready-template-section-editable-button-image");
		for(Element ex:e) {
			parseElement(ex.parent());
		}
		BaseConfig.FEED_FILE = new String[] { BaseConfig.FEED_STORE_PATH + "/mockFeed.xml" };
	}
	
	private void parseElement(Element e) {
		String name = e.select("h1").text();
		String pic = e.select("img").attr("src");
		String url = e.select("a").attr("href");
		String price = e.select("span.ready-template-section-editable-text").text();
		name = FilterUtil.toPlainTextString(name);
		price = FilterUtil.removeCharNotPrice(price);
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name);
		pdb.setUrl(url);
		pdb.setPictureUrl("https://www.tiffood.com"+pic);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setCategoryId((131006));
		pdb.setKeyword("Coconut Oil น้ำมันมะพร้าว โคโค่วัน");
		mockResult(pdb);
	}
	
	private String mockResult(ProductDataBean pdb) {

		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();

		desc = FilterUtil.toPlainTextString(desc);

		if (StringUtils.isBlank(url))
			return null;
		if (StringUtils.isBlank(name))
			return null;
		if (price == 0)
			return null;
		if (expire)
			return null;

		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if (StringUtils.isNotBlank(name))
			result.append("<name>" + name + "</name>");
		if (StringUtils.isNotBlank(desc))
			result.append("<desc>" + desc + "</desc>");
		if (price != 0)
			result.append("<price>" + price + "</price>");
		if (basePrice != 0)
			result.append("<basePrice>" + basePrice + "</basePrice>");
		if (StringUtils.isNotBlank(image))
			result.append("<pictureUrl>" + image + "</pictureUrl>");
		if (StringUtils.isNotBlank(url))
			result.append("<url>" + url + "</url>");
		if (StringUtils.isNotBlank(id))
			result.append("<realProductId>" + id + "</realProductId>");
		if (cat != 0)
			result.append("<categoryId>" + cat + "</categoryId>");
		if (StringUtils.isNotBlank(keyword))
			result.append("<keyword>" + keyword + "</keyword>");
		if (cat != 0)
			result.append("<cat>" + cat + "</cat>");
		result.append("</product>");

		if (StringUtils.isNotBlank(result)) {
			if (StringUtils.isNotBlank(result)) {
				try (PrintWriter pwr = new PrintWriter(
						new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"), true));) {
					pwr.println(result);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();

	}
	
	public ProductDataBean parse(String xml) {

		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc = FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate = FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice = FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId = FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword = FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId = FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc = FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if (productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;
		}

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);

		if (StringUtils.isNotBlank(productDesc))
			pdb.setDescription(productDesc);

		if (StringUtils.isNotBlank(productPictureUrl))
			pdb.setPictureUrl(productPictureUrl);

		pdb.setUrl(productUrl);

		if (StringUtils.isNotBlank(productUrlForUpdate))
			pdb.setUrlForUpdate(productUrlForUpdate);
		else
			pdb.setUrlForUpdate(productUrl);

		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));

		if (StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));

		if (StringUtils.isNotBlank(productCatId))
			pdb.setCategoryId(Integer.parseInt(productCatId));

		if (StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);

		if (StringUtils.isNotBlank(productRealProductId))
			pdb.setRealProductId(productRealProductId);

		if (StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);

		return pdb;
	}

}
