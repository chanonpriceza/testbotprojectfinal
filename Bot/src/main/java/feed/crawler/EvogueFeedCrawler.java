package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class EvogueFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private static final String REQUEST_URL = "https://www.evogue.co.th:8443/api/TH/export/excel";
	private boolean haveResult = false;

	public EvogueFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		String[] response = httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
		if(response == null || response.length != 2) return;
		
		String data = response[0];
		if(StringUtils.isBlank(data)) return;
		
		List<String> allObj = FilterUtil.getAllStringBetween(data, "{", "}");
		if(allObj != null && allObj.size() > 0) {
			for (String obj : allObj) {
				parseJson("{"+obj+"}");
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.setConnectTimeout(300000);
			conn.setReadTimeout(300000);		
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf("")};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	
	public void parseJson(String data) {
		try{
			
			JSONObject jsonObj = (JSONObject) new JSONParser().parse(data);

			String productName = (String) jsonObj.get("productName");
			String productPrice = jsonObj.get("retailPrice")+"";
			String basePrice = jsonObj.get("stickerPrice")+"";
			String sku = (String) jsonObj.get("sku");
			String path = (String) ((JSONArray) jsonObj.get("path")).get(0);
			String productDesc = (String) jsonObj.get("description");
			boolean productExpire = (Boolean) jsonObj.get("status");
			JSONArray cateID = (JSONArray) jsonObj.get("categoriesID");
			String url = ((String) jsonObj.get("productName")).replace("#", "");
			
			if (StringUtils.isBlank(productName) || StringUtils.isBlank(productPrice)) {
				return;
			}
			
			String pictureUrl = "https://www.evogue.co.th:8443/product/img/"+path;
			String productUrl = "https://www.evogue.co.th/product//?title="+url+"&sku="+sku;
			
			DecimalFormat df = new DecimalFormat(".##");  

			ProductDataBean pdb = new ProductDataBean();
			
			if(productExpire) {
				pdb.setExpire(false);
			}else {
				pdb.setExpire(true);								
			}
			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(df.format(Double.valueOf(FilterUtil.removeCharNotPrice(productPrice)))));
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);

			if (!productPrice.equals(basePrice)) {
				pdb.setBasePrice(FilterUtil.convertPriceStr(df.format(Double.valueOf(FilterUtil.removeCharNotPrice(basePrice)))));
			}

			if (StringUtils.isNotBlank(pictureUrl)) {
				pdb.setPictureUrl(pictureUrl);
			}

			if (StringUtils.isNotBlank(productDesc)) {
				pdb.setDescription(productDesc);
			}
			
			for (int count=2; 0<=count; count--) {
				if (cateID.size()<=count) {
					continue;
				}
				String cateName = (String) cateID.get(count);
				String[] mapping = getCategory(cateName);
				
				if (mapping != null) {
					pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
					pdb.setKeyword(mapping[1]);
				} else if (count==0) {
					pdb.setCategoryId(130608);
				}
			}
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
			
		} catch(Exception e) {
			logger.error(e);
		}
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
}
