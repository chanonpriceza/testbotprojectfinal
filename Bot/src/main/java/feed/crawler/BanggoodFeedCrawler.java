package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BanggoodFeedCrawler extends FeedManager {

	public BanggoodFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public String[] setFeedFile() {
		return new String[] {"https://adsyndata.blob.core.windows.net/file/BG-Affiliate-InvolveAsia-EN-USD(CN).csv"};
	}
	
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
//		SKU=0
//				 Title=1
//				 Price=2
//				 URL=3
//				 Images=4
//				 Description=5
//				 Availability=6
//				 Category=7
//				 Currency=8
//				 Shipping Fee=9
		String id = data.get(0);
		String name = data.get(1);
		String desc = data.get(5);
		String price = data.get(2);
		String url = data.get(3);
		String image = data.get(4);
		String category = data.get(7);
		String expire = data.get(6);

		ProductDataBean pdb = new ProductDataBean();
		
		if(StringUtils.isBlank(name) || StringUtils.isBlank(url) || StringUtils.isBlank(price) ||StringUtils.isBlank(id)) {
			return null;
		}
		price = FilterUtil.removeCharNotPrice(price);
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			return null;
		}

		return pdb;
	}

}
