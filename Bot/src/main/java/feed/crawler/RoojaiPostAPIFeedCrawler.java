package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.DateTimeUtil;
import utils.FTPUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class RoojaiPostAPIFeedCrawler extends FeedManager{
	
	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
	
//    private static final String[] planTypeSet = new String[] {"1", "2", "2%2B", "3", "3%2B" }; 
    private boolean haveResult = false;
    private boolean first = true;
    
	public RoojaiPostAPIFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			first = true;
			feedRequest(feed, FEED_USER, FEED_PASS, "UTF-8", false);
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void feedRequest(String url, String username, String password, String charset, boolean redirectEnable) {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			
			if(StringUtils.isNotBlank(username)){
				String userpass = username + ":" + password;
	  			String basicAuth = "Basic " + new String(Base64.encodeBase64(userpass.getBytes()));
	  			conn.setRequestProperty ("Authorization", basicAuth);
			}
			
			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr);) {
		    		
		    		String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						processLine(line);
						
						if (Thread.currentThread().isInterrupted()) {
							throw new InterruptedException();
						}
					}
					return ;
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				logger.info("Error status : " + conn.getResponseCode() + " - " + url);
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return ;
	}
	
	public void processLine(String line) {
		if(first) {
			first = false;
		}else {
			try(StringReader lineReader = new StringReader(line);
				CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
				){
				for (CSVRecord csvRecord : record) {
					if(first){
						first = false;
						continue;
					}
					try{
						parseCSV(csvRecord);
					}catch(Exception e){
						continue;
					}
					
					if (Thread.currentThread().isInterrupted()) {
						throw new InterruptedException();
					}
				}
			}catch(Exception e){
				logger.error(e); 
			}
		}
	}
	
	private void parseCSV(CSVRecord data) {
		
//		3400 record
//		0	brand_name       HONDA
//		1   year        	 2013
//		2   model            ODYSSEY
//		3   engineSize       2300
		
		try {

			if(data == null || data.size() < 4) {
				return;
			}
			
			String original_brand 		= String.valueOf(data.get(0));
			String original_year 		= String.valueOf(data.get(1));
			String original_model 		= String.valueOf(data.get(2));
			String original_engine 		= String.valueOf(data.get(3));
			
			Calendar d = Calendar.getInstance();
			int birthYear = d.getWeekYear()-34;
			
//			for(int i=0; i<planTypeSet.length; i++) {
//				String planType = planTypeSet[i];
					
//				String url = "https://api2.uat-roojai.com/api/RoojaiPricing/getPlanList"
				String url = "https://batch-api2.roojai.com/api/RoojaiPricing/getPlanList"
						+ "?username=priceza"
						+ "&password=XxWam%3FD%3CZre%3FX9W%25"
						+ "&planType="
						+ "&brand=" + original_brand.replaceAll(" ", "%20").replaceAll("/", "%2F").replaceAll("'", "%27")
						+ "&year=" + original_year
						+ "&model=" + original_model.replaceAll(" ", "%20").replaceAll("/", "%2F").replaceAll("'", "%27")
						+ "&subModel="
						+ "&engineSize=" + original_engine.replaceAll(" ", "%20").replaceAll("/", "%2F").replaceAll("'", "%27")
						+ "&noOfDoor="
						+ "&redbookCode="
						+ "&voluntaryCode="
						+ "&isAutomaticTransmission="
						+ "&seatCapacity="
						+ "&modelTrim=A"
						+ "&isMale=true"
						+ "&isSingle=false"
						+ "&yrOfDriving=6"
						+ "&ncbRate=N"
						+ "&isDashcam=true"
						+ "&isGoToWorkOnly=false"
						+ "&dob=01%2F01%2F" + birthYear
						+ "&noOfClaim=0";
				
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/sample-data-"+DateTimeUtil.generateStringDate(new Date(), DateTimeUtil.DATE_FORMAT)+".txt"),  true));){
					String rtnResult = postRequest(url);
					int itemCount = 0;
					if(StringUtils.isNotBlank(rtnResult)) {
						itemCount = parseJson(rtnResult, original_year, original_brand, original_model, original_engine);
					}
					pwr.println(DateTimeUtil.generateStringDateTime(new Date()) +" - " + itemCount + " data from " + url);
					Thread.sleep(2000);
				}catch(Exception e) {
					e.printStackTrace();
				}
//			}
		}catch(Exception e) {
			logger.error(e);
		}
	}
	
	private int parseJson(String rtnResult, String original_year, String original_brand, String original_model, String original_engine) {

//		{
//			"planList": [{
//					"compulsoryPerYear": 900.0,
//					"other": "",
//					"isPromotion": false,
//					"promotionTH": "",
//					"pricePerYear": 18038.32,
//					"buynowURL": "https://insure.uat-roojai.com/backEnd/partner/buyNow?nonce=mrkumka",
//					"planName": "Roojai",
//					"planShortDescriptionTH": "รู้ใจ",
//					"excessAmount": 0,
//					"insurerID": "6fbe7cd0-cef3-4184-9554-ca0a18e3911b",
//					"isRoadside": true,
//					"planLongDescriptionTH": "รู้ใจ",
//					"isPanelWorkshop": false,
//					"model": "D-Max 2 doors",
//					"partnerID": "c3524c55-2230-4fbc-98ab-61acc77fd2d4",
//					"isCompulsoryAvailable": false,
//					"planType": "1",
//					"callBackURL": "https://insure.uat-roojai.com/backEnd/partner/callMeBack?nonce=mrkumka",
//					"planLongDescription": "Roojai",
//					"liability": {
//						"maxDeathCoverage": 10000000,
//						"deathPerPersonCoverage": 2000000,
//						"propertyCoverage": 5000000
//					},
//					"isBuynowAvailable": true,
//					"personalCoverage": {
//						"bailBondCoverage": 300000,
//						"medicalCoverage": 100000,
//						"paCoverage": 100000,
//						"driverPlan": 0
//					},
//					"planNameTH": "รู้ใจ 30+ แผน Premier A",
//					"planShortDescription": "Roojai",
//					"otherTH": "",
//					"isInstallment": true,
//					"planID": "59d86378-45f4-4951-9881-70e2fc2b690f",
//					"isOnlinePayment": true,
//					"installmentMessage": "ผ่อน 0% นาน 10 เดือน",
//					"carCoverage": {
//						"vehiclesumInsuredAmount": 530000,
//						"isFireNTheft": true,
//						"isFlood": true
//					},
//					"promotion": ""
//				}, {...}, {...}, {...}, {...}
//			]
//		}
		
		int itemCount = 0;
		
		try {
			JSONObject jObj = (JSONObject) new JSONParser().parse(rtnResult);
			if(jObj == null || jObj.size() == 0) return 0;
			
			JSONArray jArr = (JSONArray) jObj.get("planList");
			if(jArr == null || jArr.size() == 0) return 0;
			
			for (Object arr : jArr) {
				try {
					
					JSONObject obj = (JSONObject) arr;
					
					JSONObject liabilityObj = (JSONObject) obj.get("liability");
					JSONObject carCoverageObj = (JSONObject) obj.get("carCoverage");
					JSONObject personalCoverageObj = (JSONObject) obj.get("personalCoverage");
					
					String id 			  = String.valueOf((Object) obj.get("planID"));
					String poType 		  = String.valueOf((Object) obj.get("planType"));
					String insName 		  = "รู้ใจ กรุงไทย";
					String brand 		  = original_brand.replace("-", "_");
					String model 		  = original_model.replace("-", "_");                 
					String subModel 	  = String.valueOf((Object) obj.get("model")).replace("-", "_");                            
					String od 			  = String.valueOf((Object) carCoverageObj.get("vehiclesumInsuredAmount")); // vehiclesumInsuredAmount
					String oddd 		  = String.valueOf((Object) obj.get("excessAmount"));                      
					String garage 		  = String.valueOf((Object) obj.get("isPanelWorkshop"));                    // false = ห้าง 
					String packageName 	  = String.valueOf((Object) obj.get("planNameTH"));                         
					String carAct 		  = String.valueOf((Object) obj.get("isCompulsoryAvailable"));              // true = รวม
					                                                                                                
					String fire 		  = String.valueOf((Object) carCoverageObj.get("isFireNTheft"));            // isFireNTheft → vehiclesumInsuredAmount
					String theft 		  = String.valueOf((Object) carCoverageObj.get("isFireNTheft"));            // isFireNTheft → vehiclesumInsuredAmount
					String flood 		  = String.valueOf((Object) carCoverageObj.get("isFlood"));                 // isFlood → vehiclesumInsuredAmount
					String bi 			  = String.valueOf((Object) liabilityObj.get("deathPerPersonCoverage"));    // -ชีวิตบุคคลภายนอกต่อคน deathPerPersonCoverage
					String bt 			  = String.valueOf((Object) liabilityObj.get("maxDeathCoverage"));          // -ชีวิตบุคคลภายนอกต่อครั้ง maxDeathCoverage
					String tp 			  = String.valueOf((Object) liabilityObj.get("propertyCoverage"));          // -ทรัพย์สินบุคคลภายนอก  propertyCoverage
					String pa 			  = String.valueOf((Object) personalCoverageObj.get("paCoverage"));         // -อุบัติเหตุส่วนบุคคล   paCoverage
					String bb 			  = String.valueOf((Object) personalCoverageObj.get("bailBondCoverage"));   // -ประกันตัวผู้ขับขี่   bailBondCoverage
					String me 			  = String.valueOf((Object) personalCoverageObj.get("medicalCoverage"));    // -ค่ารักษาพยาบาล       medicalCoverage
//					String driver		  = String.valueOf((Object) personalCoverageObj.get("driverPlan"));         // 0 = ไม่ระบุคนขับ
	                                                      
					String productPrice   = String.valueOf((Object) obj.get("pricePerYear")); // รอถาม
					String productUrl 	  = "http://money.priceza.com/ประกันภัยรถยนต์";
					String productPic     = "https://halobe.files.wordpress.com/2017/08/insure-company-9419.jpg";
					
					String cc 			  = String.valueOf((new DecimalFormat("#######.0").format(Double.parseDouble(original_engine) / 1000)) + "cc");
					
					// remove product by condition.
					if(NumberUtils.toDouble(oddd) > 7500) {
						continue;
					}
					
					String uniqueText = toMD5(id);
					
					// ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
					String productName = "";
					productName += "ประกันชั้น " + poType;
					productName += " - " + insName;
					productName += " - " + brand;
					productName += " - " + model + " " + cc;
					productName += " - " + ((StringUtils.isNotBlank(subModel) && !subModel.equals("0"))? subModel : "ไม่ระบุรุ่นย่อย");
					productName += " - ปี " + original_year;
					productName += " (" + uniqueText.substring(0,10) + ")";
					productName = productName.replace(",", "");
					productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
					productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
					productName = productName.replaceAll("\\s+", " ");                                                  
					productName = productName.replace("( ", "(");                                                       
					productName = productName.replace(" )", ")");    
		 			productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
					productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");      
					
					// ( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx) (PlanId)
					
					String productDesc = "";
					productDesc += ((StringUtils.isNotBlank(packageName))? packageName : "ไม่ระบุแผน" ) ; 
					productDesc += " - " + (StringUtils.isNotBlank(garage)?"ซ่อม"+((garage.equals("true"))?"อู่":"ห้าง"):"ไม่ระบุอู่ซ่อม");
					productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
					productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
					productDesc += " - " + ((StringUtils.isNotBlank(carAct) && (carAct.equals("Yes"))) ? "รวมพรบ." : "ไม่รวมพรบ.");
					productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && fire.equals("true"))? ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od )) : "ไม่ระบุ" );
					productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && theft.equals("true"))? ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od )) : "ไม่ระบุ" );
					productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && flood.equals("true"))? ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od )) : "ไม่ระบุ" ); 
					productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
					productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
					productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
					productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
					productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );          
					productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
					productDesc += " ("+id+")";
					productDesc = productDesc.replaceAll("\\s+", " ");                                               
					productDesc = productDesc.replace("( ", "(");                                                    
					productDesc = productDesc.replace(" )", ")");                                                    
					productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
					productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
					productDesc = productDesc.replace(".00", "");
					productDesc = productDesc.replace(",", "");
				
					ProductDataBean pdb = new ProductDataBean();
					pdb.setName(productName);
					pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
					pdb.setPictureUrl(productPic);
					pdb.setDescription(productDesc);
					pdb.setUrl(productUrl);
					pdb.setUrlForUpdate(uniqueText);
					pdb.setCategoryId(250101);
					pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");
					
					String result = mockResult(pdb);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
							pwr.println(result);
							haveResult = true;
						}catch(Exception e) {
							logger.error(e);
						}
					}

					itemCount++;
					
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
			
			
		return itemCount;
	}

	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	private static String postRequest(String url) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	try {
	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(false);
	 		conn.setRequestMethod("POST");
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
	 		conn.setDoOutput(true);
	 	
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		isr = new InputStreamReader(conn.getInputStream());
	 		brd = new BufferedReader(isr);
	 		StringBuilder rtn = new StringBuilder(5000);
	 		String line = "";
	 		while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 		return rtn.toString();
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e2) {
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
		return null;
	 	
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	
	
	
}
