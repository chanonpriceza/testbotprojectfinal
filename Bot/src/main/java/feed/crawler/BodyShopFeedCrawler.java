package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class BodyShopFeedCrawler extends FeedManager {
	private boolean haveResult;
	private static final Logger logger = LogManager.getRootLogger();
	private final static String  FILE_NAME = "dataHolder.json";
	private static String currentCat = "";
	
	public BodyShopFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : getCategoryKeySet()) {
			haveResult = false;
			int page = 1;
			do {
				haveResult = false;
				String req_url = feed.replace("{page}",String.valueOf(page));
				currentCat = feed;
				feedRequest(req_url,"UTF-8", false);
				logger.info("Page: "+page);
				logger.info(req_url);
				mockData();
				page += 1;
				if(haveResult)
					BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
			}while(haveResult);
		}
		
	}
	
	private void feedRequest(String url, String charset, boolean redirectEnable) {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("accept","application/json, text/plain, */*");
			conn.setRequestProperty("referer","https://www.central.co.th/th/the-body-shop/products?page=2");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
			conn.setRequestProperty("x-store-code","th");
			
			conn.setConnectTimeout(180000);
			conn.setReadTimeout(180000);
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" +FILE_NAME).toPath();
			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream()){
    					Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);	
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				logger.info("Error status : " + conn.getResponseCode() + " - " + url);
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		logger.info("Error "+e.getMessage()+" url: "+url);
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return ;
	}
	
	private void mockData() {
		try {
			String feed = BaseConfig.FEED_STORE_PATH + "/" +FILE_NAME;
			 JSONParser jsonParser = new JSONParser();
			   try (FileReader reader = new FileReader(feed))
		        {
		            Object obj = jsonParser.parse(reader); 
		            JSONObject allJSON = (JSONObject) obj;
		            JSONObject cutJSON = (JSONObject)allJSON.get("products");
		            JSONArray productList = (JSONArray)cutJSON.get("products");
		    		for(Object e:productList) {
		    			JSONObject json = (JSONObject) e;
		    			parseData(json);
		    		}
		 
		        }
		
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private final static String IMAGE_URL = "https://backend.central.co.th/media/catalog/product";
	private final static String PRODUCT_URL = "https://www.central.co.th/th/";
	
	private void parseData(JSONObject json) {	
		
		String id = String.valueOf(json.get("id")).trim();
		String name = String.valueOf(json.get("name")).trim();
		String desc = String.valueOf(json.get("description_option")).trim();
		String price = String.valueOf(json.get("online_price_option")).trim();
		String url  = String.valueOf(json.get("url_key")).trim();
		String basePrice = String.valueOf(json.get("price")).trim();
		String image = String.valueOf(json.get("small_image_option")).trim();
		String sku = String.valueOf(json.get("sku")).trim();
		
		if( StringUtils.isBlank(name) ||StringUtils.isBlank(url) || StringUtils.isBlank(price) || StringUtils.isBlank(id)){
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		String prefix = "";
		if(!name.toLowerCase().contains("the body shop")) {
			prefix = "The Body Shop ";
		}
		pdb.setName(prefix+name+" ("+sku+")");	
		pdb.setPrice((FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
		pdb.setUrl(PRODUCT_URL+url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		
		if(!image.isEmpty()){
			pdb.setPictureUrl(IMAGE_URL+image);
		}
		
		if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
		}
		
		if(StringUtils.isNotBlank(desc)){
			pdb.setDescription(desc);
		}
		
		String[] mapping = getCategory(currentCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		mockResult(pdb);
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	

}
