package feed.crawler;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AltheaFeedCrawler extends FeedManager {

	public AltheaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://gsf-xml-feeds.simpshopifyapps.com/althea-inc-JkiQYXjH8P/catalog1.xml"};
	}
	
	@Override
	public ProductDataBean parse(String data) {
		Document doc = Jsoup.parse(data);
		String name = doc.select("g|title").html();
		String price = doc.select("g|sale_price").html();
		String basePrice = doc.select("g|price").html();
		String id = doc.select("g|id").html();
		String desc = doc.select("g|description").html();
		String url = doc.select("g|link").html();
		String brand = doc.select("g|brand").html();
		String image = doc.select("g|image_link").html();
		double p  = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price));
		double bp = FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice));
		String tmp = "";
		
		if(StringUtils.isNotBlank(id)) 
			tmp = "("+id+")";
		
		if(p > bp || p == 0) {
			double swap = p;
			p = bp;
			bp = swap;
		}
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(brand+" "+name+" "+tmp);
		pdb.setPrice(p);
		pdb.setBasePrice(bp);
		pdb.setDescription(desc);
		pdb.setUrl(url);
		pdb.setPictureUrl(image);
		String[] mapping = getCategory(brand);
		
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		
		return pdb;
	}

}
