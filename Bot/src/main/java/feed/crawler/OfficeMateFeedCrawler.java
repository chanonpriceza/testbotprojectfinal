package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class OfficeMateFeedCrawler extends FeedManager{

	public OfficeMateFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://aumento.officemate.co.th/feedrss/Product-feed-google.xml"
		};
	}

	public ProductDataBean parse(String xml){
		xml = xml.replace(" xmlns:g=\"http://base.google.com/ns/1.0\"", "");
		xml = xml.replace("<![CDATA[","").replace("]]>","");
		
		String productName 			= FilterUtil.getStringBetween(xml, "<g:title>", "</g:title>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<g:sale_price>", "</g:sale_price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<g:price>", "</g:price>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<g:link>", "</g:link>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<g:description>", "</g:description>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<g:image_link>", "</g:image_link>").trim();
		String realProductId 		= FilterUtil.getStringBetween(xml, "<g:id>", "</g:id>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<g:google_product_category>", "</g:google_product_category>").trim();
		String expire 				= FilterUtil.getStringBetween(xml, "<g:availability>", "</g:availability>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName+" ("+realProductId+")");
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		if(!BotUtil.checkStringEqual(productPrice, productBasePrice)){
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));		
		}
		
		if (!productPictureUrl.isEmpty()) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(!realProductId.isEmpty()){
			pdb.setRealProductId(realProductId);
		}
		
		if(!productDesc.isEmpty()){
			pdb.setDescription(FilterUtil.toPlainTextString(productDesc));
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);		
		}
		
		if(StringUtils.isBlank(productCatId)) {
			pdb.setCategoryId(80575);
		}
		
		if("Out of stock".equalsIgnoreCase(expire)) {
			pdb.setExpire(true);
			return null;
		}
		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(realProductId);
		
		return pdb;
		
	}


}
