package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.SocketFactory;

public class TemplateShopeeDistributedFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	static List<ProductDataBean> productList = new ArrayList<ProductDataBean>();
	static List<ProductDataBean> productListWithCat = new ArrayList<ProductDataBean>();
	
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
	protected static String shopId;
	protected static boolean nextPage = false;
	protected static boolean error = false;
	protected static int itemCount = 0;
	protected static int errorCount = 0;
	private static int itemRound = 0;
	private static int page = 0;
	private static final int PAGE_OFFSET = 100;
	private static  boolean haveResult = false;
	private static int fixcat;
	
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);

	public TemplateShopeeDistributedFeedCrawler() throws Exception {
		super();
	}

	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		if(BaseConfig.CONF_ENABLE_PROXY) {
			logger.info("Enable proxy !!!!!!!!!!!");
			enableUseProxy();
		}
		shopId = BaseConfig.CONF_FEED_CRAWLER_PARAM;
    	SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
    	
		
		if(shopId.contains(",")) {
			String[] inputData = shopId.split(",");
			if(inputData.length>=2) {
				shopId = inputData[0];
				fixcat = BotUtil.stringToInt(inputData[1],0);
			}
		}
		
		if (!StringUtils.isBlank(shopId)) {
			
			String cookie = getCookies(shopId);
			if(StringUtils.isBlank(cookie)) return;
			
			int i = 0;
			do {
				nextPage = false;
				String url = "https://shopee.co.th/api/v2/search_items/"
						+ "?by=ctime"
						+ "&order=desc"
						+ "&newest=" + page * PAGE_OFFSET
						+ "&limit="+PAGE_OFFSET
						+ "&skip_price_adjust=false"
						+ "&page_type=shop"
						+ "&match_id=" + shopId;
				String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, cookie);
				if(StringUtils.isBlank(data)){ break;}
				try {
					parseJSON(data);
				}catch(Exception e) {
					logger.error("Error",e);
				}
				
				page++ ;
				i++;
			}while(!Thread.currentThread().isInterrupted() && nextPage && !error);
			if(error){
				logger.info("This current Merchant is ExceedLimit :..............");
				return;
			}
			getProductWithCat(cookie,shopId);
			mapCat(productList,productListWithCat);
			logger.info("Item Count: " + itemCount + " ,Error Count: " + errorCount+" Round : "+i);
		}else {
			logger.error("Cant't get shop id.");
			return ;
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void getProductWithCat(String cookie,String shopId){
		if (!StringUtils.isBlank(shopId)) {
			if(StringUtils.isBlank(cookie)) return;
			
			Set<String> keySet = getCategoryKeySet();
			for (String catId : keySet) {
				page = 0;
				String[] catMap = getCategory(catId);
				if(catMap == null || catMap.length != 2) continue;
				String pzCatId = catMap[0];
				String pzKeyword = catMap[1];
				if(!NumberUtils.isCreatable(pzCatId)) continue;
				
				boolean isFirstParamName = true;
				
				do {
					nextPage = false;
					itemRound = 0;
					
					if(isFirstParamName) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&shop_categoryids=" + catId;
						
						String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, cookie);
						
						try {
							parseJSON(data, pzCatId, pzKeyword);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
					}
					
					if(itemRound == 0) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&original_categoryid=" + catId;
						
						String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, cookie);
						
						try {
							parseJSON(data, pzCatId, pzKeyword);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
						if(itemRound > 0) isFirstParamName = false;
					}
					
					if(nextPage) page++ ;
					
				}while(!Thread.currentThread().isInterrupted() && nextPage);
				
			}
			
		}else {
			logger.error("Cant't get shop id.");
			return ;
		}
		
	}
	
	private void mapCat(List<ProductDataBean> product,List<ProductDataBean> productWithCat){
		for(ProductDataBean pdb:product){
			String mainProductCat = pdb.getRealProductId();
			for(ProductDataBean productCat:productWithCat){
				if(mainProductCat.equals(productCat.getRealProductId())){
					pdb.setCategoryId(productCat.getCategoryId());
					pdb.setKeyword(productCat.getKeyword());
				}
			}
			
			if(fixcat!=0&&pdb.getCategoryId()==0)
				pdb.setCategoryId(fixcat);
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error("Error",e);
				}
			}
		}
	}

	private void parseJSON(String data,String cat,String keyword){
		try{
			
		JSONObject allObj = (JSONObject) new JSONParser().parse(data);
		JSONArray jArr = (JSONArray) allObj.get("items");
		
		for (Object object : jArr) {
			
			JSONObject jObj 		= (JSONObject) object;
			String productId 		= jObj.get("itemid").toString();
			String productName 		= (String) String.valueOf(jObj.get("name"));
			String productPrice 	= (String) String.valueOf(jObj.get("price"));
			String basePrice 		= (String) String.valueOf(jObj.get("price_before_discount"));
			String productImageUrl 	= (String) String.valueOf(jObj.get("image"));
			String productDesc 		= (String) String.valueOf(jObj.get("description")).trim();
			String productUrl 		= "";
			String productNameUrl 	= "";
			
			productName = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(productName).trim());
			productNameUrl = productName.replaceAll("%", "");
			
			String linkName = productNameUrl + "-i." + shopId + "." + productId;
			productUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
			productImageUrl = productImageUrl.replace("&amp;", "&");
			
			if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(productName)) {
				continue;
			}
			ProductDataBean pdb = new ProductDataBean();

			pdb.setName(productName);
			if(StringUtils.isNotBlank(productPrice) && NumberUtils.isCreatable(productPrice)) {
				if(Double.parseDouble(productPrice) != 0) {
					pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)) / 100000);
				}else {
					pdb.setPrice(BotUtil.CONTACT_PRICE);
					error = true;
				}
			}else {
				pdb.setPrice(BotUtil.CONTACT_PRICE);
				error = true;
			}
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(shopId + "." + productId);
			pdb.setRealProductId(productId);

			if (StringUtils.isNotBlank(basePrice)) {
				pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)) / 100000);
			}

			if (StringUtils.isNotBlank(productImageUrl)) {
				pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
			}

			if (StringUtils.isNotBlank(productDesc)&&!"null".equals(productDesc)) {
				productDesc = StringEscapeUtils.unescapeHtml4(productDesc).trim();
				productDesc = FilterUtil.toPlainTextString(productDesc);
				pdb.setDescription(productDesc);
			}
			if(cat==null&&keyword==null){
				pdb.setCategoryId(0);
				productList.add(pdb);
			}else{
				pdb.setCategoryId(Integer.parseInt(cat));
				pdb.setKeyword(keyword);
				productListWithCat.add(pdb);
			}
			nextPage = true;
		}
		}catch (Exception e) {
			logger.info(e);
		}
	}
	
	protected void parseJSON(String data)	throws UnsupportedEncodingException, ClassNotFoundException, SQLException, ParseException {
		parseJSON(data,null, null);
	}
	
	private String getCookies(String shopId) {
		String cookie = null;
		
		URL u = null;
		HttpURLConnection conn = null;

		try {
//			u = new URL("https://shopee.co.th/shop/" + shopId + "/");
			u = new URL("https://shopee.co.th/api/v1/account_info/");
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(1800000);
			conn.setReadTimeout(1800000);
			
			cookie = conn.getHeaderField("Set-Cookie");
			
		}catch(Exception e) {
			logger.error("Error",e);
		}
		
		return cookie;
	}
	
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();

	public static String httpRequestWithStatusIgnoreCookies(String url, int page, String shopId,String cookie) {
		HttpGet httpPost = new HttpGet(url);
		httpPost.setConfig(HTTP_LOCAL_CONFIG);
		httpPost.addHeader("User-Agent", USER_AGENT);
		httpPost.addHeader("cookie", cookie);
		httpPost.addHeader("referer", "https://shopee.co.th/shop/"+shopId+"/search/?page="+page+"&sortBy=ctime");
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpPost)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is,"UTF-8");
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while(!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null)
    					rtn.append(line);
    				return rtn.toString();
    			}
    		}else{
				return null;
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpPost != null)
				httpPost.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    }
	
	public String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getRealProductId()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
