package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class Level51PCFeedCrawler extends FeedManager {
	private static boolean haveResult = false;
	private static final Logger logger = LogManager.getRootLogger();
	private final static String FILE_NAME = "feed.json";
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
	
	public Level51PCFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Level51DownloadFile("https://www.level51pc.com/__priceza/feed.json",FILE_NAME);
		mockFeed();
		if(haveResult) {
			logger.info("Set Feed Path");
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		}
	}
	
	private void parseData(String data) {
		try {

				JSONObject ob = (JSONObject)new JSONParser().parse(data);
				String name = String.valueOf(ob.get("name"));
				String id = String.valueOf(ob.get("id"));
				String desc = String.valueOf(ob.get("description"));
				double price = (double)ob.get("price");
				double basePrice = (double)ob.get("base_price");
				String cat = String.valueOf(ob.get("category_id"));
				String url = String.valueOf(ob.get("url"));
				String image = String.valueOf(ob.get("image_url"));

				
				JSONObject pricezaDetail = (JSONObject) ob.get("pricezadetails");
				JSONObject performance = (JSONObject) pricezaDetail.get("performance");
				
				String cpu = String.valueOf(performance.get("cpu"));
				String hdd = String.valueOf(performance.get("harddisk"));
				String display = String.valueOf(performance.get("displaySize"));
					

				ProductDataBean pdb = new ProductDataBean();
				
				if(StringUtils.isBlank(name)||price==0||StringUtils.isBlank(url)||StringUtils.isBlank(cpu)||StringUtils.isBlank(hdd)) {
					logger.info("not complete data at: "+id);
					return;
				}
				
				if(StringUtils.isNotBlank(name)&&StringUtils.isNotBlank(cpu)&&StringUtils.isNotBlank(hdd)) {
					pdb.setName(name+" "+cpu+" "+hdd + " (" + id + ")");
				}
				pdb.setUrl(url);
				pdb.setDescription(display+","+cpu+","+hdd+","+desc);
				pdb.setPrice(price);
				pdb.setBasePrice(basePrice);
				pdb.setUrl(url);
				pdb.setRealProductId(id);
				pdb.setPictureUrl(image);
				pdb.setUrlForUpdate(id);
				
				String[] mapping = getCategory(cat);
				if(mapping != null) {
					pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
					pdb.setKeyword(mapping[1]);				
				} else {
					pdb.setCategoryId(0);
				}
				 mockResult(pdb);
			
			
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) 			result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) 			result.append("<desc>"+desc+"</desc>");
		if(price!=0) 								result.append("<price>"+price+"</price>");
		if(basePrice!=0) 							result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) 			result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) 			result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(urlForUpdate))	result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(StringUtils.isNotBlank(id)) 				result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) 									result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword))	 		result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) 									result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					logger.info("Write Mock File");
					haveResult = true;
				}catch(Exception e) {
					logger.info("Line 134 "+e.getMessage());
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}

	private void mockFeed() {
		try {
			String feed = BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME;
			logger.info("Feed: "+feed);
			File file = new File(feed);
			FileInputStream ins = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(ins,BaseConfig.CONF_PARSER_CHARSET);
			@SuppressWarnings("resource")
			BufferedReader brd = new BufferedReader(isr); 
			StringBuilder rtn = new StringBuilder();
			int c ;
			while ((c = brd.read()) != -1 ) {
					rtn.append((char) c);
					if(rtn.toString().contains("},{")) {
						String result = rtn.toString();
						result = FilterUtil.getStringBetween(result,"id\":","},{");
						result = ("{\"id\":"+result+"}");
						parseData(result);
						rtn = new StringBuilder();
					}
			}

		}catch (Exception e) {	
			logger.info("Line 212 "+e.getMessage());
		}
	}
	
	
	public static String[] Level51DownloadFile(String url, String fileName) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();		
			conn.setRequestProperty("User-Agent", USER_AGENT);

			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream is = conn.getInputStream();) {
				Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);
	    	}
		}catch(IOException e){
			if(!e.getMessage().contains("Premature EOF")) {
				logger.info(e.getMessage());	
			}
		}catch (Exception e) {
			logger.info("247 "+e.getMessage());
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
