package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class PatPatFeedCrawler extends FeedManager {

	public PatPatFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	private static JSONParser parser = new JSONParser();
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		try {
		for(String cat:getCategoryKeySet()) {
			for(int i = 1;i<=100;i++) {
				URL u = new URL(cat.replace("{page}",String.valueOf(i))) ;
				HttpURLConnection http = (HttpURLConnection) u.openConnection();
				http.setRequestProperty("Content-Type","application/json");
				http.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
				InputStream in = http.getInputStream();
				InputStreamReader reader = new InputStreamReader(in);
				JSONObject mainObj = (JSONObject) parser.parse(reader);
				mainObj = (JSONObject) mainObj.get("data");
				JSONArray docs = (JSONArray) mainObj.get("items");
				if(docs==null||docs.size()==0) {
					break;
				}
				JSONArray array = new JSONArray();
				for(Object d:docs) {
					JSONObject o =  (JSONObject)d;
					JSONObject result = new JSONObject();					
					result.put("name",String.valueOf(o.get("product_name")));
					result.put("id",String.valueOf(o.get("product_id")));
					result.put("price",String.valueOf(o.get("price")));
					result.put("basePrice",String.valueOf(o.get("msrp")));
					result.put("url",String.valueOf(o.get("product_url")));
					result.put("image",String.valueOf(o.get("icon")));
					String[] mapping = getCategory(cat);
					if(mapping != null) {
						result.put("catId",BotUtil.stringToInt(mapping[0], 0));
						result.put("keyword",mapping[1]);			
					} else {
						result.put("catId",0);
					}
					array.add(result);
					
				}
				String fileName = toMD5(cat);
				fileName = fileName.substring(0,10);
				InputStream is = new ByteArrayInputStream(array.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+fileName+"-"+BaseConfig.MERCHANT_ID+"-"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
			
		}
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error",e);
		}
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		String  name = String.valueOf(data.get("name"));
		String  id = String.valueOf(data.get("id"));
		String  price = String.valueOf(data.get("price"));
		String  basePrice = String.valueOf(data.get("basePrice"));
		String  url = String.valueOf(data.get("url"));
		String  image = String.valueOf(data.get("image"));
		String  cat = String.valueOf(data.get("catId"));
		String  keyword = String.valueOf(data.get("keyword"));
		
		price =FilterUtil.removeCharNotPrice(price);
		basePrice =FilterUtil.removeCharNotPrice(basePrice);
		
		double priceReal = FilterUtil.convertPriceStr(price); //0
		double baseReal = FilterUtil.convertPriceStr(basePrice);//100
		
		if(priceReal==0) {
			double tmp = priceReal;
			priceReal = baseReal;
			baseReal = tmp;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl("https://www.patpat.com/product/"+url+".html?is_new_category=1&sku_id="+id);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(image);
		int catInt = BotUtil.stringToInt(cat, 0);
		if(catInt!=0) {
			pdb.setCategoryId(catInt);
			pdb.setKeyword(keyword);
		}
	
				
				
		return pdb;
	}
	
	
}
