package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class JDCentralKeywordFeedCrawler extends FeedManager {
	//https://api.jd.co.th/client.action?body={"page":"4","pagesize":"60","cid":"","filed":"","expandName":{},"brand":"","isCorrect":"1","sort":"0","stock":1,"keyword":"ถุงยาง"}&functionId=search&client=pc&clientVersion=2.0.0&lang=th_TH&area=184549376-185008128-185008132-0
	private static String url = "https://api.jd.co.th/client.action?body={param}&functionId=search&client=pc&clientVersion=2.0.0&lang=th_TH&area=184549376-185008128-185008132-0";
	private static String param = "{\"page\":\"{page}\",\"pagesize\":\"60\",\"cid\":\"\",\"filed\":\"\",\"expandName\":{},\"brand\":\"\",\"isCorrect\":\"1\",\"sort\":\"0\",\"stock\":1,\"keyword\":\"{keyword}\"}";
	private static JSONParser jsonParser = new  JSONParser();
	
	public JDCentralKeywordFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		for(String cat:getCategoryKeySet()) {
			for(int page = 1; page <= 20 ; page++) {
				try {
				String genParam = param.replace("{page}",String.valueOf(page));
				genParam = genParam.replace("{keyword}",cat);
				String genUrl = url.replace("{param}", genParam);
				InputStream in = new URL(genUrl).openStream();
				JSONObject	ob = (JSONObject) jsonParser.parse(new InputStreamReader(in));
				JSONArray	ar = (JSONArray) ob.get("wareInfo");
				JSONArray array = new JSONArray();
				String keyword = "";
				for(Object a:ar) {
					JSONObject o =  (JSONObject)a;
					JSONObject result = new JSONObject();	
					result.put("name",String.valueOf(o.get("wname")));
					result.put("id",String.valueOf(o.get("spuId")));
					result.put("price",String.valueOf(o.get("jdPrice")));
					result.put("basePrice",String.valueOf(o.get("originPrice")));
					result.put("url","https://www.jd.co.th/product/"+String.valueOf(o.get("spuId"))+".html");
					result.put("image",String.valueOf(o.get("imageurl")));
					String[] mapping = getCategory(cat);
					if(mapping != null) {
						result.put("catId",BotUtil.stringToInt(mapping[0], 0));
						result.put("keyword",mapping[1]);
						keyword = mapping[1];
					} else {
						result.put("catId",0);
					}
					array.add(result);
				}
				String fileName = toMD5(cat+keyword);
				fileName = fileName.substring(0,10);
				InputStream is = new ByteArrayInputStream(array.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+fileName+"-"+BaseConfig.MERCHANT_ID+"-"+page+".json").toPath(), StandardCopyOption.REPLACE_EXISTING);
				}catch (Exception e) {
					logger.error("Error",e);
				}
				
			}
		}
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		
		String  name = String.valueOf(data.get("name"));
		String  id = String.valueOf(data.get("id"));
		String  price = String.valueOf(data.get("price"));
		String  basePrice = String.valueOf(data.get("basePrice"));
		String  url = String.valueOf(data.get("url"));
		String  image = String.valueOf(data.get("image"));
		String  cat = String.valueOf(data.get("catId"));
		String  keyword = String.valueOf(data.get("keyword"));
		
		price =FilterUtil.removeCharNotPrice(price);
		basePrice =FilterUtil.removeCharNotPrice(basePrice);
		
		double priceReal = FilterUtil.convertPriceStr(price); //0
		double baseReal = FilterUtil.convertPriceStr(basePrice);//100
		
		if(priceReal==0) {
			double tmp = priceReal;
			priceReal = baseReal;
			baseReal = tmp;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(image);
		int catInt = BotUtil.stringToInt(cat, 0);
		if(catInt!=0) {
			pdb.setCategoryId(catInt);
			pdb.setKeyword(keyword);
		}
	
				
				
		return pdb;
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}

}
