package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class JDCentralSharebuyFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
//	private static List<String> catList = null;
	
	public JDCentralSharebuyFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
//		catList = new ArrayList<>();
		return new String[] {
				"http://sharebuy.jd.co.th/getLineFeed"
			};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed+" at:"+new Date());
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				editFileReplaceNewLine(fileName);
			}catch (ClosedByInterruptException e) {
				logger.error(e);
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		logger.info("Finish : Load : Feed at:"+new Date());
	}
	
	private void editFileReplaceNewLine(String fileName) {
		String newFileName = BaseConfig.FEED_STORE_PATH + "/editedFile.xml";
		boolean haveResult = false;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(BaseConfig.FEED_STORE_PATH +"/"+fileName),"UTF-8"), 10);
			 PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/editedFile.xml"),  false))){
			
			String text = "";
			while((text = br.readLine()) != null) {
				text = text.replaceAll("\r\n", "");
				pwr.print(text.toString().replaceAll("</item>","</item>\n"));
				haveResult = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {newFileName};
	}
	
	public ProductDataBean parse(String xml){
		
//		<item>
//	        <g:extra_deeplink_url_ios>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/4029119/utm_source/line/utm_medium/line_shopping</g:extra_deeplink_url_ios>
//	        <g:gtin></g:gtin>
//	        <g:image_link>https://img.jd.co.th/babelth/jfs/t31/167/1417887533/111505/f60d6515/5dd3b306N44775235.jpg!q50.jpg.webp</g:image_link>
//	        <g:color></g:color>
//	        <g:gender>unisex</g:gender>
//	        <g:link>https://item.jd.co.th/4029119.html</g:link>
//	        <g:description>Huawei Honor band 5 นาฬิกาสมาร์ทวอทช์ สายรัดข้อมือเพื่อสุขภาพ Smart Watch</g:description>
//	        <g:extra_deeplink_url_andriod>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/4029119</g:extra_deeplink_url_andriod>
//	        <g:availability>in stock</g:availability>
//	        <g:title>Huawei Honor band 5 นาฬิกาสมาร์ทวอทช์ สายรัดข้อมือเพื่อสุขภาพ Smart Watch</g:title>
//	        <g:sale_price>919 THB</g:sale_price>
//	        <g:condition>new</g:condition>
//	        <g:product_type>หน้าหลัก &amp;gt; Consumer Electronics &amp;gt; Smart Electronics &amp;gt; Smartwatches</g:product_type>
//	        <g:size></g:size>
//	        <g:price>2090 THB</g:price>
//	        <g:vendor_id>16538</g:vendor_id>
//	        <g:code_of_vendor></g:code_of_vendor>
//	        <g:mobile_link>https://m.jd.co.th/product/4029119.html?utm_source=line&utm_medium=line_shopping</g:mobile_link>
//	        <g:id>4029119</g:id>
//	        <g:unit_pricing_measure>22.7g</g:unit_pricing_measure>
//	        <g:adult>no</g:adult>
//	        <g:brand>Huawei(หัวเว่ย)</g:brand>
//	        <g:google_product_category></g:google_product_category>
//	        <g:sharebuy_price>788 THB</g:sharebuy_price>
//	        <g:members>2</g:members>
//	        <g:sale_volumes>0</g:sale_volumes>
//	    </item>
		
		String productName 				= FilterUtil.getStringBetween(xml, "<g:title>", "</g:title>").trim();		
		String productPrice 			= FilterUtil.getStringBetween(xml, "<g:price>", "</g:price>").trim();
		String productSalePrice			= FilterUtil.getStringBetween(xml, "<g:sale_price>", "</g:sale_price>").trim();
		String productSharebuyPrice		= FilterUtil.getStringBetween(xml, "<g:sharebuy_price>", "</g:sharebuy_price>").trim();
		String productSharebuyMember	= FilterUtil.getStringBetween(xml, "<g:members>", "</g:members>").trim();
		String productUrl 				= FilterUtil.getStringBetween(xml, "<g:link>", "</g:link>").trim();
		String productImageUrl 			= FilterUtil.getStringBetween(xml, "<g:image_link>", "</g:image_link>");
		String productCatId 			= FilterUtil.getStringBetween(xml, "<g:product_type>", "</g:product_type>");
		String productDesc 				= FilterUtil.getStringBetween(xml, "<g:description>", "</g:description>").trim();
		String productId 				= FilterUtil.getStringBetween(xml, "<g:id>", "</g:id>").trim();	
		String status	 				= FilterUtil.getStringBetween(xml, "<g:availability>", "</g:availability>").trim();	
		
		
		if(!status.equals("in stock")){
			return null;
		}
		
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;		
		}
				
		if(StringUtils.isBlank(productSalePrice)) {
			productSalePrice = productPrice;
			productPrice = "";
		}
		
		productName += " (" + productId + ")";
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productSharebuyPrice)));
		pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		
		String descriptionCode = "[SB"
				+ "-" + productSharebuyMember
				+ "-" + FilterUtil.removeCharNotPrice(productSharebuyPrice)
				+ "-" + FilterUtil.removeCharNotPrice(productSalePrice)
				+ "-" + FilterUtil.removeCharNotPrice(productPrice)
				+ "] ";
				
		if (productDesc.length() != 0) {					
			pdb.setDescription(descriptionCode + productDesc);
		}
		
		if (productImageUrl.length() != 0) {
			pdb.setPictureUrl(productImageUrl);
		}
		
		pdb.setUrlForUpdate(productUrl);
		
		productUrl = productUrl + "||https://jdcentral.onelink.me/SJGK?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_dp=openjdthai%3A%2F%2Fdeeplink%2Fcategory%2Fjump%2FM_sourceFrom%2Fsx_thai%2Fdes%2FproductDetail%2FskuId%2F******&af_reengagement_window=30d&clickid=(xxxxx)&af_ios_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html&af_android_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html";
		productUrl = productUrl.replace("******", productId);
		
		pdb.setUrl(productUrl);
		
		String targetCat = StringEscapeUtils.unescapeHtml4(productCatId).replace("&gt;",">").replace("&amp;","&");
		String[] catMap = getCategory(targetCat);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}
		
		
//		if(!catList.contains(targetCat)){
//			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File("D:/listCatJDSharebuy.txt"),  true));){
//				System.out.println(targetCat);
//				pwr.println(targetCat + "|" + productName + "|" + productUrl);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
//			catList.add(targetCat);
//		}
		
		
		return pdb;
	}
	
}
