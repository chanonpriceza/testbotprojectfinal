package feed.crawler;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class FrankFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
    private DecimalFormat formatPrice = null;
    
	public FrankFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){
		
		try{
			
//					product id=0
//					 broker=1
//					 insurer=2
//					 title=3
//					 type=4
//					 premium=5
//					 =6
//					 brand=7
//					 model=8
//					 sub_model=9
//					 cc=10
//					 year=11
//					 URL=12
//					 compulsory_insurance=13
//					 sum_insured=14
//					 fire_damage=15
//					 theft=16
//					 flood_damage=17
//					 deductible=18
//					 repair=19
//					 bodily_injury_liability=20
//					 bodily_injury_liability_Accident=21
//					 property_damage_liability=22
//					 personal_injury_protection=23
//					 bail_bond=24
//					 medical_payments=25
//			
			String poType = data.get(4).toLowerCase().replace("type", "").trim().replace("-", " ").replace(",", "");                // -ชั้นประกัน
			String insName = data.get(2).trim().replace("-", " ");                                       // -บริษัทประกัน
			if("BKI".equals(FilterUtil.toPlainTextString(insName))) {
				return null;
			}
			String brand = data.get(7).trim().replace("-", " ");                                                                    // -ยี่ห้อรถ
			String model = data.get(8).trim().replace("-", " ");                                                                    // -รุ่นรถ
			String subModel = data.get(9).trim().replace("-", " ");                                                                 // -รุ่นย่อย
			String year = data.get(11).trim().replace("-", " ");                                                                     // -ปีรถ
			String od = data.get(14).trim().replace("-", " ");                                                                       // -ทุนประกัน
			String oddd = data.get(18).trim().replace("-", " ");                                                                    // -ค่าเสียหายส่วนแรก
			String garage = data.get(19).trim().replace("-", " ");                                                                  // -ซ่อม
			if(garage.contains("อู่"))
				garage = "ซ่อมอู่";
			else if(garage.contains("ศูนย์")||garage.contains("ห้าง"))
				garage = "ซ่อมศูนย์";
			else
				garage = "ไม่ระบุอู่ซ่อม";
			String packageName = data.get(3).trim().replace("-", " ");                                                            // -แผน
			String carAct = data.get(13).trim().replace("-", " ").replace(",", "");	                
			
			String fire = data.get(15).trim().replace("-", " ");           										                    // -คุ้มครองไฟไหม้
			String theft = data.get(16).trim().replace("-", " ");          										                    // -คุ้มครองโจรกรรม
			String flood = data.get(17).trim().replace("-", " ");          										                    // -คุ้มครองน้ำท่วม
			String bi = data.get(20).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อคน
			String bt = data.get(21).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อครั้ง
			String tp = data.get(22).trim().replace("-", " ");             										                    // -ทรัพย์สินบุคคลภายนอก
			String pa = data.get(23).trim().replace("-", " ");             										                    // -อุบัติเหตุส่วนบุคคล
			String bb = data.get(24).trim().replace("-", " ");             										                    // -ประกันตัวผู้ขับขี่
			String me = data.get(25).trim().replace("-", " ");             										                    // -ค่ารักษาพยาบาล
			
			String id = data.get(0).trim();
			String productPrice = data.get(5).trim();
			String productUrl = data.get(12).trim();
			
			String uniqueText = "";
			uniqueText += id;
			uniqueText = toMD5(uniqueText);
			
//			ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
			String productName = "";
			productName += "ประกันชั้น " + poType;
			productName += " - " + insName;
			productName += " - " + brand;
			productName += " - " + ((StringUtils.isNotBlank(subModel) && !subModel.equals("0"))? subModel : "ไม่ระบุรุ่นย่อย");
			productName += " - " + model;
			productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
			productName += " (" + uniqueText.substring(0,10) + ")";
			productName = productName.replace(",", "");
			productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
			productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
			productName = productName.replaceAll("\\s+", " ");                                                  
			productName = productName.replace("( ", "(");                                                       
			productName = productName.replace(" )", ")");    
 			productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
			productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");                            
			                             
//			( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
			String productDesc = "";
			productDesc += ((StringUtils.isNotBlank(packageName))? packageName : "ไม่ระบุแผน" ); 
			productDesc += " - " + (StringUtils.isNotBlank(garage)?garage:"ไม่ระบุอู่ซ่อม");
			productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
			productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
			productDesc += " - " + ((StringUtils.isNotBlank(carAct) && (carAct.equals("Yes"))) ? "รวมพรบ." : "ไม่รวมพรบ.");
			productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
			productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
			productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood : "ไม่ระบุ" ); 
			productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
			productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
			productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
			productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
			productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );          
			productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
			productDesc = productDesc.replaceAll("\\s+", " ");                                               
			productDesc = productDesc.replace("( ", "(");                                                    
			productDesc = productDesc.replace(" )", ")");                                                    
			productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
			productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
			productDesc = productDesc.replace(".00", "");
			productDesc = productDesc.replace(",", "");
			
			formatPrice = new DecimalFormat("###");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			productPrice = formatPrice.format(Double.parseDouble(productPrice));
			
			String productUrlForUpdate = uniqueText;
			productUrl = productUrl.replaceAll(" ", "-");
			if(productUrl.startsWith("www")) {
				productUrl = "https://" + productUrl;
			}
			
			String productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5558.jpg";
			
			if(StringUtils.isBlank(productPrice) || StringUtils.isBlank(productUrl)){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
			pdb.setPictureUrl(productImage);
			pdb.setDescription(productDesc);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrlForUpdate);
			pdb.setCategoryId(250101);
			pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		
		return null;
		
	}

	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}

}
