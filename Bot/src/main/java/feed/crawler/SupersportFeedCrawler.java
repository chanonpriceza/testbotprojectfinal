package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.io.CharStreams;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SupersportFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private static boolean haveResult = false;
	
	
	public SupersportFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {"https://www.semtrack.de/e?i=2d7d4cbb53cfb3d4fc07d58ba48305849aed92bc"};
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
				processLog.addException(e.toString()+"-"+this.getClass().getSimpleName(),e);
			}
		}
		mockFeed();
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private void mockFeed() {
		try {

			String url = BaseConfig.FEED_FILE[0];
			String feed = BaseConfig.FEED_STORE_PATH + "/" + StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			File file = new File(feed);
			FileInputStream ins = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(ins,BaseConfig.CONF_PARSER_CHARSET);
			String mock = CharStreams.toString(isr);
			mock = mock.replaceAll(";","\t");
			isr =  new InputStreamReader(new ByteArrayInputStream(mock.getBytes(StandardCharsets.UTF_8)));
			@SuppressWarnings("resource")
			final CSVParser parser = new CSVParser(isr, CSVFormat.TDF.withDelimiter('	').withFirstRecordAsHeader());
			for (final CSVRecord record : parser) {
				if(Thread.currentThread().isInterrupted()) {
					break;
				}
				try {
					parseCSV(record);
				} catch (Exception e) {
					logger.error(e);
					continue;
				}
			}
		
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	static int i = 0;
	public void parseCSV(CSVRecord data) {
		i++;
//				 simple_sku=0
//				 availability_instock=1
//				 brand=2
//				 product_name=3
//				 description=4
//				 price=5
//				 cat_m=6
//				 category_1=7
//				 category_2=8
//				 category_3=9
//				 picture_url=10
//				 deeplink=11
//				 delivery_period=12
		String title = data.get(3).replaceAll("\"","");
		String description = data.get(4).replaceAll("\"","");
		String link = data.get(11).replaceAll("\"","");
		String image_link = data.get(10).replaceAll("\"","");
		String id = data.get(0).replaceAll("\"","");
		String price = data.get(5).replaceAll("\"","");
		String brand = data.get(2).replaceAll("\"","");
		String basePrice = data.get(6).replaceAll("\"","");
		String cat = data.get(7).replaceAll("\"","");
		String cat2 = data.get(8).replaceAll("\"","");
		String mixCat = cat+"|"+cat2;
		String stringId = "";
		
		basePrice = FilterUtil.toPlainTextString(FilterUtil.removeCharNotPrice(basePrice));
		price = FilterUtil.toPlainTextString(FilterUtil.removeCharNotPrice(price));
		if(FilterUtil.convertPriceStr(price) == 0.00) {
			price = basePrice;
		}

		if(StringUtils.isBlank(title)||StringUtils.isBlank(link)||StringUtils.isBlank(id)) 
			logger.info("Data not complete : "+title+" : "+link+" : "+id);
		
		ProductDataBean pdb = new ProductDataBean();
		
		if(StringUtils.isNotBlank(brand)) 
			brand = " "+brand+" ";
		
		stringId = StringUtils.isNotBlank(id)?" ("+id+")":"";
		
		pdb.setName(title.contains(brand)?title+stringId:title+brand+stringId);
		pdb.setUrl(link);
		pdb.setUrlForUpdate(id);
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		
		if(StringUtils.isNotBlank(image_link))
			pdb.setPictureUrl(image_link);
		
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(description);
		pdb.setRealProductId(id);
		
		
		String[] mapping = getCategory(mixCat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else 
			pdb.setCategoryId(0);
		
		mockResult(pdb);
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		String urlForUpdate = pdb.getUrlForUpdate();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) {return null;};
		if(StringUtils.isBlank(name)) {return null;};
		if(price==0){return null;};
		if(expire) {return null;};
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(StringUtils.isNotBlank(urlForUpdate))result.append("<urlForUpdate>"+urlForUpdate+"</urlForUpdate>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	
	
}
