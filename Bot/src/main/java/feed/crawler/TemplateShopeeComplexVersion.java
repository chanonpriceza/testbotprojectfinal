package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class TemplateShopeeComplexVersion extends FeedManager {
	
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36";
	protected static String shopId;
	protected static int fixcat;
	protected static String fixKeyword;
	protected static String etag;
	private static int page = 0;
	private static final int PAGE_OFFSET = 100;
	private static String etag1;
	protected static String GET_PRODUCT_LINK = "https://shopee.co.th/api/v2/item/get?itemid={itemid}&shopid={shopId}";
	protected static JSONParser parser = new JSONParser();
	protected static Map<String,String[]> mapData = new HashMap<String,String[]>();


	public TemplateShopeeComplexVersion() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		shopId = BaseConfig.CONF_FEED_CRAWLER_PARAM;
		String[] inputData = shopId.split(",");
		
		if(inputData.length>=2) {
			shopId = inputData[0];
			fixcat = BotUtil.stringToInt(inputData[1],0);
		}else if(StringUtils.isNotBlank(BaseConfig.CONF_FIX_CATEGORY)) {
			fixcat =  BotUtil.stringToInt(BaseConfig.CONF_FIX_CATEGORY,0);
		}
		
		if (StringUtils.isBlank(shopId)) {
			return;
		}
		
		if(StringUtils.isNotBlank(BaseConfig.CONF_FIX_KEYWORD))
			fixKeyword = BaseConfig.CONF_FIX_KEYWORD;
		
		etag1 = getCookies(shopId);
		if(StringUtils.isBlank(etag1)) return;
		
		getProductWithCat();
		parseProductNormal();
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}
	
	
	
	private void getProductWithCat(){
		boolean nextPage = false;
		int page = 0;
		
		if (!StringUtils.isBlank(shopId)) {
			if(StringUtils.isBlank(etag1)) return;
			
			Set<String> keySet = getCategoryKeySet();
			for (String catId : keySet) {
				page = 0;
				String[] catMap = getCategory(catId);
				if(catMap == null || catMap.length != 2) continue;
				String pzCatId = catMap[0];
				if(!NumberUtils.isCreatable(pzCatId)) continue;
				
				boolean isFirstParamName = true;
				
				do {
					nextPage = false;
					
					if(isFirstParamName) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&shop_categoryids=" + catId;
						
						String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, etag1);
						
						try {
							nextPage = mapCat(data,catMap);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
					}
					
					if(nextPage ==false) {
						String url = "https://shopee.co.th/api/v2/search_items/"
								+ "?by=ctime"
								+ "&order=desc"
								+ "&newest=" + page * PAGE_OFFSET
								+ "&limit="+PAGE_OFFSET
								+ "&skip_price_adjust=false"
								+ "&page_type=shop"
								+ "&match_id=" + shopId
								+ "&original_categoryid=" + catId;
						
						String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, etag1);
						
						try {
							nextPage = mapCat(data,catMap);
						}catch(Exception e) {
							logger.error("Error",e);
						}
						
					}
					
					if(nextPage) page++ ;
					
				}while(!Thread.currentThread().isInterrupted() && nextPage);
				
			}
			
		}else {
			logger.error("Cant't get shop id.");
			return ;
		}
		
	}
	
	private boolean mapCat(String data,String[] cat) {
		boolean isCon = false;
		
		try{
			if(data.contains("\"bundle_deal_id\":,"))
				data = data.replace("\"bundle_deal_id\":,","");
			JSONObject allObj = (JSONObject) parser.parse(data);
			if(allObj==null)
				return false;
			JSONArray jArr = (JSONArray) allObj.get("items");
			if(jArr==null)
				return false;
			
			for (Object object : jArr) {
				JSONObject jObj 		= (JSONObject) object;
				String productId 		= jObj.get("itemid").toString();
				mapData.put(productId, cat);
				isCon= true;
			}
				
		}catch (Exception e) {
			logger.error("Error",e);
			isCon = false;
		}
			
		return isCon;
			
		
	}
	
	
	private String getCookies(String shopId) {
//		String etag1 = null;
//		
//		URL u = null;
//		HttpURLConnection conn = null;
//		int count  = 0;
//		while(count < 3) {
//			try {
//	//			u = new URL("https://shopee.co.th/shop/" + shopId + "/");
//				u = new URL("https://shopee.co.th/shop/"+shopId+"/search");
//				conn = (HttpURLConnection) u.openConnection();
//				conn.setInstanceFollowRedirects(true);
//				conn.setRequestProperty("User-Agent", USER_AGENT);
//				conn.setConnectTimeout(1800000);
//				conn.setReadTimeout(1800000);
//				etag1 = conn.getHeaderField("etag");
//				if(StringUtils.isNotBlank(etag1)) {
//					etag = etag1;
//					System.err.println(etag1);
//					return etag1;
//				}
//				count++;
//			}catch(Exception e) {
//				logger.error("Error",e);
//			}
//		}
		return "55b03-8";
	}
	
	private void parseProductNormal() {
		boolean isContinue = true;
		do {
			String url = "https://shopee.co.th/api/v2/search_items/"
					+ "?by=ctime"
					+ "&order=desc"
					+ "&newest=" + page * PAGE_OFFSET
					+ "&limit="+PAGE_OFFSET
					+ "&skip_price_adjust=false"
					+ "&page_type=shop"
					+ "&match_id=" + shopId;
			String data = httpRequestWithStatusIgnoreCookies(url, page, shopId, etag1);
			if(StringUtils.isBlank(data)){ break;}
			try {
				isContinue = getUrl(data);
			}catch(Exception e) {
				logger.error("Error",e);
			}
			page++ ;
		}while(isContinue||page < 100);
	}
		
		public static String httpRequestWithStatusIgnoreCookies(String url, int page, String shopId,String cookie) {
			HttpURLConnection http = null;
			boolean success = false;
			
	    	try{
				URL u = new URL(url);
				http = (HttpURLConnection) u.openConnection();
				http.addRequestProperty("User-Agent", USER_AGENT);
				http.addRequestProperty("cookie", cookie);
				http.addRequestProperty("referer", "https://shopee.co.th/shop/"+shopId+"/search/?page="+page+"&sortBy=ctime");
				http.addRequestProperty("if-none-match-", etag);
				http.addRequestProperty("pragma", "no-cache");
				
				
	    		int status = http.getResponseCode();
	    		//etag = http.getHeaderField("etag");
	    		if(status == HttpURLConnection.HTTP_OK) {
	    			try(InputStream is = http.getInputStream();
	    				InputStreamReader isr = new InputStreamReader(is,"UTF-8");
		    			BufferedReader brd = new BufferedReader(isr)) {
	    				StringBuilder rtn = new StringBuilder();
	    				String line = null;
	    				while(!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null)
	    					rtn.append(line);
	    				return rtn.toString();
	    			}
	    		}else{
					return null;
				}
	    		
	    	}catch (Exception e) {
				e.printStackTrace();
	    	} finally {
	    		if(!success)
					HTTPUtil.consumeInputStreamQuietly(http.getErrorStream());
				if(http != null)
					http.disconnect();
	    	}
	    	return null;
	    }	
		
	private boolean getUrl(String data) {
		boolean isCon = false;
		
		try{
			if(data.contains("\"bundle_deal_id\":,"))
					data = data.replace("\"bundle_deal_id\":,","");
			
			JSONObject allObj = (JSONObject) parser.parse(data);
			if(allObj==null)
				return false;
			JSONArray jArr = (JSONArray) allObj.get("items");
			if(jArr==null)
				return false;
			
			for (Object object : jArr) {
				JSONObject jObj 		= (JSONObject) object;
				String productId 		= jObj.get("itemid").toString();
				getProductDetail(productId);
				
				isCon= true;
			}
				
		}catch (Exception e) {
			logger.error("Error",e);
			isCon = false;
		}
		
		return isCon;
		
	}
	
	protected void getProductDetail(String productId) throws Exception {
		getProductDetail(productId,null);
	}
	
	protected void getProductDetail(String productId,String cat) throws Exception {
		
		String req = GET_PRODUCT_LINK.replace("{itemid}", productId).replace("{shopId}", shopId);
		String[] response = HTTPUtil.httpRequestWithStatus(req, "UTF-8", true);
		
		if(response==null||response.length==0) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		if(response[0]==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		JSONObject obj = (JSONObject) parser.parse(response[0]);
		
		obj = (JSONObject) obj.get("item");
		
		if(obj==null) {
			logger.info(shopId+","+productId+" ไม่พบข้อมูล");
			return;
		}
		
		String price = String.valueOf(obj.get("price"));
		//String description = String.valueOf(obj.get("description"));
		String basePrice = String.valueOf(obj.get("price_before_discount"));
		String name = String.valueOf(obj.get("name"));
		String productImageUrl =  String.valueOf(obj.get("image"));
		String productUrl 		= "";
		String productNameUrl 	= "";
		double priceDouble = FilterUtil.convertPriceStr(price) / 100000;
		double basePriceDouble = FilterUtil.convertPriceStr(basePrice) / 100000;
		productImageUrl = productImageUrl.replace("&amp;", "&");
		name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim()).trim();
		productNameUrl = name.replaceAll("%", "");
		String linkName = productNameUrl + "-i." + shopId + "." + productId;
		
		productUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
		//name = removeNonUnicodeBMP(StringEscapeUtils.unescapeHtml4(name).trim());
		
		if (StringUtils.isBlank(productUrl) || StringUtils.isBlank(name)) {
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setName(name+" ("+productId+")");
		pdb.setPrice(priceDouble);
		pdb.setBasePrice(basePriceDouble);
		//pdb.setDescription(description);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(shopId + "." + productId);
		pdb.setRealProductId(productId);
		
		if (StringUtils.isNotBlank(productImageUrl)) {
			pdb.setPictureUrl("https://cf.shopee.co.th/file/" + productImageUrl);
		}
		

		String[] mapping = mapData.get(productId);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			if(fixcat!=0) 
				pdb.setCategoryId(fixcat);
			else
			pdb.setCategoryId(0);
		}
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
			}catch(Exception e) {
				logger.error("Error",e);
			}
		}
	
		
		
	}



	protected String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getRealProductId()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	public String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
		
		
}
