package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class AppPerfumeFeedCrawler  extends FeedManager{

	public AppPerfumeFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.app-perfume.com/feed_product_xml.xml"
		};
	}
	
	public ProductDataBean parse(String xml) {
		
		String id  = FilterUtil.getStringBetween(xml,"<id>","</id>").trim();
		String name = FilterUtil.getStringBetween(xml,"<name>","</name>").trim();
		String url = FilterUtil.getStringBetween(xml,"<url>","</url>").trim();
		String price = FilterUtil.getStringBetween(xml,"<price>","</price>").trim();
		String pictureUrl = FilterUtil.getStringBetween(xml,"<image_url>","</image_url>").trim();
		String basePrice = FilterUtil.getStringBetween(xml,"<base_price>","</base_price>");
		String  description = FilterUtil.getStringBetween(xml,"<description>","</description>").trim();
		String catId  = FilterUtil.getStringBetween(xml,"<category_id>","</category_id>").trim();
		
		if(StringUtils.isBlank(id)||StringUtils.isBlank(name)||StringUtils.isBlank(url)||StringUtils.isBlank(price)){
			return null;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		
		pdb.setId(BotUtil.stringToInt(id,0));
		pdb.setName(name);
		pdb.setUrl(url);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		
		if(StringUtils.isNotBlank(pictureUrl)){
			pdb.setPictureUrl(pictureUrl);
		}
		
		if(StringUtils.isNotBlank(basePrice)){
			pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		}
		
		if(StringUtils.isNotBlank(description)){
			pdb.setDescription(description);
		}
		
		String[] catMap = getCategory(catId);
		if (catMap != null) {
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);
		} else {
			pdb.setCategoryId(0);
		}

		return pdb;
			
	}

}





