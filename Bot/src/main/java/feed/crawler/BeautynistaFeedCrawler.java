package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeautynistaFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public BeautynistaFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		boolean haveResult = false;
		int page = 1;
		while(!Thread.currentThread().isInterrupted()){
			String REQUEST_URL = "https://beautynista.com/productfeed/standard/make-up?page="+page+"&record=100";
			
			String[] response = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
			if(response == null || response.length != 2) break;
			
			String data = response[0];
			if (StringUtils.isBlank(data)) { 
				break;
			}
			
			List<String> pdList = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
			if(pdList != null && pdList.size() > 0){
				for (String pd : pdList) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println("<product>"+pd+"</product>");
					}catch(Exception e) {
						logger.error(e);
					}
				}
				haveResult = true;
			}else {
				break;
			}
			page++;
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public ProductDataBean parse(String xml) {

		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();		
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productImageUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>");
		String productCatId = FilterUtil.getStringBetween(xml, "<category_id>", "</category_id>");
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();
		String productId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();	
		
		if(productUrl.length() == 0 || productName.length() == 0 || productPrice.length() == 0 || productId.length() == 0 ) {
			return null;			
		}
				
		ProductDataBean pdb = new ProductDataBean();
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		productUrl = productUrl.replace("&amp;", "&");
		productImageUrl = productImageUrl.replace("&amp;", "&");
		
		if (productDesc.length() != 0) {					
			pdb.setDescription(productDesc);
		}
		
		if (productImageUrl.length() != 0) {
			productImageUrl = productImageUrl.replace("http:", "https:");
			pdb.setPictureUrl(productImageUrl);
		}
		
		String newUrl = productUrl;
		if(!productUrl.startsWith("http")){
			newUrl = "https://www.beautynista.com" + productUrl;
		}
		
		if (productUrl.length() != 0) {					
			pdb.setUrl(newUrl);
			pdb.setUrlForUpdate(productUrl);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(160244);
			pdb.setKeyword("Deal Deals Tawaran");
		}
		
		return pdb;
		
	}
	
}
