package feed.crawler;

import java.io.File;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedAuthenticator;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class BigCOnlineFeedCrawler extends FeedManager {

	public BigCOnlineFeedCrawler() throws Exception {
		super();
		
	}

	@Override
	public String[] setFeedFile() {
		
		return new String[] {"https://backend-api-spo.bigc.co.th/amfeed/main/get/file/facebook_pixel_big_power/?SID=ac7792bd08a22f8b317ccddf218e4f52&___store=thLink"};
	}
	
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = "feed.csv";
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try  {
				URL u = new URL(feed);
				HttpURLConnection http = (HttpURLConnection)u.openConnection();
				http.setRequestProperty("User-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36");
				//http.setRequestProperty("Accept","*/*");
				http.setInstanceFollowRedirects(true);
				http.setRequestProperty("accept-encoding","gzip, deflate, br");
				//http.setRequestProperty("cookie","__cfduid=dcf5970d34806f63afbda3cd1f2db7e5d1574156949; frontend=32aa430b8e80b1c08f8262d7cecc43d1; frontend_cid=fJPwoULcGthkE6FI; __cflb=2563821755");
				http.setRequestProperty("Connection","keep-alive");
				http.setRequestProperty("Cache-Contro","no-cache");
				//logger.info(http.getHeaderFields());
				InputStream in = http.getInputStream();
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (Exception e) {
				logger.error(e);
			}
		}
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/" +"feed.csv"};
	}
	
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String name 		=data.get(1); 
		String id 			=data.get(0); 
		String url			=data.get(6); 
		String image		=data.get(7); 
		String cat 			=data.get(13); 
		String desc 		=data.get(2); 
		String basePrice	=data.get(5); 
		String price 		=data.get(10); 
		price = FilterUtil.removeCharNotPrice(price);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		double price_d = FilterUtil.convertPriceStr(price);
		double base_d = FilterUtil.convertPriceStr(basePrice);
		double temp_price = 0;

		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
		
		String id_temp = (StringUtils.isNotBlank(id))?" ("+id+")":"";
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+id_temp);
		pdb.setRealProductId(id);
		pdb.setUrl(url);
		pdb.setPrice(price_d);
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setDescription(desc);
		pdb.setPictureUrl(image);
		pdb.setUrlForUpdate(id);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		
		
		return pdb;
	}

}
