package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import utils.HTTPUtil;

public class AmazonFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private static final String UTF8_CHARSET = "UTF-8";
	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
	private static final String REQUEST_URI = "/onca/xml";
	private static final String REQUEST_METHOD = "GET";
	private static String endpoint = "webservices.amazon.com"; // must be lowercase
	private static String awsAccessKeyId = "AKIAIDZ3HRRLLHR3VW5A";
	private static String awsSecretKey = "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";
	private static SecretKeySpec secretKeySpec = null;
	private static Mac mac = null;
	private static boolean haveResult = false;
	private static int priceOffset = 450;
	
	private static boolean isKilled = false;
	
	private static int countRequestSuccess = 0;
	private static int countRequestFalse = 0;
	private static int countRequestTotal = 0;
	
	public AmazonFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		startCrawlProduct();
	}
	
	public void startCrawlProduct() {
		
//		String searchIndex 	= "Books";
//		String keyword 		= "Bound Series Paperback";
//		String catId 		= "230204";
//		String keywordPZ 	= "Bound Series Paperback";
//		crawlProduct(searchIndex,keyword,catId,keywordPZ);
		
		for(String cat:getCategoryKeySet()) {
			if(Thread.currentThread().isInterrupted()||isKilled) {
				return;
			}
			String[] splitCatData = cat.split("\\|");
			if(splitCatData==null || splitCatData.length < 2) {
				logger.info("Category can't Split :"+cat);
				continue;
			}
			String browseNode = splitCatData[0];
			String searchIndex  = splitCatData[1];
			if(StringUtils.isBlank(searchIndex)) {
				logger.info("Not Found Search Index :"+cat);
				continue;
			}
			logger.info("Current BrowseNode :"+browseNode);
			String[] priceZaCatId = getCategory(cat);
			for(int i = 0;i<4500;i=i+priceOffset) {
				if(Thread.currentThread().isInterrupted()||isKilled) {
					return;
				}
				crawlProduct(searchIndex,browseNode, priceZaCatId[0], priceZaCatId[1],i,i+priceOffset);
			}
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	private void crawlProduct(String searchIndex, String keyword, String catId, String keywordPZ,int minPrice,int maxPrice){
		try {
			for(int page = 1; page <= 10; page++){
				if(Thread.currentThread().isInterrupted()||isKilled) {
					return;
				}
				logger.info("searchIndex: "+searchIndex+" page: "+page+" min "+minPrice+" maxPrice "+maxPrice);
				boolean isNotPass = true;
				String[] response =  new String[2];
				countRequestTotal++;
				int i = 0;
				while(isNotPass) {
					String url = sign(searchIndex, keyword, page,minPrice,maxPrice);
					response = HTTPUtil.httpRequestWithStatus(url, "UTF-8", true);
					Thread.sleep(1000);
					if(i >= 1) {
						logger.info("OverLimit :"+response[1]+" searchIndex: "+searchIndex+" browseNode: "+keyword+" page: "+page+" minPrice: "+minPrice+" maxPrice: "+maxPrice);
						isNotPass = false;
						countRequestFalse++;
						break;
					}
					if(response == null || response.length != 2 ||response[0]==null) {
						//logger.info("Http Request Error code :"+response[1]+" searchIndex: "+searchIndex+" browseNode: "+keyword+" page: "+page+" minPrice: "+minPrice+" maxPrice: "+maxPrice);
						Thread.sleep(1000);
						i++;
					}else {
						logger.info("Success :"+response[1]+" searchIndex: "+searchIndex+" browseNode: "+keyword+" page: "+page+" minPrice: "+minPrice+" maxPrice: "+maxPrice);
						 isNotPass = false;
						countRequestSuccess++;
					}
					
				}
				logger.info("Request stat Total: "+countRequestTotal+" Success: "+countRequestSuccess+" Fail: "+countRequestFalse);
				String data = response[0];
				if(StringUtils.isNotBlank(data)) {
					List<String> itemList = FilterUtil.getAllStringBetween(data, "<Item>", "</Item>");
					if(itemList != null && itemList.size() > 0){
						for (String itemData : itemList) {
							try {
								parseXML(searchIndex,itemData,catId,keywordPZ);
							} catch (Exception e) {
								logger.error(e);
							}
						}
					}
				}
			}
		}catch (InterruptedException e) {
			logger.error("Kill Command");
			Thread.currentThread().interrupt();
			isKilled = true;
			return;
		}catch (Exception e) {
			logger.error(e);
		}
	}
	
	private void parseXML(String searchIndex, String data, String catId, String keyword){
		if(StringUtils.isBlank(data) || StringUtils.isBlank(catId) || StringUtils.isBlank(keyword)){
			return;
		}
		String name 		= FilterUtil.getStringBetween(data, "<Title>", "</Title>");
		String desc 		= FilterUtil.getStringBetween(data, "<Content>", "</Content>");
		String price 		= FilterUtil.getStringBetween(data, "<ListPrice>", "</ListPrice>");
		if(StringUtils.isBlank(price)) {
			price = 	FilterUtil.getStringBetween(data, "<FormattedPrice>", "</FormattedPrice>");
		}
		String picture 		= FilterUtil.getStringBetween(data, "<LargeImage>", "</LargeImage>");
		String url	 		= FilterUtil.getStringBetween(data, "<DetailPageURL>", "</DetailPageURL>");
		url = url.replaceAll("&amp;","&");
		String isbn 		= FilterUtil.getStringBetween(data, "<ISBN>", "</ISBN>");
		String realProductId = FilterUtil.getStringBetween(data,"<ASIN>","</ASIN>");
		
		if(StringUtils.isBlank(name) || StringUtils.isBlank(price) || StringUtils.isBlank(url)){
			logger.info("Data not Complete detail name:"+name +" price: "+price+" url: "+url+" at "+searchIndex);
			return;
		}
		
		if(searchIndex.equals("Books")){
			name = name + " (" + FilterUtil.toPlainTextString(isbn) + ")" ;
		}
		if(price.contains("<FormattedPrice>"))
		price = FilterUtil.getStringBetween(price, "<FormattedPrice>", "</FormattedPrice>");
		
		price = FilterUtil.toPlainTextString(price);
		price = FilterUtil.removeCharNotPrice(price);
		
		picture = FilterUtil.getStringBetween(picture, "<URL>","</URL>");
		
		ProductDataBean pdb = new ProductDataBean();
		String productName = FilterUtil.toPlainTextString(StringEscapeUtils.unescapeHtml4(name));
		if(StringUtils.isNotBlank(realProductId)) {
			productName += " ("+realProductId+")";
		}
		pdb.setName(productName);
		pdb.setRealProductId(realProductId);
		pdb.setUrlForUpdate(realProductId);
		pdb.setDescription(FilterUtil.toPlainTextString(StringEscapeUtils.unescapeHtml4(desc)));
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setPictureUrl(FilterUtil.toPlainTextString(picture));
		pdb.setUrl(FilterUtil.toPlainTextString(url));
		pdb.setUrlForUpdate(realProductId);
		pdb.setCategoryId(Integer.parseInt(catId));
		pdb.setKeyword(keyword);
		
		
		String result = mockResult(pdb);
		if(StringUtils.isNotBlank(result)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
				pwr.println(result);
				haveResult = true;
			}catch(Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	public void SignedRequestsHelper()
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] secretyKeyBytes = awsSecretKey.getBytes(UTF8_CHARSET);
		secretKeySpec = new SecretKeySpec(secretyKeyBytes, HMAC_SHA256_ALGORITHM);
		mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(secretKeySpec);
	}

	public String sign(String searchIndex, String keyword, int page,int minPrice,int maxPrice)throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		SignedRequestsHelper();
		
		Map<String, String> params = new HashMap<String, String>();
		
		//String paramss = "&Operation=ItemSearch&Keywords=the%20hunger%20games";
		params.put("AWSAccessKeyId", awsAccessKeyId);
		params.put("AssociateTag", "priceza09-20");
		//params.put("Operation", "BrowseNodeLookup");
		//params.put("BrowseNodeId", "20");
		//params.put("IdType", "SKU");
		
		//params.put("ResponseGroup", "Images,ItemAttributes,Offers,RelatedItems");
		params.put("Operation", "ItemSearch");
		params.put("BrowseNode",keyword);
		//params.put("Condition", "All");
		params.put("ResponseGroup", "Medium,ItemAttributes");
		params.put("SearchIndex",searchIndex);
		//params.put("Keywords", keyword);
		//params.put("VariationPage", "1");
		
		params.put("MaximumPrice",String.valueOf(maxPrice));
		params.put("MinimumPrice",String.valueOf(minPrice));
		
		params.put("ItemPage", Integer.toString(page));
		params.put("RelatedItemPage", Integer.toString(120));
		params.put("Availability", "Available");
		params.put("Version", "2013-08-01");

		//params.put("Availability", "Available");
		//params.put("Title", "Harry%20Potter");
		params.put("Timestamp", timestamp());

		SortedMap<String, String> sortedParamMap = new TreeMap<String, String>(params);
		String canonicalQS = canonicalize(sortedParamMap);
		String toSign = REQUEST_METHOD + "\n" + endpoint + "\n" + REQUEST_URI + "\n" + canonicalQS;

		String hmac = hmac(toSign);
		String sig = percentEncodeRfc3986(hmac);
		String url = "http://" + endpoint + REQUEST_URI + "?" + canonicalQS + "&Signature=" + sig;

		return url;
	}

	private String hmac(String stringToSign) {
		String signature = null;
		byte[] data;
		byte[] rawHmac;
		try {
			data = stringToSign.getBytes(UTF8_CHARSET);
			rawHmac = mac.doFinal(data);
			signature = new String(Base64.encodeBase64(rawHmac));
		} catch (UnsupportedEncodingException e) {throw new RuntimeException(UTF8_CHARSET + " is unsupported!", e);
		}
		return signature;
	}

	private String timestamp() {
		String timestamp = null;
		Calendar cal = Calendar.getInstance();

		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
		timestamp = dfm.format(cal.getTime());
		return timestamp;
	}

	private String canonicalize(SortedMap<String, String> sortedParamMap) {
		if (sortedParamMap.isEmpty()) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		Iterator<Map.Entry<String, String>> iter = sortedParamMap.entrySet().iterator();
		while (!Thread.currentThread().isInterrupted() && iter.hasNext()) {
			Map.Entry<String, String> kvpair = iter.next();
			buffer.append(percentEncodeRfc3986(kvpair.getKey()));
			buffer.append("=");
			buffer.append(percentEncodeRfc3986(kvpair.getValue()));
			if (iter.hasNext()) {
				buffer.append("&");
			}
		}
		String canonical = buffer.toString();
		return canonical;
	}

	private String percentEncodeRfc3986(String s) {
		String out;
		try {
			out = URLEncoder.encode(s, UTF8_CHARSET).replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
		} catch (UnsupportedEncodingException e) {
			out = s;
		}
		return out;
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	

	
//	private void calculateBrowseNode(String response,String k){
//		List<String> cut = FilterUtil.getAllStringBetween(response,"<Name>","</Name>");
//		if(cut==null||cut.size() ==0) {
//			System.err.println("err "+response);
//			System.out.println("catId :"+k);
//			logger.info("catId :"+k);
//			return;
//		}else {
//			//System.out.println("catId:	"+k+"	CatName:	"+(cut.get(cut.size()-1)).replace("&amp;","&"));
//			System.out.println(k);
//			logger.info("catId :"+k+" CatName: "+cut.get(cut.size()-1));
//		}
//		
//	}
}
