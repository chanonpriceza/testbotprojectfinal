package feed.crawler;

import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SurajitFeedCrawler extends FeedManager{

	public SurajitFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return new String[] {
				"https://www.surajit.co.th/vmart/datafeed/getdatafeed.php?priceza=y&product=y"
		};
	}

	public ProductDataBean parse(String xml){
		
		String productName = FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();//
		String productPrice = FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();//
		String basePrice = FilterUtil.getStringBetween(xml, "<base_price>", "</base_price>").trim();//
		String productUrl = FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();//
		String productDesc = FilterUtil.getStringBetween(xml, "<description>", "</description>").trim();//
		String productPictureUrl = FilterUtil.getStringBetween(xml, "<image_url>", "</image_url>").trim();//
		String realProductId = FilterUtil.getStringBetween(xml, "<id>", "</id>").trim();//
		String productCatId = FilterUtil.getStringBetween(xml, "<category_name>", "</category_name>").trim();//

		if(StringUtils.isBlank(productUrl) || StringUtils.isBlank(productName) 
				|| StringUtils.isBlank(productPrice) || StringUtils.isBlank(realProductId)) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();
		productName += " (" + realProductId + ")";
		pdb.setName(productName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(realProductId);
		
		if(StringUtils.isNotBlank(basePrice)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice)));
		}
		
		if (StringUtils.isNotBlank(productPictureUrl)) {	
			pdb.setPictureUrl(productPictureUrl);
		}
		
		if(StringUtils.isNotBlank(realProductId)){
			pdb.setRealProductId(realProductId);
		}
		
		if(StringUtils.isNotBlank(productDesc)){
			pdb.setDescription(productDesc);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}

		return pdb;
	}
	
}
