package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;

public class SupersportApiFeedCrawler extends FeedManager {
	
	private static String mainUrl = "https://www.supersports.co.th/_c/rpc?&req={\"method\":\"Costa.ListCatalogProducts\",\"params\":[{\"category_id\":[\"{catId}\"],\"limit\":48,\"offset\":{offset},\"segment\":\"shop\",\"dir\":\"desc\",\"sort\":\"popularity\",\"catalog_type\":\"\",\"url_key\":\"/shop/running/running-shoes\"}]}&lang=th";
	private static JSONParser parser = new JSONParser();
	
	
	public SupersportApiFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		try {
		for(String cat:getCategoryKeySet()) {
			for(int i = 0;i<100;i++) {
				InputStream in = new URL(mainUrl.replace("{catId}", cat).replace("{offset}",String.valueOf(i*48))).openStream() ;
				InputStreamReader reader = new InputStreamReader(in);
				JSONObject mainObj = (JSONObject) parser.parse(reader);
				mainObj = (JSONObject) mainObj.get("result");
				mainObj = (JSONObject) mainObj.get("response");
				JSONArray docs = (JSONArray) mainObj.get("docs");
				if(docs==null||docs.size()==0) {
					break;
				}
				JSONArray array = new JSONArray();
				for(Object d:docs) {
					JSONObject o = (JSONObject)d;
					JSONObject result = new JSONObject();					
					JSONObject meta = (JSONObject)o.get("meta");
					result.put("name",String.valueOf(meta.get("name")));
					result.put("id",String.valueOf(meta.get("sku")));
					result.put("price",String.valueOf(meta.get("special_price")));
					result.put("basePrice",String.valueOf(meta.get("price")));
					result.put("color",String.valueOf(meta.get("color_family")));
					result.put("url",String.valueOf(o.get("link")));
					result.put("image",String.valueOf(o.get("image")));
					result.put("brand",String.valueOf(meta.get("brand")));
					result.put("cat",cat);
					array.add(result);
					
				}
				InputStream is = new ByteArrayInputStream(array.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+cat+"-"+BaseConfig.MERCHANT_ID+"-"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
			
		}
		BaseConfig.FEED_FILE = (new File(BaseConfig.FEED_STORE_PATH)).list();
		}catch (Exception e) {
			logger.error("Error",e);
		}
	}
	
	@Override
	public ProductDataBean parse(JSONObject data) {
		
		String  name = String.valueOf(data.get("name"));
		String  id = String.valueOf(data.get("id"));
		String  price = String.valueOf(data.get("price"));
		String  basePrice = String.valueOf(data.get("basePrice"));
		String  color = String.valueOf(data.get("color"));
		String  url = String.valueOf(data.get("url"));
		String  image = String.valueOf(data.get("image"));
		String  cat = String.valueOf(data.get("cat"));
		price =FilterUtil.removeCharNotPrice(price);
		basePrice =FilterUtil.removeCharNotPrice(basePrice);
		
		double priceReal = FilterUtil.convertPriceStr(price); //0
		double baseReal = FilterUtil.convertPriceStr(basePrice);//100
		
		if(priceReal==0) {
			double tmp = priceReal;
			priceReal = baseReal;
			baseReal = tmp;
		}
		
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+" สี"+color+" ("+id+")");
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setUrl("https://www.supersports.co.th/"+BotUtil.decodeURL(url));
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setPictureUrl(image);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
				
				
		return pdb;
	}

}
