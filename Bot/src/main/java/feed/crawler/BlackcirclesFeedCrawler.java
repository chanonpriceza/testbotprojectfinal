package feed.crawler;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class BlackcirclesFeedCrawler extends FeedManager{
	
	private boolean haveResult;
    private boolean first = true;
    
	private static final Logger logger = LogManager.getRootLogger();
	
	public BlackcirclesFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {

	feedRequest("http://export.beezup.com/BlackCircles_Thailande_TH/Google_Shopping_CHN/dfefcc74-db1b-5481-b1d3-793a2552b709", "UTF-8", false);
	if(haveResult)
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	

	
	private void feedRequest(String url, String charset, boolean redirectEnable) {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);

			if(conn.getResponseCode() == 200) {
    			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr);) {
		    		
		    		String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						processLine(line);
						
						if (Thread.currentThread().isInterrupted()) {
							throw new InterruptedException();
						}
					}
					return ;
		    	}catch(IOException e){
		    		HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				logger.info("Error status : " + conn.getResponseCode() + " - " + url);
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return ;
	}
	
	public void processLine(String line) {
		if(first) {
			first = false;
		}else {
			if(first){
				first = false;
			}else {
				parseCSV(line);
			}
		}
	}
	
	private void parseCSV(String data) {
		List<String> parseTabObj = Arrays.asList(data.split("\t"));
		
		String id = parseTabObj.get(0).trim();
		String name = parseTabObj.get(1).trim();
		String desc = parseTabObj.get(2).trim();
		String price = parseTabObj.get(3).trim();
		String url  = parseTabObj.get(4).trim();
		String category = parseTabObj.get(6).trim();
		String basePrice = parseTabObj.get(32).trim();
		String image = parseTabObj.get(8).trim();
		String expire = parseTabObj.get(24).trim();
		
		if("out of stock".equals(expire)){
			return;
		}
		
		if( StringUtils.isBlank(name) || StringUtils.isBlank(category) || StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
			return;
		}
		
		ProductDataBean pdb = new ProductDataBean();
		name = "ยางรถ "+name;
		pdb.setName(name);	
		pdb.setPrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price))));
		pdb.setUrl(url);
		pdb.setRealProductId(id);
		
		if(!image.isEmpty()){
			pdb.setPictureUrl(image);
		}
		
		if(StringUtils.isNotBlank(basePrice) && !basePrice.equals(price)) {
			pdb.setBasePrice(Math.ceil(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(basePrice))));
		}
		
		if(StringUtils.isNotBlank(desc)&&!"NC".equals(desc)){
			pdb.setDescription(desc);
		}
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
		}
		mockResult(pdb);
		
	}
	
	
	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String mockResult(ProductDataBean pdb){
		
		String id = pdb.getRealProductId();
		String name = pdb.getName();
		String url = pdb.getUrl();
		String image = pdb.getPictureUrl();
		double price = pdb.getPrice();
		String desc = pdb.getDescription();
		int cat = pdb.getCategoryId();
		double basePrice = pdb.getBasePrice();
		boolean expire = pdb.isExpire();
		String keyword = pdb.getKeyword();
		
		desc = FilterUtil.toPlainTextString(desc);

		if(StringUtils.isBlank(url)) return null;
		if(StringUtils.isBlank(name)) return null;
		if(price==0) return null;
		if(expire) return null;
		
		StringBuilder result = new StringBuilder();
		result.append("<product>");
		if(StringUtils.isNotBlank(name)) result.append("<name>"+name+"</name>");
		if(StringUtils.isNotBlank(desc)) result.append("<desc>"+desc+"</desc>");
		if(price!=0) result.append("<price>"+price+"</price>");
		if(basePrice!=0) result.append("<basePrice>"+basePrice+"</basePrice>");
		if(StringUtils.isNotBlank(image)) result.append("<pictureUrl>"+image+"</pictureUrl>");
		if(StringUtils.isNotBlank(url)) result.append("<url>"+url+"</url>");
		if(StringUtils.isNotBlank(id)) result.append("<realProductId>"+id+"</realProductId>");
		if(cat!=0) result.append("<categoryId>"+cat+"</categoryId>");
		if(StringUtils.isNotBlank(keyword)) result.append("<keyword>"+keyword+"</keyword>");
		if(cat!=0) result.append("<cat>"+cat+"</cat>");
		result.append("</product>");
		
		if(StringUtils.isNotBlank(result)) {
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/" + "mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		return result.toString();
		
	}
}

