package feed.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ToHomeFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	private boolean haveResult = false;
	
	public ToHomeFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	private int page = 1;
	private int errorRequestCount = 0;
	
	
	@Override
	public void loadFeed() {
		while(!Thread.currentThread().isInterrupted()){
			
			if(page >= 1000){
				break;
			}
			
			if(errorRequestCount >= 3) {
				break;
			}
			
			String[] response = httpRequestWithStatus("http://www.tohome.com/json_data/productlist.aspx?page=" + page, "UTF-8", false);
			if(response == null || response.length != 2) {
				logger.info("Skip tohome page : " + page);
				errorRequestCount++;
				page++;
				continue;
			}
			
			boolean success = false;
			String data = response[0];
			if(StringUtils.isNotBlank(data)){
				try {
					logger.info("Request success  : " + page);
					success = parseHtml(data);
				} catch (Exception e) {
					logger.info("Skip tohome page : " + page);
					errorRequestCount++; 
					logger.error(e);
				}
			}else {
				logger.info("Skip tohome page : " + page);
				errorRequestCount++; 
			}
			
			if(!success) {
				logger.info("Skip nodata page : " + page);
				errorRequestCount++; 
			}
			
			page++;
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	private boolean parseHtml(String data) throws UnsupportedEncodingException, ClassNotFoundException, SQLException, ParseException {
		
		boolean success = false;
		List<String> jarr = FilterUtil.getAllStringBetween(data, "{", "}");
		if(jarr!=null && jarr.size() > 0) {
			for(String object : jarr) {
				object = "{" + object + "}";
				try {
					
					JSONObject jObj = (JSONObject) new JSONParser().parse(object);
					String productName = (String)jObj.get("name");		
					String productPrice = (String)jObj.get("price");
					String productUrl = (String)jObj.get("url");
					String productImageUrl = (String)jObj.get("image_url");
					String productCatId = (String)jObj.get("category_id");
					String productDesc = (String)jObj.get("description");
					
					productUrl = productUrl.replace("&amp;", "&");
					productImageUrl = productImageUrl.replace("&amp;", "&");
					if(productUrl.trim().length() == 0 || productName.trim().length() == 0 || productPrice.trim().length() == 0 ) {
						continue;				
					}		
					
					ProductDataBean pdb = new ProductDataBean();	
					
					productName = StringEscapeUtils.unescapeHtml4(productName).trim();
					pdb.setName(productName);
					pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
					
					if (productImageUrl.length() != 0) {
						pdb.setPictureUrl(productImageUrl);
					}
					if (productUrl.length() != 0) {					
						pdb.setUrl(productUrl);
						pdb.setUrlForUpdate(productUrl);
					}
					
					productDesc = FilterUtil.toPlainTextString(productDesc);
					pdb.setDescription(productDesc);
					
					String[] catMap = getCategory(productCatId);
					if(catMap != null) {			
						pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
						pdb.setKeyword(catMap[1]);
					}else{
						pdb.setCategoryId(0);
					}
			
					errorRequestCount = 0;
					
					String result = mockResult(pdb);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
							pwr.println(result);
							haveResult = true;
							success = true;
						}catch(Exception e) {
							logger.error(e);
						}
					}
					
				}catch(Exception e) {
				}
			}
		}
		return success;
	}
	

	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
}
