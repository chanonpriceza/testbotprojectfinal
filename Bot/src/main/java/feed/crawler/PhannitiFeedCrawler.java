package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PhannitiFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public PhannitiFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	String[][] PRODUCT = {
			// name, price, description, picture, url, category, keyword 
			{"Taifu (DC PUMP) 80 วัตต์", "0", "80 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 120 วัตต์", "0", "120 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 210 วัตต์", "28005", "210 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 500 วัตต์", "34960", "500 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 1000 วัตต์", "40787", "1000 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 1500 วัตต์", "44119", "1500 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 2200 วัตต์", "60571", "2200 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"Taifu (DC PUMP) 3000 วัตต์", "65907", "3000 วัตต์", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus1090618736770.jpg", "http://phanniti.com/Taifu_(DC_PUMP)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec515a1130c40001dc7a3a/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-10-12", "4781", "12 ใบพัด ท่อส่ง 1-1/4” นิ้ว 1 แรงม้า ปริมาณน้ำ 1.5-3.0 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-25-09", "3770", "9 ใบพัด ท่อส่ง 1-1/2” นิ้ว 1 แรงม้า ปริมาณน้ำ 3-6 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-25-12", "7893", "12 ใบพัด ท่อส่ง 1-1/2” นิ้ว 1.5 แรงม้า ปริมาณน้ำ 3-6 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-35-09", "17893", "9 ใบพัด ท่อส่ง 2” นิ้ว 1.5 แรงม้า ปริมาณน้ำ 6.0-8.0 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-55-07", "19682", "7 ใบพัด ท่อส่ง 2” นิ้ว 1.5 แรงม้า ปริมาณน้ำ 6.0-12 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-10-17", "22560", "17 ใบพัด ท่อส่ง 1-1/2” นิ้ว 2 แรงม้า ปริมาณน้ำ 1.5-3.0 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-25-15", "22560", "15 ใบพัด ท่อส่ง 1-1/2” นิ้ว 2 แรงม้า ปริมาณน้ำ 3-6 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-15-17", "22560", "17 ใบพัด ท่อส่ง 1-1/2” นิ้ว 2 แรงม้า ปริมาณน้ำ 3-6 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-35-13", "22560", "13 ใบพัด ท่อส่ง 2” นิ้ว 2 แรงม้า ปริมาณน้ำ 6.0-8.0 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-55-10", "22560", "10 ใบพัด ท่อส่ง 2” นิ้ว 2 แรงม้า ปริมาณน้ำ 6.0-12 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-60-07", "22560", "7 ใบพัด ท่อส่ง 2” นิ้ว 2 แรงม้า ปริมาณน้ำ 8.0-14 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-80-06", "22560", "6 ใบพัด ท่อส่ง 2” นิ้ว 2 แรงม้า ปริมาณน้ำ 9-16 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-35-18", "25750", "18 ใบพัด ท่อส่ง 2” นิ้ว 3 แรงม้า ปริมาณน้ำ 6.0-8.0 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-80-08", "25672", "8 ใบพัด ท่อส่ง 2” นิ้ว 3 แรงม้า ปริมาณน้ำ 9-24 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-60-10", "26450", "10 ใบพัด ท่อส่ง 2” นิ้ว 3 แรงม้า ปริมาณน้ำ 6.0-16 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) SP-300-2", "33451", "2 ใบพัด ท่อส่ง 3” นิ้ว 3 แรงม้า ปริมาณน้ำ 18-36 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-60-14", "36019", "14 ใบพัด ท่อส่ง 2” นิ้ว 5 แรงม้า ปริมาณน้ำ 6.0-16 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-80-11", "35785", "11 ใบพัด ท่อส่ง 2” นิ้ว 5 แรงม้า ปริมาณน้ำ 9-24 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) ST-80-13", "37321", "13 ใบพัด ท่อส่ง 2” นิ้ว 5 แรงม้า ปริมาณน้ำ 9-24 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) SP-300-3", "46326", "3 ใบพัด ท่อส่ง 3” นิ้ว 5 แรงม้า ปริมาณน้ำ 18-36 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) SP-300-4", "49438", "4 ใบพัด ท่อส่ง 3” นิ้ว 5 แรงม้า ปริมาณน้ำ 18-36 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ProEn SUBMERSIBLE AC PUMP(ปั๊มน้ำบาดาล ProEn AC) SP-46-02", "46326", "2 ใบพัด ท่อส่ง 4” นิ้ว 5 แรงม้า ปริมาณน้ำ 24-56 คิว/ชั่วโมง", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus103191531267.jpg", "http://phanniti.com/ProEn_SUBMERSIBLE_AC_PUMP(%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9A%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A5_ProEn_AC)/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4fd71130c40001dc7928/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			
			{"ปั๊มหอยโข่ง WCH1505T", "9452", "ปั๊ม 1.5 ท่อส่ง 1 1/4x1 นิ้ว 2 แรงม้า ระยะส่ง 48.3-21.9 เมตร ปริมาณน้ำ 0-140 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCH2205T", "10568", "ปั๊ม 2.2 ท่อส่ง 2 1/4x1 นิ้ว 3 แรงม้า ระยะส่ง 56.4-21.2 เมตร ปริมาณน้ำ 0-160 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCH3705T", "13037", "ปั๊ม 5 ท่อส่ง 3 1/2x1 นิ้ว 5 แรงม้า ระยะส่ง 52.5-21.4 เมตร ปริมาณน้ำ 0-220 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCM1505T", "9438", "ปั๊ม 1.5 ท่อส่ง 2x2 นิ้ว 2 แรงม้า ระยะส่ง 26.4-10.7 เมตร ปริมาณน้ำ 0-500 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCM2205T", "10568", "ปั๊ม 2.2 ท่อส่ง 2x2 นิ้ว 3 แรงม้า ระยะส่ง 32-15.5 เมตร ปริมาณน้ำ 0-500 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCM3705FT", "14922", "ปั๊ม 5 ท่อส่ง 3.7 นิ้ว 5 แรงม้า ระยะส่ง 43-23.7 เมตร ปริมาณน้ำ 0-500 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCM5505FT", "26008", "ปั๊ม 5.5 ท่อส่ง 5.5 นิ้ว 7.5 แรงม้า ระยะส่ง 58-34.8 เมตร ปริมาณน้ำ 0-550 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCM7505FT", "29509", "ปั๊ม 7.5 ท่อส่ง 7.5 นิ้ว 10 แรงม้า ระยะส่ง 66.5-44 เมตร ปริมาณน้ำ 0-550 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCL1505T", "9951", "ปั๊ม 1.5 ท่อส่ง 3x3 นิ้ว 2 แรงม้า ระยะส่ง 17-7.7 เมตร ปริมาณน้ำ 0-1100 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCL2205T", "11338", "ปั๊ม 2.2 ท่อส่ง 3x3 นิ้ว 3 แรงม้า ระยะส่ง 21.2-8.9 เมตร ปริมาณน้ำ 0-1100 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCL3705FT", "15669", "ปั๊ม 5 ท่อส่ง 2 1/2x2 นิ้ว 5 แรงม้า ระยะส่ง 28-18 เมตร ปริมาณน้ำ 0-900 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCL5505FT", "26904", "ปั๊ม 5.5 ท่อส่ง 3 1/2x2 นิ้ว 7.5 แรงม้า ระยะส่ง 34-25 เมตร ปริมาณน้ำ 0-1100 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			{"ปั๊มหอยโข่ง WCL7505FT", "30340", "ปั๊ม 7.5 ท่อส่ง 4 1/2x2 นิ้ว 10 แรงม้า ระยะส่ง 42.6-29.5 เมตร ปริมาณน้ำ 0-1200 ลิตร/นาที ", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus682777199540.jpg", "http://phanniti.com/%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%A1%E0%B8%AB%E0%B8%AD%E0%B8%A2%E0%B9%82%E0%B8%82%E0%B9%88%E0%B8%87/58eb568c0a431a0001604e2e/58eb500d69649b0001a433d7/Products_58ec4b6969649b0001a439bb/58ec56c91130c40001dc7ca8/Master_58d4ce000a431a00015ec65f", "50523", "เครื่องปั้มน้ำ Pump  Solarpump"},
			
			
			{"PROJOY 1 แรงม้า", "13500", "1 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus738725340331.jpg", "http://phanniti.com/PROJOY/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec55880a431a0001605b7b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"PROJOY 1.5 แรงม้า", "14500", "1.5 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus738725340331.jpg", "http://phanniti.com/PROJOY/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec55880a431a0001605b7b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"PROJOY 2 แรงม้า", "15500", "2 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus738725340331.jpg", "http://phanniti.com/PROJOY/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec55880a431a0001605b7b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"PROJOY 3 แรงม้า", "16000", "3 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus738725340331.jpg", "http://phanniti.com/PROJOY/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec55880a431a0001605b7b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			
			{"Goodrive100-PV Pump Inverter GD100-0R4G-SS25-PV-AS", "15500", "Power 0.4 Kw 0.5 Hp Current 4.2 A", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus295714166241.jpg", "http://phanniti.com/Goodrive100-PV_Pump_Inverter/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec54b60a431a0001605b0b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"Goodrive100-PV Pump Inverter GD100-0R7G-SS25-PV-AS", "15960", "Power 0.75 Kw 1 Hp Current 7.2 A", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus295714166241.jpg", "http://phanniti.com/Goodrive100-PV_Pump_Inverter/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec54b60a431a0001605b0b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"Goodrive100-PV Pump Inverter GD100-1R5G-SS25-PV-AS", "16337", "Power 1.5 Kw 2 Hp Current 10.2 A", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus295714166241.jpg", "http://phanniti.com/Goodrive100-PV_Pump_Inverter/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec54b60a431a0001605b0b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"Goodrive100-PV Pump Inverter GD100-2R2G-SS25-PV-AS", "18671", "Power 2.2 Kw 3 Hp Current 14 A", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus295714166241.jpg", "http://phanniti.com/Goodrive100-PV_Pump_Inverter/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec54b60a431a0001605b0b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"Goodrive100-PV Pump Inverter GD100-004G-SS25-PV-AS", "21782", "Power 4 Kw 5 Hp Current 25 A", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus295714166241.jpg", "http://phanniti.com/Goodrive100-PV_Pump_Inverter/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec54b60a431a0001605b0b/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			
			{"ProEn BPD 1.5 แรงม้า", "23500", "รุ่น  1.5 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"ProEn BPD 2 แรงม้า", "23500", "รุ่น  2 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"ProEn BPD 3 แรงม้า", "24700", "รุ่น  3 แรงม้า", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"ProEn BPD Single Phase 3KW With DC", "49034", "รุ่น MG3KTL Single Phase 3KW With DC", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"ProEn BPD Three Phase 6KW With DC", "106962", "รุ่น BG6KTR Three Phase 6KW With DC", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			{"ProEn BPD Three Phase 10KW With DC", "133402", "รุ่น BG10KTR Three Phase 10KW With DC", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus898833372200.jpg", "http://phanniti.com/ProEn_BPD/58eb568c0a431a0001604e2e/58eb50181130c40001dc6b3c/Products_58ec52de1130c40001dc7b0a/58ec57210a431a0001605c0d/Master_58d4ce000a431a00015ec65f", "51309", "ตัวแปลงไฟรถเป็นไฟบ้าน Inverter"},
			
			
//			{"", "", "", "", "", "51310", "โซล่าเซลล์และอุปกรณ์ Solar panal"},
//			{"", "", "", "", "", "50201", "เครื่องทำน้ำร้อนพลังงานแสงอาทิตย์ Solar hot water heater"},
			
			
			{"Street light SW-ISL801-50W-A", "41482.4", "Model 100W/18V Battery Model 25.6V20Ah Control Model 10A LED 50W ความสูง 8M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL801-50W-B", "37467.98", "Model 100W/18V Battery Model 25.6V15Ah Control Model 10A LED 50W ความสูง 8M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL701-40W-A", "35203.43", "Model 80W/18V Battery Model 25.6V15Ah Control Model 10A LED 40W ความสูง 7M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL701-40W-B", "32733.01", "Model 80W/18V Battery Model 25.6V12Ah Control Model 10A LED 40W ความสูง 7M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL601-30W-A", "29027.39", "Model 60W/18V Battery Model 12.8V24Ah Control Model 5A LED 30W ความสูง 6M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL601-30W-B", "26659.91", "Model 60W/18V Battery Model 12.8V18Ah Control Model 5A LED 30W ความสูง 6M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL501-20W-A", "24292.42", "Model 40W/18V Battery Model 12.8V12Ah Control Model 5A LED 20W ความสูง 5M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL501-20W-B", "21822.01", "Model 40W/18V Battery Model 12.8V12Ah Control Model 5A LED 20W ความสูง 5M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL401-15W-A", "17189.98", "Model 30W/18V Battery Model 12.8V9Ah Control Model 5A LED 15W ความสูง 4M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL401-15W-B", "15954.77", "Model 30W/18V Battery Model 12.8V9Ah Control Model 5A LED 15W ความสูง 4M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light sW-ISL301-10W-A", "13175.55", "Model 20W/18V Battery Model 12.8V9Ah Control Model 5A LED 10W ความสูง 3M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL301-10W-B", "11940.34", "Model 20W/18V Battery Model 12.8V6Ah Control Model 5A LED 10W ความสูง 3M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL301-5W-A", "10190.47", "Model 16W/18V Battery Model 12.8V6Ah Control Model 5A LED 5W ความสูง 3M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},
			{"Street light SW-ISL301-5W-B", "8955.26", "Model 16W/18V Battery Model 12.8V3Ah Control Model 5A LED 5W ความสูง 3M", "http://itp1.itopfile.com/ImageServer/2d937427a395b344/0/0/iTopPlus428813749644.jpg", "http://phanniti.com/Street_light/58eb568c0a431a0001604e2e/58eb502969649b0001a433e3/Products_58eb55db0a431a0001604de0/58eb568b0a431a0001604e24/Master_58d4ce000a431a00015ec65f", "51310", "ไฟถนนโซล่าเซลล์ Street light"},

			
//			{"", "", "", "", "", "180310", "ระบบน้ำ Water system"},
//			{"", "", "", "", "", "50201", "ปั้มความร้อน Solar hybrid system Heat pump"},
//			{"", "", "", "", "", "180310", "โรงเรือนเพาะปลูกพืช Green house"},
//			{"", "", "", "", "", "180310", "ระบบให้น้ำพืช Irrigation system"},
		};

	
	@Override
	public void loadFeed() {

		for(int i=0; i<PRODUCT.length; i++){
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName(PRODUCT[i][0]);
			pdb.setPrice(FilterUtil.convertPriceStr(PRODUCT[i][1]));
			pdb.setDescription(PRODUCT[i][2]);
			pdb.setPictureUrl(PRODUCT[i][3]);
			pdb.setUrl(PRODUCT[i][4]);
			pdb.setUrlForUpdate(toMD5(PRODUCT[i][0]));
			pdb.setCategoryId(Integer.parseInt(PRODUCT[i][5]));
			pdb.setKeyword(PRODUCT[i][6]);
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
				}catch(Exception e) {
					logger.error(e);
				}
			}
		}
		
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
		
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
}
