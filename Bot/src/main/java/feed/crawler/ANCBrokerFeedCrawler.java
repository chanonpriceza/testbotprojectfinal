package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;
import utils.HTTPUtil;

public class ANCBrokerFeedCrawler extends FeedManager {

	private static final Logger logger = LogManager.getRootLogger();

	private static final int maxConcurrent = 6;
	private static boolean haveResult = false;
	
	private static final String[] blockPackage = {
			"ชับบ์ ป1 OROP 2019",
	};
	
	public ANCBrokerFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {

		String[] allBrandResult = HTTPUtil.httpRequestWithStatus("http://getprice.anc-service.com/x_Api/resultFrom.php?user=priceza&pass=pri@pa88&gettype=brand", "UTF-8", false);
		if(allBrandResult == null || allBrandResult.length != 2) return;
		
		String allBrand = allBrandResult[0];
		if(StringUtils.isBlank(allBrand)) return;
		List<String> allBrandList = FilterUtil.getAllStringBetween(allBrand, "value=\"", "\"");

		logger.info("Main starting. allBrandList = " + allBrandList.size());
		
		if (allBrandList.isEmpty()) {
			return;
		}

		try {
			
			ExecutorService executor = Executors.newFixedThreadPool(maxConcurrent);
			
			for (String brand : allBrandList) {

				if (StringUtils.isBlank(brand)) continue;
				
				ancLoadFeedThread t = new ancLoadFeedThread(brand);
				executor.execute(t);	

			}
			
			executor.shutdown();
			if(executor.awaitTermination(120, TimeUnit.HOURS)){
				logger.info("Done All.");
			}else{
				logger.info("Time out.");
				executor.shutdownNow();
			}
			
		} catch (InterruptedException e) {
			logger.error(e);
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
	private class ancLoadFeedThread implements Runnable {

		private int count;
		private int successCount;
		private String targetBrand;

		ancLoadFeedThread(String brand) {
			targetBrand = brand;
		}

		public void run() {
			try {
				
				logger.info("Thread " + targetBrand + " starting.");
				
				String[] allModelResult = HTTPUtil.httpRequestWithStatus("http://getprice.anc-service.com/x_Api/resultFrom.php?user=priceza&pass=pri@pa88&gettype=model&brand=" + targetBrand, "UTF-8", false);
				if(allModelResult == null || allModelResult.length != 2) return;
				
				String allModel = allModelResult[0];
				if(StringUtils.isBlank(allModel)) return;
				List<String> allModelList = FilterUtil.getAllStringBetween(allModel, "value=\"", "\"");
				
				logger.info("Thread " + targetBrand + " allModelList = " + allModelList.size());
				
				if (allModelList.isEmpty()) return; 
	
				for (String model : allModelList) {
	
					try {
						
						if (StringUtils.isBlank(model)) continue;
						
						String[] allYearResult = HTTPUtil.httpRequestWithStatus("http://getprice.anc-service.com/x_Api/resultFrom.php?user=priceza&pass=pri@pa88&gettype=yearcar&brand=" + targetBrand + "&model=" + model, "UTF-8", false);
						if(allYearResult == null || allYearResult.length != 2) continue;
						
						String allYear = allYearResult[0];
						if(StringUtils.isBlank(allYear)) continue;
						List<String> allYearList = FilterUtil.getAllStringBetween(allYear, "value=\"", "\"");
		
						logger.info("Thread " + targetBrand + " with modelId " + model + " have " + allYearList.size() + " year(s).");
						
						if (allYearList.isEmpty() || allYearList == null) {
							continue;
						}
		
						for (String year : allYearList) {
		
							if (StringUtils.isBlank(year)) continue;
		
							String parentBrand = "";
							String parentYear = "";
							String parentMotorId = "";
							String parentModel = "";
							String parentCC = "";
							String parentModelDes = "";
							String parentModelType = "";
							String parentCode = "";
		
							String REQUEST_URL = "http://getprice.anc-service.com/src/calPrice.php?user=priceza&motorID=" + model + "&yCar=" + year + "&randomString=" + System.currentTimeMillis();
							String[] rtnResult = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
							if(rtnResult == null || rtnResult.length != 2) continue;
							
							String result = rtnResult[0];
							result = result.replaceAll("<PRE[0-9]+>", "<PRE>").replaceAll("</PRE[0-9]+>", "</PRE>");
							
							String currentBrand = FilterUtil.getStringBetween(result, "<BRAND>", "</BRAND>");
							String currentYear = FilterUtil.getStringBetween(result, "<YCAR>", "</YCAR>");
							String currentMotorId = FilterUtil.getStringBetween(result, "<MOTORID>", "</MOTORID>");
							String currentModel = FilterUtil.getStringBetween(result, "<MODEL>", "</MODEL>");
							String currentCC = FilterUtil.getStringBetween(result, "<CC>", "</CC>");
							String currentModelDes = FilterUtil.getStringBetween(result, "<MODELDES>", "</MODELDES>");
							String currentModelType = FilterUtil.getStringBetween(result, "<MODELTYPE>", "</MODELTYPE>");
							String currentCode = FilterUtil.getStringBetween(result, "<CODECAR>", "</CODECAR>");
							
							String checktime = FilterUtil.getStringBetween(result, "<CHECKTIME0>", "</CHECKTIME0>");
							
							if (StringUtils.isNotBlank(currentBrand)) 		parentBrand = currentBrand;
							if (StringUtils.isNotBlank(currentYear)) 		parentYear = currentYear;
							if (StringUtils.isNotBlank(currentMotorId)) 	parentMotorId = currentMotorId;
							if (StringUtils.isNotBlank(currentModel)) 		parentModel = currentModel;
							if (StringUtils.isNotBlank(currentModelDes)) 	parentModelDes = currentModelDes;
							if (StringUtils.isNotBlank(currentCode)) 		parentCode = currentCode;
							if (StringUtils.isNotBlank(currentCC)) 			parentCC = new DecimalFormat("##.0").format(Double.parseDouble(currentCC) / 1000);
							if (StringUtils.isNotBlank(currentModelType)) 	parentModelType = new DecimalFormat("##.0").format(Double.parseDouble(currentCC) / 1000);

							List<String> product = FilterUtil.getAllStringBetween(result, "<PRE>", "</PRE>");
							
							if(StringUtils.isNotBlank(parentModelDes)) {
								logger.info(REQUEST_URL + " | " + checktime + " | " + year +" | " + parentModelDes + " | " + product.size() + " product(s).");
							}
							
							if (product != null && product.size() > 0) {
								for (String s : product) {
									try {
										parseXml(s, parentBrand, parentYear, parentMotorId, parentModel, parentCC, parentModelDes, parentModelType, parentCode);
									} catch (Exception e) {
										logger.error(e);
									}
								}
							}
							
						}
						
					}catch(Exception e) {
						logger.info("Thread " + targetBrand + " with modelId : " + model + " got exception : " + e.toString());
//						System.out.println("Thread " + targetBrand + " with modelId : " + model + " got exception : " + e.toString());
						logger.error(e);
					}
					
				}
				
			} catch (Exception e) {
				logger.info("Thread " + targetBrand + " got exception : " + e.toString());
//				System.out.println("Thread " + targetBrand + " got exception : " + e.toString());
				logger.error(e);
			}
			logger.info("Thread " + targetBrand + " exiting. success " + successCount + "/" + count);
//			System.out.println("Thread " + targetBrand + " exiting. success " + successCount + "/" + count);
		}
		
		private void parseXml(String data, String brand, String year, String motorId, String model, String cc, String modelDes, String modelType, String code) throws UnsupportedEncodingException, ClassNotFoundException, SQLException, ParseException {

			if(StringUtils.isBlank(data) || StringUtils.isBlank(brand) || StringUtils.isBlank(year) || StringUtils.isBlank(model)){
				return;
			}
			
			count++;
			
			String poType 		= FilterUtil.getStringBetween(data, "<POTYPE>", "</POTYPE>");
			String poTypeId		= FilterUtil.getStringBetween(data, "<POTYPEID>", "</POTYPEID>");
			String od 			= FilterUtil.getStringBetween(data, "<OD>", "</OD>");
			String fix 			= FilterUtil.getStringBetween(data, "<FIX>", "</FIX>");
			String oddd			= FilterUtil.getStringBetween(data, "<ODDD>", "</ODDD>");
			String price		= FilterUtil.getStringBetween(data, "<OFFER_PRICE>", "</OFFER_PRICE>");
			String discount		= FilterUtil.getStringBetween(data, "<OFFER_DISCOUNT>", "</OFFER_DISCOUNT>");
//			String carId		= FilterUtil.getStringBetween(data, "<IDCAR>", "</IDCAR>");
			String insCode 		= FilterUtil.getStringBetween(data, "<INS>", "</INS>");
			String insNum 		= FilterUtil.getStringBetween(data, "<INSID>", "</INSID>");
			String packId		= FilterUtil.getStringBetween(data, "<IDPACK>", "</IDPACK>");
			String compulsory 	= FilterUtil.getStringBetween(data, "<CMI_INCLUDE>", "</CMI_INCLUDE>");
			String packName 	= FilterUtil.getStringBetween(data, "<PACKNAME>", "</PACKNAME>");
			String packNameCus 	= FilterUtil.getStringBetween(data, "<PACKNAMECUS>", "</PACKNAMECUS>");
//			String cctv			= FilterUtil.getStringBetween(data, "<CCTV_DISCOUNT>", "</CCTV_DISCOUNT>");
			String netPre		= FilterUtil.getStringBetween(data, "<NETPREMIUM>", "</NETPREMIUM>");
			String pre			= FilterUtil.getStringBetween(data, "<PREMIUM>", "</PREMIUM>");
			String ref1			= FilterUtil.getStringBetween(data, "<REF1>", "</REF1>");
			String ref2			= FilterUtil.getStringBetween(data, "<REF2>", "</REF2>");
			String ref3			= FilterUtil.getStringBetween(data, "<REF3>", "</REF3>");
			String insName 		= FilterUtil.getStringBetween(data, "<INSNAME>", "</INSNAME>");
			
			if(StringUtils.isNotBlank(packNameCus)){
				packName = packNameCus;
			}
			 
			if(StringUtils.isBlank(poType)){
				logger.info("no poType. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			if(StringUtils.isBlank(od)) {
				logger.info("no od. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			if(StringUtils.isBlank(price)) {
				logger.info("no price. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			if(blockPackage != null && blockPackage.length > 0) {
				for(String blockPack : blockPackage) {
					if(StringUtils.isNotBlank(packName) && packName.contains(blockPack)) {
						logger.info("block package. motorId:" + motorId + ", year:" + year + ", packName:" + packName);
						return;
					}
				}
			}
			
			ProductDataBean pdb = new ProductDataBean();
			
			String productName = "";
			String productPrice = "";
			String productDesc = "";
			String productPic = "";
			String productUrl = "https://www.ancbroker.com/motor/ref.aspx";
			String productUrlForUpdate = "";
			
			String uniqueText = "";
			uniqueText += pre;
			uniqueText += poType;
			uniqueText += insName;
			uniqueText += brand;
			uniqueText += model;
			uniqueText += modelDes;
			uniqueText += year;
			uniqueText += packName;
			uniqueText += fix;
			uniqueText += compulsory;
			uniqueText = toMD5(uniqueText);

			// V.2
			uniqueText += 2;
			
//			ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
			productName += "ประกันชั้น " + poType.replaceAll("ป", "").trim();
			productName += " - " + insName.replaceAll("-", " ");
			productName += " - " + brand.replaceAll("-", " ");
			productName += " - " + model.replaceAll("-", " ");
			productName += " - " + ((StringUtils.isNotBlank(modelDes))? modelDes.replaceAll("-", " ") : "ไม่ระบุรุ่นย่อย"); 
			productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
			productName += " (" + uniqueText.substring(0,10) + ")";
			productName = productName.replace(",", "");
			productName = productName.replaceAll("\\s+", " ");                                                  
			productName = productName.replace("( ", "(");                                                       
			productName = productName.replace(" )", ")");                                                       
			productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
			productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")"); 
			
			productPrice = new DecimalFormat("###").format(Double.parseDouble(FilterUtil.removeCharNotPrice(price)));
			
			String fire		= FilterUtil.getStringBetween(data, "<FIRE>", "</FIRE>");
			String flood 	= FilterUtil.getStringBetween(data, "<FLOOD>", "</FLOOD>");
			String bi 		= FilterUtil.getStringBetween(data, "<BI>", "</BI>");
			String bt		= FilterUtil.getStringBetween(data, "<BT>", "</BT>");
			String tp 		= FilterUtil.getStringBetween(data, "<TP>", "</TP>");
			String pa 		= FilterUtil.getStringBetween(data, "<PA>", "</PA>");
			String bb		= FilterUtil.getStringBetween(data, "<BB>", "</BB>");
			String me 		= FilterUtil.getStringBetween(data, "<ME>", "</ME>");
					
			if(StringUtils.isBlank(fire) && (data.contains("<FIRE/>") || data.contains("<FIRE>"))) fire = "0";
			if(StringUtils.isBlank(flood) && (data.contains("<FLOOD/>") || data.contains("<FLOOD>"))) flood = "0";
			if(StringUtils.isBlank(bi) && (data.contains("<BI/>") || data.contains("<BI>"))) bi = "0";
			if(StringUtils.isBlank(bt) && (data.contains("<BT/>") || data.contains("<BT>"))) bt = "0";
			if(StringUtils.isBlank(tp) && (data.contains("<TP/>") || data.contains("<TP>"))) tp = "0";
			if(StringUtils.isBlank(pa) && (data.contains("<PA/>") || data.contains("<PA>"))) pa = "0";
			if(StringUtils.isBlank(bb) && (data.contains("<BB/>") || data.contains("<BB>"))) bb = "0";
			if(StringUtils.isBlank(me) && (data.contains("<ME/>") || data.contains("<ME>"))) me = "0";
			
			String poTypeText = poType.replaceAll("ป", "ชั้น").trim();
			
//			( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
			productDesc += ((StringUtils.isNotBlank(packName))? packName.replaceAll("-", " ") : "ไม่ระบุแผน" );
			productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(fix))? fix : " ไม่ระบุอู่/ห้าง");
			productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od) || od.equals("000")|| od.equals("0"))?"ไม่ระบุ":od);
			productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd)|| oddd.equals("000")|| oddd.equals("0"))?"ไม่ระบุ": oddd);
			productDesc += " - " + ((StringUtils.isBlank(compulsory) || compulsory.equals("no"))?"ไม่รวมพรบ.":"รวมพรบ. ");
			productDesc += " - คุ้มครองไฟไหม้  " + ((StringUtils.isBlank(fire) || fire.equals("000")|| fire.equals("0"))?"ไม่ระบุ":fire);
			productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isBlank(fire)|| fire.equals("000")|| fire.equals("0"))?"ไม่ระบุ":fire);
			productDesc += " - คุ้มครองน้ำท่วม  " + ((StringUtils.isBlank(flood)|| flood.equals("000")|| flood.equals("0"))?"ไม่ระบุ":flood);
			productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isBlank(bi)|| bi.equals("000")|| bi.equals("0"))?"ไม่ระบุ":bi);
			productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isBlank(bt)|| bt.equals("000")|| bt.equals("0"))?"ไม่ระบุ":bt);
			productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isBlank(tp)|| tp.equals("000")|| tp.equals("0"))?"ไม่ระบุ":tp);
			productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isBlank(pa)|| pa.equals("000")|| pa.equals("0"))?"ไม่ระบุ":pa);
			productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isBlank(bb)|| bb.equals("000")|| bb.equals("0"))?"ไม่ระบุ":bb);
			productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isBlank(me)|| me.equals("000")|| me.equals("0"))?"ไม่ระบุ":me);
			productDesc = productDesc.replaceAll("\\s+", " ");                                               
			productDesc = productDesc.replace("( ", "(");                                                    
			productDesc = productDesc.replace(" )", ")");                                                    
			productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
			productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
			productDesc = productDesc.replace(",", "");
			
			productPic = getPictureByInsName(poTypeText, insName, "https://halobe.files.wordpress.com/2017/01/anc-car-logo-2.png");
			
			productUrl += "?inscode="+ 	((insCode == null) ? "":insCode.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_name=" + ((packName == null) ? "": packName.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_od=" + ((od == null) ? "":od.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_fix=" + ((fix == null) ? "":fix.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_netpre=" +	((netPre == null) ? "":netPre.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_pre=" + ((pre == null) ? "":pre.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_offerprice=" + ((price == null) ? "":price.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_offerdiscount=" + ((discount == null) ? "":discount.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&pack_id=" + ((packId == null) ? "":packId.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_brand=" + ((brand == null) ? "":brand.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_model=" + ((model == null) ? "":model.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_cc=" + 	((cc == null) ? "":cc.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_year=" + ((year == null) ? "":year.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_code=" + ((code == null) ? "":code.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&car_id=" + 	((motorId == null) ? "":motorId.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&afp_name=priceza";
			productUrl += "&ref1=" + ((ref1 == null) ? "":ref1.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&ref2=" + ((ref2 == null) ? "":ref2.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&ref3=" + ((ref3 == null) ? "":ref3.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&INSID=" + ((insNum == null) ? "":insNum.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&POTYPE=" + ((poType == null) ? "":poType.replace("&", "%26").replace("?", "%3F"));
			productUrl += "&POTYPEID=" + ((poTypeId == null) ? "":poTypeId.replace("&", "%26").replace("?", "%3F"));

			productUrlForUpdate += uniqueText;
			
			if(StringUtils.isBlank(productPrice)){
				logger.info("no price. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			if(StringUtils.isBlank(productDesc)) {
				logger.info("no desc. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			if(StringUtils.isBlank(productUrl)) {
				logger.info("no url. motorId:" + motorId + ", year:" + year + ", packId:" + packId);
				return;
			}
			
			
			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
			pdb.setDescription(productDesc);
			pdb.setPictureUrl(productPic);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrlForUpdate);
			pdb.setCategoryId(250101);
			pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");
			
			// BanInsure
			if(insName.equals("เคเอสเคประกันภัย") || insName.equals("เจ้าพระยาประกันภัย")) { 
				return ;
			}
			
			successCount++;
			
			String result = mockResult(pdb);
			if(StringUtils.isNotBlank(result)) {
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
					pwr.println(result);
					haveResult = true;
				}catch(Exception e) {
					logger.error(e);
				}
			}
			
		}

		private String mockResult(ProductDataBean pdb) {
			StringBuilder result = new StringBuilder();
			
			result.append("<product>");
			if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
			if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
			if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
			if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
			if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
			if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
			if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
			if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
			if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
			if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
			if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
			result.append("</product>");
			
			return result.toString();
		}

		private String getPictureByInsName(String poTypeText, String insName, String defaultPicture) {
			String productPic = defaultPicture;
			if(insName.equals("เคเอสเคประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c60.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c216.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c316.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c416.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c516.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c616.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c716.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c816.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c916.jpg";}
			}
			else if(insName.equals("แอลเอ็มจีประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c11.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c23.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c33.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c43.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c53.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c63.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c73.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c83.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c93.jpg";}
			}
			else if(insName.equals("เอ็มเอสไอจีประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c19.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c211.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c311.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c411.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c511.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c611.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c711.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c811.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c911.jpg";}
			}
			else if(insName.equals("เทเวศประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c30.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c213.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c312.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c412.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c512.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c612.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c712.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c812.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c912.jpg";}
			}
			else if(insName.equals("เมืองไทยประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c105.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c225.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c325.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c425.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c525.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c625.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c725.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c825.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c925.jpg";}
			}
			else if(insName.equals("เอเชียประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c13.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c25.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c35.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c45.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c55.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c65.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c75.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c85.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c95.jpg";}
			}
			else if(insName.equals("แอกซ่าประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c80.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c218.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c318.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c418.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c518.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c618.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c718.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c818.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c918.jpg";}
			}
			else if(insName.equals("ไทยเศรษฐกิจประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c17.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c29.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c39.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c49.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c59.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c69.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c79.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c89.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c99.jpg";}
			}
			else if(insName.equals("ไทยประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c2.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c3.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c4.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c5.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c6.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c7.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c8.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c9.jpg";}
			}
			else if(insName.equals("ประกันภัยไทยวิวัฒน์")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c1.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c21.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c31.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c41.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c51.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c61.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c71.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c81.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c91.jpg";}
			}
			else if(insName.equals("ไอโออิกรุงเทพประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c15.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c27.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c37.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c47.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c57.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c67.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c77.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c87.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c97.jpg";}
			}
			else if(insName.equals("กรุงเทพประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) 		{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c70.jpg";}
				else if(poTypeText.equals("ชั้น2")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c217.jpg";}
				else if(poTypeText.equals("ชั้น2+")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c317.jpg";}
				else if(poTypeText.equals("ชั้น3")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c417.jpg";}
				else if(poTypeText.equals("ชั้น3+")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c517.jpg";}
				else if(poTypeText.equals("ชั้น4")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c617.jpg";}
				else if(poTypeText.equals("ชั้น4+")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c717.jpg";}
				else if(poTypeText.equals("ชั้น5")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c817.jpg";}
				else if(poTypeText.equals("ชั้น5+")) 	{ productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c917.jpg";}
			}
			else if(insName.equals("กรุงไทยพานิชประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c12.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c24.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c34.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c44.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c54.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c64.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c74.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c84.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c94.jpg";}
			}
			else if(insName.equals("ทิพยประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c18.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c210.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c310.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c410.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c510.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c610.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c710.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c810.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c910.jpg";}
			}
			else if(insName.equals("ธนชาตประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c90.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c219.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c319.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c419.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c519.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c619.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c719.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c819.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c919.jpg";}
			}
			else if(insName.equals("นวกิจประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c14.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c26.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c36.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c46.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c56.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c66.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c76.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c86.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c96.jpg";}
			}
			else if(insName.equals("นำสินประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c10.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c22.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c32.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c42.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c52.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c62.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c72.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c82.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c92.jpg";}
			}
			else if(insName.equals("ประกันคุ้มภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c102.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c222.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c322.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c422.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c522.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c622.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c722.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c822.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c922.jpg";}
			}
			else if(insName.equals("วิริยะประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c101.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c221.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c321.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c421.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c521.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c621.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c721.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c821.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c921.jpg";}
			}
			else if(insName.equals("สามัคคีประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c103.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c223.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c323.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c423.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c523.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c623.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c723.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c823.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c923.jpg";}
			}
			else if(insName.equals("สินทรัพย์ประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c16.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c28.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c38.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c48.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c58.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c68.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c78.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c88.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c98.jpg";}
			}
			else if(insName.equals("สินมั่นคงประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c20.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c212.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c313.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c413.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c513.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c613.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c713.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c813.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c913.jpg";}
			}
			else if(insName.equals("อลิอันซ์ซีพีประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c40.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c214.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c314.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c414.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c514.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c614.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c714.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c814.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c914.jpg";}
			}
			else if(insName.equals("อาคเนย์ประกันภัย")) { 
				if(poTypeText.equals("ชั้น1")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c104.jpg";}
				else if(poTypeText.equals("ชั้น2")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c224.jpg";}
				else if(poTypeText.equals("ชั้น2+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c324.jpg";}
				else if(poTypeText.equals("ชั้น3")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c424.jpg";}
				else if(poTypeText.equals("ชั้น3+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c524.jpg";}
				else if(poTypeText.equals("ชั้น4")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c624.jpg";}
				else if(poTypeText.equals("ชั้น4+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c724.jpg";}
				else if(poTypeText.equals("ชั้น5")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c824.jpg";}
				else if(poTypeText.equals("ชั้น5+")) { productPic = "https://hdskasl.files.wordpress.com/2017/01/e0b89be0b8a3e0b8b0e0b881e0b8b1e0b899e0b8a3e0b896e0b8a2e0b899e0b895e0b98c924.jpg";}
			}
			return productPic;
		}

		private String toMD5(String text){
			MessageDigest m = null;
			try {
				m = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				logger.error(e);
				return "";
			}
			m.reset();
			m.update(text.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			return hashtext;
		}
		
	}
	
}
