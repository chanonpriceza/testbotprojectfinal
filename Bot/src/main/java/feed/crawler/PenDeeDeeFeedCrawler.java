package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.parser.ParseException;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PenDeeDeeFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	private boolean haveResult = false;
	
	public PenDeeDeeFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		Set<String> keySet = getCategoryKeySet();
		for (String link : keySet) {
			String[] response = HTTPUtil.httpRequestWithStatus(link, "UTF-8", true);
			if(response == null || response.length != 2) continue;
			
			String html = response[0];
			if(StringUtils.isBlank(html)) continue;
			
			int allPages = getAllPageNumber(html);
			for (int pageNum = 1; pageNum <= allPages; pageNum++) {
				String newUrl = link + "?tskp=" + pageNum;
				String[] dataResponse = HTTPUtil.httpRequestWithStatus(newUrl, "UTF-8", true);
				if(dataResponse == null || dataResponse.length != 2) continue;
				
				if (dataResponse[0] != null) {
					List<String> productUrlList = FilterUtil.getAllStringBetween(dataResponse[0], "href=\"", "\"");
					if(productUrlList != null && productUrlList.size() > 0){
						for(String url : productUrlList){
							if(url.contains("/product/")){
								try {
									Thread.sleep(1500);
									String[] htmlResponse = HTTPUtil.httpRequestWithStatus(url, "UTF-8", true);
									if(htmlResponse == null || htmlResponse.length != 2) continue;
									
									if(htmlResponse != null && StringUtils.isNotBlank(htmlResponse[0])){
										parseHTML(htmlResponse[0], link, url);
									}
								} catch (ParseException e) {
									logger.error(e);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									logger.error(e);
								}
							}
						}
					}
				}
			}
		}
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public int getAllPageNumber(String data) {
		int pageAll = 1;
		String countProduct = FilterUtil.getStringBetween(data, "<div class=\"show_page\"", "ชิ้น</div>");
		countProduct = FilterUtil.getStringAfter(countProduct, "ทั้งหมด ", countProduct).trim();
		if(StringUtils.isNotBlank(countProduct) && StringUtils.isNumeric(countProduct))
			pageAll =pageAll + Integer.parseInt(countProduct) / 30;
		return pageAll;
	}
	
	
	private void parseHTML(String html, String originalLink, String url) throws ParseException {
		List<ProductDataBean> allPdList = new ArrayList<ProductDataBean>();
		String tmpHTML= FilterUtil.getStringBetween(html, "<div itemscope itemtype=\"http://schema.org/Product\">", "<div class=\"unitShoe\">");
		List<String> subproductList = FilterUtil.getAllStringBetween(tmpHTML, "<td class=\"subproductItem\"", "<div class=\"addCartArea\">");
		String mainDescription = "";
		String category = "0";
		String keyword = null;
		String[] catMap = getCategory(originalLink);
		if(catMap != null && catMap.length > 0){
			if(NumberUtils.isCreatable(catMap[0])) category = catMap[0];
			if(StringUtils.isNotBlank(keyword)) keyword = catMap[1];
		}
		
		try{
			if(StringUtils.isNotBlank(tmpHTML)){
				ProductDataBean mainProduct = parseMainProduct(tmpHTML, category, keyword);
				if(mainProduct != null){
					mainDescription = mainProduct.getDescription();
					allPdList.add(mainProduct);
				}
				
			}
			
			if(subproductList != null && subproductList.size() > 0){
				List<ProductDataBean> subpdbList = parseChildProduct(subproductList, category, keyword);
				if(subpdbList != null && subpdbList.size() > 0){
					for(ProductDataBean subPdb : subpdbList){
						subPdb.setDescription(mainDescription);
						allPdList.add(subPdb);
					}
				}
			}
			
			if(allPdList != null && allPdList.size() > 0){
				for(ProductDataBean pdb : allPdList){
					String result = mockResult(pdb);
					if(StringUtils.isNotBlank(result)) {
						try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
							pwr.println(result);
							haveResult = true;
						}catch(Exception e) {
							logger.error(e);
						}
					}
				}
			}

		}catch(Exception e){
			logger.error(e);
		}

	}
	
	public ProductDataBean parseMainProduct(String tmpHTML, String category, String keyword){
		String name 			= getMainProductName(tmpHTML);
		String price 			= getMainProductPrice(tmpHTML);
		String pictureUrl 		= getMainProductPictureUrl(tmpHTML);
		String realProductId 	= getMainRealProductId(tmpHTML);
		String description 		= getMainProductDescription(tmpHTML);
		if(name.length() == 0 || price.length()  == 0|| realProductId.length()  == 0 || realProductId.length() == 0){
			return null;
		}

		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+ " ("+realProductId + ")");
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		String url = "http://www.pendeedee.com/p/"+realProductId;
		pdb.setUrl(url);
		pdb.setKeyword(keyword);
		pdb.setCategoryId(Integer.parseInt(category));
		pdb.setRealProductId(realProductId);
		pdb.setPictureUrl(pictureUrl);
		pdb.setDescription(description);
		return pdb;
		
	}
	
	public List<ProductDataBean> parseChildProduct(List<String> subproductList, String category, String keyword){
		List<ProductDataBean> pList = new ArrayList<ProductDataBean>();
		if(subproductList != null && subproductList.size() > 0){
			for(String tmpHTML : subproductList){
				String name 			= getProductName(tmpHTML);
				String price 			= getProductPrice(tmpHTML);
				String pictureUrl 		= getProductPictureUrl(tmpHTML);
				String realProductId 	= getRealProductId(tmpHTML);
				if(name.length() == 0 || price.length()  == 0|| realProductId.length()  == 0 || realProductId.length() == 0){
					continue;
				}
				ProductDataBean pdb = new ProductDataBean();
				pdb.setName(name+ " ("+realProductId + ")");
				pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
				String url = "http://www.pendeedee.com/p/"+realProductId;
				pdb.setUrl(url);
				pdb.setKeyword(keyword);
				pdb.setCategoryId(Integer.parseInt(category));
				pdb.setRealProductId(realProductId);
				pdb.setPictureUrl(pictureUrl);
				pList.add(pdb);
			}
		}
		return pList;
		
		
	}
	
	public String getProductName(String html) {
		
		String productName = "";   
				
		if(html.length() >= 1) {	
			productName = FilterUtil.getStringBetween(html, "<span class=\"headerText product_name\">", "</span>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return productName;
	}
	
	public String getProductPrice(String html) {
		
		String productPrice = "";
    	if (html.length() >= 1) {
    		productPrice = FilterUtil.getStringBetween(html, "<span class=\"product_price\">", "</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return productPrice;
	}
	
	
	public String getProductPictureUrl(String html) {
		
		String productPictureUrl = null;
    	if (html.length() >= 1) {
    		
    		productPictureUrl = FilterUtil.getStringBetween(html, "<img class=\"subproductImage\" src=\"", "\"");
        }
    	
    	return productPictureUrl;
	}
	
	public String getRealProductId(String html) {
		String pdId = null;
		if(html.length() >= 1){
			pdId = FilterUtil.getStringBetween(html, "proid=\"", "\"");
		}
		return pdId;
	}
	
	public String getMainProductName(String html) {
		
		String productName = "";   
		if(html.length() >= 1) {	
			productName = FilterUtil.getStringBetween(html, "<h1 class=\"headerText\" itemprop=\"name\">", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return productName;
	}
	
	public String getMainProductPrice(String html) {
		
		String productPrice = "";
    	if (html.length() >= 1) {
    		productPrice = FilterUtil.getStringBetween(html, "<tr class=\"priceTR\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">", "</tr>");
    		String tmp = FilterUtil.getStringBetween(productPrice, "<span itemprop=\"lowPrice\">", "</span>");
    		if(StringUtils.isBlank(tmp)){
    			productPrice = FilterUtil.getStringBetween(productPrice, "<span itemprop=\"price\">", "</span>"); 
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return productPrice;
	}
	
	public String getMainProductDescription(String html) {
		
		String productDesc = "";
    	if(html.length() >= 1) {
    		productDesc = FilterUtil.getStringBetween(html, "<div id=\"detail\" class=\"tabPanel mceContentBody\" itemprop=\"description\">", "<div id=\"review\" class=\"tabPanel\">");    		
    		productDesc = FilterUtil.toPlainTextString(productDesc);  
     	}
    	
    	return productDesc;
	}
	
	public String getMainProductPictureUrl(String html) {
		
		String productPictureUrl = null;
    	if (html.length() >= 1) {
    		
    		productPictureUrl = FilterUtil.getStringBetween(html, "<div class=\"productPhoto\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
        }
    	
    	return productPictureUrl;
	}
	
	public String getMainRealProductId(String html) {
		String pdId = null;
		if(html.length() >= 1){
			pdId = FilterUtil.getStringBetween(html, "<input type=\"hidden\" name=\"product_id\" value=\"", "\"");
		}
		return pdId;
	}
	
	private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}
	
}
