package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class KinokuniyaThailandFeedCrawler extends FeedManager {
	
	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public KinokuniyaThailandFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){

			String id = data.get(0);
			String name = data.get(1);
			String price = data.get(2);
			String url  = data.get(4);
			String category_name = data.get(7);
			String basePrice = data.get(8);
						
			if( StringUtils.isBlank(name) || StringUtils.isBlank(url) ||  StringUtils.isBlank(price) ||  StringUtils.isBlank(id)){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			
			if(!FilterUtil.checkContainOnlyCharacter(name, new String[] {"th", "eng"})) {
				logger.info("Product name contain restrict language : " + name);
				return null;
			}
			if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"th", "eng"})) {
				logger.info("Product url contain restrict language : " + url);
				return null;
			}
			
			
			pdb.setName(name+" ("+id+")");
			pdb.setUrl(url);
			pdb.setPrice(FilterUtil.convertPriceStr(price));
			pdb.setUpc(id);
			pdb.setDescription(category_name+" - Domestic delivery จัดส่งฟรี สำหรับรายการสั่งซื้อที่เกินตามยอดที่ทางร้านกำหนด");
		
			pdb.setPictureUrl(generatePictureUrl(id));
			
			if(StringUtils.isNotBlank(basePrice)) {
				pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
			}
			
			String[] mapping = getCategory(category_name);
			if(mapping != null) {
				pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
				pdb.setKeyword(mapping[1]);				
			} else {
				pdb.setCategoryId(0);
			}

			return pdb;
			
	}

	private String generatePictureUrl(String id) {
		
		if(id.length()==13) {
			String main = "https://bci.kinokuniya.com/jsp/images/book-img";
			String first = id.substring(0,5);
			String second = id.substring(0,8);
			
			main = main+"/"+first+"/"+second+"/"+id+".JPG";
			
			return main;
		}
		
		return null;
	}
	
}
