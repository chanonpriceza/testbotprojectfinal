package feed.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class VoyaginFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
	public VoyaginFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}
	
	public ProductDataBean parse(CSVRecord data){

		// 0     id
		// 1     name
		// 2     main_option_name
		// 3     duration
		// 4     hero_image
		// 5     country
		// 6     main_area
		// 7     url_with_main_option
		// 8     price
		// 9     average_rating
		// 10    review_counts
		
		String name = data.get(1);
		String main_option_name = data.get(2);
		String image = data.get(4);
		String country = data.get(5);
		String main_area  = data.get(6);
		String url = data.get(7);			
		String price = data.get(8);	
		String average_rating = data.get(9);
		String cReview = data.get(10);
	
		if( StringUtils.isBlank(name)  || StringUtils.isBlank(url) || StringUtils.isBlank(price) || !url.startsWith("http")){
			return null;
		}
		ProductDataBean pdb = new ProductDataBean();
		String pName = name + ", " + main_area + " "+ country;
		pdb.setName(pName);
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
		pdb.setUrl(url+"?acode=priceza-TH");
		pdb.setUrlForUpdate(url);
		
		if(!image.isEmpty()){
			pdb.setPictureUrl(image);
		}
		
		String desc =  "Star rating of this package is " + average_rating + " and ranked " + cReview +  " by Voyagin member reviews. This package for " + main_option_name + ".";
		if(StringUtils.isNotBlank(desc)){
			pdb.setDescription(desc);
		}

		pdb.setCategoryId(500402);

		return pdb;
		
	}
	
}
