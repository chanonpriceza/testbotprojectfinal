package feed.crawler;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class GoldTradersHTMLParser extends DefaultHTMLParser{

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("price", new String[]{"<div id=\"DetailPlace_uc_goldprices1_GoldPricesUpdatePanel\">"});
		map.put("description", new String[]{"<div id=\"DetailPlace_uc_goldprices1_GoldPricesUpdatePanel\">"});
		
		return map;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		if (evaluateResult.length == 1) {    		
    		
    		String gold1Buy = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"DetailPlace_uc_goldprices1_lblBLBuy\" style=\"color:Red;font-size:X-Large;font-weight:bold;\">", "</span>");
    		gold1Buy = FilterUtil.toPlainTextString(gold1Buy);
        	
    		String gold1Sell = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"DetailPlace_uc_goldprices1_lblBLSell\" style=\"color:Red;font-size:X-Large;font-weight:bold;\">", "</span>");
    		gold1Sell = FilterUtil.toPlainTextString(gold1Sell);
        	    		
    		String gold2Buy = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"DetailPlace_uc_goldprices1_lblOMBuy\" style=\"color:Red;font-size:X-Large;font-weight:bold;\">", "</span>");
    		gold2Buy = FilterUtil.toPlainTextString(gold2Buy);
        	
    		String gold2Sell = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"DetailPlace_uc_goldprices1_lblOMSell\" style=\"color:Red;font-size:X-Large;font-weight:bold;\">", "</span>");
    		gold2Sell = FilterUtil.toPlainTextString(gold2Sell);
        	   		
    		
    		gold1Buy = FilterUtil.removeCharNotPrice(gold1Buy);
    		gold1Sell = FilterUtil.removeCharNotPrice(gold1Sell);
    		gold2Buy = FilterUtil.removeCharNotPrice(gold2Buy);
    		gold2Sell = FilterUtil.removeCharNotPrice(gold2Sell);
    		if(gold1Buy.trim().length() != 0 && gold1Sell.trim().length() != 0 &&
    			gold2Buy.trim().length() != 0 && gold2Sell.trim().length() != 0) {
    			
    			return new String[] {gold1Buy, gold1Sell, gold2Buy, gold2Sell};
    		}
        }		
		
    	return new String[] {""};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		
    		String desc = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"DetailPlace_uc_goldprices1_lblAsTime\" style=\"font-size:Small;font-weight:bold;\">", "</span>");
    		desc = FilterUtil.toPlainTextString(desc);    		
    		if(desc.length() != 0) {
    			productDesc = "ประจำวันที่ " + desc;
    		}
    	}
    	return new String[] {productDesc};
	}

    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();

		if (productPrice == null || productPrice.length != 4) {
			return null;
		}
		
		double price0 = FilterUtil.convertPriceStr(productPrice[0]);
		double price1 = FilterUtil.convertPriceStr(productPrice[1]);
		double price2 = FilterUtil.convertPriceStr(productPrice[2]);
		double price3 = FilterUtil.convertPriceStr(productPrice[3]);
		
		if(price0 == 0 || price1 == 0 || price2 == 0 || price3 == 0) {
			return null;
		}
		
		String desc = "";
		if (productDescription != null && productDescription.length != 0 &&
				productDescription[0] != null && productDescription[0].trim().length() != 0) {
			desc = productDescription[0];
		}
		
		ProductDataBean[] rtn = new ProductDataBean[4];
		rtn[0] = new ProductDataBean();
		rtn[0].setName("ทองคำแท่ง รับซื้อ (บาท)");
		rtn[0].setPrice(price0);
        rtn[0].setDescription("ราคาทองคำ "+desc+" ทองคำแท่ง  96.5% รับซื้อ (บาท) Gold Price by GTA ราคาทองตามประกาศของสมาคมค้าทองคำ ");
		rtn[0].setPictureUrl("");
		rtn[0].setCategoryId(220101);
		rtn[0].setKeyword("ซื้อทองคำแท่ง ราคาซื้อทองคำแท่ง ราคาทองคำแท่ง");
		rtn[0].setPictureUrl("https://goldtraders.or.th/uploads/picture/icon/icon_fb.jpg");
		
		rtn[1] = new ProductDataBean();
		rtn[1].setName("ทองคำแท่ง ขายออก (บาท)");
		rtn[1].setPrice(price1);
        rtn[1].setDescription("ราคาทองคำ "+desc+" ทองคำแท่ง  96.5% ขายออก (บาท) Gold Price by GTA ราคาทองตามประกาศของสมาคมค้าทองคำ ");
		rtn[1].setPictureUrl("");
		rtn[1].setCategoryId(220102);
		rtn[1].setKeyword("ขายทองคำแท่ง ราคาขายทองคำแท่ง ราคาทองคำแท่ง");
		rtn[1].setPictureUrl("https://goldtraders.or.th/uploads/picture/icon/icon_fb.jpg");
		
		rtn[2] = new ProductDataBean();
		rtn[2].setName("ทองรูปพรรณ รับซื้อ (บาท)");
		rtn[2].setPrice(price2);
        rtn[2].setDescription("ราคาทองคำ "+desc+"  ทองรูปพรรณ  96.5% รับซื้อ (บาท) Gold Price by GTA ราคาทองตามประกาศของสมาคมค้าทองคำ ");
		rtn[2].setPictureUrl("");
		rtn[2].setCategoryId(220103);
		rtn[2].setKeyword("ซื้อทองรูปพรรณ ราคาซื้อทองรูปพรรณ ราคาทองรูปพรรณ");
		rtn[2].setPictureUrl("https://goldtraders.or.th/uploads/picture/icon/icon_fb.jpg");
		
		rtn[3] = new ProductDataBean();
		rtn[3].setName("ทองรูปพรรณ ขายออก (บาท)");
		rtn[3].setPrice(price3);
        rtn[3].setDescription("ราคาทองคำ "+desc+" ทองรูปพรรณ  96.5% ขายออก (บาท) Gold Price by GTA ราคาทองตามประกาศของสมาคมค้าทองคำ ");
		rtn[3].setPictureUrl("");
		rtn[3].setCategoryId(220104);
		rtn[3].setKeyword("ขายทองรูปพรรณ ราคาขายทองรูปพรรณ ราคาทองรูปพรรณ");
		rtn[3].setPictureUrl("https://goldtraders.or.th/uploads/picture/icon/icon_fb.jpg");
		
		return rtn;
	}
	    
}