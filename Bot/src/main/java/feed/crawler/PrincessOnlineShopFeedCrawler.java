package feed.crawler;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class PrincessOnlineShopFeedCrawler extends FeedManager {

	public PrincessOnlineShopFeedCrawler() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String[] setFeedFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		String FEED_URL = "http://27.254.82.243:8081/300640.csv";
		String FILE_NAME = "300640.csv";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		FTPUtil.downloadFileWithLogin(FEED_URL,BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME, FEED_USER, FEED_PASS);
		BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/"+FILE_NAME};
		

	}
	
	@Override
	public ProductDataBean parse(CSVRecord data) {
		
		String id = data.get(0);
		String name = data.get(1);
		String desc = data.get(2);
		String price = data.get(3);
		String base_price = data.get(4);
		String url = data.get(5);
		String image = data.get(6);
		String category = data.get(7);
		String keyword = data.get(8);

		ProductDataBean pdb = new ProductDataBean();
		
		if(StringUtils.isBlank(name) || StringUtils.isBlank(url) || StringUtils.isBlank(price) ||StringUtils.isBlank(id)) {
			return null;
		}
		price = FilterUtil.removeCharNotPrice(price);
		base_price = FilterUtil.removeCharNotPrice(base_price);
		pdb.setName(name);
		pdb.setPrice(FilterUtil.convertPriceStr(price));
		pdb.setDescription(desc);
		pdb.setRealProductId(id);
		pdb.setBasePrice(FilterUtil.convertPriceStr(base_price));
		pdb.setPictureUrl(image);
		pdb.setUrl(url);
		
		String[] mapping = getCategory(category);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(keyword);				
		} else {
			pdb.setCategoryId(0);
		}
		//pdb.setExpire(true);
		return pdb;
	}
	
	

}
