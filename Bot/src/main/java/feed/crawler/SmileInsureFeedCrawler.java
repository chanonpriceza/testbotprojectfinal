package feed.crawler;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class SmileInsureFeedCrawler extends FeedManager {

	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";
    
    private DecimalFormat formatPrice = null;
    
	public SmileInsureFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}

	
	public ProductDataBean parse(CSVRecord data){
		
		try{
			
//			product_id=0
//					 broker=1
//					 insurer=2 /
//					 title=3 /
//					 type=4 /
//					 premium=5
//					 round=6
//					 brand=7 /
//					 Model=8 /
//					 sub_model=9 /
//					 cc=10
//					 year=11 /
//					 URL ของสินค้านั้นๆ (ถ้ามี)=12
//					 compulsory_insurance=13 /
//					 sum_insured=14 /
//					 fire_damage=15 /
//					 theft=16 /
//					 flood_damage=17 /
//					 deductible=18 /
//					 repair=19 /
//					 bodily_injury_liability=20 /
//					 bodily_injury_liability/Accident=21 /
//					 property_damage_liability=22 /
//					 personal_injury_protection=23 /
//					 bail_bond=24 /
//					 medical_payments=25  /                 50000
//			
			String poType = data.get(4).toLowerCase().replace("type", "").trim().replace("-", " ").replace(",", "");                // -ชั้นประกัน
			if(!"1".equals(poType)) {
				return null;
			}
			String insName = data.get(2).trim().replace("-", " ");                                       // -บริษัทประกัน
			String brand = data.get(7).trim().replace("-", " ");                                                                    // -ยี่ห้อรถ
			String model = data.get(8).trim().replace("-", " ");                                                                    // -รุ่นรถ
			String subModel = data.get(9).trim().replace("-", " ");                                                                 // -รุ่นย่อย
			String cc = data.get(10).trim().replace("-", " ");                                                                 // -รุ่นย่อย
			String year = data.get(11).trim().replace("-", " ");                                                                     // -ปีรถ
			String od = data.get(14).trim().replace("-", " ");                                                                       // -ทุนประกัน
			String oddd = data.get(18).trim().replace("-", " ");                                                                    // -ค่าเสียหายส่วนแรก
			String garage = data.get(19).trim().replace("-", " ");                                                                  // -ซ่อม
			if(garage.contains("อู่"))
				garage = "ซ่อมอู่";
			else if(garage.contains("ศูนย์")||garage.contains("ห้าง"))
				garage = "ซ่อมศูนย์";
			else
				garage = "ไม่ระบุอู่ซ่อม";
			String packageName = data.get(3).trim().replace("-", " ");                                                            // -แผน
			String carAct = data.get(13).trim().replace("-", " ").replace(",", "");	                
			
			String fire = data.get(15).trim().replace("-", " ");           										                    // -คุ้มครองไฟไหม้
			String theft = data.get(16).trim().replace("-", " ");          										                    // -คุ้มครองโจรกรรม
			String flood = data.get(17).trim().replace("-", " ");          										                    // -คุ้มครองน้ำท่วม
			String bi = data.get(20).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อคน
			String bt = data.get(21).trim().replace("-", " ");             										                    // -ชีวิตบุคคลภายนอกต่อครั้ง
			String tp = data.get(22).trim().replace("-", " ");             										                    // -ทรัพย์สินบุคคลภายนอก
			String pa = data.get(23).trim().replace("-", " ");             										                    // -อุบัติเหตุส่วนบุคคล
			String bb = data.get(24).trim().replace("-", " ");             										                    // -ประกันตัวผู้ขับขี่
			String me = data.get(25).trim().replace("-", " ");             										                    // -ค่ารักษาพยาบาล
			
			String id = data.get(0).trim();
			String productPrice = data.get(6).trim();
			String productUrl = data.get(12).trim();
			
			String uniqueText = "";
			uniqueText += id;
			uniqueText = toMD5(uniqueText);
			cc = convertCC(cc);
			
//			ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
			String productName = "";
			productName += "ประกันชั้น " + poType;
			productName += " - " + insName;
			productName += " - " + brand;
			productName += " - " + model+" "+cc+"cc";
			productName += " - " + ((StringUtils.isNotBlank(subModel) && !subModel.equals("0"))? subModel : "ไม่ระบุรุ่นย่อย");
			productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
			productName += " (" + uniqueText.substring(0,10) + ")";
			productName = productName.replace(",", "");
			productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
			productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
			productName = productName.replaceAll("\\s+", " ");                                                  
			productName = productName.replace("( ", "(");                                                       
			productName = productName.replace(" )", ")");    
 			productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
			productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");                            
			                             
//			( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
			String productDesc = "";
			productDesc += ((StringUtils.isNotBlank(packageName))? packageName : "ไม่ระบุแผน" ); 
			productDesc += " - " + (StringUtils.isNotBlank(garage)?garage:"ไม่ระบุอู่ซ่อม");
			productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
			productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
			productDesc += " - " + ((StringUtils.isNotBlank(carAct) && (carAct.equals("Yes"))) ? "รวมพรบ." : "ไม่รวมพรบ.");
			productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
			productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
			productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood : "ไม่ระบุ" ); 
			productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
			productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
			productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
			productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
			productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );          
			productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
			productDesc = productDesc.replaceAll("\\s+", " ");                                               
			productDesc = productDesc.replace("( ", "(");                                                    
			productDesc = productDesc.replace(" )", ")");                                                    
			productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
			productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
			productDesc = productDesc.replace(".00", "");
			productDesc = productDesc.replace(",", "");
			
			formatPrice = new DecimalFormat("###");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			productPrice = formatPrice.format(Double.parseDouble(productPrice));
			
			String productUrlForUpdate = uniqueText;
			productUrl = productUrl.replaceAll(" ", "-");
			if(productUrl.startsWith("www")) {
				productUrl = "https://" + productUrl;
			}
			
			String productImage = genImage(insName);
			
			if(StringUtils.isBlank(productPrice) || StringUtils.isBlank(productUrl)){
				return null;
			}
			
			ProductDataBean pdb = new ProductDataBean();
			if(productName.contains("อาคเนย์")) {
				return null;
			}
			
			if(productName.contains("HONDA - CITY 1.5cc")&&productName.contains("เมืองไทย")&&productName.contains("ประกันชั้น 1 ")) {
				return null;
			}
			
			pdb.setName(productName);
			pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
			pdb.setPictureUrl(productImage);
			pdb.setDescription(productDesc);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrlForUpdate);
			pdb.setCategoryId(250101);
			pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;

			return pdb;
			
		}catch(Exception e){
			logger.error(e);
		}
		
		return null;
		
	}

	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	private String convertCC(String cc) {
		String result  = cc;
		double con = FilterUtil.convertPriceStr(cc);
		if(con%1==0)
			result = cc+".0";
		return  result;
	}
	
	private String genImage(String insurer) {
		String productImage = "";
		if(insurer.contains("สินทรัพย์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5567.jpg"; 
		if(insurer.contains("เอเชีย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5570.jpg"; 
		if(insurer.contains("อลิอันซ์ ซี.พี. "))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.contains("อลิอันซ์ "))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.contains("กรุงเทพ"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5558.jpg"; 
		if(insurer.contains("ทิพย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5565.jpg"; 
		if(insurer.contains("กรุงไทยพานิช"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5571.jpg"; 
		if(insurer.contains("เคเอสเค"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5559.jpg"; 
		if(insurer.contains("แอลเอ็มจี"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5572.jpg"; 
		if(insurer.contains("เมืองไทย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5551.jpg"; 
		if(insurer.contains("นวกิจ"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5569.jpg"; 
		if(insurer.contains("อาคเนย์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5552.jpg"; 
		if(insurer.contains("สินมั่นคง"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5562.jpg"; 
		if(insurer.contains("ประกันคุ้มภัย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5554.jpg"; 
		if(insurer.contains("ไทย"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5575.jpg"; 
		if(insurer.contains("ไทยไพบูลย์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9559.jpg"; 
		if(insurer.contains("ไทยวิวัฒน์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5574.jpg"; 
		if(insurer.contains("วิริยะ"))				productImage = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg"; 
		if(insurer.contains("อลิอันซ์ โกลบอล แอสซิสแทนซ์"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9605.jpg"; 
		if(insurer.contains("ไอโออิ กรุงเทพ "))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5568.jpg"; 
		if(insurer.contains("แอกซ่า"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5557.jpg"; 
		if(insurer.contains("บูพา"))						productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9606.jpg"; 
		if(insurer.contains("จรัญ"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9558.jpg"; 
		if(insurer.contains("ซิกน่า"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9604.jpg"; 
		if(insurer.contains("เจ้าพระยา"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5560.jpg"; 
		if(insurer.contains("เทเวศ"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5563.jpg"; 
		if(insurer.contains("เอราวัณ"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9569.jpg"; 
		if(insurer.contains("อินทร"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9572.jpg"; 
		if(insurer.contains("กมล"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9576.jpg"; 
		if(insurer.contains("เอ็มเอสไอจี"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5564.jpg"; 
		if(insurer.contains("นำสิน"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5573.jpg"; 
		if(insurer.contains("เอไอจี "))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9435.jpg"; 
		if(insurer.contains("ฟีนิกซ์"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9573.jpg"; 
		if(insurer.contains("รู้ใจ รับประกันโดย KPI กรุงไทยพานิช"))	productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9419.jpg"; 
		if(insurer.contains("ศรีอยุธยา เจนเนอรัล "))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9565.jpg"; 
		if(insurer.contains("สามัคคี"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5553.jpg"; 
		if(insurer.contains("ธนชาต"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5556.jpg"; 
		if(insurer.contains("โตเกียวมารีน"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9601.jpg"; 
		if(insurer.contains("ไทยเศรษฐกิจ"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5566.jpg"; 
		if(insurer.contains("ไทยพัฒนา"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9556.jpg"; 
		if(insurer.contains("ไทยศรี"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9561.jpg"; 
		if(insurer.contains("มิตรแท้"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9566.jpg"; 
		if(insurer.contains("พุทธธรรม"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9564.jpg"; 
		if(insurer.contains("บางกอกสห"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9568.jpg"; 
		if(insurer.contains("สหนิรภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9580.jpg"; 
		if(insurer.contains("สหมงคล"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9567.jpg";
		return productImage;
	}

}
