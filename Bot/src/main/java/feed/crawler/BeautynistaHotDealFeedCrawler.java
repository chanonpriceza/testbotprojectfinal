package feed.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import feed.FeedManager;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeautynistaHotDealFeedCrawler extends FeedManager{

	private static final Logger logger = LogManager.getRootLogger();

	public BeautynistaHotDealFeedCrawler() throws Exception {
		super();
	}

	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		boolean haveResult = false;
		int page = 1;
		while(!Thread.currentThread().isInterrupted()){
			String REQUEST_URL = "https://beautynista.com/productfeed/hot-deal/make-up?page="+page+"&record=100";
			
			String[] response = HTTPUtil.httpRequestWithStatus(REQUEST_URL, "UTF-8", false);
			if(response == null || response.length != 2) break;
			
			String data = response[0];
			if (StringUtils.isBlank(data)) { 
				break;
			}
			
			List<String> pdList = FilterUtil.getAllStringBetween(data, "<product>", "</product>");
			if(pdList != null && pdList.size() > 0){
				for (String pd : pdList) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println("<product>"+pd+"</product>");
					}catch(Exception e) {
						logger.error(e);
					}
				}
				haveResult = true;
			}else {
				break;
			}
			page++;
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}

	public ProductDataBean parse(String product) {
		
		String productName = FilterUtil.getStringBetween(product, "<name>", "</name>").trim();
		String productUrl = FilterUtil.getStringBetween(product, "<url>", "</url>").trim();
		String productImageUrl = "";
		String productCatId = FilterUtil.getStringBetween(product, "<category_id>", "</category_id>");
		String productDesc = FilterUtil.getStringBetween(product, "<description>", "</description>").trim();
		String productId = FilterUtil.getStringBetween(product, "<id>", "</id>").trim();	
		
		if(productUrl.length() == 0 || productName.length() == 0 || productId.length() == 0 ) {
			return null;			
		}
		productUrl = productUrl.replace("&amp;", "&");
		productUrl = BotUtil.encodeURL(productUrl);
		
		String[] response = HTTPUtil.httpRequestWithStatus(productUrl, "UTF-8", true);
		if(response == null || response.length != 2) return null;
		
		String pageData = response[0];
		if(StringUtils.isBlank(pageData)) return null;
		
		String productPrice = FilterUtil.getStringBetween(pageData, "<div class=\"net-price\">", "</div>");
		productPrice = StringEscapeUtils.unescapeHtml4(productPrice);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		if(StringUtils.isBlank(productPrice)) {
			return null;
		}
		
		String productBasePrice = FilterUtil.getStringBetween(pageData, "<div class=\"full-price\" style=\"\">", "</div>");
		productBasePrice = StringEscapeUtils.unescapeHtml4(productBasePrice);
		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		
		ProductDataBean pdb = new ProductDataBean();
		productName = StringEscapeUtils.unescapeHtml4(productName);	
		pdb.setName(productName);
		pdb.setRealProductId(productId);
		pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
		productUrl = productUrl.replace("&amp;", "&");
		
		if(StringUtils.isNoneBlank(productBasePrice)) {
			pdb.setBasePrice(FilterUtil.convertPriceStr(productBasePrice));
		}
		
		
		if (productDesc.length() != 0) {					
			pdb.setDescription(productDesc);
		}
		
		productImageUrl = FilterUtil.getStringBetween(pageData, "<div class=\"product-main-image zoom\">", "</div>");
		productImageUrl = FilterUtil.getStringBetween(productImageUrl, "src=\"", "\"");
		String currenturl = "https://beautynista.com/";
		if (productImageUrl != null && productImageUrl.length() != 0) {			
			if(productImageUrl != null && productImageUrl.trim().length() != 0) {				
				if(productImageUrl.startsWith("http")) {
					pdb.setPictureUrl(productImageUrl);					
				} else {
					try {
						URL url = new URL(new URL(currenturl), productImageUrl);
						pdb.setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {
						
					}
				}
			}
		}
		
		if (productUrl.length() != 0) {					
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productUrl);
		}
		
		String[] catMap = getCategory(productCatId);
		if(catMap != null) {			
			pdb.setCategoryId(BotUtil.stringToInt(catMap[0], 0));
			pdb.setKeyword(catMap[1]);			
		}else{
			pdb.setCategoryId(160244);
			pdb.setKeyword("Deal Deals Tawaran");
		}
	
		return pdb;
		
	}
	
}
