package feed.crawler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.FilterUtil;

public class JDCentralBranchFeedCrawler extends FeedManager{
	
	public JDCentralBranchFeedCrawler() throws Exception {
		super();
	}
	
	private static String detail 	= "https://p.jd.co.th/prices/mgets?skuIds={ID_LIST}&language=th_TH";
	private static String main 		= "https://jshop-rest.jd.co.th/shop/product/new/search?venderId={SHOPID}&siteFlag=th&pageSize=60&currency=THB&pageNo={PAGE}";
	private static JSONParser parser = new  JSONParser();
	
	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadFeed() {
		List<String> file = new ArrayList<String>();
		for (int i = 0;i<10;i++) {

			try (InputStream in = new URL(main.replace("{SHOPID}","1002032").replace("{PAGE}",String.valueOf(i))).openStream()) {
				InputStreamReader reader = new InputStreamReader(in);
				JSONObject obj = (JSONObject)parser.parse(reader);
				obj = (JSONObject) obj.get("productResult");
				JSONArray arr = (JSONArray) obj.get("list");
				JSONArray cut = new JSONArray();
				for(Object o:arr) {
					JSONObject cast = (JSONObject) o;
					cast.remove("subProductList");
					cut.add(cast);
				}
				InputStream is = new ByteArrayInputStream(cut.toString().getBytes());
				Files.copy(is,new File(BaseConfig.FEED_STORE_PATH+"/"+i+".json").toPath(), StandardCopyOption.REPLACE_EXISTING) ;
				file.add(BaseConfig.FEED_STORE_PATH+"/"+i+".json");
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
			
		}
		BaseConfig.FEED_FILE = file.toArray(String[]::new);
	}

	@Override
	public ProductDataBean parse(JSONObject data) {
		String name = String.valueOf(data.get("warename"));
		String id = String.valueOf(data.get("wareid"));
		String image = String.valueOf(data.get("imageurl"));
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+"- NOW Foods Shop "+" ("+id+")");
		pdb.setRealProductId(id);
		pdb.setUrlForUpdate(id);
		pdb.setUrl("https://item.jd.co.th/"+id+".html");
		pdb.setPictureUrl("https://img10.jd.co.th/n0/"+image+"!q70.jpg");
		pdb = getPrice(pdb);
		pdb.setCategoryId(131001);
		pdb.setKeyword("now food อาหารเสริม บำรุงสุขภาพ");
		

		return pdb;
	}
	
	private static ProductDataBean getPrice(ProductDataBean pdb) {
		try {
			URL u =  new URL(detail.replace("{ID_LIST}",String.valueOf(pdb.getRealProductId())));
			InputStreamReader reader = new InputStreamReader(u.openStream());
			JSONArray arr = (JSONArray) parser.parse(reader);
			if(arr.size()>0) {
				JSONObject one = (JSONObject) arr.get(0);
				String price = String.valueOf(one.get("p"));
				String oprice = String.valueOf(one.get("op"));
				
				pdb.setPrice(FilterUtil.convertPriceStr(price));
				pdb.setBasePrice(FilterUtil.convertPriceStr(oprice));
				
			}
		}catch (Exception e) {
			logger.error("Error",e);
		}
		return pdb;
	}
	
}
