package feed.crawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import feed.crawler.init.TipInsureCarFeedCrawlerInit01;
import utils.FilterUtil;

public class TipInsureCarMultiThreadFeedCrawler extends FeedManager {

	private static final Logger logger = LogManager.getRootLogger();
	
	private static final String apiUrl = "https://www.tipinsure.com/JsonService/get_motor_coverage";
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	private static final int maxConcurrent = 3;
	private static boolean haveResult = false;;

	public TipInsureCarMultiThreadFeedCrawler() throws Exception {
		super();
	}
		
	@Override
	public String[] setFeedFile() {
		return null;
	}
	
	@Override
	public void loadFeed() {
		
		try{
			
			ExecutorService executor = Executors.newFixedThreadPool(maxConcurrent);
			List<TipInsureCarThread> targetList = new ArrayList<>();
			
			String[] motorTypeList = {
					"2plus",
					"3plus",
					"first_class",
					"first_class_lady",
			};
			
			String[][][] masterDataAllList 	= new String[1][1500][7];
			masterDataAllList[0] =	TipInsureCarFeedCrawlerInit01.masterData;
			
			Calendar now = Calendar.getInstance();
			int currentYear = now.get(Calendar.YEAR); 
			
			for(int h=0; h<masterDataAllList.length; h++) {
				String[][] masterDataList = masterDataAllList[h];
				for(int i=0; i<masterDataList.length; i++){
					for(int j=0; j<motorTypeList.length; j++){
						for(int k = 0; k<=15; k++){
							
//							{"Priceza_brand", "Priceza_sub_brand", "brand_name", "model_name", "spec_name", "voluntary_code", "More 2000 cc"},
//							{"Alfa Romeo", "156 2.0CC", "ALFA ROMEO", "156", "2.0i", "110", "N"},
							
							String motorType 		= motorTypeList[j];
							if(k >= 10 && motorType.indexOf("first_class") > -1) continue;
							
							String pz_brand			= masterDataList[i][0];
							String pz_subBrand		= masterDataList[i][1];
							String tip_brand 		= masterDataList[i][2]; 
							String tip_model 		= masterDataList[i][3];
							String tip_subModel		= masterDataList[i][4];
							String tip_carType 		= masterDataList[i][5];
							String tip_param110		= masterDataList[i][6];
							String carYear 			= Integer.toString(currentYear - k);
							
							if(StringUtils.isBlank(motorType) || StringUtils.isBlank(tip_brand) || StringUtils.isBlank(tip_model) || 
								StringUtils.isBlank(tip_subModel) || StringUtils.isBlank(tip_carType) || StringUtils.isBlank(carYear)) continue;
							
							String mainParam = "";
							mainParam +=  "username=";							mainParam += "priceza";
							mainParam += "&password=";							mainParam += "Apy6TM8FErY9";
							mainParam += "&brand=";								mainParam += tip_brand;
							mainParam += "&model=";								mainParam += tip_model;
							mainParam += "&spec=";								mainParam += tip_subModel;
							mainParam += "&register_year=";						mainParam += carYear;
							mainParam += "&motor_type=";						mainParam += motorType;
							mainParam += "&voluntary_code=";					mainParam += tip_carType;
							
							List<String> paramList = new ArrayList<>();
							
							if(tip_carType.equals("110")){
								paramList.add(mainParam + "&is_cc_over_2000=" + tip_param110);
							}else if(tip_carType.equals("210")){
								paramList.add(mainParam + "&is_seat_over_15=Y");
								paramList.add(mainParam + "&is_seat_over_15=N");
							}else if(tip_carType.equals("320")){
								paramList.add(mainParam + "&car_weight_over_3=Y");
								paramList.add(mainParam + "&car_weight_over_3=N");
								
							}
							
							for (String p : paramList) {
								TipInsureCarThread t = new TipInsureCarThread(pz_brand, pz_subBrand, tip_subModel, p);
								targetList.add(t);
							}
							
							if(targetList.size() % 100 == 0){
								executor.invokeAll(targetList);
								targetList.clear();
							}
							
						}
					}
				}
			}
			
			executor.invokeAll(targetList);
			executor.shutdown();
			if(executor.awaitTermination(120, TimeUnit.HOURS)){
				System.out.println("Done All.");
			}else{
				System.out.println("Time out.");
				executor.shutdownNow();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(haveResult)
			BaseConfig.FEED_FILE = new String[] {BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"};
	}
	
	class TipInsureCarThread implements Callable<Void>{

		private String currentParam;
		private String pzBrand;
		private String pzSubBrand;
		private String tipSubModel;

		TipInsureCarThread(String pz_brand, String pz_subBrand, String tip_subModel, String param) {
			currentParam = param;
			pzBrand = pz_brand;
			pzSubBrand = pz_subBrand;
			tipSubModel = tip_subModel;
		}
		
		@Override
		public Void call() {
			try {
				String rtnResult = postRequest(apiUrl,currentParam);
				if(StringUtils.isNotBlank(rtnResult)){
					parseJson(pzBrand, pzSubBrand, tipSubModel, StringEscapeUtils.unescapeJava(rtnResult));
//					System.out.println(currentParam);
//					System.out.println(StringEscapeUtils.unescapeJava(rtnResult));
//					System.out.println(FilterUtil.getStringBetween(rtnResult, "{\"success\":", ",") + "\n");
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
			
		}
		
	}
	
	private void parseJson(String pzBrand, String pzSubBrand, String tipSubModel, String data){
		
//		{
//			"success": true,
//			"msg": "Found Data",
//			"product": [{
//				"product_id": "96337",
//				"broker": "",
//				"insurer": "ทิพยประกันภัย",
//				"title": "ประกันรถยนต์ ประเภท 1 สำหรับผู้หญิง",
//				"type": "1",
//				"premium": 26842.02,
//				"brand": "ALFA ROMEO",
//				"model": "156",
//				"sub_model": "2.0i",
//				"cc": "2.0",
//				"year": "2009",
//				"URL": "https://www.tipinsure.com/Motor/motor_step_1/first_class_priceza",
//				"compulsory_insurance": "ไม่มี",
//				"sum_insured": "730000",
//				"fire_damage": "730000",
//				"theft": "730000",
//				"flood_damage": "730000",
//				"deductible": 0,
//				"repair": "ซ่อมอู่",
//				"bodily_injury_liability": "1,000,000",
//				"bodily_injury_liability/Accident": "10,000,000",
//				"property_damage_liability": "5,000,000",
//				"personal_injury_protection": "100,000",
//				"bail_bond": "300,000",
//				"driver_flag": "ไม่ระบุ",
//				"medical_payments": "100,000"
//			}, {...}, {...}, {...}, {...}, {...}]
//		}
		
		try{
			JSONObject jData = (JSONObject)new JSONParser().parse(data);
			Object result = jData.get("result");
			JSONObject jResult = null;
			
			if(result == null){
				jResult = (JSONObject)new JSONParser().parse(data.toString());
			}else{
				jResult = (JSONObject)new JSONParser().parse(result.toString());
			}
			
			JSONArray jarr = (JSONArray)jResult.get("product");
			
			if(jarr == null || jarr.size() == 0) return;
			
			for (Object obj : jarr) {
				JSONObject jObj = (JSONObject)obj;
				                                                                                                        
				String id 			= (String)jObj.get("product_id");                                       // - รหัสสินค้า
				String insurer 		= (String)jObj.get("insurer");                                          // - บริษัทประกัน
				String title		= (String)jObj.get("title");                                            // - ชื่อสินค้า
				String type			= (String)jObj.get("type");                                             // - ชั้นประกัน
				String brand		= (String)jObj.get("brand");                                            // - ยี่ห้อรถ
				String model		= (String)jObj.get("model");                                            // - รุ่นรถ
				String subModel		= (String)jObj.get("sub_model");                                        // - รุ่นย่อย
				String year			= (String)jObj.get("year");                                             // - ปีรถ
				String repair		= (String)jObj.get("repair");                                           // - อู่ซ่อม
				String carAct		= (String)jObj.get("compulsory_insurance");                             // - พรบ.
				String driver_flag	= (String)jObj.get("driver_flag");             			                // - ระบุ / ไม่ระบุผู้ขับขี่

				String fire			= toPriceString((Object)jObj.get("fire_damage"));                       // - คุ้มครองไฟไหม้
				String theft		= toPriceString((Object)jObj.get("theft"));                             // - คุ้มครองโจรกรรม
				String flood		= toPriceString((Object)jObj.get("flood_damage"));                      // - คุ้มครองน้ำท่วม
				String bi			= toPriceString((Object)jObj.get("bodily_injury_liability"));           // - ชีวิตบุคคลภายนอก ต่อคน
				String bt			= toPriceString((Object)jObj.get("bodily_injury_liability/Accident"));  // - ชีวิตบุคคลภายนอก ต่อครั้ง
				String tp			= toPriceString((Object)jObj.get("property_damage_liability"));         // - ทรัพย์สินบุคคลภายนอก
				String pa			= toPriceString((Object)jObj.get("personal_injury_protection"));        // - อุบัติเหตุส่วนบุคคล
				String bb			= toPriceString((Object)jObj.get("bail_bond"));                         // - ประกันตัวผู้ขับขี่
				String me			= toPriceString((Object)jObj.get("medical_payments"));                  // - ค่ารักษาพยาบาล 
			                                                           
				String premium		= toPriceString((Object)jObj.get("premium"));                           // - เบี้ยประกัน
				String od			= toPriceString((Object)jObj.get("sum_insured"));                       // - ทุนประกัน
				String oddd			= toPriceString((Object)jObj.get("deductible"));                        // - ค่าเสียหายส่วนแรก
				
				String url			= (String)jObj.get("URL");                                              // - URL
				
				if(type.equals("5")){ // ชั้น 2+ 3+
					String checkTitle = title.replace(" ", "").replace("พลัส", "plus").replace("บวก", "plus");
					if(checkTitle.indexOf("2plus") > -1){
						type = "2+";
					}else if(checkTitle.indexOf("3plus") > -1){
						type = "3+";
					}else{
						continue;
					}
				}
				
				if(title.indexOf("สำหรับผู้หญิง")>-1 && (driver_flag.equals("ระบุ") || NumberUtils.toInt(oddd) > 0)) {
					continue;
				}
					
				String uniqueText = "";
				uniqueText += id;
				uniqueText += insurer;
				uniqueText += title;
				uniqueText += type;
				uniqueText += brand;
				uniqueText += model;
				uniqueText += subModel;
				uniqueText += year;
				uniqueText += repair;
				uniqueText += carAct;
				uniqueText += driver_flag;
				uniqueText += oddd;
				uniqueText += od;
				uniqueText = toMD5(uniqueText);
				
//		old		ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - (ยี่ห้อรถ) - (รุ่นรถ) - (รุ่นย่อย) - ปี (ปีรถ) (------auto gen-----)
//		new		ประกันชั้น (ชั้นประกัน) - (บริษัทประกัน) - pzBrand - pzSubBrand - tipSubModel - ปี (ปีรถ) (------auto gen-----)
				
				String productName = "";
				productName += "ประกันชั้น " + type;
				productName += " - " + insurer.replace("-", " ");
//		old		productName += " - " + brand.replace("-", " ");
//		old		productName += " - " + model.replace("-", " ");
//		old		productName += " - " + ((StringUtils.isNotBlank(subModel))? subModel.replace("-", " ") : "ไม่ระบุรุ่นย่อย");
				productName += " - " + pzBrand.replace("-", " ");
				productName += " - " + pzSubBrand.replace("-", " ");
				productName += " - " + ((StringUtils.isNotBlank(tipSubModel))? tipSubModel.replace("-", " ") : "ทุกรุ่น");
				productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
				productName += " (" + uniqueText.substring(0,10) + ")";
				productName = productName.replace(",", "");
				productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
				productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
				productName = productName.replaceAll("\\s+", " ");                                                  
				productName = productName.replace("( ", "(");                                                       
				productName = productName.replace(" )", ")");                                                       
				productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
				productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");   				
				
//				( แผน ) - (ซ่อม) - ทุนประกัน (xxx) - ค่าเสียหายส่วนแรก (xxx) - (พรบ.) - คุ้มครองไฟไหม้ (xxx) - คุ้มครองโจรกรรม (xxx)- คุ้มครองน้ำท่วม (xxx)- ชีวิตบุคคลภายนอกต่อคน (xxx)- ชีวิตบุคคลภายนอกต่อครั้ง (xxx)- ทรัพย์สินบุคคลภายนอก (xxx)- อุบัติเหตุส่วนบุคคล (xxx)- ประกันตัวผู้ขับขี่ (xxx)- ค่ารักษาพยาบาล (xxx)
				String productDesc = "";
				productDesc += ((StringUtils.isBlank(title))?"ไม่ระบุแผน": title + " " +(title.indexOf("ระบุผู้ขับขี่")>-1?"":(((StringUtils.isNotBlank(driver_flag))?driver_flag:"ไม่ระบุ")+"คนขับ"))); 
				productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(repair))? repair : " ไม่ระบุอู่/ห้าง");
				productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
				productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : oddd ));
				productDesc += " - " + ((StringUtils.isBlank(carAct) || carAct.contains("ไม่")) ? "ไม่รวมพรบ." : "รวมพรบ.");
				productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
				productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
				productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0"))? flood : "ไม่ระบุ" ); 
				productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
				productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
				productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
				productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
				productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );         
				productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
				productDesc = productDesc.replaceAll("\\s+", " ");                                               
				productDesc = productDesc.replace("( ", "(");                                                    
				productDesc = productDesc.replace(" )", ")");                                                    
				productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
				productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
				productDesc = productDesc.replace("ซ่อมซ่อม", "ซ่อม");                         
				productDesc = productDesc.replace(",", "");
				
				String productPrice = premium;
				
				String productUrl = url;
				String productUrlForUpdate = uniqueText;
				String productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-55651.jpg";
				
				ProductDataBean pdb = new ProductDataBean();
				pdb.setName(productName);
				pdb.setPrice(FilterUtil.convertPriceStr(productPrice));
				pdb.setPictureUrl(productImage);
				pdb.setDescription(productDesc);
				pdb.setUrl(productUrl);
				pdb.setUrlForUpdate(productUrlForUpdate);
				pdb.setCategoryId(250101);
				pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;
				
				String mockResult = mockResult(pdb);
				if(StringUtils.isNotBlank(mockResult)) {
					try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(BaseConfig.FEED_STORE_PATH + "/mockFeed.xml"),  true));){
						pwr.println(mockResult);
						haveResult = true;
					}catch(Exception e) {
						logger.error(e);
					}
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return;
	}
	
	private String postRequest(String url, String requestParam) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	try {
	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(false);
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 	
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(requestParam.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		isr = new InputStreamReader(conn.getInputStream());
	 		brd = new BufferedReader(isr);
	 		StringBuilder rtn = new StringBuilder(5000);
	 		String line = "";
	 		while ((line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 		return rtn.toString();
	 	} catch (Exception e) {
	 		e.printStackTrace();
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
		return null;
	 	
	}
	
	private String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	private String toPriceString(Object obj){
		try{
			return formatPrice.format(Double.parseDouble(FilterUtil.removeCharNotPrice(obj.toString())));
		}catch(Exception e){
			return "0";
		}
	}
		private String mockResult(ProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrl()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}

	public ProductDataBean parse(String xml) {
		
		String productName 			= FilterUtil.getStringBetween(xml, "<name>", "</name>").trim();
		String productDesc 			= FilterUtil.getStringBetween(xml, "<desc>", "</desc>").trim();
		String productPictureUrl 	= FilterUtil.getStringBetween(xml, "<pictureUrl>", "</pictureUrl>").trim();
		String productUrl 			= FilterUtil.getStringBetween(xml, "<url>", "</url>").trim();
		String productUrlForUpdate	= FilterUtil.getStringBetween(xml, "<urlForUpdate>", "</urlForUpdate>").trim();
		String productPrice 		= FilterUtil.getStringBetween(xml, "<price>", "</price>").trim();
		String productBasePrice 	= FilterUtil.getStringBetween(xml, "<basePrice>", "</basePrice>").trim();
		String productCatId 		= FilterUtil.getStringBetween(xml, "<categoryId>", "</categoryId>").trim();
		String productKeyword 		= FilterUtil.getStringBetween(xml, "<keyword>", "</keyword>").trim();
		String productRealProductId	= FilterUtil.getStringBetween(xml, "<realProductId>", "</realProductId>").trim();
		String productUpc			= FilterUtil.getStringBetween(xml, "<upc>", "</upc>").trim();

		if(productUrl.isEmpty() || productName.isEmpty() || productPrice.isEmpty()) {
			return null;		
		}
		
		ProductDataBean pdb = new ProductDataBean();		
		pdb.setName(productName);
		
		if(StringUtils.isNotBlank(productDesc)) 		
			pdb.setDescription(productDesc);
		
		if(StringUtils.isNotBlank(productPictureUrl)) 	
			pdb.setPictureUrl(productPictureUrl);
		
		pdb.setUrl(productUrl);

		if(StringUtils.isNotBlank(productUrlForUpdate)) 
			pdb.setUrlForUpdate(productUrlForUpdate);
		else 											
			pdb.setUrlForUpdate(productUrl);
		
		pdb.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));		
		
		if(StringUtils.isNotBlank(productBasePrice) && !productBasePrice.equals("0"))
			pdb.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		
		if(StringUtils.isNotBlank(productCatId) && NumberUtils.isCreatable(productCatId) && !productCatId.equals("0"))
			pdb.setCategoryId(Integer.parseInt(productCatId));
		
		if(StringUtils.isNotBlank(productKeyword))
			pdb.setKeyword(productKeyword);
		
		if(StringUtils.isNotBlank(productRealProductId)) 
			pdb.setRealProductId(productRealProductId);
		
		if(StringUtils.isNotBlank(productUpc))
			pdb.setUpc(productUpc);
		
		return pdb;
	}


}
