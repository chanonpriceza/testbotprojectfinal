package feed.crawler;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import bean.ProductDataBean;
import engine.BaseConfig;
import feed.FeedManager;
import utils.BotUtil;
import utils.FTPUtil;
import utils.FilterUtil;

public class NocNocApiFeedCrawler extends FeedManager {
	
	private final String SFTP_HOST = "27.254.82.243";
    private final int    SFTP_PORT = 98;
    private final String SFTP_USER = "sale";
    private final String SFTP_PASS = "Sale@pr1ceza#";
    private final String SFTP_FDIR = "/home/sale/300617-nocnoc";
    private final String FEED_USER = "bot";
    private final String FEED_PASS = "pzad4r7u";

	public NocNocApiFeedCrawler() throws Exception {
		super();
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			String destination = BaseConfig.FEED_STORE_PATH + "/" + fileName;
			FTPUtil.downloadFileWithLogin(feed, destination, FEED_USER, FEED_PASS);
		}
	}

	@Override
	public String[] setFeedFile() {
		List<String> allFile = new ArrayList<>();
		try {
			List<String> feedOfflineFiles = FTPUtil.sftpGetFileNameList(SFTP_FDIR, SFTP_HOST, SFTP_PORT, SFTP_USER, SFTP_PASS);
			for (String fileName : feedOfflineFiles) {
  				if(fileName.indexOf(String.valueOf(BaseConfig.MERCHANT_ID)) > -1) {
  					String offlineFeed = "http://"+SFTP_HOST+":8081/300617-nocnoc/"+fileName;
  					allFile.add(offlineFeed);
  				}
  			}
		} catch (IOException e) {
			logger.error(e);
		}
		if(allFile!= null && allFile.size() > 0) {
			String[] result = new String[allFile.size()];
			return allFile.toArray(result);
		}
		return null;
	}
	
	
	@Override
	public ProductDataBean parse(CSVRecord data) {

		DecimalFormat formatPrice = new DecimalFormat("###");
		
		String name 		=data.get("name"); 
		String id 			=data.get("id"); 
		String url			=data.get("url"); 
		String image		=data.get("image_url"); 
		String cat 			=data.get("category_id"); 
		//String catName =data.get("category_name"); 
		String desc 		=data.get("description"); 
		String basePrice	=data.get("base_price"); 
		String price 		=data.get("price");
		String invStatus	=data.get("inv_status");
		
		if(StringUtils.isNotBlank(invStatus) && invStatus.contains("out of stock")) {
			return null;
		}
		
		price = FilterUtil.removeCharNotPrice(price);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		
		
		double temp_price = 0;
		String id_temp = (StringUtils.isNotBlank(id))?" ("+id+")":"";
		
		double price_d = FilterUtil.convertPriceStr(price);
		double base_d = FilterUtil.convertPriceStr(basePrice);
		
		price = formatPrice.format(price_d);
		basePrice = formatPrice.format(base_d);
		
		price_d = FilterUtil.convertPriceStr(price);
		base_d = FilterUtil.convertPriceStr(basePrice);
		
		if(price_d == 0 && base_d != 0) {
			price_d = base_d;
		}
		
		
		if(price_d > base_d) {
			temp_price = price_d;
			price_d = base_d;
			base_d = temp_price;
		}
		
		url = url.replaceAll("\r\n", " ").replaceAll("\\s+", " ");
		
		ProductDataBean pdb = new ProductDataBean();
		pdb.setName(name+id_temp);
		pdb.setRealProductId(id);
		pdb.setUrl(url);
		pdb.setPrice(price_d);
		pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice));
		pdb.setDescription(removeIllegaCharactor(desc,new String[]{"th","eng"}));
		if(BaseConfig.CONF_FEED_CRAWLER_PARAM==null||!BaseConfig.CONF_FEED_CRAWLER_PARAM.equalsIgnoreCase("skipImage")&&BaseConfig.RUNTIME_TYPE.equals("DUE")) {
			pdb.setPictureUrl(image);
		}
		pdb.setUrlForUpdate(id);
		
		String[] mapping = getCategory(cat);
		if(mapping != null) {
			pdb.setCategoryId(BotUtil.stringToInt(mapping[0], 0));
			pdb.setKeyword(mapping[1]);				
		} else {
			pdb.setCategoryId(0);
			//dupCat.put(cat,cat+","+catName+","+name+","+url);
		}
		
		return pdb;
	}
	
	
	public static String removeIllegaCharactor(String inputString, String[] allowList) {
		StringBuilder build = new StringBuilder();
		for (char splitChar : inputString.toCharArray()) {
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							build.append(splitChar);
						}
					}
				}
		}
		return build.toString();
	}
	
	private static  Map<String, Map<Integer, Integer>> languageCharacterMap = createLanguageCharacterMap();
	
	static {
		languageCharacterMap = createLanguageCharacterMap();
	}
	
    private static Map<String, Map<Integer, Integer>> createLanguageCharacterMap(){
    	Map<String, Map<Integer, Integer>> maps = new HashMap<>();
    	maps.put("th", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0E00, 0x0E5B);
			}
		});
    	maps.put("eng", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0041, 0x005A);
				put(0x0061, 0x007A);
				put(0x0030, 0x0039);
			}
		});
		maps.put("viet", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0xc0, 0xc3);
				put(0xc8, 0xca);
				put(0xcc, 0xcd);
				put(0xd2, 0xd5);
				put(0xd9, 0xda);
				put(0xdd, 0xdd);
				put(0xe0, 0xe3);
				put(0xe8, 0xea);
				put(0xec, 0xed);
				put(0xf2, 0xf5);
				put(0xf9, 0xfa);
				put(0xfd, 0xfd);
				put(0x102, 0x103);
				put(0x110, 0x111);
				put(0x128, 0x129);
				put(0x168, 0x169);
				put(0x1a0, 0x1a1);
				put(0x1af, 0x1b0);
				put(0x1ea0, 0x1ef9);
			}
		});
		maps.put("chinese", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x4E00, 0x9FD5);
			}
		});
    	return maps;
    }
    
    public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
		if ((i >= minValueInclusive && i <= maxValueInclusive))
			return true;
		else
			return false;
	}
	
}
