package feed;

import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ReportBean;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import product.processor.ParserUpdate;

public class UpdateProcessor implements ProcessorInterface{
	private static final Logger logger = LogManager.getRootLogger();
	private FeedManager feedManager;
	private CountDownLatch latch;
	private TypeFeedCrawlerInterface c;
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private ProcessLogManager processLog  = ProcessLogManager.getInstance();
	private ReportBean report = ReportManager.getInstance().getReport();
	
	@Override
	public void setProperties(FeedManager feedManager, CountDownLatch latch) {
		this.feedManager = feedManager;
		this.latch = latch;
		if(BaseConfig.FEED_TYPE_CONF == null) {
			logger.info("FEED_TYPE_CONF IS NULL");
			return;
		}
		switch (BaseConfig.FEED_TYPE_CONF) {
		case CSV:
			c = CSVFeedCrawler.getInstance();
			break;
		case XML:
			c = XMLFeedCrawler.getInstance();
			break;
		case JSON:
			c = JSONFeedCrawler.getInstance();
			break;
		}
	}
	
	@Override
	public Boolean call() {
		int exceptionCount = 0;
		boolean result = true;
		try {
    		while(!Thread.currentThread().isInterrupted() && exceptionCount < 10) {
    			try {
	    			ProductDataBean pdb = c.readData(feedManager);
	    			if(pdb == null)
	    				break;
	    			ParserResultBean rb = ParserUpdate.analyzeProduct(pdb);
	 
	        		if(rb != null) {
	        			if(rb.isExpire()) {
	        				report.increaseExpire();
	        			}else {
	        				ParserUpdate.update(rb);	        				
	        			} 
	        		}
	        		exceptionCount = 0;
    			} catch (SQLException | IllegalStateException e) {
    				result = false;
    				exceptionCount++;
    				e.printStackTrace();
    				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
    			}
	    	}
		} catch(Exception e) {
			result = false;
			logger.error("", e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		} finally {
			try{
				latch.countDown();
				logger.info("UpdateProcessor : latch countdown.");
			}catch(Exception e) {
				result = false;
				logger.error("", e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
		return result;
	}
}
