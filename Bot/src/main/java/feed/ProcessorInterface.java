package feed;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public interface ProcessorInterface extends Callable<Boolean>{
	void setProperties(FeedManager feedManager, CountDownLatch latch);
}
