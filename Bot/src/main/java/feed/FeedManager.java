package feed;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.Authenticator;
import java.net.URL;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import bean.ReportBean;
import bean.SendDataBean;
import bean.UrlPatternBean;
import db.MerchantDB;
import db.ProductDataDB;
import db.SendDataDB;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import product.processor.ProductFilter;
import product.processor.ProductPreparing;
import utils.BotUtil;

public abstract class FeedManager implements CallBack{
	public abstract String[] setFeedFile();
	protected static final Logger logger = LogManager.getRootLogger();
	private MerchantDB merchantDB;
	private ReportBean report;
	private ProductDataDB productDataDB;
	private SendDataDB sendDataDB;
	private Map<String, String[]> categoryMap = new HashMap<>();
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	protected ProcessLogManager processLog  = ProcessLogManager.getInstance();
	private boolean success = true; 
	private  Set<Integer> categoryList;
	
	public FeedManager() throws Exception {
		BaseConfig.FEED_FILE = setFeedFile();
		getCategoryMapping();
		merchantDB = DatabaseManager.getInstance().getMerchantDB();
		productDataDB = DatabaseManager.getInstance().getProductDataDB();
		sendDataDB = DatabaseManager.getInstance().getSendDataDB();
		report = ReportManager.getInstance().getReport();
		run();
	}

	public boolean run() {
		try {
			clearFeed();
			if(!BaseConfig.IS_DELETE_ALL&&!BaseConfig.IS_DELETE_NOT_MAP){
				merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_LOAD, MerchantBean.STATUS.RUNNING);
				loadFeed();
				merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_LOAD, MerchantBean.STATUS.FINISH);
				ProductPreparing.setRunning();
				merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.RUNNING);
				readFeed();
			}else {
				ProductPreparing.setRunning();
				merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_RUN, MerchantBean.STATUS.FINISH);
			}		
			
			finishRunning();
		}catch(RuntimeException e) {
			logger.error(e);
			success = false;
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		}catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
			success = false;
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		}
		return isSuccess();

	
	}
	
	public void clearFeed() throws SQLException {
		logger.info("Feed : Delete old feed");
		Arrays.stream(new File(BaseConfig.FEED_STORE_PATH).listFiles()).forEach(File::delete);
	}
	
	@Override
	public void loadFeed() {
		for (String feed : BaseConfig.FEED_FILE) {
			String fileName = BotUtil.getFileNameURL(feed);
			if(feed.isEmpty()) {
				logger.error("Feed | Cannot Load | " + feed);
				continue;
			}
			logger.info("Feed : Load : " + feed);
			
			if(StringUtils.isNotBlank(BaseConfig.CONF_FEED_USERNAME) && StringUtils.isNotBlank(BaseConfig.CONF_FEED_PASSWORD))
				Authenticator.setDefault(new FeedAuthenticator(BaseConfig.CONF_FEED_USERNAME, BaseConfig.CONF_FEED_PASSWORD));
			
			Path path = new File(BaseConfig.FEED_STORE_PATH + "/" + fileName).toPath();
			try (InputStream in = new URL(feed).openStream()) {
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			}catch (ClosedByInterruptException e) {
				logger.error("Closed By Kill Commad");
				return;
			}catch (IOException e) {
				e.printStackTrace();
				logger.error(e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
	}
	
	public void readFeed() throws SQLException {
		logger.info("Feed : Type : "+ BaseConfig.FEED_TYPE_CONF);
		logger.info("Feed : Thread : "+ BaseConfig.THREAD_NUM);
		CountDownLatch latch = new CountDownLatch(BaseConfig.THREAD_NUM);
		ExecutorService parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
		List<Future<?>> parserProcess = new ArrayList<>();
		try {
			for(int i = 0; i < BaseConfig.THREAD_NUM; i++) {
				ProcessorInterface processor = null;
				try {
					processor = (ProcessorInterface)Class.forName(BaseConfig.FEED_PROCESSOR).getDeclaredConstructor().newInstance();
					processor.setProperties(this, latch);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					logger.error(e);
					processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
				}
				parserProcess.add(parserThread.submit(processor));
			}
			parserThread.shutdown();
			latch.await();
		}catch(InterruptedException e) {
			if(parserProcess != null && parserProcess.size() > 0)
				for (Future<?> process : parserProcess) 
					process.cancel(true);
			logger.error(e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		}
	}
	
	public void finishRunning() throws SQLException {
		merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_FIND_INACTIVE, MerchantBean.STATUS.RUNNING);
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			report.setCountNew(report.getCountOld() + report.getAdd());
			return;
		}
		
		logger.info("Feed : Set finish running");
		final int offset = 1000;
		int countDelete = 0;
		boolean log = true;
		delete:while(!Thread.currentThread().isInterrupted()) {
			List<ProductDataBean> list = productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, offset);
			for(ProductDataBean b : list) {
				int id = b.getId();
				int errorUpdateCount = b.getErrorUpdateCount();
				int oldCategory = b.getCategoryId();
				errorUpdateCount++;
				report.increaseErrorCount();
				
				if(ProductFilter.checkCOVIDInstantDelete(b.getName()) || checkDeleteCondition(errorUpdateCount,oldCategory)) {
					SendDataBean sendDataBean = SendDataBean.createSendDataBean(b, null, SendDataBean.ACTION.DELETE);
					sendDataDB.insertSendData(sendDataBean);
					productDataDB.delete(id);
					
					countDelete++;
					report.increaseDelete();
					log = true;
				}else{
					if(!BaseConfig.IS_DELETE_NOT_MAP) {
						productDataDB.updateStatusAndErrorUpdateCount(id, STATUS.E, errorUpdateCount);
					}
				}
				
				if(countDelete >= BaseConfig.CONF_DELETE_LIMIT) {
					int remain = productDataDB.countByStatus(BaseConfig.MERCHANT_ID, STATUS.W);
					remain += report.getErrorCount();
					report.setErrorCount(remain);
					String msg = "";
					msg = "exceed delete limit = " + BaseConfig.CONF_DELETE_LIMIT;
					merchantDB.updateActiveErrorMessage(BaseConfig.MERCHANT_ID, 2, msg);
					logger.warn("Feed : Exceed delete limit :" + BaseConfig.CONF_DELETE_LIMIT);
					break delete;
				}
				
				if(log && countDelete > 0 && countDelete % 1000 == 0) {
					log = false;
					logger.info("Feed : Delete : "+ countDelete);
				}
			}
			if(list.size() < offset)
				break;
		}
		logger.info("Feed : Finish delete : "+ countDelete);
		merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_FIND_INACTIVE, MerchantBean.STATUS.FINISH);
	}
	
	@Override
	public ProductDataBean process(String data) {
		return parse(data);
	}
	
	@Override
	public ProductDataBean process(CSVRecord data) {
		return parse(data);
	}
	
	@Override
	public ProductDataBean process(JSONObject data) {
		return parse(data);
	}
	
	public void getCategoryMapping() throws Exception {
		categoryMap = setCategoryMapping();
		if(BaseConfig.IS_DELETE_NOT_MAP)
			initialCatList();	
	}
	
	public String[] getCategory(String merchantCat) {
		if(merchantCat == null)
			return null;
		return categoryMap.get(merchantCat);
	}
	
	public Set<String> getCategoryKeySet() {
		return categoryMap.keySet();
	}
	
	public Map<String, String[]> setCategoryMapping() throws SQLException {
    	UrlPatternDB urlPatternDB = DatabaseManager.getInstance().getUrlPatternDB();
    	List<UrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED");
    	Map<String, String[]> categoryMap = new HashMap<>();
    	if(categoryMapping.isEmpty())
    		return categoryMap;
    	
    	for(UrlPatternBean cat : categoryMapping){
    		String merchantCat = cat.getValue();
    		if(isBlank(merchantCat))
    			continue;
    		
    		String pzCatId = Integer.toString(cat.getCategoryId());
    		String pzKeyword = cat.getKeyword();
    		String[] mapValue = new String[]{ pzCatId, pzKeyword};
    		categoryMap.put(merchantCat, mapValue);
    	}
    	return categoryMap;
    }
	
	private void initialCatList() throws Exception {
		UrlPatternDB urlPatternDB = DatabaseManager.getInstance().getUrlPatternDB();
		List<UrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "FEED");
		categoryList = new HashSet<Integer>();
		categoryList = categoryMapping.stream().map(UrlPatternBean::getCategoryId).collect(Collectors.toSet());
	}
	
	private boolean checkDeleteCondition(int errorUpdateCount,int oldCat) throws SQLException {
		boolean result;
		boolean isNonCat;
		result = BaseConfig.CONF_FORCE_DELETE || errorUpdateCount >= BaseConfig.CONF_PRODUCT_UPADTE_LIMIT || BaseConfig.IS_DELETE_ALL;
		
		if(BaseConfig.IS_DELETE_NOT_MAP) {
			isNonCat = !(categoryList.contains(oldCat))&&categoryList.size()>0;
			result = result || isNonCat;
		}
		
		return result;
	}

	public boolean isSuccess() {
		return success;
	}
	
}
