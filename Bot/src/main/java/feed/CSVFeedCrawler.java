package feed;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import unittest.MockObjectHolder;
import utils.BotUtil;

public class CSVFeedCrawler implements TypeFeedCrawlerInterface{
	private static final Logger logger = LogManager.getRootLogger();
	private InputStream ins;
	private GZIPInputStream gzip;
	private InputStreamReader isr;
	private Iterator<CSVRecord> record;
	private CSVParser parser;
	private CSVFormat format;
	private int CURRENT_FILE = -1;
	private int HEADER_SIZE;
	private int PROCESS;
	private boolean CHECKOUT;
	
	private static class SingletonHelper {
        private static final CSVFeedCrawler INSTANCE = new CSVFeedCrawler();
    }
	
	public static CSVFeedCrawler getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.csvFeedCrawler;
		}
		return SingletonHelper.INSTANCE;
	}

	@Override
	public boolean readFile() {
		if(parser != null && record.hasNext())
			return true;
		
		while(!Thread.currentThread().isInterrupted()) {
			close();
			if(BaseConfig.FEED_FILE.length - CURRENT_FILE == 1)
				return false;
			String url = BaseConfig.FEED_FILE[++CURRENT_FILE];
			String feed = BaseConfig.FEED_STORE_PATH + "/" + StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			BaseConfig.FEED_READING = StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			try {
				File file = new File(feed);
				logger.info("Start read Feed : "+feed);
				ins = new FileInputStream(file);
				if(BotUtil.isGZip(file))
					gzip = new GZIPInputStream(ins);
				isr = new InputStreamReader(gzip != null ? gzip : ins, BaseConfig.CONF_PARSER_CHARSET);	
				
				format = CSVFormat.DEFAULT.withFirstRecordAsHeader();
				if(BotUtil.checkStringEqual(StringUtils.defaultString(BaseConfig.CONF_FEED_CSV_FORMAT).toUpperCase(), "TDF")) format = CSVFormat.TDF.withFirstRecordAsHeader();
				if(BotUtil.checkStringEqual(StringUtils.defaultString(BaseConfig.CONF_FEED_CSV_FORMAT).toUpperCase(), "EXCEL")) format = CSVFormat.EXCEL.withFirstRecordAsHeader();
				
				parser = new CSVParser(isr, format);
				record = parser.iterator();
				HEADER_SIZE = parser.getHeaderMap().size();
				if(record.hasNext())
					return true;
			}catch (IOException e) {
				logger.error("Feed : Read Error : "+ feed, e);
			}
		}
		return false;
	}
	
	public void close() {
		if(parser != null)
			try { parser.close();} catch(Exception e) {};
		if(isr != null)
			try { isr.close();} catch(Exception e) {};
		if(gzip != null)
			try { gzip.close();} catch(Exception e) {};
		if(ins != null)
			try { ins.close();} catch(Exception e) {};
	}
	
	@Override
	public synchronized ProductDataBean readData(CallBack callback) {
		try {
			while(!Thread.currentThread().isInterrupted()) {			
				if(CHECKOUT)
					return null;
				
				if(!readFile()) {
					logger.info("Feed : Finish Process : "+ PROCESS);
					CHECKOUT = true;
					return null;
				}
				
				PROCESS++;
				if(PROCESS % 100000 == 0)
					logger.info("Feed : Parse process : "+ PROCESS);
				
				CSVRecord product = record.next();
				if(product.size() != HEADER_SIZE)
					continue;
				
				ProductDataBean b = callback.process(product);
				if(b != null)
					return b;
			}
		} catch(Exception e) {
			logger.error("Feed : Read Data Error : ", e);
		}
		return null;
	}

	
}
