package feed;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FeedAuthenticator extends Authenticator {
	
	private static final Logger logger = LogManager.getRootLogger();
	private String username, password;

	public FeedAuthenticator(String user, String pass) {
		username = user;
		password = pass;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		logger.info("Requesting Host  : " + getRequestingHost());
		logger.info("Requesting Port  : " + getRequestingPort());
		logger.info("Requesting Prompt : " + getRequestingPrompt());
		logger.info("Requesting Protocol: " + getRequestingProtocol());
		logger.info("Requesting Scheme : " + getRequestingScheme());
		logger.info("Requesting Site  : " + getRequestingSite());
		return new PasswordAuthentication(username, password.toCharArray());
	}
}
