package feed;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.zip.GZIPInputStream;

import static org.apache.commons.collections4.CollectionUtils.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import engine.BaseConfig;
import unittest.MockObjectHolder;
import utils.BotUtil;
import utils.FilterUtil;

public class XMLFeedCrawler implements TypeFeedCrawlerInterface{
	private static final Logger logger = LogManager.getRootLogger();
	private InputStream ins;
	private GZIPInputStream gzip;
	private InputStreamReader isr;
	private char[] buffer;
	private String tmp = "";
	private List<String> products;
	private int CURRENT_FILE = -1;
	private int PROCESS;
	private boolean CHECKOUT;
	
	private static class SingletonHelper {
        private static final XMLFeedCrawler INSTANCE = new XMLFeedCrawler();
    }
	
	public static XMLFeedCrawler getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.xmlFeedCrawler;
		}
		return SingletonHelper.INSTANCE;
	}
	
	public boolean readFile() {
		buffer = new char[8192];
		try {
			if(isr != null && isr.read(buffer) > -1)
				return true;
		}catch(Exception e) {
			logger.error(e);
		}
		
		while(!Thread.currentThread().isInterrupted()) {
			close();
			if(BaseConfig.FEED_FILE.length - CURRENT_FILE == 1)
				return false;
			String url = BaseConfig.FEED_FILE[++CURRENT_FILE];
			String feed = BaseConfig.FEED_STORE_PATH + "/" + StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			BaseConfig.FEED_READING = StringUtils.defaultIfBlank(BotUtil.getFileNameURL(url), BotUtil.getFileNameDirectory(url));
			try {
				File file = new File(feed);
				logger.info("Start read Feed : "+feed);
				ins = new FileInputStream(file);
				if(BotUtil.isGZip(file))
					gzip = new GZIPInputStream(ins);
				isr = new InputStreamReader(gzip != null ? gzip : ins, BaseConfig.CONF_PARSER_CHARSET);
				if(isr.read(buffer) > -1)
					return true;
			}catch (IOException e) {
				logger.error("Feed : Read Error : "+ feed, e);
			}
		}
		return false;		
	}
	
	public void close() {
		if(isr != null)
			try { isr.close();} catch(Exception e) {};
		if(gzip != null)
			try { gzip.close();} catch(Exception e) {};
		if(ins != null)
			try { ins.close();} catch(Exception e) {};
	}
			
	@Override
	public synchronized ProductDataBean readData(CallBack callback) {
		while(!Thread.currentThread().isInterrupted()) {
			while(isNotEmpty(products)) {
				String product = products.get(0);
				products.remove(0);
				ProductDataBean b = callback.process(product);
				PROCESS++;
				if(PROCESS % 100000 == 0)
					logger.info("Feed : Parse process : "+ PROCESS);
				if(b != null)
					return b;
			}
			
			if(CHECKOUT)
				return null;
			
			if(!readFile()) {
				logger.info("Feed : Finish Process : "+ PROCESS);
				CHECKOUT = true;
				return null;
			}
			
			tmp += new String(buffer);
			products = FilterUtil.getAllStringBetween(tmp, BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG, BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG);
			if(isNotEmpty(products))
				tmp = FilterUtil.getStringAfterLastIndex(tmp, BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG, tmp);
		}
		return null;
	}
}