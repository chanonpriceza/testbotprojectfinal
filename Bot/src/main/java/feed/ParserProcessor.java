package feed;

import static org.apache.commons.lang3.StringUtils.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import bean.ReportBean;
import bean.SendDataBean;
import db.ProductDataDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import product.processor.ParserUpdate;
import utils.BotUtil;
import utils.FilterUtil;

public class ParserProcessor implements ProcessorInterface{
	private static final Logger logger = LogManager.getRootLogger();
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private FeedManager feedManager;
	private CountDownLatch latch;
	private ProductDataDB productDataDB = DatabaseManager.getInstance().getProductDataDB();
	private SendDataDB sendDataDB = DatabaseManager.getInstance().getSendDataDB();
	private ReportBean report = ReportManager.getInstance().getReport();
	private ProcessLogManager processLog  = ProcessLogManager.getInstance();
	private TypeFeedCrawlerInterface c;
	
	@Override
	public void setProperties(FeedManager feedManager, CountDownLatch latch) {
		this.feedManager = feedManager;
		this.latch = latch;
		if(BaseConfig.FEED_TYPE_CONF==null) {
			logger.info("FEED_TYPE_CONF IS NULL");
			return;
		}
		switch (BaseConfig.FEED_TYPE_CONF) {
		case CSV:
			c = CSVFeedCrawler.getInstance();
			break;
		case XML:
			c = XMLFeedCrawler.getInstance();
			break;
		case JSON:
			// IMPORTANT : use for json without sub object inside only
			c = JSONFeedCrawler.getInstance();
			break;
		}
	}
	
	@Override
	public Boolean call(){
		int exceptionCount = 0;
		boolean result = true;
		try {
    		while(!Thread.currentThread().isInterrupted() && exceptionCount < 10) {
    			try {
	    			ProductDataBean pdb = c.readData(feedManager);
	    			if(pdb == null)
	    				break;
		    		if(analyzeProduct(pdb))
		    			insert(pdb);
		    		exceptionCount = 0;
    			}catch (SQLException | IllegalStateException e) {
    				result = false;
    				exceptionCount++;
    				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
    			}
	    	}
		} catch (Exception e) {
			result = false;
			logger.error("", e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);		
		} finally {
			try{
				latch.countDown();
				logger.info("ParserProcessor : latch countdown.");
			}catch(Exception e) {
				result = false;
				logger.error("", e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
		return result;
	}
	
	private boolean analyzeProduct(ProductDataBean pdb) throws SQLException {
		if (pdb == null)
			return false;
		
		if(pdb.isExpire()) {
			report.increaseExpire();
			return false;
		}
		String name = pdb.getName();
		String url = pdb.getUrl();
		double price = pdb.getPrice();
		if(isBlank(name) || price < 1 || isBlank(url)
				|| !FilterUtil.checkAllowCharacter(name,BotUtil.LOCALE) || !FilterUtil.checkAllowCharacter(url,BotUtil.LOCALE)) {
			if(isBlank(name))
				processLog.addNameIsBlank((StringUtils.isBlank(name)?"":name)+" "+(StringUtils.isBlank(pdb.getUrl())?"":pdb.getUrl())+" "+(StringUtils.isBlank(pdb.getRealProductId())?"":"("+pdb.getRealProductId()+")"),name);
			if(price==0)
				processLog.addPriceZero((StringUtils.isBlank(name)?"":name)+" "+(StringUtils.isBlank(pdb.getUrl())?"":pdb.getUrl())+" "+(StringUtils.isBlank(pdb.getRealProductId())?"":"("+pdb.getRealProductId()+")"),String.valueOf(price));
			if(isBlank(url))
				processLog.addUrlIsBlank((StringUtils.isBlank(name)?"":name)+" "+(StringUtils.isBlank(pdb.getUrl())?"":pdb.getUrl())+" "+(StringUtils.isBlank(pdb.getRealProductId())?"":"("+pdb.getRealProductId()+")"),url);
			return false;
		}

		ParserUpdate.convertContent(pdb);
		
		ProductDataBean b = null;
		if(BaseConfig.CONF_IGNORE_NAME_CHANGE) 
			b = productDataDB.getProductByUrlForUpdate(BaseConfig.MERCHANT_ID, StringUtils.defaultIfBlank(pdb.getUrlForUpdate(), url));
		else
			b = productDataDB.getProduct(BaseConfig.MERCHANT_ID, name);
		
		if(b != null) {
			report.increaseDuplicate();
			processLog.addDuplicateName(b.getName(),name);
			return false;
		}
		
		pdb.setMerchantId(BaseConfig.MERCHANT_ID);
		if (BaseConfig.CONF_CURRENCY_RATE != 1.0) {
			price = price * BaseConfig.CONF_CURRENCY_RATE;
			price = Double.valueOf(BotUtil.FORMAT_PRICE.format(price));
			pdb.setPrice(price);

			double basePrice = pdb.getBasePrice();
			if(basePrice > 0) {
				basePrice = basePrice * BaseConfig.CONF_CURRENCY_RATE;
				basePrice = Double.valueOf(BotUtil.FORMAT_PRICE.format(basePrice));
				pdb.setBasePrice(basePrice);
			}
		}
		
		if(price > BotUtil.CONTACT_PRICE)
			pdb.setPrice(BotUtil.CONTACT_PRICE);
		
		if(isBlank(pdb.getUrlForUpdate()))
			pdb.setUrlForUpdate(url);
		
		pdb.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		
		return true;
	}
	
	private void insert(ProductDataBean pdb) throws SQLException {
		byte[] imageByte = null;
		String pictureUrl = pdb.getPictureUrl();
		if(isNotBlank(pictureUrl)) {
    		imageByte = BaseConfig.IMAGE_PARSER.parse(pictureUrl, BaseConfig.IMAGE_WIDTH, BaseConfig.IMAGE_HEIGHT);
    		if(imageByte == null) {
    			logger.warn("Feed : Cannot Parse Image : " + pictureUrl);
    			pdb.setPictureUrl(null);
    		}
    	}
		
		int insert = productDataDB.insertProductData(pdb);
		if(insert > 0) {
			SendDataBean sendDataBean = SendDataBean.createSendDataBean(pdb, imageByte, SendDataBean.ACTION.ADD);
			sendDataDB.insertSendData(sendDataBean);
			report.increaseAdd();
		}
	}
}
