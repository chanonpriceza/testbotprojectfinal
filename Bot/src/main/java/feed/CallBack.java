package feed;

import org.apache.commons.csv.CSVRecord;
import org.json.simple.JSONObject;

import bean.ProductDataBean;

public interface CallBack {
	
	default void loadFeed() {
		return ;
	}
	
	default ProductDataBean process(String data) {
		return null;
	}
	
	default ProductDataBean process(CSVRecord data) {
		return null;
	}
	
	default ProductDataBean process(JSONObject data) {
		return null;
	}
	
	default ProductDataBean parse(String data) {
		return null;
	}
	
	default ProductDataBean parse(CSVRecord data) { 
		return null;
	}
	
	default ProductDataBean parse(JSONObject data) {
		return null;
	}
}
