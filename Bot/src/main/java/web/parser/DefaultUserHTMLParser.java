package web.parser;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import bean.MergeProductDataBean;
import bean.ParserConfigBean;
import bean.ProductDataBean;
import bean.ReturningValueBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class DefaultUserHTMLParser implements HTMLParser{

	private final String HTTP_NOT_FOUND = String.valueOf(HttpURLConnection.HTTP_BAD_REQUEST); 
	
	private Map<ParserConfigBean.FIELD, List<String[]>> pConfig;
	private boolean foundConcatRealProductId;
	
	public FILTER_TYPE getFilterType() {
		return null;
	}
	
	public void setParserConfig(List<ParserConfigBean> configList) {
		if(configList != null) {
			setConfig(configList);
		}else if(BaseConfig.PARSER_CONFIG != null) {
			setConfig(BaseConfig.PARSER_CONFIG);
		}
	}

	public ProductDataBean[] parse(String url) {
		
		try {
			
			if(pConfig == null || pConfig.isEmpty())
				return null;
			

    		String targetUrl = url;
	        if(!BaseConfig.CONF_SKIP_ENCODE_URL)
	            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"}))
	            	targetUrl = BotUtil.encodeURL(url);
	        
    		String[] htmlContent = null;
    		if(BaseConfig.CONF_DISBLE_PARSER_COOKIES || HTTPUtil.USE_PROXY)
				htmlContent = HTTPUtil.httpRequestWithStatusIgnoreCookies(targetUrl, BaseConfig.CONF_PARSER_CHARSET, true);
			else
				htmlContent = HTTPUtil.httpRequestWithStatus(targetUrl, BaseConfig.CONF_PARSER_CHARSET, true);
			
    		if (htmlContent == null || htmlContent.length != 2)
				return null;
			
    		String httpStatus = htmlContent[1];
			if(httpStatus.equals(HTTP_NOT_FOUND))
				return processHttpStatus(htmlContent[1]);
			
			String html = htmlContent[0];
			if(html == null)
				return processHttpStatus(httpStatus, false);
			
			ProductDataBean productDataBean = processProductData(html, url, pConfig);
			if(productDataBean != null) {
	        	productDataBean.setHttpStatus(httpStatus);
	        	return new ProductDataBean[] {productDataBean};
	        }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setConfig(List<ParserConfigBean> configList) {
		pConfig = new HashMap<>();
		if(configList != null && !configList.isEmpty()){
			String lastNode = "";
			List<String[]> tmp = new ArrayList<String[]>();
			
			for(ParserConfigBean c : configList){
				String fieldType = c.getFieldType();
				String filterType = c.getFilterType();
				String value = c.getValue();
				
				if(lastNode.isEmpty()){
					lastNode = fieldType;
				}else if(!fieldType.equals(lastNode)){
					pConfig.put(ParserConfigBean.FIELD.valueOf(lastNode), tmp);
					tmp = new ArrayList<String[]>();
					lastNode = fieldType;
				}
				
				String[] cond = new String[]{ filterType, value}; 
				tmp.add(cond);
			}
			
			pConfig.put(ParserConfigBean.FIELD.valueOf(lastNode), tmp);
		}
		
		List<String[]> listConcatId = pConfig.get(ParserConfigBean.FIELD.concatId);
		if(listConcatId != null){
			for(String[] conf : listConcatId){
				ParserConfigBean.TYPE filter = ParserConfigBean.TYPE.valueOf(conf[0]);
				if(filter == ParserConfigBean.TYPE.concatRealProductId){
					this.foundConcatRealProductId = true;
					break;
				}
			}
		}
	}
	
	private ProductDataBean processProductData(String html, String  url, Map<ParserConfigBean.FIELD, List<String[]>> pConfig){
		ProductDataBean productDataBean = new ProductDataBean();
		MergeProductDataBean mpdBean = new MergeProductDataBean();

		String name = null;
		String price = null;
		String description = null;
		String pictureUrl = null;
		String realProductId = null;
		String basePrice = null;
		String upc = null;
		String concatId = "";
		String concatWord = "";
		String expire = "";
		
		List<String> nameStepResult = new ArrayList<String>();
		List<String> priceStepResult = new ArrayList<String>();
		List<String> descriptionStepResult = new ArrayList<String>();
		List<String> pictureUrlStepResult = new ArrayList<String>();
		List<String> realProductIdStepResult = new ArrayList<String>();
		List<String> urlStepResult= new ArrayList<String>();
		List<String> basePriceStepResult = new ArrayList<String>();
		List<String> upcStepResult = new ArrayList<String>();
		List<String> concatIdStepResult = new ArrayList<String>();
		List<String> concatWordStepResult = new ArrayList<String>();
		List<String> expireStepResult = new ArrayList<String>();
		
		List<String[]> mExpire = pConfig.get(ParserConfigBean.FIELD.expire);
		if(mExpire != null){
			ReturningValueBean expireValueBean = getData(ParserConfigBean.FIELD.expire, mExpire, html, url);
			String expireData = expireValueBean.getStringValue();
			if(StringUtils.isNotBlank(expireData) && expireData.equals("true")){
				productDataBean.setExpire(true);
				if(BaseConfig.RUNTIME_TYPE!= null && (BaseConfig.RUNTIME_TYPE.equals("WCE") || BaseConfig.RUNTIME_TYPE.equals("DUE"))) {
					return productDataBean;
				}
			}
			expireStepResult = expireValueBean.getListStringValue();
			expire = expireValueBean.getStringValue();
		}
		
		for(Map.Entry<ParserConfigBean.FIELD, List<String[]>> mNode : pConfig.entrySet()){
			ParserConfigBean.FIELD nodeType = mNode.getKey();
			if(ParserConfigBean.FIELD.concatId == nodeType && foundConcatRealProductId){
				concatId = "true";
				continue;
			}
			
			List<String[]> filterType = mNode.getValue();
			ReturningValueBean resultBean = getData(nodeType, filterType, html, url);
			String resultString = resultBean.getStringValue();
			List<String> resultStringList = resultBean.getListStringValue();
			resultString = FilterUtil.removeNonChar(resultString);
			
			if(ParserConfigBean.FIELD.name == nodeType) {
				name = resultString;
				nameStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.price == nodeType){
				price = resultString;
				priceStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.description == nodeType){
				description = resultString;
				descriptionStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.pictureUrl == nodeType){
				pictureUrl = resultString;
				pictureUrlStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.realProductId == nodeType){
				realProductId = resultString;
				realProductIdStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.upc == nodeType){
				upc = resultString;
				upcStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.basePrice == nodeType){
				basePrice = resultString;
				basePriceStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.concatId == nodeType){
				concatId = resultString;
				concatIdStepResult = resultStringList;
			}else if(ParserConfigBean.FIELD.concatWord == nodeType){
				concatWord = resultString;
				concatWordStepResult = resultStringList;
			}
			
		}
		
		mpdBean.setCurrentUrl(url);
		productDataBean = mergeProductData(name, price, description, pictureUrl, realProductId, url, basePrice, upc, concatId, concatWord, expire);
		productDataBean = mergeProductStepData(productDataBean,nameStepResult,priceStepResult, descriptionStepResult, pictureUrlStepResult, realProductIdStepResult, urlStepResult, basePriceStepResult,upcStepResult,concatIdStepResult, concatWordStepResult, expireStepResult);
		
		return productDataBean;
		
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus) {
		return processHttpStatus(httpStatus, true);
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
		ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
		rtn[0].setHttpStatus(httpStatus);
		rtn[0].setExpire(expire);
		return rtn;
	}
	
	private ReturningValueBean getData(ParserConfigBean.FIELD nodeType, List<String[]> filterType, String html, String url){
		String nodeHtml = html;
		ReturningValueBean stepResult = new ReturningValueBean();
		List<String> historyHtml = new ArrayList<String>();
		int nowStep = 0, jumpStep = 0;
		int allStep = filterType.size();
		boolean getByJSOUP = false;
		
		for(String[] conf : filterType){
			nowStep++;
			if(jumpStep != 0){
				if(jumpStep > allStep || jumpStep < nowStep)
					break;
				
				if(jumpStep == nowStep)
					jumpStep = 0;
				else{
					historyHtml.add(nodeHtml);
					continue;
				}
			}
			
			ParserConfigBean.TYPE type = ParserConfigBean.TYPE.valueOf(conf[0]);
			String value = conf[1];
			
			if(ParserConfigBean.TYPE.inTag == type){
				nodeHtml = FilterUtil.evaluate(nodeHtml, value);
			}else if(ParserConfigBean.TYPE.between == type){
				String[] between = value.split("\\|,\\|");
				if(between.length == 2){
					nodeHtml = FilterUtil.getStringBetween(nodeHtml, between[0], between[1]);
				}
			}else if(ParserConfigBean.TYPE.before == type){
				nodeHtml = FilterUtil.getStringBefore(nodeHtml, value, "");
			}else if(ParserConfigBean.TYPE.after == type){
				nodeHtml = FilterUtil.getStringAfter(nodeHtml, value, "");
			}else if(ParserConfigBean.TYPE.plainText == type){
				nodeHtml = FilterUtil.toPlainTextString(nodeHtml);
			}else if(ParserConfigBean.TYPE.removeNotPrice == type){
				nodeHtml = FilterUtil.removeCharNotPrice(nodeHtml);
			}else if(ParserConfigBean.TYPE.unescape == type){
				nodeHtml = StringEscapeUtils.unescapeHtml4(nodeHtml);
			}else if(ParserConfigBean.TYPE.replaceSpace == type){
				nodeHtml = FilterUtil.removeSpace(nodeHtml);
			}else if(ParserConfigBean.TYPE.ifEqual == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					if(nodeHtml.equals(arr[0])){
						try{
							jumpStep = Integer.parseInt(arr[1]);
						}catch(Exception e){ break; }
					}
				}
			}else if(ParserConfigBean.TYPE.ifNotEqual == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					if(!nodeHtml.equals(arr[0])){
						try{
							jumpStep = Integer.parseInt(arr[1]);
						}catch(Exception e){ break; }
					}
				}
			}else if(ParserConfigBean.TYPE.ifContain == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					if(nodeHtml.contains(arr[0])){
						try{
							jumpStep = Integer.parseInt(arr[1]);
						}catch(Exception e){ break; }
					}
				}
			}else if(ParserConfigBean.TYPE.ifNotContain == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					if(!nodeHtml.contains(arr[0])){
						try{
							jumpStep = Integer.parseInt(arr[1]);
						}catch(Exception e){ break; }
					}
				}
			}else if(ParserConfigBean.TYPE.textStep == type){
				try{
					int i = Integer.parseInt(value) - 1;
					if(i == -1){
						nodeHtml = html;
					}else{
						nodeHtml = historyHtml.get(i);
					}
				}catch(Exception e){ break; }
			}else if(ParserConfigBean.TYPE.expireEqual == type && value != null && nodeHtml.equals(value)){
				nodeHtml = "true";
			}else if(ParserConfigBean.TYPE.expireContain == type && value != null && nodeHtml.contains(value)){
				nodeHtml = "true";
			}else if(ParserConfigBean.TYPE.betweenUrl == type){
				String[] between = value.split("\\|,\\|");
				if(between.length == 2){
					nodeHtml = FilterUtil.getStringBetween(url, between[0], between[1]);
				}
			}else if(ParserConfigBean.TYPE.encodeUrl == type){
				nodeHtml = BotUtil.encodeURL(nodeHtml);
			}else if(ParserConfigBean.TYPE.contactPrice == type){
				nodeHtml = BotUtil.CONTACT_PRICE_STR;
				break;
			}else if(ParserConfigBean.TYPE.nonPrice == type){
				nodeHtml = BotUtil.NO_PRICE_STR;
				break;
			}else if(ParserConfigBean.TYPE.auctionPrice == type){
				nodeHtml = BotUtil.BID_PRICE_STR;
				break;
			}else if(ParserConfigBean.TYPE.specialPrice == type){
				nodeHtml = BotUtil.SPECIAL_PRICE_STR;
				break;
			}else if(ParserConfigBean.TYPE.replace == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					nodeHtml = nodeHtml.replace(StringUtils.defaultIfBlank(arr[0],""), StringUtils.defaultIfBlank(arr[1],""));
				}
			}else if(ParserConfigBean.TYPE.addTextBefore == type){
				nodeHtml = value + nodeHtml;
			}else if(ParserConfigBean.TYPE.addTextAfter == type){
				nodeHtml = nodeHtml + value;
			}else if(ParserConfigBean.TYPE.goToStep == type){
				jumpStep = Integer.parseInt(value);
			}else if(ParserConfigBean.TYPE.stop == type){
				break;
			}else if(ParserConfigBean.TYPE.JSoupSelectorNum == type){
				try{
					if(getByJSOUP) continue;
					Document doc = Jsoup.parse(nodeHtml);
					Element result = doc.select(value).first();
					nodeHtml = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(result.ownText()));
					nodeHtml = FilterUtil.removeSpace(nodeHtml);
					if(StringUtils.isNotBlank(nodeHtml)) getByJSOUP = true;
				}catch(Exception e){}
			}else if(ParserConfigBean.TYPE.JSoupSelectorTxt == type){
				try{
					if(getByJSOUP) continue;
					Document doc = Jsoup.parse(nodeHtml);
					Element result = doc.select(value).first();
					nodeHtml = FilterUtil.toPlainTextString(result.ownText());
					nodeHtml = FilterUtil.removeSpace(nodeHtml);
					if(StringUtils.isNotBlank(nodeHtml)) {
						if(ParserConfigBean.FIELD.expire == nodeType) nodeHtml = "true";
						getByJSOUP = true;
					}
				}catch(Exception e){}
			}else if(ParserConfigBean.TYPE.JSoupSelectorLongTxt == type){
				try{
					if(getByJSOUP) continue;
					Document doc = Jsoup.parse(nodeHtml);
					Element result = doc.select(value).first();
					nodeHtml = FilterUtil.toPlainTextString(result.outerHtml());
					nodeHtml = FilterUtil.removeSpace(nodeHtml);
					if(StringUtils.isNotBlank(nodeHtml)) getByJSOUP = true;
				}catch(Exception e){}
			}else if(ParserConfigBean.TYPE.JSoupSelectorAttr == type){
				String[] arr = value.split("\\|,\\|");
				if(arr.length == 2){
					try{
						if(getByJSOUP) continue;
						Document doc = Jsoup.parse(nodeHtml);
						Element result = doc.select(arr[1]).first();
						nodeHtml = FilterUtil.toPlainTextString(result.attr(arr[0]));
						nodeHtml = FilterUtil.removeSpace(nodeHtml);
						if(StringUtils.isNotBlank(nodeHtml)) {
							if(ParserConfigBean.FIELD.expire == nodeType) nodeHtml = "true";
							getByJSOUP = true;
						}
					}catch(Exception e){}
				}
			}
			
			historyHtml.add(nodeHtml);
		}
		
		if(nodeHtml.equals(html)){
			stepResult.setStringValue("");
		}else {
			stepResult.setStringValue(nodeHtml.trim());
		}
		
		stepResult.setListStringValue(historyHtml);
		
		return stepResult;
	}
	
	private ProductDataBean mergeProductData(String name, String price, String description, String pictureUrl, 
			String realProductId, String currentUrl, String basePrice, String upc, String concatId, String concatWord, String expire) {
		
    	if ((StringUtils.isBlank(name) || StringUtils.isBlank(price)) && BaseConfig.RUNTIME_TYPE!= null && (BaseConfig.RUNTIME_TYPE.equals("WCE") || BaseConfig.RUNTIME_TYPE.equals("DUE"))) {
    		return null;
    	}
    	
		ProductDataBean returnBean = new ProductDataBean();
		
		boolean isRealProductId = StringUtils.isNotBlank(realProductId);
		if(StringUtils.isNotBlank(concatId) && !concatId.equals("true")){
			concatId = concatId.trim();
			name += " ("+ concatId +")";
		}else if(concatId.equals("true") && isRealProductId){
			realProductId = realProductId.trim();
			name += " ("+ realProductId +")";
		}

		if(StringUtils.isNotBlank(concatWord))
			name = concatWord.trim() + " " + name;
		
		returnBean.setName(name);
		if(FilterUtil.convertPriceStr(price) > BotUtil.CONTACT_PRICE)
			returnBean.setPrice(BotUtil.CONTACT_PRICE);
		else
			returnBean.setPrice(FilterUtil.convertPriceStr(price));

		if (StringUtils.isNotBlank(description))
			returnBean.setDescription(description);
		
		if (StringUtils.isNotBlank(pictureUrl)){ 
			if(pictureUrl.startsWith("http")) {
				returnBean.setPictureUrl(pictureUrl);					
			} else {
				try {
					URL url = new URL(new URL(currentUrl), pictureUrl);
					returnBean.setPictureUrl(url.toString());
		    	} catch (MalformedURLException e) {}
			}
		}
		
		if (isRealProductId)
			returnBean.setRealProductId(realProductId);
		
		if (StringUtils.isNotBlank(basePrice) && returnBean.getPrice() != BotUtil.CONTACT_PRICE)
			returnBean.setBasePrice(FilterUtil.convertPriceStr(basePrice));
				
		if (StringUtils.isNotBlank(upc))
			returnBean.setUpc(upc);
		
		if (StringUtils.isNotBlank(expire))
			returnBean.setExpire(expire);
		
		return returnBean;
	}
	
	private static ProductDataBean mergeProductStepData(ProductDataBean bean, List<String> nameStepResult,List<String> priceStepResult, List<String> descriptionStepResult, List<String> pictureUrlStepResult,
			List<String> realProductIdStepResult, List<String> urlStepResult, List<String> basePriceStepResult,List<String> upcStepResult, List<String> concatIdStepResult, List<String> concatWordStepResult,
			List<String> expireStepResult) {
		if(bean == null)  return null;
		if(nameStepResult != null && nameStepResult.size() > 0) {bean.setNameStepResult(nameStepResult);}
		if(priceStepResult != null && priceStepResult.size() > 0) {bean.setPriceStepResult(priceStepResult);}
		if(descriptionStepResult != null && descriptionStepResult.size() > 0) {bean.setDescriptionStepResult(descriptionStepResult);}
		if(pictureUrlStepResult != null && pictureUrlStepResult.size() > 0) {bean.setPictureUrlStepResult(pictureUrlStepResult);}
		if(realProductIdStepResult != null && realProductIdStepResult.size() > 0) {bean.setRealProductIdStepResult(realProductIdStepResult);}
		if(urlStepResult != null && urlStepResult.size() > 0) {bean.setUrlStepResult(urlStepResult);};
		if(basePriceStepResult != null && basePriceStepResult.size() > 0) {bean.setBasePriceStepResult(basePriceStepResult);}
		if(upcStepResult != null && upcStepResult.size() > 0) {bean.setUpcStepResult(upcStepResult);}
		if(concatIdStepResult != null && concatIdStepResult.size() > 0) {bean.setConcatIdStepResult(concatIdStepResult);}
		if(concatWordStepResult != null && concatWordStepResult.size() > 0) {bean.setConcatWordStepResult(concatWordStepResult);}
		if(expireStepResult != null && expireStepResult.size() > 0) {bean.setExpireStepResult(expireStepResult);}
		return bean;
	}
	
}
