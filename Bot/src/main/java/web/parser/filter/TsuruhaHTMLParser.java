package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TsuruhaHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-shop product-simple\">"});
		map.put("price", new String[]{"<div class=\"product-shop product-simple\">"});
		map.put("basePrice", new String[]{"<div class=\"product-shop product-simple\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-image\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = evaluateResult[0];
    		if(StringUtils.isNotBlank(productPrice)){
    			if(productPrice.contains("<p class=\"special-price\">")){
    				productPrice = FilterUtil.getStringBetween(productPrice, "<p class=\"special-price\">", "</p>");
    				productPrice = FilterUtil.getStringBetween(productPrice, "฿", "</span>");
    			}else{
    				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    			}
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = evaluateResult[0];
    		if(StringUtils.isNotBlank(bPrice)){
    			if(bPrice.contains("<p class=\"old-price\">")){
    				bPrice = FilterUtil.getStringBetween(bPrice, "<p class=\"old-price\">", "</p>");
    				bPrice = FilterUtil.getStringBetween(bPrice, "฿", "</span>");
    			}
    		}
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
