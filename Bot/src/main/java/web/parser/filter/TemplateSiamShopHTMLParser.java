package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TemplateSiamShopHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<div id=\"r-detail\">"});
		map.put("basePrice", new String[]{"<div id=\"r-detail\">"});
		map.put("description", new String[]{"<div id=\"list-detail\">"});
		map.put("pictureUrl", new String[]{"<div id=\"l-images\" >"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	
    	for (int i = 0; i < evaluateResult.length; i++) {
				
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[i]);		
    		tmp = FilterUtil.getStringBetween(tmp, "ลดเหลือ :", "฿");    
    		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		if(productPrice.trim().length() != 0) {
    			break;
    		}
		}	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productbPrice = "";    	
    	
    	for (int i = 0; i < evaluateResult.length; i++) {
				
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[i]);		
    		tmp = FilterUtil.getStringBetween(tmp, "ราคา :", "฿");    
    		productbPrice = FilterUtil.removeCharNotPrice(tmp);
    		if(productbPrice.trim().length() != 0) {
    			break;
    		}
		}
    		
    		
        		
		return new String[] {productbPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
