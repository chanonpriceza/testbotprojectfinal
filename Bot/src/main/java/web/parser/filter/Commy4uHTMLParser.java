package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class Commy4uHTMLParser extends BasicHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-item ");
			 e = e.select("h4.txt-bold");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		Document doc = Jsoup.parse(evaluateResult[0]);
		if(evaluateResult.length == 1) {	
			Elements e = doc.select("div.product-item");
			String cut = FilterUtil.getStringBetween(e.html(),"<span>","บาท");
			productPrice = FilterUtil.removeCharNotPrice(cut);			
		}	
		
		if(productPrice.length()==0) {
			Elements e = doc.select("div.product-item");
			 e = e.select("span.c-pink");
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-item");
			 e = e.select("span.price-txt");
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
 
}
