package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PegasusluggageHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		new String[]{"<h1 class=\"product-name product_blog_title\" ref=\"exTitle\" name=\"exTitle\">"});
		map.put("price", 		new String[]{"<div class=\"product-price\">"});
		map.put("basePrice",    new String[]{"<s ref=\"tempprice\">"});
		map.put("description", 	new String[]{"<div class=\"detail\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"productDetailImg\">"});
		map.put("realProductId", new String[]{"<div ref=\"sku\" class=\"column-inline\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price\" ref=\"priceperpiece\" id=\"priceperpiece\" price=\"", "/div>");
			productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

			if(productPrice.equals("0")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length > 0) {
			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
			
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if(evaluateResult.length > 0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
			productPictureUrl = "www.pegasus-luggage.com"+productPictureUrl;
		}
    	return new String[] {productPictureUrl};
	}
	
	 public String[] getRealProductId(String[] evaluateResult) {
		 String pdId = "";
			if (evaluateResult.length > 0 ) {
				pdId = FilterUtil.toPlainTextString(evaluateResult[0]);
	    	}
			return new String[] { pdId};
	 }
	 
	 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			
			String[] productId = mpdBean.getRealProductId();
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productBasePrice = mpdBean.getProductBasePrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();
			
			if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName("Pegasus " + productName[0] + " ("+productId[0] + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			
			if(StringUtils.isNotBlank(productBasePrice[0])){
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			}
			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				rtn[0].setPictureUrl("https://"+productPictureUrl[0]);
			}
			rtn[0].setUrl(currentUrl + "/");
			rtn[0].setUrlForUpdate(currentUrl);
			
			return rtn;
		}
}
