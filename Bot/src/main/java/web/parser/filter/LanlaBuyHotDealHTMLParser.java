package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class LanlaBuyHotDealHTMLParser extends DefaultHTMLParser {		
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<div class=\"product-name\">"});
		map.put("price", 			new String[]{"<div class=\"iSpecialPrice\">"});
		map.put("basePrice", 		new String[]{"<div class=\"iNormalPrice\">"});
		map.put("description", 		new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"product-image-gallery\">"});
		map.put("realProductId", 	new String[]{"<div class=\"no-display\">"});
		map.put("expire", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";			
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName += "(Hot Deal)";
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "name=\"product\" value=\"", "\"");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	   	
	    return new String[]{realProductId}; 
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {

		if(!evaluateResult[0].contains("<div class=\"add-to-cart-buttons\">"))
		{
			return new String[]{"true"}; 
		}
		
		return new String[]{"false"};
	}
	    
}