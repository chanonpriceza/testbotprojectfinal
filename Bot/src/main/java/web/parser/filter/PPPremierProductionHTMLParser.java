package web.parser.filter;

import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PPPremierProductionHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<h1 class=\"item name fn\" itemprop=\"name\">"});
		map.put("price", new String[]{"<li class=\"price\">"});
		map.put("description", new String[]{"<div class=\"tab-pane active\" id=\"tab-description\">"});
		map.put("pictureUrl", new String[]{"<li class=\"image\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
		 
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}else{
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0){
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		
		if(evaluateResult.length > 0){
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
			try {
    			productPictureUrl = URLEncoder.encode(productPictureUrl, "UTF-8");
    			productPictureUrl = productPictureUrl.replace("%3A", ":");
    			productPictureUrl = productPictureUrl.replace("%2F", "/");
    		} catch (Exception e) { }
        }
    	return new String[] {productPictureUrl};
	}
	
		
	
}