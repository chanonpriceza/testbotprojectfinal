package web.parser.filter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ModernAirMakeWebEasyHTMLParser extends DefaultHTMLParser{
	
	private static final Logger logger = LogManager.getRootLogger();
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">","<div class=\"productItemDetail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});

		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}

	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		
		List<String> priceAttrList = FilterUtil.getAllStringBetween(evaluateResult[0], "class=\"hiddenAttributeID\" value=\"", "\"");
		if(priceAttrList != null && priceAttrList.size() > 0) {
			return priceAttrList.toArray(new String[priceAttrList.size()]);
		}
		String priceAttrArea = FilterUtil.getStringBetween(evaluateResult[0], "<select class=\"form-control dropdownDetail hiddenAttributeID\">", "</select>");
		if(StringUtils.isNotBlank(priceAttrArea)) {
			priceAttrList = FilterUtil.getAllStringBetween(priceAttrArea, "value=\"", "\"");
			if(priceAttrList != null && priceAttrList.size() > 0) {
				return priceAttrList.toArray(new String[priceAttrList.size()]);
			}
		}
		
		return null;
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = evaluateResult[0].replaceAll("\\<.*?>", " ").replaceAll("\\s+"," ").trim();
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = null;
		if (evaluateResult.length >= 1) {
			upc = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"bodyTD\">", "</td>").trim();
			upc = FilterUtil.toPlainTextString(upc);
			if(!StringUtils.isNumeric(upc) || upc.length() != 13){
				upc = null;
			}
		}
		return new String[] { upc};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "";
		if (evaluateResult.length == 1) {
			if(evaluateResult[0].contains("สินค้าหมด")|| evaluateResult[0].contains("เร็วๆนี้")){
				exp = "true";
			}
		}
		return new String[] { exp};
	}
	
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	if (evaluateResult.length == 1) {
			productId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" id=\"hiddenProductID\" value=\"", "\"").trim();
    	}
    	return new String[]{ productId};
    }
    
    @Override
	public String[] getProductNameDescription(String[] evaluateResult) {
    	String productNameDescription = "";
    	if(evaluateResult.length > 0){
    		productNameDescription = FilterUtil.getStringAfter(evaluateResult[0],"<td class=\"bodyTD\">",evaluateResult[0]);
    		productNameDescription = FilterUtil.toPlainTextString(productNameDescription);
    	}
		return new String[]{productNameDescription};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || realProductId == null || realProductId.length == 0) {
			return null;
		}

		
		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		if(productPrice == null || productPrice.length == 0) {
			productPrice = new String[] {"0"};
		}
		
		String domain = FilterUtil.getStringBetween(FilterUtil.getStringAfter(currentUrl,"http",""), "://", "/");
		
		boolean isHttps = (currentUrl.indexOf("https")> -1)? true:false ; 
		
		if(StringUtils.isBlank(domain)) return null;
		
		String req_url = (isHttps)?"https":"http" ;
		req_url += "://" + domain;
		req_url += "/page/page_product_item/ajaxPageProductItemController.php";
		
		ProductDataBean[] rtn = new ProductDataBean[productPrice.length];
		int productCount = 0;
		
		for(String priceAttr : productPrice) {
			String data = postRequest(currentUrl, req_url, realProductId[0].trim(), priceAttr, domain, isHttps);
			data = String.valueOf(data);
			data = StringEscapeUtils.unescapeJson(data);
			
			String price = FilterUtil.getStringBetween(data, "<span class=\"h4\">", "</span>");
			if(price.isEmpty()){
				price = FilterUtil.getStringBetween(data, "<span class=\"price h4\">", "</span>");
			}
			if(price.isEmpty()){
				price = FilterUtil.getStringAfter(data,"class=\"ff-price fs-price fc-price\"","");
				price = FilterUtil.getStringAfter(price,">","");
				price = FilterUtil.getStringBefore(price,"<","");
			}
			
			if(price.contains("THB")) {
				price = FilterUtil.getStringAfterLastIndex(price,"THB",price);
				price = FilterUtil.toPlainTextString(price);
			}
			
			if(price.isEmpty() || price.equals("0")){
				price = String.valueOf(BotUtil.CONTACT_PRICE);
			}
			String bPrice =  FilterUtil.getStringBetween(data, "<span class=\"productOldPrice h5\">", "</span>");
			if(bPrice.isEmpty()) {
				bPrice = FilterUtil.getStringAfter(data,"class=\"productOldPrice h5","");
				bPrice = FilterUtil.getStringAfter(bPrice,">","");
				bPrice = FilterUtil.getStringBefore(bPrice,"<","");
			}
			
			
			ProductDataBean resultBean = new ProductDataBean();
			if(pdName.contains("(ของหมด")) {
				resultBean.setExpire(true);
				rtn[productCount] = resultBean;
				return rtn;
			}
			resultBean.setName(pdName);
			resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			
			if (!bPrice.isEmpty()) {
				resultBean.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(bPrice)));
			}
			
			if (productDescription != null && productDescription.length != 0) {
				resultBean.setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						resultBean.setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							resultBean.setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			if (nameDescription != null && nameDescription.length != 0){
				resultBean.setKeyword(nameDescription[0]);
			}
			
			rtn[productCount] = resultBean;
			productCount++;
			
			logger.info("name:" + resultBean.getName() + "|price:" + resultBean.getPrice());
			
		}
		
				
		return rtn;
	}
	
	protected static String postRequest(String currentUrl, String url, String id, String priceAttr, String domain, boolean isHttps) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;
	 	
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
		
	 	String origin = (isHttps)?"https":"http" ;
	 	origin += "://" + domain;
		
	 	String param = "productID="+id+"&attributeSet=%5B%22"+priceAttr+"%22%5D&type=getAttributeGroupID";
	 	
	 	try {
	 		
	 		u = new URL(currentUrl);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", userAgent);
			conn.setConnectTimeout(1800000);
			conn.setReadTimeout(1800000);

			Map<String,List<String>> cookies = conn.getHeaderFields();
			List<String> setCookie =  cookies.get("Set-Cookie");
			String pid = "pid=";
			String PHPSESSID = "PHPSESSID=";
			
			for(String c:setCookie) {
				if(c.startsWith("pid")) {
					pid += FilterUtil.getStringBetween(c,"pid=",";")+";";
				}
				
				if(c.startsWith("PHPSESSID")) {
					PHPSESSID += FilterUtil.getStringBetween(c,"PHPSESSID=",";")+";";
				}
			}
			String genCookie = pid+PHPSESSID;
	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 	    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	 	    conn.setRequestProperty("Accept", "*/*");
	 	   // conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
	 	    //conn.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
	 	    //conn.setRequestProperty("Connection", "keep-alive");
	 	    //conn.setRequestProperty("Host", domain);
	 	    conn.setRequestProperty("Origin", origin);
	 	    conn.setRequestProperty("Referer","https://www.woodyshop2016.com/product/13623/t2311vsam");
	 	    conn.setRequestProperty("User-Agent", userAgent);
	 	    conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
	 	    conn.setRequestProperty("Cookie",genCookie);
	 	    //"PHPSESSID=5bb437dfd07cb7ed5470b65f5ac243db; pid=13623"
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(param.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while ((line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while ((line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					e.printStackTrace();
					//logger.error(e);
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}
}
