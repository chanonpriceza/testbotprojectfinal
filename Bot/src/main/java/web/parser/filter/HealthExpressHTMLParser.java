package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HealthExpressHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div style='margin:5px 0 5px 0;'>"});
		map.put("pictureUrl", new String[]{"<div style='margin:15px 0 15px 0;'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			String id = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส    :", "</div>");
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</span></div>");
			id = FilterUtil.toPlainTextString(id);
			if(id.trim().length() == 0){
				productName = FilterUtil.toPlainTextString(rawName);
			}else{
				productName = FilterUtil.toPlainTextString(rawName+" ("+id+")");
			}			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:5px 0 1px 0;'>", "<div style='margin:20px 0 5px 0;'>");
    		
    		if(rawPrice.contains("<strike>")){
    			rawPrice = FilterUtil.getStringAfter(rawPrice, "</strike>", "");
    		}
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String baseprice = "";    	
    	if (evaluateResult.length > 0) {
    		baseprice = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:5px 0 1px 0;'>", "<div style='margin:20px 0 5px 0;'>");
    		if(baseprice.contains("<strike>")){
    			baseprice = FilterUtil.getStringBetween(baseprice, "<strike>", "</strike>");
    		} else {
    			return new String[] {""};
    		}
    		baseprice = FilterUtil.toPlainTextString(baseprice);
    		baseprice = FilterUtil.removeCharNotPrice(baseprice);
    		if(baseprice.trim().length() == 0){
    			baseprice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {baseprice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href='", "'");
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}