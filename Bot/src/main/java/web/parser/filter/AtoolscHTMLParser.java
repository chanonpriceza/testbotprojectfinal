package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AtoolscHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] {""});
		map.put("price", new String[] { "<div class=\"row\">" });
		map.put("basePrice", new String[] { "<div class=\"row\">" });
		map.put("description",new String[] { "<div class=\"product-short-description\">" });
		map.put("pictureUrl", new String[] { "" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = ""; 
		String brand = "";
		String subname = "";
		String id = "";
		if (evaluateResult.length > 0) {
			brand = FilterUtil.getStringBetween(evaluateResult[0], "หน้าหลัก</a> <span class=\"divider\">", "<span class=\"divider\">");
			brand = FilterUtil.getStringBetween(brand, "<a href=\"", "/a>");
			brand = FilterUtil.getStringBetween(brand, "\">", "<");
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"divider\">&#47;</span> <a href=\"", "</nav>");
			productName = FilterUtil.getStringBetween(productName, "<span class=\"divider\">&#47;</span> <a href=\"https://www.atoolsc.com/product-category/", "/a>");
			productName = FilterUtil.getStringBetween(productName, "\">", "<");
			subname = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product-title entry-title\">", "</h1>");
			id = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"shop-container\">", "class=\"");
			id = FilterUtil.getStringBetween(id, "<div id=\"product-", "\"");
			if(StringUtils.isNotBlank(brand)) {
				productName = brand.toUpperCase() + " " +productName + " " +subname + " (" +id+ ")"; 	
			}
		}
		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length > 0) {
			if(evaluateResult[0].contains("<ins><span class=\"woocommerce-Price-amount amount\">")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "<span class=");
			}else {				
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "<span class=\"");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
			
			if(productPrice.equals("")) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}

		return new String[] { productPrice };
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String basePrice = "";
		if (evaluateResult.length > 0) {				
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "<span class=\"");

			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);

		}
		
		return new String[] { basePrice };
	}
	
	

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"first slide woocommerce-product-gallery__image\">", ">");
			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<a href=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}

}

