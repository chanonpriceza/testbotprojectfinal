package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class DeskerHTMLParser extends TemplateMakeWebEasyHTMLParser{
	
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"col-12 col-md-7\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">","<div class=\"productItemDetail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{"<div class=\"product_comingsoon warningBox\" v-bind:class=\"'product_'+show_pdata.status.sell\" itemprop=\"availability\" content=\"out_of_stock\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		String nameCut = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h2 productName\">", "</h1>");		
			nameCut = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "</p>");		
			productName = productName + "  " + nameCut;
		}
		return new String[] {productName};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || realProductId == null || realProductId.length == 0) {
			return null;
		}

		
		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		if(productPrice == null || productPrice.length == 0) {
			productPrice = new String[] {"0"};
		}
		
		String domain = FilterUtil.getStringBetween(FilterUtil.getStringAfter(currentUrl,"http",""), "://", "/");
		
		boolean isHttps = (currentUrl.indexOf("https")> -1)? true:false ; 
		
		if(StringUtils.isBlank(domain)) return null;
		
		String req_url = (isHttps)?"https":"http" ;
		req_url += "://" + domain;
		req_url += "/page/page_product_item/ajaxPageProductItemController.php";
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		
			String priceAttr = productPrice[0];
			String data = postRequest(currentUrl, req_url, realProductId[0].trim(), priceAttr, domain, isHttps);
			data = String.valueOf(data);
			data = StringEscapeUtils.unescapeJson(data);
			
			String price = FilterUtil.getStringBetween(data, "<span class=\"h4\">", "</span>");
			if(price.isEmpty()){
				price = FilterUtil.getStringBetween(data, "<span class=\"price h4\">", "</span>");
			}
			if(price.isEmpty()){
				price = FilterUtil.getStringAfter(data,"class=\"ff-price fs-price fc-price\"","");
				price = FilterUtil.getStringAfter(price,">","");
				price = FilterUtil.getStringBefore(price,"<","");
			}
			
			if(price.contains("THB")) {
				price = FilterUtil.getStringAfterLastIndex(price,"THB",price);
				price = FilterUtil.toPlainTextString(price);
			}
			
			if(price.isEmpty() || price.equals("0")){
				price = String.valueOf(BotUtil.CONTACT_PRICE);
			}
			String bPrice =  FilterUtil.getStringBetween(data, "<span class=\"productOldPrice h5\">", "</span>");
			if(bPrice.isEmpty()) {
				bPrice = FilterUtil.getStringAfter(data,"class=\"productOldPrice h5","");
				bPrice = FilterUtil.getStringAfter(bPrice,">","");
				bPrice = FilterUtil.getStringBefore(bPrice,"<","");
			}
			
			ProductDataBean resultBean = new ProductDataBean();
			resultBean.setName(pdName);
			resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			
			if (!bPrice.isEmpty()) {
				resultBean.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(bPrice)));
			}
			
			if (productDescription != null && productDescription.length != 0) {
				resultBean.setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						resultBean.setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							resultBean.setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			if (nameDescription != null && nameDescription.length != 0){
				resultBean.setKeyword(nameDescription[0]);
			}
			
			rtn[0] = resultBean;

		
				
		return rtn;
	}
	
}
