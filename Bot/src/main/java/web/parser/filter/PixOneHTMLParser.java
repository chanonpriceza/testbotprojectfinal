package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PixOneHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "windows-874";
	}	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<td class='content'>"});
		map.put("price", new String[]{"<body style=\"margin:0px auto 0px auto;\">","<table cellpadding=\"3\" cellspacing=\"1\" width=\"100%\" border=\"0\">"});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<body style=\"margin:0px auto 0px auto;\">"});
		map.put("pictureUrl", new String[]{"<body style=\"margin:0px auto 0px auto;\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = ""; 
		if(evaluateResult.length > 0) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<div class='h1'>", "</div>");
			productName = FilterUtil.toPlainTextString("<div>"+rawName+"</div>");
			if(rawName.contains("สินค้าหมด")){
				productName = "";
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
		if(evaluateResult.length > 0){
			
			String rawProductPrice =  FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
			if(StringUtils.isBlank(rawProductPrice)){
				rawProductPrice =  FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
			}
			
			
			if(rawProductPrice.length() == 0){
				String rawData = FilterUtil.getStringAfter(evaluateResult[0], "<table cellpadding=\"3\" cellspacing=\"1\" width=\"100%\" border=\"0\">", "");
				List<String> allPrice = FilterUtil.getAllStringBetween(rawData, "<tr valign='top'", "</tr>");
				ArrayList<String> arr = new ArrayList<String>();
				
				for (String string : allPrice) {
					String price= "";
					String name1 = FilterUtil.getStringBetween(string, "<span style='color:#333333'>", "</span>");
					name1 = FilterUtil.toPlainTextString(name1);
					
					String basePrice = FilterUtil.getStringBetween(string, "<strike>", "</strike>");
					if(StringUtils.isNotBlank(basePrice)){
						price = FilterUtil.getStringBetween(string, "</strike></div><div class='h3' style='color:#333333'>", "<");
						basePrice = FilterUtil.toPlainTextString(basePrice);
						basePrice = FilterUtil.removeCharNotPrice(basePrice);
					}else{
						price = FilterUtil.getStringBetween(string, "<div style='color:#333333'>", "</div>");
					}
		
					price = FilterUtil.toPlainTextString(price);
					price = FilterUtil.removeCharNotPrice(price);
					
					if(name1.trim().length() != 0 && price.length() != 0) {
						if(basePrice.length() != 0){
							arr.add(name1.trim() + "|" + price + "|" + basePrice);
						}else{
							arr.add(name1.trim() + "|" + price);
						}
					
					}
				}
				
				return (String[])arr.toArray(new String[0]);
			}
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawProductPrice+"</div>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		if(evaluateResult.length > 0){
			productBasePrice =  FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
			if(StringUtils.isNotBlank(productBasePrice)){
				productBasePrice =  FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
			
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawProductDesc = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd2'>รายละเอียดย่อ", "<input type='hidden' name='cart' value='add'>");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawProductDesc+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<td width='30%' align='center'>", "</a>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPictureUrl, "src='", "'");
        }
    	return new String[] {productPictureUrl};
	}
	

    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ArrayList<ProductDataBean> arr = new ArrayList<ProductDataBean>();
		
		for (String strData : productPrice) {
			
			ProductDataBean productDataBean = new ProductDataBean();
			
			if(strData.contains("|")) {
				String[] priceData = strData.split("\\|");
				productDataBean.setName(productName[0]+" "+priceData[0]);
				productDataBean.setPrice(FilterUtil.convertPriceStr(priceData[1]));
				
				if(priceData != null && priceData.length == 3){
					productDataBean.setBasePrice(FilterUtil.convertPriceStr(priceData[2]));
				}
								
			} else {
				productDataBean.setName(productName[0]);
				productDataBean.setPrice(FilterUtil.convertPriceStr(strData));
				if(productBasePrice != null && productBasePrice.length != 0){
					productDataBean.setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				}	
			}
				
			if (productDescription != null && productDescription.length != 0) {
				productDataBean.setDescription(productDescription[0]);
			}

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null
						&& productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						productDataBean.setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl),
									productPictureUrl[0]);
							productDataBean.setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}

			arr.add(productDataBean);
		}
		
		return (ProductDataBean[])arr.toArray(new ProductDataBean[0]);
	}
	    
}