package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BlisbyHTMLParser extends DefaultHTMLParser{
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<h2 class=\"dark-grey display-price\">"});
		map.put("description", new String[]{"<div class=\"tab-pane active\" id=\"product-details\">"});
		map.put("pictureUrl", new String[]{"<div class=\"bxslider-wrapper \">"});
		map.put("expire", new String[] {""});
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("สินค้าหมด")) {
    			return new String[] {"true"};
    		}
    	}
    	return new String[] {"false"};    
    }
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("เกี่ยวกับสินค้า"))
    		{
    			evaluateResult[0] = FilterUtil.getStringAfter(evaluateResult[0], "เกี่ยวกับสินค้า", "");
    		}
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
}