package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TaradPremiumNoCatHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 			new String[]{"<h1 class=\"product-title\">"});
		map.put("price", 			new String[]{"<div class=\"cdetail\">"});
		map.put("basePrice", 		new String[]{"<div class=\"cdetail\">"});
		map.put("description", 		new String[]{"<div id=\"p-detail\">"});
		map.put("pictureUrl", 		new String[]{""});
		map.put("expire", 			new String[]{"<div class=\"cdetail\">"});
		map.put("nameDescription", 	new String[]{"<div id=\"sproduct-title\" class=\"clearfix\">"});
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
		
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("ico-nostock.png") != -1) {
        		return new String[]{"true"};
        	}     	
    	}    	
        return new String[]{""};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li>ราคาพิเศษ", "บาท");
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li>ราคา", "บาท");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<li>ราคาปกติ", "บาท");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductNameDescription(String[] evaluateResult) {
		
		String productNameDesc = null;
		if(evaluateResult.length > 0){
			productNameDesc = FilterUtil.toPlainTextString(evaluateResult[0]).replaceAll("&nbsp;", " ").replaceAll("\\s{2,}", " ").trim();
			productNameDesc = FilterUtil.getStringAfter(productNameDesc, ">", "");
			productNameDesc = FilterUtil.getStringAfter(productNameDesc, ">", "");
			productNameDesc = new StringBuilder(productNameDesc).reverse().toString();
			productNameDesc = FilterUtil.getStringAfter(productNameDesc, ">", "");
			productNameDesc = new StringBuilder(productNameDesc).reverse().toString();
		}
		return new String[] {productNameDesc};
	}
	
	
	
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productNameDesc = mpdBean.getProductNameDescription();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		currentUrl = currentUrl.replace("?per_page=0", "");
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}
		
		if(productNameDesc != null && productNameDesc.length != 0){
			rtn[0].setKeyword(productNameDesc[0]);
		}
		
		return rtn;
	}
	    
}
