package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class UKKShoppingHTMLParser extends WeLoveShoppingPlatinumNoPriceHTMLParser{

	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		String name = productName[0];		
		String price = productPrice[0];
		String priceXXX = "";
		if(name.startsWith("ราคาพิเศษ ")) {
			String xPrice = FilterUtil.getStringBetween(name, "ราคาพิเศษ ", " ");			
			if(FilterUtil.isPriceXXX(xPrice.trim())) {
				priceXXX = xPrice.trim();				
			}
		}
		
		priceXXX = priceXXX.trim();
		if(priceXXX.length() != 0) {			
			if(priceXXX.startsWith("x") && priceXXX.endsWith("x")){
				rtn[0].setPrice(9999999);
			}else{
				rtn[0].setPrice(FilterUtil.convertPriceXXXStr(priceXXX));
			}
		} else {
			rtn[0].setPrice(FilterUtil.convertPriceStr(price));	
		}
				
		if(rtn[0].getPrice() == 0) {
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		} 
						
		if (productDescription != null && productDescription.length != 0)
			rtn[0].setDescription(productDescription[0]);
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			
			try {
				URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
				rtn[0].setPictureUrl(url.toString());
	    	} catch (MalformedURLException e) {
				
			}
		}

		return rtn;
	}    
            
}
