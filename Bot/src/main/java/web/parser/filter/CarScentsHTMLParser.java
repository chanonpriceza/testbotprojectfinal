package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CarScentsHTMLParser extends DefaultHTMLParser{
	
	public String getCharset() {
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<h2 class=\"txt-red\">"});
		map.put("basePrice", new String[]{"<span style=\"text-decoration: line-through;\">"});
		map.put("description", new String[]{"<ul class=\"well\" style=\"padding:0px\">"});
		map.put("pictureUrl", new String[]{"<ul class=\"thumbnails\">"});
		map.put("expire", new String[]{"<div id=\"product\">"});
		
		return map;
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "false";
		if(evaluateResult.length > 0) {	
			if(evaluateResult[0].contains("Out Of Stock")){
				exp = "true";
			}
		}
		
		return new String[] {exp};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = evaluateResult[0];
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }
    	
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if(evaluateResult.length > 0) {	
			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		
        }
    	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = evaluateResult[0];
    		if(StringUtils.isNoneBlank(productDesc)){
    			productDesc = productDesc.replace("&nbsp;", "");
    			productDesc = productDesc.replace("+", " ");
    			productDesc = FilterUtil.toPlainTextString(productDesc);    	
    		}
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	    
}