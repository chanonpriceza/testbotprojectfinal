package web.parser.filter;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ILoveToGoHTMLParser extends DefaultHTMLParser {
	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", 	new String[]{""});
			map.put("price", 	new String[]{"<div class=\"product-detail-list-num-right\">"});
			map.put("basePrice", new String[]{"<div class=\"product-detail-list-num-right\">"});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{"<div class=\"product-picBig\">"});
			map.put("expire", 	new String[]{"<div style=\"font-size:20px; padding-top:20px; margin:0 auto; width:250px; height:80px; color:red;\">"});
			
			return map;
		}
	  	@Override
	  	public String[] getProductName(String[] evaluateResult) {
	  		String[] productName = {};   
	  		
			if(evaluateResult.length > 0) {	
				
				
				List<String> property = FilterUtil.getAllStringBetween(evaluateResult[0], "<div class=\"product-detail-list-num-left\">", "<div class=\"product-detail-list-num-right\">");
				productName = new String[property.size()+1];
				
				productName[0] = FilterUtil.getStringBetween(evaluateResult[0], "<h2>", "</h2>");
				productName[0] = FilterUtil.toPlainTextString(productName[0]);
				productName[0] = StringEscapeUtils.unescapeHtml3(productName[0]);
				int i = 1;
				for( String p : property){
					p = FilterUtil.toPlainTextString(p);
					productName[i] = StringEscapeUtils.unescapeHtml3(p);
					i++;
				}
			}
			return productName;
		}
	  	
	  	public String[] getProductPrice(String[] evaluateResult) {

			String[] productPrice = new String[evaluateResult.length];
			int i = 0;
			if(evaluateResult.length > 0) {
				for( String p : evaluateResult){
					p = FilterUtil.getStringBetween(p, "<span class=\"label\">ราคา</span>", "<br>");
					p = FilterUtil.toPlainTextString(p);
					p = FilterUtil.removeCharNotPrice(p);
					productPrice[i] = p;
					i++;
				}
			}
			return productPrice;
		}
	  	
	  	public String[] getProductBasePrice(String[] evaluateResult) {
			
	  		String[] productBasePrice = new String[evaluateResult.length];
	  		int i =0;
			if(evaluateResult.length > 1) {
				for( String p : evaluateResult){
					p = FilterUtil.getStringBetween(p, "<span class=\"product-detail-list-right-price-normal\">ปกติ <span>", "</span>");
					p = FilterUtil.toPlainTextString(p);
					p = FilterUtil.removeCharNotPrice(p);
					
					productBasePrice[i] = p;
					i++;
				}
			}
			return productBasePrice;
		}

		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
			if(evaluateResult.length == 1) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"padBot20 padTop10\">", "</div>");
				productDesc += " " + FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"grey08em padBot20 \">","</div>");
				productDesc = FilterUtil.toPlainTextString(productDesc);
				productDesc = StringEscapeUtils.unescapeHtml3(productDesc);
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

	        }
	    	return new String[] {productPictureUrl};
		}
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			String productExp = "false";
			if(evaluateResult.length == 1)
			{
				if(evaluateResult[0].contains("สินค้าหมดชั่วคราว")){
					productExp = "true";
				}
			}
			return new String[] {productExp};
		}
		
		@Override
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();
			String[] productBasePrice = mpdBean.getProductBasePrice();

			if (productName == null || productName.length == 0
					|| productPrice == null || productPrice.length == 0) {
				return null;
			}
			ProductDataBean[] rtn = new ProductDataBean[productName.length-1];
			for(int i=0;i<productName.length-1;i++){
				rtn[i] = new ProductDataBean();
				rtn[i].setName(productName[0]+" "+productName[i+1]);
				rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));
				
				if (productDescription != null && productDescription.length != 0) {
					rtn[i].setDescription(productDescription[0]);
				}
				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
						if (productPictureUrl[0].startsWith("http")) {
							rtn[i].setPictureUrl(productPictureUrl[0]);
						} else {
							rtn[i].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
						}
					}
				}

				if (productBasePrice != null && productBasePrice.length != 0) {
					rtn[i].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				}
				
			}

			

			return rtn;
		}
		
}
