package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class GoodChoizHTMLParser extends DefaultHTMLParser{

	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"prices\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">"});
		map.put("basePrice", new String[]{"<div class=\"old-product-price\">"});
		map.put("description", new String[]{"<div class=\"full-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"picture-wrapper\">"});
		map.put("expire", new String[]{""});
		return map;
	}
	
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("discontinued-product")) {
    			return new String[] {"true"};
    		}
    	}
		return new String[] {"false"};
    }
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String rawProductPrice = FilterUtil.getStringBetween(evaluateResult[0], "itemprop=\"price\" ", "</div>");
    		rawProductPrice = FilterUtil.getStringAfter(rawProductPrice, ">", rawProductPrice);
    		productPrice = FilterUtil.toPlainTextString(rawProductPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length == 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	

}