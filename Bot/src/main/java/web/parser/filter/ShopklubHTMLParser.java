package web.parser.filter;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ShopklubHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{"<div class=\"product-shop\">"});
		map.put("description", 	new String[]{"<div id=\"product_tabs_description_tabbed_contents\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-img-box\">"});
		map.put("nameDescription", new String[]{"<ul class=\"tier-prices product-pricing\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			String status = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"availability in-stock\">", "</span>");
			if(StringUtils.isNotBlank(status)){
				String rawName = FilterUtil.getStringBetween(evaluateResult[0], " <h1>", "</h1>");
				String productId = FilterUtil.getStringBetween(evaluateResult[0], "<ul class=\"add-to-links\">", "onclick");
				productId = FilterUtil.getStringBetween(productId, "product/", "/");
				productName = FilterUtil.toPlainTextString(rawName+" ("+productId+")");
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</div>");
    		if(rawPrice.trim().length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price-box\">", "</div>");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductNameDescription(String[] evaluateResult) {
		
		String productNameDescription = null;
		if(evaluateResult.length == 1){
			List<String> priceList = FilterUtil.getAllStringBetween(evaluateResult[0], "<li>", "</li>");
			double cheapest = BotUtil.CONTACT_PRICE;
			if(priceList != null && priceList.size()> 0){
				for (String priceStr : priceList) {
					String price = FilterUtil.getStringBetween(priceStr, "<span class=\"price\">", "</span>");
					price = FilterUtil.toPlainTextString(price);
					price = FilterUtil.removeCharNotPrice(price);
					double p = Double.parseDouble(price);
					if(p < cheapest){
						productNameDescription = FilterUtil.toPlainTextString(priceStr).replace("&nbsp;", "");
						cheapest = p;
					}
				}
			}
		}
		return new String[] {productNameDescription};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productNameDescription = mpdBean.getProductNameDescription();
		String currentUrl = mpdBean.getCurrentUrl();
		String price = "";

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		if(productNameDescription != null && productNameDescription.length > 0){
			if(StringUtils.isNotBlank(productNameDescription[0])){
				price = FilterUtil.getStringBetween(productNameDescription[0], "for", "each");
				price = FilterUtil.toPlainTextString(price);
				price = FilterUtil.removeCharNotPrice(price);
				productDescription[0] = "[" + productNameDescription[0] + "] " + productDescription[0];
			}
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if(StringUtils.isNotBlank(price)){
			rtn[0].setPrice(FilterUtil.convertPriceStr(price));
		}else{
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}

		return rtn;
	}

	    
}