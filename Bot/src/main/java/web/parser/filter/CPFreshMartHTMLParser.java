package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;


import utils.FilterUtil;

public class CPFreshMartHTMLParser extends BasicHTMLParser{

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.product-title");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("span.amount");
			Element ex = e.select("span").last();
			productPrice = 	FilterUtil.removeHtmlTag(ex.html());
			productPrice  = FilterUtil.removeCharNotPrice(productPrice);
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div[itemprop=offers]");
			e = e.select("span[id^=old-price-]");	
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-images");
			e = e.select("img");
			productPrice = (e.attr("src"));			
		}	
		return new String[] {productPrice};
	}
	    
}