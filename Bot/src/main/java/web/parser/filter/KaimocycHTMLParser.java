package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KaimocycHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name",			new String[]{"<table id=\"content_TblDetailData\" class=\"detailtable\">"});
		map.put("price", 		new String[]{"<td class=\"detailprice\" style=\"padding-top: 2px; border-bottom: 1px solid #D0D0D0\">"});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl",	new String[]{"<div class=\"photoview\" style=\"position: relative;\">"});
		map.put("expire", 		new String[]{""});
		map.put("realProductId",new String[]{""});
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			

//			ยี่ห้อ / รุ่น + คำว่า ปี + รุ่นปี + คำว่า สี + สี + (สภาพ)
//			HONDA Scoopy i s12 ปี 2016 สีน้ำเงิน (รถมือสอง)
//			ยี่ห้อ / รุ่น + ประเภท + คำว่า 'ปี' + รุ่นปี + คำว่า 'สี' + สี + เกียร์ + (รถมือสอง)
//			HONDA Rebel 500 บิ๊กไบค์ ปี 2017 สีเทา เกียร์ธรรมดา (รถมือสอง)
			String brand 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalMakeModel\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			String category 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalCategory\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			String year 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalYear\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			String color 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalColor\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			String transmission 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalTransmission\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			String status 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalCondition\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
			
			if(StringUtils.isNotBlank(brand))			productName += brand;
			if(StringUtils.isNotBlank(category))		productName += " "+category;
			if(StringUtils.isNotBlank(year))			productName += " ปี " + year;
			if(StringUtils.isNotBlank(color))			productName += " สี" + color;
			if(StringUtils.isNotBlank(transmission))	productName += " "+transmission;
			if(StringUtils.isNotBlank(status))			productName += " (" + status + ")";

		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		
//    		ประเภท + เกียร์ + cc. + คำว่า เลขไมล์ (กม.) + เลขไมล์ (กม.) + .
//    		มอเตอร์ไซค์ทั่วไป เกียร์ธรรมดา 108 ccm เลขไมล์ (กม.) 11,067 km. รถส่งมา 18 งวด แล้วค่ะ ที่ขาย เพราะอยากได้ m slaz ส่งต่อ อีกแค่ 18 งวด งวดละ 2590 บาท ค่ะ รถไม่เคย ล้ม เพราะ ใช้คนเดียว ขับไปกับแค่ที่ทำงานและบ้าน
    		
    		String category 		= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalCategory\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
    		String transmission 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalTransmission\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
    		String ccm			 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalCcm\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
    		String mileage		 	= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(FilterUtil.getStringBetween(evaluateResult[0], "<tr id=\"content_OptionalMileage\">", "</tr>"),"<td class=\"detaildata\">","</td>"));
    		String desc 			= FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0], "<p id=\"content_OptionalDescription\">", "</p>"));
    		
    		if(StringUtils.isNotBlank(category))		productDesc += category;
    		if(StringUtils.isNotBlank(transmission))	productDesc += " " + transmission;
    		if(StringUtils.isNotBlank(ccm))				productDesc += " " + ccm;
    		if(StringUtils.isNotBlank(mileage))			productDesc += " เลขไมล์ (กม.) " + mileage;
    		if(StringUtils.isNotBlank(desc))			productDesc += ". " + desc;
    		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
	String sold = FilterUtil.getStringBetween(evaluateResult[0],"<div id=\"content_DivSold\" style=\"position: absolute; top: 40%; width: 100%;\">","</div>");
	boolean soldBoolean = false;
	if (sold.contains("ขายไปแล้ว")) {
		soldBoolean = true;
	}
	String year = FilterUtil.getStringBetween(evaluateResult[0],"<span style=\"font-size: 11px;\" class=\"detaillegend\">","</span>");
	boolean yearBoolean = true;
	if (year.contains("2018")||year.contains("2019")) {
		yearBoolean = false;
	}
	String pdExpire = String.valueOf(yearBoolean||soldBoolean);
	return new String[] {pdExpire};
}
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String real = "";
		if(evaluateResult.length > 0) {
			real = FilterUtil.getStringBetween(evaluateResult[0],"id: '","'");
		}
		return new String[]{real};
	}
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}
		
//		เข้าหมวด : 70201 
//		keyword : มอเตอร์ไซค์มือสอง

//		rtn[0].setCategoryId(70201);
//		rtn[0].setKeyword("มอเตอร์ไซค์มือสอง");
		
		return rtn;
	}
	    
}