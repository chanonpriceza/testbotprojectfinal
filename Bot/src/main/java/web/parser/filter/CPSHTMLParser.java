package web.parser.filter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CPSHTMLParser extends DefaultHTMLParser{
			
	
	//FilterUtil
	/*
	    
		FilterUtil.removeCharNotPrice(String price)
          - to remove char except "0123456789."

		FilterUtil.toPlainTextString(String html);
       
		FilterUtil.getStringAfter(target, begin, defaultValue)
		FilterUtil.getStringAfterLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBefore(target, begin, defaultValue)
		FilterUtil.getStringBeforeLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBetween(target, begin, end)

	*/
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{"<span class=\"SpePrice\">"});
		map.put("basePrice", new String[]{"<span class=\"NmPrice\">"});
		map.put("description", new String[]{"<div class=\"divProductDescription\">"});
		map.put("pictureUrl", new String[]{"<div class=\"gall-item-container\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
//			String rawPID = FilterUtil.getStringBetween(evaluateResult[0], "<b>รหัสสินค้า :", "</b><br />");
//			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span  class=\"topicColor\">", "</span>");
//			rawName = rawName.replace("Oc&#233;", "");
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {BotUtil.CONTACT_PRICE_STR};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";    	
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"NmPrice\">", "</span>");
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียดสินค้า", evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String pId = null;
		if(evaluateResult.length == 1) {
			pId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" id=\"hiddenProductID\" value=\"", "\"");
			pId = FilterUtil.toPlainTextString(pId);
		}
		return new String[] {pId};
	}
	
	private String httpRequest(String productId) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	try {
	 		u = new URL("https://www.cps.co.th/page/page_product_item/ajaxPageProductItemController.php");
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(false);
	 		conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
	 		conn.setRequestMethod("POST");
	 		conn.setDoOutput(true);
	 	
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(("productID="+ productId +"&attributeSet=%5B%220%22%5D&type=getAttributeGroupID").getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		isr = new InputStreamReader(conn.getInputStream());
	 		brd = new BufferedReader(isr);
	 		StringBuilder rtn = new StringBuilder(5000);
	 		String line = "";
	 		while ((line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 		return rtn.toString();
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e2) {
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
		return null;
	}
	 
	   @Override
	    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();
			String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
			String[] realProductId = mpdBean.getRealProductId();
			
			if (productName == null || productName.length == 0 || productName[0] == "") {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			String name = productName[0];
			if(realProductId != null && realProductId.length != 0 ){
				name += " ("+ realProductId[0] +")";
			}
			rtn[0].setName(name);
			
			String basePrice = "";
			String price = "";
			
			price = httpRequest(realProductId[0]);
			basePrice = price;
			price = FilterUtil.getStringBetween(price, "<span class=\\\"h4\\\">", "<\\/span>");
			if(price.isEmpty()) {
				price = FilterUtil.getStringBetween(basePrice, "<span class=\\\"price h4\\\">", "<\\/span>");
			}
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
			
			if(price.isEmpty() || price.equals("0.0") || price.equals("0") ){
				price = BotUtil.CONTACT_PRICE_STR;
    		}
			
			rtn[0].setPrice(FilterUtil.convertPriceStr(price));
			
			basePrice = FilterUtil.getStringBetween(basePrice, "<span class=\\\"productOldPrice h5\\\">", "<\\/span>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(basePrice));

			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}

			if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null) {
					rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
				}
			}
			return rtn;
		} 

	    
}