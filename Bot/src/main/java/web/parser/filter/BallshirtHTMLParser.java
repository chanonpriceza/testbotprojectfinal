package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BallshirtHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "utf-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("basePrice", new String[]{"<div class=\"summary entry-summary\">"});
		map.put("description", new String[]{"<div class=\"woocommerce-tabs wc-tabs-wrapper\">"});
		map.put("pictureUrl", new String[]{"<figure class=\"woocommerce-product-gallery__wrapper\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<ins>", "</ins>");
    		if (tmp.equals("")) {
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "&nbsp;");
    		} else {
    			tmp = FilterUtil.getStringBetween(tmp, "<span class=\"woocommerce-Price-amount amount\">", "&nbsp;");
    		}
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<ins>", "</ins>");
    		if(!tmp.isEmpty()){
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "&nbsp;");
    			tmp = StringEscapeUtils.unescapeHtml4(tmp);
    			productPrice = FilterUtil.toPlainTextString(tmp);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab\" id=\"tab-description\" role=\"tabpanel\" aria-labelledby=\"tab-title-description\">", "</div>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl); 
        }
    	return new String[] {productPictureUrl};
	}
	
	
	    
}