package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TheFaceShopThailandHTMLParser extends DefaultHTMLParser{

	@Override
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name",				new String[]{""});
		map.put("price",			new String[]{""});
		//map.put("basePrice",		new String[]{"<div class=\"price-box\">"});
		map.put("description",		new String[]{""});
		map.put("pictureUrl",		new String[]{""});
		map.put("expire",			new String[]{""});
		return map;
	}
	
	@Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1){
			evaluateResult[0] = Jsoup.parse(evaluateResult[0]).select("h1").html();
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length == 1){
			evaluateResult[0] = Jsoup.parse(evaluateResult[0]).select("span.price").html();
			productPrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length == 1){
			basePrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"old-price\">", "</p>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDescription = ""; 
		if(evaluateResult.length == 1){
			evaluateResult[0] = Jsoup.parse(evaluateResult[0]).select("div.detail-product").html();
			productDescription = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDescription};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productUrl = "";
		if(evaluateResult.length == 1){
			evaluateResult[0] = Jsoup.parse(evaluateResult[0]).select("div.MagicToolboxContainer").html();
			productUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] {productUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		evaluateResult[0] = Jsoup.parse(evaluateResult[0]).select("p.availability").html();
		String expire = "false";
		if(evaluateResult[0].contains("สินค้าหมด")){
			expire = "true";
		}
		return new String[] {expire};
	}
}
