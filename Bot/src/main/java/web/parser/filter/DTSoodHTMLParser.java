package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;

public class DTSoodHTMLParser extends TemplateIGetWebHTMLParser{
		
	public String getCharset() {
		return "TIS-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title\">", "<h1 class=\"entry-full-title prodtc-detail-title\">"});
		map.put("price", new String[]{"<span class=\"num\" id=\"product_price\" style=\"margin:0; padding:0;\">"});
		map.put("description", new String[]{"<div class=\"clear_desc clearfix\">"});
		map.put("pictureUrl", new String[]{"<div  id=\"photoshow\">", "<div id=\"photoshow\">", "<div  id=\"photoshow\" class=\"third\">", "<div id=\"photoshow\" class=\"third\">","<div  id=\"photoshow\" class=\" default\">"});
		
		return map;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[0]);    
    		if(tmp.contains("ราคา") || tmp.isEmpty()) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else {
    			if(tmp.contains("ลดเหลือ")){
    				tmp = FilterUtil.getStringAfter(tmp, "ลดเหลือ", "");
    			} else if(tmp.contains("-")) {
    				tmp = FilterUtil.getStringAfter(tmp, "-", "");
    			}
        		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		} 
        }		
		return new String[] {productPrice};
	} 
}