package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.DefaultHTMLParser;

public class TemplateLazadaJSIncludeIdHTMLParser extends DefaultHTMLParser{
	
	protected static Logger   logger = LogManager.getRootLogger();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 	new String[]{""});
		map.put("price", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String html = mpdBean.getProductName()[0];
    	String currentUrl = FilterUtil.getStringBefore(mpdBean.getCurrentUrl(), "?", mpdBean.getCurrentUrl());

    	String allScriptTxt = FilterUtil.getStringBetween(html, "try{", "} catch(e) {");
    	String allObjectTxt = FilterUtil.getStringBetween(allScriptTxt, "app.run(", ");");
    	
    	if(StringUtils.isBlank(allObjectTxt)) return null;
    	try {
			JSONObject appObj = (JSONObject) new JSONParser().parse(allObjectTxt);
			if(appObj == null) return null;
			
			JSONObject dataObj = (JSONObject) appObj.get("data");
			if(dataObj == null) return null;
			
			JSONObject rootObj = (JSONObject) dataObj.get("root");
			if(rootObj == null) return null;
			
			JSONObject fieldObj = (JSONObject) rootObj.get("fields");
			if(fieldObj == null) return null;
			
			JSONObject skuInfoObj = (JSONObject) fieldObj.get("skuInfos");
			if(skuInfoObj == null) return null;
			
			JSONObject productObj = (JSONObject) fieldObj.get("product");
			if(productObj == null) return null;
			
			JSONObject sku0Obj = (JSONObject) skuInfoObj.get("0");
			if(sku0Obj == null) return null;
			
			JSONObject dataLayerObj = (JSONObject) sku0Obj.get("dataLayer");
			if(dataLayerObj == null) return null;
			
			JSONObject priceObj = (JSONObject) sku0Obj.get("price");
			if(priceObj == null) return null;
			
			String sellerName = String.valueOf((Object) dataLayerObj.get("seller_name"));
			
			String itemId = String.valueOf((Object) sku0Obj.get("itemId"));
			String skuId = String.valueOf((Object) sku0Obj.get("skuId"));
			
			String productName = String.valueOf((Object) productObj.get("title"));
			if(StringUtils.isNotBlank(productName)) {
				productName = FilterUtil.toPlainTextString(productName);
				if(productName.length() > 200) {
					productName = productName.substring(0, 180);				
				}
			}
			
			String productDesc = String.valueOf((Object) productObj.get("highlights"));
			if(StringUtils.isNotBlank(productDesc)) productDesc = FilterUtil.toPlainTextString(productDesc);
			
			if("null".equals("null"))
				productDesc = null;
			
			String productPrice = String.valueOf((Object) dataLayerObj.get("pdt_price"));
			String productBasePrice = "0";
			JSONObject originalPriceObj = (JSONObject) priceObj.get("originalPrice");
			JSONObject salePriceObj = (JSONObject) priceObj.get("salePrice");
			if(originalPriceObj != null && salePriceObj != null) {
				String originalPrice = String.valueOf((Object) originalPriceObj.get("text"));
				String salePrice = String.valueOf((Object) salePriceObj.get("text"));
				originalPrice = FilterUtil.removeCharNotPrice(originalPrice);
				salePrice = FilterUtil.removeCharNotPrice(salePrice);
				if(!originalPrice.equals(salePrice)) {
					productPrice = salePrice;
					productBasePrice = originalPrice;
				}
			}
			JSONObject q = (JSONObject) sku0Obj.get("quantity");
			if(q!=null) {
				String ex = String.valueOf(q.get("text"));
				if(ex.contains("สินค้าหมด")) {
					return null;
				}
				
			}
			String productImage = String.valueOf((Object) sku0Obj.get("image"));
			if(!productImage.startsWith("http")) {
				URL url = new URL(new URL(currentUrl), productImage);
				productImage = url.toString();
				productImage = productImage.replace(".jpg", ".jpg_400x400q80.jpg");
			}
			
			ProductDataBean result = getResult(sellerName, productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
			result = genDynamicField(result, appObj);
			if(result != null) {
				return new ProductDataBean[] {result};
			}
			
		} catch (Exception e) {
			logger.error(e);
		}
    	
    	return null;
	}
	    
	protected ProductDataBean getResult(String sellerName, String productName, String productPrice, String productBasePrice, 
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {
		
		if(sellerName.contains(BaseConfig.CONF_MERCHANT_NAME)) {
			return gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
		}
		return null;
	}
	
	protected ProductDataBean genDynamicField(ProductDataBean pdb, JSONObject appObj) {
		return pdb;
	}
	
	protected ProductDataBean gatherData(String productName, String productPrice, String productBasePrice,
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {
		
		ProductDataBean rtn = new ProductDataBean();
		rtn.setName(productName + " (" + skuId + ")");
		rtn.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice)));
		rtn.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productBasePrice)));
		rtn.setUrl("https://c.lazada.co.th/t/c.PVm?url="+ URLEncoder.encode(TemplateLazadaHTMLParser.generateURL(currentUrl), Charset.defaultCharset()));
		if(BaseConfig.MERCHANT_ID==440001||BaseConfig.MERCHANT_ID==400314)
			rtn.setUrl("https://c.lazada.co.th/t/c.PVm?url="+ URLEncoder.encode(TemplateLazadaHTMLParser.generateURL(currentUrl+"&pztrack"), Charset.defaultCharset()));
		rtn.setUrlForUpdate(currentUrl);
		rtn.setDescription(productDesc);
		rtn.setPictureUrl(productImage);
		rtn.setRealProductId(skuId);
		
//		System.out.println("rtn.getName();           " + rtn.getName());
//		System.out.println("rtn.getPrice();          " + rtn.getPrice());
//		System.out.println("rtn.getBasePrice();      " + rtn.getBasePrice());
//		System.out.println("rtn.getUrl();            " + rtn.getUrl());
//		System.out.println("rtn.getUrlForUpdate();   " + rtn.getUrlForUpdate());
//		System.out.println("rtn.getDescription();    " + rtn.getDescription());
//		System.out.println("rtn.getPictureUrl();     " + rtn.getPictureUrl());
//		System.out.println("rtn.getRealProductId();  " + rtn.getRealProductId());
		
		return rtn;
	}
	
	
	@Override
	public ProductDataBean[] parse(String url) {
		try {
			String checkResult = checkUrlBeforeParse(url);
			if(checkResult != null && checkResult.equals("delete"))
				return processHttpStatus(null);
			
			String targetUrl = url;
	        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
	            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"})) {
	            	targetUrl = BotUtil.encodeURL(url);
	            }
	        }
			
			String[] htmlContent = null;
			htmlContent = HTTPUtil.httpRequestWithStatusIgnoreCookies(targetUrl, "UTF-8", false);
			
			if (htmlContent == null || htmlContent.length != 2)
				return null;
			
			String httpStatus = htmlContent[1];
			if(httpStatus.equals(String.valueOf(HttpURLConnection.HTTP_BAD_REQUEST)))
				return processHttpStatus(htmlContent[1]);
			
			String html = htmlContent[0];
			if(html == null)
				return processHttpStatus(httpStatus, false);
			
			Map<String, String[]> config = getConfiguration();
			if (config == null)
				return null;
			
			String[] productName = null;
			String[] productPrice = null;
			String[] productUrl = null;
			String[] productDescription = null;
			String[] productPictureUrl = null;
			String[] merchantUpdateDate = null;
			String[] productNameDescription = null;
			String[] productExpire = null;
			String[] realProductId = null;
			String[] productBasePrice = null;
			String[] productUpc = null;
			String[] data = null;
			
			String[] result = evaluate(html, config.get("expire"));
			if (result != null) {
	    		productExpire = getProductExpire(result);                
	    		if(productExpire != null && productExpire.length == 1 && productExpire[0].equals("true"))
	    			return processHttpStatus(httpStatus);
	    	}
			
			result = evaluate(html, config.get("name"));
			if (result != null) {
				productName = getProductName(result);
				productName = FilterUtil.formatHtml(productName);
				productName = FilterUtil.removeSpace(productName);
			}
			
			result = evaluate(html, config.get("price"));
			if (result != null) {
				productPrice = getProductPrice(result);
				productPrice = FilterUtil.removeSpace(productPrice);
			}
			result = evaluate(html, config.get("url"));
			if (result != null) {
				productUrl = getProductUrl(result);
				productUrl = FilterUtil.removeSpace(productUrl);
			}
			result = evaluate(html, config.get("description"));
			if (result != null) {
				productDescription = getProductDescription(result);
				productDescription = FilterUtil.formatHtml(productDescription);
				productDescription = FilterUtil.removeSpace(productDescription);
			}
			result = evaluate(html, config.get("pictureUrl"));
			if (result != null) {
				productPictureUrl = getProductPictureUrl(result);				
				productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
			}
			result = evaluate(html, config.get("merchantUpdateDate"));
			if (result != null) {
				merchantUpdateDate = getProductMerchantUpdateDate(result);
				merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
				merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
			}
			result = evaluate(html, config.get("nameDescription"));
			if (result != null) {
				productNameDescription = getProductNameDescription(result);
				productNameDescription = FilterUtil.removeSpace(productNameDescription);
			}
						
			result = evaluate(html, config.get("realProductId"));
			if (result != null) {
				realProductId = getRealProductId(result);
				realProductId = FilterUtil.removeSpace(realProductId);
			}
			
			result = evaluate(html, config.get("basePrice"));
			if (result != null) {
				productBasePrice = getProductBasePrice(result);
				productBasePrice = FilterUtil.removeSpace(productBasePrice);
			}
			
			result = evaluate(html, config.get("upc"));
			if (result != null) {
				productUpc = getProductUpc(result);
				productUpc = FilterUtil.removeSpace(productUpc);
			}
			
			result = evaluate(html, config.get("data"));
			if (result != null) {
				data = getData(data);
				data = FilterUtil.removeSpace(data);
			}
			
			ProductDataBean[] productDataBean = null;
			FILTER_TYPE filterType = getFilterType();
			MergeProductDataBean mpdBean = new MergeProductDataBean();
			mpdBean.setProductName(productName);
			mpdBean.setProductPrice(productPrice);
			mpdBean.setProductUrl(productUrl);
			mpdBean.setProductDescription(productDescription);
			mpdBean.setProductPictureUrl(productPictureUrl);
			mpdBean.setMerchantUpdateDate(merchantUpdateDate);
			mpdBean.setProductNameDescription(productNameDescription);
			mpdBean.setRealProductId(realProductId);
			mpdBean.setCurrentUrl(url);
			mpdBean.setProductBasePrice(productBasePrice);
			mpdBean.setProductUpc(productUpc);
			mpdBean.setData(data);
			
			if (filterType.equals(FILTER_TYPE.DEFAULT)) {
	        	productDataBean = mergeProductData(mpdBean);   	   	
	        } else if (filterType.equals(FILTER_TYPE.PRODUCTLIST)) {
	        	result = evaluate(html, config.get("allData"));
				if (result != null) {
					productDataBean = getAllProductData(result);
					for (int i = 0; i < productDataBean.length; i++) {
						if(productDataBean[i] == null)
							continue;
						String name = FilterUtil.formatHtml(productDataBean[i].getName());
						name = FilterUtil.removeSpace(name);
						productDataBean[i].setName(name);
						
						String description = FilterUtil.formatHtml(productDataBean[i].getDescription());
						description = FilterUtil.removeSpace(description);
						productDataBean[i].setDescription(description);
						
						productDataBean[i].setPictureUrl(FilterUtil.removeSpace(productDataBean[i].getPictureUrl()));
					}
				}	        	                
	        	productDataBean = mergePriceListProductData(productDataBean, mpdBean);
	        }
			
			if(isEmpty(productDataBean))
				productDataBean = new ProductDataBean[]{ new ProductDataBean()};
			productDataBean[0].setHttpStatus(httpStatus);
			
			return productDataBean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus) {
		return processHttpStatus(httpStatus, true);
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
		ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
		rtn[0].setHttpStatus(httpStatus);
		rtn[0].setExpire(expire);
		return rtn;
	}
	
}
