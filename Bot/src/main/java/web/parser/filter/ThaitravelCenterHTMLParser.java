package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThaitravelCenterHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("basePrice", new String[]{""});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		productPrice = FilterUtil.toPlainTextString(doc.select("h2#tour-name").html());
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("span.text-green");
    		productPrice  = FilterUtil.getStringBetween(e.html(),"<strong>","</strong>");
    		if(productPrice.contains("<span style=\"color:#f00\">")) {
    			productPrice =  FilterUtil.getStringBetween(e.html(),"<span style=\"color:#f00\">","</span>");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}

	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("span.text-green");
    		productPrice  = FilterUtil.getStringBetween(e.html(),"<strong>","</strong>");
    		if(productPrice.contains("<span style=\"color:#f00\">")) {
    			productPrice =  FilterUtil.getStringBetween(e.html(),"<span class=\"strike\">","</span>");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		if(evaluateResult.length > 0) {
			productDes = FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"description\" content=\"","\""));
			productDes = FilterUtil.removeHtmlTag(productDes);
		}
		return new String[] {productDes};
	}


	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("meta");
    		productPic = FilterUtil.getStringBetween(e.toString(),"\"og:image\" content=\"","\"");	
		}
		return new String[] {productPic};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String real = "";
		if (evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("span#hiddenLogin");
    		real = e.html();
    		real = FilterUtil.toPlainTextString(real);
		}
		return new String[] {real};
		
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        String tmp = "";

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if(realProductId!=null&&realProductId.length>0) {
			tmp = " ("+realProductId[0]+")";
		}
		rtn[0].setName(productName[0]+tmp);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}

	
}
