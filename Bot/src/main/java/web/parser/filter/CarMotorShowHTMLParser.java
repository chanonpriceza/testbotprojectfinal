package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CarMotorShowHTMLParser extends DefaultHTMLParser{
			
//	public String getCharset() {
//		return "UTF-8";
//	}

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 			new String[]{""});
		map.put("price", 			new String[]{"<span id=\"price_auto\" class=\"pull-left mg-right-10 radius pd-right-20 pd-top-10 pd-bottom-10 gray-price fsize-18 fweight-bold lh-20\">"});
		map.put("description", 		new String[]{"<ul class=\"info-detail-prd baseinfo box\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"show-img\">"});
		map.put("realProductId", 	new String[]{""});
		map.put("expire", 			new String[]{"<div class=\"notify-expires\">"});
		
		return map;
	}
    public String[] getRealProductId(String[] evaluateResult) {
    		String Id = "";
    		if(evaluateResult.length == 1){
    			Id = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" value=\"", "\" id=\"hddProductId\" />");
    			Id = FilterUtil.toPlainTextString(Id);
    		}
    		return new String[] {Id};
    	}
  
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {
			productName =  FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"blue-title\">", "</h1>");
			if(productName.isEmpty()){
				return null;
			}
			
			String condition = FilterUtil.getStringBetween(evaluateResult[0], "<i class=\"icon-filter-12\">", "</li>");
			condition = FilterUtil.getStringBetween(condition, "<span class=\"cont\">", "</span>");
			
			if(!condition.isEmpty()){
				productName +=  " " + 	condition;		
			}
			productName = FilterUtil.toPlainTextString(productName);
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().equals("0")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {

    	if (evaluateResult.length > 0) {
    		if (evaluateResult[0].indexOf("expires.png") > -1) {
    			return new String[]{"true"};
    		}
    		if (evaluateResult[0].indexOf("auto-sold.png") > -1) {
    			return new String[]{"true"};
    		}
        }
    	return new String[]{"false"};
	}
	
	 @Override
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String[] realProductId = mpdBean.getRealProductId();
			String[] basprice = mpdBean.getProductBasePrice();
			
			if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0]);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (basprice != null && basprice.length != 0) {
				try{
				rtn[0].setBasePrice( Double.parseDouble(basprice[0]));
				}catch (Exception e) {
					rtn[0].setBasePrice(0.00);
				}
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
				}
			}

			// can remove if realProductId, basePrice and upc are not required
			if (realProductId != null && realProductId.length != 0) {
				rtn[0].setRealProductId(realProductId[0]);
				if (StringUtils.isNoneBlank(productName[0])) {
					rtn[0].setName(productName[0] +" ("+ realProductId[0] +")");
				}	
			}

			return rtn;
		}    
}