package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TemplateShopeeRealtimeHTMLParser extends DefaultHTMLParser{
	
	private static Logger logger = LogManager.getRootLogger();
	
	@Override
	public ProductDataBean[] parse(String url) {
		String shopId = "";
		String itemId = "";

		if(url.contains("/universal-link/product/")) {
			String tmp = FilterUtil.getStringBetween(url, "/universal-link/product/", "?");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
		} else if (url.contains("/universal-link/")){
			String tmp = FilterUtil.getStringBetween(url, "-i.", "?");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
		} else if (url.contains("/product/")) {
			String tmp = FilterUtil.getStringAfter(url, "/product/", "");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		} else {
			String tmp = FilterUtil.getStringAfter(url, "-i.", "");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		}
		
		if(StringUtils.isNotBlank(shopId) && StringUtils.isNotBlank(itemId)) {
			String newUrl = "https://shopee.co.th/api/v2/item/get?itemid=" + itemId + "&shopid=" + shopId;
			return super.parse(newUrl);
		}
				
		return super.parse(url);
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 	new String[]{""});
		map.put("price", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String json = mpdBean.getProductName()[0];
    	
    	try {
			JSONObject obj = (JSONObject) new JSONParser().parse(json);
			if(obj == null) return null;
			
			JSONObject itemObj = (JSONObject) obj.get("item");
			if(itemObj == null) return null;
			
			String tempPrice = StringUtils.defaultString(String.valueOf(itemObj.get("price")), "");
			String resultName = StringUtils.defaultString(String.valueOf(itemObj.get("name")), "");
			String resultPrice = "0";

			if(NumberUtils.isCreatable(tempPrice)) {
				int priceFull = Integer.parseInt(tempPrice);
				int priceReal = priceFull / 100000;
				if(priceReal > 0) resultPrice = String.valueOf(priceReal);
			}
			
			ProductDataBean resultBean = new ProductDataBean();
			resultBean.setPrice(FilterUtil.convertPriceStr(resultPrice));
			resultBean.setName(resultName);
			return new ProductDataBean[] {resultBean};
			
		} catch (Exception e) {
			logger.error(e);
		}
    	return null;
	}
	    
}
