package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class ChimChunShopHTMLParser extends DefaultHTMLParser{
		
	public String getCharset() {
		return "utf-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"title col-xs-12 col-sm-12 col-md-12 col-lg-12\">"});
		map.put("basePrice", new String[]{"<span class=\"show-discount-price disable primary-text\">"});
		map.put("price", new String[]{"<div class=\"price disable-text col-xs-12 col-sm-12 col-md-12 col-lg-12\">"});
		map.put("description", new String[]{"<div class=\"description top-border bottom-border col-xs-12 col-sm-12 col-md-12 col-lg-12\">"});
		map.put("pictureUrl", new String[]{"<div class=\"body-main-image\">"});
		
		
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
}