package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PlayHouseHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
//		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product_title entry-title\">", "Add to cart");
    		productPrice = FilterUtil.getStringBetween(productPrice, "</span></span></del> <ins>", "spanclass=\"woocommerce-Price-currencySymbol\">");
    		productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
    		if(productPrice.equals("")){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product_title entry-title\">", "Add to cart");
    			productPrice = FilterUtil.getStringBetween(productPrice, "class=\"woocommerce-Price-amount amount\"", "</span></span></p>");
    			productPrice = FilterUtil.getStringBetween(productPrice, ">", "<span");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product_title entry-title\">", "Add to cart");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, "class=\"woocommerce-Price-amount amount\"", "</span></span></del> <ins>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, ">", "<span");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		if (evaluateResult.length > 0) {
			productDes = FilterUtil.getStringBetween(evaluateResult[0],"<h2>Description</h2><p>","<");
		}	
		return new String[] {productDes};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"class=\"woocommerce-product-gallery__wrapper\">","class=\"col-lg-6\">");
			productPic = FilterUtil.getStringBetween(productPic,"src=\"","\"");
		}	
		return new String[] {productPic};
	}
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "false";
		String area = "";
    	if(evaluateResult.length > 0){
    		area = FilterUtil.getStringBefore(evaluateResult[0], "<h1 class=\"product_title entry-title\">", "class=\"outofstock\">");
    		if (area.contains("Out of stock")) {
    			pdExpire = "true";
    		}
    	}
    	return new String[] {pdExpire};
	}
}
