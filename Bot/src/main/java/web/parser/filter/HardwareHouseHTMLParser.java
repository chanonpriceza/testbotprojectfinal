package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class HardwareHouseHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] {""});
		map.put("price", new String[] {"<div class=\"col-xs-12 col-sm-6 col-lg-5 product-shop\">"});
		map.put("basePrice", new String[] {"<div class=\"col-xs-12 col-sm-6 col-lg-5 product-shop\">"});
		map.put("description",new String[] { "<div class=\"product-specs\">" });
		map.put("pictureUrl", new String[] { "<div class=\"product-image  \">" });
		map.put("realProductId", new String[] { "<span class=\"product-code\">" });
		map.put("expire", new String[] { "<div class=\"col-xs-12 detail-product\">" });
		

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = ""; 
		if (evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "</title>");
		}
		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">Special Price</span>", "</p>");
				productPrice = FilterUtil.getStringBetween(productPrice, "\">", "</span>");
				
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] { productPrice };
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String basePrice = "";
		if (evaluateResult.length > 0) {				
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">ราคาปกติ:</span>", "</p>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);

		}
		
		return new String[] { basePrice };
	}
	
	

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
			productDesc = productDesc.replace("&#8211;", "");
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if (evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<strong>", "</strong></span>");
		}
		return new String[] { realProductId };
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
		if(evaluateResult[0].length() > 0) {
			if(evaluateResult[0].contains("สินค้าหมด")) {
				expire = "true";
			}
		}
		return new String[] {expire};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0 && realProductId[0] != "") {
			productName[0] = productName[0] + " (" + realProductId[0]+")";
		}
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		


		rtn[0].setPictureUrl(productPictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);


		return rtn;
	}
}

