package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KudsonHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String getCharset() {
		return "TIS-620";
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";    	
    	if (evaluateResult.length > 0) {
    		productName = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=og:title content='","'");
        }	    	
		return new String[] {productName};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan=2> <font color=696969>", "</td>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan=2><font color=696969><b>", "</b>");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		if(evaluateResult.length > 0) {
			productDes = FilterUtil.getStringBetween(evaluateResult[0],"<Td width=\"180\" style=\"border-width:1; border-style:dotted; border-color:666666;\">","</td></tr></table>");
			productDes = FilterUtil.toPlainTextString(productDes);
		}
		return new String[] {productDes};
	}


	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:image\" content=\"","\"");
		}
		return new String[] {productPic};
	}
}
