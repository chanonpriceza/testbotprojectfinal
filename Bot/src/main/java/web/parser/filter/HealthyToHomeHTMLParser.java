package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HealthyToHomeHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});

		
		return map;
	}
	
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";				
			if(evaluateResult.length > 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}		
			return new String[] {productName};
		}
		
		
		public String[] getProductPrice(String[] evaluateResult) {
			return new String[] {BotUtil.CONTACT_PRICE_STR};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			// TODO Auto-generated method stub
			return super.getProductBasePrice(evaluateResult);
		}
		
		@Override
		public String[] getProductDescription(String[] evaluateResult) {
			String productDec = "";
			if(evaluateResult.length > 0) {
				evaluateResult[0] = FilterUtil.getStringBetween(evaluateResult[0],"class=\"entry-content\">","<aside");
				productDec = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "</p>");
				productDec = FilterUtil.toPlainTextString(productDec);
			}
			return new String[] {productDec};
		}
		
		@Override
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String pic = "";
			//System.out.println(evaluateResult[0]);
			if(evaluateResult.length > 0) {
				evaluateResult[0] = FilterUtil.getStringBetween(evaluateResult[0],"class=\"entry-content\">","<aside");
				evaluateResult[0] =FilterUtil.getStringBetween(evaluateResult[0],"wp-image","/>");
				pic = FilterUtil.getStringBetween(evaluateResult[0],"src=\"","\"");
				
			}
			return new String[] {pic};
		}
		
		 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		    	String[] productName = mpdBean.getProductName();
		    	String[] productPrice = mpdBean.getProductPrice();
		        String[] productDescription = mpdBean.getProductDescription();
		        String[] productPictureUrl = mpdBean.getProductPictureUrl();
		        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
		        String[] realProductId = mpdBean.getRealProductId();
		        String currentUrl = mpdBean.getCurrentUrl();
		        String[] productBasePrice = mpdBean.getProductBasePrice();
		        String[] productUpc = mpdBean.getProductUpc();

		    	if (productName == null || productName.length == 0
						|| productPrice == null || productPrice.length == 0) {
					return null;
				}
				ProductDataBean[] rtn = new ProductDataBean[1];
				rtn[0] = new ProductDataBean();
				rtn[0].setName(productName[0]);
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

				if (productDescription != null && productDescription.length != 0) {
					rtn[0].setDescription(productDescription[0]);
				}
				
				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {				
						if(productPictureUrl[0].startsWith("http")) {
							rtn[0].setPictureUrl(productPictureUrl[0]);					
						} else {
							try {
								URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
								rtn[0].setPictureUrl(url.toString());
					    	} catch (MalformedURLException e) {
								
							}
						}
					}
				}

				if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
					Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
					if (date != null) {
						rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
					}
				}
				
				if (realProductId != null && realProductId.length != 0){
					rtn[0].setRealProductId(realProductId[0]);
				}
				
				if (productBasePrice != null && productBasePrice.length != 0) {
					rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				}
						
				if (productUpc != null && productUpc.length != 0) {
					rtn[0].setUpc(productUpc[0]);
				}
				
				
				return rtn;
			}
		    
		
		
}