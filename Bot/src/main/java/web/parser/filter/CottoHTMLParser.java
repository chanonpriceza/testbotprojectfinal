package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class CottoHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:title\" content=\"","\"");
			productName = FilterUtil.toPlainTextString(productName);
			productName = "cotto คอตโต้ "+productName;
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {BotUtil.CONTACT_PRICE_STR};
	}
	

	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:description\" content=\"","\"");
			productDesc = FilterUtil.toPlainTextString(productDesc);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
}
