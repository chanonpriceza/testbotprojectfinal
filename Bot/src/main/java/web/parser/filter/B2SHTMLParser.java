package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class B2SHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<table class=\"property\">"});
		map.put("nameDescription", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"product-image-wrap\">"});
		map.put("upc", new String[]{""});
		map.put("realProductId", new String[]{"<div class=\"sku\">"});
		 
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "</title>");			
			productName = productName.replace("&amp;", "");
			if(productName.equals("")) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product-name\" itemprop=\"name\">", "</h1>");
			}
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {    
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\"", "<p class=\"availability-only\">");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "<p class=\"availability-only\">");
        		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\" id=\"product-price", "/span>");
        		productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
    		}
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">Special Price</span>", "</p>");
        		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\" id=\"product-price-", "/span>");
        		productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
    		}
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"wapper-view\"><div class=\"price-box\">", "</p>");
    			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	    	
    	if (evaluateResult.length > 0) {    
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-type-data clearfix\">", "<p class=\"special-price\">");
    		productBasePrice = FilterUtil.getStringBetween(productBasePrice, "<p class=\"old-price\">", "</p>");
    		productBasePrice = FilterUtil.getStringBetween(productBasePrice, "<span class=\"price\" id=\"old-price-", "/span>");
    		productBasePrice = FilterUtil.getStringBetween(productBasePrice, ">", "<");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<tr><td>ชื่อสำนักพิมพ์</td><td>","</td>");
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc).replace(":", "").trim();
			productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
		return new String[] {productDesc};
	}
	
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String pdNameDescription = "";
		String translator = "";
		if(evaluateResult.length > 0) {
			pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<td> ชื่อศิลปิน/ ชื่อ ผู้แต่ง / ค่าย</td><td>", "</td>");
			pdNameDescription = StringEscapeUtils.unescapeHtml4(pdNameDescription).replace(":", "").trim();
			pdNameDescription = FilterUtil.toPlainTextString(pdNameDescription);
		
			translator = FilterUtil.getStringBetween(evaluateResult[0], "<td>ชื่อผู้แปล</td><td>", "</td>");
			translator = StringEscapeUtils.unescapeHtml4(translator).replace(":", "").trim();
			translator = FilterUtil.toPlainTextString(translator);
    	}
		return new String[] {pdNameDescription,translator};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductUpc(String[] evaluateResult) {
	   	String UPC = "";
	   	if(evaluateResult.length > 0) {
	   		UPC = FilterUtil.getStringAfter(evaluateResult[0],"<tr><td>ISBN</td>","");
	   		UPC = FilterUtil.getStringBetween(UPC,"<td>","</td>");
	   		if(!NumberUtils.isCreatable(UPC) || UPC.length() != 13){
	   			UPC = "";
	   		}
	   		
	   		if(UPC.equals("")) {
	   			
	   		}
	   	}
	    return new String[]{UPC}; 
   }
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if(evaluateResult.length > 0){
    		realProductId = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"title\"><strong>รหัสสินค้า: </strong></span>", "");
    		realProductId = FilterUtil.getStringBetween(realProductId, "<strong>", "</strong>");
    	}
		return new String[] {realProductId};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();	
		String[] UPC = mpdBean.getProductUpc();
		String[] productNameDescription = mpdBean.getProductNameDescription();
		String[] realproductID = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
				|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		rtn[0].setName(pdName);
		rtn[0].setUpc(UPC[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setRealProductId(realproductID[0]);
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		String author = "";
		String translate = "";
		if (productNameDescription != null && productNameDescription.length != 0) {
			author = productNameDescription[0];
			translate  = productNameDescription[1];
			if(!author.isEmpty()){
				rtn[0].setKeyword(author);
				author = "ผู้แต่ง/ผู้แปล: " + author+"/"+ translate;
			}
		}
		
		if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
			String publisher = productDescription[0];
			String desc = null;
			if(!publisher.isEmpty()){
				desc = "สำนักพิมพ์: " + publisher;
			}
			if(!author.isEmpty()){
				if(desc != null){
					desc = author + ", " + desc;
				}else{
					desc = author;
				}
			}
			rtn[0].setDescription(desc);
		}

		return rtn;
	}
	    
}