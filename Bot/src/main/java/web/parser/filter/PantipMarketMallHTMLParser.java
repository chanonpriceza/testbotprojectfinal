package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PantipMarketMallHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<div id=\"catepath\">"});
		map.put("price", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{"<div class=\"overview\">"});
		map.put("pictureUrl", new String[]{"<div id=\"showOnePic\">"});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{"<div class=\"price\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   		
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "</font></a> >", "</h2>");
			if(productName.isEmpty()){
				productName = FilterUtil.getStringBetween(evaluateResult[0], "</font></a> /", "</h2>");
			}
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {   		
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.getStringAfter(productPrice, "ราคา&nbsp;&nbsp;", "");
    		productPrice = productPrice.replace("บาท", "").replace(",", "").trim();
    		if(!StringUtils.isNumeric(productPrice) || productPrice.length() == 0 || productPrice.startsWith("0")){;
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}else{
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}

        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
    	if (evaluateResult.length == 1) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"goodsID\" value=\"", "\"");
        }
    	return new String[] {realProductId};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "false";
    	if (evaluateResult.length == 1) {
    		if (evaluateResult[0].contains("Sold")) {
    			pdExpire = "true";
    		}
        }
    	return new String[] {pdExpire};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		String realPId = realProductId[0];
		String pdName = productName[0];
		if (StringUtils.isNotBlank(realPId) && StringUtils.isNotBlank(pdName)){
			pdName += " ("+ realPId +")";
		}
		rtn[0].setName(pdName);
				
		return rtn;
	}
}