package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TaladroddHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		 new String[]{"<h3 id=\"notice_title\">"});
		map.put("price", 		 new String[]{"<h3 class=\"text-colored m-b-10\">"});
		map.put("description", 	 new String[]{""});
		map.put("pictureUrl", 	 new String[]{"<div class=\"car-pic\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  

		if(evaluateResult.length > 0) {
			if (StringUtils.isNotBlank(evaluateResult[0]) && evaluateResult[0].indexOf("null") < 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0])+" รถมือสอง";
			} else {
				productName = "";
			}
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";

		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			if (productPrice.equals("-")) {
//				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "ข้อมูลรถยนต์", "<div class=\"row\">");
			productDesc = productDesc.replace("<dd>", " ").replace("</dd>", ",");
			productDesc = productDesc.replace("<strong>", " ").replace("</p>", ",");
			productDesc = FilterUtil.toPlainTextString(productDesc);
			productDesc = FilterUtil.getStringBeforeLastIndex(productDesc, ",", "");
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if(evaluateResult.length > 0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
}
