package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PowerBuyHotDealHTMLParser extends DefaultHTMLParser {	
	
	private static final Logger logger = LogManager.getRootLogger();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<div class=\"page-title-wrapper product\">"});
		map.put("price", new String[]{"<div class=\"block_move\">"});
		map.put("basePrice", new String[]{"<div class=\"block_move\">"});
		map.put("description", new String[]{"<div class=\"product attribute description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<div class=\"value\" itemprop=\"sku\">"});
		map.put("expire", new String[]{""});

		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBefore(evaluateResult[0], "</h1>", evaluateResult[0]);
			productName = FilterUtil.toPlainTextString(productName);
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price has-special-price\">", "</p>");
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price \">", "</p>");
    		}
 
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length == 1) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"first-price\">", "</p>");
    		if(!bPrice.isEmpty()) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);	    			
    		}
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "[data-gallery-role=gallery-placeholder]", "</script>");
    		if(StringUtils.isNoneBlank(productPictureUrl)){
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "img\":\"", "\"");
    			productPictureUrl = productPictureUrl.replace("\\", "");
    		}
        } 
     	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if (evaluateResult.length == 1) {
			pdId = FilterUtil.toPlainTextString(evaluateResult[0]);	
        }
		return new String[] {pdId};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String strExpire = null;
		if(evaluateResult[0].contains("<button class=\"soldout_btn\">") || evaluateResult[0].contains("ขออภัย ไม่พบหน้าที่ท่านค้นหา")){
			return new String[]{ "true"};
		}
		if(evaluateResult[0].contains("<div class=\"offer-valid\">")){
			strExpire = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"offer-valid\">", "</div>");
			strExpire = FilterUtil.getStringAfter(strExpire, "ถึง", "");
			if(StringUtils.isNoneBlank(strExpire)){
				try {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					Date dateCurrent = new Date();
					Date dateProduct = dateFormat.parse(strExpire);
					if(dateCurrent.before(dateProduct)){
		                return new String[]{ "true"};
		            }
				} catch (ParseException e) {
					logger.error(e);
				}
				
			}
			
		}
		return null;
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		String[] realProductId = mpdBean.getRealProductId();
		String[] bPrice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0 || realProductId == null || realProductId.length == 0 || !StringUtils.isNumeric(realProductId[0])) {
			return null;
		}
		
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0] + " ("+ realProductId[0] +") (Hot deal)";
		rtn[0].setName(StringEscapeUtils.unescapeHtml4(pdName));
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		currentUrl = currentUrl.replace("?isProduct=true", "");
		if(currentUrl.indexOf("?utm_source=PriceZa&utm_medium=comparison&utm_campaign=deal-of-the-day") == -1) {
			currentUrl += "?utm_source=PriceZa&utm_medium=comparison&utm_campaign=deal-of-the-day";
		}
		rtn[0].setUrl(currentUrl);
		rtn[0].setUrlForUpdate(currentUrl);

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(StringEscapeUtils.unescapeHtml4(productDescription[0]));
		}
		
		if(bPrice != null && bPrice.length == 1){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}

		if (realProductId != null && realProductId.length != 0){
			rtn[0].setRealProductId(realProductId[0]);
		}
		rtn[0].setCategoryId(500405);
		
		return rtn;
	}
	    
}