package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class RoyalEnfieldHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("title");
			productName = FilterUtil.toPlainTextString(e.html());	
			productName = productName.replace(" | Royalenfield","");
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("span.color_11");
			productPrice = FilterUtil.getStringBetween(e.html(),"ราคา","บาท");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);			
		}	
		return new String[] {productPrice};
	}
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("img[alt=legendary-dependability.jpg]");
			productPrice = e.attr("src");
			productPrice = FilterUtil.getStringBefore(productPrice,"/v1/",productPrice);
		}	
		return new String[] {productPrice};
	}
	
}
