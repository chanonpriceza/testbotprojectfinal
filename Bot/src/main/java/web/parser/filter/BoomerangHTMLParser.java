package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BoomerangHTMLParser extends DefaultHTMLParser{
			
	
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<div class=\"nine mobile-six columns pull-lr-5 product-info\">"});
		map.put("price", 		new String[]{"<div class=\"six columns\">"});
		map.put("basePrice", 	new String[]{"<div class=\"six columns\">"});
		map.put("description", 	new String[]{"<div id=\"product-credit\" >"});
		map.put("pictureUrl", 	new String[]{""});
		map.put("upc", 			new String[]{"<div class=\"nine mobile-six columns pull-lr-5 product-info\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<strong>","</strong>");
			productName = FilterUtil.toPlainTextString(productName);
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"Our Price (Baht) :","</li>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	}		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"SRP (Baht) :","</li>");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductUpc(String[] evaluateResult) {
		String productPictureUpc = null;
    	if (evaluateResult.length == 1) {
    		productPictureUpc = FilterUtil.getStringBetween(evaluateResult[0],"UPC:","</div>");
    		productPictureUpc = FilterUtil.toPlainTextString(productPictureUpc);
        }
    	return new String[] {productPictureUpc};
	}
	
	
}