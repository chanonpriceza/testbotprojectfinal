package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class GrandSportHTMLParser extends DefaultHTMLParser{
	@Override
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name",				new String[]{"<h1>"});
		map.put("price",			new String[]{"<span class=\"price\" id=\"price\" >"});
		map.put("description",		new String[]{""});
		map.put("pictureUrl",		new String[]{"<ul class=\"product-bxslider\">"});
		map.put("expire",			new String[]{"<ul aria-labelledby=\"qualities\" role=\"menu\" class=\"\" id=\"qualities\">"});
		return map;
	}
	@Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length == 1){
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDescription = ""; 
		if(evaluateResult.length == 1){
			productDescription = FilterUtil.getStringBetween(evaluateResult[0], "<dd><p>", "</p></dd>");
			productDescription = FilterUtil.toPlainTextString(productDescription);
		}
		return new String[] {productDescription};
	}
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productUrl = "";
		if(evaluateResult.length == 1){
			productUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
		}
		return new String[] {productUrl};
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "";
		if(evaluateResult.length > 0 && evaluateResult[0].contains("ไม่มีสินค้าใน Stock")){
			expire = "true";
		}
		return new String[] {expire};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		
		if(name == null || name.length == 0 || price == null || price.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		rtn[0].setName("Grandsport " + name[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(price[0]));
		if(desc != null && desc.length > 0) {
			rtn[0].setDescription(desc[0]);
		}
		if(pictureUrl != null && pictureUrl.length > 0) {
			if (pictureUrl[0] != null && pictureUrl[0].trim().length() != 0) {
				if (pictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl[0]);
						
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) { }
				}
			}
		}
		
		return rtn;
	}
}
