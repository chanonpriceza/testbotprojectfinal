package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class WemallHotDealHTMLParser extends DefaultHTMLParser{	
	
	private static final String[][] CAT_ID_KEYWORD_MAPPING = {
		{"20305", "อุปกรณ์ถนอมสายชาร์จ", ""},
		{"20101", "Samsung Galaxy", ""},
		{"80307", "อุปกรณ์ปอกเปลือกผลไม้", ""},
		{"50120", "ไม้ช็อตยุง", ""},
		{"50605", "ปลั๊กไฟ", ""},
		{"30103", "Triple3shop Action Camera", "กล้องถ่ายใต้น้ำ"},
		{"110305", "รองเท้าปุ่มนวดเพื่อสุขภาพ", ""},
		{"130807", "มาส์ก", "มาส์คหน้า Mask มาร์คหน้า Mask Sheet"},
		{"20210", "สายชาร์จ", ""},
		{"80715", "ชั้นวางของ", ""},
		{"50516", "เตาย่าง", ""},
		{"210206", "ถุงซักผ้า", ""},
		{"130112", "TEAR DROP LINER", "อายไลน์เนอร์ eyeliner ดินสอเขียนขอบตา"},
		{"130404", "แชมพู", ""},
		{"130201", "สบู่", ""},
		{"51007", "Speaker Bluetooth", "ลำโพง"},
		{"50801", "DVD PLAYER", "เครื่องเล่น DVD"},
		{"51314", "แบตเตอรี่สำรอง", ""},
		{"10303", "แฟลชไดรฟ์", "แฟลชเมมโมรี่ flashdrive thumbdrive memorydrive"},
		{"30213", "ฟิล์มโพลารอยด์", ""},
		{"30102", "Fujifilm Instax", "กล้องใช้ฟิลม์ กล้องอินสแตนท์ กล้องโพลารอยด์"},
		{"20101", "Asus Zenfone", ""},
		{"50514", "หม้อตุ๋น", ""},
		{"130110", "โลชั่น", ""},
		{"80307", "เครื่องสับหอม", ""},
		{"20202", "เมมโมรี่การ์ด", "หน่วยความจำ เมมโมรี่ memory card"},
		{"20210", "แท่นวางโทรศัพท์", ""},
		{"80103", "แปรงฟองน้ำ", ""},
		{"50601", "โคมไฟ", ""},
		{"110613", "Xiaomi Mi Band", "นาฬิกา SmartWatch สมาร์ทวอช"},
		{"170904", "เครื่องชั่งน้ำหนัก", ""},
		{"20101", "Huawei Alex", ""},
		{"30101", "Xiaomi Yi Action Camera", ""},
		{"20203", "หูฟังบลูทูธ", ""},
		{"80316", "ผ้าเช็ดมือ", ""},
		{"80566", "ม่านในห้องน้ำ", ""},
		{"30404", "Lens Cap", "ฝาปิดเลนส์"},
		{"50601", "ไฟ 4LED", ""},
		{"130904", "แปรงสีฟัน", ""},
		{"130116", "สำลี", ""},
		{"140401", "EXERCISE WHEEL MB-47001", "เครื่องบริหารหน้าท้อง"},
		{"50603", "หลอดตะเกียบ", ""},
		{"50115", "ไฟฉาย", ""},
		{"130206", "okamoto Dot De Cool", "ถุงยางอนามัย"},
		{"130206", "Durex Feather Lite", "ถุงยางอนามัย"},
		{"80518", "ไม้แขวนเสื้อ", ""},
		{"70109", "ที่บังแดด", ""},
		{"80317", "กรวยกรอกน้ำ", ""},
		{"10317", "ซิลิโคนคีย์บอร์ด", ""},
		{"20101", "ZTE Blade", ""},
		{"140401", "เครื่องซิทอัพ", "เครื่องบริหารหน้าท้อง"},
		{"80103", "อุปกรณ์จัดระเบียบ", ""},
		{"20101", "i-mobile i-style", ""},
		{"80103", "กล่องใส่เสื้อผ้า", ""},
		{"130206", "Durex Love", "ถุงยางอนามัย"},
		{"20210", "ตัวยึดโทรศัพท์", ""},
		{"10805", "USB แบบพกพา", ""},
		{"110216", "รองเท้ากันฝน", ""},
		{"130206", "okamoto Strawberry", "ถุงยางอนามัย"},
		{"10318", "เม้าส์", ""},
		{"30406", "Album for Instax", ""},
		{"10611", "ฮาร์ดดิส", ""},
		{"50801", "เครื่องเล่น Blu-ray", ""},
		{"80103", "ที่จัดระเบียบไม้แขวนเสื้อ", ""},
		{"51303", "ถ่าน", ""},
		{"20210", "สายรัดแขนโทรศัพท์", ""},
		{"130206", "Durex Kingtex", "ถุงยางอนามัย"},
		{"130206", "okamoto XL", "ถุงยางอนามัย"},
		{"130215", "เกลือสปาขัดผิว", ""},
		{"130809", "TONER", ""},
		{"20101", "Cherry Mobile", ""},
		{"80307", "เครื่องมือชุดครัว", ""},
		{"130403", "เครื่องหนีบและม้วนผม", ""},
		{"80519", "จักรเย็บผ้า", ""},
		{"20210", "ขาตั้งSmartphone", ""},
		{"50514", "หม้อสุกี้ไฟฟ้า", ""},
		{"170716", "เครื่องทำไอศกรีม", ""},
		{"20208", "USB Power Adapter", ""},
		{"130119", "Eyebrow", "ดินสอเขียนคิ้ว"},
		{"130111", "Lipstick", "ลิปสติก"},
        {"50501", "เตาอบ", "เตาอบไฟฟ้า"}
	};
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<span class=\"special_price\">"});
		map.put("basePrice", new String[]{"<span class=\"normal_price\">"});
		map.put("description", new String[]{"<div class=\"product__description_bottom\">"});
		map.put("pictureUrl", new String[]{"<div class=\"prd_media\">"});
		map.put("expire", new String[]{ ""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		if(bPrice.contains("-")){
    			bPrice = FilterUtil.getStringBefore(bPrice, "-", "");
    		}
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}

	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		if(productPrice.contains("-")){
    			productPrice = FilterUtil.getStringBefore(productPrice, "-", "");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "true";
		String endTime = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"countdown\" data-countdown=\"", "\"").trim();
		if(!endTime.isEmpty() && endTime.length() == 19){
			DateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			try{
				long endSec = formatDate.parse(endTime).getTime();
				long now = new Date().getTime();
				if(now < endSec){
					exp = "false";
				}
			}catch(Exception e){}
		}
		return new String[] { exp};
	}

	  public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productBPrice = mpdBean.getProductBasePrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();

			if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			String pdName = productName[0];
			if(pdName != null && pdName.trim().length() != 0) {
				pdName = pdName.trim() + " (Hot Deal)";
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(pdName);
			double price = FilterUtil.convertPriceStr(productPrice[0]);
			rtn[0].setPrice(price);
			
			if (productBPrice != null && productBPrice.length != 0 && !productBPrice[0].isEmpty()) {
				double bPrice = FilterUtil.convertPriceStr(productBPrice[0]);
				if(bPrice > price){
					rtn[0].setBasePrice(bPrice);
				}
			}

			if (productDescription != null && productDescription.length != 0 && !productDescription[0].isEmpty()) {
				rtn[0].setDescription(productDescription[0]);
			}

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl),
									productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}

			String[] matchCatId = mapCategory(pdName, CAT_ID_KEYWORD_MAPPING);				
			if(matchCatId != null) {
				rtn[0].setKeyword(matchCatId[2]);
				rtn[0].setCategoryId(BotUtil.stringToInt(matchCatId[0], 160255));										
			}else{
				rtn[0].setCategoryId(160255);
			}

			return rtn;
		}
	    
}
