package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SmartSolutionComputerHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<div class=\"block-header\">"});
			map.put("price", new String[]{"<div class=\"product-price\">"});
			map.put("basePrice", new String[]{"<div class=\"product-price\">"});
			map.put("description", new String[]{"<div class=\"detail\">"});
			map.put("pictureUrl", new String[]{"<div class=\"productDetailImg\">"});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length > 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		if (evaluateResult[0].contains("tempprice")) {
	    			productPrice = FilterUtil.getStringAfter(evaluateResult[0], "</s>", "");
	    		} else {
	    			productPrice = evaluateResult[0];
	    		}
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    		if(productPrice.equals("0.00")){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}
	        }	    	
			return new String[] {productPrice};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String basePrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<s ref=\"tempprice\">", "</s>");
	    		basePrice = FilterUtil.toPlainTextString(basePrice);
	    		basePrice = FilterUtil.removeCharNotPrice(basePrice);
	        }	    	
			return new String[] {basePrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length > 0) {
	    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
	    		productDesc = FilterUtil.toPlainTextString(productDesc);
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length > 0) {	
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
}
