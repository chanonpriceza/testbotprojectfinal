package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SiamNaligaHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<span  itemprop=\"price\"  >"});
		map.put("basePrice", new String[]{"<div class=\"old-product-price\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"picture\">"});
		map.put("expire", new String[]{"<div class=\"stock\">"});	
		
		return map;
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("หมด") != -1) {
        		return new String[]{"true"};
        	}     	
    	}    	
        return new String[]{""};
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[]{productName};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		return new String[]{productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		
		if(evaluateResult.length == 1) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		
		return new String[]{productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[]{productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		
		if(evaluateResult.length >0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		
		return new String[]{productPictureUrl};
	}
}
