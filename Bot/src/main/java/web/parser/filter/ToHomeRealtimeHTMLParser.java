package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ToHomeRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<span id=\"lblProductName\">"});
		map.put("price", 		 new String[]{"<span id=\"lblOurPrice\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] { FilterUtil.removeCharNotPrice((FilterUtil.toPlainTextString(evaluateResult[0]))) };
		}
		return null;
	}
	
}
