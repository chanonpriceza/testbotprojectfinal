package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class IStudioHTMLParser extends BasicHTMLParser {
	
	private static JSONParser parser = new JSONParser();
	private  JSONObject global_obj = new JSONObject();
	private  List<DetailObj> objList = new ArrayList<DetailObj>();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String[] result = null;
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements brand = doc.select("div[itemprop=brand]");
			Elements name = doc.select("span[itemprop=name]");
			String brandString = brand.html();
			String nameString= name.html();
			Elements attrColorList = doc.select("script[type=text/x-magento-init]");
			Elements idx = doc.select("input[name=product]");
			String idxString = "";
			if(StringUtils.isBlank(idx.html())) {
				idx = doc.select("div[data-role=priceBox]");
				if(idx.attr("data-product-id").length()>0)
					idxString = " ("+idx.attr("data-product-id")+")";
			}
			
			Element attr =   null;
			for(Element e:attrColorList) {
				if(e.html().contains("data-role=swatch-options"))
					attr = e;
			}
			if(attr!=null) {
				try {
					JSONObject obj = (JSONObject) parser.parse(attr.html());
					global_obj = obj;
					global_obj = (JSONObject)global_obj.get("[data-role=swatch-options]");
					global_obj = (JSONObject)global_obj.get("Magento_Swatches/js/swatch-renderer");
					global_obj = (JSONObject)global_obj.get("jsonConfig");
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				
				//404 color
				//251 capacity
				JSONObject	data = (JSONObject)global_obj.get("index");
				for(Object key:data.keySet()) {
					String id = String.valueOf(key);
					JSONObject k = (JSONObject)data.get(id);
					DetailObj obj = new DetailObj();
					@SuppressWarnings("unchecked")
					Set<String> attrList= k.keySet();
					for(String ka:attrList) {
						JSONObject objK = (JSONObject)global_obj.get("attributes");
						JSONObject attrGet = (JSONObject) objK.get(ka);
						String banner = String.valueOf(attrGet.get("label"));
						if(banner.toLowerCase().contains("color")) 
							obj.colorId = BotUtil.stringToInt(ka,0);
						if(banner.toLowerCase().contains("capacity")) 
							obj.capaId = BotUtil.stringToInt(ka,0);
					}
					obj.brand = brandString;
					obj.id = BotUtil.stringToInt(key.toString(),0);
					obj.name = nameString;
					obj.jsonData = global_obj;
					obj.generateData();
					objList.add(obj);
					
				}
				result = objList.stream().map(x -> x.displayName).toArray(String[]::new);
			}else {
				String productName = brandString+" "+nameString+""+idxString;
				return new String[] {productName};
			}
		}

		return result;
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (objList.size()>0) {
    		return objList.stream().map(x -> x.imagePicture).toArray(String[]::new);
        }else {
        	Document doc  = Jsoup.parse(evaluateResult[0]);
			Elements e  = doc.select("meta[property=og:image]");
			productPictureUrl = e.attr("content");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	
	public String[] getProductPrice(String[] evaluateResult) {
		String[] d = null;
		if(objList.size()>0) {
			d = objList.stream().map(x -> String.valueOf(x.price)).toArray(String[]::new);
		}else {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements price  = doc.select("div.product-info-main");
			price = price.select("span[itemprop=price]");
			return new String[] {price.attr("data-price-amount")};
		}
		return d;
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String[] d = null;
		if(objList.size()>0) {
			d = objList.stream().map(x -> String.valueOf(x.basePrice)).toArray(String[]::new);
		}else {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements price  = doc.select("div.product-info-main");
			price = price.select("span[id^=old-price]");
			return new String[] {price.attr("data-price-amount")};
		}
		return d ;
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String[] d = null;
		if(objList.size()>0) {
			d = objList.stream().map(x -> String.valueOf(x.id)).toArray(String[]::new);
		}else {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements id = doc.select("input[name=product]");
			
			if(StringUtils.isBlank(id.html())) {
				id = doc.select("div[data-role=priceBox]");
				return new String[] {id.attr("data-product-id")};
			}
			
			return  new String[] {id.html()};
		}
		return d ;
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		boolean result;
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements id = doc.select("span.text-stock");
		result  = id.html().contains("สินค้าหมด")||id.html().contains("Out of Stock");
		
		return  new String[] {String.valueOf(result)};
	}

	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
	
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[productName.length];
		for (int i = 0; i < productName.length; i++) {
			rtn[i] = new ProductDataBean();
			rtn[i].setName(productName[i]);
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));
			if (productDescription != null && productDescription.length != 0) {
				rtn[i].setDescription(productDescription[0]);
			}
			rtn[i].setUrl(mpdBean.getCurrentUrl());
			rtn[i].setRealProductId(mpdBean.getRealProductId()[i]);
			rtn[i].setBasePrice(FilterUtil.convertPriceStr(mpdBean.getProductBasePrice()[i]));
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[i] != null && productPictureUrl[i].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[i].setPictureUrl(productPictureUrl[i]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[i]);
							rtn[i].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {
	
						}
					}
				}
			}
	
		}
		objList.clear();
		return rtn;
	}
	
	
	
	class DetailObj{
		
		JSONObject jsonData;
		
		int id;
		int colorId;
		int capaId;
		
		double price;
		double basePrice;
		
		
		String imagePicture = "";
		String colorString = "";
		String capaString = "";
		String name = "";
		String brand = "";
		String displayName = "" ;
		
		boolean isExpire;
		boolean isModifyName = false;
		
		private void generateData() {
			this.checkExpire();
			if(!this.isExpire) {
				this.setColorName();
				this.setCapaName();
				this.setPrice();
				this.setImagePicture();
				this.showDisplayName();
			}
		}
		
		private void checkExpire() {
			JSONObject attributes = (JSONObject)this.jsonData.get("stockItem");
			String expire = String.valueOf(attributes.get(String.valueOf(this.id)));
			if("0".equals(expire)) {
				this.isExpire = true;
			}
			
		}
		private void setColorName() {
			
			JSONObject attributes = (JSONObject)this.jsonData.get("attributes");
			if(attributes==null||this.colorId==0)
				return;
			
			attributes = (JSONObject)attributes.get(String.valueOf(this.colorId));
			if(attributes==null)
				return;
			JSONArray colorArray = (JSONArray)attributes.get("options");
			for(Object color:colorArray) {
				if(this.colorId!=302) {
					JSONObject c = (JSONObject)color;
					String id  = String.valueOf(c.get("products"));
					String label = String.valueOf(c.get("label"));
					if(id.contains(String.valueOf(this.id)))
						this.colorString = label;	
				}else {
					JSONObject c = (JSONObject)color;
					String id  = String.valueOf(c.get("products"));
					String label = String.valueOf(c.get("label"));
					if(id.contains(String.valueOf(this.id)))
						this.colorString = label;	
				}
			}
		}
		
		private void setCapaName() {
			JSONObject attributes = (JSONObject)this.jsonData.get("attributes");
			if(attributes==null||this.capaId==0)
				return;
			attributes = (JSONObject)attributes.get(String.valueOf(this.capaId));
				
				
			JSONArray colorArray = (JSONArray)attributes.get("options");
			for(Object color:colorArray) {
				JSONObject c = (JSONObject)color;
				String id  = String.valueOf(c.get("products"));
				String label = String.valueOf(c.get("label"));
				if(id.contains(String.valueOf(this.id)))
					this.capaString = label;		
			}
		}
		
		private void setPrice() {
			//optionPrices
			JSONObject attributes = (JSONObject)this.jsonData.get("optionPrices");
			if(attributes==null)
				return;
			attributes = (JSONObject)attributes.get(String.valueOf(this.id));
			JSONObject price = (JSONObject)attributes.get("finalPrice");
			JSONObject basePrice = (JSONObject)attributes.get("basePrice");;
			this.price =  FilterUtil.convertPriceStr(String.valueOf(price.get("amount")));
			this.basePrice =  FilterUtil.convertPriceStr(String.valueOf(basePrice.get("amount")));
			
		}
		
		private void setImagePicture() {
			JSONObject attributes = (JSONObject)this.jsonData.get("images");
			if(attributes==null)
				return;
			JSONArray array = (JSONArray)attributes.get(String.valueOf(this.id));
			String image = "";
			for(Object a:array) {
				JSONObject i = (JSONObject)a;
				image = String.valueOf(i.get("img")) ;
				this.imagePicture = image;
				if(StringUtils.isNotBlank(image)) {
					break;
				}
			}
		}
		
		private void showDisplayName() {
			 this.isModifyName = true;
			 this.displayName = brand+" "+name+" "+capaString+" "+colorString+" ("+id+")";
		}
		
	}
 
}
