package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class Attorney285HTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<h1>"});
		map.put("price", 		new String[]{"<div style='padding-bottom: 5px;' class='NewProductDetail'>"});
		map.put("basePrice", 	new String[]{"<font class='ProductPrice'>"});
		map.put("description", 	new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});
		map.put("pictureUrl", 	new String[]{"<table align='center' border='0' cellspacing='0' cellpadding='0' class='ProductPictureBorder' width='150' height='150'>"});
		map.put("expire", 		new String[]{"<div style='padding-bottom: 5px;' class='NewProductDetail'>"});
		map.put("upc", 			new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});		
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("หมดแล้ว") != -1) {
        		return new String[]{"true"};
        	}	
    	}    	
        return new String[]{""};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคาพิเศษ", "บาท");
    		if(productPrice.length() == 0){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", "บาท");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";    	
    	if (evaluateResult.length == 1) {
    		productBasePrice = FilterUtil.getStringBefore(evaluateResult[0],"</strike>","");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "ผู้แต่ง", "");
    		if(productDesc.length() <= 0){
    			productDesc = FilterUtil.getStringAfter(productDesc, "ผู้แปล", "");
    		}
    		productDesc = FilterUtil.getStringAfter(productDesc, ":", productDesc);
    		productDesc = FilterUtil.getStringBefore(productDesc, "</", productDesc);
    		productDesc = FilterUtil.getStringBefore(productDesc, "<br", productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductUpc(String[] evaluateResult) {
		String productUPC = "";
		if(evaluateResult.length > 0){
			productUPC = FilterUtil.getStringAfter(evaluateResult[0], "ISBN", "");
			productUPC = FilterUtil.getStringAfter(productUPC, ":", productUPC);
			productUPC = FilterUtil.getStringBefore(productUPC, "</", productUPC);
			productUPC = FilterUtil.getStringBefore(productUPC, "<br", productUPC);
			productUPC = FilterUtil.toPlainTextString(productUPC);	
			productUPC = FilterUtil.removeCharNotPrice(productUPC);
			productUPC = productUPC.replace(".", "");
			
		}
		return new String[] {productUPC};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productUpc = mpdBean.getProductUpc();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productDescription != null && productDescription.length != 0) {
			if(productDescription != null && productDescription[0].trim().length() > 0){
				rtn[0].setDescription("ผู้แต่ง/ผู้แปล : " + productDescription[0]);
				rtn[0].setKeyword(productDescription[0]);
			}
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}

		// can remove if realProductId, basePrice and upc are not required
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
		}

		return rtn;
	}
	    
}