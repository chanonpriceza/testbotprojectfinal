package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PhanphaBookHTMLParser extends DefaultHTMLParser {
	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1 class=\"title\">"});
			map.put("price", new String[]{"<div class=\"product-info product sell\">"});
			map.put("basePrice", new String[]{"<div class=\"product-info product list\">"});
			map.put("description", new String[]{"<div class=\"content\">"});
			map.put("nameDescription", new String[]{"<div id=\"content-body\">"});
			map.put("pictureUrl", new String[]{"<div class=\"product-image\">"});
			map.put("realProductId", new String[]{"<div class=\"product-info model\">"});
			map.put("expire", new String[]{"<div id=\"price-group\">"});
			
			return map;
		}
		boolean isBook = true;
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length == 1) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"price-prefixes\">", evaluateResult[0]);
	    		
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }
			return new String[] {productPrice};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String bPrice = "";    
	    	if (evaluateResult.length == 1) {
	    		bPrice = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"price-prefixes\">", evaluateResult[0]);
	    		
	    		bPrice = FilterUtil.toPlainTextString(bPrice);
	    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
	        }
			return new String[] {bPrice};
		}

		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
			if(evaluateResult.length == 1) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<strong>สำนักพิมพ์ :", "<br />");
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<strong>สำนักพิมพ์", "<br />");
				}
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td>สำนักพิมพ์", "</tr>");
					productDesc = productDesc.replace("&nbsp;", " ");
				}
				
				productDesc = FilterUtil.getStringBefore(productDesc, "</p>", productDesc);
				productDesc = FilterUtil.toPlainTextString(productDesc);
				if(productDesc.equals("-")){
					productDesc="";
				}
	    	}
			return new String[] {productDesc};
		}
		
		@Override
		public String[] getProductNameDescription(String[] evaluateResult) {
			
			isBook = true;
			String pdNameDescription = "";
			if(evaluateResult.length > 0) {
				
				pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>โดย", "<br />");
				pdNameDescription = FilterUtil.getStringBefore(pdNameDescription, "</p>", pdNameDescription);
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>โดย", "</p>");
					
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>Story", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>STORY", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>Writer", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรื่องและภาพ", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "ผู้เขียน", "</p>");
					pdNameDescription = FilterUtil.getStringBefore(pdNameDescription, "<br />", pdNameDescription);
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ผู้เขียน", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ผลงานของ", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรียบเรียงโดย", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรียบเรียง", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรื่องโดย", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ผู้แต่ง", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรื่องและภาพ", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เรื่อง", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>Original", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>เขียน", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ประพันธ์", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>สร้างสรรค์โดย", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>Illust &amp; Story", "<br />");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>Illust &amp; Story", "</p>");
				}
				if(pdNameDescription.isEmpty()){
					pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ภาพ", "<br />");
				}
				pdNameDescription = FilterUtil.getStringAfterLastIndex(pdNameDescription, "</strong>", pdNameDescription);
				if(pdNameDescription.isEmpty()){
					if(evaluateResult[0].contains("ISBN")||evaluateResult[0].contains("Barcode")){
						pdNameDescription = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "</p>", "");
					}else{
						pdNameDescription = evaluateResult[0];
						isBook = false;
					}
				}
				pdNameDescription = FilterUtil.toPlainTextString(pdNameDescription);
				pdNameDescription = pdNameDescription.replace(":", "");
				pdNameDescription = pdNameDescription.replaceAll("&nbsp;", "");
				pdNameDescription = pdNameDescription.replaceAll("&amp;", "&");
				
				if(pdNameDescription.equals("-")){
					pdNameDescription="";
				}
	    	}
			return new String[] {pdNameDescription};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
//	    		if(productPictureUrl.indexOf("/noimage") > -1){
//	    			productPictureUrl = null;
//	    		}
	        }
	    	return new String[] {productPictureUrl};
		}
		

		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pdId = null;
			if (evaluateResult.length == 1) {
	    		pdId = FilterUtil.getStringAfter(evaluateResult[0], "รหัสสินค้า / Barcode:", evaluateResult[0]);
	    		pdId = FilterUtil.toPlainTextString(pdId);
	        }
			return new String[] { pdId};
		}
		
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			String exp = "true";
			if(evaluateResult.length == 1){
				if(evaluateResult[0].contains("หยิบใส่ตะกร้า")){
					exp = "false";
				}
			}
			return new String[] {exp};
		}
		
		   @Override
		    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

				String[] productName = mpdBean.getProductName();
				String[] productPrice = mpdBean.getProductPrice();
				String[] productBasePrice = mpdBean.getProductBasePrice();
				String[] productDescription = mpdBean.getProductDescription();
				String[] productPictureUrl = mpdBean.getProductPictureUrl();
				String currentUrl = mpdBean.getCurrentUrl();
				String[] realProductId = mpdBean.getRealProductId();		
				String[] productNameDescription = mpdBean.getProductNameDescription();
				
				if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
						|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])
						|| realProductId == null || realProductId.length == 0 || StringUtils.isBlank(realProductId[0])) {
					return null;
				}
				ProductDataBean[] rtn = new ProductDataBean[1];
				rtn[0] = new ProductDataBean();
				String pdName = productName[0];
				String ISBN = realProductId[0];
				
				if(isBook){
					pdName += " (ISBN:"+ ISBN +")";
				}
				
				rtn[0].setName(pdName);
				rtn[0].setRealProductId(ISBN);		
				rtn[0].setUpc(ISBN);
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if (StringUtils.isNotBlank(productPictureUrl[0])) {
						if (productPictureUrl[0].startsWith("http")) {
							rtn[0].setPictureUrl(productPictureUrl[0]);
						} else {
							try {
								URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
								rtn[0].setPictureUrl(url.toString());
							} catch (MalformedURLException e) {}
						}
					}
				}
				String desc = null;
				if(isBook){
					
					String author = "";
					if (productNameDescription != null && productNameDescription.length != 0) {
						author = productNameDescription[0];
						if(!author.isEmpty()){
							rtn[0].setKeyword(author);
							author = "ผู้แต่ง/ผู้แปล: " + author;
						}
					}
					
					if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
						String publisher = productDescription[0];
						if(!publisher.isEmpty()){
							desc = "สำนักพิมพ์: " + publisher;
						}
						if(!author.isEmpty()){
							if(desc != null){
								desc = author + ", " + desc;

							}else{
								desc = author;
							}
						}
					}
				}
				if(desc != null){
					rtn[0].setDescription(desc);
				}else{
					rtn[0].setDescription(productNameDescription[0]);
				}

				return rtn;
			}
		
}
