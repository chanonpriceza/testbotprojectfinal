package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class MORINGA365HTMLParser extends DefaultHTMLParser{

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<div class=\"detail-container column-left col-xs-12 col-sm-12 col-md-12 col-lg-12\">"});
		map.put("price", 		new String[]{"<div class=\"price disable-text col-xs-12 col-sm-12 col-md-12 col-lg-12\">"});
		map.put("description", 	new String[]{"<div class=\"text-detail-container\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"body-main-image\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}