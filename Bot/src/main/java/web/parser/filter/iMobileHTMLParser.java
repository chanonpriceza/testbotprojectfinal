package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class iMobileHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>","</h1>"});
		map.put("price", new String[]{"<div class=\"price\">","</div>"});
		map.put("pictureUrl", new String[]{"<div class=\"image clearafter\">","</div>"});
		map.put("description", new String[]{"<tbody>","</tbody>"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "product_id=" , "\"");
	   		realProductId = FilterUtil.getStringBefore(realProductId, "'" , realProductId);
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	   	
	    return new String[]{realProductId}; 
    }
	
	
	
}