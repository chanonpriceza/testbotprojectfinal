package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AsiaBooksHTMLParser extends DefaultHTMLParser{
	private static final Logger logger = LogManager.getRootLogger();
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span class=\"h1\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<table class=\"data-table\" id=\"product-attribute-specs-table\">"});
		map.put("nameDescription", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("upc", new String[]{""});
		map.put("expire", new String[]{""});
		
		
		return map;
	}
    
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	String expire = "";
    	Document doc = Jsoup.parse(evaluateResult[0]);
		if(evaluateResult.length > 0) {
			Elements e = doc.select("div.box-left");
			expire = FilterUtil.toPlainTextString(e.select("p.out-of-stock").text());
		}
    	return new String[] {expire.contains("Out of stock")+""};
    }
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("div.box-left");
		String productPrice = e.select("p.special-price").text();    	
		String result = "";
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		result = productPrice;
        }
    	if(StringUtils.isBlank(result)) {
    		productPrice = e.select("p.price").text();    	
    		productPrice = FilterUtil.toPlainTextString(productPrice);
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);	
        	result = productPrice;
    	}
    	
    	if(StringUtils.isBlank(result)) {
    		productPrice = e.select("span.price").text();    	

    			productPrice = FilterUtil.toPlainTextString(productPrice);
        		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        		result = productPrice;
    	}
		return new String[] {result};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("div.box-left");
		String basePrice = e.select("p.old-price").text();    	
    	if (evaluateResult.length > 0) {
    		basePrice = FilterUtil.toPlainTextString(basePrice);
    		basePrice = FilterUtil.removeCharNotPrice(basePrice);
        }
    	return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "Publisher</th>", "</td>");
			productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
		return new String[] {productDesc};
	}
	
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String pdNameDescription = "";
		if(evaluateResult.length == 1) {
			pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "By</label>", "</div>");
			pdNameDescription = FilterUtil.toPlainTextString(pdNameDescription);
    	}
		return new String[] {pdNameDescription};
	}
	
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String pdNameDescription = "";
		if(evaluateResult.length == 1) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			pdNameDescription = doc.select("div.breadcrumbs").text();
			
    	}
		return new String[] {pdNameDescription};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
    		if(productPictureUrl.indexOf("/no-picture.png") > -1){
    			productPictureUrl = null;
    		}
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if (evaluateResult.length == 1) {
    		pdId = FilterUtil.getStringBetween(evaluateResult[0], "ISBN</th>", "</tr>");
    		pdId = FilterUtil.toPlainTextString(pdId);
        }
		return new String[] { pdId};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
	 	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	String[] productNameDescription = mpdBean.getProductNameDescription();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] realProductId = mpdBean.getRealProductId();
        String[] upc = mpdBean.getProductUpc();
        String[] basePrice = mpdBean.getProductBasePrice();
        String currentUrl = mpdBean.getCurrentUrl();


    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
		
		
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
				|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])
				|| realProductId == null || realProductId.length == 0 || StringUtils.isBlank(realProductId[0])) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		String ISBN = realProductId[0];
		pdName += " ("+ ISBN +")";
		if(upc[0].contains(" Stationery & Gift Ideas"))
			rtn[0].setName(pdName);
		else if(upc[0].contains("Magazine Subscription"))
			rtn[0].setName("นิตยสาร "+pdName);
		else
			rtn[0].setName("หนังสือ "+pdName);
		rtn[0].setRealProductId(ISBN);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		String author = "";
		if (productNameDescription != null && productNameDescription.length != 0) {
			author = productNameDescription[0];
			if(!author.isEmpty()){
				rtn[0].setKeyword(author);
				author = "ผู้แต่ง/ผู้แปล: " + author;
			}
		}
		
		if(basePrice!=null&&basePrice.length!=0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
		}
		
		if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
			String publisher = productDescription[0];
			String desc = null;
			if(!publisher.isEmpty()){
				desc = "สำนักพิมพ์: " + publisher;
			}
			if(!author.isEmpty()){
				if(desc != null){
					desc = author + ", " + desc;
				}else{
					desc = author;
				}
			}
			rtn[0].setDescription(desc);
		}
		return rtn;
	}
	
	 public ProductDataBean[] mergeProductData(String[] productName, String[] productPrice, String[] productUrl,
            String[] productDescription, String[] productPictureUrl,
            String[] merchantUpdateDate, String[] productNameDescription,
            String[] realProductId,
            String currentUrl) {

		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
				|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])
				|| realProductId == null || realProductId.length == 0 || StringUtils.isBlank(realProductId[0])) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		String ISBN = realProductId[0];
		pdName += " (ISBN:"+ ISBN +")";
		
		rtn[0].setName(pdName);
		rtn[0].setRealProductId(ISBN);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		String author = "";
		if (productNameDescription != null && productNameDescription.length != 0) {
			author = productNameDescription[0];
			if(!author.isEmpty()){
				rtn[0].setKeyword(author);
				author = "ผู้แต่ง/ผู้แปล: " + author;
			}
		}
		
		if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
			String publisher = productDescription[0];
			String desc = null;
			if(!publisher.isEmpty()){
				desc = "สำนักพิมพ์: " + publisher;
			}
			if(!author.isEmpty()){
				if(desc != null){
					desc = author + ", " + desc;
				}else{
					desc = author;
				}
			}
			rtn[0].setDescription(desc);
		}

		return rtn;
	}
	 
	 
	 @Override
		public ProductDataBean[] parse(String url) {
			try {
				String checkResult = checkUrlBeforeParse(url);
				if(checkResult != null && checkResult.equals("delete"))
					return processHttpStatus(null);
				
				String targetUrl = url;
		        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
		            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"})) {
		            	targetUrl = BotUtil.encodeURL(url);
		            }
		        }
				
				String[] htmlContent = null;
				htmlContent = httpRequestWithStatus(targetUrl, getCharset(), true);
				
				if (htmlContent == null || htmlContent.length != 2)
					return null;
				
				String httpStatus = htmlContent[1];
				
				if(httpStatus!="200") {
					logger.info("status : "+htmlContent[1]+" url: "+targetUrl);
				}
				
				if(httpStatus.equals("400"))
					return processHttpStatus(htmlContent[1]);
				
				String html = htmlContent[0];
				if(html == null)
					return processHttpStatus(httpStatus, false);
				
				Map<String, String[]> config = getConfiguration();
				if (config == null)
					return null;
				
				String[] productName = null;
				String[] productPrice = null;
				String[] productUrl = null;
				String[] productDescription = null;
				String[] productPictureUrl = null;
				String[] merchantUpdateDate = null;
				String[] productNameDescription = null;
				String[] productExpire = null;
				String[] realProductId = null;
				String[] productBasePrice = null;
				String[] productUpc = null;
				String[] data = null;
				
				String[] result = evaluate(html, config.get("expire"));
				if (result != null) {
		    		productExpire = getProductExpire(result);                
		    		if(productExpire != null && productExpire.length == 1 && productExpire[0].equals("true"))
		    			return processHttpStatus(httpStatus);
		    	}
				
				result = evaluate(html, config.get("name"));
				if (result != null) {
					productName = getProductName(result);
					productName = FilterUtil.formatHtml(productName);
					productName = FilterUtil.removeSpace(productName);
				}
				
				result = evaluate(html, config.get("price"));
				if (result != null) {
					productPrice = getProductPrice(result);
					productPrice = FilterUtil.removeSpace(productPrice);
				}
				result = evaluate(html, config.get("url"));
				if (result != null) {
					productUrl = getProductUrl(result);
					productUrl = FilterUtil.removeSpace(productUrl);
				}
				result = evaluate(html, config.get("description"));
				if (result != null) {
					productDescription = getProductDescription(result);
					productDescription = FilterUtil.formatHtml(productDescription);
					productDescription = FilterUtil.removeSpace(productDescription);
				}
				result = evaluate(html, config.get("pictureUrl"));
				if (result != null) {
					productPictureUrl = getProductPictureUrl(result);				
					productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
				}
				result = evaluate(html, config.get("merchantUpdateDate"));
				if (result != null) {
					merchantUpdateDate = getProductMerchantUpdateDate(result);
					merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
					merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
				}
				result = evaluate(html, config.get("nameDescription"));
				if (result != null) {
					productNameDescription = getProductNameDescription(result);
					productNameDescription = FilterUtil.removeSpace(productNameDescription);
				}
							
				result = evaluate(html, config.get("realProductId"));
				if (result != null) {
					realProductId = getRealProductId(result);
					realProductId = FilterUtil.removeSpace(realProductId);
				}
				
				result = evaluate(html, config.get("basePrice"));
				if (result != null) {
					productBasePrice = getProductBasePrice(result);
					productBasePrice = FilterUtil.removeSpace(productBasePrice);
				}
				
				result = evaluate(html, config.get("upc"));
				if (result != null) {
					productUpc = getProductUpc(result);
					productUpc = FilterUtil.removeSpace(productUpc);
				}
				
				result = evaluate(html, config.get("data"));
				if (result != null) {
					data = getData(data);
					data = FilterUtil.removeSpace(data);
				}
				
				ProductDataBean[] productDataBean = null;
				FILTER_TYPE filterType = getFilterType();
				MergeProductDataBean mpdBean = new MergeProductDataBean();
				mpdBean.setProductName(productName);
				mpdBean.setProductPrice(productPrice);
				mpdBean.setProductUrl(productUrl);
				mpdBean.setProductDescription(productDescription);
				mpdBean.setProductPictureUrl(productPictureUrl);
				mpdBean.setMerchantUpdateDate(merchantUpdateDate);
				mpdBean.setProductNameDescription(productNameDescription);
				mpdBean.setRealProductId(realProductId);
				mpdBean.setCurrentUrl(url);
				mpdBean.setProductBasePrice(productBasePrice);
				mpdBean.setProductUpc(productUpc);
				mpdBean.setData(data);
				
				if (filterType.equals(FILTER_TYPE.DEFAULT)) {
		        	productDataBean = mergeProductData(mpdBean);   	   	
		        } else if (filterType.equals(FILTER_TYPE.PRODUCTLIST)) {
		        	result = evaluate(html, config.get("allData"));
					if (result != null) {
						productDataBean = getAllProductData(result);
						for (int i = 0; i < productDataBean.length; i++) {
							if(productDataBean[i] == null)
								continue;
							String name = FilterUtil.formatHtml(productDataBean[i].getName());
							name = FilterUtil.removeSpace(name);
							productDataBean[i].setName(name);
							
							String description = FilterUtil.formatHtml(productDataBean[i].getDescription());
							description = FilterUtil.removeSpace(description);
							productDataBean[i].setDescription(description);
							
							productDataBean[i].setPictureUrl(FilterUtil.removeSpace(productDataBean[i].getPictureUrl()));
						}
					}	        	                
		        	productDataBean = mergePriceListProductData(productDataBean, mpdBean);
		        }
				
				if(isEmpty(productDataBean))
					productDataBean = new ProductDataBean[]{ new ProductDataBean()};
				productDataBean[0].setHttpStatus(httpStatus);
				
				return productDataBean;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
			ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
			rtn[0].setHttpStatus(httpStatus);
			rtn[0].setExpire(expire);
			return rtn;
		}
		
		private ProductDataBean[] processHttpStatus(String httpStatus) {
			return processHttpStatus(httpStatus, true);
		}
		
		public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
			HttpURLConnection conn = null;
			boolean success = false;
			try {
				URL u = new URL(url);
				conn = (HttpURLConnection)  u.openConnection();
				conn.setInstanceFollowRedirects(redirectEnable);			
				conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
				conn.setConnectTimeout(20000);
				conn.setReadTimeout(20000);
				
				int status = conn.getResponseCode();
				success = status < HttpURLConnection.HTTP_BAD_REQUEST;
				if(!success)
					return new String[]{null, String.valueOf(status)};
				
				boolean gzip = false;
				String encode = conn.getContentEncoding();
				if(encode != null)
					gzip = encode.equals("gzip");
				
				try (InputStream is = conn.getInputStream();
					InputStreamReader isr = charset == null ? 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
	    			BufferedReader brd = new BufferedReader(isr);) {
		    		
					StringBuilder rtn = new StringBuilder();
					String line = null;
					while ((line = brd.readLine()) != null)
						rtn.append(line);
					return new String[]{rtn.toString(), String.valueOf(status)};
		    	}
			}catch (Exception e) {
				e.printStackTrace();
				success = false;
			}finally {
				if(!success)
					consumeInputStreamQuietly(conn.getErrorStream());
				if(conn != null)
					conn.disconnect();
			}
			return null;
	    }
		
		public static void consumeInputStreamQuietly(InputStream in) {
			if (in == null) return;
			byte[] buffer = new byte[1024];
			try {
				while (in.read(buffer) != -1) {}
			} catch (IOException ex) {
			} finally {
				if(in != null)
					try { in.close(); } catch (IOException e) {}
			}
		}
	    
}