package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BatorastoreHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1>"});
		map.put("price", 		new String[]{"<div class=\"price\">"});
		map.put("basePrice", 	new String[]{"<div class=\"price\">"});
		map.put("description", 	new String[]{"<div class=\"view-product\" style=\"width:60%; float:left;\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-info\">"});
		map.put("expire", 		new String[]{"<div class=\"right\">"});
		map.put("upc", 			new String[]{"<span class=\"product-label-value\">"});
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("มีสินค้า") != -1) {
        		return new String[]{""};
        	}	
    	}    	
        return new String[]{"true"};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-new\">", "</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-old\">", "</span>");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String detail = "";
    	if(evaluateResult.length == 1) {
    		detail = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	
    	return new String[] {detail};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = "";
		if(evaluateResult.length > 0){
			for(String s : evaluateResult){
				upc = FilterUtil.toPlainTextString(s);
				if(StringUtils.isNotBlank(upc)){
					break;
				}
			}
		}
		return new String[] {upc};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productUpc = mpdBean.getProductUpc();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();

		String newProductName = productName[0];
		
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
			newProductName = newProductName + " (" + productUpc[0] + ")";
		}

		rtn[0].setName(newProductName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}


		return rtn;
	}
	    
}