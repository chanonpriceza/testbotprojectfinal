package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BPMUSCLEHTMLParser extends DefaultHTMLParser{
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("nameDescription", new String[]{"<label id=\"option_description.*class=\"option-items\".*>"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"unit size1of3 images\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		String brand = "";
		String size = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"productsizeprice\">", "<table class=\"productsellertable\">");
		if(evaluateResult.length > 0) {
			brand = FilterUtil.getStringBetween(evaluateResult[0] ,"<span style=\"text-transform: uppercase;\">","</span>");
			size = FilterUtil.getStringBetween(size, "<div class=\"size\">", "</div>");
			size = FilterUtil.getStringBetween(size, "<b>", "</b>");
			productName = FilterUtil.getStringBetween(evaluateResult[0] ,"<h1>","</h1>");
			if(size.equals("")){
				productName = brand + " " +productName;				
			}
			else{
				productName = brand + " " +productName + " [" + size+"]";
			}
			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(StringUtils.isNoneBlank(evaluateResult[0])){
			String aresPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-buy\">", "<tr class=\"productsellertablerow\">");    	
			productPrice = FilterUtil.getStringBetween(aresPrice, "<div class=\"price\">", "</div>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if(productPrice.equals("")){				
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"BPproductdetailheadgroup\">", "<div style=\"margin-top:0px;\">");
				productPrice = FilterUtil.getStringBetween((productPrice), "<div class=\"price\">", "</div>");
				productPrice = FilterUtil.getStringBetween((productPrice), "<b>", "</b>");
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
		}	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
			bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice).trim();
    		if(bPrice.equals("")){
    			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"BPproductdetailheadgroup\">", "<div style=\"margin-top:0px;\">");
    			bPrice = FilterUtil.getStringBetween((bPrice), "<div class=\"price\">", "</div>");
				bPrice = FilterUtil.getStringBetween((bPrice), "<p> <span>", "</span></p>");
				bPrice = FilterUtil.toPlainTextString(bPrice);
				bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {	
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"description\" content=\"", "\"");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img id=\"myImg\" src=\"", "\"");
    		if(productPictureUrl.equals("")){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src='", "'");
    		}
        }
    	return new String[] {productPictureUrl};
	}

}
