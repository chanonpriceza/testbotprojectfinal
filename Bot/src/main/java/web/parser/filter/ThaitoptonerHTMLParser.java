package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThaitoptonerHTMLParser extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-view\">"});
		map.put("price", new String[]{"<div class=\"product-essential\">"});
		map.put("basePrice", new String[]{"<div class=\"product-essential\">"});
		map.put("description", new String[]{"<div class=\"product-view\">"});
		map.put("pictureUrl", new String[]{"<p class=\"product-image\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<h1>","</h1>");
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"price-box\">","<div class=\"clear\">");
    		if(productPrice.contains("<p class=\"special-price\">")) {
    			productPrice = FilterUtil.getStringBetween(productPrice, "<p class=\"special-price\">", "/span>");
    			productPrice = FilterUtil.getStringBetween(productPrice, "\">", "<");
    		}else if(productPrice.contains("<span class=\"regular-price\"")){
    			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"price-box\">","<p class=\"special-price\">");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, "<span class=\"price\"", "/span>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, "\">", "<");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		if(evaluateResult.length > 0) {
			productDes = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"std\">" ,"</tbody>");
			productDes = FilterUtil.getStringAfter(productDes, "<h1>" ,"</table>");
			productDes = FilterUtil.toPlainTextString(productDes);
			productDes = FilterUtil.removeHtmlTag(productDes);
		}
		return new String[] {productDes};
	}


	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"<img src=\"","\"");
		}
		return new String[] {productPic};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		if(productBasePrice.length == 0 || StringUtils.isBlank(productBasePrice[0]) ) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productPrice[0]));
		}
		rtn[0].setUrl(currentUrl);
		if(StringUtils.isNotBlank(productBasePrice[0])){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}
		
		return rtn;
	}
}
