package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class HippoHTMLParser extends DefaultHTMLParser{
			
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<h1 class=\"heading-title\" itemprop=\"name\">"});
		map.put("price", 		new String[]{"<li class=\"price-new\" itemprop=\"price\">"});
		map.put("basePrice", 	new String[]{"<li class=\"price-old\">"});
		map.put("description", 	new String[]{"<div class=\"tab-pane tab-content active\" id=\"tab-description\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"row product-info split-50-50\">"});
		map.put("realProductId", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBefore(evaluateResult[0], "รหัสสินค้า", evaluateResult[0]);
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";    	
    	if (evaluateResult.length == 1) {
    		productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if(evaluateResult.length > 0){
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:url\"", "/>");
    		realProductId = FilterUtil.getStringBetween(realProductId, "product_id=", "\"");
    	}
		return new String[] {realProductId};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productId = mpdBean.getRealProductId();
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0] + " ("+productId[0] + ")");
		rtn[0].setRealProductId(productId[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setUrl(currentUrl);
		if(StringUtils.isNotBlank(productBasePrice[0])){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}
		
		return rtn;
	}
}