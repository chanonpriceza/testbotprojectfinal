package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class FnoutletHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] {"<h1 itemprop=\"name\" class=\"product_title entry-title\">"});
		map.put("price", new String[] {"<div class=\"summary-inner \">"});
		map.put("basePrice", new String[] { "<div class=\"summary-inner \">" });
		map.put("description",new String[] { "<div class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab\" id=\"tab-description\">"});
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
//		map.put("expire", new String[] { "<p class=\"stock in-stock\">" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = ""; 
		if (evaluateResult.length > 0) {

			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = productName.replace("&#8211;", "");
		
		}
		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#3647;</span>", "</span></ins>");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\"><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#3647;</span>", "</span></p>");
			}

			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		if(StringUtils.isBlank(productPrice))
			productPrice = BotUtil.CONTACT_PRICE_STR;
		return new String[] { productPrice };
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String basePrice = "";
		if (evaluateResult.length > 0) {				
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\"><del><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#3647;</span>", "</span></del>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);

		}
		
		return new String[] { basePrice };
	}
	
	

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
			if(productPictureUrl.equals("")) {
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"twitter:image\" content=\"", "\"");
			}
		}
		return new String[] { productPictureUrl };
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
		if (evaluateResult.length == 1) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"product-", "\"");
		}
		return new String[] { realProductId };
	}
	
	
	
	
//	@Override
//	public String[] getProductExpire(String[] evaluateResult) {
//		String expire = "false";
//		if(evaluateResult[0].contains("มีสินค้า")) {
//			expire = "true";
//		}
//		return new String[] { expire };
//	}

	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0 && realProductId[0] != "") {
			productName[0] = productName[0] + " (" + realProductId[0]+")";
		}
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		rtn[0].setPictureUrl(productPictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);


		return rtn;
	}
}


