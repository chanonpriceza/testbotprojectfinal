package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class MilleBeauteHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"price\">"});
		map.put("basePrice", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{"<div id=\"tabs-1\">"});
		map.put("pictureUrl", new String[]{"<div class=\"img\">"});
		map.put("realProductId", new String[]{"<div class=\"small grey\">"});
		map.put("expire", new String[]{"<div class=\"status\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringAfter(evaluateResult[0], "<div class=\"head\"", "");
			productName = FilterUtil.getStringAfter(productName, ">", "");
			productName = FilterUtil.getStringBefore(productName, "<div", "");
			productName = StringEscapeUtils.unescapeHtml4(productName);
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"sell\">", "</span>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"pre\">", "</span>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) { 
			productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
			productDesc = FilterUtil.toPlainTextString(productDesc);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringAfter(evaluateResult[0], ":", "");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   		realProductId = realProductId.trim();
	   	}
	   	
	    return new String[]{realProductId}; 
   }
	
	public String[] getProductExpire(String[] evaluateResult) {
    	
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].contains("พร้อมส่ง")) {
        		return new String[]{"false"};
        	}
    	}    	
        return new String[]{"true"};
	}
}