package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WishbeerHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", 			new String[]{""});
		map.put("price", 			new String[]{"<span id=\"our_price_display\" class=\"price\" itemprop=\"price\">"});
		map.put("basePrice", 		new String[]{"<span id=\"old_price_display\">"});
		map.put("description", 		new String[]{"<div class=\"pb-right-column col-xs-12 col-sm-8 col-md-9\">"});
		map.put("pictureUrl", 		new String[]{""});
		map.put("realProductId", 	new String[]{"<span class=\"product_code\">"});
		map.put("expire", 			new String[]{"<div id=\"image-block\" class=\"clearfix\">"});
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].toLowerCase().indexOf("out of stock") != -1) {
        		return new String[]{"true"};
        	}     	
    	}    	
        return new String[]{""};
	}
   
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length > 0){
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 class=\"prod_name\" itemprop=\"name\">","</h1>");			
			productName = FilterUtil.toPlainTextString(productName);
			
			if(evaluateResult[0].indexOf("_product_type=\"Beer\"") > 0){
				productName = "Beer " + productName;
			}
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		if(evaluateResult.length > 0){
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0){
			productDesc = FilterUtil.getStringAfter(evaluateResult[0], "</h1>", evaluateResult[0]);
			productDesc = FilterUtil.toPlainTextString(productDesc);	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		
		/*
		if(evaluateResult.length > 0){
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "itemprop='image'", "vCSS_img_larger_photo");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "href='", "'>");
        }
		*/
		
		//issue --> https://priceza.atlassian.net/browse/NMC-2307
		productPictureUrl = "https://www.wishbeer.com/img/cms/wb.png";
		
		//issue --> https://priceza.atlassian.net/browse/NMC-4813
		productPictureUrl = "http://s.pictub.club/2016/10/25/PtSBQ.png";
		
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.toPlainTextString(evaluateResult[0]);			
        }
    	return new String[] {realProductId};
	}    
	
}
