package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PositifHTMLParser extends DefaultHTMLParser {
//	public String getCharset() {
//		return "tis-620";
//	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-info-main pc-div\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"product attibute description\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"page-title-wrapper product\">" , "<div class=\"product-reviews-summary empty\">");
			productName = FilterUtil.toPlainTextString(productName);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"price-box price-final_price\" data-role=\"priceBox\"","<div class=\"product-add-form\">");
			if(productPrice.contains("<span class=\"special-price\">")){
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price-label\">Special Price</span>", "</span>");
			}else{
				productPrice = FilterUtil.getStringBetween(productPrice, "class=\"price-wrapper", "</span>");
			}
			
			productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";    	
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"price-box price-final_price\" data-role=\"priceBox\"","<div class=\"product-add-form\">");
    		productBasePrice = FilterUtil.getStringAfter(productBasePrice, "<span class=\"old-price\">","");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {  	
    		productPictureUrl = FilterUtil.getStringAfter(evaluateResult[0], "<meta property=\"og:type\" content=\"og:product\" />", "");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<meta property=\"og:image\"", "/>"); 
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}