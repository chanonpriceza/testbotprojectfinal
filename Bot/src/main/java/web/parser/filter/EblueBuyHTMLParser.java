package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class EblueBuyHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span  itemprop=\"name\" >"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"description\"  itemprop=\"description\" >"});
		map.put("pictureUrl", new String[]{"<div class=\"images-area \" style=\" \">"});
		
		return map;
	}
	
    public String[] getProductName(String[] evaluateResult) {
		String productName = "";   	
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta itemprop=\"price\" content=\"", "\"");
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], " <link rel=\"image_src\" href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}  
}