package web.parser.filter;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class IDealInThaiHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span class=\"txt_head2\">"});
		map.put("price", new String[]{"<div style=\"margin-left:60px;margin-top:-75px;\">"});
		map.put("basePrice", new String[]{"<td align=\"center\" class=\"txt_price3\">"});
		map.put("description", new String[]{"<td align=\"left\" valign=\"top\" class=\"txt_desc\" style=\"padding:10px 20px 10px 10px;\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{ ""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length > 0) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}

	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"wlt-DealSlideshow-1\" class=\"worklet wlt-DealSlideshow-worklet\">", "alt");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "true";
		String endTime = FilterUtil.getStringBetween(evaluateResult[0], "untilDate = new Date(", ")").trim();
		if(StringUtils.isNumeric(endTime) && endTime.length() == 13){
			long now = new Date().getTime();
			long endSec = Long.parseLong(endTime);
			if(now < endSec){
				exp = "false";
			}
		}
		return new String[] { exp};
	}
	    
}