package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class MTIHTMLParser extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{"<div class=\"wp_detailprice\">"});
		map.put("basePrice", 	new String[]{"<div class=\"txt_price txt_special\">"});
		map.put("description", 	new String[]{"<div class=\"wp_detailproduct\">"});
		map.put("pictureUrl",	new String[]{"<div class=\"picZoomer\">"});
		
		return map;
	}
	

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			String name  = FilterUtil.getStringBetween(evaluateResult[0], "<h2>", "</h2>");
			String name_desc  = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"txt_descript\">", "</p>");
			
			if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(name_desc)){
				productName  = "MTI "+ name_desc+ " "+name;
			}
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"txt_price\">", "</div>");
			if(StringUtils.isBlank(productPrice)){
				productPrice = FilterUtil.getStringAfter(evaluateResult[0], "à¸£à¸²à¸„à¸² :", evaluateResult[0]);
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length > 0) {
			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

}
