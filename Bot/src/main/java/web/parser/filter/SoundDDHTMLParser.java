package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;

import utils.FilterUtil;

public class SoundDDHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[] { "" });
		map.put("basePrice", new String[] { "<p class=\"price product-page-price \">","<p class=\"price product-page-price price-on-sale\">" });
		map.put("expire", new String[] { "<p class=\"stock out-of-stock\">" });
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
    
    

//		map.put("price", new String[] { "<p class=\"price product-page-price \">","<p class=\"price product-page-price price-on-sale\">" });
//		map.put("basePrice", new String[] { "<p class=\"price product-page-price \">","<p class=\"price product-page-price price-on-sale\">" });



	public String[] getProductName(String[] evaluateResult) {

		String productName = "";

		if (evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
//		<input type="hidden" name="gtm4wp_price" value="4990" />

		if (evaluateResult.length > 0) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("input[name=gtm4wp_price]");
				productPrice = FilterUtil.toPlainTextString(e.attr("value"));		
			
			if(productPrice.equals("")) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] { productPrice };
	}
	
	

	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		
		if (evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0],
					"<p class=\"price\"><del><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\"><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">"
					, "<span class=\"woocommerce-Price-amount amount\">");
			basePrice = FilterUtil.getStringBetween(basePrice, "</span>", "</span>");
			if(basePrice.equals("")) {
				basePrice = FilterUtil.getStringBetween(evaluateResult[0],
						"<del><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">"
						, "<ins><span class=\"");
				basePrice = FilterUtil.getStringBetween(basePrice, "</span>", "</span>");
			}
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] { basePrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[name=description]");
			productDesc = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length > 0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"woocommerce-product-gallery__image slide first\">", "<img width=\"");
			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "href=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}
	

	@Override
	public String[] getRealProductId(String[] evaluateResult) {

		String realProductId = null;
		if (evaluateResult.length == 1) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<button type=\"submit\" name=\"add-to-cart\" value=\"", "\"");
			if(realProductId.equals("")) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("input[name=gtm4wp_id]");
				realProductId = FilterUtil.toPlainTextString(e.attr("value"));
			}
		}
		return new String[] { realProductId };
	}
	
	
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
		if (evaluateResult.length > 0) {
			if(evaluateResult[0].contains("<p class=\"stock out-of-stock\">สินค้าหมดแล้ว</p>"))
			expire = "true";
		}
		return new String[] { expire };
	}

	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productId = mpdBean.getRealProductId();
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		
		if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if(StringUtils.isNoneBlank(productId[0])) {
			rtn[0].setName(productName[0] + " ("+productId[0] + ")");			
		}else {
			rtn[0].setName(productName[0]);	
		}
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setRealProductId(realProductId[0]);
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}
		
		return rtn;
	}

}