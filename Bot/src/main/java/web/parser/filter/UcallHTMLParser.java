package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class UcallHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<table width=\"100%\">"});
		map.put("description", new String[]{"<div id=\"tab_description\" class=\"tab_page\">"});
		map.put("pictureUrl", new String[]{"<td style=\"text-align: center; width: 250px; vertical-align: top;\">"});
		map.put("realProductId", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	if (evaluateResult.length == 1) {    		
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<td><b>ราคา:</b></td>", "บาท");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl); 
    	}
    	return new String[] {productPictureUrl};
	}
	
public String[] getRealProductId(String[] evaluateResult) {
		
		String realProductId = null;
    	if (evaluateResult.length == 1) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div><input type=\"hidden\" name=\"product_id\" value=\"", "\" />");
        }
    	return new String[] {realProductId};
	}

@Override
public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

	String[] productName = mpdBean.getProductName();
	String[] productPrice = mpdBean.getProductPrice();
	String[] productDescription = mpdBean.getProductDescription();
	String[] productPictureUrl = mpdBean.getProductPictureUrl();
	String[] realProductId = mpdBean.getRealProductId();
	String currentUrl = mpdBean.getCurrentUrl();


	if (productName == null || productName.length == 0
			|| productPrice == null || productPrice.length == 0) {
		return null;
	}
	ProductDataBean[] rtn = new ProductDataBean[1];
	rtn[0] = new ProductDataBean();
	String pname = productName[0];
	String pdId = realProductId[0];
	if(StringUtils.isNotBlank(pname) && StringUtils.isNotBlank(pdId)){
		pname += " ("+ pdId + ")";
	}else{
		return null;
	}
	rtn[0].setName(pname);
	rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

	if (productDescription != null && productDescription.length != 0) {
		rtn[0].setDescription(productDescription[0]);
	}
	if (productPictureUrl != null && productPictureUrl.length != 0) {
		if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
			if (productPictureUrl[0].startsWith("http")) {
				rtn[0].setPictureUrl(productPictureUrl[0]);
			} else {
				rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
			}
		}
	}

	return rtn;
}
	    
}
