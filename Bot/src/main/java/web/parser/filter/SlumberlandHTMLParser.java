package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SlumberlandHTMLParser  extends DefaultHTMLParser {
	
//	private String[] banName = {
//			"Slumberland Regina ที่นอนขนาด3.5 ฟุต",
//			"Slumberland Regina ที่นอนขนาด 6 ฟุต"
//	};

   public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", new String[]{"<h1 class=\"mainbox-title\">"});
		map.put("nameDescription", new String[]{"<label id=\"option_description.*class=\"option-items\".*>"});
		map.put("price", new String[]{"<label id=\"option_description.*class=\"option-items\".*>","<div class=\"float-left product-prices\">"});
		map.put("basePrice", new String[]{"<label id=\"option_description.*class=\"option-items\".*>","<div class=\"float-left product-prices\">"});
		map.put("description", new String[]{"<div id=\"content_description\" class=\"wysiwyg-content\">"});
		map.put("pictureUrl", new String[]{"<div class=\"border-image-wrap cm-preview-wrapper\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
   
   
   public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
   }
   
   @Override
   public String[] getProductExpire(String[] evaluateResult) {
	   if(evaluateResult.length > 0) {
		   if(evaluateResult[0].contains("qty-out-of-stock")) {
			   return new String[]{"true"};
		   }
		   if(evaluateResult[0].contains("This product is electronically distributed.")) {
			   return new String[]{"true"};
		   }
	   }
	   return new String[]{"false"};
   }
	
	public String[] getProductNameDescription(String[] evaluateResult) {
		
		List<String> productNameDesc = new ArrayList<String>();
		for(int i = 0; i < evaluateResult.length; i++) {
			String tmp = FilterUtil.getStringBetween(evaluateResult[i], "<input type=\"radio\" class=\"radio\"", "<span");
			tmp = FilterUtil.getStringAfter(tmp, ">", "");
			tmp = StringEscapeUtils.unescapeHtml4(tmp);
			tmp = FilterUtil.toPlainTextString(tmp);
			productNameDesc.add(tmp);
		}
		
		return productNameDesc.toArray(new String[0]);
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		List<String> productPrice = new ArrayList<String>();
		if(evaluateResult.length > 0 && evaluateResult[0].contains("<span class=\"price\" id=\"line_discounted_price_")){
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\" id=\"line_discounted_price_", "/span>");
			tmp = FilterUtil.getStringBetween(tmp, "<span>", "<");
			tmp = FilterUtil.removeCharNotPrice(tmp);
			productPrice.add(tmp);
		}else{
			for(int i = 0; i < evaluateResult.length; i++) {
				String tmp = FilterUtil.getStringBetween(evaluateResult[i], "ราคาพิเศษ</span>", "</span>");
				tmp = StringEscapeUtils.unescapeHtml4(tmp);
				tmp = FilterUtil.toPlainTextString(tmp);
				tmp = FilterUtil.removeCharNotPrice(tmp);
				productPrice.add(tmp);
			}				
		}
		
		return productPrice.toArray(new String[0]);
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		List<String> basePrice = new ArrayList<String>();
		if(evaluateResult.length > 0 && evaluateResult[0].contains("<span class=\"old_price")){
			String tmp = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"strike\"><span>","</span>");
			tmp = FilterUtil.removeCharNotPrice(tmp);
			basePrice.add(tmp);
		}else{
			for(int i = 0; i < evaluateResult.length; i++) {
				String tmp = FilterUtil.getStringBetween(evaluateResult[i], "ราคาปกติ</span>", "</span>");
				tmp = StringEscapeUtils.unescapeHtml4(tmp);
				tmp = FilterUtil.toPlainTextString(tmp);
				tmp = FilterUtil.removeCharNotPrice(tmp);
				basePrice.add(tmp);
			}
		}
		
		return basePrice.toArray(new String[0]);
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {	
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = StringEscapeUtils.unescapeHtml4(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] name = mpdBean.getProductName();
		String[] nameDesc = mpdBean.getProductNameDescription();
		String[] price = mpdBean.getProductPrice();
		String[] basePrice = mpdBean.getProductBasePrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String brand = "Slumberland";
		
		
		if(name == null || name.length == 0 || price == null || price.length == 0 || basePrice == null) {
			return null;
		}
		
		if( price.length != basePrice.length) {
			return null;
		}
		
		List<ProductDataBean> rtn = new ArrayList<ProductDataBean>();
		if(nameDesc.length > 0 ){
			for(int i = 0; i < nameDesc.length; i ++) {
				ProductDataBean tmp = new ProductDataBean();
				tmp.setName(brand + " " + name[0] + " " + nameDesc[i]);
				tmp.setPrice(FilterUtil.convertPriceStr(price[i]));
				tmp.setBasePrice(FilterUtil.convertPriceStr(basePrice[i]));
				if(desc != null && desc.length > 0) {
					tmp.setDescription(desc[0]);
				}
				if(pictureUrl != null && pictureUrl.length > 0) {
					if (pictureUrl[0] != null && pictureUrl[0].trim().length() != 0) {
						if (pictureUrl[0].startsWith("http")) {
							tmp.setPictureUrl(pictureUrl[0]);
						} else {
							try {
								URL url = new URL(new URL(currentUrl), pictureUrl[0]);
								
								tmp.setPictureUrl(url.toString());
							} catch (MalformedURLException e) { }
						}
					}
				}
				
				if(currentUrl.contains("?size=")){
					tmp.setUrl(currentUrl);
					tmp.setUrlForUpdate(currentUrl);
				}
				else{
					tmp.setUrl(currentUrl + "?size=" + (i + 1));
					tmp.setUrlForUpdate(currentUrl + "?size=" + (i + 1));
				}
				
//				if(Arrays.asList(banName).contains(tmp.getName().trim())) {
//					continue;
//				}
				
				rtn.add(tmp);
			}
		}else{
			ProductDataBean tmp = new ProductDataBean();
			tmp.setName(brand + " " + name[0]);
			tmp.setPrice(FilterUtil.convertPriceStr(price[0]));
			tmp.setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
			if(desc != null && desc.length > 0) {
				tmp.setDescription(desc[0]);
			}
			if(pictureUrl != null && pictureUrl.length > 0) {
				if (pictureUrl[0] != null && pictureUrl[0].trim().length() != 0) {
					if (pictureUrl[0].startsWith("http")) {
						tmp.setPictureUrl(pictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), pictureUrl[0]);
							
							tmp.setPictureUrl(url.toString());
						} catch (MalformedURLException e) { }
					}
				}
			}
			
			if(currentUrl.contains("?size=")){
				tmp.setUrl(currentUrl);
				tmp.setUrlForUpdate(currentUrl);
			}
			else{
				tmp.setUrl(currentUrl);
				tmp.setUrlForUpdate(currentUrl);
			}
			
//			if(Arrays.asList(banName).contains(tmp.getName().trim())) {
//				return null;
//			}
			
			rtn.add(tmp);
		}
		
		
		return rtn.toArray(new ProductDataBean[0]);
	}
		
}
