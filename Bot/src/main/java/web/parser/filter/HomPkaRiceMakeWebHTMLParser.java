package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class HomPkaRiceMakeWebHTMLParser extends TemplateMakeWebEasyHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{"<div class=\"divProductDescription\">"});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">","<div class=\"productItemDetail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("expire",new String[] {"<div class=\"productStock\">"});

	//	map.put("expire", new String[]{"<div class=\"product_comingsoon warningBox\" v-bind:class=\"'product_'+show_pdata.status.sell\" itemprop=\"availability\" content=\"out_of_stock\">"});
		
		return map;
	}
	
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String price = BotUtil.CONTACT_PRICE_STR;
		if(evaluateResult.length > 0) {
			price = FilterUtil.getStringBetween(evaluateResult[0],"ราคา", "บาท");
			if(StringUtils.isBlank(price)) {
				price = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] {price};
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
		if(evaluateResult.length >0) {
			if(evaluateResult[0].contains("สินค้าหมด")) {
				expire = "true";
			}
		}
		return new String[] {expire};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
    	String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
}
