package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BebebeHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"t-page-title m-search-title\">"});
		map.put("price", new String[]{"<div itemprop=\"offers\" >"});
		map.put("description", new String[]{"<span class=\"posted_in\" style=\"margin-bottom: 5px\">"});
		map.put("pictureUrl", new String[]{"<div class=\"img-thumbnail\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-currencySymbol\">", "</span>");
			productPrice = FilterUtil.toPlainTextString(productPrice);	
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียด:", evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = "http://www.be-bebe.com/"+productPictureUrl;
        }
    	
    	return new String[] {productPictureUrl};
	}
}
