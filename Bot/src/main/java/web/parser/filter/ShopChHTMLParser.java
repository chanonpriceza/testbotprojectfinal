package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ShopChHTMLParser extends DefaultHTMLParser {
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
			map.put("name", new String[]{"<div class=\"detail\">"});
			map.put("price", new String[]{"<p class=\"price\">"});
			map.put("basePrice", new String[]{"<p class=\"price\">"});
			map.put("description", new String[]{"<div class=\"tabs-detail\">"});
			map.put("pictureUrl", new String[]{"<div class=\"img-product\">"});		
			map.put("expire", new String[]{""});
			map.put("realProductId", new String[]{"<div class=\"detail\">"});
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			if(evaluateResult.length != 0 ) {	
				String temp = "";
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>").trim();
				temp = FilterUtil.getStringBetween(evaluateResult[0], "<strong>", "</strong>").trim();
				if(StringUtils.isNotEmpty(temp)) {
					productName += " " + temp;
				}
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<b>", "</b>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }		
			return new String[] {productPrice};
		}

		public String[] getProductBasePrice(String[] evaluateResult) {
			String bPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		bPrice = FilterUtil.getStringBefore(evaluateResult[0], "<b>", "");
	    		bPrice = FilterUtil.toPlainTextString(bPrice);
	    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
	        }		
			return new String[] {bPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
	        }
	    	return new String[] {productPictureUrl};
		}
		
		public String[] getProductExpire(String[] evaluateResult) {
			
	    	if(evaluateResult.length == 1) {
	        	if(evaluateResult[0].indexOf("<p class=\"have-pd\">มี</p>") != -1) {
	        		 return new String[]{""}; 
	        	}     	
	    	}    	
	        return new String[]{"true"};  // no get product
		}
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pdId = "";
			if(evaluateResult.length == 1){
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า", "</p>");
				pdId =  FilterUtil.toPlainTextString(pdId).trim();
			}
				return new String[] {pdId};
		}
}
