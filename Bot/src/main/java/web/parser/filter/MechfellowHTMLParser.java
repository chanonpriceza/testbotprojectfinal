package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class MechfellowHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<span id=\"old_price_display\">"});
		map.put("description", new String[]{"<div id=\"product-description-tab-content\" class=\"product-description-tab-content tab-pane\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product clearfix col-xs-12 col-sm-6\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"taxInclPrice","taxExclPrice");
    		productPrice = productPrice.replace("\\u0e3f","");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
 
        } 
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";   
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
    		double bPriceTax = FilterUtil.convertPriceStr(productPrice)*1.07;
    		productPrice = String.valueOf(bPriceTax);
 
        } 
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}
