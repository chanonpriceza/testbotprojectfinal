package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class MattressCityHTMLParser extends BasicHTMLParser {
	protected static final Logger logger = LogManager.getRootLogger();
	private static 	JSONParser parser  = new JSONParser();
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			mixIdName =  true;
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("expire", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:title]");
				productName = FilterUtil.toPlainTextString(e.attr("content"));			
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String json = "";
			try {
				String idR = FilterUtil.getStringBetween(evaluateResult[0],"products\":[\"","\"");
				json = FilterUtil.getStringBetween(evaluateResult[0],"optionPrices\":",",\"priceFormat");
				JSONObject x = (JSONObject)parser.parse(json);
				x = (JSONObject) x.get(idR);
				x = (JSONObject) x.get("finalPrice");
				json = String.valueOf(x.get("amount"));
				json = FilterUtil.removeCharNotPrice(json);;
			}catch (Exception e) {
				logger.error(e.getMessage());
			}
			if(StringUtils.isBlank(json)) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-info-price");
				e = e.select("span[data-price-type=finalPrice]");
				return new String[] {FilterUtil.removeCharNotPrice(e.html())};
			}
			return new String[] {json};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String json = "";
			try {
				String idR = FilterUtil.getStringBetween(evaluateResult[0],"products\":[\"","\"");
				json = FilterUtil.getStringBetween(evaluateResult[0],"optionPrices\":",",\"priceFormat");
				JSONObject x = (JSONObject)parser.parse(json);
				x = (JSONObject) x.get(idR);
				x = (JSONObject) x.get("oldPrice");
				json = String.valueOf(x.get("amount"));
				json = FilterUtil.removeCharNotPrice(json);;
			}catch (Exception e) {
				e.printStackTrace();
			}
			if(StringUtils.isBlank(json)) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-info-price");
				e = e.select("span[data-price-type=oldPrice]");
				return new String[] {FilterUtil.removeCharNotPrice(e.html())};
			}
			return new String[] {json};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String id = "";    	
			if(evaluateResult.length == 1) {	
				String idR = FilterUtil.getStringBetween(evaluateResult[0],"products\":[\"","\"");
				String json = FilterUtil.getStringBetween(evaluateResult[0],"skus\":{","},");
				if(StringUtils.isBlank(json))
					 json = FilterUtil.getStringBetween(evaluateResult[0],"sku\":","\",");
				
				String idCheck  = FilterUtil.getStringBetween(json,idR,",");
				if(StringUtils.isBlank(idCheck))
					json = FilterUtil.getStringAfter(json,idR,",");
				else
					json =	FilterUtil.getStringBetween(json,idR,",");
				json = FilterUtil.removeCharNotPrice(json);;
				id = json;
			}	
			return new String[] {id};
		}
		

}
