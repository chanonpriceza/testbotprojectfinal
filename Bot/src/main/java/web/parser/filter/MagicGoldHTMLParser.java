package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MagicGoldHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<h1 class=\"product-title product_title entry-title\">"});
    	map.put("price", new String[]{"<p class=\"price product-page-price \">"});
		map.put("description", new String[]{"<div class=\"panel entry-content active\" id=\"tab-description\">"});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";        
		if(evaluateResult.length == 1) { 
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		
		if(evaluateResult.length == 1) {	
			productPrice = FilterUtil.getStringBefore(evaluateResult[0], "<span class=\"woocommerce-Price-currencySymbol\">", "");
			productPrice = FilterUtil.toPlainTextString(productPrice);		
	    	productPrice = FilterUtil.removeCharNotPrice(productPrice);	
	        if(StringUtils.isBlank(productPrice)) productPrice = BotUtil.CONTACT_PRICE_STR;
		}
    		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = evaluateResult[0];
    		productDesc = FilterUtil.getStringBefore(productDesc,"More details", productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
		if(evaluateResult.length == 1){
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"","\"");
    	}	
    	return new String[] {productPictureUrl};
	}
	
}