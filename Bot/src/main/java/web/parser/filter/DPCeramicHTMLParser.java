package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class DPCeramicHTMLParser extends BasicHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			productName = FilterUtil.toPlainTextString(e.attr("content"));			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e  = doc.select("p.price");
			e = e.select("ins");
			if(StringUtils.isNotBlank(e.html())){
				productPrice = FilterUtil.removeCharNotPrice(e.html());	
				return new String[] {productPrice};
			}
			
			e  = doc.select("p.price");
			e = e.select("span.woocommerce-Price-amount");
			if(e!=null&&e.size()>0) {
				productPrice = FilterUtil.removeCharNotPrice(e.get(0).html());	
			}
        	
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("del");	
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String real = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.summary");
			e = e.select("a.add-to-compare-link");
			real = FilterUtil.removeCharNotPrice(e.attr("data-product_id"));			
		}	
		return new String[] {real};
	}
 
}	
