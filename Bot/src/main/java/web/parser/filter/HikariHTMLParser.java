package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HikariHTMLParser extends DefaultHTMLParser{

	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", 			new String[]{""});
			map.put("price", 			new String[]{"<span class=\"price\">"});
			map.put("description", 		new String[]{"<div class=\"description\"  itemprop=\"description\" >"});
			map.put("pictureUrl", 		new String[]{"<div class=\"image-wrapper\">"});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";   
			if(evaluateResult.length == 1) {	
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"title title-with-text-alignment\">", "</h1>");
				if(productName.isEmpty()){
					productName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"breadcrumb\">", "</div>");
					productName = FilterUtil.getStringBetween(productName, "<span class=\"current\">", "</span>");
				}
				productName = FilterUtil.toPlainTextString(productName);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";   
			if(evaluateResult.length == 1) {
				productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
			if(evaluateResult.length > 0) {
				productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
	    		if(productPictureUrl.equals("#")){
	    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-src=\"", "\"");
	    		}

	        }
	    	return new String[] {productPictureUrl};
		}
		
		   @Override
		    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		    	String[] productName = mpdBean.getProductName();
				String[] productPrice = mpdBean.getProductPrice();
				String[] productDescription = mpdBean.getProductDescription();
				String[] productPictureUrl = mpdBean.getProductPictureUrl();
				String currentUrl = mpdBean.getCurrentUrl();
					
				
				if (productName == null || productName.length == 0
						|| productPrice == null || productPrice.length == 0) {
					return null;
				}
				String pdId = FilterUtil.getStringBetween(currentUrl, "www.hikaritool.com/", "/");
				String pdName = productName[0];
				
				ProductDataBean[] rtn = new ProductDataBean[1];
				rtn[0] = new ProductDataBean();
				if(StringUtils.isNotBlank(pdId)){
					pdName += " ("+ pdId +")";
					rtn[0].setRealProductId(pdId);
				}
				rtn[0].setName("Hikari " + pdName);
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
				rtn[0].setUrl(currentUrl);
				rtn[0].setUrlForUpdate(currentUrl);


				if (productDescription != null && productDescription.length != 0)
					rtn[0].setDescription(productDescription[0]);
				
				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
						if (productPictureUrl[0].startsWith("http")) {
							rtn[0].setPictureUrl(productPictureUrl[0]);
						} else {
							try {
								URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
								rtn[0].setPictureUrl(url.toString());
							} catch (MalformedURLException e) {}
						}
					}
				}

				return rtn;

			}    
			   
}
