package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ZENZENDREAMThailandHTMLParser extends DefaultHTMLParser {
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
	    	map.put("name", 			new String[]{"<div class=\"viewProdInfo\">"});
			map.put("price", 			new String[]{"<div class=\"viewProdPrice\" id=\"viewProdPrice\">"});
			map.put("basePrice", 		new String[]{"<div class=\"viewProdPrice\" id=\"viewProdPrice\">"});
			map.put("description", 		new String[]{""});
			map.put("pictureUrl", 		new String[]{"<div class=\"viewProdPicture\" style=\"position:relative\">"});
			map.put("expire", 			new String[]{"<span id=\"viewProdStock\">"});
			map.put("realProductId", 	new String[]{""});
			return map;
		}
	  
	   public String[] getProductExpire(String[] evaluateResult) {
	    	
	    	if(evaluateResult.length == 1) {
	        	if(evaluateResult[0].toLowerCase().indexOf("out of stock") != -1) {
	        		return new String[]{"true"};
	        	}     	
	    	}    	
	        return new String[]{"false"};
		}
	   
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";
			if(evaluateResult.length > 0){
				productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 class=\"viewProd_prodItemName proditemnamelang\" itemprop=\"name\"","</h1>");			
				productName = FilterUtil.getStringAfter(productName,">",productName);						
				productName = FilterUtil.toPlainTextString(productName);
			}
			return new String[] {productName};
		}
		public String[] getProductPrice(String[] evaluateResult) {
			
			String productPrice = "";   
			if(evaluateResult.length > 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"sellingPrice\" itemprop=\"price\">","</span>");
				if(productPrice.isEmpty()){
					productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"normalPrice\" itemprop=\"price\">","</span>");
				}
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			return new String[] {productPrice};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productBasePrice = "";   
			if(evaluateResult.length > 0){
				productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"normalPrice\">","</span>");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
			return new String[] {productBasePrice};
		}

		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
			if(evaluateResult.length > 0){
				productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"viewProd_prodItemLongDesc proditemlongdesclang\"","<div class=\"divViewProdRelated\">");
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"viewProd_prodItemLongDesc proditemlongdesclang\"","<div class=\"viewProd_related\">");
				}
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"viewProd_prodItemLongDesc proditemlongdesclang\"","<div style=\"text-align: center;\">");
				}
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"viewProd_prodItemLongDesc proditemlongdesclang\"","<p style=\"text-align: center;\">");
				}
				
				
				
				productDesc = FilterUtil.getStringAfter(productDesc, ">", productDesc);
				productDesc = FilterUtil.toPlainTextString(productDesc);	
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
			
			if(evaluateResult.length > 0){
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0]," href=\"","\"");
				productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
			}
	    	return new String[] {productPictureUrl};
		}
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pid = "";
			if(evaluateResult.length == 1){
				pid = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"iPKProduct0\" value=\"", "\"");
			}
			return new String[] {pid};
		}
		
		 @Override
		    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

				String[] productName = mpdBean.getProductName();
				String[] productPrice = mpdBean.getProductPrice();
				String[] productDescription = mpdBean.getProductDescription();
				String[] productPictureUrl = mpdBean.getProductPictureUrl();
				String[] realProductId = mpdBean.getRealProductId();
				String currentUrl = mpdBean.getCurrentUrl();
				String[] bPrice = mpdBean.getProductBasePrice();

				if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
					return null;
				}
				ProductDataBean[] rtn = new ProductDataBean[1];
				rtn[0] = new ProductDataBean();
				String pdName = productName[0];
				if(realProductId != null && realProductId.length != 0){
					String ProductId = realProductId[0];
					if(StringUtils.isNotBlank(ProductId)){
						pdName += " ("+ ProductId + ")";
					}
				}
				rtn[0].setName(pdName);
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
				
				if(bPrice != null && bPrice.length == 1){
					rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
				}


				if (productDescription != null && productDescription.length != 0) {
					rtn[0].setDescription(productDescription[0]);
				}

				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
						if (productPictureUrl[0].startsWith("http")) {
							rtn[0].setPictureUrl(productPictureUrl[0]);
						} else {
							try {
								URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
								rtn[0].setPictureUrl(url.toString());
							} catch (MalformedURLException e) {}
						}
					}
				}
				
				if(rtn[0].getBasePrice() == rtn[0].getPrice() ){
					rtn[0].setBasePrice(0.00);
				}
				
				return rtn;
			}
		
}
