package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateJDCentralHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.p-head-name");
			productName = FilterUtil.toPlainTextString(e.text());			
		}
		
		return new String[] {productName};
	}
	
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			productPrice = FilterUtil.getStringBetween(doc.toString().replaceAll("\n",""),"ราคา\",\"value\":\"","\"");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			productPrice = FilterUtil.getStringBetween(doc.toString().replaceAll("\n",""),"\"crossLinePrice\":\"","\"");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {

		String id = "";    	
		if(evaluateResult.length == 1) {	
			id = FilterUtil.getStringBetween(evaluateResult[0],"\"wareId\":\"","\"");
			id = FilterUtil.removeCharNotPrice(id);			
		}	
		//		productUrl = productUrl + "||https://jdcentral.onelink.me/SJGK?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_dp=openjdthai%3A%2F%2Fdeeplink%2Fcategory%2Fjump%2FM_sourceFrom%2Fsx_thai%2Fdes%2FproductDetail%2FskuId%2F******&af_reengagement_window=30d&clickid=(xxxxx)&af_ios_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html&af_android_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html";
		//productUrl = productUrl.replace("******", productId);
		return new String[] {id};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        String tmp = "";

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
    	if(mixIdName&&realProductId!=null&&realProductId.length>0&&StringUtils.isNotBlank(realProductId[0]))
    		tmp = " ("+realProductId[0]+")";
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+tmp);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if(allowContractPrice&&rtn[0].getPrice()==0)
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
			
		if(StringUtils.isNotBlank(currentUrl)&&isNotEmpty(realProductId)) {
			String productUrl = currentUrl + "||https://jdcentral.onelink.me/SJGK?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_dp=openjdthai%3A%2F%2Fdeeplink%2Fcategory%2Fjump%2FM_sourceFrom%2Fsx_thai%2Fdes%2FproductDetail%2FskuId%2F******&af_reengagement_window=30d&clickid=(xxxxx)&af_ios_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html&af_android_url=https%3A%2F%2Fm.jd.co.th%2Fproduct%2F******.html";
			productUrl = productUrl.replace("******", realProductId[0]);
			rtn[0].setUrl(productUrl);
		}
		
		
		return rtn;
	}
	
	
}
