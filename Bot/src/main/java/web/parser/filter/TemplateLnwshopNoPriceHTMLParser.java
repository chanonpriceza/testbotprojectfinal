package web.parser.filter;


import utils.FilterUtil;
import utils.BotUtil;
public class TemplateLnwshopNoPriceHTMLParser extends TemplateLnwshopHTMLParser{
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<tr class=\"priceTR\"", "</tr>");
    		if(!productPrice.isEmpty()){
	    		productPrice = FilterUtil.getStringBetween(productPrice, "<td class=\"bodyTD\">", "</td>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		if(productPrice.contains("-")) {
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		} else {
	    			productPrice = FilterUtil.removeCharNotPrice(productPrice); 
	    		}
	    		if("0.00".equals(productPrice)) {
	        		productPrice = BotUtil.CONTACT_PRICE_STR;
	        	}
    		}
        }
		return new String[] {productPrice};
	}
	    
}
