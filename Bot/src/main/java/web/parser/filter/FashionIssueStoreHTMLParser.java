package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class FashionIssueStoreHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "utf-8";
	}
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span itemprop=\"name\">",""});
		map.put("price", new String[]{"<span style=\"font-size:14px;\">"});
		map.put("basePrice", new String[]{"<form name=\"frmProduct\" method=\"post\" action=\"\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"zoom-img-p\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
   
	@Override
	public String[] getProductName(String[] evaluateResult) {
		String name = "";   
		
		if(evaluateResult.length > 0) {			
			name = FilterUtil.toPlainTextString(evaluateResult[0]);		
		}
		return new String[] {name};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {		
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<b>", "</b>");
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	   
	public String[] getRealProductId(String[] evaluateResult) {
		
		String realProductId = "";   
				
		if(evaluateResult.length > 0) {			
			realProductId = FilterUtil.getStringBetween(evaluateResult[0],"รหัสสินค้า :", "</b>");
			realProductId = FilterUtil.toPlainTextString(realProductId);		
		}
		
		return new String[] {realProductId};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if(evaluateResult.length == 1 && evaluateResult[0].contains("ราคาพิเศษ :")) {	
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา :", "&nbsp;บาท");
			bPrice = FilterUtil.toPlainTextString(bPrice);		
			bPrice = FilterUtil.removeCharNotPrice(bPrice);	
		}		
		return new String[] {bPrice};
		    
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"Description\" content=\"","\"");
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] basprice = mpdBean.getProductBasePrice();
		String currentUrl = mpdBean.getCurrentUrl();

		
    	if (productName == null || productName.length == 0	|| productPrice == null || productPrice.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setName(productName[0] + " (" + realProductId[0] + ") ");
		}else{
			rtn[0].setName(productName[0]);
		}

		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (basprice != null && basprice.length != 0) {
			try{
			rtn[0].setBasePrice( Double.parseDouble(basprice[0]));
			}catch (Exception e) {
				rtn[0].setBasePrice(0.00);
			}
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {
						//something
					}
				}
			}
		}
		
		rtn[0].setUrlForUpdate(currentUrl);
		rtn[0].setUrl(currentUrl);

		return rtn;
	}
}