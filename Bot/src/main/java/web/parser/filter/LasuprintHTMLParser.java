package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class LasuprintHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("price", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("description", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			String rawBrand = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> ยี่ห้อ ", "</div>");
			if(rawBrand.length() == 0){
				rawBrand = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'>ยี่ห้อ ", "</div>");
			}
			String rawType = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รุ่น ", "</div>");
			if(rawType.length() == 0){
				rawType = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'>รุ่น ", "</div>");
			}
			
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</div>");
			String rawId = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส ", "</div>");
			rawBrand = FilterUtil.toPlainTextString(rawBrand);
			rawType = FilterUtil.toPlainTextString(rawType);
			rawName = FilterUtil.toPlainTextString(rawName);
			rawId = FilterUtil.toPlainTextString(rawId);
			
			rawBrand = rawBrand.replace(":", "");
			rawType = rawType.replace(":", "");
			rawName = rawName.replace("&nbsp;", "");
			rawId = rawId.replace(":", "");
			
			if(rawName.trim().equals(rawBrand.trim())){
				rawBrand = "";
			}
			if(rawType.trim().equals(rawId.trim())){
				rawType = "";
			}
			productName = rawId+" "+rawBrand+" "+rawName+" "+rawType;
			
			if(productName.contains("สินค้าหมด stock")){
				productName = "";
    		}
			
			if(productName.contains("(Sold out)")){
				productName = "";
			}
			
			if(productName.length() == 0){
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span style=\"font-family: Tahoma;\">", "ราคา");
				rawName = rawName.replace("&nbsp;", "");
				rawName = FilterUtil.toPlainTextString("<div>"+rawName+"</div>");
				productName = rawName;
			}

			if(productName.length() == 0){
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span style=\"font-family: Tahoma; font-size: 11pt;\">", "ราคา");
				rawName = rawName.replace("&nbsp;", "");
				rawName = FilterUtil.toPlainTextString("<div>"+rawName+"</div>");
				productName = rawName;
			}
			
			if(productName.length() == 0){
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span style=\"outline: none; font-family: Tahoma;\">", "ราคา");
				rawName = rawName.replace("&nbsp;", "");
				rawName = FilterUtil.toPlainTextString("<div>"+rawName+"</div>");
				productName = rawName;
			}
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>สมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ราคาสมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ชิ้น</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 คู่</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ตัว</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p align=\"left\"><font color=\"#ff0000\" size=\"2\" face=\"Arial\">", "</font>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<br/><br/><table cellpadding=\"3\" cellspacing=\"1\" width=\"100%\" border=\"0\">", "</table>");
    			rawPrice = FilterUtil.getStringBetween(rawPrice, "<td align='right' style='padding:0px 10px 0px 3px;'><div style='color:#333333'>", "</div>");
    			if(rawPrice.contains("<strike>")){
    				rawPrice = "";
    			}
    		}
    		
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<br/><br/><table cellpadding=\"3\" cellspacing=\"1\" width=\"100%\" border=\"0\">", "</table>");
    			rawPrice = FilterUtil.getStringBetween(rawPrice, "</strike></div><div class='h3' style='color:#333333'>", "<input type='hidden'");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคา : </span>", "บาท");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		productPrice = productPrice.replace("&nbsp;", "");
    		if("** สินค้านำเข้า ประมาณ 20 วัน**".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดทั้งหมด :", "<input type='hidden' name='cart' value='add'>");
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "คุณสมบัติพิเศษ", "คุณสมบัติสินค้า");
    		}
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:20 0 5 0;'><span class='hd2'>", "<input type='hidden' name='cart' value='add'>");
    		}
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDescription+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<div style = \"clear:both;padding:5px\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href = \"", "\"");
    		if(productPictureUrl.length() == 0){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
    		}
        }
    	return new String[] {productPictureUrl};
	}  
}