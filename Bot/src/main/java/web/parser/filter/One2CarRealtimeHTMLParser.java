package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class One2CarRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<title>"});
		map.put("price", 		 new String[]{"<div class=\"listing__price  delta  weight--bold\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(evaluateResult[0]));
		return new String[] { price };
	}
	
}

