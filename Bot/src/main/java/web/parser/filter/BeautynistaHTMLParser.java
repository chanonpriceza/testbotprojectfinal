package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class BeautynistaHTMLParser extends DefaultHTMLParser {
	
	private static String LINK = "https://beautynista.com/market/post_productDetail_LoadProductPoint";
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<td align=\"left\" valign=\"top\" width=\"22%\">"});;
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("nameDescription", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:title\" content=\"","\"");
			productName = FilterUtil.toPlainTextString(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String stock = FilterUtil.getStringBetween(evaluateResult[0],"<b><font color=\"blue\">","</font></b>");
    		if(StringUtils.isNotBlank(stock) && stock.contains("มีสินค้าพร้อมส่ง")){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"salesPrice\">","</span>");
    			if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"price\">","</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);	
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
			String real = "";
			if (evaluateResult.length == 1) 	
				real = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:image\" content=\"","\"");	
			return new String[] {real};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String real = "";
		if (evaluateResult.length == 1) 	
			real = FilterUtil.getStringBetween(evaluateResult[0],"sku_id = '","'");	
		return new String[] {real};
	}
	
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String real = "";
		if (evaluateResult.length == 1) 	
			real = FilterUtil.getStringBetween(evaluateResult[0],"slug = '","'");	
		return new String[] {real};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		ProductDataBean[] rtn = new ProductDataBean[1];
		
		String sku = mpdBean.getRealProductId()[0]; 
		String slug = mpdBean.getProductNameDescription()[0];
		
		mpdBean = generateMergeProductData(mpdBean,sku,slug);
		
    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        

    	if (isEmpty(productName)||isEmpty(productPrice))
			return null;
    	
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+" ("+realProductId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
	
	private MergeProductDataBean generateMergeProductData(MergeProductDataBean mpdBean,String sku,String slug) {
		//ProductDataBean[] rtn = new ProductDataBean[1];
		try {
			String param = "slug={slug}&sku_id={sku}";
			param = param.replace("{slug}",slug).replace("{sku}",sku);
			param = BotUtil.encodeURL(param);
			String data = postRequest(LINK,param);
			if(!data.startsWith("{"))
				data = data.substring(1);
			JSONParser parser = new JSONParser();
			JSONObject dataJSON = (JSONObject) parser.parse(data);
			dataJSON = (JSONObject)dataJSON.get("debug");
			String  basePrice = "";
			String price = "";
			String desc = "";
			basePrice = String.valueOf(dataJSON.get("price"));
			price = String.valueOf(dataJSON.get("saleprice"));
			desc = String.valueOf(dataJSON.get("main_product_detail"));
			desc = FilterUtil.removeHtmlTag(desc);
			desc = FilterUtil.toPlainTextString(desc);
	
			mpdBean.setProductBasePrice(new String[] {basePrice});
			mpdBean.setProductDescription(new String [] {desc});
			mpdBean.setProductPrice(new String[] {price});
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mpdBean;
	}
	
	public static String postRequest(String url,String data) {
		HttpURLConnection conn = null;
		boolean success = false;
		String charset = "UTF-8";
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(true);			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.addRequestProperty("origin","https://www.jd.id");
			conn.addRequestProperty("referer","https://www.jd.id/search?keywords=acer&brandId=40");
			
			
			DataOutputStream	outStream = new DataOutputStream(conn.getOutputStream());
			outStream.writeBytes(data);
			outStream.flush();
			outStream.close();
			
			int status = conn.getResponseCode();

			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return null;
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
			
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return rtn.toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
