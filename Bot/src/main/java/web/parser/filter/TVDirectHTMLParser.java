package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class TVDirectHTMLParser extends DefaultTimeOutHTMLParser{
	
	// ISO-8859-1
	public String getCharset() {
		return "UTF-8";
	}
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">" });
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-desc-content\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<div class=\"product-sku product-info-block\">", "<div class=\"product-sku product-info-block clearfix\">"});
		
		return map;
	}
    
    public String[] getRealProductId(String[] evaluateResult) {
    	String realProductId = ""; 
		if(evaluateResult.length == 1) {	
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span>", "</span>");			
			realProductId = FilterUtil.toPlainTextString(tmp);
		}
		return new String[] {realProductId};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = ""; 
		if(evaluateResult.length == 1) {	
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"h1\">", "</span>");
			productName = FilterUtil.toPlainTextString(tmp);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	
    	if (evaluateResult.length == 1) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements html = doc.select("div.price-info");
    		Elements e = html.select("p.special-price");
    		Elements select = e.select("span.price");
   
    		if(select.size() > 1) {
    			productPrice = FilterUtil.removeCharNotPrice(select.get(0).html());
    		}else {
    			productPrice = FilterUtil.removeCharNotPrice(select.html());
    		}
    		
    		if(StringUtils.isBlank(productPrice)) {
    			 html = doc.select("div.price-info");
        		 e = html.select("span.regular-price");
        		 select = e.select("span.price");
        		 productPrice = FilterUtil.removeCharNotPrice(select.html());
    		}
        } 
    	
    	return new String[] {productPrice};  
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements html = doc.select("div.price-info");
    		Elements e = html.select("p.old-price");
    		Elements select = e.select("span.price");
    		productBasePrice  = FilterUtil.removeCharNotPrice(select.html());		
	    	
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDescription = null;
    	if (evaluateResult.length == 1) {
    		evaluateResult[0] = FilterUtil.removeHtmlTag(evaluateResult[0]);
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[0]);  
    		if(tmp.startsWith("รายละเอียด")) {
    			tmp = tmp.substring(10);
    		}
    			
    		productDescription = tmp;
    		productDescription = FilterUtil.toPlainTextString(productDescription);
        }    	
    	    	    	
    	return new String[] {productDescription};    
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-image=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
