package web.parser.filter;

import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;

public class PregelThailandShopHTMLParser extends IndividualLogHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"                            product-price\">"});
		map.put("description", new String[]{"<div class=\"full-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"picture-wrapper\">"});
		map.put("basePrice", new String[]{"<div class=\"old-product-price\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 itemprop=\"name\">", "</h1>");
			productName = FilterUtil.toPlainTextString(rawName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"                            product-price >", "</span>");
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		String rawDescript = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"full-description\" itemprop=\"description\">", "</div>");
    		productDesc = FilterUtil.toPlainTextString(rawDescript);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\"");
    		try{
    			productPictureUrl = URLEncoder.encode(productPictureUrl, "UTF-8");
    			productPictureUrl = productPictureUrl.replace("%2F", "/");
    			productPictureUrl = productPictureUrl.replace("%3A", ":");
    		} catch (Exception e) {}

        }
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		// TODO Auto-generated method stub
		String productBaseprice = null;
		if(evaluateResult.length == 1){
			productBaseprice = FilterUtil.getStringBetween(evaluateResult[0], "<span>", "</span>");
			productBaseprice = FilterUtil.removeCharNotPrice(productBaseprice);
		}
		return new String[] {productBaseprice};
	}
	
}
