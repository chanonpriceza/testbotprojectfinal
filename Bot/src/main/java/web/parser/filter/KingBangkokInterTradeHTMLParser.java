package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class KingBangkokInterTradeHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" border=\"0\">"});
		map.put("pictureUrl", new String[]{"<div style='margin:15px 0 15px 0;'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length > 0) {
			String id = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส    :", "</div>");
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "<span class='h3'></span>");
			if(rawName.equals("")) {
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"DESCRIPTION\" content=\"", "\"");
			}
			if(rawName.equals("")) {
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "</title>");
			}
			id = FilterUtil.toPlainTextString(id);
			if(id.trim().length() == 0){
				productName = FilterUtil.toPlainTextString(rawName);
			}else{
				productName = FilterUtil.toPlainTextString(rawName+" ("+id+")");
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'><b>ราคาพิเศษ</b>", "บาท");
    		
    		if(rawPrice.trim().length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'><b>ราคาปกติ</b>", "บาท");
    		}
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringAfter(evaluateResult[0], "<span class='hd'><b>ราคาปกติ</b>", "");
			if(!basePrice.contains("<span class='h3'><b>ราคาพิเศษ</b>")) {
				return new String[] {""};
			}
			basePrice = FilterUtil.getStringBefore(basePrice, "บาท", "");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href='", "'");
        }
    	return new String[] {productPictureUrl};
	}
	
	

}