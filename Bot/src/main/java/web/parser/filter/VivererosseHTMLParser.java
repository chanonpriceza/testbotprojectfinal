package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class VivererosseHTMLParser extends DefaultHTMLParser{	
	
	private double MYR_TO_THB = 7.89;
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h2 itemprop=\"name\">"});
		map.put("price", new String[]{"<span class=\"price\" itemprop=\"price\">", "<span class=\"price on-sale\" itemprop=\"price\">"});
		map.put("basePrice", new String[]{"<span class=\"compare-price\">"});
		map.put("description", new String[]{"<div class=\"description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-photo-container\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
		
    	if (evaluateResult.length > 0) {  		  		
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		productPrice = (Math.round(FilterUtil.convertPriceStr(productPrice)*MYR_TO_THB))+"";
        }		
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";
		
    	if (evaluateResult.length > 0) {  		  		
    		productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
    		productBasePrice = (Math.round(FilterUtil.convertPriceStr(productBasePrice)*MYR_TO_THB))+"";
        }		
    	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
		
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
}
