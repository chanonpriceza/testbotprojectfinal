package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SpecHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] {""});
		map.put("price", new String[] {""});
//		map.put("basePrice", new String[] { "<div class=\"row\">" });
		map.put("description",new String[] { "<div class=\"panel-body detailcss\">","<div class=\"electro-description clearfix\">"});
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = ""; 
		String title = "";
		if (evaluateResult.length > 0) {
			title = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "</title>");
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"gtm4wp_category\" value=\"", "\"");
			if(StringUtils.isNotBlank(title)) {
				productName = title + " " +productName ; 	
			}
		}
		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "เปรียบเทียบ</a></div><p class=\"price\"><span class=\"electro-price\">", "<span class=\"woocommerce-Price-currencySymbol\">");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-actions-wrapper\">", "</span></span></span>");
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"electro-price\"><span class=\"woocommerce-Price-amount amount\">", "<span class=\"");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\"><span class=\"electro-price\"><span class=\"woocommerce-Price-amount amount\">", "<span class=\"woocommerce-Price-currencySymbol\">");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] { productPrice };
	}
	
//	public String[] getProductBasePrice(String[] evaluateResult) {
//		
//		String basePrice = "";
//		if (evaluateResult.length > 0) {				
//			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "<span class=\"");
//			basePrice = FilterUtil.toPlainTextString(basePrice);
//			basePrice = FilterUtil.removeCharNotPrice(basePrice);
//
//		}
//		
//		return new String[] { basePrice };
//	}
	
	

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
			productDesc = productDesc.replace("&#8211;", "");
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
			if(productPictureUrl.equals("")) {
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"twitter:image\" content=\"", "\"");
			}
		}
		return new String[] { productPictureUrl };
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
		if (evaluateResult.length == 1) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"gtm4wp_sku\" value=\"", "\"");
		}
		return new String[] { realProductId };
	}
	
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0 && realProductId[0] != "") {
			productName[0] = productName[0] + " (" + realProductId[0]+")";
		}
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		


		rtn[0].setPictureUrl(productPictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);


		return rtn;
	}
}

