package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class YesstyleRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<h1>"});
		map.put("price", 		 new String[]{""});
		map.put("basePrice", 	 new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(evaluateResult!= null && evaluateResult.length > 0) {
			price = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"sellingPrice\"", "</div>");
			price = FilterUtil.getStringAfter(price, "&nbsp;", price);
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
		}
		return new String[] { price };
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult!= null && evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"listPrice\"", "</span>");
			basePrice = FilterUtil.getStringAfter(basePrice, "&nbsp;", basePrice);
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] { basePrice };
	}

	
}

