package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;



public class KareThailandHTMLParser extends DefaultHTMLParser{
			
//	public String getCharset() {
//		return "UTF-8";
//	}	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<div class=\"product-view\">"});
		map.put("basePrice", new String[]{"<div class=\"product-info\">"});
		map.put("description", new String[]{"<div class=\"tab-content\" id=\"tab_description_tabbed_contents\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-img-box col-sm-5\">"});
		map.put("realProductId", new String[]{"<table class=\"data-table\" id=\"product-attribute-specs-table\">"});
		map.put("expire", new String[]{"<p class=\"availability in-stock\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);		
			productName+=" KARE";
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		//"<span class=\"price\">","<p class=\"special-price\">"
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {	
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements cut = doc.select("p.special-price");
    		String cutString = cut.toString();
    		if(StringUtils.isBlank(cutString))
    			cutString = evaluateResult[0];
    		productPrice = FilterUtil.getStringBetween(cutString,"฿","</span>");
        }		
    	productPrice = FilterUtil.removeCharNotPrice(productPrice);
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">ราคาปกติ:</span>", "</p>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, ">", "</span>");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult.length > 0) {
	    	productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "true";
    	if(evaluateResult.length > 0){
    		if (evaluateResult[0].contains("มีสินค้าพร้อมจัดส่ง : <span>ในคลังสินค้า</span></p>")) {
    			pdExpire = "false";
    		}
    	}
    	return new String[] {pdExpire};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if(evaluateResult.length > 0){
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<th class=\"label\">SKU</th>", "</td>");
    		realProductId = FilterUtil.getStringAfter(realProductId, "<td class=\"data\">", "");
    	}
		return new String[] {realProductId};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] baseprice = mpdBean.getProductBasePrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		
		if(name == null || name.length == 0 || price == null || price.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setRealProductId(String.valueOf(realProductId[0]));
		rtn[0].setName(name[0] + "("+realProductId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(price[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(baseprice[0]));
		if(desc != null && desc.length > 0) {
			rtn[0].setDescription(desc[0]);
		}
		if(pictureUrl != null && pictureUrl.length > 0) {
			rtn[0].setPictureUrl(pictureUrl[0]);
		}
		
		return rtn;
	}

	

}