package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;

public class SumonHitechHTMLParser extends TaradHTMLParser{
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1>" });
		map.put("price", new String[] { "" });
		map.put("basePrice", new String[] { "<font class='ProductPrice'>" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>" });
		
		return map;
	}
	
	
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<font class='ProductPrice'>ราคา:  <font class='ProductPricePrice'>","</font>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if(productPrice.equals("")) {				
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		
		return new String[] {productPrice};
	}
	
}
