package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class GreenWaterHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<td align=\"left\" valign=\"top\">"});
		map.put("price", new String[]{"<td align=\"left\" valign=\"top\">"});
		map.put("description", new String[]{"<td align=\"left\" valign=\"top\">"});
		map.put("pictureUrl", new String[]{"<div align=\"center\">"});
		map.put("expire", new String[]{"<span class=\"style11\">"});
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
		if (evaluateResult.length > 0 && evaluateResult[0].indexOf("จ่ายแพงกว่าทำไม !!! สินค้าคุณภาพ ในราคาย่อมเยาว์") != -1) {
			return new String[]{"true"};
		}	
        return new String[]{""};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"style11\">", "</span>");
			productName = FilterUtil.toPlainTextString(productName);	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"style11\">พิเศษ:", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		int priceCheck = 0;
    		if (productPrice.trim().length() > 0) {
    			priceCheck = Integer.parseInt(productPrice);
    		}
    		
    		if ("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0 || priceCheck > 100000) {
				productPrice = "9999999";
			}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}