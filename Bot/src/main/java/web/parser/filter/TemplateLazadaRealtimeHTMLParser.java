package web.parser.filter;

import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateLazadaRealtimeHTMLParser extends TemplateLazadaJSIgnoreMerchantCheckHTMLParser{

	@Override
	public ProductDataBean[] parse(String url) {
		while(url.contains("https://c.lazada.co.th/t/") && url.contains("?url=")) {
			url = FilterUtil.getStringAfter(url, "?url=", url);
		}
		url = BotUtil.decodeURL(url);
		return super.parse(url);
	}
	
}
