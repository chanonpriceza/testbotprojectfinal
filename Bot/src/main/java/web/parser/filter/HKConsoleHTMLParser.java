package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class HKConsoleHTMLParser extends TemplateMakeWebEasyHTMLParser{	
	
	public String getCharset() {
		return "utf-8";
	}
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span itemprop=\"name\">"});
		map.put("price", new String[]{"<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"  border=\"0\">"});
		map.put("basePrice", new String[]{"<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"  border=\"0\">"});
		map.put("description", new String[]{"<span itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
    	//if (evaluateResult.length == 1) {	
    		pdId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"c_id\" id=\"c_id\" value=\"", "\"");
    		pdId = FilterUtil.toPlainTextString(pdId).trim();
    		if(!StringUtils.isNumeric(pdId)){
    			return null;
    		}
        //}
    	return new String[] {pdId};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] pdId = mpdBean.getRealProductId();
		String[] productBaseprice = mpdBean.getProductBasePrice();
		String currentUrl = mpdBean.getCurrentUrl();;

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || productPictureUrl == null || productPictureUrl.length == 0 || pdId == null || pdId.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		rtn[0].setName(pdName + " (" + pdId[0] + ")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productBaseprice != null && productBaseprice.length != 0) {
			try{
			rtn[0].setBasePrice( Double.parseDouble(productBaseprice[0]));
			}catch (Exception e) {
				rtn[0].setBasePrice(0.00);
			}
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		return rtn;
	}

	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {	
    		String tmp = "";
    		if(evaluateResult[0].contains("<b>ราคาพิเศษ :")){
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<b>ราคาพิเศษ :", "</b>");
    		}
    		if(StringUtils.isBlank(tmp)){
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<b>ราคา :", "</b>");
    		}
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		if(productPrice.equals("0.000.00")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.equals("0.00")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.trim().length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1 && evaluateResult[0].contains("<b>ราคาพิเศษ :")) {	
    		String tmp = "";
    		tmp = FilterUtil.getStringBetween(evaluateResult[0], "<b>ราคา :", "</b>");
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString("<div>"+productDesc+"</div>");
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"zoom-img-p\">", "</table>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}