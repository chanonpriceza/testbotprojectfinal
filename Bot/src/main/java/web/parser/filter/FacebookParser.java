package web.parser.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.restfb.types.Photo.Image;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Photo;

import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.FacebookManager;
import web.crawler.bot.FacebookUtil;
import web.parser.DefaultHTMLParser;


public class FacebookParser extends DefaultHTMLParser{
	
	private static final Logger logger = LogManager.getRootLogger();
	
	protected Properties config;
	
	public ProductDataBean[] parse(String url) {
		
		if(url == null) {
			return null;
		}
		try {
			
			FacebookClient facebookClient = new DefaultFacebookClient(FacebookManager.getInstance().getAccessToken(), com.restfb.Version.VERSION_2_7);	
			Photo photo = facebookClient.fetchObject(url, Photo.class, Parameter.with("fields", "name,link,images"));
			
			String name = photo.getName();
			
			ProductDataBean[] rtn = null;
			
			if(name != null && name.contains(FacebookUtil.PZ_PRICE_TAG)) {				
				rtn = parseFacebookPZTag(photo, url);
			} else {
				rtn = new ProductDataBean[1];
    			rtn[0] = new ProductDataBean();
    			rtn[0].setExpire(true);
			}
			
			Thread.sleep(1000);

			return rtn;
		} catch (Exception e){
			logger.error(e);
		}
		
		return null;
	}
	
	
	public ProductDataBean[] parseFacebookPZTag(Photo photo, String url) {
		
		String s = photo.getName();
		List<ProductDataBean> rtn = new ArrayList<ProductDataBean>();
		String[] allLine = s.split("\n");
		String cat = "";
		String name = "";
		String price = "";
		
		for (int i = 0; i < allLine.length; i++) {
			
			String line = allLine[i];
			if(line.contains(FacebookUtil.PZ_PRICE_TAG)) {
				
				price = FacebookUtil.parseTag(s, FacebookUtil.PZ_PRICE_TAG);
				price = FilterUtil.removeCharNotPrice(price);
				
				if(name.trim().length() == 0) {
					name = photo.getName();
				}
				name = FilterUtil.removeSpace(name) + " (" + url + ")";
				
				if(price.equals("0")) {
					price = BotUtil.CONTACT_PRICE_STR;
				}

				String image = null;
				List<Image> listImage = photo.getImages();
				if(listImage != null && !listImage.isEmpty()){
					Image objImage = listImage.get(0);
					if(objImage != null){
						image = objImage.getSource();
						if(StringUtils.isBlank(image)){
							image = null;
						}
					}
				}
				
				ProductDataBean p = new ProductDataBean();
				
				p.setName(name);			
				p.setDescription(FilterUtil.removeSpace(photo.getName()));
				p.setPrice(FilterUtil.convertPriceStr(price));			
				p.setPictureUrl(image);
				p.setUrl(photo.getLink());
				p.setCategoryId(BotUtil.stringToInt(cat, 0));
				
				rtn.add(p);
				
				price = "";
				name = "";
				cat = "";
			} else if(line.contains(FacebookUtil.PZ_CAT_TAG)) {				
				cat = FacebookUtil.parseTag(s, FacebookUtil.PZ_CAT_TAG);
			} else if(line.contains(FacebookUtil.PZ_NAME_TAG)) {				
				name = FacebookUtil.parseTagWithSpace(s, FacebookUtil.PZ_NAME_TAG);
			}
		}
		
		return rtn.toArray(new ProductDataBean[0]);
	}
}
