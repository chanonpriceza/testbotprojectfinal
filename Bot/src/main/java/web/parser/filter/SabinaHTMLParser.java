package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SabinaHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-detail\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"product-detail\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span style=\"font-size:16px; font-weight:bold;\">", "</span>");
		    productName = FilterUtil.toPlainTextString(productName);	
					
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		List<String> productPrices = FilterUtil.getAllStringBetween(evaluateResult[0], "<td><strong>Price</strong> :</td>", "฿");
    		if(productPrices != null && productPrices.size() > 0){
    			productPrice = FilterUtil.toPlainTextString(productPrices.get(0));
    			productPrice = FilterUtil.removeCharNotPrice(productPrices.get(0));
    			for(String price : productPrices){
        			price = FilterUtil.toPlainTextString(price);
        			price = FilterUtil.removeCharNotPrice(price);
        			if(Double.valueOf(price) < Double.valueOf(productPrice)){
        				productPrice = price;
        			}
        		}
    		}
        }	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		List<String> productDescs = FilterUtil.getAllStringBetween(evaluateResult[0], "<td><strong>Description</strong> :</td>", "</tr");
    		if(productDescs != null && productDescs.size() > 0){
    			int descSize = productDescs.size();
    			if(descSize == 1){
    				productDesc = FilterUtil.toPlainTextString(productDescs.get(0));
  
    			}else if(descSize > 1){
    				productDesc = FilterUtil.toPlainTextString(productDescs.get(1));
    			}
    		}
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"clearfix\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if (evaluateResult.length == 1) {
    		pdId = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:url\"", "<meta property=\"og:image\"");
    		pdId = FilterUtil.getStringBetween(pdId, "&id=", "\"");
    		pdId = FilterUtil.toPlainTextString(pdId);
        }
		return new String[] { pdId};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (StringUtils.isBlank(productName[0])|| StringUtils.isBlank(productPrice[0])|| StringUtils.isBlank(realProductId[0])) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
	
		rtn[0].setName(productName[0] + " ("+realProductId[0]+ ")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		if ((productDescription != null && productDescription.length != 0) ) {
			rtn[0].setDescription(productDescription[0]);
		}

		return rtn;
	}
}
