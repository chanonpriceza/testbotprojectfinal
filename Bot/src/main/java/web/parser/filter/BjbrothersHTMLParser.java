package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BjbrothersHTMLParser extends DefaultHTMLParser{	

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	map.put("name", new String[]{"<div class=\"col-sm-4\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"item\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   		
		if(evaluateResult.length == 1) {
			String productName1 = FilterUtil.getStringBetween(evaluateResult[0], "<h3 class=\"m0\">", "</h3>");
			String productName2 = FilterUtil.getStringBetween(evaluateResult[0], ">ยี่ห้อ", "</tr>");
			String productName3 = FilterUtil.getStringBetween(evaluateResult[0], ">รหัส", "</tr>");

			
			if(StringUtils.isNoneBlank(productName1)){
				productName = FilterUtil.toPlainTextString(productName1);
				if(StringUtils.isNoneBlank(productName2)){
					productName2 = FilterUtil.getStringBetween(productName2, "<td style=\"font-size:1em\">", "</td>");
					if(StringUtils.isNoneBlank(productName2)){
						productName = productName+" "+ FilterUtil.toPlainTextString(productName2);
					}
				
				}
				if(StringUtils.isNoneBlank(productName3)){
					productName3 = FilterUtil.getStringBetween(productName3, "<td style=\"font-size:1em\">", "</td>");
					if(StringUtils.isNoneBlank(productName3)){
						productName = productName+" " + FilterUtil.toPlainTextString(productName3);
					}
					
				}
				
			}
			
			productName = FilterUtil.toPlainTextString(productName);		
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = BotUtil.CONTACT_PRICE_STR; 
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = FilterUtil.getStringAfter(productDesc, "DESCRIPTION", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = productPictureUrl.replace("&amp;", "&");
    		
        }
    	return new String[] {productPictureUrl};
	}
	
}