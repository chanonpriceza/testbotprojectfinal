package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class BanmorHTMLParser extends IndividualLogHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("basePrice", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    
    	if(evaluateResult.length>0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("div.summary");
    		String sale_price = e.select("ins").html();
    		if(StringUtils.isBlank(sale_price)) {
        		e = e.select("span.woocommerce-Price-amount");
        		productPrice = FilterUtil.removeCharNotPrice(e.html());
        		return new String[] {productPrice};
    		}
    		productPrice = FilterUtil.removeCharNotPrice(sale_price);
    	}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:description\" content=\"","\"");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:image\" content=\"","\"");
        }
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    
    	if(evaluateResult.length>0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("div.summary");
    		String sale_price = e.select("del").html();
    		productPrice = FilterUtil.removeCharNotPrice(sale_price);
    	}
		return new String[] {productPrice};
	}
	
}
