package web.parser.filter;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class MelinHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<div class=\"summary entry-summary\">"});
		map.put("basePrice", new String[]{"<span class=\"market-price\">"});
		map.put("description", new String[]{"<div class=\"woocommerce-tabs wc-tabs-wrapper\">"});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
 
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = "Meilin " + productName;
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\">", "</p>");
    		productPrice = StringEscapeUtils.unescapeHtml4(productPrice);
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "</p>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div data-thumb=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
}
