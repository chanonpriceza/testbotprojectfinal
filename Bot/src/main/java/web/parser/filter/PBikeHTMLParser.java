package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;

public class PBikeHTMLParser extends ReadyPlanetHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<div style='margin:8px 0 8px 0;'>" });
		map.put("price", new String[] { "<table width='100%' cellpadding='5' cellspacing='0' border='0'>", "<p style=\"text-align: center;\">" });
		map.put("basePrice", new String[] { "<table width='100%' cellpadding='5' cellspacing='0' border='0'>", "<p style=\"text-align: center;\">" });
		map.put("description", new String[] { "<table width='100%' cellpadding='5' cellspacing='0' border='0'>", "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>" });
		map.put("pictureUrl", new String[] { "<table width='100%' cellpadding='5' cellspacing='0' border='0'>", "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>" });
		map.put("expire", new String[] { "" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if (evaluateResult.length > 1) {
			String brand = FilterUtil.getStringAfterLastIndex(evaluateResult[1], "</span>", "");
			String model = "";
			if (evaluateResult.length > 2) {
				model = FilterUtil.getStringAfterLastIndex(evaluateResult[2], "</span>", "");
			} else {
				model = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "</span>", "");
			}
			brand = FilterUtil.toPlainTextString(brand);
			model = FilterUtil.toPlainTextString(model);
			productName = brand + " " + model;
		}

		return new String[] { productName };
	}

}
