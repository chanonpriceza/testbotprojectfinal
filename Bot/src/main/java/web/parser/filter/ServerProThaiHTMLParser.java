package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ServerProThaiHTMLParser extends DefaultHTMLParser {
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("basePrice", new String[]{"<p class=\"price\">"});
		map.put("description", new String[]{"<div class=\"electro-description\">"});
		map.put("pictureUrl", new String[]{"<figure class=\"woocommerce-product-gallery__wrapper\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";  
    	if (evaluateResult.length > 0) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "</ins>");
    		if(tmp.isEmpty()){
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "</p>");
    		}
    		tmp = FilterUtil.getStringAfter(tmp, "</span>", "");
    		if(tmp.isEmpty()){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}else{
    			productPrice = FilterUtil.toPlainTextString(tmp);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    	}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";    	
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"woocommerce-Price-amount amount\">", "</del>");
			bPrice = FilterUtil.getStringAfter(bPrice, "</span>", "");
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
		    
}