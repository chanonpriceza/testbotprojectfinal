package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ITCheapHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<p class=\"f_l\">"});
		map.put("price", new String[]{"<font class=\"shop\" id=\"ECS_SHOPPRICE\">"});
		map.put("description", new String[]{"<body>"});
		map.put("pictureUrl", new String[]{"<div class=\"imgInfo\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawDesc = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดของสินค้า:", "สินค้าติดป้าย/ติดป้ายสินค้า");
    		rawDesc = rawDesc.replace("รายการสินค้า", "");
    		rawDesc = rawDesc.replace("Functions", "");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDesc+"</div>");
    		productDesc = FilterUtil.getStringAfter(productDesc, "เงื่อนไขการรับประกันสินค้า", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult[0].contains("สินค้าหมดจากรายการ")) {
    		return new String[]{"true"};
    	}    	
        return new String[]{""};
	}

}