package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class CheeroHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1 class=\"product_title-cheero\">"});
			map.put("price", new String[]{"<span class=\"price-sale\">"});
			map.put("basePrice", new String[]{"<s>"});
			map.put("description", new String[]{"<div class=\"top-description\">"});
			map.put("pictureUrl", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";  
			if(evaluateResult.length == 1) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
				productName = StringEscapeUtils.unescapeXml(productName);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		productPrice = StringEscapeUtils.unescapeXml(evaluateResult[0]);
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }	    
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {	
	    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String basePrice = "";    	
	    	if (evaluateResult.length == 1) {
	    		basePrice = StringEscapeUtils.unescapeXml(evaluateResult[0]);
	    		basePrice = FilterUtil.toPlainTextString(basePrice);
	    		basePrice = FilterUtil.removeCharNotPrice(basePrice);
	        }	    	
			return new String[] {basePrice};
		}
}
