package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;


public class CRDsolutionHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"name_th\">"});
		map.put("price", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{"<div class=\"tab-pane active\" id=\"tab1\">"});
		map.put("pictureUrl", new String[]{"<ul class=\"thumbnails\">"});
		
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "style=\"font-size:18px\">", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	if(productPrice.equals("") || productPrice.equals("0.0")) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src='", "'");
        }
    	
    	return new String[] {productPictureUrl};
	}
	    
}