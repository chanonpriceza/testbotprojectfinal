package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CleanWorldHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "tis-620";
	}	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=center>"});
		map.put("price", new String[]{"<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"});
		map.put("basePrice", new String[]{"<span class='grey arial9'>"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=center>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			String rawProductName = FilterUtil.getStringBetween(evaluateResult[0], "<span class='arial13'>", "<br>");
			productName = FilterUtil.toPlainTextString(rawProductName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = ""; 
		String rawProductPrice = "";
    	if (evaluateResult.length == 1) {
    		rawProductPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=red>", "</span>");
    		if(rawProductPrice.length() == 0){
    			rawProductPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='grey arial9'>", "</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(rawProductPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.equals("0.00")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length == 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);	    			
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		String rawProductDesc = FilterUtil.getStringBetween(evaluateResult[0], "Description :", "</td>");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawProductDesc+"</div>");	
    	}

    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawProductName = FilterUtil.getStringBetween(evaluateResult[0], "<span class='arial13'>", "</td></tr><tr><td valign='top'>");
    		productPictureUrl = FilterUtil.getStringBetween(rawProductName, "src='", "'");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	/**
	 * default mergeProductData
	 * 
	 * 
	 */
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] bPrice = mpdBean.getProductBasePrice();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		//rtn[0].setName(productName[0]);
		//http://www.cleanworld-th.com/view_product.php?product=92&cat_id=61&page=1&pg=product.php
		String productId = FilterUtil.getStringBetween(currentUrl, "product=", "&cat_id");
		if(productId.trim().length() != 0) {
			rtn[0].setName(productName[0] + " " + productId.trim());
		} else {
			rtn[0].setName(productName[0]);
		}
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		
		if(bPrice != null && bPrice.length == 1){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
		}

		return rtn;
	}
	    
}