package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class NESCAFEDolceGustoHTMLParser extends DefaultHTMLParser {	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", 			new String[]{"<div class=\"product-name\">"});
		map.put("price", 			new String[]{""});
		map.put("basePrice", 		new String[]{""});
		map.put("description", 		new String[]{"<div class=\"std\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"product-image-gallery\">"});
		map.put("realProductId", 	new String[]{""});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"add-to-cart-wrapper\">", "</div>");
		productPrice = FilterUtil.getStringAfter(productPrice, "<p class=\"special-price\">", productPrice);
		productPrice = FilterUtil.toPlainTextString(productPrice);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"add-to-cart-wrapper\">", "</div>");
		productBasePrice = FilterUtil.getStringBetween(productBasePrice, "<p class=\"old-price\">", "</p>");
		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[1]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
    	if (evaluateResult.length == 1) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"");
        }
    	return new String[] {realProductId};
	} 
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
			
		
		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName("NESCAFE "+productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setRealProductId(realProductId[0]);

		if (productDescription != null && productDescription.length != 0)
			rtn[0].setDescription(productDescription[0]);
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if(productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {
						
					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		return rtn;

	}    
	   
}