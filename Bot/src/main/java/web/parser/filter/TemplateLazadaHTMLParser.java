package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TemplateLazadaHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"pdp-product-price\">"});
		map.put("description", new String[]{"<div id=\"module_product_detail\" class=\"pdp-block module\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"origin-block\">"});
		map.put("realProductId", new String[]{""});
		map.put("expire",  new String[] {""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {
			String vendor = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"seller-name\">", "</a>");
			vendor = FilterUtil.getStringAfterLastIndex(vendor,">","");
			vendor = FilterUtil.toPlainTextString(vendor);
			vendor = StringEscapeUtils.unescapeHtml4(vendor);
				
			if(vendor.equals(BaseConfig.CONF_MERCHANT_NAME)) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"pdp-product-title\">", "</h1>");
				productName = FilterUtil.toPlainTextString(productName);
			}
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	
		if (evaluateResult.length > 0) {
			if (evaluateResult[0].indexOf("<div class=\"origin-block\">") > -1) {
				productPrice = FilterUtil.getStringBefore(evaluateResult[0], "<div class=\"origin-block\">", "");
			} else {
				productPrice = evaluateResult[0];
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice); 
        }
		    
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";
    	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBefore(evaluateResult[0], "</span>", "");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }
		    
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(evaluateResult.length > 0){
			pdId = FilterUtil.getStringBetween(evaluateResult[0], "configId=", "&");
		}
		return new String[] {pdId};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expireText = FilterUtil.getStringBetween(evaluateResult[0], "quantity-content-warning", "</span>");
		if(expireText.contains("สินค้าหมด")) {
			return new String[] {"true"};
		}
		return new String[] {""};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] realProductId = mpdBean.getRealProductId();
        
    	if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		currentUrl  =  FilterUtil.getStringBefore(currentUrl, "?", currentUrl);
		rtn[0].setUrl("https://c.lazada.co.th/t/c.PVm?url="+ URLEncoder.encode(generateURL(currentUrl), Charset.defaultCharset()));
		rtn[0].setUrlForUpdate(currentUrl);

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {			
			if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {				
				if(productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {
						
					}
				}
			}
		}
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}		
		
		return rtn;
	}
    
	public static String generateURL(String url) {
		String result = "";
		if(!url.contains("/products/")) {
			String main = "https://www.lazada.co.th";
			String surname = FilterUtil.getStringAfterLastIndex(url,"/", url);
			result = main+"/products/"+surname;
			return result;
		}
		return url;
	}
	    
}
