package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import engine.BaseConfig;
import utils.FilterUtil;

public class SkinfoodThailandInLazadaHTMLParser extends TemplateLazadaHTMLParser{
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("expire",  new String[] {""});
		
		return map;
	}
	
public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {
			String vendor = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"seller-name\">", "</a>");
			vendor = FilterUtil.getStringAfterLastIndex(vendor,">","");
			vendor = FilterUtil.toPlainTextString(vendor);
			vendor = StringEscapeUtils.unescapeHtml4(vendor);
			
			if(vendor.equals("")) {
				vendor = FilterUtil.getStringBetween(evaluateResult[0], "\"seller_name\":\"", "\",");
			}

			if(vendor.equals(BaseConfig.CONF_MERCHANT_NAME)) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "\"pdt_name\":\"", "\"");
				productName = FilterUtil.toPlainTextString(productName);
			}
		}
		return new String[] {productName};
	}

	public String[] getProductPrice(String[] evaluateResult) {
	
		String productPrice = "";
		
		if (evaluateResult.length > 0) {
			if(evaluateResult[0].contains("\"salePrice\":{\"text")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "salePrice\":{\"text\":\"", "\"");
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice); 	
			}else {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "\"pdt_price\":\"", "\"");
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice); 				
			}
	    }
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";
    	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "\"pdt_price\":\"", "\"");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice); 	
        }
		    
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
	    	if(evaluateResult.length > 0) {
	    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "\"Product\",\"description\":\"", "\"");
	    		productDesc = FilterUtil.toPlainTextString(productDesc);
	    		productDesc = StringEscapeUtils.unescapeJava(productDesc);
	    		
	    	}
	    	return new String[] {productDesc};
		}
	}
