package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class JamsaiHTMLParser extends BasicHTMLParser{
			
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("expire", new String[]{""});
			map.put("upc", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";   
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:title]");
				productName = FilterUtil.toPlainTextString(e.attr("content"));			
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements a = doc.select("div.info");
				a = a.select("tr");
				Element aselect= a.stream().filter(x -> x.html().contains("ราคาสมาชิก")).findAny().orElse(a.last());
				Elements priceTR = aselect.select("td");
				Element p =  priceTR.stream().filter(x -> x.html().contains("บาท")).findAny().orElse(a.last());
				productPrice = FilterUtil.toPlainTextString(p.html());
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements a = doc.select("div.info");
				a = a.select("tr");
				Element aselect= a.stream().filter(x -> x.html().contains("ราคา")).findAny().orElse(a.last());
				Elements priceTR = aselect.select("td");
				Element p =  priceTR.stream().filter(x -> x.html().contains("บาท")).findAny().orElse(a.last());
				productPrice = FilterUtil.toPlainTextString(p.html());
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductDescription(String[] evaluateResult) {
			String desc = ""; 
			String author = "";
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements a = doc.select("div.info");
				a = a.select("tr");
				Element aselect= a.stream().filter(x -> x.html().contains("ผู้แต่ง")).findAny().orElse(a.last());
				aselect = aselect.select("a").size()>0?aselect.select("a").get(0):a.last();
				author = aselect.attr("title");
				author = author.length() > 0?"ผู้แต่ง: "+author:"";
				Elements d = doc.select("div.tab_description");
				d = d.select("div.detail");
				desc = d.html();
				desc = FilterUtil.toPlainTextString(desc);
	
			}	
			return new String[] {author+" สนพ. แจ่มใส "+desc};
		}

		@Override
		public String[] getProductUpc(String[] evaluateResult) {
			String real = "";
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements a = doc.select("div.info");
				a = a.select("tr");
				Element aselect= a.stream().filter(x -> x.html().contains("ISBN")).findAny().orElse(a.last());
				Elements isbn = aselect.select("td");
				isbn = isbn.select("font");
				real = isbn.html();
				real = FilterUtil.removeSpace(real);
	
			}
			return  new String[] {real};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String real = "";
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements a = doc.select("input#Product_comment_pid");
				real = FilterUtil.removeSpace(a.attr("value"));
			}
			return  new String[] {real};
		}
		
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

	    	String[] productName = mpdBean.getProductName();
	    	String[] productPrice = mpdBean.getProductPrice();
	    	//String[] productUrl = mpdBean.getProductUrl();
	        String[] productDescription = mpdBean.getProductDescription();
	        String[] productPictureUrl = mpdBean.getProductPictureUrl();
	        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
	        String[] realProductId = mpdBean.getRealProductId();
	        String currentUrl = mpdBean.getCurrentUrl();
	        String[] productBasePrice = mpdBean.getProductBasePrice();
	        String[] productUpc = mpdBean.getProductUpc();
	        String tmp = "";

	    	if (isEmpty(productName) || isEmpty(productPrice))
				return null;
	    	
	    	if(mixIdName&&realProductId!=null&&realProductId.length>0)
	    		tmp = " ("+realProductId[0]+")";
	    	
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0]+tmp);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (isNotEmpty(productDescription))
				rtn[0].setDescription(productDescription[0]);
			
			if (isNotEmpty(productPictureUrl)) {
				String pictureUrl = productPictureUrl[0];
				if(isNotBlank(pictureUrl)) {				
					if(pictureUrl.startsWith("http")) {
						rtn[0].setPictureUrl(pictureUrl);					
					} else {
						try {
							URL url = new URL(new URL(currentUrl), pictureUrl);
							rtn[0].setPictureUrl(url.toString());
				    	} catch (MalformedURLException e) {}
					}
				}
			}

			if (isNotEmpty(merchantUpdateDate)) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null)
					rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
			
			if (isNotEmpty(productUpc)&&isNotBlank(productUpc[0])) {
				rtn[0].setUpc(productUpc[0]);
				rtn[0].setName("หนังสือ "+productName[0]);
			}
			
			if (isNotEmpty(realProductId)&&isNotBlank(realProductId[0])) {
				rtn[0].setRealProductId(realProductId[0]);
			}
			
			if (isNotEmpty(productBasePrice))
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			
			if(rtn!=null && rtn[0].getBasePrice() < rtn[0].getPrice()) {
				rtn[0].setPrice(rtn[0].getBasePrice());
			}
			
			return rtn;
		}
		
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			boolean expire = false ;   
			if(evaluateResult.length == 1) {	
					Document doc = Jsoup.parse(evaluateResult[0]);
					Elements e = doc.select("div.producticon");
					e = e.select("a.out");
					expire = isNotBlank(e.toString());
				}
				return new String[] {String.valueOf(expire)};
		}
		

}