package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ItsskinthailandHTMLParser extends DefaultHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price",			new String[]{"<p class=\"price\">"});
		map.put("basePrice", 		new String[]{"<p class=\"price\">"});
		map.put("description", 		new String[]{"<div class=\"woocommerce-product-details__short-description\">"});
		map.put("pictureUrl", 		new String[]{""});
		map.put("expire", 			new String[]{"<p class=\"stock out-of-stock\">"});
		map.put("realProductId", 	new String[]{"<span class=\"sku\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = "	It's Skin "+ productName;
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length > 0) {	
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "</ins>");
    			productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
    			if(productPrice.equals("")) {
    				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">", "<span class=\"woocommerce-Price-amount amount\">");
    				productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
    			}
    			if(productPrice.equals("")) {
    				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">", "</p>");
    				productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
    			}
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    			if(productPrice.equals("")) {
    				productPrice = BotUtil.CONTACT_PRICE_STR;
    			}
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length > 0) {	
    			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"woocommerce-Price-amount amount\">", "</del>");
    			bPrice = FilterUtil.getStringBetween(bPrice, "</span>", "</span>");
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a  class=\"venobox \" href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "false";
    	if (evaluateResult.length > 0) {
    		if (evaluateResult[0].contains("Sold Out")) {
    			pdExpire = "true";
    		}
        }
    	return new String[] {pdExpire};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(evaluateResult.length == 1){
			pdId = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(pdId.equals("N/A")) {
				pdId = "";
			}
		}
		return new String[] {pdId};
	}
	
	

}

