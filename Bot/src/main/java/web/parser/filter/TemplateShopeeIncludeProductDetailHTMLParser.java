package web.parser.filter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.DefaultHTMLParser;

public class TemplateShopeeIncludeProductDetailHTMLParser extends DefaultHTMLParser{
	
	public static final String DATE_FORMAT = "yyyy.MM.dd";
	private static Logger logger = LogManager.getRootLogger();
	
	@Override
	public ProductDataBean[] parse(String url) {
		String shopId = "";
		String itemId = "";

		if(url.contains("/universal-link/product/")) {
			String tmp = FilterUtil.getStringBetween(url, "/universal-link/product/", "?");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
		} else if (url.contains("/universal-link/")){
			String tmp = FilterUtil.getStringBetween(url, "-i.", "?");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
		} else if (url.contains("/product/")) {
			String tmp = FilterUtil.getStringAfter(url, "/product/", "");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		} else {
			String tmp = FilterUtil.getStringAfter(url, "-i.", "");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		}
		
		if(StringUtils.isNotBlank(shopId) && StringUtils.isNotBlank(itemId)) {
			String newUrl = "https://shopee.co.th/api/v2/item/get?itemid=" + itemId + "&shopid=" + shopId;
			return super.parse(newUrl);
		}
				
		return super.parse(url);
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 	new String[]{""});
		map.put("price", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String json = mpdBean.getProductName()[0];
    	String link = mpdBean.getCurrentUrl();
    	String itemId = FilterUtil.getStringBetween(link, "?itemid=", "&");
    	String shopId = FilterUtil.getStringAfter(link, "&shopid=", "");
    	
    	if(StringUtils.isBlank(itemId) || StringUtils.isBlank(shopId)) return null;
    	
    	try {
    		    		
    		String shopName = "";
    		String shopScore = "";
    		String soldCount = "";
    		String isFreeShipping = "";
    		String isOfficialShop = "false";
    		JSONArray promotions = new JSONArray();
    		Set<String> shopNameSet = new HashSet<>();
    		
			JSONObject obj = (JSONObject) new JSONParser().parse(json);
			if(obj == null) return null;
			
			JSONObject itemObj = (JSONObject) obj.get("item");
			if(itemObj == null) return null;
			
			// name and price
			String tempPrice = StringUtils.defaultString(String.valueOf(itemObj.get("price")), "");
			String resultName = StringUtils.defaultString(String.valueOf(itemObj.get("name")), "") + "(" + itemId + ")";
			String resultPrice = "0";

			if(NumberUtils.isCreatable(tempPrice)) {
				int priceFull = Integer.parseInt(tempPrice);
				int priceReal = priceFull / 100000;
				if(priceReal > 0) resultPrice = String.valueOf(priceReal);
			}
			
			// official check
			isOfficialShop = StringUtils.defaultString(String.valueOf(itemObj.get("is_official_shop")), "false");
			
			// sold count
			soldCount = StringUtils.defaultString(String.valueOf(itemObj.get("historical_sold")), "");
			
			// shipping check
			// detect location
			// api = https://shopee.co.th/api/v0/shop/180596561/item/2872893227/shipping_info_to_address/?city=เขตพระนคร&district=&state=จังหวัดกรุงเทพมหานคร
			
			// promotion
			String promotionAPILink = "https://shopee.co.th/api/v2/voucher_wallet/get_shop_vouchers_by_shopid?shopid="+shopId+"&with_claiming_status=false";
			String[] promotionAPIResponse = HTTPUtil.httpRequestWithStatus(promotionAPILink, "UTF-8", false);
			if(promotionAPIResponse != null && promotionAPIResponse.length == 2) {
				String promotionData = promotionAPIResponse[0];
				if(StringUtils.isNotBlank(promotionData)) {
					try {
						JSONObject promotionObj = (JSONObject) new JSONParser().parse(promotionData);
						JSONObject dataObj = (JSONObject) promotionObj.get("data");
						if(dataObj != null) {
							JSONArray voucherList = (JSONArray) dataObj.get("voucher_list");
							if(voucherList != null && voucherList.size() > 0) {
								for (Object object : voucherList) {
									try {
										JSONObject promotion = new JSONObject();
										JSONObject voucher = (JSONObject) object;
										String minSpend = StringUtils.defaultString(String.valueOf(voucher.get("min_spend")), "");
										String discountValue = StringUtils.defaultString(String.valueOf(voucher.get("discount_value")), "");
										String discountPercentage = StringUtils.defaultString(String.valueOf(voucher.get("discount_percentage")), "");
										String startTime = StringUtils.defaultString(String.valueOf(voucher.get("start_time")), "");
										String endTime = StringUtils.defaultString(String.valueOf(voucher.get("end_time")), "");
										String code = StringUtils.defaultString(String.valueOf(voucher.get("voucher_code")), "");
										String iconText = StringUtils.defaultString(String.valueOf(voucher.get("icon_text")), "");
										
										if(StringUtils.isNotBlank(iconText)) {
											shopNameSet.add(iconText);
										}
										
										String promotionName = "";
										String promotionDesc = "";
										if(StringUtils.isNotBlank(code)) {
											promotionName = "ใช้โค้ด " + code;
											promotionDesc += "ใช้โค้ด " + code;
										}
										if(NumberUtils.isCreatable(discountValue)) {
											String discountText = "";
											long discountLong = Integer.parseInt(discountValue);
											long discountReal = discountLong / 100000;
											if(discountReal > 0) discountText = String.valueOf(discountReal);
											if(StringUtils.isNotBlank(discountText)) {
												promotionName = "ลด ฿" + discountText;
												promotionDesc += " ลดทันที ฿" + discountText;
											}
										}
										if(NumberUtils.isCreatable(discountPercentage)) {
											String discountText = "";
											long discountLong = Integer.parseInt(discountPercentage);
											long discountReal = discountLong;
											if(discountReal > 0) discountText = String.valueOf(discountReal);
											if(StringUtils.isNotBlank(discountText)) {
												promotionName = "ลด " + discountText +"%";
												promotionDesc += " รับส่วนลด " + discountText + "%";
											}
										}
										if(NumberUtils.isCreatable(minSpend)) {
											String minSpendText = "";
											long minSpendLong = Integer.parseInt(minSpend);
											long minSpendReal = minSpendLong / 100000;
											if(minSpendReal > 0) minSpendText = String.valueOf(minSpendReal);
											if(StringUtils.isNotBlank(minSpendText)) {
												promotionDesc += " เมื่อซื้อขั้นต่ำ ฿" + minSpendText;
											} else {
												promotionDesc += " ไม่มีขั้นต่ำ";
											}
										}
										
										if(StringUtils.isNotBlank(promotionName)) {
											promotion.put("title", promotionName);
											promotion.put("description", promotionDesc);
											if(StringUtils.isNotBlank(startTime) && NumberUtils.isCreatable(startTime)) {
												Calendar cal = Calendar.getInstance();
												cal.setTimeInMillis(Long.parseLong(startTime)* 1000);
												String startDate = new SimpleDateFormat(DATE_FORMAT).format(cal.getTime());
												if(StringUtils.isNotBlank(startDate)) {
													promotion.put("startDate", startDate);
												}
											}
											if(StringUtils.isNotBlank(endTime) && NumberUtils.isCreatable(endTime)) {
												Calendar cal = Calendar.getInstance();
												cal.setTimeInMillis(Long.parseLong(endTime)* 1000);
												String endDate = new SimpleDateFormat(DATE_FORMAT).format(cal.getTime());
												if(StringUtils.isNotBlank(endDate)) {
													promotion.put("endDate", endDate);
												}
											}
											promotions.add(promotion);
										}
									} catch(Exception e) {
										logger.error("Cannot parse promotion obj", e);
									}
								}
							}
						}
					} catch(Exception e) {
						logger.error("Cannot get promotion from shopid.itemid : " + shopId + "." + itemId);
					}
				}
			}
			
			// check shop name
			if(shopNameSet.size() == 1) {
				for (String name : shopNameSet) {
					shopName = name;
				}
			}else {
				String shopDetailAPILink = "https://shopee.co.th/api/v2/shop/get?is_brief=1&shopid=" + shopId;
				String[] shopDetailAPIResponse = HTTPUtil.httpRequestWithStatus(shopDetailAPILink, "UTF-8", false);
				if(shopDetailAPIResponse != null && shopDetailAPIResponse.length == 2) {
					String shopDetailData = shopDetailAPIResponse[0];
					if(StringUtils.isNotBlank(shopDetailData)) {
						try {
							JSONObject promotionObj = (JSONObject) new JSONParser().parse(shopDetailData);
							JSONObject dataObj = (JSONObject) promotionObj.get("data");
							if(dataObj != null) {
								isOfficialShop = StringUtils.defaultString(String.valueOf(dataObj.get("is_official_shop")), "false");
								shopName = StringUtils.defaultString(String.valueOf(dataObj.get("name")), "");
								shopScore = StringUtils.defaultString((String) dataObj.get("rating_star"), "");
								shopScore = validateShopScore(shopScore);
							}
						} catch(Exception e) {
							logger.error("Cannot get parse shopDetail data", e);
						}
					}
				}
			}
			
			// setting bean
			ProductDataBean resultBean = new ProductDataBean();
			resultBean.setPrice(FilterUtil.convertPriceStr(resultPrice));
			resultBean.setName(resultName);
			
			JSONObject dynamicField = new JSONObject();
			JSONObject productDetail = new JSONObject();
			if(StringUtils.isNotBlank(shopName)) 					productDetail.put("shopName", shopName);
			if(StringUtils.isNotBlank(shopScore)) 					productDetail.put("shopScore", shopScore);
			if(StringUtils.isNotBlank(soldCount)) 					productDetail.put("soldCount", soldCount);
			if(StringUtils.isNotBlank(isFreeShipping)) 				productDetail.put("isFreeShipping", Boolean.parseBoolean(isFreeShipping));
			if(StringUtils.isNotBlank(isOfficialShop)) 				productDetail.put("isOfficialShop", Boolean.parseBoolean(isOfficialShop));
			if(promotions != null && promotions.size()> 0) 			productDetail.put("promotions", promotions);
			if(productDetail != null && productDetail.size() > 0) 	dynamicField.put("productDetail", productDetail);
			resultBean.setDynamicField(dynamicField.toJSONString());
			
			return new ProductDataBean[] {resultBean};
			
		} catch (Exception e) {
			logger.error(e);
		}
    	return null;
	}
	 
	private String validateShopScore(String input) {
		if(StringUtils.isNotBlank(input) && NumberUtils.isCreatable(input)) {
			return String.valueOf((new DecimalFormat("#######.0").format(Double.parseDouble(input))));
		}
		return null;
	}
	
}
