package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;

public class WarosecosmeticsHTMLParser extends TemplateMakeWebEasyHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">","<div class=\"productItemDetail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});

	//	map.put("expire", new String[]{"<div class=\"product_comingsoon warningBox\" v-bind:class=\"'product_'+show_pdata.status.sell\" itemprop=\"availability\" content=\"out_of_stock\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = "Warose "+productName;
		}
		return new String[] {productName};
	}
}
