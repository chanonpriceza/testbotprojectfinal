package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ProjectorProHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"left-product\">"});
		map.put("price", new String[]{"<div class=\"left-product\">"});
		map.put("basePrice"		, new String[]{"<div class=\"left-product\">"});
		map.put("description", new String[]{"<div class=\"left-product\">"});
		map.put("pictureUrl", new String[]{"<div class=\"show-product-pic\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"", "/span>");  
			productName = FilterUtil.getStringBetween(productName, ">", "<");  
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"show-sprice\">", "</span>");  
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);
        	if(productPrice.equals("")){
        		productPrice = BotUtil.CONTACT_PRICE_STR;
        	}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"nprice\">", "</span>");   
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div style=\"height:10px;\">", "<div style=");  
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");    		
        }
    	return new String[] {productPictureUrl};
	}
	    
}