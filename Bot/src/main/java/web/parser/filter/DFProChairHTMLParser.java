package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class DFProChairHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{"<div class=\"product_price\">"});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("description", new String[]{"<ul class=\"elementor-icon-list-items\">"});
		map.put("pictureUrl", new String[]{"<div class=\"elementor-main-swiper swiper-container\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "<title>");
			productName = FilterUtil.getStringBetween(productName, ">", "</");
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
		
		return new String[] {productName};
	}
	
		
		public String[] getProductPrice(String[] evaluateResult) {
                    
			String productPrice = "";   
		 if (evaluateResult.length > 0) { 
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product_price\">", "</div>");
	    		productPrice = FilterUtil.getStringBetween(productPrice, ">", "</");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 	
		        }	
	         
			return new String[] {productPrice};
		}
	

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
  
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\" data-elementor");	
        }
 
    	
    	return new String[] {productPictureUrl};
	}
}
