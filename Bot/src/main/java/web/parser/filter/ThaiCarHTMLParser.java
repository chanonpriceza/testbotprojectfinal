package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class ThaiCarHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<span class=\"title\" itemprop=\"name\">"});
			map.put("price", new String[]{"<div class=\"post-highlight\">"});
			map.put("description", new String[]{"<div id=\"single-post-detail\" class=\"single-section naked\">", "<span itemprop=\"addressLocality\">"});
			map.put("pictureUrl", new String[]{"<div id=\"slider\" class=\"flexslider\">"});
			map.put("realProductId", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";  
			if(evaluateResult.length == 1) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}

		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\">", "</p>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    		if(productPrice.isEmpty()){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}
	        }	    	
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
			int size = evaluateResult.length;
	    	if(size > 0) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
	    		if(size == 2){
	    			productDesc = FilterUtil.toPlainTextString(evaluateResult[1]) + " " + productDesc;
	    		}
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {	
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	    		if(productPictureUrl.contains("?")){
	    			productPictureUrl = FilterUtil.getStringBefore(productPictureUrl, "?", productPictureUrl);
	    		}
	        }
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pdId = FilterUtil.getStringBetween(evaluateResult[0], "name=\"saveid\" type=\"hidden\" value=\"", "\"").trim();
			if(!pdId.isEmpty()){
				return new String[] { pdId};
			}
			return null;
		}
		
		@Override
	    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String[] pdId = mpdBean.getRealProductId();
			String currentUrl = mpdBean.getCurrentUrl();;

			if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 
					|| pdId == null || pdId.length == 0 || pdId[0].isEmpty()) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			String pdName = productName[0];
			rtn[0].setName(pdName + " (" + pdId[0] + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			return rtn;
		}
}
