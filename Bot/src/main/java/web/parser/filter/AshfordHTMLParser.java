package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class AshfordHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1#prodName");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=product:price:amount]");
			productPrice = FilterUtil.toPlainTextString(e.attr("content"));				
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("tr.ashford_price");  
		productPrice = e.html();
		if(StringUtils.isBlank(productPrice)) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"Retail:","</td>");		
		}	
		productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String desc = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-description");
			desc = FilterUtil.toPlainTextString(e.html());			
		}	
		return new String[] {desc};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		
		String desc = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h2#prodID");
			e = e.select("div#qvSkuId");
			desc = FilterUtil.toPlainTextString(e.attr("data-skuid"));			
		}	
		return new String[] {desc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		Document doc  = Jsoup.parse(evaluateResult[0]);
			Elements e  = doc.select("img#prdDetailImg");
			productPictureUrl = e.attr("src");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
