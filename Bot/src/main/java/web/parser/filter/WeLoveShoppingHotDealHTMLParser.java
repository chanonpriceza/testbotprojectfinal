package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WeLoveShoppingHotDealHTMLParser extends DefaultHTMLParser{
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-name\" itemprop=\"name\">"});
		//map.put("price", new String[]{"<body  >", "<ul class=\"box-breadcrumb\" itemprop=\"breadcrumb\">"});
		map.put("price", new String[]{"<body>", "<ul class=\"breadcrumb-box\">"});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"box-more-product-detail\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-img\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}

    @Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "true";
		String endTime = FilterUtil.getStringBetween(evaluateResult[0], "data-time-end=\"", "\"").trim();
		if(StringUtils.isNumeric(endTime) && endTime.length() == 10){
			endTime += "000";
			long now = new Date().getTime();
			long endSec = Long.parseLong(endTime);
			if(now < endSec){
				exp = "false";
			}
		}
		return new String[] { exp};
	}
    
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(productName.contains("ระเบิดเวลา") && productName.contains("นาฬิกา")){
				productName = "";
			}
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		String catAlcohol = "";
    	if (evaluateResult.length > 0) {
    		if(evaluateResult.length == 2 && (evaluateResult[1].contains("เครื่องดืมแอลกฮอล์") || evaluateResult[1].contains("เครื่องดืมแอลกอฮอล์"))){
    			catAlcohol = "true";
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ng-bind=\"comparePrice\">", "</span>");
    		}
    		if(productPrice.isEmpty()){
    			//productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ng-bind=\"price\">", "</span>");
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "itemprop=\"price\" content=\"", "\"");
    		}    		
    		if(productPrice.indexOf("-") > -1){
    			String[] tmp = productPrice.split("-");
    			productPrice = tmp[0];
    		}    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice, catAlcohol};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "ng-bind=\"comparePrice\">", "<");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		if(evaluateResult.length == 1) {
			return new String[] {BotUtil.encodeURL(evaluateResult[0])};
		}
		return new String[] {""};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();		
		
		String pdName = productName[0];
		/*if (StringUtils.isNotBlank(pdName)){
			String[] urlSplit = currentUrl.split("/");
			if(urlSplit != null && urlSplit.length > 4){
				String realPdId = urlSplit[urlSplit.length -1].trim();
				if(realPdId.isEmpty()){
					return null;
				}
				rtn[0].setRealProductId(realPdId);
				pdName += " ("+ realPdId +")";
			}
		}else{
			return null;
		}*/
		
		if(pdName != null && pdName.trim().length() != 0) {
			pdName = pdName.trim() + " (Hot Deal)";
		}
		
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setName(pdName);
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		String catAlcohol = productPrice[1];
		if(catAlcohol.isEmpty()){
			productPictureUrl[0] = FilterUtil.getStringBetween(productPictureUrl[0], "src=\"", "\"");
		}else{
			productPictureUrl[0] = "https://halobe.files.wordpress.com/2017/04/weloveshopping.png";
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		/*String[] matchCatId = mapCategory(pdName, CAT_ID_KEYWORD_MAPPING);				
		if(matchCatId != null) {
			rtn[0].setKeyword(matchCatId[2]);
			rtn[0].setCategoryId(FilterUtil.stringToInt(matchCatId[0], 160255));										
		}else{
			rtn[0].setCategoryId(160255);
		}*/
		//Set All as 160255 
		rtn[0].setCategoryId(160255);
		
		return rtn;
	}
	
	/*@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBPrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		String pdName = productName[0];
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(pdName);
		double price = FilterUtil.convertPriceStr(productPrice[0]);
		rtn[0].setPrice(price);
		
		if (productBPrice != null && productBPrice.length != 0 && !productBPrice[0].isEmpty()) {
			double bPrice = FilterUtil.convertPriceStr(productBPrice[0]);
			if(bPrice > price){
				rtn[0].setBasePrice(bPrice);
			}
		}

		if (productDescription != null && productDescription.length != 0 && !productDescription[0].isEmpty()) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		String[] matchCatId = mapCategory(pdName, CAT_ID_KEYWORD_MAPPING);				
		if(matchCatId != null) {
			rtn[0].setKeyword(matchCatId[2]);
			rtn[0].setCategoryId(FilterUtil.stringToInt(matchCatId[0], 160255));										
		}else{
			rtn[0].setCategoryId(160255);
		}

		return rtn;
	}*/
	    
}
