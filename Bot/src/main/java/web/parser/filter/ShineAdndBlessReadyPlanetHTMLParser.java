package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class ShineAdndBlessReadyPlanetHTMLParser extends ReadyPlanetHTMLParser {
	
	Map<String,String> nameMap = new HashMap<String, String>();
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("price", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("basePrice", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("description", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("expire", new String[]{""});
		map.put("upc", new String[]{"<table cellpadding=\"3\" cellspacing=\"1\" width=\"100%\" border=\"0\">"});
		
		return map;
	}
    
    @Override
    public String[] getProductPrice(String[] evaluateResult) {
    	String price = "";
    	if(evaluateResult.length> 0) {
    		price = FilterUtil.getStringBetween(evaluateResult[0],"<input type='hidden' name='p_price_multiple' value='","'");
    	}
    	return new String[] {price};
    }
    
    @Override
    public String[] getProductUpc(String[] evaluateResult) {
    	
    	if(evaluateResult.length >0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("td");
    		List<Element> cutHeader = e.subList(3,e.size());
    		List<String> name = new ArrayList<String>();
    		List<String> price = new ArrayList<String>();
    		for(int i =0;i<cutHeader.size();i++) {
    			if(i%4!=1 && i%4!=2) {
    				continue;
    			}
    			Element x = cutHeader.get(i);
    			if(i%4==1) {
    				Elements cutSubName = x.select("span");
    				String plainTextName = FilterUtil.toPlainTextString(cutSubName.html());
    				name.add(plainTextName);
    			}
    			if(i%4==2) {
    				Elements cutPrice = x.select("input");
    				String plainTextPrice = FilterUtil.toPlainTextString(FilterUtil.removeCharNotPrice(cutPrice.attr("value")));
    				price.add(plainTextPrice);
    			}

    		}
    		AtomicInteger index = new AtomicInteger(); 
    		String [] result = name.stream().map(x->x+","+price.get(index.getAndIncrement())).toArray(String[]::new);
    		return result;

    	}
    	return null;
    }
    
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[productUpc.length];
		for(int i = 0;i<productUpc.length;i++) {
			rtn[i] = new ProductDataBean();
			String[] subNameAndPrice = productUpc[i].split(",");
			String subName = subNameAndPrice[0];
			String priceCut = subNameAndPrice[1];
			rtn[i].setName(productName[0]+" "+subName);
			rtn[i].setPrice(FilterUtil.convertPriceStr(priceCut));
	
			if (isNotEmpty(productDescription))
				rtn[i].setDescription(productDescription[0]);
			
			if (isNotEmpty(productPictureUrl)) {
				String pictureUrl = productPictureUrl[0];
				if(isNotBlank(pictureUrl)) {				
					if(pictureUrl.startsWith("http")) {
						rtn[i].setPictureUrl(pictureUrl);					
					} else {
						try {
							URL url = new URL(new URL(currentUrl), pictureUrl);
							rtn[i].setPictureUrl(url.toString());
				    	} catch (MalformedURLException e) {}
					}
				}
			}
	
			if (isNotEmpty(merchantUpdateDate)) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null)
					rtn[i].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
			
			if (isNotEmpty(realProductId))
				rtn[i].setRealProductId(realProductId[0]);
			
			if (isNotEmpty(productBasePrice))
				rtn[i].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
					
		}
		
		return rtn;
	}
    
}
