package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AugustSocietyHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_name\" itemprop=\"name\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<li class=\"active\" id=\"tab1\">"});
		map.put("pictureUrl", new String[]{"<div class=\"nine columns alpha\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length >= 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		String	productPrices = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\" content=\"", "\"");
    		productPrice = FilterUtil.toPlainTextString(productPrices);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 
        }	
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	

}
