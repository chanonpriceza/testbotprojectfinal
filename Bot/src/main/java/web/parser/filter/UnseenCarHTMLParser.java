package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class UnseenCarHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1>"});
			map.put("price", new String[]{"<span class=\"red t24 f_bold\">"});
			map.put("description", new String[]{"<div id=\"tabs1\">"});
			map.put("pictureUrl", new String[]{"<div class=\"thumbs\">"});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";  
			if(evaluateResult.length >= 1) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
	    		if(productPrice.equals("-")){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}else{
	    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    		}
	        }	    	
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {	
	    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
		@Override
	    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();;

			if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || productPictureUrl == null || productPictureUrl.length == 0) {
				return null;
			}
			
			String id = FilterUtil.getStringAfter(currentUrl, "carid=", "").trim();
			if(StringUtils.isBlank(id)){
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			String pdName = productName[0];
			rtn[0].setName(pdName + " (" + id + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			return rtn;
		}
}
