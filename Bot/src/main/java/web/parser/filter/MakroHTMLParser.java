package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class MakroHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<div class=\"product-detail-name\">"});
		map.put("price", 		new String[]{"<div class=\"product-detail-price-red\">"});
		map.put("basePrice", 	new String[]{"<div class=\"product-detail-text\">"});
		map.put("description", 	new String[]{"<div id=\"productCollapse\" class=\"text2-tab-product-detail collapse in\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"carousel-inner\" role=\"listbox\">"});
		map.put("realProductId",new String[]{"<div class=\"product-detail-text\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[2], "<strike>", "</strike>");
			bPrice = FilterUtil.removeCharNotPrice(bPrice);	    			
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if (evaluateResult.length > 0) {;
			realProductId = FilterUtil.toPlainTextString(evaluateResult[0]);
			realProductId = realProductId.replace("รหัสสินค้า", "");
        }
		return new String[] {realProductId};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		String[] realProductId = mpdBean.getRealProductId();
		String[] bPrice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+" ("+ realProductId[0] +")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if(bPrice != null && bPrice.length == 1 && !bPrice[0].equals(productPrice[0])){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						String imgUrl = FilterUtil.getStringBefore(currentUrl, "th/", currentUrl);
						URL url = new URL(new URL(imgUrl+"public/"),productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}
	    
}