package web.parser.filter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SmokyBikeHTMLParser extends DefaultHTMLParser{

	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{"<div class=\"owl-carousel owl-theme\" data-slider-id=\"1\">"});
			//map.put("basePrice", new String[]{"<span class=\"show-discount-price disable primary-text\">"});
			map.put("expire", new String[]{"<div class=\"image-slider__tag image-slider__tag-price\">"});
			return map;
		}
	 
		 @Override
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length > 0) {
				
				String id = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"canonical\" href=\"https://smokybike.com/", "/");
				
				productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 class=\"text-2xl mb-2\">","<div class=\"card-body\">");
				productName = FilterUtil.getStringBetween(productName,"<span itemprop=\"name\">","</span>");
				productName = FilterUtil.toPlainTextString(productName);	
				
				if(productName.equals("")) {
					productName = FilterUtil.getStringBetween(evaluateResult[0],"<span itemprop=\"name\">","</span>");
				}
				
				if(productName.equals("")) {
					id = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"canonical\" href=\"https://smokybike.com/", "/");
					productName = FilterUtil.getStringBetween(evaluateResult[0],"<h2 class=\"h4\"><i class=\"fas fa-motorcycle\"></i>","</h2>");
				}
				if(StringUtils.isNoneBlank(id)) {
					productName = productName + "("+id+") (มอเตอร์ไซค์มือสอง)"  ;					
				}else {
					productName = "รถมอเตอร์ไซค์  "+productName  ;					
				}
				
				productName = StringEscapeUtils.unescapeHtml3(productName);
	
				
			}			
				return new String[] {productName};
		}
	 
		 @Override
		public String[] getProductPrice(String[] evaluateResult) {
			 String productPrice = "";    
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<th class=\"text-right\">ราคา:</th>", "</span>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		if(productPrice.equals("")) {
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h3 class=\"h5\">ราคาเปิดตัว</h3><h3 class=\"price h1\">", "</h3>");	    			
	    		}
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }		
			return new String[] {productPrice};
		}
	 
		@Override
		public String[] getProductDescription(String[] evaluateResult) {
			String productDescription = ""; 
			if(evaluateResult.length > 0){
				
				productDescription = FilterUtil.getStringBetween(evaluateResult[0], "<p itemprop=\"description\" >", "<p itemprop=\"description\" >");
				productDescription = FilterUtil.toPlainTextString(productDescription);
				if(productDescription.equals("")) {
					String newDescription = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"mr-4 ml-4 border-bottom\">", "<div class=\"fb-like border-right pr-2");
//					ยี่ห้อ: BMW; รุ่น: R 1200 R; ปี: 2017 - 2018; สี: Light white/ Cordoba blue; เครื่องยนต์: Boxer 2 สูบ, DOHC พร้อมCentral Balance shaft, ระบายความร้อนด้วยอากาศและของเหลว; ปริมาตรกระบอกสูบ (ซีซี): 1,170
					String brand = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">ยี่ห้อ:</th>", "</tr>");
					brand = FilterUtil.getStringBetween(brand, "<td>", "</td>");
					String generation = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">รุ่น:</th>", "</tr>");
					generation = FilterUtil.getStringBetween(generation, "<td>", "</td>");
					String year = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">ปี:</th>", "</tr>");
					year = FilterUtil.getStringBetween(year, "<td>", "</td>");
					String color = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">สี:</th>", "</tr>");
					color = FilterUtil.getStringBetween(color, "<td>", "</td>");
					String motor = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">เครื่องยนต์:</th>", "</tr>");
					motor = FilterUtil.getStringBetween(motor, "<td>", "</td>");
					String cylinder = FilterUtil.getStringBetween(newDescription, "<th scope=\"row\" class=\"text-right\" width=\"200\">ปริมาตรกระบอกสูบ (ซีซี):</th>", "</tr>");
					cylinder = FilterUtil.getStringBetween(cylinder, "<td>", "</td>");

					if(StringUtils.isNoneBlank(brand)) {
						productDescription += " ยี่ห้อ:  "+brand;
					}
					if(StringUtils.isNoneBlank(generation)) {
						productDescription += " รุ่น:   "+generation;
					}
					if(StringUtils.isNoneBlank(year)) {
						productDescription += " ปี:   "+year;
					}
					if(StringUtils.isNoneBlank(color)) {
						productDescription += " สี:  "+color;
					}
					if(StringUtils.isNoneBlank(motor)) {
						productDescription += " เครื่องยนต์:   "+motor;
					}
					if(StringUtils.isNoneBlank(cylinder)) {
						productDescription += " ปริมาตรกระบอกสูบ (ซีซี):   "+cylinder;
					}

				}
				
			}
			return new String[] {productDescription};
		}
		
		@Override
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productUrl = "";
			if(evaluateResult.length == 1){
				productUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
				productUrl = decode(productUrl);
			}
			return new String[] {productUrl};
		}
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			String productExp = "false";
			if(evaluateResult.length == 1){
				if(evaluateResult[0].contains("ขายแล้ว")){
					productExp = "true";	
				}
			}
			return new String[] {productExp};
		}
		
		private String decode(String url)
		{
			StringBuilder tmp = new StringBuilder();  
			  char cc[] = url.toCharArray();
			     for (int i = 0; i < cc.length; i++) {
			         int eachCharAscii = (int) cc[i];
			         if (3585 <= eachCharAscii && eachCharAscii <= 3675) {          
			          try {
			     tmp.append(URLEncoder.encode(String.valueOf(cc[i]), "UTF-8"));
			    } catch (UnsupportedEncodingException e) {     
			    }          
			            } else if(eachCharAscii == 32) { // space
			             tmp.append("%20");   
			            } else {
			             tmp.append(cc[i]);             
			            }
			     }     
			  return tmp.toString();
		}
}
