package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import product.processor.ProductFilter;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class TemplateWeLoveShoppingNoCatIncludeIdHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<div class=\"pro-name\" id=\"moreimage\">"});
		map.put("price", 			new String[]{"<div class=\"pro-price\">"});
		map.put("basePrice", 		new String[]{"<div class=\"pro-orgprice\">"});
		map.put("description", 		new String[]{"<table class=\"dataset-3\" id=\"product-description\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"pro-image\">" , "<div class=\"fullImage\" style=\"text-align: center;\">"});
		map.put("realProductId", 	new String[]{"<td class=\"bttn-area\">"});
		map.put("nameDescription", 	new String[]{"<div id=\"product-nav\">"});
		map.put("expire", 			new String[]{"<div class=\"pro-name\" id=\"moreimage\">"});
		
		return map;
	}
	
    @Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0){
			if(evaluateResult[0].toLowerCase().contains("out of stock")){
				return new String[] {"true"};
			}
			if(evaluateResult[0].contains("หมดแล้วค่ะ")){
				return new String[] {"true"};
			}
		}
		return new String[] {""};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
        if(ProductFilter.checkCOVIDInstantDelete(productName))
        	return null;
        
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		
    		String dcPriceArea = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pro-dcprice\">", "</div>");
    		if(StringUtils.isNotBlank(dcPriceArea)){
    			productPrice = FilterUtil.toPlainTextString(dcPriceArea);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    			if("0.00".equals(productPrice)) {
    				productPrice = BotUtil.CONTACT_PRICE_STR;
    			}
    		}else{
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			if("0.00".equals(productBasePrice)) {
				productBasePrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0],"</th>",evaluateResult[0]); 
    		productDesc = FilterUtil.toPlainTextString(productDesc); 
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	if(evaluateResult.length == 1){
    		productId = FilterUtil.getStringBetween(evaluateResult[0], "name=\"product_id\"", ">").trim();
    		productId = FilterUtil.getStringBetween(productId, "value=\"", "\"").trim();
    	}
    	return new String[]{ productId};
    }
    
    @Override
	public String[] getProductNameDescription(String[] evaluateResult) {
    	String productNameDescription = "";
    	if(evaluateResult.length > 0){
    		productNameDescription = FilterUtil.getStringBetween(evaluateResult[0],"</li>","<li class=\"point\">");
    		productNameDescription = FilterUtil.toPlainTextString(productNameDescription);
    	}
		return new String[]{productNameDescription};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] nameDescription = mpdBean.getProductNameDescription();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || productPrice == null || productPrice.length == 0) {
			return null;
		}

		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		if (nameDescription != null && nameDescription.length != 0){
			rtn[0].setKeyword(nameDescription[0]);
		}
		
		return rtn;
	}
    
}
