package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ReadyPlanetNoPriceHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("price", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("basePrice", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("description", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			String rawId = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส", "</div>");
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</div>");
			
			rawId = FilterUtil.toPlainTextString(rawId);
			rawName = FilterUtil.toPlainTextString(rawName);
			
			productName = rawId.replace(":", "")+" "+rawName;
			if(productName.contains("สินค้าหมด stock")){
				productName = "";
    		}else if(productName.contains("สินค้าหมด")){
    			productName = "";
    		}
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>สมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ราคาสมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ชิ้น</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 คู่</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ตัว</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p align=\"left\"><font color=\"#ff0000\" size=\"2\" face=\"Arial\">", "</font>");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		productPrice = productPrice.replace("&nbsp;", "");
    		if("** สินค้านำเข้า ประมาณ 20 วัน**".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ปกติ</span>", "</div>");
    		}
    		
    		bPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		bPrice = bPrice.replace("&nbsp;", "");
    		
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดทั้งหมด :", "<input type='hidden' name='cart' value='add'>");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDescription+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<div style = \"clear:both;padding:5px\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href = \"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String isExpire = "false";
		if(evaluateResult.length > 0) {
			String expire = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>", "</span>");
			if(expire.contains("สินค้าหมด")){
				isExpire = "true";
			}
			if(expire.contains("สินค้าหมด stock")){
				isExpire = "true";
    		}
			
			if(expire.contains("(Sold out)")){
				isExpire = "true";
			}
		}
		return new String[] {isExpire};
	}
}