package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class TemplateLnwshopIncludeIdHTMLParser extends TemplateLnwshopHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"headerText\" itemprop=\"name\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div id=\"detail\" class=\"tabPanel mceContentBody\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"productPhoto\">"});
		map.put("realProductId", new String[]{""});
		map.put("upc", new String[]{"<tr class=\"barcodeTR\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	    
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	productId = FilterUtil.getStringBetween(evaluateResult[0], "name=\"product_id\" value=\"", "\"").trim();
    	return new String[]{ productId};
    }
    
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] upc = mpdBean.getProductUpc();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || productPrice == null || productPrice.length == 0) {
			return null;
		}

		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		if (upc != null && upc.length != 0) {
			rtn[0].setUpc(upc[0]);
		}
		
		return rtn;
	}
    
}
