package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class MitzXyzHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(evaluateResult.length > 1 && StringUtils.isBlank(productName)) {
				productName =  FilterUtil.toPlainTextString(evaluateResult[1]);
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e= doc.select("p.price");
    		e= e.select("span.woocommerce-Price-amount");
    		productPrice = FilterUtil.removeCharNotPrice(e.size()>1 ? e.get(0).html():e.html());
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String result = "";
		if (evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[name=twitter:image]");
			result = e.attr("content");
		}
		return new String[] {result};
	}

	 
	
}
