package web.parser.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bean.ProductDataBean;
import utils.FilterUtil;

public class TemplateLazadaJSIncludeProductDetailHTMLParser extends TemplateLazadaRealtimeHTMLParser{

	@SuppressWarnings("unchecked")
	@Override
	protected ProductDataBean genDynamicField(ProductDataBean pdb, JSONObject appObj) {
		
		if(appObj == null) return null;
		
		JSONObject dataObj = (JSONObject) appObj.get("data");
		if(dataObj == null) return null;
		
		JSONObject rootObj = (JSONObject) dataObj.get("root");
		if(rootObj == null) return null;
		
		JSONObject fieldObj = (JSONObject) rootObj.get("fields");
		if(fieldObj == null) return null;
		
		String shopName = "";
		String shopScore = "";
		String isFreeShipping = "";
		String isOfficialShop = "false";
		JSONArray promotions = new JSONArray();
		
		// shopname, shopscore and official check
		JSONObject sellerObj = (JSONObject) fieldObj.get("seller");
		if(sellerObj != null) {
			shopName = String.valueOf((Object) sellerObj.get("name"));
			JSONObject positiveSellerRatingObj = (JSONObject) sellerObj.get("positiveSellerRating");
			if(positiveSellerRatingObj != null) {
				shopScore = String.valueOf((Object) positiveSellerRatingObj.get("value"));
				shopScore = validateShopScore(shopScore, "\\d+(?:\\.\\d+)?%");
			}
			String shopType = String.valueOf((Object) sellerObj.get("type"));
			if(shopType.equals("6")) {
				isOfficialShop = "true";
			}
		}
		
		// shipping check
		JSONObject deliveryOptionsObj = (JSONObject) fieldObj.get("deliveryOptions");
		if(deliveryOptionsObj != null) {
			JSONArray optionArr = (JSONArray) deliveryOptionsObj.get(pdb.getRealProductId());
			if(optionArr != null && optionArr.size() > 0) {
				for (Object option : optionArr) {
					JSONObject optionObj = (JSONObject) option;
					String dataType = String.valueOf((Object) optionObj.get("dataType"));
					if(dataType.equals("delivery")) {
						String fee = String.valueOf((Object) optionObj.get("fee"));
						if(fee.equals("ฟรี")) {
							isFreeShipping = "true";
							break;
						}else {
							isFreeShipping = "false";
						}
					}
				}
			}
		}

		// promotion
		JSONObject promotionTagsObj = (JSONObject) fieldObj.get("promotionTags");
		if(promotionTagsObj != null) {
			JSONObject tagObj = (JSONObject) promotionTagsObj.get("data");
			if(tagObj != null) {
				JSONArray data = (JSONArray) tagObj.get(pdb.getRealProductId());
				if(data != null && data.size() > 0) {
					for (Object object : data) {
						JSONObject obj = (JSONObject) object;
						String name = String.valueOf((Object) obj.get("name"));
						String description = String.valueOf((Object) obj.get("description"));
						String timeline = String.valueOf((Object) obj.get("timeline"));
						String startDate = FilterUtil.getStringBefore(timeline, "-", null);
						String endDate = FilterUtil.getStringAfter(timeline, "-", null);
						JSONObject promotion = new JSONObject();
						if(StringUtils.isNotBlank(name))		promotion.put("title", FilterUtil.toPlainTextString(name));
						if(StringUtils.isNotBlank(description))	promotion.put("description", FilterUtil.toPlainTextString(description));
						if(StringUtils.isNotBlank(startDate))	promotion.put("startDate", startDate);
						if(StringUtils.isNotBlank(endDate))		promotion.put("endDate", endDate);
						promotions.add(promotion);
					}
				}
			}
		}
		
		JSONObject dynamicField = new JSONObject();
		JSONObject productDetail = new JSONObject();
		if(StringUtils.isNotBlank(shopName)) 					productDetail.put("shopName", shopName);
		if(StringUtils.isNotBlank(shopScore)) 					productDetail.put("shopScore", shopScore);
		if(StringUtils.isNotBlank(isFreeShipping)) 				productDetail.put("isFreeShipping", Boolean.parseBoolean(isFreeShipping));
		if(StringUtils.isNotBlank(isOfficialShop)) 				productDetail.put("isOfficialShop", Boolean.parseBoolean(isOfficialShop));
		if(promotions != null && promotions.size()> 0) 			productDetail.put("promotions", promotions);
		if(productDetail != null && productDetail.size() > 0) 	dynamicField.put("productDetail", productDetail);
		
		pdb.setDynamicField(dynamicField.toJSONString());
		return pdb;
	}

	private String validateShopScore(String input, String testCase) {
		Pattern pattern = Pattern.compile(testCase);
		Matcher matcher = pattern.matcher(input);
		if(matcher.matches()) {
			return input;
		}
		return null;
	}
}
