package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;

public class LightAndBathHTMLParser extends ReadyPlanetNoPriceHTMLParser{
			
	public String getCharset() {
		return "tis-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("expire", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("price", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("description", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			String rawId = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส", "</div>");
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</div>");
			
			rawId = FilterUtil.toPlainTextString(rawId);
			rawName = FilterUtil.toPlainTextString(rawName);
			productName = rawId.replace(":", "")+" "+rawName;
		
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style = 'font-weight:bold'>ราคาหลังหักส่วนลด", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>สมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ราคาสมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ชิ้น</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 คู่</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ตัว</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p align=\"left\"><font color=\"#ff0000\" size=\"2\" face=\"Arial\">", "</font>");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		productPrice = productPrice.replace("&nbsp;", "");
    		if("** สินค้านำเข้า ประมาณ 20 วัน**".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดทั้งหมด :", "<input type='hidden' name='cart' value='add'>");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDescription+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<div style = \"clear:both;padding:5px\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href = \"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";   
		if(evaluateResult.length == 1) {
			String rawId = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8 0 8 0;'><span class='hd'> รหัส", "</div>");
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</div>");
			expire = rawId.replace(":", "")+" "+rawName;
			if(expire.contains("สินค้าหมด stock")){
				expire = "true";
			}else if(expire.contains("สินค้าหมด")){
				expire = "true";
			}
		}
		return new String[] {expire};
	}
}