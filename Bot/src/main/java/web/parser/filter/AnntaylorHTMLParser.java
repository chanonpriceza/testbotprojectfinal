package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class AnntaylorHTMLParser extends BasicHTMLParser {
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			mixIdName =  true;
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("expire", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("h1[itemprop=name]");
				productName = FilterUtil.toPlainTextString(e.html());			
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	

			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("strong.price");
			productPrice = FilterUtil.removeCharNotPrice(e.html());					
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.price-info");
				e = e.select("p.old-price");
				e = e.select("span.price");
				productPrice = FilterUtil.removeCharNotPrice(e.html());			
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String id = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("input[name=skuId]");
				id = e.attr("value");			
			}	
			return new String[] {id};
		}
		
}
