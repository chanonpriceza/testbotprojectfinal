package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AvramaHTMLParser extends DefaultHTMLParser{
		
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class='h1'>"});
		map.put("price", new String[]{"<span class='h3'>", "<span class='price_set'>"});
		map.put("description", new String[]{"<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"});
		map.put("pictureUrl", new String[]{"<td class='content'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(productName.contains("(สินค้าหมด)")) {
				productName = "";
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[evaluateResult.length - 1]);
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = FilterUtil.getStringAfter(productDesc, "รายละเอียดย่อ  :", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringAfter(evaluateResult[0], "<div style='margin:15 0 15 0;'>", "");    		
    		productPictureUrl = FilterUtil.getStringBetween(tmp, "src='", "'");
        }
    	return new String[] {productPictureUrl};
	}
	    
}