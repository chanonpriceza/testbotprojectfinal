package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class Best2HomeHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{"<div class=\"product-view row\">"});
			map.put("basePrice", new String[]{"<div class=\"product-view row\">"});
			map.put("description", new String[]{"<div class=\"product-box-desc\">"});
			map.put("pictureUrl", new String[]{"<div class=\"large-image\">"});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length > 0) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
				productName = FilterUtil.toPlainTextString(productName);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-new\" itemprop=\"price\">", "</span>");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }	    	
			return new String[] {productPrice};
		}
		
	
		public String[] getProductBasePrice(String[] evaluateResult) {
			String BasePrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		BasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-old\">", "</span>");
	    		BasePrice = FilterUtil.removeCharNotPrice(BasePrice);
	        }	    	
			return new String[] {BasePrice};
		}

		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length > 0) {	
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
}
