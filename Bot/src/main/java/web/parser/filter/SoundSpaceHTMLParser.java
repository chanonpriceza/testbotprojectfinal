package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SoundSpaceHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1 itemprop=\"name\" class=\"product-title\">"});
		map.put("price", 		new String[]{"<p class=\"price\">"});
		map.put("basePrice", 	new String[]{"<p class=\"price\">"});
		map.put("description", 	new String[]{"<div class=\"single-product-description\" itemprop=\"description\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"images\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringAfter(evaluateResult[0], "</del>", evaluateResult[0]);
    		productPrice = productPrice.replace("&#3647;", "");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
			productBasePrice = productBasePrice.replace("&#3647;", "");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}