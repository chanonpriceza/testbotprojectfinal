package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class KaideeMarketplaceHTMLParser extends KaideeHTMLParser{	
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 

    	map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<span class=\"sc-1x47nlp-0 ldElVD inner-text\" type=\"input-text\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	@Override
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h4rcyv-3 jRltnw\"><span color=\"#121212\" type=\"h-x-small\" class=\"sc-1x47nlp-0 ilIovr\">", "</span></h1>");
			productName = FilterUtil.toPlainTextString(productName);	
			String secondHand = FilterUtil.getStringBetween(evaluateResult[0], "</span><span type=\"body-text\" color=\"#5E5E5E\" class=\"sc-1x47nlp-0 glSrFR\">", "</span></div>");
			String itemId = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:url\" content=\"https://www.kaidee.com/product-", "\"");
			
			if(secondHand.indexOf("มือสอง") > -1){
				productName = productName + " (มือสอง)";			
			}else if(secondHand.indexOf("มือหนึ่ง") > -1){
				productName = productName + " (มือหนึ่ง)";			
			}
			
			if(StringUtils.isNotBlank(itemId)) {
				productName = productName + " (" + itemId + ")";
			}
			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span type=\"h-small\" color=\"#00B46B\" class=\"sc-1x47nlp-0 kyTrnR\">", "</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
				
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productDesc};
	}
	
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];		
		
		//breadcrumb = มอเตอร์ไซค์ || รถมือสอง = 2
		
		
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setDescription(productDescription[0]);

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		return rtn;
	}
	
}