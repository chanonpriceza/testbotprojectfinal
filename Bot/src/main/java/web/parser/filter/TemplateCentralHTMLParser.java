package web.parser.filter;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class TemplateCentralHTMLParser extends BasicHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e =  doc.select("title");
			productName = FilterUtil.toPlainTextString(e.html());		
			productName = productName.replace(" | Central.co.th","");
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			productPrice = FilterUtil.getStringBetween(doc.toString().replaceAll("\n",""),"\\\"sale_price_min\\\":",",");
			List<String> prices = FilterUtil.getAllStringBetween(doc.toString().replaceAll("\n",""),"\\\"sale_price_min\\\":",",").stream().filter(x->!x.equals("0")).collect(Collectors.toList());
			if(prices!=null&&prices.size() > 0) {
				productPrice = prices.get(0);
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			productPrice = FilterUtil.getStringBetween(doc.toString().replaceAll("\n",""),"\"price\\\":",",");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {

		String id = "";    	
		if(evaluateResult.length == 1) {	
			id = FilterUtil.getStringBetween(evaluateResult[0],"\\\"sku\\\":\\\"","\\\"");
			id = FilterUtil.removeCharNotPrice(id);			
		}	
		return new String[] {id};
	}
}
