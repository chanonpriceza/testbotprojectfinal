package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PlazaHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"prices-container clear\">"});
		map.put("description", new String[]{"<div class=\"cm-tabs-content clear\" id=\"tabs_content\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"float-left product-prices\">"});
		map.put("realProductId", new String[]{"<div class=\"float-left product-prices\">"});
		map.put("upc", new String[]{"<div class=\"product-info\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 class=\"mainbox-title\">","</h1>");			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {

    		String tmp = FilterUtil.getStringBetween(evaluateResult[0],"<span id=\"sec_discounted_price","/span>");
    		tmp = FilterUtil.getStringBetween(tmp, "class=\"price\">", "<");

    		if(tmp.length() == 0) {
    			tmp = productPrice = BotUtil.CONTACT_PRICE_STR;
  			
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคาปกติ:", "</span>");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
		if (evaluateResult.length > 0) {
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\" cm-thumbnails\" ", "</a>");
			productPictureUrl = FilterUtil.getStringBetween(tmp, "src=\"", "\""); 
		}
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if(evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"cm-reload-", "\"");
		}
		return new String[] {realProductId};
	}
	
	
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = "";
		if(evaluateResult.length > 0) {
			upc = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"product_code_", "/span>");
			upc = FilterUtil.getStringBetween(upc, "\">", "<");
		}
		return new String[] {upc};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productUpc = mpdBean.getProductUpc();
		String[] realProductId = mpdBean.getRealProductId();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();

		
		if (productName != null && productName.length != 0 && productUpc != null && productUpc.length != 0 ) {	
			rtn[0].setName("นาฬิกาข้อมือ "+productName[0] + "["+productUpc[0]+"]");
		}
		if (productPrice != null && productPrice.length != 0) {
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		}
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		rtn[0].setDescription(productDescription[0]);
		rtn[0].setPictureUrl("https://www.plaza.co.th"+productPictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);
		return rtn;
	}

	
	
	

	    
}