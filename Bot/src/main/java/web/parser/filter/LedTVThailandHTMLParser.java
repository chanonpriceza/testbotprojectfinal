package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class LedTVThailandHTMLParser extends TemplateTaradHTMLParser{
	
	public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<div style='padding-bottom: 5px;' class='FontCaption NewProductDetail'>"});
			map.put("price", new String[]{"<font class='ProductPricePrice'>"});
			map.put("description", new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});
			map.put("pictureUrl", new String[]{"<table align='center' border='0' cellspacing='0' cellpadding='0' class='ProductPictureBorder' width='150' height='150'>"});
			map.put("realProductId",new String[]{"<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>"});
			map.put("basePrice",new String[]{"<strike>"});
			return map;
	}		
	
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);				
		}
		
		return new String[] {productName};
	}
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String prodcutPrice= "";
		if(evaluateResult.length > 0) {
			prodcutPrice = evaluateResult[0];
			prodcutPrice = FilterUtil.removeCharNotPrice(prodcutPrice);
			if(prodcutPrice.equals("") || prodcutPrice.equals("0.0") || prodcutPrice.equals("0") ||prodcutPrice.equals("0.00")) {
				prodcutPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[]{prodcutPrice};
	}
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		String[] realProductId = mpdBean.getRealProductId();
		String [] basePrice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+" ("+realProductId[0]+")");
		
		double price = FilterUtil.convertPriceStr(productPrice[0]);


				
		if(price < 0) {
			price = 0;
		}
		
		rtn[0].setPrice(price);

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (basePrice != null && basePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}

		return rtn;
	}
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String id = "";
    	if(evaluateResult.length > 0) {
    		id = evaluateResult[0];
    		id = FilterUtil.getStringAfterLastIndex(id,"รหัสสินค้า:",id);
    		id = FilterUtil.toPlainTextString(id);
    	}
    	return new String[]{id};
    }
    
    @Override
    public String[] getProductBasePrice(String[] evaluateResult) {
    	String base = "";
    	if(evaluateResult.length > 0) {
    		base = evaluateResult[0];
    		base = FilterUtil.toPlainTextString(base);
    		base =FilterUtil.removeCharNotPrice(base);
    	}
    	return new String[]{base};
    }
	    
}