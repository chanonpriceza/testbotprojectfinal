package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class GoodsHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"clearfix\">"});
		map.put("price", new String[]{"<p class=\"actual-price\">"});
		map.put("basePrice", new String[]{"<div class=\"product-prices\">"});
		map.put("description", new String[]{"<div class=\"note-descr\">"});
		map.put("pictureUrl", new String[]{"<div class=\"ty-product-img cm-preview-wrapper\">"});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{"<div class=\"ty-control-group product-list-field\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"ty-mainbox-title\" >", "</h1>")+
					" ("+FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"call_request_", "\"")+")";
			productName = FilterUtil.toPlainTextString(productName);  			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
    		String tmp = evaluateResult[0];
    		if(tmp.contains("line-through")) {
    			tmp = FilterUtil.getStringAfter(tmp , "</span>", "");
    		}    		
    		tmp = FilterUtil.toPlainTextString(tmp);	   
    		
    		if(tmp.toUpperCase().indexOf("X") > -1){
    			productPrice = BotUtil.CONTACT_PRICE_STR;    			
    		} else {
    			productPrice = FilterUtil.removeCharNotPrice(tmp);   
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringAfter(evaluateResult[0], "</p>", "");
			basePrice = FilterUtil.getStringBetween(basePrice, "<span class=\"ty-strike\">", "</span>");
		}
		basePrice = FilterUtil.toPlainTextString(basePrice);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		return new String[]{basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    		
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if(evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"call_request_", "\"");
		}
		return new String[]{realProductId};
	}	
	
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "false";
		
    	if (evaluateResult.length > 0) {
    		if (evaluateResult[1].indexOf("มีสินค้า") < 0) {
    			pdExpire = "true";
    		}
        }
    	return new String[] {pdExpire};
	}
	
	    
}