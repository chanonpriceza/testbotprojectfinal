package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class StationerymineHTMLParser extends BasicHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		String brand = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			productName = FilterUtil.toPlainTextString(e.attr("content"));		
			Elements b = doc.select("td[data-th=แบรนด์]");
			brand = b.text();
		}
		
		if("N/A".equals(brand)) 
			brand = "";
		
		
		return new String[] {brand+" "+productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[itemprop=price]");
			productPrice = FilterUtil.removeCharNotPrice(e.attr("content"));			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("span[id^=old-price]");
			productPrice = FilterUtil.removeCharNotPrice(e.attr("data-price-amount"));			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements b = doc.select("td[data-th=รหัสสินค้า]");
		return new String[] {b.text()};
	}
 
}
