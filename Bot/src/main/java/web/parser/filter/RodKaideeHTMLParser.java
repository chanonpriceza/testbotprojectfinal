package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class RodKaideeHTMLParser extends DefaultHTMLParser {
	
	private static final Logger logger = LogManager.getRootLogger();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{""});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			String name = evaluateResult[0];
			name = FilterUtil.getStringBetween(evaluateResult[0], "<script type=\"application/ld+json\"", "</script>");
			name = FilterUtil.getStringAfter(name, ">", name);
			if(StringUtils.isBlank(name)) { return null; }
			
			try {
				JSONObject json = (JSONObject) new JSONParser().parse(name);
				productName = String.valueOf(json.get("name"));
				
				JSONObject offer = (JSONObject) json.get("offers");
				if(offer != null && offer.size() > 0) {
					String itemCondition = (String) offer.get("itemCondition");
					if(StringUtils.isNotBlank(itemCondition) && itemCondition.equals("used")) {
						productName += " (สินค้ามือสอง)";
					}
				}
			}catch(Exception e) {
				logger.error(e);
			}
			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li id=\"view-price\"", "</li>");
    		productPrice = FilterUtil.getStringAfter(productPrice, ">", productPrice);
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    	    productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		String bPrices = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\" id=\"old-price", "</span>");
    		if (StringUtils.isNotBlank(bPrices)) {
    			bPrice = FilterUtil.getStringAfter(bPrices, ">", bPrices);
				
    		}
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"ad-description-detail\"", "</div>");
    		productDesc = FilterUtil.getStringAfter(productDesc, ">", productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
