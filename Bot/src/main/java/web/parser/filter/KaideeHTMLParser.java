package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class KaideeHTMLParser extends DefaultHTMLParser{	
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 

    	map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductExpire(String[] evaluateResult) {				
		if(evaluateResult[0].contains("ประกาศนี้ไม่มีในระบบ") || evaluateResult[0].contains("สินค้าได้ถูกขายไปแล้ว")) {
			return new String[] { "true"};
		}
		return new String[] { ""};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"reset\"", "</h1>");
			if(!productName.isEmpty()){
				productName = "<h1 class=\"reset\"" + productName;
			}
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"block-contact price df-icon-tag\"", "</");
			if(!productPrice.isEmpty()){
				productPrice = "<span class=\"block-contact price df-icon-tag\"" + productPrice;
			}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
				
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"ad-description\"", "</div>");
			if(!productDesc.isEmpty()){
				productDesc = "<div class=\"ad-description\"" + productDesc;
			}
			productDesc = FilterUtil.toPlainTextString(productDesc);
		}

		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";

		if(evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "property=\"og:image\" content=\"", "\"");
			productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);			
		}
		
		return new String[] {productPictureUrl};
	}
	
}