package web.parser.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.SocketFactory;
import web.parser.DefaultHTMLParser;

public class TemplateMakeWebEasyHTMLParser extends DefaultHTMLParser{
	private static Logger   logger = LogManager.getRootLogger();
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "priceza.shader.io";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
	
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">","<div class=\"productItemDetail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});

	//	map.put("expire", new String[]{"<div class=\"product_comingsoon warningBox\" v-bind:class=\"'product_'+show_pdata.status.sell\" itemprop=\"availability\" content=\"out_of_stock\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {		
			Document doc = Jsoup.parse(evaluateResult[0]);
			String name1 = doc.select("h1.productName").html();
			productName = name1;
			
		}
		return new String[] {productName};
	}

	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		
		List<String> priceAttrList = FilterUtil.getAllStringBetween(evaluateResult[0], "class=\"hiddenAttributeID\" value=\"", "\"");
		if(priceAttrList != null && priceAttrList.size() > 0) {
			return priceAttrList.toArray(new String[priceAttrList.size()]);
		}
		String priceAttrArea = FilterUtil.getStringBetween(evaluateResult[0], "<select class=\"form-control dropdownDetail hiddenAttributeID\">", "</select>");
		if(StringUtils.isNotBlank(priceAttrArea)) {
			priceAttrList = FilterUtil.getAllStringBetween(priceAttrArea, "value=\"", "\"");
			if(priceAttrList != null && priceAttrList.size() > 0) {
				return priceAttrList.toArray(new String[priceAttrList.size()]);
			}
		}
		
		return null;
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = evaluateResult[0].replaceAll("\\<.*?>", " ").replaceAll("\\s+"," ").trim();
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = null;
		if (evaluateResult.length >= 1) {
			upc = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"bodyTD\">", "</td>").trim();
			upc = FilterUtil.toPlainTextString(upc);
			if(!StringUtils.isNumeric(upc) || upc.length() != 13){
				upc = null;
			}
		}
		return new String[] { upc};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "";
		if (evaluateResult.length == 1) {
			if(evaluateResult[0].contains("สินค้าหมด")|| evaluateResult[0].contains("เร็วๆนี้")){
				exp = "true";
			}
		}
		return new String[] { exp};
	}
	
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	if (evaluateResult.length == 1) {
			productId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" id=\"hiddenProductID\" value=\"", "\"").trim();
    	}
    	return new String[]{ productId};
    }
    
    @Override
	public String[] getProductNameDescription(String[] evaluateResult) {
    	String productNameDescription = "";
    	if(evaluateResult.length > 0){
    		productNameDescription = FilterUtil.getStringAfter(evaluateResult[0],"<td class=\"bodyTD\">",evaluateResult[0]);
    		productNameDescription = FilterUtil.toPlainTextString(productNameDescription);
    	}
		return new String[]{productNameDescription};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || realProductId == null || realProductId.length == 0) {
			return null;
		}

		
		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		if(productPrice == null || productPrice.length == 0) {
			productPrice = new String[] {"0"};
		}
		
		String domain = FilterUtil.getStringBetween(FilterUtil.getStringAfter(currentUrl,"http",""), "://", "/");
		
		boolean isHttps = (currentUrl.indexOf("https")> -1)? true:false ; 
		
		if(StringUtils.isBlank(domain)) return null;
		
		String req_url = (isHttps)?"https":"http" ;
		req_url += "://" + domain;
		req_url += "/page/page_product_item/ajaxPageProductItemController.php";
		
		ProductDataBean[] rtn = new ProductDataBean[productPrice.length];
		int productCount = 0;
		
		for(String priceAttr : productPrice) {
			String data = postRequest(currentUrl, req_url, realProductId[0].trim(), priceAttr, domain, isHttps);
			data = String.valueOf(data);
			String dataEscaped = StringEscapeUtils.unescapeJson(data);
			
			String price = FilterUtil.getStringBetween(dataEscaped, "<span class=\"h4\">", "</span>");
			if(price.isEmpty()){
				price = FilterUtil.getStringBetween(dataEscaped, "<span class=\"price h4\">", "</span>");
			}
			if(price.isEmpty()){
				price = FilterUtil.getStringAfter(dataEscaped,"class=\"ff-price fs-price fc-price\"","");
				price = FilterUtil.getStringAfter(price,">","");
				price = FilterUtil.getStringBefore(price,"<","");
			}
			
			if(price.contains("THB")) {
				price = FilterUtil.getStringAfterLastIndex(price,"THB",price);
				price = FilterUtil.toPlainTextString(price);
			}
			
			if(price.isEmpty() || price.equals("0")){
				price = String.valueOf(BotUtil.CONTACT_PRICE);
			}
			String bPrice =  FilterUtil.getStringBetween(dataEscaped, "<span class=\"productOldPrice h5\">", "</span>");
			if(bPrice.isEmpty()) {
				bPrice = FilterUtil.getStringAfter(dataEscaped,"class=\"productOldPrice h5","");
				bPrice = FilterUtil.getStringAfter(bPrice,">","");
				bPrice = FilterUtil.getStringBefore(bPrice,"<","");
			}
			
			
			ProductDataBean resultBean = new ProductDataBean();
			if(pdName.contains("(ของหมด")) {
				resultBean.setExpire(true);
				rtn[productCount] = resultBean;
				return rtn;
			}
			
			try {
				JSONObject jData = (JSONObject) new JSONParser().parse(data);
				if(jData != null) {
					String cartStr = String.valueOf((Object) jData.get("cart"));
					if(StringUtils.isNotBlank(cartStr) && NumberUtils.isCreatable(cartStr)) {
						int cartInt = Integer.parseInt(cartStr);
						if(cartInt == 0) {
							resultBean.setExpire(true);
							rtn[productCount] = resultBean;
						}
					}
				}
			} catch(Exception e) {
				logger.error(e);
			}
			
			
			resultBean.setName(pdName);
			resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			
			if (!bPrice.isEmpty()) {
				resultBean.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(bPrice)));
			}
			
			if (productDescription != null && productDescription.length != 0) {
				resultBean.setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						resultBean.setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							resultBean.setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			if (nameDescription != null && nameDescription.length != 0){
				resultBean.setKeyword(nameDescription[0]);
			}
			
			rtn[productCount] = resultBean;
			productCount++;
		}
		
		ProductDataBean[] realRtn = Arrays.stream(rtn).filter(Objects::nonNull).filter(it->!it.isExpire()).toArray(ProductDataBean[]::new);	
		return realRtn;
	}
	
	private static String  genCookie = null;
	
	protected static String postRequest(String currentUrl, String url, String id, String priceAttr, String domain, boolean isHttps) {
		
	 	String origin = (isHttps)?"https":"http" ;
	 	origin += "://" + domain;
		
	 	String param = "productID="+id+"&attributeSet=%5B%22"+priceAttr+"%22%5D&type=getAttributeGroupID";
	 	
	 	try {
	 		if(genCookie==null) {
	 			 genCookie =  genCookie(currentUrl);
	 		}
	 		String[] result = httpRequestWithStatusIgnoreCookies(url,"UTF-8",origin,param,true);
	 		return result[0];
	 	}catch (Exception e) {
	 		logger.error("Error",e);
		}
	 	return null;
	}
	
	
	public static String[] httpRequestWithStatusIgnoreCookies(String url, String charset,String origin,String param,boolean redirectEnable) {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(HTTP_LOCAL_CONFIG);
		httpPost.addHeader("User-Agent", USER_AGENT);
		StringWriter writer = new StringWriter();
		writer.append(param);
 	    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
 	    httpPost.addHeader("Accept", "*/*");
 	    httpPost.addHeader("Origin", origin);
 	    httpPost.addHeader("Referer","https://www.woodyshop2016.com/product/13623/t2311vsam");
 	    httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
 	    httpPost.addHeader("Cookie",genCookie);

		try {
			StringEntity userEntity = new StringEntity(writer.getBuffer().toString());
			httpPost.setEntity(userEntity);
		} catch (UnsupportedEncodingException e1) {
			logger.error("Error",e1);
		}

		HttpEntity entity = null;
		boolean pass = false;
		int countRun = 0;
		while(!pass&&!Thread.currentThread().isInterrupted()&&countRun < 3) {
	    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
	    		CloseableHttpResponse response = httpclient.execute(httpPost)){
	    		
	    		int status = response.getStatusLine().getStatusCode();
	    		entity = response.getEntity();
	    		if(status == HttpURLConnection.HTTP_OK) {
	    			try(InputStream is = entity.getContent();
	    				InputStreamReader isr = new InputStreamReader(is, charset);
		    			BufferedReader brd = new BufferedReader(isr)) {
	    				StringBuilder rtn = new StringBuilder();
	    				String line = null;
	    				while((line = brd.readLine()) != null)
	    					rtn.append(line);
	    				pass = true;
	    				return new String[]{rtn.toString(), String.valueOf(status)};
	    			}
	    		}else{
	    			pass = true;
					return new String[]{null, String.valueOf(status)};
				}
	    		
	    	}catch (ConnectTimeoutException e) {
	    		try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					logger.info(e);
				}
				pass = false;
				logger.error("TimeOut Exception !! "+countRun);
			}catch (Exception e) {
	    		logger.error("Error",e);
	    	} finally {
	    		if(httpPost != null)
					httpPost.releaseConnection();
	    		EntityUtils.consumeQuietly(entity);
	    		countRun++;
	    	}
		}
    	return null;
    }
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null){
				try { in.close(); } catch (IOException e) {}
			}
		}
	}
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	
	private static String genCookie(String currentUrl) {
		enableUseProxy();
		String genCookie = "";
		HttpGet httpGet = new HttpGet(currentUrl);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", USER_AGENT);
		@SuppressWarnings("unused")
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		Header[] header = response.getHeaders("Set-Cookie");
			//Map<String,List<String>> cookies = conn.getHeaderFields();
			//List<String> setCookie =  cookies.get("Set-Cookie");
			String pid = "pid=";
			String PHPSESSID = "PHPSESSID=";
			String cookie = "";
			for(Header h:header) {
				cookie += h.getValue();
			}


			pid += FilterUtil.getStringBetween(cookie,"pid=",";")+";";
			PHPSESSID += FilterUtil.getStringBetween(cookie,"PHPSESSID=",";")+";";
			genCookie = pid+PHPSESSID;
			
			return genCookie;
    	}catch (Exception e) {
    		logger.error("Error",e);
		}
    	return null;
	}
}
