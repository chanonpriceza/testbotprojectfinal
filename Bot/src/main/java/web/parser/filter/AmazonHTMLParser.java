package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AmazonHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name", new String[] { "" });
		map.put("price", new String[] { "<tr id=\"priceblock_ourprice_row\">" });
		map.put("basePrice", new String[] { "<span class=\"priceBlockStrikePriceString a-text-strike\">" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1#title");
			productName = e.html();
			productName = FilterUtil.toPlainTextString(productName);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("span#priceblock_ourprice");
			productPrice = e.html();
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.getStringAfter(productPrice,"$",productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		if("0".equals(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}

		return new String[] {productPrice};
	}
	
	@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult.length > 0) {
//			String cutword = FilterUtil.getStringBetween(evaluateResult[0],"<script type=\"text/javascript\">","</script>");
//			productDesc = evaluateResult[0].replace(cutword,"");
//			productDesc = FilterUtil.toPlainTextString(productDesc);
			productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"description\" content=\"","\"");
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-old-hires=\"","\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" id=\"ASIN\" name=\"ASIN\" value=\"","\"");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    	}
    	return new String[] {realProductId};
	}

}
