package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class JapanAddictHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1 class=\"product-title entry-title\">"});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{"<p class=\"price product-page-price price-on-sale\">"});
			map.put("description", new String[]{"<div class=\"panel entry-content active\" id=\"tab-description\">"});
			map.put("pictureUrl", new String[]{""});
			map.put("expire", new String[]{""});
			
			return map;
		}
	   
	   @Override
	   public String[] getProductExpire(String[] evaluateResult) {
		   if(evaluateResult.length > 0) {
			   if(evaluateResult[0].contains("<p class=\"stock out-of-stock\">")) {
				   return new String[]{"true"};
			   }
		   }
		   return new String[]{"false"};
	   }
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length > 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price product-page-price \">", "</p>");
	    		if(productPrice == "") {
		    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price product-page-price price-on-sale\">", "</p>");
		    		productPrice = FilterUtil.getStringAfter(productPrice, "</del>", "");
	    		}
	    		productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
	    		productPrice = StringEscapeUtils.unescapeHtml4(productPrice);
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }	    	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String basePrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		basePrice = FilterUtil.getStringBetween(evaluateResult[0], "</span>", "</span>");
	    		basePrice = StringEscapeUtils.unescapeHtml4(basePrice);
	    		basePrice = FilterUtil.toPlainTextString(basePrice);
	    		basePrice = FilterUtil.removeCharNotPrice(basePrice);
	        }	    	
			return new String[] {basePrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		
	    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
	    		productDesc = FilterUtil.toPlainTextString(productDesc);
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length > 0) {	
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"first slide woocommerce-product-gallery__image\">", "</a>");
	    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "href=\"", "\"");
	    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
	        }
	    	return new String[] {productPictureUrl};
		}
				
}
