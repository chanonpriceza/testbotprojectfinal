package web.parser.filter;


import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class NexsglassesHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-7 product-shop\">"});
		map.put("pictureUrl", new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-5 product-img-box\">"});
		map.put("expire", new String[]{"<p class=\"availability in-stock\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = evaluateResult[0];
			productName = FilterUtil.getStringBetween(productName, "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = evaluateResult[0];
    		if(productPrice.contains("<p class=\"special-price\">")){
    			productPrice = FilterUtil.getStringBetween(productPrice, "<p class=\"special-price\">", "</p>");
    			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\"", "</span>");
    			productPrice = FilterUtil.getStringBetween(productPrice, ">", "บาท");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
			bPrice = FilterUtil.getStringBetween(bPrice, "<span class=\"price\"", "</span>");
			bPrice = FilterUtil.getStringBetween(bPrice, ">", "บาท");
			
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<h4>รายละเอียดโดยย่อ</h4>", "</ul>");
    		if(productDesc.isEmpty()){
    			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<h4>รายละเอียดโดยย่อ</h4>", "</p>");
    		}
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        } 
     	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length == 1){
			return null;
		}else{
			return new String[]{ "true"};
		}
	}
	    
}