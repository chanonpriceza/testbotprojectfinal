package web.parser.filter;

import bean.ProductDataBean;
import engine.BaseConfig;

public class TemplateLazadaJSCheckManyNameMerchantHTMLParser extends TemplateLazadaJSIncludeIdHTMLParser{

	@Override
	protected ProductDataBean getResult(String sellerName, String productName, String productPrice, String productBasePrice, 
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {
		if(BaseConfig.CONF_MERCHANT_NAME.contains(sellerName)) {
			if(productName.equals("Sudio หูฟังบลูทูธ รุ่น TOLV")) {
				productImage = "https://th-test-11.slatic.net/p/2335fbacb145ecf49618b23b07a36f81.jpg";
			}
			return gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
		}
		return null;
	}
	
}
