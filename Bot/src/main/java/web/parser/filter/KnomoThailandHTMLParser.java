package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KnomoThailandHTMLParser extends DefaultHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-title\" itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("basePrice", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div id=\"rte\" class=\"clearfix\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div id=\"product-gallery\">"});
		map.put("expire", new String[]{""});
		return map;
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			if(evaluateResult[0].contains("<div class=\"sold-out\">")) {
				return new String[] {"true"};
			}
		}
		return new String[] {"false"};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		if(evaluateResult.length > 0) {	
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }
		return new String[] {productBasePrice};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "itemprop=\"price\" content=\"", "\"");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
}