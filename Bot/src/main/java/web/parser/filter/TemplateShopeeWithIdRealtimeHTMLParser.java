package web.parser.filter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class TemplateShopeeWithIdRealtimeHTMLParser extends TemplateShopeeRealtimeHTMLParser{
	
	private static Logger logger = LogManager.getRootLogger();

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String json = mpdBean.getProductName()[0];
    	
    	try {
			JSONObject obj = (JSONObject) new JSONParser().parse(json);
			if(obj == null) return null;
			
			JSONObject itemObj = (JSONObject) obj.get("item");
			if(itemObj == null) return null;
			
			String productId = itemObj.get("itemid").toString();
			String tempPrice = StringUtils.defaultString(String.valueOf(itemObj.get("price")), "");
			String resultName = StringUtils.defaultString(String.valueOf(itemObj.get("name")), "");
			String resultPrice = "0";

			if(NumberUtils.isCreatable(tempPrice)) {
				int priceFull = Integer.parseInt(tempPrice);
				int priceReal = priceFull / 100000;
				if(priceReal > 0) resultPrice = String.valueOf(priceReal);
			}
			
			ProductDataBean resultBean = new ProductDataBean();
			resultBean.setPrice(FilterUtil.convertPriceStr(resultPrice));
			resultBean.setName(resultName + "(" + productId + ")");
			return new ProductDataBean[] {resultBean};
			
		} catch (Exception e) {
			logger.error(e);
		}
    	return null;
	}
	    
}
