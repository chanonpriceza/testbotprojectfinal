package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

public class ADayFreshHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("description", new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		String	productPrices = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if (StringUtils.isBlank(productPrices)) {
    			productPrices = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		}
    			productPrice = FilterUtil.toPlainTextString(productPrices);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img itemprop=\"image\" src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
