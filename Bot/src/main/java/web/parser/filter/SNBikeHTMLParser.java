package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SNBikeHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 align=\"left\">"});
		map.put("price", new String[]{"<span class=\"price-item\">"});
		map.put("basePrice", new String[]{"<span class=\"price-item\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("<span class=\"price-normal\">")) {
    			productPrice = FilterUtil.getStringAfter(evaluateResult[0], "</span>", "");
    			productPrice = FilterUtil.getStringAfter(productPrice, "</span>", "");
    		} else {
    			productPrice = evaluateResult[0];
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	} 
    	if(productPrice.trim().equals("0")) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";    	
		   
		if (evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-normal\">", "</span>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		} 
		return new String[] {basePrice};
	}
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan=\"4\"><div align=\"left\"><img src=", "</td>");
    		productDesc = FilterUtil.getStringAfter(productDesc, ">", productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan=\"4\"><div align=\"left\"><img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}