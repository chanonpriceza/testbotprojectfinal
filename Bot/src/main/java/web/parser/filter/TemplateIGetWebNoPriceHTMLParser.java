package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
public class TemplateIGetWebNoPriceHTMLParser extends TemplateIGetWebHTMLParser{
	
	
	@Override
	public String getCharset() {
		// TODO Auto-generated method stub
		return "TIS-620";
	}

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"entry-full-title prodtc-detail-title\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"product_detail\">"});
		map.put("pictureUrl", new String[]{"<div  id=\"photoshow\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		   		
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"num\" id=\"product_price\">", "</span>");
    		
    		if(tmp.contains("-"))
    		tmp = FilterUtil.getStringBefore(tmp, "-", "");
    		
    		
    		if(tmp.contains("โทรสอบถาม")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else {
    			tmp = FilterUtil.getStringAfter(tmp, "ลดเหลือ", tmp);    		
        		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		} 
    		
    		if(productPrice.equals("")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "<p class=\"title_detail product_description\">", "");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
		
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if(productPrice == null || productPrice.length == 0 || 
				productPrice[0] == null || productPrice[0].trim().length() == 0	) {			
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		} else {
			if(productPrice[0].equals("0.00")) {
				rtn[0].setPrice(BotUtil.CONTACT_PRICE);
			} else {
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			}
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}


		return rtn;
	}
	
}
