package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class SmartCamHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"product-image  \">"});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{""});

		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("title");
			productName = FilterUtil.toPlainTextString(e.text());		
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	   	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"NmPrice\">", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}
		return new String[] {productPrice};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-specs\">", "</div>");
			product = FilterUtil.toPlainTextString(product);
		}
		return  new String[] {product};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String product = "false";
		if(evaluateResult.length == 1) {	
			String check = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"availability out-of-stock\">", "</p>");
			if(check.contains("สินค้าหมด")) {
				product = "true";			
			}
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return  new String[] {product};
	}
	


}