package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KtwHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "UTF-8";
	}
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"product-price discounted-price\">"});
		map.put("basePrice", new String[]{"<div class=\"old-product-price\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"gallery\">"});
		map.put("expire", new String[]{"<span class=\"valueoff\">"});
		
		return map;
	}
    
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length > 0){
    		return new String[]{ "true"};
    	}
    	return null;
    }
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";	
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);		
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = ""; 
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\"", "</span>");
    		if(tmp.isEmpty()){
    			return null;
    		}
    		tmp = "<span   itemprop=\"price\"" + tmp;
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String pbPrice = ""; 
		if (evaluateResult.length == 1) {
			pbPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			pbPrice = FilterUtil.removeCharNotPrice(pbPrice);
		}		
		return new String[] {pbPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
}