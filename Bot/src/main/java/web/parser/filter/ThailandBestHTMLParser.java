package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThailandBestHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	CookieManager cookieManager = new CookieManager();
    	CookieHandler.setDefault(cookieManager);
		map.put("name", 		new String[]{"<span class=\"h1\">"});
		map.put("price", 		new String[]{"<div class=\"product-shop\">"});
		map.put("basePrice", 	new String[]{"<div class=\"product-shop\">"});
		map.put("description", 	new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-image\">"});
		map.put("realProductId", new String[]{"<div class=\"sku-product\">"});
		//map.put("expire", new String[]{"<div class=\"product-shop\">"});
		return map;
	}
    
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = ""; 
		if (evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productName };
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";   
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\"", "</div>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\"", "/span>");
				productPrice = FilterUtil.getStringBetween(productPrice, "\">", "<");			
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
    		productBasePrice = FilterUtil.getStringBetween(productBasePrice, "<span class=\"price\"", "/span>");
    		productBasePrice = FilterUtil.getStringBetween(productBasePrice, "\">", "<");		
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		if(evaluateResult.length == 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<span>", "</span>");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	    return new String[]{realProductId}; 
   }
	
	
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
	   	if(evaluateResult[0].contains("ขออภัยสินค้าหมด")) {
	   		expire = "true";
	   	}
	    return new String[]{expire};
	   
	}


	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0 && realProductId[0] != "") {
			productName[0] = productName[0] + " (" + realProductId[0]+")";
		}
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		


		rtn[0].setPictureUrl(productPictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);


		return rtn;
	}
	
	@Override
	public ProductDataBean[] parse(String url) {
		try {
			String checkResult = checkUrlBeforeParse(url);
			if(checkResult != null && checkResult.equals("delete"))
				return processHttpStatus(null);
			
			String targetUrl = url;
	        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
	            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"})) {
	            	targetUrl = BotUtil.encodeURL(url);
	            }
	        }
			
			String[] htmlContent = null;

			htmlContent = httpRequestWithStatus(targetUrl, getCharset(), true);
			
			if (htmlContent == null || htmlContent.length != 2)
				return null;
			
			String httpStatus = htmlContent[1];
			if(httpStatus.equals(String.valueOf(HttpURLConnection.HTTP_BAD_REQUEST)))
				return processHttpStatus(htmlContent[1]);
			
			String html = htmlContent[0];
			if(html == null)
				return processHttpStatus(httpStatus, false);
			
			Map<String, String[]> config = getConfiguration();
			if (config == null)
				return null;
			
			String[] productName = null;
			String[] productPrice = null;
			String[] productUrl = null;
			String[] productDescription = null;
			String[] productPictureUrl = null;
			String[] merchantUpdateDate = null;
			String[] productNameDescription = null;
			String[] productExpire = null;
			String[] realProductId = null;
			String[] productBasePrice = null;
			String[] productUpc = null;
			String[] data = null;
			
			String[] result = evaluate(html, config.get("expire"));
			if (result != null) {
	    		productExpire = getProductExpire(result);                
	    		if(productExpire != null && productExpire.length == 1 && productExpire[0].equals("true"))
	    			return processHttpStatus(httpStatus);
	    	}
			
			result = evaluate(html, config.get("name"));
			if (result != null) {
				productName = getProductName(result);
				productName = FilterUtil.formatHtml(productName);
				productName = FilterUtil.removeSpace(productName);
			}
			
			result = evaluate(html, config.get("price"));
			if (result != null) {
				productPrice = getProductPrice(result);
				productPrice = FilterUtil.removeSpace(productPrice);
			}
			result = evaluate(html, config.get("url"));
			if (result != null) {
				productUrl = getProductUrl(result);
				productUrl = FilterUtil.removeSpace(productUrl);
			}
			result = evaluate(html, config.get("description"));
			if (result != null) {
				productDescription = getProductDescription(result);
				productDescription = FilterUtil.formatHtml(productDescription);
				productDescription = FilterUtil.removeSpace(productDescription);
			}
			result = evaluate(html, config.get("pictureUrl"));
			if (result != null) {
				productPictureUrl = getProductPictureUrl(result);				
				productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
			}
			result = evaluate(html, config.get("merchantUpdateDate"));
			if (result != null) {
				merchantUpdateDate = getProductMerchantUpdateDate(result);
				merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
				merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
			}
			result = evaluate(html, config.get("nameDescription"));
			if (result != null) {
				productNameDescription = getProductNameDescription(result);
				productNameDescription = FilterUtil.removeSpace(productNameDescription);
			}
						
			result = evaluate(html, config.get("realProductId"));
			if (result != null) {
				realProductId = getRealProductId(result);
				realProductId = FilterUtil.removeSpace(realProductId);
			}
			
			result = evaluate(html, config.get("basePrice"));
			if (result != null) {
				productBasePrice = getProductBasePrice(result);
				productBasePrice = FilterUtil.removeSpace(productBasePrice);
			}
			
			result = evaluate(html, config.get("upc"));
			if (result != null) {
				productUpc = getProductUpc(result);
				productUpc = FilterUtil.removeSpace(productUpc);
			}
			
			result = evaluate(html, config.get("data"));
			if (result != null) {
				data = getData(data);
				data = FilterUtil.removeSpace(data);
			}
			
			ProductDataBean[] productDataBean = null;
			FILTER_TYPE filterType = getFilterType();
			MergeProductDataBean mpdBean = new MergeProductDataBean();
			mpdBean.setProductName(productName);
			mpdBean.setProductPrice(productPrice);
			mpdBean.setProductUrl(productUrl);
			mpdBean.setProductDescription(productDescription);
			mpdBean.setProductPictureUrl(productPictureUrl);
			mpdBean.setMerchantUpdateDate(merchantUpdateDate);
			mpdBean.setProductNameDescription(productNameDescription);
			mpdBean.setRealProductId(realProductId);
			mpdBean.setCurrentUrl(url);
			mpdBean.setProductBasePrice(productBasePrice);
			mpdBean.setProductUpc(productUpc);
			mpdBean.setData(data);
			
			if (filterType.equals(FILTER_TYPE.DEFAULT)) {
	        	productDataBean = mergeProductData(mpdBean);   	   	
	        } else if (filterType.equals(FILTER_TYPE.PRODUCTLIST)) {
	        	result = evaluate(html, config.get("allData"));
				if (result != null) {
					productDataBean = getAllProductData(result);
					for (int i = 0; i < productDataBean.length; i++) {
						if(productDataBean[i] == null)
							continue;
						String name = FilterUtil.formatHtml(productDataBean[i].getName());
						name = FilterUtil.removeSpace(name);
						productDataBean[i].setName(name);
						
						String description = FilterUtil.formatHtml(productDataBean[i].getDescription());
						description = FilterUtil.removeSpace(description);
						productDataBean[i].setDescription(description);
						
						productDataBean[i].setPictureUrl(FilterUtil.removeSpace(productDataBean[i].getPictureUrl()));
					}
				}	        	                
	        	productDataBean = mergePriceListProductData(productDataBean, mpdBean);
	        }
			
			if(isEmpty(productDataBean))
				productDataBean = new ProductDataBean[]{ new ProductDataBean()};
			productDataBean[0].setHttpStatus(httpStatus);
			
			return productDataBean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus) {
		return processHttpStatus(httpStatus, true);
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
		ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
		rtn[0].setHttpStatus(httpStatus);
		rtn[0].setExpire(expire);
		return rtn;
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("Cookie","SPSI=405ff631b15403c69537bf149b78c968; spcsrf=23a060f092c1c5239a22c20a10bad752; frontend=c5oln2onbevb3tjig30p2bpvc5; frontend_cid=4oymXA8PCPIOJFKI; sp_lit=DuSLvXCO0yC0pOKD9f2kbA==; PRLST=cq; UTGv2=h41ed3bbcd13311ca5f396428f74ee1e8e22; adOtr=ffKI415b351");
			conn.setConnectTimeout(100000);
			conn.setReadTimeout(1000000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
}
