package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class LadyidolHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span class=\"product-title\">"});
		map.put("price", new String[]{"<td bgcolor=\"#DCC991\">"});
		map.put("description", new String[]{"<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"});
		map.put("pictureUrl", new String[]{"<th bgcolor=\"#FFFFFF\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    
		
		if(evaluateResult.length == 1){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], ":</strong> <span class=\"style2\">", "</span>");
		}
		
		if("".equals(productPrice)){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<s><span class=\"text4\">", "</span>");
		}
		
    	if (evaluateResult.length == 1) {
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		if("0.0".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(productPictureUrl == null || "".equals(productPictureUrl)){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
		}
		
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		
    	return new String[] {productPictureUrl};
	}
}