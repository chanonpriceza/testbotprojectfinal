package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class RosewhoesaleHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<i class=\"costPrice ml20 fl\">"});
		map.put("description", new String[]{"<div class=\"xxkkk\">"});
		map.put("pictureUrl", new String[]{"<div class=\"goodImg_b pr\" id=\"js_jqzoom\">"});
		map.put("realProductId", new String[]{""});
		
		map.put("expire", new String[]{""});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		String color = "";
	            
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"twitter:title\" content=\"","\">");			
			color = FilterUtil.getStringBetween(evaluateResult[0],"<li class=\"item selected\">",">");
			color = FilterUtil.getStringBetween(color,"title=\"","\"");
			productName = productName +" - "+color; 
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   

    	if (evaluateResult.length > 0) {	
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"twitter:data1\" content=\"", "\"");
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<strong data-orgp=\"", "\"");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length >= 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "id=\"goods_id\" value=\"", "\"");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	   	
	    return new String[]{realProductId}; 
   }

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productId = mpdBean.getRealProductId();
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0] + " ("+productId[0] + ")");
		rtn[0].setRealProductId(productId[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setUrl(currentUrl);
		if(StringUtils.isNotBlank(productBasePrice[0])){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}
		
		return rtn;
	}
}