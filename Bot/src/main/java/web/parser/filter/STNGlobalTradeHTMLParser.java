package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class STNGlobalTradeHTMLParser extends DefaultHTMLParser {
	public String getCharset() {
		return "UTF-8";
	}
	 
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"product-detail-price-original no-special-price\">", "</p>");
    		}
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-detail-price-original no-special-price\">", "</div>");
    		}
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"product-detail-price-original \">", "</p>");
    		}
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-detail-price-original\">", "</div>");
    		}
    		
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		if(StringUtils.isBlank(productPrice)) 
    			productPrice =  BotUtil.CONTACT_PRICE_STR;

        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"description\">", "<div class=\"product-detail-bottom\">");
    		productDesc = FilterUtil.getStringAfter(productDesc, "รายละเอียด</stron", "");
    		productDesc = FilterUtil.getStringBetween(productDesc, "</strong>", "</p>");
    		productDesc = FilterUtil.replaceHtmlCode(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\"lazyload\" data-src=\"", "\"");
    		if(productPictureUrl.length() == 0){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"image-wrapper\">", "title=\"");
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<a href=\"", "\"");
    		}
        }
    	return new String[] {productPictureUrl};
	}  
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String isExpire = "false";
		if(evaluateResult.length > 0) {
			String expire = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>", "</span>");
			if(expire.contains("สินค้าหมด")){
				isExpire = "true";
			}
			if(expire.contains("สินค้าหมด stock")){
				isExpire = "true";
    		}
			
			if(expire.contains("(Sold out)")){
				isExpire = "true";
			}
		}
		return new String[] {isExpire};
	}
}
