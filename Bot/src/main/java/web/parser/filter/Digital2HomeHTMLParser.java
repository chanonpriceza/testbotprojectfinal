package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class Digital2HomeHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<h1 class=\"product-title product_title entry-title\">"});
		map.put("price", 			new String[]{"<p class=\"price product-page-price \">", "<p class=\"price product-page-price price-on-sale\">"});
		map.put("basePrice", 		new String[]{"<p class=\"price product-page-price \">", "<p class=\"price product-page-price price-on-sale\">"});
		map.put("description", 		new String[]{""});
		map.put("pictureUrl", 		new String[]{""});
		map.put("realProductId", 	new String[]{"<span class=\"sku\">"});
		map.put("expire", 			new String[]{"<p class=\"stock out-of-stock\">"});
		
		return map;
	}
	
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	String exp = "false";
    	if(evaluateResult.length == 1) {			
			if(evaluateResult[0].contains("สินค้าหมดแล้ว"))	{
				exp = "true";
			}
		}
    	return new String[] {exp};
    }
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "</ins>");
    		if(productPrice.isEmpty()){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price product-page-price \">", "</p>");
    		}
    		productPrice = FilterUtil.getStringAfter(productPrice, "&#3647;", productPrice);
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = "9999999";
			}
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";    	
		if (evaluateResult.length == 1) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"woocommerce-Price-amount amount\">", "</del>");
			bPrice = FilterUtil.getStringAfter(bPrice, "&#3647;", bPrice);
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0 ) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String productId = "";
    	if (evaluateResult.length == 1) {
    		productId = FilterUtil.toPlainTextString(evaluateResult[0]);
        }
    	return new String[] {productId};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] bPrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		String[] productId = mpdBean.getRealProductId();
		
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String name = productName[0];
		if(productId != null && productId.length != 0){
			name += " ("+ productId[0] +")" ;
			rtn[0].setRealProductId(productId[0]);
		}
		rtn[0].setName(name);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (bPrice != null && bPrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				try {
					URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
					rtn[0].setPictureUrl(url.toString());
				} catch (MalformedURLException e) {

				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		return rtn;
	}    
}