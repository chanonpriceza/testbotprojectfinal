package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class TemplateLnwshopNoCatIncludeIdHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"headerText\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<tr class=\"oldpriceTR\">"});
		map.put("description", new String[]{"<div id=\"detail\" class=\"tabPanel mceContentBody\" itemprop=\"description\">","<tr class=\"descTR\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("nameDescription", new String[]{"<tr class=\"categoryTR\">"});
		map.put("upc", new String[]{"<tr class=\"barcodeTR\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<tr class=\"priceTR\">", "</tr>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<td class=\"bodyTD\">", "</td>");
    		if(productPrice.isEmpty()){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"productPrice\">", "</div>");
    			
    		}
    		if(!productPrice.isEmpty()){
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		if(productPrice.contains("-")) {
	    			productPrice = FilterUtil.getStringBefore(productPrice, "-", productPrice);
	    		}
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 
	    		if("0.00".equals(productPrice)) {
	        		productPrice = BotUtil.CONTACT_PRICE_STR;
	        	}
    		}
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<s class=\"price_old\">", "</s>");
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = evaluateResult[0].replaceAll("\\<.*?>", " ").replaceAll("\\s+"," ").trim();
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "focus_image_url: '", "'");
    		if(productPictureUrl.isEmpty()){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"productPhoto\">", "</div>");
        		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
    		}
    		if(productPictureUrl.isEmpty()){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"layoutLeft\">", "<div class=\"layoutRight\">");
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<img class=\"productImage\" src=\"", "\"");
    		}
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = null;
		if (evaluateResult.length == 1) {
			upc = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"bodyTD\">", "</td>").trim();
			upc = FilterUtil.toPlainTextString(upc);
			if(!StringUtils.isNumeric(upc) || upc.length() != 13){
				upc = null;
			}
		}
		return new String[] { upc};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "";
		if(evaluateResult[0].contains("itemprop=\"availability\" content=\"out_of_stock\">")||evaluateResult[0].contains("<span>ขออภัย สินค้าหมดค่ะ</span>")){
			exp = "true";
		}
		return new String[] { exp};
	}
	
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	productId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product_id\" value=\"", "\"").trim();
    	return new String[]{ productId};
    }
    
    @Override
	public String[] getProductNameDescription(String[] evaluateResult) {
    	String productNameDescription = "";
    	if(evaluateResult.length > 0){
    		productNameDescription = FilterUtil.getStringAfter(evaluateResult[0],"<td class=\"bodyTD\">",evaluateResult[0]);
    		productNameDescription = FilterUtil.toPlainTextString(productNameDescription);
    	}
		return new String[]{productNameDescription};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();
		String[] upc = mpdBean.getProductUpc();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || productPrice == null || productPrice.length == 0) {
			return null;
		}

		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
			}
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		if (nameDescription != null && nameDescription.length != 0){
			rtn[0].setKeyword(nameDescription[0]);
		}
		
		if (upc != null && upc.length != 0) {
			rtn[0].setUpc(upc[0]);
		}
		
		return rtn;
	}
    
}
