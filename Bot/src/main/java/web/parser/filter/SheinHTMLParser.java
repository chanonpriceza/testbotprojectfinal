package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SheinHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		new String[]{"<h1 class=\"name\">"});
		map.put("price", 		new String[]{""});
		map.put("basePrice",    new String[]{""});
		map.put("description", 	new String[]{"<div class=\"desc-con j-desc-con desc-wrap-default\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"img-ctn\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  
		String designName ="";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = designName + productName;
			
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "info\":{\"price\"", "</script>");
			productPrice = FilterUtil.getStringBetween(productPrice, "\"salePrice\":{\"amount\":\"", "\"");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if(productPrice.equals("0")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "info\":{\"price\"", "</script>");
			bPrice = FilterUtil.getStringBetween(bPrice, "{\"retailPrice\":{\"amount\":\"", "\"");
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
			
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if(evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-src=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	
	 public String[] getRealProductId(String[] evaluateResult) {
		 String pdId = "";
			if (evaluateResult.length > 0 ) {
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "goods_id:'", "'");
	    	}
			return new String[] { pdId};
	 }
	 
	 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			
			String[] productId = mpdBean.getRealProductId();
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productBasePrice = mpdBean.getProductBasePrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			
			if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0] + " ("+productId[0] + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			
			if(StringUtils.isNotBlank(productBasePrice[0])){
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			}
			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				rtn[0].setPictureUrl("https:"+productPictureUrl[0]);
			}
			
			return rtn;
		}
	 
}
