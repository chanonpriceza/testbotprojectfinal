package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class YiipoonHTMLParser extends DefaultHTMLParser{
	public String getCharset() {
		return "TIS-620";
	}
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"pro_name\">"});
		map.put("price", new String[]{"<div class=\"cart_box_price\">"});
		map.put("basePrice"		, new String[]{"<div class=\"cart_box_price\">"});
		map.put("description", new String[]{"<div class=\"pro_detail_box\" style=\"text-align:justify;\">"});
		map.put("pictureUrl", new String[]{"<div class='zoom' style=\" float:left; display:block; width:350px; height:330px; vertical-align:middle; display: table-cell; text-align: center;vertical-align: middle;\" >"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = evaluateResult[0];
    		if(productPrice.contains("<div class=\"subproduct_price\">")){
    			productPrice = FilterUtil.getStringBetween(productPrice, "<h2 >", " ฿</h2>");
    		}else{
    			productPrice = FilterUtil.getStringBetween(productPrice, "<h2>", "</h2>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		if("0.0".equals(productPrice) || productPrice.trim().length() == 0) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length == 1){
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<h3>ปกติ", "</h3>");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);		
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src='", "'");
        }
    	
    	return new String[] {productPictureUrl};
	}
	    
}
