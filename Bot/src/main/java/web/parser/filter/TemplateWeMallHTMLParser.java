package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TemplateWeMallHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1 class=\"product-name\" itemprop=\"name\">" });
		map.put("price", new String[] { "" });
		map.put("description", new String[] { "<div class=\"box-more-product-detail\">" });
		map.put("pictureUrl", new String[] { "" });
		return map;
	}
	
		public String[] getProductName(String[] evaluateResult) {
			String productName ="";
			if(evaluateResult.length==1){
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
	    	if (evaluateResult.length ==1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<meta itemprop=\"price\" content=\"","\"");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }		
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
	    	
	    	if(evaluateResult.length == 1) {

	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);

	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] desc = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();
			
			String trackingUrl = currentUrl + "||https://wefreshapp.onelink.me/a1mp?pid=priceza_int&is_retargeting=true&af_click_lookback=7d&af_reengagement_window=30d&clickid=r-th--300395&c=DataFeed&af_adset=ProductList&af_ad=Product";
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0]);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			rtn[0].setDescription(desc[0]);
			rtn[0].setPictureUrl(productPictureUrl[0]);
			rtn[0].setUrl(trackingUrl);
			rtn[0].setUrlForUpdate(currentUrl);

			return rtn;
		}
}
