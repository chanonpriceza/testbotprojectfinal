package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AkibkkHTMLParser extends DefaultHTMLParser {

	public String getCharset() {
		return "TIS-620";
	}

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1 class=\"entry-full-title prodtc-detail-title\">" });
		map.put("price", new String[] { "<span class=\"num\" id=\"product_price\" style=\"margin:0; padding:0;\">" });
		map.put("description",new String[] { "<div class=\"desc_\">" });
		map.put("pictureUrl", new String[] { "<div id=\"photoshow\">" });
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = "";
		if (evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length == 1) {
			if(!evaluateResult[0].contains("<font color=\"red\">")) {
				productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}else {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<font color=\"red\">","</font>");
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
		}
		
		if(productPrice.isEmpty()) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		return new String[] { productPrice };
	}

	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length >= 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] { productPrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0],"src=\"","\"");
		}
		return new String[] { productPictureUrl };
	}

}
