package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SMTVHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<td class='head1'>" });
		map.put("price", new String[] { "<font color=\'red\' face=\'Times New Roman\' size=\'3\'>" });
		map.put("basePrice", new String[] { "<font face='Times New Roman'>" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "<td valign='top'  width='20%' class='text3' align='center'>" });
		map.put("realProductId",new String[] { "<td valign='top'  width='20%' class='text3' align='center'>" });
		map.put("expire", new String[] { "" });

		return map;
	}
	
	public String getCharset() {
		return "tis-620";
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = "";

		if (evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";

		if (evaluateResult.length >= 1) {
			productPrice = evaluateResult[0];
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		if(StringUtils.isBlank(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}

		return new String[] { productPrice };
	}

	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if (evaluateResult.length >= 1) {
				bPrice = evaluateResult[0];
				bPrice = FilterUtil.toPlainTextString(bPrice);
				bPrice = FilterUtil.removeCharNotPrice(bPrice);
			
		}
		return new String[] { bPrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";

		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<P><STRONG><U>", "</td>");
			if (StringUtils.isBlank(productDesc)) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p><strong><u>", "</td>");
			}
			if (StringUtils.isBlank(productDesc)) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<P><U><STRONG>", "</td>");
			}
			if(StringUtils.isBlank(productDesc)) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดสินค้า", "</td>");
			}
			if (StringUtils.isBlank(productDesc)) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'>", "</td>");
			}
			productDesc = FilterUtil.toPlainTextString(productDesc);
			productDesc = FilterUtil.removeHtmlTag(productDesc);
			productDesc = productDesc.replaceAll("[\\�]", "");
		}
		return new String[] { productDesc };
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		
		String url = "";
		
		if(evaluateResult.length > 0) {
			
			url = FilterUtil.getStringBetween(evaluateResult[0], "src='","'");
			
		}
		return new String[] {url};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String id = "";
		
		if(evaluateResult.length > 0) {
			
			id = FilterUtil.getStringBetween(evaluateResult[0], "src='","'");
			id = FilterUtil.getStringBetween(id,"tn_",".jpg");
			
		}
		return new String[] {id};
	}
	
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String name = productName[0]+" ("+realProductId[0]+")";
		rtn[0].setName(name);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
	
}
