package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CamWorldServiceHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<td width=\"95%\">"});
    	map.put("price", new String[]{"<font color=\"#FF0000\">"});
		map.put("description", new String[]{"<div class=\"productstext\">"});
		map.put("pictureUrl", new String[]{"<div style=\"padding-right:10px; padding-bottom:10px;\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";        
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "<br>", "");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length == 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}	
    	return new String[] {productPictureUrl};
	}
}