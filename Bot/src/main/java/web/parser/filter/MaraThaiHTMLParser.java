package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;



import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MaraThaiHTMLParser extends DefaultHTMLParser {
			

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"showTopic\" >"});
		map.put("price", new String[]{"<div class=\"right-table\" id=\"show_page\">"});
		map.put("description", new String[]{"<div id=\"list-detail\">"});
		map.put("pictureUrl", new String[]{"<div id=\"l-images\" >"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
   		
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ลดเหลือ", "</span></li>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		if(productPrice.trim().length() == 0) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", "</span></li>");
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}