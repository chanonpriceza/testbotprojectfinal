package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class JOHNSONHTMLParser extends DefaultHTMLParser {
	
	public String getCharset() {
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<div class=\"status\">"});
		
		return map;
	}
	
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	String exp = "false";
    	if(evaluateResult.length == 1){
    		if(evaluateResult[0].contains("Out of Stock")){
    			exp = "true";
    		}
    	}
    	return new String[] {exp};
    }
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h2[style=text-align: left]");
			productName = FilterUtil.toPlainTextString(e.html());
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("ins");
		if(evaluateResult.length > 0) {	
			productPrice = e.text();
			if(productPrice.isEmpty()){
				productPrice = doc.select("span.amount").text();
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("del");
		if (evaluateResult.length > 0 ) {
			bPrice = FilterUtil.toPlainTextString(e.text());	
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:description\" content=\"", "\"");
    		if(StringUtils.isNoneBlank(productDesc)){
    			productDesc = FilterUtil.toPlainTextString(productDesc);    	
    		}
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	  
//	@Override
//	public String[] getRealProductId(String[] evaluateResult) {
//		String pdId = "";
//		if(evaluateResult.length > 0){
//			pdId = FilterUtil.getStringBetween(evaluateResult[0], "id=\"product_id\" value=\"", "\"");
//			pdId =  FilterUtil.toPlainTextString(pdId);
//		}
//			return new String[] {pdId};
//	}
}