package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class EcmallHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"single-product-wrapper\">"});
		map.put("description", new String[]{"<div class=\"electro-description clearfix\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-images-wrapper\">"});
		map.put("realProductId", new String[]{""});
		 
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product_title entry-title\">", "</h1>");			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {    
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"electro-price\">", "</span>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"woocommerce-Price-amount amount\">", "<span class=\"woocommerce-Price-currencySymbol\">");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if(evaluateResult.length > 0){
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0],"<input type=\"hidden\" name=\"product_id\" value=\"","\"");
    		if(StringUtils.isBlank(realProductId)){
    			realProductId = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"sku\">","</span>");
    		}
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    	}
		return new String[] {realProductId};
	}
	
	    
}
