package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class IntertoShopHTMLParser extends DefaultHTMLParser{	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"col-sm-6 item-product-detail\">"});
		map.put("price", new String[]{"<div class=\"sale-price\">"});
		map.put("basePrice", new String[]{"<strike>"});
		map.put("description", new String[]{"<div class=\"item-description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<div id=\"PDetail\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"title\">","</div>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = ""; 
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"sale-price\">","</div>");
	    	if(productPrice.contains("-")) {
	    		String[] spiltPrice = productPrice.split("-");
	    		if(spiltPrice!=null&&spiltPrice.length >1) {
	    			productPrice = spiltPrice[0];
	    		}
	    	}
	    	productPrice = FilterUtil.toPlainTextString(productPrice);
	    	productPrice = FilterUtil.removeCharNotPrice(productPrice);	
			
		}
    		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length == 1) {
			bPrice  = evaluateResult[0];
			if(bPrice.contains("-")) {
	    		String[] spiltPrice = bPrice.split("-");
	    		if(spiltPrice!=null&&spiltPrice.length >1) {
	    			bPrice = spiltPrice[0];
	    		}
	    	}
			
    		if(!bPrice.isEmpty()) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);	    			
    		}
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		
    		productDesc  = evaluateResult[0];
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    		
    		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if (evaluateResult.length == 1) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pro_detail\">", "</div>");
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า", "</div>");
			realProductId = FilterUtil.toPlainTextString(realProductId);
        }
		
		return new String[] {realProductId};
	}
	
}