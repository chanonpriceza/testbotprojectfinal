package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class BeautyPlazaHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name", new String[] { "<div class=\"container content\">" });
		map.put("price", new String[] { "<div class=\"product-price float-left\" style=\"line-height:26px;\">" });
		map.put("basePrice", new String[] {"<div class=\"pro_retailprice\">"});
		map.put("description",new String[] { "<div class=\"product-desc detail\">" });
		map.put("pictureUrl", new String[] { "<div class=\"product-img_2016\">" });
		map.put("realProductId", new String[] { "<div class=\"input-num\">" });
		map.put("expire", new String[] { "<div class=\"cart-add\">" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = "";
		String band = "";

		if (evaluateResult.length == 1) {
			band  = FilterUtil.getStringBetween(evaluateResult[0], "<a href='index.php?connect=category&action=", "<br/>");
			band  = FilterUtil.getStringBetween(band, "'>", "</a>");
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"product-name\">","</div>");
			productName = band + " " + productName; 
		}

		return new String[] { productName };
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if (evaluateResult.length >= 1) {
			basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			basePrice = FilterUtil.getStringBetween(basePrice,"ราคา ฿","ประหยัด");
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}

		return new String[] { basePrice };
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";

		if (evaluateResult.length >= 1) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		if (StringUtils.isBlank(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}

		return new String[] { productPrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;

		if (evaluateResult.length >= 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";

		if (evaluateResult.length == 1) {
			realProductId  = FilterUtil.getStringBetween(evaluateResult[0], "<input id=\"nump", "\"");
			realProductId  = FilterUtil.toPlainTextString(realProductId);
		}
		return new String[] { realProductId };
	}

	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
		if(evaluateResult[0].contains("สินค้าหมด")) {
			expire = "true";
		}	
		return new String[] { expire };
	}
	
	

}
