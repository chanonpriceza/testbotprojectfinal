package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class MedvineHTMLParser extends BasicHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.ProductAction__Name-n0xm1-3");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.Grouped__Price-lr6kzj-6");
			if(e.size()>0)
				productPrice = FilterUtil.removeCharNotPrice(e.get(0).html());
			else
				productPrice = FilterUtil.removeCharNotPrice(e.html());
			
			
			if(productPrice.length()==0) {
				e = doc.select("div.ProductAction__RegularPrice-n0xm1-9");
				productPrice = FilterUtil.removeCharNotPrice(e.html());
			}
			
			if(productPrice.length()==0) {
				e = doc.select("div.Grouped__SpecialPrice-lr6kzj-7 ");
				e = e.select("p.price");
				if(e.size()>0)
					productPrice = FilterUtil.removeCharNotPrice(e.get(0).html());
				else
					productPrice = FilterUtil.removeCharNotPrice(e.html());
			}
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPrice = "";    	
		String main = "https://api.medvine.com/pub/media/catalog/product";
		if(evaluateResult.length == 1) {	
			String cut = FilterUtil.getStringBetween(evaluateResult[0],"file\":\"","\"");
			if(cut.length()==0) {
				cut = "https://api.medvine.com/pub/media/catalog/product/placeholder/default/a1501_2.jpg";
			}
			productPrice = main+cut;
					
		}	
		return new String[] {productPrice};
	}
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div[itemprop=offers]");
			e = e.select("span[id^=old-price-]");	
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		Document doc = Jsoup.parse(evaluateResult[0]);
		Element e = doc.select("div.Grouped__Item-lr6kzj-1").first();
		Elements e2 = e.select("p[type=outofStock]");
		boolean ex = e2.html().length()>0;
		return new String[] {String.valueOf(ex)};
	}
 
}