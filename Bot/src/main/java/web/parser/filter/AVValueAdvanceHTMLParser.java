package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AVValueAdvanceHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1 class=\"product_title entry-title\">" });
		map.put("price", new String[] { "<p class=\"price\">" });
		map.put("description", new String[] { "<div class=\"woocommerce-product-details__short-description\">" });
		map.put("basePrice", new String[] { "<p class=\"price\">" });
		map.put("pictureUrl", new String[] { "<figure class=\"woocommerce-product-gallery__wrapper\">" });
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = "";
		if (evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}

		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", ".00");
			productPrice = FilterUtil.getStringAfter(productPrice,"</span>","");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		if(StringUtils.isBlank(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		return new String[] { productPrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult.length == 1) {
			productDesc = evaluateResult[0];
			productDesc = FilterUtil.toPlainTextString(productDesc);

		}

		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}

	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length == 1) {
			productPrice = evaluateResult[0];
			productPrice = FilterUtil.getStringBetween(productPrice, "<del><span class=\"woocommerce-Price-amount amount\">", ".00");
			productPrice = FilterUtil.getStringAfter(productPrice,"</span>","");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] { productPrice };
	}
}
