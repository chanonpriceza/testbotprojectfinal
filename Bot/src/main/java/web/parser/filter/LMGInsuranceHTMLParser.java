package web.parser.filter;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class LMGInsuranceHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String[] name = new String[150];
		if(evaluateResult.length == 1) {	
			
			int count = 0;
					
			String prb = FilterUtil.getStringBetween(evaluateResult[0], "<table width=\"379\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"../../images/th/02-products/00-statute/bg-table.gif\">", "</table>");
			List<String> prbtr = FilterUtil.getAllStringBetween(prb, "<tr>", "</tr>");
			if(prbtr != null && prbtr.size() > 0){
				for (String tr : prbtr) {
					List<String> prbtd = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prbtd != null && prbtd.size() >= 4){
						String productName = FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prbtd.get(1), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim());
						if(StringUtils.isNotBlank(productName)){
							name[count] = "พรบ. " + productName + " แอลเอ็มจีประกันภัย"; count++;
						}
					}
				}
			}
			
			String prakun3 = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"../../images/th/02-products/05-products-super-save/t-interest-3.gif\" width=\"172\" height=\"16\" />", "</table>");
			List<String> prakun3tr = FilterUtil.getAllStringBetween(prakun3, "<tr>", "</tr>");
			if(prakun3tr != null && prakun3tr.size() > 0){
				for (String tr : prakun3tr) {
					List<String> prakun3td = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prakun3td != null && prakun3td.size() >= 2){
						String productName = FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun3td.get(0), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim());
						String productOD = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun3td.get(1), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productName) && StringUtils.isNotBlank(productOD)){
							name[count] = "ประกันภัยรถยนต์ LMG SUPER SAVE 3 " + productName + " ทุนประกัน " + productOD + " ซ่อมอู่ แอลเอ็มจีประกันภัย"; count++;
							name[count] = "ประกันภัยรถยนต์ LMG SUPER SAVE 3 " + productName + " ทุนประกัน " + productOD + " ซ่อมศูนย์ แอลเอ็มจีประกันภัย"; count++;
						}
					}
				}
				
			}
			
			String prakun2 = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"../../images/th/02-products/05-products-super-save/t-interest-4.gif\" width=\"172\" height=\"16\" />", "</table>");
			List<String> prakun2tr = FilterUtil.getAllStringBetween(prakun2, "<tr>", "</tr>");
			if(prakun2tr != null && prakun2tr.size() > 0){
				for (String tr : prakun2tr) {
					List<String> prakun2td = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prakun2td != null && prakun2td.size() >= 2){
						String productName = FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun2td.get(0), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim());
						String productOD = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun2td.get(1), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productName) && StringUtils.isNotBlank(productOD)){
							name[count] = "ประกันภัยรถยนต์ LMG SUPER SAVE 2 " + productName + " ทุนประกัน " + productOD + " ซ่อมอู่"; count++;
							name[count] = "ประกันภัยรถยนต์ LMG SUPER SAVE 2 " + productName + " ทุนประกัน " + productOD + " ซ่อมศูนย์"; count++;
						}
					}
				}
				
			}
			
		}
		
		return name;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String[] price = new String[150];
		if(evaluateResult.length == 1) {	
			
			int count = 0;
				
			String prb = FilterUtil.getStringBetween(evaluateResult[0], "<table width=\"379\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"../../images/th/02-products/00-statute/bg-table.gif\">", "</table>");
			List<String> prbtr = FilterUtil.getAllStringBetween(prb, "<tr>", "</tr>");
			if(prbtr != null && prbtr.size() > 0){
				for (String tr : prbtr) {
					List<String> prbtd = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prbtd != null && prbtd.size() >= 4){
						String productPrice = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prbtd.get(3), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productPrice)){
							price[count] = productPrice; count++;
						}
					}
				}
			}
			
			String prakun3 = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"../../images/th/02-products/05-products-super-save/t-interest-3.gif\" width=\"172\" height=\"16\" />", "</table>");
			List<String> prakun3tr = FilterUtil.getAllStringBetween(prakun3, "<tr>", "</tr>");
			if(prakun3tr != null && prakun3tr.size() > 0){
				for (String tr : prakun3tr) {
					List<String> prakun3td = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prakun3td != null && prakun3td.size() >= 4){
						String productPrice2 = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun3td.get(2), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productPrice2)){
							price[count] = productPrice2; count++;
						}
						String productPrice3 = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun3td.get(3), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productPrice3)){
							price[count] = productPrice3; count++;
						}
					}
				}
				
			}
			
			String prakun2 = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"../../images/th/02-products/05-products-super-save/t-interest-4.gif\" width=\"172\" height=\"16\" />", "</table>");
			List<String> prakun2tr = FilterUtil.getAllStringBetween(prakun2, "<tr>", "</tr>");
			if(prakun2tr != null && prakun2tr.size() > 0){
				for (String tr : prakun2tr) {
					List<String> prakun2td = FilterUtil.getAllStringBetween(tr, "<td", "</td>");
					if(prakun2td != null && prakun2td.size() >= 4){
						String productPrice2 = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun2td.get(2), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productPrice2)){
							price[count] = productPrice2; count++;
						}
						String productPrice3 = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringAfter(prakun2td.get(3), ">", "").replace("&nbsp;", " ").replaceAll("\\s", "").trim()));
						if(StringUtils.isNotBlank(productPrice3)){
							price[count] = productPrice3; count++;
						}
					}
				}
				
			}
			
		}
		
		return price;
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || productName.length != productPrice.length) {
			return null;
		}
		
		int countAll = 0;
		for(String s : productName){
			if(StringUtils.isNotBlank(s)){
				countAll++;
			}
		}
		
		ProductDataBean[] rtn = new ProductDataBean[productName.length] ;
		for(int i=0; i< countAll; i++){
			rtn[i] = new ProductDataBean();
			rtn[i].setName(StringEscapeUtils.unescapeHtml4(productName[i]));
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));
			rtn[i].setDescription(StringEscapeUtils.unescapeHtml4(productName[i]));
			rtn[i].setPictureUrl("http://s.pictub.club/2016/11/08/HyvTC.jpg");
		}
		
		return rtn;
	}
	    
}