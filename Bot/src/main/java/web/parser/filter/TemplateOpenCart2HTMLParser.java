package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TemplateOpenCart2HTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<div class=\"product-info\">"});
		map.put("basePrice", new String[]{"<div class=\"product-info\">"});
		map.put("description", new String[]{"<div id=\"tab-description\" class=\"tab-content\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-info\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "false";
		if(evaluateResult.length > 0){
			String status = FilterUtil.getStringBetween(evaluateResult[0], "<span>สถานะสินค้าออนไลน์:", "<br />");
			if(status.contains("สินค้าหยุดการผลิต")||status.contains("สินค้ารอการนำเข้า")){
				exp = "true";
			}
		}
		return new String[] {exp};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-new\">", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		
    		if(productPrice == null || productPrice.trim().length() == 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price\">", "</div>");
	    		productPrice = FilterUtil.getStringBefore(productPrice, "฿", "");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
    		if(productPrice == null || productPrice.trim().length() == 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price\">", "</div>");
	    		productPrice = FilterUtil.getStringBefore(productPrice, "บาท", "");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
    		if(productPrice == null || productPrice.trim().length() == 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\">", "</span>");
	    		productPrice = FilterUtil.getStringBefore(productPrice, "฿", "");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
    		if(productPrice == null || productPrice.trim().length() == 0) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\">", "</span>");
	    		productPrice = FilterUtil.getStringBefore(productPrice, "บาท", "");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
    		if(productPrice == null || productPrice.trim().length() == 0) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\"", "</span>");
    			productPrice = FilterUtil.getStringAfter(productPrice, ">", productPrice);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<s>","</s>");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		
    		String tmp = FilterUtil.getStringAfter(evaluateResult[0], "class=\"soldout\"", evaluateResult[0]);
    		productPictureUrl = FilterUtil.getStringBetween(tmp, "src=\"", "\"");  		
    		
        }
    	return new String[] {productPictureUrl};
	}
	    
}
