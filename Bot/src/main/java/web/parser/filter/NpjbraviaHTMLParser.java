package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class NpjbraviaHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<h1 class=\"title title-with-text-alignment\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"description\" itemprop=\"description\">", "<div class=\"description\"  itemprop=\"description\" >"});
		map.put("pictureUrl", new String[]{"<div class=\"images-area \" style=\" \">"});
		map.put("realProductId", new String[]{"<div class=\"order-block\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0){
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span  itemprop=\"name\" >", "</span>");
			productName = FilterUtil.toPlainTextString(productName);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		if(evaluateResult.length > 0){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\" style=\"text-decoration: line-through\">", "</span>");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0){
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
		
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\"");
        }
        
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
		if(evaluateResult.length > 0){
			realProductId = FilterUtil.getStringBetween(evaluateResult[0],"<input type=\"hidden\" id=\"content_id\" name=\"content_id\" value=\"","\"");
		}
    	return new String[] {realProductId};
	}  
}
