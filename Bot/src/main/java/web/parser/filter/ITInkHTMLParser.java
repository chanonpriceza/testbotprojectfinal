package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class ITInkHTMLParser extends BasicHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.product_title");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			if(!e.select("ins").isEmpty()) {
				e = e.select("ins");
			}
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("del");
			productPrice = FilterUtil.removeCharNotPrice(e.html());			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {

		String desc = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div#tab-description");
			desc = FilterUtil.toPlainTextString(e.html());			
		}	
		return new String[] {desc};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String pic = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("figure.woocommerce-product-gallery__wrapper");
			e = e.select("img");
			pic = FilterUtil.toPlainTextString(e.attr("src"));			
		}	
		return new String[] {pic};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pic = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("a.add_to_wishlist");
			pic = FilterUtil.toPlainTextString(e.attr("data-product-id"));			
		}	
		return new String[] {pic};
	}
	
}