package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThaiMartShopHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name",		 	new String[]{"<h1 class=\"name\">"});
		map.put("price", 		new String[]{"<div class=\"price\">"});
		map.put("basePrice",	new String[]{"<div class=\"price\">"});
		map.put("description", 	new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"image\">"});		
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price-new\">","</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";    	
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<span style=\"text-decoration: line-through;\">","</span>");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
		    
}
