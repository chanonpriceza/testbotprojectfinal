package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class WoodyShopHTMLParser extends TemplateMakeWebEasyHTMLParser {
	@Override
	public Map<String, String[]> getConfiguration() {
	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("nameDescription", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:description\" content=\"","\"");
    	}
    	return new String[] {productDesc};
	}
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String brand = "";
    	if (evaluateResult.length == 1) {
			brand = FilterUtil.getStringBetween(evaluateResult[0], "หมวดหมู่ :", "</a>").trim();
			brand = FilterUtil.toPlainTextString(brand);
    	}
    	return new String[]{ brand};
    
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || realProductId == null || realProductId.length == 0) {
			return null;
		}
		String brand = nameDescription!=null&&nameDescription.length>0?nameDescription[0]:"";
		String[] brandArray = brand.split(" ").length > 0? brand.split(" "):new String[]{""};
		brand = brandArray[brandArray.length-1];
		
		
		String pdName = productName[0];		
		if (!brand.isEmpty()){
			pdName = brand+" "+pdName;
		}
		

		if(productPrice == null || productPrice.length == 0) {
			productPrice = new String[] {"0"};
		}
		
		String domain = FilterUtil.getStringBetween(FilterUtil.getStringAfter(currentUrl,"http",""), "://", "/");
		
		boolean isHttps = (currentUrl.indexOf("https")> -1)? true:false ; 
		
		if(StringUtils.isBlank(domain)) return null;
		
		String req_url = (isHttps)?"https":"http" ;
		req_url += "://" + domain;
		req_url += "/page/page_product_item/ajaxPageProductItemController.php";
		
		ProductDataBean[] rtn = new ProductDataBean[productPrice.length];
		int productCount = 0;
		
		for(String priceAttr : productPrice) {
			String data = postRequest(currentUrl, req_url, realProductId[0].trim(), priceAttr, domain, isHttps);
			data = String.valueOf(data);
			data = StringEscapeUtils.unescapeJson(data);
			String price = FilterUtil.getStringBetween(data, "<span class=\"h4\">", "</span>");
			if(price.isEmpty()){
				price = FilterUtil.getStringBetween(data, "<span class=\"price h4\">", "</span>");
			}
			if(price.isEmpty()){
				price = FilterUtil.getStringAfter(data,"class=\"ff-price fs-price fc-price\"","");
				price = FilterUtil.getStringAfter(price,">","");
				price = FilterUtil.getStringBefore(price,"<","");
			}
			if(price.isEmpty()){
				return null;
			}
			String bPrice =  FilterUtil.getStringBetween(data, "<span class=\"productOldPrice h5\">", "</span>");
			if(bPrice.isEmpty()) {
				bPrice = FilterUtil.getStringAfter(data,"class=\"productOldPrice h5","");
				bPrice = FilterUtil.getStringAfter(bPrice,">","");
				bPrice = FilterUtil.getStringBefore(bPrice,"<","");
			}
			
			ProductDataBean resultBean = new ProductDataBean();
			resultBean.setName(pdName);
			resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(price)));
			
			if (!bPrice.isEmpty()) {
				resultBean.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(bPrice)));
			}
			
			if (productDescription != null && productDescription.length != 0) {
				resultBean.setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						resultBean.setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							resultBean.setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}
			
			if (nameDescription != null && nameDescription.length != 0){
				resultBean.setKeyword(nameDescription[0]);
			}
			
			rtn[productCount] = resultBean;
			productCount++;
		}
		
				
		return rtn;
	}

}
