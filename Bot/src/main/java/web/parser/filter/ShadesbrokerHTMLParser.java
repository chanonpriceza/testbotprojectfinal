package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ShadesbrokerHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"productDisplay\">"});
		map.put("basePrice", new String[]{"<div class=\"productDisplay\">"});
		map.put("description", new String[]{"<div class=\"inner-left-box\">"});
		map.put("pictureUrl", new String[]{"<div class=\"productDisplay\">"});
		map.put("expire",new String[]{"<div class=\"productDisplay\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			String	productNameFirst = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"productBuy\" style=\"position:relative;\" >", "</div>");
			productNameFirst =  FilterUtil.getStringBetween(productNameFirst, "<h1>", "</h1>");
			String productNameSecond = FilterUtil.getStringBetween(productNameFirst, "<h2>", "</h2>"); 
			if(StringUtils.isBlank(productNameSecond)){
				String	productId = FilterUtil.getStringBetween(evaluateResult[0], "arrayProducts[0]['productItemID'] =", ";");
				productNameSecond = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" productItemId=\""+productId.trim()+"\" value=\"", "\"");
			}
			productName = productNameFirst+" "+productNameSecond;			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "arrayProducts[1]['newPrice'] = '", "';");
    		if (StringUtils.isNotBlank(productPrice)) {
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    			double price = FilterUtil.convertPriceStr(productPrice);
    			price = price * 35.33;
    			productPrice = String.valueOf(Math.round(price));
    		}
    		
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "arrayProducts[1]['listPrice'] = '", "';");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    			double price = FilterUtil.convertPriceStr(bPrice);
    			price = price * 35.33;
    			bPrice = String.valueOf(Math.round(price));
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[1], "<ul>", "</ul>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "arrayProducts[0]['imgPath'] = '", "';");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
    		String isExpireStr = FilterUtil.getStringBetween(evaluateResult[0], "arrayProducts[0]['isAvailable'] = '", "';");
        	if(isExpireStr.indexOf("False") != -1) {
        		return new String[]{"true"};
        	}	
    	}    	
        return new String[]{""};
	}
}
