package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ItalThaiCellarHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<p class=\"product-image\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
   
    		if (evaluateResult.length > 0) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price\">","</span>");
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"std\">","</div>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a  href='", "\'");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		ProductDataBean result = new ProductDataBean();
		String name[] = mpdBean.getProductName();
		String price[] = mpdBean.getProductPrice();
		String productDesc[] = mpdBean.getProductDescription();
		String pictureUrl[] = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String linkProgram = "http://click.accesstrade.in.th/adv.php?rk=0001zo0005fq&url=";
		
		result.setName(name[0]);
		result.setPrice(Double.valueOf(price[0]));
		result.setDescription(productDesc[0]);
		if(currentUrl.contains("/wine/") || currentUrl.contains("/spirits/")) {
			result.setPictureUrl("https://dl.dropboxusercontent.com/s/bpi7xvyml2s1s81/italthaicellar_Product.png?dl=0");
		} else {
			result.setPictureUrl(pictureUrl[0]);
		}
		
		result.setUrl(linkProgram + currentUrl);
		
		return new ProductDataBean[] {result};
	}
	
	
}