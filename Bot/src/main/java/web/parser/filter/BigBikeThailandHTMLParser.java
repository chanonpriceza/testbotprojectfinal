package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BigBikeThailandHTMLParser extends DefaultHTMLParser {
	 public Map<String, String[]> getConfiguration() {
	    	
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			map.put("name", new String[]{""});
			map.put("price", new String[]{"<span id=\"txtPrice\">"});
			map.put("basePrice", new String[]{"<div class=\"price-box\">"});
			map.put("description", new String[]{"<div class=\"form-horizontal list-detail\">"});
			map.put("pictureUrl", new String[]{""});
			
			return map;
		} 
		
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";     
			if(evaluateResult.length == 1) {
				String  name = FilterUtil.getStringBetween(evaluateResult[0],"<h3 class=\"page-header padding-right\">","</a>");
				String  id = StringUtils.isNotBlank(FilterUtil.getStringBetween(evaluateResult[0],"<input name=\"motorcycle_id\" type=\"hidden\" value=\"","\""))?" ("+FilterUtil.getStringBetween(evaluateResult[0],"<input name=\"motorcycle_id\" type=\"hidden\" value=\"","\"")+")":"";
				String  province = StringUtils.isNotBlank(FilterUtil.getStringBetween(evaluateResult[0],"จังหวัด</label>","</div>"))?"("+(FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0],"จังหวัด</label>","</div>")).trim())+")":"";
				productName = name+id+province;
				productName = FilterUtil.toPlainTextString(productName);			
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = ""; 
			if(evaluateResult.length == 1) {
				productPrice = evaluateResult[0]; 
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			
	    	if (productPrice != null && productPrice.trim().length() != 0) {
	    		
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    		if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice)) {
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}
	        }		
			return new String[] {productPrice};
		}
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = evaluateResult[0];
	    		productDesc = productDesc.replaceAll("-->","");
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
			if(evaluateResult.length == 1){
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img data-u=\"image\" src2=\"","\"");
			}
	    	return new String[] {productPictureUrl};
		}

}
