package web.parser.filter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AdidasThailandHTMLParser extends DefaultHTMLParser{	

	private static final Logger logger = LogManager.getRootLogger();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{""});
		map.put("description", 	new String[]{"<div class=\"pdp-info\">"});
		map.put("pictureUrl",	new String[]{"<p class=\"product-image\">"});
		map.put("nameDescription",	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = evaluateResult[0];
			String tmp_name = productName;
			tmp_name = FilterUtil.getStringBetween(tmp_name,"<h1 itemprop=\"name\" class=\"name\">", "</h1>");
			if(StringUtils.isNoneBlank(tmp_name)){
				String tmp_name2 = productName;
				tmp_name2 = FilterUtil.getStringBetween(tmp_name2,"<div class=\"division-value\">", "</div>");
				if(StringUtils.isNotBlank(tmp_name2)){
					tmp_name = tmp_name +" ("+ tmp_name2 +")";
				}
			}
			
			if(StringUtils.isNoneBlank(tmp_name)){
				tmp_name = FilterUtil.toPlainTextString(tmp_name);
				tmp_name = tmp_name.trim();
				if(!(tmp_name.toLowerCase()).startsWith("Adidas")){
					tmp_name = "Adidas "+tmp_name;
				}
			}
			productName = tmp_name;
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
		if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"special-price\">","</p>");
			if(productPrice.trim().length() == 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"old-price\">","</p>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String productNameDescription = "";
    	if (evaluateResult.length == 1) {
    		productNameDescription =evaluateResult[0];
        }
		return new String[]{productNameDescription};
	}
	
	  @Override
	    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String[] htmllist = mpdBean.getProductNameDescription();
			String currentUrl = mpdBean.getCurrentUrl();
			String[] productBasePrice = mpdBean.getProductBasePrice();
			String[] productPrice = mpdBean.getProductPrice();

			if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])){
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			String tmp_html ="";
			
			String productid = FilterUtil.getStringAfter(currentUrl,"?a=", "");
			productid = FilterUtil.getStringAfter(productid, "_", "");
			productid = FilterUtil.getStringBefore(productid, "&",productid);
			
			if(htmllist[0] != null && htmllist[0].length() > 0 && StringUtils.isNotBlank(productid)){
				String tmp_parm = FilterUtil.getStringBetween(htmllist[0], "jQuery.getJSON('", "'");
				if(StringUtils.isNotBlank(tmp_parm)){
					try {
						CloseableHttpClient httpclient = HttpClients.createDefault();
						HttpGet httpGet = new HttpGet("http:"+tmp_parm);
						httpGet.addHeader("X-Requested-With", "XMLHttpRequest");
						CloseableHttpResponse res = httpclient.execute(httpGet);
						HttpEntity ent = res.getEntity();
						if(res.getStatusLine().getStatusCode() == 200){
							InputStreamReader isr = new InputStreamReader(ent.getContent(), "UTF-8");
							BufferedReader brd = new BufferedReader(isr);
							StringBuilder result = new StringBuilder();
							String line = "";
							while ((line = brd.readLine()) != null) {
								result.append(line);
							}
							tmp_html = result.toString();
						}
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		
			
			if(StringUtils.isNotBlank(tmp_html)){
				String color = FilterUtil.getStringBetween(tmp_html, "value\":\""+productid+"\",\"label\":\"", "\",");
				if(StringUtils.isNotBlank(color)){
					color =color.replace("\\", "");
					productName[0] = productName[0]+" "+ color;
				}
			}
			
			rtn[0].setName(productName[0]);
			
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			if (productDescription != null && productDescription.length != 0)
				rtn[0].setDescription(productDescription[0]);
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if(productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);					
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
				    	} catch (MalformedURLException e) {
							
						}
					}
				}
			}

			return rtn;
		}    
	
	
	
}