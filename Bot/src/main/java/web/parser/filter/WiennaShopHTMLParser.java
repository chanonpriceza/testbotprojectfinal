package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;

public class WiennaShopHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "utf-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice"		, new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"short-description desktop\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-image\">"});
		map.put("realProductId", new String[]{""});
		map.put("nameDescription", new String[]{"<div class=\"short-description desktop\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length > 0) {				
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"h1\">", "</span>");	
			productName = FilterUtil.toPlainTextString(tmp);	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"special-price\">","</p>");
			productPrice = FilterUtil.getStringBetween(productPrice,"<span class=\"price\"","span>");
			productPrice = FilterUtil.getStringBetween(productPrice,"\">","</");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.equals("")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if (evaluateResult.length > 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"old-price\">","</p>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice,"<span class=\"price\"","span>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice,"\">","</");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"std\">", "</div>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		
		String realProductId = null;
    	if (evaluateResult.length == 1) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"");
        }
    	return new String[] {realProductId};
	}

	public String[] getProductNameDescription(String[] evaluateResult) {
		// TODO Auto-generated method stub
		String NameDescription = "";
		if(evaluateResult.length > 0) {				
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"std", "br");	
			tmp = FilterUtil.getStringBetween(tmp, ">", ")") + ")";	
			NameDescription = FilterUtil.toPlainTextString(tmp);	
		}
		return new String[] {NameDescription};
	}
			
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		String[] realProductId = mpdBean.getRealProductId();
		String[] nameDescription = mpdBean.getProductNameDescription();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		String brand = "";
		if(currentUrl.contains("contentId=08&") ||
				currentUrl.contains("contentId=01&") ||
				currentUrl.contains("contentId=11&") ||
				currentUrl.contains("contentId=02&") ||
				currentUrl.contains("contentId=03&")) {
			brand = "Wienna เวียนนา";
		} else if(currentUrl.contains("contentId=15&")) {
			brand = "Revinus Lingerie";
		} //else if(currentUrl.contains("contentId=04&")) {
			//brand = "Vanezza Lingerie";
		//} 
						
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if(brand.length() != 0) {
			rtn[0].setName(productName[0] + " ["+nameDescription[0]+"]  " + brand);			
		} else {
			rtn[0].setName(productName[0] + " ["+nameDescription[0]+"]");			
		}
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setRealProductId(realProductId[0]);
		

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}
	    
}
