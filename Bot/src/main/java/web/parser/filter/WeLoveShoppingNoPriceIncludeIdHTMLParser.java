package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class WeLoveShoppingNoPriceIncludeIdHTMLParser extends WeLoveShoppingPlatinumHTMLParser {

     
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if(productPrice == null || productPrice.length == 0 || 
				productPrice[0] == null || productPrice[0].trim().length() == 0	) {			
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		} else {
			if(productPrice[0].equals("0.00")) {
				rtn[0].setPrice(BotUtil.CONTACT_PRICE);
			} else {
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			}
		}		
		
		if (productDescription != null && productDescription.length != 0)
			rtn[0].setDescription(productDescription[0]);
		if (productPictureUrl != null && productPictureUrl.length != 0 
				&& productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0)  {
			
			//parse relative path to absolute path	
			try {
				URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
				rtn[0].setPictureUrl(url.toString());
	    	} catch (MalformedURLException e) {
				
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		return rtn;
	}    
        
}



