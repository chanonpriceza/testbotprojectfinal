package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TrueStoreHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<div class=\"cmn-form-style\">"});
		map.put("price", 		new String[]{""});
		map.put("description", 	new String[]{"<div class=\"tab-2 tgl-content\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"img-box-wrap\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"txt-in-box tbold-large-size\">","</div>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"nmr-price-txt\">", "</span>");
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.length()==0 || productPrice.equals("0")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	    
}
