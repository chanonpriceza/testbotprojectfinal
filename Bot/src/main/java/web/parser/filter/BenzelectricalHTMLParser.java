package web.parser.filter;

import utils.FilterUtil;

public class BenzelectricalHTMLParser extends TemplateTaradIncludeIDHTMLParser{
		
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		
        } else if (evaluateResult.length > 1) { 
        	productPictureUrl = FilterUtil.getStringBetween(evaluateResult[1], "src=\"", "\"");
    		if(productPictureUrl.trim().length() == 0) {
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		}	
        }
    	return new String[] {productPictureUrl};
	}
	    
}