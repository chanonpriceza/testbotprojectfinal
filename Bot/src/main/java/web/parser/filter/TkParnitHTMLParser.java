package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TkParnitHTMLParser extends DefaultHTMLParser{
			
//	public String getCharset() {
//		return "UTF-8";
//	}
	
	//FilterUtil
	/*
	    
		FilterUtil.removeCharNotPrice(String price)
          - to remove char except "0123456789."

		FilterUtil.toPlainTextString(String html);
       
		FilterUtil.getStringAfter(target, begin, defaultValue)
		FilterUtil.getStringAfterLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBefore(target, begin, defaultValue)
		FilterUtil.getStringBeforeLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBetween(target, begin, end)

	*/
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"post\">"});
		map.put("price", new String[]{"<div style=\"text-align:center;position:relative;\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div id=\"canadaprovinces\" class=\"glidecontentwrapper\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {		
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h2>", "</h2>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		

    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");

//    		if(tmp.length() == 0) {
//    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");    			
//    		}
    		
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดสินค้า", "ป้ายกำกับ");
    	if(productDesc.length() !=0) {
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}
