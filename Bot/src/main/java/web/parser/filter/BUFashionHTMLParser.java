package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BUFashionHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table border=\"0\" style=\"text-align:left;\">"});
		map.put("price", new String[]{"<table border=\"0\" style=\"text-align:left;\">"});
		map.put("description", new String[]{"<table width=\"62%\"  border=\"0\" align=\"center\" cellpadding=\"1\" cellspacing=\"1\">"});
		map.put("pictureUrl", new String[]{"<div class=\"imgInfo\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			if(evaluateResult[0].indexOf("Out of stock") == -1){
				productName = FilterUtil.getStringBetween(evaluateResult[0], "(รหัสสินค้า)", "</div>");
				productName = FilterUtil.toPlainTextString(productName);
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
    		if(evaluateResult[0].indexOf("<font color='red'>Out of stock</font>") == -1){
   		
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"goods-price style1\" id=\"ECS_SHOPPRICE\">", "</span>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	
	    		if(productPrice.length() == 0) {
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"market-price\">", "</span>");
	    			productPrice = FilterUtil.toPlainTextString(productPrice);
	        		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    		}
    		
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "</span>", "<table");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}