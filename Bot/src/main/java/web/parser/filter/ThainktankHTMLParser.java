package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThainktankHTMLParser extends DefaultHTMLParser{
	
	@Override
	public String getCharset() {
		return "TIS-620";
	}
	public Map<String, String[]> getConfiguration() {
		
		Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<H1>","</H1>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"RowTblOthModel\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<form name = frmorder method = post action = \"basket.asp\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if (evaluateResult!=null && evaluateResult.length >0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult!=null && evaluateResult.length>0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคา :","บาท");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult!=null && evaluateResult.length>0) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"description\" content=\"","\"");
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if (evaluateResult!=null && evaluateResult.length>0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src = \"", "\"");
		}
		return new String[] {productPictureUrl};
	}

  
}

