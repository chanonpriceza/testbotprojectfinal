package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ThaiPhatanasinHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"ColRDesModel\">", "<div class=\"RowTblOthModel\">"});
		map.put("price", new String[]{"<div class=\"RowTblOthModel\">"});
		map.put("basePrice", new String[]{"<div class=\"RowTblOthModel\">"});
		map.put("description", new String[]{"<div class=\"RowModelDes\">"});
		map.put("pictureUrl", new String[]{"<div class=\"ColLImgModelB\">"});
		map.put("expire", new String[]{"<div class=\"RowTblOthModel\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		List<String> productNameList = new ArrayList<String>();
		String productName = "";
		if (evaluateResult!=null && evaluateResult.length==2) {
			List<String> nameList = FilterUtil.getAllStringBetween(evaluateResult[0], "<td align=\"left\" valign=\"top\">", "</td>");
			productName = nameList.get(0)+" "+nameList.get(1);
			
			List<String> codeList = FilterUtil.getAllStringBetween(evaluateResult[1], "<tr", "</tr>");
			for (int count=1; count<codeList.size(); count++) {
				List<String> tmp = FilterUtil.getAllStringBetween(codeList.get(count), "<td align=\"center\">", "</td>");
				if (tmp.size()==4) {
					String expire = tmp.get(3);
					if (expire.indexOf("สินค้าหมด") > -1) {
						productNameList.add("");
						continue;
					}
					
					String productCode = tmp.get(0);
					productCode = FilterUtil.toPlainTextString(productCode);
					
					if (StringUtils.isNotBlank(productCode)) {
						productNameList.add(productName+" ("+productCode+")");
					}
				} else {
					productNameList.add("");
				}
			}
		}
		return productNameList.toArray(new String[productNameList.size()]);
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		List<String> productPriceList = new ArrayList<String>();
		
		if (evaluateResult!=null && evaluateResult.length>0) {
			List<String> codeList = FilterUtil.getAllStringBetween(evaluateResult[0], "<tr", "</tr>");
			for (int count=1; count<codeList.size(); count++) {
				List<String> tmp = FilterUtil.getAllStringBetween(codeList.get(count), "<td align=\"center\">", "</td>");
				if (tmp.size()==4) {
					productPrice = tmp.get(2);
					if (productPrice.indexOf("price_prom") > -1) {
						productPrice = FilterUtil.getStringAfter(productPrice, "</span>", "");
					}
					productPrice = FilterUtil.toPlainTextString(productPrice);
					productPrice = FilterUtil.removeCharNotPrice(productPrice);
					productPriceList.add(productPrice);
				} else {
					productPriceList.add("");
				}
				
			}
		}
		return productPriceList.toArray(new String[productPriceList.size()]);
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		List<String> productBasePriceList = new ArrayList<String>();
		
		if (evaluateResult!=null && evaluateResult.length>0) {
			List<String> codeList = FilterUtil.getAllStringBetween(evaluateResult[0], "<tr", "</tr>");
			for (int count=1; count<codeList.size(); count++) {
				List<String> tmp = FilterUtil.getAllStringBetween(codeList.get(count), "<td align=\"center\">", "</td>");
				if (tmp.size()==4) {
					productBasePrice = tmp.get(2);
					if (productBasePrice.indexOf("price_prom") > -1) {
						productBasePrice = FilterUtil.getStringBefore(productBasePrice, "</span>", "");
						productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
						productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
					} else {
						productBasePrice = "";
					}
				}
				
				productBasePriceList.add(productBasePrice);
			}
		}
		return productBasePriceList.toArray(new String[productBasePriceList.size()]);
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult!=null && evaluateResult.length>0) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td width=\"80%\" align=\"left\" valign=\"top\">", "</td>");
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if (evaluateResult!=null && evaluateResult.length>0) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] {productPictureUrl};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	String[] productDescription = mpdBean.getProductDescription();
    	String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String currentUrl = mpdBean.getCurrentUrl();

    	if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
    	
		ProductDataBean[] rtn = new ProductDataBean[productName.length];
		for (int count=0; count<productName.length; count++) {
			rtn[count] = new ProductDataBean();
			rtn[count].setName(productName[count]);
			rtn[count].setPrice(FilterUtil.convertPriceStr(productPrice[count]));
			rtn[count].setUrl(currentUrl);

			if (productDescription != null && productDescription.length != 0) {
				rtn[count].setDescription(productDescription[0]);
			}
					
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null
						&& productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[count].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl),
									productPictureUrl[0]);
							rtn[count].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}
		}
		
		return rtn;
	}

	

}
