package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class LinkMobileHTMLParser extends DefaultHTMLParser{
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<div class=\"col-sm-6 item-product-detail\">"});
			map.put("price", new String[]{"<div class=\"sale-price\">"});
			map.put("basePrice", new String[]{"<div class=\"regular-price\">"});
			map.put("description", new String[]{"<div class=\"item-description\">"});
			map.put("pictureUrl", new String[]{""});
			map.put("realProductId", new String[]{"<div class=\"add-cart\">"});

			return map;
		}

	 @Override
	public String[] getProductName(String[] evaluateResult) {
		String name = "";
		if(evaluateResult.length > 0) {
			name = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"title\">", "</div>");
			name = StringEscapeUtils.unescapeHtml4(name);
			name = FilterUtil.toPlainTextString(name);
		}
		return new String[]{name};
	}
		 
	 public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if (evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product_id\" value=\"", "\"");
			if(StringUtils.isNoneBlank(realProductId)){
				realProductId = FilterUtil.toPlainTextString(realProductId);
			}
	       }
			
		return new String[] {realProductId};
	}
	 
	 @Override
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(evaluateResult.length > 0) {
			price = FilterUtil.getStringBefore(evaluateResult[0], " - ", evaluateResult[0]);
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
		}
		return new String[] {price};
	}
	 
	 @Override
	public String[] getProductDescription(String[] evaluateResult) {
		String desc = "";
		if(evaluateResult.length > 0)  {
			desc = FilterUtil.getStringAfter(evaluateResult[0], "</div>", evaluateResult[0]);
			desc = StringEscapeUtils.unescapeHtml4(desc);
			desc = FilterUtil.toPlainTextString(desc);
		}
		return new String[]{desc};
	}
	 
	 @Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String pictureUrl = "";
		if(evaluateResult.length > 0) {
			pictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
		}
		return new String[]{pictureUrl};
	}
	 @Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String bPrice = "";
			if(evaluateResult.length > 0) {
				
				bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
				bPrice = FilterUtil.removeCharNotPrice(bPrice);
			}
			return new String[] {bPrice};
		}
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] basprice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if(productPrice == null || productPrice.length == 0 || productPrice[0] == null || productPrice[0].trim().length() == 0	) {			
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		} else {
			if(productPrice[0].equals("0.00")) {
				rtn[0].setPrice(BotUtil.CONTACT_PRICE);
			} else {
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			}
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (basprice != null && basprice.length != 0) {
			try{
			 rtn[0].setBasePrice( Double.parseDouble(basprice[0]));
			}catch (Exception e) {
				rtn[0].setBasePrice(0.00);
			}
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
			if (StringUtils.isNoneBlank(productName[0])) {
				rtn[0].setName(productName[0] +" ("+ realProductId[0] +")");
			}
		}

		return rtn;
	}
	
}