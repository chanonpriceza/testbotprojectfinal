package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BerdedHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{"<span class='price-lg'>"});
		map.put("description", 	new String[]{"<div class=\"detail\">"});
		map.put("pictureUrl", 	new String[]{"<div align=\"center\" style=\"padding:5%;padding-bottom:5px;background:#FFFFFF;padding-top: 30px;\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";
		
		String productNetwork = "";
		String productNetworkImage = "";
		productNetworkImage = FilterUtil.getStringAfter(evaluateResult[0],"<span class='sum-lg'>",evaluateResult[0]);
		productNetworkImage = FilterUtil.getStringBetween(productNetworkImage, "<img src='", "'");
		
			 if(productNetworkImage.equals("images/logo_ais_150.jpg"))	{ productNetwork = "AIS ซิม"; }
		else if(productNetworkImage.equals("images/logo_dtac_150.jpg"))	{ productNetwork = "DTAC ซิม"; }
		else if(productNetworkImage.equals("images/logo_true_150.jpg"))	{ productNetwork = "TrueMove ซิม"; }
		else if(productNetworkImage.equals("images/logo_imobile_150.jpg"))	{ productNetwork = "i-Mobile ซิม"; }
		else if(productNetworkImage.equals("images/logo_mycat_150.jpg"))	{ productNetwork = "MybyCAT ซิม"; }
		else if(productNetworkImage.equals("images/logo_tot_150.jpg"))		{ productNetwork = "TOT3G ซิม"; }
		else if(productNetworkImage.equals("images/logo_pg_150.png"))		{ productNetwork = "PENGUIN ซิม"; }
		
		String productNumber = FilterUtil.getStringBetween(evaluateResult[0],"<span class='phone-lg'>","</span>");
		if(productNumber.trim().length() == 0){ 
			return new String[]{""}; 
		}else{
			productNumber = productNumber.replace("-","");
			productNumber = String.format("%s-%s-%s", productNumber.substring(0, 3), productNumber.substring(3, 6), productNumber.substring(6, 10));
		}
		
		String productNumberSum = "";
		productNumberSum = FilterUtil.getStringBetween(evaluateResult[0], "<span class='sum-lg'>", "</span>");
			 
		productName = productNetwork + " เบอร์มงคล " + productNumber + " " + productNumberSum;
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";    	
    	if(evaluateResult.length > 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[1]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringAfter(evaluateResult[0],"<span class='sum-lg'>",evaluateResult[0]);
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<img src='", "'");
        }
    	return new String[] {productPictureUrl};
	}

}