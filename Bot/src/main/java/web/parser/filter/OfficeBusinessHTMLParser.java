package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class OfficeBusinessHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<p class=\"prg title\">"});
		map.put("price", new String[]{"<td width=\"443\" valign=\"top\">"});
		map.put("basePrice", new String[]{"<td width=\"443\" valign=\"top\">"});
		map.put("description", new String[]{"<div class=\"prodetail\">"});
		map.put("pictureUrl", new String[]{"<table border=\"0\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" class=\"productdetail\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		String	productPrices = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"prg saleprice\">", "</p>");
    		if (StringUtils.isBlank(productPrices)) {
    			productPrice= FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"prg price \">", "</p>");
    			if(productPrice.contains("สอบถามราคา") || productPrice.contains("ติดต่อ")) {
        			productPrice = BotUtil.CONTACT_PRICE_STR;
        		}else{
        			productPrice = FilterUtil.toPlainTextString(productPrice);
        		}
    		}else{
    			productPrice = FilterUtil.toPlainTextString(productPrices);
    		}
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"prg price delete\">", "</p>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
