package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;



public class KirenBarBQHTMLParser extends DefaultHTMLParser {
			
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1 itemprop=\"name\" class=\"entry-title\">"});
			map.put("price", new String[]{"<div class=\"single-product-price\">"});
			map.put("description", new String[]{"<div class=\"panel wc-tab entry-content active\" id=\"tab-description\">"});
			map.put("pictureUrl", new String[]{"<div class=\"product-image\">"});
			//map.put("expire", new String[]{"<h1 class=\"title title-with-text-alignment\">"});
			return map;
		}
		
	    @Override
	    public String[] getProductExpire(String[] evaluateResult) {
	    	String exp = "false";
			if(evaluateResult.length == 1) {			
				
				if(evaluateResult[0].contains("สินค้าหมด")){
					exp = "true";
				}
			}
			
			return new String[] {exp};
	    }
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";
			if(evaluateResult.length == 1) {			
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"ganeral_price\">", "</span>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }
	    	if(productPrice.equals("0") || productPrice.isEmpty()){
	    		productPrice = BotUtil.CONTACT_PRICE_STR;
	    	}
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = evaluateResult[0];
	    		productDesc = FilterUtil.toPlainTextString(productDesc);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) { 
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		String[] realProductId = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if (productPrice != null && productPrice.length != 0 && productPrice[0] != null
					&& productPrice[0].trim().length() != 0) {
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		} else {
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		if (realProductId != null && realProductId.length != 0){
			rtn[0].setRealProductId(realProductId[0]);
		}
		
		return rtn;
	}
	
	
 
	
}