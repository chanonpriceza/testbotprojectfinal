package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WellnessMarkShopHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"product-info-price\">"});
		map.put("basePrice", new String[]{"<div class=\"product-info-price\">"});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
    
    
    public String[] getProductName(String[] evaluateResult) {
		String product = "";   
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}

		return new String[] {product};
	}
    
    public String[] getProductPrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">ราคาพิเศษ</span>", "</span>");
			product = FilterUtil.getStringBetween(product, "<span class=\"price\">", "บาท");
			product = FilterUtil.removeCharNotPrice(product);	
			if(product.equals(""))
				product = BotUtil.CONTACT_PRICE_STR;
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">ราคาปรกติ</span>", "</span>");
			product = FilterUtil.getStringBetween(product, "<span class=\"price\">", "บาท");
			product = FilterUtil.removeCharNotPrice(product);				
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"value\" itemprop=\"sku\">", "</div>");
		}
		return  new String[] {product};
	}
}
