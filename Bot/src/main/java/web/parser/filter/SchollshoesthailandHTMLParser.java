package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SchollshoesthailandHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<li class=\"item product\">"});
		map.put("price", new String[]{"<span class=\"price\">"});
		map.put("description", new String[]{"<div class=\"value\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}

    public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
	            
		if(evaluateResult.length == 1) {			
			productName = "Scholl สกอลล์ รองเท้า "+FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		
		if(evaluateResult.length > 0){
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(evaluateResult.length == 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
		}
		
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		
    	return new String[] {productPictureUrl};
	}
}
