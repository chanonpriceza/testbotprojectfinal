package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SahapatHomeDeliveryHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name",		 	new String[]{"<div class=\"product-information\">"});
		map.put("price", 		new String[]{"<div class=\"product-information\">"});
		map.put("description", 	new String[]{"<div class=\"product-information\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"view-product\">"});
		map.put("realProductId",new String[]{"<div class=\"product-information\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<h2>","</h2>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"฿","</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดสินค้า :", "</p>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if(evaluateResult.length == 1) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า :", "</p>");
			realProductId = FilterUtil.toPlainTextString(realProductId);
		}
		return new String[] {realProductId};
	}
		    
	
}