package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PantipHotSaleHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<h1 class=\"product-title entry-title\">"});
    	map.put("price", new String[]{""});
    	map.put("basePrice", 	new String[]{"<p class=\"price product-page-price price-on-sale\">"});
		map.put("description", new String[]{"<table class=\"shop_attributes\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-gallery large-5 col\">"});
		
		return map;
	}
    public String getCharset() {
		return "UTF-8";
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   		
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price product-page-price \"><span class=\"woocommerce-Price-amount amount\">", "&nbsp;");
    		if(productPrice.equals("")){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "&nbsp;");
    		}
    		
    		if(productPrice.isEmpty()){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";    	
    	if (evaluateResult.length == 1) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<del><span class=\"woocommerce-Price-amount amount\">","&nbsp;");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    				
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div data-thumb=\"", "</figure>");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}    
}