package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class AISOnlineStoreHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<table class=\"data-table\" id=\"product-attribute-specs-table\">"});		
		map.put("pictureUrl", new String[]{"<div class=\"product-img-box\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String[] productName = null;   
		if(evaluateResult.length == 1) {
			String mainProductName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"h1\">", "</span>");
			mainProductName = FilterUtil.toPlainTextString(mainProductName);
			if(StringUtils.isNotBlank(mainProductName)) {
				String colorScript = FilterUtil.getStringBetween(evaluateResult[0], "Product.Config(", ");");
				colorScript = FilterUtil.getStringBetween(colorScript.toLowerCase(), "\"label\":\"color\",\"options\":[{", "}]");
				List<String> colorList = FilterUtil.getAllStringBetween(colorScript, "\"label\":\"", "\"");
				if(colorList != null && colorList.size() > 0) {
					productName = new String[colorList.size()];
					for (int i=0; i<colorList.size(); i++) {
						productName[i] = mainProductName + " (" + StringUtils.capitalize(colorList.get(i)) + ")";
					}
				}else {
					productName = new String[]{mainProductName};
				}
			}
		}
		return productName;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		for (String string : evaluateResult) {
    			productPrice = FilterUtil.getStringBetween(string, "<span class=\"price\">", "</span>");
        		productPrice = FilterUtil.toPlainTextString(productPrice);
            	productPrice = FilterUtil.removeCharNotPrice(productPrice);
            	StringEscapeUtils.unescapeHtml4(productPrice);
            	if(StringUtils.isNotBlank(productPrice)) break;
			}
        }
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "itemprop=\"price\">", "<");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);	
        }
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = evaluateResult[0].replace("<td class=\"data\">", " : ");
    		productDesc = productDesc.replace("</tr>", ", ");
    		productDesc = FilterUtil.toPlainTextString(productDesc);		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] desc = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[productName.length];
		for(int i=0; i<productName.length; i++) {
			rtn[i] = new ProductDataBean();
			rtn[i].setName(StringEscapeUtils.unescapeJava(productName[i]));
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (StringUtils.isNotBlank(productPictureUrl[0])) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[i].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[i].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {}
					}
				}
			}

			if (desc != null && desc.length != 0){
				rtn[i].setDescription(desc[0]);
			}
		}
		
		
		return rtn;
	}
}