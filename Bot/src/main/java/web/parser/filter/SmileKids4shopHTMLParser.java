package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class SmileKids4shopHTMLParser extends TaradHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1>" });
		map.put("price", new String[] { "" });
		map.put("basePrice", new String[] { "<font class='ProductPrice'>" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
		
		return map;
	}
	
	
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<font class='ProductPrice'>ราคา:  <font class='ProductPricePrice'>","</font>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		

		return new String[] {productPrice};
	}


	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<b>รหัสสินค้า:</b>", "</div>");
		return new String[] {realProductId};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] baseprice = mpdBean.getProductBasePrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		
		
		if(name == null || name.length == 0 || price == null || price.length == 0) {
			return null;
		}
		if(StringUtils.isBlank(baseprice[0])){
			baseprice[0] = price[0];
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setRealProductId(String.valueOf(realProductId[0]));
		rtn[0].setName(name[0] + " ("+realProductId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(price[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(baseprice[0]));
		if(desc != null && desc.length > 0) {
			rtn[0].setDescription(desc[0]);
		}
		if(pictureUrl != null && pictureUrl.length > 0) {
			if (pictureUrl[0] != null && pictureUrl[0].trim().length() != 0) {
				if (pictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl[0]);
						
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) { }
				}
			}
		}
		
		return rtn;
	}   
	
	
}

