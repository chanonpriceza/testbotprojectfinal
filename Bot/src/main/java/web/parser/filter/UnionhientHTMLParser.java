package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class UnionhientHTMLParser extends DefaultHTMLParser{

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1.*>"});
		map.put("price", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div id=\"showOnePic\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = productName.replace("ขาย", "");
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		if(productPrice.contains("ไม่ระบุราคา")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else if(productPrice.contains("สอบถาม")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else if(productPrice.toLowerCase().contains("xx")) {
    			productPrice = FilterUtil.getStringAfter(productPrice, "ราคา", productPrice);
    			productPrice = productPrice.replace("&nbsp;", "");
    			productPrice = productPrice.trim();				
			} else {
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawProductDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"overview\">", "</div>");
    		if(rawProductDesc.length() == 0){
    			rawProductDesc = FilterUtil.getStringBetween(evaluateResult[0], "ข้อมูลทั่วไป / Overview:", "</tbody>");
    		}
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawProductDesc+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = productPictureUrl.replace("/..", "");
        }
    	return new String[] {productPictureUrl};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		//http://www.pantipmarket.com/mall/unionhient/?node=products&id=198616
		String productId = FilterUtil.getStringAfter(currentUrl, "id=", "");
		if(productName[0].trim().length() != 0 && productId.trim().length() != 0) {
			rtn[0].setName(productName[0] + " (" + productId.trim()+")");
		} else {
			rtn[0].setName(productName[0]);
		}
				
		
		if(FilterUtil.isPriceXXX(productPrice[0])) {
			rtn[0].setPrice(FilterUtil.convertPriceXXXStr(productPrice[0]));
		} else {
			double rawPrice = FilterUtil.convertPriceStr(productPrice[0]);
			if(rawPrice < 10){
				rawPrice = BotUtil.CONTACT_PRICE;
			}
			rtn[0].setPrice(rawPrice);			
		}
		

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						productPictureUrl[0] = url.toString();
						productPictureUrl[0] = productPictureUrl[0].replace("/mall/mall", "/mall");
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}
	
}
