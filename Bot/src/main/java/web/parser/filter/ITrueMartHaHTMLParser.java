package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ITrueMartHaHTMLParser extends DefaultHTMLParser{	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-name\" itemprop=\"name\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"box-more-product-detail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("basePrice", new String[]{""});
	
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	String expire = "false";
    	if(evaluateResult.length == 1) {
    		if(!evaluateResult[0].contains("<div class=\"sold-out display-none\"")) {
    			expire ="true";
    		}
    	}    	
        return new String[]{expire};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = evaluateResult[0];
		}
		productName = FilterUtil.toPlainTextString(productName);
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";   
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<meta itemprop=\"price\" content=\"","\"");
			productPrice = FilterUtil.toPlainTextString(productPrice);
	    	productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\"active\"", "data-zoom-image");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0){
			basePrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price-compare\" ng-show=\"comparePrice\" ng-bind=\"comparePrice\">","</span>");
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] basePrice = mpdBean.getProductBasePrice();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		if(basePrice.length > 0){
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		//set Real Product ID		
		String productId = FilterUtil.getStringAfterLastIndex(currentUrl, "-", "");
		if (StringUtils.isBlank(productId)) {
			productId = FilterUtil.getStringAfterLastIndex(currentUrl, "/", "");
		}
		
		productId = FilterUtil.getStringBefore(productId, ".html", "").trim();
		if(productId.length() != 0) {
			rtn[0].setRealProductId(productId);
		}
		return rtn;
	}
	    
}