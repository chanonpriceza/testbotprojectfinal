package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultUserHTMLParser;
import web.parser.image.ImageParser;

public class HPThailandOnlineStoreHTMLParser extends BasicHTMLParser {
	
	@Override
	public ProductDataBean[] parse(String url) {
		BaseConfig.IMAGE_PARSER = new ImageParser();
		return super.parse(url);
	}
	

	    public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
	    	mixIdName = true;
			map.put("name", new String[]{""});
			map.put("price", new String[]{"<div class=\"product-info-price\">"});
//			map.put("basePrice", new String[]{""});
//			map.put("expire", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("realProductId", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String product = "";
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:title]");
				product = e.attr("content");
			}
			
			return new String[] {product};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String product = "";    	
			if(evaluateResult.length == 1) {	
				product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
				product = FilterUtil.removeCharNotPrice(product);				
			}		
			return new String[] {product};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {

			String product = "";    	
			if(evaluateResult.length == 1) {	
				product = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"oldPrice\">", "</div>");
				product = FilterUtil.removeCharNotPrice(product);				
			}	
			return new String[] {product};
		}
	
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String product = "";
			if(evaluateResult.length == 1) {	
				product = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"sku\" value=\"", "\"");
			}
			return  new String[] {product};
		}

		@Override
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String product = "";
			if(evaluateResult.length == 1) {	
				product = FilterUtil.getStringBetween(evaluateResult[0], "\"full\":\"", "\"");
				product = product.replace("\\","");
	
			}
			return  new String[] {product};
		}

		@Override
		public String[] getProductDescription(String[] evaluateResult) {
			String product = "";
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:description]");
				product = e.attr("content");
			}
			return  new String[] {product};
		}
}
