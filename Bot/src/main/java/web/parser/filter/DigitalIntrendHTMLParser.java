package web.parser.filter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;


public class DigitalIntrendHTMLParser extends DefaultHTMLParser {
	
	public String getCharset() {
		return "tis-620";
	}
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new HashMap<String, String[]>();
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<span class=\"fpricer\">"});
		map.put("basePrice", new String[]{"<span class=\"fkillprice\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "ชื่อสินค้า:", "</font>");
			if(productName.length() == 0) {
				productName = FilterUtil.getStringAfter(evaluateResult[0], "<td width=\"399\" height=\"20\" class=f12bbold>", "</td>");
			}
			productName = FilterUtil.toPlainTextString(productName);
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(evaluateResult.length > 0) {
			price = FilterUtil.toPlainTextString(evaluateResult[0]);
			price = FilterUtil.removeCharNotPrice(price);
		}
		return new String[] {price};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String desc = "";
		if(evaluateResult.length > 0) {
			desc = FilterUtil.getStringBetween(evaluateResult[0], "<td width=\"591\" height=\"25\" bgcolor=\"#D7E9FF\" class=\"boxdot\">", "<td height=\"20\" colspan=\"3\" bgcolor=\"#68A0E7\" class=f12wbold align=\"right\">");
			desc = FilterUtil.toPlainTextString(desc);
			desc = StringEscapeUtils.unescapeHtml4(desc);
		}
		return new String[] {desc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String pictureUrl = "";
		if(evaluateResult.length > 0) {
			pictureUrl = FilterUtil.getStringAfter(evaluateResult[0], "<input type=\"hidden\" name=\"save1\" id=\"save1\" value=\"\">", "");
			pictureUrl = FilterUtil.getStringBetween(pictureUrl, "<img src=\"", "\"");
		}
		return new String[] {pictureUrl};
	}
}



