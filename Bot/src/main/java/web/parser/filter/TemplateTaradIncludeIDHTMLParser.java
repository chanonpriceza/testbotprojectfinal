package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;

public class TemplateTaradIncludeIDHTMLParser extends TemplateTaradHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>", "<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>"});
		map.put("price", new String[]{"<div style='padding-bottom: 5px;' class='NewProductDetail'>"});
		map.put("description", new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});
		map.put("pictureUrl", new String[]{"<table align='center' border='0' cellspacing='0' cellpadding='0' class='ProductPictureBorder' width='150' height='150'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length >= 2) {
			
			String tmpId = "";
			for (int i = 0; i < evaluateResult.length; i++) {
				if(evaluateResult[i].contains("<h1>")) {			
					if(!evaluateResult[i].contains("http://www.taradplaza.com/_tarad/images/product_word_icon/sold.gif")){
						productName = FilterUtil.toPlainTextString(evaluateResult[i]);		
					}
					break;
				}
			}
			
			for (int i = 0; i < evaluateResult.length; i++) {
				if(evaluateResult[i].contains("รหัสสินค้า:")) {					
					tmpId = FilterUtil.toPlainTextString(evaluateResult[i]);	
					tmpId = FilterUtil.getStringAfter(tmpId, "รหัสสินค้า:", "");
					break;
				}
			}
			
			if(productName.trim().length() != 0 && tmpId.trim().length() != 0) {
				productName = productName + " (" + tmpId.trim() + ")";
    		} 
							
		}
		
		return new String[] {productName};
	}
	
	
	    
}
