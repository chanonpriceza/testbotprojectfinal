package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import utils.FilterUtil;
import utils.BotUtil;
public class TemplateLnwshopNoPriceIncludeIdHTMLParser extends TemplateLnwshopIncludeIdHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"headerText\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div id=\"detail\" class=\"tabPanel mceContentBody\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"productPhoto\">"});
		map.put("realProductId", new String[]{"<tr class=\"codeTR\">"});
		map.put("upc", new String[]{"<tr class=\"barcodeTR\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<tr class=\"priceTR\">", "</tr>");
    		if(!productPrice.isEmpty()){
	    		productPrice = FilterUtil.getStringBetween(productPrice, "<td class=\"bodyTD\">", "</td>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		if(productPrice.contains("-")) {
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		} else {
	    			productPrice = FilterUtil.removeCharNotPrice(productPrice); 
	    		}
	    		if("0.00".equals(productPrice)) {
	        		productPrice = BotUtil.CONTACT_PRICE_STR;
	        	}
    		}
        }
		return new String[] {productPrice};
    }
	
	@Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	productId = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"bodyTD\">", "</td>").trim();
    	return new String[]{ productId};
    }
}
