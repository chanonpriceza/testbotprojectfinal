package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ReadyPlanetSSHTMLParser extends DefaultHTMLParser {
	public String getCharset() {
		return "windows-874";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class='h1'>"});
		map.put("price", new String[]{"<tr class='table-form' valign='top'>"});
		map.put("basePrice", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		productName = StringEscapeUtils.unescapeHtml4(productName);
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>สมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ราคาสมาชิก</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ชิ้น</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 คู่</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>1 ตัว</span>", "</div>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p align=\"left\"><font color=\"#ff0000\" size=\"2\" face=\"Arial\">", "</font>");
    		}
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", "บาท");
    			if(rawPrice.contains("<div class='h3' style='color:#333333'>")){
        			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class='h3' style='color:#333333'>", "</div>");
        		}
    		}
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		productPrice = productPrice.replace("&nbsp;", "");
    		if("** สินค้านำเข้า ประมาณ 20 วัน**".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		if(productPrice.length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ", "</div>");
    		
    		if(rawPrice.length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style='color:#333333'>ปกติ</span>", "</div>");
    		}
    		
    		bPrice = FilterUtil.toPlainTextString("<div>"+rawPrice+"</div>");
    		bPrice = bPrice.replace("&nbsp;", "");
    		
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดทั้งหมด :", "<input type='hidden' name='cart' value='add'>");
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "คุณสมบัติพิเศษ", "คุณสมบัติสินค้า");
    		}
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:20 0 5 0;'><span class='hd2'>", "<input type='hidden' name='cart' value='add'>");
    		}
    		rawDescription = StringEscapeUtils.unescapeHtml4(rawDescription);
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDescription+"</div>");	
    		productDesc = productDesc.replaceAll(" ", " "); // replace special space to common space 
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<div style = \"clear:both;padding:5px\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href = \"", "\"");
    		if(productPictureUrl.length() == 0){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
    		}
        }
    	return new String[] {productPictureUrl};
	}  
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String isExpire = "false";
		if(evaluateResult.length > 0) {
			String expire = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>", "</span>");
			if(expire.contains("สินค้าหมด")){
				isExpire = "true";
			}
			if(expire.contains("สินค้าหมด stock")){
				isExpire = "true";
    		}
			
			if(expire.contains("(Sold out)")){
				isExpire = "true";
			}
		}
		return new String[] {isExpire};
	}
}
