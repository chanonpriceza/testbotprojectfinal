package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SiamPillHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		new String[]{"<h1>"});
		map.put("price", 		new String[]{"<div class=\"product-info\">"});
//		map.put("basePrice",    new String[]{"<div class=\"product-info\">"});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-info\">"});  
		map.put("realProductId", new String[]{"<div class=\"product-info\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  
		
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price\">", "<br");
			if(productPrice.equals("")){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\" id=", "</div>");
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\"", "/span>");
				productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
		}
		return new String[] {productPrice};
	}
	
//	@Override
//	public String[] getProductBasePrice(String[] evaluateResult) {
//		String bPrice = "";
//		if(evaluateResult.length > 0) {
//			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</div>");
//			bPrice = FilterUtil.getStringBetween(bPrice, "<span class=\"price\" id=", "/span>");
//			bPrice = FilterUtil.getStringBetween(bPrice, ">", "<");
//			bPrice = FilterUtil.removeCharNotPrice(bPrice);
//		}
//		return new String[] {bPrice};
//	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"description\" content=\"", "\"");
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if(evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	
	 public String[] getRealProductId(String[] evaluateResult) {
		 String pdId = "";
			if (evaluateResult.length > 0 ) {
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<span>รหัสสินค้า:</span>", "<br");
	    	}
			return new String[] { pdId};
	 }
	 
	 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			
			String[] productId = mpdBean.getRealProductId();
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			
			if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0] + " ("+productId[0] + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				rtn[0].setPictureUrl(productPictureUrl[0]);
			}
			
			return rtn;
		}
}

