package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class MunkongGadgetHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		new String[]{"<div class=\"product-name\">"});
		map.put("price", 		new String[]{"<div class=\"product-price\">"});
		map.put("basePrice",    new String[]{"<div class=\"product-price\">"});
		map.put("description", 	new String[]{"<div class=\"product-brand\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"col-xs-10 padding-right product-detail\">"});
		map.put("expire"		, new String[]{""});
		map.put("realProductId"		, new String[]{""});
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h3>", "</h3>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<td><span class=\"price\" style=\"color:#F00\">", "</span></td>");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price_original\">", "</span>");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price_original strikethrough\" style=\"color:#000\">", "</span>");
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"src=\"","\"");
		}
		return new String[] {productPic};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {

		if(evaluateResult.length > 0) { 
			if(evaluateResult[0].contains("<h4>*สินค้าหมดชั่วคราว</h4>")){
				return new String[]{"true"};
			}
		}
		return new String[]{"false"};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if (evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0],"<input type=\"hidden\" id=\"param_product_id\" value=\"","\"");
		}
		return new String[] {realProductId};
	}
	
	

}
