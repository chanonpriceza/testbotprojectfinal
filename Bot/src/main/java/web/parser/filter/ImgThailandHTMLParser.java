package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ImgThailandHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<h1>"});
		map.put("price", 		new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", 	new String[]{"<p class=\"old-price\">"});
		map.put("description", 	new String[]{"<div class=\"tab-pane  active\" id=\"tab1\">"});
		map.put("pictureUrl", 	new String[]{"<p class=\"product-image\">"});
		
		map.put("expire", new String[]{"<p class=\"view-mode\">","<div class=\"product-shop\">"});
		
		return map;
	}
   
   public String[] getProductExpire(String[] evaluateResult) {
    	
	   	if(evaluateResult.length == 2) { //list
		   return new String[]{"true"};
		}    	
		if(evaluateResult.length == 1) { //product
			if(evaluateResult[0].indexOf("out-of-stock") != -1) {
				return new String[]{"true"};
			}     	
		}    	
		return new String[]{""};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		if(evaluateResult.length > 0){
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(evaluateResult.length > 0){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\"");
		}
		
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		
    	return new String[] {productPictureUrl};
	}
}