package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class PowerdigitalshopHTMLParser extends DefaultHTMLParser {	
	
	public String getCharset() {
		return "UTF-8";
	}
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"descriptionContainer \">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<h1 class=\"title title-with-text-alignment\">"});
		return map;
	}
	
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	String exp = "false";
		if(evaluateResult.length == 1) {			
			
			if(evaluateResult[0].contains("สินค้าหมด")){
				exp = "true";
			}
		}
		
		return new String[] {exp};
    }
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length == 1) {
			if(!evaluateResult[0].contains("<input type=\"hidden\" id=\"content_id\" name=\"content_id\" value=\"")){
				return null;
			}
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"title title-with-text-alignment\">", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = "<span class=\"price\">"+FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "<div id=\"footer\">");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	if(productPrice.equals("0") || productPrice.isEmpty()
//    			|| FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"hidable-container\">", "<div id=\"footer\">").contains("สอบถามราคาสินค้า")
//    			|| FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"hidable-container\">", "<div id=\"footer\">").contains("สอบถามสินค้า")
    	){
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = evaluateResult[0];
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], " <link rel=\"image_src\" href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}