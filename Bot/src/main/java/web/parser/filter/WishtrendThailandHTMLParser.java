package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WishtrendThailandHTMLParser extends DefaultHTMLParser {

	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
	    	map.put("name", 			new String[]{"<div id=\"product_name_wrap\" class=\"\">"});
			map.put("price", 			new String[]{"<div class=\"content_prices clearfix\">"});
			map.put("basePrice", 		new String[]{"<div class=\"content_prices clearfix\">"});
			map.put("description", 		new String[]{"<div id=\"short_description_content\" class=\"rte align_justify\" itemprop=\"description\">"});
			map.put("pictureUrl", 		new String[]{"<div id=\"image-block\" class=\"clearfix\">"});
			map.put("expire", 			new String[]{"<span id=\"availability_value\" class=\" st-label-danger\">"});
			return map;
		}
	  
	   public String[] getProductExpire(String[] evaluateResult) {
	    	
	    	if(evaluateResult.length == 1) {
	        	if(evaluateResult[0].toLowerCase().indexOf("out of stock") != -1) {
	        		return new String[]{"true"};
	        	}     	
	    	}    	
	        return new String[]{"false"};
		}
	   
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";
			if(evaluateResult.length > 0){						
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		public String[] getProductPrice(String[] evaluateResult) {
			
			String productPrice = "";   
			if(evaluateResult.length > 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"our_price_display pull-left\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">","</p>");
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			return new String[] {productPrice};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productBasePrice = "";   
			if(evaluateResult.length > 0){
				productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"<p id=\"old_price\" class=\" pull-left\">","</p>");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
			return new String[] {productBasePrice};
		}

		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
			if(evaluateResult.length > 0){
				
				productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
			
			if(evaluateResult.length > 0){
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0]," src=\"","\"");
				productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
			}
	    	return new String[] {productPictureUrl};
		}
}
