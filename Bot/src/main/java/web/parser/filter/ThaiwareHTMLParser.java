package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class ThaiwareHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 style=\"margin: 0;padding: 10px;line-height:1.5;\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{"<div class=\"short_detail\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<div class=\"code\" style=\"float:none;text-align:center;\">"});
		map.put("expire", new String[]{"<div class=\"remain_qty\" style=\"font-size:14px;color:#333;\">"});
		
		return map;
	}
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	String exp = "false";
    	if(evaluateResult.length == 1){
    		if(evaluateResult[0].contains("สินค้าหมด")){
    			exp = "true";
    		}
    	}
    	return new String[]{exp};
    }
    public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length == 1) {
	   		realProductId = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "รหัสสินค้า :", "000000");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	} 	
	    return new String[]{realProductId}; 
    }
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   	
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	if (evaluateResult.length > 0) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style=\"font-size:40px;margin-bottom:-10px;color:#666\">", "</div>");
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style=\"font-size:32px;color:#339933\">", "</div>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"numbers\">", "</span>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product_description\"  style=\"font-size: 13px;\">", "&nbsp;</p>					</div>");
		if(tmp.trim().length()==0){
			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pic_detail\">", "</div>");
		}
		
    	if(tmp.length() !=0) {
    		productPictureUrl = FilterUtil.getStringBetween(tmp, "/><img", "/>");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"../", "\"");
    		if(productPictureUrl.length() == 0) {
    			productPictureUrl =FilterUtil.getStringBetween(tmp, "src=\"../", "\"");   			
    		}
    		if(productPictureUrl.trim().length()==0){
        		tmp = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pic_detail\">", "</div>");
        		productPictureUrl = FilterUtil.getStringBetween(tmp, "/><img", "/>");
        		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"../", "\"");
        		if(productPictureUrl.length() == 0) {
        			productPictureUrl =FilterUtil.getStringBetween(tmp, "src=\"../", "\"");   			
        		}
        	}
    		
        }
    	return new String[] {productPictureUrl};
	}
}
