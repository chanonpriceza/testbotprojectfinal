package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ClassicGameShopHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<h1 class=\"product-name\">"});
		map.put("price", 		new String[]{""});
		map.put("description", 	new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"thumbnails\">"});
		
		return map;
	}
    
      public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";	
		if (evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
        }
	     
 		return new String[] {productName};
	}
      public String[] getProductPrice(String[] evaluateResult) {
          
			String productPrice = "";   
		 if (evaluateResult.length > 0) { 
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price-box box-regular\">", "</div>");
	    		productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 	
	    		if(StringUtils.isBlank(productPrice)) {
		        	productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"form-group mt-15\">", "</div>");
		    		productPrice = FilterUtil.getStringBetween(productPrice, "style=\"display:inline-block;margin-left:10px;\">มัดจำ (", " ฿)</label>");
		    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 
		        }	
	        } 
	        
  		return new String[] {productPrice};
  	}
      public String[] getProductPictureUrl(String[] evaluateResult) {
  		
  		String productPictureUrl = null;
      	if (evaluateResult.length > 0) {
      		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\" title=\"");
          }
      	
      	return new String[] {productPictureUrl};
  	}
      public String[] getProductDescription(String[] evaluateResult) {
  		
  		String productDesc = "";    	
      	if(evaluateResult.length > 0) {
      		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "DESCRIPTION", evaluateResult[0]);
      		productDesc = FilterUtil.toPlainTextString(productDesc);
      	}
      	return new String[] {productDesc};
  	}
	
}