package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class OHVAVAHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-7 product-shop\">"});
		map.put("basePrice"		, new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-7 product-shop\">"});
		map.put("description", new String[]{"<div class=\"product-specs\">"});
		map.put("pictureUrl", new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-5 product-img-box\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length >= 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String checkStock = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"availability in-stock\"><b>สถานะสินค้า:</b> <span><FONT COLOR=#009900><b>", "</b>");
    		if(checkStock.contains("พร้อมจัดส่ง")){
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if(productPrice.length() < 1){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	}
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if (evaluateResult.length == 1) {
			String checkBase = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
			if(checkBase.length() > 0){
				productBasePrice = FilterUtil.getStringBefore(checkBase, "บาท", "\">");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);		
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}