package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class StardustShopsHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("basePrice", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div id=\"rte\" class=\"clearfix\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"thumb clearfix\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";   				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}		
		return new String[] {productName};
	}

	public String[] getRealProductId(String[] evaluateResult) {
		String pId = "";
    	if (evaluateResult.length == 1) {
    		pId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" id=\"product\" value=\"3", "\"");
    		pId = FilterUtil.toPlainTextString(pId);
    		pId = pId.trim();
        }
    	return new String[]{pId};
	}
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins itemprop=\"price\"", "</ins>");
    		if(productPrice.isEmpty()){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\"", "</span>");
    		}
    		productPrice = FilterUtil.getStringAfter(productPrice, ">", productPrice);
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";    	
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);

		}		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";   	
    	if(evaluateResult.length > 0) {
    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0 && realProductId[0] != "") {
			productName[0] = productName[0] + " (" + realProductId[0]+")";
		}
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}


		return rtn;
	}
}
