package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ELVIRAHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"title\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"tab-content tab-description\" id=\"content_tab_description\" style=\"display:block;\">"});
		map.put("pictureUrl", new String[]{"<div class=\"images\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length == 1) {
			
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"product:price:amount\" content=\"", "\"");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

			if("0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = "9999999";
			}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียด", evaluateResult[0]);    	
			productDesc = FilterUtil.toPlainTextString(productDesc);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
}