package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class WorldBikeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-container productdetails-view productdetails\">"});
		map.put("price", new String[]{"<div class=\"spacer-buy-area\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"main-image\">"});	
		map.put("expire", new String[]{"<div class=\"yjsg-newsitems\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		String category = "";   
		if(evaluateResult.length > 0) {
			category = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"product-short-description\">","</div>");
			productName = category + productName;
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"PricesalesPrice\">","</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.length() == 0 || productPrice.equals(("0"))){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDescription = "";
    	if(evaluateResult.length > 0) {

			String Sizes = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Sizes</span></th>", "</td>");
			String Colors = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Colors</span></th>", "</td>");
			String Frame = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Frame</span></th>", "</td>");
			String Fork = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Fork</span></th>", "</td>");
			String Shock = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Shock</span></th>", "</td>");
			String Handlebar = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Handlebar</span></th>", "</td>");
			String Stem = FilterUtil.getStringAfter(evaluateResult[0], "<th style=\"border-bottom: 1px solid #dbdbdb;\"><span style=\"color: #000000;\">Stem</span></th>", "</td>");
			Sizes = FilterUtil.getStringBetween(Sizes, "<span style=\"color: #000000;\">", "</span>");
			Colors = FilterUtil.getStringBetween(Colors, "<span style=\"color: #000000;\">", "</span>");
			Frame = FilterUtil.getStringBetween(Frame, "<span style=\"color: #000000;\">", "</span>");
			Fork = FilterUtil.getStringBetween(Fork, "<span style=\"color: #000000;\">", "</span>");
			Shock = FilterUtil.getStringBetween(Shock, "<span style=\"color: #000000;\">", "</span>");
			Handlebar = FilterUtil.getStringBetween(Handlebar, "<span style=\"color: #000000;\">", "</span>");
			Stem = FilterUtil.getStringBetween(Stem, "<span style=\"color: #000000;\">", "</span>");


			if(StringUtils.isNoneBlank(Sizes)) {
				productDescription += " Sizes:  "+Sizes+";";
			}
			if(StringUtils.isNoneBlank(Colors)) {
				productDescription += " Colors:  "+Colors+";";
			}
			if(StringUtils.isNoneBlank(Frame)) {
				productDescription += " Frame:  "+Frame+";";
			}
			if(StringUtils.isNoneBlank(Fork)) {
				productDescription += " Fork:  "+Fork+";";
			}
			if(StringUtils.isNoneBlank(Shock)) {
				productDescription += " Shock:  "+Shock+";";
			}
			if(StringUtils.isNoneBlank(Sizes)) {
				productDescription += " Handlebar:  "+Handlebar+";";
			}
			if(StringUtils.isNoneBlank(Stem)) {
				productDescription += " Stem:  "+Stem+";";
			}
			
	
    	}
    	return new String[] {productDescription};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
    		productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductExpire(String[] evaluateResult) {
		
    	if(evaluateResult.length == 1) {
        	return new String[]{"true"};
    	}    	
        return new String[]{""};
	}
}
