package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ThevisionopticHTMLParser extends DefaultHTMLParser{

	@Override
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name",				new String[]{"<div class=\"summary entry-summary\">"});
		map.put("price",			new String[]{"<p class=\"price\">"});
		map.put("basePrice",		new String[]{"<p class=\"price\">"});
		map.put("description",		new String[]{"<div itemprop=\"description\">"});
		map.put("pictureUrl",		new String[]{"<div class=\"a3dg-thumbs \">"});
		map.put("expire",			new String[]{""});
		return map;
	}

	
	@Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		
		if(evaluateResult.length == 1){
			String brand = FilterUtil.getStringBetween(evaluateResult[0], "ยี่ห้อ :", "<br />");
			brand = FilterUtil.toPlainTextString(brand);
			String subbrand = FilterUtil.getStringBetween(evaluateResult[0], "รุ่น :", "<br />");
			subbrand = FilterUtil.toPlainTextString(subbrand);
			
//			if(catName.isEmpty() || brand.isEmpty() || subbrand.isEmpty()){
//				return new String[] {};
//			}
			productName = "แว่นตา  "+brand.trim()+" รุ่น  " + subbrand.trim();
		}
		return new String[] {productName};
	}
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length == 1){			
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<ins>","</ins>");
			productPrice = FilterUtil.getStringBetween(productPrice,"<span class=\"woocommerce-Price-amount amount\">","&nbsp;");
			if(productPrice.isEmpty()){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"woocommerce-Price-amount amount\">","&nbsp;");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length == 1){
			basePrice = FilterUtil.getStringBetween(evaluateResult[0],"<del>","</del>");
			basePrice = FilterUtil.getStringBetween(basePrice,"<span class=\"woocommerce-Price-amount amount\">","&nbsp;");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDescription = ""; 
		if(evaluateResult.length == 1){
			//productDescription = FilterUtil.getStringAfter(evaluateResult[0], "</p>", evaluateResult[0]);
			String pre = "ยี่ห้อ :";
			productDescription = FilterUtil.getStringBetween(evaluateResult[0], "ยี่ห้อ :", "</p>");
			if(productDescription.isEmpty()){
				productDescription = FilterUtil.getStringBetween(evaluateResult[0], "รุ่น :", "</p>");
				pre = "รุ่น :";
			}

			if(!productDescription.isEmpty()){
				productDescription = pre + productDescription;
			}
			
			productDescription = FilterUtil.toPlainTextString(productDescription);
			
		}
		return new String[] {productDescription};
	}
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productUrl = "";
		if(evaluateResult.length == 1){
			productUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
		}
		return new String[] {productUrl};
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "";
		if(evaluateResult[0].contains("out-of-stock")){
			expire = "true";
		}
		return new String[] {expire};
	}
}
