package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TPProPrinterHTMLParser extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"showTopic\" >"});
		map.put("price", new String[]{"<div id=\"r-detail\">"});
		map.put("basePrice", new String[]{"<div id=\"r-detail\">"});
		map.put("description", new String[]{"<h2 class=\"showTopic\" >"});
		map.put("pictureUrl", new String[]{"<div class=\"right-table\" id=\"show_page\">"});
		map.put("expire", new String[]{""});
		return map;
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0 && evaluateResult[0].contains("ปิดการขายสินค้านี้แล้ว")) {
			return new String[] {"true"};
		}
		return new String[] {"false"};
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li style=\"margin-left:0px;\" class=\"price-sale\">", "<div id=\"list-unit\" >");
    		productPrice = FilterUtil.getStringBetween(productPrice, "<li><label>ลดเหลือ :", "</li>");
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li style=\"margin-left:0px;\" class=\"right-price\">", "</li>");	
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li style=\"margin-left:0px;\" class=\"price-sale\">", "</li>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "<h2 class=\"showTopic\" >รายละเอียดสินค้า", "<h2 class=\"showTopic\">คำค้นหา");
    		productDesc = FilterUtil.toPlainTextString(rawDescription);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}
