package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BanchaintermediaHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"productdetails-view\">"});
		map.put("price", new String[]{"<div class=\"productdetails-view\">"});
		map.put("description", new String[]{"<div class=\"product-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"additional-images\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(rawName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"spacer-buy-area\">", "</div>");
    		rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"PricesalesPrice\" >", "บาท");
    		if(rawName.trim().length() == 0){
    			rawName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-short-description\">", "บาท");
    		}
    		productPrice = FilterUtil.toPlainTextString(rawName);
    		if("โทรสอบถามราคาพิเศษ".contains(rawName)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = FilterUtil.getStringBefore(productDesc, "document", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	

}