package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;
public class ThaiSmartCashHTMLParser extends DefaultHTMLParser{
			
//	public String getCharset() {
//		return "UTF-8";
//	}
	
	//FilterUtil
	/*
	    
		FilterUtil.removeCharNotPrice(String price)
          - to remove char except "0123456789."

		FilterUtil.toPlainTextString(String html);
       
		FilterUtil.getStringAfter(target, begin, defaultValue)
		FilterUtil.getStringAfterLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBefore(target, begin, defaultValue)
		FilterUtil.getStringBeforeLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBetween(target, begin, end)

	*/
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price_style\">", "คะแนนและความคิดเห็น");
			productName = FilterUtil.toPlainTextString("<div>"+rawName+"</div>");			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		return new String[] {BotUtil.CONTACT_PRICE_STR};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		String rawProductDesc = FilterUtil.getStringBetween(evaluateResult[0], "<table summary=\"\" border=\"0\">", "โดยโอนมาที่");
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawProductDesc+"</div>");
    		//productDesc = productDesc.replace("&middot;", " ");
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<table summary=\"\" border=\"0\">", "<br><br>");
    		rawPicture = FilterUtil.getStringAfter(rawPicture, "<td valign=\"top\">", "");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}
