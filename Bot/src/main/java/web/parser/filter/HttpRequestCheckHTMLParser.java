package web.parser.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import web.parser.HTMLParser;

public class HttpRequestCheckHTMLParser implements HTMLParser{
	
	protected static final Logger logger = LogManager.getRootLogger();
	
	@Override
	public void setParserConfig(List<ParserConfigBean> configList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProductDataBean[] parse(String url) {
		String[] response = httpRequestWithStatus(url,"UTF-8",true);
		if(response!=null) {
			logger.info("Status ---> "+response[1]);
			logger.info("Response ---> "+response[0]);
		}else {
			logger.error("Response IS NULL");
		}
		return null;
	}

	@Override
	public FILTER_TYPE getFilterType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			logger.error("Error",e);
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
	
	

}
