package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HBXHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"offers on-sale\">"});
		map.put("description", new String[]{"<div class=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"flexslider\">"});
		map.put("expire", new String[]{"<h2 class=\"sold-out-header\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"sale-price\" content=\"", "span>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "\">", "<");
    		if (StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\">", "</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if (evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\">", "</span>");
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
   		if(evaluateResult.length > 0) {
   			if (evaluateResult[0].indexOf("sold out") > -1) {
   				return new String[] {"true"};
   			}
   		}
   		return new String[] {"false"};
   	}
	
}
