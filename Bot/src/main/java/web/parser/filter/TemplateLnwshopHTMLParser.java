package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class TemplateLnwshopHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"headerText\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div id=\"detail\" class=\"tabPanel mceContentBody\">"});
		map.put("pictureUrl", new String[]{"<div class=\"productPhoto\">"});
		map.put("upc", new String[]{"<tr class=\"barcodeTR\">"});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<tr class=\"priceTR\">", "</tr>");
    		if(!productPrice.isEmpty()){
	    		productPrice = FilterUtil.getStringBetween(productPrice, "<td class=\"bodyTD\">", "</td>");
	    		productPrice = FilterUtil.toPlainTextString(productPrice);
	    		if(productPrice.contains("-")) {
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		} else {
	    			productPrice = FilterUtil.removeCharNotPrice(productPrice); 
	    		}
    		}
        }		
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length == 1) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<tr class=\"oldpriceTR\">", "</tr>");
			if(!bPrice.isEmpty()){
				bPrice = FilterUtil.getStringBetween(bPrice, "<td class=\"bodyTD\">", "</td>");
				bPrice = FilterUtil.toPlainTextString(bPrice);
				bPrice = FilterUtil.removeCharNotPrice(bPrice); 
			}
		}		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]); 
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String upc = null;
		if (evaluateResult.length == 1) {
			upc = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"bodyTD\">", "</td>").trim();
			upc = FilterUtil.toPlainTextString(upc);
			if(!StringUtils.isNumeric(upc) || upc.length() != 13){
				upc = null;
			}
		}
		return new String[] { upc};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "";
		if(evaluateResult[0].contains("itemprop=\"availability\" content=\"out_of_stock\">")){
			exp = "true";
		}
		
		if(evaluateResult[0].contains("<div class=\"product_soldout warningBox\"><span>ขออภัย สินค้าหมดค่ะ</span></div>")){
			exp = "true";
		}
		return new String[] { exp};
	}
	    
}
