package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TemplateWeLovePlatinumWithIDHTMLParser extends DefaultHTMLParser{

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<div class=\"pro-name\" id=\"moreimage\">" });
		map.put("price", new String[] { "<div class=\"pro-dcprice\">" });
		map.put("basePrice", new String[] { "<div class=\"pro-orgprice\">" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
		map.put("expire", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
	}
	
	@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
    	Document doc = Jsoup.parse(evaluateResult[0]);
		Element result = doc.select("table.dataset-3").first();
		String productDesc = "";
		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(result.html());
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"image_src\" href=\"","\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<table class=\"dataset-1\">","</table>");
    		realProductId = FilterUtil.getStringBetween(realProductId, "</th>","</td>");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    		if(realProductId.equals("")) {
    			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pro-desc\" id=\"product-specification\">","<table class=\"dataset-1\">");
        		realProductId = FilterUtil.getStringBetween(realProductId, "<span id=\"itemid-","\">");
        		realProductId = FilterUtil.toPlainTextString(realProductId);
    		}
    	}
    	return new String[] {realProductId};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		String product = "false";   
		if(evaluateResult.length > 0) {
			if(evaluateResult[0].contains("Out of stock"))
			product = "true";
		}

		return new String[] {product};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+" ("+realProductId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
}

