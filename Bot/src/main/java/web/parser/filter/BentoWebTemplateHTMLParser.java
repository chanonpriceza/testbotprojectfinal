package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class BentoWebTemplateHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-title\" itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("basePrice", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div class=\"rte product-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-gallery\">"});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{"<div class=\"overlay-default position absolute soldout-div\" style=\"\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   	
		if(evaluateResult.length >= 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins>", "</ins>");
    		
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span>", "</span>");
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		
		String pdExpire = "false";
		if (evaluateResult.length >= 1) {	
			if(StringUtils.isNotBlank(evaluateResult[0]) && evaluateResult[0].contains("Sold out")){
				pdExpire = "true";
			}
		}
    	return new String[] {pdExpire};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(evaluateResult.length == 1){
			pdId = FilterUtil.getStringBetween(evaluateResult[0], "name=\"product_id\" value=\"", "\"");
		}
		return new String[] {pdId};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] nameDescription = mpdBean.getProductNameDescription();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productName.length == 0  || productName[0].isEmpty() || productPrice == null || productPrice.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		String pdName = productName[0];		
		if(realProductId[0] != null && realProductId.length > 0){
			String realPId = realProductId[0];
			if (!realPId.isEmpty()){
				pdName += " ("+ realPId +")";
				rtn[0].setRealProductId(realPId);
			}
		}
		
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		if (nameDescription != null && nameDescription.length != 0){
			rtn[0].setKeyword(nameDescription[0]);
		}
		
		return rtn;
	}
	
}
