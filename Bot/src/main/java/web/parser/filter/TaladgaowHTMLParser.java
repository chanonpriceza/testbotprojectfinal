package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TaladgaowHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", new String[]{""});
		map.put("basePrice"		, new String[]{""});
		map.put("description", new String[]{"<div class=\"divProductDescription\">"});
		map.put("pictureUrl", new String[]{"<div class=\"col-12 col-md-5\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length >= 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "\"price\":\"", "\",");
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if (evaluateResult.length == 1) {
			String checkBase = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
			if(checkBase.length() > 0){
				productBasePrice = FilterUtil.getStringBefore(checkBase, "บาท", "\">");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);		
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
