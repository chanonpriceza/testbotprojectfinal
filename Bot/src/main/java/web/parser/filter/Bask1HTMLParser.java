package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class Bask1HTMLParser extends DefaultHTMLParser{

		public Map<String, String[]> getConfiguration() {
	    	
			Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("description", new String[]{"<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" border=\"0\">"});
			map.put("pictureUrl", new String[]{"<div style='margin:15px 0 15px 0;'>"});
//			map.put("expire", new String[]{""});
			
			return map;
		}
	 
		@Override
		public String getCharset() {			
			return "windows-874";
		}
	 	public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";
			if(evaluateResult.length == 1) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "<");				
			}
			return new String[]{productName};
		}
	 	
	 	public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = ""; 
			
	    	if (evaluateResult.length == 1) {
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:5px 0 1px 0;'><span class='h3'>", "</div>");

	    		if(productPrice.isEmpty()){
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='price_set'>", "</span>");
	    		}
	    		if(productPrice.isEmpty()){
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class='h3' style='color:#333333'>", "<");
	    		}
	    		if(productPrice.isEmpty()){
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style='color:#333333'>", "<");
	    		}
	    		if(productPrice.length() == 0){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}
	        }	
	    	productPrice = FilterUtil.toPlainTextString(productPrice);
	    	productPrice = FilterUtil.removeCharNotPrice(productPrice);
			return new String[]{productPrice};
		}
	 	
	 	@Override
	 	public String[] getProductBasePrice(String[] evaluateResult) {
	 		String basePrice = "";
	 		if(evaluateResult.length == 1){
	 			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='price_set'><strike>", "</strike>");
	 			if(basePrice.isEmpty()){
	 				basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<div style='color:#333333'><strike>", "</strike>");
	    		}
	 		}
	 		basePrice = FilterUtil.toPlainTextString(basePrice);
	 		basePrice = FilterUtil.removeCharNotPrice(basePrice);
	 		return new String[]{basePrice};
	 	}
	 	
	 	public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";    	
	    	if(evaluateResult.length == 1) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
	 
	 	public String[] getProductPictureUrl(String[] evaluateResult) {

			String productPictureUrl = "";
	    	if (evaluateResult.length > 0) {
	    		productPictureUrl = FilterUtil.getStringBefore(evaluateResult[0], "</a>", evaluateResult[0]);
	    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "href='", "'");
	        }
	    	return new String[] {productPictureUrl};
		}
	 	@Override
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			
			String currentUrl = mpdBean.getCurrentUrl();
			String[] productBasePrice = mpdBean.getProductBasePrice();
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			
			rtn[0].setName(productName[0]);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			
			if (productBasePrice != null && productBasePrice.length != 0) {
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			}
			
			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}

			return rtn;
		}
		    
	
}
