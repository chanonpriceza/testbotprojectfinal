package web.parser.filter;

import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class OfficeEzyHTMLParser extends DefaultHTMLParser {

	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<td class=\"mainhead alignLeft\">"});
		map.put("price", new String[]{"<font color=red>"});
		map.put("basePrice", new String[]{"<s>"});
		map.put("description", new String[]{""});		
		map.put("pictureUrl", new String[]{"<td align=\"center\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<b>", "</b>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.removeHtmlTag(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	productPrice = formatPrice.format(FilterUtil.convertPriceStr(productPrice)*1.07);
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.removeHtmlTag(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"border=\"0\" /></a></td>","</td>");
    		productDesc = FilterUtil.removeHtmlTag(productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0],"<img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	@Override
	public String getCharset() {
		
		return "TIS-620";
	}
}
