package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class HomeProHTMLParser extends DefaultHTMLParser {
	
	@Override
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>();
		map.put("name",				new String[]{""});
		map.put("price",			new String[]{""});
		map.put("description",		new String[]{""});
		map.put("basePrice",		new String[]{"<div style=\"margin-top:18px;font-size:18px;color:#000000;font-weight:600;font-style:italic;text-decoration:line-through;\">"});
		map.put("pictureUrl",		new String[]{""});
		map.put("realProductId",	new String[]{""});
		map.put("expire",	new String[]{"<button class=\"ui button ui-ext\">"});
		return map;
	}
	
	@Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		Document doc = Jsoup.parse(evaluateResult[0]);
		String brandString = "";
		Elements brand = doc.select("input[id^=gtmBrand]");
		brandString = FilterUtil.toPlainTextString(brand.attr("value"))+" ";

		if(evaluateResult.length == 1){
			Elements e = doc.select("input[id^=gtmName]");
			productName = FilterUtil.toPlainTextString(e .attr("value"));
		}	
		return new String[] {brandString+productName};
	}
	
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length == 1){
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("input[id^=gtmPrice]");
			productPrice = FilterUtil.toPlainTextString(e.attr("value"));
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDescription = ""; 
		if(evaluateResult.length == 1){
			//<div id="product-information
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div#product-information");
			productDescription = FilterUtil.toPlainTextString(e.html());
		}
		return new String[] {productDescription};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productUrl = "";
		if(evaluateResult.length > 0){
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("img[id^=image-index]");
			productUrl = FilterUtil.getStringBetween(e.toString(), "src=\"", "\"");
		}
		return new String[] {productUrl};
	}
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length == 1){
			basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1){
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("input#gtmId");
			productName = FilterUtil.toPlainTextString(e.attr("value"));
		}	
		return new String[] {productName};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String productName = "false";
		if(evaluateResult.length == 1){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(productName.equals("สินค้าหมด"))
				productName = "true";
		}	
		return new String[] {productName};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	String dummy = "";
    	if(isNotEmpty(realProductId)&&realProductId[0].length()>0)
    		dummy = " ("+realProductId[0]+")";
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+dummy);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
	

}
