package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HealthyCreationHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"pro-name\" id=\"moreimage\">"});
		map.put("price", new String[]{"<span class=\"c-red\">"});
		map.put("basePrice", new String[]{"<span id=\"price-de\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	    	
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		String tmp = "";
		if (evaluateResult.length > 0) {
			tmp = FilterUtil.toPlainTextString(evaluateResult[0]);   
			int	index = tmp.indexOf("รายละเอียดสินค้า :");
    		if(index != -1) {
    			productDes = tmp.substring(index + 18);				
    		}
		}
		return new String[] {productDes};
	}


	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"list_carousel\">", "title=");
			productPic = FilterUtil.getStringBetween(productPic, "href=\"", "\"");
		}
		return new String[] {productPic};
	}

}
