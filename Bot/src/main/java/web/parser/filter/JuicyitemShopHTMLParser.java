package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class JuicyitemShopHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("basePrice", new String[]{"<del>"});
		map.put("description", new String[]{"<div class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab\" id=\"tab-description\" role=\"tabpanel\" aria-labelledby=\"tab-title-description\">"});
		map.put("pictureUrl", new String[]{"<figure class=\"woocommerce-product-gallery__wrapper\">"});
		map.put("expire", new String[]{"<p class=\"stock out-of-stock\">"});
	
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length >= 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length > 0) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins>", "</ins>");
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "");
    		} else {
    			productPrice = FilterUtil.getStringAfter(productPrice, "<span class=\"woocommerce-Price-amount amount\">", "");
    		}
    		productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";
    	if (evaluateResult.length > 0) {	
    		bPrice = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"woocommerce-Price-amount amount\">", "");
    		bPrice = FilterUtil.getStringBetween(bPrice, "</span>", "</span>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "false";
		if (evaluateResult.length == 1) {
			exp = "true";
		}
		return new String[] {exp};
	}
}
