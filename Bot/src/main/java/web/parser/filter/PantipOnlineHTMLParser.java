package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PantipOnlineHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<div class=\"prices-block\">"});
		map.put("description", new String[]{"<td class=\"descr\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {			
			String html = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"product-market-price\">", evaluateResult[0]);
			html = FilterUtil.getStringBetween(html, "<span id=\"product_price\">", "</span>");
			html = FilterUtil.toPlainTextString(html);
			productPrice = FilterUtil.removeCharNotPrice(html);
			if("0.0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = "9999999";
			}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"image-box\"", "</div>");
		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
		
    	return new String[] {productPictureUrl};
	}
}