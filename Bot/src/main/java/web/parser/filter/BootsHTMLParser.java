package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BootsHTMLParser  extends DefaultHTMLParser {
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1>"});
			map.put("price", new String[]{""});
			map.put("description", new String[]{"<div class=\"std\">"});
			map.put("pictureUrl", new String[]{"<div class=\"zoom-container\">"});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";
			if(evaluateResult.length > 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) { 	
			return new String[] {BotUtil.CONTACT_PRICE_STR};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
	    		productDesc = FilterUtil.toPlainTextString(productDesc);
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length > 0) {	
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
	    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
	        }
	    	return new String[] {productPictureUrl};
		}
				
}
