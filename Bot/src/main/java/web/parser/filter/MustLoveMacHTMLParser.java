package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MustLoveMacHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"additional-attributes-wrapper table-wrapper\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<span class=\"text-stock\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		String brand = "";
		if(evaluateResult.length > 0) {			
			brand = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"value\" itemprop=\"brand\">", "</div>");	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"base\" data-ui-id=\"page-title-wrapper\" itemprop=\"name\">", "</span>");	
			productName = brand+" "+productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	} 
    	if(productPrice.trim().equals("0")) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";    	
		if (evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"old-price\">", "</div>");
			basePrice = FilterUtil.getStringBetween(basePrice, "<span class=\"price\">", "</span>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
    	} 
		return new String[] {basePrice};
	}
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if (evaluateResult.length > 0) {
			if (evaluateResult[0].contains("Out of stock")) {
				return new String[] { "true" };
			}
		}
		return new String[] { "false" };
	}
}