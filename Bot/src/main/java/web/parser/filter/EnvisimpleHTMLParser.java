package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class EnvisimpleHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span id=\"ctl00_ContentPlaceHolder2_FormViewContent_ItemTitle\" class=\"title\">"});
		map.put("price", new String[]{"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"});
		map.put("pictureUrl", new String[]{"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
 
   		
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<font class='realprice'>", "</font>");

    		if(tmp.length() == 0) {
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<font class='saleprice'>", "</font>");    			
    		}
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		if(productPrice.length() == 0) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;   			
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}