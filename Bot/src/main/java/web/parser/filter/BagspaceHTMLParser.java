package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BagspaceHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-single__title\">"});
		map.put("price", new String[]{"<span class=\"price-item price-item--sale\" data-sale-price>"});
		map.put("basePrice", 	new String[]{"<span class=\"price-item price-item--regular\" data-regular-price>"});
		map.put("description", new String[]{"<div class=\"product-single__description rte\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<span class=\"price-item price-item--regular\" data-regular-price>"});

		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = "Bagspace " + productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"feature-row__image product-featured-img lazyload\"", "data-src=\"");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, " src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}

	
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "";
    	if (evaluateResult.length == 1) {
    		if(evaluateResult[0].contains("Sold out")) {
    			expire = "true";
    		}
  
        }
    	return new String[] {expire};
	}	
	
	

}