package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TVDmomoRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<title>"});
		map.put("price", 		 new String[]{"<span class=\"js-goodsPrice\">"});
		map.put("basePrice", 	 new String[]{"<span class=\"js-goodsCustPrice\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			String price = "";
			for (String eva : evaluateResult) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(eva));
			}
			return new String[] { price };
		}
		return null;
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			String price = "";
			for (String eva : evaluateResult) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(eva));
				break;
			}
			return new String[] { price };
		}
		return null;
	}
	
	
	
}

