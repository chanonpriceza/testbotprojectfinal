package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ISmartLifeHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "TIS-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<td class=\"mainhead alignLeft\">"});
		map.put("price", new String[]{"<div id=\"productBox\">"});
		map.put("description", new String[]{"<div id=\"productBox\">"});
		map.put("pictureUrl", new String[]{"<div id=\"productBox\">"});
		map.put("basePrice",new String[] {"<div id=\"productBox\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<b>", "</b>");    		
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
    		String html = evaluateResult[0];
    		String tmp = FilterUtil.getStringBetween(html, "<b> ราคาพิเศษ", "</b>");
    		
    		if(tmp.length() == 0) {
    			tmp = FilterUtil.getStringBetween(html, "<b> ราคาปกติ", "</b>");
    		}    		
    		productPrice = FilterUtil.toPlainTextString("<div>" + tmp + "</div>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		if(productPrice.trim().length() == 0) {
    			if(html.contains("<font color=red>&nbsp; Call </font>")) {
    				productPrice = BotUtil.CONTACT_PRICE_STR;
    			}
    		} else {
    		
	    		if(html.contains("(สินค้ายังไม่รวมภาษี)") || html.contains("( Exclude Vat )")) {
	    			double price = FilterUtil.convertPriceStr(productPrice);
	    			price = price * 1.07;
	    			productPrice = String.valueOf(Math.round(price));
	    		}
    		}
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"msgShoppingCart\">", "");
    		tmp = FilterUtil.getStringAfter(tmp, "</tr>", "");
    		productDesc = FilterUtil.toPlainTextString("<div>" + tmp + "</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		
    		String tmp = FilterUtil.getStringAfter(evaluateResult[0], "addcart.png", "");
    		productPictureUrl = FilterUtil.getStringBetween(tmp, "<img src=\"", "\"");
    		
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
    		String html = evaluateResult[0];
    		String tmp = FilterUtil.getStringBetween(html, "<s>", "</s>");
    		tmp = FilterUtil.removeCharNotPrice(tmp);
    		productPrice = tmp;
    		double price = FilterUtil.convertPriceStr(productPrice);
			price = price * 1.07;
			productPrice = String.valueOf(Math.round(price));
    		
    	}
    	return new String[]{productPrice};
	}
	    
}