package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class Ploymarble999HTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"col-sm-6 item-product-detail\">"});
		map.put("price", new String[]{"<div class=\"price\">"});
		map.put("description", new String[]{"<div class=\"item-description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<div class=\"product-detail-type clearfix\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"title\">", "</div>");			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {BotUtil.CONTACT_PRICE_STR};
	}
	

	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
    	if (evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("สินค้าหมด"))
    		expire = "true";
        }
    	return new String[] {expire};
	}

	
}

