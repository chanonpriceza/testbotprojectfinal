package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CutePressHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", 		new String[]{"<div class=\"product-shop\">"});
		map.put("price", 		new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", 	new String[]{"<div class=\"price-box\">"});
		map.put("description", 	new String[]{"<div id=\"product_tabs_description_tabbed_contents\">"});
		map.put("pictureUrl", 	new String[]{"<p class=\"product-image\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";  
		if(evaluateResult.length != 0){
			productName = "Cute press "+FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>"));		
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		if(evaluateResult.length != 0){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price special-price\"", "</span>");
			if(productPrice.length() == 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\"", "</span>");
			}
			
			productPrice = FilterUtil.getStringAfter(productPrice, ">", productPrice);
			productPrice = FilterUtil.toPlainTextString(productPrice);		
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";   
		if(evaluateResult.length != 0){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price old-price\"", "</span>");
			productPrice = FilterUtil.getStringAfter(productPrice, ">", productPrice);
			productPrice = FilterUtil.toPlainTextString(productPrice);		
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length != 0){
			productDesc = FilterUtil.getStringAfter(evaluateResult[0],"<div class=\"std\">",evaluateResult[0]);	
			productDesc = FilterUtil.toPlainTextString(productDesc);	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		if(evaluateResult.length != 0){	
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href='", "'");
    		productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
        }
    	return new String[] {productPictureUrl};
	}

}