package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


import utils.FilterUtil;

public class BaojiHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<div class=\"sku\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String product = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		
		return new String[] {product};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String product = "";   
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[itemprop=price]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		
		return new String[] {product};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\"", "/span>");
			productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);				
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"value\"><strong>", "</strong>");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";   
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:image]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		
		return new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:description]");
			product = e.attr("content");
		}
		return  new String[] {product};
	}
	

}