package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class KitchaHTMLParser extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<li class=\"product\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"product-tabs-content\" id=\"product_tabs_description_contents\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-img-box-bottom\">"});
		map.put("expire", new String[]{"<p class=\"availability out-of-stock\">"});
		return map;
	}

	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			return new String[]{"true"};
		}
		return new String[]{"false"};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		
		if(evaluateResult.length > 0) {
			price = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"price\" itemprop=\"price\"", "");
			price = FilterUtil.getStringBetween(price, ">", "</span>");
			price = FilterUtil.toPlainTextString(price);
			price = FilterUtil.removeCharNotPrice(price);
		}
		if(price.equals("")) {
			price = BotUtil.CONTACT_PRICE_STR;
		}
		
		return new String[] {price};
	}
	
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href='", "'");
        }
    	return new String[] {productPictureUrl};
	}
}
