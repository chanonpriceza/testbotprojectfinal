package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class ChristyNgHTMLParser extends DefaultHTMLParser{
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-info\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div id=\"tab-description\" class=\"tab-content product-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"show-mobile-320\">"});
				
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h2 class=\"heading_title\"><span>", "</span></h2>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		double MYR_TO_THB = 7.81665;
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-new\">", "</span>");
    		if(productPrice.isEmpty()){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"pricetag\">","</div>");
    		}  
    		
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = Math.round((FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice(productPrice))*MYR_TO_THB))+"";
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0],"</h2><p>","</div></body></html>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
    		productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}   
}