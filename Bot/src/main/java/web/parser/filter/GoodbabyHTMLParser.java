package web.parser.filter;

import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class GoodbabyHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		return evaluateResult;
	}

	public String[] getProductPrice(String[] evaluateResult) {
		return evaluateResult;
	}

	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		ProductDataBean[] rtn = null;
		
		try {
			String html = mpdBean.getProductName()[0];
			String data = FilterUtil.getStringBetween(html, "<script type=\"application/json\" id=\"ProductJson-product-template\">", "</script>");
			JSONObject allObj = (JSONObject) new JSONParser().parse(data);
			JSONArray objArr = (JSONArray) allObj.get("variants");
			if(objArr != null && objArr.size() > 0){
				
				rtn = new ProductDataBean[objArr.size()];
			
				int count=0;
				for(Object obj : objArr){
	
					JSONObject jObj = (JSONObject) obj;
					JSONObject imageObj = (JSONObject) jObj.get("featured_image");
	
					String urlName = StringUtils.defaultIfBlank(String.valueOf(allObj.get("handle")), "");
					
					String variant = StringUtils.defaultIfBlank(String.valueOf(jObj.get("id")), "");
					String name = StringUtils.defaultIfBlank(String.valueOf(jObj.get("name")), "");
					String price = StringUtils.defaultIfBlank(String.valueOf(jObj.get("price")), "");
					String desc = StringUtils.defaultIfBlank(String.valueOf(jObj.get("name")), "");
					String image = (imageObj != null)? StringUtils.defaultIfBlank(String.valueOf(imageObj.get("src")), "") : "";
					String url = "https://goodbabyshop.com/collections/goodbaby-international/products/" + URLEncoder.encode(urlName,"UTF-8") + "?variant=" + variant;
					String sku = StringUtils.defaultIfBlank(String.valueOf(jObj.get("sku")), "");
					ProductDataBean pdb = new ProductDataBean();
					pdb.setName(name);
					pdb.setPrice(FilterUtil.convertPriceStr(price)/100);
					pdb.setDescription(FilterUtil.toPlainTextString(desc));
					pdb.setPictureUrl(image);
					pdb.setUrl(url);
					pdb.setUrlForUpdate(url);
					pdb.setRealProductId(sku);
					
					rtn[count] = pdb;
					
					count++;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return rtn;
	}	

}
