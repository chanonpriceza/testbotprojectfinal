package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BBBigInfoHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "windows-874";
	}
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<div class=\"product-name\">"});
		map.put("price", 		new String[]{"<p class=\"special-price\">"});
		map.put("basePrice",	new String[]{"<span class=\"pricenomal\">"});
		map.put("description", 	new String[]{"<div class=\"detail_x col-lg-12 col-sm-12 col-xs-12\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-image\">"});
		
		map.put("expire", new String[]{"<span class=\"label label-danger\" style=\"color:#FFF;\">"});		

		return map;
	}
    
    //check product expire
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("หมด") != -1) {
        		return new String[]{"true"};
        	}     	
    	}    	
        return new String[]{""};
	}
	
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBefore(evaluateResult[0] ,"<div class=\"sub-product-name\">",evaluateResult[0]);			
			productName = FilterUtil.toPlainTextString(productName);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.length() <= 0 || productPrice.length() > 7){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getproductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			if(productBasePrice.length() <= 0 || productBasePrice.length() > 7){
				productBasePrice = BotUtil.CONTACT_PRICE_STR;
			}
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length > 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[1]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length >0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}