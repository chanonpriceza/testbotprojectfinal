package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MadoocomHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"product-specs\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-view\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   			
		if(evaluateResult.length  == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}	
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
		if (evaluateResult.length >= 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
			if(StringUtils.isBlank(productPrice)){
				productPrice = evaluateResult[0];
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
    	
    	if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice) || "0.00".equals(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], " <p class=\"old-price\">", "</p>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img itemprop=\"image\" src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
