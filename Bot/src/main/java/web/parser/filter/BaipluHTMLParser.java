package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BaipluHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-title\" itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div class=\"rte product-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<div class=\"sold-out\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = BotUtil.CONTACT_PRICE_STR;
    	if (evaluateResult.length > 0) {
        	productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);
	
    		if(productPrice.isEmpty()){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"").trim();
    		/*if(productPictureUrl.startsWith("https://")){
    			productPictureUrl = productPictureUrl.replace("https://", "http://");
    		}*/
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "";
		if(evaluateResult.length > 0){
			exp = "true";
		}
		return new String[] { exp};
	}
}