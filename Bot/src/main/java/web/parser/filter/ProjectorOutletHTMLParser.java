package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ProjectorOutletHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"product-specs\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-image\">"});
		map.put("expire", 		new String[]{"<p class=\"availability out-of-stock\">"});
		map.put("basePrice", new String[]{"<div class=\"price-box\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		
		if (evaluateResult.length == 1) {

			if (evaluateResult[0].indexOf("สินค้าหมด") > -1) {
				return new String[] { "true" };
			}
		}
		return new String[] { "false" };
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String priceBuff = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if (priceBuff == null || priceBuff.trim().length() == 0) {
    			priceBuff = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		} else {
    			priceBuff = FilterUtil.getStringBeforeLastIndex(priceBuff, "</span>", "");
    			priceBuff = FilterUtil.getStringAfterLastIndex(priceBuff, ">", "");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(priceBuff);
    		
    		if("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = "9999999";
			}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		if (productDesc.indexOf("?php")>-1) {
    			String tmp = FilterUtil.getStringBetween(productDesc, "<?php", "?>");
    			productDesc = productDesc.replace(tmp, "").replace("<?php?>", "");
    		}
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}