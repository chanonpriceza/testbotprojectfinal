package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class BeautySaveShopHTMLParser extends WeLoveShoppingFilterHTMLParser{

	 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();
			String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

			if (productName == null || productName.length == 0
					|| productPrice == null || productPrice.length == 0) {
				return null;
			}
			if(productName[0].contains("หมดค่ะ") || productName[0].contains("SOLD OUT")
					|| productName[0].contains("Sold out") || productName[0].contains("ขายแล้วค่ะ") || productName[0].contains("sold out")
					|| productName[0].contains("จองหมดแล้วค่ะ")	|| productName[0].contains("ขายหมดแล้วค่ะ ") || productName[0].contains("ขายแล้วจ้า")){
				return null;
			}
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0]);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (productDescription != null && productDescription.length != 0)
				rtn[0].setDescription(productDescription[0]);
			if (productPictureUrl != null && productPictureUrl.length != 0 
					&& productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0)  {
				
				//parse relative path to absolute path	
				try {
					URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
					rtn[0].setPictureUrl(url.toString());
		    	} catch (MalformedURLException e) {
					
				}
			}

			if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null) {
					rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
				}
			}
			return rtn;
		}    
	    
}
