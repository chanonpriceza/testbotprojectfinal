package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class HeadDaddyRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<title>"});
		map.put("price", 		 new String[]{"<strong style=\"color:red;\">"});
		map.put("basePrice", 	 new String[]{"<strike>"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			String price = "";
			for (String eva : evaluateResult) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(eva));
			}
			return new String[] { price };
		}
		return null;
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			String price = "";
			for (String eva : evaluateResult) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(eva));
				break;
			}
			return new String[] { price };
		}
		return null;
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		if(evaluateResult != null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product_id\" id=\"product_id\" value=\"", "\"")};
		}
		return null;
		
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		try {
		
    	String name = mpdBean.getProductName()[0];
    	String price = mpdBean.getProductPrice()[0];
    	String basePrice = mpdBean.getProductBasePrice()[0];
    	String realId = mpdBean.getRealProductId()[0];

    	ProductDataBean resultBean = new ProductDataBean();
    	resultBean.setBasePrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice((FilterUtil.toPlainTextString(basePrice)))));
		resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice((FilterUtil.toPlainTextString(price)))));
		resultBean.setName(FilterUtil.toPlainTextString(name) + "(" + FilterUtil.toPlainTextString(realId) + ")");
		
		return new ProductDataBean[] {resultBean};
		
		}catch(Exception e) {
			return null;
		}
	}
	
	
}

