package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class GigDigitalHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "windows-874";
	}
	
	//FilterUtil
	/*
	    
		FilterUtil.removeCharNotPrice(String price)
          - to remove char except "0123456789."

		FilterUtil.toPlainTextString(String html);
       
		FilterUtil.getStringAfter(target, begin, defaultValue)
		FilterUtil.getStringAfterLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBefore(target, begin, defaultValue)
		FilterUtil.getStringBeforeLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBetween(target, begin, end)

	*/
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<font color='#FF8A00'>"});
		map.put("price", new String[]{"<FONT color=#FF0000>"});
		map.put("description", new String[]{"<td valign=\"top\">"});
		map.put("pictureUrl", new String[]{"<td valign=\"top\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		String rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียด", "Replace Code");
    		rawDescription = FilterUtil.getStringBefore(rawDescription, "Fit Model", "");
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียด", "Fit Model");
    			rawDescription = FilterUtil.getStringBefore(rawDescription, "Replace Code", "");
    		}
    		if(rawDescription.length() == 0){
    			rawDescription = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียด", "");
    		}
    		productDesc = FilterUtil.toPlainTextString("<div>"+rawDescription+"</div>");	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawpictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<td width=\"22%\" valign=top><div align=\"center\">", "</td>");
    		productPictureUrl = FilterUtil.getStringBetween(rawpictureUrl, "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	/**
	 * default mergeProductData
	 * 
	 * 
	 */
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			if(productDescription[0].equals(productName[0])){
				productDescription[0] = "";
			}
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}
	    
}