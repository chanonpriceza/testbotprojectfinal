package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BeautiCoolHTMLParser extends DefaultHTMLParser{
	
	public String getCharset() {
		
		return "UTF-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name"			, new String[]{""});
		map.put("price"			, new String[]{"<strong itemprop=\"price\" >"});
		map.put("basePrice"		, new String[]{"<span class=\"txt_grey5\">"});
		map.put("description"	, new String[]{"<div id=\"tab-1\" class=\"tab-content current\">"});
		map.put("pictureUrl"	, new String[]{"<div class=\"panelContainer\">"});
		map.put("realProductId"	, new String[]{"<div style=\"margin-top:10px; float:left;\">"});
		map.put("expire"		, new String[]{"<div id=\"pd_button\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:title\"", "/>");
			productName = FilterUtil.getStringBetween(productName, "content=\"", "\"");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";   
		if(evaluateResult.length > 0){
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);		
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
		
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);		
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
    	
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length > 0){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
    	return new String[] {productPictureUrl};
    	
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
	    String realProductId = "";
	   	if(evaluateResult.length > 0) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "\"pro_id\" value=\"", "\"");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	    return new String[]{realProductId}; 
	}
   
   
	public String[] getProductExpire(String[] evaluateResult) {
   	
		if(evaluateResult.length > 0) {
		   	if(evaluateResult[0].indexOf("bt_soldout.png") != -1) {
		   		return new String[]{"true"};
		   	}     	
		}    	
		return new String[]{""};
		
	}
   
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		
		if(!currentUrl.contains("--")) {
			return null;
		}
		
		
    	if (productName == null || productName.length == 0	|| productPrice == null || productPrice.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setName(productName[0] + " (" + realProductId[0] + ") ");
		}else{
			rtn[0].setName(productName[0]);
		}

		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL("https://www.beauticool.com/"),productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {
						//something
					}
				}
			}
		}
		
		rtn[0].setUrlForUpdate(currentUrl);
		rtn[0].setRealProductId(realProductId[0]);
		
		currentUrl = currentUrl.replace("&", "%26");
		String urlTrack = "";//"https://www.tagserve.sg/clickServlet?AID=574&MID=37&PID=37&SID=925&CID=120&SUBID=&targeturl" + currentUrl;
		rtn[0].setUrl(urlTrack);

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}
      
   
}