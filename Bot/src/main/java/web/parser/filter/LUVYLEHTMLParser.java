package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class LUVYLEHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.heading-title");
			productName = FilterUtil.toPlainTextString(e.text());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		Document doc = Jsoup.parse(evaluateResult[0]);
		if(evaluateResult.length == 1) {	
			Elements e = doc.select("li.price-new");
			productPrice = FilterUtil.removeCharNotPrice(e.text());			
		}	
		if(StringUtils.isBlank(productPrice)) {
			Elements e = doc.select("li.product-price");
			productPrice = FilterUtil.removeCharNotPrice(e.text());	
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		Document doc = Jsoup.parse(evaluateResult[0]);
		if(evaluateResult.length == 1) {	
			Elements e = doc.select("li.price-old");
			productPrice = FilterUtil.removeCharNotPrice(e.text());	
		}	
		

		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		Document doc 	= Jsoup.parse(evaluateResult[0]);
		Elements e		= doc.select("div.image");
		e 				= e.select("img");
		return new String[] {e.attr("src")};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String id = FilterUtil.getStringBetween(evaluateResult[0],"sku: \"","\"");
		return  new String[] {id};
	}
 
}
