package web.parser.filter;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import engine.BaseConfig;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TaradHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h1>" });
		map.put("price", new String[] { "<font class='ProductPrice'>" });
		map.put("basePrice", new String[] { "<font class='ProductPrice'>" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคาพิเศษ","</font>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		if(evaluateResult.length > 0 && StringUtils.isBlank(productPrice)) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคาปกติ","</font>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}
		if(evaluateResult.length > 0 && StringUtils.isBlank(productPrice)) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคา:" ,"</font>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}

		return new String[] {productPrice};
	}
	
	@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0 && StringUtils.isBlank(productPrice)) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคาปกติ","</font>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}
		return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0],"<meta name=\"description\" content=\"","\"");
			productDesc = FilterUtil.toPlainTextString(productDesc);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"image_src\" href=\"","\"");
    	}
    	try {    	
    		if("yes".equals(BaseConfig.CONF_FEED_CRAWLER_PARAM)) {
    			URI u = new URI(productPictureUrl); 
    			productPictureUrl = "https://img.tarad.com"+u.getPath();
    		}
		} catch (URISyntaxException e) {
			
		}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า:","</div>");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    	}
    	return new String[] {realProductId};
	}

	
}

