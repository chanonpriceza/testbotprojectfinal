package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AoonjaiShopHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-title entry-title\">"});
		map.put("price", new String[]{"<div class=\"price-wrapper\">"});
		map.put("description", new String[]{"<div class=\"product-short-description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[] {""});
		map.put("nameDescription", new String[] {""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductNameDescription(String[] evaluateResult) {
		String productNameDescription = "";
		if(evaluateResult.length > 0) {
			productNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "selected='selected'>", "</option>");			
		}
		return new String[] {productNameDescription};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price-wrapper\">", "/div>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "</span></span> &ndash; <span class=\"woocommerce-Price-amount amount\">", "<span class=\"woocommerce-Price-currencySymbol\">");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) { 
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img width=\"600\" height=\"600\" ", "class=\"");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productNameDescription = mpdBean.getProductNameDescription();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productName.length == 0 || productName[0].isEmpty()
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName("ที่นอนอุ่นใจ  "+productName[0] +" "+productNameDescription[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
	
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {
	
					}
				}
			}
		}
		
		return rtn;
	}
	
}