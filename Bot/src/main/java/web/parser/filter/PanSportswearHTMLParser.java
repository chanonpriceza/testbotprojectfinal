package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class PanSportswearHTMLParser extends DefaultHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();
		
		map.put("name", new String[]{"<div class=\"right col_zoom_detail\">"});
		map.put("price", new String[]{"<div class=\"right col_zoom_detail\">"});
		map.put("description", new String[]{"<div class=\"mgl20\">"});
		map.put("pictureUrl", new String[]{"<div class=\"big_img\" align=\"center\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"prod_h\">", "</div>");
			if(productName.length() > 0){
				String pdId = FilterUtil.getStringBetween(evaluateResult[0], "Code :", "</div>");
				pdId = pdId.replace(" ", "");
				productName = "Pan " + productName + " (" + pdId + ")";
			}
			productName = FilterUtil.toPlainTextString(productName);
		}
		
		return new String[]{productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		
		if(evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price_bay\" style=\"font-size: 16px;\">", "</div>");
			productPrice = FilterUtil.getStringBetween(productPrice, "เหลือ", "THB");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		return new String[]{productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		
		if(evaluateResult.length == 1) {
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "OVERVIEW, PERFORMANCE & TECHNOLOGY", "<hr class=\"hr_line\" />");
			productDesc = FilterUtil.toPlainTextString(tmp);
		}
		
		return new String[]{productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		
		if(evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-large=\"", "\"");
		}
		
		return new String[]{productPictureUrl};
	}
	
}
