package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ShopupNoPriceHTMLParser extends DefaultHTMLParser {
			
	public String getCharset() {
		return "tis-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"pro_name\">"});
		map.put("price", new String[]{"<div class=\"cart_box_price\">"});
		map.put("description", new String[]{"<div class=\"pro_detail_box\" style=\"text-align:justify;\">"});
		map.put("pictureUrl", new String[]{"<div class=\"pro_intro\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			String rawProductName = FilterUtil.getStringBefore(evaluateResult[0], "<div class=\"pro_intro\">", evaluateResult[0]);
			productName = FilterUtil.toPlainTextString(rawProductName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		String rawProductPrice = "";
    	if (evaluateResult.length > 0) {
    		rawProductPrice = FilterUtil.getStringBefore(evaluateResult[0], "ปกติ", "");
    		if(rawProductPrice.length() == 0){
    			rawProductPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h2 >", "</h2>");
    		}
    		if(rawProductPrice.length() == 0){
    			rawProductPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h2>", "</h2>");
    		}
    		productPrice = FilterUtil.toPlainTextString("<div>"+rawProductPrice+"</div>");
    		if("โทรมาสอบถาม".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0 || productPrice.equals("0.00")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		String rawPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class='zoom'", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPictureUrl, "src='", "'");
        }
    	return new String[] {productPictureUrl};
	}
}