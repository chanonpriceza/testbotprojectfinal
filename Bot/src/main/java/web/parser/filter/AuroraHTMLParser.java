package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class AuroraHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"col-md-12 col-sm-3 col-xs-3\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			productName = FilterUtil.toPlainTextString(e.attr("content"));		
			productName = "Aurora "+productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"c-product-price-final\" style=\"font-size: 30px;margin-right: 15px;\">", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span  class=\"c-product-old-price\" style=\"font-size: 20px;margin-right: 15px;\">", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);				
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:url]");
			product = e.attr("content");
			product = FilterUtil.getStringBetween(product, "/detail/", "/");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "imgshow=\"", "\"");
			product = "https://www.aurora.co.th"+product;
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:description]");
			product = e.attr("content");
		}
		return  new String[] {product};
	}
	

}