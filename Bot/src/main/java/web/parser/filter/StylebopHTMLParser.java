package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class StylebopHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	
		map.put("name", 		new String[]{"<div class=\"product-shop sticky-parent\">"});
		map.put("price", 		new String[]{"<div class=\"price-box\">"});
		map.put("basePrice",    new String[]{"<div class=\"price-box\">"});
		map.put("description", 	new String[]{"<div class=\"desc-content\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-image-gallery\">"});
		map.put("realProductId", new String[]{"<div class=\"customer-care\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";  
		String designName ="";
		if(evaluateResult.length > 0) {
			designName = FilterUtil.getStringBetween(evaluateResult[0], "<h2 class=\"h1\">", "</h2>");
			designName = FilterUtil.getStringBetween(designName, "\">", "</a>");
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h2\">", "</h1>");
			productName = designName + productName;
			
		}
		return new String[] {productName};
	}
			
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		double EUR_TO_THB = 39.5412;
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "<p class=\"old-price\">");
			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\" id=", "</p>");
			productPrice = FilterUtil.getStringBetween(productPrice, "\">", "</span>");
			if(productPrice.equals("")){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"regular-price\" id=", "</div>");
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\"", "/span>");
				productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			productPrice = Double.toString(Math.round(Double.parseDouble(productPrice)*EUR_TO_THB));
			if(productPrice.equals("0")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		double EUR_TO_THB = 39.5412;
		if(evaluateResult.length > 0) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</div>");
			bPrice = FilterUtil.getStringBetween(bPrice, "<span class=\"price\" id=", "/span>");
			bPrice = FilterUtil.getStringBetween(bPrice, ">", "<");
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
			if(StringUtils.isNoneBlank(bPrice)){
				bPrice = Double.toString(Math.round(Double.parseDouble(bPrice)*EUR_TO_THB));
			}
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if(evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"//", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	
	 public String[] getRealProductId(String[] evaluateResult) {
		 String pdId = "";
			if (evaluateResult.length > 0 ) {
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<span>Product code:", "</span>");
	    	}
			return new String[] { pdId};
	 }
	 
	 public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
			
			String[] productId = mpdBean.getRealProductId();
			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productBasePrice = mpdBean.getProductBasePrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			
			if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[0] + " ("+productId[0] + ")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			
			if(StringUtils.isNotBlank(productBasePrice[0])){
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
			}
			if (productDescription != null && productDescription.length != 0) {
				rtn[0].setDescription(productDescription[0]);
			}
			
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				rtn[0].setPictureUrl("https://"+productPictureUrl[0]);
			}
			
			return rtn;
		}
}
