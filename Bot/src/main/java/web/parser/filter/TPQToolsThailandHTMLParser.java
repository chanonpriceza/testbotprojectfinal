package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.FilterUtil;

public class TPQToolsThailandHTMLParser extends TemplateMakeWebEasyHTMLParser{
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});	
		map.put("name", new String[]{""});
	    map.put("description", new String[]{"<div class=\"divProductDescription\">"});		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {		
			Document doc = Jsoup.parse(evaluateResult[0]);
			String name1 = doc.select("h1.productName").html();
			String name2 = doc.select("p.productSku").html();
			name2 = name2.replace("รหัสสินค้า :", "").trim();
			
			//productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h2 productName\">", "</h1>");
			productName = name1+" ("+name2+")";
			
		}
		return new String[] {productName};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "<span class=\"h4\"><strong>รายละเอียดสินค้า</strong></span>", "");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
		
}
