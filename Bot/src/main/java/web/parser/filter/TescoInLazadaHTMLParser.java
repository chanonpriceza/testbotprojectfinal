package web.parser.filter;

import bean.ProductDataBean;

public class TescoInLazadaHTMLParser extends TemplateLazadaJSIncludeIdHTMLParser{

	@Override
	protected ProductDataBean getResult(String sellerName, String productName, String productPrice, String productBasePrice, 
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {

		if(sellerName.contains("Tesco Supermarket") || sellerName.contains("Tesco Lotus")) {
			return gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
		}
		return null;
	} 
}

