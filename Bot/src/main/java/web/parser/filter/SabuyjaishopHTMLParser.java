package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SabuyjaishopHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span id=\"I_PDT_NAME2\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div style=\"width:710px;color:#1f1000;margin-left:auto;margin-right:auto;padding-top:10px;padding-bottom:10px;word-wrap:break-word;\" class=\"text12\">"});
		map.put("pictureUrl", new String[]{"<div class=\"wraptocenter_pdtdetail\">"});
		
		return map;
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"I_PDT_PRICE_SPECIAL\"><font color=ff6f20><b>", "</span>");
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"I_PDT_PRICE\"><b>", "</span>");
    			productPrice = FilterUtil.toPlainTextString(rawPrice);
        		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}