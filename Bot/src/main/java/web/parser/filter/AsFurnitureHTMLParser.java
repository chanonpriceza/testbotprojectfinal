package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

import org.apache.commons.lang3.StringUtils;


public class AsFurnitureHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<span class=\"h1\" itemprop=\"name\">","<div class=\"std\">"});
		map.put("price", 		new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", 	new String[]{"<p class=\"old-price\">"});
		map.put("description", 	new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"product-image\">"});
		map.put("realProductId",new String[]{"<div class=\"std\">"});
		map.put("expire",new String[]{"<p class=\"availability out-of-stock\">"});
		return map;
	}
    
    @Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";			
		String productColor = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(evaluateResult.length > 1) {
				productColor = FilterUtil.getStringBetween(evaluateResult[1],"สี","</span>");
				productColor = productColor.replaceAll("&nbsp;","");
				productColor = FilterUtil.toPlainTextString(productColor);
				if (StringUtils.isNotBlank(productColor)) {
					productColor = "สี"+productColor;
				}
			}
		}		
		return new String[] {productName+" "+productColor};
	}
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";			
		if(evaluateResult.length > 0) {
			if(!evaluateResult[0].contains("<p class=\"special-price\">")) {
				productPrice = FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price\">","</span>"));
				productPrice = "0"+FilterUtil.removeCharNotPrice(productPrice);
			}else {
				productPrice = FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"special-price\">","</p>"));
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
		
		}		
		return new String[] {productPrice};
	}
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";				
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}		
		return new String[] {basePrice};
	}
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDes = "";				
		if(evaluateResult.length > 0) {
			productDes = FilterUtil.getStringAfter(evaluateResult[0], "</h2>", "");
			productDes = FilterUtil.toPlainTextString(productDes);
		}		
		return new String[] {productDes};
	}
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";				
		if(evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
			productPic = FilterUtil.removeHtmlTag(productPic);
		}		
		return new String[] {productPic};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";				
		if(evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0],"รหัสสินค้า","</span>");
			realProductId = FilterUtil.toPlainTextString(realProductId);
			realProductId = realProductId.replaceAll("&nbsp;","");
		}		
		return new String[] {realProductId};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";				
		if(evaluateResult.length > 0) {
			if(evaluateResult[0].contains("สินค้าหมด")) expire = "true";
		}		
		return new String[] {expire};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if(productPrice[0].startsWith("0")) {
			productBasePrice[0] = "";
			productPrice[0] = productPrice[0].substring(1);
		}
		
		rtn[0].setName(productName[0]+(StringUtils.isNotBlank(realProductId[0])?" ("+realProductId[0]+")":""));
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {			
			if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {				
				if(productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {
						
					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
				
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
		}
		
		return rtn;
	}

}
