package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class BananaITHTMLParser extends DefaultHTMLParser{	

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<title>"});
		map.put("price", 		new String[]{"<div class=\"product-info-price\">"});
		map.put("basePrice", 	new String[]{"<div class=\"product-info-price\">"});
		map.put("description", 	new String[]{"<div class=\"product attribute overview\">"});
		map.put("pictureUrl",	new String[]{""});
		map.put("realProductId",new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
		if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"special-price\">","</p>");
			if(productPrice.trim().length() == 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price-label\">Regular Price</span>","<span class=\"save-price\">");
    		bPrice = FilterUtil.getStringBetween(bPrice, "<span class=\"price\">", "</span>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String productRealProductId = FilterUtil.getStringBetween(evaluateResult[0],"<form data-product-sku=\"", "\"");
		return new String[]{productRealProductId};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0] + ((realProductId != null && StringUtils.isNotBlank(realProductId[0]))?" ("+realProductId[0]+")":""));
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setRealProductId(realProductId[0]);
		rtn[0].setDescription(productDescription[0]);
		rtn[0].setPictureUrl(StringEscapeUtils.unescapeJava(productPictureUrl[0]));
		
		return rtn;

	}    
	
	
  
}
