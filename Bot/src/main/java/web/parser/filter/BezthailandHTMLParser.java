package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;

public class BezthailandHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"price-wrapper\">"});
		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-short-description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String product = "";   
		
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"product-title product_title entry-title\">", "</h1>");
			product = "BEZ "+product;
		}
		
		return new String[] {product};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"woocommerce-Price-currencySymbol\">", "</p>");
			product = FilterUtil.getStringBetween(product, "</span>", "</span>");
			product = FilterUtil.removeCharNotPrice(product);	
			if(product.equals("")) {
				product = FilterUtil.getStringAfter(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">", "</ins>");	
				product = FilterUtil.getStringBetween(product, "</span>", "</span>");
				product = FilterUtil.removeCharNotPrice(product);	
			}
			if(product.equals(""))
			product = BotUtil.CONTACT_PRICE_STR;
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"woocommerce-Price-amount amount\">", "</del>");
			product = FilterUtil.getStringBetween(product, "</span>", "</span>");
			product = FilterUtil.removeCharNotPrice(product);				
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:url]");
			product = e.attr("content");
			product = FilterUtil.getStringBetween(product, "/detail/", "/");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<img width=\"600\" height=\"600\" src=\"", "\"");

		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return  new String[] {product};
	}
	

}