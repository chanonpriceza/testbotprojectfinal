package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KinohimitsuHTMLParser extends DefaultHTMLParser {
			
	//private static final double currencyUSDtoTHB = 34.23 ; // Currency USD to THB @20170525 11.40
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1 class=\"product_name\" itemprop=\"name\">"});
		map.put("price", 		new String[]{"<span class=\"current_price\">"});
		map.put("description", 	new String[]{"<li class=\"active\" id=\"tab1\">"});
		map.put("pictureUrl", 	new String[]{"<ul class=\"slides\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = "Kinohimitsu " + productName;
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		double newPrice = FilterUtil.convertPriceStr(productPrice);
			productPrice = Double.toString(newPrice);
			
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length > 0 ) {
    		productDesc = evaluateResult[0].replaceAll("\\<.*?>", " ").replaceAll("\\s+"," ").trim();	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}