package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class ToBeShopHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<strong class=\"text_white\">"});
		map.put("price", new String[]{"<td align=\"left\" valign=\"bottom\" class=\"product_hilight_blue\">"});
		map.put("description", new String[]{"<td align=\"left\" valign=\"middle\" class=\"product_content\">"});
		map.put("pictureUrl", new String[]{"<td width=\"400\" align=\"left\" valign=\"top\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			//productName = FilterUtil.getStringBetween(evaluateResult[0], "<strong>", "</strong>");
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {    		
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	if(productPrice.length() == 0 || productPrice.startsWith("0")){
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "</strong>", "</td>");
			productDesc = FilterUtil.toPlainTextString(productDesc);			
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
