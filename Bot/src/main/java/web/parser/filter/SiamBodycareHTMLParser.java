package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SiamBodycareHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("description", new String[]{"<div class=\"product-specs\">"});
		map.put("pictureUrl", new String[]{"<div style=\"position: relative;\">"});
		map.put("expire", new String[]{"<p class=\"availability in-stock\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]); 		
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "false";
		if(evaluateResult.length > 0 && evaluateResult[0].contains("หมด")){
			expire = "true";
		}
		return new String[] {expire};
	}

	    
}