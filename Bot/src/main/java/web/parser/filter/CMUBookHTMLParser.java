package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CMUBookHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"rcolumn\">"});
		map.put("price", new String[]{"<span style=\"font-weight:bold;color:#ff0000\">"});
		//map.put("description", new String[]{""});
		map.put("description", new String[]{"<div class=\"jwts_tabber\" id=\"jwts_tab\">"});
		//map.put("nameDescription", new String[]{"<div class=\"pprice\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("upc", new String[]{"<div class=\"pprice\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h3>", "</h3>");
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
		return new String[] {productDesc};
	}
	
	/*public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td class='left'>สำนักพิมพ์", "</tr>");
			productDesc = productDesc.replace(":", "").replace("&nbsp;", "");
			productDesc = FilterUtil.toPlainTextString(productDesc).trim();
    	}
		return new String[] {productDesc};
	}*/
	
	@Override
	/*public String[] getProductNameDescription(String[] evaluateResult) {
		String pdNameDescription = "";
		if(evaluateResult.length == 1) {
			pdNameDescription = FilterUtil.getStringBetween(evaluateResult[0], "<b>ผู้เขียน:", "<br />");
			pdNameDescription = FilterUtil.toPlainTextString(pdNameDescription);
    	}
		return new String[] {pdNameDescription};
	}*/
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"").trim();
        }
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String UPC = null;
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<b>ISBN:", "<br />").trim();
    		tmp = FilterUtil.toPlainTextString(tmp);
    		if(StringUtils.isNumeric(tmp)){
    			UPC = tmp;
    		}
        }
    	return new String[] {UPC};
	}
/*	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		//String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productNameDescription = mpdBean.getProductNameDescription();
		String[] ISBNArr = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
				|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])
				|| ISBNArr == null || ISBNArr.length == 0 || StringUtils.isBlank(ISBNArr[0])) {
			return null;
		}
		
		String ISBN = ISBNArr[0];
		if(!StringUtils.isNumeric(ISBN) && ISBN.length() != 13){
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		pdName += " (ISBN:"+ ISBN +")";
		
		rtn[0].setName(pdName);
		rtn[0].setRealProductId(ISBN);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}		
			}
		}
		
		String author = "";
		if (productNameDescription != null && productNameDescription.length != 0) {
			author = productNameDescription[0];
			if(!author.isEmpty()){
				rtn[0].setKeyword(author);
				author = "ผู้แต่ง/ผู้แปล: " + author;
				rtn[0].setDescription(author);
			}
		}
		
		if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
			String publisher = productDescription[0];
			String desc = null;
			if(!publisher.isEmpty()){
				desc = "สำนักพิมพ์: " + publisher;
			}
			if(!author.isEmpty()){
				if(desc != null){
					desc = author + ", " + desc;
				}else{
					desc = author;
				}
			}
			rtn[0].setDescription(desc);
		}

		return rtn;
	}
	  */  
}