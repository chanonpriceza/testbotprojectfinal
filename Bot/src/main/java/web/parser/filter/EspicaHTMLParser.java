package web.parser.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class EspicaHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"wrap-short-desc sys_product_short_desc_val\">"});
		map.put("pictureUrl", new String[]{""});

		
		return map;
	}
    HashMap<String, String[]> colorproductMap = new HashMap<String, String[]>();
    HashMap<String, String[]> sizeproductMap = new HashMap<String, String[]>();
	
	public String[] getProductName(String[] evaluateResult) {
		List<String> productNameList = new ArrayList<String>();
		String band = "";   
		String title = "";   
	
		if(evaluateResult.length > 0) {	
			try {
				String data = evaluateResult[0];
				String cut = FilterUtil.getStringBetween(evaluateResult[0], "<table class=\"\" id=\"product-attribute-specs-table\">", "</table>");   
				title = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"rs sys_product_name_val\">", "</h1>");
				band = FilterUtil.getStringBetween(cut, "แบรนด์</th><td class=\"data\">", "</td>");
				
				
				data = FilterUtil.getStringBetween(data, "var spConfig = new Product.Config(", ");");
				if(StringUtils.isNoneBlank(data)) {
					JSONObject allObj = (JSONObject) new JSONParser().parse(data);
					if(allObj != null && allObj.size() > 0) {
						
						JSONObject objArr = (JSONObject) allObj.get("attributes");
						@SuppressWarnings("unchecked")
						Set<String> objSet = objArr.keySet();
						if(objArr != null && objArr.size() > 0){				
							for(Object obj : objSet){
								JSONObject jObj = (JSONObject) objArr.get(String.valueOf(obj));
								String checklabel = StringUtils.defaultIfBlank(String.valueOf(jObj.get("label")), "");
								
								if(checklabel.equals("Color")) {
									JSONArray jObjArr = (JSONArray) jObj.get("options");
									for (Object ob : jObjArr) {
										JSONObject jlabel = (JSONObject)ob;
										String label = StringUtils.defaultIfBlank(String.valueOf(jlabel.get("label")), "");
										
										String productlist = StringUtils.defaultIfBlank(String.valueOf(jlabel.get("products")), "");
										productlist = productlist.replace("]", "").replace("[", "");
										String[] strArray = productlist.split(",");
										colorproductMap.put(label, strArray);
									}
								}		
								
								if(checklabel.equals("Size")) {
									JSONArray jObjArr = (JSONArray) jObj.get("options");
									for (Object ob : jObjArr) {
										JSONObject jlabel = (JSONObject)ob;
										String label = StringUtils.defaultIfBlank(String.valueOf(jlabel.get("label")), "");
										
										String productlist = StringUtils.defaultIfBlank(String.valueOf(jlabel.get("products")), "");
										productlist = productlist.replace("]", "").replace("[", "");
										String[] strArray = productlist.split(",");
										sizeproductMap.put(label, strArray);
									}
								}
								
							}
						}
						for(Entry<String, String[]> entryColor : colorproductMap.entrySet()){
							String finalcolor = entryColor.getKey();
							String[] colorIdList = entryColor.getValue();
							
							for(Entry<String, String[]> entrySize : sizeproductMap.entrySet()){
								String finalsize = entrySize.getKey();
								String[] sizeIdList = entrySize.getValue();
								
								Boolean x = Collections.disjoint(Arrays.asList(colorIdList), Arrays.asList(sizeIdList));
								if(!x) {
									productNameList.add(band + " " + title + " สี " + finalcolor + " ขนาด " + finalsize );
								}
							}
						}
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	return productNameList.toArray(new String[0]);
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length > 0) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "var simpleProducts", "</p>");
    		if (StringUtils.isNoneBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"\"price\":",",");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if (evaluateResult.length > 0) {	
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "var simpleProducts", "</p>");
			if (StringUtils.isNoneBlank(bPrice)) {
				bPrice = FilterUtil.getStringBetween(evaluateResult[0],"\"oldPrice\":",",");
			}
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}	
		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	HashMap<String, String> imgMap = new HashMap<String, String>();
	public String[] getProductPictureUrl(String[] evaluateResult) {
		List<String> productImageList = new ArrayList<String>();
		if(evaluateResult.length > 0) {	
			try {
				String data = evaluateResult[0];
				
				data = FilterUtil.getStringBetween(data, "var spConfig = new Product.Config(", ");");
				if(StringUtils.isBlank(data)) {
					return null;
				}
				
				JSONObject allObj = (JSONObject) new JSONParser().parse(data);
				JSONObject objSwitcher = (JSONObject) allObj.get("imageSwitcher");
				
				JSONObject objCollection = (JSONObject) objSwitcher.get("collection");
				
				for(Entry<String, String[]> entryColor : colorproductMap.entrySet()){
					String[] colorIdList = entryColor.getValue();
					for (int i = 0; i < colorIdList.length; i++) {
						String id = colorIdList[i].replace("\"", "");
						JSONObject objNumberkey = (JSONObject) objCollection.get(id);
						if(objNumberkey != null && objNumberkey.size() > 0) {
							JSONObject objMain = (JSONObject) objNumberkey.get("main");
							if(objMain != null && objMain.size() > 0) {
								String imgOriginal = StringUtils.defaultIfBlank(String.valueOf(objMain.get("original")), "");
								productImageList.add(imgOriginal);
							}
						}	
					}
				}
				
		
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	
		return productImageList.toArray(new String[0]);
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] basePrice = mpdBean.getProductBasePrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		
		if(name == null || name.length == 0 || price == null || price.length == 0 || basePrice == null) {
			return null;
		}
		
		if( price.length != basePrice.length) {
			return null;
		}
		

		List<ProductDataBean> rtn = new ArrayList<ProductDataBean>();
		if(name.length > 0 ){
			for(int i = 0; i < name.length; i ++) {
				ProductDataBean tmp = new ProductDataBean();
				tmp.setName(name[i]);
				tmp.setPrice(FilterUtil.convertPriceStr(price[0]));
				tmp.setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
				if(desc != null && desc.length > 0) {
					tmp.setDescription(desc[0]);
				}
				if(pictureUrl != null && pictureUrl.length > 0) {
					if(i >= pictureUrl.length) 
						continue;
					else
						tmp.setPictureUrl(pictureUrl[i]);
				}
				
		
				tmp.setUrl(currentUrl);
				tmp.setUrlForUpdate(currentUrl);

				
//				if(Arrays.asList(banName).contains(tmp.getName().trim())) {
//					continue;
//				}
				
				rtn.add(tmp);
			}
		}
		
		
		return rtn.toArray(new ProductDataBean[0]);
	}
	
	
	  
}
