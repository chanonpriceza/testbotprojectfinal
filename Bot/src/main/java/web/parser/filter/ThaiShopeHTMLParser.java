package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ThaiShopeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<hgroup>"});
		map.put("price", new String[]{"<div class=\"prize right\">"});
		map.put("basePrice", new String[]{"<div class=\"prize right\">"});
		map.put("description", new String[]{"<div class=\"discription left\">"});
		map.put("pictureUrl", new String[]{"<div class=\"zoom-small-image\">"});
		map.put("expire", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<strong>", "</strong>");
		}
		productPrice = FilterUtil.toPlainTextString(productPrice);
		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<s>", "</s>");
		}
		basePrice = FilterUtil.toPlainTextString(basePrice);
		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href='", "'");
    		productPictureUrl = "https://www.thaishope.com/" + productPictureUrl;
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			if(!evaluateResult[0].contains("<div class=\"div_img_sold_out_full_size\">")) {
				return new String[] {"false"};
			}
		}
		return new String[] {"true"};
	}

	    
}
