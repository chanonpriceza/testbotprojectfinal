package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TowraiHTMLParser extends DefaultHTMLParser{
			
	public String getCharset() {
		return "UTF-8";
	}
		
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name",		 	new String[]{"<h2 itemprop=\"name\">"});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{"<span class=\"product__price--reg\">"});
		map.put("description", 	new String[]{"<div class=\"product-single__description rte\" itemprop=\"description\">"});
		map.put("pictureUrl", 	new String[]{""});
		map.put("expire", 		new String[]{"<p class=\"availability out-of-stock\">"});
		return map;
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length == 1){
			return new String[]{"true"};
		}
		return new String[]{""};
	}


	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		String data = FilterUtil.getStringBetween(evaluateResult[0],"<meta property=\"og:price:amount\" content=\"","\"");
    	if (evaluateResult.length == 1) {
    		
			productPrice = FilterUtil.toPlainTextString(data);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
		    
}
