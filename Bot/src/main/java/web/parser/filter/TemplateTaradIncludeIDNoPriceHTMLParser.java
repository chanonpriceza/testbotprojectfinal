package web.parser.filter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateTaradIncludeIDNoPriceHTMLParser extends TemplateTaradHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>", "<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>"});
		map.put("price", new String[]{"<div style='padding-bottom: 5px;' class='NewProductDetail'>"});
		map.put("basePrice", new String[]{"<div style='padding-bottom: 5px;' class='NewProductDetail'>"});
		map.put("description", new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});
		map.put("expire", new String[]{""});
		map.put("pictureUrl", new String[]{"<table align='center' border='0' cellspacing='0' cellpadding='0' class='ProductPictureBorder' width='150' height='150'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length >= 2) {
			String tmpId = "";
			for (int i = 0; i < evaluateResult.length; i++) {
				if(evaluateResult[i].contains("<h1>")) {				
					if(!evaluateResult[i].contains("sold.gif")){
						productName = FilterUtil.toPlainTextString(evaluateResult[i]);	
					}else {
						return null;
					}
					break;
				}
			}
			
			for (int i = 0; i < evaluateResult.length; i++) {
				if(evaluateResult[i].contains("รหัสสินค้า:")) {					
					tmpId = FilterUtil.toPlainTextString(evaluateResult[i]);	
					tmpId = FilterUtil.getStringAfter(tmpId, "รหัสสินค้า:", "");
					break;
				}
			}
			
			if(productName.trim().length() != 0 && tmpId.trim().length() != 0) {
				productName = productName + " (" + tmpId.trim() + ")";
    		} 
							
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
 		String basePrice = "";
 		if(evaluateResult.length > 0){
 			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<font color='#999999'>", "</font>");
 			basePrice = FilterUtil.removeCharNotPrice(basePrice);
 		}

 		return new String[]{basePrice};
 	}
	
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();

		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		if(productPrice == null || productPrice.length == 0 || 
				productPrice[0] == null || productPrice[0].trim().length() == 0	) {			
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);
		} else {
			if(productPrice[0].equals("0.00")) {
				rtn[0].setPrice(BotUtil.CONTACT_PRICE);
			} else {
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
			}
		}
		
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		
		
		if (productDescription != null && productDescription.length != 0)
			rtn[0].setDescription(productDescription[0]);
		if (productPictureUrl != null && productPictureUrl.length != 0)
			rtn[0].setPictureUrl(productPictureUrl[0]);

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		return rtn;
	}
	    
}
