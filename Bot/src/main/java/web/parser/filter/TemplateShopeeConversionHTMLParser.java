package web.parser.filter;

import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class TemplateShopeeConversionHTMLParser extends TemplateShopeeRealtimeHTMLParser{
	
	private static Logger logger = LogManager.getRootLogger();

	@Override
	public ProductDataBean[] parse(String url) {
		String shopId = "";
		String itemId = "";

		if(url.contains("/universal-link/product/")) {
			String tmp = FilterUtil.getStringBetween(url, "/universal-link/product/", "?");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
		} else if (url.contains("/universal-link/")){
			String tmp = FilterUtil.getStringBetween(url, "-i.", "?");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
		} else if (url.contains("/product/")) {
			String tmp = FilterUtil.getStringAfter(url, "/product/", "");
			shopId = FilterUtil.getStringBefore(tmp, "/", "");
			itemId = FilterUtil.getStringAfter(tmp, "/", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		} else {
			String tmp = FilterUtil.getStringAfter(url, "-i.", "");
			shopId = FilterUtil.getStringBefore(tmp, ".", "");
			itemId = FilterUtil.getStringAfter(tmp, ".", "");
			itemId = FilterUtil.getStringBefore(itemId, "?", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "&", itemId);
			itemId = FilterUtil.getStringBefore(itemId, "/", itemId);
		}
		
		if(StringUtils.isNotBlank(shopId) && StringUtils.isNotBlank(itemId)) {
			String newUrl = "https://shopee.co.th/api/v2/item/get?itemid=" + itemId + "&shopid=" + shopId;
			return super.parse(newUrl);
		}
				
		return super.parse(url);
	}
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 	new String[]{""});
		map.put("price", 	new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String json = mpdBean.getProductName()[0];
    	String url = mpdBean.getCurrentUrl();
    	
    	try {
    		
    		String itemId = FilterUtil.getStringBetween(url, "?itemid=", "&");
    		String shopId = FilterUtil.getStringAfter(url, "&shopid=", "");

    		if(StringUtils.isAnyBlank(shopId, itemId)) return null;
    		
    		JSONObject obj = (JSONObject) new JSONParser().parse(json);
    		obj = (JSONObject) obj.get("item");
    		if(obj==null) {
    			return null;
    		}

    		String productName = String.valueOf(obj.get("name"));
    		String productDesc = String.valueOf(obj.get("description"));
    		String price = String.valueOf(obj.get("price"));
    		String stock = String.valueOf(obj.get("stock"));
    		String basePrice = StringUtils.defaultString(String.valueOf(obj.get("price_before_discount")), "0");
    		String image = null;
    		
    		JSONArray imageArr = (JSONArray) obj.get("images");
    		if(imageArr != null && imageArr.size() > 0) {
    			for (Object img : imageArr) {
    				image = "https://cf.shopee.co.th/file/" + String.valueOf(img);
    				break;
    			}
    		}
    		
    		productName = StringEscapeUtils.unescapeHtml4(productName).trim();
    		productName = FilterUtil.removeEmoji(productName);
    		productName = FilterUtil.removeNonUnicodeBMP(productName);
    		productName = productName.replaceAll("\\s+", " ");
    		
    		String productUrl = ""
    				+ "https://shopee.co.th/universal-link/product/" + shopId + "/" + itemId
    				+ "?smtt=9"
    				+ "&deep_and_deferred=1"
    				+ "&pid=priceza_int"
    				+ "&c=datafeed"
    				+ "&af_click_lookback=7d"
    				+ "&is_retargeting=true"
    				+ "&af_reengagement_window=7d"
    				+ "&af_installpostback=false"
    				+ "&clickid=(xxxxx)"
    				+ "&utm_source=priceza"
    				+ "&utm_medium=affiliates"
    				+ "&utm_campaign=datafeed"
    				;
    		
    		
    		productDesc = StringUtils.defaultString(FilterUtil.removeEmoji(FilterUtil.toPlainTextString(productDesc)), "").replaceAll("/r/n", " ").replaceAll("//s+", " ");
    		
    		double priceDouble = FilterUtil.convertPriceStr(price) / 100000;
    		double basePriceDouble = FilterUtil.convertPriceStr(basePrice) / 100000;
    		
    		ProductDataBean pdb = new ProductDataBean();
    		pdb.setName(productName.trim() + " (" + itemId.trim() + ")");
    		pdb.setDescription(productDesc);
    		pdb.setRealProductId(itemId.trim());
    		pdb.setPrice(priceDouble);
    		pdb.setBasePrice(basePriceDouble);
    		pdb.setPictureUrl(image);
    		pdb.setUrl(productUrl);
    		pdb.setUrlForUpdate(itemId.trim());
    		pdb.setUpdateDate(new Timestamp(System.currentTimeMillis()));
    		
    		if(BotUtil.stringToInt(stock, 0) == 0) {
    			pdb.setExpire(true);
    		}
    		
    		return new ProductDataBean[] {pdb};
    		
    	} catch(Exception e) {
    		logger.error(e);
    	}
    	return null;
	}
	
}
