package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class SeangpaisanHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class='h1'>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div style='margin:15px 0 15px 0;'>"});
		
		return map;
	}
	@Override
	public String getCharset() {			
		return "windows-874";
	}

		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			if(evaluateResult.length > 0) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
				if(productName.contains("(สินค้าหมด)")){
					return new String[] {};
				}
			}
			return new String[] {productName};
		}
		boolean special = true;
		public String[] getProductPrice(String[] evaluateResult) {
			special = true;
			String productPrice = ""; 
			if(evaluateResult.length > 0) {
				evaluateResult[0] = FilterUtil.getStringBetween(evaluateResult[0], "<form name='frmaddtocart' action=\"action_order_process.php\" method='post' onsubmit='return confirmaddtocart()' enctype='multipart/form-data'>", "</form>");				
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='h3'>ราคาพิเศษ", "</div>");
				if (StringUtils.isBlank(productPrice)) {
					productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ :", "</div>");
					special = false;
				}
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);

				if("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0) {
					productPrice = BotUtil.CONTACT_PRICE_STR;
					special = false;
				}
			}
			return new String[] {productPrice};
		}
		public String[] getProductBasePrice(String[] evaluateResult) {
			
			String bPrice = "";   
			if(evaluateResult.length > 0) {			
				if(!special){
					return new String[] {};
				}
				bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd'>ราคาปกติ :", "</div>");
				bPrice = FilterUtil.toPlainTextString(bPrice);
				bPrice = FilterUtil.removeCharNotPrice(bPrice);

			}
			return new String[] {bPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
			if(evaluateResult.length == 1) {
				productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<span class='hd2'>รายละเอียดทั้งหมด :", "</table>");
				productDesc = FilterUtil.toPlainTextString(productDesc);
				productDesc = productDesc.replace("&nbsp;", "");
				if(productDesc.isEmpty()){
					productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:5px 0 5px 0;'>", "</div>");
				}
				productDesc = FilterUtil.toPlainTextString(productDesc);
				productDesc = StringEscapeUtils.unescapeHtml4(productDesc);

			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href = \"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
}
