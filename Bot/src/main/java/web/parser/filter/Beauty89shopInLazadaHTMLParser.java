package web.parser.filter;

import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.FilterUtil;

public class Beauty89shopInLazadaHTMLParser extends TemplateLazadaJSHTMLParser {

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	if(mpdBean == null || StringUtils.isBlank(mpdBean.getProductName()[0])) return null;
    	
    	String html = mpdBean.getProductName()[0];
    	String currentUrl = FilterUtil.getStringBefore(mpdBean.getCurrentUrl(), "?", mpdBean.getCurrentUrl());

    	String allScriptTxt = FilterUtil.getStringBetween(html, "try{", "} catch(e) {");
    	String allObjectTxt = FilterUtil.getStringBetween(allScriptTxt, "app.run(", ");");
    	
    	if(StringUtils.isBlank(allObjectTxt)) return null;
    	
    	try {
			JSONObject appObj = (JSONObject) new JSONParser().parse(allObjectTxt);
			if(appObj == null) return null;
			
			JSONObject dataObj = (JSONObject) appObj.get("data");
			if(dataObj == null) return null;
			
			JSONObject rootObj = (JSONObject) dataObj.get("root");
			if(rootObj == null) return null;
			
			JSONObject fieldObj = (JSONObject) rootObj.get("fields");
			if(fieldObj == null) return null;
			
			JSONObject skuInfoObj = (JSONObject) fieldObj.get("skuInfos");
			if(skuInfoObj == null) return null;
			
			JSONObject productObj = (JSONObject) fieldObj.get("product");
			if(productObj == null) return null;
			
			JSONObject sku0Obj = (JSONObject) skuInfoObj.get("0");
			if(sku0Obj == null) return null;
			
			JSONObject dataLayerObj = (JSONObject) sku0Obj.get("dataLayer");
			if(dataLayerObj == null) return null;
			
			JSONObject priceObj = (JSONObject) sku0Obj.get("price");
			if(priceObj == null) return null;
			
			String sellerName = String.valueOf((Object) dataLayerObj.get("seller_name"));
			
			String itemId = String.valueOf((Object) sku0Obj.get("itemId"));
			
			String productName = String.valueOf((Object) productObj.get("title"));
			if(StringUtils.isNotBlank(productName)) productName = FilterUtil.toPlainTextString(productName);
			
			productName = productName+" ("+itemId+")";
			
			String productDesc = String.valueOf((Object) productObj.get("highlights"));
			if(StringUtils.isNotBlank(productDesc)) productDesc = FilterUtil.toPlainTextString(productDesc);
			
			String productPrice = String.valueOf((Object) dataLayerObj.get("pdt_price"));
			String productBasePrice = "0";
			JSONObject originalPriceObj = (JSONObject) priceObj.get("originalPrice");
			JSONObject salePriceObj = (JSONObject) priceObj.get("salePrice");
			if(originalPriceObj != null && salePriceObj != null) {
				String originalPrice = String.valueOf((Object) originalPriceObj.get("text"));
				String salePrice = String.valueOf((Object) salePriceObj.get("text"));
				originalPrice = FilterUtil.removeCharNotPrice(originalPrice);
				salePrice = FilterUtil.removeCharNotPrice(salePrice);
				if(!originalPrice.equals(salePrice)) {
					productPrice = salePrice;
					productBasePrice = originalPrice;
				}
			}
			
			String productImage = String.valueOf((Object) sku0Obj.get("image"));
			if(!productImage.startsWith("http")) {
				URL url = new URL(new URL(currentUrl), productImage);
				productImage = url.toString();
				productImage = productImage.replace(".jpg", ".jpg_400x400q80.jpg");
			}
			
			if(sellerName.contains(BaseConfig.CONF_MERCHANT_NAME)) {
				ProductDataBean result = gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId);
				return new ProductDataBean[] {result};
			}
			
//			System.out.println("sellerName       " + sellerName);
//			System.out.println("itemId           " + itemId);
//			System.out.println("productName      " + productName);
//			System.out.println("productPrice     " + productPrice);
//			System.out.println("productBasePrice " + productBasePrice);
//			System.out.println("productDesc      " + productDesc);
//			System.out.println("productImage     " + productImage);
//			System.out.println("url              " + "https://c.lazada.co.th/t/c.PVm?url="+ currentUrl);
//			System.out.println("urlForUpdate     " + currentUrl);
			
		} catch (Exception e) {
			//logger.error(e);
		}
    	
    	return null;
	}
}
