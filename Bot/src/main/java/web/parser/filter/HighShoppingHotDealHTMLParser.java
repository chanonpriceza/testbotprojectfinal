package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class HighShoppingHotDealHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<form name=\"goodsFrm\" id=\"goodsFrm\" method=\"post\">"});
		map.put("price", 			new String[]{"<span class=\"value big bold highlight bprice\">"});
		map.put("basePrice", 		new String[]{"<span class=\"value middle number5 through\">"});
		map.put("description", 		new String[]{"<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" id=\"goods-t2\" style=\"width:900px\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"img-main\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"goods_name\"      value=\"", "\"");
			productName = FilterUtil.toPlainTextString(productName);
			productName += " (Hot Deal)";
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";   
		if(evaluateResult.length > 0) {
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}

}