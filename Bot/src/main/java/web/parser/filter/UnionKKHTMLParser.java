package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class UnionKKHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<title>" });
		map.put("price", new String[] { "<span class=\"price\">" });
		map.put("basePrice", new String[] { "<span class=\"price\" style=\"text-decoration: line-through\">" });
		map.put("description", new String[] { "<div class=\"descriptionContainer left\">" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		if("0".equals(productPrice)) {
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}

		return new String[] {productPrice};
	}
	
	@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"image_src\" href=\"","\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" id=\"content_id\" name=\"content_id\" value=\"","\"");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    	}
    	return new String[] {realProductId};
	}

}
