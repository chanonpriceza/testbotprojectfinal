package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MissBshopHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h2 class=\"product-name\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{"<div class=\"MagicToolboxContainer selectorsBottom minWidth\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "title=\"", "\"");
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"availability", "<form");
			if(productPrice.contains("regular-price")) {
				productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
			} else {
				productPrice = FilterUtil.getStringBetween(productPrice, "<p class=\"special-price\">", "</p>");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
			if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice)) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
			if(!basePrice.equals("")) {
				basePrice = FilterUtil.toPlainTextString(basePrice);
				basePrice = FilterUtil.removeCharNotPrice(basePrice);
			}
		}
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<h4>รายละเอียดโดยย่อ</h4>", "<!--div class=\"col-xs-12 col-md-4 col-lg-3 relate-pro\">");
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			productDesc = FilterUtil.toPlainTextString(productDesc);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
}