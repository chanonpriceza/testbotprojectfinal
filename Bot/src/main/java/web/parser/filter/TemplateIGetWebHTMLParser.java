package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TemplateIGetWebHTMLParser extends DefaultHTMLParser{
		
//	public String getCharset() {
//		return "TIS-620";
//	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title\">", "<h1 class=\"entry-full-title prodtc-detail-title\">"});
		map.put("price", new String[]{"<span class=\"num\" id=\"product_price\">"});
		map.put("description", new String[]{"<div class=\"clear_desc clearfix\">"});
		map.put("pictureUrl", new String[]{"<div  id=\"photoshow\">", "<div id=\"photoshow\">", "<div  id=\"photoshow\" class=\"third\">", "<div id=\"photoshow\" class=\"third\">","<div  id=\"photoshow\" class=\" default\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length >= 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		   		
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[0]);    
    		if(tmp.contains("โทรสอบถาม")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else {
    			tmp = FilterUtil.getStringAfter(tmp, "ลดเหลือ", tmp);    		
        		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		} 
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}