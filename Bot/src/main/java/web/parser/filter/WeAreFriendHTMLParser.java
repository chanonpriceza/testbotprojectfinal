package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WeAreFriendHTMLParser extends DefaultHTMLParser {
	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<h1 class=\"product-title\" itemprop=\"name\">"});
			map.put("price", new String[]{"<ins>"});
			map.put("basePrice", new String[]{"<del>"});
			map.put("description", new String[]{"<div class=\"rte product-description\" itemprop=\"description\">"});
			map.put("pictureUrl", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("expire", new String[]{""});
			return map;
		}
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			if(evaluateResult.length > 0) {
				if(evaluateResult[0].contains("<div class=\"sold-out\">")) {
					return new String[] {"true"};
				}
			}
			return new String[] {"false"};
		}
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";   
					
			if(evaluateResult.length > 0) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
			}
			return new String[] {productName};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productBasePrice = "";
			if(evaluateResult.length > 0) {	
				productBasePrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
	        }
			return new String[] {productBasePrice};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			
			String productPrice = "";    	
			if(evaluateResult.length > 0) {	
	    		productPrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
	        }
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
	    	if(evaluateResult.length > 0) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
	    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
	    	}
	    	
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length > 0) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
	        }
	    	
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String realProductId = "";
			if(evaluateResult.length > 0) {
				realProductId = FilterUtil.getStringBetween(evaluateResult[0],"product_id",",");
				realProductId = FilterUtil.removeCharNotPrice(realProductId);
			}
			return new String[] {realProductId};
		}
		
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

	    	String[] productName = mpdBean.getProductName();
	    	String[] productPrice = mpdBean.getProductPrice();
	    	//String[] productUrl = mpdBean.getProductUrl();
	        String[] productDescription = mpdBean.getProductDescription();
	        String[] productPictureUrl = mpdBean.getProductPictureUrl();
	        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
	        String[] realProductId = mpdBean.getRealProductId();
	        String currentUrl = mpdBean.getCurrentUrl();
	        String[] productBasePrice = mpdBean.getProductBasePrice();
	        String[] productUpc = mpdBean.getProductUpc();

	    	if (isEmpty(productName) || isEmpty(productPrice))
				return null;
	    	
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			String name = "";
			if(realProductId!=null&&StringUtils.isNotBlank(realProductId[0])) {
				name = productName[0]+" ("+realProductId[0]+")";
			}else {
				name = productName[0];
			}
			rtn[0].setName(name);
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (isNotEmpty(productDescription))
				rtn[0].setDescription(productDescription[0]);
			
			if (isNotEmpty(productPictureUrl)) {
				String pictureUrl = productPictureUrl[0];
				if(isNotBlank(pictureUrl)) {				
					if(pictureUrl.startsWith("http")) {
						rtn[0].setPictureUrl(pictureUrl);					
					} else {
						try {
							URL url = new URL(new URL(currentUrl), pictureUrl);
							rtn[0].setPictureUrl(url.toString());
				    	} catch (MalformedURLException e) {}
					}
				}
			}

			if (isNotEmpty(merchantUpdateDate)) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null)
					rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
			
			if (isNotEmpty(realProductId))
				rtn[0].setRealProductId(realProductId[0]);
			
			if (isNotEmpty(productBasePrice))
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
					
			if (isNotEmpty(productUpc))
				rtn[0].setUpc(productUpc[0]);
			
			return rtn;
		}

}
