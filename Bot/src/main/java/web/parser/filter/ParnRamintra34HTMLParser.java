package web.parser.filter;


import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class ParnRamintra34HTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
//		map.put("expire", new String[]{""});
		
		return map;
	}
    
	@Override
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {
			Document doc =  Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-info");
			e = e.select("h1");
			productName = e.html();	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		
		if(evaluateResult.length == 1) {
			Document doc =  Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("ins");
			if(StringUtils.isBlank(e.html())) {
				e = doc.select("p.price");
				e = e.select("span.amount");
			}
			productPrice = e.html();	
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		
		if(evaluateResult.length == 1) {
			Document doc =  Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("del");
			productPrice = e.html();	
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

        }		
		return new String[] {productPrice};
	}
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		Document doc =  Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.product-short-description");
			productDesc = e.html();	
    		productDesc = FilterUtil.toPlainTextString(productDesc);    		
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		Document doc =  Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.woocommerce-product-gallery__image");
			productPictureUrl = e.attr("data-thumb");	
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	
//	@Override
//	public String[] getProductExpire(String[] evaluateResult) {
//
//		String expire = "";
//		if (evaluateResult.length == 1) {
//    		Document doc =  Jsoup.parse(evaluateResult[0]);
//			Elements e = doc.select("p.out-of-stock");
//			expire = e.html();
//			expire = String.valueOf(expire.contains("สินค้าหมดแล้ว"));
//        }
//		return new String[] {expire};
//	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        String tmp = "";
        
        if(productDescription!=null&&productDescription.length>0&&StringUtils.isNotBlank(productDescription[0])) 
        	tmp = " "+productDescription[0];

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+tmp);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
	
	
  
	    
}