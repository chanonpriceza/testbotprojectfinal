package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class FruitShop336HTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\" class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("description", new String[]{"<div class=\"panel entry-content\" id=\"tab-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"images\">"});
		map.put("expire", new String[]{"<p class=\"stock out-of-stock\">"});
		return map;
	}
    
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			if(evaluateResult[0].contains("Out of stock")) {
				return new String[] {"true"};
			}
		}
		return new String[] {""};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {		
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], ";", "</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {    		
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "</p>");    		
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}