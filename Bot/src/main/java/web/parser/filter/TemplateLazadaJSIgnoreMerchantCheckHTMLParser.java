package web.parser.filter;

import bean.ProductDataBean;

public class TemplateLazadaJSIgnoreMerchantCheckHTMLParser extends TemplateLazadaJSIncludeIdHTMLParser{

	@Override
	protected ProductDataBean getResult(String sellerName, String productName, String productPrice, String productBasePrice, 
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {

		return gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
	}
	
}
