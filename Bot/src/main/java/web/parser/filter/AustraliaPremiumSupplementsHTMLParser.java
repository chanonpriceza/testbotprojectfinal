package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

import org.apache.commons.lang3.StringUtils;


public class AustraliaPremiumSupplementsHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"pricetag\">"});
		map.put("basePrice", new String[]{"<div class=\"pricetag\">"});
		map.put("description", new String[]{"<div id=\"rte\" class=\"clearfix\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div id=\"product-gallery\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins itemprop=\"price\"", "/ins>");
    		if (StringUtils.isNotBlank(productPrice)) {
    			productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
    			
    		}else{
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span itemprop=\"price\"", "/span>");
    			productPrice = FilterUtil.getStringBetween(productPrice, ">", "<");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		if(productPictureUrl.startsWith("https://")){
    			productPictureUrl = productPictureUrl.replace("https://", "http://");
    		}
        }
    	return new String[] {productPictureUrl};
	}
	
}
