package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class RayKitchenwareHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-title product_title entry-title\">"});
		map.put("price", new String[]{"<div class=price-wrapper>"});
		map.put("basePrice", new String[]{"<div class=price-wrapper>"});
		map.put("description", new String[]{"<div class=\"panel entry-content active\" id=tab-description>"});
		map.put("pictureUrl", new String[]{"<div class=product-main>"});
		map.put("expire", new String[]{"<p class=\"stock out-of-stock\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {

			productPrice = FilterUtil.getStringAfter(evaluateResult[0], "<ins>", "");
			productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
			if (StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "</span>", "</span>");
			}
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if("0".equals(productPrice) || "0.0".equals(productPrice) || "0.00".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringAfter(evaluateResult[0], "<del>", "");
			productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=", " ");

        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		
		String productExpire = "false";   
		if(evaluateResult.length > 0) {	
			if (evaluateResult[0].contains("สินค้าหมด")) {
				productExpire = "true";
			}
		}
		return new String[] {productExpire};
	}
}