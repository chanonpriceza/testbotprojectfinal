package web.parser.filter;

import product.processor.ProductFilter;
import utils.FilterUtil;

public class SiamTrendShopHTMLParser extends WeLoveShoppingPlatinumIncludeIdHTMLParser{
	
	@Override
	public String[] getProductName(String[] evaluateResult) {
		 String productName = "";
	        
	        if (evaluateResult.length == 1) {        	
	        	String tmp = evaluateResult[0];
	        	if(!tmp.startsWith("<tr")) {
	        		productName = evaluateResult[0];
	        	}        	
	        } else if (evaluateResult.length >= 2) {
	            
	        	productName = evaluateResult[0];
	        	String pId =  evaluateResult[1];
	        	pId = FilterUtil.getStringAfter(pId, "รหัสสินค้า :", "");
	        	if(pId.trim().length() != 0) {
	        		productName = productName + " (" + pId.trim() + ")";
	        	}
	        	
	        }
	        
	        
	        if(productName.contains("หมดแล้วค่ะ")) {
	    		productName = "";
	    	} else if(productName.toLowerCase().contains("out of stock")) {
	    		productName = "";
	    	} else if(productName.toLowerCase().contains("d416")) {
	    		productName = "";
	    	}
	        
	        if(ProductFilter.checkCOVIDInstantDelete(productName))
	        	return null;
	        
	        return new String[] {FilterUtil.toPlainTextString(productName)};
	}
		 
	    
}