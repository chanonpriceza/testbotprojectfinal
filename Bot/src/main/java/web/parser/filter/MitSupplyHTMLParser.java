package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class MitSupplyHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<td class=\"product_name\">" });
		map.put("price", new String[] { "<td class=\"product_price1\">" });
		map.put("description", new String[] { "<td class=\"product_detail\">" });
		map.put("pictureUrl", new String[] { "<div class=\"product_view-showimg\">" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {

		String productName = "";

		if (evaluateResult.length >= 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length == 1) {			
			//String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"product_price1\">", "</td>");
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"product_price\"", "</td>");
			if (tmp.isEmpty()) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			} else {
				tmp = "<span class=\"product_price\"" + tmp;
				if(tmp.indexOf("บาท") > -1){
					tmp = FilterUtil.getStringBefore(tmp, "บาท", BotUtil.CONTACT_PRICE_STR);
				}
				productPrice = FilterUtil.toPlainTextString(tmp); 
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
				if (productPrice.isEmpty() || productPrice.contains("XX")) {
					productPrice = BotUtil.CONTACT_PRICE_STR;
				}
			}
		}
		return new String[] { productPrice };
	}

	public String[] getProductDescription(String[] evaluateResult) {

		String productDesc = "";

		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] { productDesc };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		return new String[] { productPictureUrl };
	}
}
