package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ThianThongHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", new String[]{"<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"em-details-tabs-content\">"});
		map.put("pictureUrl", new String[]{"<div id=\"amasty_gallery\">","<ul class=\"em-moreviews-slider \">"});
		map.put("realProductId", new String[]{""});
		//map.put("expire", new String[]{"<p class=\"availability in-stock\">"});
		
		return map;
	}
	
    @Override
//    public String[] getProductExpire(String[] evaluateResult) {
//    	String exp = "true";
//    	if(evaluateResult.length > 0) {	
//    		if(evaluateResult[0].contains("มีสินค้า")){
//    			exp = "false";
//    		}
//    	}
//    	return new String[] {exp};
//    }
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		if(evaluateResult.length > 0) {

			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
			if (StringUtils.isBlank(productPrice)) {
				productPrice = evaluateResult[0];
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);

			if("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] {productPrice};
	}
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String bPrice = "";   
		if(evaluateResult.length > 0) {
			
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"old-price\">", "</p>");
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);

		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-image=\"", "\"");
    		if(productPictureUrl.isEmpty()){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-magnify-zoom=\"", "\"");
    		}
    		
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pid = "";
		if(evaluateResult.length > 0){
			pid = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"");
		}
		return new String[] {pid};
	}
	
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();

		
		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		if(productName[0].isEmpty()) return null;
		String name = productName[0];
		if(realProductId != null && realProductId.length != 0 ){
			name += " ("+realProductId[0]+")";
		}
		rtn[0].setName(name);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		return rtn;
	}
}
