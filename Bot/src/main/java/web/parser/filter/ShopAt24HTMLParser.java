package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class ShopAt24HTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"price-block\">"});
		map.put("basePrice", new String[]{"<div class=\"price-block\">"});
		map.put("description", new String[]{"<span itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{"<span class=\"art-no\">"});
		
		map.put("expire", new String[]{""});
		
		return map;
	}
    
   
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
	            
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   

    	if (evaluateResult.length >= 1) {	
    			Document doc = Jsoup.parse(evaluateResult[0]);
    			Elements cut = doc.select("span.currentPrice");
    			productPrice = cut.html();
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
    	
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<strike>", "</strike>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(evaluateResult.length >= 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length > 0) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"art-no\">รหัสสินค้า&nbsp;", "</span>");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	   	
	    return new String[]{realProductId}; 
   }

	
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"later-available\">","</div>");
		if(expire.indexOf("สินค้าหมด") > -1 || expire.indexOf("สินค้าจะมีเร็วๆนี้") > -1) {
			return new String[] {"true"};
		}
		return new String[] {""};
	}



	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {			
			if(productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {				
				if(productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {
						
					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		
		if (realProductId != null && realProductId.length != 0){
			rtn[0].setRealProductId(realProductId[0]);
		}
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
				
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
		}
		
//		SHOP CANCEL APP LINK
//		
//		String appUrl = "";
//		if(realProductId != null && realProductId.length > 0){
//			String newUrl = currentUrl.replace(FilterUtil.getStringBetween(currentUrl, "/p/", "/"),"itemName");
//			appUrl = "https://shopat24.onelink.me/FG2f?pid=priceza_int&af_click_lookback=7d&af_dp=shopat24%3A%2F%2Fp%2F"+realProductId[0]+"&af_web_dp="
//					+ java.net.URLEncoder.encode(newUrl) + "&is_retargeting=true";
//		}
//		String new_url = currentUrl + (appUrl.isEmpty()? "" : "||" + appUrl);
//		
//		rtn[0].setUrl(new_url);
//		rtn[0].setUrlForUpdate(currentUrl);

		rtn[0].setUrl(currentUrl);
		rtn[0].setUrlForUpdate(currentUrl);
		
		return rtn;
	}
	   
}