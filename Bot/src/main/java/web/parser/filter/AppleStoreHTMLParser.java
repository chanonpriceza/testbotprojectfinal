package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class AppleStoreHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
//		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String product = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		
		return new String[] {product};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"current_price\">", "</span>");
			product = FilterUtil.removeCharNotPrice(product);	
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String product = "";    	
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span  class=\"c-product-old-price\" style=\"font-size: 20px;margin-right: 15px;\">", "</span>");
			product = FilterUtil.removeCharNotPrice(product);				
		}	
		return new String[] {product};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:url]");
			product = e.attr("content");
			product = FilterUtil.getStringBetween(product, "/detail/", "/");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length > 0) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:image]");
			product = e.attr("content");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:description]");
			product = e.attr("content");
		}
		return  new String[] {product};
	}
	

}