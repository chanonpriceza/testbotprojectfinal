package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BromptHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<div class=\"product-name\">"});
		map.put("price", 		new String[]{"<p class=\"special-price\">"});
		map.put("basePrice", 	new String[]{"<p class=\"old-price\">"});
		map.put("description",	new String[]{"<div class=\"product-specs\">"});
		map.put("pictureUrl",	new String[]{"<div class=\"MagicToolboxContainer selectorsBottom minWidth\">"});
		map.put("expire",		new String[]{"<div class=\"col-xs-12 col-sm-6 col-lg-7 product-shop\">"});	
		
		return map;
	}
    
    @Override
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length > 0) {
    		if(evaluateResult[0].contains("<p class=\"availability out-of-stock\">")) {
    			return new String[]{"true"};
    		}
    	}
    	return new String[]{"false"};    
    }
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
        }
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if(evaluateResult.length > 0) {	
			productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			
		}
		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
        	productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"container-fluid\"", "</div>");
        	productDesc = FilterUtil.getStringAfter(productDesc, ">", "");
			productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	    
}