package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class TssBikeHTMLParser extends BasicHTMLParser {
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:title]");
				productName = FilterUtil.toPlainTextString(e.attr("content"));			
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.price-wrap");
				 e = e.select("span.price-neostyle");
				 Elements s = e.select("s").remove();
				productPrice = FilterUtil.removeCharNotPrice(e.text());			
			}	
			
			if(StringUtils.isBlank(productPrice)) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.price-wrap");
				productPrice = FilterUtil.removeCharNotPrice(e.text());		
			}
			
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {

			String productPrice = "";    	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.price-wrap");
			 e = e.select("span.price-neostyle");
			 e = e.select("s");		
			 productPrice = FilterUtil.removeCharNotPrice(e.text());			
			
			return new String[] {productPrice};
		}
	 
}
