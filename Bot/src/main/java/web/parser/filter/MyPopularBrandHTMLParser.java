package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class MyPopularBrandHTMLParser extends DefaultHTMLParser {
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span id=\"I_PDT_NAME2\">", "<font face=\"Verdana\">"});
		map.put("price", new String[]{"<span id=\"I_PDT_PRICE\">", "<span id=\"I_PDT_PRICE_SPECIAL\">"});
		map.put("description", new String[]{"<div style=\"width:710px;color:#4b4b4b;margin-left:auto;margin-right:auto;padding-top:10px;padding-bottom:10px;word-wrap:break-word;\" class=\"text12\">"});
		map.put("pictureUrl", new String[]{"<div class=\"wraptocenter_pdtdetail\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		} else if(evaluateResult.length == 2) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]) + " (" + FilterUtil.toPlainTextString(evaluateResult[1]) + ")";
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 2) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[1]);
        	productPrice = FilterUtil.removeCharNotPrice(productPrice); 
        	
        	if(productPrice.length() == 0) {
        		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
            	productPrice = FilterUtil.removeCharNotPrice(productPrice);             	
        	}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = FilterUtil.getStringBefore(productDesc, "&gt;&gt;", productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
//	public ProductDataBean[] parse(String url) {
//
//		CookieHandler.setDefault(new CookieManager());		
//		ProductDataBean[] rtn = super.parse(url);
//		CookieHandler.setDefault(null);
//		
//		return rtn;
//	}
    
}