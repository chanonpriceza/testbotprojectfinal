package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class LopburiMotorHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<header>" });
		map.put("price", new String[] {""});
		map.put("pictureUrl", new String[] { "" });

		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";

		if (evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] { productName };
	}

	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringAfterLastIndex(evaluateResult[0],"ราคา","");
			if(StringUtils.isBlank(productPrice)){
				productPrice = FilterUtil.getStringAfterLastIndex(evaluateResult[0],"Price","");
			}
			String dummy = productPrice;
			productPrice = FilterUtil.getStringBefore(dummy,"บาท","");
			if(StringUtils.isBlank(productPrice)){
				productPrice = FilterUtil.getStringBefore(dummy,"฿","");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if ("0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = BotUtil.CONTACT_PRICE_STR ;
			}
		}
		return new String[] { productPrice };
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "srcset=\"", "\"");
			productPictureUrl = productPictureUrl.split(",").length > 0 ? productPictureUrl.split(",")[0] : "";
			int dotPos = productPictureUrl.lastIndexOf(".");
			if (dotPos > -1)
				productPictureUrl = productPictureUrl.substring(0, dotPos + 4);
			productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		}
		return new String[] { productPictureUrl };
	}

}
