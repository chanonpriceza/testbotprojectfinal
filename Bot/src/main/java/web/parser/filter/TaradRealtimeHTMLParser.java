package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TaradRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<title>"});
		map.put("price", 		 new String[]{"<div class=\"box-detail-area\" id=\"purchase\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(evaluateResult != null && evaluateResult.length > 0) {
			price = FilterUtil.toPlainTextString(FilterUtil.getStringBetween(evaluateResult[0], "<ul>", "</ul>"));
			if(price.equals("ราคาไม่ระบุ")){
				price = BotUtil.CONTACT_PRICE_STR;
			}
			price = FilterUtil.removeCharNotPrice(price);
		}
		return new String[] { price };
	}
	
}

