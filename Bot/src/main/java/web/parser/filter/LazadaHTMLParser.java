package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class LazadaHTMLParser  extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		map.put("name", 	new String[]{"<div class=\"pdp-product-title\">"});
		map.put("price", 	new String[]{"<div class=\"pdp-product-price\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String name = "";
		if(ArrayUtils.isNotEmpty(evaluateResult))
			name = FilterUtil.toPlainTextString(evaluateResult[0]);
		return new String[] { name};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(ArrayUtils.isNotEmpty(evaluateResult)) {
			price = FilterUtil.toPlainTextString(evaluateResult[0]);
			price = FilterUtil.removeCharNotPrice(price);
		}
		return new String[] { price};
	}
	
	@Override
	public String checkUrlBeforeParse(String url) {
		url = FilterUtil.getStringAfter(url, "?url=", "");
		return BotUtil.decodeURL(url);
	}
}
