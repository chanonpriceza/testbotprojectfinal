package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class IndexLivingMallHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 			new String[]{"<div class=\"-name fontUp\">"});
		map.put("price", 			new String[]{"<div class=\"col-sm-4 padding-none  text-medium align-self-center\">", "<div class=\"col-sm-4 padding-none text-offer text-medium align-self-center\">"});
		map.put("basePrice", 		new String[]{"<span class=\"txt_pricedetail\">"});
		map.put("description", 		new String[]{"<div class=\"col-sm-6 description-detail\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"col-sm-2 px-2 mx-0 col-lg-3 slider-nav slide\">"});
		map.put("realProductId", 	new String[]{"<div class=\"pdDetail\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		for (String e : evaluateResult) {
    			productPrice = FilterUtil.getStringBefore(e, "<span class=\"txt_pricedetail\">", e);
    			productPrice = FilterUtil.toPlainTextString(productPrice);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    			if(StringUtils.isNotBlank(productPrice)) break;
			}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = FilterUtil.getStringAfter(productDesc, "คุณสมบัติเด่น", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		
		if(evaluateResult.length == 1){
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า :", "</div>");
			realProductId = FilterUtil.toPlainTextString(realProductId);
		}
		
		return new String[] {realProductId};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		String name = productName[0];
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}

		// can remove if realProductId, basePrice and upc are not required
		if (realProductId != null && realProductId.length != 0) {
			if(realProductId[0].length() > 0){
				rtn[0].setRealProductId(realProductId[0]);
				name = name + " (" + realProductId[0] + ")" ;
			}
		}

		rtn[0].setName(name);
		
		return rtn;
	}
	    
}