package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;

public class TaladWirelessHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<td class=\"cont_heading_td\">"});
		map.put("price", new String[]{"<b class=\"productSpecialPrice\">"});
		map.put("description", new String[]{"<div class=\"desc2\">"});
		map.put("pictureUrl", new String[]{"<span class=\"tozoom\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			String tmp = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "&raquo;", "");
			productName = FilterUtil.toPlainTextString("<div>" + tmp + "</div>");			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringAfter(evaluateResult[0], ";", "");
    		productPrice = FilterUtil.removeCharNotPrice(tmp);
        }

    	if(productPrice.isEmpty()){
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
