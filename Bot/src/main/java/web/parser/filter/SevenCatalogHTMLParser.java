package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SevenCatalogHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<span class=\"title\">"});
		map.put("price", new String[]{"<div class=\"price-block\">"});
		map.put("basePrice", new String[]{"<div class=\"price-block\">"});
		map.put("description", new String[]{"<span itemprop=\"description\">","</div>"});
		map.put("pictureUrl", new String[]{"<div class=\"js-pdt pdt-lg\">"});
		map.put("realProductId", new String[]{"<span itemprop=\"sku\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		if(evaluateResult.length > 0){
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta itemprop=\"price\" content=\"", "\"");
			productPrice = StringEscapeUtils.unescapeHtml4(productPrice);
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}

	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length > 0){
			
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<strike>", "</strike>");
			productBasePrice = StringEscapeUtils.unescapeHtml4(productBasePrice);
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}
		return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0){
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = "";
		
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "content=\"", "\"");
        }
        
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
		if(evaluateResult.length > 0){
			realProductId = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
    	return new String[] {realProductId};
	}  
}