package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;

public class MynkeHTMLParser extends ReadyPlanetHTMLParser {
	
	public Map<String, String[]> getConfiguration() {
    	
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	map.put("name", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("price", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("basePrice",new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<p style=\"text-align: center;\">"});
		map.put("description", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		map.put("pictureUrl", new String[]{"<table width='100%' cellpadding='5' cellspacing='0' border='0'>","<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"});
		return map;
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class='price_set'><strike>", "</strike>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}

}
