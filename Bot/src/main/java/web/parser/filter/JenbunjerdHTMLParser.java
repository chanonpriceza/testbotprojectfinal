package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class JenbunjerdHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 			new String[]{"<h1>"});
		map.put("price", 			new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", 		new String[]{"<p class=\"old-price 5\">"});
		map.put("description", 		new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl",		new String[]{"<div class=\"product-image-gallery\">"});
		map.put("realProductId", 	new String[]{"<div class=\"additional-info\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if(StringUtils.isBlank(productPrice)) {
    			productPrice = evaluateResult[0];
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";    	
    	if (evaluateResult.length > 0) {
    		productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 2) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[1], "</h2>", evaluateResult[1]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    		productDesc = removeChar(productDesc);
    		if(productDesc.contains("Conductive Rate")) { //remove omega sign
    			productDesc = FilterUtil.getStringBefore(productDesc, "Conductive Rate", productDesc);
    		}
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if(evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringAfter(evaluateResult[0], "Code:", evaluateResult[0]);
			realProductId = FilterUtil.getStringBetween(realProductId, "<span class=\"sku-number\">", "</span>");
			realProductId = FilterUtil.toPlainTextString(realProductId);
		}
		return new String[] {realProductId};
	}
	
	private String removeChar(String s){
		if (s == null || s.equals("0 0 0 0 0 0")){
			return null;
		}
		char[] charArray = s.toCharArray();
		StringBuilder convertedBufferString = new StringBuilder();
		for (int i = 0; i < charArray.length; i++){
			int eachCharAscii = (int) charArray[i];
			if(eachCharAscii < 32 || eachCharAscii == 127) {
				convertedBufferString.append(" ");
				continue;
			}
			convertedBufferString.append((char) eachCharAscii);
		}
		return convertedBufferString.toString();
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] basePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || realProductId == null || realProductId.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		rtn[0].setName(productName[0] + " (" + realProductId[0] + ")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if(StringUtils.isNoneBlank(basePrice)) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(basePrice[0]));
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(removeChar(productDescription[0]));
		}
	
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}
		
		currentUrl = FilterUtil.getStringBefore(currentUrl, "?", currentUrl);
		rtn[0].setUrl(currentUrl);
		rtn[0].setUrlForUpdate(currentUrl);
		if(StringUtils.isBlank(rtn[0].getDescription()))
			return null;
		

		return rtn;
	}
	
}