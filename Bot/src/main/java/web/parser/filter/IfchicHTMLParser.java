package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class IfchicHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<p class=\"price\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		map.put("expire", new String[]{"<div class=\"product-info\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		String brand = "";   
		
		if(evaluateResult.length > 0) {	
			brand = FilterUtil.getStringBetween(evaluateResult[0], "<span>", "</span>");
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"product_name\">", "</span>");
			productName = brand + " " + productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=product:price:amount]");
			productPrice = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"old_price_display\" class=\"hidden\" data-price=\"", "\">");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);				
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length > 0) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"iconProductId\"></span><p>", "</p>");
			product = product.replace("Item Code :", "");
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";    	
		if(evaluateResult[0].length() > 0) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:image]");
			product = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		return  new String[] {product};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		Document doc  = Jsoup.parse(evaluateResult[0]);
			Elements e  = doc.select("meta[name=description]");
			productDesc = FilterUtil.toPlainTextString(e.attr("content"));
    	}
    	
    	return new String[] {productDesc};
	}

	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String product = "true";
		if(!evaluateResult[0].contains("<div class=\"addToCart\" style=\"display:none;\">")) {	
			product = "false";
		}
		return  new String[] {product};
	}
	
}