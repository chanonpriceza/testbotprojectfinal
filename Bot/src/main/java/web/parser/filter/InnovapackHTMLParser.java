package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class InnovapackHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"product-left\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"four column\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"entry-title has-brand\" itemprop=\"name\">", "</h1>");
			if(StringUtils.isBlank(productName) ){
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"entry-title \" itemprop=\"name\">", "</h1>");
			}
			productName = productName.replaceAll("®", "");
			productName = FilterUtil.toPlainTextString(productName);	
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = BotUtil.CONTACT_PRICE_STR;    	
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "</p>");
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	    
}