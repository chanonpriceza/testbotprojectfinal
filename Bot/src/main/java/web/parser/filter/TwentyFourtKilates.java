package web.parser.filter;


import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TwentyFourtKilates extends DefaultHTMLParser{
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{"<span id=\"our_price_display\" itemprop=\"price\">"});
		map.put("basePrice", 	new String[]{"<div class=\"six columns\">"});
		map.put("description", 	new String[]{"<div id=\"short_description_content\" class=\"rte align_justify\" itemprop=\"description\">"});
		map.put("pictureUrl", 	new String[]{"<span id=\"view_full_size\">"});
		map.put("expire", 			new String[]{"<span class=\"release-text\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 itemprop=\"name\">","</h1>");
			String model ="";
			model = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"editable\" itemprop=\"sku\">","</span>");
			String color = "";
			color =  FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"productleftvkts\">","</h3>");
			if(StringUtils.isNotBlank(model)){
				productName = productName+"("+model+")";
			}
			if(StringUtils.isNotBlank(color)){
				productName = productName+"("+color+")";
			}
			productName = FilterUtil.toPlainTextString(productName);
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span id=\"our_price_display\" itemprop=\"price\">","</span>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	}		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";    	
		if (evaluateResult.length == 1) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"SRP (Baht) :","</li>");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img id=\"bigpic\" itemprop=\"image\" src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String productExpire = "false";
		if(evaluateResult.length == 1){
			if(evaluateResult[0].contains("SOLD OUT")){
				productExpire = "true";
			}
		}
		return new String[] {productExpire};
	}
	
	
}
