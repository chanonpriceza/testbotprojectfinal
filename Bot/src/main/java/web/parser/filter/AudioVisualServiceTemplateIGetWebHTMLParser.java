package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import utils.FilterUtil;

public class AudioVisualServiceTemplateIGetWebHTMLParser extends TemplateIGetWebHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title\">", "<h1 class=\"entry-full-title prodtc-detail-title\">"});
		map.put("price", new String[]{"<span class=\"num\" id=\"product_price\">", "<span class=\"num\" id=\"product_price\" style=\"margin:0; padding:0;\">"});
		map.put("basePrice", new String[]{"<span class=\"num\" id=\"product_price\">", "<span class=\"num\" id=\"product_price\" style=\"margin:0; padding:0;\">"});
		map.put("description", new String[]{"<div class=\"clear_desc clearfix\">"});
		map.put("pictureUrl", new String[]{"<div  id=\"photoshow\">", "<div id=\"photoshow\">", "<div  id=\"photoshow\" class=\"third\">", "<div id=\"photoshow\" class=\"third\">","<div  id=\"photoshow\" class=\" default\">"});
		
		return map;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		   		
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[0]); 
    		
    		if(tmp.contains("สอบถาม")||tmp.contains("กรุณาโทรติดต่อบริษัท")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}else if(StringUtils.isBlank(tmp)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}else {
    			tmp = FilterUtil.getStringAfter(tmp, "ลดเหลือ", tmp);
        		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		}    		
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length == 1 && evaluateResult[0].contains("ลดเหลือ")) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span style=\"text-decoration: line-through\">", "</span>");
			if(StringUtils.isNotBlank(bPrice)){
				bPrice = FilterUtil.toPlainTextString(bPrice);	
				bPrice = FilterUtil.removeCharNotPrice(bPrice);
			}	
        }		
		return new String[] {bPrice};
	}
	    
}