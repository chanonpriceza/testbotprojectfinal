package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class Watch2HomeHTMLParser extends DefaultHTMLParser{
		@Override
		public String getCharset() {
			return "TIS-620";
		}
	   public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{"<span class=\"big1\">"});
			map.put("price", new String[]{"<td width=\"250\" align=\"left\" class=\"text1\">"});
			map.put("basePrice", new String[] {"<td width=\"250\" align=\"left\" class=\"text1\">" });
			map.put("description", new String[]{"<td class=\"text1\">"});
			map.put("pictureUrl", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {

			String productName = "";   
			if(evaluateResult.length > 0) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}

			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length > 0) {
				
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"Special : <font color=\"#FF6600\">","</b>");
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			
			if(evaluateResult.length > 0 && StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<font color=\"#333333\">","</font>");
				productPrice = FilterUtil.removeCharNotPrice(productPrice);	
			}
	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String basePrice = "";    	
			if(evaluateResult.length > 0) {
				basePrice = FilterUtil.getStringBetween(evaluateResult[0],"<font color=\"#333333\">","</font>");
				basePrice = FilterUtil.removeCharNotPrice(basePrice);	
			}
	
			return new String[] {basePrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
			if (evaluateResult.length == 1) {
				productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img   src=","     >");
	    	}
	    	return new String[] {productPictureUrl};
		}
	
 
		
}

