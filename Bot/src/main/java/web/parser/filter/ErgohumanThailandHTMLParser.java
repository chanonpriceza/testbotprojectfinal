package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;

public class ErgohumanThailandHTMLParser extends DefaultTimeOutHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<div class=\"summary entry-summary single-product-info\">"});
    	map.put("price", new String[]{"<div class=\"summary entry-summary single-product-info\">"});
    	map.put("basePrice", new String[]{"<div class=\"summary entry-summary single-product-info\">"});
    	map.put("description", new String[]{"<div class=\"short-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"single-product-image\">"});
		map.put("expire", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";        
		if(evaluateResult.length == 1) {		
			productName = FilterUtil.getStringBefore(evaluateResult[0],"</h1>", "");
			productName = FilterUtil.toPlainTextString(productName);	
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   
		
		if(evaluateResult.length == 1 && evaluateResult[0].contains("<meta itemprop=\"price\"") ) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<meta itemprop=\"price\" content=\"", "\">");
				productPrice = FilterUtil.toPlainTextString(productPrice);		
		    	productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		    	if(productPrice.equals("")){
		    		productPrice = BotUtil.CONTACT_PRICE_STR;
		    	}
		}
    		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if(evaluateResult.length == 1 && evaluateResult[0].contains("<span class=\"old-price\">") ) {	
			bPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"old-price\">", "&#3647;</span>");
			bPrice = FilterUtil.toPlainTextString(bPrice);		
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length == 1){
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"images\">", "</div>");
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
    		}	
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String result = "";
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("div.stock-status");
		if(e.html().toLowerCase().contains("out of stock")) {
			result = "true";
		}
		return new String[] {result};
	}
}