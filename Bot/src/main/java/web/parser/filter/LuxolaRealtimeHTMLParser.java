package web.parser.filter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ProductDataBean;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.DefaultHTMLParser;

public class LuxolaRealtimeHTMLParser extends DefaultHTMLParser{
	
	private static Logger logger = LogManager.getRootLogger();
	
	@Override
	public ProductDataBean[] parse(String url) {

		String slug = FilterUtil.getStringAfter(FilterUtil.getStringBefore(url, "%23", url), "/products/", "");
		String json = request("https://www.sephora.co.th/api/v2.1/products/" + slug);
		if(StringUtils.isNotBlank(json)) {
			try {
				JSONObject jsonObj = (JSONObject) new JSONParser().parse(json);
				if(jsonObj == null) return null;
				
				JSONObject dataObj = (JSONObject) jsonObj.get("data");
				if(dataObj == null) return null;
				
				JSONObject attributeObj = (JSONObject) dataObj.get("attributes");
				if(attributeObj == null) return null;
				
				String id = StringUtils.defaultString(String.valueOf((Object) dataObj.get("id")), "");
				String name = StringUtils.defaultString(String.valueOf((Object) attributeObj.get("name")), "");
				String price = StringUtils.defaultString(String.valueOf((Object) attributeObj.get("price")), "000");
				String basePrice = StringUtils.defaultString(String.valueOf((Object) attributeObj.get("original-price")), "000");
				
				if(StringUtils.isBlank(name) || price.equals("000") || basePrice.equals("000")) return null;
				ProductDataBean pdb = new ProductDataBean();
				pdb.setName(name + " (" + id + ")");
				pdb.setPrice(FilterUtil.convertPriceStr(price.substring(0, price.length()-2)));
				pdb.setBasePrice(FilterUtil.convertPriceStr(basePrice.substring(0, basePrice.length()-2)));
				return new ProductDataBean[] {pdb};
				
			} catch(Exception e) {
				logger.error(e);
			}
		}
		
		return null;
	}
	
	private String request(String url) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(false);			
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("accept", "application/json, text/plain, */*");
			conn.setRequestProperty("accept-encoding", "gzip, deflate, br");
			conn.setRequestProperty("accept-language", "th-TH");
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return String.valueOf(status);
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = new InputStreamReader(gzip ? new GZIPInputStream(is) : is, "UTF-8");
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return rtn.toString();
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				HTTPUtil.consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
}

