package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BabyBestBuyinThailandHTMLParser extends DefaultHTMLParser{
   
	public String getCharset() {
		return "TIS-620";
	}
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<td align=\"left\" valign=\"top\" width=\"22%\">"});
		map.put("basePrice", new String[]{"<td align=\"left\" valign=\"top\" width=\"22%\">"});
		map.put("description", new String[]{"<div id=\"desc_data\" style=\"\">"});
		map.put("pictureUrl", new String[]{"<td align=\"center\" width=\"1%\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String stock = FilterUtil.getStringBetween(evaluateResult[0],"<b><font color=\"blue\">","</font></b>");
    		if(StringUtils.isNotBlank(stock) && stock.contains("มีสินค้าพร้อมส่ง")){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"salesPrice\">","</span>");
    			if(StringUtils.isBlank(productPrice)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"price\">","</span>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);	
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	}
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length == 1) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"priceBlockOld\">ราคา:&nbsp;","</span>");
			bPrice = FilterUtil.toPlainTextString(bPrice);	
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		
        }
    	return new String[] {productPictureUrl};
	}
	

}
