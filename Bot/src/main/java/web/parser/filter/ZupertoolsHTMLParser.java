package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;

public class ZupertoolsHTMLParser extends DefaultHTMLParser{	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
			map.put("name", new String[]{"<h1 itemprop=\"name\">"});
			map.put("price", new String[]{"<div class=\"prices\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">"});
			map.put("basePrice", new String[]{"<div class=\"prices\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">"});
			map.put("description", new String[]{"<div class=\"short-description\">"});
			map.put("pictureUrl", new String[]{""});		
			map.put("realProductId", new String[]{""});
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			if(evaluateResult.length > 0) {	
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length >0) {
	    		
	    		if(evaluateResult[0].contains("<div class=\"product-price call-for-price\">")){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}else{
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span   itemprop=\"price\"", "</span>");
	    		productPrice = FilterUtil.getStringAfter(productPrice, ">", "");
	    		if(productPrice.endsWith("0")){
	    			productPrice = BotUtil.CONTACT_PRICE_STR;
	    		}
	    		}
	    		productPrice = FilterUtil.toPlainTextString(productPrice) ;
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }		
			return new String[] {productPrice};
		}

		public String[] getProductBasePrice(String[] evaluateResult) {
			String productbasePrice = "";    	
	    	if (evaluateResult.length >0) {
	    		productbasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"old-product-price\">", "</div>");
	    		productbasePrice = FilterUtil.getStringBetween(productbasePrice, "<span class=\"oldprice\">", "</span>");
	    		productbasePrice = FilterUtil.toPlainTextString(productbasePrice) ;
	    		productbasePrice = FilterUtil.removeCharNotPrice(productbasePrice);
	        }		
			return new String[] {productbasePrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
	    	if(evaluateResult.length >0) {
	    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
	    	if (evaluateResult.length >0 ) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
	        }
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pdId = "";
			if(evaluateResult.length > 0){
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"product-ribbon-info\" data-productid=\"", "\"");
				pdId =  FilterUtil.toPlainTextString(pdId);
			}
				return new String[] {pdId};
		}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] basprice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || realProductId == null || realProductId.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		String realPId = realProductId[0];
		if(StringUtils.isBlank(realPId)){
			return null;
		}
		String pdName = productName[0];
		rtn[0].setName(pdName);
		rtn[0].setRealProductId(realPId);
		
		if (basprice != null && basprice.length != 0) {
			try{
			rtn[0].setBasePrice( Double.parseDouble(basprice[0]));
			}catch (Exception e) {
				rtn[0].setBasePrice(0.00);
			}
		}
		
		return rtn;
	}
}
