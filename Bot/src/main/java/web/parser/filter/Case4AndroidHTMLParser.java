package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class Case4AndroidHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<h3 class='post-title entry-title'>" });
		map.put("price", new String[] { "" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });		
		map.put("expire", new String[] { "" });		
		return map;
	}
	
	
	   public String[] getProductName(String[] evaluateResult) {

	        String productName = "";
	        if (evaluateResult.length == 1) {
	        	productName = FilterUtil.toPlainTextString(evaluateResult[0]);         	  	
	        }      	
	        
	        return new String[] {productName};
	    }
	    
//	    public String[] getProductPrice(String[] evaluateResult) {
//	    	String productPrice = "";
//	    	if (evaluateResult.length == 1) {
//	    	Document doc = Jsoup.parse(evaluateResult[0]);
//			Element result = doc.select("div#post-body-7508120749880950918.post-body.entry-content").first();
//			if(result==null) {
//				return new String[] {"0"};
//			}
//			productPrice = FilterUtil.toPlainTextString(result.outerHtml());
//	        	String tmp = evaluateResult[0];  
//	        	tmp = FilterUtil.getStringBetween(tmp, "ราคา", "<br />");
//	        	
//	        	if(tmp.indexOf("</strike>") != -1) {
//	        		tmp = FilterUtil.getStringAfter(tmp, "</strike>", tmp);
//	        	}
//	        	tmp = FilterUtil.getStringBefore(tmp, "บาท", "");
//	        	if(tmp != null && tmp.trim().length() != 0) {
//	        		tmp = tmp.trim();
//	        		int index = tmp.lastIndexOf(" ");
//	        		if(index != -1) {
//	        			tmp = tmp.substring(index);
//	        		}
//	        		tmp = FilterUtil.removeCharNotPrice(tmp); 
//	                if(tmp != null && tmp.trim().length() != 0) {
//	                	productPrice = tmp;
//	                }                
//	        	}
//	        }
//	        
//	        return new String[] {productPrice};
//	    }
	    
//	    public String[] getProductDescription(String[] evaluateResult) {
//	    	String productDesc = "";
//	    	if (evaluateResult.length == 1) {
//	    		Document doc = Jsoup.parse(evaluateResult[0]);
//				Element result = doc.select("div#post-body-7508120749880950918.post-body.entry-content").first();
//	    		List<Node> n = result.childNodes();
//	    		if(n.size() >= 3) {
//	    			productDesc = n.get(1).outerHtml();
//	    			productDesc = FilterUtil.toPlainTextString(productDesc);
//	    		}
//	        } 
//	        return new String[] {productDesc};
//	    }
	    
	    public String[] getProductPictureUrl(String[] evaluateResult) {
	    	String productPictureUrl = "";
	        if (evaluateResult.length == 1) {
	        	String html = evaluateResult[0];        	
	        	productPictureUrl = FilterUtil.getStringBetween(html,"height=\"155\" src=\"", "\"");
	        }
	        return new String[] {productPictureUrl};
	    }
//	    @Override
//	    public String[] getProductExpire(String[] evaluateResult) {
//	    	String expire = "";
//	    	if (evaluateResult.length == 1) {
//	    		Document doc = Jsoup.parse(evaluateResult[0]);
//				Element result = doc.select("div#post-body-7508120749880950918.post-body.entry-content").first();
//				if(result==null) {
//					return new String[] {"false"};
//				}
//	    		expire = result.outerHtml();
//	    		 doc = Jsoup.parse(expire);
//				 result = doc.select("span.Apple-style-span").first();
//				 expire = result.toString();
//				 if(expire.contains("สินค้าหมด")) {
//					 expire = "true";
//				 }else {
//					 expire = "false";
//				 }
//	        } 
//	    	return new String[] {expire};
//	    }
	    
}

