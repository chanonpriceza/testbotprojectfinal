package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class IPlawahnHTMLParser extends DefaultHTMLParser {
			
//	public String getCharset() {
//		return "UTF-8";
//	}
	
	//FilterUtil
	/*
	    
		FilterUtil.removeCharNotPrice(String price)
          - to remove char except "0123456789."

		FilterUtil.toPlainTextString(String html);
       
		FilterUtil.getStringAfter(target, begin, defaultValue)
		FilterUtil.getStringAfterLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBefore(target, begin, defaultValue)
		FilterUtil.getStringBeforeLastIndex(target, begin, defaultValue)
		FilterUtil.getStringBetween(target, begin, end)

	*/
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div id=\"content\">"});
		map.put("price", new String[]{"<div class=\"right\">"});
		map.put("description", new String[]{"<div id=\"tab-description\" class=\"tab-content\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-info\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");
			productName = FilterUtil.toPlainTextString(rawName);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price\">", "<br />");
    		rawPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(rawPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}