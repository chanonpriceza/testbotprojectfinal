package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class MeesoKoreaHTMLParser extends DefaultHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<s ref=\"tempprice\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		return map;
	}
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			if(evaluateResult[0].contains("<div class=\"sold-out\">")) {
				return new String[] {"true"};
			}
		}
		return new String[] {"false"};
	}
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";
		if(evaluateResult.length > 0) {	
			productBasePrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
        }
		return new String[] {productBasePrice};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
		if(evaluateResult.length > 0) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"product:price:amount\" content=\"", "\"");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:description\"	content=\"", "\"");
    		productDesc = FilterUtil.toPlainTextString(productDesc);  
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" 		content=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
}
