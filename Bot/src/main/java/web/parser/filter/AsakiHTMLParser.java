package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AsakiHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		return map;
	}
    
    @Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		String id = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:title\" content=\"", "\"");
			id = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h2 productName\">", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);
			productName = productName + " " + id ;
		}		
		return new String[] {productName};
	}
    
    @Override
	public String[] getProductPrice(String[] evaluateResult) {
    	String productPrice = "";
    	if(evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "\"productPrice\":", "\"");
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"price-box\">", " </div>");
    			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span>");
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	}
		return new String[] {productPrice};
	}
    
    @Override
    public String[] getRealProductId(String[] evaluateResult) {
    	String productId = "";
    	if (evaluateResult.length == 1) {
			productId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"").trim();
    	}
    	return new String[]{ productId};
    }
    
    public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
    
    public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = evaluateResult[0].replaceAll("\\<.*?>", " ").replaceAll("\\s+"," ").trim();
    	}
    	return new String[] {productDesc};
	}
	
}