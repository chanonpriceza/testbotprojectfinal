package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class JewelryOfficialStoreHTMLParser extends DefaultHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 id=\"prod_title\">"});
		map.put("price", new String[]{"<span id=\"special_price_box\">"});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-description__block\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<div id=\"prod_brand\">"});
		map.put("realProductId", new String[]{"<div class=\"prod_cta js-cta-block \">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"product_special_price_label\">ราคาปกติ</span>", "<div class=\"prod_saving\">");
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String pdExpire = "false";
    	if (evaluateResult.length == 1) {
    		if (!evaluateResult[0].contains("555jewelry")) {
    			pdExpire = "true";
    		}
        }
    	return new String[] {pdExpire};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(evaluateResult.length == 1){
			pdId = FilterUtil.getStringBetween(evaluateResult[0], "<input id=\"config_id\" type=\"hidden\" name=\"config_id\" value=\"", "\"/>");
		}
		return new String[] {pdId};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] producBasetPrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productId = mpdBean.getRealProductId();
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+" "+ "(" +productId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(producBasetPrice[0]));
			
		rtn[0].setUrl("http://ho.lazada.co.th/SHCOQR?url="+currentUrl+"%3Foffer_id%3D%7Boffer_id%7D%26affiliate_id%3D%7Baffiliate_id%7D%26offer_name%3D%7Boffer_name%7D_%7Boffer_file_id%7D%26affiliate_name%3D%7Baffiliate_name%7D%26transaction_id%3D%7Btransaction_id%7D%26aff_source%3D%7Bsource%7D&aff_sub=&aff_sub2=&aff_sub3=&aff_sub4=&aff_sub5=&source=");

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}

		return rtn;
	}
	

}
