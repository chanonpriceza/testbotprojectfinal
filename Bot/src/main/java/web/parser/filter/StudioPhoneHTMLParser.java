package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class StudioPhoneHTMLParser extends DefaultHTMLParser {
	
	@Override
	public String getCharset() {
		return "windows-874";
	}
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h2.product-title");
			productName = FilterUtil.toPlainTextString(e.html());			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {   		
    		//price-section row
    		Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.price-section");
			e= e.select("div[style=font-family: 'Prompt','Kanit',tahoma;color:#f30; font-size:2.2em; font-weight:400; padding:0 0 5px 0; text-align:right; line-height:30px]");
			productPrice = FilterUtil.getStringBefore(e.html(),"บาท",e.html());
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String desc = "";    	
    	if (evaluateResult.length == 1) {   		
    		Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[name=description]");
    		desc =e.attr("content");
        }		
		return new String[] {desc};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {   		
    		//price-section row
    		Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("div.price-section");
			e= e.select("span[style=color:#888; font-size:0.6em;text-decoration:line-through; line-height:20px]");
			productPrice = FilterUtil.removeCharNotPrice(e.html());
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String image = "";    	
    	if (evaluateResult.length == 1) {   		
    		//price-section row
    		Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("ul#productGallery");
			e= e.select("a");
    		image =e.attr("data-image");
        }		
		return new String[] {image};
	}
	

	    
}