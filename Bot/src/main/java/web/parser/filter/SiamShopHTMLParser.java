package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SiamShopHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<ul id=\"detail-product\" class=\"clr clear\">"});
		map.put("description", new String[]{"<div class=\"detail-data\">"});
		map.put("pictureUrl", new String[]{"<div id=\"l-images\" >"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		
   		
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "ลดเหลือ :", "วันที่อัพเดทสินค้า :");

    		if(tmp.length() == 0) {
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<li class=\"price-sale\">", "</li>");    			
    		}
    		
    		if(tmp.length() == 0) {
    			tmp = FilterUtil.getStringBetween(evaluateResult[0], "<li class=\"price-sale\">", "</li>");    			
    		}
    		
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		if(productPrice.length()==0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[1]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	
}