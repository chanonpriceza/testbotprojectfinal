package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class CondomHTMLParser extends BasicHTMLParser {
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("expire", new String[]{""});
			mixIdName = true;
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = "";   
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-name");
				productName = FilterUtil.toPlainTextString(e.html());			
			}
			
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-shop");
				e = e.select("p.special-price");
				e = e.select("span.price");
				productPrice = FilterUtil.removeCharNotPrice(e.text());			
			}	
			
			if(StringUtils.isBlank(productPrice)) {
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-shop");
				e = e.select("span.regular-price");
				e = e.select("span.price");
				productPrice = FilterUtil.removeCharNotPrice(e.text());			
			}
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-shop");
				e = e.select("p.old-price");
				e = e.select("span.price");
				productPrice = FilterUtil.removeCharNotPrice(e.text());			
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			//input 
			
			String id = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("input[name=product]");
				id = FilterUtil.removeCharNotPrice(e.attr("value"));			
			}	
			return new String[] {id};
		}
		
		public String[] getProductExpire(String[] evaluateResult) {
			//input 
			
			String expire = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div.product-shop");
				e = e.select("p.availability");
				expire = e.text();			
			}	
			return new String[] {String.valueOf(expire.contains("สินค้าหมด"))};
		}
		
		@Override
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String img = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("img#amasty_zoom");
				img = e.attr("src");			
			}	
			return new String[] {img};
		}
		

	 
}
