package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class JIBHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"panel-body detailcss\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{"<div id=\"price_box\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("div.price_inner_2");
    		productPrice = FilterUtil.getStringBefore(e.html(), "</strong>", "");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content='", "\'");
        }
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		if (evaluateResult.length > 0) {
			if (evaluateResult[0].indexOf("สินค้าหมด") > -1) {
				return new String[]{"true"};
			}
			if (evaluateResult[0].indexOf("Out of stock") > -1) {
				return new String[]{"true"};
			}
		}
		return new String[]{"false"};
	}

	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productName[0].isEmpty()
				|| productPrice == null || productPrice.length == 0 || productPrice[0].isEmpty()) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String productId = FilterUtil.getStringBetween(currentUrl, "/readProduct/", "/").trim();
		if(productId.length() > 0){
//			cancel concat id 08-08-2019
//			rtn[0].setName(productName[0]+" ("+productId+")");
			
			rtn[0].setName(productName[0]);
		}else{
			return null;
		}
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		rtn[0].setRealProductId(productId);

		return rtn;
	}
}