package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class RevHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{"<div class=\"f-left\">"});
		map.put("basePrice", new String[]{"<div class=\"f-left\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-img-box\">"});
		map.put("expire", new String[]{"<p class=\"availability in-stock\">"});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = BotUtil.CONTACT_PRICE_STR;    	
    	if (evaluateResult.length > 0) {
        	productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price\">","</span>");
        	productPrice = FilterUtil.removeCharNotPrice(productPrice);
        	
        	if(productPrice.isEmpty()) {
        		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price\"","<p class=\"old-price\">");
        		productPrice = FilterUtil.getStringBetween(productPrice,"\">","</span>");
            	productPrice = FilterUtil.removeCharNotPrice(productPrice);
        	}
	
    		if(productPrice.isEmpty()){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }
		return new String[] {productPrice};
	}
	
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";    	
    	if (evaluateResult.length > 0) {
    		basePrice = FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"old-price\">","</p>");
    		basePrice = FilterUtil.getStringBetween(basePrice,"<span class=\"price\"","/span>");
    		basePrice = FilterUtil.getStringBetween(basePrice,"\">","<");
    		basePrice = FilterUtil.removeCharNotPrice(basePrice);

        }
		return new String[] {basePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"").trim();
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "false";
		if(evaluateResult.length > 0){
			if(evaluateResult[0].contains("หมด"))
			exp = "true";
		}
		return new String[] { exp};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realproductID = null;
    	if (evaluateResult.length == 1) {
    		realproductID = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"/>").trim();
        }
    	return new String[] {realproductID};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0 || productPictureUrl == null || productPictureUrl.length == 0) {
			return null;
		}

		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();

		rtn[0].setName(productName[0] + " (" + realProductId[0] + ")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		rtn[0].setPictureUrl(productPictureUrl[0]);

		return rtn;
	}
	
	
}