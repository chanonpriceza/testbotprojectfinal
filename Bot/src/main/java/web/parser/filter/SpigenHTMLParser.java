package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SpigenHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 			new String[]{"<div style=\"position:relative\">"});
		map.put("price", 			new String[]{"<div style=\"position:relative\">"});
		map.put("description", 		new String[]{"<div class=\"TabbedPanelsContent\">"});
		map.put("pictureUrl", 		new String[]{"<div id=\"product_detail\">"});
		map.put("realProductId",	new String[]{""});
		
		//optional
//		map.put("basePrice", new String[]{""});
//		map.put("upc", new String[]{""});		
//		map.put("url", new String[]{""});		
//		map.put("expire", new String[]{""});		
		
		//tmp data
//		map.put("data", new String[]{""});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.getStringBefore(evaluateResult[0], "ราคา", evaluateResult[0]);
			productName = FilterUtil.toPlainTextString(productName);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", "</h2>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
		if(evaluateResult.length > 0){
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "idcolor=", "&");
		}
		return new String[] {realProductId};
	}
	
	
	
//	public String[] getProductPictureUrl(String[] evaluateResult) {
//		String productPictureUrl = null;
//    	if (evaluateResult.length == 1) {
//    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
//    		String tmp = FilterUtil.getStringBetween(evaluateResult[0],"<div id=\"thumbnail\">","</div>");
//    		if(StringUtils.isNotBlank(tmp)){
//    			List<String> src = FilterUtil.getAllStringBetween(tmp, "src=\"", "\"");
//    			if(src != null && src.size() >= 2){
//    				productPictureUrl = src.get(1);
//    			}
//    		}
//        }
//    	return new String[] {productPictureUrl};
//	}
	
}