package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class DiscChannelHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h3 id=\"page-product-title\" >"});
		map.put("price", new String[]{"<span class=\"price_sale\">"});
		map.put("description", new String[]{"<div class=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-image-wrapper\">"});
		map.put("expire",new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<p>", "<div");
    		productDesc = FilterUtil.toPlainTextString(productDesc); 
    		
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img  id=", ">");
    		if(StringUtils.isNotBlank(productPictureUrl)){
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
    		}
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
    		String isExpireStr = FilterUtil.getStringBetween(evaluateResult[0], "<form action=\"#\" method=\"post\" class=\"variants\" id=\"product-actions\">", "</form>");
        	if(isExpireStr.indexOf("btn-lost.png") != -1) {
        		return new String[]{"true"};
        	}	
    	}    	
        return new String[]{""};
	}
	
}
