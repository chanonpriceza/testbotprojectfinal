package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TPQToolsHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<div class=\"prize right\">"});
		/*map.put("description", new String[]{"<div class=\"divProductDescription\">"});*/
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {		
			Document doc = Jsoup.parse(evaluateResult[0]);
			String name1 = doc.select("h1.productName").html();
			String name2 = doc.select("p.productSku").html();
			name2 = name2.replace("รหัสสินค้า :", "").trim();
			
			//productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"h2 productName\">", "</h1>");
			productName = name1+" ("+name2+")";
			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {BotUtil.CONTACT_PRICE_STR};
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = BotUtil.encodeURL(FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\""));
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}
