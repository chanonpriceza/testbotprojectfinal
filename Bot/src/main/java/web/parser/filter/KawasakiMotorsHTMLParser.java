package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KawasakiMotorsHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
	Map<String, String[]> map = new Hashtable<String, String[]>(); 
	
	map.put("name", new String[]{""});
	map.put("price", new String[]{"<ul class=\"kw-product-color-price-num\">"});
	map.put("pictureUrl", new String[]{"<div class=\"kw-slider\">"});
	
	return map;
}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";    	
		if (evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"kw-product-color-head\">", " </div>");
			String edition = FilterUtil.getStringAfter(productName, "</h1>", "</div>");
			edition = FilterUtil.getStringBetween(edition, "<i>", "</i>");
			productName = FilterUtil.getStringBetween(productName, "<h1>", "</h1>");
			if(productName.equals("")){
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<ul id=\"kw-color-slider-0\">", "</li>");
				productName = FilterUtil.getStringBetween(productName, "alt=\"", ":");
			}
			if(!productName.equals("")){
				productName = "Kawasaki รุ่น  "+productName;
			}
			if(!edition.equals("")){
				productName = productName + " ("+edition+")";
			}
	    }	    	
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<li class=\"kw-color-0 active\">", "</li>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
	    }	    	
		return new String[] {productPrice};
	}
		
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"<img src=\"","\"");
		}
		return new String[] {productPic};
	}
	

}