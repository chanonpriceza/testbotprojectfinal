package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BowmapHTMLParser extends DefaultHTMLParser{

	private static Map<String,String> cat_list = new HashMap<String, String>();
	
	static {
		cat_list.put("7219","CHUOKU");
		cat_list.put("7239","BINACHO");
		cat_list.put("7221","BORCAM");
		cat_list.put("13436","BOWMAP");
		cat_list.put("7220","MACAW");
		cat_list.put("10728","EDEC");
		cat_list.put("10487","BOWSEL");
		cat_list.put("15142","BOSCHTAC");
		cat_list.put("10379","3CC");
		cat_list.put("18131","MITUTOYO");
		cat_list.put("10487","MITUTOYO");
		cat_list.put("18351","BOWSEL");
		cat_list.put("7228","HARTLOT,UMICORE");
		cat_list.put("17626","BISCHUFOUR");
		cat_list.put("0","");
		cat_list.put("14330","COCH-FS");
		cat_list.put("15696","UMICORE");
		cat_list.put("20131","BAHCO");
		cat_list.put("17626","BISCHUFOUR");
		cat_list.put("14330","COCHUN");
	}
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("nameDescription", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		String productBrand = "";
		
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "\"title\":\"", "\"");
			if(productName.length() == 0) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<title>", "</title>");
			}
			productName = StringEscapeUtils.unescapeJava(productName);
			productName = FilterUtil.toPlainTextString(productName);
			productBrand = FilterUtil.getStringBetween(evaluateResult[0], "\"brand_id\":", ",");
			if(cat_list.get(productBrand)==null) {
				//System.out.println(productBrand);
				return null;
			}else {
				productBrand = cat_list.get(productBrand) + " ";
			}
			productBrand = StringEscapeUtils.unescapeJava(productBrand);
			productBrand = FilterUtil.toPlainTextString(productBrand);
			
			
		}
	
		
		return new String[] {productBrand + productName};
	}
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		List<String> names = new ArrayList<String>();    	
    	if (evaluateResult.length == 1) {
			String sku = FilterUtil.getStringBetween(evaluateResult[0], "\"id\":", ",");
			sku = StringEscapeUtils.unescapeJava(sku);
			names.add(sku);
    		
        }		
		return names.toArray(new String[names.size()]);
	}
	public String[] getProductPrice(String[] evaluateResult) {

		List<String> prices = new ArrayList<String>();    	
    	if (evaluateResult.length == 1) {
    		
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "\"variants\":", "\"title\":");
    		String variant =  FilterUtil.getStringBetween(tmp, "{", "}");
    			String p = FilterUtil.getStringBetween(variant, "\"price\":",",");
    			p = FilterUtil.removeCharNotPrice(p);
    			if (p.isEmpty() || p.equals("0")) {
        			p = BotUtil.CONTACT_PRICE_STR;
        		}
    			prices.add(p);
    		
        }		
		return  prices.toArray(new String[prices.size()]);
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		List<String> prices = new ArrayList<String>();    	
		if (evaluateResult.length == 1) {
			
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "\"variants\":", "\"title\":");
			String variants =  FilterUtil.getStringBetween(tmp, "{", "}");
			String p = FilterUtil.getStringBetween(variants, "\"normal_price\":", ",");
			p = FilterUtil.removeCharNotPrice(p);
			prices.add(p);
			
		}		
		return prices.toArray(new String[prices.size()]);
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "\"content\":\"", "\",\"seo_title\":");	
    		productDesc = StringEscapeUtils.unescapeJava(productDesc);
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "\"cover_image\":\"", "\"");
    		productPictureUrl = "https://cache-igetweb-v2.mt108.info/uploads/4323/product/" + productPictureUrl + "_full.jpg";
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] productName = mpdBean.getProductName();
		String[] nameDesc = mpdBean.getProductNameDescription();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productName.length == 0 || productName[0].isEmpty()
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		int size = nameDesc.length;
		if(size != productPrice.length){
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[size];
		for(int i=0;i<size;i++){
			
			rtn[i] = new ProductDataBean();
			String name  = productName[0];
			if(!nameDesc[i].isEmpty() && !nameDesc[i].equals("0")){
				name  = name + " (" + nameDesc[i]+")";
			}
			rtn[i].setName(name);
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));
			if (productDescription != null && productDescription.length != 0) {
				rtn[i].setDescription(productDescription[0]);
			}
			rtn[i].setUrl(FilterUtil.getStringBefore(currentUrl, "/json", currentUrl));
			rtn[i].setUrlForUpdate(currentUrl);
			if(productBasePrice != null && productBasePrice.length == size){
				rtn[i].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[i]));
			}
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null
						&& productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[i].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[i].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {
							
						}
					}
				}
			}
		}
		
		return rtn;
	}
	
	@Override
	public ProductDataBean[] parse(String url) {
		try {
			String checkResult = checkUrlBeforeParse(url);
			if(checkResult != null && checkResult.equals("delete"))
				return processHttpStatus(null);
			
			String targetUrl = url;
	        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
	            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"})) {
	            	targetUrl = BotUtil.encodeURL(url);
	            }
	        }
			
			String[] htmlContent = null;
			htmlContent = httpRequestWithStatus(targetUrl, getCharset(), true);
			
			
			if (htmlContent == null || htmlContent.length != 2)
				return null;
			
			String httpStatus = htmlContent[1];
			if(httpStatus.equals("400"))
				return processHttpStatus(htmlContent[1]);
			
			String html = htmlContent[0];
			if(html == null)
				return processHttpStatus(httpStatus, false);
			
			Map<String, String[]> config = getConfiguration();
			if (config == null)
				return null;
			
			String[] productName = null;
			String[] productPrice = null;
			String[] productUrl = null;
			String[] productDescription = null;
			String[] productPictureUrl = null;
			String[] merchantUpdateDate = null;
			String[] productNameDescription = null;
			String[] productExpire = null;
			String[] realProductId = null;
			String[] productBasePrice = null;
			String[] productUpc = null;
			String[] data = null;
			
			String[] result = evaluate(html, config.get("expire"));
			if (result != null) {
	    		productExpire = getProductExpire(result);                
	    		if(productExpire != null && productExpire.length == 1 && productExpire[0].equals("true"))
	    			return processHttpStatus(httpStatus);
	    	}
			
			result = evaluate(html, config.get("name"));
			if (result != null) {
				productName = getProductName(result);
				productName = FilterUtil.formatHtml(productName);
				productName = FilterUtil.removeSpace(productName);
			}
			
			result = evaluate(html, config.get("price"));
			if (result != null) {
				productPrice = getProductPrice(result);
				productPrice = FilterUtil.removeSpace(productPrice);
			}
			result = evaluate(html, config.get("url"));
			if (result != null) {
				productUrl = getProductUrl(result);
				productUrl = FilterUtil.removeSpace(productUrl);
			}
			result = evaluate(html, config.get("description"));
			if (result != null) {
				productDescription = getProductDescription(result);
				productDescription = FilterUtil.formatHtml(productDescription);
				productDescription = FilterUtil.removeSpace(productDescription);
			}
			result = evaluate(html, config.get("pictureUrl"));
			if (result != null) {
				productPictureUrl = getProductPictureUrl(result);				
				productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
			}
			result = evaluate(html, config.get("merchantUpdateDate"));
			if (result != null) {
				merchantUpdateDate = getProductMerchantUpdateDate(result);
				merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
				merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
			}
			result = evaluate(html, config.get("nameDescription"));
			if (result != null) {
				productNameDescription = getProductNameDescription(result);
				productNameDescription = FilterUtil.removeSpace(productNameDescription);
			}
						
			result = evaluate(html, config.get("realProductId"));
			if (result != null) {
				realProductId = getRealProductId(result);
				realProductId = FilterUtil.removeSpace(realProductId);
			}
			
			result = evaluate(html, config.get("basePrice"));
			if (result != null) {
				productBasePrice = getProductBasePrice(result);
				productBasePrice = FilterUtil.removeSpace(productBasePrice);
			}
			
			result = evaluate(html, config.get("upc"));
			if (result != null) {
				productUpc = getProductUpc(result);
				productUpc = FilterUtil.removeSpace(productUpc);
			}
			
			result = evaluate(html, config.get("data"));
			if (result != null) {
				data = getData(data);
				data = FilterUtil.removeSpace(data);
			}
			
			ProductDataBean[] productDataBean = null;
			FILTER_TYPE filterType = getFilterType();
			MergeProductDataBean mpdBean = new MergeProductDataBean();
			mpdBean.setProductName(productName);
			mpdBean.setProductPrice(productPrice);
			mpdBean.setProductUrl(productUrl);
			mpdBean.setProductDescription(productDescription);
			mpdBean.setProductPictureUrl(productPictureUrl);
			mpdBean.setMerchantUpdateDate(merchantUpdateDate);
			mpdBean.setProductNameDescription(productNameDescription);
			mpdBean.setRealProductId(realProductId);
			mpdBean.setCurrentUrl(url);
			mpdBean.setProductBasePrice(productBasePrice);
			mpdBean.setProductUpc(productUpc);
			mpdBean.setData(data);
			
			if (filterType.equals(FILTER_TYPE.DEFAULT)) {
	        	productDataBean = mergeProductData(mpdBean);   	   	
	        } else if (filterType.equals(FILTER_TYPE.PRODUCTLIST)) {
	        	result = evaluate(html, config.get("allData"));
				if (result != null) {
					productDataBean = getAllProductData(result);
					for (int i = 0; i < productDataBean.length; i++) {
						if(productDataBean[i] == null)
							continue;
						String name = FilterUtil.formatHtml(productDataBean[i].getName());
						name = FilterUtil.removeSpace(name);
						productDataBean[i].setName(name);
						
						String description = FilterUtil.formatHtml(productDataBean[i].getDescription());
						description = FilterUtil.removeSpace(description);
						productDataBean[i].setDescription(description);
						
						productDataBean[i].setPictureUrl(FilterUtil.removeSpace(productDataBean[i].getPictureUrl()));
					}
				}	        	                
	        	productDataBean = mergePriceListProductData(productDataBean, mpdBean);
	        }
			
			if(isEmpty(productDataBean))
				productDataBean = new ProductDataBean[]{ new ProductDataBean()};
			productDataBean[0].setHttpStatus(httpStatus);
			
			return productDataBean;
		} catch (Exception e) {
			
		}
		return null;
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
		ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
		rtn[0].setHttpStatus(httpStatus);
		rtn[0].setExpire(expire);
		return rtn;
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus) {
		return processHttpStatus(httpStatus, true);
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setConnectTimeout(120000);
			conn.setReadTimeout(120000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
		
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
}
