package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class InkSupplyThailandCoLtdHTMLParser extends DefaultHTMLParser{
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		map.put("name", new String[]{"<table border = 0 width = \"98%\" cellspacing = 0 bordercolor =\"#00ffff\"  align=\"center\">"});
		map.put("price", new String[]{"<table border = 0 width = \"98%\" cellspacing = 0 bordercolor =\"#00ffff\"  align=\"center\">"});
		map.put("description", new String[]{"<table border = 0 width = \"98%\" cellspacing = 0 bordercolor =\"#00ffff\"  align=\"center\">"});
		map.put("pictureUrl", new String[]{"<table border = 0 width = \"98%\" cellspacing = 0 bordercolor =\"#00ffff\"  align=\"center\">"});
		return map; 
	}
	@Override
	public String getCharset() {
		// TODO Auto-generated method stub
		return "windows-874";
	}
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult != null && evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<td height=\"10\"><font  face=\"AngsanaUPC\" color = \"#FF0000\">", "</font></td>");
			if(productName.trim().isEmpty()) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<td height=\"10\"><font face = \"MS SANS SERIF\" style = \"font-size :12pt;\"color = \"#FF0000\">", "</b></font></td>");
			}
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult != null && evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<td height=\"10\"><font face = \"MS SANS SERIF\" style = \"font-size :8pt;\"color = \"#FF0000\"", "</font></td>");
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(".00".equals(productPrice.trim()))
    			productPrice = "9999999";
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td height=\"10\"><font face = \"MS SANS SERIF\" style = \"font-size :10pt;\"color = #000000\">", "<strong>");
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		if (evaluateResult != null && evaluateResult.length == 1) {
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src = \"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}
