package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class DKTODAYHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<div class=\"col-xs-12 col-sm-6  product-shop-l\">"});
		map.put("basePrice"		, new String[]{"<div class=\"col-xs-12 col-sm-6  product-shop-l\">"});
		map.put("description", new String[]{"<div class=\"tab-content\">"});
		map.put("nameDescription", new String[]{"<div class=\"tab-content\">"});
		map.put("pictureUrl", new String[]{"<div class=\"amlabel-div\">"});
		map.put("upc", new String[]{"<div class=\"tab-content\">"});
		
		return map;
	}
	
public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {    
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "สถานะของสินค้า : <span>", "</span></p>");
    		if("สินค้าพร้อมส่ง".contains(tmp)){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price-label\">Special Price</span>","</span>");
    			productPrice = FilterUtil.getStringBefore(productPrice,"บาท","\">");
    			productPrice = FilterUtil.toPlainTextString(productPrice);
        		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
    		
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length == 1){
			productBasePrice = FilterUtil.getStringAfter(evaluateResult[0],"<span class=\"price-label\">ราคาปกติ:</span>","</span>");
			productBasePrice = FilterUtil.getStringBefore(productBasePrice,"บาท","\">");
			productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}	
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDescription = "";
		if(evaluateResult.length == 1) {
			String Desc1 = FilterUtil.getStringBetween(evaluateResult[0], "สำนักพิมพ์", "<br />");
			String Desc3 = FilterUtil.getStringAfter(evaluateResult[0], "สำนักพิมพ์</td>", "</td>");
			
			if(Desc1 != null & Desc1.length() != 0){
				productDescription = FilterUtil.toPlainTextString(Desc1.replace(":", "").replace(")", ""));
			}else if(Desc3 != null & Desc3.length() != 0){
				productDescription = FilterUtil.toPlainTextString(Desc3);
			}else{
			}
			productDescription = FilterUtil.toPlainTextString(productDescription);
			productDescription = productDescription.replace(":", "").replace("/", "").replace("    ", "").replace("              ", "").trim();
			
			
    	}
		return new String[] {productDescription};
	}
	
	@Override
	public String[] getProductNameDescription(String[] evaluateResult) {
		String pdNameDescription = "";
		if(evaluateResult.length == 1) {
			String Desc1 = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"label\">ผู้แต่ง</td>", "<td class=\"label\">ผู้แปล</td>");
			String Desc2 = FilterUtil.getStringBetween(evaluateResult[0], "ผู้แต่ง", "<br />");
			if(Desc1 != null & Desc1.length() != 0){
				pdNameDescription = FilterUtil.toPlainTextString(Desc1);
			}else if (Desc2 != null & Desc2.length() != 0){
				pdNameDescription = FilterUtil.toPlainTextString(Desc2.replace(")", "").replaceAll("&nbsp;", ""));
			}else{
				
			}
			
			pdNameDescription = FilterUtil.toPlainTextString(pdNameDescription.trim().replace(":", ""));
			pdNameDescription = pdNameDescription.replaceAll("        ", "").replace("                  ", "").replace("    ", "").replace("                  ", "").replaceAll("      ", "").trim();
			
    	}
		return new String[] {pdNameDescription};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {

		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img id=\"image\" src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	public String[] getProductUpc(String[] evaluateResult) {
	   	String UPC = "";
	   	
	   	if(evaluateResult.length == 1) {
	   		String Desc1 = FilterUtil.getStringBetween(evaluateResult[0], "ISBN-13: <span> </span>", "<td class=\"label\">ผู้แต่ง</td>");
			String Desc3 = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"label\">ISBN-13</td>", "</td>");
			
			if(Desc1 != null && Desc1.length() != 0){
				UPC = FilterUtil.toPlainTextString(Desc1);
			}else if(Desc3 != null & Desc3.length() != 0){
				UPC = FilterUtil.toPlainTextString(Desc3);
			}else{
			}
			UPC = FilterUtil.toPlainTextString(UPC).replace(" ", "").trim();
	   		if(!NumberUtils.isCreatable(UPC) || UPC.length() != 13){
	   			UPC = "";
	   		}
	   	}
	    return new String[]{UPC}; 
   }
	
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();	
		String[] UPC = mpdBean.getProductUpc();
		String[] productNameDescription = mpdBean.getProductNameDescription();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productName[0])
				|| productPrice == null || productPrice.length == 0 || StringUtils.isBlank(productPrice[0])
				|| UPC == null || UPC.length == 0 || StringUtils.isBlank(UPC[0])) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String pdName = productName[0];
		rtn[0].setName(pdName);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		rtn[0].setUpc(UPC[0]);
		
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}
		
		String author = "";
		if (productNameDescription != null && productNameDescription.length != 0) {
			author = productNameDescription[0];
			if(!author.isEmpty()){
				rtn[0].setKeyword(author);
				author = "ผู้แต่ง/ผู้แปล: " + author;
			}
		}
		
		if ((productDescription != null && productDescription.length != 0) || !author.isEmpty()) {
			String publisher = productDescription[0];
			String desc = null;
			if(!publisher.isEmpty()){
				desc = "สำนักพิมพ์:" + publisher;
			}
			if(!author.isEmpty()){
				if(desc != null){
					desc = author + ", " + desc;
				}else{
					desc = author;
				}
			}
			if(desc.contains("ปีพิมพ์")){
				desc = FilterUtil.getStringBefore(desc, "ปีพิมพ์", "");
			}
			rtn[0].setDescription(desc);
		}

		return rtn;
	}
}