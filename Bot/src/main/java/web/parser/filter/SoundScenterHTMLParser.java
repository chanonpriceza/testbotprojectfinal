package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class SoundScenterHTMLParser extends DefaultHTMLParser {	
	
	public String getCharset() {
		return "TIS-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class='h1'>"});
		map.put("price", new String[]{"<span class='h3'>", "<span class='price_set'>"});
		map.put("description", new String[]{"<div style='margin:5 0 5 0;'>"});
		map.put("pictureUrl", new String[]{"<div style = \"clear:both;padding:5px\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
   
    		if (evaluateResult.length > 2) {
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[2]);
    		} else if(evaluateResult.length > 1) {
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[1]);
    		}
    		
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if("0".equals(productPrice) || "0.0".equals(productPrice) || productPrice.trim().length() == 0) {
				productPrice = "9999999";
			}
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href = \"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
}