package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class LionShopOnlineHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		 
    	map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"product-shop col-lg-6 col-md-6 col-sm-6 col-xs-12\">","<div class=\"price-box\">"});
		map.put("description", new String[]{"<div class=\"std\">"});
		map.put("pictureUrl", new String[]{"<p class=\"product-image\">"});
		map.put("realProductId", new String[]{"<div class=\"product-shop col-lg-6 col-md-6 col-sm-6 col-xs-12\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";  
		if(evaluateResult.length != 0){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);		
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		if(evaluateResult.length > 0){
			String cutproductPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"product-shop col-lg-6 col-md-6 col-sm-6 col-xs-12\">","<div class=\"product-collateral\">");
			productPrice = FilterUtil.getStringBetween(cutproductPrice,"<p class=\"special-price\">","</p>");
			productPrice = FilterUtil.getStringBetween(productPrice,"฿","</span>");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(cutproductPrice,"<span class=\"regular-price\"","/span>");
				productPrice = FilterUtil.getStringBetween(productPrice,"฿","<");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(cutproductPrice,"<span class=\"price\">","</span>");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(cutproductPrice,"฿","</span>");
			}
		
			productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length != 0){
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
		if(evaluateResult.length != 0){	
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
    		productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";   
		if(evaluateResult.length > 0){
			basePrice =  FilterUtil.getStringBetween(evaluateResult[0],"<p class=\"old-price\">","</p>");
			basePrice = FilterUtil.getStringBetween(basePrice,"฿","</span>");
		}
		return new String[] {basePrice};
	}
	
	
	

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";   
		if(evaluateResult.length > 0){
			realProductId =  FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"price\" id=\"amlabel-product-price-","\"");
			if(realProductId.equals("")) {
				realProductId =  FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"price\" id=\"old-price-","\"");	
			}
		}
		return new String[] {realProductId};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] baseprice = mpdBean.getProductBasePrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		if(name == null || name.length == 0 || price == null || price.length == 0) {
			return null;
		}
		if(StringUtils.isBlank(baseprice[0])){
			baseprice[0] = price[0];
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		
		
		rtn[0].setName(name[0] + "  ("+realProductId[0]+")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(price[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(baseprice[0]));
		if(desc != null && desc.length > 0) {
			rtn[0].setDescription(desc[0]);
		}
		rtn[0].setPictureUrl(pictureUrl[0]);
		rtn[0].setRealProductId(realProductId[0]);
		
		return rtn;
	}
	
}