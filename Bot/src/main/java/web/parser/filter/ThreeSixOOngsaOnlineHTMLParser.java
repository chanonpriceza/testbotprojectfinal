package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class ThreeSixOOngsaOnlineHTMLParser extends DefaultHTMLParser{
		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<span id=\"our_price_display\">"});
		map.put("basePrice", new String[]{"<span id=\"our_price_display\">"});
		map.put("description", new String[]{"<p class=\"product_condition\">"});
		map.put("pictureUrl", new String[]{"<div id=\"img-1\" class=\"zoomWrapper single-zoom\">"});
		map.put("expire", new String[] {"<div class=\"stock_button sold-out\">"});
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length > 0) {
    		return new String[] {"true"};
    	}
    	return new String[] {"false"};    
    }
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		if(evaluateResult.length > 1) {
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[1]);
    		} else {
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		}
    		if (productPrice.contains("ติดต่อสอบถามราคา")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if(evaluateResult.length > 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);	    			
		}
		return new String[] {bPrice};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
//	@Override
//    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
//
//		String[] productName = mpdBean.getProductName();
//		String[] productPrice = mpdBean.getProductPrice();
//		String[] productPictureUrl = mpdBean.getProductPictureUrl();
//		String[] productDesc = mpdBean.getProductDescription();
//		String currentUrl = mpdBean.getCurrentUrl();
//		String[] bPrice = mpdBean.getProductBasePrice();
//		
//		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
//			return null;
//		}
//		
//		ProductDataBean[] rtn = new ProductDataBean[1];
//		rtn[0] = new ProductDataBean();
//		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
//		rtn[0].setName(productName[0]);
//		
//		if(bPrice != null && bPrice.length == 1 && !bPrice[0].equals(productPrice[0])){
//			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
//		}
//		if(productDesc != null && productDesc.length > 0 && StringUtils.isNoneBlank(productDesc[0])) {
//			rtn[0].setDescription(productDesc[0]);
//		}
//		if (productPictureUrl != null && productPictureUrl.length != 0) {
//			if (StringUtils.isNotBlank(productPictureUrl[0])) {
//				if (productPictureUrl[0].startsWith("http")) {
//					rtn[0].setPictureUrl(productPictureUrl[0]);
//				} else {
//					try {
//						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
//						rtn[0].setPictureUrl(url.toString());
//					} catch (MalformedURLException e) {}
//				}
//			}
//		}
//		
//		return rtn;
//	}
	
	    
}
