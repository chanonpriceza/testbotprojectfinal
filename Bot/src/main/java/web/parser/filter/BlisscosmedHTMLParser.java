package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class BlisscosmedHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\">","</h1>"});
		map.put("price", new String[]{"<span  itemprop=\"price\"  >","</span>"});
		map.put("description", new String[]{"<div class=\"short-description\">","</div>"});
		map.put("pictureUrl", new String[]{"<div class=\"gallery\">","</div>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
	            
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   

		if(evaluateResult.length > 0) {			
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(evaluateResult.length > 0){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
		
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
		
    	return new String[] {productPictureUrl};
	}
}