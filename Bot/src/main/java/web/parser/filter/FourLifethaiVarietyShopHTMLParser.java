package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class FourLifethaiVarietyShopHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"mid\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<span id=\"productDetail\" style=\"font-weight: normal; color: #ff4900;\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<span id=\"productName\">", "</span>");
			if(rawName.trim().length() == 0){
				rawName = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"promotionName\">", "</div>");
			}
			productName = FilterUtil.toPlainTextString(rawName);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div id=\"pricePanel\">", "</span>");
    		if(rawPrice.trim().length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "<table class=\"proPriceText\">", "</td>");
    		}
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a dojoType=\"dojox.image.Lightbox\" class=\"show\" href=\"", "\"");
    		if(StringUtils.isBlank(productPictureUrl)){
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product_img_big\">", "</div");
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
    		}
        }
    	return new String[] {productPictureUrl};
	}
	
	

	    
}