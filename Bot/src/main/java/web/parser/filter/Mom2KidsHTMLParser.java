package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class Mom2KidsHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	map.put("name", new String[]{"<span itemprop=\"name\">"});
		map.put("price", new String[]{"<span id=\"id_price_net\" itemprop=\"price\">"});
		map.put("description", new String[]{"<div class=\"pdescription\">"});
		map.put("pictureUrl", new String[]{"<div class=\"pimagetop\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0){
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
		 
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";   
		
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 0) {
      		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียดสินค้า", evaluateResult[0]);
      		productDesc = FilterUtil.toPlainTextString(productDesc);
      	}
    	return new String[] {productDesc};
	}
	
public String[] getProductPictureUrl(String[] evaluateResult) {
  		
  		String productPictureUrl = null;
      	if (evaluateResult.length > 0) {
      		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img itemprop=\"image\" id=\"img_show_3\" src=\"", "\" data-zoom");
          }
      	return new String[] {productPictureUrl};
  	}
	
		
	
}