package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class BurapavalueHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"entry-title\">"});
		map.put("price", new String[]{""});
//		map.put("basePrice", new String[]{"<span id=\"old_price_display\">"});
		map.put("description", new String[]{"<div class=\"entry-content\">"});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = productName.replace("&#8243;", "");
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h2><em>", "</em></h2>");
			productPrice = FilterUtil.getStringAfter(productPrice, "ราคา", "");
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<em>คา", "บาท");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h2><strong>", "</strong></h2>");
			}
			if(productPrice.equals("")) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<h2>ราคา", "บาท</h2>");
			}
			if(StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", "บาท");
			}
			if(StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคา", ".-");
			}
			if(StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p><strong><em>", "</em></strong></p>");
			}
			
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
			if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice)) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
		if (evaluateResult.length == 1) {			
			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
		}	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);   
			productDesc = productDesc.replace("&#8243;", "");
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e	= doc.select("figure.wp-block-image");
    		e = e.select("img");
    		productPictureUrl = e.attr("src");

        }
    	return new String[] {productPictureUrl};
	}

}