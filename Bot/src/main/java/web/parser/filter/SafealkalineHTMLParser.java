package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SafealkalineHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", 		new String[]{"<div class=\"price-box\">"});
		map.put("basePrice", 	new String[]{"<p class=\"old-price\">"});
		map.put("description", 	new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", 	new String[]{"<div class=\"highslide-gallery\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {"SAFE เซฟ "+productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String special = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"special-price\">", "</p>");
    		if(StringUtils.isNotBlank(special)){
    			productPrice = special;
    		}else{
    			String normal = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    			if(StringUtils.isNotBlank(normal)){
    				if(normal.indexOf("Call for price")>-1 || normal.indexOf("ติดต่อ")>-1){
    					productPrice = BotUtil.CONTACT_PRICE_STR;
    				}else{
    					productPrice = normal;
    				}
        		}
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";    	
    	if(evaluateResult.length == 1) {
    		productBasePrice = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
    	}
    	return new String[] {productBasePrice};
	}

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	    
}