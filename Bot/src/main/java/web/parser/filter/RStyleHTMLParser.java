package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class RStyleHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("description", new String[]{"<div class=\"tab-content-inner\">"});
		map.put("pictureUrl", new String[]{"<div class=\"woocommerce-product-gallery__wrapper product-images-slider main-images images-popups-gallery \">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if (evaluateResult.length == 1) { 
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = StringEscapeUtils.unescapeHtml4(productName);
    
        } 
		
		return new String[] {productName};
	}
		public String[] getProductPrice(String[] evaluateResult) {
                   
			String productPrice = "";   
			 if (evaluateResult.length > 0) { 
		    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p class=\"price\">", "</p>");
		    		productPrice = FilterUtil.getStringBetween(productPrice, "amount\">", "<span");
		    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 	
		        } 
			return new String[] {productPrice};
		}
	

	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียดสินค้า", evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc); 	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href=\"", "\" itemprop=\"image\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
