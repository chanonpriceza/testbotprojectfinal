package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import bean.MergeProductDataBean;
import bean.ParserConfigBean;
import bean.ProductDataBean;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTMLParserUtil;
import utils.HTTPUtil;
import web.parser.HTMLParser;

public class SheetandbookHTMLParser implements HTMLParser {
	
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

	private static String cookies = null; 
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{"<span class=\"productPrice\">"});
		map.put("basePrice", new String[]{"<span class=\"product-Old-Price\" style=\"color:red;text-decoration:line-through;\">"});
		map.put("description", new String[]{"<td rowspan=\"1\" colspan=\"2\">"});
		map.put("pictureUrl", new String[]{"<td width=\"33%\" rowspan=\"2\" valign=\"top\">"});
		map.put("expire", new String[]{"<div class=\"vmCartContainer\">"});
		return map;
	}
	
	
	
	private final String HTTP_NOT_FOUND = String.valueOf(HttpURLConnection.HTTP_BAD_REQUEST); 
	

	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   	
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductUrl(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	for(String desc: evaluateResult) {
    		productDesc += FilterUtil.toPlainTextString(desc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	
	public String[] getProductMerchantUpdateDate(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public String[] getProductNameDescription(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		
		String result = "false";
		if(evaluateResult.length >0) {
			if(evaluateResult[0].contains("สินค้าหมด")) {
				return new String[] {"true"};
			}
		}
		return new String[] {result};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if (evaluateResult.length > 0) {
    		basePrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		return new String[] {basePrice};
	}
	
	public String[] getProductUpc(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public String[] getData(String[] evaluateResult) {
		return evaluateResult;
	}
	
	public void setParserConfig(List<ParserConfigBean> configList) {
		return ;
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		return rtn;
	}
	
	public ProductDataBean[] mergePriceListProductData(ProductDataBean[] productDataBean, MergeProductDataBean mpdBean) {
		return productDataBean;
	}
	
	public Date convertMerchantUpdateDate(String merchantUpdateDate) {
		if(isBlank(merchantUpdateDate))
			return null;
		Date d = FilterUtil.parseDateString(merchantUpdateDate, getDateFormat());						
		return FilterUtil.convertThaiToEngDate(d);
	}
	
	public String getDateFormat() {
		return BaseConfig.FORMAT_DATE;
	}
	
	public String getCharset() {
		return BaseConfig.CONF_PARSER_CHARSET;
	}
	
	public String[] evaluate(String html, String pattern) {
		if (pattern == null)
			return null;
		return HTMLParserUtil.getTagList(html, pattern);
	}
	
	public String[] evaluate(String html, String[] pattern) {
		if (pattern == null)
			return null;
		
		if(pattern.length == 1 && pattern[0].length() == 0)
			return new String[]{html};
		
		List<String> rtn = new ArrayList<String>();		
		for (int i = 0; i < pattern.length; i++) {
			String[] result = HTMLParserUtil.getTagList(html, pattern[i]);
			if(result != null) {
				for (int j = 0; j < result.length; j++)
					rtn.add(result[j]);
			}
		}
		return (String[])rtn.toArray(new String[0]);
	}

	@Override
	public ProductDataBean[] parse(String url) {
		try {
			String checkResult = checkUrlBeforeParse(url);
			if(checkResult != null && checkResult.equals("delete"))
				return processHttpStatus(null);
			
			String targetUrl = url;
	        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
	            if(!FilterUtil.checkContainOnlyCharacter(url, new String[] {"eng"})) {
	            	targetUrl = BotUtil.encodeURL(url);
	            }
	        }
			
			String[] htmlContent = null;
			if(BaseConfig.CONF_DISBLE_PARSER_COOKIES || HTTPUtil.USE_PROXY) {
				htmlContent = HTTPUtil.httpRequestWithStatusIgnoreCookies(targetUrl, getCharset(), true);
			}else {
				htmlContent = onlyMehttp(targetUrl, getCharset(), true);
			}
			
			if (htmlContent == null || htmlContent.length != 2)
				return null;
			
			String httpStatus = htmlContent[1];
			if(httpStatus.equals(HTTP_NOT_FOUND))
				return processHttpStatus(htmlContent[1]);
			
			String html = htmlContent[0];
			if(html == null)
				return processHttpStatus(httpStatus, false);
			
			Map<String, String[]> config = getConfiguration();
			if (config == null)
				return null;
			
			String[] productName = null;
			String[] productPrice = null;
			String[] productUrl = null;
			String[] productDescription = null;
			String[] productPictureUrl = null;
			String[] merchantUpdateDate = null;
			String[] productNameDescription = null;
			String[] productExpire = null;
			String[] realProductId = null;
			String[] productBasePrice = null;
			String[] productUpc = null;
			String[] data = null;
			
			String[] result = evaluate(html, config.get("expire"));
			if (result != null) {
	    		productExpire = getProductExpire(result);                
	    		if(productExpire != null && productExpire.length == 1 && productExpire[0].equals("true"))
	    			return processHttpStatus(httpStatus);
	    	}
			
			result = evaluate(html, config.get("name"));
			if (result != null) {
				productName = getProductName(result);
				productName = FilterUtil.formatHtml(productName);
				productName = FilterUtil.removeSpace(productName);
			}
			
			result = evaluate(html, config.get("price"));
			if (result != null) {
				productPrice = getProductPrice(result);
				productPrice = FilterUtil.removeSpace(productPrice);
			}
			result = evaluate(html, config.get("url"));
			if (result != null) {
				productUrl = getProductUrl(result);
				productUrl = FilterUtil.removeSpace(productUrl);
			}
			result = evaluate(html, config.get("description"));
			if (result != null) {
				productDescription = getProductDescription(result);
				productDescription = FilterUtil.formatHtml(productDescription);
				productDescription = FilterUtil.removeSpace(productDescription);
			}
			result = evaluate(html, config.get("pictureUrl"));
			if (result != null) {
				productPictureUrl = getProductPictureUrl(result);				
				productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
			}
			result = evaluate(html, config.get("merchantUpdateDate"));
			if (result != null) {
				merchantUpdateDate = getProductMerchantUpdateDate(result);
				merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
				merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
			}
			result = evaluate(html, config.get("nameDescription"));
			if (result != null) {
				productNameDescription = getProductNameDescription(result);
				productNameDescription = FilterUtil.removeSpace(productNameDescription);
			}
						
			result = evaluate(html, config.get("realProductId"));
			if (result != null) {
				realProductId = getRealProductId(result);
				realProductId = FilterUtil.removeSpace(realProductId);
			}
			
			result = evaluate(html, config.get("basePrice"));
			if (result != null) {
				productBasePrice = getProductBasePrice(result);
				productBasePrice = FilterUtil.removeSpace(productBasePrice);
			}
			
			result = evaluate(html, config.get("upc"));
			if (result != null) {
				productUpc = getProductUpc(result);
				productUpc = FilterUtil.removeSpace(productUpc);
			}
			
			result = evaluate(html, config.get("data"));
			if (result != null) {
				data = getData(data);
				data = FilterUtil.removeSpace(data);
			}
			
			ProductDataBean[] productDataBean = null;
			FILTER_TYPE filterType = getFilterType();
			MergeProductDataBean mpdBean = new MergeProductDataBean();
			mpdBean.setProductName(productName);
			mpdBean.setProductPrice(productPrice);
			mpdBean.setProductUrl(productUrl);
			mpdBean.setProductDescription(productDescription);
			mpdBean.setProductPictureUrl(productPictureUrl);
			mpdBean.setMerchantUpdateDate(merchantUpdateDate);
			mpdBean.setProductNameDescription(productNameDescription);
			mpdBean.setRealProductId(realProductId);
			mpdBean.setCurrentUrl(url);
			mpdBean.setProductBasePrice(productBasePrice);
			mpdBean.setProductUpc(productUpc);
			mpdBean.setData(data);
			
			if (filterType.equals(FILTER_TYPE.DEFAULT)) {
	        	productDataBean = mergeProductData(mpdBean);   	   	
	        } else if (filterType.equals(FILTER_TYPE.PRODUCTLIST)) {
	        	result = evaluate(html, config.get("allData"));
				if (result != null) {
					productDataBean = getAllProductData(result);
					for (int i = 0; i < productDataBean.length; i++) {
						if(productDataBean[i] == null)
							continue;
						String name = FilterUtil.formatHtml(productDataBean[i].getName());
						name = FilterUtil.removeSpace(name);
						productDataBean[i].setName(name);
						
						String description = FilterUtil.formatHtml(productDataBean[i].getDescription());
						description = FilterUtil.removeSpace(description);
						productDataBean[i].setDescription(description);
						
						productDataBean[i].setPictureUrl(FilterUtil.removeSpace(productDataBean[i].getPictureUrl()));
					}
				}	        	                
	        	productDataBean = mergePriceListProductData(productDataBean, mpdBean);
	        }
			
			if(isEmpty(productDataBean))
				productDataBean = new ProductDataBean[]{ new ProductDataBean()};
			productDataBean[0].setHttpStatus(httpStatus);
			
			return productDataBean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public FILTER_TYPE getFilterType() {
		return FILTER_TYPE.DEFAULT;
	}
	
	public ProductDataBean[] getAllProductData(String[] evaluateResult) {
		return new ProductDataBean[0];
	}
	
	protected String[] mapCategory(String text, String[][] catIdKeywordMapping) {
		
		text = text.replace("-", " ");		
		for (int i = 0; i < catIdKeywordMapping.length; i++) {			
			String[] catMapping = catIdKeywordMapping[i];			
			String keyword = catMapping[1];
			if(FilterUtil.isMatchKeyword(keyword.trim(), text.trim())) {
				return catMapping;			
			}			
		}		
		return null;		
	}

	public String checkUrlBeforeParse(String url) {
		return null;
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus) {
		return processHttpStatus(httpStatus, true);
	}
	
	private ProductDataBean[] processHttpStatus(String httpStatus, boolean expire) {
		ProductDataBean[] rtn = new ProductDataBean[] {new ProductDataBean()};
		rtn[0].setHttpStatus(httpStatus);
		rtn[0].setExpire(expire);
		return rtn;
	}
	
	public static String getCookies(String url) {
		HttpURLConnection conn = null;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(false);			
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(100000);
			conn.setReadTimeout(10000);
			List<String> cookiesx = conn.getHeaderFields().get("Set-Cookie");
			String cookies1 = cookiesx.get(0);
			String cookies2 = cookiesx.get(1).replace(" path=/","");
			String cookies = cookies2+cookies1;
			return cookies;
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
    }
	
	
	public static String[] onlyMehttp(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(100000);
			conn.setReadTimeout(100000);
			if(cookies==null) {
				cookies = getCookies(url);
			}

			conn.setRequestProperty("cookie",cookies);
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
