package web.parser.filter;

import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class BSGlobalTradeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product_title entry-title\">"});
		map.put("price", new String[]{"<p class=\"price\">"});
		map.put("basePrice", new String[]{"<p class=\"price\">"});
		map.put("description", new String[]{"<div class=\"woocommerce-product-details__short-description\">"});
		map.put("pictureUrl", new String[]{"<div id=\"sns_thumbs\" class=\"thumbnails\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		
		if(evaluateResult.length > 0) {
			price = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
			if(price.contains("<del>")) {
				price = FilterUtil.getStringAfter(price, "</del>", "");
			}
			price = FilterUtil.getStringBetween(price, "<span class=\"woocommerce-Price-amount amount\">", "<");
			price = FilterUtil.removeCharNotPrice(price);
		}
		
		return new String[] {price};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		
		if(evaluateResult.length > 0) {
			basePrice = StringEscapeUtils.unescapeHtml4(evaluateResult[0]);
			basePrice = FilterUtil.getStringBetween(basePrice, "<del>", "</del>");
			basePrice = FilterUtil.getStringBetween(basePrice, "<span class=\"woocommerce-Price-amount amount\">", "<");
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		
		return new String[] {basePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
    		try {
    			productPictureUrl = URLEncoder.encode(productPictureUrl, "UTF-8");
    			productPictureUrl = productPictureUrl.replace("%3A", ":");
    			productPictureUrl = productPictureUrl.replace("%2F", "/");
    		} catch (Exception e) {
				// TODO: handle exception
			}
        }
    	return new String[] {productPictureUrl};
	}

	    
}