package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import product.processor.ProductFilter;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class RungsiyoHTMLParser extends DefaultHTMLParser {
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"product-detail\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-picture\">"});
		
		return map;
	}

	public String[] getProductName(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"product-name\">","</div>");
    		
        }	    	
        if(ProductFilter.checkCOVIDInstantDelete(productPrice))
        	return null;
		return new String[] {productPrice};
	}

	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
        }	    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) { 
		String productDes = "";
		if (evaluateResult.length > 0) {
			productDes = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productDes};
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";
		if (evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0],"src=\"","\"");
		}
		return new String[] {productPic};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		
		if (productName == null || productPrice == null || productPrice.length == 0) {
			return null;
		}
		String idFromUrl = currentUrl.substring(currentUrl.lastIndexOf("/")+1, currentUrl.length());
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0] + " ("+idFromUrl+")");	
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}

		return rtn;
	}
}
