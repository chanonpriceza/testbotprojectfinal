package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SiamOfficeFurnitureHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div style='padding-bottom: 10px;' class='NewProductDetail'>"});
		map.put("pictureUrl", new String[]{"<table align='center' border='0' cellspacing='0' cellpadding='0' class='ProductPictureBorder' width='150' height='150'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<font class='ProductPricePrice'>", "</font>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		
    		if(productPrice.equals("") || productPrice.equals("0.00")){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		for (int i = 0; i < evaluateResult.length; i++) {
				if(evaluateResult[i].contains("รายละเอียด")) {					
					productDesc = FilterUtil.toPlainTextString(evaluateResult[i]);
					productDesc = productDesc.trim();
					if(productDesc.startsWith("รายละเอียด")) {
						productDesc = productDesc.substring(10).trim();		
					}					
					if(productDesc.startsWith(":")) {
						productDesc = productDesc.substring(1);		
					}
					
					break;
				}
			}
    			
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}