package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class MosmannaustraliaThailandHTMLParser extends DefaultHTMLParser {

	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
	    	map.put("name", 			new String[]{"<h1 class=\"product_name\" itemprop=\"name\">"});
			map.put("price", 			new String[]{"<span class=\"current_price \">"});
			map.put("basePrice", 		new String[]{"<span class=\"was_price\">"});
			map.put("description", 		new String[]{"<div class=\"description\" itemprop=\"description\">"});
			map.put("pictureUrl", 		new String[]{""});
			map.put("expire", 			new String[]{"<span class=\"sold_out\">"});
			map.put("realProductId", 	new String[]{"<div class=\"seven columns medium-down--one-whole  omega\">"});
			return map;
		}

		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";
			if(evaluateResult.length > 0){						
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			}
			return new String[] {productName};
		}
		public String[] getProductPrice(String[] evaluateResult) {
			
			String productPrice = "";   
			if(evaluateResult.length > 0){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0],"$","</span>");
				productPrice = FilterUtil.toPlainTextString(productPrice);
				productPrice = FilterUtil.removeCharNotPrice(productPrice);
			}
			return new String[] {productPrice};
		}
		
		public String[] getProductBasePrice(String[] evaluateResult) {
			String productBasePrice = "";   
			if(evaluateResult.length > 0){
				productBasePrice =FilterUtil.getStringBetween(evaluateResult[0],"$","</span>");
				productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
			}
			return new String[] {productBasePrice};
		}

		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
			if(evaluateResult.length > 0){
				productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
			}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = null;
			
			if(evaluateResult.length > 0){
				productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0],"og:image\" content=\"","\"");
				productPictureUrl = FilterUtil.toPlainTextString(productPictureUrl);	
			}
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pid = "";
			if(evaluateResult.length == 1){
				pid = FilterUtil.getStringBetween(evaluateResult[0], "data-product-id=\"", "\"");
			}
			return new String[] {pid};
		}
		
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			String expire = "false";
			if(evaluateResult.length >0) {
				if(evaluateResult[0].contains("Sold Out")) {
					expire = "true";
				}
			}
			return new String[] {expire};
		}
		
		  @Override
		    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

				String[] productName = mpdBean.getProductName();
				String[] productPrice = mpdBean.getProductPrice();
				String[] productDescription = mpdBean.getProductDescription();
				String[] productPictureUrl = mpdBean.getProductPictureUrl();
				String[] realProductId = mpdBean.getRealProductId();
				String currentUrl = mpdBean.getCurrentUrl();
				String[] bPrice = mpdBean.getProductBasePrice();

				if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
					return null;
				}
				ProductDataBean[] rtn = new ProductDataBean[1];
				rtn[0] = new ProductDataBean();
				String pdName = productName[0];
				if(realProductId != null && realProductId.length != 0){
					String ProductId = realProductId[0];
					if(StringUtils.isNotBlank(ProductId)){
						pdName += " ("+ ProductId + ")";
					}
				}
				rtn[0].setName(pdName);
				rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
				
				if(bPrice != null && bPrice.length == 1){
					rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
				}


				if (productDescription != null && productDescription.length != 0) {
					rtn[0].setDescription(productDescription[0]);
				}

				if (productPictureUrl != null && productPictureUrl.length != 0) {
					if (productPictureUrl[0] != null && !productPictureUrl[0].trim().isEmpty()) {
						if (productPictureUrl[0].startsWith("http")) {
							rtn[0].setPictureUrl(productPictureUrl[0]);
						} else {
							try {
								URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
								rtn[0].setPictureUrl(url.toString());
							} catch (MalformedURLException e) {}
						}
					}
				}
				
				if(rtn[0].getBasePrice() == rtn[0].getPrice() ){
					rtn[0].setBasePrice(0.00);
				}
				
				return rtn;
			}
		
}
