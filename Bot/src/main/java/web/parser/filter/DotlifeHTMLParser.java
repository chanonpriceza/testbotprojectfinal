package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class DotlifeHTMLParser extends DefaultHTMLParser{	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<div class=\"product-options-bottom\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("realProductId", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		String brand = "";
		if(evaluateResult.length > 0) {			
			brand = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"value\" itemprop=\"brand\">", "</div>");	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"base\" data-ui-id=\"page-title-wrapper\" itemprop=\"name\">", "</span>");	
			productName = brand+" "+productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"product:price:amount\" content=\"", "\"");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    	} 
    	if(productPrice.trim().equals("0")) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
		return new String[] {productPrice};
	}
	
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";    	
		if (evaluateResult.length > 0) {
			basePrice = FilterUtil.getStringBetween(evaluateResult[0], "data-price-type=\"oldPrice\"", "</span>");
			basePrice = FilterUtil.toPlainTextString(basePrice);
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
    	} 
		return new String[] {basePrice};
	}
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"description\" content=\"", "\"");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		
		String realProductId = null;
    	if (evaluateResult.length > 0) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("input[name=product]");
    		realProductId = e.val();
        }
    	return new String[] {realProductId};
	}
	
	@Override
	   public String[] getProductExpire(String[] evaluateResult) {
		String checkOutofstock ="";   
		if(evaluateResult.length > 0) {
			  checkOutofstock = FilterUtil.getStringBetween(evaluateResult[0], "<span  class=\"text-stock\">", "</span>");
			   if(checkOutofstock.contains("สินค้าหมด")) {
				   return new String[]{"true"};
			   }
		   }
		   return new String[]{"false"};
	   }
}