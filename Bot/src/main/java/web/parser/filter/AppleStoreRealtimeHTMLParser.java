package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AppleStoreRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<h1 class=\"materializer\" data-autom=\"productTitle\">"});
		map.put("price", 		 new String[]{"<span class=\"current_price\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			return new String[] {FilterUtil.toPlainTextString(evaluateResult[0])};
		}
		return null;
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		if(evaluateResult!= null && evaluateResult.length > 0) {
			String price = "";
			for (String eva : evaluateResult) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(eva));
				break;
			}
			return new String[] { price };
		}
		return null;
	}

}

