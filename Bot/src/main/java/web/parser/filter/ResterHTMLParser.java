package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class ResterHTMLParser extends DefaultHTMLParser {
	
	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<title>"});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{""});
		map.put("description", 	new String[]{"<div class=\"column mcb-column mcb-item-hv7lxa8yl one column_column\">"});
		map.put("pictureUrl", 	new String[]{"<dt class='gallery-icon landscape'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";
		if(evaluateResult.length > 0) {		
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
			productName = FilterUtil.getStringBefore(productName,"|",productName);	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<p style =\" color : black; font-size : 16px;\">", "฿");
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-detail-price\">", "</div>");
    		}
    		if(productPrice.equals("")) {
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"product-detail-price-original no-special-price\">", "</div>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
    	if(productPrice.length() == 0){
			productPrice = BotUtil.CONTACT_PRICE_STR;
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<font style =\"font-size : 14px; color : grey\"><s>", "</s>");
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}
		return new String[] {productPrice};
	}
	
	
	
	public String[] getProductDescription(String[] evaluateResult) {		
		String productDesc = "";    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a href='", "'");
        }
    	return new String[] {productPictureUrl};
	}
}