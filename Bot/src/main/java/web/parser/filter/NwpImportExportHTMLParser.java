package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class NwpImportExportHTMLParser extends DefaultHTMLParser {
	public String getCharset() {
		return "windows-874";
	}		
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<font color='#000000' size='3'>"});
    	map.put("price", new String[]{""});
    	map.put("basePrice"		, new String[]{""});
		map.put("description", new String[]{"<td colspan='2'>"});
		map.put("pictureUrl", new String[]{"<td valign='top' width='90%' align='center'>"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0], ">", "</font>");
			if(productName.contains("โบชัวร์สินค้าหน้าที่") || productName.contains("วิธี")){
				productName = "";
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length == 1) {
			 String priceSpecial = FilterUtil.getStringBetween(evaluateResult[0], "<Td class='text3'>ราคาพิเศษ&nbsp;<b><FONT FACE='MS Sans Serif,BrowalliaUPC'  size='3'>฿<font COLOR='red'>","</font></font></b>");
			 if(StringUtils.isBlank(priceSpecial)){
				 priceSpecial = FilterUtil.getStringBetween(evaluateResult[0], "<Td class='text3'>ราคา&nbsp;<b><FONT FACE='MS Sans Serif,BrowalliaUPC' size='3'>฿<font COLOR='red'>","</font></font></b>");
			 }
			 if(priceSpecial!= null && priceSpecial.toString().length() > 0 ){
				 productPrice = priceSpecial;
				 productPrice = FilterUtil.removeCharNotPrice(productPrice); 
			 }else{
				 productPrice = BotUtil.CONTACT_PRICE_STR;	
			 }
			
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = "";   
		if(evaluateResult.length == 1){
		String checkBase = FilterUtil.getStringBetween(evaluateResult[0], "<Td class='text3'>ราคา&nbsp;<b><FONT FACE='MS Sans Serif,BrowalliaUPC' size='3'>฿<font COLOR='red'><del>", "</del></font></font></b></Td>");
		if(checkBase!= null && checkBase.toString().length() > 0 ){
			productBasePrice = FilterUtil.toPlainTextString(checkBase);		
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		 }
		}
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img src='", "' border='0' border='0'");
    	}
    	return new String[] {productPictureUrl};
	}
	
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();
		 
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		if(StringUtils.isBlank(productName[0]) || StringUtils.isBlank(productPrice[0])) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		String rid = "";
		rid = FilterUtil.getStringAfterLastIndex(currentUrl, "product&productid=", "");
		 if (StringUtils.isNotBlank(rid)) {
			rtn[0].setName(productName[0] + " ("+rid+")" );
		} else {
			return null;
		}
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}

		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}

		return rtn;
	}    
}