package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class NanmeebooksHTMLParser extends BasicHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
    	//require
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{""});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{""});	
		map.put("expire", 		new String[]{"<TD valign=top width=50% >"});
		map.put("realProductId", new String[]{""});
		/*map.put("nameDescription", new String[]{""});*/
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	if(evaluateResult.length == 1) {
        	if(evaluateResult[0].indexOf("หมดแล้ว") != -1) {
        		return new String[]{"true"};
        	}	
    	}    	
        return new String[]{""};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			productName = FilterUtil.toPlainTextString(e.attr("content"));		
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"amount\">ราคา : </span>", "</span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);	
		}	
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String productBasePrice = ""; 	
		if (evaluateResult.length > 0) {
			if(evaluateResult.length == 1) {	
				productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"amount weight-700\">", "</span>");
				productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);	
			}	
		}	
		return new String[] {productBasePrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String product = ""; 	
		if (evaluateResult.length > 0) {
				product = FilterUtil.getStringBetween(evaluateResult[0], "data-productid=\"", "\"");
				product = FilterUtil.removeCharNotPrice(product);	
		}	
		return new String[] {product};
	}
	
	
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String head = "";    	
		String detail = "";    	
    	if(evaluateResult.length > 0) {
    		
    		String author = FilterUtil.getStringBetween(evaluateResult[0], "\">ผู้เขียน :", "</a>");
    		String translater = FilterUtil.getStringBetween(evaluateResult[0], "\">นักแปล :", "</a>");
    		if(StringUtils.isNotBlank(author)){
    			if(!author.equals("-")){
    				head = "ผู้แต่ง";
    				detail = author;
    			}

    		}
    		
    		if(StringUtils.isNotBlank(translater)){
    			if(!translater.equals("-")){
    				if(StringUtils.isNotBlank(detail)){
    					head = head + "/";
    					detail = detail + " / ";
    				}
    				head = head + "ผู้แปล";
    				detail = detail + translater;
    			}
    		}
    		
    		
    	}
    	
    	return new String[] {head, detail};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;				
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:image]");
			productPictureUrl = FilterUtil.toPlainTextString(e.attr("content"));		
		}		
    	return new String[] {productPictureUrl};
	}
	
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        String tmp = "";

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
    	if(mixIdName&&realProductId!=null&&realProductId.length>0&&StringUtils.isNotBlank(realProductId[0]))
    		tmp = " ("+realProductId[0]+")";
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+tmp);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if(allowContractPrice&&rtn[0].getPrice()==0)
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);

		if (productDescription != null && productDescription.length == 2) {
			if(productDescription != null && productDescription[1].trim().length() > 0){
				rtn[0].setDescription(productDescription[0] + " : " + productDescription[1]);
				rtn[0].setKeyword(productDescription[1]);
			}
		}
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		if(rtn!=null&&rtn.length>0&&StringUtils.isBlank(rtn[0].getUrl()))
			rtn[0].setUrl(currentUrl);
		
		
		return rtn;
	}

	    
}