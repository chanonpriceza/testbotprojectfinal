package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class UniqloHTMLParser extends DefaultHTMLParser{	

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<h1 id=\"goodsNmArea\">"});
		map.put("price", 		new String[]{"<p class=\"special-price\">"});
		map.put("basePrice", 	new String[]{"<p class=\"old-price\">"});
		map.put("description", 	new String[]{"<div class=\"content con-wdt\">"});
		map.put("pictureUrl",	new String[]{"<div id=\"prodImgDefault\">"});
		
		return map;
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0){
			return new String[]{""};
		}
		return new String[]{"true"};
	}

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length > 0) {
			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

}
