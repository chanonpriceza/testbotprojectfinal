package web.parser.filter;


import org.apache.commons.text.StringEscapeUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;

public class HappyMoveHTMLParser extends ReadyPlanetHTMLParser {

	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			String rawBrand = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8px 0 8px 0;'><span class='hd'>ยี่ห้อ", "</div>");
			if(rawBrand.isEmpty()){
				rawBrand = FilterUtil.getStringBetween(evaluateResult[0], "<div  style='margin:5px 0 5px 0;'><span class='hd2'>ยี่ห้อ", "</div>");
			}
			String rawType = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8px 0 8px 0;'><span class='hd'>รุ่น", "</div>");
			
			String rawName = FilterUtil.getStringBetween(evaluateResult[0], "<td colspan='2'><div class='h1'>", "</div>");
			String rawId = FilterUtil.getStringBetween(evaluateResult[0], "<div style='margin:8px 0 8px 0;'><span class='hd'> รหัส", "</div>");
			rawBrand = FilterUtil.toPlainTextString(rawBrand);
			rawType = FilterUtil.toPlainTextString(rawType);
			rawName = FilterUtil.toPlainTextString(rawName);
			rawId = FilterUtil.toPlainTextString(rawId);
			
			rawBrand = rawBrand.replace(":", "");
			rawBrand = rawBrand.replace("้", "");
			rawBrand = rawBrand.replace("็", "").trim();
			if(rawBrand.equals("happymove") || rawBrand.equals("Happymove") || rawBrand.equals("HAPPY MOVE")
					|| rawBrand.equals("happy move") || rawBrand.equals("Happy move")){
				rawBrand = "Happy Move";
			}
			rawType = rawType.replace(":", "").trim();
			rawName = rawName.replace("&nbsp;", "").trim();
			rawId = rawId.replace(":", "").trim();
			
			if(rawName.trim().equals(rawBrand.trim())){
				rawBrand = "";
			}
			if(rawType.trim().equals(rawId.trim())){
				rawType = "";
			}

			productName = rawBrand+" "+rawName+" "+rawType + " ("+rawId+")";
			productName = StringEscapeUtils.unescapeHtml4(productName);
			if(productName.contains("สินค้าหมด stock")){
				productName = "";
    		}
			
			if(productName.contains("(Sold out)")){
				productName = "";
			}
			
			if(productName.contains("(สินค้าหมด)")){
				productName = "";
			}
			
		}
		return new String[] {productName};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productUpc = mpdBean.getProductUpc();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productDescription != null && productDescription.length != 0) {
			if(productDescription != null && productDescription[0].trim().length() > 0){
				rtn[0].setDescription(productDescription[0]);
			}
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}

		// can remove if realProductId, basePrice and upc are not required
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}
		if (productBasePrice != null && productBasePrice.length != 0 && !productBasePrice[0].equals(productPrice[0])) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
		}

		return rtn;
	}
	
}
