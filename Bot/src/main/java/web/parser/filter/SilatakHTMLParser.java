package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;

import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SilatakHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{""});
		map.put("price", 		new String[]{""});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{""});
//		map.put("realProductId", 	new String[]{""});
		map.put("expire", new String[]{""});
		return map;
	}
    
    @Override
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1 class=\"w3-", "</h1>");
			productName = FilterUtil.getStringAfter(productName, ">", productName);
			if(productName.equals("")){
				productName = FilterUtil.getStringBetween(evaluateResult[0], "<h1>", "</h1>");				
			}
			productName = FilterUtil.toPlainTextString(productName);
			
			String type = FilterUtil.getStringBetween(evaluateResult[0], "ประเภท: ", " ");
			type = FilterUtil.getStringBefore(type, "<", type);
			if(type != null && type.length() > 0){
				productName += " " + type;
			}
			
			type = FilterUtil.getStringBetween(evaluateResult[0], "ประเภท:</strong>", "</li>");
			type = FilterUtil.getStringBefore(type, "<", type);
			if(type != null && type.length() > 0){
				productName += " " + type;
			}
		}		
		return new String[] {productName};
	}
	@Override
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";			
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<strong class=\"w3-text-red\" style=\"font-size: 2em;\">", "</strong>");
			if(productPrice.equals("")){
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
			}
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			if(productPrice.equals("") || productPrice.equals("0")){
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}		
		return new String[] {productPrice};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String productDes = "";				
		if(evaluateResult.length > 0) {
			productDes = FilterUtil.getStringBetween(evaluateResult[0], "<ul class=\"w3-medium\">", "</ul>");
			if(productDes.equals("")){
				productDes = FilterUtil.getStringBetween(evaluateResult[0], "<h2>", "</h2>");
			}
			productDes = FilterUtil.toPlainTextString(productDes);
		}		
		return new String[] {productDes};
	}
	
	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPic = "";				
		if(evaluateResult.length > 0) {
			productPic = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"images-area \" style=\" \">", "<div class=\"descriptionContainer \">");
			productPic = FilterUtil.getStringBetween(productPic, "data-src=\"", "\"");
			productPic = FilterUtil.toPlainTextString(productPic);

			
			if(productPic == null || productPic.length() <= 0){
				productPic = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\"itemShow", "alt");
				productPic = FilterUtil.getStringBetween(productPic, "src=\"", "\"");
			}
			if(productPic == null || productPic.length() <= 0){
				productPic = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"text-section-loader \">", "alt");
				productPic = FilterUtil.getStringBetween(productPic, "src=\"", "\"");
			}
			
			if(StringUtils.isNoneBlank(productPic)){
				productPic = "http://www.silatak.com"+productPic;				
			}
		}		
		return new String[] {productPic};
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] name = mpdBean.getProductName();
		String[] price = mpdBean.getProductPrice();
		String[] desc = mpdBean.getProductDescription();
		String[] pictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		
		if(name == null || name.length == 0 || price == null || price.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		rtn[0].setName(name[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(price[0]));

		if(desc != null && desc.length > 0) {
			rtn[0].setDescription(desc[0]);
		}
		
		if(realProductId != null && realProductId.length > 0){
			rtn[0].setRealProductId(realProductId[0]);
		}
	
		if(pictureUrl != null && pictureUrl.length > 0) {
			rtn[0].setPictureUrl(pictureUrl[0]);
//			if (pictureUrl[0] != null && pictureUrl[0].trim().length() != 0) {
//				if (pictureUrl[0].startsWith("http")) {
//					rtn[0].setPictureUrl(pictureUrl[0]);
//				} else {
//					try {
//						URL url = new URL(new URL(currentUrl), pictureUrl[0]);
//						
//						rtn[0].setPictureUrl(url.toString());
//					} catch (MalformedURLException e) { }
//				}
//			}
		}
		
		return rtn;
	}

}
