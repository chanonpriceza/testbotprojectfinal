package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TreeboxpackageHTMLParser extends DefaultHTMLParser {
	  public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});		
			map.put("expire", new String[]{""});
			map.put("realProductId", new String[]{""});
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			String productName = ""; 
			String pack = "";
			String pdId = "";
			if(evaluateResult.length > 0 ) {	
				productName = FilterUtil.getStringBetween(evaluateResult[0],"<h1 class=\"product_title entry-title\">","</h1>");
				pack = FilterUtil.getStringAfter(evaluateResult[0],"<span class='pack'>","<div class=\"quantity\">");
				pack = FilterUtil.getStringBetween(evaluateResult[0],"</span><span class='pack'>","</span>");
				pack = pack.replace("จำนวนสินค้า", "");
				pack = FilterUtil.toPlainTextString(pack);
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"redirect_to\" value=\"", "\"");
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"sku\">", "</span>");
				pdId =  FilterUtil.toPlainTextString(pdId).trim();
				productName = productName + " ("+pack+ ") ("+pdId+")";			
			}
			return new String[] {productName};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			String productPrice = "";    	
	    	if (evaluateResult.length > 0) {
	    		Document doc = Jsoup.parse(evaluateResult[0]);
	    		productPrice = doc.select("p.price").html();
	    		productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
	    		if(productPrice.equals("")) {
	    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"summary entry-summary\">", "<p class=\"stock in-stock\">");
		    		productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span></p>");
	    		}
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
	        }		
			return new String[] {productPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			String productDesc = "";
			String productDescMain = "";
	    	if(evaluateResult.length > 0) {
	    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"woocommerce-product-details__short-description\">", "</div>");	
	    		productDesc = FilterUtil.getStringBetween(productDesc, "<p>", "</p>");	
	    		productDescMain = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab\" id=\"tab-description\" role=\"tabpanel\" aria-labelledby=\"tab-title-description\">", "</div>");	
	    		productDescMain = FilterUtil.getStringAfter(productDescMain, "<h2>รายละเอียด</h2><p>", productDescMain);
	    		productDesc = productDesc +" "+ productDescMain;
	    		productDesc = FilterUtil.toPlainTextString(productDesc);
	    		productDesc = FilterUtil.removeHtmlTag(productDesc);
	    	}
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			String productPictureUrl = "";
	    	if (evaluateResult.length == 1) {
	    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<figure class=\"woocommerce-product-gallery__wrapper\">", "</figure>");
	    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<a href=\"", "\"");
	    		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
	        }
	    	return new String[] {productPictureUrl};
		}
		
		public String[] getProductExpire(String[] evaluateResult) {
			
	    	if(evaluateResult.length == 1) {
	        	if(evaluateResult[0].indexOf("สินค้าหมดแล้ว") != -1) {
	        		 return new String[]{"true"}; 
	        	}     	
	    	}    	
	        return new String[]{""};  // no get product
		}
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			String pdId = "";
			if(evaluateResult.length == 1){
				pdId = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"sku\">", "</span>");
				pdId =  FilterUtil.toPlainTextString(pdId).trim();

			}
				return new String[] {pdId};
		}
		
		
}
