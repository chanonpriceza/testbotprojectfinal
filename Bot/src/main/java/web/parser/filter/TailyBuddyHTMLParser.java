package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

public class TailyBuddyHTMLParser  extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 itemprop=\"name\" class=\"hidden-xs margin-bottom-xs\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{"<p class=\"old-price\">"});
		map.put("description", new String[]{"<div id=\"myTabContent\" class=\"tab-content col-xs-12 padding-v-xs\">"});
		map.put("pictureUrl", new String[]{"<div class=\"text-center margin-bottom-sm\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);	
			productName = StringEscapeUtils.unescapeHtml4(productName);
		}
		
		return new String[] {productName};
	}
	
		
		public String[] getProductPrice(String[] evaluateResult) {
                    
			String productPrice = ""; 
		 if (evaluateResult.length > 0) { 
	    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<td class=\"text-danger\">", "</td>");
	    		productPrice = FilterUtil.getStringBetween(productPrice, "<strong>", "</strong>");
	    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 	
	    		if(StringUtils.isBlank(productPrice)) {
		        	productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"col-sm-10\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">", "</div>");
		    		productPrice = FilterUtil.getStringBetween(productPrice, "<strong>", "</strong>");
		    		productPrice = FilterUtil.removeCharNotPrice(productPrice); 
		        }	
	        } 
			return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);  
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\">");
        }
    	
    	return new String[] {productPictureUrl};
	}
}
