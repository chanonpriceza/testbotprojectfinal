package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.BotUtil;
import utils.FilterUtil;

public class ZoomCameraHTMLParser extends BasicHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String name = "";
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("h1.product_title");
			name = e.html();
		}
		return new String[] {name};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String price = "";
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("ins");
			price = FilterUtil.removeCharNotPrice(e.html());
			if(StringUtils.isBlank(price)) {
				doc = Jsoup.parse(evaluateResult[0]);
				e = doc.select("p.price");
				e = e.select("span.woocommerce-Price-amount");
				price = FilterUtil.removeCharNotPrice(e.html());
				if("0".equals(price))
					price = BotUtil.CONTACT_PRICE_STR;
			}
		}
		return new String[] {price};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.price");
			e = e.select("del");
			basePrice = FilterUtil.removeCharNotPrice(e.html());
		}
		return new String[] {basePrice};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "";
		if(evaluateResult.length > 0) {
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("p.stock");
			expire = e.html();
			if(expire.contains("Out of stock"))
				expire = "true";
		}
		return new String[] {expire};
	}
		
	
}
