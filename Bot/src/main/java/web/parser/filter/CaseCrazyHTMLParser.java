package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class CaseCrazyHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"productinfoname\">"});
		map.put("price", new String[]{"<div class=\"product-info-price\">"});
//		map.put("basePrice", new String[]{"<div class=\"product-info-price\">"});
		map.put("description", new String[]{"<div class=\"value std\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product media\">"});
		map.put("expire", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
				
		if(evaluateResult.length > 0) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		String	productPrices = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price\">", "</span>");
    		if (StringUtils.isBlank(productPrices)) {
				productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		}else{
    			productPrice = FilterUtil.toPlainTextString(productPrices);
    		}
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if (evaluateResult.length >= 1) {

			bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);

		}
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length >= 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\"img-responsive\" src=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
	public String[] getProductExpire(String[] evaluateResult) {
		String expire = "";
		if (evaluateResult.length == 1) {
			expire = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"stock1 unavailable1\" title=\"Availability\">", "</div>");
			if (expire.indexOf("Out of stock") != -1) {
				return new String[] { "true" };
			}
		}
		return new String[] { "" };
	}
	
	  public String[] getRealProductId(String[] evaluateResult) {
	    	String realProductId = "";   
			
			if(evaluateResult.length == 1) {	
				realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"product\" value=\"", "\"").trim();
			}
			return new String[] {realProductId};
		}
}
