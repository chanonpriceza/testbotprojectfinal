package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AGNConsultanHaHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<td class=\"mainhead alignLeft\">"});
		map.put("basePrice",new String[]{"<table width=\"99%\" cellspacing=\"0\" cellpadding=\"5\">"});
		map.put("price", new String[]{"<table width=\"99%\" cellspacing=\"0\" cellpadding=\"5\">"});
		map.put("description", new String[]{"<td class=\"txt12pt alignLeft\">"});
		map.put("pictureUrl", new String[]{"<td align=\"center\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<b>", "</b>");
			productName = productName.replaceAll("&quot;", "\"");
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}

	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String basePrice = "";
		if(evaluateResult.length==1){
			basePrice = FilterUtil.getStringBetween(evaluateResult[0],"ราคาปกติ","</s>");
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
		}
		
		return new String[] {basePrice};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length == 1) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<font color=red>", "</font>");
			if(StringUtils.isBlank(productPrice)) {
				productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<s>", "</s>");
			}
			
			productPrice = FilterUtil.toPlainTextString(productPrice);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
			if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice)) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length > 3) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[3]);    	
		}
		productDesc = productDesc.replaceAll("�","");
		productDesc = productDesc.replaceAll("&#65533;","");
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		//http://m4supply.com/showproduct/1437018569467222658/Notebook-Lenovo-B50-80-(80EW02D5TA).html
		if("http://m4supply.com/showproduct/1437018569467222658/Notebook-Lenovo-B50-80-(80EW02D5TA).html".equals(currentUrl)){
			return null;
		}
	
		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		
		String priceStr = productPrice[0];
		double  price = Double.parseDouble(BotUtil.CONTACT_PRICE_STR);
		if (StringUtils.isNotBlank(priceStr) && Double.parseDouble(priceStr) < 8000000) {
			price = Double.parseDouble(String.format("%.1f", (Double.parseDouble(priceStr) * 1.07)));
		}
		rtn[0].setPrice(price);

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
			
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {
	
					}
				}
			}
		}
		
		if (realProductId != null && realProductId.length != 0){
			rtn[0].setRealProductId(realProductId[0]);
		}
		return rtn;
	}
}