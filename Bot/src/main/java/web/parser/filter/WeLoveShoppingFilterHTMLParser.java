package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WeLoveShoppingFilterHTMLParser extends BasicHTMLParser {
	
	@Override
	public String getCharset() {
		// TODO Auto-generated method stub
		return "TIS-620";
	}
	
	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();
		allowContractPrice = true;
		map.put("name", new String[] { "<title>" });
		map.put("price", new String[] { "<span class=\"fpricer\">" });
		map.put("description", new String[] {"<table width=\"615\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" });
		map.put("pictureUrl", new String[] { "<td width=\"512\" bgcolor=\"#FFFFFF\">","<td width=\"191\" height=\"190\" align=\"center\" valign=\"middle\">"});
		map.put("realProductId", new String[] { "<div style='padding-bottom: 5px; padding-top: 20px;' class='NewProductDetail'>" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			if(StringUtils.isNotBlank(productName))
				productName = FilterUtil.getStringAfter(productName, " : ", productName);
				productName = productName.replace("[Powered by Weloveshopping.com]", "");
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}


		return new String[] {productPrice};
	}
	
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;

    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src='","'");
    		if(productPictureUrl.length()==0)
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"","\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า:","</div>");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    	}
    	return new String[] {realProductId};
	}

}



