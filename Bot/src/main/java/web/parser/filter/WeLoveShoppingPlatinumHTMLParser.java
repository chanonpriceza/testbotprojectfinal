package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WeLoveShoppingPlatinumHTMLParser extends DefaultHTMLParser{

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<div class=\"pro-name\" id=\"moreimage\">" });
		map.put("price", new String[] { "<div class=\"pro-dcprice\">" });
		map.put("basePrice", new String[] { "<div class=\"pro-orgprice\">" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {

		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
	}
	
	@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
		}
	
	public String[] getProductDescription(String[] evaluateResult) {
    	Document doc = Jsoup.parse(evaluateResult[0]);
		Element result = doc.select("table.dataset-3").first();
		String productDesc = "";
		if (evaluateResult.length > 0) {
			productDesc = FilterUtil.toPlainTextString(result.html());
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<link rel=\"image_src\" href=\"","\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<table class=\"dataset-1\">","</table>");
    		realProductId = FilterUtil.getStringBetween(realProductId, "</th>","</td>");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    		if(realProductId.equals("")) {
    			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"pro-desc\" id=\"product-specification\">","<table class=\"dataset-1\">");
        		realProductId = FilterUtil.getStringBetween(realProductId, "<span id=\"itemid-","\">");
        		realProductId = FilterUtil.toPlainTextString(realProductId);
    		}
    	}
    	return new String[] {realProductId};
	}

        
}



