package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.DefaultHTMLParser;

public class TescoLotusHTMLParser extends DefaultHTMLParser{

	String pictureUrl = "";
	
	public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"product-details-tile__title\">"});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("description", new String[]{"<div class=\"memo\">"});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length == 1) {
    		if(!evaluateResult[0].contains("กิโลกรัม")){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "class=\"value\"", "</span>");
        		productPrice = FilterUtil.getStringAfterLastIndex(productPrice, ">", "");
    		}else{
    			productPrice = FilterUtil.getStringAfterLastIndex(evaluateResult[0],"class=\"value\"",evaluateResult[0]);
    			productPrice = FilterUtil.getStringBetween(productPrice,">","<");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if("0.0".equals(productPrice)){
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"offer-text\"", "</span>");
    		if (StringUtils.isNotBlank(bPrice)) {
    			bPrice = FilterUtil.getStringBetween(bPrice, "ราคาปกติ", "บาท");
    			bPrice = FilterUtil.toPlainTextString(bPrice);
    			bPrice = FilterUtil.removeCharNotPrice(bPrice);
    		}
        }	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length > 0) {
    		for (String desc : evaluateResult) {
    			productDesc += desc;
    		}
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
		if(evaluateResult.length == 1){
			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"product-image\"", "/>");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "src=\"", "\"");
    		if(StringUtils.isBlank(productPictureUrl)) {
    			productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "\"gs1:image\":\"", "\"");
    		}
    		pictureUrl = productPictureUrl;
		}
		productPictureUrl = BotUtil.encodeURL(productPictureUrl);
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(!pictureUrl.isEmpty()){
			String tmp = FilterUtil.getStringAfter(pictureUrl, "/assets/TH/", "");
			String[] val = tmp.split("/");
			if(val.length > 1 && StringUtils.isNumeric(val[1])){
				pdId = val[1];
			}
		}
		pictureUrl = "";
		return new String[] {pdId};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();	
		
		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		String pdName = productName[0];
		String engUrl = currentUrl.replace("/th-TH/", "/en-GB/");
		String[] engHTML = HTTPUtil.httpRequestWithStatus(engUrl,"utf-8", true);
		if(engHTML!=null && StringUtils.isNotBlank(engHTML[0])){
			String tmpEng = FilterUtil.getStringBetween(engHTML[0], "<div class=\"description\">", "</h2>");
			tmpEng = FilterUtil.toPlainTextString(tmpEng);		
			tmpEng = FilterUtil.getStringAfter(tmpEng, "m.**", tmpEng).trim();
			if(!pdName.equals(tmpEng) && !tmpEng.isEmpty()){
				pdName += " ("+ tmpEng +")";
			}
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		rtn[0].setName(pdName);

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}

		if (realProductId != null && realProductId.length != 0){
			rtn[0].setRealProductId(realProductId[0]);
			rtn[0].setUpc(realProductId[0]);
		}
		
		return rtn;
	}
	
	
}
