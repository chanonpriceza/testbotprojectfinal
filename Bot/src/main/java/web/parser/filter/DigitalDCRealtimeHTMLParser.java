package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class DigitalDCRealtimeHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		 new String[]{"<span id=\"ctl10_Contentdfcf830f-50fc-4834-aefc-e4eb0636a7c8_Content7d7eb1cf-605a-496e-84e1-e03e10ff9391_Label4\" style=\"font-weight: 700; \">"});
		map.put("price", 		 new String[]{"<span id=\"ctl10_Contentdfcf830f-50fc-4834-aefc-e4eb0636a7c8_Content7d7eb1cf-605a-496e-84e1-e03e10ff9391_Label2\" style=\"font-weight: 700\">"});
		map.put("realProductId", new String[] {"<span id=\"ctl10_Contentdfcf830f-50fc-4834-aefc-e4eb0636a7c8_Content7d7eb1cf-605a-496e-84e1-e03e10ff9391_Label1\" style=\"font-weight: 700\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}
	
	public String[] getRealProductId(String[] evaluateResult) {
		return new String[] {evaluateResult[0]};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		try {
		
    	String name = mpdBean.getProductName()[0];
    	String price = mpdBean.getProductPrice()[0];
    	String realId = mpdBean.getRealProductId()[0];

    	ProductDataBean resultBean = new ProductDataBean();
		resultBean.setPrice(FilterUtil.convertPriceStr(FilterUtil.removeCharNotPrice((FilterUtil.toPlainTextString(price)))));
		resultBean.setName(FilterUtil.toPlainTextString(name) + " (" + FilterUtil.toPlainTextString(realId) + ")");
		
		return new ProductDataBean[] {resultBean};
		
		}catch(Exception e) {
			return null;
		}
	}
	    
}
