package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class TakaroonsinHTMLParser extends DefaultHTMLParser {
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"title\" itemprop=\"name\">"});
		map.put("price", new String[]{"<span itemprop=\"price\">"});
		map.put("description", new String[]{"<div itemprop=\"description\" id=\"tab-description\" class=\"tab-pane active\">"});
		map.put("pictureUrl", new String[]{"<div class=\"image\">"});
		
		return map;
	}
    
   
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
	            
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";   

    	if (evaluateResult.length >= 1) {	
    			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    			productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
    	if(StringUtils.isBlank(productPrice)) {
    		productPrice = BotUtil.CONTACT_PRICE_STR;
    	}
    	
    	
		return new String[] {productPrice};
	}
	

	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
	
		if(evaluateResult.length >= 1){
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
		}
    	return new String[] {productPictureUrl};
	}
	

	

}
