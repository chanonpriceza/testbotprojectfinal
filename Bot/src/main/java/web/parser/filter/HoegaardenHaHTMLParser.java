package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class HoegaardenHaHTMLParser extends BasicHTMLParser {
	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = ""; 
			String info  ="";
			
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("meta[property=og:title]");
				Elements e2 = doc.select("div.product-info");
				info = FilterUtil.getStringBefore(e2.html(),"<p>",e2.html());
				info = FilterUtil.toPlainTextString(info.replaceAll("\\|",""));			
				productName = FilterUtil.toPlainTextString(e.attr("content"));			
			}
			
			return new String[] {productName+" "+info};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("span[itemprop=price]");
				productPrice = FilterUtil.removeCharNotPrice(e.html());			
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {

			String productPrice = "";    	
			if(evaluateResult.length == 1) {	
				Document doc = Jsoup.parse(evaluateResult[0]);
				Elements e = doc.select("div[itemprop=offers]");
				e = e.select("span[id^=old-price-]");	
				productPrice = FilterUtil.removeCharNotPrice(e.html());			
			}	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductPictureUrl(String[] evaluateResult) {
			return new String[] {"https://halobe.files.wordpress.com/2020/01/artboard-200-5.jpg"};
		}
		
		@Override
		public String[] getProductDescription(String[] evaluateResult) {
			return null;
		}
}
