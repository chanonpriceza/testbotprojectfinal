package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.parser.DefaultHTMLParser;

public class BangnashopHTMLParser extends DefaultHTMLParser {
	

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] {"<h1>"});
		map.put("price", new String[] { "<pricefont>" });
		map.put("basePrice", new String[] { "" });
		map.put("description", new String[] { "" });
		map.put("pictureUrl", new String[] { "" });
		map.put("realProductId", new String[] { "<h2>" });
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}

		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length > 0) {
			productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
		}

		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		return new String[] {""};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
    	return new String[] {""};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringAfter(evaluateResult[0], "http://www.bangnashop.com/images/stories/logo/bangnashop-QR-code.jpg","");
    		productPictureUrl = FilterUtil.getStringBetween(productPictureUrl, "<img src=\"", "\"");
    	}
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";
    	if (evaluateResult.length > 0) {
    		realProductId = FilterUtil.getStringAfter(evaluateResult[0], ":","");
    		realProductId = FilterUtil.toPlainTextString(realProductId).trim();
    	}
    	return new String[] {realProductId};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String url = mpdBean.getCurrentUrl();
		
		String[] data = HTTPUtil.httpRequestWithStatus(url, "UTF-8", true);
		if(StringUtils.isNotBlank(data[0])){
			String realUrl = FilterUtil.getStringBetween(data[0], "content=\"0;", "\"");
			if(StringUtils.isNotBlank(realUrl)) {
				ProductDataBean[] result = parse(realUrl);
				return result;
			}else {
				String[] productName = null;
				String[] productPrice = null;
				String[] productUrl = null;
				String[] productDescription = null;
				String[] productPictureUrl = null;
				String[] merchantUpdateDate = null;
				String[] productNameDescription = null;
				String[] realProductId = null;
				String[] productBasePrice = null;
				String[] productUpc = null;
				String[] result = null;
				
				Map<String, String[]> config = getConfiguration();
				
				result = evaluate(data[0], config.get("name"));
				if (result != null) {
					productName = getProductName(result);
					productName = FilterUtil.formatHtml(productName);
					productName = FilterUtil.removeSpace(productName);
				}
				
				result = evaluate(data[0], config.get("price"));
				if (result != null) {
					productPrice = getProductPrice(result);
					productPrice = FilterUtil.removeSpace(productPrice);
				}
				result = evaluate(data[0], config.get("url"));
				if (result != null) {
					productUrl = getProductUrl(result);
					productUrl = FilterUtil.removeSpace(productUrl);
				}
				result = evaluate(data[0], config.get("description"));
				if (result != null) {
					productDescription = getProductDescription(result);
					productDescription = FilterUtil.formatHtml(productDescription);
					productDescription = FilterUtil.removeSpace(productDescription);
				}
				result = evaluate(data[0], config.get("pictureUrl"));
				if (result != null) {
					productPictureUrl = getProductPictureUrl(result);				
					productPictureUrl = FilterUtil.replaceURLSpace(productPictureUrl);
				}
				result = evaluate(data[0], config.get("merchantUpdateDate"));
				if (result != null) {
					merchantUpdateDate = getProductMerchantUpdateDate(result);
					merchantUpdateDate = FilterUtil.formatHtml(merchantUpdateDate);
					merchantUpdateDate = FilterUtil.removeSpace(merchantUpdateDate);
				}
				result = evaluate(data[0], config.get("nameDescription"));
				if (result != null) {
					productNameDescription = getProductNameDescription(result);
					productNameDescription = FilterUtil.removeSpace(productNameDescription);
				}
							
				result = evaluate(data[0], config.get("realProductId"));
				if (result != null) {
					realProductId = getRealProductId(result);
					realProductId = FilterUtil.removeSpace(realProductId);
				}
				
				result = evaluate(data[0], config.get("basePrice"));
				if (result != null) {
					productBasePrice = getProductBasePrice(result);
					productBasePrice = FilterUtil.removeSpace(productBasePrice);
				}
				
				result = evaluate(data[0], config.get("upc"));
				if (result != null) {
					productUpc = getProductUpc(result);
					productUpc = FilterUtil.removeSpace(productUpc);
				}
				
				result = evaluate(data[0], config.get("data"));
				if (result != null) {
					data = getData(data);
					data = FilterUtil.removeSpace(data);
				}
				
				ProductDataBean[] productDataBean = new ProductDataBean[1];
				productDataBean[0] = new ProductDataBean();
				productDataBean[0].setName(StringUtils.defaultIfBlank(productName[0], ""));
				productDataBean[0].setPrice(FilterUtil.convertPriceStr(StringUtils.defaultIfBlank(productPrice[0], "")));
				productDataBean[0].setBasePrice(FilterUtil.convertPriceStr(StringUtils.defaultIfBlank(productBasePrice[0], "")));
				productDataBean[0].setDescription(StringUtils.defaultIfBlank(productDescription[0], ""));
				productDataBean[0].setPictureUrl(StringUtils.defaultIfBlank(productPictureUrl[0], ""));
				productDataBean[0].setRealProductId(StringUtils.defaultIfBlank(realProductId[0], ""));
				productDataBean[0].setUrl(StringUtils.defaultIfBlank(url, ""));
				return productDataBean;
			}
		}
		return null;
	}

	
		
}

