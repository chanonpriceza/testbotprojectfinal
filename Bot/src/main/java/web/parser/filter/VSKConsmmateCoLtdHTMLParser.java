package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;


import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class VSKConsmmateCoLtdHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class='clearfix col-lg-10 col-lg-offset-1 mb50 table-responsive' id='pro-table'>"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {
			
			String productNamecat = FilterUtil.getStringBetween(evaluateResult[0], "<h2 class=\"mb20\">","</h2>");
			productNamecat = FilterUtil.getStringAfter(productNamecat, "Product Category","");
			productNamecat = FilterUtil.removeHtmlTag(productNamecat);
			productNamecat = FilterUtil.toPlainTextString(productNamecat);
			productNamecat = productNamecat.replace("อื่นๆ", "");
			
			String productNamesub = FilterUtil.getStringBetween(evaluateResult[0], "class='pull-left'>","</h3>");
			productNamesub = FilterUtil.toPlainTextString(productNamesub);
			if(productNamesub.trim().length() > 0 ){
				productName = productNamecat+" "+productNamesub;
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		productPrice = BotUtil.CONTACT_PRICE_STR;		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if (evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<a class='fancybox' rel='product' title='' href='", "'>");
    	}
    	return new String[] {productPictureUrl};
	}
	    
}
