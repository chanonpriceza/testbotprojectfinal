package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;

public class WarrixHTMLParser extends BasicHTMLParser {

    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
    	mixIdName = true;
		map.put("name", new String[]{""});
		map.put("price", new String[]{""});
		map.put("basePrice", new String[]{""});
		map.put("expire", new String[]{""});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("realProductId", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:title]");
			productName = FilterUtil.toPlainTextString(e.attr("content"));		
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=product:price:amount]");
			productPrice = FilterUtil.removeCharNotPrice(e.attr("content"));	
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productPrice = "";    	
		if(evaluateResult.length == 1) {	
			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price-label\">ราคาปกติ</span>", "<span id=\"saleper\"");
			productPrice = FilterUtil.getStringBetween(productPrice, "<span class=\"price\">", "</span></span>");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);			
		}	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String real = "";
		if(evaluateResult.length == 1) {	
			real = FilterUtil.getStringBetween(evaluateResult[0], "\"sku\": \"", "\"");
//			real = FilterUtil.removeSpace(real);
		}
		return  new String[] {real};
	}

	@Override
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			product = FilterUtil.getStringBetween(evaluateResult[0], "<img class=\"img-responsive\" src=\"", "\"");
			product = FilterUtil.removeSpace(product);
		}
		return  new String[] {product};
	}

	@Override
	public String[] getProductDescription(String[] evaluateResult) {
		String product = "";
		if(evaluateResult.length == 1) {	
			Document doc = Jsoup.parse(evaluateResult[0]);
			Elements e = doc.select("meta[property=og:description]");
			product = FilterUtil.removeCharNotPrice(e.attr("content"));
		}
		return  new String[] {product};
	}
	
	
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		Document doc = Jsoup.parse(evaluateResult[0]);
		Elements e = doc.select("div.unavailable");
		boolean  x = (e.html().contains("ไม่มีสินค้าในสต็อก"));
		return new String[] {x+""};
	}
		
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

    	String[] productName = mpdBean.getProductName();
    	String[] productPrice = mpdBean.getProductPrice();
    	//String[] productUrl = mpdBean.getProductUrl();
        String[] productDescription = mpdBean.getProductDescription();
        String[] productPictureUrl = mpdBean.getProductPictureUrl();
        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
        String[] realProductId = mpdBean.getRealProductId();
        String currentUrl = mpdBean.getCurrentUrl();
        String[] productBasePrice = mpdBean.getProductBasePrice();
        String[] productUpc = mpdBean.getProductUpc();
        String tmp = "";

    	if (isEmpty(productName) || isEmpty(productPrice))
			return null;
    	
    	if(mixIdName&&realProductId!=null&&realProductId.length>0&&StringUtils.isNotBlank(realProductId[0]))
    		tmp = " ("+realProductId[0]+")";
    	
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]+tmp);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if(allowContractPrice&&rtn[0].getPrice()==0)
			rtn[0].setPrice(BotUtil.CONTACT_PRICE);

		if (isNotEmpty(productDescription))
			rtn[0].setDescription(productDescription[0]);
		
		if (isNotEmpty(productPictureUrl)) {
			String pictureUrl = productPictureUrl[0];
			if(isNotBlank(pictureUrl)) {				
				if(pictureUrl.startsWith("http")) {
					rtn[0].setPictureUrl(pictureUrl);					
				} else {
					try {
						URL url = new URL(new URL(currentUrl), pictureUrl);
						rtn[0].setPictureUrl(url.toString());
			    	} catch (MalformedURLException e) {}
				}
			}
		}

		if (isNotEmpty(merchantUpdateDate)) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null)
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
		}
		
		if (isNotEmpty(realProductId))
			rtn[0].setRealProductId(realProductId[0]);
		
		if (isNotEmpty(productBasePrice))
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
				
		if (isNotEmpty(productUpc))
			rtn[0].setUpc(productUpc[0]);
		
		currentUrl = currentUrl.replace("?isRealProductId=true", "");
		if(rtn!=null&&rtn.length>0&&StringUtils.isBlank(rtn[0].getUrl()))
			rtn[0].setUrl(currentUrl+"?utm_content="+realProductId[0]);
		
		
		return rtn;
	}
}