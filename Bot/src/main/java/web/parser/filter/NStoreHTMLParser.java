package web.parser.filter;

import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class NStoreHTMLParser extends DefaultHTMLParser {
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1 class=\"heading-title\" itemprop=\"name\">"});
		map.put("price", 		new String[]{""});
		map.put("basePrice", 	new String[]{""});
		map.put("description", 	new String[]{""});
		map.put("pictureUrl", 	new String[]{""});
		map.put("expire", 		new String[]{""});
		map.put("upc", 			new String[]{""});
		map.put("realProductId",new String[]{""});
		/*map.put("nameDescription", new String[]{""});*/
		
		return map;
	}
    
    public String[] getProductExpire(String[] evaluateResult) {
    	String stock = FilterUtil.getStringBetween(evaluateResult[0],"class=\"p-stock\">","</li"); 
    	if(evaluateResult.length == 1) {
        	if(stock.indexOf("มีสินค้า") == -1) {
        		return new String[]{"true"};
        	}	
    	}    	
        return new String[]{""};
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "class=\"product-price\" itemprop=\"price\">", "</li>");
    		if(productPrice.length() == 0){
    			productPrice = FilterUtil.getStringBetween(evaluateResult[0], "class=\"price-new\" itemprop=\"price\">", "</li>");
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {

		String productBasePrice = "";    	
    	if (evaluateResult.length == 1) {
    		productBasePrice = FilterUtil.getStringBetween(evaluateResult[0],"class=\"price-old\">","</li>");
    		productBasePrice = FilterUtil.toPlainTextString(productBasePrice);
    		productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
        }		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "class=\"p-authors\">", "</li>");
    		if(productDesc.length() > 0){
    			productDesc = FilterUtil.getStringAfter(productDesc, ":", productDesc);
    			productDesc = FilterUtil.getStringBefore(productDesc, "</", productDesc);
    			productDesc = FilterUtil.getStringBefore(productDesc, "<br", productDesc);
    			productDesc = FilterUtil.toPlainTextString(productDesc);
    			productDesc = "ผู้แต่ง/ผู้แปล : " + productDesc;
    		}else {
    			productDesc = FilterUtil.getStringBetween(evaluateResult[0], "class=\"tab-pane tab-content active\" id=\"tab-description\">", "</div>");
    			productDesc = FilterUtil.toPlainTextString(productDesc);	
			}
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "class=\"row product-info split-50-50\">", "</div>");
    		if(productPictureUrl.length() > 0){
    			productPictureUrl = FilterUtil.getStringBetween(productPictureUrl,"href=\"","\"");
    			try {
        			productPictureUrl = URLEncoder.encode(productPictureUrl, "UTF-8");
        			productPictureUrl = productPictureUrl.replace("%3A", ":");
        			productPictureUrl = productPictureUrl.replace("%2F", "/");
        			productPictureUrl = productPictureUrl.replace("+", "%20");
        		} catch (Exception e) {
    				// TODO: handle exception
    			}
    		}
    		
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductUpc(String[] evaluateResult) {
		String productUpc = "";
		if(evaluateResult.length > 0){
			productUpc = FilterUtil.getStringBetween(evaluateResult[0], "class=\"p-isbn\">", "</li>");
			if(productUpc.length() > 0){
    			productUpc = FilterUtil.getStringAfter(productUpc, ":", productUpc);
    			productUpc = FilterUtil.getStringBefore(productUpc, "</", productUpc);
    			productUpc = FilterUtil.getStringBefore(productUpc, "<br", productUpc);
    		}
    		productUpc = FilterUtil.toPlainTextString(productUpc);	
			
		}
		return new String[] {productUpc};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = "";    	
    	if (evaluateResult.length == 1) {
    		realProductId = FilterUtil.getStringBetween(evaluateResult[0],"type=\"hidden\" name=\"product_id\" value=\"","\"");
    		realProductId = FilterUtil.toPlainTextString(realProductId);
    		realProductId = FilterUtil.removeCharNotPrice(realProductId);
        }		
		return new String[] {realProductId};
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] productUpc = mpdBean.getProductUpc();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(productName[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		
		if (productDescription != null && productDescription.length != 0) {
			if(productDescription != null && productDescription[0].trim().length() > 0){
				rtn[0].setDescription(productDescription[0]);
				rtn[0].setKeyword(productDescription[0]);
			}
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					rtn[0].setPictureUrl(FilterUtil.mergeUrl(currentUrl, productPictureUrl[0]));					
				}
			}
		}

		// can remove if realProductId, basePrice and upc are not required
		if (realProductId != null && realProductId.length != 0) {
			rtn[0].setRealProductId(realProductId[0]);
		}
		if (productBasePrice != null && productBasePrice.length != 0) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}
		if (productUpc != null && productUpc.length != 0) {
			rtn[0].setUpc(productUpc[0]);
		}

		return rtn;
	}
	    
}