package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class WhitesootThailandHTMLParser extends DefaultHTMLParser {
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h2 itemprop=\"name\">"});
		map.put("price", new String[]{"<div class=\"prices\">"});
		map.put("basePrice", new String[]{"<div class=\"prices\">"});
		map.put("description", new String[]{"<div class=\"panel-group\" id=\"accordion\">"});
		map.put("pictureUrl", new String[]{"<div class=\"product-photo-container\">"});
		map.put("realProductId", new String[]{""});
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		
		String productPrice = "";
    	if (evaluateResult.length >= 1) {	
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"price on-sale\" itemprop=\"price\">", "</span>");
    		
    		if(StringUtils.isBlank(productPrice)){
    			productPrice = evaluateResult[0];
    		}
    		productPrice = FilterUtil.toPlainTextString(productPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }	
    	
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
    	if (evaluateResult.length >= 1) {	
    		bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"compare-price\">", "</span>");
    		bPrice = FilterUtil.toPlainTextString(bPrice);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }	
    	
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length >= 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);		
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = "";
		if(evaluateResult.length > 0){
			pdId = FilterUtil.getStringBetween(evaluateResult[0], "rid\":", "}");
			pdId =  FilterUtil.toPlainTextString(pdId);
		}
			return new String[] {pdId};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();
		String[] basprice = mpdBean.getProductBasePrice();
		
		if (StringUtils.isBlank(productName[0]) || productPrice == null || productPrice.length == 0 || realProductId == null || realProductId.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		String pdName = productName[0];
		rtn[0].setName(pdName + " ("+realProductId[0]+")");
		
		if (StringUtils.isNotBlank(basprice[0])) {
			rtn[0].setBasePrice( Double.parseDouble(basprice[0]));
			
		}
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		return rtn;
	}
	
}
