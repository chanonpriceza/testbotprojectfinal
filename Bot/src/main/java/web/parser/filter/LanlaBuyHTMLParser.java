package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class LanlaBuyHTMLParser extends DefaultHTMLParser {		
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<h1 class=\"product_title entry-title elementor-heading-title elementor-size-large\">"});
		map.put("price", 			new String[]{"<p class=\"price\">"});
		map.put("basePrice", 		new String[]{"<p class=\"price\">"});
		map.put("description", 		new String[]{"<div class=\"woocommerce-product-details__short-description\">"});
		map.put("pictureUrl", 		new String[]{""});
		map.put("realProductId", 	new String[]{"<div class=\"product_meta\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {		
		String productName = "";			
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.getStringBetween(evaluateResult[0], "<ins><span class=\"woocommerce-Price-amount amount\">", "</ins></p>");
    		productPrice = FilterUtil.getStringBetween(productPrice, "</span>", "</span>");
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductBasePrice(String[] evaluateResult) {
		
		String productBasePrice = "";    	
		if (evaluateResult.length > 0) {
			productBasePrice = FilterUtil.getStringBetween(evaluateResult[0], "<del><span class=\"woocommerce-Price-amount amount\">", "</del>");
			productBasePrice = FilterUtil.getStringBetween(productBasePrice, "</span>", "</span>");
			productBasePrice = FilterUtil.removeCharNotPrice(productBasePrice);
		}		
		return new String[] {productBasePrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
	   	String realProductId = "";
	   	
	   	if(evaluateResult.length >  0) {
	   		realProductId = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"sku\">", "</span>");
	   		realProductId = FilterUtil.toPlainTextString(realProductId);
	   	}
	   	
	    return new String[]{realProductId}; 
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		
		String[] productId = mpdBean.getRealProductId();
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productBasePrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		
		String replaceName = productName[0].toLowerCase()
				.replaceAll("trylagina", "Official trylagina")
				.replaceAll("ไตรลาจีน่า", "Official ไตรลาจีน่า")
				.replaceAll("ไตรลาจิน่า",  "Official ไตรลาจิน่า");
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName(replaceName + " ("+productId[0] + ")");
		rtn[0].setRealProductId(productId[0]);
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		rtn[0].setUrl(currentUrl);
		if(StringUtils.isNotBlank(productBasePrice[0])){
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
		}

		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}
		if (productPictureUrl != null && productPictureUrl.length != 0) {
			rtn[0].setPictureUrl(productPictureUrl[0]);
		}
		
		return rtn;
	}
}
	    