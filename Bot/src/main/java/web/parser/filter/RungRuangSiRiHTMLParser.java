package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class RungRuangSiRiHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<title>"});
		map.put("price", new String[]{"<textarea name=\"product\" style=\"display: none !important;\" hidden>"});
		map.put("description", new String[]{"<div class=\"ecom-product-long-description\" itemprop=\"description\">"});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {
				productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
		if (evaluateResult.length > 0) {
			productPrice = FilterUtil.getStringBetween(evaluateResult[0],"[{\"price\":\"","\"");
			productPrice = FilterUtil.removeCharNotPrice(productPrice);
			
			if("".equals(productPrice) || "0.0".equals(productPrice) || "0".equals(productPrice)) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
		}
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}



}
