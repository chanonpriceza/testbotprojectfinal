package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TPQToolHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"text-detail-head\" style=\"float:right; padding-left:35px;\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<div class=\"text-detail-head\" style=\"float:right; padding-left:35px;\">"});
		map.put("pictureUrl", new String[]{"<div class=\"column_center_right\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<strong>ชื่อสินค้า : </strong>", "</p>");
			productName = productName.replaceAll("&quot;", "\"");
			productName = FilterUtil.toPlainTextString(productName);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "9999999";
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
		if(evaluateResult.length == 1) {
			productDesc = FilterUtil.getStringAfterLastIndex(evaluateResult[0], "<p>", "");
			productDesc = FilterUtil.getStringBefore(productDesc, "</p>", "");
			productDesc = FilterUtil.toPlainTextString(productDesc);    	
		}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");

        }
    	return new String[] {productPictureUrl};
	}
}
