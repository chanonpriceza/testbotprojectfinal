package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class KiehlsHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1>"});
		map.put("price", new String[]{"<span class=\"price\">"});
		map.put("description", new String[]{"<div class=\"short-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"six columns product-img-box\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
				
		if(evaluateResult.length > 0) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
			productName = "Kiehl's " + productName;
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";    
		
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.removeCharNotPrice(evaluateResult[0]);
        }
    	
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	
    	if(evaluateResult.length > 0) {
     		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = "";
		
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	
    	return new String[] {productPictureUrl};
	}
	
}