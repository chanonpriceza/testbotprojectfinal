package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class DtacStoreHTMLParser extends DefaultHTMLParser{
		
	// priceza catId, mapping keyword , priceza keyword
//	private static final String[][] CAT_ID_KEYWORD_MAPPING = {
//			{"20101", "iPhone", "Dtac ดีแทค"},
//			{"10102", "iPad", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy S Duos", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Win", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Grand", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Core", "Dtac ดีแทค"},
//			{"110613", "Samsung Galaxy Gear", "นาฬิกา SmartWatch Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy S4", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy S5", "Dtac ดีแทค"},
//			{"20101", "Samsung Note 8", "Dtac ดีแทค"},
//			{"10102", "Samsung Note 10.1", "Dtac ดีแทค"},
//			{"20101", "Samsung Note", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Note 3 Neo Duos", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Note 3 LTE", "Dtac ดีแทค"},
//			{"20101", "Samsung Galaxy Note 3", "Dtac ดีแทค"},
//			{"20101", "samsung galaxy mega", "Dtac ดีแทค"},
//			{"20101", "Lumia", "Dtac ดีแทค"},
//			{"20101", "Asha", "Dtac ดีแทค"},
//			{"10102", "Samsung Galaxy Tab", "Dtac ดีแทค"},
//			{"10102", "Samsung Galaxy Tab S 8.4", "Dtac ดีแทค"},
//			{"10102", "Samsung Galaxy Tab 3 7.0 Lite (3G)", "Dtac ดีแทค"},
//			{"20106", "Desire", "Dtac ดีแทค"},
//			{"20106", "Smartphone", "Dtac ดีแทค"},
//			{"20106", "Mobilephone", "Dtac ดีแทค"},
//			{"20106", "Phone ", "Dtac ดีแทค"},
//			{"20106", "Dual sim", "Dtac ดีแทค"},
//			{"20101", "HTC", "Dtac ดีแทค"},
//			{"20106", "TriNet Phone", "Dtac ดีแทค"},
//			{"20101", "LG", "Dtac ดีแทค"},
//			{"20101", "Nokia", "Dtac ดีแทค"},
//			{"20101", "Oppo ", "Dtac ดีแทค"},
//			{"10102", "Tab", "Dtac ดีแทค"},
//			{"10102", "Tablet", "Dtac ดีแทค"}
//	};
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 		new String[]{"<div class=\"product-info-main\">","<span class=\"base\" data-ui-id=\"page-title-wrapper\" itemprop=\"name\">"});
		map.put("price", 		new String[]{""});
		map.put("description", 	new String[]{"<div class=\"additional-attributes-wrapper table-wrapper\">"});
		map.put("pictureUrl", 	new String[]{""});
		map.put("url", 	new String[]{""});
		map.put("realProductId", 	new String[]{""});
		
		
//		map.put("expire", 		new String[]{"<div id=\"device-gall-container\" class=\"swiper-container\">","<div class=\"pic-iphone\">"});
		
		return map;
	}
    @Override
    public String[] getProductName(String[] evaluateResult) {
    	List<String> productName = new ArrayList<String>();
    	if(evaluateResult.length > 0) {
	    	String mainName = FilterUtil.getStringBetween(evaluateResult[0],"<span class=\"base\" data-ui-id=\"page-title-wrapper\" itemprop=\"name\">","</span>");
	    	if(mainName.equals("")) {
	    		mainName = FilterUtil.getStringBetween(evaluateResult[1],"<span class=\"base\" data-ui-id=\"page-title-wrapper\" itemprop=\"name\">","</span>");  
	    		productName.add(mainName);
	    	}else {
	    		List<String> suffixName = FilterUtil.getAllStringBetween(evaluateResult[0],"<div class=\"landing-item-details\">","</a>");
	    		for(int i=0;i<suffixName.size();i++){
	    			String suffixStromg = FilterUtil.toPlainTextString(suffixName.get(i));
	    			if(StringUtils.isNotBlank(suffixStromg) && !suffixStromg.contains("ผ่อนชำระ 0% สูงสุด 24")) {
	    				productName.add( mainName+"("+suffixStromg+")");    			
	    			}
	    		}    		
	    	}
    	}
    	return productName.toArray(new String[0]);
    }
    public String[] getProductPrice(String[] evaluateResult) {
    	for(int i = 0;i<evaluateResult.length;i++){
    		String dummy = FilterUtil.getStringBetween(evaluateResult[i],"\"finalPrice\" class=\"price-wrapper \"><span class=\"price\">","</span></span>");
    		dummy = FilterUtil.toPlainTextString(dummy);
    		dummy = FilterUtil.removeCharNotPrice(dummy);
    		if(dummy.equals("")){
    			dummy = BotUtil.CONTACT_PRICE_STR;
    		}
    		evaluateResult[i] = dummy;
    	}
    	return evaluateResult;
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);
    	}
    	return new String[] {productDesc};
	}
	
	@Override
	public String[] getProductUrl(String[] evaluateResult) {
		List<String> urls = new ArrayList<String>();
	 	if(evaluateResult.length == 1) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("div.landing-detail,div.landing-content-segment");
    		e = e.select("a");
    		for (Element element : e) {
				urls.add(element.attr("href"));
			}
    	}
		return urls.stream().toArray(String[]::new);
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		List<String> ids = new ArrayList<String>();
	 	if(evaluateResult.length == 1) {
    		Document doc = Jsoup.parse(evaluateResult[0]);
    		Elements e = doc.select("div.price-box");
    		for (Element element : e) {
				ids.add(element.attr("data-product-id"));
    		}
	 	}
    	return ids.stream().toArray(String[]::new);
	}
	
	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String[] productRealProductId = mpdBean.getRealProductId();
		String currentUrl = mpdBean.getCurrentUrl();

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[productName.length];
		for (int i = 0; i < productName.length; i++) {
			rtn[i] = new ProductDataBean();
			rtn[i].setName(productName[i] + "("+productRealProductId[i]+") "+"(ย้ายค่ายเบอร์เดิม)");
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));
//			rtn[i].setBasePrice(FilterUtil.convertPriceStr(productPrice[i]));
			if (productDescription != null && productDescription.length != 0) {
				rtn[i].setDescription(productDescription[0]);
			}
//			String productUrl = mpdBean.getProductUrl()[i];
//			if(!productUrl.startsWith("http")) {
//				try {
//					URL url = new URL(new URL(currentUrl), productUrl);
//					rtn[i].setUrl(url.toString());
//				} catch (MalformedURLException e) {
//					
//				}
//			}else
//				rtn[i].setUrl(mpdBean.getProductUrl()[i]);
				
			rtn[i].setRealProductId(mpdBean.getRealProductId()[i]);
			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[i].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
							rtn[i].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}

			//map cat and keyword			
//			String keyword = FilterUtil.getStringAfterLastIndex(currentUrl, "/", "");
//			if (keyword.length() != 0) {
//				String[] matchCatId = mapCategory(keyword, CAT_ID_KEYWORD_MAPPING);
//				if (matchCatId != null) {
//					rtn[i].setKeyword(matchCatId[2]);
//					rtn[i].setCategoryId(BotUtil.stringToInt(matchCatId[0], 0));
//				} else {
//					rtn[i].setCategoryId(20101);
//				}
//			}
		}
		return rtn;
	}

}