package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AmornOnlineHTMLParser extends DefaultHTMLParser{
			
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
    	//require
		map.put("name", 		new String[]{"<h1 itemprop=\"name\">"});
		map.put("price", 		new String[]{"<span class=\"price\" itemprop=\"price\">"});
		map.put("description", 	new String[]{"<div class=\"short-description\" id=\"short_description\">"});
		map.put("pictureUrl", 	new String[]{""});
		map.put("realProductId", new String[] {"<span id=\"product_code\" class=\"product-code\">"});
		map.put("expire", 		new String[]{"<p class=\"availability out-of-stock\">"});
		
		return map;
	}
    
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";				
		if(evaluateResult.length > 0) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length > 0) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";    	
    	if(evaluateResult.length > 0) {
    		productDesc = FilterUtil.getStringAfter(evaluateResult[0], "รายละเอียดสินค้า", evaluateResult[0]);
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}

	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String realProductId = null;
		if(evaluateResult.length > 0) {
			realProductId = FilterUtil.getStringBetween(evaluateResult[0], "รหัสสินค้า:", "<");
		}
		return new String[] {realProductId};
	}

	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		if(evaluateResult.length > 0) {
			return new String[] {"true"};
		}
		return new String[]{""};
	}
	
	
}