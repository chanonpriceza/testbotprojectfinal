package web.parser.filter;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;
public class WtCommerceHTMLParser extends DefaultHTMLParser {

	public Map<String, String[]> getConfiguration() {
		Map<String, String[]> map = new Hashtable<String, String[]>();

		map.put("name", new String[] { "<td width=\"50%\" align=\"center\" valign='top'>" });
		map.put("price", new String[] { "<td width=\"50%\" align=\"center\" valign='top'>" });
		map.put("pictureUrl", new String[] { "<td width=\"50%\" align=\"center\" valign='top'>" });
		return map;
	}
	
	public String getCharset() {
	return "windows-874";
}

	public String[] getProductName(String[] evaluateResult) {

		List<String> productName = new ArrayList<>();
		if (evaluateResult.length > 0) {
			for (int i = 0; i < evaluateResult.length; i++) {
				String tmp = FilterUtil.getStringBetween(evaluateResult[i], "Name:", "</a>");
				tmp = FilterUtil.getStringAfter(tmp, ">", "");
				productName.add(tmp);
			}
		}
		return productName.toArray(new String[0]);
	}

	public String[] getProductPrice(String[] evaluateResult) {
		List<String> productPrice = new ArrayList<>();
		for (int i = 0; i < evaluateResult.length; i++) {
			if (evaluateResult.length > 0) {
				productPrice.add(BotUtil.CONTACT_PRICE + "");
			}
		}
		return productPrice.toArray(new String[0]);
	}

	public String[] getProductPictureUrl(String[] evaluateResult) {

		List<String> productPictureUrl = new ArrayList<>();
		if (evaluateResult.length > 0) {
			for (int i = 0; i < evaluateResult.length; i++) {
				String tmp = FilterUtil.getStringBetween(evaluateResult[i], "<td valign=\"top\">",
						"<td valign=\"top\">");
				tmp = FilterUtil.getStringBetween(tmp, "src=", " ");
				tmp = FilterUtil.getStringBetween(tmp, "\"", "\"");
				productPictureUrl.add(tmp);
			}
		}
		return productPictureUrl.toArray(new String[0]);
	}

	@Override
	public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {
		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();

		if (productName == null || productName.length == 0) {
			return null;
		}

		ProductDataBean[] rtn = new ProductDataBean[productName.length];

		for (int i = 0; i < productName.length; i++) {
			rtn[i] = new ProductDataBean();
			if (productName[i].isEmpty()) {
				rtn[i].setExpire(true);
				continue;
			}

			rtn[i].setName(productName[i]);
			rtn[i].setPrice(FilterUtil.convertPriceStr(productPrice[i]));

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					rtn[i].setPictureUrl(productPictureUrl[i]);
				}
			}
		}
		return rtn;
	}

}
