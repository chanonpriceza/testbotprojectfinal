package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class RayShoppingHTMLParser extends DefaultHTMLParser {
	
	@Override
	public String getCharset() {
		return "utf-8";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<span itemprop=\"name\">"});
		map.put("price", new String[]{""});
		map.put("description", new String[]{"<form name=\"frmProduct\" method=\"post\" action=\"\">"});
		map.put("pictureUrl", new String[]{""});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";
		if(evaluateResult.length == 1) {
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {	
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "<input type=\"hidden\" name=\"p_price\" value=\"", "\"");
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<td style=\"text-align:center;\">			<table style=\"width:98%; margin:auto;\" cellpadding=\"0\" cellspacing=\"0\">", "</tr>");
    		productDesc = FilterUtil.toPlainTextString(productDesc);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<img  itemprop=\"image\"  src=\"", "\"");    		
        }
    	return new String[] {productPictureUrl};
	}
	    
}