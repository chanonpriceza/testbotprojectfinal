package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;

import utils.FilterUtil;

public class ChulabookHTMLParser extends DefaultHTMLParser{
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<table width=\"95%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">"});
		map.put("price", new String[]{"<table width=\"95%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";
		if(evaluateResult.length > 0) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "<span class=\"big-text\">", "</span>");
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		String productPrice = "";
    	if (evaluateResult.length > 0) {
    		String rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคาพิเศษ", "บาท");
    		productPrice = FilterUtil.toPlainTextString(rawPrice);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		if(productPrice.trim().length() == 0){
    			rawPrice = FilterUtil.getStringBetween(evaluateResult[0], "ราคาปก", "บาท");
        		productPrice = FilterUtil.toPlainTextString(rawPrice);
        		productPrice = FilterUtil.removeCharNotPrice(productPrice);
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
		String author = "";
    	if(evaluateResult.length > 0) {
    		String rawDesc = FilterUtil.getStringBetween(evaluateResult[0], "รายละเอียดสินค้า", "<table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"5\">");
    		rawDesc = FilterUtil.toPlainTextString(rawDesc);
    		if(rawDesc.contains("- - - - - ไม่มีรายละเอียดสินค้า - - - - -")){
    			rawDesc = "";
    		}
    		if(rawDesc.trim().length() == 0){
    			rawDesc = FilterUtil.getStringBetween(evaluateResult[0], "Description", "<table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"5\">");
        		rawDesc = FilterUtil.toPlainTextString(rawDesc);
    		}
    		if(rawDesc.contains("- - - - - No Description - - - - -")){
    			rawDesc = "";
    		}
    		author = FilterUtil.getStringBetween(evaluateResult[0], "<td><div align=\"left\"><span class=\"blacktext\">ผู้แต่ง/ผู้แปล", "</span>");
    		author = FilterUtil.toPlainTextString(author);
    		author = author.replace(":", "");
    		if(rawDesc.trim().length() != 0 && author.trim().length() != 0){
    			productDesc = "ผู้แต่ง/ผู้แปล: "+author.trim()+", รายละเอียด: "+rawDesc.trim();
    		}else if(rawDesc.trim().length() != 0 && author.trim().length() == 0){
    			productDesc = "รายละเอียด: "+rawDesc;
    			author = "";
    		}else if(author.trim().length() != 0 && rawDesc.trim().length() == 0){
    			productDesc = "ผู้แต่ง/ผู้แปล: "+author;
    		}else{
    			productDesc = "";
    			author = "";
    		}
    		productDesc = productDesc.replace(", ,", ",");
    	}
    	return new String[] {productDesc,author};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		String rawPicture = FilterUtil.getStringBetween(evaluateResult[0], "<td width=\"26%\" rowspan=\"2\" valign=\"top\" ><table border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">", "</table>");
    		productPictureUrl = FilterUtil.getStringBetween(rawPicture, "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	/**
	 * default mergeProductData
	 * 
	 * 
	 */
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate();

		if (productName == null || productName.length == 0
				|| productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		String realPID = FilterUtil.getStringAfter(currentUrl, "?barcode=", "");
		if(realPID.length() == 0) {
			return null;
		}
		realPID = realPID.trim();
		rtn[0].setName(productName[0] + " (ISBN:"+ realPID +")");
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));
		//Real product ID
		rtn[0].setRealProductId(realPID);
		rtn[0].setUpc(realPID);
		rtn[0].setKeyword(productDescription[1]);
		
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}


		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null
					&& productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl),
								productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
		
		if (merchantUpdateDate != null && merchantUpdateDate.length != 0) {
			Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
			if (date != null) {
				rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
		}
		
		return rtn;
	}
}