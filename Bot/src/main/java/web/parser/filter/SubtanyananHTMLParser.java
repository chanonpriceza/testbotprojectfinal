package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;


import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class SubtanyananHTMLParser extends DefaultHTMLParser {	
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<div class=\"col-sm-6 item-product-detail\">"});
		map.put("price", new String[]{"<div class=\"sale-price\">"});
		map.put("description", new String[]{"<div class=\"item-description\">"});
		map.put("pictureUrl", new String[]{"<div class=\"col-sm-6 item-product-photo\">"});
		map.put("basePrice",new String[]{"<div class=\"regular-price\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length >= 1) {			
			productName = FilterUtil.getStringBetween(evaluateResult[0],"<div class=\"title\">","</div>");
			productName = FilterUtil.toPlainTextString(productName);			
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";
		if (evaluateResult.length == 1) {
			
			String tmp = FilterUtil.toPlainTextString(evaluateResult[0]);

			productPrice = FilterUtil.removeCharNotPrice(tmp);
			
			if("0.00".equals(productPrice)) {
				productPrice = BotUtil.CONTACT_PRICE_STR;
			}
		}

		return new String[] { productPrice };
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
    	}
    	return new String[] {productDesc};
	}
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String base = "";
		if(evaluateResult.length > 0) {
			String tmp = FilterUtil.toPlainTextString(evaluateResult[0]);
			base = FilterUtil.removeCharNotPrice(tmp);
		}
		
		return new String[]{base};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}