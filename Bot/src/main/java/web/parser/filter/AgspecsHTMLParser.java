package web.parser.filter;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.FilterUtil;
import web.parser.DefaultHTMLParser;

public class AgspecsHTMLParser extends DefaultHTMLParser {
	
	 public Map<String, String[]> getConfiguration() {
	    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
			
			map.put("name", new String[]{""});
			map.put("price", new String[]{""});
			map.put("basePrice", new String[]{""});
			map.put("description", new String[]{""});
			map.put("pictureUrl", new String[]{""});
			map.put("realProductId", new String[]{""});
			map.put("expire", new String[]{""});
			
			return map;
		}
		
		public String[] getProductName(String[] evaluateResult) {
			
			String productName = "";   
			String productBrand = "";
			String productCat = "";
					
			if(evaluateResult.length == 1) {	
				Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("h1.product_title");
				productName = FilterUtil.toPlainTextString(e.html());	
				productName = StringEscapeUtils.unescapeHtml4(productName);
			}
			
			if(evaluateResult.length == 1) {	
				Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("div.wb-posted_in");
				productBrand = FilterUtil.toPlainTextString(e.html().replace("แบรนด์:: ", ""));	
				productBrand = StringEscapeUtils.unescapeHtml4(productBrand);
			}
			
			if(evaluateResult.length == 1) {	
				Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("tr.posted_in");
				e = e.select("td.value");
				productCat = FilterUtil.toPlainTextString(e.html());	
				productCat = StringEscapeUtils.unescapeHtml4(productCat);
			}
			
			return new String[] {productName,productBrand,productCat};
		}
		
		public String[] getProductPrice(String[] evaluateResult) {
			
			String productPrice = "";
	    	if (evaluateResult.length >= 1) {	
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("p.price");
				e = e.select("ins");
				if(StringUtils.isNotBlank(e.html())){
					productPrice = FilterUtil.removeCharNotPrice(e.html());	
					return new String[] {productPrice};
				}
				
				e  = doc.select("p.price");
				e = e.select("span.woocommerce-Price-amount");
				if(e!=null&&e.size()>0) {
					productPrice = FilterUtil.removeCharNotPrice(e.get(0).html());	
				}
	        }	
			return new String[] {productPrice};
		}
		
		@Override
		public String[] getProductBasePrice(String[] evaluateResult) {
			String bPrice = "";
	    	if (evaluateResult.length >= 1) {	
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("p.price");
				e = e.select("del");
				bPrice = FilterUtil.removeCharNotPrice(e.html());	
	        }	
			return new String[] {bPrice};
		}
		
		public String[] getProductDescription(String[] evaluateResult) {
			
			String productDesc = "";
	    	if(evaluateResult.length == 1) {
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("div.description");
				productDesc = e.html();
	    	}
	    	
	    	return new String[] {productDesc};
		}
		
		public String[] getProductPictureUrl(String[] evaluateResult) {
			
			String productPictureUrl = null;
	    	if (evaluateResult.length == 1) {
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("div.product-images img");
				productPictureUrl = FilterUtil.toPlainTextString(e.attr("src"));
				if(StringUtils.isBlank(productPictureUrl)) {
					e  = doc.select("div.product-images a");
					productPictureUrl = FilterUtil.toPlainTextString(e.attr("href"));
				}
	        }
	    	
	    	return new String[] {productPictureUrl};
		}
		
		@Override
		public String[] getRealProductId(String[] evaluateResult) {
			
			String productId = "";
	    	if(evaluateResult.length == 1) {
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
				Elements e  = doc.select("input[name=product_id]");
				productId = e.attr("value");
	    	}
	    	
	    	return new String[] {productId};
		}
		
		@Override
		public String[] getProductExpire(String[] evaluateResult) {
			String expire = "";
	    	if(evaluateResult.length == 1) {
	    		Document doc  = Jsoup.parse(evaluateResult[0]);
	    		Elements e  = doc.select("woocommerce-product-attributes");
	    		e  = doc.select("form.isw-swatches");
				expire = e.attr("data-product_variations");
				if(expire.contains("\\u0e2a\\u0e34\\u0e19\\u0e04\\u0e49\\u0e32\\u0e2b\\u0e21\\u0e14\\u0e41\\u0e25\\u0e49\\u0e27")) {
					expire =  "true";
				}else {
					expire = "";
				}
				
	    	}
	    	return new String[] {expire};
		}
		
		public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

	    	String[] productName = mpdBean.getProductName();
	    	String[] productPrice = mpdBean.getProductPrice();
	    	//String[] productUrl = mpdBean.getProductUrl();
	        String[] productDescription = mpdBean.getProductDescription();
	        String[] productPictureUrl = mpdBean.getProductPictureUrl();
	        String[] merchantUpdateDate = mpdBean.getMerchantUpdateDate(); 
	        String[] realProductId = mpdBean.getRealProductId();
	        String currentUrl = mpdBean.getCurrentUrl();
	        String[] productBasePrice = mpdBean.getProductBasePrice();
	        String[] productUpc = mpdBean.getProductUpc();

	    	if (isEmpty(productName) || isEmpty(productPrice))
				return null;
	    	
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(productName[2]+" "+productName[1]+" "+productName[0]+" ("+realProductId[0]+")");
			rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

			if (isNotEmpty(productDescription))
				rtn[0].setDescription(productDescription[0]);
			
			if (isNotEmpty(productPictureUrl)) {
				String pictureUrl = productPictureUrl[0];
				if(isNotBlank(pictureUrl)) {				
					if(pictureUrl.startsWith("http")) {
						rtn[0].setPictureUrl(pictureUrl);					
					} else {
						try {
							URL url = new URL(new URL(currentUrl), pictureUrl);
							rtn[0].setPictureUrl(url.toString());
				    	} catch (MalformedURLException e) {}
					}
				}
			}

			if (isNotEmpty(merchantUpdateDate)) {
				Date date = convertMerchantUpdateDate(merchantUpdateDate[0]);
				if (date != null)
					rtn[0].setMerchantUpdateDate(new Timestamp(date.getTime()));
			}
			
			if (isNotEmpty(realProductId))
				rtn[0].setRealProductId(realProductId[0]);
			
			if (isNotEmpty(productBasePrice))
				rtn[0].setBasePrice(FilterUtil.convertPriceStr(productBasePrice[0]));
					
			if (isNotEmpty(productUpc))
				rtn[0].setUpc(productUpc[0]);
			
			return rtn;
		}
		

}
