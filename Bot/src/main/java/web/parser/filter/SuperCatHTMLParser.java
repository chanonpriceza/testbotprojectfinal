package web.parser.filter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

@SuppressWarnings("deprecation")
public class SuperCatHTMLParser extends DefaultHTMLParser {
	
	private static final Logger logger = LogManager.getRootLogger();
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<h1 class=\"h2 productName\">"});
		map.put("price", 			new String[]{""});
		map.put("nameDescription", 	new String[]{""});
		map.put("basePrice", 		new String[]{""});
		map.put("description", 		new String[]{"<div class=\"divProductDescription\">"});
		map.put("pictureUrl", 		new String[]{"<div class=\"gall-item-container\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		if(evaluateResult.length == 1) {	
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);
		}
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {
		return new String[] {""};
	}
	
	public String[] getProductNameDescription(String[] evaluateResult) {
		String productNameDesc = "";
    	if(evaluateResult.length > 0) {
    		productNameDesc = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"form-group\"><div class=\"marginInner\">", "\"");
    		productNameDesc = FilterUtil.getStringBetween(productNameDesc, "<p>", "</p>");
    	}
		return new String[] {productNameDesc};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		return new String[] {""};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    		productDesc = FilterUtil.getStringAfter(productDesc, "รายละเอียดสินค้า", productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		String productPictureUrl = null;
    	if (evaluateResult.length > 0) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "href=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productDesc = mpdBean.getProductDescription();
		String[] productNameDesc = mpdBean.getProductNameDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		
		String productId = FilterUtil.getStringBetween(currentUrl, "/product/", "/");
		
		if (productName == null || productName.length == 0 || StringUtils.isBlank(productId)) {
			return null;
		}
		
		String price = "";
		String basePrice = "";
		String priceSourceCode = "";
		
		try {
			priceSourceCode = sendPost("https://www.supercatthailand.com/page/page_product_item/ajaxPageProductItemController.php", productId);
		} catch (Exception e) {
			logger.error(e);
		}
		
		if(StringUtils.isNotBlank(priceSourceCode)) {
				price = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringBetween(priceSourceCode, 
						"<span class=\\\"h4\\\">", "<\\/span>")));
				basePrice = FilterUtil.removeCharNotPrice(FilterUtil.toPlainTextString(FilterUtil.getStringBetween(priceSourceCode, 
						"<span class=\\\"productOldPrice h5\\\">", "<\\/span>")));
		}
		if(StringUtils.isBlank(price)) {
			return null;
		}
		
		if(StringUtils.isBlank(basePrice)) {
			basePrice = "0";
		}
		
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		rtn[0].setName("SUPERCAT "+productName[0] +productNameDesc[0] + " ("+productId+")");
		rtn[0].setPrice(Double.parseDouble(price));
		rtn[0].setBasePrice(Double.parseDouble(basePrice));

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (StringUtils.isNotBlank(productPictureUrl[0])) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {}
				}
			}
		}

		if (productDesc != null && productDesc.length != 0){
			rtn[0].setDescription(productDesc[0]);
		}
		
		return rtn;
	}
	
	@SuppressWarnings({ "resource" })
	private static String sendPost(String url, String productId) throws Exception {

		url = url.replace("https:", "http:");
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36");
//		post.setHeader("Cookie","GA1.2.191377705.1528343838; __zlcmid=mrhPLH3XrDgC3G; PHPSESSID=31a67e3780978d4949d1be2fc5fb1925; pid="+productId+"-33693; _gid=GA1.2.113300220.1529467686");
//		post.setHeader("Cookie","GA1.2.191377705.1528343838; __zlcmid=mrhPLH3XrDgC3G; _gid=GA1.2.113300220.1529467686; PHPSESSID=f443078202435d00f8412fbaa46a265f; pid="+productId+"-33693-33694-57635");
//		post.setHeader("Cookie","GA1.2.191377705.1528343838; __zlcmid=mrhPLH3XrDgC3G; PHPSESSID=0c1606f626e2b8ac51c7ee197df5e6d0; _gid=GA1.2.256458952.1534847751; pid="+productId+"");
		post.setHeader("Cookie","GA1.2.446971875.1542794513; PHPSESSID=f26496a31ee0d851c38b200025f3b173; _gid=GA1.2.1054225955.1542794513; pid="+productId);
		post.setHeader("Referer", URLEncoder.encode(url, "UTF-8"));
		post.setHeader("","");
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("productID", productId));
		urlParameters.add(new BasicNameValuePair("attributeSet", "[\"0\"]"));
		urlParameters.add(new BasicNameValuePair("type", "getAttributeGroupID"));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		

		HttpResponse response = client.execute(post);
//		System.out.println();
//		System.out.println("Sending 'POST' request to URL : " + url);
//		System.out.println("Post parameters : " + post.getEntity());
//		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
//		System.out.println();

		BufferedReader rd = new BufferedReader( new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();

	}
}