package web.parser.filter;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import engine.BaseConfig;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;
import utils.BotUtil;
public class TemplateIGetWebIncludeIdHTMLParser extends DefaultHTMLParser{
		
	public String getCharset() {
		return "TIS-620";
	}
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{"<h1 class=\"entry-full-title prodtc-detail-title\">", "<li id=\"product-id-content\" class=\"clearfix\">", "<li id=\"product-id-content\">"});
		map.put("price", new String[]{"<span class=\"num\" id=\"product_price\">", "<span class=\"num\" id=\"product_price\" style=\"margin:0; padding:0;\">"});
		map.put("description", new String[]{"<div class=\"clear_desc clearfix\">"});
		map.put("pictureUrl", new String[]{"<div  id=\"photoshow\">", "<div id=\"photoshow\">", "<div  id=\"photoshow\" class=\"third\">", "<div id=\"photoshow\" class=\"third\">", "<div id=\"photoshow_2\">"});
		
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
		
		if(evaluateResult.length == 1) {			
			productName = FilterUtil.toPlainTextString(evaluateResult[0]);			
		} else if(evaluateResult.length == 2){
			String name = FilterUtil.toPlainTextString(evaluateResult[0]);	
			String pId = FilterUtil.getStringBetween(evaluateResult[1], "<p class=\"detail_product\">", "</p>");
			if(pId.trim().length() == 0) {
				pId = FilterUtil.getStringBetween(evaluateResult[1], "<p class=\"detail_product\" style=\"margin-bottom: 0;\">", "</p>");				
			}
			pId = FilterUtil.toPlainTextString(pId);	
			if(name.trim().length() != 0) {
				if(pId.trim().length() != 0) {
					productName = name.trim() + " " + pId.trim();
				} else {
					productName = name.trim();
				}				
			}	
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		   		
    		String tmp = FilterUtil.toPlainTextString(evaluateResult[0]); 
    		
    		if(tmp.contains("สอบถาม")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		} else {
    			tmp = FilterUtil.getStringAfter(tmp, "ลดเหลือ", tmp);    		
        		productPrice = FilterUtil.removeCharNotPrice(tmp);
    		}    		
    		
    		double p = FilterUtil.convertPriceStr(productPrice);
    		if(p==0&&StringUtils.isNotBlank(BaseConfig.CONF_FEED_CRAWLER_PARAM)&&BaseConfig.CONF_FEED_CRAWLER_PARAM.equals("contractPrice")) {
    			productPrice = BotUtil.CONTACT_PRICE_STR;
    		}
        }		
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "src=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	    
}
