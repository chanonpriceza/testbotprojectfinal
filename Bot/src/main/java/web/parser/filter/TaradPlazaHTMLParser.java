package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.DefaultHTMLParser;
import utils.FilterUtil;

public class TaradPlazaHTMLParser extends DefaultHTMLParser{
	
	 public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", 			new String[]{"<div class=\"box-detail-area\" id=\"purchase\">"});
		map.put("price", 			new String[]{""});
		map.put("description", 		new String[]{"<div class=\"box-content-detail\">"});
		map.put("pictureUrl", 		new String[]{""});
		map.put("realProductId", 	new String[]{""});
		map.put("basePrice", 		new String[]{"<div class=\"box-detail-area\" id=\"purchase\">"});
		
		map.put("expire", 			new String[]{""});	
		
		return map;
	}
	 
	public String[] getProductExpire(String[] evaluateResult) {
			
	   	if(evaluateResult.length == 1) {
	   		String name = "";
	   		String shop = "";
	   		String quantity = "";
	   		name = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"box-detail-area\" id=\"purchase\">", "</div>");
	   		shop = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"shop-name\">", "</div>");
	   		quantity = FilterUtil.getStringBetween(evaluateResult[0], "<div class=\"quantity\">", "</div>");
	   		
	       	if(
	       		(name.toLowerCase().indexOf("sony") != -1 && shop.toLowerCase().indexOf("www.ledtvthailand.com") != -1) || 
	       	    (name.toLowerCase().indexOf("sony") != -1 && shop.toLowerCase().indexOf("เอวี ดิจิตอล เซ็นเตอร์") != -1) || 
	       	    (shop.toLowerCase().indexOf("สะพานบุญสังฆภัณฑ์") != -1 ) ||
	       	    (quantity.contains("สินค้าหมด"))
	       	   ){
	    		return new String[]{"true"};
	    	}     	
		}    	
	    return new String[]{""};
	}
	
	public String[] getProductName(String[] evaluateResult) {
		
		String productName = "";   
				
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "\">", "</h1>");
			productName = FilterUtil.toPlainTextString(productName);
			if(productName.contains("ระเบิดเวลา") && productName.contains("นาฬิกา")){
				productName = "";
			}
		}
		
		return new String[] {productName};
	}
	
	public String[] getProductPrice(String[] evaluateResult) {

		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		String tmp = FilterUtil.getStringBetween(evaluateResult[0], "data-product-net-price=\"", "\"");
    		if(tmp.length() == 0) {
    			tmp = BotUtil.CONTACT_PRICE_STR;
    		}
    		productPrice = FilterUtil.toPlainTextString(tmp);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }		
		return new String[] {productPrice};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";
		if (evaluateResult.length == 1) {
			bPrice = FilterUtil.getStringBetween(evaluateResult[0], "<del>", "</del>");
			bPrice = FilterUtil.toPlainTextString(bPrice);
			bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }
		return new String[] {bPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		
		String productDesc = "";
    	
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.toPlainTextString(evaluateResult[0]);	
    	}
    	if(productDesc.trim().isEmpty()){
    		productDesc = null;
    	}
    	
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "data-product-image=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getRealProductId(String[] evaluateResult) {
		String pdId = null;
		if(evaluateResult.length == 1){
			String tmp = FilterUtil.getStringBetween(evaluateResult[0], "\">บาร์โค้ด :", "<");
			if(!tmp.isEmpty()){
				tmp = FilterUtil.toPlainTextString(tmp);
				if(StringUtils.isNumeric(tmp) && tmp.length() == 13){
					pdId = tmp;
				}
			}
		}
		return new String[]{ pdId};
	}
	
    @Override
    public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

		String[] productName = mpdBean.getProductName();
		String[] productPrice = mpdBean.getProductPrice();
		String[] bPrice = mpdBean.getProductBasePrice();
		String[] productDescription = mpdBean.getProductDescription();
		String[] productPictureUrl = mpdBean.getProductPictureUrl();
		String currentUrl = mpdBean.getCurrentUrl();
		String[] realProductId = mpdBean.getRealProductId();		

		if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
			return null;
		}
		ProductDataBean[] rtn = new ProductDataBean[1];
		rtn[0] = new ProductDataBean();
		
		String[] urlTmp = currentUrl.split("/");
		String id = urlTmp[urlTmp.length - 1];
		String pdName = productName[0];
		if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(pdName)){
			pdName += " ("+ id +")";
			rtn[0].setName(pdName);
		}else{
			return null;
		}
		
		if(realProductId != null && StringUtils.isNumeric(realProductId[0])){
			rtn[0].setRealProductId(realProductId[0]);
			rtn[0].setUpc(realProductId[0]);
		}
		
		rtn[0].setPrice(FilterUtil.convertPriceStr(productPrice[0]));

		if (bPrice != null && bPrice.length != 0 && !bPrice[0].equals(productPrice[0])) {
			rtn[0].setBasePrice(FilterUtil.convertPriceStr(bPrice[0]));
		}
		
		if (productDescription != null && productDescription.length != 0) {
			rtn[0].setDescription(productDescription[0]);
		}

		if (productPictureUrl != null && productPictureUrl.length != 0) {
			if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
				if (productPictureUrl[0].startsWith("http")) {
					rtn[0].setPictureUrl(productPictureUrl[0]);
				} else {
					try {
						URL url = new URL(new URL(currentUrl), productPictureUrl[0]);
						rtn[0].setPictureUrl(url.toString());
					} catch (MalformedURLException e) {

					}
				}
			}
		}
				
		return rtn;
	}
	    
}
