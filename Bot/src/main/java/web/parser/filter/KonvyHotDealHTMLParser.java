package web.parser.filter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import bean.MergeProductDataBean;
import bean.ProductDataBean;
import web.parser.DefaultHTMLParser;
import utils.BotUtil;
import utils.FilterUtil;

public class KonvyHotDealHTMLParser extends DefaultHTMLParser {	
	
	private static final String[][] CAT_ID_KEYWORD_MAPPING = {
		{"130110", "Carmine Lotion Deluxe", "ครีมบำรุงผิว โลชั่นบำรุงผิว"},
		{"130117", "Lucas Papaw Ointment", "ลิปบำรุง ลิปบาล์ม LipBalm Lip Therapy"},
		{"130114", "Eye Creme", "ครีมบำรุงผิวรอบดวงตา"},
		{"130214", "Soothing Gel", "เจลบำรุงผิว มอยส์เจอไรเซอร์บำรุงผิวกาย body moisturizer"},
		{"130120", "Estee Lauder Advanced Night Repair", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130117", "Lip Therapy", "ลิปบำรุง ลิปบาล์ม LipBalm"},
		{"130120", "Sulwhasoo Renewing", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130915", "Mouthwash", "น้ำยาบ้วนปาก"},
		{"130120", "Sulwhasoo GOA", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130807", "Laneige Sleep Pack", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130120", "Sulwhasoo Serum", "เซรั่มบำรุงผิว"},
		{"130103", "Make Up Base", "เบสแต่งหน้า"},
		{"130120", "Tony Moly Intense Care", "ผลิตภัณฑ์บำรุงผิว เอสเซนส์"},
		{"130103", "Pressed Powder", "แป้งฝุ่น"},
		{"130104", "Cleansing", "ทำความสะอาดผิวหน้า"},
		{"130103", "Face Blemish Balm", "บีบีครีม"},
		{"170509", "DHC Vitamin C", "วิตามินซี"},
		{"130120", "Water Bank Essence", "ผลิตภัณฑ์บำรุงผิว เอสเซนส์"},
		{"130120", "Laneige White Plus Renew Trial", "ผลิตภัณฑ์บำรุงผิว"},
		{"130104", "Perfect Whip Foam", "ทำความสะอาดผิวหน้า โฟมล้างหน้า"},
		{"130303", "Eau de Toilette", "น้ำหอม"},
		{"130214", "Real Mucin Restore Gel", "เจลบำรุงผิว มอยส์เจอไรเซอร์บำรุงผิวกาย body moisturizer"},
		{"130120", "V7 Renewal Serum", "เซรั่มบำรุงผิว"},
		{"130112", "Lifeford Eye Pen Black", "ปากกาเขียนขอบตา อายไลน์เนอร์ eyeliner"},
		{"130120", "JOA Collagen Ampoule", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130120", "Micro Essence Skin", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130103", "Illamasqua Matte Veil", "ไพรเมอร์ primer"},
		{"130111", "Lipstick", "ลิปสติก"},
		{"130118", "Sleek Face Contour", "ไฮไลท์ เฉดดิ้ง"},
		{"130120", "Facial Treatment Essence", "ผลิตภัณฑ์บำรุงผิว เอสเซนส์"},
		{"130118", "Bronzing Face Powder", "บรอนเซอร์"},
		{"170509", "NeoCell Super Collagen", "คอลลาเจน"},
		{"130108", "Eyeshadow Primer", "อายชาโดว์ eyeshadow"},
		{"130807", "Mask Sheet", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130807", "Sleeping Mask", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130809", "Facial Treatment Clear Lotion", "โทนเนอร์"},
		{"130807", "Facial Scrub Cleansing", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130219", "Underarm Cream", "ครีมบำรุงผิวใต้วงแขน"},
		{"130114", "Night Repair Eye", "ครีมบำรุงผิวรอบดวงตา"},
		{"130120", "SK II Stempower", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130103", "Loose Setting Powder", "แป้งฝุ่น"},
		{"130120", "The Saem Urban ECO Harakeke Gift Set", "ครีมบำรุงผิวหน้า"},
		{"130807", "Magic White Massage Pack", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130104", "Bioderma H2O", "เช็ดเครื่องสำอางค์"},
		{"130120", "The Green Tea Seed Serum", "เซรั่มบำรุงผิว"},
		{"130120", "It's Skin Power 10 Formula", "เซรั่มบำรุงผิว"},
		{"130103", "Mineral Powder", "แป้งฝุ่น"},
		{"130120", "SK II Cellumination Deep Surge", "ครีมบำรุงผิวหน้า"},
		{"110201", "Hand Mirror", "กระจกด้ามถือ"},
		{"130114", "Laneige Water Bank Eye Gel", "ครีมบำรุงผิวรอบดวงตา"},
		{"130119", "Bisous Bisous Eyebrow", "ดินสอเขียนคิ้ว"},
		{"130103", "Benefit the Pore Fessional Pro Balm to Minimize", "ไพรเมอร์ primer"},
		{"130103", "BB Cream", "บีบีครีม"},
		{"130103", "Sulwhasoo Evenfair Perfecting Cushion", "รองพื้น"},
		{"130120", "Emulsion", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130120", "Etude House Wonder Pore Kit", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130110", "Her Highness Royale Rebalancing Water With Royal Jelly", "ครีมบำรุงผิวหน้า โลชั่นบำรุงผิว"},
		{"130205", "UV Aqua Rich Watery Essence", "ครีมกันแดด"},
		{"130120", "Horse Oil Cream", "ครีมบำรุงผิวหน้า"},
		{"130205", "Sun Block", "ครีมกันแดด"},
		{"130111", "Dear Darling Tint", "ลิปทินท์ Lip Tint"},
		{"130120", "Lancome Advanced Genifique Youth Activating Concentrate", "เซรั่มบำรุงผิว"},
		{"130120", "Clinique Moisture Surge Extended Thirst Relief", "ครีมบำรุงผิวหน้า"},
		{"130114", "Eye Cream", "ครีมบำรุงผิวรอบดวงตา"},
		{"130106", "Skinfood Rose Cheek", "บลัชออน"},
		{"130120", "Estee Lauder Perfectionist Wrinkle Lifting", "เซรั่มบำรุงผิวหน้า"},
		{"130602", "Shiseido Powder Puff", "พัฟแป้งฝุ่น"},
		{"130111", "Berrisom My Lip Tint", "ลิปทินท์ Lip Tint"},
		{"130120", "Clinique Smart Custom Repair Serum", "เซรั่มบำรุงผิว"},
		{"130107", "Mascara", "มาสคาร่า ปัดขนตา"},
		{"130303", "CK One EDT", "น้ำหอม"},
		{"110203", "Laura Mercier Bag", "กระเป๋าสะพาย"},
		{"130108", "Urban Decay Naked", "อายแชโดว์ อายชาโดว์ eyeshadow"},
		{"130807", "Treatment Mask", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130120", "Estee Lauder Advanced Night Repair", "ครีมบำรุงผิวหน้า"},
		{"130120", "VITACREME B12 Regenerative Cream", "ครีมบำรุงผิวหน้า"},
		{"130214", "Aloe Vera Gel", "เจลบำรุงผิว มอยส์เจอไรเซอร์บำรุงผิวกาย body moisturizer"},
		{"130118", "Espoir Dewy Face Glow", "ไฮไลท์"},
		{"130104", "Cleanser", "ทำความสะอาดผิวหน้า โฟมล้างหน้า"},
		{"130120", "Night Cream", "ครีมบำรุงผิวหน้า"},
		{"130103", "Palladio Rice Powder", "แป้งฝุ่น"},
		{"130118", "Highlighter", "ไฮไลท์"},
		{"130120", "Snail White Cream", "ครีมบำรุงผิวหน้า"},
		{"130103", "CC Cream", "ซีซีครีม"},
		{"130807", "The Original Mint Julep Masque ", "มาส์คหน้า Mask มาร์คหน้า มาส์กหน้า"},
		{"130205", "Sunscreen ", "ครีมกันแดด"},
		{"130119", "Mei Linda Smart Auto Brow Liner", "ดินสอเขียนคิ้ว"},
		{"130120", "Laneige White Plus Renew Original Cream ", "ครีมบำรุงผิวหน้า"},
		{"130804", "Flawless Skin Face Polish", "สครับขัดหน้า"},
		{"130103", "Foundation Powder", "แป้งผสมรองพื้น"},
		{"130120", "Repairwear Laser Focus Smooths Restores Corrects", "ครีมบำรุงผิวหน้า"},
		{"130125", "Lip Concealer", "ลิปคอนซีลเลอร์ รองพื้นสำหรับริมฝีปาก"},
		{"130104", "Lip and Eye Remover", "เช็ดเครื่องสำอางค์"},
		{"130112", "Eyeliner", "อายไลเนอร์"},
		{"130602", "Oni Brush", "แปรงทำความสะอาดผิวหน้า"},
		{"130120", "Sulwhasoo Basic Kit", "ครีมบำรุงผิวหน้า เซรั่มบำรุงผิว"},
		{"130103", "NARS Optimizing Primer", "ไพรเมอร์ primer"},
		{"130120", "Cellumination Day Surge", "ครีมบำรุงผิวหน้า"},
		{"130810", "Facial Spray", "สเปรย์น้ำแร่"},
		{"130104", "Cleansing Oil", "ทำความสะอาดผิวหน้า โฟมล้างหน้า"},
		{"130120", "Water Bank Gel Cream", "ครีมบำรุงผิวหน้า เจลบำรุงผิวหน้า"},
		{"130120", "Kose Cream Excellent", "ครีมบำรุงผิวหน้า"},
		{"130105", "Raser Dark Circles", "คอนซีลเลอร์ concealer"},
		{"130104", "Cleansing Gel", "ทำความสะอาดผิวหน้า เจลล้างหน้า"},
		{"130407", "Hair Treatment", "ทรีทเมนต์สำหรับผม"},
		{"130111", "Lip Color", "ลิปสติก"},
		{"130120", "Cream Whitening", "ครีมบำรุงผิวหน้า"},
		{"130120", "Moisture Cream", "ครีมบำรุงผิวหน้า"},
		{"130119", "Eyebrow Pencil", "ดินสอเขียวคิ้ว"},
		{"130602", "Complexion Sponge", "ฟองน้ำรูปใข่ ฟองน้ำเกลี่ยรองพื้น"},
		{"130117", "Lip Balm", "ลิปบำรุง ลิปบาล์ม"},
		{"130117", "DHC Lip Cream", "ลิปบำรุง ลิปบาล์ม"},
		{"130504", "Foot Balm", "ครีมทาส้นเท้า"},
		{"130810", "Treatment Young Spray", "สเปรย์น้ำแร่"},
		{"130103", "Smooth Finish Flawless Fluide", "รองพื้น"},
		{"130214", "Skin Moisturiser", "มอยส์เจอไรเซอร์บำรุงผิวกาย body moisturizer"},
		{"130120", "Renovating Cream", "ครีมบำรุงผิวหน้า"},
		{"130809", "Toner", "โทนเนอร์"},
		{"130114", "Night Repair Eye", "ครีมบำรุงผิวรอบดวงตา"},
		{"130114", "Eyes Reduces Circles", "ครีมบำรุงผิวรอบดวงตา"},
		{"130104", "Liquid Facial Soap", "ทำความสะอาดผิวหน้า โฟมล้างหน้า"},
		{"130102", "Eyelash Curler", "ที่ดัดขนตา"},
		{"170509", "DHC Collagen", "คอลลาเจน"},
		{"130120", "Multiberry Yogurt Repair Pack", "ครีมบำรุงผิวหน้า"},
		{"130120", "Lancome Beauty Lotion", "โลชั่นบำรุงผิวหน้า"},
		{"130103", "NARS Tinted Moisturizer", "รองพื้น"},
		{"130801", "Blemish Drying Lotion", "ผลิตภัณฑ์แต้มสิว"},
		{"130105", "Concealer Palette", "คอนซีลเลอร์ concealer"},
		{"130106", "Wet n Wild Coloricon", "บลัชออน"},
		{"130114", "Unperfumed Eye Gel", "เจลบำรุงผิวรอบดวงตา"},
		{"130120", "Repairwear Uplifting", "ครีมบำรุงผิวหน้า"},
		{"130120", "Brightening Moisture Cream", "ครีมบำรุงผิวหน้า"},
		{"130103", "Foundation Primer", "ไพรเมอร์ primer"},
		{"130104", "Makeup Remover", "เช็ดเครื่องสำอางค์"},
		{"130119", "Etude House Color My Brows", "มาสคาร่าสำหรับคิ้ว"},
		{"130106", "Sleek Blush", "บลัชออน"},
		{"130108", "Eye Shadow", "อายแชโดว์ อายชาโดว์ eyeshadow"},
		{"130120", "Anti Aging Creme", "ครีมบำรุงผิวหน้า"},
		{"130103", "Laura Mercier Tinted Moisturizer", "รองพื้น"},
		{"130103", "Estee Lauder Double Wear Stay In Place", "รองพื้น"},
		{"130106", "Polka Dot Blush", "บลัชออน"},
		{"130120", "Sulwhasoo First Care Serum", "เซรั่มบำรุงผิวหน้า"},
		{"130111", "Peripera Tint", "ลิปทินท์ Lip Tint"},
		{"130120", "Oil Cream", "ครีมบำรุงผิวหน้า"},
		{"130104", "Cleansing Water", "เช็ดเครื่องสำอางค์"},
		{"170509", "Colly Pink Collagen", "คอลลาเจน"}
	};
	
    public Map<String, String[]> getConfiguration() {
    	Map<String, String[]> map = new Hashtable<String, String[]>(); 
		
		map.put("name", new String[]{""});
		map.put("price", new String[]{"<span class=\"color-pink pro-top-con-list-price\">"});
		map.put("basePrice", new String[]{"<span style=\"text-decoration:line-through\" class=\"color-9 font14\">"});
		map.put("description", new String[]{""});
		map.put("pictureUrl", new String[]{""});
		map.put("expire", new String[]{ "<div class=\"pro_dotteh_Bought\">"});
		return map;
	}
	
	public String[] getProductName(String[] evaluateResult) {
		String productName = "";   
		if(evaluateResult.length == 1) {
			productName = FilterUtil.getStringBetween(evaluateResult[0], "\"name\": \"", "\"");
			if(productName.length()==0) {
				productName = FilterUtil.getStringBetween(evaluateResult[0], "\"name\":\"", "\"");
			}
			productName = FilterUtil.toPlainTextString(productName);
			productName = productName.replace("×", "x");
		}
		return new String[] {productName};
	}
	
	@Override
	public String[] getProductBasePrice(String[] evaluateResult) {
		String bPrice = "";    	
    	if (evaluateResult.length == 1) {
    		bPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		bPrice = FilterUtil.removeCharNotPrice(bPrice);
        }		
		return new String[] {bPrice};
	}

	public String[] getProductPrice(String[] evaluateResult){
		String productPrice = "";    	
    	if (evaluateResult.length == 1) {
    		productPrice = FilterUtil.toPlainTextString(evaluateResult[0]);
    		productPrice = FilterUtil.removeCharNotPrice(productPrice);
        }
		return new String[] {productPrice};
	}
	
	public String[] getProductDescription(String[] evaluateResult) {
		String productDesc = "";
    	if(evaluateResult.length == 1) {
    		productDesc = FilterUtil.getStringBetween(evaluateResult[0], "<meta name=\"description\" content=\"", "\"");
    		productDesc = FilterUtil.toPlainTextString(productDesc);
    	}
    	return new String[] {productDesc};
	}
	
	public String[] getProductPictureUrl(String[] evaluateResult) {
		
		String productPictureUrl = null;
    	if (evaluateResult.length == 1) {
    		productPictureUrl = FilterUtil.getStringBetween(evaluateResult[0], "<meta property=\"og:image\" content=\"", "\"");
        }
    	return new String[] {productPictureUrl};
	}
	
	@Override
	public String[] getProductExpire(String[] evaluateResult) {
		String exp = "false";
		if (evaluateResult.length == 1) {
			String data = evaluateResult[0];
    		if(data.contains("SOLD OUT")){
    			exp = "true";
    		}
        }
		return new String[] { exp};
	}

	  public ProductDataBean[] mergeProductData(MergeProductDataBean mpdBean) {

			String[] productName = mpdBean.getProductName();
			String[] productPrice = mpdBean.getProductPrice();
			String[] productBPrice = mpdBean.getProductBasePrice();
			String[] productDescription = mpdBean.getProductDescription();
			String[] productPictureUrl = mpdBean.getProductPictureUrl();
			String currentUrl = mpdBean.getCurrentUrl();

			if (productName == null || productName.length == 0 || productPrice == null || productPrice.length == 0) {
				return null;
			}
			
			String pdName = productName[0];
			ProductDataBean[] rtn = new ProductDataBean[1];
			rtn[0] = new ProductDataBean();
			rtn[0].setName(pdName);
			double price = FilterUtil.convertPriceStr(productPrice[0]);
			rtn[0].setPrice(price);
			
			if (productBPrice != null && productBPrice.length != 0 && !productBPrice[0].isEmpty()) {
				double bPrice = FilterUtil.convertPriceStr(productBPrice[0]);
				if(bPrice > price){
					rtn[0].setBasePrice(bPrice);
				}
			}

			if (productDescription != null && productDescription.length != 0 && !productDescription[0].isEmpty()) {
				rtn[0].setDescription(productDescription[0]);
			}

			if (productPictureUrl != null && productPictureUrl.length != 0) {
				if (productPictureUrl[0] != null && productPictureUrl[0].trim().length() != 0) {
					if (productPictureUrl[0].startsWith("http")) {
						rtn[0].setPictureUrl(productPictureUrl[0]);
					} else {
						try {
							URL url = new URL(new URL(currentUrl),
									productPictureUrl[0]);
							rtn[0].setPictureUrl(url.toString());
						} catch (MalformedURLException e) {

						}
					}
				}
			}

			String[] matchCatId = mapCategory(pdName, CAT_ID_KEYWORD_MAPPING);				
			if(matchCatId != null) {
				rtn[0].setKeyword(matchCatId[2]);
				rtn[0].setCategoryId(BotUtil.stringToInt(matchCatId[0], 160255));
			}else{
				rtn[0].setCategoryId(160255);
			}

			return rtn;
		}
	    
}