package web.parser.image;

public class Limit2MBImageParser extends DefaultImageParser {
	public static final int LIMIT_SIZE = 2000000;
	
	@Override
	public byte[] processImage(String imageUrl, int width, int height) throws Exception{
		return processImage(imageUrl, width, height, LIMIT_SIZE);
	}
		
}
