package web.parser.image;

import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.byteSources.ByteSource;
import org.apache.sanselan.common.byteSources.ByteSourceArray;
import org.apache.sanselan.formats.jpeg.JpegImageParser;
import org.apache.sanselan.formats.jpeg.segments.UnknownSegment;

import utils.BotUtil;

public class InsureImageParser extends DefaultImageParser {
	
	@Override
	public byte[] processImage(String imageUrl, int width, int height) throws Exception {
		return processImage(imageUrl, width, height, 2000000);
	}
	
	public byte[] processImage(String imageUrl, int width, int height, long limitLength) throws Exception {
		URL url = null;
		HttpURLConnection conn = null;
		//System.out.println(System.getProperties());
		try {
			url = new URL(imageUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("Accept","*/*");
			conn.setConnectTimeout(1000000);
			conn.setReadTimeout(1000000);

			String contentLength = "1";
			
			int length = BotUtil.stringToInt(contentLength,0);
			
			
			if(limitLength > 0 && length > limitLength) {
				throw new Exception("Exceed file size  "+(length/1000000)+" MB --> "+imageUrl);
			}
			
			try (InputStream inputStream = conn.getInputStream();
				ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
				
				int read = 0;
				byte[] bytes = new byte[20480];
				while ((read = inputStream.read(bytes)) != -1)
					byteOutputStream.write(bytes, 0, read);

				try (ImageInputStream imageStream = ImageIO.createImageInputStream(new ByteArrayInputStream(byteOutputStream.toByteArray()))) {
					Iterator<ImageReader> iter = ImageIO.getImageReaders(imageStream);
					BufferedImage image = null;
					String imageFormat = null;
					
					if (iter.hasNext()) {
						ImageReader reader = null;
						try {
							reader = iter.next();
							reader.setInput(imageStream);
							image = reader.read(0);
							imageFormat = reader.getFormatName();
						} catch (IIOException e) {
							try {
								WritableRaster raster = (WritableRaster) reader.readRaster(0, null);
								ICC_Profile profile = Sanselan.getICCProfile(byteOutputStream.toByteArray());

								checkAdobeMarker(byteOutputStream.toByteArray(), raster);

								image = convertCmykToRgb(raster, profile);
							} catch (ImageReadException e1) {
								throw e1;
							}
						} finally {
							if (reader!=null) {
								reader.dispose();
							}
						}
					}
					
					byte[] output;
					if (image != null) {
						byteOutputStream.reset();
						if(imageFormat==null)
								imageFormat = "";
						try{
							switch(imageFormat.toLowerCase()) {
							case "jpeg"	: ImageIO.write(image, "jpg", byteOutputStream); break;
							case "jpg"	: ImageIO.write(image, "jpg", byteOutputStream); break;
							case "png"	: ImageIO.write(image, "png", byteOutputStream); break;
							default		: throw new Exception("imageFormat type not supported --> "+imageFormat);
						}
						}catch (Exception e) {
							throw e;
						}
					}
					output = parse(byteOutputStream.toByteArray(), width, height, imageFormat);
						
					return output;
				}
			} catch(FileNotFoundException e) {
				throw e;
			} 
		} catch (Exception e) {
			BotUtil.consumeInputStreamQuietly(conn.getErrorStream());
			throw e;
		} finally {
			if(conn != null)
	 			conn.disconnect();
		}
	}
	
	
	private void checkAdobeMarker(byte[] imageByte, WritableRaster raster) throws Exception {
		JpegImageParser imageParser = new JpegImageParser();
		ByteSource byteSource = new ByteSourceArray(imageByte);
		
		@SuppressWarnings("rawtypes")
		ArrayList segments = null;
		try {
			segments = imageParser.readSegments(byteSource, new int[] { 0xffee }, true);
		} catch (ImageReadException | IOException e) {
			throw e;
		}
		if (segments != null && segments.size() >= 1) {
			UnknownSegment app14Segment = (UnknownSegment) segments.get(0);
			byte[] segmentByte = app14Segment.bytes;
			if (segmentByte.length >= 12 && segmentByte[0] == 'A' && segmentByte[1] == 'd' && segmentByte[2] == 'o' && segmentByte[3] == 'b' && segmentByte[4] == 'e') {
				if ((app14Segment.bytes[11] & 0xff) == 2) {
					convertYcckToCmyk(raster);
				}
				convertInvertedColors(raster);
			}
		}
	}
	
	private void convertYcckToCmyk(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);

			for (int x = 0; x < stride; x += 4) {
				int y = pixelRow[x];
				int cb = pixelRow[x + 1];
				int cr = pixelRow[x + 2];

				int c = (int) (y + 1.402 * cr - 178.956);
				int m = (int) (y - 0.34414 * cb - 0.71414 * cr + 135.95984);
				y = (int) (y + 1.772 * cb - 226.316);

				if (c < 0) {
					c = 0;
				} else if (c > 255) {
					c = 255;
				}
					
				if (m < 0) {
					m = 0;
				} else if (m > 255) {
					m = 255;
				}
					
				if (y < 0) {
					y = 0;
				} else if (y > 255) {
					y = 255;
				}

				pixelRow[x] = 255 - c;
				pixelRow[x + 1] = 255 - m;
				pixelRow[x + 2] = 255 - y;
			}

			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}
	
	private void convertInvertedColors(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);
			for (int x = 0; x < stride; x++) {
				pixelRow[x] = 255 - pixelRow[x];
			}
			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}
	
	private BufferedImage convertCmykToRgb(Raster srcRaster, ICC_Profile profile) throws IOException {
		if (profile == null) {
			profile = ICC_Profile.getInstance(DefaultImageParser.class.getResourceAsStream("/ISOcoated_v2_300_eci.icc"));
		}

		if (profile.getProfileClass() != ICC_Profile.CLASS_DISPLAY) {
			byte[] profileData = profile.getData();

			if (profileData[ICC_Profile.icHdrRenderingIntent] == ICC_Profile.icPerceptual) {
				intToBigEndian(ICC_Profile.icSigDisplayClass, profileData, ICC_Profile.icHdrDeviceClass);
				
				profile = ICC_Profile.getInstance(profileData);
			}
		}

		BufferedImage resultImage = new BufferedImage(srcRaster.getWidth(), srcRaster.getHeight(), BufferedImage.TYPE_INT_RGB);
		WritableRaster resultRaster =  resultImage.getRaster();

		ICC_ColorSpace cmykCS = new ICC_ColorSpace(profile);
		ColorSpace rgbCS = resultImage.getColorModel().getColorSpace();

		ColorConvertOp cmykToRgb = new ColorConvertOp(cmykCS, rgbCS, null);
		cmykToRgb.filter(srcRaster, resultRaster);

		return resultImage;
	}
	
	private void intToBigEndian(int value, byte[] profileData, int index) {
		profileData[index] = (byte) (value >> 24);
		profileData[index + 1] = (byte) (value >> 16);
		profileData[index + 2] = (byte) (value >> 8);
		profileData[index + 3] = (byte) (value);
	}

}
