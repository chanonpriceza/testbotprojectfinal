package web.parser.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageParser extends DefaultImageParser{
	
	public byte[] parse(String imageUrl, int width, int height) {
		try {
			URL url = new URL(imageUrl);			
			ImageIcon ii = new ImageIcon(url);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if(iWidth == -1 && iHeight == -1)
				return null;
			
			double xPos = (width - 1 * iWidth) / 2;
			double yPos = (height - 1 * iHeight) / 2;
			Image temp = null;
			
			if (iHeight > height || iWidth > width) {
				if (iWidth > iHeight)
					resizedImage = i.getScaledInstance(width, (height * iHeight) / iWidth, Image.SCALE_SMOOTH);
				else
					resizedImage = i.getScaledInstance((width * iWidth) / iHeight, height, Image.SCALE_SMOOTH);

				double xScale = (double) width / iWidth;
				double yScale = (double) height / iHeight;
				double scale = Math.min(xScale, yScale);

				xPos = (width - scale * iWidth) / 2;
				yPos = (height - scale * iHeight) / 2;
				temp = new ImageIcon(resizedImage).getImage();
				
			} else {
				temp = i;
			}
					
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			
			Graphics g = bufferedImage.createGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, width, height);
			g.drawImage(temp, (int)xPos, (int)yPos, null);
			g.dispose();

			try(ByteArrayOutputStream outStream = new ByteArrayOutputStream()){
				ImageIO.write(bufferedImage, "jpg", outStream);
				outStream.flush();
				byte[] imageInByte = outStream.toByteArray();
				outStream.close();
				
				return imageInByte;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
