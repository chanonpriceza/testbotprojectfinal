package web.parser.image;

public class IgnoreSSL extends DefaultImageParser {
	@Override
	public byte[] processImage(String imageUrl, int width, int height) throws Exception {
		System.setProperty("jsse.enableSNIExtension", "false");
		return processImage(imageUrl, width, height, 2000000);
	}
}
