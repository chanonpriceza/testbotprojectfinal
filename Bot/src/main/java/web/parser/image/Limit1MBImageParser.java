package web.parser.image;

public class Limit1MBImageParser extends DefaultImageParser {
	public static final int LIMIT_SIZE = 1000000;
	
	@Override
	public byte[] processImage(String imageUrl, int width, int height) throws Exception {
		return processImage(imageUrl, width, height, LIMIT_SIZE);
	}
		
}
