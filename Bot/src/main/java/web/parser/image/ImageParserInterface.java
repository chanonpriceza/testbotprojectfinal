package web.parser.image;

public interface ImageParserInterface {
	byte[] parse(String imageUrl, int width, int height) ;
}
