package web.parser;

import bean.ProductDataBean;

import java.util.List;

import bean.ParserConfigBean;

public interface HTMLParser {
	public enum FILTER_TYPE {
		DEFAULT, PRODUCTLIST
	}
	
	public void setParserConfig(List<ParserConfigBean> configList);
	public ProductDataBean[] parse(String url);
	public FILTER_TYPE getFilterType();
	
}