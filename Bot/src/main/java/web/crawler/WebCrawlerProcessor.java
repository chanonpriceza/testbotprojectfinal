package web.crawler;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import bean.ProductUrlBean;
import engine.BaseConfig;
import manager.ProcessLogManager;
import product.processor.ParserCrawler;
import product.processor.ProductUrlReader;
import web.parser.HTMLParser;

public class WebCrawlerProcessor implements Callable<Boolean>{
	private static final Logger logger = LogManager.getRootLogger();
	private CountDownLatch latch;
	private HTMLParser parser;
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private ProcessLogManager processLog  = ProcessLogManager.getInstance();
	
	@Override
	public Boolean call() {
		boolean success = true;
		try {
			while(!Thread.currentThread().isInterrupted()) {
    			try {
    				ProductUrlBean urlBean = ProductUrlReader.getInstance().readData();
	    			if(urlBean == null)
	    				break;
	    			crawl(urlBean);
    			} catch (SQLException e) {
    				success = false;
    				logger.error(e);
    				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
    			}
	    	}
		} catch(Exception e) {
			success = false;
			logger.error(e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		} finally {
			try{
				latch.countDown();
				logger.info("WebCrawlerProcessor : latch countdown.");
			}catch(Exception e) {
				success = false;
				logger.error(e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
		return success;
	}
	
	public void setProperties(CountDownLatch latch) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		this.latch = latch;
		this.parser = (HTMLParser)Class.forName(BaseConfig.CONF_PARSER_CLASS).getDeclaredConstructor().newInstance();
		this.parser.setParserConfig(null);
	}
	
	private void crawl(ProductUrlBean urlBean) throws SQLException {
		ProductDataBean[] dataBeanList = parser.parse(urlBean.getUrl());
		if(dataBeanList != null && dataBeanList.length > 0) {
			ParserCrawler.process(dataBeanList, urlBean);
		}else {
			ParserCrawler.markNoResult(urlBean);
		}
		
		if(BaseConfig.CONF_WCE_SLEEP_TIME > 0) {
    		try {
				Thread.sleep(BaseConfig.CONF_WCE_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error("Error ", e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
    	
		return ;
	}
	
}
