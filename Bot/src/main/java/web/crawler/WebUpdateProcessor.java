package web.crawler;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ReportBean;
import bean.UrlPatternBean;
import db.UrlPatternDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import product.processor.ParserUpdate;
import product.processor.ProductFilter;
import product.processor.ProductReader;
import utils.BotUtil;
import web.parser.HTMLParser;

public class WebUpdateProcessor implements Callable<Boolean>{
	private static final Logger logger = LogManager.getRootLogger();
	private final ReportBean report = ReportManager.getInstance().getReport();
	private CountDownLatch latch;
	private HTMLParser parser;
	private String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private ProcessLogManager processLog = ProcessLogManager.getInstance();
	private  Set<Integer> categoryList;
	
	@Override
	public Boolean call() {
		boolean result = true;
		try {
    		while(!Thread.currentThread().isInterrupted()) {
    			try {
    				if(ParserUpdate.deleteLimit())
	    				break;
    				
    				ProductDataBean b = ProductReader.getInstance().readData();
	    			if(b == null)
	    				break;
	    			ParserResultBean rb = analyzeProduct(b);
	    			if(rb != null)
	    				if(rb.isExpire()) {
	    					report.increaseExpire();
	    					ParserUpdate.deleteProduct(b);
	    				}else
	    					ParserUpdate.update(rb);
	    			else
	    				markError(b);
    			} catch (SQLException e) {
    				result = false;
    				logger.error(e);
    				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
    			}
	    	}
		} catch(Exception e) {
			result = false;
			logger.error(e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		} finally {
			try{
				latch.countDown();
				logger.info("WebUpdateProcessor : latch countdown.");
			}catch(Exception e) {
				result = false;
				logger.error(e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
		return result;
	}
	
	public void setProperties(CountDownLatch latch) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		this.latch = latch;
		parser = (HTMLParser)Class.forName(BaseConfig.CONF_PARSER_CLASS).getDeclaredConstructor().newInstance();
		parser.setParserConfig(null);
		try {
			UrlPatternDB urlPatternDB = DatabaseManager.getInstance().getUrlPatternDB();
			List<UrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, "STARTURL");
			categoryList = categoryMapping.stream().map(UrlPatternBean::getCategoryId).collect(Collectors.toSet());
		}catch (Exception e) {
			logger.error("Error",e);
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
		}
		
	}
	
	private ParserResultBean analyzeProduct(ProductDataBean b) throws SQLException {
		if(BaseConfig.IS_DELETE_ALL) 
			return null;
		
		if(BaseConfig.IS_DELETE_NOT_MAP&& !categoryList.contains(b.getCategoryId()) && categoryList.size() > 0)
			return null;

		if(BaseConfig.IS_DELETE_NOT_MAP) {
			ParserResultBean result = new ParserResultBean();
			result.setPdb(b);
			return result;
		}
		String urlForUpdate = b.getUrlForUpdate();
		String url = urlForUpdate.startsWith("http") ? urlForUpdate : b.getUrl();		
		ProductDataBean pdb = getProduct(parser.parse(url), b);
		
		if(pdb == null) 
			return null;

		if(StringUtils.isBlank(pdb.getUrl()))
			pdb.setUrl(urlForUpdate);
		
		pdb.setUrlForUpdate(urlForUpdate);
		return ParserUpdate.analyzeProduct(pdb, b);
	}
	
	private void markError(ProductDataBean b) throws SQLException {
		int errorUpdateCount = b.getErrorUpdateCount();
		errorUpdateCount++;
		report.increaseErrorCount();
		if(ProductFilter.checkCOVIDInstantDelete(b.getName()) || BaseConfig.CONF_FORCE_DELETE || (errorUpdateCount >= BaseConfig.CONF_PRODUCT_UPADTE_LIMIT ) || BaseConfig.IS_DELETE_ALL || BaseConfig.IS_DELETE_NOT_MAP) {
			ParserUpdate.deleteProduct(b);
		}else {
			ParserUpdate.updateErrorCount(b, errorUpdateCount);
		}
	}
	
	private ProductDataBean getProduct(ProductDataBean[] pdb, ProductDataBean b) {
		if(pdb == null)
			return null;
		
		for(ProductDataBean newBean : pdb) {
			if(newBean.isExpire()) {
				report.increaseExpire();
				return newBean;
			}
			else {
			ParserUpdate.convertContent(newBean);
				if(BaseConfig.CONF_IGNORE_NAME_CHANGE && StringUtils.isNotBlank(newBean.getUrlForUpdate())) {
					if(BotUtil.checkStringEqual(newBean.getUrlForUpdate(), b.getUrlForUpdate())) {
						return newBean;
					}
				} else {
					if(BotUtil.checkStringEqual(newBean.getName(), b.getName())) {
						return newBean;
					}else {
						if(StringUtils.isNotBlank(newBean.getName()))
							processLog.addNameIsChange(b.getName(),newBean.getName());
						else
							processLog.addNameIsBlank(b.getName(), newBean.getName());
					}
				}
				
			}
		}
		
		return null;
	}


	
}
