package web.crawler;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.net.URI;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.PatternResultBean;
import bean.ProductUrlBean;
import bean.ReportBean;
import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.WorkloadDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import manager.UrlPatternManager;
import product.processor.WorkloadReader;
import utils.BotUtil;
import web.crawler.bot.CrawlerInterface;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLCrawlerClient;
import web.crawler.bot.URLCrawlerInterface;
import web.crawler.bot.URLUtility;

public class UrlCrawlerProcessor implements Callable<Boolean>, URLCrawlerClient{
	
	private static final Logger logger = LogManager.getRootLogger();
	private CountDownLatch latch;
	private WorkloadDB workloadDB;
	private ProductUrlDB productUrlDB;
	private CrawlerInterface initCrawler;
	private URLCrawlerInterface urlCrawler;
	private ProcessLogManager processLog = ProcessLogManager.getInstance();
	private final String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
	private static final ReportBean report = ReportManager.getInstance().getReport();
	
	public void setProperties(CountDownLatch latch) throws Exception {
		this.productUrlDB = DatabaseManager.getInstance().getProductUrlDB();
		this.workloadDB = DatabaseManager.getInstance().getWorkloadDB();
		this.latch = latch;
		if(!isBlank(BaseConfig.CONF_INIT_CRAWLER_CLASS)) {
    		Class<?> c1 = Class.forName(BaseConfig.CONF_INIT_CRAWLER_CLASS);
    		this.initCrawler = (CrawlerInterface) c1.getDeclaredConstructor().newInstance();
		}
		
		if(isBlank(BaseConfig.CONF_URL_CRAWLER_CLASS)) {
			this.urlCrawler = new HeatonURLCrawler(true);
    	} else {
			Class<?> c1 = Class.forName(BaseConfig.CONF_URL_CRAWLER_CLASS);
			this.urlCrawler = (URLCrawlerInterface) c1.getDeclaredConstructor().newInstance();
    	}
		this.urlCrawler.init(this);
	}
	
	@Override
	public Boolean call() {
		boolean success = true;
		try {
    		while(!Thread.currentThread().isInterrupted()) {
    			try {
    				WorkloadBean workloadBean = WorkloadReader.getInstance().readData();
	    			if(workloadBean == null)
	    				break;
	    			crawl(workloadBean);
    			} catch (SQLException e) {
    				success = false;
    				logger.error(e);
    			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
    				success = false;
    				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
					throw new Exception(e);
				} catch (Exception e) {
					success = false;
					logger.error(e);
					processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
				}
	    	}
		} catch(Exception e) {
			success = false;
			processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			logger.error(e);
		} finally {
			try{
				latch.countDown();
				logger.info("UrlCrawlerProcessor : latch countdown.");
			}catch(Exception e) {
				success = false;
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
		return success;
	}
	
	private void crawl(WorkloadBean workloadBean) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, Exception {
		
		boolean continueProcess = true;
    	
		if(initCrawler != null) {				
			String startUrl = workloadBean.getUrl();
			List<ProductUrlBean> productUrlBean = initCrawler.initProductUrl(startUrl);
			for (ProductUrlBean pu : productUrlBean) {
				ProductUrlBean newProductUrlBean = ProductUrlBean.createNewProductUrlBean(BaseConfig.MERCHANT_ID, 
						pu.getUrl(), 
						pu.getCategoryId(), 
						BotUtil.limitString(pu.getKeyword(), ProductDataDB.KEYWORD_LENGTH));
    			productUrlDB.insertProductUrl(newProductUrlBean);
    			report.increaseWceProductUrlCount();
			}
			continueProcess = initCrawler.continueProcess();
		}
    	
    	try {
        	if(continueProcess) {
        		processTarget(urlCrawler, workloadBean);
        	} else {
        		completePage(workloadBean.getMerchantId(), workloadBean.getUrl(), STATUS.C);
        		report.increaseWceWorkloadCountSuccess();
        	}           	
        } catch (Exception e) {
        	logger.error("Error ", e);
        	processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
        	completePage(workloadBean.getMerchantId(), workloadBean.getUrl(), STATUS.E);
        	report.increaseWceWorkloadCountFailed();
        }
    	
    	if(BaseConfig.CONF_WCE_SLEEP_TIME > 0) {
    		try {
				Thread.sleep(BaseConfig.CONF_WCE_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error("Error ", e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		}
    	
		return ;
	}

	private void processTarget(URLCrawlerInterface urlCrawler, WorkloadBean workloadBean) throws Exception  {
		String targetUrl = workloadBean.getUrl();
        int targetDepth = workloadBean.getDepth();
        int targetMerchantId = workloadBean.getMerchantId();
        int targetCategoryId = workloadBean.getCategoryId();
        String targetKeyword = workloadBean.getKeyword();
        
        checkStartUrlForMatch(targetMerchantId, targetUrl, targetCategoryId, targetKeyword);
        
        if (targetDepth + 1 > BaseConfig.CONF_MAX_DEPTH) {
        	workloadDB.updateStatus(targetMerchantId , targetUrl, STATUS.C);
        	report.increaseWceWorkloadCountSuccess();
            return;
        }
        
        String encodedtargetUrl = null;
        if(!BaseConfig.CONF_SKIP_ENCODE_URL) {
            try {
            	URL normalURL = new URL(targetUrl);			
            	URI encodedURI = new URI(
            			normalURL.getProtocol(), 
            			normalURL.getHost(), 
            			normalURL.getPath(),
            			normalURL.getQuery(),
            			null);
            	encodedtargetUrl = encodedURI.toString();

            } catch (Exception e) {
            	processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
        }
        
        if(encodedtargetUrl != null && encodedtargetUrl.trim().length() != 0) {		
        	workloadBean.setRequestUrl(BotUtil.encodeURL(encodedtargetUrl));				
		} else {
			workloadBean.setRequestUrl(BotUtil.encodeURL(targetUrl));
		}
                  
		urlCrawler.process(workloadBean);	
	}
	
	public void checkStartUrlForMatch(int merchantId, String url, int startCategoryId, String keyword) throws SQLException{
		
		PatternResultBean result = UrlPatternManager.getInstance().isPatternMatch(url);

        if (result.isPass()) {
            int matchCat = startCategoryId;
            String matchKeyword = keyword;
            if(result.getCategoryId() > 0) {
            	matchCat = result.getCategoryId();            	
            }
            if(result.getKeyword() != null && result.getKeyword().trim().length() != 0) {
            	matchKeyword = result.getKeyword();            	
            }
            
            ProductUrlBean newProductUrlBean = ProductUrlBean.createNewProductUrlBean(BaseConfig.MERCHANT_ID, url, matchCat, BotUtil.limitString(matchKeyword, ProductDataDB.KEYWORD_LENGTH));
			productUrlDB.insertProductUrl(newProductUrlBean);
			report.increaseWceProductUrlCount();
        }
		
	}

	@Override
	public void completePage(int merchantId, String url, STATUS status) throws SQLException {
		workloadDB.updateStatus(merchantId, url, status);
	}

	@Override
	public void foundInternalLink(int merchantId, String url, int depth, int categoryId, String keyword) throws SQLException {
		
		if((url.toUpperCase().endsWith(".JPG")) ||
           (url.toUpperCase().endsWith(".GIF")) ||
           (url.toUpperCase().endsWith(".PNG")) ||
           (url.toUpperCase().endsWith(".JS")) ||
           (url.toUpperCase().endsWith(".CSS"))) {
            return;
        }
        
        url = url.replaceAll("&amp;","&");
        
        url = URLUtility.removeJSessionID(url);
        
        UrlPatternManager urlPatternManager = UrlPatternManager.getInstance();
        PatternResultBean continueResult = urlPatternManager.isPatternContinue(url);
        PatternResultBean matchResult = urlPatternManager.isPatternMatch(url);

        	
        if(( !continueResult.isPass() &&
             !matchResult.isPass() ) ||
             urlPatternManager.isPatternDrop(url).isPass()){
            return;
        }

        url = urlPatternManager.processRemoveQuery(url);
                                        
        if (matchResult.isPass() && depth != 0) {
           
            int matchCat = categoryId;
            String matchKeyword = keyword;
            if(matchResult.getCategoryId() > 0) {
            	matchCat = matchResult.getCategoryId();            	
            }
            if(matchResult.getKeyword() != null && matchResult.getKeyword().trim().length() != 0) {
            	matchKeyword = matchResult.getKeyword();            	
            }         
            
            ProductUrlBean newProductUrlBean = ProductUrlBean.createNewProductUrlBean(merchantId, url, matchCat, matchKeyword);
			productUrlDB.insertProductUrl(newProductUrlBean);
			report.increaseWceProductUrlCount();
            
            if(!continueResult.isPass())
                return;
        }

        int continueCat = categoryId;
        String continueKeyword = keyword;
        if(continueResult.getCategoryId() > 0) {
        	continueCat = continueResult.getCategoryId();            	
        }
        if(continueResult.getKeyword() != null && continueResult.getKeyword().trim().length() != 0) {
        	continueKeyword = continueResult.getKeyword();            	
        } 
        if(continueCat < 0) {
        	continueCat = 0;
        }
        
        workloadDB.insert(merchantId, url, depth, continueCat, continueKeyword, STATUS.W.toString());
        report.increaseWceWorkloadCount();
		
	}

}
