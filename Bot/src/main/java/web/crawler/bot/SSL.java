package web.crawler.bot;

import java.io.*;
import java.net.*;
import java.security.Security;

import javax.net.ssl.*;

public class SSL {

	private SSL() {}

	protected static SSLSocketFactory factory = null;

	static public Socket getSSLSocket(String host, int port) throws UnknownHostException, IOException {
		obtainFactory();
		SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
		return socket;
	}

	static private void obtainFactory() {
		if (factory == null) {
//			java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			java.security.Security.addProvider(Security.getProvider("SunJSSE"));
			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
			factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
		}
	}

	static public Socket getSSLSocket(Socket s, String host, int port) throws UnknownHostException, IOException {
		obtainFactory();
		SSLSocket socket = (SSLSocket) factory.createSocket(s, host, port, true);
		return socket;
	}

}
