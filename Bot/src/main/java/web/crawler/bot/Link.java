package web.crawler.bot;

public class Link {

	protected String alt;
	protected String href;
	protected String prompt;

	public Link(String alt, String href, String prompt) {
		this.alt = alt;
		this.href = href;
		this.prompt = prompt;
	}

	public String getALT() {
		return alt;
	}

	public String getHREF() {
		return href;
	}

	public String getPrompt() {
		return prompt;
	}

	public String toString() {
		return href;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	
}
