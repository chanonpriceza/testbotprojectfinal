package web.crawler.bot;

import java.sql.*;

public class SpiderSQLWorkload implements IWorkloadStorable {

	Connection connection;
	PreparedStatement prepClear;
	PreparedStatement prepAssign;
	PreparedStatement prepGetStatus;
	PreparedStatement prepSetStatus1;
	PreparedStatement prepSetStatus2;
	PreparedStatement prepSetStatus3;
	PreparedStatement prepSetStatus4;
	PreparedStatement prepSetStatus5;
	PreparedStatement prepGetDepth;

	// maximum error message's length in table tblworkload
	public static final int MAX_ERROR_MSG_LENGTH = 1000;

	public SpiderSQLWorkload(String driver, String source, String user, String pwd)
			throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		connection = DriverManager.getConnection(source, user, pwd);
		prepSQLStatement();
	}

	public SpiderSQLWorkload(Connection con) throws SQLException {
		connection = con;
		prepSQLStatement();
	}

	private void prepSQLStatement() throws SQLException {

		prepClear = connection.prepareStatement("DELETE FROM tblWorkload;");
		prepAssign = connection.prepareStatement("SELECT URL FROM tblWorkload WHERE Status = 'W';");
		prepGetStatus = connection.prepareStatement("SELECT Status FROM tblWorkload WHERE URL = ?;");
		prepSetStatus1 = connection.prepareStatement("SELECT count(*) as qty FROM tblWorkload WHERE URL = ?;");
		prepSetStatus2 = connection.prepareStatement("UPDATE tblWorkload SET Status = ?, Depth = ? WHERE URL = ?;");
		prepSetStatus3 = connection.prepareStatement("UPDATE tblWorkload SET Status = ? WHERE URL = ?;");
		prepSetStatus4 = connection.prepareStatement("INSERT INTO tblWorkload(URL,Status,Depth) VALUES (?,?,?);");
		prepSetStatus5 = connection.prepareStatement("UPDATE tblWorkload SET ERROR_MSG = ? WHERE URL = ?;");
		prepGetDepth = connection.prepareStatement("SELECT Depth FROM tblWorkload WHERE URL = ?;");

	}

	synchronized public String assignWorkload() {
		ResultSet rs = null;

		try {
			rs = prepAssign.executeQuery();

			if (!rs.next())
				return null;
			String url = rs.getString("URL");
			setStatus(url, RUNNING);
			return url;
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
		}
		return null;
	}

	synchronized public void addWorkload(String url, int depth) {
		char status = getURLStatus(url);

		if (status == UNKNOWN)
			setNewURL(url, WAITING, depth);

		int d = getURLDepth(url);
		if (depth < d) {
			if (status == WAITING || status == COMPLETE || status == ERROR) {
				setStatus(url, WAITING, depth);
				Log.log(Log.LOG_LEVEL_ERROR, "old status = " + status + " new status = " + WAITING + " old depth = " + d
						+ " new = " + depth + " url = " + url);
			} else {
				setStatus(url, AGAIN, depth);
				Log.log(Log.LOG_LEVEL_ERROR, "old status = " + status + " new status = " + AGAIN + " old depth = " + d
						+ " new = " + depth + " url = " + url);
			}
		}
	}

	synchronized public void completeWorkload(String url, boolean error, String errorMsg) {
		if (error)
			setStatus(url, ERROR);
		else {
			if (getURLStatus(url) == AGAIN)
				setStatus(url, WAITING); // process again
			else
				setStatus(url, COMPLETE);
		}

		if (errorMsg != null && errorMsg.trim().length() > 0) {
			if (errorMsg.length() > MAX_ERROR_MSG_LENGTH)
				errorMsg = errorMsg.substring(0, MAX_ERROR_MSG_LENGTH);
			setErrorMsg(url, errorMsg);
		}
	}

	protected void setStatus(String url, char status) {
		try {
			// Update Status
			prepSetStatus3.setString(1, (String.valueOf(status)));
			prepSetStatus3.setString(2, url);
			prepSetStatus3.executeUpdate();
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		}
	}

	protected void setStatus(String url, char status, int depth) {
		try {
			prepSetStatus2.setString(1, (String.valueOf(status)));
			prepSetStatus2.setInt(2, depth);
			prepSetStatus2.setString(3, url);
			prepSetStatus2.executeUpdate();
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		}
	}

	protected void setNewURL(String url, char status, int depth) {
		try {
			prepSetStatus4.setString(1, url);
			prepSetStatus4.setString(2, (String.valueOf(status)));
			prepSetStatus4.setInt(3, depth);
			prepSetStatus4.executeUpdate();
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		}
	}

	protected void setErrorMsg(String url, String errorMsg) {
		try {
			prepSetStatus5.setString(1, errorMsg);
			prepSetStatus5.setString(2, url);
			prepSetStatus5.executeUpdate();
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		}
	}

	synchronized public char getURLStatus(String url) {
		ResultSet rs = null;

		try {
			// first see if one exists
			prepGetStatus.setString(1, url);
			rs = prepGetStatus.executeQuery();

			if (!rs.next())
				return UNKNOWN;

			return rs.getString("Status").charAt(0);
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
		}
		return UNKNOWN;
	}

	synchronized public int getURLDepth(String url) {
		ResultSet rs = null;

		try {
			// first see if one exists
			prepGetDepth.setString(1, url);
			rs = prepGetDepth.executeQuery();

			if (!rs.next())
				return 0;

			return rs.getInt("Depth");
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}

	synchronized public void clear() {
		try {
			prepClear.executeUpdate();
		} catch (SQLException e) {
			Log.logException("SQL Error: ", e);
		}
	}

	public void releaseResource() {
		try {
			if (prepClear != null)
				prepClear.close();
			if (prepAssign != null)
				prepAssign.close();
			if (prepGetStatus != null)
				prepGetStatus.close();
			if (prepSetStatus1 != null)
				prepSetStatus1.close();
			if (prepSetStatus2 != null)
				prepSetStatus2.close();
			if (prepSetStatus3 != null)
				prepSetStatus3.close();
			if (prepSetStatus4 != null)
				prepSetStatus4.close();
			if (prepSetStatus5 != null)
				prepSetStatus5.close();
			if (prepGetDepth != null)
				prepGetDepth.close();
			// System.out.println("releaseResource");
		} catch (Exception e) {
			Log.logException("releaseResource Error: ", e);
		}
	}
}
