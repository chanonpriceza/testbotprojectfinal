package web.crawler.bot;

import java.util.ArrayList;
import java.util.List;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Album;
import com.restfb.types.Photo;

import bean.ProductUrlBean;



public class FacebookCrawler implements CrawlerInterface {

	public boolean continueProcess() {
		return false;
	}
	
	@Override
	public List<ProductUrlBean> initProductUrl(String startUrl) {		
			
		List<ProductUrlBean> productUrlList = new ArrayList<ProductUrlBean>();
		//FacebookClient facebookClient = new DefaultFacebookClient(token);
		FacebookClient facebookClient = new DefaultFacebookClient(FacebookManager.getInstance().getAccessToken(), com.restfb.Version.VERSION_2_7);	
				
		Connection<Album> albumConn = facebookClient.fetchConnection(startUrl+"/albums", Album.class, Parameter.with("fields", "description"));
		
		for (List<Album> albumList : albumConn) {
			for (Album album : albumList) {
	
				String albumDesc = album.getDescription();			
				String albumId = album.getId();
				
				//System.out.println("albumId = " + albumId);
				//System.out.println("albumDesc = " + albumDesc);
				
				//System.out.println("cat tag = "+ FacebookUtil.parseTag(albumDesc, FacebookUtil.PZ_CAT_TAG));
				
				String albumCatId = FacebookUtil.parseTag(albumDesc, FacebookUtil.PZ_CAT_TAG);
				Connection<Photo> photoConn = facebookClient.fetchConnection(albumId+"/photos", Photo.class);
				
				for (List<Photo> photoList : photoConn) {
					for (Photo photo : photoList) {
						
						String photoName = photo.getName();
									
						if(photoName == null || !photoName.contains(FacebookUtil.PZ_PRICE_TAG)) {
							continue;
						}
						ProductUrlBean p = new ProductUrlBean();
						p.setUrl(photo.getId());
						
						if(albumCatId.trim().length() != 0) {
							try {
								p.setCategoryId(Integer.valueOf(albumCatId));
							} catch (NumberFormatException e) {						
							}				
						}
						
						productUrlList.add(p);
						
					}
				}
				
			}
		}
		
		return productUrlList;
	}
	
	public static void main(String[] args) {
		
		FacebookCrawler fbCrawler = new FacebookCrawler();
		
		fbCrawler.initProductUrl("thepaztel");
		
		
	}

	
}
