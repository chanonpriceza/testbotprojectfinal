package web.crawler.bot;

import java.net.*;

public class SpiderWorker extends Thread {

	protected String target;
	protected int targetDepth;
	protected Spider owner;
	protected boolean busy;
	protected HTTP http;

	public SpiderWorker(Spider owner, HTTP http) {
		this.http = http;
		this.owner = owner;
	}

	public boolean isBusy() {
		return this.busy;
	}

	public void run() {
		for (;;) {
			target = this.owner.getWorkload();
			if (target == null)
				return;
			owner.getSpiderDone().workerBegin();
			processWorkload();
			owner.getSpiderDone().workerEnd();
		}
	}

	public void processWorkload() {
		try {
			busy = true;
			Log.log(Log.LOG_LEVEL_NORMAL, "Spidering " + target);

			if (owner.isPatternMatch(target)) {
				owner.processURL(target);
				return;
			}

			http.send(target, null);
			Attribute typeAttribute = http.getServerHeaders().get("Content-Type");

			// if no content-type at all, its PROBABLY not HTML
			if (typeAttribute == null) {
				owner.completePage(http, false, "no content-type at all, its PROBABLY not HTML");
				return;
			}
			// now check to see if is HTML, ONLY PARSE text type files(namely HTML)
			// owner.processPage(http);

			targetDepth = this.owner.getURLDepth(target);

			if (targetDepth + 1 > owner.getMaxDepth()) {
				owner.completePage(http, false, "");
				return;
			}

			if (!typeAttribute.getValue().startsWith("text/")) {
				owner.completePage(http, false, "Content-Type = " + typeAttribute.getValue());
				return;
			}
			HTMLParser parse = new HTMLParser();

			parse.source = new StringBuffer(http.getBody());
			// find all the links
			while (!parse.eof()) {
				char ch = parse.get();
				if (ch == 0) {
					HTMLTag tag = parse.getTag();
					Attribute link = tag.get("HREF");
					if (link == null)
						link = tag.get("SRC");

					if (link == null)
						continue;

					URL target = null;
					try {
						target = new URL(new URL(this.target), link.getValue());
					} catch (MalformedURLException e) {
						Log.log(Log.LOG_LEVEL_TRACE, "Spider found other link: " + link);
						owner.foundOtherLink(link.getValue(), targetDepth + 1);
						continue;
					}

					if (owner.getRemoveQuery())
						target = URLUtility.stripQuery(target);
					target = URLUtility.stripAnhcor(target);

					if (target.getHost().equalsIgnoreCase(new URL(this.target).getHost())) {
						Log.log(Log.LOG_LEVEL_NORMAL, "Spider found internal link: " + target.toString());
						owner.foundInternalLink(target.toString(), targetDepth + 1);
					} else {
						Log.log(Log.LOG_LEVEL_NORMAL, "Spider found external link: " + target.toString());
						owner.foundExternalLink(target.toString(), targetDepth + 1);
					}
				}
			}
			owner.completePage(http, false, "");
		} catch (java.io.IOException e) {
			String errorMsg = "Error loading file(" + target + "): " + e.toString();
			Log.log(Log.LOG_LEVEL_ERROR, errorMsg);
			owner.completePage(http, true, errorMsg);
		} catch (Exception e) {
			Log.logException("Exception while processing file(" + target + "): ", e);
			owner.completePage(http, true, "Exception while processing file(" + target + "): " + e.toString());
		} finally {
			busy = false;
		}
	}

	public HTTP getHTTP() {
		return http;
	}
}
