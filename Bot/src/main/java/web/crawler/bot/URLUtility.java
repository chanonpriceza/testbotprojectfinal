package web.crawler.bot;

import java.io.*;
import java.net.*;

public class URLUtility {

	public static String indexFile = "index.html";

	private URLUtility() {}

	static public URL stripQuery(URL url) throws MalformedURLException {
		String file = url.getFile();
		int i = file.indexOf("?");
		if (i == -1)
			return url;
		file = file.substring(0, i);
		return new URL(url.getProtocol(), url.getHost(), url.getPort(), file);
	}

	static public URL stripAnhcor(URL url) throws MalformedURLException {
		String file = url.getFile();
		return new URL(url.getProtocol(), url.getHost(), url.getPort(), file);
	}

	static public URL addNewQuery(URL url, String newQuery) throws MalformedURLException {

		String file = url.getPath() + newQuery;
		return new URL(url.getProtocol(), url.getHost(), url.getPort(), file);
	}

	static public URL setQueryString(URL url, String name, String value) throws MalformedURLException {

		String query = url.getQuery();
		if (query == null)
			query = name + "=" + value;
		else {
			if (query.indexOf(name) == -1) {
				query = query + "&" + name + "=" + value;
			} else {

				boolean change = false;
				String[] st = query.split("&");
				for (int i = 0; i < st.length; i++) {
					int eqSign = st[i].indexOf("=");
					if (eqSign != -1) {
						String qName = st[i].substring(0, eqSign);
						if (qName.equalsIgnoreCase(name)) {
							st[i] = st[i].substring(0, eqSign) + "=" + value;
							change = true;
							break;
						}
					}
				}

				if (change == false) {
					query = query + "&" + name + "=" + value;
				} else {
					query = "";
					for (int i = 0; i < st.length; i++) {
						if (i == 0)
							query = st[i];
						else
							query = query + "&" + st[i];
					}
				}
			}
		}

		return new URL(url.getProtocol(), url.getHost(), url.getPort(), url.getPath() + "?" + query);
	}

	static public String getQueryString(URL url, String name) throws MalformedURLException {

		String query = url.getQuery();
		if (query == null)
			return null;

		String[] st = query.split("&");
		for (int i = 0; i < st.length; i++) {
			int eqSign = st[i].indexOf("=");
			if (eqSign != -1) {
				String qName = st[i].substring(0, eqSign);
				if (qName.equalsIgnoreCase(name)) {
					return st[i].substring(eqSign + 1, st[i].length());
				}
			}
		}
		return null;
	}

	static public String removeQueryString(String url, String name) {

		if (url == null || name == null)
			return url;

		if (url.toUpperCase().indexOf(name.toUpperCase()) == -1)
			return url;

		String tempAnhcor = "";
		String query = "";

		if (url.indexOf("#") != -1) {
			tempAnhcor = url.substring(url.indexOf("#"), url.length());
			url = url.substring(0, url.indexOf("#"));
		}

		if (url.indexOf("?") != -1) {
			query = url.substring(url.indexOf("?") + 1, url.length());
			url = url.substring(0, url.indexOf("?"));
		}

		String[] st = query.split("&");
		query = "";
		for (int i = 0; i < st.length; i++) {
			int eqSign = st[i].indexOf("=");
			if (eqSign != -1) {
				String qName = st[i].substring(0, eqSign);
				if (!qName.equalsIgnoreCase(name)) {
					if (query.equals(""))
						query = st[i];
					else
						query = query + "&" + st[i];
				}
			} else {
				if (query.equals(""))
					query = st[i];
				else
					query = query + "&" + st[i];

			}
		}
		if (!query.equals(""))
			query = "?" + query;

		return url + query + tempAnhcor;
	}

	static public boolean haveQueryString(String url, String name, String value) {

		if (url == null || name == null)
			return false;

		if (url.toUpperCase().indexOf(name.toUpperCase()) == -1)
			return false;

		String query = "";

		if (url.indexOf("#") != -1)
			url = url.substring(0, url.indexOf("#"));

		if (url.indexOf("?") != -1)
			query = url.substring(url.indexOf("?") + 1, url.length());

		String[] st = query.split("&");
		for (int i = 0; i < st.length; i++) {
			int eqSign = st[i].indexOf("=");
			if (eqSign != -1) {
				String qName = st[i].substring(0, eqSign);
				String qValue = st[i].substring(eqSign + 1, st[i].length());
				if (qName.equalsIgnoreCase(name)) {
					if (value == null || value.equals("") || qValue.equalsIgnoreCase(value))
						return true;
				}
			}
		}
		return false;
	}

	public static URL formatToValidURL(URL u) throws MalformedURLException {

		if (u == null)
			return null;

		String file = u.getFile();

		while (true) {
			if (file.startsWith("/.."))
				file = file.substring(3);
			else
				break;
		}

		return new URL(u.getProtocol(), u.getHost(), file);
	}

	public static String removeJSessionID(String url) {

		if (url == null)
			return null;

		if (url.indexOf(";jsessionid=") != -1) {

			int index = url.indexOf(";jsessionid=");
			int stopIndex = url.length();
			String start = url.substring(0, index);
			for (int i = index; i < url.length(); i++) {
				if ('?' == url.charAt(i)) {
					stopIndex = i;
					break;
				}
			}

			String stop = url.substring(stopIndex);
			url = start + stop;
		}

		return url;
	}

	public static void main(String[] args) {

		String s = "http://www.shopsanova.com/commartxgen2007.htm;jsessionid=4B4694E702202C20E8A30C3C51B23091?offlineMerchantId=1&ompCategory=2";

		System.out.println(removeJSessionID(s));

	}

	@SuppressWarnings("resource")
	static public String base64Encode(String s) {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		Base64OutputStream out = new Base64OutputStream(bout);
		try {
			out.write(s.getBytes());
			out.flush();
		} catch (IOException e) {
		}

		return bout.toString();
	}

	static public String resolveBase(String base, String rel) {
		String protocol;
		int i = base.indexOf(':');
		if (i != -1) {
			protocol = base.substring(0, i + 1);
			base = "http:" + base.substring(i + 1);
		} else
			protocol = null;

		URL url;

		try {
			url = new URL(new URL(base), rel);
		} catch (MalformedURLException e) {
			return "";
		}

		if (protocol != null) {
			base = url.toString();
			i = base.indexOf(':');
			if (i != -1)
				base = base.substring(i + 1);
			base = protocol + base;
			return base;
		} else
			return url.toString();
	}

	public static String convertFilename(String base, String path) {
		return convertFilename(base, path, true);
	}

	public static String convertFilename(String base, String path, boolean mkdir) {
		String result = base;
		int index1;
		int index2;

		// add ending slash if needed
		if (result.charAt(result.length() - 1) != File.separatorChar)
			result = result + File.separator;

		// strip the query if needed

		int queryIndex = path.indexOf("?");
		if (queryIndex != -1)
			path = path.substring(0, queryIndex);

		// see if an ending / is missing from a directory only

		int lastSlash = path.lastIndexOf(File.separatorChar);
		int lastDot = path.lastIndexOf('.');

		if (path.charAt(path.length() - 1) != '/') {
			if (lastSlash > lastDot)
				path += "/" + indexFile;
		}

		// determine actual filename
		lastSlash = path.lastIndexOf('/');

		String filename = "";
		if (lastSlash != -1) {
			filename = path.substring(1 + lastSlash);
			path = path.substring(0, 1 + lastSlash);

			if (filename.equals(""))
				filename = indexFile;
		}
		// create the directory structure, if needed

		index1 = 1;
		do {
			index2 = path.indexOf('/', index1);

			if (index2 != -1) {
				String dirpart = path.substring(index1, index2);
				result += dirpart;
				result += File.separator;

				if (mkdir) {
					File f = new File(result);
					f.mkdir();
				}

				index1 = index2 + 1;

			}
		} while (index2 != -1);

		// attach name
		result += filename;

		return result;
	}
	
}
