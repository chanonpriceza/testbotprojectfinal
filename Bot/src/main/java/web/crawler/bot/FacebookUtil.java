package web.crawler.bot;

import utils.FilterUtil;

public class FacebookUtil {

	public static final String PZ_CAT_TAG = "pz-cat-";
	public static final String PZ_NAME_TAG = "pz-name-";
	public static final String PZ_PRICE_TAG = "pz-price-";
	
	
	public static String parseTag(String target, String tag) {
		
		if(target == null) {
			return "";
		}
		if(target.contains(tag)) {	    	
	    	String tmp = FilterUtil.getStringAfter(target, tag, "");	    	
	    	tmp = FilterUtil.getStringBefore(tmp, "\n", tmp);
	    	tmp = FilterUtil.getStringBefore(tmp, " ", tmp);
	    	
	    	return tmp.trim();	    	
	    } else {
	    	return "";
	    }		
	}	
	
	public static String parseTagWithSpace(String target, String tag) {
		
		if(target == null) {
			return "";
		}
		if(target.contains(tag)) {	    	
	    	String tmp = FilterUtil.getStringAfter(target, tag, "");	    	
	    	tmp = FilterUtil.getStringBefore(tmp, "\n", tmp);
	    	
	    	return tmp.trim();	    	
	    } else {
	    	return "";
	    }		
	}	
	
}
