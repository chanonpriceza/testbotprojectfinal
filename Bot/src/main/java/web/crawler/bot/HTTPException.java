package web.crawler.bot;

public class HTTPException extends java.io.IOException {
    
	private static final long serialVersionUID = 1L;

    public HTTPException() {}
    
    public HTTPException(String msg) {
        super(msg);
    }
    
}
