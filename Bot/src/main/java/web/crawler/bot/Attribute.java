package web.crawler.bot;

public class Attribute implements Cloneable {

	private String name;
	private String value;
	private char delim;

	public Object clone() {
		return new Attribute(name, value, delim);
	}

	public Attribute(String name, String value, char delim) {
		this.name = name;
		this.value = value;
		this.delim = delim;
	}

	public Attribute() {
		this("", "", (char) 0);
	}

	public Attribute(String name, String value) {
		this(name, value, (char) 0);
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public char getDelim() {
		return delim;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setDelim(char ch) {
		delim = ch;
	}
}
