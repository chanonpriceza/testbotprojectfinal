package web.crawler.bot;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.BotExclusion;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HTTP;
import web.crawler.bot.HTTPSocket;
import web.crawler.bot.URLUtility;

import engine.BaseConfig;
import manager.UrlPatternManager;

public class HeatonURLCrawler implements URLCrawlerInterface {
	
	protected HTTP http;
    protected BotExclusion botExclusion;
    protected URLCrawlerClient client;
    boolean enableCookies;
    boolean checkInit;
    
    public HeatonURLCrawler() {
    	
    }
    
    public HeatonURLCrawler(boolean check) {
    	checkInit = check;
    }
    
	public void init(URLCrawlerClient client) throws Exception {
		
		this.client = client;
		
		http = new HTTPSocket();
        if(BaseConfig.CONF_ENABLE_COOKIES) {
        	http.setUseCookies(true, false);
        }
        
        if(BaseConfig.CONF_IGNORE_HTTP_ERROR) {
            http.setIgnoreHttpError(true);        	
        }

        try{
        	
        	botExclusion = new BotExclusion();
        	if(checkInit) {
	            
	        	String startUrl = UrlPatternManager.getInstance().getFirstStartUrl();
	            if(StringUtils.isBlank(startUrl)) {
	            	throw new Exception("not found start url");
	            }
	            
	            try {
	            	botExclusion.load(http, startUrl);
	            } catch(Exception e) {
	            	// cannot load robots.txt            	
	            }
	            
	        }
        }catch(Exception e){
        	e.printStackTrace();
        	throw e;
        }
        
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        
		try {		
		        
				http.send(requestUrl, null);		
		        Attribute typeAttribute = http.getServerHeaders().get("Content-Type");
		
		        // if no content-type at all, its PROBABLY not HTML
		        if (typeAttribute == null) {
		        	completePage(targetMerchantId, http, STATUS.C, targetUrl);
		            return;
		        }
		
		        if (!typeAttribute.getValue().startsWith("text/")) {
		            completePage(targetMerchantId, http, STATUS.C, targetUrl);
		            return;
		        }
		        String charset = null;
		        
		        
		        if(!isBlank(BaseConfig.CONF_URL_CRAWLER_CHARSET)) {
		        	charset = BaseConfig.CONF_URL_CRAWLER_CHARSET;            	
		        } else {
		        	if(typeAttribute.getValue().toUpperCase().indexOf("UTF-8") != -1) {
		            	charset = "UTF-8";
		            } else {
		            	//default charset
		            }
		        }            
		        
		        HTMLParser parse = new HTMLParser();
		        if(charset != null) {
		        	parse.source = new StringBuffer(processHTMLBeforeParse(http.getBody(charset)));
		        } else {
		        	parse.source = new StringBuffer(processHTMLBeforeParse(http.getBody()));
		        }
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(targetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
		                	 if(!botExclusion.isExcluded(u.toString())){
		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     }		                	
		                }
		            }
		        }
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());		
		} catch (Exception e) {
			if (!http.getURL().equals(http.getRootURL())) {
	        	client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        } 
		    throw e;
		}
	}

	public String processURLAfterParse(String url) throws Exception {
		return url;
	}
	
	public String processHTMLBeforeParse(String html) throws Exception {
		return html;
	}

	public void completePage(int merchantId, HTTP page, STATUS status, String url) throws SQLException {

		client.completePage(merchantId, url, status);
		
        // if this was a redirect, then also complete the root page
        if (!page.getURL().equals(page.getRootURL())) {
        	client.completePage(merchantId, page.getRootURL(), status);
        }
    }
	
}
