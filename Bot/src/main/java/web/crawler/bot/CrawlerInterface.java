package web.crawler.bot;

import java.util.List;

import bean.ProductUrlBean;

public interface CrawlerInterface {

	
	public List<ProductUrlBean> initProductUrl(String startUrl);

	public boolean continueProcess();

}
