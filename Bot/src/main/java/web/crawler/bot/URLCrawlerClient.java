package web.crawler.bot;

import java.sql.SQLException;

import bean.WorkloadBean.STATUS;

public interface URLCrawlerClient{
	
	public void completePage(int merchantId, String url, STATUS status) throws SQLException;
	
	public void foundInternalLink(int merchantId, String url, int depth, int categoryId, String keyword) throws SQLException;
    
}
