package web.crawler.bot;

import bean.WorkloadBean;

public interface URLCrawlerInterface{
	
	public void process(WorkloadBean target) throws Exception;

	public void init(URLCrawlerClient client) throws Exception;

}
