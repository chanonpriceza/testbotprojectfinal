package web.crawler.bot;

public interface ISpiderReportable {

	public boolean foundInternalLink(String url, int depth);

	public boolean foundExternalLink(String url, int depth);

	public boolean foundOtherLink(String url, int depth);

	public void processPage(HTTP page);

	public void completePage(HTTP page, boolean error, String errorMsg);

	public boolean getRemoveQuery();

	public void processURL(String url);

	public boolean isPatternMatch(String url);

	public void spiderComplete();

}
