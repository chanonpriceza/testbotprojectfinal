package web.crawler.bot;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.AccessToken;

public class FacebookManager {

	private static FacebookManager facebookManager;
	//App Priceza.com 
	//Facebook Account ->  Username : pzdev.fb@gmail.com, Password : Priceza@27

	private static final String FACEBOOK_APP_ID = "127483620599219";
	private static final String FACEBOOK_APP_SECRET = "36b331f870eeb4c29ffac91bf2d4309c";
	
	private String accessToken;
	
	public String getAccessToken() {
		return accessToken;
	}

	public static synchronized FacebookManager getInstance() {
		
		if(facebookManager == null) {			
			facebookManager = new FacebookManager();	
			FacebookClient facebookClient = new DefaultFacebookClient(com.restfb.Version.VERSION_2_7);
			AccessToken aToken = facebookClient.obtainAppAccessToken(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);			
			facebookManager.accessToken = aToken.getAccessToken();
		}
		
		return facebookManager;
	}	
		
	
}
