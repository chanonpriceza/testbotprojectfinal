package web.crawler.bot;

import java.util.zip.*;
import java.net.*;
import java.io.*;

public abstract class HTTP {

	protected byte body[];
	protected StringBuffer header = new StringBuffer();
	protected String url;
	protected boolean autoRedirect = true;
	protected AttributeList cookieStore = new AttributeList();
	protected AttributeList clientHeaders = new AttributeList();
	protected AttributeList serverHeaders = new AttributeList();
	protected boolean useCookies = false;
	protected boolean usePermCookies = false;
	protected String response;
	protected int timeout = 30000;
	protected String referrer = null;
	protected String agent = "Mozilla/5.0 (Windows NT 5.1; rv:8.0.1) Gecko/20100101 Firefox/8.0.1";
	protected String user = "";
	protected String password = "";
	protected int maxBodySize = -1;
	protected String rootURL;
	protected int err;
	
	abstract HTTP copy();

	abstract protected void lowLevelSend(String url, String post) throws java.net.UnknownHostException, java.io.IOException;

	public void clearCookies() {
		cookieStore.clear();
	}

	protected void addCookieHeader() {
		int i = 0;
		String str = "";

		if (cookieStore.get(0) == null)
			return;

		while (cookieStore.get(i) != null) {
			CookieParse cookie;
			if (str.length() != 0)
				str += "; ";// add on ; if needed
			cookie = (CookieParse) cookieStore.get(i);
			str += cookie.toString();
			i++;
		}

		clientHeaders.set("Cookie", str);
	}

	protected void addAuthHeader() {
		if ((user.length() == 0) || (password.length() == 0))
			return;
		String hdr = user + ":" + password;
		String encode = URLUtility.base64Encode(hdr);
		clientHeaders.set("Authorization", "Basic " + encode);
	}

	public void send(String requestedURL, String post) throws HTTPException, java.net.UnknownHostException, java.io.IOException {

		rootURL = requestedURL;
		setURL(requestedURL);

		if (clientHeaders.get("referrer") == null)
			if (referrer != null)
				clientHeaders.set("referrer", referrer.toString());

		if (clientHeaders.get("Accept") == null)
			clientHeaders.set("Accept",
					"image/gif," + "image/x-xbitmap,image/jpeg, " + "image/pjpeg, application/vnd.ms-excel,"
							+ "application/msword," + "application/vnd.ms-powerpoint, */*");

		if (clientHeaders.get("Accept-Language") == null)
			clientHeaders.set("Accept-Language", "en-us");
		if (clientHeaders.get("User-Agent") == null)
			clientHeaders.set("User-Agent", agent);

		while (true) {
			if (useCookies)
				addCookieHeader();
			addAuthHeader();
			lowLevelSend(this.url, post);

			if (useCookies)
				parseCookies();

			Attribute encoding = serverHeaders.get("Content-Encoding");
			if ((encoding != null) && "gzip".equalsIgnoreCase(encoding.getValue()))
				processGZIP();

			Attribute a = serverHeaders.get("Location");

			if ((a == null) || !autoRedirect) {
				referrer = getURL();
				return;
			}

			URL u = new URL(new URL(url), a.getValue());
			setURL(u.toString());
		}
	}

	public String getBody() {
		return new String(body);
	}

	public String getBody(String enc) throws UnsupportedEncodingException {
		return new String(body, enc);
	}

	public byte[] getBodyBytes() {
		return body;
	}

	public String getURL() {
		return url;
	}

	public void setURL(String u) {
		url = u;
	}

	public void SetAutoRedirect(boolean b) {
		autoRedirect = b;
	}

	public AttributeList getClientHeaders() {
		return clientHeaders;
	}

	public AttributeList getServerHeaders() {
		return serverHeaders;
	}

	protected void parseCookies() {
		Attribute a;

		int i = 0;
		while ((a = serverHeaders.get(i++)) != null) {
			if (a.getName().equalsIgnoreCase("set-cookie")) {
				CookieParse cookie = new CookieParse();

				cookie.source = new StringBuffer(a.getValue());
				cookie.get();
				cookie.setName(cookie.get(0).getName());
				if (cookieStore.get(cookie.get(0).getName()) == null) {
					if ((cookie.get("expires") == null) || (cookie.get("expires") != null) && usePermCookies)
						cookieStore.add(cookie);
				}

			}
		}
	}

	protected void processGZIP() throws IOException {
		ByteArrayInputStream bis = new ByteArrayInputStream(body);
		GZIPInputStream is = new GZIPInputStream(bis);

		ByteList byteList = new ByteList();
		byteList.read(is, -1);
		body = byteList.detach();
	}

	public CookieParse getCookie(String name) {
		return ((CookieParse) cookieStore.get(name));
	}

	public void setUseCookies(boolean session, boolean perm) {
		useCookies = session;
		usePermCookies = perm;
	}

	public boolean getUseCookies() {
		return useCookies;
	}

	public boolean getPerminantCookies() {
		return usePermCookies;
	}

	protected void processResponse(String name) throws java.io.IOException {
		int i1, i2;
		response = name;
		i1 = response.indexOf(' ');
		if (i1 != -1) {
			i2 = response.indexOf(' ', i1 + 1);
			if (i2 != -1) {
				try {
					err = Integer.parseInt(response.substring(i1 + 1, i2));
				} catch (Exception e) {
				}
			}
		}

	}

	protected void parseHeaders() throws java.io.IOException {
		boolean first;
		String name, value;
		int l;

		first = true;

		serverHeaders.clear();
		name = "";

		String parse = new String(header);

		for (l = 0; l < parse.length(); l++) {
			if (parse.charAt(l) == '\n') {
				if (name.length() == 0)
					return;
				else {
					if (first) {
						first = false;
						processResponse(name);
					} else {
						int ptr = name.indexOf(':');
						if (ptr != -1) {
							value = name.substring(ptr + 1).trim();
							name = name.substring(0, ptr);
							Attribute a = new Attribute(name, value);
							serverHeaders.add(a);

						}
					}
				}
				name = "";
			} else if (parse.charAt(l) != '\r')
				name += parse.charAt(l);
		}
	}

	public void setTimeout(int i) {
		timeout = i;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setMaxBody(int i) {
		maxBodySize = i;
	}

	public int getMaxBody() {
		return maxBodySize;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String a) {
		agent = a;
	}

	public void setUser(String u) {
		user = u;
	}

	public void setPassword(String p) {
		password = p;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String p) {
		referrer = p;
	}

	public AttributeList getCookies() {
		return cookieStore;
	}

	public String getRootURL() {
		return rootURL;
	}

	public String getContentType() {
		Attribute a = serverHeaders.get("Content-Type");
		if (a == null)
			return null;
		else
			return a.getValue();
	}

	protected boolean ignoreHttpError;

	public void setIgnoreHttpError(boolean ignoreHttpError) {
		this.ignoreHttpError = ignoreHttpError;
	}

}
