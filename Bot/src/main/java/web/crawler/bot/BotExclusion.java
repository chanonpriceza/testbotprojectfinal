package web.crawler.bot;

import java.util.*;
import java.net.*;
import java.io.*;

public class BotExclusion {

	protected String robotFile;
	protected Vector<String> exclude = new Vector<String>();

	public void load(HTTP http, String url) throws MalformedURLException, UnknownHostException, java.io.IOException {
		String str;
		boolean active = false;

		URL u = new URL(url);
		URL u2 = new URL(u.getProtocol(), u.getHost(), u.getPort(), "/robots.txt");
		robotFile = u2.toString();
		http.send(robotFile, null);

		StringReader sr = new StringReader(http.getBody());
		BufferedReader r = new BufferedReader(sr);
		while ((str = r.readLine()) != null) {
			str = str.trim();
			if (str.length() < 1)
				continue;
			if (str.charAt(0) == '#')
				continue;
			int i = str.indexOf(':');
			if (i == -1)
				continue;
			String command = str.substring(0, i);
			String rest = str.substring(i + 1).trim();
			if (command.equalsIgnoreCase("User-agent")) {
				active = false;
				if (rest.equals("*"))
					active = true;
				else {
					if (rest.equalsIgnoreCase(http.getAgent()))
						active = true;
				}
			}
			if (active) {
				if (command.equalsIgnoreCase("disallow")) {
					URL u3 = new URL(new URL(robotFile), rest);
					if (!isExcluded(u3.toString()))
						exclude.addElement(u3.toString());
				}
			}
		}
	}

	public boolean isExcluded(String url) {
		for (Enumeration<String> e = exclude.elements(); e.hasMoreElements();) {
			String str = (String) e.nextElement();
			if (str.startsWith(url))
				return true;
		}
		return false;
	}

	public Vector<String> getExclude() {
		return exclude;
	}

	public String getRobotFile() {
		return robotFile;
	}
}
