package web.crawler.bot;

public interface IWorkloadStorable {

	public static final char RUNNING = 'R';
	public static final char ERROR = 'E';
	public static final char WAITING = 'W';
	public static final char COMPLETE = 'C';
	public static final char UNKNOWN = 'U';
	public static final char AGAIN = 'A';

	public String assignWorkload();

	public void addWorkload(String url, int depth);

	public void completeWorkload(String url, boolean error, String errorMsg);

	public char getURLStatus(String url);

	public int getURLDepth(String url);

	public void clear();

	public void releaseResource();

}
