package web.crawler.bot;

import java.net.*;

public class HTMLForm extends AttributeList {

	protected String method;
	protected String action;

	public HTMLForm(String method, String action) {
		this.method = method;
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public String getMethod() {
		return method;
	}

	public void addInput(String name, String value, String type, String prompt, AttributeList options) {
		FormElement e = new FormElement();
		e.setName(name);
		e.setValue(value);
		e.setType(type.toUpperCase());
		e.setOptions(options);
		e.setPrompt(prompt);
		add(e);
	}

	@SuppressWarnings("deprecation")
	public String toString() {
		String postdata;

		postdata = "";
		int i = 0;
		while (get(i) != null) {
			Attribute a = get(i);
			if (postdata.length() > 0)
				postdata += "&";
			postdata += a.getName();
			postdata += "=";
			if (a.getValue() != null)
				postdata += URLEncoder.encode(a.getValue());
			i++;
		}
		return postdata;
	}

	public class FormElement extends Attribute {
		protected String type;
		protected AttributeList options;
		protected String prompt;

		public void setOptions(AttributeList options) {
			this.options = options;
		}

		public AttributeList getOptions() {
			return options;
		}

		public void setType(String t) {
			type = t;
		}

		public String getType() {
			return type;
		}

		public String getPrompt() {
			return prompt;
		}

		public void setPrompt(String str) {
			prompt = str;
		}
	}
}
