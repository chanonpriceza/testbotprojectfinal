package web.crawler.bot;

class SpiderDone {

	private int activeThreads = 0;

	private boolean started = false;

	synchronized public void waitDone() {
		try {
			while (activeThreads > 0) {
				wait();
			}
		} catch (InterruptedException e) {
		}
	}

	synchronized public void waitBegin() {
		try {
			while (!started) {
				wait();
			}
		} catch (InterruptedException e) {
		}
	}

	synchronized public void workerBegin() {
		activeThreads++;
		started = true;
		notify();
	}

	synchronized public void workerEnd() {
		activeThreads--;
		notify();
	}

	synchronized public void reset() {
		activeThreads = 0;
	}

}
