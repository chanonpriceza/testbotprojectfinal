package web.crawler.bot;

import java.util.*;

public class SpiderInternalWorkload implements IWorkloadStorable {

	Hashtable<String, String> complete = new Hashtable<String, String>();
	Vector<String> waiting = new Vector<String>();
	Vector<String> running = new Vector<String>();

	synchronized public String assignWorkload() {
		if (waiting.size() < 1)
			return null;

		String w = (String) waiting.firstElement();
		if (w != null) {
			waiting.remove(w);
			running.addElement(w);
		}
		Log.log(Log.LOG_LEVEL_TRACE, "Spider workload assigned:" + w);
		return w;
	}

	synchronized public void addWorkload(String url, int depth) {
		if (getURLStatus(url) != IWorkloadStorable.UNKNOWN)
			return;
		waiting.addElement(url);
		Log.log(Log.LOG_LEVEL_TRACE, "Spider workload added:" + url);
	}

	synchronized public void completeWorkload(String url, boolean error, String errorMsg) {
		running.remove(url);

		if (error) {
			Log.log(Log.LOG_LEVEL_TRACE, "Spider workload ended in error:" + url);
			complete.put(url, "e");
		} else {
			Log.log(Log.LOG_LEVEL_TRACE, "Spider workload complete:" + url);
			complete.put(url, "c");
		}
	}

	synchronized public char getURLStatus(String url) {
		if (complete.get(url) != null)
			return COMPLETE;

		if (waiting.size() > 0) {
			for (Enumeration<String> e = waiting.elements(); e.hasMoreElements();) {
				String w = (String) e.nextElement();
				if (w.equals(url))
					return WAITING;
			}
		}

		if (running.size() > 0) {
			for (Enumeration<String> e = running.elements(); e.hasMoreElements();) {
				String w = (String) e.nextElement();
				if (w.equals(url))
					return RUNNING;
			}
		}

		return UNKNOWN;
	}

	synchronized public int getURLDepth(String url) {
		return 0;
	}

	synchronized public void clear() {
		waiting.clear();
		complete.clear();
		running.clear();
	}

	public void releaseResource() {

	}
}
