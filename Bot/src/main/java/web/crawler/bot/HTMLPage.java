package web.crawler.bot;

import java.util.*;
import java.net.*;
import java.io.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

public class HTMLPage {

	protected Vector<String> images = new Vector<String>();
	protected Vector<Link> links = new Vector<Link>();
	protected Vector<HTMLForm> forms = new Vector<HTMLForm>();
	protected HTTP http;
	protected String base;

	public HTMLPage(HTTP http) {
		this.http = http;
	}

	public void open(String url, HTMLEditorKit.ParserCallback callback) throws IOException, BadLocationException {
		http.send(url, null);
		base = url;
		processPage(callback);
	}

	protected void processPage(HTMLEditorKit.ParserCallback callback) throws IOException {
		StringReader r = new StringReader(http.getBody());
		HTMLEditorKit.Parser parse = new HTMLParse().getParser();

		if (callback == null) {
			HTMLPage.Parser p = new HTMLPage.Parser();
			parse.parse(r, p, true);
		} else
			parse.parse(r, callback, false);

	}

	public HTTP getHTTP() {
		return http;
	}

	public Vector<Link> getLinks() {
		return links;
	}

	public Vector<String> getImages() {
		return images;
	}

	public Vector<HTMLForm> getForms() {
		return forms;
	}

	public void post(HTMLForm form) throws IOException {
		http.getClientHeaders().set("Content-Type", "application/x-www-form-urlencoded");
		http.send(form.getAction(), form.toString());
		processPage(null);
	}

	public String getURL() {
		return http.getURL();
	}

	protected void addImage(String img) {
		img = URLUtility.resolveBase(base, img);
		for (int i = 0; i < images.size(); i++) {
			String s = (String) images.elementAt(i);
			if (s.equalsIgnoreCase(img))
				return;
		}
		images.addElement(img);
	}

	protected class Parser extends HTMLEditorKit.ParserCallback {

		protected HTMLForm tempForm;
		protected AttributeList tempOptions;
		protected Attribute tempElement = new Attribute();
		protected String tempPrompt = "";
		protected Link tempLink;
		
		public void handleComment(char[] data, int pos) {}
		
		public void handleEndTag(HTML.Tag t, int pos) {
			if (t == HTML.Tag.OPTION) {
				if (tempElement != null) {
					tempElement.setName(tempPrompt);
					tempOptions.add(tempElement);
					tempPrompt = "";
				}
				tempElement = null;
			} else if (t == HTML.Tag.FORM) {
				if (tempForm != null)
					forms.addElement(tempForm);
				tempPrompt = "";
			} else if (t == HTML.Tag.A) {
				if (tempLink != null)
					tempLink.setPrompt(tempPrompt);
				tempPrompt = "";
			}

		}

		public void handleError(String errorMsg, int pos) {}

		public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			handleStartTag(t, a, pos);
		}

		public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			String type = "";

			// is it some sort of a link
			String href = (String) a.getAttribute(HTML.Attribute.HREF);

			if ((href != null) && (t != HTML.Tag.BASE)) {
				String alt = (String) a.getAttribute(HTML.Attribute.ALT);
				Link link = new Link(alt, URLUtility.resolveBase(base, href), null);
				links.addElement(tempLink = link);
			} else if (t == HTML.Tag.OPTION) {
				tempElement = new Attribute();
				tempElement.setName("");
				tempElement.setValue((String) a.getAttribute(HTML.Attribute.VALUE));
			} else if (t == HTML.Tag.SELECT) {
				if (tempForm == null)
					return;

				tempOptions = new AttributeList();
				tempForm.addInput((String) a.getAttribute(HTML.Attribute.NAME), null, "select", tempPrompt,
						tempOptions);
				tempPrompt = "";
			} else if (t == HTML.Tag.TEXTAREA) {
				if (tempForm == null)
					return;

				tempForm.addInput((String) a.getAttribute(HTML.Attribute.NAME), null, "textarea", tempPrompt, null);
				tempPrompt = "";
			}

			else if (t == HTML.Tag.FORM) {
				if (tempForm != null)
					forms.addElement(tempForm);

				String action = (String) a.getAttribute(HTML.Attribute.ACTION);
				if (action != null) {
					try {
						URL aurl = new URL(new URL(http.getURL()), action);
						action = aurl.toString();
					} catch (MalformedURLException e) {
						action = null;
					}
				}

				tempForm = new HTMLForm((String) a.getAttribute(HTML.Attribute.METHOD), action);
				tempPrompt = "";
			} else if (t == HTML.Tag.INPUT) {
				if (tempForm == null)
					return;
				if (t != HTML.Tag.INPUT) {
					type = (String) a.getAttribute(HTML.Attribute.TYPE);
					if (type == null)
						return;
				} else
					type = "select";

				if (type.equalsIgnoreCase("text") || type.equalsIgnoreCase("edit") || type.equalsIgnoreCase("password")
						|| type.equalsIgnoreCase("select") || type.equalsIgnoreCase("hidden")) {
					tempForm.addInput((String) a.getAttribute(HTML.Attribute.NAME),
							(String) a.getAttribute(HTML.Attribute.VALUE), type, tempPrompt, null);
					tempOptions = new AttributeList();
				}
			} else if (t == HTML.Tag.BASE) {
				href = (String) a.getAttribute(HTML.Attribute.HREF);
				if (href != null)
					base = href;
			} else if (t == HTML.Tag.IMG) {
				String src = (String) a.getAttribute(HTML.Attribute.SRC);
				if (src != null)
					addImage(src);
			}

		}

		public void handleText(char[] data, int pos) {
			tempPrompt += new String(data) + " ";
		}

	}
}
