package web.crawler.bot;

import java.lang.reflect.*;

public class Spider extends Thread implements ISpiderReportable {
	protected IWorkloadStorable workload;
	protected SpiderWorker pool[];
	protected boolean worldSpider;
	protected ISpiderReportable manager;
	protected boolean halted = false;
	protected SpiderDone done = new SpiderDone();
	protected int maxBodySize;
	protected int maxdepth;

	public Spider(ISpiderReportable manager, String url, HTTP http, int poolSize, int maxdepth) {
		this(manager, url, http, poolSize, new SpiderInternalWorkload(), maxdepth);
	}

	public Spider(ISpiderReportable manager, String url, HTTP http, int poolSize, IWorkloadStorable w, int maxdepth) {
		try {
			init(manager, url, http.getClass(), SpiderWorker.class, poolSize, w, maxdepth);
		}
		// mostly ignore the exceptions since we're using the standard SpiderWorker
		// stuff
		catch (InstantiationException e) {
			Log.logException("Spider reflection exception", e);
		} catch (NoSuchMethodException e) {
			Log.logException("Spider reflection exception", e);
		} catch (IllegalAccessException e) {
			Log.logException("Spider reflection exception", e);
		} catch (InvocationTargetException e) {
			Log.logException("Spider reflection exception", e);
		}

	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private Spider(ISpiderReportable manager, String url, Class http, Class worker, int poolSize, IWorkloadStorable w, int maxdepth)
			throws InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		init(manager, url, http, worker, poolSize, w, maxdepth);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void init(ISpiderReportable manager, String url, Class http, Class worker, int poolSize,
			IWorkloadStorable w, int maxdepth)
			throws InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		this.manager = manager;
		this.maxdepth = maxdepth;
		worldSpider = false;

		Class types[] = { Spider.class, HTTP.class };
		Constructor constructor = worker.getConstructor(types);

		pool = new SpiderWorker[poolSize];
		for (int i = 0; i < pool.length; i++) {
			HTTP hc = (HTTP) http.newInstance();
			Object params[] = { this, hc };

			pool[i] = (SpiderWorker) constructor.newInstance(params);
		}
		workload = w;
		if (url.length() > 0) {
			workload.clear();
			addWorkload(url, 0);
		}
	}

	public SpiderDone getSpiderDone() {
		return done;
	}

	public void run() {
		if (halted)
			return;
		for (int i = 0; i < pool.length; i++)
			pool[i].start();

		try {
			done.waitBegin();
			done.waitDone();
			Log.log(Log.LOG_LEVEL_NORMAL, "Spider has no work.");
			spiderComplete();

			for (int i = 0; i < pool.length; i++) {
				pool[i].interrupt();
				pool[i].join();
				pool[i] = null;
			}

		} catch (Exception e) {
			Log.logException("Exception while starting spider", e);
		}

	}

	synchronized public String getWorkload() {
		try {
			for (;;) {
				if (halted)
					return null;
				String w = workload.assignWorkload();
				if (w != null)
					return w;
				wait();
			}
		} catch (java.lang.InterruptedException e) {
		}
		return null;
	}

	synchronized public int getURLDepth(String url) {
		if (halted)
			return 0;
		return workload.getURLDepth(url);
	}

	synchronized public void addWorkload(String url, int depth) {
		if (halted)
			return;
		workload.addWorkload(url, depth);
		notify();
	}

	public void setWorldSpider(boolean b) {
		worldSpider = b;
	}

	public boolean getWorldSpider() {
		return worldSpider;
	}

	synchronized public boolean foundInternalLink(String url, int depth) {
		if (depth > maxdepth)
			return false;

		if (manager.foundInternalLink(url, depth))
			addWorkload(url, depth);

		return true;
	}

	synchronized public boolean foundExternalLink(String url, int depth) {
		if (worldSpider) {
			foundInternalLink(url, depth);
			return true;
		}

		if (depth > maxdepth)
			return false;

		if (manager.foundExternalLink(url, depth))
			addWorkload(url, depth);
		return true;
	}

	synchronized public boolean foundOtherLink(String url, int depth) {
		if (depth > maxdepth)
			return false;

		if (manager.foundOtherLink(url, depth))
			addWorkload(url, depth);
		return true;
	}

	synchronized public void processPage(HTTP page) {
		manager.processPage(page);
	}

	synchronized public void processURL(String url) {
		manager.processURL(url);
		workload.completeWorkload(url, false, "");
	}

	synchronized public boolean isPatternMatch(String url) {
		return manager.isPatternMatch(url);
	}

	synchronized public boolean getRemoveQuery() {
		// return true;
		return false;
	}

	synchronized public void completePage(HTTP page, boolean error, String errorMsg) {
		workload.completeWorkload(page.getURL(), error, errorMsg);

		// if this was a redirect, then also complete the root page
		if (page.getURL().equals(page.getRootURL()))
			workload.completeWorkload(page.getRootURL(), error, errorMsg);
	}

	synchronized public void spiderComplete() {
		workload.releaseResource(); // Add by ha
		manager.spiderComplete();
	}

	synchronized public void halt() {
		halted = true;
		workload.clear();
		notifyAll();
	}

	public boolean isHalted() {
		return halted;
	}

	public void setMaxBody(int mx) {
		maxBodySize = mx;
		for (int i = 0; i < pool.length; i++)
			pool[i].getHTTP().setMaxBody(mx);
	}

	public int getMaxBody() {
		return maxBodySize;
	}

	public int getMaxDepth() {
		return maxdepth;
	}

}
