package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class ITrueMartHotDealURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		return html.replace("data-url=\"", "href=\"");
	}
		
}