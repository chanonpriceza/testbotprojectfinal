package web.crawler.impl;

import java.util.ArrayList;
import java.util.List;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class SuperTStoreURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String c = "";
		String ca = FilterUtil.getStringBetween(html, "<ol class=\"breadcrumb no-hide\">", "</ol>");
		List<String> listC = FilterUtil.getAllStringBetween(ca, "<a href=\"/products/category/", "\"");
		
		if(listC != null && listC.size() > 0){
			for (String cat : listC) {
				c = cat;
			}
		}
		
		String pagination  = FilterUtil.getStringBetween(html, "<ul class=\"pagination\">", "</ul>");
		if(StringUtils.isNotBlank(pagination)){
			pagination  = FilterUtil.getStringAfter(pagination, "<li> <a>...</a> </li>", pagination);
			pagination =  FilterUtil.getStringBetween(pagination, "javascript:paginavigation(",")");
			if(StringUtils.isNotBlank(pagination) && StringUtils.isNumeric(pagination)){
				List<String> newString = new ArrayList<String>();
				int totalPage = Integer.parseInt(pagination);
				for(int page= 1;page <=totalPage;page++){
					String link = "<a href=\"http://supertstore.com/products/category/"+c+"?page="+page+"\"></a>";
					newString.add(link);
				}
				return StringEscapeUtils.unescapeHtml4(html + newString.toString());
			}
		}
		
		return StringEscapeUtils.unescapeHtml4(html);
	}

	
}