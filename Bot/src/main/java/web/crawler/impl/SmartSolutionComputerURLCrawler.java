package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class SmartSolutionComputerURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
				
		//parse content zone
		
		String rtnHtml = html.replaceAll("onclick=\"location.href='", "<a href=\"");
		rtnHtml = rtnHtml.replaceAll("'\">", "\">");
		rtnHtml = rtnHtml.replaceAll("'\" src=", "\">");
		return rtnHtml;		
	}
}
