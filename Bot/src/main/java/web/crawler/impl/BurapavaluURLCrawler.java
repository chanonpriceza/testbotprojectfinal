package web.crawler.impl;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import web.crawler.bot.HeatonURLCrawler;

public class BurapavaluURLCrawler extends HeatonURLCrawler{
		
	@Override
	public void process(WorkloadBean target) throws Exception{
		
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        
		try {		
				client.foundInternalLink(targetMerchantId, requestUrl, targetDepth + 1, targetCategoryId, targetKeyword);
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());		
		} catch (Exception e) {
			if (!http.getURL().equals(http.getRootURL())) {
	        	client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        } 
		    throw e;
		}
	}

}
