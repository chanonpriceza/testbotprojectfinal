package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class OnehundredToHomeURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String trn = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("ul.products-grid");
		e = e.select("a");
		for (Element productLinkCode : e) {
			String genLink = "<a href=\""+productLinkCode.attr("href")+"?isRealProductId=true\">";	
			trn += genLink;
		}
		
		return trn;
	}

	
}