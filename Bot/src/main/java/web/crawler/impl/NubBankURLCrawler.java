package web.crawler.impl;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class NubBankURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rHtml = FilterUtil.getStringBetween(html, "<div id=\"content_res\">", "<div id=\"footer_res\">");
		rHtml = FilterUtil.getStringBetween(rHtml, "<div id=\"content_right_res\">", "<div style=\"clear:both;\"></div>");
		return rHtml;
	}
}
