package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;
	

public class AsiaBookURLCrawler extends HeatonURLCrawler {
	//ul.products-grid
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements cutPage = doc.select("div.pages");
		String newHTML = "";
		Elements cut = doc.select("ul.products-grid");
		newHTML = cut.html()+cutPage.html();
		return newHTML;
	}
	public static void main(String[] args) {
		AsiaBookURLCrawler x = new AsiaBookURLCrawler();
		String[] ww = HTTPUtil.httpRequestWithStatus("https://www.asiabooks.com/thaibooks/art-design.html","UTF-8", true);
		try {
			x.processHTMLBeforeParse(ww[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
