package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;
import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BentoWebURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String rootLink = FilterUtil.getStringBetween(html, "<link rel=\"canonical\" href=\"", "\"");
		String pageContent  = FilterUtil.getStringBetween(html, "<a href=\"javascript:void(0);\">หมวดหมู่สินค้า</a>", "<ul class=\"nav navbar-nav navbar-right pull-right\">");
		int pageNum = 0;
		int maxProductPerPage = 12;
		if(StringUtils.isNotBlank(rootLink) && StringUtils.isNotBlank(pageContent)){
			String tmp  = FilterUtil.getStringBetween(pageContent, "<a href=\""+rootLink+"\" title=\"", "</a>");
			tmp  = FilterUtil.getStringAfter(tmp, ">", "");
			tmp = FilterUtil.getStringAfterLastIndex(tmp, "(", "");
			if(StringUtils.isNotBlank(tmp) && tmp.contains(")")){
				tmp = tmp.replace(")", "").trim();
				if(StringUtils.isNumeric(tmp)){
					int  allProducts = Integer.parseInt(tmp);
					pageNum = allProducts / maxProductPerPage;
					
				}
			}
		}
		
		if(pageNum > 1){
			pageNum += 1;
			rootLink = rootLink.replace("/th/category/", "/th/category/category_page/");
			for (int page= 2; page <= pageNum; page++) {
					String link = "<a href=\""+rootLink+"/"+ page + "?page=" + page+"\"></a>" ;
					html  = html+ link;
			}
		}
		
		return html;
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		
//		if(url.indexOf("?category_id=")>-1){
			url = FilterUtil.getStringBefore(url,"?category_id=","");
//		}
		
		return url;
	}
	
}