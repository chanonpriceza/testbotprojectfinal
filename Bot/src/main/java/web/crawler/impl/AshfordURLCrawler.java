package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class AshfordURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.products");
		return e.html();
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {

		if(url.contains("&amp;so=")) {
			String x  = FilterUtil.getStringBeforeLastIndex(url,"&amp;so=", url);
			return x;
		}
		return url;

	}
}
