package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.BotUtil;
import web.crawler.bot.HeatonURLCrawler;

public class TssBikeUrlCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		return doc.select("div.content-inner").html();
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
	
		return BotUtil.decodeURL(url);
	}
}
