package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.HTMLParserUtil;

public class ModifyInkURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String rtnHtml = html;
		if(rtnHtml.contains("<div class=\"vmgroup\">")){
			String[] removeTag = HTMLParserUtil.getTagList(rtnHtml, "<div class=\"vmgroup\">");
			if(removeTag!=null && removeTag.length >0){
				rtnHtml = rtnHtml.replace(removeTag[0],"");
			}
		}
		
		return rtnHtml;		
	}
		
}
