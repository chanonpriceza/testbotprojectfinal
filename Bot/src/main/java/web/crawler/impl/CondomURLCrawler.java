package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class CondomURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHTML =  "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("ul.products-grid");
		e = e.select("a");
		for(Element ex:e) {
			String genLink  = "<a href=\""+ex.attr("href")+"?isRealProduct=true"+"\"></a>";
			cutHTML += genLink;
		}
		Elements p = doc.select("div.pages");
		cutHTML += p.html();
		return cutHTML;
	}
}
