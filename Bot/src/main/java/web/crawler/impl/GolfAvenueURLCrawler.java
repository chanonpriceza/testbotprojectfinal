package web.crawler.impl;

import java.net.URI;

import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class GolfAvenueURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String h = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.product-image");
		e = e.select("a");
		for(Element ex:e) {
			URI x = new  URIBuilder(ex.attr("href")).removeQuery().addParameter("isRealProductId","true").build();
			String genLink = "<a href=\""+x+"\"></a>";
			h += genLink;
		}
		html  = html + h;
		return html;
	}
}
