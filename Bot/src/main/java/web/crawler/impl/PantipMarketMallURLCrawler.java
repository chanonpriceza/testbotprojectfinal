package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class PantipMarketMallURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {
		String result = url.replace("http:", "https:");
		return super.processURLAfterParse(result);
	}
	
}
