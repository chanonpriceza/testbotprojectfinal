package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class KiddoPacificURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String asideString = FilterUtil.getStringAfterLastIndex(html,"</aside>",html);
		return asideString;
	}
}
