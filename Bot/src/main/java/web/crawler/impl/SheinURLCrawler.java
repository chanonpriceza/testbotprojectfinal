package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class SheinURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
//		html = FilterUtil.getStringBetween(html, "<div class=\"row j-expose__container-list\">", "<div class=\"she-clearfix\" style=\"margin-bottom:60px;\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"c-goodsli j-goodsli j-goodsli-", "<div class=\"result-txt j-result-txt\" style=\"display: none;\"></div>");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
