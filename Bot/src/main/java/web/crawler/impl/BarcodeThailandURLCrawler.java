package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BarcodeThailandURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> listLink = FilterUtil.getAllStringBetween(html,"<div class=\"item-top\">","</a>");
		for(String link:listLink){
			String cutLink = FilterUtil.getStringBetween(link,"href=\"","\"");
			cutLink = cutLink+"?realProduct";
			html = html+"<a href="+cutLink+"\"></a>";
		}
		return html;
	}
}
