package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class HitechCenterURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\" product-cate-wrapper\">", "<footer> ");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"product-thumb transition\">", "<div class=\"button-group\">");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
