package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class DKTODAYURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String content = FilterUtil.getStringBetween(html, "<div class=\"category-products csstransforms3d\">", "<div class=\"footer-box\"></div>");
				
		String rtnHtml = "<html>><div>" + content + "</div></html>";
				
		return rtnHtml;		
	}
		
}
