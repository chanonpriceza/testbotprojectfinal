package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BarbaStoreURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String paginationDiv = FilterUtil.getStringBetween(html, "<div class=\"divPagination\">", "<script type=\"text/javascript\">");
		String linkArea = FilterUtil.getStringAfter(paginationDiv, "First", paginationDiv);
		String lastPage = FilterUtil.getStringBetween(linkArea, "&p=", "\"");
		String mainLink = FilterUtil.getStringBetween(linkArea, "href=\"",lastPage);
		
		if(StringUtils.isBlank(lastPage) || StringUtils.isBlank(mainLink) || !StringUtils.isNumeric(lastPage)) return html;
				
		StringBuilder pageBuilder = new StringBuilder();
		for(int i = 1; i<= Integer.parseInt(lastPage); i++){
			String newLink = "<a href=\"" + mainLink + i + "\"> genPage </a>";
			pageBuilder.append(newLink);
		}

		html = html.replace("</body>", pageBuilder.toString() + "</body>");
		
		return html;
	}
	
	
}
