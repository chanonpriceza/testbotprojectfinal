package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class LookfantasticURLCrawler  extends HeatonURLCrawler{
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		String re = "";
		Elements e = doc.select("div.productListProducts_products");
		e = e.select("a");
		for(Element ex:e) {
			String genString = "<a href=\""+ex.attr("href")+"?isRealProduct=true\"></a>";
			re += genString;
		}
		Elements page = doc.select("nav.responsivePaginationPages");
		re += page.html();
		return re;
	}
}
