package web.crawler.impl;



import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;

public class ISmartLifeURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = html.replace("onclick=\"location='", "href=\"");
		html = html.replace("';\">","\">");
		return StringEscapeUtils.unescapeHtml4(html);
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		char[] charArray = url.toCharArray();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < charArray.length; i++){
			int eachCharAscii = (int) charArray[i];
			if(eachCharAscii < 32 || eachCharAscii == 127) {
				continue;
			}
			s.append(charArray[i]);
		}
		return s.toString();
	}

}