package web.crawler.impl;


import java.util.List;

import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class BqbestqualityURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
//		html = FilterUtil.getStringBetween(html,"<div class=\"descriptionContainer \">", "<div class=\"clear\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"title\">", "</div>");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, "http://www.bqbestquality.com"+productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
	
	public static void main(String[] args) throws Exception {
		BqbestqualityURLCrawler x = new BqbestqualityURLCrawler();
		 String xxx = x.processHTMLBeforeParse(HTTPUtil.httpRequestWithStatus("http://www.bqbestquality.com/16622109/%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%84%E0%B8%AD%E0%B8%A1%E0%B8%9E%E0%B8%B4%E0%B8%A7%E0%B9%80%E0%B8%95%E0%B8%AD%E0%B8%A3%E0%B9%8C-1-%E0%B8%8A%E0%B8%B8%E0%B8%94","UTF-8", true)[0]);
//		 System.out.println(xxx);
	}
	
}