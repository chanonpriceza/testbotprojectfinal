package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class LEDTVSomchicURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.main-products.product-grid");
		String cutHtml = e.html();
		return cutHtml;
	}
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replace("&amp;limit=100","");
		return url;
	}

}
