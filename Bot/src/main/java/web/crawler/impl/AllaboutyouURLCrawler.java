package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class AllaboutyouURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		return html.replace("/en/", "/th/");
	}

	
}