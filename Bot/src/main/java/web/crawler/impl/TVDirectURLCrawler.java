package web.crawler.impl;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class TVDirectURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements cutString = doc.select("div.category-products");
				
		List<String> urlList = FilterUtil.getAllStringBetween(cutString.html(),"href=\"","\"");
		for(String url:urlList){
			if(url.contains("?p="))continue;
			html += "<a href=\""+url+"?isProduct=false\">";
		}
		return html;
	}
	
	
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		if(!url.contains("https")){
			url = url.replace("http","https");
		}
		return url;
	}

}
