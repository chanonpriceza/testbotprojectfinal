package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.BotUtil;
import utils.FilterUtil;

public class TescoLotusURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = html.replaceAll("amp;","");
		List<String> x = FilterUtil.getAllStringBetween(html,"href=\"","\"");
		for(String xx:x){
			html = html+"<a href=\""+BotUtil.decodeURL(xx)+"\">";
		}
		return html;
	}
}
