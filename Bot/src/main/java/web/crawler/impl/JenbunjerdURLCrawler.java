package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class JenbunjerdURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = html;
		
		List<String> productLinkList = FilterUtil.getAllStringBetween(html, "<meta property=\"og:url\" content=\"", "\"/>");
		if(productLinkList != null && productLinkList.size() > 0){
			for (String productLink : productLinkList) {
				String newProductLink = productLink + "?isProductLink=true";
				rtnHtml = rtnHtml.replace(productLink, newProductLink);
			}
		}
		
		return rtnHtml;
	}
		
}