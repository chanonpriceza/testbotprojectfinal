package web.crawler.impl;
import web.crawler.bot.HeatonURLCrawler;

public class BromptURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
				
		//parse content zone
		String rtnHtml = html.replaceAll("http://" ,"https://");			
		
		return rtnHtml;		
	}
	
	
	
}
