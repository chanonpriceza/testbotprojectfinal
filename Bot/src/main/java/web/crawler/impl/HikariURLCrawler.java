package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class HikariURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rHtml = FilterUtil.getStringBetween(html, "<div class=\"hidable-container\"> ", "<div id=\"footer-link\">");
		return rHtml;
	}
}
