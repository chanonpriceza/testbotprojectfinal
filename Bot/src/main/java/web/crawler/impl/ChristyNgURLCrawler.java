package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ChristyNgURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		if (StringUtils.isNotBlank(html)) {
			if (html.indexOf("<div class=\"product-info\">") > -1) {
				html = FilterUtil.getStringBetween(html, "<div id=\"content\">", "<div class=\"box related-products\">");
			}
		}
		return html;
	}
}