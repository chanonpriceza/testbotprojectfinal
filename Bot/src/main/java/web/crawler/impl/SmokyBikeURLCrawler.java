package web.crawler.impl;

import java.util.List;

import org.apache.commons.text.StringEscapeUtils;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class SmokyBikeURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		 String rtnHtml = html;
		 String mLink = StringEscapeUtils.unescapeHtml4(StringEscapeUtils.unescapeHtml4(StringEscapeUtils.unescapeHtml4(html)));
		 List<String> productLinkArea = FilterUtil.getAllStringBetween(mLink, "<div class=\"col-6 col-md-4\">", "</listing-block>");	 
		 if(productLinkArea != null && productLinkArea.size() > 0){
				for (String productLinkCode : productLinkArea) {
					String productLink = FilterUtil.getStringBetween(productLinkCode, "\"url\":\"", "\"");
					productLink = StringEscapeUtils.unescapeJson(productLink);
					productLink = String.valueOf(productLink);
					rtnHtml += "<a href=\""+productLink+"?isProduct=true\""+"</a>";
				}
				return rtnHtml;
		 }else {
			  productLinkArea = FilterUtil.getAllStringBetween(mLink, "<div class=\"col-6 col-sm-3\">", "src=\"");
			  if(productLinkArea != null && productLinkArea.size() > 0){
					for (String productLinkCode : productLinkArea) {
						String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");
						productLink = StringEscapeUtils.unescapeJson(productLink);
						productLink = String.valueOf(productLink);
						rtnHtml += "<a href=\""+productLink+"?isProduct=true\""+"</a>";
					}
					return rtnHtml;
			 }
			
		 }
			
//		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"col-xs-12 col-sm-4 col-md-4 listing__block\">", "<div class=\"listing--detailWrapper\">");
//		if(productLinkArea != null && productLinkArea.size() > 0){
//			String rtnHtml = html;
//			for (String productLinkCode : productLinkArea) {
//				String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");
//				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
//			}
//			return rtnHtml;
//		}
		return html;
	}
}
