package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class UnionKKURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHTML = FilterUtil.getStringBetween(html,"<ul class=\"subpages\">","<div id=\"content-visit-history-container\">");
		List<String> links = FilterUtil.getAllStringBetween(cutHTML,"href=\"","\"");
		for(String l:links) {
			String genLink = "<a href=\""+l+"?isRealProductId=true"+"\" >";
			html+= genLink;
		}
		
		cutHTML = FilterUtil.getStringBetween(html,"<div id=\"midbar\">","<h1 class=\"title title-with-text-alignment\">");
		links = FilterUtil.getAllStringBetween(cutHTML,"href=\"","\"");
		for(String l:links) {
			String genLink = "<a href=\""+l+"?isRealProductId=true"+"\" >";
			html+= genLink;
		}
		
		return html;
	}
}
