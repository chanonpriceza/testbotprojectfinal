package web.crawler.impl;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class SpecURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutUrl = FilterUtil.getStringBetween(html,"<div id=\"primary\" class=\"content-area\">","<div id=\"sidebar\" class=\"sidebar\" role=\"complementary\">");
		
		return cutUrl;
	}
}
