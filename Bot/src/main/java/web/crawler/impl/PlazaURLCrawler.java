package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class PlazaURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		html = FilterUtil.getStringBetween(html, "<div class=\"mainbox-body\">	", "<div id=\"footer\">");	
		String domain = "https://www.plaza.co.th/";
		String rtnHtml = html;
		List<String> links = FilterUtil.getAllStringBetween(rtnHtml, "<a name=\"pagination\" href=\"", "\"");
		if(links!= null && links.size() > 0) {
			for (String link : links) {							
				if(link.contains("isNextPage")) {
					break;
				}else{
					String newLink = domain + link + "&isNextPage";
					html = html.replace(link,newLink).replace("amp;","");
				}					
			}
			return html;
		}
		return html;
	}

	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replace("http:", "https:");
		return url;
	}
	
}
