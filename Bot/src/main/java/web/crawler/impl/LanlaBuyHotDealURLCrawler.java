package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class LanlaBuyHotDealURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = "";
		List<String> hotDealLi = FilterUtil.getAllStringBetween(html, "<div class=\"hotdeal_owl_item owl-item cloned\">", "<div class=\"iPriceButtonBox\">");
		for(String hotDeal : hotDealLi){
			if(hotDeal.contains("<span class=\"clock\">")){
				rtnHtml += hotDeal;
			}
		}
		if(rtnHtml.isEmpty()){
			rtnHtml = html;
		}
		return rtnHtml;
	}
}
