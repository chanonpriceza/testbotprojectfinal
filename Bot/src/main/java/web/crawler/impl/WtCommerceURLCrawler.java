package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class WtCommerceURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtn = FilterUtil.getStringBetween(html, "<td width=\"678\" valign=\"top\">","<td id=\"leftnav\"></td>");
		return rtn;
	}
}