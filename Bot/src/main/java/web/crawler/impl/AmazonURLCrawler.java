package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class AmazonURLCrawler extends HeatonURLCrawler {
	private static final Logger logger = LogManager.getRootLogger();
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		boolean isShow = false;
		Document doc = Jsoup.parse(html);
		Elements cutBox = doc.select("div#mainResults");
		
		if(StringUtils.isBlank(cutBox.html())) {
			cutBox = doc.select("div.s-result-list.s-search-results.sg-row");
		}
		
		if(StringUtils.isBlank(cutBox.html()))		
			isShow = true;
		
		Elements cutPage = doc.select("div#pagn");
		String cutString = cutPage.html();
		if(StringUtils.isBlank(cutPage.html())) {
			cutPage = doc.select("ul.a-pagination");
			cutPage = cutPage.select("li.a-normal");
			Element cutFinal = cutPage.last();
			cutString = cutFinal.html();
			isShow = true;
		}
		
		if(!isShow) {
			logger.info(html);
		}
		html = cutBox.html()+cutString;
		
		return html;
	}
	
	String param = "?psc=1&SubscriptionId=AKIAIMCPQY45DVN3WDTA&tag=priceza09-20&linkCode=xm2&camp=2025&creative=165953&creativeASIN={productId}";
	
	int i = 0;
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		if(url.contains("ref")&&url.contains("/dp/")) {
			url = FilterUtil.getStringBefore(url,"ref",url);
			url = url.replace("?","");
			if(url.endsWith("/")) {
				url = url.substring(0,url.length()-1);
			}
			String[] split = url.split("/");
			String productId = split[split.length-1];	
			String genParam = param.replace("{productId}",productId);
			url+=genParam;
			i++;
		}
		
		return url;
	}
	
}
