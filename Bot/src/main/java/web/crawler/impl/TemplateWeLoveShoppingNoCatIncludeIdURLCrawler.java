package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TemplateWeLoveShoppingNoCatIncludeIdURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		// rtnHtmlTODO Auto-generated method stub
		String rtnHtml = html;
		
		String productLi = FilterUtil.getStringBetween(rtnHtml, "<!-- Begin Category -->", "<!-- End Category -->");
		
		if(productLi == null || productLi.length() == 0){
			productLi = FilterUtil.getStringBetween(rtnHtml, "<!-- Begin greatmsg -->", "<!--End greatmsg Text -->");
		}
		
		if(productLi != null && productLi.length() > 0){
				List<String> productLink = FilterUtil.getAllStringBetween(productLi, "href=\"", "\"");
				if(productLink != null && productLink.size() > 0){
					for (String link : productLink) {
						if(!link.contains("ps")){
							if(link.contains("?")){
								rtnHtml = rtnHtml.replace(link, link+"&ps=1");
							}else{
								rtnHtml = rtnHtml.replace(link, link+"?ps=1");
							}
						}
					}
				}
		}
		
		return rtnHtml;
	}

	
	
}
