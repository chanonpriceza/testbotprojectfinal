package web.crawler.impl;

import utils.BotUtil;
import web.crawler.bot.HeatonURLCrawler;


public class Commy4uURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		return html.replace("/en", "/th");
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = BotUtil.decodeURL(url);
		return url;
	}
}

