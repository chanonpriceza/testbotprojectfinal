package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class VoIPThailandURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String dumpHTML = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.tab-content");
		Elements tagA = e.select("a.woocommerce-LoopProduct-link");
		for(Element href:tagA) {
			String hrefString = href.attr("href");
			hrefString= "<a href=\""+hrefString+"?isRealProduct=true\"></a>";
			dumpHTML+= hrefString;
		}
		
		return dumpHTML;
	}
	
	public static void main(String[] args) {
		VoIPThailandURLCrawler x = new VoIPThailandURLCrawler();
		String[] ww = HTTPUtil.httpRequestWithStatus("https://www.voip-thailand.com/product-category/ip-phones/?q=/filter/&product_tag=fritz","UTF-8", true);
		try {
			x.processHTMLBeforeParse(ww[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
