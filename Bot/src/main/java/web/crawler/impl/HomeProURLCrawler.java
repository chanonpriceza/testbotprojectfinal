package web.crawler.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HTTPException;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLUtility;

public class HomeProURLCrawler extends HeatonURLCrawler {

	private static int offset = 60;
	private static String realURL = "https://www.homepro.co.th/search.jsp?q={q}&f={start}&s={offset}";
	private static Logger  logger = LogManager.getRootLogger();

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {

		return super.processHTMLBeforeParse(html);
	}

	@Override
	public void process(WorkloadBean target) throws Exception {

		String targetUrl = target.getUrl();
		int targetDepth = target.getDepth();
		int targetMerchantId = target.getMerchantId();
		int targetCategoryId = target.getCategoryId();
		String targetKeyword = target.getKeyword();
		String requestUrl = target.getRequestUrl();
		int start = 0;
		String q = "";
		q = prepareParam(requestUrl);

		
		try {
			boolean isContinue = true;
			do {
				String modifyURL = realURL.replace("{q}", q).replace("{start}", String.valueOf(start)).replace("{offset}", String.valueOf(offset));
				//logger.info(modifyURL);
				
				if(StringUtils.isBlank(q)) {
					modifyURL = requestUrl;
					isContinue = false;
				}
				
				String response = httpRequest(modifyURL, "UTF-8", true);
				if (StringUtils.isBlank(response)) {
					isContinue = false;
					break;
				}
				http.send(modifyURL, null);
				HTMLParser parse = new HTMLParser();
				parse.source = new StringBuffer(processHTMLBeforeParse(response));

				// find all the links
				while (!parse.eof()) {
					char ch = parse.get();
					if (ch == 0) {
						HTMLTag tag = parse.getTag();
						Attribute link = tag.get("HREF");
						if (link == null)
							link = tag.get("SRC");

						if (link == null)
							continue;

						URL u = null;
						try {
							if (link.getValue().startsWith("?")) {
								u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
							} else {
								u = new URL(new URL(targetUrl), link.getValue());
							}
						} catch (MalformedURLException e) {
							continue;
						}

						u = URLUtility.stripAnhcor(u);

						// change www.ha.com/../index.html -> www.ha.com/index.html
						u = URLUtility.formatToValidURL(u);

						if (u.getHost().equalsIgnoreCase(new URL(targetUrl).getHost())) {
							if (!botExclusion.isExcluded(u.toString())) {

								String url = processURLAfterParse(u.toString());

								client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId,
										targetKeyword);
							}
						}
					}
				}
				start += offset;
			} while (isContinue);
			completePage(targetMerchantId, http, STATUS.C, target.getUrl());
		} catch (Exception e) {
			if (!http.getURL().equals(http.getRootURL())) {
				client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
			}
			throw e;
		}
	}

	private String prepareParam(String url) throws HTTPException, UnknownHostException, IOException {
		http.send(url, null);
		String body = http.getBody();
		Document doc = Jsoup.parse(body);
		Elements e = doc.select("div#q");
		return e.html();
	}

	public String httpRequest(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setConnectTimeout(3000);
			conn.setReadTimeout(3000);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Cookie","h11e_csrf=a; ");
			conn.setRequestProperty("X-CSRF-TOKEN","a");

			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;

			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if (encode != null)
				gzip = encode.equals("gzip");

			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = charset == null ? new InputStreamReader(gzip ? new GZIPInputStream(is) : is)
							: new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
					BufferedReader brd = new BufferedReader(isr);) {

				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return rtn.toString();
			}
		} catch (Exception e) {
			logger.error(e,e);
			success = false;
		} finally {
			if (!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if (conn != null)
				conn.disconnect();
		}
		return null;
	}

	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null)
			return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {
			}
		} catch (IOException ex) {
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
				}
		}

	}

}
