package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class PegasusLuggageURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		List<String> allLink = FilterUtil.getAllStringBetween(html, "<h5 class=\"product-box_title product_blog_title\" onclick=\"location.href='", "'");
		if(allLink != null && allLink.size() > 0){
			for (String link : allLink) {
				link = link.replace("\\\"", "%22");
				html = html + "<a href=\""+ link + "\"></a>";
			}
		}
		return html;
	}
}