package web.crawler.impl;

import java.util.List;

import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class BezthailandURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "href='", "'");
		String rtnHtml = html;
		if(productLinkArea != null && productLinkArea.size() > 0){
			for (String productLink : productLinkArea) {
				if(productLink.contains("/page/")) {
					rtnHtml = rtnHtml.replace(productLink, BotUtil.decodeURL(productLink));
				}
			}
			return rtnHtml;
		}
		return html;
	}
	
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		return BotUtil.decodeURL(url);
	}
	
	
	public static void main(String[] args) throws Exception {
		BezthailandURLCrawler x = new BezthailandURLCrawler();
		 String xxx = x.processHTMLBeforeParse(HTTPUtil.httpRequestWithStatus("https://www.bezthailand.com/product-category/%E0%B8%AD%E0%B8%B8%E0%B8%9B%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C%E0%B8%8A%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%88/%E0%B8%AA%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%88/","UTF-8", true)[0]);
//		 System.out.println(xxx);
	}

}