package web.crawler.impl;

import java.util.List;



import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class LuckywayShopURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = html;
		String mLink = FilterUtil.getStringBetween(rtnHtml, "<div class=\"catalog-top\">", "<div class=\"catalog-top\">");
		 List<String> productLinkArea = FilterUtil.getAllStringBetween(mLink, "<div class=\"product block-product block-product-m  \"", "<img class=");	 
		 if(productLinkArea != null && productLinkArea.size() > 0){
				for (String productLinkCode : productLinkArea) {
					String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");;
					rtnHtml += "<a href=\""+productLink+"?isProduct=true\""+"</a>";
				}
				return rtnHtml;
		 }
		 return rtnHtml;
	}
}
