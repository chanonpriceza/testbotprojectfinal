package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class KaideeMarketplaceURLCrawler extends HeatonURLCrawler {
	
	public String processHTMLBeforeParse(String html) throws Exception {
		
		if(StringUtils.isNotBlank(html)) {
			List<String> addStr = FilterUtil.getAllStringBetween(html, "href=\"", "\"");
			for(String link : addStr){
				if(link.indexOf("/p-") > -1) {
					if(StringUtils.countMatches(link, "/") > 5 ){
						html = html.replaceAll(link, "");
					}
					
					if(!link.endsWith("/")) {
						html = html.replaceAll(link+"/", "");
					}
				}
			 }
		}
		return html;
	}
	
}
