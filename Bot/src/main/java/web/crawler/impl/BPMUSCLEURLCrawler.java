package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;
  
public class BPMUSCLEURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		if (StringUtils.isNotBlank(html)) {
				String cutHtml = FilterUtil.getStringBetween(html, "<div class=\"grid BPspecialgrid\">", "<div class=\"js-tooltip-content-wrap is-hidden\">");
					List<String> productLinkArea = FilterUtil.getAllStringBetween(cutHtml, "<div class=\"grid__item product-group\">", "<div class=\"box-product\">");
					if(productLinkArea != null && productLinkArea.size() > 0){
						String rtnHtml = html;
						for (String productLinkCode : productLinkArea) {
							String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");
							rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
						}
						return rtnHtml;
					}

		}
		return html;
	}
}