package web.crawler.impl;

import utils.BotUtil;
import web.crawler.bot.HeatonURLCrawler;

public class FnoutletUrlCrawler extends HeatonURLCrawler {
	@Override
	public String processURLAfterParse(String url) throws Exception {
		return BotUtil.decodeURL(url);
	}
}
