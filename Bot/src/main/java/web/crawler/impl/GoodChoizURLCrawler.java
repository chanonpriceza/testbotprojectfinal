package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class GoodChoizURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		if (StringUtils.isNotBlank(html)) {
			if (html.indexOf("<div class=\"page category-page\">") > -1) {
				html = FilterUtil.getStringBetween(html, "<div class=\"product-filters\">", "<div class=\"side-2\">");
			}
		}
		return html;
	}

}
