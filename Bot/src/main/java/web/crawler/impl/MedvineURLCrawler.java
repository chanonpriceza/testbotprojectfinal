package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class MedvineURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String re = "";
		String page = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.Category__Section-sc-1pcz80a-6");
		 e = doc.select("a");
		 for(Element ex:e) {
			 String genString = "<a href=\""+ex.attr("href")+"?isRealProduct=true\"></a>";
			 if(genString.contains("///product"))
				 genString = genString.replace("///product","/th-th/product");
			 re += genString;
		 }
		 
		 Elements pages = doc.select("div.Pagination__Item-sc-3mhwbg-2");
		 pages = pages.select("a.Pagination__A-sc-3mhwbg-3");
		 String currentUrl = FilterUtil.getStringBetween(html,"\"url_path\":\"","\"");
		 
		 for(int i = 1;i<=pages.size();i++) {
			 String genPage = "<a href=\"/th-th/"+currentUrl+"?current_page="+i+"\"></a>";
			 page+= genPage;
		 }
		 
		
		return re+page;
	}

	
}