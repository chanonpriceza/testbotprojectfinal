package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class CaseCrazyURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String rtn = FilterUtil.getStringBetween(html, "<div class=\"products wrapper grid products-grid\" id=\"product-wrapper\">","<footer class=\"footer footer1\">");
		return rtn;
	}

	
}