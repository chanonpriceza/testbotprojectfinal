package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.BotUtil;
import utils.FilterUtil;

public class HomeNet48URLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHtml = FilterUtil.getStringBetween(html,"<ul class=\"pagination\" style=\"margin: 0;\"></ul>","</div>");
		String link = FilterUtil.getStringBetween(cutHtml,"<a href=\"","&");
		String page = FilterUtil.getStringBetween(cutHtml,"&p=","\"");;
		page = FilterUtil.removeCharNotPrice(page);
		int maxPage = BotUtil.stringToInt(page,0);
		for(int i = 1;i<=maxPage;i++) {
			String genLink = "<a href=\""+link+"&p="+i+"\">";
			html += genLink;
		}
		return html;
	}

}
