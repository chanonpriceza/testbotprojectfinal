package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class PakoEngineeringURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutUrl = FilterUtil.getStringBetween(html,"<div class=\"category-products category-column-count-3 category-image-size-100\">","<div class=\"toolbar-bottom\">");
		List<String> link = FilterUtil.getAllStringBetween(cutUrl,"href=\"","\"");
		
		for(String l:link) {
			if(l.contains("p=")||l.contains("dir=desc"))continue;
			html += "<a href=\""+l+"?isRealproduct=true"+"\">";
		}
		
		return html;
	}
}
