package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

import utils.FilterUtil;

public class SapashopURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String rtnHtml = html;

		if(rtnHtml.contains("<td   valign='top' CLASS='TabRight'  width='180' height='100%'>")){
			String removeTag = FilterUtil.getStringBetween(rtnHtml, "<td   valign='top' CLASS='TabRight'  width='180' height='100%'>", "<div id=\"page\">");
			if(removeTag!=null && removeTag.length() >0){
				rtnHtml = rtnHtml.replace(removeTag,"");
			}
		}
		
		return rtnHtml;		
	}
		
}
