package web.crawler.impl;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLUtility;

public class SE_EDURLCrawler extends HeatonURLCrawler{
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
	
		return html+"<a href=\"https://www.se-ed.com/product/Moonlight-Flower-มนตร์รักร้ายป่วนหัวใจนายปีศาจ.aspx?no=9786165354226\"></a>";
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        
		try {		
		        
				String body = ajax(requestUrl);	
				HTMLParser parse = new HTMLParser();
				http.setURL(requestUrl);        
		        parse.source = new StringBuffer(processHTMLBeforeParse(body));       
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(targetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
		                	 if(!botExclusion.isExcluded(u.toString())){
		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     }		                	
		                }
		            }
		        }
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());		
		} catch (Exception e) {
			if (!http.getURL().equals(http.getRootURL())) {
	        	client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        } 
		    throw e;
		}
	}
	
	private static String urlAjax = "https://www.se-ed.com/mvc/list/getnc?nodeCode={node}&saleType=all&sortNumber=1&pageIndex={page}&pageSize=20";
	private static JSONParser parser = new JSONParser();
	
	private String ajax(String url) throws Exception {
		String node = FilterUtil.getStringBetween(url,"nc=","&");
		String result = "";
		for(int i = 0;i<25;i++) {
			URL u = new URL(urlAjax.replace("{node}", node).replace("{page}",i+""));
			HttpURLConnection con = (HttpURLConnection) u.openConnection();
			con.setRequestProperty("Referer", BotUtil.encodeURL(url));
			con.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36");
			InputStreamReader reader = new InputStreamReader(con.getInputStream());
			JSONObject obj = (JSONObject)parser.parse(reader);
			if(obj==null)
				break;
			JSONArray array = (JSONArray)obj.get("Items");
			if(array==null||array.size()==0) {
				break;
			}
			for(Object o:array) {
				JSONObject ob = (JSONObject)o;
				result += String.valueOf(ob.get("Item"));
			}
		}
		
		return result;
		
	}

}
