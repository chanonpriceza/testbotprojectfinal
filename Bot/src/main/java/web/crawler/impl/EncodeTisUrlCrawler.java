package web.crawler.impl;

import bean.WorkloadBean;
import engine.BaseConfig;
import web.crawler.bot.HeatonURLCrawler;

public class EncodeTisUrlCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		BaseConfig.CONF_URL_CRAWLER_CHARSET = "TIS-620";
		return super.processHTMLBeforeParse(html);
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception {
		BaseConfig.CONF_URL_CRAWLER_CHARSET = "TIS-620";
		super.process(target);
	}
}
