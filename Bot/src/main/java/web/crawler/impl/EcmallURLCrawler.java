package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class EcmallURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<main id=\"main\" class=\"site-main\">", "<div id=\"sidebar\" class=\"sidebar\" role=\"complementary\">");
		return html;
	}
}