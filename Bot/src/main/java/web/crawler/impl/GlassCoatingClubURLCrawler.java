package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class GlassCoatingClubURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		//parse content zone
		String content = FilterUtil.getStringBetween(html, "<div class=\"product-grid\">", "<div class=\"side-2\">");
		String rtnHtml = "<html><div>" + content + "</div></html>";
				
		return rtnHtml;		
	}
	
}
