package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;
import utils.HTTPUtil;

public class KlookURLCrawler extends HeatonURLCrawler {
	String param = "?city_id={id}&limit=15&template_ids=1&tag_ids=&instant=0&sort=&page={page}";

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String link = FilterUtil.getStringBetween(html, "<link rel=\"canonical\" href=\"", "\"");
		String cityId = FilterUtil.getStringBetween(link, "city/", "-");
		String[] data = null;
		param = param.replace("{id}", cityId);
		int page= 1;
		do {
			String x = param.replace("{page}",String.valueOf(page));
			data = HTTPUtil.httpRequestWithStatus(link+x, "UTF-8", false);
			String cutHTML = FilterUtil.getStringBetween(data[0],"<div class=\"filter-block-area main-set\">","<div class=\"pagination-box\">");
			List<String> cutlink = FilterUtil.getAllStringBetween(cutHTML, "href=\"", "\"");
			if (cutlink == null || cutlink.size() == 0) {
				break;
			}
			html += "<a href=\""+link+x+"\">";
			page++;
		} while (data != null && data.length > 0 && StringUtils.isNotBlank(data[0]));

		return html;
	}
}
