package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;

public class ErgohumanThailandURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String tmp = html;
		if(StringUtils.isNotBlank(tmp) && tmp.contains("http://ergohumanthailand.com") ){
			tmp = tmp.replace("http://ergohumanthailand.com", "http://www.ergohumanthailand.com");
		}
		return tmp;
	}
	
}
