package web.crawler.impl;

import utils.FilterUtil;

public class TPQToolsURLCrawler extends MakeWebEasyProxyURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String rootLink  = "";
		String lastPage ="";
		String currentPage ="";
		rootLink = FilterUtil.getStringBetween(html, "<link rel=\"canonical\" href=\"", "\"");
		
		if(rootLink.contains("https://www.tpqtools.co.th/category")){
			
			lastPage = FilterUtil.getStringBetween(html, "<a href=\"/category/&p=", "\"");
			currentPage = FilterUtil.getStringBetween(html, "currentPage:", "}");
			currentPage = FilterUtil.removeSpace(currentPage);
			if(!currentPage.isEmpty() && !lastPage.isEmpty())	
			{
				int page = Integer.parseInt(currentPage)+1;
				if(Integer.parseInt(currentPage)  <= Integer.parseInt(lastPage)){
					html = html+"<a href=\"/category/&p="+page+"\""+"/>";
				}
			}	
			else if(lastPage.isEmpty()){
				return html;
			}
		}
		return html;
	}
}
