package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class SevenCatalogURLCrawler extends HeatonURLCrawler {

	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rHtml = html;
		String prodcutBox = FilterUtil.getStringBetween(html,"<div class=\"col-md-12 col-xs-12 product-list xxs-padding-left-right-5\">","<div class=\"row\">");
		List<String> productBoxList = FilterUtil.getAllStringBetween(prodcutBox,"href=\"","\"");
		for(String product:productBoxList){
			if(!product.contains("/p/")){
				continue;
			}
			rHtml = rHtml +"<a href=\""+product+"?realProductId=true\">";
		}
		String catString = FilterUtil.getStringBefore(html,"\" rel=\"canonical\"","");
		catString = FilterUtil.getStringAfterLastIndex(catString,"href=\"","");
		String pagingArea = FilterUtil.getStringBetween(rHtml, "<ul class=\"pagination list-pager\">", "</ul>");
		List<String> pageNumber = FilterUtil.getAllStringBetween(pagingArea, "<li>", "</li>");
		if(pageNumber != null && pageNumber.size() > 0 &&StringUtils.isNotBlank(catString)){
			int max = 0;
			for(String page:pageNumber){
				page = FilterUtil.getStringBetween(page, ">", "<").trim();
				if(StringUtils.isNotBlank(page)){
					int p = Integer.valueOf(page)-1;
					if(p > max){
						max = p;
					}
				}
			}
			for(int i = 0;i <= max;i++){
				rHtml = rHtml+"<a href=\""+catString+"?landing=true&p="+i+"&view=0&realContinue=true\">";
			}
		}
		return rHtml;
	}
}
