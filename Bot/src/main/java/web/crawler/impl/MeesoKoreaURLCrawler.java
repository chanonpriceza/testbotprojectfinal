package web.crawler.impl;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class MeesoKoreaURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements cutBox = doc.select("div#ct_product");
		String cutBoxString = cutBox.html();
		List<String> cutLinks = FilterUtil.getAllStringBetween(cutBoxString,"location.href='","'");
		for(String c:cutLinks) {
			String genLink = "<a href=\""+c+"\">";
			html+=genLink;
		}
		return html;
	}
}
