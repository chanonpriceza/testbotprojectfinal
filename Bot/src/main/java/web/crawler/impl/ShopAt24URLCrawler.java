package web.crawler.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ShopAt24URLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		
		String tmp = FilterUtil.getStringBetween(html , "rel=\"next\">", "rel=\"canonical\">");
		String rootLink = FilterUtil.getStringBetween(tmp , "href=\"", "\"");
		String nextPage = FilterUtil.getStringBetween(html , "<ul class=\"pagination list-pager\">", "</ul>");
		List<String> list = FilterUtil.getAllStringBetween(nextPage , "data-page=\"", "\"");
		html = html.replaceAll("p=", "");
		if(list.size()==0){
			return StringEscapeUtils.unescapeHtml4(html);
		}else{
			List<String> newString = new ArrayList<String>();
			for(String s : list){
				if(s.trim().length() > 0){
					s = "<a href=\""+rootLink+"?p="+s.toString()+"\"></a>";
					newString.add(s);
				}
			}
			html = html+newString.toString();
			return html;
		}
	}
		
}