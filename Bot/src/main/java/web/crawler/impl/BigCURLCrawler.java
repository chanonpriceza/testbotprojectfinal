package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class BigCURLCrawler extends HeatonURLCrawler {
	final String PARAM = "?isRealProduct=true";
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHtml = FilterUtil.getStringBetween(html,"<ul class=\"products-grid products-grid--max-3-col\">","<div class=\"toolbar-bottom\">");
		List<String> linkList = FilterUtil.getAllStringBetween(cutHtml,"href=\"","\"");
		for(String link:linkList) {
			String genLink = "<a href=\""+link+PARAM+"\">";
			html += genLink;
		}
		return html;
	}
}
