package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class CoolandsweetclubURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {				
		return url.replace("template/w15/s_product.php?", "shop/s_product.php?");
	}
}
