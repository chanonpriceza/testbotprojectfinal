package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class IrobotthailandURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html,"<main id=\"main\" class=\"site-main\" role=\"main\">","</main>");
		return html;
	}
}
