package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class CottoURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		//html = HTTPUtil.httpRequestWithStatus("https://www.cotto.com/product/toilets","UTF-8", true)[0];
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("ul.pagination");
		Elements cutHtml = doc.select("section#infinite-scroll-wrap-product");
		String cutString = cutHtml.html();
		e = e.select("li");
		e = e.select("a");
		for(Element loop:e) {
			String cut = loop.html();
			if(StringUtils.isNotBlank(cut)&&StringUtils.isNumeric(cut)) {
				if(StringUtils.isBlank(loop.attr("href-data"))) {
					continue;
				}
				String link = "<a href=\""+loop.attr("href-data")+"?isPage=true"+"\">";
				cutString += link;
			}
		}
		
		return cutString;
	}
	/*public static void main(String[] args) throws Exception {
		CottoURLCrawler x = new CottoURLCrawler();
		x.processHTMLBeforeParse("");
	}*/
	
}
