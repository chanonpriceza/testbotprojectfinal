package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class LnwshopURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
				
		//parse content zone
		
		String content = FilterUtil.getStringBetween(html, "class=\"widgetZone z-article\">", "class=\"widgetZone z-aside  z-aside-left\"");
		if(content.trim().length() != 0) {			
			content = "<div class=\"widgetZone z-article\">" + content + "class=\"widgetZone z-aside  z-aside-left\"></div>";
		}
		
		//parse cat zone
		
		String cat = FilterUtil.getStringBetween(html, "class=\"widgetUnit u-category\">", "<div class=\"unitShoe\">");
		if(cat.trim().length() != 0) {			
			cat = "<div class=\"widgetUnit u-category\">" + cat + "</div>";
		}
				
		String rtnHtml = "<div>" + content + cat + "</div>";
				
		return rtnHtml;		
	}
		
}
