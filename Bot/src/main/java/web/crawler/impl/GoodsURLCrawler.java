package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class GoodsURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		if (StringUtils.isNotBlank(html)) {
			if (html.indexOf("<div class=\"product-details\">") > -1) {
				html = FilterUtil.getStringBetween(html, "<div class=\"vs-mainbox-body\">", "<div class=\"span4 product_scroller_right\" >");
			} else if (html.indexOf("category_products") > -1) {
				html = FilterUtil.getStringBetween(html, "<div class=\"vs-mainbox-body\">", "<div class=\"vs-sidebox clearfix\">");
			}
		}
		return html;
	}
}
