package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class FjallravenThailandURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"col-sm-9 col-sm-12", "<aside class=\"col-sm-3\" id=\"column-right\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"name\">", "</div>");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
