package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TemplateLnwshopNoPriceIncludeIdLongTitleURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtn = FilterUtil.getStringBetween(html, "<div class=\"zoneShirt\">","<div id=\"gadget-9\" config_id=\"9\" class=\"widgetZone z-aside  z-aside-left\">");
		return rtn;
	}
}
