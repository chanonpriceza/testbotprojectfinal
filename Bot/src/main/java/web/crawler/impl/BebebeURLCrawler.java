package web.crawler.impl;


import java.util.ArrayList;
import java.util.List;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BebebeURLCrawler extends HeatonURLCrawler {
		
		
	public String processURLAfterParse(String url) throws Exception {
		if (StringUtils.isNotBlank(url)) {
			if(url.contains("viewproduct")){ 
				url = url.replace("/products", "");
			}
		}
		return url;
	}

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String nextPage = FilterUtil.getStringBetween(html, "<span class=\"text-pd-page\">", "</span>");
		List<String> list = FilterUtil.getAllStringBetween(nextPage, "<a href=\"","\"");
		
		if(list.size()==0){
			return StringEscapeUtils.unescapeHtml4(html);
		}else{
			List<String> newString = new ArrayList<String>();
			for(String s : list){
				if(s.trim().length() > 0){
					s = "<a href=\"http://www.be-bebe.com/"+s.toString()+"page\"></a>";
					newString.add(s);
				}
			}
			return StringEscapeUtils.unescapeHtml4(html + newString.toString());
		}
	}


}