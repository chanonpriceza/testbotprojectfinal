package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import web.crawler.bot.HeatonURLCrawler;
import org.jsoup.select.Elements;

public class IstuidioURlCrawler extends HeatonURLCrawler{
	
	
	public String processHTMLBeforeParse(String html) throws Exception {
		String re = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.products");
		 e = doc.select("a.product-item-link");
		 for(Element ex:e) {
			 String genString = "<a href=\""+ex.attr("href")+"?isRealProduct=true\"></a>";
			 re += genString;
		 }
		 
		return re;
	}

}
