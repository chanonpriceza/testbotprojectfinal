package web.crawler.impl;



import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ThailuckwoodURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String link = FilterUtil.getStringBetween(html, "<div id='product'>", "<div id='hit'>");
		return link;
	}

}
