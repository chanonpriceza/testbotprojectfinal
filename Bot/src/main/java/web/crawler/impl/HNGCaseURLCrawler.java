package web.crawler.impl;

import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;

public class HNGCaseURLCrawler extends HeatonURLCrawler {
		
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		return StringEscapeUtils.unescapeHtml4(html);
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replace("https://", "http://");
		return url;
	}	
		
}