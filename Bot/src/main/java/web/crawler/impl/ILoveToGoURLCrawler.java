package web.crawler.impl;

import java.util.List;


import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ILoveToGoURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"product-content\">", "<div class=\"padTop10\">");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
