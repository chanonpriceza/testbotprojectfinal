package web.crawler.impl;

import java.util.ArrayList;
import java.util.List;


import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TrueStoreURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = html.replaceAll("\\\\", "");
		List<String> list = FilterUtil.getAllStringBetween(html , "url_level_d\":\"", "\"");
		if(list != null && list.size() > 0){
			List<String> newString = new ArrayList<String>();
			for(String link : list){
				if(link.trim().length() > 0){
					link = "<a href=\""+link+"\"></a>";
					newString.add(link);
				}
			}
			html = html+newString.toString();
			return html;
		}
		return null;
	}
}
