package web.crawler.impl;

import java.net.URLDecoder;
import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ThevisionopticURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = html;
		html = FilterUtil.getStringBetween(html, "<div class=\"page-links\">", "<aside id=\"sidebar\" class=\"sidebar\">");
		List<String> links = FilterUtil.getAllStringBetween(html, "a href=\"", "\"");
		if(links!= null && links.size() > 0) {
			for (String link : links) {
					String newLink = URLDecoder.decode(link,"UTF-8");
					rtnHtml = rtnHtml.replace(link,newLink);
			}
		}
		return rtnHtml;		
	}

}
