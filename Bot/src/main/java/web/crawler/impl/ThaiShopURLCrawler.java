package web.crawler.impl;
import java.util.List;
import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ThaiShopURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String result = html;
		
		List<String> boxList = FilterUtil.getAllStringBetween(html, "<li class=\"item last\">", "</li>");
		if(boxList!=null && boxList.size()>0) {
			for (String box : boxList) {
				String h2 = FilterUtil.getStringBetween(box, "<h2 class=\"product-name\">", "</h2>");
				String link = FilterUtil.getStringBetween(h2, "href=\"", "\"");
				result = result.replace(link, link+"?___store=th&___from_store=th&isProductLink=true");
				
			}
		}
		return result;
	}
	
}
