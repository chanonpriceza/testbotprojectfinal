package web.crawler.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLUtility;

public class thaiTravelURLCrawler extends HeatonURLCrawler {
	String param = "url_parameter={param}&sort=price,asc&page={page}&search[keyword]=&search[price_range]=&search[calendar_range]=&search[calendar_range_value]=&search[tour]=13&search[country]=Japan&search[destination]=ญี่ปุ่น&country_code=&loadmore=1&min_price_tl_id=&tour_name={param2}";
	String url  = "https://www.thaitravelcenter.com/th/tour/_loadtours.php";
	int page = 0;
	int maxPage = 15;
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        page = 1;
       
        
		try {	
			do {
		        String mockParam = param;
				mockParam = mockParam.replace("{page}",String.valueOf(page)).replace("{param}",targetUrl).replace("{param2}",targetUrl);
				String response = httpPostRequestWithStatus(url,mockParam,"UTF-8",true,false)[0];
		        String charset = "UTF-8";
		        http.setURL("https://www.thaitravelcenter.com/"+requestUrl);    
		        if(response.contains("ครบแล้ว ")) {
		        	break;
		        }
		        String mockTargetUrl = "https://www.thaitravelcenter.com/"+target.getUrl();
		        HTMLParser parse = new HTMLParser();
		        if(charset != null) {
		        	parse.source = new StringBuffer(processHTMLBeforeParse(response));
		        } 
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(mockTargetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(mockTargetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(mockTargetUrl).getHost())) {

		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     	                	
		                }
		            }
		        }
		        if(BaseConfig.CONF_WCE_SLEEP_TIME > 0) {
		        	Thread.sleep(BaseConfig.CONF_WCE_SLEEP_TIME);
		        }
		        page++;
			}while(page <= maxPage);
	        completePage(targetMerchantId, http, STATUS.C, target.getUrl());	
		} catch (Exception e) {
			e.printStackTrace();
	        client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        
		    throw e;
		}
	}
	
	public static String[] httpPostRequestWithStatus(String url, String data, String charset, boolean redirectEnable, boolean isJsonRequestParam) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			
			
			DataOutputStream	outStream = new DataOutputStream(conn.getOutputStream());
			outStream.writeBytes(data);
			outStream.flush();
			outStream.close();
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
			
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
}
