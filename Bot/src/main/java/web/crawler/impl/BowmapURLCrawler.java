package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.BotUtil;
import utils.FilterUtil;

public class BowmapURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String area = FilterUtil.getStringBetween(html, "<section class=\"module-product list-content col-sm-12\">", "</section>");
		area = FilterUtil.getStringAfter(area, "<div id=\"layout-view\" class=\"layout-view layout-view-grid\" style=\"display:none;\">", area);
		List<String> allLinkArea = FilterUtil.getAllStringBetween(area, "<div class=\"item col-xs-6 col-sm-4\">", "<div class=\"caption\">");
		if(allLinkArea != null && allLinkArea.size() > 0) {
			for (String linkArea : allLinkArea) {
				if(linkArea.contains("page=")) {
					continue;
				}
				String link = FilterUtil.getStringBetween(linkArea, "<a href=\"", "\"");
				if(StringUtils.isNotBlank(link)) {
					String pId = FilterUtil.getStringBetween(link,"/products/","-");
					if(pId.isEmpty())continue;
					String newLink = "https://www.bowmap.com/th/products/"+pId+"/json";
					html = html.replace(link, newLink);
				}
			}
		}
		return html;
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		if(url.contains("page=")) {
			url = BotUtil.decodeURL(url);
		}
		url = BotUtil.decodeURL(url);
		url = url.replace("http://","https://");
		return url;
	}
}
