package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BananaBoatLoverURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"container\">", "<div class=\"section-navcate sec\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4\">", "<div class=\"thumb-text-name\">");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
