package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TheBodyShopURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String nextPage =  FilterUtil.getStringBetween(html, "<link rel=\"canonical\"", "</title>");
		String newHTML =  FilterUtil.getStringAfter(html, "<div class=\"grid_19 omega\" ", html);
		List<String> linkList = FilterUtil.getAllStringBetween(newHTML, "href=\"", "\"");
		if(linkList != null && linkList.size() > 0){
			for(String s : linkList){
				if(s.contains("™")){
					String newLink = s.replace("™", "%E2%84%A2");
					newHTML = newHTML.replace(s, newLink);
				}
				
			}
		}
	
		return newHTML+nextPage;
	}
}

