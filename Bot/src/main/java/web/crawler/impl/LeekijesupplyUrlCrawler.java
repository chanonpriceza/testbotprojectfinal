package web.crawler.impl;

import engine.BaseConfig;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLCrawlerClient;

public class LeekijesupplyUrlCrawler extends HeatonURLCrawler {
	@Override
	public void init(URLCrawlerClient client) throws Exception {
		BaseConfig.CONF_URL_CRAWLER_CHARSET = "TIS-620";
		super.init(client);
	}
}
