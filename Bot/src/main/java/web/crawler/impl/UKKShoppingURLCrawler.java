package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class UKKShoppingURLCrawler extends HeatonURLCrawler {

		
	@Override
	public String processURLAfterParse(String url) throws Exception {
		return url.replaceAll("\\s", "");
	}	
		
}