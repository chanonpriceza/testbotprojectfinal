package web.crawler.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLUtility;

public class ThailandBestURLCrawler extends HeatonURLCrawler {
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        
		try {		
		        
				String[] response = httpRequestWithStatus(requestUrl,"UTF-8", true);	
		        http.setURL(requestUrl);        
		        HTMLParser parse = new HTMLParser();
		        parse.source = new StringBuffer(processHTMLBeforeParse(response[0]));
		        
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(targetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
		                	 if(!botExclusion.isExcluded(u.toString())){
		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     }		                	
		                }
		            }
		        }
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());		
		} catch (Exception e) {
			if (!http.getURL().equals(http.getRootURL())) {
	        	client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        } 
		    throw e;
		}
	}
	

	
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		String htmlModify = "";
		Elements e = doc.select("ul.products-grid");
		Elements page = doc.select("div.pages");
		e = e.select("a");
		for(Element el:e) {
			String link = el.attr("href");
			link = genNewURL(link,"isRealProductId","true");
			String genLink = "<a href=\""+link+"\"></a>";
			htmlModify += genLink;
		}
		htmlModify += page.html();
		return htmlModify;
	}
	
	public static String genNewURL(String url,String param,String value) throws Exception{
		URI x = new  URIBuilder(url).addParameter(param,value).build();
		return x.toString();
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
			conn.setRequestProperty("Cookie","SPSI=405ff631b15403c69537bf149b78c968; spcsrf=23a060f092c1c5239a22c20a10bad752; frontend=c5oln2onbevb3tjig30p2bpvc5; frontend_cid=4oymXA8PCPIOJFKI; sp_lit=DuSLvXCO0yC0pOKD9f2kbA==; PRLST=cq; UTGv2=h41ed3bbcd13311ca5f396428f74ee1e8e22; adOtr=ffKI415b351");
			conn.setConnectTimeout(100000);
			conn.setReadTimeout(1000000);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
}
