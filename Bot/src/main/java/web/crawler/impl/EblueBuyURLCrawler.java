package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.HTMLParserUtil;

public class EblueBuyURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String[] tmpList = HTMLParserUtil.getTagList(html,"<div id=\"siblings-container\" class=\"hidable-content\" >");
		if(tmpList != null && tmpList.length > 0){
			for(String tmp:tmpList){
				if(StringUtils.isNoneBlank(tmp)){
					html = html.replace(tmp,"");
				}
			}
		}
		
		return html;
	}
		
}