package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;

public class SptoolsURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {
		String rtnUrl = url ;
		
		if(StringUtils.isNotBlank(url) && url.contains("&amp;pid=")){
			rtnUrl = rtnUrl.replaceAll("&amp;cid=([0-9]*)&amp;pid=", "&amp;pid=");
		}
		
		return rtnUrl;
	}	
	
}
