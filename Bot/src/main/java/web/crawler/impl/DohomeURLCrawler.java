package web.crawler.impl;

import java.net.URI;

import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class DohomeURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String h = "";
		html = FilterUtil.getStringBetween(html, "<div class=\"category-products category-products-list\">", "</html>");
		Document doc = Jsoup.parse(html);
		Elements page = doc.select("div.pager");
		Elements e = doc.select("li.item");
		e = e.select("a");
		for(Element ex:e) {
			URI x = new  URIBuilder(ex.attr("href")).addParameter("isRealProductId","true").build();
			String genLink = "<a href=\""+x+"\"></a>";
			h += genLink;
		}
		h += page.html(); 
		return h;
	}

}
 