package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class TwinkleUrlCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements cut = doc.select("p.listspan");
		Elements cutLink = cut.select("a");
		for(Element e:cutLink) {
			String genLink = "<a href=\""+e.attr("href")+"?isPage=true\"></a>";
			html+=genLink;
		}
		return html;
	}
	public static void main(String[] args) {
		TwinkleUrlCrawler x = new TwinkleUrlCrawler();
		String[] ww = HTTPUtil.httpRequestWithStatus("https://www.twinkledeals.com/clothes-cc38/","UTF-8", true);
		try {
			x.processHTMLBeforeParse(ww[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
