package web.crawler.impl;


import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BeautiCoolURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String protype_id = FilterUtil.getStringBetween(html, "<input type=\"hidden\" id=\"protype_id\" value=\"","\"");
		String cat_id = FilterUtil.getStringBetween(html, "category_id :",",").trim();
		
		if(html.contains("<div id=\"product\">")){
			String rHtml = html;
			String url = "https://www.beauticool.com/category_mod.php?m=get_productnew&category_id="+cat_id+"&orderby=2&protype_id="+protype_id;
			rHtml = rHtml.replace("<div id=\"product\">", "<div id=\"product\"> <a href=\""+url+"\" target=\"_blank\" class=\"txt_grey3\"></a>");
			return rHtml;
		}
		
		if(html.contains("<div id=\"product\" style=\"padding-top:125px;\">")){
			String rHtml = html;
			String url = "https://www.beauticool.com/category_mod.php?m=get_productnew&category_id="+cat_id+"&orderby=2&protype_id="+protype_id;
			rHtml = rHtml.replace("<div id=\"product\" style=\"padding-top:125px;\">", "<div id=\"product\" style=\"padding-top:125px;\"> <a href=\""+url+"\" target=\"_blank\" class=\"txt_grey3\"></a>");
			return rHtml;
		}

		
		
		return html;
	}

	@Override
	public String processURLAfterParse(String url) throws Exception {
		
		if(url.contains("/product/")){
			String postUrl = FilterUtil.getStringAfter(url,"/product/","");
			if(StringUtils.isNotBlank(postUrl)){
				return "https://www.beauticool.com/product/" + postUrl+"/";
			}
		}
		
		return url;
	}
	
}
