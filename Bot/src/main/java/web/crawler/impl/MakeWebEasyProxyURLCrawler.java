package web.crawler.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.SocketFactory;
import web.crawler.bot.URLUtility;

public class MakeWebEasyProxyURLCrawler extends HeatonURLCrawler {
	
	//private static Logger   logger = LogManager.getRootLogger();
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
	
	
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		enableUseProxy();
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        
		try {		
				String[] response = httpRequestWithStatusIgnoreCookies(requestUrl,"UTF-8", true);
		        String charset = "UTF-8";
		        http.setURL(requestUrl);        
		        HTMLParser parse = new HTMLParser();
		        if(charset != null) {
		        	parse.source = new StringBuffer(processHTMLBeforeParse(response[0]));
		        } 
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(targetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
		                	 if(!botExclusion.isExcluded(u.toString())){
		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     }		                	
		                }
		            }
		        }
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());	

		} catch (Exception e) {
			e.printStackTrace();
	        client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        
		    throw e;
		}
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null){
				try { in.close(); } catch (IOException e) {}
			}
		}
	}
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	public static String[] httpRequestWithStatusIgnoreCookies(String url, String charset, boolean redirectEnable) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", USER_AGENT);
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while((line = brd.readLine()) != null)
    					rtn.append(line);
    				return new String[]{rtn.toString(), String.valueOf(status)};
    			}
    		}else{
				return new String[]{null, String.valueOf(status)};
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpGet != null)
				httpGet.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    }
	
}
