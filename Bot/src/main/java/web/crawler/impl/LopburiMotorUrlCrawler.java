package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class LopburiMotorUrlCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> productLink = FilterUtil.getAllStringBetween(html,"<div class=\"layer-media\">","</a>");
		for(String link:productLink){
			String cutLink = FilterUtil.getStringBetween(link,"href=\"","\"");
			link = link.replaceAll(cutLink, cutLink+"?product=true");
			html +=  link;
		}
		return html;
	}
}
