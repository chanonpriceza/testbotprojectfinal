package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class RosewhoesaleURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String mLink = FilterUtil.getStringBetween(html, "<div class=\"proList clearfix mt40\" id=\"js_proList\">", "<div class=\"search_key\">");
		List<String> allLinkArea = FilterUtil.getAllStringBetween(mLink, "<p class=\"pr\">", "</p>");
		if(allLinkArea != null && allLinkArea.size() > 0) {
			for (String linkArea : allLinkArea) {
				String link = FilterUtil.getStringBetween(linkArea, "href=\"", "\"");
				if(StringUtils.isNotBlank(link)) {
					html = html.replace(link, link+"?ProductisTrue");
				}
			}
		}
		
		return html;
	}


}


