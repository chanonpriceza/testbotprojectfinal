package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class BeautynistaURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.i-all-product-container");
		return e.html();
		
	}
}
