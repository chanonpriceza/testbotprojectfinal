package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class AsFurnitureURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHTML = FilterUtil.getStringBetween(html,"<div class=\"category-products\">","<div class=\"ma-footer-top\">");
		List<String> productList = FilterUtil.getAllStringBetween(cutHTML,"href=\"","\"");
		for(String url:productList) {
			
			String genLink = "<a href=\""+url+"?isRealProduct=true"+"\">";
			html += genLink;
		}
		
		return html;
	}
}
