package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class AllBetterURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		String genNextPage = "";
		Elements e = doc.select("meta[property=og:url]");
		String link = e.attr("content");
		for(int i=2;i<=5;i++) {
			String geString = "<a href=\""+link+"?itemPerPage=16&page="+i+"&viewType=Grid&sortField=Name&sortType=ASC"+"\"></a>";
			genNextPage+=geString;
		}
		html += genNextPage;
		return html;
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		return url.replace("?","#&&");
	}

}
