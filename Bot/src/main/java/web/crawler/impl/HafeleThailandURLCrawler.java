package web.crawler.impl;

import utils.BotUtil;
import web.crawler.bot.HeatonURLCrawler;

public class HafeleThailandURLCrawler extends HeatonURLCrawler {
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = BotUtil.decodeURL(url);
		return url;
	}

}
