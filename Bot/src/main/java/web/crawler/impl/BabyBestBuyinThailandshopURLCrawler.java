package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class BabyBestBuyinThailandshopURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		//parse content zone
		String content = FilterUtil.getStringBetween(html, "<div class=\"block-products-list\">", "<div class=\"block-offers\">");
		if(content.trim().length() != 0) {			
			content = "<div class=\"block-products-list\">" + content;
		}
		
		//parse cat zone
		String cat = FilterUtil.getStringBetween(html, "<div class=\"block-categories-list\">", "<div class=\"block-products-recently-viewed\">");
		if(cat.trim().length() != 0) {			
			cat = "<div class=\"block-categories-list\">" + cat;
		}
				
		String rtnHtml = "<div>" + content + cat + "</div>";
				
		return rtnHtml;		
	}
	
}
