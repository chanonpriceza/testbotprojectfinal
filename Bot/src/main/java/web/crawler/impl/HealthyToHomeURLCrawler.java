package web.crawler.impl;

import java.util.List;

import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class HealthyToHomeURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutString = FilterUtil.getStringBetween(html,"class=\"wpb_wrapper\">","aside");
		List<String> parseLink = FilterUtil.getAllStringBetween(cutString,"href=\"","\"");
		for(String link:parseLink) {
			link = BotUtil.decodeURL(link);
			String genLink = "<a href=\""+link+"?isProduct=true\"></a>";
			html += genLink;
		}
		
		return html;
	}
	
}
