package web.crawler.impl;
import java.net.URLDecoder;

import web.crawler.bot.HeatonURLCrawler;

public class NanajipataURLCrawler extends HeatonURLCrawler {

	@SuppressWarnings("deprecation")
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = URLDecoder.decode(url);
		return super.processURLAfterParse(url);
	}
	
}
