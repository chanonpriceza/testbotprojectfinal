package web.crawler.impl;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import engine.BaseConfig;
import manager.UrlPatternManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
//import web.crawler.bot.HttpRequestURLCrawler;
import web.crawler.bot.URLCrawlerClient;
import web.crawler.bot.URLCrawlerInterface;
import web.crawler.bot.URLUtility;

//public class AVValueAdvanceUrlCrawler extends HttpRequestURLCrawler {
//
//	@Override
//	public String processHTMLBeforeParse(String html) throws Exception {
//		
//		html = FilterUtil.getStringBetween(html,"<main id=\"main\" class=\"site-main\">","<div id=\"sidebar\" class=\"sidebar\" role=\"complementary\">");
//		return html;
//	}
//	
//	@Override
//	public String processURLAfterParse(String url) throws Exception {
//		url = BotUtil.decodeURL(url);
//		return url;
//	}
//	
//}


public class AVValueAdvanceUrlCrawler implements URLCrawlerInterface {
	
	protected URLCrawlerClient client;
	
	@Override
	public void init(URLCrawlerClient client) throws Exception {
		this.client = client;
		
		String startUrl = UrlPatternManager.getInstance().getFirstStartUrl();
        if(StringUtils.isBlank(startUrl)) {
        	throw new Exception("not found start url");
        }
	}	
	
	@Override
	public void process(WorkloadBean target) throws Exception {
		if(StringUtils.isNotBlank(target.getUrl())) {
			
			String[] htmlContent = null;
			if(BaseConfig.CONF_DISBLE_PARSER_COOKIES || HTTPUtil.USE_PROXY) {
				htmlContent = HTTPUtil.httpRequestWithStatusIgnoreCookies(target.getUrl(), BaseConfig.CONF_URL_CRAWLER_CHARSET, true);
			}else {
				htmlContent = HTTPUtil.httpRequestWithStatus(target.getUrl(), BaseConfig.CONF_URL_CRAWLER_CHARSET, true);
			}
			
			if (htmlContent == null || htmlContent.length != 2) {
				completePage(target, STATUS.E);
				return;
			}
			
			String httpStatus = htmlContent[1];
			if(httpStatus.equals(String.valueOf(HttpURLConnection.HTTP_BAD_REQUEST))) {
				completePage(target, STATUS.E);
				return;
			}
			
			String html = htmlContent[0];
			if(StringUtils.isBlank(html)) {
				completePage(target, STATUS.E);
				return;
			}

			HTMLParser parse = new HTMLParser();
	        parse.source = new StringBuffer(processHTMLBeforeParse(html));
	        while (!parse.eof()) {
	            char ch = parse.get();
	            if (ch == 0) {
	                HTMLTag tag = parse.getTag();
	                Attribute link = tag.get("HREF");
	                if (link == null)
	                    link = tag.get("SRC");

	                if (link == null)
	                    continue;

	                URL u = null;     
	                try {
	                    if (link.getValue().startsWith("?")) {
	                        u = URLUtility.addNewQuery(new URL(target.getUrl()), link.getValue());
	                    } else {
	                        u = new URL(new URL(target.getUrl()), link.getValue());
	                    }
	                } catch (MalformedURLException e) {
	                    continue;
	                }

	                u = URLUtility.stripAnhcor(u);
	                u = URLUtility.formatToValidURL(u);
	                
	                if (u.getHost().equalsIgnoreCase(new URL(target.getUrl()).getHost())) {
	            		 String url = processURLAfterParse(u.toString());
	            		 client.foundInternalLink(target.getMerchantId(), url, target.getDepth() + 1, target.getCategoryId(), target.getKeyword());
	                }
	            }
	        }	
			completePage(target,STATUS.C);
			return;
			
		} else {
			completePage(target, STATUS.E);
			return;
		}
		
	}
    
	public String processHTMLBeforeParse(String html) throws Exception {
		
		html = FilterUtil.getStringBetween(html,"<main id=\"main\" class=\"site-main\">","<div id=\"sidebar\" class=\"sidebar\" role=\"complementary\">");
		return html;
	}
	
	public String processURLAfterParse(String url) throws Exception {
		url = BotUtil.decodeURL(url);
		return url;
	}
	
	public void completePage(WorkloadBean bean, STATUS status) throws SQLException {
		client.completePage(bean.getMerchantId(), bean.getUrl(), status);
	}
	
}
