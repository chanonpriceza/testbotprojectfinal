package web.crawler.impl;

import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class AppleStoreURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<meta property=\"og:url\" content=\"", "\"");
		html = "<a href=\""+html+"\"></a>";
		
		return html;
	}
	
	public static void main(String[] args) throws Exception {
		AppleStoreURLCrawler x = new AppleStoreURLCrawler();
		 String xxx = x.processHTMLBeforeParse(HTTPUtil.httpRequestWithStatus("https://www.apple.com/th/shop/buy-ipad/ipad-mini","UTF-8", true)[0]);
		 System.out.println(xxx);
	}

}