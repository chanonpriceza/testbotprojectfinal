package web.crawler.impl;

import java.util.List;

import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class FloristSupplyURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<ul class=\"products columns-4\">", "<footer id=\"colophon\" class=\"site-footer\">");		
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<li class=\"product", "<span class=\"price\">");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}

	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = BotUtil.decodeURL(url);
		return url;
	}
	
	
}

