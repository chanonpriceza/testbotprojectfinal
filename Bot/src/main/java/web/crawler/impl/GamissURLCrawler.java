package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class GamissURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		if (StringUtils.isNotBlank(html)) {
				html = FilterUtil.getStringBetween(html, "<body class=\"min-w1200 lag-en\">", "</html>");
		}
		return html;
	}
}