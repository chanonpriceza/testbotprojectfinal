package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class RakbagURLCrawler extends HeatonURLCrawler {
	@Override
	public String processURLAfterParse(String url) throws Exception {
		String cutUrl = "";
		String[] splitUrlFirst = url.split("/");
		if (splitUrlFirst.length > 0) {
			cutUrl = splitUrlFirst[splitUrlFirst.length - 1];
		}
		String[] splitUrl = cutUrl.split("-");
		if (splitUrl.length > 0) {
			url = url.replaceAll(splitUrl[0], "t");
		}
		return url;
	}
}
