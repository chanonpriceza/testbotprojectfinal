package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class MonzyshopURLCrawler extends HeatonURLCrawler {
	
	public String processURLAfterParse(String url) throws Exception {
		return url.replace("\t", "");
	}	

}
