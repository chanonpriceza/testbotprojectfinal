package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class LeviURLCrawler extends HeatonURLCrawler {
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		String cutHTML = "";
		Elements e = doc.select("ol.product-items");
		e = e.select("a");
		for(Element ex:e) {
			String g = "<a href=\""+ex.attr("href")+"?isRealProduct=true\"></a>";
			cutHTML += g;
		}
		Elements p = doc.select("div.pages");
		cutHTML += p.html();
		return cutHTML; 
	}
	
	public static void main(String[] args) throws Exception {
		LeviURLCrawler x = new LeviURLCrawler();
		 String xxx = x.processHTMLBeforeParse(HTTPUtil.httpRequestWithStatus("https://www.levis.co.th/men.html","UTF-8", true)[0]);
		 System.out.println(xxx);
	}
	
}
