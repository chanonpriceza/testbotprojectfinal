package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ServerProThaiURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		if (html.contains("<div id=\"list-view\" class=\"tab-pane \" role=\"tabpanel\">")) {
			StringBuilder rtnHtml = new StringBuilder();
			rtnHtml.append(html.replace("</html>", ""));
			try{
				String targetContent = FilterUtil.getStringBetween(html, "<div id=\"list-view\" class=\"tab-pane \" role=\"tabpanel\">", "<div id=\"grid\" class=\"tab-pane \" role=\"tabpanel\">");
				if(targetContent != null && StringUtils.isNotBlank(targetContent)){
					List<String> productList = FilterUtil.getAllStringBetween(targetContent, "<li class=\"list-view post-", "</a>");
					if(productList != null && productList.size() > 0){
						for (String product : productList) {
							String link = FilterUtil.getStringBetween(product, "<a href=\"", "\"");
							if(!link.contains("isProductLink=")){
								link = link + "?isProductLink=true";
							}
							rtnHtml.append("<a href=\""+link+"\">addLink</a>");
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			rtnHtml.append("</html>");
			return rtnHtml.toString();
		}
		return html;
		
	}
}
