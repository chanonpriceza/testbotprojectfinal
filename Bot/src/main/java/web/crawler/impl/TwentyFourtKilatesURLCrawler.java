package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TwentyFourtKilatesURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> datas = FilterUtil.getAllStringBetween(html,"<a class=\"product_img_link\" href=\"","\"");
		for (String  data: datas) {
			html = html.replaceAll(data,data+"?isProductlink=true");
		}
		return super.processHTMLBeforeParse(html);
	}
}
