package web.crawler.impl;

import utils.FilterUtil;
import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class ModernSiamURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<meta property=\"og:url\" content=\"", "\"");
		html = "<a href=\""+html+"\"></a>";
		
		return html;
	}
	
	public static void main(String[] args) throws Exception {
		ModernSiamURLCrawler x = new ModernSiamURLCrawler();
		 String xxx = x.processHTMLBeforeParse(HTTPUtil.httpRequestWithStatus("https://modern-siam.com/?page_id=1194","UTF-8", true)[0]);
	}
}