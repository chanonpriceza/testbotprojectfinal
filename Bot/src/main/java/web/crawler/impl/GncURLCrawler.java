package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;

public class GncURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replace("/product/product/", "/product/");
		if(StringUtils.countMatches(url, "/product/") == 2){
			return "http://www.gnc.co.th";
		}
		return url;
	}	
		
}