package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class NpjbraviaURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String link =  FilterUtil.getStringBetween(html, "<ul class=\"subpages\">", "<div id=\"content-visit-history-container\">");
		List<String> allLink = FilterUtil.getAllStringBetween(link, "href=\"", "\">");
		if(!allLink.isEmpty()){
			for(String url : allLink){
				html += "<a href=\"https://www.npjbravia.com"+ url +"?ProductisTrue\"></a>";
			}
		}
		return html;
	}
		
}