package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ThailedmartURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String fakeLink = FilterUtil.getStringBetween(html, "<link rel=\"prerender\" href=\"", "\"");
		return html.replace(fakeLink, "");
	}
	
}

