package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class SRMCHomeURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> allLink = FilterUtil.getAllStringBetween(html, "onclick=\"location.href='", "'");
		String rtnHtml = html;
		if(allLink != null && allLink.size() > 0){
			for (String link : allLink) {
				rtnHtml += "<a href=\"" + link + "\">" + link + "</a>";
			}
		}
		String allPageDiv = FilterUtil.getStringBetween(html,"<div id=\"paginationdiv\" class=\"align-right\" style=\"margin-top:30px;\">","</div>");
		List<String> nextPageList = FilterUtil.getAllStringBetween(allPageDiv,"<a","</a>");
		for(String nextPage:nextPageList){
			String url = FilterUtil.getStringBetween(nextPage, "href=\"","\"");
			url = url.replaceAll("&p","&realProduct=true&p");
			rtnHtml += "<a href=\"" + url + "\">" + url + "</a>";
		}
		return rtnHtml;		
	}
}
