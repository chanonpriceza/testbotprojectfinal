package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;
import utils.HTTPUtil;

public class StardustShopsURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String pageLink = FilterUtil.getStringBetween(html, "<nav id=\"page_nav\">", "</nav>");
		pageLink  = FilterUtil.getStringBetween(pageLink, "href=\"", "\"");
		pageLink  = FilterUtil.getStringBeforeLastIndex(pageLink, "/", pageLink);
		int page = 2 ;
		if(pageLink.isEmpty()){
			return html;
		}
		while(!Thread.currentThread().isInterrupted()){
			String[] rHtml = HTTPUtil.httpRequestWithStatus(pageLink+"/"+page, "utf-8", false);
			if(rHtml == null || rHtml.length == 0)
				break;
			
			
			html = html.replace("<nav id=\"page_nav\">", rHtml[0]+"<nav id=\"page_nav\">");
			page++;
			
		}
		return html;
	}

}
