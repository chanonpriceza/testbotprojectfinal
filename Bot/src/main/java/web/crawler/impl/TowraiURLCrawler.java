package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TowraiURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		// rtnHtmlTODO Auto-generated method stub
		String rtnHtml = html;
		
		List<String> productLi = FilterUtil.getAllStringBetween(rtnHtml, "<li class=\"item last\">", "</li>");
		if(productLi != null && productLi.size() > 0){
			for (String product : productLi) {
				List<String> productLink = FilterUtil.getAllStringBetween(product, "href=\"", "\"");
				if(productLink != null && productLink.size() > 0){
					for (String link : productLink) {
						if(!link.contains("isProductLink")){
							if(link.contains("?")){
								rtnHtml = rtnHtml.replace(link, link+"&isProductLink=true");
							}else{
								rtnHtml = rtnHtml.replace(link, link+"?isProductLink=true");
							}
						}
					}
				}
			}
		}
		
		rtnHtml = rtnHtml.replace("http:", "https:");

		return rtnHtml;
	}

	
	
}
