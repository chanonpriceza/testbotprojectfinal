package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class SahapatHomeDeliveryURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = FilterUtil.getStringBetween(html, "<div class=\"category-products\">", "<section class=\"brandlogo-wrapper\">");
		if(StringUtils.isNotBlank(rtnHtml)){
			return rtnHtml;
		}else{
			return html;
		}
	}
}

