package web.crawler.impl;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class PordeeShopURLCrawler extends HeatonURLCrawler {
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replaceAll("http://","https://");
		return url;
	}
	
	public static void main(String[] args) {
		PordeeShopURLCrawler x = new PordeeShopURLCrawler();
		String[] ww = HTTPUtil.httpRequestWithStatus("https://www.asiabooks.com/thaibooks/art-design.html","UTF-8", true);
		try {
			x.processHTMLBeforeParse(ww[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
