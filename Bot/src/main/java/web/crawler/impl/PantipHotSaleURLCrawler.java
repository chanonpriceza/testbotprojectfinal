package web.crawler.impl;

import java.net.URLDecoder;

import web.crawler.bot.HeatonURLCrawler;

public class PantipHotSaleURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {
		String rtnUrl = "";
		rtnUrl = URLDecoder.decode(url, "UTF-8");
		return rtnUrl;
	}
}
