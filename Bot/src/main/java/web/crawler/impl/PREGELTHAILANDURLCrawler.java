package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.HTTPUtil;
import web.crawler.bot.HeatonURLCrawler;

public class PREGELTHAILANDURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.item-grid");
		String cutHTML = e.html();
		Elements page = doc.select("div.pager");
		cutHTML+= page.html();
		return cutHTML;
	}
	
	
	public static void main(String[] args) {
		PREGELTHAILANDURLCrawler x = new PREGELTHAILANDURLCrawler();
		String[] f = HTTPUtil.httpRequestWithStatus("https://pregelthailandshop.com/3dpowder","UTF-8",true);
		try {
			x.processHTMLBeforeParse(f[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
