package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ShopAt24HotDealURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		String area = FilterUtil.getStringBetween(html, "<div class=\"flashsale-wrapper\">", "<div class=\"row product_grid outer hidden js-campaign is-upcoming-campaign\"");
		
		return area;
	}
		
}