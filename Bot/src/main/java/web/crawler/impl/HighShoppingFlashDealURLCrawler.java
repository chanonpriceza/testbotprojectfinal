package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class HighShoppingFlashDealURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String firstLink = FilterUtil.getStringBetween(html,"<div class=\"main-section\">","</a>");
		firstLink = FilterUtil.getStringBetween(firstLink,"href=\"","\"");
		firstLink += "?isDealProduct=true";
		firstLink = "<a href=\""+firstLink+"\" ></a>";
		
		return firstLink;
	}
}
