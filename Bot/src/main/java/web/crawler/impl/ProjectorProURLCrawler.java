package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ProjectorProURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		List<String> allLink = FilterUtil.getAllStringBetween(html, "window.location='", "';");
		if(!allLink.isEmpty()){
			for(String url : allLink){
				html += "<a href=\""+ url +"\"></a>";
			}
		}
		return html;
	}
		
}