package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.BotUtil;
import utils.FilterUtil;

public class TrekkingthaiURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
	html = FilterUtil.getStringBetween(html, "<div id=\"wrapper\">", "<div class=\"footer-widgets footer footer-1\">");
	html = BotUtil.decodeURL(html);
	return html;
	}
}
