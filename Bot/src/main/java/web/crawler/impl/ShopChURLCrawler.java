package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ShopChURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
				
		//parse content zone
		
		String rtnHtml = FilterUtil.getStringBefore(html, "<div class=\"box-related\">", "");
		
		return rtnHtml;		
	}
}
