package web.crawler.impl;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class AuroraURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"row mar-top-30\">", "</html>");
		
		return html;
	}

}
