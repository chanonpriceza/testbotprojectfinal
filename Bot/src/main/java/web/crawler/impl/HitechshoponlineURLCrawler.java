package web.crawler.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class HitechshoponlineURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		
		List<String> list = FilterUtil.getAllStringBetween(html, "<a style=\"cursor:hand\" onclick=\"location='","'");
		
		if(list.size()==0){
			return StringEscapeUtils.unescapeHtml4(html);
		}else{
			List<String> newString = new ArrayList<String>();
			for(String s : list){
				if(s.trim().length() > 0){
					s = "<a href=\"http://www.hitechshoponline.com"+s.toString()+"\"></a>";
					newString.add(s);
				}
			}
			return StringEscapeUtils.unescapeHtml4(html + newString.toString());
		}
	}

	
}