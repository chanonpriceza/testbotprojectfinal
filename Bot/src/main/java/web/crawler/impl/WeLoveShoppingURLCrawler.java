package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class WeLoveShoppingURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
				
		//parse content zone
		
		String rtnHtml = html.replace("http://portal.weloveshopping.com/product/","http://store.weloveshopping.com/product/")
							 .replace("https://portal.weloveshopping.com/product/","https://store.weloveshopping.com/product/")
							 .replace("http://","https://");
		
		return rtnHtml;		
	}
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		
		//parse content zone
		
		String rtnHtml = url.replace("http://store.weloveshopping.com/product/","http://portal.weloveshopping.com/product/")
							.replace("https://store.weloveshopping.com/product/","https://portal.weloveshopping.com/product/")
							.replace("http://","https://");
		
		return rtnHtml;	
	}	
	
	
	
}
