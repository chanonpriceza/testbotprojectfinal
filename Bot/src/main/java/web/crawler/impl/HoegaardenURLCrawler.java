package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class HoegaardenURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("div.list-group");
		Elements page = doc.select("ul.pagination");
		e = e.select("a");
		String ret = "";
		for(Element x:e) {
			String genLink = "<a href=\""+x.attr("href")+"?isRealProduct=true\"></a>";
			ret += genLink;
			//System.out.println(genLink);
		}
		ret += page.html();
		
		return ret;
	}
}
