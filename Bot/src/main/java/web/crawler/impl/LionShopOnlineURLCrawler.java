package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class LionShopOnlineURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {

		html = FilterUtil.getStringBetween(html, "<div class=\"col-main\">", "<div class=\"toolbar-bottom\" style=\"clear:both;\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"product-item hover-effect\">", "<div class=\"price-box\">");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<h3 class=\"product-name\"><a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
	
	
//	@Override
//	public String processHTMLBeforeParse(String html) throws Exception {
//		html = FilterUtil.getStringBetween(html, "<div class=\"category-products long-box-shadow\">", "<div class=\"toolbar-bottom\" style=\"clear:both;\">");
//		return html;
//	}
}