package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class IndexLivingMallURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
	
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<div class=\"col-sm-3 item itemCol3\">", "<img class=\"img-fluid lists_imgFluid img-list_zoom");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				if(productLink.contains(".jpg")) continue;
				if(productLink.contains("/add/")) continue;
				if(!productLink.contains("isProductLink")){
					if(productLink.contains("?")){
						rtnHtml = rtnHtml.replace(productLink, productLink+"&isProduct=true");
					}else{
						rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
					}
				}
			}
			return rtnHtml;
		}
		return html;
	}

	
	
}
