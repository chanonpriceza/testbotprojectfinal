package web.crawler.impl;

import java.util.List;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class Best2HomeURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHtml = FilterUtil.getStringBetween(html," <div class=\"col-sm-12 text-center\">","Next");
		List<String> urlList = FilterUtil.getAllStringBetween(cutHtml,"href=\"","\"");
		
		for(String url:urlList) {
			String genLink = "<a href=\""+url+"?isNextPage=true"+"\"></a>";
			html += genLink;
		}
		return html;
	}
}
