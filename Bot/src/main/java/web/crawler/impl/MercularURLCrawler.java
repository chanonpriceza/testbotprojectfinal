package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class MercularURLCrawler extends HeatonURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutProductBox = FilterUtil.getStringBetween(html,"<ul class=\"products-list grid\">","<div class=\"row container pagination-container\">");
		if(StringUtils.isBlank(cutProductBox)){
			cutProductBox = FilterUtil.getStringBetween(html,"<ul class=\"products\">","<div class=\"taxonomy-description\">");
		}
		List<String> linkList = FilterUtil.getAllStringBetween(cutProductBox,"href=\"","\"");
		for(String link:linkList){
			String createLink = "<a href=\""+link+"?isProductLink=true"+"\">";
			html += createLink;
		}
		return html;
	}
}
