package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class UnderwearSecretURLCrawler extends HeatonURLCrawler{
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {

		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "a href=\"", "\"");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				if(productLinkCode.contains("&p=")) {
					String cutproductLinkCode = FilterUtil.getStringBefore(productLinkCode, "&p=", "");
					rtnHtml = rtnHtml.replace(productLinkCode, cutproductLinkCode);		
				}
			}
			return rtnHtml;
		}
		
		return html;
	}

	
	
}
