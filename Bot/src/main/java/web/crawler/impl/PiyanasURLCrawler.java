package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;

public class PiyanasURLCrawler extends HeatonURLCrawler {
	@Override
	public String processURLAfterParse(String url) throws Exception {
		url = url.replaceAll("\r\n","");
		return url;
	}
}
