package web.crawler.impl;

import org.apache.commons.text.StringEscapeUtils;

import web.crawler.bot.HeatonURLCrawler;

public class AGNConsultanHaURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = html.replaceAll("onclick=\"location=\'/","href=\"http://m4supply.com/");
		html = html.replaceAll("';","");
		return StringEscapeUtils.unescapeHtml4(html);		
	}
		
	@Override
	public String processURLAfterParse(String url) throws Exception {	
		return url.replace("\t", "%20");
	}	
		
}