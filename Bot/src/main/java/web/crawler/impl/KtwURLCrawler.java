package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class KtwURLCrawler extends HeatonURLCrawler {
	
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"product-grid\">", "<div class=\"pager\">");
		return html;
	}
		
}