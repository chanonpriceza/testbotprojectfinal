package web.crawler.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class ThreeTfurnitureURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String mLink = FilterUtil.getStringBetween(html, "<div class=\"product-body row\">", "<div class=\"col-md-12 col-xs-12 col-sm-12 col-lg-12 pagination\">");
		List<String> allLinkArea = FilterUtil.getAllStringBetween(mLink, "<div class=\"product-layout product-list col-xs-12\">", "<div class=\"img-icon\">");
		if(allLinkArea != null && allLinkArea.size() > 0) {
			for (String linkArea : allLinkArea) {
				String link = FilterUtil.getStringBetween(linkArea, "href=\"", "\"");
				if(StringUtils.isNotBlank(link)) {
					html = html.replace(link, link+"?ProductisTrue");
				}
			}
		}
		
		return html;
	}
	
}

