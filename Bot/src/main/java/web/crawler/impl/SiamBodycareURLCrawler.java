package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class SiamBodycareURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		if (StringUtils.isNotBlank(html)) {
			if (html.indexOf("<div class=\"category-products\">") > -1) {
				return FilterUtil.getStringBetween(html, "<div class=\"category-products\">", "<div class=\"footer-container\">");
			}
		}
		return html;
	}
	
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		
		return FilterUtil.getStringBefore(url, "\\", url);
	}	
}
