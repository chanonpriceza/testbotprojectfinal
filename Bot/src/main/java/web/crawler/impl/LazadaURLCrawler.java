package web.crawler.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import engine.BaseConfig;
import utils.BotUtil;
import utils.FilterUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.SocketFactory;
import web.crawler.bot.URLUtility;

public class LazadaURLCrawler extends HeatonURLCrawler {
	
	private static Logger   logger = LogManager.getRootLogger();
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	private static JSONParser jsonParser = new JSONParser();
	
	private int maxPage = 1;
	private int page = 1;

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		if(html==null) {
			return "";
		}
		String data = FilterUtil.getStringBetween(html, "window.pageData=", "</script>");
		if(data==null)
			return "";
		JSONObject jsonObj = (JSONObject) jsonParser.parse(data);
		JSONObject jsonObjMods = (JSONObject) jsonObj.get("mods");
		JSONArray  jsonArray = (JSONArray) jsonObjMods.get("listItems");
		List<String> urlList = new ArrayList<String>();
		for(Object j:jsonArray) {
			JSONObject jObj = (JSONObject) j;
			String link = String.valueOf(jObj.get("productUrl"));
			link = FilterUtil.getStringBetween(link,"//","?mp=1");
			urlList.add(link);
		}
		//List<String> urlList = FilterUtil.getAllStringBetween(data, "\"productUrl\":\"//", "\"");
		String totalResult = FilterUtil.getStringBetween(data, "\"totalResults\":\"", "\"");
		String size = FilterUtil.getStringBetween(data, "\"pageSize\":\"", "\"");
		//logger.info("data : "+data);
		for (String link: urlList) {
			link  = FilterUtil.getStringBefore(link, "?mp=1", link);
			html += "<link href=\"https://"+link+"\">";
		}
		
		if (page == 1 && StringUtils.isNotBlank(totalResult) && StringUtils.isNotBlank(size)) {
			int page = (int) Math.ceil(Double.parseDouble(totalResult)/Double.parseDouble(size));
			maxPage = page;
		}
		
		logger.info("maxPage: "+maxPage);
		html = html.replace("?&", "?");
		
		return html;
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception{
		if(!USE_PROXY)
			enableUseProxy();
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        String requestUrl = target.getRequestUrl();
        page = 1;
        
		try {		
		      do {
				String response = httpRequest(requestUrl+(requestUrl.contains("/?") ?"&":"?") + "page="+page,"UTF-8", true);
				logger.info(requestUrl+(requestUrl.contains("/?") ?"&":"?") + "page="+page);
		        String charset = "UTF-8";
		        http.send(requestUrl, null);        
		        HTMLParser parse = new HTMLParser();
		        if(charset != null) {
		        	parse.source = new StringBuffer(processHTMLBeforeParse(response));
		        } 
		        
		        // find all the links
		        while (!parse.eof()) {
		            char ch = parse.get();
		            if (ch == 0) {
		                HTMLTag tag = parse.getTag();
		                Attribute link = tag.get("HREF");
		                if (link == null)
		                    link = tag.get("SRC");
		
		                if (link == null)
		                    continue;
		
		               // ComCare                    
		                if(targetMerchantId == 205) {
		                	// current page = http://www.goodandsave.com/store/2/category/327
		                    // target link = goods/329                    	
		                	if(!link.getValue().startsWith("http") && !link.getValue().startsWith("/")) {
		                		link.setValue("http://www.goodandsave.com/" + link.getValue());
		                	}
		                } else if(targetMerchantId == 270) {  
		                	String tmpUrl = link.getValue();                    	
		                	if(tmpUrl != null && tmpUrl.startsWith("javascript:callfrm")) {
		                		tmpUrl = FilterUtil.getStringBetween(tmpUrl, "'", "'");
		                		link.setValue(tmpUrl);
		                	}                    	
		                } else if(targetMerchantId == 275) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                } else if(targetMerchantId == 282) {  
		                	String tmpUrl = link.getValue();
		                	if(tmpUrl != null) {
		                		tmpUrl = BotUtil.convertInvalidChar(tmpUrl, 120);
		                    	link.setValue(tmpUrl);
		                	} 
		                }
		                
		                URL u = null;     
		                try {
		                    if (link.getValue().startsWith("?")) {
		                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
		                    } else {
		                        u = new URL(new URL(targetUrl), link.getValue());
		                    }
		                } catch (MalformedURLException e) {
		                    continue;
		                }
		
		                u = URLUtility.stripAnhcor(u);
		                
		                // change www.ha.com/../index.html -> www.ha.com/index.html
		                u = URLUtility.formatToValidURL(u);
		                
		                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
		                	 if(!botExclusion.isExcluded(u.toString())){
		                		 
		                		 String url = processURLAfterParse(u.toString());
		                		 
		                		 client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);
		                     }		                	
		                }
		            }
		        }
		        if(BaseConfig.CONF_WCE_SLEEP_TIME > 0) {
		        	Thread.sleep(BaseConfig.CONF_WCE_SLEEP_TIME);
		        }
		        completePage(targetMerchantId, http, STATUS.C, target.getUrl());	
		        page++;
		      }while(page <= maxPage);
		} catch (Exception e) {
			e.printStackTrace();
	        client.completePage(targetMerchantId, http.getRootURL(), STATUS.E);
	        
		    throw e;
		}
	}
	
	
	public String httpRequest(String url, String charset, boolean redirectEnable) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
		// cookies from 20180125 11.32 
		httpGet.addHeader("cookie", "_bl_uid=zmjj6cgbqm23mtf1mcX7nhpgt1ww; sid=0.31569499831342; PHPSESSID_27981be0ca23b42ba5661bbace0c1d22=10bfa5e02794fe33641422e165dbf98a; lzd_cid=093b4dc3-bf7d-4d89-bffb-7c51de28e269; t_uid=093b4dc3-bf7d-4d89-bffb-7c51de28e269; t_fv=1516618377918; t_sid=2yXlgBsT5ugGhl9oOkpd8tXKuNavrcat; __utma=165234105.1089536378.1516618378.1516618378.1516618378.1; __utmc=165234105; __utmz=165234105.1516618378.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; cna=irDsEmiDkksCAWVshFt4iPW6; hng=TH|th|THB|764; userLanguageML=th; AMCV_126E248D54200F960A4C98C6%40AdobeOrg=-1506950487%7CMCMID%7C55779283494891040497627813694587648856%7CMCAAMLH-1517223179%7C6%7CMCAAMB-1517223179%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCAID%7CNONE; gpv_pn=campaign%3A; s_vnum=1548154379695%26vn%3D1; s_invisit=true; _tsm=m%3DDirect%2520%252F%2520Brand%2520Aware%253A%2520Typed%2520%252F%2520Bookmarked%2520%252F%2520etc%7Cs%3D%28none%29; s_cc=true; cto_lwid=9f925198-a9d9-410d-bedf-58693d5d8e2f; JSESSIONID=F71389E2D3FC604FF6F94B7421388D35; __utmb=165234105.2.10.1516618378; _uetsid=_uet47847ad4; s_ppvl=D%253Dch%2B%2522%253A%2522%2C100%2C100%2C547%2C1922%2C547%2C1920%2C1080%2C1%2CP; _ceg.s=p2ye8o; _ceg.u=p2ye8o; rr_rcs=eF4FwbsVgCAMBdCGyl2ex5AfbOAaEFJY2Knze28pb5wxlNi9QlYmJKojeR4I1SaucwjLdn_PtXYXAimZUeNe3Tq6AfQDibwRZQ; s_ppv=D%253Dch%2B%2522%253A%2522%2C100%2C12%2C5351%2C1536%2C331%2C1536%2C864%2C1.25%2CP; s_sq=lazwebth%3D%2526pid%253DD%25253Dch%25252B%252522%25253A%252522%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.lazada.co.th%25252Ftescoelectronics%25252F%25253Fspm%25253Da2o4m.campaign.0.0.580584c2rCBEst%252526page%25253D5%2526ot%253DA");
		httpGet.addHeader("referer", url);
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = clientBuliderLogin();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while((line = brd.readLine()) != null)
    					rtn.append(line);
    				return  rtn.toString();
    			}
    		}else{
				return null;
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpGet != null)
				httpGet.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null){
				try { in.close(); } catch (IOException e) {}
			}
		}
	}
	
	public static void disableUseProxy() {
		USE_PROXY = false;
		SocketFactory.setProxyHost(null);
		CLIENT_BUILDER = HttpClients.custom();
	}
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(90000).setConnectTimeout(90000).setSocketTimeout(90000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	private CloseableHttpClient clientBuliderLogin() {
		int countTimeoutLimit = 0;
		CloseableHttpClient conn = null;
		do {
			try {
				conn = CLIENT_BUILDER.build();
			}catch (Exception e) {
				countTimeoutLimit++;
				logger.info("Reconecting Proxy Server ------------------> "+countTimeoutLimit);
				continue;
			}
			break;
		}while(countTimeoutLimit < 3);
		return conn;
		
	}
	
}
