package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class TaladroddURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String page = FilterUtil.getStringBetween(html, "total: ", ",");
		
		for (int count=2; count<Integer.parseInt(page); count++) {
			String link = "<a href=\"http://www.xn--l3caabg1fm8h5ab.com/%E0%B8%9C%E0%B8%A5%E0%B8%81%E0%B8%B2%E0%B8%A3%E0"
					+ "%B8%84%E0%B9%89%E0%B8%99%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%96-{PAGE}.html?page={PAGE}\">";
			html += link.replace("{PAGE}", count+"");
		}
		
		return html;		
	}

}
