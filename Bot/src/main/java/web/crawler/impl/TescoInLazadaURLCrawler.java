package web.crawler.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import engine.BaseConfig;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLUtility;
import utils.FilterUtil;

public class TescoInLazadaURLCrawler extends HeatonURLCrawler {
	
	private static final Logger logger = LogManager.getRootLogger();
	private int maxPage = 1;
	private int page = 1;
	private int count = 0;

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String data = FilterUtil.getStringBetween(html, "window.pageData=", "</script>");
		
		List<String> urlList = FilterUtil.getAllStringBetween(data, "\"productUrl\":\"//", "\"");
		String totalResult = FilterUtil.getStringBetween(data, "\"totalResults\":\"", "\"");
		String size = FilterUtil.getStringBetween(data, "\"pageSize\":\"", "\"");
		
		for (String link: urlList) {
			link  = FilterUtil.getStringBefore(link, "?mp=1", link);
			html += "<link href=\"https://"+link+"\">";
			count++;
		}
		
		if (page == 1 && StringUtils.isNotBlank(totalResult) && StringUtils.isNotBlank(size)) {
			int page = (int) Math.ceil(Double.parseDouble(totalResult)/Double.parseDouble(size));
			maxPage = page;
		}

		html = html.replace("?&", "?");
		return html;
	}

	
	public String httpRequest(String url, String charset, boolean redirectEnable) {
		URL u = null;
    	HttpURLConnection conn = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			
			// cookies from 20180125 11.32 
			conn.setRequestProperty("cookie", "_bl_uid=zmjj6cgbqm23mtf1mcX7nhpgt1ww; sid=0.31569499831342; PHPSESSID_27981be0ca23b42ba5661bbace0c1d22=10bfa5e02794fe33641422e165dbf98a; lzd_cid=093b4dc3-bf7d-4d89-bffb-7c51de28e269; t_uid=093b4dc3-bf7d-4d89-bffb-7c51de28e269; t_fv=1516618377918; t_sid=2yXlgBsT5ugGhl9oOkpd8tXKuNavrcat; __utma=165234105.1089536378.1516618378.1516618378.1516618378.1; __utmc=165234105; __utmz=165234105.1516618378.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; cna=irDsEmiDkksCAWVshFt4iPW6; hng=TH|th|THB|764; userLanguageML=th; AMCV_126E248D54200F960A4C98C6%40AdobeOrg=-1506950487%7CMCMID%7C55779283494891040497627813694587648856%7CMCAAMLH-1517223179%7C6%7CMCAAMB-1517223179%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCAID%7CNONE; gpv_pn=campaign%3A; s_vnum=1548154379695%26vn%3D1; s_invisit=true; _tsm=m%3DDirect%2520%252F%2520Brand%2520Aware%253A%2520Typed%2520%252F%2520Bookmarked%2520%252F%2520etc%7Cs%3D%28none%29; s_cc=true; cto_lwid=9f925198-a9d9-410d-bedf-58693d5d8e2f; JSESSIONID=F71389E2D3FC604FF6F94B7421388D35; __utmb=165234105.2.10.1516618378; _uetsid=_uet47847ad4; s_ppvl=D%253Dch%2B%2522%253A%2522%2C100%2C100%2C547%2C1922%2C547%2C1920%2C1080%2C1%2CP; _ceg.s=p2ye8o; _ceg.u=p2ye8o; rr_rcs=eF4FwbsVgCAMBdCGyl2ex5AfbOAaEFJY2Knze28pb5wxlNi9QlYmJKojeR4I1SaucwjLdn_PtXYXAimZUeNe3Tq6AfQDibwRZQ; s_ppv=D%253Dch%2B%2522%253A%2522%2C100%2C12%2C5351%2C1536%2C331%2C1536%2C864%2C1.25%2CP; s_sq=lazwebth%3D%2526pid%253DD%25253Dch%25252B%252522%25253A%252522%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.lazada.co.th%25252Ftescoelectronics%25252F%25253Fspm%25253Da2o4m.campaign.0.0.580584c2rCBEst%252526page%25253D5%2526ot%253DA");
	 		conn.setRequestProperty("referer", url);
			
			if(conn.getResponseCode() == 200) {
				String enc = conn.getContentEncoding();
				
    			try (InputStream is = conn.getInputStream();
					InputStreamReader isr = charset == null ? 
							new InputStreamReader(enc != null && enc.equals("gzip") ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(enc != null && enc.equals("gzip") ? new GZIPInputStream(is) : is, charset);
	    			BufferedReader brd = new BufferedReader(isr);) {
		    		
		    		StringBuilder rtn = new StringBuilder(5000);
		    		String line = "";
					while ((line = brd.readLine()) != null) {
						rtn.append(line);
					}
					return rtn.toString();
		    	}catch(IOException e){
		    		consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}else{
				consumeInputStreamQuietly(conn.getErrorStream());
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null){
				try { in.close(); } catch (IOException e) {}
			}
		}
	}
	
	@Override
	public void process(WorkloadBean target) throws Exception {
		
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();
        if(!target.getRequestUrl().contains("&from=wangpu")) {
        	target.setRequestUrl(target.getRequestUrl()+"&from=wangpu");
        }
        page = 1;
        maxPage = 1;
        count = 0;
        
        String rootLink = target.getRequestUrl();
        do{
        	String requestUrl = rootLink + (rootLink.contains("/?") ?"&":"?") + "page="+page;
	        String html = httpRequest(requestUrl, BaseConfig.CONF_URL_CRAWLER_CHARSET, true);
	                
	        if(html == null || html.trim().length() == 0) {
	        	client.completePage(targetMerchantId, target.getUrl(), STATUS.E);
	        	return;
	        }
	        
	        html = processHTMLBeforeParse(html);
	        
	        HTMLParser parse = new HTMLParser();
	        parse.source = new StringBuffer(html);
	               
	        // find all the links
	        while (!parse.eof()) {
	            char ch = parse.get();
	            if (ch == 0) {
	                HTMLTag tag = parse.getTag();
	                Attribute link = tag.get("HREF");
	                if (link == null)
	                    link = tag.get("SRC");
	
	                if (link == null)
	                    continue;         
	                
	                URL u = null;     
	                try {
	                    if (link.getValue().startsWith("?")) {
	                        u = URLUtility.addNewQuery(new URL(targetUrl), link.getValue());
	                    } else {
	                        u = new URL(new URL(targetUrl), link.getValue());
	                    }
	                } catch (MalformedURLException e) {
	                    //log.log(BotLog.LOG_LEVEL_TRACE, "Spider found other link: " + link);
	                    continue;
	                }
	
	               // target = URLUtility.stripQuery(target);
	                u = URLUtility.stripAnhcor(u);
	                
	                //change www.ha.com/../index.html -> www.ha.com/index.html
	                u = URLUtility.formatToValidURL(u);          	                              
	                               
	                
	                
	                if (u.getHost().equalsIgnoreCase( new URL(targetUrl).getHost())) {
	                    //log.log(BotLog.LOG_LEVEL_TRACE, "Spider found internal link: " + u.toString()); 
	                	
	                	String url = processURLAfterParse(u.toString());
	                	
	                	client.foundInternalLink(targetMerchantId, url, targetDepth + 1, targetCategoryId, targetKeyword);                    		                	
	                } else {
	                    //log.log(BotLog.LOG_LEVEL_TRACE, "Spider found external link: " + u.toString());
	                }
	            }
	        }
	        page++;
	        if(BaseConfig.CONF_WCE_SLEEP_TIME > 0) {
	        	try {
	        		Thread.sleep(BaseConfig.CONF_WCE_SLEEP_TIME);
	        	}catch(Exception e) {
	        		logger.error(e);
	        	}
	        }
	        
	        logger.info(count + " FROM URL : " + requestUrl);
        }while(page<=maxPage);
        
        client.completePage(targetMerchantId, target.getUrl(), STATUS.C);	
			
	}
}
