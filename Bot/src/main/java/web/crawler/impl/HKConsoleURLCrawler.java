package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;

public class HKConsoleURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnUrl = html ;
		Thread.sleep(1000);
		if(StringUtils.isNotBlank(html)){
			rtnUrl = rtnUrl.replace("javascript:callfrm('", "http://www.hkconsole.com/");
			rtnUrl = rtnUrl.replace("');\"", "\"");
		}
		
		return rtnUrl;
	}
	
}
