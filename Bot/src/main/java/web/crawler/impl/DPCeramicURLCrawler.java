package web.crawler.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.crawler.bot.HeatonURLCrawler;

public class DPCeramicURLCrawler extends HeatonURLCrawler {
	
	
	public String processHTMLBeforeParse(String html) throws Exception {
		String re = "";
		Document doc = Jsoup.parse(html);
		Elements e = doc.select("ul.products");
		 e = doc.select("a.woocommerce-loop-product__link");
		 for(Element ex:e) {
			 String genString = "<a href=\""+ex.attr("href")+"?isRealProduct=true\"></a>";
			 re += genString;
		 }
		 
		String page = "";
		Elements p = doc.select("nav.woocommerce-pagination");
		page = p.html(); 
		
		return re+page;
	}
	
	
//	public static void main(String[] args) {
//		DPCeramicURLCrawler x = new DPCeramicURLCrawler();
//		x.run("https://www.dpceramic.com/bathroom-sanitary-ware/faucet-main/outdoor-faucet/");
//	}
}
