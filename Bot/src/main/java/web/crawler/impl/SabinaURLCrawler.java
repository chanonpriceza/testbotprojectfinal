package web.crawler.impl;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;


public class SabinaURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rootLink = "";
		rootLink = FilterUtil.getStringBetween(html, "<div id=\"left_content\">", "</a>"); 
		rootLink = FilterUtil.getStringBetween(rootLink, "href=\"", "\""); 
		rootLink = "http://www.sabina.co.th"+rootLink;
		String pageList = FilterUtil.getStringBetween(html, "<div class=\"brand_page\">", "</table>"); 
		List<String> pageLink = FilterUtil.getAllStringBetween(pageList, ")\">", "</a>");
		String allLink = "";
		if(pageLink != null && pageLink.size() > 0){
			for(String link : pageLink){
				if(StringUtils.isNumeric(link)){
					String newlink = "<a href=\""+rootLink+"&page_="+link+"\">";
					allLink += newlink;
				}
			}
		}
		return html+allLink;
	}
}

