package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class LanlaBuyURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"woocommerce columns-3 \">", "<h2 class=\"elementor-heading-title elementor-size-default\">");
		return html;
	}
}
