package web.crawler.impl;

import web.crawler.bot.HeatonURLCrawler;
import utils.BotUtil;

public class magicgoldURLCrawler extends HeatonURLCrawler {

	 @Override
	public String processURLAfterParse(String url) throws Exception {
		
		 url = BotUtil.decodeURL(url); 
		return url;
	}
	 
}
