package web.crawler.impl;
import java.net.URLDecoder;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;

public class YiipoonURLCrawler extends HeatonURLCrawler {
	
	@Override
	public String processURLAfterParse(String url) throws Exception {
		
		if (StringUtils.isNotBlank(url)&& !url.contains("www.yiipoon.com")) {
			url = "http://www.yiipoon.com/"+ url;
			return url = URLDecoder.decode(url, "UTF-8");
		}else{
			return url = URLDecoder.decode(url, "UTF-8");
		}
	}
}

