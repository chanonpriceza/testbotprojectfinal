package web.crawler.impl;

import java.util.List;

import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class SchollshoesthailandURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		html = FilterUtil.getStringBetween(html, "<div class=\"column main\">", "<div class=\"sidebar sidebar-main\">");
		List<String> productLinkArea = FilterUtil.getAllStringBetween(html, "<li class=\"item product product-item\">", "<span class=\"product-image-container\"");
		if(productLinkArea != null && productLinkArea.size() > 0){
			String rtnHtml = html;
			for (String productLinkCode : productLinkArea) {
				String productLink = FilterUtil.getStringBetween(productLinkCode, "<a href=\"", "\"");
				rtnHtml = rtnHtml.replace(productLink, productLink+"?isProduct=true");
			}
			return rtnHtml;
		}
		return html;
	}
}
