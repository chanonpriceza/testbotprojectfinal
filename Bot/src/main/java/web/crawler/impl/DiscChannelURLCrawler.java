package web.crawler.impl;

import java.net.URLDecoder;

import web.crawler.bot.HeatonURLCrawler;

public class DiscChannelURLCrawler extends HeatonURLCrawler {

	@Override
	public String processURLAfterParse(String url) throws Exception {
		return url = URLDecoder.decode(url, "UTF-8");
	}	
		
}