package web.crawler.impl;

import java.util.List;

import manager.UrlPatternManager;
import utils.FilterUtil;
import web.crawler.bot.HeatonURLCrawler;

public class TemplateWeLoveShoppingURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {

		String rtnHtml = html;
		
		String productLi = FilterUtil.getStringBetween(rtnHtml, "<!-- Begin Category -->", "<!-- End Category -->");
		
		if(productLi == null || productLi.length() == 0){
			productLi = FilterUtil.getStringBetween(rtnHtml, "<!-- Begin greatmsg -->", "<!--End greatmsg Text -->");
		}
		
		if(productLi != null && productLi.length() > 0){
				List<String> productLink = FilterUtil.getAllStringBetween(productLi, "href=\"", "\"");
				if(productLink != null && productLink.size() > 0){
					for (String link : productLink) {
						if(!link.contains("ps")){
							if(link.contains("?")){
								rtnHtml = rtnHtml.replace(link, link+"&ps=1");
							}else{
								rtnHtml = rtnHtml.replace(link, link+"?ps=1");
							}
						}
					}
				}
		}else {

				String startUrl = UrlPatternManager.getInstance().getFirstStartUrl();
				if(startUrl.contains("store.weloveshopping.com")) {
					rtnHtml = html
							.replace("http://portal.weloveshopping.com/product/","http://store.weloveshopping.com/product/?swapDomain=true")
							.replace("https://portal.weloveshopping.com/product/","https://store.weloveshopping.com/product/?swapDomain=true")
							.replace("http://","https://");
				}else {
					rtnHtml = html;
				}
				
		}
		return rtnHtml;
		
	}

	
	@Override
	public String processURLAfterParse(String url) throws Exception {

		String rtnHtml = url;
		if(url.contains("swapDomain=true")){
			 rtnHtml = url
					 .replace("?swapDomain=true","")
					 .replace("http://store.weloveshopping.com/product/","http://portal.weloveshopping.com/product/")
					 .replace("https://store.weloveshopping.com/product/","https://portal.weloveshopping.com/product/")
					 .replace("http://","https://");
		}

		return rtnHtml;	
	}	
	
	
}
