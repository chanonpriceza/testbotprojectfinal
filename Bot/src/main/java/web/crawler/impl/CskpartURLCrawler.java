package web.crawler.impl;

import org.apache.commons.lang3.StringUtils;

import web.crawler.bot.HeatonURLCrawler;
import utils.FilterUtil;

public class CskpartURLCrawler extends HeatonURLCrawler {

	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String rtnHtml = FilterUtil.getStringBetween(html, "<div class=\"span-16\">", "<div class=\"span-8 last\">");
		if(StringUtils.isBlank(rtnHtml)) {
			return html;
		}
		return rtnHtml;		
	}
	
}
