package web.crawler.impl;

import utils.BotUtil;
import utils.FilterUtil;

public class HOMENET48MakWebURLCrawler extends MakeWebEasyProxyURLCrawler {
	@Override
	public String processHTMLBeforeParse(String html) throws Exception {
		String cutHTML = FilterUtil.getStringBetween(html," <div class=\"divPagination\">","Last");
		String link = FilterUtil.getStringBetween(cutHTML,"</ul>","class=\"textShow\"");
		link = FilterUtil.getStringBetween(link,"href=\"","\"");
		link = FilterUtil.getStringBefore(link,"/&p=", link);
		String cutMaxPage = FilterUtil.getStringBetween(cutHTML,"/&p=","\"");
		int maxPage = BotUtil.stringToInt(cutMaxPage,0);
		for(int i = 1;i<=maxPage;i++) {
			String genLink = "<a href=\""+link+"/&p="+i+"\">";
			html+= genLink;
		}
		return html;
	}
}
