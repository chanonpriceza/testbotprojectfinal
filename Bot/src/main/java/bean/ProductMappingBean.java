package bean;

public class ProductMappingBean {
	private int id;
	private int merchantId;
	private int botId;
	private int prodId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getBotId() {
		return botId;
	}
	public void setBotId(int botId) {
		this.botId = botId;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
}
