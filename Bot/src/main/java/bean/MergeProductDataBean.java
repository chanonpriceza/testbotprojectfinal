package bean;

public class MergeProductDataBean {

	private String[] productName; 
	private String[] productPrice;
	private String[] productUrl;
	private String[] productDescription;
	private String[] productPictureUrl;
	private String[] merchantUpdateDate; 
    private String[] productNameDescription;
    private String[] realProductId;
    private String currentUrl;
    private String[] productBasePrice;
    private String[] productUpc;
    private String[] data;
    
	public String[] getProductName() {
		return productName;
	}
	public void setProductName(String[] productName) {
		this.productName = productName;
	}
	public String[] getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String[] productPrice) {
		this.productPrice = productPrice;
	}
	public String[] getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String[] productUrl) {
		this.productUrl = productUrl;
	}
	public String[] getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String[] productDescription) {
		this.productDescription = productDescription;
	}
	public String[] getProductPictureUrl() {
		return productPictureUrl;
	}
	public void setProductPictureUrl(String[] productPictureUrl) {
		this.productPictureUrl = productPictureUrl;
	}
	public String[] getMerchantUpdateDate() {
		return merchantUpdateDate;
	}
	public void setMerchantUpdateDate(String[] merchantUpdateDate) {
		this.merchantUpdateDate = merchantUpdateDate;
	}
	public String[] getProductNameDescription() {
		return productNameDescription;
	}
	public void setProductNameDescription(String[] productNameDescription) {
		this.productNameDescription = productNameDescription;
	}
	public String[] getRealProductId() {
		return realProductId;
	}
	public void setRealProductId(String[] realProductId) {
		this.realProductId = realProductId;
	}
	public String getCurrentUrl() {
		return currentUrl;
	}
	public void setCurrentUrl(String currentUrl) {
		this.currentUrl = currentUrl;
	}
	public String[] getProductBasePrice() {
		return productBasePrice;
	}
	public void setProductBasePrice(String[] productBasePrice) {
		this.productBasePrice = productBasePrice;
	}
	public String[] getProductUpc() {
		return productUpc;
	}
	public void setProductUpc(String[] productUpc) {
		this.productUpc = productUpc;
	}
	public String[] getData() {
		return data;
	}
	public void setData(String[] data) {
		this.data = data;
	}
	
}
