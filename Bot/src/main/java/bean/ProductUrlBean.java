package bean;

public class ProductUrlBean {

	public enum STATUS {
		W, R ,C ,E
		//Waiting, Running, Complete, Error
	}
	public enum STATE {
		N, S ,F, D ,L,X
		//New, Parse Success , Parse Fail, Duplicate,Language Error,Expire
	}
	
	private int merchantId;
	private String url;
	private String state;
	private String status;
	private int categoryId;	
	private String keyword;
	
	public static ProductUrlBean createNewProductUrlBean(int merchantId, String url, int categoryId, String keyword) {
		ProductUrlBean rtn = new ProductUrlBean();
		rtn.setMerchantId(merchantId);
		rtn.setUrl(url);
		rtn.setCategoryId(categoryId);
		rtn.setKeyword(keyword);
		rtn.setState(STATE.N.toString());
		rtn.setStatus(STATUS.W.toString());
		return rtn;
	}
	
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}    
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}