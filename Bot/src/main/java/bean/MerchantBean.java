package bean;

import java.util.Date;

public class MerchantBean {
	
	public enum STATE{
		CRAWLER_START, CRAWLER_RUN, CRAWLER_FINISH, PARSER_START, PARSER_RUN, PARSER_FINISH,
		DATA_UPDATE_START, DATA_UPDATE_RUN, DATA_UPDATE_FINISH,
		INITIAL, SET_PRODUCT, BOT_FINISH,
		FEED_START, FEED_LOAD, FEED_RUN, FEED_FIND_INACTIVE, FEED_FINISH
	}
	
	public enum STATUS {
		WAITING, RUNNING, FINISH, CONTINUE
	}
	
    private int id;
    private String name;
    private String state;
    private int active;
    private String status;
    private String command;

    private String dataUpdateState;
    private String dataUpdateStatus;
    private int packageType;
    private int dueErrorCount;
    private int wceErrorCount;
    private String server;
    private String dataServer;
	private int ownerId;
	private String ownerName;
	private String coreUserConfig;
    
	private int nodeTest;
    private int shardNo;
    private int shardTotal;
    
    private Date nextStart;
    private Date wceNextStart;
    private Date dueNextStart;
    
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDataUpdateState() {
		return dataUpdateState;
	}
	public void setDataUpdateState(String dataUpdateState) {
		this.dataUpdateState = dataUpdateState;
	}
	public String getDataUpdateStatus() {
		return dataUpdateStatus;
	}
	public void setDataUpdateStatus(String dataUpdateStatus) {
		this.dataUpdateStatus = dataUpdateStatus;
	}
	public int getPackageType() {
		return packageType;
	}
	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}
	public int getDueErrorCount() {
		return dueErrorCount;
	}
	public void setDueErrorCount(int dueErrorCount) {
		this.dueErrorCount = dueErrorCount;
	}
	public int getWceErrorCount() {
		return wceErrorCount;
	}
	public void setWceErrorCount(int wceErrorCount) {
		this.wceErrorCount = wceErrorCount;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getDataServer() {
		return dataServer;
	}
	public void setDataServer(String server) {
		this.dataServer = server;
	}
	public int getNodeTest() {
		return nodeTest;
	}
	public void setNodeTest(int nodeTest) {
		this.nodeTest = nodeTest;
	}
	public int getShardNo() {
		return shardNo;
	}
	public void setShardNo(int shardNo) {
		this.shardNo = shardNo;
	}
	public int getShardTotal() {
		return shardTotal;
	}
	public void setShardTotal(int shardTotal) {
		this.shardTotal = shardTotal;
	}
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getCoreUserConfig() {
		return coreUserConfig;
	}
	public void setCoreUserConfig(String coreUserConfig) {
		this.coreUserConfig = coreUserConfig;
	}
	public Date getNextStart() {
		return nextStart;
	}
	public void setNextStart(Date nextStart) {
		this.nextStart = nextStart;
	}
	public Date getWCENextStart() {
		return wceNextStart;
	}
	public void setWCENextStart(Date wceNextStart) {
		this.wceNextStart = wceNextStart;
	}
	public Date getDUENextStart() {
		return dueNextStart;
	}
	public void setDUENextStart(Date dueNextStart) {
		this.dueNextStart = dueNextStart;
	}
	
}
