package bean;

import javax.sql.DataSource;

import db.ProductDataDB;
import db.SendDataDB;

public class DataObjectBean {
	private DataSource dataSource;
	private ProductDataDB productDataDB;
	private SendDataDB sendDataDB;
	
	
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	public ProductDataDB getProductDataDB() {
		return productDataDB;
	}
	public void setProductDataDB(ProductDataDB productDataDB) {
		this.productDataDB = productDataDB;
	}
	public SendDataDB getSendDataDB() {
		return sendDataDB;
	}
	public void setSendDataDB(SendDataDB sendDataDB) {
		this.sendDataDB = sendDataDB;
	}
	
	
	
}
