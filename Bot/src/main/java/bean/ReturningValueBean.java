package bean;

import java.util.List;

public class ReturningValueBean {
	
	private int intValue;
	private String stringValue;
	private List<String> listStringValue;
	
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public List<String> getListStringValue() {
		return listStringValue;
	}
	public void setListStringValue(List<String> listStringValue) {
		this.listStringValue = listStringValue;
	}
	
	

}
