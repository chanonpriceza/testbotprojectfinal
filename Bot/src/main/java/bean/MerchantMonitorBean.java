package bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;

import utils.BotUtil;

public class MerchantMonitorBean {
	
	public MerchantMonitorBean(JSONObject obj) {
		this.type = String.valueOf(obj.get("type"));
		this.merchantId = BotUtil.stringToInt(String.valueOf(obj.get("merchantId")), 0);
		this.analyzeDate = convertTimeStamp(String.valueOf(obj.get("analyzeDate")));
		this.analyzeResult = String.valueOf(obj.get("analyzeResult"));
		this.analyzeDetail = String.valueOf(obj.get("analyzeDetail"));
		this.topic = this.analyzeResult;
		this.detail = this.analyzeDetail;
		this.recentFoundDate = analyzeDate;
		this.count = 1;
	}
	
	public MerchantMonitorBean() {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int id;
	private String type;
	private String topic;
	private String detail;
	private int merchantId;
	private int count;
	private Timestamp firstFoundDate;
	private Timestamp recentFoundDate;
	
	
	private Timestamp analyzeDate;
	private String analyzeResult;
	private String analyzeDetail;
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Timestamp getFirstFoundDate() {
		return firstFoundDate;
	}
	public void setFirstFoundDate(Timestamp firstFoundDate) {
		this.firstFoundDate = firstFoundDate;
	}
	public Timestamp getRecentFoundDate() {
		return recentFoundDate;
	}
	public void setRecentFoundDate(Timestamp recentFoundDate) {
		this.recentFoundDate = recentFoundDate;
	}

	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public Timestamp getAnalyzeDate() {
		return analyzeDate;
	}
	public void setAnalyzeDate(Timestamp analyzeDate) {
		this.analyzeDate = analyzeDate;
	}
	public String getAnalyzeResult() {
		return analyzeResult;
	}
	public void setAnalyzeResult(String analyzeResult) {
		this.analyzeResult = analyzeResult;
	}
	public String getAnalyzeDetail() {
		return analyzeDetail;
	}
	public void setAnalyzeDetail(String analyzeDetail) {
		this.analyzeDetail = analyzeDetail;
	}
	
	public Timestamp convertTimeStamp(String timeStamp) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date parsedDate = dateFormat.parse(timeStamp);
			return new Timestamp(parsedDate.getTime());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
