package bean;

public class ParserResultBean {
	private ProductDataBean pdb;
	private byte[] imageByte;
	private boolean isUpdate;
	private boolean isUpdatePic;
	private boolean isDuplicate;
	private boolean isNotAllowLanguage;
	private boolean isExpire;
	private boolean isExist;
	
	public ProductDataBean getPdb() {
		return pdb;
	}
	public void setPdb(ProductDataBean pdb) {
		this.pdb = pdb;
	}
	public byte[] getImageByte() {
		return imageByte;
	}
	public void setImageByte(byte[] imageByte) {
		this.imageByte = imageByte;
	}
	public boolean isUpdate() {
		return isUpdate;
	}
	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}
	public boolean isUpdatePic() {
		return isUpdatePic;
	}
	public void setUpdatePic(boolean isUpdatePic) {
		this.isUpdatePic = isUpdatePic;
	}
	public boolean isDuplicate() {
		return isDuplicate;
	}
	public void setDuplicate(boolean isDuplicate) {
		this.isDuplicate = isDuplicate;
	}
	public boolean isNotAllowLanguage() {
		return isNotAllowLanguage;
	}
	public void setNotAllowLanguage(boolean isNotAllowLanguage) {
		this.isNotAllowLanguage = isNotAllowLanguage;
	}
	public boolean isExpire() {
		return isExpire;
	}
	public void setExpire(boolean isExpire) {
		this.isExpire = isExpire;
	}
	public boolean isExist() {
		return isExist;
	}
	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}
	
	
}
