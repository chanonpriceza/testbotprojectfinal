package bean;

import java.util.Date;

public class ReportBean {
	
	public enum STATUS {
		RUNNING, FAILED, WARNNING, PASSED, KILLED, TIMEOUT, EXCEED
	}
	
	// STANDARD REPORT
	
	private int id;
	private String type;
	private String name;
	private int merchantId;
	private int packageType;
	
	private int countOld;
	private int countNew;
	private int successCount;
	private int errorCount;
	
	private int add;
	private int update;
	private int updatePic;
	private int duplicate;
	private int delete;
	
	private int notAllow;
	private int expire;
	
	private String status;
	private String msgData;
	
	private Date startDate;
	private Date endDate;
	private Date defineDate;
	
	private int serverRef;

	// FOR MONITORING 

	private boolean isWeb;
	private boolean isFeed;
	private boolean isExceed;
	private int wceWorkloadCount;
	private int wceWorkloadCountSuccess;
	private int wceWorkloadCountFailed;
	private int wceProductUrlCount;
	private int wceProductUrlCountSuccess;
	private int wceProductUrlCountFailed;
	
	// ##################################################################
	// ############### GETTER / SETTER ##################################
	// ##################################################################
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public int getPackageType() {
		return packageType;
	}

	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}

	public int getCountOld() {
		return countOld;
	}

	public void setCountOld(int countOld) {
		this.countOld = countOld;
	}

	public int getCountNew() {
		return countNew;
	}

	public void setCountNew(int countNew) {
		this.countNew = countNew;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public int getAdd() {
		return add;
	}

	public void setAdd(int add) {
		this.add = add;
	}

	public int getUpdate() {
		return update;
	}

	public void setUpdate(int update) {
		this.update = update;
	}

	public int getUpdatePic() {
		return updatePic;
	}

	public void setUpdatePic(int updatePic) {
		this.updatePic = updatePic;
	}

	public int getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(int duplicate) {
		this.duplicate = duplicate;
	}

	public int getDelete() {
		return delete;
	}

	public void setDelete(int delete) {
		this.delete = delete;
	}

	public int getNotAllow() {
		return notAllow;
	}

	public void setNotAllow(int notAllow) {
		this.notAllow = notAllow;
	}

	public int getExpire() {
		return expire;
	}

	public void setExpire(int expire) {
		this.expire = expire;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsgData() {
		return msgData;
	}

	public void setMsgData(String msgData) {
		this.msgData = msgData;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDefineDate() {
		return defineDate;
	}

	public void setDefineDate(Date defineDate) {
		this.defineDate = defineDate;
	}

	public int getServerRef() {
		return serverRef;
	}

	public void setServerRef(int serverRef) {
		this.serverRef = serverRef;
	}

	public boolean isWeb() {
		return isWeb;
	}

	public void setWeb(boolean isWeb) {
		this.isWeb = isWeb;
	}

	public boolean isFeed() {
		return isFeed;
	}

	public void setFeed(boolean isFeed) {
		this.isFeed = isFeed;
	}

	public boolean isExceed() {
		return isExceed;
	}

	public void setExceed(boolean isExceed) {
		this.isExceed = isExceed;
	}
	
	public int getWceWorkloadCount() {
		return wceWorkloadCount;
	}

	public void setWceWorkloadCount(int wceWorkloadCount) {
		this.wceWorkloadCount = wceWorkloadCount;
	}

	public int getWceWorkloadCountSuccess() {
		return wceWorkloadCountSuccess;
	}

	public void setWceWorkloadCountSuccess(int wceWorkloadCountSuccess) {
		this.wceWorkloadCountSuccess = wceWorkloadCountSuccess;
	}

	public int getWceWorkloadCountFailed() {
		return wceWorkloadCountFailed;
	}

	public void setWceWorkloadCountFailed(int wceWorkloadCountFailed) {
		this.wceWorkloadCountFailed = wceWorkloadCountFailed;
	}

	public int getWceProductUrlCount() {
		return wceProductUrlCount;
	}

	public void setWceProductUrlCount(int wceProductUrlCount) {
		this.wceProductUrlCount = wceProductUrlCount;
	}

	public int getWceProductUrlCountSuccess() {
		return wceProductUrlCountSuccess;
	}

	public void setWceProductUrlCountSuccess(int wceProductUrlCountSuccess) {
		this.wceProductUrlCountSuccess = wceProductUrlCountSuccess;
	}

	public int getWceProductUrlCountFailed() {
		return wceProductUrlCountFailed;
	}

	public void setWceProductUrlCountFailed(int wceProductUrlCountFailed) {
		this.wceProductUrlCountFailed = wceProductUrlCountFailed;
	}

	// ##################################################################
	// ############### SYNCHRONIZED METHOD ##############################
	// ##################################################################
	
	public synchronized void increaseSuccessCount() {
		this.successCount++;
	}
	
	public synchronized void increaseAdd() {
		this.add++;
	}
	
	public synchronized void increaseUpdate() {
		this.update++;
	}
	
	public synchronized void increaseUpdatePic() {
		this.updatePic++;
	}
	
	public synchronized void increaseDelete() {
		this.delete++;
	}
	
	public synchronized void increaseDuplicate() {
		this.duplicate++;
	}
	
	public synchronized void increaseNotAllow() {
		this.notAllow++;
	}
	
	public synchronized void increaseExpire() {
		this.expire++;
	}
	
	public synchronized void increaseErrorCount() {
		this.errorCount++;
	}
	
	public synchronized void increaseWceWorkloadCount() {
		this.wceWorkloadCount++;
	}
	
	public synchronized void increaseWceWorkloadCountSuccess() {
		this.wceProductUrlCountSuccess++;
	}
	
	public synchronized void increaseWceWorkloadCountFailed() {
		this.wceProductUrlCountFailed++;
	}
	
	public synchronized void increaseWceProductUrlCount() {
		this.wceWorkloadCount++;
	}
	
	public synchronized void increaseWceProductUrlCountSuccess() {
		this.wceWorkloadCountSuccess++;
	}
	
	public synchronized void increaseWceProductUrlCountFailed() {
		this.wceWorkloadCountFailed++;
	}
}