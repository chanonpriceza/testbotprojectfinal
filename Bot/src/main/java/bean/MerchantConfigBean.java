package bean;

public class MerchantConfigBean {
	
	public enum FIELD {		
		maxDepth, parserClass, initCrawlerClass, urlCrawlerClass, feedCrawlerClass, 
		productUpdateLimit, urlCrawlerCharset, wceParserThreadNum, 
		dueParserThreadNum, enableCookies, ignoreHttpError, skipEncodeURL, forceDelete,
		enableUpdatePic, dueSleepTime, wceSleepTime, userOldProductUrl, imageParserClass,
		enableLargeImage, forceUpdateImage, enableUpdateDesc, enableSmallImage,
		updatePricePercent, enableVeryLargeImage, deleteLimit, forceUpdateKeyword, forceUpdateCategory, forceUpdateLimit,
		disableParserCookies, disableDUEContinue, parserCharset, enableUpdateUrl, feedCrawlerParam,
		wceParserClass, dueParserClass, enableUpdateBasePrice, wceClearProductUrl, maxRuntime, hourRate, 
		feedType, feedCheckGz, feedUrl, feedUsername, feedPassword, merchantName, currencyRate, enableProxy,
		feedXmlMegaTag, feedCSVFormat, parserRealtimeClass, parserDetailClass, disableProcessLog ,deleteNotMapCategory,wceHourRate,dueHourRate,fixCategoryId,fixKeyword
	}
	
	private int id;
	private int merchantId;
	private String field;
	private String value;
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}