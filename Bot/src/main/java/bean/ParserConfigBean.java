package bean;

public class ParserConfigBean {
	
	public enum FIELD {
		name, price, description, pictureUrl, expire, realProductId, upc, basePrice, concatId, concatWord,
	}
	
	public enum TYPE {
		inTag, between, before, after, plainText, removeNotPrice, unescape, replaceSpace, replace, 
		expireEqual, expireContain, concatRealProductId, betweenUrl,
		encodeUrl, contactPrice, nonPrice, auctionPrice, specialPrice, textStep,
		ifEqual, ifNotEqual, ifContain, ifNotContain,
		getproduct, xmltag, xmltaglist, jsonobject, jsonobjectkey, jsonobjectlist, jsonobjectlistkey, csvcolumn,
		addTextBefore, addTextAfter, goToStep, stop,
		JSoupSelectorNum, JSoupSelectorTxt, JSoupSelectorLongTxt, JSoupSelectorAttr
	}
	
	private int id;
	private int merchantId;
	private String fieldType;
	private String filterType;
	private String value;
	private int depth;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getFilterType() {
		return filterType;
	}
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	
	
}
