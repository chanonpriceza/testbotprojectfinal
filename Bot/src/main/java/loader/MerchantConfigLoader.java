package loader;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import bean.ConfigBean;
import bean.MerchantConfigBean;
import bean.MerchantConfigBean.FIELD;
import bean.ParserConfigBean;
import db.ConfigDB;
import db.MerchantConfigDB;
import db.ParserConfigDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import feed.TypeFeedCrawlerInterface;
import manager.CommandManager;
import web.parser.image.DefaultImageParser;

import static org.apache.commons.lang3.StringUtils.*;
import static utils.BotUtil.*;

public class MerchantConfigLoader {
	
	public static void loadMerchantConfig() throws SQLException {
		MerchantConfigDB mDB = DatabaseManager.getInstance().getMerchantConfigDB();
		List<MerchantConfigBean> list = mDB.findMerchantConfig(BaseConfig.MERCHANT_ID);
		
		ParserConfigDB pDB = DatabaseManager.getInstance().getParserConfigDB();
		List<ParserConfigBean> parserConfig = pDB.findParserConfig(BaseConfig.MERCHANT_ID);
		
		ConfigDB cDB = DatabaseManager.getInstance().getConfigDB();
		ConfigBean cBean = cDB.getByName("merchant.ignore.name.change");
		if(cBean != null) {
			String[] merchantList = cBean.getConfigValue().split("\\|");
			if(merchantList != null && merchantList.length > 0 && Arrays.asList(merchantList).contains(String.valueOf(BaseConfig.MERCHANT_ID)))
				BaseConfig.CONF_IGNORE_NAME_CHANGE = true;
		}
		
		if(list.isEmpty())
			return;
		
		Properties prop = new Properties();
		for (MerchantConfigBean b : list) {
			if(isNotBlank(b.getField()) && isNotBlank(b.getValue()) )
				prop.setProperty(b.getField(), b.getValue());
		}

		BaseConfig.CONF_BOT_QUALITY_HOUR_RATE = stringToInt(prop.getProperty(FIELD.hourRate.toString()), BaseConfig.CONF_BOT_QUALITY_HOUR_RATE);
		BaseConfig.CONF_BOT_WCE_HOUR_RATE = stringToInt(prop.getProperty(FIELD.wceHourRate.toString()), BaseConfig.CONF_BOT_WCE_HOUR_RATE);
		BaseConfig.CONF_BOT_DUE_HOUR_RATE = stringToInt(prop.getProperty(FIELD.dueHourRate.toString()), BaseConfig.CONF_BOT_DUE_HOUR_RATE);
		BaseConfig.CONF_MAX_DEPTH = stringToInt(prop.getProperty(FIELD.maxDepth.toString()), BaseConfig.CONF_MAX_DEPTH);
		BaseConfig.CONF_PARSER_CLASS = defaultString(prop.getProperty(FIELD.parserClass.toString()), BaseConfig.CONF_PARSER_CLASS);
		BaseConfig.CONF_INIT_CRAWLER_CLASS = defaultString(prop.getProperty(FIELD.initCrawlerClass.toString()), BaseConfig.CONF_INIT_CRAWLER_CLASS);
		BaseConfig.CONF_URL_CRAWLER_CLASS = defaultString(prop.getProperty(FIELD.urlCrawlerClass.toString()), BaseConfig.CONF_URL_CRAWLER_CLASS);
		BaseConfig.CONF_FEED_CRAWLER_CLASS = defaultString(prop.getProperty(FIELD.feedCrawlerClass.toString()), BaseConfig.CONF_FEED_CRAWLER_CLASS);
		
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = stringToInt(prop.getProperty(FIELD.productUpdateLimit.toString()), BaseConfig.CONF_PRODUCT_UPADTE_LIMIT);
		BaseConfig.CONF_FEED_CRAWLER_CLASS = defaultString(prop.getProperty(FIELD.feedCrawlerClass.toString()), BaseConfig.CONF_FEED_CRAWLER_CLASS);
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = stringToInt(prop.getProperty(FIELD.wceParserThreadNum.toString()), BaseConfig.CONF_WCE_PARSER_THREAD_NUM);
		
		BaseConfig.CONF_DUE_PARSER_THREAD_NUM = stringToInt(prop.getProperty(FIELD.dueParserThreadNum.toString()), BaseConfig.CONF_DUE_PARSER_THREAD_NUM);
		BaseConfig.CONF_ENABLE_COOKIES = defaultBoolean(prop.getProperty(FIELD.enableCookies.toString()), BaseConfig.CONF_ENABLE_COOKIES);
		BaseConfig.CONF_IGNORE_HTTP_ERROR = defaultBoolean(prop.getProperty(FIELD.ignoreHttpError.toString()), BaseConfig.CONF_IGNORE_HTTP_ERROR);
		BaseConfig.CONF_SKIP_ENCODE_URL = defaultBoolean(prop.getProperty(FIELD.skipEncodeURL.toString()), BaseConfig.CONF_SKIP_ENCODE_URL);
		BaseConfig.CONF_FORCE_DELETE = defaultBoolean(prop.getProperty(FIELD.forceDelete.toString()), BaseConfig.CONF_FORCE_DELETE);
		
		BaseConfig.CONF_ENABLE_UPDATE_PIC = defaultBoolean(prop.getProperty(FIELD.enableUpdatePic.toString()), BaseConfig.CONF_ENABLE_UPDATE_PIC);
		BaseConfig.CONF_DUE_SLEEP_TIME = stringToInt(prop.getProperty(FIELD.dueSleepTime.toString()), BaseConfig.CONF_DUE_SLEEP_TIME);
		BaseConfig.CONF_WCE_SLEEP_TIME = stringToInt(prop.getProperty(FIELD.wceSleepTime.toString()), BaseConfig.CONF_WCE_SLEEP_TIME);
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = defaultBoolean(prop.getProperty(FIELD.userOldProductUrl.toString()), BaseConfig.CONF_USER_OLD_PRODUCT_URL);
		BaseConfig.CONF_IMAGE_PARSER_CLASS = defaultString(prop.getProperty(FIELD.imageParserClass.toString()), BaseConfig.CONF_IMAGE_PARSER_CLASS);
		
		BaseConfig.CONF_ENABLE_LARGE_IMAGE = defaultBoolean(prop.getProperty(FIELD.enableLargeImage.toString()), BaseConfig.CONF_ENABLE_LARGE_IMAGE);
		BaseConfig.CONF_FORCE_UPDATE_IMAGE = defaultBoolean(prop.getProperty(FIELD.forceUpdateImage.toString()), BaseConfig.CONF_FORCE_UPDATE_IMAGE);
		BaseConfig.CONF_ENABLE_UPDATE_DESC = defaultBoolean(prop.getProperty(FIELD.enableUpdateDesc.toString()), BaseConfig.CONF_ENABLE_UPDATE_DESC);
		BaseConfig.CONF_ENABLE_SMALL_IMAGE = defaultBoolean(prop.getProperty(FIELD.enableSmallImage.toString()), BaseConfig.CONF_ENABLE_SMALL_IMAGE);
		
		
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = stringToInt(prop.getProperty(FIELD.updatePricePercent.toString()), BaseConfig.CONF_UPDATE_PRICE_PERCENT);
		BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE = defaultBoolean(prop.getProperty(FIELD.enableVeryLargeImage.toString()), BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE);		
		BaseConfig.CONF_DELETE_LIMIT = stringToInt(prop.getProperty(FIELD.deleteLimit.toString()), BaseConfig.CONF_DELETE_LIMIT);
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = defaultBoolean(prop.getProperty(FIELD.forceUpdateKeyword.toString()), BaseConfig.CONF_FORCE_UPDATE_KEYWORD);
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = defaultBoolean(prop.getProperty(FIELD.forceUpdateCategory.toString()), BaseConfig.CONF_FORCE_UPDATE_CATEGORY);
		BaseConfig.CONF_FORCE_UPDATE_LIMIT = defaultBoolean(prop.getProperty(FIELD.forceUpdateLimit.toString()), BaseConfig.CONF_FORCE_UPDATE_LIMIT);
		
		BaseConfig.CONF_DISBLE_PARSER_COOKIES = defaultBoolean(prop.getProperty(FIELD.disableParserCookies.toString()), BaseConfig.CONF_DISBLE_PARSER_COOKIES);
		BaseConfig.CONF_DISABLE_DUE_CONTINUE = defaultBoolean(prop.getProperty(FIELD.disableDUEContinue.toString()), BaseConfig.CONF_DISABLE_DUE_CONTINUE);
		BaseConfig.CONF_PARSER_CHARSET = defaultString(prop.getProperty(FIELD.parserCharset.toString()), BaseConfig.CONF_PARSER_CHARSET);
		BaseConfig.CONF_ENABLE_UPDATE_URL = defaultBoolean(prop.getProperty(FIELD.enableUpdateUrl.toString()), BaseConfig.CONF_ENABLE_UPDATE_URL);
		BaseConfig.CONF_FEED_CRAWLER_PARAM = defaultString(prop.getProperty(FIELD.feedCrawlerParam.toString()), BaseConfig.CONF_FEED_CRAWLER_PARAM);
		
		BaseConfig.CONF_WCE_PARSER_CLASS = defaultString(prop.getProperty(FIELD.wceParserClass.toString()), BaseConfig.CONF_WCE_PARSER_CLASS);
		BaseConfig.CONF_DUE_PARSER_CLASS = defaultString(prop.getProperty(FIELD.dueParserClass.toString()), BaseConfig.CONF_DUE_PARSER_CLASS);
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = defaultBoolean(prop.getProperty(FIELD.enableUpdateBasePrice.toString()), BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE);
		BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = defaultBoolean(prop.getProperty(FIELD.wceClearProductUrl.toString()), BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL);
		BaseConfig.CONF_MAX_RUNTIME = stringToInt(prop.getProperty(FIELD.maxRuntime.toString()), BaseConfig.CONF_MAX_RUNTIME);
		
		/*BaseConfig.CONF_FEED_TYPE = defaultString(prop.getProperty(FIELD.feedType.toString()), BaseConfig.CONF_FEED_TYPE);
		BaseConfig.CONF_FEED_CHECK_GZ = defaultBoolean(prop.getProperty(FIELD.feedCheckGz.toString()), BaseConfig.CONF_FEED_CHECK_GZ);
		BaseConfig.CONF_FEDD_URL = defaultString(prop.getProperty(FIELD.feedUrl.toString()), BaseConfig.CONF_FEDD_URL);*/
		BaseConfig.CONF_FEED_USERNAME = defaultString(prop.getProperty(FIELD.feedUsername.toString()), BaseConfig.CONF_FEED_USERNAME);
		BaseConfig.CONF_FEED_PASSWORD = defaultString(prop.getProperty(FIELD.feedPassword.toString()), BaseConfig.CONF_FEED_PASSWORD);
		BaseConfig.CONF_MERCHANT_NAME = defaultString(prop.getProperty(FIELD.merchantName.toString()), BaseConfig.CONF_MERCHANT_NAME);
		BaseConfig.CONF_CURRENCY_RATE = convertStringToDouble(prop.getProperty(FIELD.currencyRate.toString()), BaseConfig.CONF_CURRENCY_RATE);
		
		BaseConfig.CONF_ENABLE_PROXY = defaultBoolean(prop.getProperty(FIELD.enableProxy.toString()), BaseConfig.CONF_ENABLE_PROXY);
		
		BaseConfig.PARSER_CONFIG = parserConfig;
		
		BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = defaultString(prop.getProperty(FIELD.feedXmlMegaTag.toString()), BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG);
		if(isNotBlank(BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG)) {
			BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG = "</"+ BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG +">";
			BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = "<"+ BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG +">";
		}

		BaseConfig.CONF_FEED_CSV_FORMAT = defaultString(prop.getProperty(FIELD.feedCSVFormat.toString()), BaseConfig.CONF_FEED_CSV_FORMAT);
		
		try {
			BaseConfig.FEED_TYPE_CONF = TypeFeedCrawlerInterface.FEED_TYPE.valueOf(prop.getProperty(FIELD.feedType.toString()).toUpperCase());
		}catch(Exception e) {}
		
		BaseConfig.CONF_PARSER_REALTIME_CLASS = defaultString(prop.getProperty(FIELD.parserRealtimeClass.toString()), BaseConfig.CONF_PARSER_REALTIME_CLASS);
		BaseConfig.CONF_PARSER_DETAIL_CLASS = defaultString(prop.getProperty(FIELD.parserDetailClass.toString()), BaseConfig.CONF_PARSER_DETAIL_CLASS);
		
		BaseConfig.IS_DELETE_ALL = CommandManager.getInstance().checkCommand(CommandManager.DELETE_ALL);
		BaseConfig.IS_DELETE_NOT_MAP = CommandManager.getInstance().checkCommand(CommandManager.DELETE_NOT_MAP);
		if(BaseConfig.IS_DELETE_ALL || BaseConfig.IS_DELETE_NOT_MAP) 
			BaseConfig.CONF_DELETE_LIMIT = 50000;
		
		BaseConfig.CONF_DISABLE_LOG = defaultBoolean(prop.getProperty(FIELD.disableProcessLog.toString()),BaseConfig.CONF_DISABLE_LOG);
		BaseConfig.CONF_FIX_CATEGORY = defaultString(prop.getProperty(FIELD.fixCategoryId.toString()),BaseConfig.CONF_FIX_CATEGORY);
		BaseConfig.CONF_FIX_KEYWORD = defaultString(prop.getProperty(FIELD.fixKeyword.toString()),BaseConfig.CONF_FIX_KEYWORD);
	}
	
	public static void updateMerchantConfig() throws SQLException {
		MerchantConfigDB merchantConfig = DatabaseManager.getInstance().getMerchantConfigDB();
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			
			if(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL)
				merchantConfig.updateConfigValue(FIELD.wceClearProductUrl, "false", BaseConfig.MERCHANT_ID);
			
			if(BaseConfig.CONF_FORCE_UPDATE_KEYWORD && isNotBlank(String.valueOf(BaseConfig.FEED_TYPE_CONF)))
				merchantConfig.updateConfigValue(FIELD.forceUpdateKeyword, "false", BaseConfig.MERCHANT_ID);
			
			if(BaseConfig.CONF_FORCE_UPDATE_CATEGORY && isNotBlank(String.valueOf(BaseConfig.FEED_TYPE_CONF)))
				merchantConfig.updateConfigValue(FIELD.forceUpdateCategory, "false", BaseConfig.MERCHANT_ID);
			
		}else if(BaseConfig.RUNTIME_TYPE.equals("DUE")) {
			
			if(BaseConfig.CONF_FORCE_DELETE)
				merchantConfig.updateConfigValue(FIELD.forceDelete, "false", BaseConfig.MERCHANT_ID);
			
			if(BaseConfig.CONF_FORCE_UPDATE_IMAGE)
				merchantConfig.updateConfigValue(FIELD.forceUpdateImage, "false", BaseConfig.MERCHANT_ID);
			
			if(BaseConfig.CONF_FORCE_UPDATE_KEYWORD && 
					BaseConfig.CONF_FEED_CRAWLER_CLASS != null && isNotBlank(String.valueOf(BaseConfig.FEED_TYPE_CONF)))
				merchantConfig.updateConfigValue(FIELD.forceUpdateKeyword, "false", BaseConfig.MERCHANT_ID);
			
			if(BaseConfig.CONF_FORCE_UPDATE_CATEGORY && 
					BaseConfig.CONF_FEED_CRAWLER_CLASS != null && isNotBlank(String.valueOf(BaseConfig.FEED_TYPE_CONF)))
				merchantConfig.updateConfigValue(FIELD.forceUpdateCategory, "false", BaseConfig.MERCHANT_ID);
			
		}
		
	}
		
	public static void setDefault() {
		BaseConfig.BOT_FAMILY = null;
		BaseConfig.MERCHANT_ID = -1000000;
		BaseConfig.MERCHANT_NAME = null;
		BaseConfig.MERCHANT_ACTIVE = 0;
		BaseConfig.PACKAGE_TYPE = 0;
		BaseConfig.NEXT_START = null;
		BaseConfig.MERCHANT_CONF = null;
		BaseConfig.RUNTIME_TYPE = null;
		
		BaseConfig.CONF_BOT_QUALITY_HOUR_RATE = 24;
		BaseConfig.CONF_MAX_DEPTH = 5;
		BaseConfig.CONF_PARSER_CLASS = null;
		BaseConfig.CONF_INIT_CRAWLER_CLASS = null;
		BaseConfig.CONF_URL_CRAWLER_CLASS = null;
		BaseConfig.CONF_FEED_CRAWLER_CLASS = null;
		
		BaseConfig.CONF_MAX_RUNTIME = 24;
		BaseConfig.CONF_PRODUCT_UPADTE_LIMIT = 7;
		BaseConfig.CONF_URL_CRAWLER_CHARSET = null;
		BaseConfig.CONF_WCE_PARSER_THREAD_NUM = 1;
		
		BaseConfig.CONF_DUE_PARSER_THREAD_NUM = 1;
		BaseConfig.CONF_ENABLE_COOKIES = false;
		BaseConfig.CONF_IGNORE_HTTP_ERROR = false;
		BaseConfig.CONF_SKIP_ENCODE_URL = false;
		BaseConfig.CONF_FORCE_DELETE = false;
		
		BaseConfig.CONF_ENABLE_UPDATE_PIC = false;
		BaseConfig.CONF_DUE_SLEEP_TIME = 0;
		BaseConfig.CONF_WCE_SLEEP_TIME = 0;
		BaseConfig.CONF_USER_OLD_PRODUCT_URL = false;
		BaseConfig.CONF_IMAGE_PARSER_CLASS = null;
		
		BaseConfig.CONF_ENABLE_LARGE_IMAGE = true;
		BaseConfig.CONF_FORCE_UPDATE_IMAGE = false;
		BaseConfig.CONF_ENABLE_UPDATE_DESC = false;
		BaseConfig.CONF_ENABLE_SMALL_IMAGE = false;
		
		BaseConfig.CONF_UPDATE_PRICE_PERCENT = 0;
		BaseConfig.CONF_ENABLE_VERY_LARGE_IMAGE = false;
		BaseConfig.CONF_DELETE_LIMIT = 200;
		BaseConfig.CONF_FORCE_UPDATE_KEYWORD = false;
		BaseConfig.CONF_FORCE_UPDATE_CATEGORY = false;
		BaseConfig.CONF_FORCE_UPDATE_LIMIT = false;
		
		
		BaseConfig.CONF_DISBLE_PARSER_COOKIES = false;
		BaseConfig.CONF_DISABLE_DUE_CONTINUE = false;
		BaseConfig.CONF_PARSER_CHARSET = "UTF-8";
		BaseConfig.CONF_ENABLE_UPDATE_URL = false;
		BaseConfig.CONF_FEED_CRAWLER_PARAM = null;
		
		
		BaseConfig.CONF_WCE_PARSER_CLASS = null;
		BaseConfig.CONF_DUE_PARSER_CLASS = null;
		BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE = false;
		BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL = false;
		
		BaseConfig.CONF_FEED_USERNAME = null;
		BaseConfig.CONF_FEED_PASSWORD = null;
		BaseConfig.CONF_MERCHANT_NAME = null;
		BaseConfig.CONF_CURRENCY_RATE = 1.0;
		BaseConfig.CONF_ENABLE_PROXY = false;
		
		BaseConfig.CONF_IGNORE_NAME_CHANGE = false;
		
		BaseConfig.CONF_FEED_XML_OPEN_MEGA_TAG = null;
		BaseConfig.CONF_FEED_XML_CLOSE_MEGA_TAG = null;
		BaseConfig.CONF_FEED_CSV_FORMAT = null;
		BaseConfig.IMAGE_WIDTH = DefaultImageParser.DEFAULT_LARGE_WIDTH;
		BaseConfig.IMAGE_HEIGHT = DefaultImageParser.DEFAULT_LARGE_HEIGHT;
		BaseConfig.IMAGE_PARSER = new DefaultImageParser();
		BaseConfig.THREAD_NUM = 1;
		
		BaseConfig.PARSER_CONFIG = null;
		
		BaseConfig.FEED_TYPE_CONF = null;
		BaseConfig.FEED_STORE_PATH = "";
		BaseConfig.FEED_FILE = new String[] {};
		BaseConfig.FEED_PROCESSOR = null;
		BaseConfig.FEED_READING = "";
		
		BaseConfig.CONF_PARSER_REALTIME_CLASS = null;
		BaseConfig.CONF_PARSER_DETAIL_CLASS = null;
		BaseConfig.IS_DELETE_ALL = false;
		BaseConfig.CONF_DISABLE_LOG = false;
		
		BaseConfig.CONF_BOT_DUE_HOUR_RATE = 24;
		BaseConfig.CONF_BOT_WCE_HOUR_RATE = 24;
	}
}
