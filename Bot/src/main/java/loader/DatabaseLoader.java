package loader;

import db.BotConfigDB;
import db.MerchantDB;
import db.ProcessDB;
import db.manager.DatabaseFactory;
import db.manager.DatabaseUtil;
import engine.BaseConfig;
import manager.CommandManager;
import unittest.MockObjectHolder;

import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.lang3.StringUtils;

import bean.BotConfigBean;
import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;

public class DatabaseLoader {
	private static QueryRunner q;
	public static  void setDatabaseLoader(QueryRunner query) {
		q = query;
	}
	public static boolean loadDBConnection() throws Exception {		
		DatabaseFactory.registerConfigDataSource(BaseConfig.CONF_PROP.getProperty("ConfigDBURL"), BaseConfig.CONF_PROP.getProperty("ConfigDBDriverName"),
				BaseConfig.CONF_PROP.getProperty("ConfigDBUserName"), BaseConfig.CONF_PROP.getProperty("ConfigDBPassword"), 2, 5, 2, 1);
		if(!MockObjectHolder.isMock) {
			q = new QueryRunner(DatabaseFactory.getConfigDSInstance());
		}
		
		try {
			
			MerchantBean b = findMerchantQueue();
			
			if(b == null) {
				System.out.println(new Date() + " : Not Found Queue");
				return false;
			}
			
			if(b.getActive() == 3)
				b.setDataServer("Bot-verify" + b.getNodeTest());
			
			BotConfigBean bConfig = findBotConfig(b);
			if(bConfig == null)
				throw new Exception("Not Found Bot Database Config : " + b.getDataServer());
			
			setMerchantProperties(b);
			String botUrl = "jdbc:mysql://"+ bConfig.getUrl();
			DatabaseFactory.registerDataSource(botUrl, "com.mysql.jdbc.Driver", bConfig.getUsername(), bConfig.getPassword(), 10, 15, 15, 8);
			return true;
			
		} catch(RuntimeException e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	
	public static void closeDBConnection() {
		DatabaseUtil.closeDataSource(DatabaseFactory.getConfigDSInstance());
		DatabaseUtil.closeDataSource(DatabaseFactory.getBotDSInstance());
	}
	
	private static MerchantBean findMerchantQueue() throws SQLException, InterruptedException, RuntimeException {
		int round = 0;
		while(!Thread.currentThread().isInterrupted()) {
			int update = q.update(ProcessDB.UPDATE_PROCESS, BaseConfig.SESSION_ID, BaseConfig.RUNTIME_TYPE, "available");
			if(update == 1)
				break;
			round++;
			if(round == 36) {
				throw new RuntimeException(new Date() + " : Process " + BaseConfig.SESSION_ID + "reach 36 rounds, exit process.");
			}
			Thread.sleep(5000);
		}
		String command = "NULL", activeSet = "";
		switch(BaseConfig.RUNTIME_TYPE){
			case("WCE") : command = "%"+CommandManager.PRIMARY_WCE+"%"; activeSet = "1,3"; break; 
			case("DUE") : command = "%"+CommandManager.PRIMARY_DUE+"%"; activeSet = "1";   break; 
		}
		if(!StringUtils.isBlank(BaseConfig.BOT_FAMILY))
			return q.query(DatabaseUtil.replaceSQL(MerchantDB.FIND_FAMILY_QUEUE, activeSet), new BeanHandler<MerchantBean>(MerchantBean.class)
					,BaseConfig.BOT_FAMILY, STATE.BOT_FINISH.toString(), STATUS.FINISH.toString()
					, command, STATE.BOT_FINISH.toString(), STATUS.FINISH.toString(),BaseConfig.BOT_FAMILY);
		
		return q.query(DatabaseUtil.replaceSQL(MerchantDB.FIND_QUEUE, activeSet), new BeanHandler<MerchantBean>(MerchantBean.class)
				, STATE.BOT_FINISH.toString(), STATUS.FINISH.toString()
				, command, STATE.BOT_FINISH.toString(), STATUS.FINISH.toString());
	}
	
	public static void updateQueue() throws SQLException {
		q.update(ProcessDB.UPDATE_PROCESS, "available", BaseConfig.RUNTIME_TYPE, BaseConfig.SESSION_ID);
	}
	
	private static BotConfigBean findBotConfig(MerchantBean b) throws SQLException {
		return q.query(BotConfigDB.SELECT_BY_SERVER_DATA_AVAILABLE, new BeanHandler<BotConfigBean>(BotConfigBean.class), b.getDataServer(), 1);
	}
	
	private static void setMerchantProperties(MerchantBean b) {
		BaseConfig.MERCHANT_ID = b.getId();
		BaseConfig.MERCHANT_NAME = b.getName();
		BaseConfig.MERCHANT_ACTIVE = b.getActive();
		BaseConfig.PACKAGE_TYPE = b.getPackageType();
		BaseConfig.NEXT_START = (!StringUtils.isBlank(b.getCommand()) && b.getCommand().contains("PRIMARY"))? new Date() : 
			BaseConfig.RUNTIME_TYPE.equals("WCE") ? b.getWCENextStart() : b.getDUENextStart();
		System.setProperty("merchantId", String.valueOf(BaseConfig.MERCHANT_ID));
		System.setProperty("logFile", String.valueOf(BaseConfig.MERCHANT_ID) + "-" + BaseConfig.RUNTIME_TYPE.toLowerCase());
	}
}
