package db.manager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class DatabaseUtil {
	private static final String SQL_TEST = "select 1 from DUAL";

    public static DataSource createDataSource(String connectURI, String driverClassName, String username, String password, int initialSize) {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(connectURI);
        ds.setInitialSize(initialSize);
        ds.setTestOnBorrow(true);
        ds.setValidationQuery(SQL_TEST);
        return ds;
    }
    
    public static DataSource createDataSource(String connectURI, String driverClassName, String username, String password, int initialSize, int maxActive, int maxIdle, int minIdle) {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(connectURI);
        ds.setInitialSize(initialSize);
        ds.setMaxTotal(maxActive);
        ds.setMaxIdle(maxIdle);
        ds.setMinIdle(minIdle);
        ds.setTestOnBorrow(true);
        ds.setValidationQuery(SQL_TEST);
        return ds;
    }

    public static void closeDataSource(DataSource ds) {
        try {
        	BasicDataSource bds = (BasicDataSource) ds;
        	if(bds != null)
        		bds.close();
		} catch (SQLException e) {}
    }

    public static void closeResultSet(ResultSet rs) {
        if(rs != null) {
        	try {
				rs.close();
			} catch (SQLException e) {}
        }
    }
    
    public static void closeStatement(Statement stmt) {
        if(stmt != null) {
        	try {
        		stmt.close();
			} catch (SQLException e) {}
        }
    }
    
    public static void closeConnection(Connection conn) {
        if(conn != null) {
        	try {
        		conn.close();
			} catch (SQLException e) {}
        }
    }
    
    public static String replaceSQL(String base, String filter) {		
		base = base.replaceFirst("---filter---", filter);
		return base;
	}
        
    public static String replaceSQL(String base, String filter, String order) {		
		base = base.replaceFirst("---filter---", filter);
		base = base.replaceFirst("---order---", order);
		return base;
	}
}
