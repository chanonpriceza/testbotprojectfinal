package db.manager;

import java.sql.SQLException;

import javax.sql.DataSource;

public class DatabaseFactory {
	private static DataSource BOT_DS;
	private static DataSource CONFIG_DS;
	
	public static synchronized DataSource getBotDSInstance() {
		return BOT_DS;
	}
	
	public static synchronized DataSource getConfigDSInstance() {
		return CONFIG_DS;
	}
	
	public static synchronized void registerDataSource(String connectURI, String driverClassName, String username, String password, int initialSize) throws SQLException {		
		if(BOT_DS == null) {
			BOT_DS = DatabaseUtil.createDataSource(connectURI, driverClassName, username, password, initialSize);
			return;
		}
		throw new SQLException("datasource already create");
	}
	
	public static synchronized void registerDataSource(String connectURI, String driverClassName, String username, String password, int initialSize, int maxActive, int maxIdle, int minIdle) throws SQLException {		
		if(BOT_DS == null) {
			BOT_DS = DatabaseUtil.createDataSource(connectURI, driverClassName, username, password, initialSize, maxActive, maxIdle, minIdle);
			return;
		}
		throw new SQLException("datasource already create");
	}
	
	public static synchronized void registerConfigDataSource(String connectURI, String driverClassName, String username, String password, int initialSize) throws SQLException {		
		if(CONFIG_DS == null) {
			CONFIG_DS = DatabaseUtil.createDataSource(connectURI, driverClassName, username, password, initialSize);
			return;
		}
		throw new SQLException("config datasource already create");
	}
	
	public static synchronized void registerConfigDataSource(String connectURI, String driverClassName, String username, String password, int initialSize, int maxActive, int maxIdle, int minIdle) throws SQLException {		
		if(CONFIG_DS == null) {
			CONFIG_DS = DatabaseUtil.createDataSource(connectURI, driverClassName, username, password, initialSize, maxActive, maxIdle, minIdle);
			return;
		}
		throw new SQLException("config datasource already create");
	}
	
	public static void clearDataSource() {
		 BOT_DS = null;
		 CONFIG_DS = null;
	}
}
