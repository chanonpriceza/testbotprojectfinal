package db.manager;

import db.BotConfigDB;
import db.ConfigDB;
import db.MerchantConfigDB;
import db.MerchantDB;
import db.ParserConfigDB;
import db.ProductDataDB;
import db.ProductMappingDB;
import db.ProductUrlDB;
import db.ReportDB;
import db.SendDataDB;
import db.UrlPatternDB;
import db.WorkloadDB;
import unittest.MockObjectHolder;

public class DatabaseManager {
	private MerchantDB merchantDB;
	private MerchantConfigDB merchantConfigDB;
	private BotConfigDB botConfigDB;
	private ParserConfigDB parserConfigDB;
	private UrlPatternDB urlPatternDB;
	private ReportDB reportDB;
	private ProductUrlDB productUrlDB;
	private ConfigDB configDB;
	private ProductMappingDB productMappingDB; 
	
	private ProductDataDB productDataDB;
	private SendDataDB sendDataDB;
	private WorkloadDB workloadDB;
	
	private static class SingletonHelper {
        private static final DatabaseManager INSTANCE = new DatabaseManager();
    }
	
	public static DatabaseManager getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.mockDatabaseManager;
		}
		return SingletonHelper.INSTANCE;
	}
	
	private DatabaseManager() {
		merchantConfigDB = new MerchantConfigDB(DatabaseFactory.getConfigDSInstance());
		merchantDB = new MerchantDB(DatabaseFactory.getConfigDSInstance());
		botConfigDB = new BotConfigDB(DatabaseFactory.getConfigDSInstance());
		parserConfigDB = new ParserConfigDB(DatabaseFactory.getConfigDSInstance());
		urlPatternDB = new UrlPatternDB(DatabaseFactory.getConfigDSInstance());
		reportDB = new ReportDB(DatabaseFactory.getConfigDSInstance());
		configDB = new ConfigDB(DatabaseFactory.getConfigDSInstance());
		productMappingDB = new ProductMappingDB(DatabaseFactory.getConfigDSInstance());
		
		productDataDB = new ProductDataDB(DatabaseFactory.getBotDSInstance());
		productUrlDB = new ProductUrlDB(DatabaseFactory.getBotDSInstance());
		sendDataDB = new SendDataDB(DatabaseFactory.getBotDSInstance());
		workloadDB = new WorkloadDB(DatabaseFactory.getBotDSInstance());
	}
	
	public MerchantDB getMerchantDB() {
		return merchantDB;
	}

	public MerchantConfigDB getMerchantConfigDB() {
		return merchantConfigDB;
	}

	public BotConfigDB getBotConfigDB() {
		return botConfigDB;
	}
	
	public ParserConfigDB getParserConfigDB() {
		return parserConfigDB;
	}

	public UrlPatternDB getUrlPatternDB() {
		return urlPatternDB;
	}

	public ProductDataDB getProductDataDB() {
		return productDataDB;
	}

	public SendDataDB getSendDataDB() {
		return sendDataDB;
	}
	
	public WorkloadDB getWorkloadDB() {
		return workloadDB;
	}

	public ConfigDB getConfigDB() {
		return configDB;
	}
	
	public ReportDB getReportDB() {
		return reportDB;
	}

	public ProductUrlDB getProductUrlDB() {
		return productUrlDB;
	}

	public ProductMappingDB getProductMappingDB() {
		return productMappingDB;
	}
	
}
