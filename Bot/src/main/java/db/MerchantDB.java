package db;

import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.lang3.StringUtils;

import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.MerchantBean.STATUS;
import engine.BaseConfig;

public class MerchantDB {
	private ResultSetHandler<MerchantBean> beanHandler;
	private QueryRunner queryRunner;
	private static final String SELECT_MERCHANT_BY_ID = "SELECT * FROM tbl_merchant WHERE id = ?";
	private static final String UPDATE_NEXTSTART = generateField("UPDATE tbl_merchant SET $NEXTSTART = ?, $STATE = ?, $STATUS = ? WHERE id = ?");
	private static final String UPDATE_ACTIVE_NEXTSTART = generateField("UPDATE tbl_merchant SET active = ?, $NEXTSTART = ?, $STATE = ?, $STATUS = ? WHERE id = ?");
	private static final String UPDATE_ACTIVE_ERRORMESSAGE = "UPDATE tbl_merchant SET active = ?, errorMessage = ? WHERE id = ?";
	private static final String UPDATE_STATE_STATUS = generateField("UPDATE tbl_merchant SET $STATE = ?, $STATUS = ? WHERE id = ?");
	private static final String UPDATE_STATUS = generateField("UPDATE tbl_merchant SET $STATUS = ? WHERE id = ?");
	private static final String CLEAR_COMMAND = "UPDATE tbl_merchant SET command = REPLACE(command, ?, '') WHERE id = ?";
	public static final String FIND_QUEUE = generateField("(SELECT * FROM tbl_merchant WHERE active IN (---filter---) AND $STATE = ? AND $STATUS = ? AND $NEXTSTART IS NOT null AND now() >= $NEXTSTART AND botFamily IS NULL ORDER BY active DESC, $NEXTSTART LIMIT 1) "
			+ "UNION (SELECT * FROM tbl_merchant WHERE command like ? AND $STATE = ? AND $STATUS = ? AND $NEXTSTART IS NOT NULL AND botFamily IS NULL ORDER BY $NEXTSTART LIMIT 1) ORDER BY command DESC, $NEXTSTART LIMIT 1");
	public static final String FIND_FAMILY_QUEUE = generateField("(SELECT * FROM tbl_merchant WHERE active IN (---filter---) AND botFamily = ? AND $STATE = ? AND $STATUS = ? AND $NEXTSTART IS NOT NULL AND now() >= $NEXTSTART ORDER BY active DESC, $NEXTSTART LIMIT 1) "
			+ "UNION (SELECT * FROM tbl_merchant WHERE command like ? AND $STATE = ? AND $STATUS = ? AND $NEXTSTART IS NOT NULL AND botFamily = ?  ORDER BY $NEXTSTART LIMIT 1) ORDER BY command DESC, $NEXTSTART LIMIT 1");
	public MerchantDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MerchantBean>(MerchantBean.class);
	}
	
	public MerchantBean getMerchantById(int id) throws SQLException{
		return queryRunner.query(SELECT_MERCHANT_BY_ID, beanHandler, id);
	}
	
	public int updateNextStart(int merchantId, Date time, STATE state, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE_NEXTSTART, time, state.toString(), status.toString(), merchantId);
	}
	
	public int updateActiveNextStart(int merchantId, int active, Date time, STATE state, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE_ACTIVE_NEXTSTART, active, time, state.toString(), status.toString(), merchantId);
	}
	
	public int updateStateStatus(int merchantId, STATE state, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE_STATE_STATUS, state.toString(), status.toString(), merchantId);
	}
	
	public int updateStatus(int merchantId, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE_STATUS, status.toString(), merchantId);
	}
	
	public int updateActiveErrorMessage(int merchantId, int active, String errorMessage) throws SQLException {
		return queryRunner.update(UPDATE_ACTIVE_ERRORMESSAGE, active, errorMessage, merchantId);
	}

	public int clearCommand(int merchantId, String command) throws SQLException{
		return queryRunner.update(CLEAR_COMMAND, command, merchantId);
	}
	
	private static String generateField(String query) {
		if(BaseConfig.RUNTIME_TYPE == null)
			return query;
		String[] source = { "$STATE", "$STATUS", "$NEXTSTART"}; 
		String[] replace = { "", "", ""};
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")){
			replace[0] = "state";
			replace[1] = "status";
			replace[2] = "wceNextStart";
		}else if(BaseConfig.RUNTIME_TYPE.equals("DUE")){
			replace[0] = "dataUpdateState";
			replace[1] = "dataUpdateStatus";
			replace[2] = "dueNextStart";
		}
		return StringUtils.replaceEach(query, source, replace);
	}
}
