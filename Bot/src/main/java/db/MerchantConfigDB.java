package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.MerchantConfigBean;
import bean.MerchantConfigBean.FIELD;

public class MerchantConfigDB {
		
	private ResultSetHandler<List<MerchantConfigBean>> beanListHandler;
	private ScalarHandler<String> stringValueHandler;
	private QueryRunner queryRunner;
	
	private static final String SELECT_BY_MERCHANTID = "SELECT * FROM tbl_merchant_config WHERE merchantId = ?";
	private static final String SELECT_BY_MERCHANTID_FIELD = "SELECT value FROM tbl_merchant_config WHERE merchantId = ? and field = ?";
	private static final String UPDATE_CONFIG_VALUE = "UPDATE tbl_merchant_config SET value = ? WHERE field = ? AND merchantId = ?";
	
	public MerchantConfigDB(DataSource ds) {
		this.queryRunner = new QueryRunner(ds);
		stringValueHandler = new ScalarHandler<>();
		beanListHandler = new BeanListHandler<MerchantConfigBean>(MerchantConfigBean.class);		
	}
	
	public List<MerchantConfigBean> findMerchantConfig(int merchantId) throws SQLException {
		return queryRunner.query(SELECT_BY_MERCHANTID, beanListHandler, merchantId);
	}
	
	public String findMerchantConfigValue(int merchantId, String field) throws SQLException {
		return queryRunner.query(SELECT_BY_MERCHANTID_FIELD, stringValueHandler, merchantId, field);
	}
		
	public int updateConfigValue(FIELD field, String value, int merchantId) throws SQLException {
		return queryRunner.update(UPDATE_CONFIG_VALUE, value, field.toString(), merchantId);
	}
	
}
