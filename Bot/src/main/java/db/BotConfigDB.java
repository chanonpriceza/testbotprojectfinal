package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.BotConfigBean;

public class BotConfigDB {
		
	private ResultSetHandler<BotConfigBean> beanHandler;
	private ResultSetHandler<List<BotConfigBean>> beanListHandler;
	private QueryRunner queryRunner;
	
	private static final String SELECT_BY_SERVER_NAME = "SELECT * FROM tbl_botdb_config WHERE server =  ?";
	private static final String SELECT_BY_SERVER_LIST = "SELECT * FROM tbl_botdb_config ";
	public static final String SELECT_BY_SERVER_DATA_AVAILABLE = "SELECT * FROM tbl_botdb_config WHERE server = ? AND dataAvailable = ?";
	private static final String SELECT_BY_DATA_AVAILABLE = "SELECT * FROM tbl_botdb_config WHERE dataAvailable = ?";
	
	public BotConfigDB(DataSource dataSource) {		
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<BotConfigBean>(BotConfigBean.class);	
		beanListHandler = new BeanListHandler<BotConfigBean>(BotConfigBean.class);
	}
	
	public BotConfigBean findBotConfig(String serverName) throws SQLException{	
		return queryRunner.query(SELECT_BY_SERVER_NAME, beanHandler, serverName);
	}
	
	public BotConfigBean getByServerDataAvailable(String serverName, int datAvailable) throws SQLException{	
		return queryRunner.query(SELECT_BY_SERVER_DATA_AVAILABLE, beanHandler, serverName, datAvailable);
	}
	
	public List<BotConfigBean> getByDataAvailable(int datAvailable) throws SQLException{	
		return queryRunner.query(SELECT_BY_DATA_AVAILABLE, beanListHandler, datAvailable);
	}
	
	public List<BotConfigBean> getBotConfigList() throws SQLException{	
		return queryRunner.query(SELECT_BY_SERVER_LIST, beanListHandler);
	}	
}
