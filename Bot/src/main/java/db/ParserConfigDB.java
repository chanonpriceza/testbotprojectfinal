package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.ParserConfigBean;

public class ParserConfigDB {
		
	private ResultSetHandler<List<ParserConfigBean>> beanListHandler;
	
	public final String SELECT_BY_MERCHANTID = "SELECT * FROM tbl_parser_config WHERE merchantId = ? ORDER BY fieldType, id";
	private QueryRunner queryRunner;
	
	public ParserConfigDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<ParserConfigBean>(ParserConfigBean.class);
	}
	
	public List<ParserConfigBean> findParserConfig(int merchantId) throws SQLException {
		return queryRunner.query(SELECT_BY_MERCHANTID, beanListHandler, merchantId);
	}
	
}
