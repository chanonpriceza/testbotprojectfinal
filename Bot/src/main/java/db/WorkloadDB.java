package db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import utils.BotUtil;

public class WorkloadDB {
	
	private ResultSetHandler<List<WorkloadBean>> beanListHandler;
	
	public static final String DELETE = "DELETE FROM tbl_workload WHERE merchantId = ?";
	public static final String INSERT = "INSERT IGNORE INTO tbl_workload(merchantId, url, depth, categoryId, keyword, status) VALUES (?,?,?,?,?,?)";
	public static final String GET_BY_STATUS = "SELECT * FROM tbl_workload WHERE merchantId = ? AND status = ? ";
	public static final String GET_MIN_DEPTH = "SELECT * FROM tbl_workload WHERE merchantId = ? AND status = ? "
			+ "AND depth = (SELECT depth FROM tbl_workload WHERE merchantId = ? AND status = ? ORDER BY depth ASC LIMIT 1 ) LIMIT ?";
	public static final String UPDATE_STATUS = "UPDATE tbl_workload SET status = ? WHERE merchantId = ? AND url = ?";
	public static final String UPDATE_STATUS_BATCH = "UPDATE tbl_workload SET status = ? WHERE merchantId = ? AND url = ?";
	
	QueryRunner queryRunner; 		
	
	public WorkloadDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<WorkloadBean>(WorkloadBean.class);		
	}

	public int clearWorkload(int merchantId) throws SQLException {
		return queryRunner.update(DELETE, merchantId); 
	}

	public int insert(int merchantId, String url, int depth, int categoryId, String keyword, String status) throws SQLException {
		return queryRunner.update(INSERT, merchantId, url, depth, categoryId, keyword, status);
	}
	
	public List<WorkloadBean> getByStatus(int merchantId, STATUS status) throws SQLException{
		return queryRunner.query(GET_BY_STATUS, beanListHandler, merchantId, status.toString());
	}
	
	public List<WorkloadBean> getMinDepth(int merchantId, int offset) throws SQLException{
		return queryRunner.query(GET_MIN_DEPTH, beanListHandler, merchantId, STATUS.W.toString(), merchantId, STATUS.W.toString(), offset);
	}
	
	public int updateStatus(int merchantId, String url, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE_STATUS, status.toString(), merchantId, url);
	}
	
	public int[] updateStatusBatch(int merchantId, STATUS status, List<String> urls) throws SQLException {
		List<Object> fixObjList = Arrays.asList(status.toString(), merchantId);
		List<Object> urlObjList = new ArrayList<>(urls);
		Object[][] obj = BotUtil.genSQLBatchParam(fixObjList, urlObjList);
		return queryRunner.batch(UPDATE_STATUS_BATCH, obj);
	}
	
}
