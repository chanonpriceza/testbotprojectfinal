package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang3.ArrayUtils;

import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import db.manager.DatabaseUtil;
import utils.BotUtil;
import utils.FilterUtil;

public class ProductDataDB {
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	public static final int KEYWORD_LENGTH = 500;
	
	private QueryRunner queryRunner;
	private ResultSetHandler<ProductDataBean> beanHandler;
	private ResultSetHandler<List<ProductDataBean>> beanListHandler;
	private ScalarHandler<Long> longResultHandler;
	private ColumnListHandler<Long> listIdHandler;
	
	
	private static final String INSERT = "INSERT IGNORE INTO tbl_product_data" +
			"(merchantId, name, price, url, urlForUpdate, description, pictureUrl, " +
			" status, updateDate, errorUpdateCount, categoryId, keyword, merchantUpdateDate, realProductId, basePrice, upc) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE = "UPDATE tbl_product_data SET" +
			" price = ?, url = ?, description = ?, pictureUrl = ?, " +
			" status = ?, updateDate = ?, errorUpdateCount = ?, categoryId = ?, merchantUpdateDate = ?, realProductId =?, keyword = ?, basePrice = ?, upc = ?" +
			" WHERE id = ?";
	private static final String DELETE = "DELETE FROM tbl_product_data WHERE id = ?";
	private static final String UPDATE_STATUS_AND_ERROR_UPDATE_COUNT = "UPDATE tbl_product_data SET status = ?, errorUpdateCount = ? WHERE id = ?";
	private static final String GET_BY_ID = "SELECT * FROM tbl_product_data WHERE id = ?";
	private static final String GET_BY_MERCHANTID_NAME = "SELECT * FROM tbl_product_data WHERE merchantId = ? AND name = ?";
	private static final String GET_BY_MERCHANTID_URLFORUPDATE = "SELECT * FROM tbl_product_data WHERE merchantId = ? AND urlForUpdate = ?";
	private static final String COUNT_PRODUCT_BY_MERCHANT = "SELECT COUNT(*) FROM tbl_product_data WHERE merchantId = ?";
	private static final String GET_BY_MERCHANT_STATUS = "SELECT id FROM tbl_product_data WHERE merchantId = ? AND status = ? LIMIT ?";
	private static final String UPDATE_STATUS = "UPDATE tbl_product_data SET status = ? WHERE id = ?";
	private static final String UPDATE_STATUS_IDS = "UPDATE tbl_product_data SET status = ? WHERE id IN (---filter---)";
	private static final String GET_BY_STATUS_LIMIT = "SELECT * FROM tbl_product_data INNER JOIN "
			+ " (SELECT id FROM tbl_product_data WHERE merchantId = ? AND status = ? LIMIT ?) AS p ON tbl_product_data.id = p.id ";
	private static final String COUNT_BY_STATUS = "SELECT COUNT(*) FROM tbl_product_data WHERE merchantId = ? AND status = ?";
	private static final String GET_ID_BY_MERCHANTID_NAME = "SELECT id FROM tbl_product_data WHERE merchantId = ? AND name = ?";
	
	public ProductDataDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ProductDataBean>(ProductDataBean.class);
		beanListHandler = new BeanListHandler<ProductDataBean>(ProductDataBean.class);
		longResultHandler = new ScalarHandler<>();
		listIdHandler = new ColumnListHandler<>("id");
	}
	
	public ProductDataBean getProduct(int merchantId, String name) throws SQLException {
		return queryRunner.query(GET_BY_MERCHANTID_NAME, beanHandler, merchantId, name);
	}
	
	public ProductDataBean getProductByUrlForUpdate(int merchantId, String urlForUpdate) throws SQLException {
		return queryRunner.query(GET_BY_MERCHANTID_URLFORUPDATE, beanHandler, merchantId, urlForUpdate);
	}
	
	public ProductDataBean getProduct(int id) throws SQLException {
		return queryRunner.query(GET_BY_ID, beanHandler, id);
	}
	
	public int insertProductData(ProductDataBean productDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				productDataBean.getMerchantId(),
				FilterUtil.limitString(productDataBean.getName(), NAME_LENGTH),
				productDataBean.getPrice(),
				productDataBean.getUrl(),
				productDataBean.getUrlForUpdate(),
				FilterUtil.limitString(productDataBean.getDescription(), DESCRIPTION_LENGTH), 
				productDataBean.getPictureUrl(),
				STATUS.C.toString(),
				productDataBean.getUpdateDate(),
				productDataBean.getErrorUpdateCount(),
				productDataBean.getCategoryId(),
				productDataBean.getKeyword(),
				productDataBean.getMerchantUpdateDate(),
				productDataBean.getRealProductId(),
				productDataBean.getBasePrice(),
				productDataBean.getUpc()

		}); 		
	}
	
	public int updateProductData(int id, ProductDataBean productDataBean) throws SQLException {
		return queryRunner.update(UPDATE, new Object[] {
				productDataBean.getPrice(),
				productDataBean.getUrl(),
				BotUtil.limitString(productDataBean.getDescription(), DESCRIPTION_LENGTH), 
				productDataBean.getPictureUrl(),
				STATUS.C.toString(),
				productDataBean.getUpdateDate(),
				productDataBean.getErrorUpdateCount(),
				productDataBean.getCategoryId(),
				productDataBean.getMerchantUpdateDate(),
				productDataBean.getRealProductId(),
				productDataBean.getKeyword(),
				productDataBean.getBasePrice(),
				productDataBean.getUpc(),
				id 
		});
	}
	
	public int updateStatusAndErrorUpdateCount(int id, STATUS status, int errorUpdateCount) throws SQLException {
		return queryRunner.update(UPDATE_STATUS_AND_ERROR_UPDATE_COUNT, status.toString(), errorUpdateCount, id); 
	}
	
	public int countProductByMerchant(int merchantId) throws SQLException {
		return Math.toIntExact(queryRunner.query(COUNT_PRODUCT_BY_MERCHANT, longResultHandler, merchantId));		
	}
	
	public List<Integer> getByMerchantStatus(int merchantId, STATUS status, int limit) throws SQLException {
		return BotUtil.longToInt(queryRunner.query(GET_BY_MERCHANT_STATUS, listIdHandler, merchantId, status.toString(), limit));
	}
	
	public int[] updateStatus(Object[][] obj) throws SQLException {
		return queryRunner.batch(UPDATE_STATUS, obj); 
	}
	
	public int updateStatus(STATUS status, List<Integer> ids) throws SQLException {
		String filter = BotUtil.genSQLParam(ids.size());
		return queryRunner.update(DatabaseUtil.replaceSQL(UPDATE_STATUS_IDS, filter), ArrayUtils.addAll(new Object[] {status.toString()}, ids.toArray())); 
	}
	
	public List<ProductDataBean> getByStatusLimit(int merchantId, STATUS status, int limit) throws SQLException {
		return queryRunner.query(GET_BY_STATUS_LIMIT, beanListHandler, merchantId, status.toString(), limit);
	}
	
	public int countByStatus(int merchantId, STATUS status) throws SQLException {
		return Math.toIntExact(queryRunner.query(COUNT_BY_STATUS, longResultHandler, merchantId, status.toString()));		
	}
	
	public int delete(int id) throws SQLException {
		return queryRunner.update(DELETE, id); 
	}
	
	public int getIdByMerchantIdName(int merchantId, String name) throws SQLException {
		Long id = queryRunner.query(GET_ID_BY_MERCHANTID_NAME, longResultHandler, merchantId, name);
		return id == null ? -1 : Math.toIntExact(id);
	}
}
