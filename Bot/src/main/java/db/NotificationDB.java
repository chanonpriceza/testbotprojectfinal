package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.NotificationBean;

public class NotificationDB {
	private ResultSetHandler<List<NotificationBean>> beanListHandler;
	private ResultSetHandler<Long> numRecordHandler;
	QueryRunner queryRunner;
	
	public NotificationDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<NotificationBean>(NotificationBean.class);
		numRecordHandler = new ScalarHandler<>();
		new BeanHandler<NotificationBean>(NotificationBean.class);
	}
	
	private static final String INSERT_NOTIFICATION = "INSERT INTO tbl_notification (user, status,message,createDate) VALUES (?, ?, ?,NOW()) ";
	public int insertNotification(String owner, String status, String message) throws SQLException{
		return queryRunner.update(INSERT_NOTIFICATION, new Object[]{owner, status, message});
	}
	
	public static final String INSERT_BATCH = "INSERT INTO tbl_notification (user, status,message,createDate) VALUES (?,?,?,NOW())";
	public int[] insertNotiDataBatch(Object[][] obj) throws SQLException{
		return queryRunner.batch(INSERT_BATCH, obj);
	}
	
	private static final String GET_ALL_NOTIFICATION_BY_USERID = "SELECT * FROM tbl_notification  WHERE user = ? ORDER BY id DESC LIMIT ?,?";
	public List<NotificationBean> getAllNotificationByUserID(int userID ,int start, int offset) throws SQLException {
		return queryRunner.query(GET_ALL_NOTIFICATION_BY_USERID, beanListHandler, new Object[]{userID,start,offset});
	}
	
	private static final String UPDATE_STATUS_NOTIFICATION = "UPDATE tbl_notification SET status=? WHERE id = ?";
	public int updateStatusNotification(String notiId ,String status) throws SQLException {
		return queryRunner.update(UPDATE_STATUS_NOTIFICATION, new Object[] {status,notiId});
	}	
	private static final String UPDATE_STATUS_ALLREAD_NOTIFICATION = "UPDATE tbl_notification SET status='READ' WHERE user = ? AND status='UNREAD'";
	public int updateStatusAllNotification(int userid) throws SQLException {
		return queryRunner.update(UPDATE_STATUS_ALLREAD_NOTIFICATION, new Object[] {userid});
	}	
	
	private static final String COUNT_ALL_NOTIFICATION_BY_USERID = "SELECT COUNT(id) FROM tbl_notification  WHERE user = ? ORDER BY id DESC LIMIT ?,?"; 
	public int countAllNotification(int userID,int start, int offset) throws SQLException {
		return queryRunner.query(COUNT_ALL_NOTIFICATION_BY_USERID, numRecordHandler,new Object[]{userID,start,offset}).intValue();
	}
}
