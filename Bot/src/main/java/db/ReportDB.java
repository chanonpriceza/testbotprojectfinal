package db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.ReportBean;
import bean.ReportBean.STATUS;

public class ReportDB {
	
	private QueryRunner queryRunner;
	private ScalarHandler<Long> longResultHandler;
	
	private static final String INSERT_REPORT = "INSERT INTO tbl_report (`name`, `merchantId`, `package`, `type`,`status`, `startDate`, `endDate`, `defineDate`, `serverReportRef` , `duplicate`, `expire`, `delete`)  VALUE (?, ?, ?, ?, ?, NOW(), null, ?, ?, ?, ?, ?)";
	private static final String UPDATE_REPORT = "UPDATE tbl_report SET countOld = ?, countNew = ?, successCount = ?, errorCount = ?, `add` = ?, `update` = ?, updatePic = ?, `duplicate` = ?, `expire` = ?, `delete` = ?, `status` = ?, endDate = NOW(), msgData = ? WHERE id = ? AND `status` = ?";
	private static final String MARK_LAST_REPORT = "UPDATE tbl_report INNER JOIN (SELECT r.id FROM tbl_report INNER JOIN (SELECT id, `status` FROM tbl_report "
			+ " WHERE merchantId = ? and type = ? ORDER BY Id DESC LIMIT 1) AS r ON tbl_report.id = r.id WHERE r.status = ?) "
			+ " AS r ON tbl_report.id = r.id SET `status` = ?";
	
	public ReportDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		longResultHandler = new ScalarHandler<>();
	}
	
	public int insertReport(ReportBean reportBean) throws SQLException  {
		return Math.toIntExact(queryRunner.insert(INSERT_REPORT, longResultHandler,
				reportBean.getName(),
				reportBean.getMerchantId(),
				reportBean.getPackageType(),
				reportBean.getType(), 
				reportBean.getStatus(),
				reportBean.getDefineDate(),
				reportBean.getServerRef(),
				reportBean.getDuplicate(),
				reportBean.getExpire(),
				reportBean.getDelete())
		); 
	}
	
	public int updateReport(ReportBean reportBean, STATUS statusFrom) throws SQLException  {
		return queryRunner.update(UPDATE_REPORT, new Object[] {
				reportBean.getCountOld(),
				reportBean.getCountNew(),
				reportBean.getSuccessCount(),
				reportBean.getErrorCount(),
				reportBean.getAdd(),
				reportBean.getUpdate(),
				reportBean.getUpdatePic(),
				reportBean.getDuplicate(),
				reportBean.getExpire(),
				reportBean.getDelete(),
				reportBean.getStatus(),
				reportBean.getMsgData(),
				reportBean.getId(),
				statusFrom.toString()	
		});
	}
	
	public int markLastReport(int merchantId, String type, STATUS statusFrom, STATUS statusTo) throws SQLException  {
		return queryRunner.update(MARK_LAST_REPORT, merchantId, type, statusFrom.toString(), statusTo.toString());
	}	
}
