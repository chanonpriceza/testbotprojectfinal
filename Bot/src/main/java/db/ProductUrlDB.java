package db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.ArrayUtils;

import bean.ProductUrlBean;
import bean.ProductUrlBean.STATE;
import bean.ProductUrlBean.STATUS;
import db.manager.DatabaseUtil;
import utils.BotUtil;

public class ProductUrlDB {
		
	private QueryRunner queryRunner;
	private ResultSetHandler<List<ProductUrlBean>> beanListHandler;
	
	private static final String DELETE_BY_MERCHANT_URL = "DELETE FROM tbl_product_url WHERE merchantId = ? AND url = ?";
	private static final String DELETE_BY_STATUS = "DELETE FROM tbl_product_url WHERE merchantId = ? AND status IN (---filter---)";
	private static final String GET_BY_STATUS_LIMIT = "SELECT * FROM tbl_product_url INNER JOIN "
			+ "(SELECT merchantId, url FROM tbl_product_url WHERE merchantId = ? AND status = ? LIMIT ?) AS p "
			+ "ON tbl_product_url.merchantId = p.merchantId AND tbl_product_url.url = p.url ";
	private static final String UPDATE_STATUS_BATCH = "UPDATE tbl_product_url SET status = ? WHERE merchantId = ? AND url = ?";
	private static final String UPDATE_STATUS_STATE = "UPDATE tbl_product_url SET status = ?, state = ? WHERE merchantId = ? AND url = ?";
	private static final String INSERT = "INSERT IGNORE INTO tbl_product_url(merchantId, url, state, status, categoryId, keyword) VALUES (?,?,?,?,?,?)";
	
	public ProductUrlDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		this.beanListHandler = new BeanListHandler<ProductUrlBean>(ProductUrlBean.class);
	}
	
	public int clearProductUrl(int merchantId, String url) throws SQLException {
		return queryRunner.update(DELETE_BY_MERCHANT_URL, merchantId, url); 
	}
	
	public int clearProductUrlByStatus(int merchantId, Object[] statusList) throws SQLException{
		String filter = BotUtil.genSQLParam(statusList.length);
		return queryRunner.update(DatabaseUtil.replaceSQL(DELETE_BY_STATUS, filter), ArrayUtils.addAll(new Object[] {merchantId}, statusList)); 
	}
	
	public List<ProductUrlBean> getByStatusLimit(int merchantId, STATUS status, int limit) throws SQLException {
		return queryRunner.query(GET_BY_STATUS_LIMIT, beanListHandler, merchantId, status.toString(), limit);
	}
	
	public int[] updateStatus(int merchantId, STATUS status, List<String> urls) throws SQLException {
		List<Object> fixObjList = Arrays.asList(status.toString(), merchantId);
		List<Object> urlObjList = new ArrayList<>(urls);
		Object[][] obj = BotUtil.genSQLBatchParam(fixObjList, urlObjList);
		return queryRunner.batch(UPDATE_STATUS_BATCH, obj);
	}
	
	public int update(STATUS status, STATE state, int merchantId, String url) throws SQLException {
		return queryRunner.update(UPDATE_STATUS_STATE, status.toString(), state.toString(), merchantId, url); 
	}
	
	public int insertProductUrl(ProductUrlBean productUrlBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				productUrlBean.getMerchantId(), 
				productUrlBean.getUrl(), 
				productUrlBean.getState(), 
				productUrlBean.getStatus(), 
				productUrlBean.getCategoryId(), 
				productUrlBean.getKeyword()}); 
	}
}
