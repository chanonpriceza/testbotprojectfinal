package db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.SendDataBean;
import utils.FilterUtil;

public class SendDataDB {		
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
		
	private QueryRunner queryRunner;
	private ScalarHandler<Long> numRecordHandler;
	private static final String INSERT = "INSERT INTO tbl_send_data" +
			"(action, merchantId, categoryId, name, price, url, description, pictureData, pictureUrl, " +
			" merchantUpdateDate, updateDate, realProductId, keyword, basePrice, upc, dynamicField) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String DELETE = "DELETE FROM tbl_send_data WHERE merchantId = ?";
	
	public SendDataDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
		numRecordHandler = new ScalarHandler<>();
	}
	
	public long insertSendData(SendDataBean sendDataBean) throws SQLException {
		return queryRunner.insert(INSERT, numRecordHandler, new Object[] {
				sendDataBean.getAction(),
				sendDataBean.getMerchantId(),
				sendDataBean.getCategoryId(),
				FilterUtil.limitString(sendDataBean.getName(), NAME_LENGTH),
				sendDataBean.getPrice(),
				sendDataBean.getUrl(),
				FilterUtil.limitString(sendDataBean.getDescription(), DESCRIPTION_LENGTH), 
				sendDataBean.getPictureData(), 
				sendDataBean.getPictureUrl(),				
				sendDataBean.getMerchantUpdateDate(),
				sendDataBean.getUpdateDate(),
				sendDataBean.getRealProductId(),
				sendDataBean.getKeyword(),
				sendDataBean.getBasePrice(),
				sendDataBean.getUpc(),
				sendDataBean.getDynamicField()
		}); 		
	}
		
	public int clearSendData(int merchantId) throws SQLException {
		return queryRunner.update(DELETE, merchantId); 
	}
}
