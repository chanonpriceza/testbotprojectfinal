package db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

import bean.ProductMappingBean;

public class ProductMappingDB {
		
	private ResultSetHandler<ProductMappingBean> beanHandler;
	
	private final String GET_BY_PRODID = "SELECT * FROM tbl_product_mapping WHERE prodId = ?";
	private final String DELETE = "DELETE FROM tbl_product_mapping WHERE merchantId = ? AND botId = ?";
	private final String DELETE_BY_PRODID = "DELETE FROM tbl_product_mapping WHERE prodId = ?";
	private final String INSERT = "INSERT INTO tbl_product_mapping (merchantId, botId, prodId) VALUES (?, ?, ?)";
	private final String UPDATE = "UPDATE tbl_product_mapping SET botId = ? WHERE prodId = ?";
	private QueryRunner queryRunner;
	
	public ProductMappingDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ProductMappingBean>(ProductMappingBean.class);
	}
	
	public ProductMappingBean getByProdId(int prodId) throws SQLException {
		return queryRunner.query(GET_BY_PRODID, beanHandler, prodId);
	}
	
	public int delete(int merchantId, int botId) throws SQLException {
		return queryRunner.update(DELETE, merchantId, botId);
	}
	
	public int deleteByProdId(int prodId) throws SQLException {
		return queryRunner.update(DELETE_BY_PRODID, prodId);
	}
	
	public int insert(ProductMappingBean b) throws SQLException {
		return queryRunner.update(INSERT, b.getMerchantId(), b.getBotId(), b.getProdId());
	}
	
	public int update(int botId, int prodId) throws SQLException {
		return queryRunner.update(UPDATE, botId, prodId);
	}
}
