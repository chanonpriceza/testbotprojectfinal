package db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

import bean.ConfigBean;

public class ConfigDB {
	
	private QueryRunner queryRunner;
	private ResultSetHandler<ConfigBean> beanHandler;
	
	private static final String GET_CONFIG_BY_NAME = "SELECT * FROM tbl_config WHERE configName = ?";

	public ConfigDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ConfigBean>(ConfigBean.class);
	}
	
	public ConfigBean getByName(String configName) throws SQLException {
		return queryRunner.query(GET_CONFIG_BY_NAME, beanHandler, configName);
	}
}
