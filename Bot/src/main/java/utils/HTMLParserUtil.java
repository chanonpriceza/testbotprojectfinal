package utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;

public class HTMLParserUtil {
	
		
	public static String[] getTagList(String html, String patternTag) {
		
		if(html == null) {
			return null;
		}		
		if(patternTag == null || patternTag.trim().length() == 0) {
			return new String[0];
		}
		ArrayList<String> arr = new ArrayList<String>();
		StringBuilder tmpStart = new StringBuilder();
		StringBuilder tmpEnd = new StringBuilder();
		StringBuilder tmpInner = new StringBuilder();
		String tmpStartTag = null;
		String tmpEndTag = null;
		String startTag = null;
		LinkedList<String> innerMatch = new LinkedList<String>();
		
		for (int i = 0; i < html.length(); i++) {
			
			char c = html.charAt(i);	
			//find start tag	
			if(startTag == null) {
				if(c == '<') {
					if(tmpStart.length() != 0) {
						tmpStart.delete(0, tmpStart.length());
					}
					tmpStart.append(c);	
					continue;
				} else if (c == '>') {
					tmpStart.append(c);
					tmpStartTag = tmpStart.toString();	
					tmpStart.delete(0, tmpStart.length());
				} else if(tmpStart.length() != 0){
					tmpStart.append(c);	
					continue;
				} else {
					continue;
				}
				if(tmpStartTag.startsWith("<!") || tmpStartTag.startsWith("</")) {
					tmpStartTag = null;
					continue;
				} 				
				if(isMatch(tmpStartTag, patternTag)) {
					startTag = tmpStartTag;	
				}
				continue;
			}
			
			//find end tag		
			if(c == '<') {
				if(tmpEnd.length() != 0) {
					tmpEnd.delete(0, tmpEnd.length());
				}
				tmpEnd.append(c);	
				continue;
			} else if (c == '>') {
				tmpEnd.append(c);
				tmpEndTag = tmpEnd.toString();
				tmpEnd.delete(0, tmpEnd.length());
			} else if(tmpEnd.length() != 0){
				tmpEnd.append(c);
				continue;
			} else {
				tmpInner.append(c);
				continue;
			}
			
			if(tmpEndTag.startsWith("<!")) { 
				tmpInner.append(tmpEndTag);	
				continue;
			} else if(tmpEndTag.startsWith("</")) {	
				boolean nextloop = false;
				for (int j = (innerMatch.size()-1); j >= 0; j--) {					
					String lastInner = innerMatch.get(j);
					if(lastInner.equals(getTagName(tmpEndTag))) {
						innerMatch.remove(j);						
						tmpInner.append(tmpEndTag);	
						nextloop = true;
						break;
					} else {
						innerMatch.remove(j);					
						tmpInner.append(tmpEndTag);
						continue;
					}
				}		
				if(nextloop){
					continue;
				}				
			} else if(tmpEndTag.endsWith("/>")) {
				//found inner start and end tag
				tmpInner.append(tmpEndTag);	
				continue;	
			} else if(tmpEndTag.startsWith("<")) {
				//found inner start tag
				innerMatch.add(getTagName(tmpEndTag));
				tmpInner.append(tmpEndTag);	
				continue;				
			}
			
			String endTag = tmpEndTag;
			String endTagTarget = "</" + getTagName(startTag) + ">";
			
			if(!endTag.equals(endTagTarget)) {
				tmpInner.append(tmpEndTag);	
				continue;
			}
			
			arr.add(startTag + tmpInner.toString() + endTag);
			startTag = null;			
			endTag = null;
			tmpStartTag = null;	
			tmpEndTag = null;	
			tmpInner.delete(0, tmpInner.length());
		}
		
		return (String[])arr.toArray(new String[0]);
	}
	
	private static String getTagName(String tag) {
		if(tag == null) {
			return null;
		}		
		if(tag.startsWith("</")) {
			return tag.substring(2, tag.length() -1);
		}
		int index = tag.indexOf(" ");
		if(index > 1) {
			return tag.substring(1, index);
		} 
		return tag.substring(1, tag.length() -1);
	}
	
	private static boolean isMatch(String tag, String pattern) {		
		return Pattern.matches(pattern,tag);
	}
}