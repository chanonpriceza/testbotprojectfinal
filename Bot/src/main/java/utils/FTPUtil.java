package utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

import org.apache.commons.codec.binary.Base64;

public class FTPUtil {
	
	public static List<String> sftpGetFileNameList(String sourcePath, String hostname, int port, String username,String password) throws IOException {
		SSHClient ssh = new SSHClient();
		SFTPClient sftp  = null;
		List<String> result = new ArrayList<>();
		
		try {
			ssh.addHostKeyVerifier(new PromiscuousVerifier());
			ssh.connect(hostname, port);
			ssh.authPassword(username, password);
			sftp = ssh.newSFTPClient();
			List<RemoteResourceInfo> files = sftp.ls(sourcePath);
			if(files != null && files.size() > 0) {
				for (RemoteResourceInfo remoteResourceInfo : files) {
					result.add(remoteResourceInfo.getName());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();;
		} finally {
			ssh.close();
			sftp.close();
			ssh.disconnect();
		}
		return result;
	}
	
	public static List<String> sftpGetDowloadFile(String sourcePath, String hostname, int port, String username,String password,String destinationName) throws IOException {
		SSHClient ssh = new SSHClient();
		SFTPClient sftp  = null;
		List<String> result = new ArrayList<>();
		
		try {
			ssh.addHostKeyVerifier(new PromiscuousVerifier());
			ssh.connect(hostname, port);
			ssh.authPassword(username, password);
			sftp = ssh.newSFTPClient();
			sftp.get(sourcePath,destinationName);	
			
		} catch (Exception e) {
			e.printStackTrace();;
		} finally {
			ssh.close();
			sftp.close();
			ssh.disconnect();
		}
		return result;
	}
	
	
	public static boolean downloadFileWithLogin(String sourceUrl, String destination, String username, String password) {
		try {
			URL url = new URL(sourceUrl);
			URLConnection uc = url.openConnection();
			String userpass = username + ":" + password;
			String basicAuth = "Basic " + new String(Base64.encodeBase64(userpass.getBytes()));
			uc.setRequestProperty ("Authorization", basicAuth);
			try (InputStream input = uc.getInputStream()){
				Path output = new File(destination).toPath();
				Files.copy(input, output, StandardCopyOption.REPLACE_EXISTING);
				return true;
			}catch(Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
}
