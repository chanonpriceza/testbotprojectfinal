package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class DateTimeUtil {

	public static final TimeZone TIMEZONE = TimeZone.getTimeZone("GMT");
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String MONTH_YEAR_FORMAT = "yyyy-MM";
	public static final String DISPLAY_DATE_FORMAT = "dd MMM yyyy";
	public static final String DISPLAY_MONTH_FORMAT = "MMM yyyy";
	
	public static final String DATETIME_FORMAT_EMAIL = "yyyy-MMM-dd,HH:mm:ss";
	public static final String DATETIME_ZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String DISPLAY_DATETIME_FORMAT = "d MMM yy HH:mm:ss";
	
	public static final String DISPLAY_SIMPLE_FORMAT = "d MMM yy";	
	public static final Locale THAI_LOCAL = new Locale("th","TH");
	
	private static final String DATE_PATTERN = "((?:19|20|21)\\d\\d)-(0?[1-9]|1[012])-([12][0-9]|3[01]|0?[1-9])";  //yyyy-mm-dd
	
	 private static Pattern pattern;
	 private static Matcher matcher;
	
	public static String generateStringDateTimeGMT(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DATETIME_FORMAT);
		sf.setTimeZone(TIMEZONE);
		return sf.format(date);
	}
		
	public static String generateStringDateTime(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DATETIME_FORMAT);
		return sf.format(date);
	}
	
	public static String generateStringDisplayDateTime(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DISPLAY_DATETIME_FORMAT);
		return sf.format(date);
	}
	
	public static String generateStringDate(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DATE_FORMAT);
		return sf.format(date);
	}
	
	public static String generateStringDateTimeZone(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DATETIME_ZONE_FORMAT);
		return sf.format(date);
	}
	
	public static String generateStringDate(Date date, String format) {
		SimpleDateFormat sf = new SimpleDateFormat(format);
		return sf.format(date);
	}
	
	public static String generateStringDate(Date date, String format, Locale Locale) {
		SimpleDateFormat sf = new SimpleDateFormat(format, Locale);
		return sf.format(date);
	}
	
	public static Date generateDateDayOffset(Date date, int dayOffset) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, dayOffset);
		return c.getTime();
	}
	
	public static Date generateDateMonthOffset(Date date, int monthOffset) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, monthOffset);
		return c.getTime();
	}
	
	public static Date generateDateHourOffset(Date date, int hourOffset) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, hourOffset);
		return c.getTime();
	}
	
	public static Date generateDateSecOffset(Date date, int secOffset) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.SECOND, secOffset);
		return c.getTime();
	}
	
	public static Date generateDate(String strDate) throws ParseException {
		SimpleDateFormat sf = new SimpleDateFormat(DATETIME_FORMAT);
		sf.setLenient(false);
		return sf.parse(strDate);
	}
	
	public static Date generateDate(String strDate, String format) throws ParseException {
		SimpleDateFormat sf = new SimpleDateFormat(format);
		sf.setLenient(false);
		return sf.parse(strDate);
	}
	
	public static String generateStringDisplayDate(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(DISPLAY_DATE_FORMAT);
		return sf.format(date);
	}
	
	public static Date generateDateDefaultNull(String strDate, String format){
		try{
			if(StringUtils.isBlank(strDate))
				return null;
			SimpleDateFormat sf = new SimpleDateFormat(format);
			sf.setLenient(false);
			return sf.parse(strDate);
		}catch (ParseException e) {
			return null;
		}
	}
	
	public static Date clearTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		return c.getTime();
	}
	
	public static int getDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);		
		return c.get(Calendar.DAY_OF_WEEK);
	}
	
	public static int getDayOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);		
		return c.get(Calendar.DAY_OF_MONTH);
	}
	
	public static int getHourOfDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);		
		return c.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getHourOfDay() {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(System.currentTimeMillis());		
		return c.get(Calendar.HOUR_OF_DAY);
	}
	
	public static Date getFirstDayOfMonth(int month, int year) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.YEAR, year);
		c.set(Calendar.DAY_OF_MONTH, 1);
		return c.getTime();
	}
	
	public static Date getLastDayOfMonth(int month, int year) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.YEAR, year);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}
		
	public static String calcurateDiff(Date d1, Date d2) {
		String result = "";
		long diffTime =  d2.getTime() - d1.getTime();	
		double difDay = (int)(diffTime/(24 * 60 * 60 * 1000));
		int day = (int)Math.floor(difDay);
		if(day > 0){ result = result + String.valueOf(day) + "d ";}
		
		double difHour = (double)diffTime/(double)(1000*60*60);		
		int hour = (int) Math.floor(difHour);
		hour = hour - (day*24);
		
		int minute = (int) Math.round((difHour % 1) * 60);
		if(hour > 0){ result = result + String.valueOf(hour) + "h ";}
		if(minute <= 0){ minute = 1;}
		result = result +String.valueOf(minute) + "m";
		
		return result;
	}
	
	public static int calcurateDayDiff(Date d1, Date d2) {
		
		long diffTime =  d2.getTime() - d1.getTime();		
		return (int)(diffTime/(24 * 60 * 60 * 1000));
	}
	
	public static String calcurateHourDiff(Date d1, Date d2) {
		String result = "";
		long diffTime =  d2.getTime() - d1.getTime();
		double difHour = (double)diffTime/(double)(1000*60*60);
		int hour = (int) Math.floor(difHour);
		int minute = (int) Math.round((difHour % 1) * 60);
		if(hour > 0){ result = result + String.valueOf(hour) + "h ";}
		if(minute <= 0){ minute = 1;}
		result = result +String.valueOf(minute) + "m";
		return result;
	}
	
	public static String calcurateHourDiffFullFormat(Date d1, Date d2) {
		String result = "";
		long diffTime =  d2.getTime() - d1.getTime();
		double difHour = (double)diffTime/(double)(1000*60*60);
		int hour = (int) Math.floor(difHour);
		int minute = (int) Math.round((difHour % 1) * 60);
		result = result + String.valueOf(hour) + "h ";
		if(minute <= 0){ minute = 1;}
		if(minute < 0) {result = result + "0";}
		result = result +String.valueOf(minute) + "m";					
		return result;
	}
	
	public static int calcurateSecondDiff(Date d1, Date d2) {		
		long diffTime =  d2.getTime() - d1.getTime();		
		return (int)(diffTime/1000);
	}
	
	public static boolean checkpatternDate(String date) {
		pattern = Pattern.compile(DATE_PATTERN);
	    matcher = pattern.matcher(date);
	
	    if (matcher.matches()) {
	    	matcher.reset();
	    	if (matcher.find()) {
	    		int year = Integer.parseInt(matcher.group(1));
	    		String month = matcher.group(2);
	    		String day = matcher.group(3);
		     
	    		if (day.equals("31") && (month.equals("4") || month .equals("6") || month.equals("9") ||  month.equals("11") || month.equals("04") || month .equals("06") || month.equals("09"))) {
	    			return false; // only 1,3,5,7,8,10,12 has 31 days
	    		} else if (month.equals("2") || month.equals("02")) {
	                 //leap year
	    			if(year % 4==0) {
	    				if (day.equals("30") || day.equals("31")) {
	    					return false;
	    				} else {
	    					return true;
	    				}
	    			} else {
	    				if (day.equals("29")||day.equals("30")||day.equals("31")) {
	    					return false;
	    				} else {
			        	 return true;
	    				}
	    			}
	    		} else {				 
		    	  return true;				 
		      }
		   } else {
	   	      return false;
		   }	  
	    } else {
		  return false;
	    }			    
	}
	
	public static String calculateWorkDayNoHoliday() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int dateCount = 0;
		while(dateCount < 5) {
			if(calendar.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY&&calendar.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY) {
				dateCount++;
			}
			calendar.add(Calendar.DATE, 1);
		}
		return DateTimeUtil.generateStringDate(calendar.getTime());
	}
}
