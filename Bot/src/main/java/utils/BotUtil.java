package utils;

import static org.apache.commons.lang3.StringUtils.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FilenameUtils;

public class BotUtil {
	public final static DecimalFormat FORMAT_PRICE = new DecimalFormat("########.##");
	public static String LOCALE;
	public static final int CONTACT_PRICE = getContactPrice();
	public static final String CONTACT_PRICE_STR = String.valueOf(getContactPrice());
	public static final int NO_PRICE = getNoPrice();
	public static final String NO_PRICE_STR = String.valueOf(getNoPrice());
	public static final int BID_PRICE = getBidPrice();
	public static final String BID_PRICE_STR = String.valueOf(getBidPrice());
	public static final int SPECIAL_PRICE = getSpecialPrice();
	public static final String SPECIAL_PRICE_STR = String.valueOf(getSpecialPrice());
	public static final int CREDIT_BASE_PRICE = getCreditBasePrice();
	
	private static int getContactPrice(){
		return isBlank(LOCALE) || LOCALE.equalsIgnoreCase("TH") ? 9999999 : 2009999999;
	}
	
	private static int getNoPrice(){
		return isBlank(LOCALE) || LOCALE.equalsIgnoreCase("TH") ? 9999998 : 2009999998;
	}
	
	private static int getBidPrice(){
		return isBlank(LOCALE) || LOCALE.equalsIgnoreCase("TH") ? 9999997 : 2009999997;
	}
	
	private static int getSpecialPrice(){
		return isBlank(LOCALE) || LOCALE.equalsIgnoreCase("TH") ? 9999996 : 2009999996;
	}
	
	private static int getCreditBasePrice(){
		return isBlank(LOCALE) || LOCALE.equalsIgnoreCase("TH") ? 8000000 : 2008000000;
	}
	
	public static boolean checkStringEqual(String s1, String s2) {
		if (s1 == null && s2 == null)
			return true;
		return s1 != null && s1.equals(s2);
	}
	
	public static boolean checkStringEqual(String s1, String s2, int limit) {
    	if(s1 == null && s2 == null)
    		return true;
    	s1 = limitString(s1, limit);
    	s2 = limitString(s2, limit);
    	return s1 != null && s1.equals(s2);
    }
	
	public static String limitString(String s, int length) {
		if(s == null)
			return null;
		if(s.length() > length)
			return s.substring(0, length);
		return s;		
	}
	
	public static int calPercentDiff(double  d1, double d2) {		
		return (int)Math.ceil( (d2 - d1)* 100 / d1 );
	}
	
	public static String encodeURL(String url) {
    	if(url == null)
    		return null;
    	    	
		StringBuilder tmp = new StringBuilder();
		char cc[] = url.toCharArray();
	    for (int i = 0; i < cc.length; i++) {
	        int eachCharAscii = (int) cc[i];
	        if (3585 <= eachCharAscii && eachCharAscii <= 3675) {
	        	try {
					tmp.append(URLEncoder.encode(String.valueOf(cc[i]), "UTF-8"));
				} catch (UnsupportedEncodingException e) {}
            } else if(eachCharAscii == 32) {
            	tmp.append("%20");
            } else {
            	tmp.append(cc[i]);
            }
	    }
		return tmp.toString();
    }
	
	public static String decodeURL(String url) {	
		if(url == null) {
			return url;
		}
		try {
			return URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return url;
		}		
	}
	
	public static String removeLongSpace(String s){
        if(s == null)
            return null;
        return s.replaceAll("\\s+", " ").trim();
    }
	
	public static double convertStringToDouble(String s, double defaultValue) {
		if (s == null)
			return defaultValue;
		try {
			return Double.parseDouble(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static int stringToInt(String s, int defaultValue) {
		if (s == null)
			return defaultValue;
		try {
			return Integer.parseInt(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public static boolean defaultBoolean(String value, boolean defaultBoolean) {
		if(value == null)
			return defaultBoolean;			
		try {
			return Boolean.parseBoolean(value);
		} catch (Exception e) {
			return defaultBoolean;
		}
	}
	
	public static List<Integer> longToInt(List<Long> list){
		return list.stream().map(v -> v.intValue()).collect(Collectors.toList());
	}
	
	public static String genSQLParam(int size) {
		StringBuilder buf = new StringBuilder(size * 2);
		for (int i = 0; i < size; i++)
			buf.append(",?");
		if (buf.length() > 0)
			buf.deleteCharAt(0);
		return buf.toString();
	}

	@SafeVarargs
	public static Object[][] genSQLBatchParam(List<Object> fixList, List<Object>... objListSet){
		Object[][] resultObj = null;
		int dimention1Size = 0, dimention2Size = 0;
		int fixListSize = 0, eachListSize = 0;
		
		if(fixList != null && fixList.size() > 0) {
			dimention1Size += fixList.size();
			fixListSize = fixList.size();
		}
		if(objListSet != null && objListSet.length > 0) {
			dimention1Size += objListSet.length;
			for(List<Object> objList : objListSet) {
				if(eachListSize == 0) {
					eachListSize = objList.size();
					dimention2Size += objList.size();
					resultObj = new Object[dimention2Size][dimention1Size];
				}else {
					if(eachListSize != objList.size()) return null; 
				}
			}
		}
		for(int i=0; i<dimention2Size; i++) {
			for(int j=0; j<dimention1Size; j++) {
				if(j<fixListSize)
					resultObj[i][j] = fixList.get(j);
				else
					resultObj[i][j] = objListSet[j-fixListSize].get(i);
			}
		}
		return resultObj;
	}
	
	public static boolean isGZip(File file) {
		int magic = 0;
		try (RandomAccessFile raf = new RandomAccessFile(file, "r")){
			magic = raf.read() & 0xff | ((raf.read() << 8) & 0xff00);
		} catch (Exception e) {
			return false;
		}
		return magic == GZIPInputStream.GZIP_MAGIC;
	}
	
	public static String getFileNameURL(String url) {
		try {
			return FilenameUtils.getName(new URL(url).getPath());
		} catch (MalformedURLException e) {
			return "";
		}
	}
	
	public static String getFileNameDirectory(String path) {
		return new File(path).getName();
	}
	
	public static String removeNonUnicodeBMP(String str) {
		if(str == null)
			return null;
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();
		while(offset < length) {
			int codepoint = str.codePointAt(offset);
			if(codepoint < 65536)
				bmpString.append(str.charAt(offset));
			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
    public static String convertInvalidChar(String s, int newChar){

    	if (s == null)
        {
            return null;
        }

        char[] charArray = s.toCharArray();
        StringBuilder convertedBufferString = new StringBuilder();

           for (int i = 0; i < charArray.length; i++)
           {
               int eachCharAscii = (int) charArray[i];               
               if(eachCharAscii > 3675) {
            	   eachCharAscii = newChar;            	   
               }               
               convertedBufferString.append((char) eachCharAscii);
           }

        return convertedBufferString.toString();
    }
    
}
