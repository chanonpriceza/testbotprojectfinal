package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import web.crawler.bot.SocketFactory;

public class HTTPUtil {
	
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
	private final static int DEFAULT_TIMEOUT = 10000;
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	private final static Proxy PROXY_HTTP = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_DOMAIN, PROXY_PORT));
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		SocketFactory.setProxyHost(PROXY_DOMAIN);
		SocketFactory.setProxyPort(PROXY_PORT);
		SocketFactory.setProxyUID(PROXY_USER);
		SocketFactory.setProxyPWD(PROXY_PASSWORD);
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	public static void disableUseProxy() {
		USE_PROXY = false;
		SocketFactory.setProxyHost(null);
		CLIENT_BUILDER = HttpClients.custom();
	}
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
	public static String[] httpRequestWithStatusIgnoreCookies(String url, String charset, boolean redirectEnable) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", USER_AGENT);
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while((line = brd.readLine()) != null)
    					rtn.append(line);
    				return new String[]{rtn.toString(), String.valueOf(status)};
    			}
    		}else{
				return new String[]{null, String.valueOf(status)};
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpGet != null)
				httpGet.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    }
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) (USE_PROXY ? u.openConnection(PROXY_HTTP) : u.openConnection());
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(DEFAULT_TIMEOUT);
			conn.setReadTimeout(DEFAULT_TIMEOUT);
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
    			BufferedReader brd = new BufferedReader(isr);) {
	    		
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
    }
	
	public static String[] httpPostRequestWithStatus(String url, String data, String charset, boolean redirectEnable, boolean isJsonRequestParam) {
		HttpURLConnection conn = null;
		boolean success = false;
		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) (USE_PROXY ? u.openConnection(PROXY_HTTP) : u.openConnection());
			conn.setInstanceFollowRedirects(redirectEnable);			
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setConnectTimeout(DEFAULT_TIMEOUT);
			conn.setReadTimeout(DEFAULT_TIMEOUT);
			
			conn.setRequestMethod("POST");
			if(isJsonRequestParam) {
				conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				conn.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");				
			} else {
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("Accept", "*/*");				
			}
			conn.setRequestProperty("Content-Length", "" + data.length());
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return new String[]{null, String.valueOf(status)};
			
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
			
				StringBuilder rtn = new StringBuilder();
				String line = null;
				while ((line = brd.readLine()) != null)
					rtn.append(line);
				return new String[]{rtn.toString(), String.valueOf(status)};
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return null;
	}
	
	public static void comsume(HttpEntity entity) {
		EntityUtils.consumeQuietly(entity);
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
}
