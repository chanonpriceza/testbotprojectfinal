package utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.htmlparser.Parser;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

public class FilterUtil {

    private static final String PRICE_CHAR = "0123456789.";
    private static final String PRICE_CHAR_XXX = "0123456789.Xx,";
    private static final Pattern EMOJI_AND_SYMBOL_REGEX_PATTERN = Pattern.compile("([^\\p{L}\\p{N}\\p{IsThai}]|[๛๏])+");
    
    private static final Map<String, Map<Integer, Integer>> languageCharacterMap = createLanguageCharacterMap();
    private static Map<String, Map<Integer, Integer>> createLanguageCharacterMap(){
    	Map<String, Map<Integer, Integer>> maps = new HashMap<>();
    	maps.put("th", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0E00, 0x0E5B);
			}
		});
    	maps.put("eng", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0041, 0x005A);
				put(0x0061, 0x007A);
				put(0x0030, 0x0039);
			}
		});
		maps.put("viet", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0xc0, 0xc3);
				put(0xc8, 0xca);
				put(0xcc, 0xcd);
				put(0xd2, 0xd5);
				put(0xd9, 0xda);
				put(0xdd, 0xdd);
				put(0xe0, 0xe3);
				put(0xe8, 0xea);
				put(0xec, 0xed);
				put(0xf2, 0xf5);
				put(0xf9, 0xfa);
				put(0xfd, 0xfd);
				put(0x102, 0x103);
				put(0x110, 0x111);
				put(0x128, 0x129);
				put(0x168, 0x169);
				put(0x1a0, 0x1a1);
				put(0x1af, 0x1b0);
				put(0x1ea0, 0x1ef9);
			}
		});
		maps.put("chinese", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x4E00, 0x9FD5);
			}
		});
    	return maps;
    }
    
    // remove all string is not "number" or "," or "."
    public static String removeCharNotPrice(String price){

        if(price == null || price.equals("")) {
        	return price;
        }

        StringBuilder rtn = new StringBuilder();
        char charArray[] = price.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            if(PRICE_CHAR.indexOf((int)charArray[i]) != -1){
                rtn.append(charArray[i]);
            }
        }
        return removeSuffixCharNotPrice(rtn.toString());
    }
    
    public static String removeSuffixCharNotPrice(String price){
    
    	if(price == null || price.equals("")) return price;
    	
    	if(( price.charAt(price.length()-1) == ',' ) || 
    		( price.charAt(price.length()-1) == '.' )){
    		return removeSuffixCharNotPrice(price.substring(0, price.length()-1));
    	}
    	return price;    
    }
    
    public static String[] replaceURLSpace(String[] s){

        if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null) {
                continue;
            }            
            s[i] = s[i].trim().replace(" ", "%20");
        }
        return s;
    }
    
    public static String[] removeSpace(String[] s){

        if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null)
                continue;
            s[i] = s[i].replaceAll("\\s+", " ").trim();
        }
        return s;
    }
    
    public static String removeSpace(String s){
        if(s == null) return null;
        return s.replaceAll("\\s+", " ").trim();
    }
    
    public static String removeNonData(String value){
		if (value == null){
			return null;
		}

		char[] charArray = value.toCharArray();
		StringBuilder convertedBufferString = new StringBuilder();
		for (int i = 0; i < charArray.length; i++){
			int eachCharAscii = (int) charArray[i];
			if(eachCharAscii < 32 || eachCharAscii == 127) {
				convertedBufferString.append(" ");
				continue;
			}
			convertedBufferString.append((char) eachCharAscii);
		}
		return convertedBufferString.toString().trim();
	}
    
    private static final String HTML_CODE[][] = {
    	{"&nbsp;"," "},
    	{"&amp;","&"},
    	{"&bull;",""}
    };    
    
    public static String replaceHtmlCode(String s){    	
    	if(s == null) return null;        
    	
    	for (int i = 0; i < HTML_CODE.length; i++) {
           s = s.replace(HTML_CODE[i][0], HTML_CODE[i][1]);
        }    	
    	return s;    
    }
    
    public static String[] replaceHtmlCode(String[] s){
        if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null)
                continue;
            s[i] = replaceHtmlCode(s[i]);
        }
        return s;
    }
    
    public static String[] replaceAll(String[] s, String oldStr, String newStr){
        if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null)
                continue;
            s[i] = s[i].replaceAll(oldStr, newStr);
        }
        return s;
    }
    
    private static final String HTML_TAG[][] = {
    	{"//<![CDATA[","//]]>"},
    	{"<!--","-->"}
    };
    
    public static String removeHtmlTag(String s){    	
    	if(s == null) return null;        
    	
    	for (int i = 0; i < HTML_TAG.length; i++) {    		
    		while(!Thread.currentThread().isInterrupted()) {
	    		int openTag = s.indexOf(HTML_TAG[i][0]);
	    		int closeTag = s.indexOf(HTML_TAG[i][1]);
	    		if(openTag != -1 && closeTag != -1 && openTag < closeTag) {
	    			String startText = s.substring(0, openTag);
	    			String endText = s.substring(closeTag + HTML_TAG[i][1].length(), s.length());
	    			s = startText + endText;
	    		}else{
	    			break;
	    		}
    		}    		
        }    	
    	return s;    
    }
    public static String[] removeHtmlTag(String[] s){
        if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null)
                continue;
            s[i] = removeHtmlTag(s[i]);
        }
        return s;
    }
    
    public static String sum2Str(String s1, String s2) {

        double d1;
        double d2;

        if(s1 == null || s2 == null) return null;

        s1 = s1.replaceAll(",","");
        s2 = s2.replaceAll(",","");
        try {d1 = Double.parseDouble(s1);} catch (Exception e) {return null;}
        try {d2 = Double.parseDouble(s2);} catch (Exception e) {return null;}


        DecimalFormat df = new DecimalFormat("###,##0.00");
        String result = df.format(d1+d2);
        return result;

    }
   
    public static double convertPriceStr(String price) {    	    	
    	try {
			double rtn = Double.parseDouble(price);
			return rtn;
		} catch (Exception e) {
			return 0;
		}    	
    }
    
    public static DecimalFormat priceFormat = new DecimalFormat("0.00");
        
    public static double convertPriceXXXStr(String price) {    	    	
    	try {
    		
    		char charArray[] = price.toCharArray();
    		int count = 0;
    		boolean done = false;
            for (int i = 0; i < charArray.length; i++) {
            	if(charArray[i] == ',') {
            		continue;
            	}
            	
                if(charArray[i] == 'X' || charArray[i] == 'x'){
                	if(done) {
                		//case 10X0X
                		return 0;
                	}                	
                	count++;
                } else {
                	if(count > 0) {
                		done = true;
                	}
                }
            }    		
            price = price.replace('X', '9');
        	price = price.replace('x', '9');
        	price = removeCharNotPrice(price);
            double rtn = Double.parseDouble(price);
			
            if(count > 0) {            	
            	rtn = rtn + ((double)count/10000d);            	
            }
    		
            return rtn;
		} catch (Exception e) {
			return 0;
		}    	
    }
    
    public static boolean isPriceXXX(String price) {    	    	
    	
    	if(price == null || price.trim().length() == 0) {
    		return false;
    	}    	
    	char charArray[] = price.toCharArray();
    	boolean foundX = false;
        for (int i = 0; i < charArray.length; i++) {
            if(PRICE_CHAR_XXX.indexOf((int)charArray[i]) == -1){
                return false;
            }
            
            if(charArray[i] == 'X' || charArray[i] == 'x') {
            	foundX = true;
            }
        }
    	return foundX;   	
    }
  
	public static Date parseDateString(String stringdate, String format) {
		
		SimpleDateFormat sf = new SimpleDateFormat(format);
		try {
			return sf.parse(stringdate);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static Date convertThaiToEngDate(Date date) {
		
		if(date == null) {
			return null;
		}
				
		Calendar c = Calendar.getInstance();
		c.setTime(date);		
		int year = c.get(Calendar.YEAR);
		if(year > 2500) {
			year = year - 543;
			c.set(Calendar.YEAR, year);
		}
		
		return c.getTime();		
	}
	
	public static String getStringBetween(String target, String begin, String end){
        if(target == null) return "";
        
        int index = target.indexOf(begin);
        if(index == -1) return "";        
        target = target.substring(index + begin.length());
        index = target.indexOf(end);
        if(index == -1) return "";
        target = target.substring(0, index);
               
        return target;
    }
	
	public static List<String> getAllStringBetween(String target, String begin, String end){
        
		if(target == null || begin.length() == 0 || end.length() == 0) {
			return null;
		}
        
		List<String> rtn = new ArrayList<String>();
        
		do {
			
			int index = target.indexOf(begin);
	        if(index == -1) {
	        	return rtn;        
	        }
	        target = target.substring(index + begin.length());
	        index = target.indexOf(end);
	        if(index == -1) {
	        	return rtn;
	        }
	        
	        String found = target.substring(0, index);
	        rtn.add(found);
	        
	        target = target.substring(index + end.length());			
		} while (target.trim().length() != 0);
		
        return rtn;
    }
		
	public static String getStringAfter(String target, String begin, String defaultValue){
        if(target == null) {
        	return "";
        }
        
        int index = target.indexOf(begin);
        if(index == -1) {
        	return defaultValue;        
        }
        target = target.substring(index + begin.length());
                       
        return target;
    }
	
	public static String getStringAfterLastIndex(String target, String begin, String defaultValue){
        if(target == null) {
        	return "";
        }
        
        int index = target.lastIndexOf(begin);
        if(index == -1) {
        	return defaultValue;        
        }
        target = target.substring(index + begin.length());
                       
        return target;
    }
	
	public static String getStringBefore(String target, String begin, String defaultValue){
        if(target == null) {
        	return "";
        }
        
        int index = target.indexOf(begin);
        if(index == -1) {
        	return defaultValue;        
        }
        target = target.substring(0, index);
                       
        return target;
    }
	
	public static String getStringBeforeLastIndex(String target, String begin, String defaultValue){
        if(target == null) {
        	return "";
        }
        
        int index = target.lastIndexOf(begin);
        if(index == -1) {
        	return defaultValue;        
        }
        target = target.substring(0, index);
                       
        return target;
    }
	
	public static String toPlainTextString(String html) {
		if(html == null) {
			return null;
		}
		if(html.equals("")) {
			return "";
		}
		try {
			Parser parser = new Parser();
			parser.setInputHTML(html);
			NodeList nl = parser.parse(null);
			StringBuilder tmp = new StringBuilder();
			for (int i = 0; i < nl.size(); i++) {				
				tmp.append(nl.elementAt(i).toPlainTextString());	
				tmp.append(" ");
			}
			return tmp.toString().trim();
		} catch (ParserException e) {
			
		}
		return html;
	}
	
	public static String formatHtml(String s){
        s = removeHtmlTag(s);
		s = replaceHtmlCode(s);
        return s;
    }
    
    public static String[] formatHtml(String[] s){
    	if(s == null) return null;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null)
                continue;
            s[i] = removeHtmlTag(s[i]);
            s[i] = replaceHtmlCode(s[i]);
        }
        return s;
    }
    
    public static String limitString(String s, int length) {
		if(s == null)
			return null;
		if(s.length() > length)
			return s.substring(0, length);
		return s;		
	}
    
	public static boolean isMatchKeyword(String keyword, String text) {
		
		if(text == null || text.length() == 0 || keyword == null || keyword.length() == 0) {
			return false;
		}
		
		String[] keywordArr = keyword.split(" ");
		String[] textArr = text.split(" ");
		
		for (int i = 0; i < keywordArr.length; i++) {
			
			String keywordTest = keywordArr[i].trim();
			if(keywordTest.length() == 0) {
				continue;
			}
			
			boolean found = false;
			
			for (int j = 0; j < textArr.length; j++) {
				
				String textTest = textArr[j].trim();
				if(textTest.length() == 0) {
					continue;
				}				
				if(keywordTest.equalsIgnoreCase(textTest)) {
					found = true;
					break;
				}				
			}
			
			if(!found) {
				return false;
			}			
		}
		
		return true;
	}
	
	public static String mergeUrl(String baseUrl, String relativePath) {
		
		if(baseUrl == null || relativePath == null) {
			return null;
		}		
		try {
			URL url = new URL(new URL(baseUrl), relativePath);
			return url.toString();
		} catch (MalformedURLException e) {
			
		}		
		return null;
	}
	
	public static String removeNonChar(String s){
		if (s == null){
			return null;
		}
		if(s.isEmpty()){
			return "";
		}
		char[] charArray = s.toCharArray();
		StringBuilder convertedBufferString = new StringBuilder();
		for (int i = 0; i < charArray.length; i++){
			int eachCharAscii = (int) charArray[i];
			if(eachCharAscii < 32 || eachCharAscii == 127 || charArray[i] == ' ') {
				convertedBufferString.append(" ");
				continue;
			}
			convertedBufferString.append((char) eachCharAscii);
		}
		return convertedBufferString.toString().trim();
	}
	
	public static String evaluate(String html, String pattern) {
		String[] data = HTMLParserUtil.getTagList(html, pattern);
		if(data != null && data.length > 0){
			return data[0];
		}
		return "";
	}
	
	public static String getAbsoluteURL(String path, String domain) {
		if(StringUtils.isBlank(path) || StringUtils.isBlank(domain)) {
			return "";
		}
		if(!path.startsWith("http")) {
			try {
				URL url = new URL(new URL(domain), path);
				path = url.toString();
	    	} catch (MalformedURLException e) {}
		}
		return path;
	}
	
	public static boolean checkAllowCharacter(String inputString, String country) {
		return true;
	}
	
	public static boolean checkContainOnlyCharacter(String inputString , String[] allowList) {
		for (char splitChar : inputString.toCharArray()) {
			if(Character.isLetter(splitChar)) {
				boolean isInAllowList = false;
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							isInAllowList = true;
						}
					}
				}
				if(!isInAllowList) {
					return false;
				}
			}else {
				continue;
			}
		}
		return true;
	}
	
	public static String removeRestrictCharacter(String inputString, String[] allowList) {
		StringBuilder build = new StringBuilder();
		for (char splitChar : inputString.toCharArray()) {
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							build.append(splitChar);
						}
					}
				}
		}
		return build.toString();
	}
	
	public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
		if ((i >= minValueInclusive && i <= maxValueInclusive))
			return true;
		else
			return false;
	}

	public static String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	public static String removeEmoji(String text) {
    	if (text == null)
    		return null;
    	
    	return EMOJI_AND_SYMBOL_REGEX_PATTERN.matcher(text).replaceAll(" ");
    }
	
}
