package engine;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;

import bean.ParserConfigBean;
import feed.TypeFeedCrawlerInterface.FEED_TYPE;
import web.parser.image.DefaultImageParser;
import web.parser.image.ImageParserInterface;

public class BaseConfig {
	public static Properties CONF_PROP;
	public final static String SESSION_ID = RandomStringUtils.random(10, true, true);
	public static String BOT_FAMILY;
	public static int MERCHANT_ID = -1000000;
	public static String MERCHANT_NAME;
	public static int MERCHANT_ACTIVE;
	public static int PACKAGE_TYPE;
	public static Date NEXT_START;
	public final static DecimalFormat FORMAT_PRICE = new DecimalFormat("########.##");
	public final static String FORMAT_DATE = "dd/MM/yyyy";
	public static Properties MERCHANT_CONF;
	public static String RUNTIME_TYPE;
	
	public static int CONF_BOT_QUALITY_HOUR_RATE = 24;
	public static int CONF_BOT_DUE_HOUR_RATE = 24;
	public static int CONF_BOT_WCE_HOUR_RATE = 24;
	public static int CONF_MAX_DEPTH = 5;
	public static String CONF_PARSER_CLASS;
	public static String CONF_INIT_CRAWLER_CLASS;
	public static String CONF_URL_CRAWLER_CLASS;
	public static String CONF_FEED_CRAWLER_CLASS;
	
	public static int CONF_MAX_RUNTIME = 24;
	public static int CONF_PRODUCT_UPADTE_LIMIT = 7;
	public static String CONF_URL_CRAWLER_CHARSET;
	public static int CONF_WCE_PARSER_THREAD_NUM = 1;
	
	public static int CONF_DUE_PARSER_THREAD_NUM = 1;
	public static boolean CONF_ENABLE_COOKIES;
	public static boolean CONF_IGNORE_HTTP_ERROR;
	public static boolean CONF_SKIP_ENCODE_URL;
	public static boolean CONF_FORCE_DELETE;
	
	public static boolean CONF_ENABLE_UPDATE_PIC;
	public static int CONF_DUE_SLEEP_TIME;
	public static int CONF_WCE_SLEEP_TIME;
	public static boolean CONF_USER_OLD_PRODUCT_URL;
	public static String CONF_IMAGE_PARSER_CLASS;
	
	public static boolean CONF_ENABLE_LARGE_IMAGE = true;
	public static boolean CONF_FORCE_UPDATE_IMAGE;
	public static boolean CONF_ENABLE_UPDATE_DESC;
	public static boolean CONF_ENABLE_SMALL_IMAGE;
	
	public static int CONF_UPDATE_PRICE_PERCENT;
	public static boolean CONF_ENABLE_VERY_LARGE_IMAGE;
	public static int CONF_DELETE_LIMIT = 200;
	public static boolean CONF_FORCE_UPDATE_KEYWORD;
	public static boolean CONF_FORCE_UPDATE_CATEGORY;
	public static boolean CONF_FORCE_UPDATE_LIMIT;
	
	
	public static boolean CONF_DISBLE_PARSER_COOKIES;
	public static boolean CONF_DISABLE_DUE_CONTINUE;
	public static String CONF_PARSER_CHARSET = "UTF-8";
	public static boolean CONF_ENABLE_UPDATE_URL;
	public static String CONF_FEED_CRAWLER_PARAM;
	
	
	public static String CONF_WCE_PARSER_CLASS;
	public static String CONF_DUE_PARSER_CLASS;
	public static boolean CONF_ENABLE_UPDATE_BASE_PRICE;
	public static boolean CONF_WCE_CLEAR_PRODUCT_URL;
	
	//public static String CONF_FEED_TYPE;
	//public static boolean CONF_FEED_CHECK_GZ;
	//public static String CONF_FEDD_URL;
	public static String CONF_FEED_USERNAME;
	public static String CONF_FEED_PASSWORD;
	public static String CONF_MERCHANT_NAME;
	public static double CONF_CURRENCY_RATE = 1.0;
	public static boolean CONF_ENABLE_PROXY;
	
	public static boolean CONF_IGNORE_NAME_CHANGE;
	
	public static String CONF_FEED_XML_OPEN_MEGA_TAG;
	public static String CONF_FEED_XML_CLOSE_MEGA_TAG;
	public static String CONF_FEED_CSV_FORMAT;
	public static int IMAGE_WIDTH = DefaultImageParser.DEFAULT_LARGE_WIDTH;
	public static int IMAGE_HEIGHT = DefaultImageParser.DEFAULT_LARGE_HEIGHT;
	public static ImageParserInterface IMAGE_PARSER = new DefaultImageParser();
	public static int THREAD_NUM = 1;
	
	public static List<ParserConfigBean> PARSER_CONFIG;
	
	public static FEED_TYPE FEED_TYPE_CONF;
	public static String FEED_STORE_PATH = "";
	public static String[] FEED_FILE = {};
	public static String FEED_PROCESSOR;
	public static String FEED_READING = "";
	
	public static String CONF_PARSER_REALTIME_CLASS;
	public static String CONF_PARSER_DETAIL_CLASS;
	
	public static boolean IS_DELETE_ALL;
	public static boolean IS_DELETE_NOT_MAP;
	
	public static boolean CONF_DISABLE_LOG;
	public static String CONF_FIX_CATEGORY;
	public static String CONF_FIX_KEYWORD;
	
}
