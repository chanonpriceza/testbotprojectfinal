package engine;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class BotFramework{
	
	public enum APPTYPE {
		WCE, WCE_TEST, WCE_MANUAL, DUE, DUE_TEST, DUE_MANUAL
	}
	
	public abstract boolean start();
	
	public BotFramework(String[] config) {		
		onInitialize(config);
	}
	
	public boolean onInitialize(String[] config) {
		if(config == null)
			return false;
		
		if(config.length == 0 || isBlank(config[0]))
			return false;
		
		BaseConfig.CONF_PROP = new Properties();		
		try (FileInputStream fis = new FileInputStream(config[0])){
			BaseConfig.CONF_PROP.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		if(config.length > 1 && !isBlank(config[1]))
			BaseConfig.BOT_FAMILY = config[1];
		
		return true;
	}
	
	public String getConfig(String key) {
		if(BaseConfig.CONF_PROP == null)
			return null;
		return BaseConfig.CONF_PROP.getProperty(key);
	}
}