package engine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ReportBean;
import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.UrlPatternBean;
import db.MerchantDB;
import db.ProductUrlDB;
import db.UrlPatternDB;
import db.WorkloadDB;
import db.manager.DatabaseManager;
import loader.DatabaseLoader;
import loader.MerchantConfigLoader;
import manager.CommandManager;
import manager.MerchantManager;
import manager.MonitoringManager;
import manager.ProcessLogManager;
import manager.ReportManager;
import unittest.MockObjectHolder;
import utils.BotUtil;
import utils.HTTPUtil;
import web.crawler.WebCrawlerProcessor;
import web.crawler.UrlCrawlerProcessor;

public class WCEApp extends BotFramework{
	
	public static void main(String[] args) {
		BotFramework app;
		try {
			app = new WCEApp(args);
			app.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public WCEApp(String[] config) {
		super(config);
	}

	@Override
	public boolean start() {
		Logger logger = null;
		String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
		AtomicBoolean success = new AtomicBoolean(true);
		ProcessLogManager processLog = null;
		boolean run = false;
		try {
			BotUtil.LOCALE = BaseConfig.CONF_PROP.getProperty("ServerCountry");
			BaseConfig.FEED_STORE_PATH = BaseConfig.CONF_PROP.getProperty("FeedStore");
			BaseConfig.RUNTIME_TYPE = "WCE";
			
			try {
				run = DatabaseLoader.loadDBConnection();
			} catch(Exception e) {
				success.set(false);
				e.printStackTrace();
			} finally {
				if(!run){
					DatabaseLoader.updateQueue();
					return success.get();
				}
			}
			
			logger = LogManager.getRootLogger();
			processLog = ProcessLogManager.getInstance();
			logger.info("----------------------------------------------");
			logger.info("Bot : Start : " + BaseConfig.MERCHANT_ID + " " + BaseConfig.MERCHANT_NAME);
			MerchantDB merchantDB = DatabaseManager.getInstance().getMerchantDB();
			merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.INITIAL, MerchantBean.STATUS.RUNNING);
			
			logger.info("Report : Generate");
			ReportManager reportManager = ReportManager.getInstance();
			reportManager.startReport();
			
			CommandManager commandManager = CommandManager.getInstance();
			commandManager.clearCommand(CommandManager.PRIMARY_WCE, true);
			
			DatabaseLoader.updateQueue();
			
			MerchantConfigLoader.loadMerchantConfig();
			if(!commandManager.checkCommand(CommandManager.DELETE_ALL)&&!commandManager.checkCommand(CommandManager.DELETE_NOT_MAP)) {
				BaseConfig.THREAD_NUM = BaseConfig.CONF_WCE_PARSER_THREAD_NUM;
				
				ExecutorService executor = Executors.newFixedThreadPool(2);
				try {
					
					Future<?> mainProcess = executor.submit(new Runnable() {
						public void run() {
							
							Logger threadLogger = LogManager.getRootLogger();
							ProcessLogManager processLogThread = ProcessLogManager.getInstance();
							List<Future<?>> workloadProcess=null, parserProcess=null;
							ExecutorService workloadThread=null, parserThread=null;
							CountDownLatch workloadLatch=null, parserLatch=null;
	
							try {
								if(BaseConfig.CONF_FEED_CRAWLER_CLASS != null) {
									reportManager.getReport().setFeed(true);
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_START, MerchantBean.STATUS.RUNNING);
									BaseConfig.FEED_PROCESSOR = "feed.ParserProcessor";
									BaseConfig.FEED_STORE_PATH += "/" + BaseConfig.MERCHANT_ID + "/wce";
									File f = new File(BaseConfig.FEED_STORE_PATH);
									if(!f.exists())
										f.mkdirs();
									
									Class.forName(BaseConfig.CONF_FEED_CRAWLER_CLASS).getDeclaredConstructor().newInstance();
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_FINISH, MerchantBean.STATUS.FINISH);
									
									if(MockObjectHolder.isMock)
										MockObjectHolder.feedThreadSleep();
									
								}else if(BaseConfig.CONF_PARSER_CLASS != null) {
									reportManager.getReport().setWeb(true);
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.CRAWLER_START, MerchantBean.STATUS.RUNNING);
		
									WorkloadDB workloadDB = DatabaseManager.getInstance().getWorkloadDB();
									int clearWorkload = workloadDB.clearWorkload(BaseConfig.MERCHANT_ID);
									threadLogger.info("Web : Clear Workload : " + clearWorkload + " records");
									
									ProductUrlDB productUrlDB = DatabaseManager.getInstance().getProductUrlDB();
									String[] clearStatus = new String[]{ bean.ProductUrlBean.STATUS.W.toString(), bean.ProductUrlBean.STATUS.R.toString(), bean.ProductUrlBean.STATUS.E.toString()};
									if(BaseConfig.CONF_WCE_CLEAR_PRODUCT_URL || BaseConfig.CONF_FORCE_UPDATE_CATEGORY || BaseConfig.CONF_FORCE_UPDATE_KEYWORD)
										clearStatus = new String[]{ bean.ProductUrlBean.STATUS.W.toString(), bean.ProductUrlBean.STATUS.R.toString(), bean.ProductUrlBean.STATUS.E.toString(), bean.ProductUrlBean.STATUS.C.toString()};
									int clearProductUrl = productUrlDB.clearProductUrlByStatus(BaseConfig.MERCHANT_ID, clearStatus);
									threadLogger.info("Web : Clear ProductUrl : " + clearProductUrl + " records");
									
									reportManager.countOldProduct();
									threadLogger.info("Web : Count old product");
									
									UrlPatternDB urlPatternDB = DatabaseManager.getInstance().getUrlPatternDB();
									List<UrlPatternBean> patternList = urlPatternDB.findUrlPattern(BaseConfig.MERCHANT_ID, bean.UrlPatternBean.ACTION.STARTURL.toString());
									if(patternList != null && patternList.size() > 0)
										for (UrlPatternBean upBean : patternList) { 
											workloadDB.insert(BaseConfig.MERCHANT_ID, upBean.getValue(), upBean.getGroup(), upBean.getCategoryId(), upBean.getKeyword(), "W");
											ReportManager.getInstance().getReport().increaseWceWorkloadCount();
										}
	
									if(BaseConfig.CONF_ENABLE_PROXY)
										HTTPUtil.enableUseProxy();
									
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.CRAWLER_RUN, MerchantBean.STATUS.RUNNING);
									threadLogger.info("Web : Crawler Thread : "+ BaseConfig.THREAD_NUM);
									workloadLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
									workloadThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
									if(MockObjectHolder.isMock)
										workloadThread = MockObjectHolder.excecutors.get("wce_workloadThread");
									
									workloadProcess = new ArrayList<>();
									for(int i = 0; i < BaseConfig.THREAD_NUM; i++) {
										UrlCrawlerProcessor processor = new UrlCrawlerProcessor();
										try {
											if(!MockObjectHolder.isMock) {
												processor.setProperties(workloadLatch);
											}else {
												MockObjectHolder.doCountDownLatch(workloadLatch,MockObjectHolder.countDownLatchUrlCrawlerProcesserCommand);
											}
										} catch (Exception e) {
											success.set(false);
											threadLogger.error(e);
											processLogThread.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
										}
										workloadProcess.add(workloadThread.submit(processor));
									}
									workloadThread.shutdown();
									workloadLatch.await();
									
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.RUNNING);
									threadLogger.info("Web : Parser Thread : "+ BaseConfig.THREAD_NUM);
									parserLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
									parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
									if(MockObjectHolder.isMock)
										parserThread = MockObjectHolder.excecutors.get("wce_parserThread");
									
									parserProcess = new ArrayList<>();
									for(int i = 0; i < BaseConfig.THREAD_NUM; i++) {
										WebCrawlerProcessor processor = new WebCrawlerProcessor();
										try {
											if(!MockObjectHolder.isMock) {
												processor.setProperties(parserLatch);
											}else {
												MockObjectHolder.doCountDownLatch(parserLatch,MockObjectHolder.countDownLatchWebCrawlerProcessorCommand);
											}
										} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
											success.set(false);
											threadLogger.error(e);
											processLogThread.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
										}
										parserProcess.add(parserThread.submit(processor));
									}
									parserThread.shutdown();
									parserLatch.await();
									
									reportManager.countNewProduct();
									merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.FINISH);
									
								}
							}catch(InterruptedException e) {
								if(workloadProcess != null && workloadProcess.size() > 0)
									for (Future<?> process : workloadProcess)
										process.cancel(true);
								if(parserProcess != null && parserProcess.size() > 0)
									for (Future<?> process : parserProcess)
										process.cancel(true);
								threadLogger.error(e);
								processLogThread.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
								return;
							}catch(Exception e) {
								success.set(false);
								threadLogger.error(e);
								processLogThread.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
								return;
							}
						}
					});
					
					Future<?> checkProcess = executor.submit(new Runnable() {
						public void run() {
							Logger checkLogger = LogManager.getRootLogger();
							ProcessLogManager checkProcessLog = ProcessLogManager.getInstance();
							try {
								CommandManager commandManager = CommandManager.getInstance();
								while(!Thread.currentThread().isInterrupted()) {
									if(MockObjectHolder.isMock) {
										MockObjectHolder.feedThreadSleep();
									}else {
										Thread.sleep(6000); 
									}
									if(mainProcess.isDone() || mainProcess.isCancelled()) 
										return;
									
									if(BaseConfig.MERCHANT_ID > 0) {
										if(commandManager.checkCommand(CommandManager.KILL_WCE)) {
											if(!mainProcess.isDone()) {
												checkLogger.info("Found kill command");
												// mainProcess.cancel(true);
												reportManager.markStatus(ReportBean.STATUS.KILLED);
												commandManager.clearCommand(CommandManager.KILL_WCE, false);
												endProcess(true);
												return;
											}
										}
									}
								}
							}catch(Exception e) {
								success.set(false);
								checkLogger.error(e);
								checkProcessLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
							}
						}
					});
					
					executor.shutdown();
					if(executor.awaitTermination(BaseConfig.CONF_MAX_RUNTIME, TimeUnit.HOURS)){
						logger.info("Done all");
					}else{
						logger.info("Time out.");
						reportManager.markStatus(ReportBean.STATUS.TIMEOUT);
						mainProcess.cancel(true);
						checkProcess.cancel(true);
						executor.shutdownNow();
						Thread.sleep(5000);
					}
					
				}catch(Exception e) {
					success.set(false);
					logger.error(e);
					processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
				}
			}
			
		} catch (Exception e) {
			if(logger != null){
				logger.error(e);
			}else{
				e.printStackTrace();
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
		} finally {
			endProcess(run);
		}
		return success.get();
	}
	
	private synchronized void endProcess(boolean run) {
		try {
			if(run) {
				MerchantManager.updateNextStart();
				MerchantConfigLoader.updateMerchantConfig();
				ReportManager.getInstance().finishReport();
				MonitoringManager.getInstance().calculate();
			}
			DatabaseLoader.closeDBConnection();
		} catch(Exception e) {
			e.printStackTrace();
			LogManager.getRootLogger().error(e.getMessage());
		} finally {
			if(run) {
				ProcessLogManager.getInstance().printLog();
				LogManager.getRootLogger().info("Bot : End process");
			}
			if(!MockObjectHolder.isMock){
				System.exit(0);
			}
		}
		return;
	}

}
