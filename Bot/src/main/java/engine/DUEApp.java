package engine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.MerchantBean;
import bean.ReportBean;
import bean.MerchantBean.STATE;
import db.MerchantDB;
import db.manager.DatabaseManager;
import loader.DatabaseLoader;
import loader.MerchantConfigLoader;
import manager.CommandManager;
import manager.MerchantManager;
import manager.MonitoringManager;
import manager.ProcessLogManager;
import manager.ReportManager;
import product.processor.ProductPreparing;
import unittest.MockObjectHolder;
import utils.BotUtil;
import utils.HTTPUtil;
import web.crawler.WebUpdateProcessor;

public class DUEApp extends BotFramework{
	
	public static void main(String[] args) {
		BotFramework app;
		try {
			app = new DUEApp(args);
			app.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public DUEApp(String[] config) {
		super(config);
	}

	@Override
	public boolean start() {
		Logger logger = null; 
		boolean run = false;
		AtomicBoolean success = new AtomicBoolean(true);
		try {
			BotUtil.LOCALE = BaseConfig.CONF_PROP.getProperty("ServerCountry");
			BaseConfig.FEED_STORE_PATH = BaseConfig.CONF_PROP.getProperty("FeedStore");
			BaseConfig.RUNTIME_TYPE = "DUE";
			
			try {
				run = DatabaseLoader.loadDBConnection();
			} catch(Exception e) {
				success.set(false);
				e.printStackTrace();
			} finally {
				if(!run){
					DatabaseLoader.updateQueue();
					return success.get();
				}
			}
			
			logger = LogManager.getRootLogger();
			String SIMPLE_CLASS_NAME = this.getClass().getSimpleName();
			ProcessLogManager processLog = ProcessLogManager.getInstance();
			logger.info("----------------------------------------------");
			logger.info("Bot : Start : " + BaseConfig.MERCHANT_ID + " " + BaseConfig.MERCHANT_NAME);
			MerchantDB merchantDB = DatabaseManager.getInstance().getMerchantDB();
			merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.INITIAL, MerchantBean.STATUS.RUNNING);
			
			ReportManager reportManager = ReportManager.getInstance();
			reportManager.startReport();

			CommandManager commandManager = CommandManager.getInstance();
			commandManager.clearCommand(CommandManager.PRIMARY_DUE, true);
			
			DatabaseLoader.updateQueue();
			
			MerchantConfigLoader.loadMerchantConfig();
			
			BaseConfig.THREAD_NUM = BaseConfig.CONF_DUE_PARSER_THREAD_NUM;
			
			ExecutorService executor = Executors.newFixedThreadPool(2);
			try {
				
				Future<?> mainProcess = executor.submit(new Runnable() {
					public void run() {
						
						ProcessLogManager threadProcessLog = ProcessLogManager.getInstance();
						Logger threadLogger = LogManager.getRootLogger();
						List<Future<?>> parserProcess=null;
						ExecutorService parserThread=null;
						CountDownLatch parserLatch=null;
						
						try {
							if(BaseConfig.CONF_FEED_CRAWLER_CLASS != null) {
								reportManager.getReport().setFeed(true);
								merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_START, MerchantBean.STATUS.RUNNING);
								BaseConfig.FEED_PROCESSOR = "feed.UpdateProcessor";
								BaseConfig.FEED_STORE_PATH += "/" + BaseConfig.MERCHANT_ID + "/due";
								File f = new File(BaseConfig.FEED_STORE_PATH);
								if(!f.exists())
									f.mkdirs();
								
								Class.forName(BaseConfig.CONF_FEED_CRAWLER_CLASS).getDeclaredConstructor().newInstance();
								merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.FEED_FINISH, MerchantBean.STATUS.FINISH);
								
								if(MockObjectHolder.isMock)
									MockObjectHolder.feedThreadSleep();
								
							}else if(isNotBlank(BaseConfig.CONF_PARSER_CLASS)){
								reportManager.getReport().setWeb(true);
								ProductPreparing.setRunning();
								if(BaseConfig.CONF_ENABLE_PROXY)
									HTTPUtil.enableUseProxy();
								merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.RUNNING);
								threadLogger.info("Web : Thread : "+ BaseConfig.THREAD_NUM);
								parserLatch = new CountDownLatch(BaseConfig.THREAD_NUM);
								parserThread = Executors.newFixedThreadPool(BaseConfig.THREAD_NUM);
								if(MockObjectHolder.isMock)
									parserThread = MockObjectHolder.excecutors.get("due_parserThread");
								
								parserProcess = new ArrayList<>();
								for(int i = 0; i < BaseConfig.THREAD_NUM; i++) {
									WebUpdateProcessor processor = new WebUpdateProcessor();
									try {
										if(!MockObjectHolder.isMock) {
											processor.setProperties(parserLatch);
										}else {
											MockObjectHolder.doCountDownLatch(parserLatch,MockObjectHolder.countDownLatchDUEWebCrawlerProcessorCommand);
										}
									} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
										success.set(false);
										threadLogger.error(e);
										threadProcessLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
									}
									parserProcess.add(parserThread.submit(processor));
								}
								parserThread.shutdown();
								parserLatch.await();
								
								merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.PARSER_RUN, MerchantBean.STATUS.FINISH);
							}
						}catch(InterruptedException e) {
							if(parserProcess != null && parserProcess.size() > 0)
								for (Future<?> process : parserProcess)
									process.cancel(true);
							
							threadLogger.error(e);
							threadProcessLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
							return;
						}catch(Exception e) {
							success.set(false);
							threadLogger.error(e);
							threadProcessLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
							return;
						}
					}
				});
			
				Future<?> checkProcess = executor.submit(new Runnable() {
					public void run() {
						Logger checkLogger = LogManager.getRootLogger();
						try {
							CommandManager commandManager = CommandManager.getInstance();
							while(!Thread.currentThread().isInterrupted()) {
								if(MockObjectHolder.isMock) {
									MockObjectHolder.feedThreadSleep();
								}else {
									Thread.sleep(6000); 
								}
								if(mainProcess.isDone() || mainProcess.isCancelled()) 
									return;
								
								if(BaseConfig.MERCHANT_ID > 0) {
									if(commandManager.checkCommand(CommandManager.KILL_DUE)) {
										if(!mainProcess.isDone()) {
											checkLogger.info("Found kill command");
											// mainProcess.cancel(true);
											reportManager.markStatus(ReportBean.STATUS.KILLED);
											commandManager.clearCommand(CommandManager.KILL_DUE, false);
											endProcess(true);
											return;
										}
									}
								}
							}
						}catch(Exception e) {
							success.set(false);
							checkLogger.error(e);
							processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
						}
					}
				});
				
				executor.shutdown();
				if(executor.awaitTermination(BaseConfig.CONF_MAX_RUNTIME, TimeUnit.HOURS)){
					logger.info("Done all");
				}else{
					logger.info("Time out.");
					reportManager.markStatus(ReportBean.STATUS.TIMEOUT);
					mainProcess.cancel(true);
					checkProcess.cancel(true);
					executor.shutdownNow();
					Thread.sleep(5000);
				}
				
			}catch(Exception e) {
				success.set(false);
				logger.error(e);
				processLog.addException(e.toString()+"-"+SIMPLE_CLASS_NAME,e);
			}
			
			commandManager.clearCommand(CommandManager.DELETE_ALL, true);
			commandManager.clearCommand(CommandManager.DELETE_NOT_MAP, true);
			
		} catch (Exception e) {
			if(logger != null){
				logger.error(e);
			}else{
				e.printStackTrace();
				ProcessLogManager.getInstance().addException(e.toString()+"-"+this.getClass().getSimpleName(),e);
			}
		} finally {
			endProcess(run);
		}
		return success.get();
	}
	
	private synchronized void endProcess(boolean run) {
		try {
			if(run) {
				MerchantManager.updateNextStart();
				MerchantConfigLoader.updateMerchantConfig();
				ReportManager.getInstance().finishReport();
				MonitoringManager.getInstance().calculate();
			}
			DatabaseLoader.closeDBConnection();
		} catch(Exception e) {
			e.printStackTrace();
			LogManager.getRootLogger().error(e.getMessage());
		} finally {
			if(run) {
				ProcessLogManager.getInstance().printLog();
				LogManager.getRootLogger().info("Bot : End process");
			}
			if(!MockObjectHolder.isMock){
				System.exit(0);
			}
		}
		return;
	}
}
