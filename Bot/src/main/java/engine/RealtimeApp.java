package engine;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import static org.apache.commons.lang3.StringUtils.*;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.BotConfigBean;
import bean.DataObjectBean;
import bean.ProductDataBean;
import bean.ProductMappingBean;
import bean.SendDataBean;
import db.BotConfigDB;
import db.MerchantDB;
import db.ProductDataDB;
import db.ProductMappingDB;
import db.SendDataDB;
import db.manager.DatabaseFactory;
import db.manager.DatabaseManager;
import db.manager.DatabaseUtil;
import loader.MerchantConfigLoader;
import product.processor.ParserUpdate;
import utils.BotUtil;
import utils.HTTPUtil;
import web.parser.HTMLParser;

public class RealtimeApp extends BotFramework{
	private static Logger logger;
	private List<DataSource> ds = new ArrayList<>();
	private Map<String, DataObjectBean> DATA_OBJ = new HashMap<>();
	private KafkaConsumer<String, String> CONSUMER = null;
	
	public static void main(String[] args) throws SQLException {
		RealtimeApp p = new RealtimeApp(args);
		try {
			p.loadBotConnection();
			p.start();
		}finally{
			p.close();
		}
	}
	
	public RealtimeApp(String[] config) {
		super(config);
	}
	
	public void close() {
		if(CONSUMER != null)
			CONSUMER.close();
		
		if(ds != null)
			ds.forEach(d -> {
				try {
					((BasicDataSource)d).close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
			
	}
	
	public void connect() {
		String jaasFormat = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
		String jaasCfg = String.format(jaasFormat, BaseConfig.CONF_PROP.getProperty("kafka.username"), BaseConfig.CONF_PROP.getProperty("kafka.password"));
		
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BaseConfig.CONF_PROP.getProperty("kafka.server"));
		props.put(ConsumerConfig.GROUP_ID_CONFIG, BaseConfig.CONF_PROP.getProperty("kafka.group"));
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("sasl.jaas.config", jaasCfg);
		props.put("security.protocol", "SASL_PLAINTEXT");
		props.put("sasl.mechanism", "PLAIN");
		CONSUMER = new KafkaConsumer<String, String>(props);
		CONSUMER.subscribe(Arrays.asList(BaseConfig.CONF_PROP.getProperty("kafka.topic")));
	}

	@Override
	public boolean start() {
		System.setProperty("merchantId", "realtime");
		System.setProperty("logFile", "bot");
		logger = LogManager.getRootLogger();
		logger.info("Start Bot realtime");
		BotUtil.LOCALE = BaseConfig.CONF_PROP.getProperty("ServerCountry");
		ProductMappingDB mapDB = DatabaseManager.getInstance().getProductMappingDB();
		MerchantDB mDB = DatabaseManager.getInstance().getMerchantDB();	
		
		int lastMid = -1;
		String parserClass = null;
		for(int round = 0; round < 100; round++) {
			Map<Integer, String> data = getRequest();
			if(data.isEmpty()) {
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				continue;
			}
			
			for(Entry<Integer, String> entry : data.entrySet()) {
				
				int prodPdId = entry.getKey();
				String qActionType = entry.getValue();
				
				ProductMappingBean mapB = null;
				ProductDataDB productDB = null;
				SendDataDB sendDB = null;
				ProductDataBean pdb = null;
				String dataServer = null;
				
				try {
					mapB = mapDB.getByProdId(prodPdId);
					if(mapB == null)
						throw new Exception("pId : "+ prodPdId +" : not found mapping");
					
					int botPdId = mapB.getBotId();
					int merchantId = mapB.getMerchantId();
					logger.info("mId : "+ merchantId +" : pId : "+ prodPdId +" : bId : "+ botPdId);
					if(botPdId == 0 || merchantId == 0)
						throw new Exception("invalid mapping");
					
					
					dataServer = mDB.getMerchantById(merchantId).getDataServer();
					if(isBlank(dataServer))
						throw new Exception("dataserver is blank");
					
					if(merchantId != lastMid) {
						MerchantConfigLoader.setDefault();
						BaseConfig.MERCHANT_ID = merchantId;
						MerchantConfigLoader.loadMerchantConfig();
						parserClass = getParser(qActionType);
						lastMid = merchantId;
						
						if(BaseConfig.CONF_ENABLE_PROXY)
							HTTPUtil.enableUseProxy();
						else
							HTTPUtil.disableUseProxy();
					}
					
					DataObjectBean dObj = DATA_OBJ.get(dataServer);					
					productDB = dObj.getProductDataDB();
					sendDB = dObj.getSendDataDB();
					pdb = productDB.getProduct(botPdId);
					
					if(pdb == null)
						throw new Exception("not found product data");
					
					String urlForUpdate = pdb.getUrlForUpdate();
					String url = urlForUpdate.startsWith("http") ? urlForUpdate : pdb.getUrl();					
					ProductDataBean b = parse(url, parserClass);
					if(b == null)
						throw new Exception("cannot parse product");
					double newPrice = b.getPrice();
					if(newPrice == 0)
						throw new Exception("cannot parse price");
					
					double newBasePrice = b.getBasePrice();
					String newDynamicField = b.getDynamicField();
					double oldPrice = pdb.getPrice();
					double oldBasePrice = pdb.getBasePrice();
					pdb.setPrice(newPrice);
					pdb.setBasePrice(newBasePrice);
					
					boolean priceUpdate = ParserUpdate.checkPrice(pdb, oldPrice, newPrice);
					boolean bPriceUpdate = ParserUpdate.checkBasePrice(pdb, oldBasePrice, newBasePrice, newPrice);
					boolean dynamicUpdate = isNotBlank(newDynamicField);
					
					if(priceUpdate || bPriceUpdate || dynamicUpdate) {
						if(dynamicUpdate) 
							pdb.setDynamicField(newDynamicField);
						
						int update = productDB.updateProductData(pdb.getId(), pdb);
						if(update == 1) {
							logger.info("update");
							SendDataBean sendDataBean = SendDataBean.createSendDataBean(pdb, null, SendDataBean.ACTION.UPDATE);
							sendDB.insertSendData(sendDataBean);
						}else {
							logger.warn("data in DB not found");
						}
					}else {
						logger.info("price not update");
					}
				} catch (Exception e) {
					logger.error(e);
					e.printStackTrace();
					continue;
				} 
			}
		}
		logger.info("------------------------------------------------");
		return true;
	}
	
	private void loadBotConnection() throws SQLException {
		DatabaseFactory.registerConfigDataSource(BaseConfig.CONF_PROP.getProperty("ConfigDBURL"), BaseConfig.CONF_PROP.getProperty("ConfigDBDriverName"),
				BaseConfig.CONF_PROP.getProperty("ConfigDBUserName"), BaseConfig.CONF_PROP.getProperty("ConfigDBPassword"), 2, 5, 2, 1);
		BotConfigDB bDB = DatabaseManager.getInstance().getBotConfigDB();
		List<BotConfigBean> config = bDB.getByDataAvailable(1);
		for(BotConfigBean db : config) {
			String botUrl = "jdbc:mysql://"+ db.getUrl();
			DataSource ds = DatabaseUtil.createDataSource(botUrl, "com.mysql.jdbc.Driver", db.getUsername(), db.getPassword(), 1);
			this.ds.add(ds);
			
			DataObjectBean b = new DataObjectBean();
			ProductDataDB productDB = new ProductDataDB(ds);
			SendDataDB sendDB = new SendDataDB(ds);
			b.setProductDataDB(productDB);
			b.setSendDataDB(sendDB);
			DATA_OBJ.put(db.getServer(), b);
		}
	}
	
	private Map<Integer, String> getRequest() {
		Map<Integer, String> data = new HashMap<>();
		int empty = 0;
		int limit = 5;
		
		try {
			connect();
			while(empty < limit) {
				ConsumerRecords<String, String> records = CONSUMER.poll(Duration.ofMillis(1000));
				if(records.isEmpty()) {
					empty++;
					continue;
				}
				
				for(ConsumerRecord<String, String> record : records) {
					String value = record.value();
					try {
						JSONObject obj = (JSONObject)new JSONParser().parse(value);
						int prodId = Math.toIntExact((Long)obj.get("productId"));
						String qActionType = (String) obj.get("qActionType");
						data.put(prodId, qActionType);
					} catch (ParseException e) {
						logger.error("Kafka : " + value, e);
					}
				}
				if(!data.isEmpty())
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CONSUMER.close();
		}
		return data;
	}
	
	public String getParser(String qActionType) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		String actionType = (isNotBlank(qActionType))? qActionType : "CLICK";
		if(BaseConfig.CONF_PARSER_DETAIL_CLASS != null && actionType.equals("DETAIL")) 
			return BaseConfig.CONF_PARSER_DETAIL_CLASS;
		else if(BaseConfig.CONF_PARSER_REALTIME_CLASS != null)
			return BaseConfig.CONF_PARSER_REALTIME_CLASS;
		else if(BaseConfig.CONF_DUE_PARSER_CLASS != null)
			return BaseConfig.CONF_DUE_PARSER_CLASS;
		else
			return BaseConfig.CONF_PARSER_CLASS;
	}
	
	public ProductDataBean parse(String url, String parserClass) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		HTMLParser parser = (HTMLParser)Class.forName(parserClass).getDeclaredConstructor().newInstance();
		parser.setParserConfig(null);
		ProductDataBean[] pdb = parser.parse(url);
		return pdb == null ? null : pdb[0];
	}
}
