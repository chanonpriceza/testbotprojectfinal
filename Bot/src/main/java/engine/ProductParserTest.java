package engine;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.text.DecimalFormat;

import bean.ProductDataBean;
import utils.BotUtil;
import web.parser.HTMLParser;

public class ProductParserTest {
	public static void main(String[] args) throws Exception {
		
		BaseConfig.CONF_MERCHANT_NAME = "";
		//BaseConfig.CONF_DISBLE_PARSER_COOKIES = true;
		//BaseConfig.CONF_ENABLE_PROXY = true;
		
		String parserClass;
		
		parserClass = "web.parser.filter.TemplateShopeeIncludeProductDetailHTMLParser";
		parserClass = "web.parser.filter.TemplateLazadaJSIncludeProductDetailHTMLParser";
		parserClass = "web.parser.filter.TaradHTMLParser";
		BaseConfig.CONF_FEED_CRAWLER_PARAM = "yes";
		String[] urls = new String[] {
			"http://www.opto.in.th/product.detail_357632_th_6516841?pzclickid=r-th--142--1366b164c80f4b24b218432846582f3e--91673228573d8e"
		};
				
		System.out.println("Set " + urls.length + " url.");
		System.out.println("Start .. ");
		
		for (String url : urls) {
			
		    if(isBlank(url) || isBlank(parserClass))
		    	return;
		    
		    long startTime = System.nanoTime();
		    
			HTMLParser parser = (HTMLParser)Class.forName(parserClass).getDeclaredConstructor().newInstance();
			url = BotUtil.encodeURL(url);
			
			ProductDataBean[] productDataBean = parser.parse(url);
			
			if (productDataBean != null) {
				System.out.println("----------------------------------------------------------------------------------------------------------------------");
				System.out.println(String.format("%2s", productDataBean.length) + " result from url = " + url);
				for(ProductDataBean product : productDataBean) {
	            	if (product == null)
	            		continue;
	            	 System.out.println("----------------------------------------------------------------------------------------------------------------------");
	                 System.out.println("name               = " + product.getName());
	                 System.out.println("price              = " + product.getPrice());
	                 System.out.println("basePrice          = " + product.getBasePrice());
	                 System.out.println("description        = " + product.getDescription());
	                 System.out.println("pictureUrl         = " + product.getPictureUrl());
	                 System.out.println("url                = " + product.getUrl());
	                 System.out.println("urlForUpdate       = " + product.getUrlForUpdate());
	                 System.out.println("----------------------------------------------------------------------------------------------------------------------");
	                 System.out.println("isExpire           = " + product.isExpire());                     
	                 System.out.println("realProductId      = " + product.getRealProductId());
	                 System.out.println("upc                = " + product.getUpc());
	                 System.out.println("categoryId         = " + product.getCategoryId());
	                 System.out.println("keyword            = " + product.getKeyword());
	                 System.out.println("merchantUpdateDate = " + product.getMerchantUpdateDate());
	                 System.out.println("dynamicField       = " + product.getDynamicField());
	            }
	        }
			System.out.println("---------------------------------- " + new DecimalFormat("####0.000").format((System.nanoTime() - startTime) / 1000000000.0) + " sec");
		}
		
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
        System.out.println("Finish");
	}
	
}
