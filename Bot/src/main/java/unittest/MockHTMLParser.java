package unittest;

import java.util.List;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import web.parser.HTMLParser;

public class MockHTMLParser implements HTMLParser {
	public static int count = 0;
	public static commandSet command;
	
	public enum commandSet {
		NORMAL,NULL,EXCEPTION
	};
	
	@Override
	public void setParserConfig(List<ParserConfigBean> configList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProductDataBean[] parse(String url) {
		
		if(command==commandSet.NORMAL) {	
			
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("UnitTest ProductData");
			pdb.setUrl("http://www.soccersuck.com/");
			pdb.setPrice(6969.69);
			pdb.setCategoryId(9999);
			pdb.setUrlForUpdate("http://www.soccersuck.com/");
			pdb.setKeyword("Test");
			pdb.setBasePrice(12340);
			if(count==1) {
				return null;
			}
			count ++;
			return new ProductDataBean[]{pdb};
		}else if(command==commandSet.EXCEPTION) {
			
		}

		return null;
		
	}

	@Override
	public FILTER_TYPE getFilterType() {
		// TODO Auto-generated method stub
		return null;
	}

}
