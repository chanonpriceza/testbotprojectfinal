package unittest;

import java.util.List;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import web.parser.HTMLParser;

public class MockParserClass implements HTMLParser {

	@Override
	public void setParserConfig(List<ParserConfigBean> configList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProductDataBean[] parse(String url) {
		// for test webupdate Processor
		if("http://test.com?productId=1".equals(url)) {
			// same case
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("Test Product");
			pdb.setUrl("http://test.com?productId=1");
			pdb.setUrlForUpdate("http://test.com?productId=1");
			pdb.setPrice(69.00);
			pdb.setBasePrice(169.00);
			pdb.setDescription("Test Description");
			pdb.setCategoryId(1);
			pdb.setKeyword("Test Keyword");
			pdb.setMerchantId(1);
			return new ProductDataBean[] {pdb};
			
		}else if("http://test.com?productId=2".equals(url)){
			//expire
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("Test Product");
			pdb.setUrl("http://test.com?productId=1");
			pdb.setUrlForUpdate("http://test.com?productId=1");
			pdb.setPrice(69.00);
			pdb.setBasePrice(169.00);
			pdb.setDescription("Test Description");
			pdb.setCategoryId(1);
			pdb.setKeyword("Test Keyword");
			pdb.setMerchantId(1);
			pdb.setExpire(true);
			return new ProductDataBean[] {pdb};
			
		}else if("http://test.com?productId=3".equals(url)){
			//Error
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("");
			pdb.setUrl("http://test.com?productId=1");
			pdb.setUrlForUpdate("http://test.com?productId=1");
			pdb.setPrice(69.00);
			pdb.setBasePrice(169.00);
			pdb.setDescription("Test Description");
			pdb.setCategoryId(1);
			pdb.setKeyword("Test Keyword");
			pdb.setMerchantId(1);
			pdb.setExpire(true);
			return new ProductDataBean[] {pdb};
		}else if("http://test.com?productId=4".equals(url)){
			//url null
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("Test Product");
			pdb.setUrl(null);
			pdb.setUrlForUpdate("http://test.com?productId=1");
			pdb.setPrice(69.00);
			pdb.setBasePrice(169.00);
			pdb.setDescription("Test Description");
			pdb.setCategoryId(1);
			pdb.setKeyword("Test Keyword");
			pdb.setMerchantId(1);
			pdb.setExpire(true);
			return new ProductDataBean[] {pdb};
		}else if("http://test.com?productId=5".equals(url)){
			//url error count 
			ProductDataBean pdb = new ProductDataBean();
			pdb.setName("");
			pdb.setUrl("http://test.com?productId=1");
			pdb.setUrlForUpdate("http://test.com?productId=1");
			pdb.setPrice(69.00);
			pdb.setBasePrice(169.00);
			pdb.setDescription("Test Description");
			pdb.setCategoryId(1);
			pdb.setKeyword("Test Keyword");
			pdb.setMerchantId(1);
			pdb.setExpire(true);
			pdb.setErrorUpdateCount(6);
			return new ProductDataBean[] {pdb};
		}
		
		
		return null;
	}

	@Override
	public FILTER_TYPE getFilterType() {
		// TODO Auto-generated method stub
		return null;
	}
}
