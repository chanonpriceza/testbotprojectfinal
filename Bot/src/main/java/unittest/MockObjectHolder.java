package unittest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import db.manager.DatabaseManager;
import feed.CSVFeedCrawler;
import feed.JSONFeedCrawler;
import feed.XMLFeedCrawler;
import manager.CommandManager;
import manager.ReportManager;
import manager.UrlPatternManager;
import product.processor.ProductReader;
import product.processor.ProductUrlReader;
import product.processor.WorkloadReader;

public class MockObjectHolder{
	
	public static boolean isMock;
	
	public static DatabaseManager mockDatabaseManager;
	
	public static ReportManager mockReportManager;
	
	public static Logger mockLogger;
	
	public static QueryRunner queryRunnerDatabaseLoader;
	
	public static QueryRunner queryRunnerDB;
	
	public static CommandManager commandManager;
	
	public static Map<String,ExecutorService> excecutors; 
	
	public static int threadSleepTime;
	
	public static String countDownLatchUrlCrawlerProcesserCommand;
	
	public static String countDownLatchWebCrawlerProcessorCommand;
	
	public static String countDownLatchDUEWebCrawlerProcessorCommand;
	
	public static CSVFeedCrawler csvFeedCrawler;
	
	public static XMLFeedCrawler xmlFeedCrawler;
	
	public static JSONFeedCrawler jsonFeedCrawler;
	
	public static ProductUrlReader productUrlReader;
	
	public static WorkloadReader workloadReader;
	
	public static UrlPatternManager urlPatternManager;
	
	public static ProductReader productReader;
	
	public static void doCountDownLatch(CountDownLatch lacth,String command) throws Exception {
		try {
			if(!StringUtils.isBlank(command)) {
				if(command.equals("Error")) {
					throw new Exception("Error");
				}else if("InstantiationException".equals(command)) {
					throw new InstantiationException();
				}else if("IllegalAccessException".equals(command)) {
					throw new IllegalAccessException();
				}else if("ClassNotFoundException".equals(command)) {
					throw new ClassNotFoundException();
				}
				if(command.equals("Sleep")) {
					Thread.sleep(threadSleepTime);
				}
			}
		}catch (Exception e) {
			throw e;
		}finally {
			lacth.countDown();
		}
	}
	
	public static void feedThreadSleep() throws InterruptedException {
		Thread.sleep(threadSleepTime);
	}
	
	public static void addExecutors(String executorName,ExecutorService ex) {
		if(excecutors==null) {
			excecutors = new HashMap<String, ExecutorService>();
		}
		excecutors.put(executorName,ex) ;
	}
	
	
}
