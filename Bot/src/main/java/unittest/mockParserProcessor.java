package unittest;

import java.util.concurrent.CountDownLatch;

import feed.FeedManager;
import feed.ProcessorInterface;

public class mockParserProcessor implements ProcessorInterface {
	public static String command = "";
	CountDownLatch latchAttr;
	FeedManager feed;
	
	@Override
	public Boolean call() throws Exception {
		try {
			if(command.equals("InstantiationException")) {
				System.out.println("this");
				throw new InstantiationException();
			}else if(command.equals("IllegalAccessException")) {
				throw new IllegalAccessException();
			}else if(command.equals("ClassNotFoundException")) {
				throw new ClassNotFoundException();
			}else if(command.equals("InterruptedException")) {
				Thread.currentThread().interrupt();
				throw new InterruptedException();
			}
		}catch (Exception e) {
			throw e;
		}finally {
			latchAttr.countDown();
		}
		return null;
	}

	@Override
	public void setProperties(FeedManager feedManager, CountDownLatch latch) {
		latchAttr = latch;
		feed = feedManager;
		
	}

}
