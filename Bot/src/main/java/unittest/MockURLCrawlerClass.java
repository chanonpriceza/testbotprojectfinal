package unittest;

import bean.WorkloadBean;
import web.crawler.bot.BotExclusion;
import web.crawler.bot.HTTP;
import web.crawler.bot.URLCrawlerClient;
import web.crawler.bot.URLCrawlerInterface;

public class MockURLCrawlerClass implements URLCrawlerInterface {
	
	protected HTTP http;
    protected BotExclusion botExclusion;
    protected URLCrawlerClient client;
    boolean enableCookies;
    boolean checkInit;
    
	public static String  command = "";
	String[] linkPic = new String[] {
		"http://www.testPicture.com/test.jpg",
		"http://www.testPicture.com/test.png",
		"http://www.testPicture.com/test.css",
		"http://www.testPicture.com/test.js",
		"http://www.testPicture.com/test.gif",
	};

	@Override
	public void process(WorkloadBean target) throws Exception {
		String targetUrl = target.getUrl();
        int targetDepth = target.getDepth();
        int targetMerchantId = target.getMerchantId();
        int targetCategoryId = target.getCategoryId();
        String targetKeyword = target.getKeyword();

        if(command.equals("pic")) {
        	for(String l:linkPic) {
        		client.foundInternalLink(targetMerchantId,l, targetDepth + 1, targetCategoryId, targetKeyword);
        	}
        }
        else {
        	client.foundInternalLink(targetMerchantId, targetUrl+"?product_id=1", targetDepth + 1, targetCategoryId, targetKeyword);
        	
        }
		
	}

	@Override
	public void init(URLCrawlerClient client) throws Exception {
		this.client = client;
		
	}

}
