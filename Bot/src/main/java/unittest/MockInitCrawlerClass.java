package unittest;

import java.util.ArrayList;
import java.util.List;

import bean.ProductUrlBean;
import web.crawler.bot.CrawlerInterface;

public class MockInitCrawlerClass implements CrawlerInterface  {
	
	public static String command = "";

	@Override
	public List<ProductUrlBean> initProductUrl(String startUrl) {
		if(command.equals("null")) {
			return null;			
		}
		ProductUrlBean pdUrl = new ProductUrlBean();
		pdUrl.setUrl("http://www.soccersuck.com");
		pdUrl.setCategoryId(6969);
		pdUrl.setMerchantId(69);
		pdUrl.setState("N");
		pdUrl.setStatus("W");
		pdUrl.setKeyword("Test");

		List<ProductUrlBean> pdbList = new ArrayList<ProductUrlBean>();
		pdbList.add(pdUrl);
		
		return  pdbList;
	}

	@Override
	public boolean continueProcess() {
		if(command.equals("continueProcessFalse")) {
			return false;
		}
		return true;
	}

}
