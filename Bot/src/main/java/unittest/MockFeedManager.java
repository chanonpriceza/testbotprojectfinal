package unittest;

import feed.FeedManager;

public class MockFeedManager extends FeedManager {
	
	public static String command = "";
	
	public MockFeedManager() throws Exception {
		super();
	
	}

	@Override
	public String[] setFeedFile() {

		if(command.equals("null")) {
			return null;
		}else if(command.equals("empty")) {
			return new String[] {""};
		}else if(command.equals("error")) {
			return new String[] {"http://999.254.85.9/300231.csv"};
		}else if(command.equals("Interupt")) {
			Thread.currentThread().interrupt();
		}
		return new String[] {"http://27.254.82.243:8081/300231.csv"};

	}
	


}
