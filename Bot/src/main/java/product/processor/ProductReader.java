package product.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import db.ProductDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;

public class ProductReader {
	private static final Logger logger = LogManager.getRootLogger();
	private List<ProductDataBean> products = new ArrayList<ProductDataBean>();
	private ProductDataDB productDataDB = DatabaseManager.getInstance().getProductDataDB();
	private int PROCESS;
	private int OFFSET = 1000;
	private boolean CHECKOUT;
	private boolean FINISH;
	
	private static class SingletonHelper {
        private static final ProductReader INSTANCE = new ProductReader();
    }
	
	public static ProductReader getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.productReader;
		}
		return SingletonHelper.INSTANCE;
	}
	
	private boolean loadData() {
		while(!Thread.currentThread().isInterrupted()){
			try {
				products = productDataDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET);
				if(products.isEmpty())
					return false;
				if(products.size() < OFFSET)
					CHECKOUT = true;
				
				List<Integer> ids = products.stream().map(ProductDataBean::getId).collect(Collectors.toList());
				productDataDB.updateStatus(STATUS.R, ids);
				return true;
			}catch(Exception e) {
				logger.error("Web : Get Product Error : ", e);
				return false;
			}
		}
		return false;
	}
	
	public synchronized ProductDataBean readData() {
		while(!Thread.currentThread().isInterrupted()) {
			if(!products.isEmpty()) {
				PROCESS++;
				if(PROCESS % 1000 == 0)
					logger.info("Web : Parse process : "+ PROCESS);
				ProductDataBean b = products.get(0);
				products.remove(0);
				return b;
			}
			
			if(CHECKOUT) {
				if(!FINISH)
					logger.info("Web : Finish Process : "+ PROCESS);
				FINISH = true;
				return null;
			}
			
			if(!loadData()) {
				logger.info("Web : Finish Process : "+ PROCESS);
				FINISH = true;
				return null;
			}
		}
		return null;
	}
}
