package product.processor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.ProductUrlBean;
import bean.ProductUrlBean.STATUS;
import db.ProductUrlDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;

public class ProductUrlReader {
	private static final Logger logger = LogManager.getRootLogger();
	private List<ProductUrlBean> urls = new ArrayList<ProductUrlBean>();
	private ProductUrlDB productUrlDB = DatabaseManager.getInstance().getProductUrlDB();
	private int PROCESS;
	private int OFFSET = 1000;
	private boolean CHECKOUT;
	private boolean FINISH;
	
	private static class SingletonHelper {
        private static final ProductUrlReader INSTANCE = new ProductUrlReader();
    }
	
	public static ProductUrlReader getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.productUrlReader;
		}
		return SingletonHelper.INSTANCE;
	}
	
	private boolean loadData() {
		while(!Thread.currentThread().isInterrupted()){
			try {
				urls = productUrlDB.getByStatusLimit(BaseConfig.MERCHANT_ID, STATUS.W, OFFSET);
				if(urls.isEmpty())
					return false;
				if(urls.size() < OFFSET)
					CHECKOUT = true;
				
				List<String> urlList = urls.stream().map(ProductUrlBean::getUrl).collect(Collectors.toList());
				productUrlDB.updateStatus(BaseConfig.MERCHANT_ID, STATUS.R, urlList);
				return true;
			}catch(SQLException e) {
				logger.error("Web : Get ProductURL Error : ", e);
			}catch(Exception e) {
				logger.error("Web : Get ProductURL Error : ", e);
				return false;
			}
		}
		return false;
	}
	
	public synchronized ProductUrlBean readData() {
		while(!Thread.currentThread().isInterrupted()) {
			if(!urls.isEmpty()) {
				PROCESS++;
				if(PROCESS % OFFSET == 0)
					logger.info("Web : Parse process : "+ PROCESS);
				ProductUrlBean b = urls.get(0);
				urls.remove(0);
				return b;
			}
			
			if(CHECKOUT) {
				if(!FINISH)
					logger.info("Web : Finish Parse Process : "+ PROCESS);
				FINISH = true;
				return null;
			}
			
			if(!loadData()) {
				logger.info("Web : Finish Parse Process : "+ PROCESS);
				FINISH = true;
				return null;
			}
		}
		return null;
	}
}
