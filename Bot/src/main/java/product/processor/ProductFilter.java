package product.processor;

import org.apache.commons.lang3.StringUtils;

public class ProductFilter {
	
	public static boolean checkCOVIDInstantDelete(String productName) {
		if(StringUtils.isNotBlank(productName)) {
			if(productName.contains("หน้ากาก")) return true;
			if(productName.contains("เจล")) return true;
		}
		return false;
	}
	
}