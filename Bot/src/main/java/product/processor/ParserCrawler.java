package product.processor;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ProductUrlBean;
import bean.ProductUrlBean.STATE;
import bean.ProductUrlBean.STATUS;
import bean.ReportBean;
import bean.SendDataBean;
import db.ProductDataDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ParserCrawler {
	private static final Logger logger = LogManager.getRootLogger();
	private static final ProductUrlDB productUrlDB = DatabaseManager.getInstance().getProductUrlDB();
	private static final ProductDataDB productDataDB = DatabaseManager.getInstance().getProductDataDB();
	private static final SendDataDB sendDataDB = DatabaseManager.getInstance().getSendDataDB();
	private static final ReportBean report = ReportManager.getInstance().getReport();
	private static final ProcessLogManager processLog = ProcessLogManager.getInstance();
	
	public static boolean process(ProductDataBean[] dataBeanList, ProductUrlBean urlBean) throws SQLException {
		boolean result = true;
		try {
			List<ParserResultBean> resultBeanList = analyzeProductList(dataBeanList, urlBean);
			boolean ALL_SUCCESS = true;
			boolean IS_EXPIRE = false;
			boolean IS_NOTALLOW = false;
			boolean IS_DUPLICATE = false;
			
			if(resultBeanList != null && resultBeanList.size() > 0) {
				
				if(dataBeanList.length != resultBeanList.size())
					ALL_SUCCESS = false;
				
				for (ParserResultBean resultBean : resultBeanList) {
					
					if(resultBean.isExpire()) {
						report.increaseExpire();
						IS_EXPIRE = true;
						ALL_SUCCESS = false;
						continue;
					}else if(resultBean.isNotAllowLanguage()) {
						report.increaseNotAllow();
						IS_NOTALLOW = true;
						ALL_SUCCESS = false;
						continue;
					}else if(resultBean.getPdb() != null) {
						ProductDataBean oldBean = null;
						if(BaseConfig.CONF_IGNORE_NAME_CHANGE) {
							oldBean = productDataDB.getProductByUrlForUpdate(BaseConfig.MERCHANT_ID, StringUtils.defaultIfBlank(resultBean.getPdb().getUrlForUpdate(), resultBean.getPdb().getUrl()));
						}else {
							oldBean = productDataDB.getProduct(BaseConfig.MERCHANT_ID, resultBean.getPdb().getName());
						}
						if(oldBean != null) {
							resultBean = analyzeExistProduct(resultBean.getPdb(), oldBean);
						}
					}
					
					if(resultBean.isDuplicate()) {
						report.increaseDuplicate();
						IS_DUPLICATE = true;
						ALL_SUCCESS = false;
						continue;
					}
					
					if(resultBean.isUpdatePic()) {
						int updateResult = productDataDB.updateProductData(resultBean.getPdb().getId(), resultBean.getPdb());
						if(updateResult > 0) {
							resultBean = parseImage(resultBean);
							SendDataBean sendDataBean = SendDataBean.createSendDataBean(resultBean.getPdb(), resultBean.getImageByte(), SendDataBean.ACTION.UPDATE_PIC);
							sendDataDB.insertSendData(sendDataBean);
							report.increaseSuccessCount();
							report.increaseUpdatePic();
						}
					}else if(resultBean.isUpdate()) {
						int updateResult = productDataDB.updateProductData(resultBean.getPdb().getId(), resultBean.getPdb());
						if(updateResult > 0) {
							SendDataBean sendDataBean = SendDataBean.createSendDataBean(resultBean.getPdb(), null, SendDataBean.ACTION.UPDATE);
							sendDataDB.insertSendData(sendDataBean);
							report.increaseSuccessCount();
							report.increaseUpdate();
						}
					}else if(resultBean.isExist()) {
						report.increaseSuccessCount();
					}else if(resultBean.getPdb() != null) {
						int insertResult = productDataDB.insertProductData(resultBean.getPdb());
						if(insertResult > 0) {
							resultBean = parseImage(resultBean);
							SendDataBean sendDataBean = SendDataBean.createSendDataBean(resultBean.getPdb(), resultBean.getImageByte(), SendDataBean.ACTION.ADD);
							sendDataDB.insertSendData(sendDataBean);
							report.increaseSuccessCount();
							report.increaseAdd();
						}
					}
				}
			}else {
				ALL_SUCCESS = false;
				report.increaseErrorCount();
				report.increaseWceProductUrlCountFailed();
			}
			
			if(ALL_SUCCESS) {
				markUrl(urlBean, STATUS.C, STATE.S);
				report.increaseWceProductUrlCountSuccess();
			} else if(IS_EXPIRE) {
				markUrl(urlBean, STATUS.E, STATE.X);
			} else if(IS_NOTALLOW) {
				markUrl(urlBean, STATUS.E, STATE.L);
			} else if(IS_DUPLICATE) {
				markUrl(urlBean, STATUS.E, STATE.D);
			} else {
				markUrl(urlBean, STATUS.E, STATE.F);
			}
		}catch (Exception e) {
			result = false;
			logger.error(e);
		}
		
		return result;
	}

	public static void markNoResult(ProductUrlBean urlBean) throws SQLException {
		markUrl(urlBean, STATUS.E, STATE.F);
		report.increaseWceProductUrlCountFailed();
	}
	
	private static void markUrl(ProductUrlBean urlBean, STATUS s1, STATE s2) throws SQLException {
		productUrlDB.update(s1, s2, BaseConfig.MERCHANT_ID, urlBean.getUrl());
	}

	public static ParserResultBean parseImage(ParserResultBean resultBean) {
		if(resultBean == null) return null;
		if(resultBean.getPdb() != null && !isBlank(resultBean.getPdb().getPictureUrl())) {
			byte[] imageByte = BaseConfig.IMAGE_PARSER.parse(resultBean.getPdb().getPictureUrl(), BaseConfig.IMAGE_WIDTH, BaseConfig.IMAGE_HEIGHT);
    		if(imageByte != null)
				resultBean.setImageByte(imageByte);
			else
				logger.warn("Web : Cannot Parse Image : " + resultBean.getPdb().getPictureUrl());
    	}
		return resultBean;
	}
	
	public static List<ParserResultBean> analyzeProductList(ProductDataBean[] dataBeanList, ProductUrlBean urlBean) throws SQLException {
		
		if(dataBeanList == null || urlBean == null) 
			return null;
		
		List<ParserResultBean> resultBeanList = new ArrayList<>();
		
		String urlKeyword = urlBean.getKeyword();
		int urlCategoryId = urlBean.getCategoryId();
		
		for(ProductDataBean dataBean : dataBeanList) {
			
			ParserResultBean resultBean = new ParserResultBean();
			
			boolean isExpire = dataBean.isExpire();
			if(isExpire) {
				resultBean.setExpire(true);
				resultBeanList.add(resultBean);
				continue;
			}
			
			String name = dataBean.getName();
			double price = dataBean.getPrice();
			double basePrice = dataBean.getBasePrice();
			String description = StringUtils.defaultIfBlank(dataBean.getDescription(), null);
			String pictureUrl = StringUtils.defaultIfBlank(dataBean.getPictureUrl(), null);
			int categoryId = dataBean.getCategoryId();
			String keyword = StringUtils.defaultIfBlank(dataBean.getKeyword(), null);
			String realProductId = StringUtils.defaultIfBlank(dataBean.getRealProductId(), null);
			String upc = StringUtils.defaultIfBlank(dataBean.getUpc(), null);
			String url = dataBean.getUrl();
			String urlForUpdate = dataBean.getUrlForUpdate();
			String dynamicField = dataBean.getDynamicField();
			Timestamp merchantUpdateDate = dataBean.getMerchantUpdateDate();
			
			if(StringUtils.isBlank(name) || price <= 0) {
				if(isBlank(name)) 
					processLog.addNameIsBlank((StringUtils.isBlank(urlBean.getUrl())?"":urlBean.getUrl())+" "+(StringUtils.isBlank(realProductId)?"":"("+realProductId+")"),name);		
				if(price == 0 )
					processLog.addPriceZero((StringUtils.isBlank(name)?"":name)+" "+(StringUtils.isBlank(urlBean.getUrl())?"":urlBean.getUrl())+" "+(StringUtils.isBlank(realProductId)?"":"("+realProductId+")"),String.valueOf(price));
				continue;
			}
			
			if(!FilterUtil.checkAllowCharacter(name,BotUtil.LOCALE) || !FilterUtil.checkAllowCharacter(url,BotUtil.LOCALE)) {
				resultBean.setNotAllowLanguage(true);
				resultBeanList.add(resultBean);
				continue;
			}
			
			if(isBlank(url)) 
				url = urlBean.getUrl();
			
			if(isBlank(urlForUpdate))
				urlForUpdate = url;
			
			if(BaseConfig.CONF_CURRENCY_RATE != 1.0) {
				price = price * BaseConfig.CONF_CURRENCY_RATE;
				price = Double.valueOf(BotUtil.FORMAT_PRICE.format(price));
				if(basePrice > 0) {
					basePrice = basePrice * BaseConfig.CONF_CURRENCY_RATE;
					basePrice = Double.valueOf(BotUtil.FORMAT_PRICE.format(basePrice));
				}
			}
			
			if(price > BotUtil.CONTACT_PRICE)
				price = BotUtil.CONTACT_PRICE;
			
			if(basePrice > 0 && basePrice < price)
				basePrice = price;
			
			if(categoryId != urlCategoryId)
				if(categoryId == 0)
					categoryId = urlCategoryId;
			
			if(isBlank(keyword))
				keyword = urlKeyword;
			
			ProductDataBean pdBean = new ProductDataBean();
			pdBean.setMerchantId(BaseConfig.MERCHANT_ID);
			pdBean.setName(name);
			pdBean.setPrice(price);
			pdBean.setBasePrice(basePrice);
			pdBean.setDescription(description);
			pdBean.setPictureUrl(pictureUrl);
			pdBean.setRealProductId(realProductId);
			pdBean.setUpc(upc);
			pdBean.setCategoryId(categoryId);
			pdBean.setKeyword(keyword);
			pdBean.setUrl(url);
			pdBean.setUrlForUpdate(urlForUpdate);
			pdBean.setUpdateDate(new Timestamp(System.currentTimeMillis()));
			pdBean.setMerchantUpdateDate(merchantUpdateDate);
			pdBean.setErrorUpdateCount(0);
			pdBean.setDynamicField(dynamicField);
		
			if(!ParserUpdate.baseValidate(pdBean))
				continue;
			
			resultBean.setPdb(pdBean);
			resultBeanList.add(resultBean);
			
		}
		return resultBeanList;
		
	}
	
	@SuppressWarnings("unchecked")
	public static ParserResultBean analyzeExistProduct(ProductDataBean newBean, ProductDataBean oldBean) {
		
		ParserResultBean resultBean = new ParserResultBean();
		if(newBean != null && oldBean != null) {
			double newPrice = newBean.getPrice();
			double newBasePrice = newBean.getBasePrice();
			String newUrl = newBean.getUrl();
			String newUrlForUpdate = newBean.getUrlForUpdate();
			String newDescription = StringUtils.defaultIfBlank(newBean.getDescription(), null);
			String newPictureUrl = StringUtils.defaultIfBlank(newBean.getPictureUrl(), null);
			int newCategoryId = newBean.getCategoryId();
			String newKeyword = StringUtils.defaultIfBlank(newBean.getKeyword(), null);
			String newRealProductId = StringUtils.defaultIfBlank(newBean.getRealProductId(), null);
			String newUpc = StringUtils.defaultIfBlank(newBean.getUpc(), null);
			String newDynamicField = StringUtils.defaultIfBlank(newBean.getDynamicField(), null);
			Timestamp newUpdateDate = newBean.getUpdateDate();
			
			int oldId = oldBean.getId();
			int oldMerchantId = oldBean.getMerchantId();
			String name = oldBean.getName();
			double price = oldBean.getPrice();
			double basePrice = oldBean.getBasePrice();
			String url = oldBean.getUrl();
			String urlForUpdate = oldBean.getUrlForUpdate();
			String description = StringUtils.defaultIfBlank(oldBean.getDescription(), null);
			String pictureUrl = StringUtils.defaultIfBlank(oldBean.getPictureUrl(), null);
			int categoryId = oldBean.getCategoryId();
			String keyword = StringUtils.defaultIfBlank(oldBean.getKeyword(), null);
			String realProductId = StringUtils.defaultIfBlank(oldBean.getRealProductId(), null);
			String upc = StringUtils.defaultIfBlank(oldBean.getUpc(), null);
			
			if(BotUtil.checkStringEqual(newUrlForUpdate, urlForUpdate)) {
				
				ProductDataBean finalBean = new ProductDataBean();
				
				//check url
				if(!BotUtil.checkStringEqual(newUrl, url) && BaseConfig.CONF_ENABLE_UPDATE_URL) {
					url = newUrl;
					resultBean.setUpdate(true);
				}else if(!BotUtil.checkStringEqual(newUrl, url) && !BaseConfig.CONF_USER_OLD_PRODUCT_URL) {
					resultBean.setDuplicate(true);
					return resultBean;
				}
				
				//check price & baseprice
				if(newPrice > BotUtil.CONTACT_PRICE) {
					newPrice = BotUtil.CONTACT_PRICE;
				}
				if (newPrice != price) {
					if (BaseConfig.CONF_UPDATE_PRICE_PERCENT > 0 && newPrice < BotUtil.CREDIT_BASE_PRICE && price < BotUtil.CREDIT_BASE_PRICE) {
						int percentDiff = BotUtil.calPercentDiff(price, newPrice);
						if (Math.abs(percentDiff) <= BaseConfig.CONF_UPDATE_PRICE_PERCENT) {
							price = newPrice;
							resultBean.setUpdate(true);
						}
					} else {
						price = newPrice;
						resultBean.setUpdate(true);
					}
				}
				if(((basePrice == 0 && newBasePrice > 0) || (basePrice != newBasePrice && (BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE))) 
						&& newBasePrice <= BotUtil.CREDIT_BASE_PRICE && newPrice < BotUtil.CONTACT_PRICE) {
					basePrice = newBasePrice;
					resultBean.setUpdate(true);
				}
				if(basePrice > 0 && (newPrice == BotUtil.CONTACT_PRICE || newBasePrice > BotUtil.CREDIT_BASE_PRICE)){
					basePrice = 0;
					resultBean.setUpdate(true);
				}
				
				// check description
				if(!isBlank(newDescription) && isBlank(description) 
						|| (BaseConfig.CONF_ENABLE_UPDATE_DESC && !BotUtil.checkStringEqual(newDescription, description))) {
					description = newDescription;
					resultBean.setUpdate(true);
				}
				
				// check dynamic field
				if(!isBlank(newDynamicField)) {
					finalBean.setDynamicField(newDynamicField);
					resultBean.setUpdate(true);
				}
				
				// check category & keyword
				if((BaseConfig.CONF_FORCE_UPDATE_CATEGORY || BaseConfig.CONF_FORCE_UPDATE_KEYWORD) && (!BotUtil.checkStringEqual(keyword, newKeyword) || (categoryId != newCategoryId && newCategoryId > 0))) {
					if(categoryId != newCategoryId) {
						JSONObject dynamicField = new JSONObject();
						if(!isBlank(newDynamicField)) {
			        		try {
			        			dynamicField = (JSONObject) new JSONParser().parse(newDynamicField);
			        		} catch(Exception e) {
			        			processLog.addException(e.toString()+"-ParserCrawler", e);
		        			}
		        		}
						dynamicField.put("oldCategory", Integer.toString(categoryId));
						finalBean.setDynamicField(dynamicField.toJSONString());
					}
	        		categoryId = newCategoryId;
	        		keyword = newKeyword;
	        		resultBean.setUpdate(true);
				}
				
				// check realProductId 
				if(isBlank(realProductId) && !isBlank(newRealProductId)){
					realProductId = newRealProductId;
					resultBean.setUpdate(true);
				}
				
				// check upc
				if(isBlank(upc) && !isBlank(newUpc)){
					upc = newUpc;
					resultBean.setUpdate(true);
				}
				
				// check picture url
				if((isBlank(pictureUrl) && !isBlank(newPictureUrl)) || 
							(!isBlank(pictureUrl) && !isBlank(newPictureUrl) && !BotUtil.checkStringEqual(pictureUrl, newPictureUrl) && BaseConfig.CONF_ENABLE_UPDATE_PIC)) {
					pictureUrl = newPictureUrl;
					resultBean.setUpdatePic(true);
				}
				
				finalBean.setName(name);
				finalBean.setPrice(price);
				finalBean.setBasePrice(basePrice);
				finalBean.setUrl(url);
				finalBean.setUrlForUpdate(urlForUpdate);
				finalBean.setDescription(description);
				finalBean.setPictureUrl(pictureUrl);
				finalBean.setCategoryId(categoryId);
				finalBean.setKeyword(keyword);
				finalBean.setRealProductId(realProductId);
				finalBean.setUpc(upc);
				
				// summary
				if(resultBean.isUpdate() || resultBean.isUpdatePic()) {
					finalBean.setId(oldId);
					finalBean.setMerchantId(oldMerchantId);
					finalBean.setUpdateDate(newUpdateDate);
					resultBean.setPdb(finalBean);
				}
				
				resultBean.setExist(true);
				
			}else {
				// same name with difference url
				resultBean.setDuplicate(true);
			}
		}
		return resultBean;
	}
	
}