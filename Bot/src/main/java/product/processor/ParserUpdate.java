package product.processor;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.ParserResultBean;
import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import bean.ReportBean;
import bean.SendDataBean;
import db.MerchantDB;
import db.ProductDataDB;
import db.ProductMappingDB;
import db.ProductUrlDB;
import db.SendDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ProcessLogManager;
import manager.ReportManager;
import utils.BotUtil;
import utils.FilterUtil;

public class ParserUpdate {
	private static final Logger logger = LogManager.getRootLogger();
	private static final MerchantDB merchantDB = DatabaseManager.getInstance().getMerchantDB();
	private static final ProductDataDB productDataDB = DatabaseManager.getInstance().getProductDataDB();
	private static final ProductUrlDB productUrlDB = DatabaseManager.getInstance().getProductUrlDB();
	private static final SendDataDB sendDataDB = DatabaseManager.getInstance().getSendDataDB();
	private static final ProductMappingDB mapDB = DatabaseManager.getInstance().getProductMappingDB();
	private static final ReportBean report = ReportManager.getInstance().getReport();
	private static boolean CHECKOUT;
	private static ProcessLogManager processLog  = ProcessLogManager.getInstance();

	public static ParserResultBean analyzeProduct(ProductDataBean newBean) throws SQLException {
		if(!baseValidate(newBean))
			return null;
		
		ProductDataBean oldBean = null;
		if(BaseConfig.CONF_IGNORE_NAME_CHANGE) 
			oldBean = productDataDB.getProductByUrlForUpdate(BaseConfig.MERCHANT_ID, StringUtils.defaultIfBlank(newBean.getUrlForUpdate(), newBean.getUrl()));
		else
			oldBean = productDataDB.getProduct(BaseConfig.MERCHANT_ID, newBean.getName());
		
		return analyze(newBean, oldBean);
	}
	
	public static ParserResultBean analyzeProduct(ProductDataBean newBean, ProductDataBean oldBean) throws SQLException {
		if(!baseValidate(newBean))
			return null;
		return analyze(newBean, oldBean);
	}
	
	public static boolean baseValidate(ProductDataBean newBean) {
		if (newBean == null)
			return false;
		
		String newName = newBean.getName();
		double newPrice = newBean.getPrice();
		String newUrl = newBean.getUrl();
		if(isBlank(newName) || newPrice < 1 || isBlank(newUrl)
				|| !FilterUtil.checkAllowCharacter(newName,BotUtil.LOCALE) || !FilterUtil.checkAllowCharacter(newUrl,BotUtil.LOCALE)) {
			if(isBlank(newName)) 
				processLog.addNameIsBlank(StringUtils.isBlank(newBean.getUrl())?"":newBean.getUrl()+" "+(StringUtils.isBlank(newBean.getRealProductId())?"":"("+newBean.getRealProductId()+")"),newName);
			if(newPrice == 0 )
				processLog.addPriceZero((StringUtils.isBlank(newName)?"":newName)+" "+(StringUtils.isBlank(newBean.getUrl())?"":newBean.getUrl())+" "+(StringUtils.isBlank(newBean.getRealProductId())?"":"("+newBean.getRealProductId()+")"),String.valueOf(newPrice));
			if(isBlank(newUrl))
				processLog.addUrlIsBlank((StringUtils.isBlank(newName)?"":newName)+" "+(StringUtils.isBlank(newBean.getRealProductId())?"":"("+newBean.getRealProductId()+")"), newUrl);
			
			return false;
		}
		
		String pictureUrl = newBean.getPictureUrl();
		if(pictureUrl != null) {
			if(pictureUrl.startsWith("http")) {
				pictureUrl = pictureUrl.replace(" ", "%20");
				newBean.setPictureUrl(pictureUrl);
			}else {
				newBean.setPictureUrl(null);
			}
		}
		
		convertContent(newBean);
		return true;
	}
	
	public static void convertContent(ProductDataBean newBean) {
		String name = BotUtil.limitString(BotUtil.removeLongSpace(newBean.getName()), ProductDataDB.NAME_LENGTH);
		String description = BotUtil.limitString(newBean.getDescription(), ProductDataDB.DESCRIPTION_LENGTH);
		String keyword = BotUtil.limitString(newBean.getKeyword(), ProductDataDB.KEYWORD_LENGTH);
		
		if(!isBlank(name)) 
			name = name.trim();
		if(!isBlank(description)) 
			description = description.trim();
		if(!isBlank(keyword)) 
			keyword = keyword.trim();
		
		newBean.setName(name);
		newBean.setDescription(description);
		newBean.setKeyword(keyword);
	}
	
	@SuppressWarnings("unchecked")
	public static ParserResultBean analyze(ProductDataBean newBean, ProductDataBean oldBean) throws SQLException { 
		if (oldBean == null || !oldBean.getStatus().equals(STATUS.W.toString())) {
			processLog.addCantFindOldBean((StringUtils.isBlank(newBean.getUrl())?"":newBean.getUrl()+":")+(StringUtils.isBlank(newBean.getUrlForUpdate())?"":newBean.getUrlForUpdate()+":")+(StringUtils.isBlank(newBean.getRealProductId())?"":newBean.getRealProductId()),newBean.getName());
			return null;
		}

		boolean isUpdate = false;
		boolean isUpdatePic = false;
		
		String newName = newBean.getName();
		double newPrice = newBean.getPrice();
		double newBasePrice = newBean.getBasePrice();
		String newUrl = newBean.getUrl();
		String newUrlForUpdate = newBean.getUrlForUpdate();
		String newDesc = newBean.getDescription();
		String newRealId = newBean.getRealProductId();
		String newDynamicField = newBean.getDynamicField();
		String newUPC = newBean.getUpc();
		int newCategoryId = newBean.getCategoryId();
		String newKeyword = newBean.getKeyword();

		double oldPrice = oldBean.getPrice();
		double oldBasePrice = oldBean.getBasePrice();
		String oldUrl = oldBean.getUrl();
		String oldUrlForUpdate = oldBean.getUrlForUpdate();
		String oldDesc = oldBean.getDescription();
		String oldRealId = oldBean.getRealProductId();
		String oldUPC = oldBean.getUpc();
		int oldCategoryId = oldBean.getCategoryId();
		String oldKeyword = oldBean.getKeyword();
		
		if(isBlank(newUrlForUpdate)) {
			newUrlForUpdate = newUrl;
			newBean.setUrlForUpdate(newUrlForUpdate);
		}
		if(!newUrlForUpdate.equalsIgnoreCase(oldUrlForUpdate)) {
			processLog.addUrlForUpdateChange(oldUrlForUpdate, newUrlForUpdate);
			return null;
		}
		
		if(BaseConfig.CONF_IGNORE_NAME_CHANGE) {
			newBean.setName(oldBean.getName());
			newName = newBean.getName();
		} else {
			if (!newName.equalsIgnoreCase(oldBean.getName())) {
				processLog.addNameIsChange(oldBean.getName(), newName);
				return null;
			}
		}
		
		if(BaseConfig.CONF_USER_OLD_PRODUCT_URL)
			newUrl = oldUrl;
		
		if(!BotUtil.checkStringEqual(oldUrl, newUrl) && !BaseConfig.CONF_ENABLE_UPDATE_URL) {
			processLog.addUrlChange(oldUrl, newUrl);
			return null;
		}
		
		if(BaseConfig.CONF_ENABLE_UPDATE_URL && !BotUtil.checkStringEqual(oldUrl, newUrl))
			isUpdate = true;
		newBean.setUrl(newUrl);
		
		if (BaseConfig.CONF_CURRENCY_RATE != 1.0) {
			newPrice = newPrice * BaseConfig.CONF_CURRENCY_RATE;
			newPrice = Double.valueOf(BotUtil.FORMAT_PRICE.format(newPrice));

			if(newBasePrice > 0) {
				newBasePrice = newBasePrice * BaseConfig.CONF_CURRENCY_RATE;
				newBasePrice = Double.valueOf(BotUtil.FORMAT_PRICE.format(newBasePrice));
			}
		}
		
		if(newPrice > BotUtil.CONTACT_PRICE) 
			newPrice = BotUtil.CONTACT_PRICE;
		
		if(newBasePrice > 0 && newBasePrice < newPrice)
			newBasePrice = newPrice;
		
		newBean.setPrice(newPrice);
		newBean.setBasePrice(newBasePrice);
		
		boolean updateP = checkPrice(newBean, oldPrice, newPrice);
		boolean updateBP = checkBasePrice(newBean, oldBasePrice, newBasePrice, newPrice);
		if(updateP || updateBP)
			isUpdate = true;
		
		if(isBlank(oldDesc))
			oldDesc = null;
		if(isBlank(newDesc)) {
			newDesc = null;
			newBean.setDescription(newDesc);
		}
		if((oldDesc == null && newDesc != null)
				|| (BaseConfig.CONF_ENABLE_UPDATE_DESC && !BotUtil.checkStringEqual(oldDesc, newDesc, ProductDataDB.DESCRIPTION_LENGTH))) {
			isUpdate = true;
		}else {
			newBean.setDescription(oldDesc);
		}
		
		if(isBlank(oldRealId))
			oldRealId = null;
		if(isBlank(newRealId)) {
			newRealId = null;
			newBean.setRealProductId(newRealId);
		}
		if(oldRealId == null && newRealId != null) {
			isUpdate = true;
		}else {
			newBean.setRealProductId(oldRealId);
		}
		
		if(isBlank(oldUPC))
			oldUPC = null;
		if(isBlank(newUPC)) {
			newUPC = null;
			newBean.setUpc(newUPC);
		}
		if(oldUPC == null && newUPC != null) {
			isUpdate = true;
		}else {
			newBean.setUpc(oldUPC);
		}
		
		if(isBlank(oldKeyword))
			oldKeyword = null;
		if(isBlank(newKeyword)) {
			newKeyword = null;
			newBean.setKeyword(newKeyword);
		}
		
		if(!isBlank(newDynamicField)) {
			isUpdate = true;
		}
		
		if(BaseConfig.CONF_FORCE_UPDATE_CATEGORY || BaseConfig.CONF_FORCE_UPDATE_KEYWORD) {
			if((oldKeyword!=null && newKeyword!=null &&!BotUtil.checkStringEqual(oldKeyword, newKeyword)) || (oldCategoryId != newCategoryId && newCategoryId > 0)) {
				isUpdate = true;
				if(oldCategoryId != newCategoryId) {
					JSONObject dynamicField = new JSONObject();
					if(!isBlank(newDynamicField)) {
		        		try {
		        			dynamicField = (JSONObject) new JSONParser().parse(newDynamicField);
		        		} catch(Exception e) {
		        			processLog.addException(e.toString()+"-ParserUpdate", e);
	        			}
	        		}
					dynamicField.put("oldCategory", Integer.toString(oldCategoryId));
					newBean.setDynamicField(dynamicField.toJSONString());
				}
			}else {
        		newBean.setKeyword(oldKeyword);  
        		newBean.setCategoryId(oldCategoryId);
        	}
		}else {
			if(oldCategoryId == 0 && newCategoryId > 0) {
				isUpdate = true;
				JSONObject dynamicField = new JSONObject();
				if(!isBlank(newDynamicField)) {
	        		try {
	        			dynamicField = (JSONObject) new JSONParser().parse(newDynamicField);
	        		} catch(Exception e) {
	        			processLog.addException(e.toString()+"-ParserUpdate", e);
        			}
        		}
				dynamicField.put("oldCategory", Integer.toString(oldCategoryId));
				newBean.setDynamicField(dynamicField.toJSONString());
        		newBean.setKeyword(newKeyword);
			}else {
				newBean.setCategoryId(oldCategoryId);
				if (oldKeyword == null && newKeyword != null) {
					isUpdate = true;
				} else {
					newBean.setKeyword(oldKeyword);
				}
         	}
		}
		
		byte[] imageByte = null;
		String oldPic = oldBean.getPictureUrl();
		if(isBlank(oldPic))
			oldPic = null;
		String newPic = newBean.getPictureUrl();
		if(isBlank(newPic)) {
			newPic = null;
			newBean.setPictureUrl(newPic);
		}
		if(newPic != null 
				&& ((oldPic == null || (!BotUtil.checkStringEqual(oldPic, newPic) && BaseConfig.CONF_ENABLE_UPDATE_PIC) || BaseConfig.CONF_FORCE_UPDATE_IMAGE))) {
			imageByte = BaseConfig.IMAGE_PARSER.parse(newPic, BaseConfig.IMAGE_WIDTH, BaseConfig.IMAGE_HEIGHT);
			if(imageByte != null) {
    			isUpdatePic = true;
			}else {
				logger.warn("Parse : Cannot Parse Image : " + newPic);
			}
		}
		
		if(!isUpdatePic)
			newBean.setPictureUrl(oldPic);
		
		newBean.setId(oldBean.getId());
		newBean.setMerchantId(BaseConfig.MERCHANT_ID);
		newBean.setErrorUpdateCount(oldBean.getErrorUpdateCount());
		newBean.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		
		ParserResultBean rb = new ParserResultBean();
		
		if(newBean.isExpire())
			rb.setExpire(true);
		
		rb.setPdb(newBean);
		rb.setImageByte(imageByte);
		rb.setUpdate(isUpdate);
		rb.setUpdatePic(isUpdatePic);
		return rb;
	}
	
	public static void update(ParserResultBean rb) throws SQLException {
		ProductDataBean pdb = rb.getPdb();
		
		if(rb.isUpdatePic()) {
			SendDataBean sendDataBean = SendDataBean.createSendDataBean(pdb, rb.getImageByte(), SendDataBean.ACTION.UPDATE_PIC);
			sendDataDB.insertSendData(sendDataBean);
			report.increaseUpdatePic();
		}
		
		if(rb.isUpdate()) {
			SendDataBean sendDataBean = SendDataBean.createSendDataBean(pdb, null, SendDataBean.ACTION.UPDATE);
			sendDataDB.insertSendData(sendDataBean);
			report.increaseUpdate();
		}
		
		int id = pdb.getId();
		if(rb.isUpdatePic() || rb.isUpdate()) {
			pdb.setErrorUpdateCount(0);
			productDataDB.updateProductData(id, pdb);
		}else {
			productDataDB.updateStatusAndErrorUpdateCount(id, STATUS.C, 0);
		}
		report.increaseSuccessCount();
	}
	
	public static void deleteProduct(ProductDataBean pdb) throws SQLException {
		int productId = pdb.getId();
		SendDataBean sendDataBean = SendDataBean.createSendDataBean(pdb, null, SendDataBean.ACTION.DELETE);
        sendDataDB.insertSendData(sendDataBean);
        productUrlDB.clearProductUrl(BaseConfig.MERCHANT_ID, pdb.getUrlForUpdate());
        productDataDB.delete(productId);
        mapDB.delete(pdb.getMerchantId(), productId);
        report.increaseDelete();
	}
	
	public static void updateErrorCount(ProductDataBean pdb, int errorUpdateCount) throws SQLException {
		productDataDB.updateStatusAndErrorUpdateCount(pdb.getId(), STATUS.E, errorUpdateCount);
	}
	
	public synchronized static boolean deleteLimit() throws SQLException {
		if(report.getDelete() < BaseConfig.CONF_DELETE_LIMIT )
			return false;
		if(!CHECKOUT) {
			String msg = "exceed delete limit = " + BaseConfig.CONF_DELETE_LIMIT;
			ReportManager.getInstance().markStatus(ReportBean.STATUS.EXCEED);
			ReportManager.getInstance().getReport().setExceed(true);
			merchantDB.updateActiveErrorMessage(BaseConfig.MERCHANT_ID, 2, msg);
			logger.warn("Web : Exceed delete limit :" + BaseConfig.CONF_DELETE_LIMIT);
			BaseConfig.MERCHANT_ACTIVE = 2;
			CHECKOUT = true;
		}
		return true;
	}

	public static boolean checkPrice(ProductDataBean pdb, double oldPrice, double newPrice) {
		boolean isUpdate = false;
		if (newPrice != oldPrice) {
			if (BaseConfig.CONF_UPDATE_PRICE_PERCENT > 0 && newPrice < BotUtil.CREDIT_BASE_PRICE && oldPrice < BotUtil.CREDIT_BASE_PRICE) {
				int percentDiff = BotUtil.calPercentDiff(oldPrice, newPrice);
				if (Math.abs(percentDiff) <= BaseConfig.CONF_UPDATE_PRICE_PERCENT) {
					isUpdate = true;
				}else {
					pdb.setPrice(oldPrice);
				}
			} else {
				isUpdate = true;
			}
		}
		return isUpdate;
	}
	
	public static boolean checkBasePrice(ProductDataBean pdb, double oldBasePrice, double newBasePrice, double newPrice) {
		boolean isUpdate = false;
		if(((oldBasePrice == 0 && newBasePrice > 0) || (oldBasePrice != newBasePrice && (BaseConfig.CONF_ENABLE_UPDATE_BASE_PRICE))) 
				&& newBasePrice <= BotUtil.CREDIT_BASE_PRICE && newPrice < BotUtil.CONTACT_PRICE) {
			isUpdate = true;
		}else {
			pdb.setBasePrice(oldBasePrice);
		}
		
		if(oldBasePrice > 0 && (newPrice == BotUtil.CONTACT_PRICE || newBasePrice > BotUtil.CREDIT_BASE_PRICE)){
			pdb.setBasePrice(0);
			isUpdate = true;
		}
		return isUpdate;
	}
}