package product.processor;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.MerchantBean;
import bean.MerchantBean.STATE;
import bean.ProductDataBean.STATUS;
import db.MerchantDB;
import db.ProductDataDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import manager.ReportManager;

public class ProductPreparing {
	private static final Logger logger = LogManager.getRootLogger();
	private static final MerchantDB merchantDB = DatabaseManager.getInstance().getMerchantDB();
	private static final ProductDataDB productDataDB = DatabaseManager.getInstance().getProductDataDB();
	
	public static void setRunning() throws SQLException {
		merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.SET_PRODUCT, MerchantBean.STATUS.RUNNING);
		if(BaseConfig.RUNTIME_TYPE.equals("WCE")) {
			logger.info("Product : Count product");
			ReportManager.getInstance().countOldProduct();
			return;
		}
		
		logger.info("Product : Set running");
		int index = 0;
		final int offset = 200000, offsetSubset = 100;
		STATUS[] statusType = {STATUS.C, STATUS.E, STATUS.R};
		for(STATUS status : statusType) {
			boolean loop = true;
			boolean first = true;
			while(loop) {
				if(!first)
					logger.info("Product : Process : "+ index);
				first = false;
				List<Integer> ids = productDataDB.getByMerchantStatus(BaseConfig.MERCHANT_ID, status, offset);
				if(ids.isEmpty())
					break;
				if(ids.size() < offset)
					loop = false;
				while(!ids.isEmpty()) {
					int last = offsetSubset;
					int size = ids.size();
					if(size < offsetSubset)
						last = size;
					List<Integer> idsSubSet = ids.subList(0, last);
					index += idsSubSet.size();
					productDataDB.updateStatus(STATUS.W, idsSubSet);
					ids.subList(0, last).clear();
				}
			}
		}
		logger.info("Product : Finish : "+ index);
		merchantDB.updateStateStatus(BaseConfig.MERCHANT_ID, STATE.SET_PRODUCT, MerchantBean.STATUS.FINISH);
	}
}
