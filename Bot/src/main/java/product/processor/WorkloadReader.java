package product.processor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import db.WorkloadDB;
import db.manager.DatabaseManager;
import engine.BaseConfig;
import unittest.MockObjectHolder;

public class WorkloadReader {
	private static final Logger logger = LogManager.getRootLogger();
	private List<WorkloadBean> workloads = new ArrayList<WorkloadBean>();
	private WorkloadDB workloadDB = DatabaseManager.getInstance().getWorkloadDB();
	private int PROCESS;
	private int OFFSET = 100;
	private boolean FINISH;
	
	private static class SingletonHelper {
        private static final WorkloadReader INSTANCE = new WorkloadReader();
    }
	
	public static WorkloadReader getInstance(){
		if(MockObjectHolder.isMock) {
			return MockObjectHolder.workloadReader;
		}
		return SingletonHelper.INSTANCE;
	}
	
	private boolean loadData() {
		while(!Thread.currentThread().isInterrupted()){
			try {
				workloads = workloadDB.getMinDepth(BaseConfig.MERCHANT_ID, OFFSET);
				
				if(workloads.isEmpty()) {
					workloads = workloadDB.getByStatus(BaseConfig.MERCHANT_ID, STATUS.R);
					if(workloads.isEmpty()) {
						FINISH = true;
						return false;
					} else {
						Thread.sleep(5000);
						continue;
					}
				}
				
				List<String> workloadList = workloads.stream().map(WorkloadBean::getUrl).collect(Collectors.toList());
				workloadDB.updateStatusBatch(BaseConfig.MERCHANT_ID, STATUS.R, workloadList);
				return true;
			}catch(SQLException e) {
				logger.error("Web : Get Workload Error : ", e);
			}catch(Exception e) {
				logger.error("Web : Get Workload Error : ", e);
				return false;
			}
		}
		return false;
	}
	
	public synchronized WorkloadBean readData() {
		while(!Thread.currentThread().isInterrupted()) {
			if(!workloads.isEmpty()) {
				PROCESS++;
				if(PROCESS % 1000 == 0)
					logger.info("Web : Workload process : "+ PROCESS);
				WorkloadBean b = workloads.get(0);
				workloads.remove(0);
				return b;
			}
			
			if(FINISH) {
				logger.info("Web : Finish Workload Process : "+ PROCESS);
				return null;
			}
			
			if(!loadData()) {
				logger.info("Web : Finish Workload Process : "+ PROCESS);
				FINISH = true;
				return null;
			}
		}
		return null;
		
	}
}
