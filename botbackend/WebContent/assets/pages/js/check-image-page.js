function testImage(){
	var url =  $('#url').val();
	var header = "Sorry!"
	var icon = "&#xf00d;";
	var status = "danger";
	var message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>พบข้อผิดพลาด</p></div>";
	var imageData = "";
	$.ajax({
		type: "POST",
		url: 'checkImage',
		data: "url="+url,
		cache: false,
		async: false,
	  	success: function(data) {
	  	  var json = $.parseJSON(data);
		  	if(json.header=='success'){
		  		var obj = json.data;
		  		if(obj!=null){
		  			header = "Complete!";
					icon = "&#xf00c;";
		  			status = "success";
		  			imageData = obj.data;
		  			message = '<img src="data:image/png;base64,'+obj + '"/>';
		  		}
		  	}
		  	else{
	  			header = "Error!";
	  			icon = "&#xf00d;";
	  			message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>"+json.message+"</p></div>";
	  		}
	  	},
		complete: function() {
	  		generateModalResponse(icon, header, message, status)
		  	$('#responseModal').modal('show');
	  		$('#url').button('reset');
		},
	});
}

function clearUrl(){
	$('#url').val('');
}

function generateModalResponse(icon, header, message, status){
	var rowData = "";
		rowData += "<div class='modal-header'>";
		rowData += "	<div class='icon-box "+status+"'>";
		rowData += "		<i class='fa material-icons'>"+icon+"</i>";
		rowData += "	</div>";
		rowData += "	<h4 class='modal-title'>"+header+"</h4>";
		rowData += "</div>";
		rowData += message;
		rowData += "<div class='modal-footer'>";
		rowData += "<button class='btn btn-"+status+" btn-block' data-dismiss='modal'>OK</button>";
		rowData += "</div>";
		
	$('.modal-content').html(rowData);
}