$(document).ready(function() {
	$('.iconSpin').parent().hide();
});

function saveNewItem(btn){
	
	$(btn).button('loading');
	var countIndex = $(btn).closest(".recordRow").find(".recordRowCount").text();
	var data = {}
	data["webProductId"] = $("#inputWebProductId-"+countIndex).val();
	 
	$.ajax({
		type: "POST",
		url: 'focusupdate-saveitem',
		data: JSON.stringify(data),
		contentType: 'application/json',
		cache: false,
		async: true,
	  	success: function(data) {
	  		if(data.header == 'error-invalid-input'){
	  			modalAlert('เลข ProductId ที่กรอกมานั้นไม่ถูกต้อง ลองตรวจสอบใหม่อีกครั้ง');
	  		}else if(data.header == 'error-productid-out-of-range'){
	  			modalAlert('เลข ProductId ที่กรอกมานั้นไม่ถูกต้อง ลองตรวจสอบใหม่อีกครั้ง');
	  		}else if(data.header == 'duplicate-active-web-product-id'){
	  			modalAlert('เลข ProductId นี้ซ้ำกับ Record ที่มีอยู่ก่อนแล้ว');
	  		}else if(data.header == 'not-found-web-product'){
	  			modalAlert('ไม่พบสินค้าตาม ID ที่กรอกมา ลองตรวจสอบใหม่อีกครั้ง');
	  		}else if(data.header == 'not-found-bot-product-all-server'){
	  			modalAlert('ไม่พบสินค้าใน Dababase ของ Bot. <br>สินค้าอาจจะไม่ได้ถูกอัพเดทผ่านระบบ Bot หรือเกิดปัญหาในระหว่าง Sync ข้อมูล ');
	  		}else if(data.header == 'not-found-bot-merchant'){
	  			modalAlert('ไม่พบร้านค้าใน Database ของ Bot. <br>ร้านค้านั้นอาจจะไม่ถูกอัพเดทด้วยระบบ Bot.');
	  		}else if(data.header == 'success'){
	  			var currentCount = parseInt($("#currentAllCount").text());
	  			$("#currentAllCount").text(currentCount+1);
	  			
	  			$("#merchantId-"+countIndex).text(data.result.merchantId);
	  			$("#webProductId-"+countIndex).text(data.result.webProductId);
	  			$("#webProductName-"+countIndex).text(data.result.webProductName);
	  			$("#botProductId-"+countIndex).text(data.result.botProductId);
	  			$("#botServerName-"+countIndex).text(data.result.botServerName);
	  			$("#addBy-"+countIndex).text(data.result.addBy);
	  			$("#addDate-"+countIndex).text("NOW");
	  			$("#systemNote-"+countIndex).text("-");
	  			setTimeout(function() {
	  				$(btn).prop("disabled", true);
	  			}, 200);
	  		}
	  		$(btn).button('reset');
	  	},
	  	error: function(xhr, status, error) {
	  		var err = JSON.parse(xhr.responseText);
	  		$(btn).button('reset');
	  		alert(err);
	  	}
	});
}

function deleteItem(btn){
	
	$(btn).button('loading');
	var countIndex = $(btn).closest(".recordRow").find(".recordRowCount").text();
	var data = {}
	data["webProductId"] = $("#webProductId-"+countIndex).text();
	data["botProductId"] = $("#botProductId-"+countIndex).text();
	 
	$.ajax({
		type: "POST",
		url: 'focusupdate-deleteitem',
		data: JSON.stringify(data),
		contentType: 'application/json',
		cache: false,
		async: true,
	  	success: function(data) {
	  		if(data.header == 'delete-error'){
	  			modalAlert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
	  		}
	  		if(data.header == 'success'){
	  			var currentCount = parseInt($("#currentAllCount").text());
	  			$("#currentAllCount").text(currentCount-1);
	  			
	  			$("#merchantId-"+countIndex).text('-');
	  			$("#webProductId-"+countIndex).html('<input type="text" id="inputWebProductId-'+countIndex+'" style="width: 100%">');
	  			$("#webProductName-"+countIndex).text('-');
	  			$("#botProductId-"+countIndex).text('-');
	  			$("#botServerName-"+countIndex).text('-');
	  			$("#addBy-"+countIndex).text('-');
	  			$("#addDate-"+countIndex).text('-');
	  			$("#lastUpdateDate-"+countIndex).text('-');
	  			$("#systemNote-"+countIndex).text('-');
	  			setTimeout(function() {
	  				$(btn).closest(".recordRow").find(".saveNewItemBtn").prop("disabled", false);
	  			}, 200);
	  		}
	  		$(btn).button('reset');
	  	},
	  	error: function(xhr, status, error) {
	  		var err = JSON.parse(xhr.responseText);
	  		$(btn).button('reset');
	  		alert(err);
	  	}
	});
	
}

function modalAlert(result) {
	$('#modalAlertData').html('<center><span style="color:black;font-size:15px;">'+result+'</span></center>');
	$('#modalAlert').modal('show');
}



