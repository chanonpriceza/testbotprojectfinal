	$(document).ready(function() {
		
		getDataByMerchantId();
		openTab("Pattern");
		
		$('#ApproveBtn').click(function() {
			$('#selectServer').modal('show');
		});
		$('#FormReset').click(function() {
			clear();
		});
		
		$('#selectServer').on('hide.bs.modal', function() {
			clear();
		});
		
		$('#selectServerForm input').keypress(function(event){
			  if (event.which == '13') {
				  event.preventDefault();
			   }

		});
	});
	var action = {"Approve":1, "Reject":2, "Retest":3}
	
	/*pattern tab */
	var conditions = {
			'' : 1,
			'haveString' : 2,
			'haveQuery' : 3,
			'endsWith' : 4
	}	
	var actions = {
			'STARTURL' : 1,
			'CONTINUE' : 2,
			'MATCH' : 3,
			'DROP' : 4,
			'REMOVE' : 5,
			'FEED' : 6
	}
	
	function addPatternRow(){
		var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
		$('#urlPatternTable > tbody').append(rowData);
		assignEditable();
	}
	function insertPatternRow(btn){
		var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
		$(btn).parent().parent().before(rowData)
		assignEditable();
	}
	function deletePatternRow(btn){
		$(btn).parent().parent().remove();
	}
	function disableNotStarturl() {
		var rows = $('#urlPatternTable').find('tr');
		var rowsNo = $(rows).length;
		
		for(var r = 1 ; r < rowsNo - 1 ; r++) {
			var action = $(rows).eq(r).find('.rowAction').text();
			var condition = $(rows).eq(r).find('.rowCondition').text();
			
			$(rows).eq(r).find('.rowAction').editable('setValue', actions[action]);
			$(rows).eq(r).find('.rowCondition').editable('setValue', conditions[condition]);
			if( action == 'FEED'){
				$(rows).eq(r).find('.rowCheck').prop('checked', false);
				$(rows).eq(r).find('.rowCheck').attr('disabled', true);
			}else if( action != 'STARTURL') {
				$(rows).eq(r).find('.rowCheck').prop('checked', true);
				$(rows).eq(r).find('.rowCheck').attr('disabled', true);
			}
		}
	}
	function assignPatternTable(json, patternData){
		var merchantId = json.merchantId;
		if(patternData != null && patternData.length > 0){
	  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
			for(var i=0; i<patternData.length; i++){
			var jObject = patternData[i];
				var condition = (jObject.condition ? jObject.condition : '');
				var group = (jObject.group ? jObject.group : '0');
				var value = (jObject.value ? jObject.value : '');
				var action = (jObject.action ? jObject.action : 'STARTURL');
				var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
				var keyword = (jObject.keyword ? jObject.keyword : '');
				
				var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
				$('#urlPatternTable > tbody').append(rowData);
			}
  		}else {
			var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
				$('#urlPatternTable > tbody').append(rowData);
	  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
  		}
			assignEditable();
			disableNotStarturl();
	}	
	function generateRowPattern(rowCon, rowGro, rowVal, rowAct, rowCat, rowKey){
		var rowData = "";
  		rowData += "<tr class='rowPattern'>";
  		rowData += "	<td width='7%'  style='text-align:center;'><a href='#' class='rowCondition'>"+rowCon+"</a></td>";
  		rowData += "	<td width='5%'  style='text-align:center;'><a href='#' class='rowGroup'>"+rowGro+"</a></td>";
  		rowData += "	<td width='33%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowValue'>"+rowVal+"</a></td>";
  		rowData += "	<td width='8%'  style='text-align:center;'><a href='#' class='rowAction':>"+rowAct+"</a></td>";
  		rowData += "	<td width='5%'  style='text-align:center;'><a href='#' class='rowCatId'>"+rowCat+"</a></td>";
  		rowData += "	<td width='30%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowKeyword'>"+rowKey+"</a></td>";
  		rowData += "	<td width='12%' style='text-align:center;'>";
  		rowData += 			"<button type='button' class='btn btn-md btn-primary' onclick='insertPatternRow(this)'>Insert</button>";
  		rowData += 			"<button type='button' class='btn btn-md btn-danger' onclick='deletePatternRow(this)'>Del<i class='fa fa-trash' aria-hidden='true'></i></button></button>";
  		rowData += "	</td>";
  		rowData += "</tr>";
  		
		return rowData;
	}	
	function getPatternJsonForm(){
		var jsonObj = [];
	    var jsonString;
	    var table = document.getElementById("urlPatternTable");
	    for (var r = 1, n = table.rows.length; r < n-1; r++) {

	        var item = {};        	    	
	    	
	    	item ["condition"] 	= ( table.rows[r].cells[0].textContent && table.rows[r].cells[0].textContent != 'Empty' ? table.rows[r].cells[0].textContent : "");
	    	item ["group"] 		= ( table.rows[r].cells[1].textContent && table.rows[r].cells[1].textContent != 'Empty' ? table.rows[r].cells[1].textContent : "0");
	    	item ["value"] 		= ( table.rows[r].cells[2].textContent && table.rows[r].cells[2].textContent != 'Empty' ? table.rows[r].cells[2].textContent : "");
	    	item ["action"] 	= ( table.rows[r].cells[3].textContent && table.rows[r].cells[3].textContent != 'Empty' ? table.rows[r].cells[3].textContent : "STARTURL");
	    	item ["categoryId"] = ( table.rows[r].cells[4].textContent && table.rows[r].cells[4].textContent != 'Empty' ? table.rows[r].cells[4].textContent : "0");
	    	item ["keyword"] 	= ( table.rows[r].cells[5].textContent && table.rows[r].cells[5].textContent != 'Empty' ? table.rows[r].cells[5].textContent : "");

	        jsonObj.push(item);
	    }
	    jsonString = JSON.stringify(jsonObj);
		jsonString = encodeURIComponent(jsonString);
		
		return jsonString;
	}
	function savePatternTable(dataTable) {
		$("#saveResult").html("");
		
		var merchantId = $('#merchantId').val();
	    var jsonString = getPatternJsonForm();
	    var param = "cmd=saveTable&merchantId="+merchantId+"&jsonData="+jsonString;
	    
	    $('#savePatternTableBtn').button('loading');

	    $.ajax({
			type: "POST",
			url: 'urlpatternAjax',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success-empty'){
			  		$("#saveResult").html("<br><p class='text-success'>Save complete. Row with 'Empty' value were not save.</p>");
			  		modalAlert('Save complete. Row with \'Empty\' value were not save.', true);
			  	}else if(json.header == 'success') {
			  		$("#saveResult").html("<br><p class='text-success'>Save complete</p>");
			  		modalAlert('Save complete', true);
			  	}else if(json.header == 'fail-not-allow'){
			  		if(json.detail != null){
			  			var detail = "<table class=\"table table-bordered\"><thead><tr class=\"heading\"><th style=\"text-align:center;\">Value</th><th style=\"text-align:center;\">CategoryId</th></tr></thead><tbody>";
			  			var detailArr = json.detail;
			  			for(var i=0; i<detailArr.length; i++){
			  				var detailObj = detailArr[i];
			  				detail += "<tr>";
			  				detail += "    <td style=\"text-align:center;\">" + detailObj.value + "</td>";
			  				detail += "    <td style=\"text-align:center;\">" + detailObj.categoryId + "</td>";
			  				detail += "</tr>";
			  			}
			  			detail += "</tbody></table>";
			  			modalAlert('<b>Error, Save incomplete !!</b> Found Category was not allow <br>Please edit data and submit again.<br><br>' + detail , false);
			  		}
			  	}else{
			  		$("#saveResult").html("<br><b>Error, Save incomplete !!</b>");
			  		modalAlert('<b>Error, Save incomplete !!</b>', false);
			  	}
			  	window.setTimeout(function(){
			  		$("#saveResult").html("");
			 	}, 10000);
			  	$('#savePatternTableBtn').button('reset');
		  	},
		  	error: function (textStatus, errorThrown) {
		  		$("#saveResult").html("<br><p class='text-danger'>Error, Save incomplete !!</p>");
				alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
				$('#savePatternTableBtn').button('reset');
		  	}
		});
	}
	
	function getDataByMerchantId(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		$('#urlConfigTable > tbody').html('');
		$("#saveResult").html("");
		$.ajax({
			type: "POST",
			url: 'checkapprovalajax',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success') {
			  		var patternData = json.patternData;
			  		assignPatternTable(json, patternData)
			  	}else{
 				  	$("#urlConfigContent").css("display", "none");
 				  	$("#testUrlContent").css("display", "none");
			  		$("#targetMerchantIdTxt").html("");
			  		modalAlert("Do not have this merchant in the system", false);
			  	}
		  	},
		  	error: function (textStatus, errorThrown) {
			  	$("#urlConfigContent").css("display", "none");
			  	$("#testUrlContent").css("display", "none");
			  	modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
		  	}
		});
		
	}
	
	function modalAlert(result, success){
		if(success){
			$('#modalAlertTitle').html('<h3 style="color:#64FE2E;"><i class="fa fa-check"></i>&nbsp;Success</h3>');
		}else{
			$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
		}
		$('#modalAlertData').html('<span style="color:black;font-size:17px;">'+result+'</span>');
		$('#modalAlert').modal('show');
	}
	
	function updateMerchant(){

		var merchantId = $('#merchantId').val();
		var data = getUpdatedData();
		var param = "merchantId="+merchantId+"&data="+data;
		
		 $.ajax({
				type: "POST",
				url: 'updatemerchant',
				data: param, 
				cache: false,
				async: true,
			  	success: function(json) {
			  		if(json.header == 'success') {
				  		modalAlert("Success complete", true);
				  		var msg = "";
				  		 $("div[id*='Shard']").each(function () {
				 	    	
				  			$(this).find('#cActive').val($(this).find('#active').val());
					    	$(this).find('#cServername').val($(this).find('#servername').val());
					    	$(this).find('#cDataserver').val($(this).find('#dataservername').val());
					    	$(this).find('#cMessage').val($(this).find('#message').val());
					    	msg = msg.concat($(this).find('#cMessage').val()?("|"+$(this).find('#cMessage').val()):"");
				 	     });
				  		 msg = msg?msg.substring(1):"-";
				  		$("#errormsg").html("");
				  		$("#errormsg").append(": &nbsp;");
				  		$("#errormsg").append(msg);
				  	}else{
				  		modalAlert(json.message, false);
				  	}
		
			  	},
			  	error: function (textStatus, errorThrown) {
			  		$("#saveResult").html("<br><p class='text-danger'>Error, Process incomplete !!</p>");
					alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			  	},
			  	complete: function() {
			  		$('#selectServer').modal('hide');
		        }
			  	
			});
	}
	function clear(){
		 $("div[id*='Shard']").each(function () {
		    	$(this).find('#active').val($(this).find('#cActive').val());
		    	$(this).find('#servername').val($(this).find('#cServername').val());
		    	$(this).find('#dataservername').val($(this).find('#cDataserver').val());
		    	$(this).find('#message').val($(this).find('#cMessage').val());
		    	
		     });
	}
	function getUpdatedData(){
		var jsonObj = [];
	    var jsonString;
	    $("div[id*='Shard']").each(function () {
	    	$(this).find('#shardNo');
	    	var item = {};

	    	item ["shardNo"] 	= $(this).find('#shardNo').val();
	    	item ["active"] 	= $(this).find('#active').val();
	    	item ["server"] 	= $(this).find('#servername').val();
	    	item ["dataServer"] 	= $(this).find('#dataservername').val();
	    	item ["errorMessage"] 	= $(this).find('#message').val();
	    	
	    	jsonObj.push(item);
	     });
	   
	    jsonString = JSON.stringify(jsonObj);
		jsonString = encodeURIComponent(jsonString);
		
		return jsonString;
	}
	function assignEditable(){
	    /*pattern editable field*/
	    $('.rowCondition').editable({
			type: 'select',
	        title: 'Select action',
	        emptyclass: 'text-danger',
	        value: 1,
	        source: [
	            {value: 1, text: ''},
	            {value: 2, text: 'haveString'},
	            {value: 3, text: 'haveQuery'},
	            {value: 4, text: 'endsWith'},
	        ]
	    });
	    
	    $('.rowGroup').editable({
	    	type: 'text',
	        title: 'Enter Group',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
        	
	    });
	    
	    $('.rowValue').editable({
	    	type: 'text',
	        title: 'Enter Value',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
	    });
	    
	    $('.rowAction').editable({
	    	type: 'select',
	        title: 'Select action',
	        emptyclass: 'text-danger',
	        value: 1,
	        source: [
	            {value: 1, text: 'STARTURL'},
	            {value: 2, text: 'CONTINUE'},
	            {value: 3, text: 'MATCH'},
	            {value: 4, text: 'DROP'},
	            {value: 5, text: 'REMOVE'},
	            {value: 6, text: 'FEED'}
	        ],
	        success: function(response, newValue) {
	        	if(newValue == 6) {
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', true);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', false);
	        	}else if(newValue == 1) {
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', false);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', false);
	        		$(this).closest('tr').find('.rowCondition').editable('setValue', conditions[""]);
	        	} else {
	        		if(newValue == 5) {
	        			$(this).closest('tr').find('.rowCondition').editable('setValue', conditions["haveQuery"]);
	        		}
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', true);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', true);
	        	}
        		$(this).closest('tr').find('.rowCheck').parent().css('background-color', '');
        		var oldValue = actions[$(this).text()];
        		if(oldValue == 1 && newValue > oldValue) {
        			updateClickTest(-1);
        		} else {
        			updateClickTest();
        		}
		    }
	    });
	    
	    $('.rowCatId').editable({
	    	type: 'text',
	        title: 'Enter CatId',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
	    });
	    
	    $('.rowKeyword').editable({
	    	type: 'text',
	        title: 'Enter Keyword',
	        emptyclass: 'text-danger'
	    });
	    
	}	
	function toggleShow(btn){
		var buttonText = $(btn).text();
		var bodyDiv = $(btn).closest(".portlet-title").next();
		if(buttonText == "HIDE"){
			$(bodyDiv).css("display","none");
			$(btn).text("SHOW");
		}else if(buttonText == "SHOW"){
			$(bodyDiv).css("display","block");
			$(btn).text("HIDE");
		}
	}
	function openTab(value){
		var tab = value;
		if(tab ==  'Pattern'){
			$('#pTab').addClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').removeClass('active');
			$('.patternTab').show();
			$('.workloadTab').hide();
			$('.productUrlTab').hide();
			$('.productDataTab').hide();
		}else if(tab == 'Workload'){
			$('#pTab').removeClass('active');
			$('#wTab').addClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').removeClass('active');
			$('.patternTab').hide();
			$('.workloadTab').show();
			$('.productUrlTab').hide();
			$('.productDataTab').hide();
		}
		else if(tab == 'productUrl'){
			$('#pTab').removeClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').addClass('active');
			$('#dTab').removeClass('active');
			$('.patternTab').hide();
			$('.workloadTab').hide();
			$('.productUrlTab').show();
			$('.productDataTab').hide();
		}
		else if(tab == 'productData'){
			$('#pTab').removeClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').addClass('active');
			$('.patternTab').hide();
			$('.workloadTab').hide();
			$('.productUrlTab').hide();
			$('.productDataTab').show();
		}
	}