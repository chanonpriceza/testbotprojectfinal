window.onload = function(e){ 		
	var parameter_index = window.location.href.indexOf('=');
	if(parameter_index != -1){
		var merchantId = window.location.href.slice(parameter_index + 1);
		$('#merchantId').val(merchantId);
		getDataByMerchantId();
	}
}

	$(document).ready(function() {
		
		$('#merchantId').keypress(function (e) {
			  if (e.which == 13) {
				  getDataByMerchantId();
			  }
		});
		    
		$.fn.editable.defaults.mode = 'popup';
		
		$("#importPatternForm").submit(function (event) {
			importPattern(event);
	    });
		
		$('.file-upload-wrap').bind('dragover', function () {
			$('.file-upload-wrap').addClass('image-dropping');
		});
		
		$('.file-upload-wrap').bind('dragleave', function () {
			$('.file-upload-wrap').removeClass('image-dropping');
		});
		
		$("#inputPatternFile").on('change',function(){
		    readURL(this);
		});
		
		$("#btnGenerateContinue").on('click',function(){
			generateUrl(this,"CONTINUE");
		});
		
		$("#btnGenerateMatch").on('click',function(){
			generateUrl(this,"MATCH");
		});
		
		$('#showBtnGenerate').css("display", "none");
	});
	
	var conditions = {
			'' : 1,
			'haveString' : 2,
			'haveQuery' : 3,
			'endsWith' : 4
	}
	
	var actions = {
			'STARTURL' : 1,
			'CONTINUE' : 2,
			'MATCH' : 3,
			'DROP' : 4,
			'REMOVE' : 5,
			'FEED' : 6
	}
	
	function filterSearchAnalysis(el){
		var value = $(el).val().toLowerCase();
	    $("#tbodyUrlAnalazer tr").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	    countTableAnalysis();
	}
	
	function readURL(input) {
		  if (input.files && input.files[0]) {

		    var reader = new FileReader();

		    reader.onload = function(e) {
		      $('.file-upload-wrap').hide();
		      $('.file-upload-content').show();

		      $('.image-title').html(input.files[0].name);
		    };

		    reader.readAsDataURL(input.files[0]);

		  } else {
		    removeUpload();
		  }
		}

	function removeUpload() {
	  $('.file-upload-input').val('').clone(true);
	  $('.file-upload-content').hide();
	  $('.file-upload-wrap').show();
	  $('.file-upload-wrap').removeClass('image-dropping');
	}
	
	function assignEditable(){
		
		$('.rowCondition').editable({
			type: 'select',
	        title: 'Select action',
	        emptyclass: 'text-danger',
	        value: 1,
	        source: [
	            {value: 1, text: ''},
	            {value: 2, text: 'haveString'},
	            {value: 3, text: 'haveQuery'},
	            {value: 4, text: 'endsWith'},
	        ]
	    });
	    
	    $('.rowGroup').editable({
	    	type: 'text',
	        title: 'Enter Group',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
        	
	    });
	    
	    $('.rowValue').editable({
	    	type: 'text',
	        title: 'Enter Value',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
	    });
	    
	    $('.rowAction').editable({
	    	type: 'select',
	        title: 'Select action',
	        emptyclass: 'text-danger',
	        value: 1,
	        source: [
	            {value: 1, text: 'STARTURL'},
	            {value: 2, text: 'CONTINUE'},
	            {value: 3, text: 'MATCH'},
	            {value: 4, text: 'DROP'},
	            {value: 5, text: 'REMOVE'},
	            {value: 6, text: 'FEED'}
	        ],
	        success: function(response, newValue) {
	        	if(newValue == 6) {
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', true);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', false);
	        	}else if(newValue == 1) {
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', false);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', false);
	        		$(this).closest('tr').find('.rowCondition').editable('setValue', conditions[""]);
	        	} else {
	        		if(newValue == 5) {
	        			$(this).closest('tr').find('.rowCondition').editable('setValue', conditions["haveQuery"]);
	        		}
	        		$(this).closest('tr').find(".rowCheck").attr('disabled', true);
	        		$(this).closest('tr').find(".rowCheck").prop('checked', true);
	        	}
        		$(this).closest('tr').find('.rowCheck').parent().css('background-color', '');
        		var oldValue = actions[$(this).text()];
        		if(oldValue == 1 && newValue > oldValue) {
        			updateClickTest(-1);
        		} else {
        			updateClickTest();
        		}
		    }
	    });
	    
	    $('.rowCatId').editable({
	    	type: 'text',
	        title: 'Enter CatId',
	        emptyclass: 'text-danger',
        	validate: function(value) {
         		if($.trim(value) == '') return 'This field is required';
        	}
	    });
	    
	    $('.rowKeyword').editable({
	    	type: 'text',
	        title: 'Enter Keyword',
	        emptyclass: 'text-danger'
	    });
	    
	}
	
	function getDataByMerchantId(){
		
		var merchantId = $('#merchantId').val().trim();
		var param = "cmd=getDataByMerchantId&merchantId="+ merchantId;
		$('#urlPatternTable > tbody').html('');
		$('#tableMerchantId').val(merchantId);
		$("#saveResult").html("");
		$('#btnTopRunTest').text('RUN TEST');
		removeUpload();
		$.ajax({
			type: "POST",
			url: 'urlpatternAjax',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success') {
			  		if(json.mode == 'update') {
						$("#urlPatternContent").css("display", "block");
						$("#testUrlContent").css("display", "block");
				  		var data = json.result;
				  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
	 					for(var i=0; i<data.length; i++){
							var jObject = data[i];
							
	 						var condition = (jObject.condition ? jObject.condition : '');
	 						var group = (jObject.group ? jObject.group : '0');
	 						var value = (jObject.value ? jObject.value : '');
	 						var action = (jObject.action ? jObject.action : 'STARTURL');
	 						var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
	 						var keyword = (jObject.keyword ? jObject.keyword : '');
	 						
	 						var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
	 						$('#urlPatternTable > tbody').append(rowData);
	 					}
			  		}else if(json.mode == 'insert') {
			  			$("#urlPatternContent").css("display", "block");
						$("#testUrlContent").css("display", "block");
						
						var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
 						$('#urlPatternTable > tbody').append(rowData);
				  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
 						
			  		}
			  		$('#hdMerchantId').val(merchantId);
 					assignEditable();
 					disableNotStarturl();
			  	}else{
 				  	$("#urlPatternContent").css("display", "none");
 				  	$("#testUrlContent").css("display", "none");
			  		$("#targetMerchantIdTxt").html("");
			  		$('#hdMerchantId').val("");
 				  	alert("Fail to get merchant pattern data.");
			  	}
		  	},
		  	error: function (textStatus, errorThrown) {
			  	$("#urlPatternContent").css("display", "none");
			  	$("#testUrlContent").css("display", "none");
			  	$('#hdMerchantId').val("");
			  	alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
		
	}
	
	function generateRowPattern(rowCon, rowGro, rowVal, rowAct, rowCat, rowKey){
		
		var rowData = "";
  		rowData += "<tr class='rowPattern'>";
  		rowData += "	<td width='7%' data-col-name=\"Condition\" style='text-align:center;'><a href='#' class='rowCondition'>"+rowCon+"</a></td>";
  		rowData += "	<td width='5%'  data-col-name=\"Group\" style='text-align:center;'><a href='#' class='rowGroup'>"+rowGro+"</a></td>";
  		rowData += "	<td width='33%' data-col-name=\"Value\" style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowValue'>"+rowVal+"</a></td>";
  		rowData += "	<td width='8%'  data-col-name=\"Action\" style='text-align:center;'><a href='#' class='rowAction':>"+rowAct+"</a></td>";
  		rowData += "	<td width='5%'  data-col-name=\"CatTd\" style='text-align:center;'><a href='#' class='rowCatId'>"+rowCat+"</a></td>";
  		rowData += "	<td width='26%' data-col-name=\"Keyword\" style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowKeyword'>"+rowKey+"</a></td>";
  		rowData += "	<td width='4%' style='text-align:center;'><input type='checkbox' class='rowCheck' onclick='clickTest(this)'></input></td>";
  		rowData += "	<td width='12%' style='text-align:center;'>";
  		rowData += 			"<button type='button' class='btn btn-md btn-primary' onclick='insertPattern(this)'>Insert</button>";
  		rowData += 			"<button type='button' class='btn btn-md btn-danger' onclick='deletePattern(this)'>Del<i class='fa fa-trash' aria-hidden='true'></i></button></button>";
  		rowData += "	</td>";
  		rowData += "</tr>";
  		
		return rowData;
	}
	
	function getAllData(){
		var jsonObj = [];
	    var jsonString;
	    var table = document.getElementById("urlPatternTable");
	    for (var r = 1, n = table.rows.length; r < n-1; r++) {

	        var item = {};        	    	
	    	
	    	item ["condition"] 	= ( table.rows[r].cells[0].textContent && table.rows[r].cells[0].textContent != 'Empty' ? table.rows[r].cells[0].textContent : "");
	    	item ["group"] 		= ( table.rows[r].cells[1].textContent && table.rows[r].cells[1].textContent != 'Empty' ? table.rows[r].cells[1].textContent : "0");
	    	item ["value"] 		= ( table.rows[r].cells[2].textContent && table.rows[r].cells[2].textContent != 'Empty' ? table.rows[r].cells[2].textContent : "");
	    	item ["action"] 	= ( table.rows[r].cells[3].textContent && table.rows[r].cells[3].textContent != 'Empty' ? table.rows[r].cells[3].textContent : "STARTURL");
	    	item ["categoryId"] = ( table.rows[r].cells[4].textContent && table.rows[r].cells[4].textContent != 'Empty' ? table.rows[r].cells[4].textContent : "0");
	    	item ["keyword"] 	= ( table.rows[r].cells[5].textContent && table.rows[r].cells[5].textContent != 'Empty' ? table.rows[r].cells[5].textContent : "");

	        jsonObj.push(item);
	    }
	    jsonString = JSON.stringify(jsonObj);
		jsonString = encodeURIComponent(jsonString);
		
		return jsonString;
	}
	
	function importPattern(event){
		
		event.preventDefault();
		$('#submitImportPattern').button('loading');
		
        var data = new FormData();
        var merchantId = $('#tableMerchantId').val();
        jQuery.each(jQuery('#inputPatternFile')[0].files, function(i, file) {
            data.append('inputPatternFile', file);
        });
        
        $.ajax({
            url: 'urlpatternAjax',
            type: 'POST',
            data: data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
            	if(json.header == 'success') {
		  			var data = json.result;
		  			$('#urlPatternTable > tbody').html('');
					$("#urlPatternContent").css("display", "block");
					$("#testUrlContent").css("display", "block");
			  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
 					for(var i=0; i<data.length; i++){
						var jObject = data[i];
						
 						var condition = (jObject.condition ? jObject.condition : '');
 						var group = (jObject.group ? jObject.group : '0');
 						var value = (jObject.value ? jObject.value : '');
 						var action = (jObject.action ? jObject.action : 'STARTURL');
 						var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
 						var keyword = (jObject.keyword ? jObject.keyword : '');
 						
 						var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
 						$('#urlPatternTable > tbody').append(rowData);
 					}
 					assignEditable();
 					disableNotStarturl();
 					modalAlert('Import pattern successful, do not forget to save.');
			  	}else{
			  		modalAlert(json.errorMsg);
			  	}
            	$('#submitImportPattern').button('reset');
            },
            error: function (textStatus, errorThrown) {
			  	alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			  	$('#submitImportPattern').button('reset');
		  	}
        });

        return false;
	}
	
	function exportPattern(){
		var merchantId = $('#tableMerchantId').val();
	    var jsonString = getAllData();
	    var param = "cmd=exportPattern&merchantId="+ merchantId+"&jsonData="+jsonString;
	    var filename = "Pattern-"+merchantId+" @ "+generateDate()+".csv";
	    var d = new Date();
	    
	    $.ajax({
			type: "POST",
			url: 'urlpatternAjax',
			data: param,
			cache: false,
			async: true,
			headers: {'Accept': 'application/csv'},
//			beforeSend: function (xhr) {
//			    xhr.setRequestHeader("Content-Length", jsonString.length);
//			},
		  	success: function(data) {
		  		downloadToFile(data, filename, 'application/csv')
		  	},
		  	error: function (textStatus, errorThrown) {
				alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
		
	}
	
	function saveTable(dataTable) {
		
		$("#saveResult").html("");
		
		var merchantId = $('#tableMerchantId').val();
	    var jsonString = getAllData();
	    var param = "cmd=saveTable&merchantId="+merchantId+"&jsonData="+jsonString;
	    
	    $('#saveTableBtn').button('loading');
	    var hdmerchantId = $('#hdMerchantId').val();
		if(!hdmerchantId){
			modalAlert("Merchant Data Not Found.");
			setButton(false);
			$('#saveTableBtn').button('reset');
			return;
		}
	    $.ajax({
			type: "POST",
			url: 'urlpatternAjax',
			data: param,
			cache: false,
			async: true,
//			beforeSend: function (xhr) {
//			    xhr.setRequestHeader("Content-Length", jsonString.length);
//			},
		  	success: function(json) {
			  	if(json.header == 'success-empty'){
			  		$("#saveResult").html("<br><p class='text-success'>Save complete. Row with 'Empty' value were not save.</p>");
			  		modalAlert('Save complete. Row with \'Empty\' value were not save.');
			  	}else if(json.header == 'success') {
			  		$("#saveResult").html("<br><p class='text-success'>Save complete</p>");
			  		modalAlert('Save complete');
			  	}else if(json.header == 'fail-not-allow'){
			  		if(json.detail != null){
			  			var detail = "<table class=\"table table-bordered\"><thead><tr class=\"heading\"><th style=\"text-align:center;\">Value</th><th style=\"text-align:center;\">CategoryId</th></tr></thead><tbody>";
			  			var detailArr = json.detail;
			  			for(var i=0; i<detailArr.length; i++){
			  				var detailObj = detailArr[i];
			  				detail += "<tr>";
			  				detail += "    <td style=\"text-align:center;\">" + detailObj.value + "</td>";
			  				detail += "    <td style=\"text-align:center;\">" + detailObj.categoryId + "</td>";
			  				detail += "</tr>";
			  			}
			  			detail += "</tbody></table>";
			  			modalAlert('<p class="text-danger"><b>Error, Save incomplete !!</b></p>Found Category was not allow <br>Please edit data and submit again.<br><br>' + detail);
			  		}
			  	}else{
			  		$("#saveResult").html("<br><p class='text-danger'><b>Error, Save incomplete !!</b></p>");
			  		modalAlert('<b>Error, Save incomplete !!</b>');
			  	}
			  	window.setTimeout(function(){
			  		$("#saveResult").html("");
			 	}, 10000);
			  	$('#saveTableBtn').button('reset');
		  	},
		  	error: function (textStatus, errorThrown) {
		  		$("#saveResult").html("<br><p class='text-danger'>Error, Save incomplete !!</p>");
				alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
				$('#saveTableBtn').button('reset');
		  	}
		});
	}
	
	function addPattern(){
		var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
		$('#urlPatternTable > tbody').append(rowData);
		assignEditable();
	}
	
	function insertPattern(btn){
		var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
		$(btn).parent().parent().before(rowData)
		assignEditable();
	}
	function selectValueinTable(){
		$("#patternGenerateUrl > tbody tr.rowShowKeyword").each(function() {
			var action =  $(this).closest("tr").find('.rowType').text();
			var value =  $(this).closest("tr").find('.rowValue').text();
			var condition =  $(this).closest("tr").find('.rowAct').text();
			var rowData = generateRowPattern(condition, '0', value, action, '0', '');
			$("#urlPatternTable tbody tr:first").before(rowData);			
		});
		assignEditable();
		disableNotStarturl();
	}
	
	function deletePattern(btn){
		$(btn).parent().parent().remove();
	}
	
	function downloadToFile(content, filename, contentType){
		if(!contentType) contentType = 'application/csv';
		var a = document.createElement('a');
        var blob = new Blob([content], {'type':contentType});
        a.href = window.URL.createObjectURL(blob);
        a.download = filename;
        a.click();
	}
	
	function generateDate(){
		var currentdate = new Date(); 
		var datetime = "";										datetime += currentdate.getFullYear();
		if((currentdate.getMonth()+1)<10)	datetime += "0" ; 	datetime += (currentdate.getMonth()+1);
		if(currentdate.getDate()<10)		datetime += "0" ; 	datetime += (currentdate.getDate());
		datetime += " " ;  
		if(currentdate.getHours < 10)		datetime += "0" ; 	datetime += currentdate.getHours();  
		if(currentdate.getMinutes < 10)		datetime += "0" ; 	datetime += currentdate.getMinutes();  
		if(currentdate.getSeconds < 10)		datetime += "0" ; 	datetime += currentdate.getSeconds();  
		return datetime;
	}
	
	function countStartAction() {
		var patternCheckedRows = $('#urlPatternTable').find('.rowCheck:checked').closest('tr');
		var startActions = 0;
		
		for(var r = 0 ; r < patternCheckedRows.length ; r++) {
			if($(patternCheckedRows).eq(r).find('.rowAction').text() == 'STARTURL') {
				startActions++;
			}
		}
		return startActions;
	}
	
	function countStartUrlAnalysisAction() {
		var patternCheckedRows = $('.tabContent:visible').find("[id^=patternTestAnalysisTable]").find('.rowCheckAnalysis:checked').closest('tr');
		var startActions = 0;
		
		for(var r = 0 ; r < patternCheckedRows.length ; r++) {
				startActions++;
		}
		return startActions;
	}
	
	var CHECKED_LIMIT = 8;
	var CHECKED_LIMIT_ANALYSIS = 10;
	
	function updateClickTest(c = 0) {
		var startActions = countStartAction();
		startActions += c;
		if(startActions > 0) {
			if(startActions <= CHECKED_LIMIT) {
				$('#btnTopRunTest').text('RUN TEST (' + startActions + ')');				
			} else {
				modalAlert('Pattern Testing : Limit ' + CHECKED_LIMIT + ' URLs');
			}
		} else {
			$('#btnTopRunTest').text('RUN TEST');
		}
	}

	function clickTest(el) {
		updateClickTest();
		var startActions = countStartAction();
		if($(el).is(':checked') && startActions <= CHECKED_LIMIT) {
			$(el).parent().css('background-color', '#5cb85c');
		} else {
			$(el).parent().css('background-color', '');
			$(el).attr('checked', false);
		}
	}
	
	function clickTestGenerate(el) {
		updateClickTest();
		var startActions = countStartAction();
		if($(el).is(':checked') && startActions <= CHECKED_LIMIT_ANALYSIS) {
			$(el).parent().css('background-color', '#5cb85c');
		} else {
			$(el).parent().css('background-color', '');
			$(el).attr('checked', false);
		}
	}
	
	function modalAlert(result) {
		$('#modalAlertData').html('<center><span style="color:black;font-size:15px;">'+result+'</span></center>');
		$('#modalAlert').modal('show');
	}
	
	function getTestData() {
		var jsonObj = [];
	    var jsonString;
	    var table = document.getElementById("urlPatternTable");
	    for (var r = 1, n = table.rows.length; r < n-1; r++) {
	    	
	    	if(table.rows[r].cells[6].children[0].checked) {
	        	var item = {};
	        	
	    		item ["condition"] 	= ( table.rows[r].cells[0].textContent && table.rows[r].cells[0].textContent != 'Empty' ? table.rows[r].cells[0].textContent : "");
	    		item ["group"] 		= ( table.rows[r].cells[1].textContent && table.rows[r].cells[1].textContent != 'Empty' ? table.rows[r].cells[1].textContent : "0");
	    		item ["value"] 		= ( table.rows[r].cells[2].textContent && table.rows[r].cells[2].textContent != 'Empty' ? table.rows[r].cells[2].textContent : "");
	    		item ["action"] 	= ( table.rows[r].cells[3].textContent && table.rows[r].cells[3].textContent != 'Empty' ? table.rows[r].cells[3].textContent : "STARTURL");
	    		
	        	jsonObj.push(item);
	    	}
	    }
	    jsonString = JSON.stringify(jsonObj);
		jsonString = encodeURIComponent(jsonString);
		
		return jsonString;
	}
	function getTestDataAnalysis() {
		var jsonObj = [];
		var jsonString;
		var table = $('.tabContent:visible').find("[id^=patternTestAnalysisTable-]");
		$(table).find("tbody > tr").each(function() {
			if( $(this).find('td:eq(0) input').is( ":checked" )){
				console.log($(table).find('td:eq(2)').text());		
				var item = {};
				item ["value"] 		= ( $(this).find('td:eq(2)').text() && $(this).find('td:eq(2)').text() != 'Empty' ? $(this).find('td:eq(2)').text() : "");
				item ["action"] 	= ( $(this).find('td:eq(3)').text() && $(this).find('td:eq(3)').text() != 'Empty' ? $(this).find('td:eq(3)').text() : "");
				
				jsonObj.push(item);
			}
		 });

		jsonString = JSON.stringify(jsonObj);
		jsonString = encodeURIComponent(jsonString);
		
		return jsonString;
	}
	
	function disableNotStarturl() {
		var rows = $('#urlPatternTable').find('tr');
		var rowsNo = $(rows).length;
		
		for(var r = 1 ; r < rowsNo - 1 ; r++) {
			var action = $(rows).eq(r).find('.rowAction').text();
			var condition = $(rows).eq(r).find('.rowCondition').text();
			
			$(rows).eq(r).find('.rowAction').editable('setValue', actions[action]);
			$(rows).eq(r).find('.rowCondition').editable('setValue', conditions[condition]);
			if( action == 'FEED'){
				$(rows).eq(r).find('.rowCheck').prop('checked', false);
				$(rows).eq(r).find('.rowCheck').attr('disabled', true);
			}else if( action != 'STARTURL') {
				$(rows).eq(r).find('.rowCheck').prop('checked', true);
				$(rows).eq(r).find('.rowCheck').attr('disabled', true);
			}
		}
	}
	
	function setButton(type) {
		$('#search').prop("disabled", type);
		$('#btnTopRunTest').prop("disabled", type);
		$('#btnTopSave').prop("disabled", type);
	}
	
	function runTestPattern() {
		var starturlNo = countStartAction();
		setButton(true);
		
		if(starturlNo < 1) {
			modalAlert('Please select at lease 1 URL for testing.');
			setButton(false);
			return;
		}
		var jsonString = getTestData();
		$('#modalData').html('<h1 style="text-align:center;">Running, please wait...</h1>');
		$('#titleModalUrl').text("Run URL Pattern Test");
  		$('#modalRun').modal('show');
  		$("body").css("cursor", "wait");
  		$('#showBtnGenerate').css("display", "none");
		$('#groupBtnCount').css("display", "none");
  		
  		var mId = $('#merchantId').val().trim();
		var param = "obj=" + jsonString + "&mId=" + mId;
		$.ajax({
			type: "POST",
			url: 'runpattern',
			data: param,
			cache: false,
			async: true,
		  	success: function(data) {
			  	if(data.header == 'success'){
			  		var result = "";
			  		
//			  		------------------------------------------------------------generate tab button------------------------------------------------------------
			  		result += '<div class="tab">';
			  		$.each(data.result, function (index, value) {
			  			result += '<button class="btn tabLinks" onclick="openTab('+ index + ')">Link_' + (index +1) + '</button>'; 
			  		});
			  		result += '</div>';
			  		
//			  		------------------------------------------------------------generate tab content------------------------------------------------------------
			  		$.each(data.result, function (index, value) {
			  			result += '<div class="tabContent" id="tabContent_' + index +'">';
			  			result += '    <div class="tabContentHeader"><h4><a href="' + value.starturl +'" target="_blank" style="text-decoration:none;">' + value.starturl + '</a></h4>';
			  			result += '    <div class="runtime">Loading time ' + value.runtime + ' secs</div></div>';
			  			result += '    <div style="margin-top:20px;">';
	                    result += '        <table id="patternTestTable" class="table table-striped table-bordered">';
	                    result += '            <thead>';
	                    result += '                <tr class="heading">';
	                    result += '                    <th width="5%"  style="text-align:center;"> No. </th>';
	                    result += '                    <th width="85%"  style="text-align:center;"> URL </th>';
	                    result += '                    <th width="10%" style="text-align:center;"> Action </th>';
	                    result += '                </tr>';
	                    result += '            </thead>';
	                    result += '            <tbody>';
	                    var testNo = 1;
	                    $.each(value.continueUrl, function(contIndex, contUrl) {
	                    	result += generateRowTest(testNo++, contUrl.url, contUrl.action);
	                    });
	                    $.each(value.matchUrl, function(matchIndex, matchUrl) {
	                    	result += generateRowTest(testNo++, matchUrl.url, matchUrl.action);
	                    });
	                    result += '            </tbody>';
	                    result += '        </table>';
	                    result += '    </div>';
	                    result += '</div>';
			  		});

			  		$('#showBtnGenerate').css("display", "none");
			  		$('#modalData').html(result);
			  		$('#modalRun').modal('show');
			  		openTab(0);
			  	}else if(data.header == 'error'){	
			  		$('#modalRun').modal('hide');
			  		modalAlert('Error : Cannot Run Pattern Testing');
			  	}
			  	setButton(false);
		  		$("body").css("cursor", "default");
		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		$("body").css("cursor", "default");
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
		
	}
	
	function runUrlAnalysis() {
		var starturlNo = countStartAction();
		setButton(true);
		
		if(starturlNo < 1) {
			modalAlert('Please select at lease 1 URL for testing.');
			setButton(false);
			return;
		}
		var jsonString = getTestData();
		$('#modalData').html('<h2 style="text-align:center;">Running, please wait...</h2>');
		$('#titleModalUrl').text("Run Url Analysis");
		$('#modalRun').modal('show');
		$('#showBtnGenerate').css("display", "none");
		$('#groupBtnCount').css("display", "none");
		$("body").css("cursor", "wait");
		
		var mId = $('#merchantId').val().trim();
		var param = "obj=" + jsonString + "&mId=" + mId;
		$.ajax({
			type: "POST",
			url: 'runurlAnalysis',
			data: param,
			cache: false,
			async: true,
			success: function(data) {
				if(data.header == 'success'){
					var result = "";
					
//			  		------------------------------------------------------------generate tab button------------------------------------------------------------
					result += '<div class="tab">';
					$.each(data.result, function (index, value) {
						result += '<button class="btn tabLinks" onclick="openTab('+ index + ')">Link_' + (index +1) + '</button>'; 
					});
					result += '<input id="searchModal" class="form-control" type="text" onkeyup="filterSearchAnalysis(this)" placeholder="Search.." style="float: right;width: 25%;">'; 					
					result += '</div>';
					
//			  		------------------------------------------------------------generate tab content------------------------------------------------------------
					$.each(data.result, function (index, value) {
						result += '<div class="tabContent" id="tabContent_' + index +'" data-index="'+ index +'">';
						result += '    <div class="tabContentHeader"><h4><a href="' + value.starturl +'" target="_blank" style="text-decoration:none;">' + value.starturl + '</a></h4>';
						result += '    <div class="runtime">Loading time ' + value.runtime + ' secs</div></div>';
						result += '    <div style="margin-top:20px;">';
						result += '        <table id="patternTestAnalysisTable-' + index +'" class="table table-bordered">';
						result += '            <thead>';
						result += '                <tr class="heading">';
						result += '                    <th width="5%"  style="text-align:center;"></th>';
						result += '                    <th width="5%"  style="text-align:center;"> No. </th>';
						result += '                    <th width="80%"  style="text-align:center;"> URL </th>';
						result += '                    <th width="10%" style="text-align:center;"> Condition </th>';
						result += '                </tr>';
						result += '            </thead>';
						result += '            <tbody id="tbodyUrlAnalazer">';
						var testNo = 1;
						$.each(value.continueUrl, function(contIndex, contUrl) {
							result += generateRowUrlAnalysis(testNo++, contUrl.url, contUrl.action);
						});
						$.each(value.matchUrl, function(matchIndex, matchUrl) {
							result += generateRowUrlAnalysis(testNo++, matchUrl.url, matchUrl.action);
						});
						$.each(value.dropUrl, function(dropIndex, dropUrl) {
							result += generateRowUrlAnalysis(testNo++, dropUrl.url, dropUrl.action);
						});
						$.each(value.otherUrl, function(otherIndex, otherUrl) {
							result += generateRowUrlAnalysis(testNo++, otherUrl.url, otherUrl.action);
						});
						result += '            </tbody>';
						result += '        </table>';
						result += '    </div>';
						result += '</div>';
					});
					
					$('#modalData').html(result);
					$('#modalRun').modal('show');
					$('#showBtnGenerate').css("display", "block");
					$('#groupBtnCount').css("display", "block");
					openTab(0);
					countTableAnalysis();
				}else if(data.header == 'error'){	
					$('#modalRun').modal('hide');
					modalAlert('Error : Cannot Run Pattern Testing');
				}
				setButton(false);
				$("body").css("cursor", "default");
			},error: function (textStatus, errorThrown) {
				setButton(false);
				$("body").css("cursor", "default");
				alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			}
		});	
	}
	
	function countTableAnalysis(){
		var totalAllRow =  $('.tabContent:visible').find('[id^=patternTestAnalysisTable-] > tbody > tr:visible').length;
		$('#countAll').text("All : " + totalAllRow);
	}
	
	function generateUrl(input,type){
		var starturlNo = countStartUrlAnalysisAction();
		setButton(true);
		
		if(starturlNo < 1) {
			modalAlert('Please select at lease 1 URL for testing.');
			setButton(false);
			return;
		}
		var jsonString = getTestDataAnalysis();
		$('#modalRun').modal('hide');
		$('#modalGenerate').modal('show');
		$('#modalDataGenerate').html('<h1 style="text-align:center;">Running, please wait...</h1>');
		$('#titleModalUrl').text("Run Generate Url");

  		
  		var mId = $('#merchantId').val().trim();
		var param = "obj=" + jsonString + "&mId=" + mId;
		$.ajax({
			type: "POST",
			url: 'rungenerateurl',
			data: param,
			cache: false,
			async: true,
		  	success: function(data) {
			  	if(data.header == 'success'){
			  		var result = "";			  		
//			  		------------------------------------------------------------generate tab content------------------------------------------------------------
			  		$.each(data.result, function (index, value) {
			  			var testNo = 1;
			  			result += '<div class="showGenerate" id="showGenerate">';
			  			result += '    <div class="showContentGenerate">';
			  			result += '    <div style="margin-top:20px;">';
	                    result += '        <table id="patternGenerateUrl" class="table table-striped table-bordered">';
	                    result += '            <thead>';
	                    result += '                <tr class="heading">';
	                    result += '                    <th width="5%"  style="text-align:center;"> No. </th>';
	                    result += '                    <th width="30%"  style="text-align:center;"> Type </th>';
	                    result += '                    <th width="30%" style="text-align:center;"> Keyword </th>';
	                    result += '                    <th width="30%" style="text-align:center;"> Action </th>';
	                    result += '                    <th width="5%" style="text-align:center;"></th>';
	                    result += '                </tr>';
	                    result += '            </thead>';
	                    result += '            <tbody>';
	                    $.each(value.haveStringArr, function(index, value) {
	                    	result += generateRowShowValue(testNo++, value.value, value.condition,type);
	                    });
	                    $.each(value.endwithArr, function(index, value) {
	                    	result += generateRowShowValue(testNo++, value.value, value.condition,type);
	                    });
	                    $.each(value.haveQueryArr, function(index, value) {
	                    	result += generateRowShowValue(testNo++, value.value, value.condition,type);
	                    });
	                    result += '            </tbody>';
	                    result += '        </table>';
	                    result += '    </div>';
	                    result += '</div>';
			  		});

			  		$('#modalDataGenerate').html(result);
			  	}else if(data.header == 'error'){	
			  		$('#modalRun').modal('hide');
			  		modalAlert('Error : Cannot Run Pattern Testing');
			  	}
			  	setButton(false);
		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		$("body").css("cursor", "default");
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
		
	}
	
	function openTab(tabNo) {
		var modalData = $('#modalRun #modalData');
		var tab = $(modalData).find('button');
		for(var t = 0 ; t < $(tab).length ; t++) {
			$(tab).eq(t).removeClass('active');
			$(modalData).find('#tabContent_' + t).css('display', 'none');
		}
		$(tab).eq(tabNo).addClass('active');
		$(modalData).find('#tabContent_' + tabNo).css('display', 'block');
		countTableAnalysis();
	}
	
	function generateRowTest(rowNo, rowVal, rowAct){
		var rowData = "";
  		rowData += "<tr class='rowTest'>";
  		rowData += "	<td width='5%'  style='text-align:center;'><a href='#' class='rowNo'>"+rowNo+"</a></td>";
  		rowData += "	<td width='85%'  style='text-align:left; word-wrap:break-word; max-width:500px;'><a href=\'" + rowVal + "\' class='rowValue' target='_blank'>"+rowVal+"</a></td>";
  		rowData += "	<td width='10%' style='text-align:center;'  ><a href='#' class='rowAction'>"+rowAct+"</a></td>";
  		rowData += "</tr>";
  		
		return rowData;
	}
	
	function generateRowShowValue(rowNo, rowValue, rowAct,rowType){
		var rowData = "";
		rowData += "<tr class='rowShowKeyword'>";
		rowData += "	<td width='5%'  style='text-align:center;'><a href='#' class='rowNo'>"+rowNo+"</a></td>";
		rowData += "	<td width='30%' style='text-align:center;'><a href='#' class='rowType'>"+rowType+"</a></td>";
		rowData += "	<td width='30%' style='text-align:left; word-wrap:break-word; max-width:500px;' class='rowValue'>" + rowValue+"</td>";
		rowData += "	<td width='30%' style='text-align:center;'><a href='#' class='rowAct'>"+rowAct+"</a></td>";
		rowData += "	<td width='5%'  style='text-align:center;'><button type='button' class='btn btn-md btn-danger' onclick='deletePattern(this)'>Delete</button></td>";
		rowData += "</tr>";
		return rowData;
	}
	
	function generateRowUrlAnalysis(rowNo, rowVal, rowAct){
		var rowData = "";
		rowData += setColor(rowAct);
		rowData += "	<td width='5%'  style='text-align:center;'><input type='checkbox' class='rowCheckAnalysis' onclick='clickTestGenerate(this)'></input></td></td>";
		rowData += "	<td width='5%'  style='text-align:center;'><a href='#' class='rowNoAnalysis'>"+rowNo+"</a></td>";
		rowData += "	<td width='80%'  style='text-align:left; word-wrap:break-word; max-width:500px;'><a href=\'" + rowVal + "\' class='rowValueAnalysis' target='_blank'>"+rowVal+"</a></td>";
		rowData += "	<td width='10%' style='text-align:center;'  ><a href='#' class='rowActionAnalysis'>"+rowAct+"</a></td>";
		rowData += "</tr>";
		return rowData;
	}
	
	function setColor(rowAct) {
		var rowData="";
		if(rowAct != null && rowAct=='Match'){
			rowData += "<tr class='rowTest' bgcolor='#9beac9'>";
		}else if(rowAct != null && rowAct == 'Continue'){
			rowData += "<tr class='rowTest' bgcolor='#f0de92'>";
		}else if(rowAct != null && rowAct == 'Drop'){
			rowData += "<tr class='rowTest' bgcolor='#e8988f'>";
		}else{
			rowData += "<tr class='rowTest'>";
		}
		return rowData;
	}
	