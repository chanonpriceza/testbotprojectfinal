$(document).ready(function () {

	$('#dataForm').bootstrapValidator({
		excluded: ':disabled',
		fields: {
        	url: {
        		 validators: {
  	    			notEmpty: {
  		                message: 'Url is required.'
  		            },
  	            }
             }
        }
    }).on('success.form.bv', function(e){
    	$('#load').button('loading');
    	e.preventDefault();
    	runTest();
    });

	$("input").on("change keyup paste", function(){
		var inputclear = $(this).parent().find('span[class="inputclear glyphicon glyphicon-remove-circle"]'); 
		if($(this).val()!=''){
			inputclear.show();
		}else{
			inputclear.hide();
		}
	})
	
	$('#merchantId').keypress(function (e) {
		  if (e.which == 13) {
			  getDataByMerchantId();
		  }
	});
	    
	$.fn.editable.defaults.mode = 'popup';
	
	$("#importCSVForm").submit(function (event) {
		importPattern(event);
  });
	
	$('.file-upload-wrap').bind('dragover', function () {
		$('.file-upload-wrap').addClass('image-dropping');
	});
	
	$('.file-upload-wrap').bind('dragleave', function () {
		$('.file-upload-wrap').removeClass('image-dropping');
	});
	
	$("#inputPatternFile").on('change',function(){
	    readURL(this);
	});
	
	$("#btnGenerateContinue").on('click',function(){
		generateUrl(this,"CONTINUE");
	});
	
	$("#btnGenerateMatch").on('click',function(){
		generateUrl(this,"MATCH");
	});
	
	$('#showBtnGenerate').css("display", "none");
	
 });
var success = {
		"passSSL":"Pass SSL",
		"available":"This website avaliable",
		"nextpage":"Can continue next page",
		"productlink":"Can match product url",
		"producthtml":"Product page available",
		"nameMatch":"Can parse product name",
		"priceMatch":"Can parse price"		
};

var warning = {
		"nextpage":"No data for next page",
		"productlink" : "No data for product url",
		"producthtml":"Product page unavailable",
		"nameMatch":"No data for product name",
		"priceMatch":"No data for price"		
};

var fail = {
		"passSSL":"Cannot Pass SSL",
		"available":"This website unavaliable",
		"nextpage":"Cannot continue next page",
		"productlink":"Cannot match product url",
		"producthtml":"Product page unavailable",
		"nameMatch":"Cannot parse product name",
		"priceMatch":"Cannot parse price"		
};

function runTest(){
	
		var word = $('#word').val();
		var merchantId = $('#merchantId').val();
		var price = $('#price').val();
		var categoryId = $('#categoryId').val();
		
		var header = "Sorry!"
		var icon = "&#xf00d;";
		var status = "danger";
		var message = "";
		
		$.ajax({
			type: "POST",
			url: 'checkbankeywordwithword',
			data: "word="+word+"&merchantId="+merchantId+"&price="+price+"&categoryId="+categoryId,
			cache: false,
			async: true,
		  	success: function(json) {
		  		console.log(json);
			  	if(json.header=='success'){
			  		var obj = json.result;
			  		if(obj.result!=true){
			  			header = "Awesome!"
						icon = "&#xf00c;";
			  			status = "success"
			  			message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>"+"คำนี้ไม่ถูกแบน"+"</p></div>";
			  		}else{
			  			header = "Sorry!";
			  			icon = "&#xf00d;";
			  			message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>"+"คำนี้ถูกแบน"+"</p></div>";
			  		}
			  	}
		  	},
			complete: function() {
		  		generateModalResponse(icon, header, message, status)
			  	$('#responseModal').modal('show');
		  		$('#load').button('reset');
			},
		});
		$("#dataForm").data('bootstrapValidator').resetForm();
}

function generateModalResponse(icon, header, message, status){
	var rowData = "";
		rowData += "<div class='modal-header'>";
		rowData += "	<div class='icon-box "+status+"'>";
		rowData += "		<i class='fa material-icons'>"+icon+"</i>";
		rowData += "	</div>";
		rowData += "	<h4 class='modal-title'>"+header+"</h4>";
		rowData += "</div>";
		rowData += message;
		rowData += "<div class='modal-footer'>";
		rowData += "<button class='btn btn-"+status+" btn-block' data-dismiss='modal'>OK</button>";
		rowData += "</div>";
		
	$('.modal-content').html(rowData);
}

$('#dataFormReset').click(function() {
	$(".inputclear").hide();
	$("#dataForm").bootstrapValidator('resetForm', true);
	$("#word").val('');
	$("#merchantId").val('');
	$("#price").val('');
	$("#categoryId").val('');
});

$(".inputclear").click(function() {
	var input = $(this).parent().find("input");
	input.val('');
	if(input.prop('name') == "url"){
		$('#dataForm').bootstrapValidator('revalidateField', input.prop('name'));
	}
	$(this).hide();
});

$("#showCSV").click(function showCSV(){
	removeUpload();
    $('.file-upload-wrap').show();
	$('#importCSVForm').show();
	$('#dataForm').hide();
});

$("#showInputMode").click(function showCSV(){
	$('#importCSVForm').hide();
	$('.file-upload-content').hide();
	$('#dataForm').show();
});

function readURL(input) {
	  if (input.files && input.files[0]) {

	    var reader = new FileReader();

	    reader.onload = function(e) {
	      $('.file-upload-wrap').hide();
	      $('.file-upload-content').show();

	      $('.image-title').html(input.files[0].name);
	    };

	    reader.readAsDataURL(input.files[0]);

	  } else {
	    removeUpload();
	  }
}

function importPattern(event){
	
	event.preventDefault();
	$('#submitImportPattern').button('loading');
	$("#resultTable td").remove();
	$('#displayTable').show();
    var data = new FormData();
    var merchantId = $('#tableMerchantId').val();
    jQuery.each(jQuery('#inputPatternFile')[0].files, function(i, file) {
        data.append('inputPatternFile', file);
    });
    
    $.ajax({
        url: 'checkbankeywordwithcsv',
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function (json) {
        	if(json.header == 'success') {	
        		var arrayResult = json.result;
        		arrayResult.forEach(function(item, index, array) {
        			var appendString = "";
        			var keyword = item.keyWordList;
        			var minPrice = item.minPrice;
        			var merchantIdList = item.merchantIdList;
        			
        			if(keyword){
        				keyword = "คำที่โดนแบน :"+keyword;
        			}else{
        				keyword = "";
        			}
        			
        			if(minPrice){
        				minPrice = "ราคาต่ำกว่าที่กำหนด :"+minPrice;
        			}else{
        				minPrice = "";
        			}
        			
        			if(merchantIdList&&merchantIdList.length > 0){
        				merchantIdList = "เป็นร้านที่อยู่ในรายการ :"+merchantIdList;
        			}else{
        				merchantIdList = "";
        			}
        			
        			var genDetail = keyword+"<br> "+minPrice+"<br> "+merchantIdList;
        			
        			if(item.result!=true){
        				 appendString = '<tr><td class="left">'+item.word+'</td><td>'+item.merchantId+'</td><td>'+item.price+'</td><td>'+item.categoryId+'</td><td style=\"background-color: #3CB371; color: black;\">ไม่แบน</td><td class=\"left\">'+genDetail+'</td></tr>';
        			}else{
        				 appendString = '<tr><td class="left">'+item.word+'</td><td>'+item.merchantId+'</td><td>'+item.price+'</td><td>'+item.categoryId+"</td><td style=\"background-color: #E74C3C; color: white;\">แบน</td><td class=\"left\">"+genDetail+"</td></tr>";
        			}
        			$('#resultTable tr:last').after(appendString);
        		});
		  	}else {
		  		alert("Error Message : "+json.errorMsg);
		  	}
        	$('#submitImportPattern').button('reset');
        },
        error: function (textStatus, errorThrown) {
		  	alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	$('#submitImportPattern').button('reset');
	  	}
    });

    return false;
}

function removeUpload() {
	  
	  $('#displayTable').hide();
	  $('.file-upload-input').val('').clone(true);
	  $('.file-upload-content').hide();
	  $('.file-upload-wrap').show();
	  $('.file-upload-wrap').removeClass('image-dropping');
	}

