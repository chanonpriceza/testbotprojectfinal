$(document).ready(
	function() {
		$('.rowNote').editable({
			type : 'text',
			title : 'Enter Value',
			emptyclass : 'text-danger',
			emptytext : 'Click to add note',
			ajaxOptions : {
				type : 'post'
			},
			validate : function(value) {
				if (value.length > 200) {
					return "Limit 200 characters."
				}
			},
			success : function(res) {
				if (!res.success) {
					return "Error, Note is not saved.";
				}
			}
		});

		$('#checkAll').click(function() {
			$('input:checkbox').not(this).prop('checked', this.checked);
		});

		$('[data-checkbox-target]').on('click', function(e) {
			var $target = $(e.target);
			var checkGroupName = $target.data('checkbox-target')
			var $checkGroup = $(checkGroupName);
			$checkGroup.find('[type="checkbox"]').prop('checked',
			$target.prop('checked'));
		});

		$('#inputActiveTableHeader').on('change', function() {
			$('#headerSearchActiveType').val($('#inputActiveTableHeader').val());
			$('#headerSearchMerchant').submit();
		});

		$('#inputPackageTableHeader').on('change', function() {
			$('#headerSearchPackageType').val($('#inputPackageTableHeader').val());
			$('#headerSearchMerchant').submit();
		});

		$("#btnFilterSearch").click(function() {
			$("#modalFilterMerchant").slideToggle("slow");
		});
		$("#btnCloseFilter").click(function() {
			$("#modalFilterMerchant").slideToggle("slow");
		});

		// add merchant zone
		$('#addNewMerchant').click(function() {
			clearAddFormModal();
			$('#addFormMerchant').modal('show');
		});
		
		$('#addFormReset').click(function() {
			clearAddFormModal();
		});

		$('#addFormMerchant').on('hide.bs.modal', function() {
			clearAddFormModal();
		});
		
		// edit merchant zone
		$('#editNewMerchant').click(function() {
			clearEditFormModal();
			$('#editFormMerchant').modal('show');
		});
		
		$('#editFormReset').click(function() {
			clearEditFormModal();
		});
		
		$('#editFormMerchant').on('hide.bs.modal', function() {
			clearEditFormModal();
		});
		
		// fav merchant zone
		$('.favoriteMerchant').click(function() {
			ajaxFavoriteMerchant($(this), false);
		});

		$('.nofavoriteMerchant').click(function() {
			ajaxFavoriteMerchant($(this), true);
		});

		$('[data-toggle="tooltip"]').tooltip();

	}
);

function clearAddFormModal(){
	$('#addMerchantName').val('');
	$('#addSiteUrl').val('');
	$('#addPackageType').val('');
	$('#addSaleName').val('');
	$('#addNote').val('');
	$('.errorMsg').remove();
}

function clearEditFormModal(){
	$('#editMerchantName').val('');
	$('#editPackageType').val('');
	$('#editMerchantNote').val('');
	$('#editMerchantID').val('');
	$('#editCurrentserver').val('');
	$('.errorMsg').remove();
}

function showModalEditMerchant(inputMerchantId, inputCurrentServer, webCount){
	$.post('getmerchantdatabyid', 'merchantId='+ inputMerchantId + "&currentserver=" + inputCurrentServer, function(redata) {
		var merchantName = redata.header.name;
		var packageType = redata.header.packageType;
		var note = redata.header.note;
		var server = redata.header.server;
		var dataserver = redata.header.dataserver;
		var ownerName = redata.header.ownerName;
		var active = redata.header.active;
		var merchantId = redata.header.merchantId;
		var currentserver = redata.header.server;
		
		$('#editMerchantName').val(merchantName);
		$('#editPackageType').val(packageType);
		$('#editMerchantNote').val(note); // must fix it
		$('#editServername').val(server);
		$('#editDataservername').val(dataserver);
		$('#editOwnerName').val(ownerName);
		$('#editActive').val(active);
		
		$('#editMerchantID').val(merchantId);
		$('#editCurrentserver').val(currentserver);
		$('#editCurrentDataserver').val(dataserver);
		
		if (active == 1 || webCount > 0) {
			$('#editDataservername').attr('disabled', true);
		}else{
			$('#editDataservername').attr('disabled', false);
		}
		
		$('.additionalDetail').hide();
		$('.serverBox').show();
		$('#editFormMerchant').modal('show');
	});
}

$('a[name="managePattern"]').click(function() {
	var merchantId = $(this).attr("data-element-id");
	window.open("urlpattern?merchantId=" + merchantId, '_blank');
});

$('a[name="manageParser"]').click(function() {
	var merchantId = $(this).attr("data-element-id");
	window.open("parserpage?merchantId=" + merchantId, '_blank');
});

$('a[name="checkApproval"]').click(function() {
	var merchantId = $(this).attr("data-element-id");
	window.open("checkapproval?merchantId=" + merchantId, '_blank');
});

$('a[name="checkMerchant"]').click(function() {
	var merchantId = $(this).attr("data-element-id");
	window.open("checkmerchant?merchantId=" + merchantId, '_blank');
});

function submitMerchant(inputMerchantId, inputMerchantServer){
	$.post('submitmerchant','merchantId=' + inputMerchantId+ "&currentserver=" + inputMerchantServer, function(redata) {
		var warning = redata.warning;
		if(warning != ''){
			$('#modalAlert').modal('show');
			$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
			$('#modalAlertData').html('<span style="color:black;font-size:15px;">'+ warning + '</span>');
		}else{
			$('#textstatus' + inputMerchantId + inputMerchantServer).text("Waiting Test");
			$('#stopNewMerchant' + inputMerchantId + inputMerchantServer).css("display", "block");
			$('#submitNewMerchant' + inputMerchantId + inputMerchantServer).css("display", "none");
		}
	});
}

function stopMerchantShowModal(inputMerchantId, inputMerchantServer, inputErrorMessage){
	$('#stopMerchantId').val(inputMerchantId);
	$('#stopServer').val(inputMerchantServer);
	$('#txtErrorMsg').val(inputErrorMessage);
	$('#modalStop').modal('show');
}

function confirmStop(){
	var errorMessage = document.getElementById("txtErrorMsg").value;
	var merchantId = $('#stopMerchantId').val();
	var currentserver = $('#stopServer').val();
	
	var param = "merchantId=" + merchantId + "&currentserver=" + currentserver + "&errorMessage=" + errorMessage;
	$('#btnSaveErrorMsg').button('loading');
	$.ajax({
		type : "POST",
		url : 'stopmerchant',
		data : param,
		cache : false,
		async : true,
		success : function(data) {
			if (data.header == 'success') {
				$('#textstatus' + merchantId + currentserver).text("Force Stop");
				$('#textErrorMsg' + merchantId + currentserver).text(errorMessage);
				$('#stopNewMerchant' + merchantId + currentserver).css("display", "none");
				$('#submitNewMerchant' + merchantId + currentserver).css("display", "block");
				$('#btnSaveErrorMsg').button('reset');
			} else if (data.header == 'error') {
				$('#modalAlert').modal('show');
				$('#modalAlertTitle').html('<h4 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h4>');
				$('#modalAlertData').html('<span style="color:black;font-size:15px;"> Please try again </span>');
				$('#btnSaveErrorMsg').button('reset');
			}
		},
		error : function(textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" + 'Error Thrown - ' + errorThrown);
		}
	});
}

function convertStringArraytoString(value) {
	var strvalue = "";
	if (value.length > 0 && value != "") {
		for (var i = 0; i < value.length; i++) {
			if (value[i].checked) {
				strvalue += "'" + value[i].value + "'" + ",";
			}
		}
		strvalue = strvalue.substring(0, strvalue.length - 1);
		if (strvalue != "") {
			strvalue = "(" + strvalue + ")";
		}
	}
	return strvalue;
}

function validateInteger(value) {
	var regex = new RegExp('^[0-9]+$');
	if (regex.test(value)) {
		return true;
	} else {
		return false;
	}
}

function submitFilterSearch() {
	event.preventDefault();
	var checkStatus = document.getElementsByName('checkStatus[]');
	var checkOwner = document.getElementsByName('checkOwner[]');
	var checkServername = document.getElementsByName('checkServername[]');
	var checkDataServername = document.getElementsByName('checkDataServername[]');
	var checkLastUpdate = document.getElementsByName('checkLastUpdate[]');
	var firstPackage = $('#inputFirstPackage').val();
	var lastPackage = $('#inputLastPackage').val();
	var status = convertStringArraytoString(checkStatus);
	var owner = convertStringArraytoString(checkOwner);
	var servername = convertStringArraytoString(checkServername);
	var dataservername = convertStringArraytoString(checkDataServername);
	var lastUpdate = convertStringArraytoString(checkLastUpdate);
	var packageFilter = "'" + firstPackage + "','" + lastPackage + "'";
	packageFilter = "(" + packageFilter + ")";
	$('#filterActiveType').val(status);
	$('#filterPackageFilter').val(packageFilter);
	$('#filterServerType').val(servername);
	$('#filterDataserverType').val(dataservername);
	$('#filterUserFilter').val(owner);
	$('#filterUserUpdate').val(lastUpdate);

	$('#searchfilterMerchant').submit();
}

function ajaxFavoriteMerchant(value, $checkStar) {
	var data = value.attr("data-element-id");
	var merchantId = data.split(",")[0];
	var currentServer = data.split(",")[1];
	var statusFavorite = "false";

	if ($checkStar) {
		statusFavorite = "true";
	}

	var param = "merchantId=" + merchantId + "&currentServer=" + currentServer+ "&statusFavorite=" + statusFavorite;
	$.ajax({
		type : "POST",
		url : 'favoritemerchant',
		data : param,
		cache : false,
		async : true,
		success : function(data) {
			if (data.header == 'success') {
				if ($checkStar) {
					$('#favoriteMerchant' + merchantId + currentServer).css('display', 'inline-block');
					$('#nofavoriteMerchant' + merchantId + currentServer).css('display', 'none');
				} else {
					$('#favoriteMerchant' + merchantId + currentServer).css('display', 'none');
					$('#nofavoriteMerchant' + merchantId + currentServer).css('display', 'inline-block');
				}
			}
		},
		error : function(textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" + 'Error Thrown - '
					+ errorThrown);
		}
	});

}

function updateCommandWCE(id,merchantId,currentServer,command,action) {
		var param = "merchantId="+ merchantId +"&command="+command+"&action="+action;
		$('#'+id+merchantId+currentServer).button('loading');
		$.ajax({
			type: "POST",
			url: 'commandmerchant',
			data: param,
			cache: false,
			async: true,
			success: function(data) {
				if(data.header == 'success'){
					setTimeout(function(){
						if(action == "ADD" && command == "PRIMARY_WCE"){
							$('#btnPrimaryQueueWCEWithComamnd'+merchantId+currentServer).css("display", "block");
							$('#btnPrimaryQueueWCE'+merchantId+currentServer).css("display", "none");
						}else if(action == "REMOVE" && command == "PRIMARY_WCE"){ 
							$('#btnPrimaryQueueWCEWithComamnd'+merchantId+currentServer).css("display", "none");
							$('#btnPrimaryQueueWCE'+merchantId+currentServer).css("display", "block");
						}else if(action == "ADD" && command == "KILL_WCE"){ 
							$('#btnKillQueueWCEWithComamnd'+merchantId+currentServer).css("display", "block");
							$('#btnKillQueueWCE'+merchantId+currentServer).css("display", "none");
						}else{
							$('#btnKillQueueWCEWithComamnd'+merchantId+currentServer).css("display", "none");
							$('#btnKillQueueWCE'+merchantId+currentServer).css("display", "block");
						}
						$('#'+id+merchantId+currentServer).button('reset');					  		
				 	}, 2500);
				}else if(data.header == 'error'){
					$('#modalAlert').modal('show');
					$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
					$('#'+id+merchantId+currentServer).button('reset');	
				}
				
			},
			error: function (textStatus, errorThrown) {
				alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
				$('#'+id+merchantId+currentServer).button('reset');	
			}
		});		
	
}

function updateCommandDUE(id,merchantId,currentServer,command,action) {
	var param = "merchantId="+ merchantId +"&command="+command+"&action="+action;
	$('#'+id+merchantId+currentServer).button('loading');
	$.ajax({
		type: "POST",
		url: 'commandmerchant',
		data: param,
		cache: false,
		async: true,
		success: function(data) {
			if(data.header == 'success'){
				setTimeout(function(){
					if(action == "ADD" && command == "PRIMARY_DUE"){
						$('#btnPrimaryQueueDUEWithComamnd'+merchantId+currentServer).css("display", "block");
						$('#btnPrimaryQueueDUE'+merchantId+currentServer).css("display", "none");
					}else if(action == "REMOVE" && command == "PRIMARY_DUE"){ 
						$('#btnPrimaryQueueDUEWithComamnd'+merchantId+currentServer).css("display", "none");
						$('#btnPrimaryQueueDUE'+merchantId+currentServer).css("display", "block");
					}else if(action == "ADD" && command == "KILL_DUE"){ 
						$('#btnKillQueueDUEWithComamnd'+merchantId+currentServer).css("display", "block");
						$('#btnKillQueueDUE'+merchantId+currentServer).css("display", "none");
					}else{
						$('#btnKillQueueDUEWithComamnd'+merchantId+currentServer).css("display", "none");
						$('#btnKillQueueDUE'+merchantId+currentServer).css("display", "block");
					}
					$('#'+id+merchantId+currentServer).button('reset');			  		
			 	}, 2500);		
				
			}else if(data.header == 'error'){
				$('#modalAlert').modal('show');
				$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
				$('#'+id+merchantId+currentServer).button('reset');	
			}
		},
		error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			$('#'+id+merchantId+currentServer).button('reset');
		}
	});		
	
}

function checkDeleteCommand(id,merchantId,dataserver,server,command,action) {
	$('.dropdown-menu').dropdown();
	var param = "merchantId="+merchantId+"&dataserver="+dataserver;
	$('#'+id+merchantId+server).button('loading');
	$.ajax({
		type: "POST",
		url: 'checkmerchantbeforedelete',
		data: param,
		cache: false,
		async: true,
		success: function(data) {
			$('#btnDeleteProduct'+merchantId+server).button('reset');
			if(data.success == true){
			setTimeout(function(){
				if(data.canDelete==true){
					if(command=='DELETE_NOT_MAP'){
						$('#'+id+merchantId+server).toggle();
						$('#btnDeleteNonCat'+merchantId+server).toggle();
						$('#'+id+merchantId+server).button('reset');
					}else{
						$('#'+id+merchantId+server).toggle();
						$('#btnDeleteProduct'+merchantId+server).toggle();
						$('#'+id+merchantId+server).button('reset');
					}
				}else{
					alert(data.message);
					$('#'+id+merchantId+server).button('reset');
				}
			}, 2500);	
			}else if(data.sucuess == false){
				$('#modalAlert').modal('show');
				$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
				$('#'+id+merchantId+server).button('reset');
			}
		}
	});
		
}

function deleteProduct(id,merchantId,currentServer,command,action){
	var param = "merchantId="+ merchantId +"&command="+command+"&action="+action;
	if(action=="ADD"){
	if(confirm('Do you want to delete all product this merchant ?')){
		$.ajax({
			type: "POST",
			url: 'commandmerchant',
			data: param,
			cache: false,
			async: true,
			success: function(data) {
				if(action == "ADD" && command == "DELETE_ALL"){
					$('#btnDeleteProduct'+merchantId+currentServer).button('loading');
					setTimeout(function(){
						$('#btnDeleteProduct'+merchantId+currentServer).button('reset');
						$('#btnWaitingDeleteProduct'+merchantId+currentServer).css("display", "block");
						$('#btnDeleteProduct'+merchantId+currentServer).css("display", "none");
						$('#btnCheckDelete'+merchantId+currentServer).css("display", "none");
						
						$('#btnDeleteNonCat'+merchantId+currentServer).button('reset');
						$('#btnDeleteNonCat'+merchantId+currentServer).css("display", "none");
					}, 2500);
				}
				
				if(action == "ADD" && command == "DELETE_NOT_MAP"){
					$('#btnDeleteNonCat'+merchantId+currentServer).button('loading');
					setTimeout(function(){
						$('#btnDeleteProduct'+merchantId+currentServer).button('reset');
						$('#btnWaitingDeleteProduct'+merchantId+currentServer).css("display", "block");
						$('#btnDeleteProduct'+merchantId+currentServer).css("display", "none");
						$('#btnCheckDelete'+merchantId+currentServer).css("display", "none");
						
						$('#btnDeleteNonCat'+merchantId+currentServer).button('reset');
						$('#btnDeleteNonCat'+merchantId+currentServer).css("display", "none");
					}, 2500);
				}
			}
		});
	}
	}else if(action == "REMOVE"){
		param = "merchantId="+ merchantId +"&command=DELETE_ALL,DELETE_NOT_MAP"+"&action="+action;
		$.ajax({
			type: "POST",
			url: 'commandmerchant',
			data: param,
			cache: false,
			async: true,
			success: function(data) {
				if(action == "REMOVE"){ 
					$('#'+id+merchantId+currentServer).button('loading');
					$('#btnWaitingDeleteProduct'+merchantId+currentServer).button('loading');
					setTimeout(function(){
						$('#btnDeleteProduct'+merchantId+currentServer).css("display", "none");
						$('#btnWaitingDeleteProduct'+merchantId+currentServer).css("display", "none");
						$('#btnCheckDelete'+merchantId+currentServer).css("display", "block");
						$('#btnWaitingDeleteProduct'+merchantId+currentServer).button('reset');
					},2500);
				}
			}
		});
	}
}

function exportMerchant(){
    var param = "exportMerchant";
    var filename = "AllMerchant-@"+generateDate()+".csv";
    var d = new Date();
    
    $.ajax({
		type: "POST",
		url: 'getDataAllMerchantnAjax',
		data: param,
		cache: false,
		async: true,
		headers: {'Accept': 'application/csv'},
//		beforeSend: function (xhr) {
//		    xhr.setRequestHeader("Content-Length", jsonString.length);
//		},
	  	success: function(data) {
	  		downloadToFile(data, filename, 'application/csv')
	  	},
	  	error: function (textStatus, errorThrown) {
			alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
	
}

function downloadToFile(content, filename, contentType){
	if(!contentType) contentType = 'application/csv';
	var a = document.createElement('a');
    var blob = new Blob([content], {'type':contentType});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.click();
}

function generateDate(){
	var currentdate = new Date(); 
	var datetime = "";										datetime += currentdate.getFullYear();
	if((currentdate.getMonth()+1)<10)	datetime += "0" ; 	datetime += (currentdate.getMonth()+1);
	if(currentdate.getDate()<10)		datetime += "0" ; 	datetime += (currentdate.getDate());
	datetime += " " ;  
	if(currentdate.getHours < 10)		datetime += "0" ; 	datetime += currentdate.getHours();  
	if(currentdate.getMinutes < 10)		datetime += "0" ; 	datetime += currentdate.getMinutes();  
	if(currentdate.getSeconds < 10)		datetime += "0" ; 	datetime += currentdate.getSeconds();  
	return datetime;
}
