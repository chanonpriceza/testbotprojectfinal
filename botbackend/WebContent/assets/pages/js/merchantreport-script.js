$(document).ready(function () {
	
	$('#reportType').on('change', function() {
		$('#searchReport').submit();
	});
	
	$('#reportStatus').on('change', function() {
		$('#searchReport').submit();
	});
	
	$('#reportServer').on('change', function() {
		$('#searchReport').submit();
	});
	$('#reportTime').on('change', function() {
		$('#searchReport').submit();
	});

 });

function setTypes (type, name) {
	$('#searchType').val(type);
	$('#filter').text(name).append('&nbsp;<span class="fa fa-caret-down"></span>');
}


