function markRead(){
	var idList = $('#unreadList').val();
	if(idList==null||idList=='')
		return;
	$.ajax({
		type: "POST",
		url: 'service/markRead',
		data: 'idList='+idList,
		cache: false,
		async: true,
		success: function(data) {
			if(data.success == 'true'){
				 $('#unreadList').val('');
				 $('#countReadOnBaddge').hide();
			}
		},
		error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});	
}


