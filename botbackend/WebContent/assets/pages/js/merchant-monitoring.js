
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var redMerchantActive = [0,2,6,7,8,14,15];

$(document).ready(function () {
	getMonitor();
	$('#checkAll').click(function () {    
		$('input:checkbox').not(this).prop('checked', this.checked); 
	 });
	
	 $('[data-checkbox-target]').on('click', function (e) {
         var $target = $(e.target);
         var checkGroupName = $target.data('checkbox-target')
         var $checkGroup = $(checkGroupName);
         $checkGroup
             .find('[type="checkbox"]')
             .prop('checked', $target.prop('checked'));
     });
	
	$("#showMonitoringDetail").hide();

	$('#actionType').change(function() {
		showField();
	});
	
	$('#merchantType').change(function() {
		if($('#merchantType').val() == "none"){
			$("#package").val('');
			$("#packageBox").parent().hide(); 
		}else{
			$("#package").val(1);
			$("#packageBox").parent().show(); 
		}
	});

	$("#chkGz").on("click", function() {
    	if($(this).is(':checked')){
			$(this).attr('checked', 'checked');
			$(this).val(1);
		}else{
			$(this).removeAttr('checked');
			$(this).val(0);
		}
	 });	
	
	$("#buttonback").click(function() {
		$("#showMonitoringDetail").hide();
		$("#monitoringManagerTable").show();
		
		var checkurl = window.location.href;
		if(checkurl.indexOf("merchantmonitoring") > -1 && checkurl.indexOf("id=") > -1){
			var newURL = checkurl.split("?")[0];
			newURL = newURL.substring(newURL.indexOf("botbackend"), newURL.length);
		    window.history.replaceState(null, null, "/"+newURL );
		}
		
		var monitoringId = $("#monitoringId").val();
		if(monitoringId != ""){
			getMonitorById(monitoringId);
		}
		$("#monitoringId").val('');
		
		getMonitor($('#currentCountry').val(),$('#currentPage').val());
	});
	
	
	$(document).on("click",'a[name="idRefMonitor"]',function() {
		var data = $(this).attr("data-element-id");
		var monitoringId = data.split(",")[0];
		var merchantId = data.split(",")[1];
		var type = data.split(",")[2];
		var param =  "cmd=getCommentById&monitoringId="+monitoringId+"&merchantId="+merchantId+"&type="+type;
		
		$("#monitoringId").val(monitoringId);
		commentAjax(param);
		
		$("#showMonitoringDetail").show();
		$("#monitoringManagerTable").hide();
	});
	
	$(document).on("click",'a[name="pagination"]',function() {
		var pageId = $(this).attr("data-element-id");
		getMonitor($('#currentCountry').val(),pageId);
	});
	
	$(document).on("click",'button[name="countrySelector"]',function() {
		$(".active ,button[name=\"countrySelector\"]").removeClass("active");
		//addNewMonitoring
		var currentServer = $('#currentServer').val().toUpperCase();
		var country = $(this).text().toUpperCase();

		$(this).addClass("active");
	});	
	
	$('#inputTopic').on('change', function() {
		$('input[name=cmd]').val("searchMonitorManager");
		$('#topicType').val($('#inputTopic').val());
		getMonitor();
	});

	$('#inputProcesstype').on('change', function() {
		$('input[name=cmd]').val("searchMonitorManager");
		$('#processType').val($('#inputProcesstype').val());
		getMonitor();
	});
	

	
	$('[data-toggle="tooltip"]').tooltip();

	$('#searchMonitorManager').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    var currentPage = $('#currentPage').val();
		    getMonitor($('#currentCountry').val(),currentPage);
		    return false;
		  }
	});
	
	$('#searchfilterMerchant').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    var currentPage = $('#currentPage').val();
		    getMonitor($('#currentCountry').val(),currentPage);
		    return false;
		  }
	});
 });

function loadWindowSystem(){
	var parameter_index = window.location.href.indexOf('=');
	if(parameter_index != -1){
		var monitoringId = window.location.href.slice(parameter_index + 1);
		var merchantId = window.location.href.slice(parameter_index + 2);
		var type = window.location.href.slice(parameter_index + 3);
		var param =  "cmd=getCommentById&monitoringId="+monitoringId+"&merchantId="+merchantId+"&type="+type;
		
		$("#monitoringId").val(monitoringId);
		commentAjax(param);
		
		$("#showMonitoringDetail").show();
		$("#monitoringManagerTable").hide();
	}
}

function setTemplateComment(command){
	var setMsg = "";
	var setStatus = "";
	if(command == "doing"){
		setMsg = "กำลังทำ";
		setStatus = "DOING";
	}else if(command == "waitsync"){
		setMsg = "สินค้ารอซิงค์";
		setStatus = "DOING";
	}else if(command == "syncsuccess"){
		setMsg = "สินค้าซิงค์เสร็จแล้ว";
		setStatus = "RESOLVE";
	}else if(command == "deletesuccess"){
		setMsg = "ลบสินค้าแล้ว";
		setStatus = "RESOLVE";
	}

	$("#commentBox").val(setMsg);
	$("#commentStatus").val(setStatus);
}

function setTypes (type, name) {
	$('#searchType').val(type);
	$('#filter').text(name).append('&nbsp;<span class="fa fa-caret-down"></span>');
}

function convertStringArraytoString(value){
	var strvalue ="";
	if(value.length > 0 && value != ""){
		
		for (var i=0;i<value.length;i++) 
		{
			if (value[i].checked){
				strvalue += "'"+value[i].value+"'"+",";
			}
		}
		strvalue = strvalue.substring(0,strvalue.length-1);
		if(strvalue != ""){
			strvalue = "("+strvalue+")";					
		}	
	}
	return strvalue;
}

function submitFilterSearch() {
	if ($('#checkAll').prop('checked') == false) {
		event.preventDefault();
		var checkProcess = document.getElementsByName('filterCheckType[]');
		var checkTopic = document.getElementsByName('checkTopic[]');

		var processType = convertStringArraytoString(checkProcess);
		var topicType = convertStringArraytoString(checkTopic);
	}

	$('#filterprocessType').val(processType);
	$('#filtertopicType').val(topicType);
	$('#searchfilterMerchant').submit();
}
 
function toggleShow(btn){
	var comment = $(btn).parent().parent().find(".comment-content");
	var status = $(btn).parent().parent().find(".comment-status");
	if($(btn).hasClass("fa fa-angle-down")){
		$(btn).removeClass("fa fa-angle-down").addClass("fa fa-angle-right");
		$(comment).css("display","none");
		$($(btn).parent()).append(status);
	 }else{
		 $(btn).removeClass("fa fa-angle-right").addClass("fa fa-angle-down");
		 $(comment).append(status);
		 $(comment).css("display","block");
	 }
	
}

function commentAjax(param){
	
	var contextPath = genServiceUrl("merchantmonitoring");
	var url = genServiceUrl("infoMonitoringManager");
	
	$.ajax({
		type: "POST",
		url: url,
		data: param,
		xhrFields: {
		      withCredentials: true
		},
		cache: false,
		async: true,
	  	success: function(data) {
	  		if(data.header == 'success'){
	  			var message = $("#commentBox").val('');
	  			
	  			$('#commentTable > tbody').html('');
	  			$(".list-comments").empty();
	  			var dataDetailList = data.dataDetailList;
	  			var bean = data.monitorData;

	  			var merchantId = bean.merchantId;
	  			var merchantName = bean.merchantName;
	  			
	  			
	  			var i = 1;
	  			$("#detailBox").empty();
	  			$("#downloadBox").empty();

  				var tmp ;

				if(merchantId !=  null){
					var context = $("#contextPath").val();
					tmp = "<div class=\"col-md-12\">";  						
					tmp += "<div class=\"col-md-3\" style=\"padding:10px;word-break:break-all;\"><b> MerchantId </b></div>"; 	
					tmp	+="<div class=\"col-md-9\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">";  
					tmp	+= "<a target='_blank' href='"+context+"/admin/getmerchantdata?searchParam="+merchantId+"'>"+merchantId+"</a>";
					tmp	+= "<a target='_blank' href='"+context+"/admin/merchantruntimereport?searchParam="+merchantId+"'><i class='fa fa-desktop fa-lg' aria-hidden='true' style='color: royalblue;margin-left: 10px;'></i></a>"; 
					tmp += "</div><br>";  	
					tmp += "</div>";  	
				}
				
				if(merchantName != null){
					tmp += "<div class=\"col-md-12\">";  						
					tmp += "<div class=\"col-md-3\" style=\"padding:10px;word-break:break-all;\"><b> Merchant Name </b></div>"; 	
					tmp	+="<div class=\"col-md-9\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">"+ merchantName;
					tmp += "</div><br>";  	
					tmp += "</div>";
				}
				
				$("#detailBox").prepend(tmp);
				assignCommentTable(dataDetailList);
	  		}else{
	  			alert('Error Status');
	  		}
	  	},error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

function assignCommentTable(commentData){
//	var merchantId = json.merchantId;
	if(commentData != null && commentData.length > 0){
		for(var i=0; i<commentData.length; i++){
		var jObject = commentData[i];
			var type = (jObject.type ? jObject.type : '-');
			var topic = (jObject.topic ? jObject.topic : '-');
			var detail = (jObject.detail ? jObject.detail : '-');
			var count = (jObject.count ? jObject.count : '0');
			var firstFoundDate = new Date(jObject.firstFoundDate ? jObject.firstFoundDate : '-');
			var recentFoundDate = new Date(jObject.recentFoundDate ? jObject.recentFoundDate : '-');
			var solveDetail = (jObject.solveDetail ? jObject.solveDetail : '-');
			var solveBy = (jObject.solveBy ? jObject.solveBy : '-');

			var firstConvert = firstFoundDate.getDate() + " "+months[firstFoundDate.getMonth()]+" "+firstFoundDate.getFullYear()+" "+firstFoundDate.getHours()+":"+firstFoundDate.getMinutes()+":"+firstFoundDate.getSeconds();
			var recentConvert = recentFoundDate.getDate() + " "+months[recentFoundDate.getMonth()]+" "+recentFoundDate.getFullYear()+" "+recentFoundDate.getHours()+":"+recentFoundDate.getMinutes()+":"+recentFoundDate.getSeconds();
			
			var rowData = generateRowComment(type, topic ,detail, count, firstConvert,recentConvert,solveDetail,solveBy);
			$('#commentTable > tbody').append(rowData);
		}
		}else {
		var rowData = generateRowComment('-', '-', '-', '-', '-', '-', '-', '-');
			$('#commentTable > tbody').append(rowData);
		}
}	

function generateRowComment(type, topic , detail, count, firstFoundDate,recentFoundDate,solveDetail,solveBy){
	var rowData = "";
		rowData += "<tr class='rowComment'>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+type+"</td>";
		rowData += "	<td width='8%'  style='text-align:left;'>"+topic+"</td>";
		rowData += "	<td width='15%' style='text-align:left; max-width: 200px; word-wrap: break-word;'>"+detail+"</td>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+count+"</td>";
		rowData += "	<td width='10%'  style='text-align:center;'>"+firstFoundDate+"</a></td>";
		rowData += "	<td width='10%'  style='text-align:center;'>"+recentFoundDate+"</a></td>";
		rowData += "	<td width='15%' style='text-align:center; max-width: 200px; word-wrap: break-word;'>"+solveDetail+"</td>";
		rowData += "	<td width='10%'  style='text-align:center;'>"+solveBy+"</a></td>";
		rowData += "</tr>";
		
	return rowData;
}


	function setColor(rowStatus) {
		var rowData="";
		if(rowStatus != null && rowStatus=='WAITING'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#AED6F1;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='DOING'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#F9E79F;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='REOPEN'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#EBDEF0;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='HOLD'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#EC7063;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='DONE'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#ABEBC6;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='AUTOREPLY'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#ABB2B9;'>"+rowStatus+"</button>";
		}
		return rowData;
	}
	
	function popUnder(node) {
	    var newWindow = window.open("about:blank", node.target, "width=500,height=500");
	    newWindow.blur();
	    window.focus();
	    newWindow.location.href = node.href;
	    return false;
	}
	
	function isEmpty(str) {
	    return (!str || 0 === str.length);
	}
	
	function getMonitorById(monitoringId){
		var contextPath = genServiceUrl("merchantmonitoring");
		url = genServiceUrl("monitoringmanagerAjax");
		var param =  "cmd=getMonitorById&monitoringId="+monitoringId;
		$.ajax({
			type: "POST",
			url: url,
			data: param,
			cache: false,
			async: true,
			xhrFields: {
			      withCredentials: true
			},
		  	success: function(data) {
		  		if(data.header == 'success'){
		  			var bean = data.data;
		  			
		  			var type = bean.type;
		  			var updated = bean.updatedBy;
		  			var fileName = null;
		  			if(type != null || type != ""){
		  				$("#type"+monitoringId).html(setColor(type));
		  			}
		  			if(updated == null || updated == ""){
		  				$("#updated"+monitoringId).text('-');
		  			}else{
		  				$("#updated"+monitoringId).text(updated);
		  			}
		  			
		  			var lastResult = (bean.result == null)? null:JSON.parse(bean.result);
		  	
	  				if(lastResult&&isEmpty(lastResult.fileName) && (isEmpty(lastResult) || isEmpty(lastResult.message))){
		  				$("#lastResult"+monitoringId).text("-");
		  			}else if(lastResult&&isEmpty(lastResult.fileName) && !isEmpty(lastResult) && !isEmpty(lastResult.message))
		  				$("#lastResult"+monitoringId).html('<a class="fa fa-commenting fa-lg" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="'+encodeHTML(lastResult.message)+'"></a>');
		  			}else{
		  				fileName = lastResult.fileName.replace("<","&lt;").replace(">","&gt;");
		  				var tmp = "<a target='_blank'  onclick='return popUnder(this);' href='"+contextPath+"?cmd=exportCSV&fileName="+fileName+"'><button type='button' class='btn' style='background-color: #62D528;float: right;'><i class='fa fa-download fa-lg' aria-hidden='true' style='color: royalblue;'></i></button></a>";
  		  				$("#lastResult"+monitoringId).html(tmp);
		  			}
		  			$('[data-toggle="tooltip"]').tooltip();
		  			
		  		
		},error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});
	}
	
	function getMonitor(country,page){
		var bot_token = Cookies.get('bot_token');
		if ($('#checkAll').prop('checked') == false) {
			event.preventDefault();
			var checkProcess = document.getElementsByName('filterCheckType[]');
			var checkOwner = document.getElementsByName('checkOwner[]');
			var checkType = document.getElementsByName('checkTopic[]');

			var processType = convertStringArraytoString(checkProcess);
			var owner = convertStringArraytoString(checkOwner);
			var type = convertStringArraytoString(checkType);
		}
		
		var contextPath = $("#contextPath").val();
		
		if(!page){
			page = $('#currentPage').val();
			if(!page){
				page = 1;
			}
		}
		
		if(!country){
			country = $("#currentCountry").val();
			if(!country){
				country = $('#currentServer').val();
				if(!country){
					country = 'TH';
				}
			}
			
		}
		var formParam = $('#searchMonitorManager').serialize().replace("&processType=","&processType="+processType).replace("&topicType=","&topicType="+type)+"&bot_token="+bot_token;
		country = country.toLowerCase();
		var url = $("#service-url-"+country).val();
		
		if(!url){
			url = "merchantMonitoringDataAjax";
		}else{
			url = url+"/admin/merchantMonitoringDataAjax";
		}
		
		var param = "country="+country+"&page="+page+"&"+formParam;
			$.ajax({
				type: "POST",
				url: url,
				data: param,
				xhrFields: {
				      withCredentials: true
				},
				crossDomain:true,
				cache: false,
				beforeSend: function(){
					showLoading();
			    },
			  	success: function(data) {
			  		genRow(data);
			  		addServiceUrl(data);
			  		genPaging(data);
					$("#currentCountry").val(country);
					$('[data-toggle="tooltip"]').tooltip();
					loadWindowSystem();
			  	}	
			 });
	}
	
	function printDate(cDate,notPrintHour){
		var hourString = "";
		if(notPrintHour==null){
			var min = cDate.getMinutes();
			var second = cDate.getSeconds();
			if(min < 10){
				min = "0"+min;
			}
			
			if(second < 10){
				second = "0"+second;
			}
			hourString = " "+cDate.getHours()+":"+min+":"+second+"";
		}
		
		if(!cDate){
			return "-";
		}
		const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		return cDate.getDate()+" "+months[cDate.getMonth()]+" "+cDate.getFullYear()+hourString;
	}
	
	function checkString(input){
		if(input){
			return input;
		}
		return "-";
	}
	
	function genRow(data){
		var contextPath = genServiceUrl("merchantmonitoring");
		var result = "";
		var getData = data.model.monitorList;
		if(!getData){
			$("#monitoringBoxCotainer").html('');
			return false;
		}
		getData.forEach(function(bean, index, array) {
//  			var obj = JSON.parse(bean.data);
  			var active = bean.active;
  			var fDate = new Date(bean.firstFoundDate);
  			var rDate = new Date(bean.recentFoundDate);
//  			var eDate = new Date(obj.duedate);

  			result+="<tr class='rowPattern'>";
  			result+="<td width='5%' style='text-align: center;'>"+checkString(bean.id)+"</td>";
  			result+="<td width='10%' style='text-align: center;'>"
  			result+="<div class=\"row\">";
  			result+=checkString(bean.merchantId);
  			result+="</div>";
 
  			if(!redMerchantActive.includes(active)){
  				result+="<div class=\"row\" style=\"font-size: x-small;\">";
  				result+= checkString(bean.merchantName);
  				result+="</div>";
  			}else{
  				result+="<div class=\"row\" style=\"font-size: x-small;color:red\">";
  				result+= checkString(bean.merchantName);
  				result+="</div>";
  			}

  			result+="</td>";
  			result+="<td width='15%' style='text-align: left;'>";
  			result+=checkString(bean.topic)+"</td>";
  			result+="<td width='15%' style='text-align: left;'>";
  			result+=checkString(bean.detail)+"</td>";
  			result+="<td width='10%' style='text-align: center;'>";
  			result+=checkString(bean.type)+"</td>";
  			result+="<td width='7%' style='text-align: center;'>";
  			result+=checkString(bean.count)+"</td>";
  			result+="<td width='12%' style='text-align: center;'>";
  			if((fDate instanceof Date && !isNaN(fDate))){
  				result+=printDate(fDate,false);
			}else{
				result+="-";
			}
  			result+="</td>";
  			result+="<td width='12%' style='text-align: center;'>";
  			if((rDate instanceof Date && !isNaN(rDate))){
  				result+=printDate(rDate,false);
			}else{
				result+="-";
			}
  			result+="</td>";

//  			result+="<td width='5%' id="+1+" style='text-align: center;'>";
  			
//  			var detail = JSON.parse(bean.result);
//  			
//  			if(detail==null||detail.message==null&&detail.fileName==null){
//  					result+="-";
//  			}
//  			else if(detail.message!=null&&detail.fileName==null){
//  						result+="<a class='fa fa-commenting fa-lg' data-toggle='tooltip' data-placement='right' style=\"text-decoration: none;\" title="+encodeHTML(detail.message)+"></a>"; 			
//  			}


//  			result+="</td>";
  			result+="<td width='5%' style='text-align: center;'>";
  			result+="<a class=\"idRefMonitor\" data-element-id="+bean.id+","+bean.merchantId+","+bean.type+" name=\"idRefMonitor\">";
  			result+="<button class=\"btn btn-info\">→</button></a></td>";
  			result+="</tr>";
  		});
  		$("#monitoringBoxCotainer").html(result);
	}
	
	function genPaging(data){
		
		var pagingBean = data.model.pagingBean;
		if(!pagingBean){
			$("#pagingDiv").html('');
			return false;
		}
		var pagingList = pagingBean.pagingList;
		if(!pagingList){
			$("#pagingDiv").html('');
			return false;
		}

		var pagingGen = "";
		pagingGen +="<div class=\"dataTables_paginate paging_bootstrap\">";
		pagingGen +="<ul class=\"pagination\">";
		if(pagingBean.currentPage == 1){
			pagingGen +="<li class=\"prev disabled\"><a href=\"#\" class=\"pagiation\">← Previous</a></li>";
		}else{
			pagingGen +="<li class=\"prev\"><a href=\"#\" data-element-id=\""+(pagingBean.currentPage-1)+"\" name=\"pagination\">← Previous</a></li>";
		}
		pagingList.forEach(function(pageNo, index, array) {
			if(pageNo==pagingBean.currentPage){
				pagingGen +="<li><a href=\"#\" style=\"background-color: #ccc;\" class=\"pagination\">"+pageNo+"</a></li>";
			}else if(pageNo=='...'){
				pagingGen +="<li><a href=\"#\" class=\"pagiation\">...</a></li>";
			}else{
				pagingGen +="<li><a href=\"#\" data-element-id=\""+pageNo+"\" name=\"pagination\">"+pageNo+"</a></li>";
			}
		});
		if(pagingBean.currentPage == pagingBean.totalPage){
			pagingGen +="<li class=\"next disabled\"><a href=\"#\">Next → </a></li>";
		}else{
			pagingGen +="<li class=\"next\"><a href=\"#\" name=\"pagination\" data-element-id=\""+(pagingBean.currentPage+1)+"\">Next → </a></li>";
		}
		pagingGen +="</ui>";
		pagingGen +="</div>";  	
		$("#currentPage").val(pagingBean.currentPage);
		$("#pagingDiv").html(pagingGen);

	}
 
	function addServiceUrl(data){
		var serviceUrls = data.model.serviceUrls;
		for (var index in serviceUrls)  {
			$("#service-url-"+index).val(serviceUrls[index]);
		};
			
	}
	
	function genServiceUrl(path){
		var currentCountry = $("#currentCountry").val();
		var url = $("#service-url-"+currentCountry).val();
		url = url+"/admin/"+path;
		return url;
	}
	
	function showLoading(){
		   $('#monitoringBoxCotainer').html(	"	<tr>\r\n" + 
					"<td >\r\n" + 
					"</td>\r\n" + 
					"	<td align=\"center\" colspan=\"12\" style=\"padding-top:40px\">\r\n" + 
					"		<div id=\"ajaxLoading\">\r\n" + 
					"<i class=\"fa fa-spinner fa-spin iconSpin fa-5x\"></i>"+
					"		</div>\r\n" + 
					"	</td>\r\n" + 
					"<td>\r\n" + 
					"</td>\r\n" + 
					"</tr>");
	}
	
	function encodeHTML(input){
		var str = input;
		var p = document.createElement("p");
		p.textContent = str;
		var converted = p.innerHTML;
		
		converted = converted.replace(new RegExp("&lt;br /&gt;",'g'), "</br>");
		return converted;
	}
 
 	