window.onload = function(e){
	var parameter_index = window.location.href.indexOf('=');
	if(parameter_index != -1){
		var merchantId = window.location.href.slice(parameter_index + 1);
		$('#merchantId').val(merchantId);
		search();
	}
}

$(document).ready(function() {
	$('#merchantId').keypress(function (e) {
		  if (e.which == 13) {
			  search(true);
		  }
	});
	
	$("input:checkbox").on("click", function() {
    	if($(this).is(':checked')){
			$(this).attr('checked', 'checked');
			$(this).val(1);
			
			$(this).parents('.row').find('tbody:not(:has(tr))').each(function () {
				var idTypeProduct = $(this).closest('tbody').attr('id');
				var row = genRow(idTypeProduct,'inTag', '');
				$(this).append(row);
				setStepNumber($(this).parents('table'));
				assignEditable();
				setActionValue($(this).parents('table'));
				
			});
			
			enableTable($(this));
		}else{
			$(this).removeAttr('checked');
			$(this).val(0);
			disableTable($(this));
		}
	 });
	
	$(".addrow").on("click", function() {
		addRow($(this));
	});

	startButton();
	tool0AddUrlBox();
	tool1AddProductBox();
	tool1SetInputForm();
	tool2AddProductBox();
	tool2SetInputForm();
	$('[data-toggle="tooltip"]').tooltip();
	addUrlRow($('#rowUrl'));
});


var filterEmpty = ['inTag', 'before', 'after', 'expireEqual', 'expireContain', 'textStep', 'goToStep', 'addTextBefore', 'addTextAfter', 'JSoupSelectorTxt', 'JSoupSelectorLongTxt', 'JSoupSelectorNum'];

var filterNonValue = ['plainText', 'removeNotPrice', 'unescape', 'replaceSpace', 'concatRealProductId', 'contactPrice', 'nonPrice', 'auctionPrice', 'specialPrice', 'encodeUrl', 'stop'];

var filterTwoValue = ['between', 'betweenUrl', 'ifEqual', 'ifNotEqual', 'ifContain', 'ifNotContain', 'replace', 'JSoupSelectorAttr'];

var imgDefault = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooqlqGo2WlWVzqOo3MVlY2UTT3V1cNshhhQHc7sR0HAAGWJIAByAwBdor5b1X9qrwjaXktvpmg67rVvCWVr6I2lnESpwSIp5XmCHnYzIA4+YBRnGaP2tNAPI8F+IiOOl1pZA9RkTn14J2n1ReNwB9bUV8lf8ADWmgf9CV4j/8CtM/+O0f8NaaB/0JXiP/AMCtM/8AjtAH1rRXyV/w1poH/QleI/8AwK0z/wCO0f8ADWmgf9CV4j/8CtM/+O0AfWtFfJX/AA1poH/QleI//ArTP/jtH/DWmgf9CV4j/wDArTP/AI7QB9a0V8lf8NaaB/0JXiP/AMCtM/8AjtH/AA1poH/QleI//ArTP/jtAH1rRXyV/wANaaB/0JXiP/wK0z/47R/w1poH/QleI/8AwK0z/wCO0AfWtFfLel/tV+D7u8it9T0HXtEgmdEW9nW0vIEZ22/vUtZvPRFB3s6RzbVU7lXKk/S2n6haarZWuo6fcQ3ljewrcWt1buHhmhdco6MCTzzuUgNG2UcbkagC9RRRQAUUUUAFFFFABRRRQAUUUUAFfLX7VeqXdp4Q0DTbeVooNZ10xXrKSMxWdnNPEGwRlBMysych9o3AhQV+pa+Sf2tcHw/4LBxz4iuxk9gdLnBxyCeoOORkKeowQD6M8JeEdE8HaJY6No1jb2sVrbxxzzRRgXF5cKoE91dThVlnuJpAzPI5HUIipEkcSdTgegP15/nQOQCfQf0NLQA0jnAC/iOmc8+44PHB96AAc/KBzjBAyOnXBP8A+r25OP4juZ7PQNcu7Z/LubXRtTubaQKrNHcQWU8kLqrZDFXUEKQcnjkE4/NKL44/FZo4z/wmN8DsTJ+y6eNxCqCSq2owf4eWZsAEsewB+oh4PIX24689OgAJyNo5zz9aUAc/KPbgenI4z3/HOeMAE/l43xx+Kyq5/wCEzvV+ViCbewYnCPu+UWjOAmVbcABk47cfpP4Vuri+8MeHb67kM13eaFpF1dSsFDS3Fxp9vLNIwX5QXkdnOABz0FAG7geg/IUYHoPyFLRQAmB6D8hRgeg/IUtFACYHoPyFGB6D8hS0UAct4v8ACei+MNFvdG1iyt7iO6tZ4beeSNfPsriZPLiurWfaZIJopWjkDJkMUCurplG8D/ZX1S6u/B+u6ZJJ5lto2u+VZZyQkN9aRXk0aKWKrH9oaV0REjCtJISHJAP1E444JBwcEY9M55B5BGRwcngjBr5N/ZLx/YHjTAAH/CRWZAAwB/xK4enXFAH1oM4GcZ79vy6/z/wpaOlFABRRRQAUUUUAFFFFABRRRQAV8lftaf8AIA8Ff9jHdf8Apslr61r5K/a0/wCQB4K/7GO6/wDTZLQB9aL0H0H8qazYIHU9h0J4boScE8Dg4xyScchy9B9B/KvnP9on4j3ng3w/Z6Jotw8Gt+IxcBrqI7ZrHSrZP9JniOR5c1xIyQQyZPAmXYx2sgB3fjP4nfDjQUvtE8SeILVJrm3ms7zT7Mz3V6IZ4pIZo3SxV5YXKu6hQ6yI3Jxnn5jWX9kxAoWx8QhQuF2xeN8EAYJB8wA5KknBK5Y4wBhflZmd2eRmkaWSV5JJZHaWV5HO53klkLyF2OQSzsTg4IARQlAH1UZ/2TCCDZeI8EEHCeOlBB+8p2zBSGAG5SSGwOOK9n0/9of4PaXY2em2ep6xFZ2FrBZ2sT+H9dkMdvbRLDCnmSWjSPtjRV3OzEkHJJwB+eMaPK6xQxSSyu6pHHGru8ju21Y0SNXd5SRhI0RmkY4GMc/QvhT9m/xhrVrFqfiO9s/CGmsgndL/AGT6kIHG5Xkto5vs9o+35mE93kcb1RleMAH0mP2lvhPn/kLauev/ADLmtH8ttl/PH1BwC4ftK/Ccn/kLat/4Teucf+SX88Hpx1rxE/Aj4YEran4x6aL0naCJ9CEbuGwVWP8AtIk8cbROTuzznKpzHir9m/xjo9o+p+Gryw8Y6cI2lCWH7nU2gXG2S3t3drW8dlLHyoLsOxDbSxWgD6V/4aV+E/8A0FdX/wDCc1v/AOQ6T/hpX4Tj/mLar7g+HtaVvr81mFx7lgBg9MDP5yyRyRSNFLG8UkcrQzJKrxyROhYMksUiRvHKj/u5YmUMjKcMysjU0jjkFSQpywCtgdc4LEDO7Ck9CM8gNQB+p/hn4t+APF9wtlofiK0mvpP9VZXKzWN3Kep8qG8S3Mp7bY2L7gQFOOfSa/GhS8bRyK7o8TCSOVCUkhZDuWWGZSHiljOG3RlWIH7xigUV+hf7PHxIvfGPh+80PW5zca74c+zILtyWk1HS7kFbe6kOfnmgmjkiuGG0bDB8oO5iAfRTdPz/APQWr5M/ZL/5AHjT/sYbP/01Q19ZE5GcEZycHgjKE46np0PuDXyb+yX/AMgDxp/2MNn/AOmqGgD61ooooAKKKKACiiigAooooAKKKKACvkr9rT/kAeCv+xjuv/TZLX1rXyV+1p/yAPBX/Yx3X/psloA+tF6D6D+VfAf7Vbt/wnehKeVTwzEVBJx8+q3rNkAgduOmQOegavvxeg+g/lXwD+1X/wAj7ov/AGLFv/6c76gD5nPYZycZJ4yWY7iTj6+gA4GOppvIPqMdOBySFUbicDc7Lk4wqhic5yjj1/Bf5Cug8J6Quv8Aijw5ocgDw6rrWnWM6E7Qba4uY1vAD2f7MZthOQHCsVKhhQB9NfDrw74f+EvgpPix4ytftmu6hEreGNKceXNHFcR/6EIRIriG91LIuWvJI3XTrF4xjzy/meQ3mvfEb43+J4dKSeS7e5knms9ItZXtNE0m3U4uLh4VBQrbFkSS9upJ5r6SQMj+XsEfc/tPa7Jd+NNM8NRN5eneH9Hgmjt4xtiW71ETCRwoBQmOzS2hhXZtVRgo7YKea/DT4mX/AMM73Ur/AE7R9P1OXVLe1tpXv3u0ktre0kkdorVreVQonMkHmiRJEb7PbhUUq/nAHt8f7Jt49ohfxrZrfOqsFi0mR7FumcE3SzTRtuHzbAig7yGzgeVLqnxJ+AvilNKe+AULBctpzzvceH9asJTs82OJ3C2/mMpgN1Abe4sZIP3hdGCVueAb74g/Ez4sweIbK+vrdrfUodS1ZkurxNK0rRkbyzpcUTyCORHjHlW1uNrO3+tYq7yp0v7VOr6ZfeJfDelWksMt9oumaj/aZjKOYf7RnsGtrOcqvyyCOCa4MLHASeNiFLFaANP4jeHvD/xZ8FSfFnwbai18Q6fE7eJtKjAa4ngtYd9/DcRoqrNfWluBdW14qL9rso1QhpNpT5FG3kpt2nJG3lT2yCckhvvKWJO0rzwK+lv2Ytee18Z6j4ZkZm07xFo9xLJbuA0Ru9PEZjYqQF3S2k08cgKDcqIuAgK14V4t0lNB8U+I9FjAEWl63qVlAAQQLaG7l+yLnAyyWhgRyBguGICqQoAOe64PcZAx2zgHjoc4Gcg9BX05+yozDx1rqAkI/hiXcoJUHGpWOCQCMkbcL1wCSAMk18xnqe3t6V9N/sq/8j5rf/YsS/8ApxsqAPvtun5/+gtXyZ+yX/yAPGn/AGMNn/6aoa+s26fn/wCgtXyZ+yX/AMgDxp/2MNn/AOmqGgD61ooooAKKKKACiiigAooooAKKKKACvkr9rT/kAeCv+xjuv/TZLX1rXyV+1p/yAPBX/Yx3X/psloA+tF6D6D+VfAP7Vf8AyPui/wDYsW//AKc76vv5eg+g/lXwF+1Wp/4T3ROOvheFsgjIC6reA5Bz23YwD057UAfMx6/gv8hXWeAdRg0nxv4T1G5dY7a08Q6VJcSseIYmu0hkmYDkJHHK7McgcKSQAQ3JkYOM7scZxjOOmPwx6c54xglpPUe2c4xkAgHLHkcMSoHIZQeBkkA+hv2mtJubH4k/b3U/Z9c0SwntXYgKZrJ5bO6iVuhaFVtnIBc7HPyZK4+ePlwSQuMbkbadzIfmUgJ8+TiP92HG4biDnAr7Ssks/wBoT4ZW2lvdwW3xE8GBBG9ycPcyLC0MU05JaQ2urRRhbiWM7Y76AykRwlYz8gavo+p6Ff3WlatY3Gl6jayNHcW11EYpY5IgVikRjujuIwTG0Uto7oYXWRw5cLGAfUPhH4n+EPA/wVn0/QdTsrbx5PbXTPAbaV55tUurp4opZbryTbSTQ2DB7eOSYeXIih4pEdxJ8p3FxcXk093ezzXV1cyPNcXVy5kuriWVsvJIzDcZvMyWZmfkcKUwsf0v/wALP+G3/ClV8GrozDxH/ZQ08WY05CBqrNtOrnVGjVDggyrMzi4JBiMQiVGb5z0jSNU17UrTSdGs59T1O5cQwWtqheV5CdrtICQLaFATJNdTuIYY9rgzM/lxgHun7M2lXN98SG1CMbbbRNEv5rmT7yiW9aGxso2IAG5zNO+OCI4Hycc15P491GDVvG/i3UrVg9teeIdVlt2UYDwrdSRRuBknZIsYdSTyGBB7V9RXy2P7PPw0udJjuYLv4g+MopBI0DBHtd0EkD3UWVEn2PSozIY5XYiW+lkkjzGoiPxh78nPO4gfMTg59DwcHOTkYbJyaACvpv8AZV/5HzW/+xYl/wDTjZV8yV9NfsrZ/wCE81sjt4WnbvxjUrAc9uSwx34agD78bp+f/oLV8mfsl/8AIA8af9jDZ/8Apqhr6ybp+f8A6Cfc96+Tf2S/+QB40/7GGz/9NcP1oA+taKKKACiiigAooooAKKKKACiiigAr5K/a0/5AHgr/ALGO6/8ATZLX1rXyV+1p/wAgDwV/2Md1/wCmyWgD60HQfQV8k/tSeCrvUdP0nxpYRPMNER9P1kRoXeDTbmUyQX2FK5t7S5Mi3mWUqlxFIpCpID9bL0H0H8qhngiuI5IJ0SWGWN4pIZUSSKRJFZGSSOQMkiMpIaN1ZGwNykUAfjcdwUZUgj5clgyrg9C6jDADgFFcMCCHBZVBn2BHTrkMPXIxxn6+2a/QDxP+zD4J1e8mvtDvtS8MzTSGVrS08i805XfG8w2t1+8hRmIJiS4ESkny0XIB5Ifsl2mD/wAVvfEdgmk23Oe43XeFx1xuPHIOeoB8i6Fr+seGdTg1rQtQn03U7bIiuoMNtRtoeO4icrDPbzKvlvHLvBUsAFYhq+mLX45eAfG1jFp/xa8FQ3F1BGsS6vpduLuM5IG6DypotT09WYM8iw3TwfxKCdyr0R/ZKsycHxxfH3/se1JGemQ1yRj3559gdqD9kq0I+bxxenAwc6TasD1O0/6QAigEcYdRzhehoA5z7B+yuH+0nVtbCht4sidaCgYy0AzY+YwYNgsLktwNswKk0+8+OXgHwTYS6f8ACbwTFbXMsflNrGq232aNFAKh5GkkuNT1MLhZIkmu0iL5EgKgg7//AAyTZDB/4TW8PGD/AMSm2UAdsFJwG5OdvyjJPPXKj9kixUZHje+BB+XGjWaAAjk70ufmDYA2yK4HXHNAHyJr2vax4m1ObWdevbnUdTu8Gae7K5CLgLDBGm2K1t1RYxHDAsUYRUJ3Oz78cnGANoAH8TEcnknAVupJPUDJPA4r7UH7JVpzjxxfAHuNHtucjqF+1ADrn7xGcggAZIf2SrPjHje/PU4bSbQZ9OftDEYwCfl5GOnIoA+Kxz1Khc43bzt6E5ZiqqgQLkl9qvkKrLhiPuP9lvwVeadp2q+NL+GS3GuQw6fpEcqeW82nwSia5vSp3N5c1wEjgw2CsEhYtuUrveGf2YfBei3cV7rV/qPiaSBkkW0ult7PTmdTlPOt7dfMuERskJLcBCeqHkN9IwW8NvFHDBGkUMahI4YkSOGJFACpFEgCRoFACpGAo256klgB5+6MnPX8wpzkj3yffntXyd+yX/yAPGn/AGMNn/6aoa+s26D8evX7p69P5Cvkz9kv/kAeNP8AsYbP/wBNUNAH1rRRRQAUUUUAFFFFABRRRQAUUUUAFfJX7Wn/ACAPBX/Yx3X/AKbJa+ta+Sv2tP8AkAeCv+xjuv8A02S0AfWi9B9B/KqOpzyWmn393FgyWtjd3CBgWXfDC8qZUdeUI9e3OavL0H0H8qo6lbyXdjfWkJRZbqyubeNnJCrJLDJHGZCgLiMOw3FQx27gFzzQB8j6V+0J4hv/AIbavrMkOlReMNJ1Hw8BH5E/9nX2ja5qlvYrfQQvPHIxty1zDMqyttkjjZsgsieoa1448Y6343m8AeA/7HsbvSNGstW8R65rMM1zFbPqMME9lZ2dlA4Lu0U8LTu/BExKtEIGJ881L9nfVr7wD4O0WPUtKh8V+GhPZ395vu107VNKl1KTU4LWV1tjO72c5jktXeHAd51ZmGwx+ieIfAPi/T/GR8e/D7UNETVb/R7XR/EOk+IIrt9O1JbSOGK2ug9iPtUcyxpCrqAmUt1beBIyUAbnw28dav4mufE3hvxPY2dl4o8H6gljqn9mu72F5DLH5lre2qyu8kQmCklHb5uJFjjVgo5uD4k+IZNa+N2nulh5Hw906G70I+SweSSTS769YXxMzCYB7eMIsccJKCRTuZg69V8NvAd/4U/4SDWfEGpQav4t8Val/aWtXlpEIrSJVDJb2dlEyrIsEKFwrSkkghVjTyyX5u2+GGuRa38aNRN3pfkfEewtLbR0V7lmtZbfTLyyLX5MA2pvuAx+zs/DSDBypoA4XRPjl4juvhx4g1q9g02XxdaahoFpokNvBIttf23idbZ9JumtZJFdsgai0ixugKW6E7er9LafFvXE+DZ8b3sFlN4pudSn0PTLSGB47S61VtYk0uyiSDzJJGMwjMjKjMxIJChBxlaX8DNWtdX+H9/eahpr2nh3w3pVh4htYZLpl1LWdCtdSh0q7t1khAnhhS8ijKv5Dp5btGWLYSG1+CPiyfR/Avhy+1+ysNL8M6zrmv6hc6S851CTUru+mudGfT2ubNoP+JejI7PKpYSl1WMKgeQA6rSfi/dQ/DDXvFfiGygPiXwte32iavpFrmGOXWYrlbe0hRJWaeKF1nhad3XcEt7yVV2JhH6H4k+KFtqHh268T3vw/Njr/wBnmvtBS+bSdW0KwuomkiurR72VjqTRny4poQjGSVnjRlZCwwV+Bmqib4haRceIW1Lw3460+1uWvb/y5tet/E1jdLdw6hcJHaW9pNFK5ZZijJLJE8kBRV2SGhefCX4heI5/CkviKTwBJc+EdR0Xy9dsrLUF1/WNL0uXIguLt0eO2g8pnfyIVCtdBpCSXkDgFHUPjD4pj8W+NdFfxj8OvCVn4c1l7DT/APhLI7q3nvrZC5aWF4WxK0So3nHaoVpYiABuFfU+lvcSadYy3U9rdXUlpbSXFzZAiznmeFHkmtd2X+zuTug3/MYim7nJr51/4Vn4/wBK8V+N9c0e1+GWqWXi3WjqcUfiyx1a/urFTuCRp5K26QvMZc3CfvRKIogJAEVF+htFXU00nT11v7ANXFpANSGmLNHpy3vlr562KXDyTLbCTcIlkkkZVwu4gCgDRbp+f/oLV8mfsl/8gDxp/wBjDZ/+mqGvrNun5/8AoLV8mfsl/wDIA8af9jDZ/wDpqhoA+taKKKACiiigAooooAKKKKACiiigAr5b/aq0u7u/CGganBG8ltomvCe+EaM8iw3lpNaxyAKDtRZzFG7OFQGdG3fKwP1JVLUNPs9UtLnT9RtIL2xvIWt7q2uFEkU8L/eR0IIIHDK2Q6MNyFWAagDD8J+L9E8YaLZaxo99a3MVxBG80EVxHJPZXGxfOtLmMNvinhclWSRUYoUkClJEY9MTzz1GTkFeBnPfGMjrgcDv0NfLmq/sreDru7km0vXdf0a3lIP2OI2l9Gg3EBUnuovtAiRSqRxtNKVCZ3Mzlaof8Ml6B/0OfiH2xZaV09DmHJPqT1+tAH1hwcjkAkn/AJZkZ557gnJBG7PQehFKMA55wBwMggZ6474/3iMdABwK+Tv+GS9A/wCh08Q/+Aelf/GaP+GS9A/6HTxD/wCAelf/ABmgD6xyOgzgc8EH9CPXpnv/AMByuR1J59Mr2wD16c9xjPrkgV8m/wDDJegf9Dp4h/8AAPSv/jNH/DJegf8AQ6eIf/APSv8A4zQB9YnaeoyOOpU9OVGScnBBIycgnI6ZVCAME5bsclSfcc8EnHOMHgAHjFfJ/wDwyXoH/Q6eIf8AwD0r/wCM0f8ADJegf9Dp4h/8A9K/+M0AfWOR7jtgMB06AYIxj2wCOpJ4pPl5Az37pjJxnAJK8gdhjlu5NfJ//DJegf8AQ6eIf/APSv8A4zR/wyXoH/Q6eIf/AAD0r/4zQB9Y/L6dtv8ADkDsMnkYPzAEjvjoBS7h6cdRkr/8Vnrnn1z7V8m/8Ml6B/0OniH/AMA9K/8AjNH/AAyXoH/Q6eIf/APSv/jNAH0V4w8YaL4L0S91rWry3toba2nkgglnjS4vrlI2MNnZxFt08877Ywq52FgzfLll8H/ZW0q7svCGvalMhS31jXy9nuGGkisbOG1knXn/AFTyhgmVBJU9sEt0v9lXwfZXiXGpa9r2swRlG+xv9jsI5GVlcCea0gEzx5VT5YdQeeucH6Y0/T7HSrK107TrWGysbKFLe0tbdAkMEKKAqIo7cZZySzsWZ2ZmJIBcHIB9QKWiigAooooAKKKKACiiigAooooAKTAyD6f55/Hn60tFABgZzgZ9e/r/AD5ooooAKKKKACiiigAooooAKKKKACiiigAwPSiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/Z';

var CHECKED_LIMIT = 20;
var CURRENT_TAB = "";
var pConfigDisplay = {
		'inTag' : 'get by : "TAG"',
	    'between' : 'get by : "BETWEEN"',
	    'before' : 'get by : "BEFORE"',
	    'after' : 'get by : "AFTER"',
	    'textStep' : 'get by : "STEP"',
	    'betweenUrl' : 'get by : "URL BETWEEN"',
	    'plainText' : 'remove : "HTML TAG"',
	    'removeNotPrice' : 'remove : "NOT PRICE"',
	    'replaceSpace' : 'remove : "GROUPING SPACE"',
	    'contactPrice' : 'set : "CONTACT PRICE" & stop',
	    'nonPrice' : 'set : "NON SPECIFIC PRICE" & stop',
	    'auctionPrice' : 'set : "AUCTION PRICE" & stop',
	    'specialPrice' : 'set : "SPECIAL PRICE" & stop',
	    'unescape' : 'convert : "TO HTML CODE"',
	    'encodeUrl' : 'convert : "ENCODE URL"',
	    'expireEqual' : 'expire if : "EQUAL"',
	    'expireContain' : 'expire if : "CONTAIN"',
	    'concatRealProductId' : 'append : "REAL PRODUCT ID" after name',
	    'ifEqual' : 'if : "EQUAL" & go to step',
	    'ifNotEqual' : 'if : "NOT EQUAL" & go to step',
	    'ifContain' : 'if : "CONTAIN" & go to step',
	    'ifNotContain' : 'if : "NOT CONTAIN" & go to step',
	    'replace' : 'replace : "WITH"',
	    'addTextBefore' : 'add : Text before',
	    'addTextAfter' : 'add : Text After',
	    'stop' : 'force : stop',
	    'goToStep' : 'force : go to step',
	    'JSoupSelectorNum' : 'Tool : JSoup Selector Number',
		'JSoupSelectorTxt' : 'Tool : JSoup Selector Text',
		'JSoupSelectorLongTxt' : 'Tool : JSoup Selector Long Text',
		'JSoupSelectorAttr' : 'Tool : JSoup Selector Attribute'
	};

var pConfig = {
		'get by : "TAG"' : 'inTag',
		'get by : "BETWEEN"' : 'between',
		'get by : "BEFORE"' : 'before',
		'get by : "AFTER"' : 'after',
		'get by : "STEP"' : 'textStep',
		'get by : "URL BETWEEN"' : 'betweenUrl',
		'remove : "HTML TAG"' : 'plainText',
		'remove : "NOT PRICE"' : 'removeNotPrice',
		'remove : "GROUPING SPACE"' : 'replaceSpace',
		'set : "CONTACT PRICE" & stop' : 'contactPrice',
		'set : "NON SPECIFIC PRICE" & stop' : 'nonPrice',
		'set : "AUCTION PRICE" & stop' : 'auctionPrice',
		'set : "SPECIAL PRICE" & stop' : 'specialPrice',
		'convert : "TO HTML CODE"' : 'unescape',
		'convert : "ENCODE URL"' : 'encodeUrl',
		'expire if : "EQUAL"' : 'expireEqual',
		'expire if : "CONTAIN"' : 'expireContain',
		'append : "REAL PRODUCT ID" after name' : 'concatRealProductId',
	    'if : "EQUAL" & go to step' : 'ifEqual',
	    'if : "NOT EQUAL" & go to step' : 'ifNotEqual',
	    'if : "CONTAIN" & go to step' : 'ifContain',
	    'if : "NOT CONTAIN" & go to step' : 'ifNotContain',
	    'replace : "WITH"' : 'replace',
	    'add : Text before' : 'addTextBefore',
	    'add : Text After' : 'addTextAfter',
	    'force : stop' : 'stop',
	    'force : go to step' : 'goToStep',
	    'Tool : JSoup Selector Number' : 'JSoupSelectorNum',
	    'Tool : JSoup Selector Text' : 'JSoupSelectorTxt',
	    'Tool : JSoup Selector Long Text' : 'JSoupSelectorLongTxt',
	    'Tool : JSoup Selector Attribute' : 'JSoupSelectorAttr'
	};

function startButton (){
	openTabParser("hide");
	$('#tabArea').hide();
	$('#buttonResult').hide();
	$('#pTab').addClass('active');
	$('#process-tab').css("display", "none");
	$('#topRunParser').css("display", "none");
}

function deleteUrlRow(ele){
	if($('input[name=url\\[\\]]').length == 1){
		$('input[name=url\\[\\]]').val("");
		return;
	}
	
	$(ele).parents('#parserSubTab .row').remove();
	setUrlNumber();
}

function deleteUrlAllRow(){
	
	var i = 1;
	$('.testUrl').each(function() {
		$(this).text('URL '+ i +' :');
		if($('input[name=url\\[\\]]').length == 1){
			$('input[name=url\\[\\]]').val("");
			return;
		}else{
			$(this).parents('#parserSubTab .row').remove();
			setUrlNumber();			
		}
		i++;
	});
	
	
}

function setUrlNumber(){
	var i = 1;
	$('.textUrl').each(function() {
		$(this).text('URL '+ i +' :');
		i++;
	});
}

function setStepNumber(ele){
	var i = 1;
	ele.find('.step').each(function() {
		$(this).text(i);
		i++;
	});
}

function setAllActionValue(ele){
	$(ele).each(function() {
		setActionValue($(this));
	});
}

function setActionValue(ele){
	$(ele).find('.filterType').each( function() {
		var val = $(this).text();
		$(this).editable('setValue', pConfig[val]);
	});
}

function openTab(value){
	var tab = value;
	if(tab == 'Parser'){
		$('#pTab').addClass('active');
		$('#cTab').removeClass('active');
		$('#tTab').removeClass('active');
		$('.parserTab').show();
		$('.compareTab').hide();
		$('.toolTab').hide();
		$('#btnClose').hide();
		$('#hdTabType').val('Parser');
		$('#buttonResult').show();
		
		$('#topRunParser').css("display", "block");
		$('#btnTopSave').show();
		$('#btnTopRunTest').text('Run Test');
		$('#modalTitle').text('Run Parser Test');
	}else if(tab ==  'Compare'){
		$('#pTab').removeClass('active');
		$('#cTab').addClass('active');
		$('#tTab').removeClass('active');
		$('.parserTab').hide();
		$('.compareTab').show();
		$('.toolTab').hide();
		$('#btnClose').show();
		$('#buttonResult').hide();
		$('#hdTabType').val('Compare');
		$('#topRunParser').css("display", "block");
		$('#btnTopSave').hide();
		$('#btnTopRunTest').text('Compare Test');
		$('#modalTitle').text('Run Compare Test');
		getProductSample();
	}else if(tab ==  'Tool'){
		$('#pTab').removeClass('active');
		$('#cTab').removeClass('active');
		$('#tTab').addClass('active');
		$('.parserTab').hide();
		$('.compareTab').hide();
		$('.toolTab').show();
		$('#btnClose').hide();
		$('#buttonResult').hide();
		$('#hdTabType').val('Tool');
		$('#topRunParser').css("display", "none");
		$('#modalTitle').text('Parser Tool result');
	}
}

function openTabParser(value){
	var tab = value;
	if(tab == 'pName'){
		$('#pName').addClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').show();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
		
	}else if(tab == 'pPrice'){
		$('#pName').removeClass('active');
		$('#pPrice').addClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').show();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
	}else if(tab == 'pDes'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').addClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').show();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
	}
	else if(tab == 'pPic'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').addClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').show();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
	}
	else if(tab == 'pRealproductAndConcat'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').addClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').show();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
	}
	else if(tab == 'pUpc'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').addClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').show();
		$('.pExpireTab').hide();
	}
	else if(tab == 'pExpire'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').addClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').show();
	}
	else if(tab == 'hide'){
		$('#pName').removeClass('active');
		$('#pPrice').removeClass('active');
		$('#pDes').removeClass('active');
		$('#pPic').removeClass('active');
		$('#pRealproductAndConcat').removeClass('active');
		$('#pUpc').removeClass('active');
		$('#pExpire').removeClass('active');
		$('.pNameTab').hide();
		$('.pPriceTab').hide();
		$('.pDesTab').hide();
		$('.pPicTab').hide();
		$('.pRealproductAndConcatTab').hide();
		$('.pUpcTab').hide();
		$('.pExpireTab').hide();
	}
	CURRENT_TAB = tab;
}

//function openTabProcess(value){
//	var tab = value;
//	openTab('Parser');
//	if(tab == '#parserSubTab'){
//		$('#buttonResult').show();
//		$('#parserSubTab').show();
//		$('#toolSubTab').hide();
//		$('#hdTabType').val('Parser');
//		openTabParser('pName');
//		$('#topRunParser').css("display", "block");
//		$('#btnTopRunTest').closest('div').show();
//		$('#btnTopSave').closest('div').show();
//	}else if(tab ==  '#toolSubTab'){
//		openTabParser('hide');
//		$('#buttonResult').hide();
//		$('#parserSubTab').hide();
//		$('#toolSubTab').show();
//		$('#hdTabType').val('Tool');
//		$('#topRunParser').css("display", "block");
//		$('#btnTopSave').closest('div').hide();
//		$('#btnTopRunTest').closest('div').hide();
//		$('#modalTitle').text('Parser Tool result');
//	}
//}

function assignEditValue(){
	$('.editText').editable({
    	type: 'text',
        title: 'Input Value',
        emptyclass: 'text-danger',
        emptytext: 'EMPTY'
    });
}

function assignEditable(){
	
	$('#tblname, #tbldescription, #tblupc, #tblrealproductId, #tblconcatWord').find('.filterType').editable({
    	type: 'select',
        title: 'Select Action',
        emptyclass: 'text-danger',
        value: this.value,
        source: [
            {value: 'inTag', text: 'get by : "TAG"'},
            {value: 'between', text: 'get by : "BETWEEN"'},
            {value: 'before', text: 'get by : "BEFORE"'},
            {value: 'after', text: 'get by : "AFTER"'},
            {value: 'textStep', text: 'get by : "STEP"'},
            {value: 'betweenUrl', text: 'get by : "URL BETWEEN"'},
            {value: 'plainText', text: 'remove : "HTML TAG"'},
            {value: 'replaceSpace', text: 'remove : "GROUPING SPACE"'},
            {value: 'unescape', text: 'convert : "TO HTML CODE"'},
            {value: 'ifEqual', text: 'if : "EQUAL" & go to step'},
            {value: 'ifNotEqual', text: 'if : "NOT EQUAL" & go to step'},
            {value: 'ifContain', text: 'if : "CONTAIN" & go to step'},
            {value: 'ifNotContain', text: 'if : "NOT CONTAIN" & go to step'},
            {value: 'replace', text: 'replace : "WITH"'},
            {value: 'addTextBefore', text: 'add : Text before'},
            {value: 'addTextAfter', text: 'add : Text After'},
            {value: 'stop', text: 'force : stop'},
            {value: 'goToStep', text: 'force : go to step'},
            {value: 'JSoupSelectorNum', text: 'Tool : JSoup Selector Number'},
            {value: 'JSoupSelectorTxt', text: 'Tool : JSoup Selector Text'},
            {value: 'JSoupSelectorLongTxt', text: 'Tool : JSoup Selector Long Text'},
            {value: 'JSoupSelectorAttr', text: 'Tool : JSoup Selector Attribute'}
        ],success: function(response, newValue) {
        	$(this).parents('tr').find('td:eq(2)').html(setShowValue(newValue, ''));
        	assignEditValue();
        }
    });
	
	$('#tblprice, #tblbasePrice').find('.filterType').editable({
    	type: 'select',
        title: 'Select Action',
        emptyclass: 'text-danger',
        value: this.value,
        source: [
            {value: 'inTag', text: 'get by : "TAG"'},
            {value: 'between', text: 'get by : "BETWEEN"'},
            {value: 'before', text: 'get by : "BEFORE"'},
            {value: 'after', text: 'get by : "AFTER"'},
            {value: 'textStep', text: 'get by : "STEP"'},
            {value: 'plainText', text: 'remove : "HTML TAG"'},
            {value: 'replaceSpace', text: 'remove : "GROUPING SPACE"'},
            {value: 'removeNotPrice', text: 'remove : "NOT PRICE"'},
            {value: 'unescape', text: 'convert : "TO HTML CODE"'},
            {value: 'contactPrice', text: 'set : "CONTACT PRICE" & stop'},
            {value: 'nonPrice', text: 'set : "NON SPECIFIC PRICE" & stop'},
            {value: 'auctionPrice', text: 'set : "AUCTION PRICE" & stop'},
            {value: 'specialPrice', text: 'set : "SPECIAL PRICE" & stop'},
            {value: 'ifEqual', text: 'if : "EQUAL" & go to step'},
            {value: 'ifNotEqual', text: 'if : "NOT EQUAL" & go to step'},
            {value: 'ifContain', text: 'if : "CONTAIN" & go to step'},
            {value: 'ifNotContain', text: 'if : "NOT CONTAIN" & go to step'},
            {value: 'replace', text: 'replace : "WITH"'},
            {value: 'stop', text: 'force : stop'},
            {value: 'goToStep', text: 'force : go to step'},
            {value: 'JSoupSelectorNum', text: 'Tool : JSoup Selector Number'},
            {value: 'JSoupSelectorTxt', text: 'Tool : JSoup Selector Text'},
            {value: 'JSoupSelectorLongTxt', text: 'Tool : JSoup Selector Long Text'},
            {value: 'JSoupSelectorAttr', text: 'Tool : JSoup Selector Attribute'}
        ],success: function(response, newValue) {
        	$(this).parents('tr').find('td:eq(2)').html(setShowValue(newValue, ''));
        	assignEditValue();
        }
    });
	
	$('#tblconcatId .filterType').editable({
    	type: 'select',
        title: 'Select Action',
        emptyclass: 'text-danger',
        value: this.value,
        source: [
            {value: 'inTag', text: 'get by : "TAG"'},
            {value: 'between', text: 'get by : "BETWEEN"'},
            {value: 'before', text: 'get by : "BEFORE"'},
            {value: 'after', text: 'get by : "AFTER"'},
            {value: 'textStep', text: 'get by : "STEP"'},
            {value: 'betweenUrl', text: 'get by : "URL BETWEEN"'},
            {value: 'plainText', text: 'remove : "HTML TAG"'},
            {value: 'replaceSpace', text: 'remove : "GROUPING SPACE"'},
            {value: 'unescape', text: 'convert : "TO HTML CODE"'},
            {value: 'ifEqual', text: 'if : "EQUAL" & go to step'},
            {value: 'ifNotEqual', text: 'if : "NOT EQUAL" & go to step'},
            {value: 'ifContain', text: 'if : "CONTAIN" & go to step'},
            {value: 'ifNotContain', text: 'if : "NOT CONTAIN" & go to step'},
            {value: 'concatRealProductId', text: 'append : "REAL PRODUCT ID" after name'},
            {value: 'addTextBefore', text: 'add : Text before'},
            {value: 'addTextAfter', text: 'add : Text After'},
            {value: 'stop', text: 'force : stop'},
            {value: 'goToStep', text: 'force : go to step'},
            {value: 'JSoupSelectorNum', text: 'Tool : JSoup Selector Number'},
            {value: 'JSoupSelectorTxt', text: 'Tool : JSoup Selector Text'},
            {value: 'JSoupSelectorLongTxt', text: 'Tool : JSoup Selector Long Text'},
            {value: 'JSoupSelectorAttr', text: 'Tool : JSoup Selector Attribute'}
        ],success: function(response, newValue) {
        	$(this).parents('tr').find('td:eq(2)').html(setShowValue(newValue, ''));
        	assignEditValue();
        }
    });
	
	$('#tblpictureurl').find('.filterType').editable({
    	type: 'select',
        title: 'Select Action',
        emptyclass: 'text-danger',
        value: this.value,
        source: [
            {value: 'inTag', text: 'get by : "TAG"'},
            {value: 'between', text: 'get by : "BETWEEN"'},
            {value: 'before', text: 'get by : "BEFORE"'},
            {value: 'after', text: 'get by : "AFTER"'},
            {value: 'textStep', text: 'get by : "STEP"'},
            {value: 'betweenUrl', text: 'get by : "URL BETWEEN"'},
            {value: 'plainText', text: 'remove : "HTML TAG"'},
            {value: 'replaceSpace', text: 'remove : "GROUPING SPACE"'},
            {value: 'unescape', text: 'convert : "TO HTML CODE"'},
            {value: 'encodeUrl', text: 'convert : "ENCODE URL"'},
            {value: 'ifEqual', text: 'if : "EQUAL" & go to step'},
            {value: 'ifNotEqual', text: 'if : "NOT EQUAL" & go to step'},
            {value: 'ifContain', text: 'if : "CONTAIN" & go to step'},
            {value: 'ifNotContain', text: 'if : "NOT CONTAIN" & go to step'},
            {value: 'addTextBefore', text: 'add : Text before'},
            {value: 'addTextAfter', text: 'add : Text After'},
            {value: 'stop', text: 'force : stop'},
            {value: 'goToStep', text: 'force : go to step'},
            {value: 'JSoupSelectorNum', text: 'Tool : JSoup Selector Number'},
            {value: 'JSoupSelectorTxt', text: 'Tool : JSoup Selector Text'},
            {value: 'JSoupSelectorLongTxt', text: 'Tool : JSoup Selector Long Text'},
            {value: 'JSoupSelectorAttr', text: 'Tool : JSoup Selector Attribute'}
        ],success: function(response, newValue) {
        	$(this).parents('tr').find('td:eq(2)').html(setShowValue(newValue, ''));
        	assignEditValue();
        }
    });
	
	$('#tblexpire .filterType').editable({
    	type: 'select',
        title: 'Select Action',
        emptyclass: 'text-danger',
        value: this.value,
        source: [
            {value: 'inTag', text: 'get by : "TAG"'},
            {value: 'between', text: 'get by : "BETWEEN"'},
            {value: 'before', text: 'get by : "BEFORE"'},
            {value: 'after', text: 'get by : "AFTER"'},
            {value: 'textStep', text: 'get by : "STEP"'},
            {value: 'betweenUrl', text: 'get by : "URL BETWEEN"'},
            {value: 'plainText', text: 'remove : "HTML TAG"'},
            {value: 'replaceSpace', text: 'remove : "GROUPING SPACE"'},
            {value: 'unescape', text: 'convert : "TO HTML CODE"'},
            {value: 'ifEqual', text: 'if : "EQUAL" & go to step'},
            {value: 'ifNotEqual', text: 'if : "NOT EQUAL" & go to step'},
            {value: 'ifContain', text: 'if : "CONTAIN" & go to step'},
            {value: 'ifNotContain', text: 'if : "NOT CONTAIN" & go to step'},
            {value: 'expireEqual', text: 'expire if : "EQUAL"'},
            {value: 'expireContain', text: 'expire if : "CONTAIN"'},
            {value: 'addTextBefore', text: 'add : Text before'},
            {value: 'addTextAfter', text: 'add : Text After'},
            {value: 'stop', text: 'force : stop'},
            {value: 'goToStep', text: 'force : go to step'},
            {value: 'JSoupSelectorNum', text: 'Tool : JSoup Selector Number'},
            {value: 'JSoupSelectorTxt', text: 'Tool : JSoup Selector Text'},
            {value: 'JSoupSelectorLongTxt', text: 'Tool : JSoup Selector Long Text'},
            {value: 'JSoupSelectorAttr', text: 'Tool : JSoup Selector Attribute'}
        ],success: function(response, newValue) {
        	$(this).parents('tr').find('td:eq(2)').html(setShowValue(newValue, ''));
        	assignEditValue();
        }
    });	
	
	assignEditValue();
}

function save(){
	setButton(true);
	var merchantId = $('#hdMerchantId').val();
	if(!merchantId){
		modalAlert("Merchant Data Not Found.");
		setButton(false);
		return;
	}
	
	var jsonString = getAllData();
	if(jsonString === '{}'){
		modalAlert("Please setup Parser Config Value.");
		setButton(false);
		return;
	}
	var param = "merchantId="+ merchantId +"&data="+jsonString;
	
	$.ajax({
		type: "POST",
		url: 'saveparser',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		modalAlert('Save is Done.');
		  	}else if(data.header == 'notFound'){
		  		modalAlert('Save is Fail :  There Are Not Merchant.');
		  	}else if(data.header == 'error') {
		  		modalAlert('Error : Cannot Save.');
		  	}else if(data.header == 'errorValidate'){
		  		var text = "<div style='font-weight:600; height:40px; font-size:15px; color:red;'>Configuration is Wrong.</div>";
	  			$.each(data.mValidate, function(key, filter) {
	  				var field = $("table[val='"+ key +"']").parents('.row').find('.label-field').text();
	  				text += "<div style='height:25px;'><span style='font-weight:600;'>" + field + '</span> : ' + pConfigDisplay[filter] + "</div>";
	  			});
	  			modalAlert(text);
		  	}
		  	setButton(false);
	  	},
	  	error: function (textStatus, errorThrown) {
	  		setButton(false);
	  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

function getAllData(){
	var jsonObj = {};
	$(".parserTab").find("input[type=checkbox]:checked").each(function () {
		var ele = $(this).parents(".row").find("table");
		var tblName = ele.attr("id");
		var table = document.getElementById(tblName);
		fieldObj = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var row = table.rows[r];
		        var filter = row.cells[1].textContent;
		        var value = row.cells[2].textContent;
		        var valueRec = row.cells[2].innerHTML;
		        
		        var check = valueRec.match(/<a/g);
		        if(check != null && check.length == 2){
		        	if(value == 'EMPTY,EMPTY'){
		        		value = "|,|";
		        	}else{
		        		value = valueRec.replace('<span style="padding-left:7%;padding-right:7%">,</span>', '|,|');
		        		value = $(value).text();
		        		var tmp = value.split("|,|");
		        		if(tmp[0] == 'EMPTY'){
		        			tmp[0] = '';
		        		}else if(tmp[1] == 'EMPTY'){
		        			tmp[1] = '';
		        		}
		        		value = tmp[0] + "|,|" + tmp[1];
		        	}
		        }else if((jQuery.inArray(filter, filterEmpty) === -1 && value == 'EMPTY') || (jQuery.inArray(filter, filterNonValue) === -1 && value == '-')){
		        	value = "";
		        }
		        
		        var item = {};
		        item['filter'] = pConfig[filter];
		        item['value'] = value;
		        fieldObj.push(item);
			}
		if(fieldObj.length > 0){
			jsonObj[ele.attr('val')] = fieldObj;
		}
	});
	
	jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	return jsonString;
}

function getTestData() {
	var jsonObj = [];
    var jsonString;
    var table = document.getElementById("comparetbl");
    for (var r = 1, n = table.rows.length; r <= n-1; r++) {
    	
    	if(table.rows[r].cells[9].children[0].checked) {
        	var item = {};
        	
    		item ["name"] 			= (table.rows[r].cells[1].textContent != '' ? table.rows[r].cells[1].textContent : "");
    		item ["url"] 			= (table.rows[r].cells[2].textContent != '' ? table.rows[r].cells[2].textContent : "");
    		item ["price"] 			= (table.rows[r].cells[3].textContent != '' ? table.rows[r].cells[3].textContent : "");
    		item ["basePrice"] 		= (table.rows[r].cells[4].textContent != '' ? table.rows[r].cells[4].textContent : "");
    		item ["pictureUrl"] 	= (table.rows[r].cells[5].textContent != '' ? table.rows[r].cells[5].textContent : "");
    		item ["description"] 	= (table.rows[r].cells[6].textContent != '' ? table.rows[r].cells[6].textContent : "");
    		item ["realProductId"] 	= (table.rows[r].cells[7].textContent != '' ? table.rows[r].cells[7].textContent : "");
    		item ["upc"] 			= (table.rows[r].cells[8].textContent != '' ? table.rows[r].cells[8].textContent : "");

        	jsonObj.push(item);
    	}
    }
    jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	
	return jsonString;
}

function runTest(){
	setButton(true);
	var merchantId = $('#hdMerchantId').val();
	var testType = $('#hdTabType').val();
	if(!merchantId){
		modalAlert("Please Fill Merchant ID for Search.");
		setButton(false);
		return;
	}
	
	var url = [];
	$("input[name='url[]']").each(function () {
	    url.push(this.value);
	});
	
	var checkUrl = false;
	if(url != ""){
		checkUrl = true;
	}
	
	var jsonString = getAllData();
	
	if(testType == 'Compare'){
		var recordCount = countUrl();
		setButton(true);
		
		if(recordCount < 1) {
			modalAlert('Please select at lease 1 record for testing.');
			setButton(false);
			return;
		}
		var jsonData = getTestData();
		$('#modalData').html('<h1 style="text-align:center;">Running, please wait...</h1>');
			$('#modalRun').modal('show');
			$("body").css("cursor", "wait");
			
			var mId = $('#merchantId').val();
		var param = "merchantId=" + mId+ "&config=" + jsonString+ "&data=" + jsonData;
		$.ajax({
			type: "POST",
			url: 'compareproductdata',
			data: param,
			cache: false,
			async: true,
		  	success: function(data) {
			  	if(data.header == 'success'){
			  		var oldList = data.oldList;
			  		var newList = data.newList;
			  		var result = "";
			  		var isTemplate = false;
			  		
			  		result += "<div class='col-md-6' style='border-style: solid;border-color: rgba(23, 0, 0, 0.15);border-width: 2px;border-radius: 5px;background-color: rgba(255, 244, 41, 0.27);margin-bottom: 10px;'><b>Note(Important field for update data)</b>";
			  		result += "<ul>";
			  		result += "<li>URL : Old &amp; New Product Data must be the same</li>";
			  		result += "<li>Product Name : Old &amp; New Product Data must be the same</li>";
			  		result += "<li>Price : New Product Data mustn't empty or value equal to zero</li>";
			  		result += "</ul>";
			  		result += "</div>";
			  		
			  		if(oldList != null && oldList.length > 0 && newList != null && newList.length > 0 && newList.length == oldList.length){
	 					for(var i=0; i<oldList.length; i++){
	 						var oldObject = oldList[i];
	 						var newObject = newList[i];

	 						result += "<div class='row' style='margin-bottom:35px;'>";
				  			var oldpictureData = oldObject['pictureData'];
				  			var newpictureData = newObject['pictureData'];
				  			if(!oldpictureData){
				  				oldpictureData = imgDefault;
				  			}
				  			if(!newpictureData){  				
				  				newpictureData = imgDefault;
				  			}

				  			result += "<div class='col-md-12'>";
				  			result += "<table id='compareModal' class='table table-striped table-bordered' style='word-break: break-word;'>";
				  			result += "            <thead>";
		                    result += "                <tr class='heading' style='background-color:rgb(90, 90, 90);color: white;'>";
		                    result += "                    <th class='col-md-2' style='text-align:center;font-weight:bold;'> Field </th>";
		                    result += "                    <th class='col-md-5' style='text-align:center;font-weight:bold;'> Old Product Data </th>";
		                    result += "                    <th class='col-md-5' style='text-align:center;font-weight:bold;'>  New Product Data </th>";
		                    result += "                </tr>";
		                    result += "            </thead>";
				  			result += "<tr>";
			  				result += "	<td class='col-md-2' style='text-align:center;font-weight:bold;'>IMAGE</td>";
			  				result += "	<td class='col-md-5''><img src=data:image/png;base64,"+ oldpictureData +" style='max-width:150px;border-style: solid; border-color: rgba(228, 220, 220, 0.59); border-width: 1px;'></td>";
			  				result += "	<td class='col-md-5''><img src=data:image/png;base64,"+ newpictureData +" style='max-width:150px;border-style: solid; border-color: rgba(228, 220, 220, 0.59); border-width: 1px;'" +
			  						"'></td>";
			  				result += "</tr>";
			  				
			  				
				  			result += "<tr>";
			  				result += "	<td class='col-md-2' style='text-align:center;font-weight:bold;'>URL</td>";
			  				if((oldObject['url'] || newObject['url']) && oldObject['url'] != newObject['url']){
			  					result += generateComapareRow(oldObject['url'], newObject['url'], false);
			  				}else{
			  					result += generateComapareRow(oldObject['url'], newObject['url'], true);
			  				}
			  				result += "</tr>";
			  				var ckSize = $(".parserTab").find("input[type=checkbox]:checked").length;
			  				if(ckSize == 0){
			  					isTemplate = true;
			  					$(".parserTab").find("input[type=checkbox]").prop('checked', true);
			  				}
				  			var chk = $(".parserTab").find("input[type=checkbox]:checked").each(function () {
				  				var ele = $(this).parents('.row');
				  				var displayField = ele.find('.col-md-2').text();
				  				var fieldType = ele.find('table').attr('val');
				  				var oldvalue = oldObject[fieldType];
				  				var newvalue = newObject[fieldType];
			  					var isNewPrice = true;
				  				
				  				result += "<tr>";
				  				result += "	<td class='col-md-2' style='text-align:center;font-weight:bold;'>"+displayField+"</td>";
				  				
				  				if(fieldType == 'price' ){
			  						isNewPrice = newObject['isPrice'];
			  						if(isNewPrice){
			  							result += generateComapareRow(oldvalue, newvalue, true);
			  						}else{
			  							result += generateComapareRow(oldvalue, newvalue, false);
			  						}
				  				}else if(fieldType == 'basePrice'){
			  						isNewPrice = newObject['isBasePrice'];
			  							result += generateComapareRow(oldvalue, newvalue, true);
				  					
				  				}else{
					  				if((oldvalue || newvalue) && oldvalue != newvalue){
					  					result += generateComapareRow(oldvalue, newvalue, false);
					  				}else{
					  					result += generateComapareRow(oldvalue, newvalue, true);
					  				}	
				  				}
				  				result += "</tr>";
					  		});
				  			result += "</table>";
				  			result += "</div>";
				  			result += "</div>";
	 					}
			  		}
			  		
			  		if(isTemplate){
			  			$(".parserTab").find("input[type=checkbox]").prop('checked', false);
			  		}
			  		$('#modalData').html(result);
			  		$('#modalRun').modal('show');
			  	}else if(data.header == 'error'){	
			  		$('#modalRun').modal('hide');
			  		modalAlert('Error : No Template Class or Parser Config!');
			  	}
			  	setButton(false);
		  		$("body").css("cursor", "default");
		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		$("body").css("cursor", "default");
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
	}else{ // PARSER
  		if(checkUrl){
			var parserClass = $("#parserClass").val(); 
			var param = "merchantId="+ merchantId +"&url="+ encodeURIComponent(url) +"&data="+ jsonString + "&parserClass=" + parserClass;	
			$.ajax({
				type: "POST",
				url: 'runparser',
				data: param,
				cache: false,
				async: true,
			  	success: function(data) {
				  	if(data.header == 'success'){
				  		var count = 0;
				  		var result = "";		  		
				  		var setNumberTabButton = "";
				  		$('#stepResultHidden').html("");
				  		$('#buttonResult ul').html("");
			  			$('#modalData').html("<div class='col-md-11 portlet light borderedModal' id='tabContentModal' style='padding-top: 15px'></div><div class='col-md-1' id='tabBtnModal' style='padding-left: 0px; padding-right: 0px'></div>");
				  		$.each(data.list, function(index1, row) {
				  			count++;
			  				
				  			if((count-1) == 0){
				  				result += "<div class='rowTestParserUrl' id='rowTestParserUrl"+(count)+"' style='display: block;'>";			  				
				  				setNumberTabButton += "<button class='btn btn-modalTab' style='background-color: rgb(125, 206, 160);' id='"+(count)+"'onclick='clickOpenTabTest(this)'>Url  "+(count)+"</button>";
				  			}else{
				  				result += "<div class='rowTestParserUrl' id='rowTestParserUrl"+(count)+"' style='display: none;'>";			  							  				
				  				setNumberTabButton += "<button class='btn btn-modalTab' id='"+(count)+"'onclick='clickOpenTabTest(this)'>Url  "+(count)+"</button>";
				  			}
				  			
				  			if(index1.split(" ").length == 2){
				  				index1 = index1.split(" ")[1];
				  			}
				  			
				  			result += "<span style='vertical-align:bottom; font-weight:bold;'>URL "+ (count) +" : </span><span><a href='"+ index1 +"' target='_blank' style='vertical-align:bottom;max-width: 100%;word-wrap: break-word;'>" + index1 + "</a></span>";
				  			result += "<div class='row'>";
				  			
				  			if(row){
					  			$.each(row, function(index2, row) {
					  				
						  			var pictureData = row.pictureData;
						  			if(!pictureData){
						  				pictureData = imgDefault;
						  			}
						  			
						  			setButtonResult(count);			  			
						  			
						  			result += "<div class='col-md-3'>";
						  			result += "    <div style='padding:20px; background-color:#d4efdf; border-bottom:4px solid #7DCEA0; width: 240px;'>";
					  				result += "       <img src=data:image/png;base64,"+ pictureData +" style='max-width:200px;'>";
						  			result += "    </div>";
						  			result += "</div>";
						  			result += "<div style='padding-bottom:20px; background-color:#E9F7EF;width: 240px;'></div>";
						  			result += "<div class='col-md-9'>";
						  			
					  				var numberDisplayField = 0;
					  				if(!parserClass){
					  					var chk = $(".parserTab").find("input[type=checkbox]:checked").each(function () {
//				  						var chk = $("input[type=checkbox]").each(function () {
							  				var ele = $(this).parents('.row');
							  				var displayField = ele.find('.col-md-2').text();

							  				var fieldType = ele.find('table').attr('val');
							  				var value = replaceTag(row[fieldType]);
	
							  				var idStepResult = ele.find('.col-md-10').find('tbody').attr('id');
							  				if(idStepResult != null){
							  					var idStepResult = idStepResult.replace('row','')+'StepResult';
							  					var valueStepResult = row[idStepResult];
							  					
							  					$.each( valueStepResult, function( indexStep, valueStep ){
							  						setStepResultHidden(idStepResult,count,indexStep,valueStep);
							  					});
							  				}
							  				
	
							  				if(fieldType == 'price' || fieldType == 'basePrice'){
							  					var isPrice = true;
							  					if(fieldType == 'price'){
							  						isPrice = row['isPrice'];
							  					}else{
							  						isPrice = row['isBasePrice'];
							  					}
							  					
							  					if(isPrice === false){
							  						value = "<span class='redValidate'>"+ value +"</span>"; 
							  					}
							  				}
							  				
							  				if(fieldType == 'concatId' || fieldType == 'concatWord'){
							  					return;
							  				}
	
							  				result += "<div class='col-md-12' style='display: inline-flex;'>";
							  				result += "    <div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'>";
							  				result += "        <b>"+ displayField +"</b> : " + value;
								  			result += "    </div>";
								  			result += "</div>";
								  		});
					  				}else{
					  					var fieldList = ["name","price","basePrice","description","pictureUrl","expire","realProductId","upc"];
					  					result += "<div class='row one'>";
					  					for(var k in row){
					  						if(fieldList.includes(k)){
					  							result += "<div class='col-md-12' style='display: inline-flex;'>";
					  							result += "    <div style='padding:0px 20px 20px 60px;max-width: 100%;word-wrap: break-word;'>";
					  							result += "        <b>"+ k.charAt(0).toUpperCase() + k.slice(1) +"</b> : " + row[k];
					  							result += "    </div>";
					  							result += "</div>";
					  						}
					  					}
					  					result += "</div>";
					  				}
					  			
					  				result += "</div>";
					  			});
				  			}else{
				  				result += "    <div class='col-md-3'>";
				  				result += "        <div style='padding:20px; background-color:#d4efdf; border-bottom:4px solid #7DCEA0; width: 240px;'>";
				  				result += "            <img src=data:image/png;base64,"+imgDefault+" style='max-width:200px;'>';";
				  				result += "        </div>";
				  				result += "        <div style='padding-bottom:20px; background-color:#E9F7EF;width: 240px;'>";
				  				result += "        </div>";
				  				result += "    </div>";
				  				result += "    <div class='col-md-9'>";
				  				result += "        <div class='col-md-12' style='display: inline-flex;'><div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'><b> Product Name </b> : ไม่พบชื่อสินค้า หรือ ราคา</div></div>";
				  				result += "        <div class='col-md-12' style='display: inline-flex;'><div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'><b> Price </b> : 0</div></div>";
				  				result += "        <div class='col-md-12' style='display: inline-flex;'><div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'><b> Base Price </b> : 0</div></div>";
				  				result += "        <div class='col-md-12' style='display: inline-flex;'><div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'><b> Description </b> : - </div></div>";
				  				result += "        <div class='col-md-12' style='display: inline-flex;'><div style='padding:0px 20px 20px 60px; max-width: 100%;word-wrap: break-word;'><b> Picture Url </b> : - </div></div>";
				  				result += "    </div>";
				  			}
				  			result += "    </div>";
				  			result += "</div>";
				  		});
				  		$('#tabContentModal').html(result);
				  		$('#tabBtnModal').html(setNumberTabButton);
				  		$('#modalRun').modal('show');
				  		$("#rowTestParserUrl1").click();
				  		showStepResultHidden(1);
				  		openTabParser(CURRENT_TAB);
				  	}else if(data.header == 'error'){
				  		modalAlert('Error : Cannot Run for Test');
				  	}
				  	setButton(false);
			  	},error: function (textStatus, errorThrown) {
			  		setButton(false);
			  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			  	}
			});
		}else{
			modalAlert('Error : Not found url for test');
			setButton(false)
		}
		
	}
	
}
function setButtonResult(index){
	var text = "<li><a href='#' class='numUrl"+index+"' onclick='showStepResultHidden("+index+");'>Result URL- "+index+"</a></li>";	
	$('#buttonResult ul').append(text);
}

function setStepResultHidden(idStepResult,index,indexStep,valueStep){
	var text = "<input type='hidden' class="+idStepResult+ index +"-"+indexStep+" value='"+ escape(valueStep) +"'>";
	$('#stepResultHidden').append(text);
}

function showStepResultHidden(numUrl){
	if(numUrl != null && numUrl > -1){
		var chk = $(".parserTab").find("input[type=checkbox]:checked").each(function (index) {
			var ele = $(this).parents('.row');
			var idTbody = ele.find('.col-md-10').find('tbody').attr('id');
			if(idTbody != null){
				var idStepResult = idTbody.replace('row','')+'StepResult';
				
				if($("#stepResultHidden").find("[class^="+idStepResult+numUrl +"-]").length <= 0 ){
					$("#"+idTbody+" > tr > td:nth-child(5) > textarea").text("");	
					$('#buttonResult button').html("Result URL "+numUrl+"<i class='fa fa-angle-up'></i>");
				}else{
					$("#stepResultHidden").find("[class^="+idStepResult+numUrl +"-]").each(function (stepIndex) {
						var valueStepResult = $("#stepResultHidden").find("."+idStepResult+numUrl+"-"+stepIndex).val();
						$("#"+idTbody+" > tr:nth-child("+(stepIndex+1) +") > td:nth-child(5) > textarea").text(unescape(valueStepResult));	
						$('#buttonResult button').html("Result URL "+numUrl+"<i class='fa fa-angle-up'></i>");
					});					
				}
			}
		});
	}
}

function clickOpenTabTest(el) {
	var id = $(el).attr('id');
	$('.rowTestParserUrl').css('display','none');
	$('#rowTestParserUrl'+id).css('display','block');
	$('.btn-modalTab').css('background-color', '#a5e2be').removeClass('active');
	$(el).css('background-color', '#7dcea0').addClass('active');
}

function generateComapareRow(oldvalue, newvalue, isTrue){
	var rowData = "";
	if(isTrue){
		rowData += "	<td class='col-md-5' style='background-color:rgba(0, 255, 91, 0.25);'>"+(oldvalue ? oldvalue : "")+"</td>";
		rowData += "	<td class='col-md-5' style='background-color:rgba(0, 255, 91, 0.25);'>"+(newvalue ? newvalue : "")+"</td>";
	}else{
		rowData += "	<td class='col-md-5' style='background-color:rgba(255, 25, 0, 0.25);'>"+(oldvalue ? oldvalue : "")+"</td>";
		rowData += "	<td class='col-md-5' style='background-color:rgba(255, 25, 0, 0.25);'>"+(newvalue ? newvalue : "")+"</td>";

	}	
	return rowData;
}

function addRow(ele){
	var idTypeProduct = $(ele).closest('tbody').attr('id');
	var row = genRow(idTypeProduct,'inTag', '');
	$(ele).parents('tr').after(row);
	
	var eleTbl = $(ele).parents('table');
	setStepNumber(eleTbl);
	
	assignEditable();
	setActionValue(eleTbl);
}

function deleteRow(ele){
	var eleTbl = $(ele).parents('table');
	$(ele).parents('tr').remove();
	
	setStepNumber(eleTbl);
}


function getProductSample(){
	setButton(true);
	var merchantId = $('#hdMerchantId').val();
	if(!merchantId){
		modalAlert("Please Fill Merchant ID for Search.");
		setButton(false);
		return;
	}
	var param = "merchantId="+ merchantId;
	$.ajax({
		type: "POST",
		url: 'getproductdata',
		data: param,
		cache: false,
		async: true,
	  	success: function(rtn) {
		  	if(rtn.header == 'success'){
		  		var list = rtn.plist;
		  		if(list != null && list.length > 0){
					$('#comparetbl > tbody').html('');
 					for(var i=0; i<list.length; i++){
						var jObject = list[i];
						var num  = i+1;
						var url = (jObject.url ? jObject.url : '');
 						var name = (jObject.name ? jObject.name : '');
 						var price = (jObject.price ? jObject.price : '');
 						var basePrice = (jObject.basePrice ? jObject.basePrice : '');
 						var pictureUrl = (jObject.description ? jObject.pictureUrl : '');
 						var description = (jObject.description ? jObject.description : '');
 						var realProductId = (jObject.realProductId ? jObject.realProductId : '');
 						var upc = (jObject.upc ? jObject.upc : '');
 						var rowData = generateProductDataRow(num, url, name, price, basePrice, pictureUrl, description , realProductId, upc);
 						$('#comparetbl > tbody').append(rowData);
 					}
		  		}else{
		  			modalAlert('No Sample Data for this Merchant ID');
		  		}
		  	}else if(rtn.header == 'error'){
		  		modalAlert('Error : Cannot Run for Test');
		  	}
		  	setButton(false);
	  	},error: function (textStatus, errorThrown) {
	  		setButton(false);
	  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

function generateProductDataRow(number, url, name, price, basePrice, pictureUrl, description , realProductId, upc){
	var rowData = "<tr class='rowCompare'>";
	rowData += "	<td width='5%'  style='text-align:center;'>"+number+"</td>";		
	rowData += "	<td width='45%' style='word-wrap:break-word; max-width:200px;'>"+name+"</td>";	
	rowData += "	<td width='40%' style='word-wrap:break-word; max-width:200px;'>"+url+"</td>";
	rowData += "	<td style='text-align:center;display:none;'>"+price+"</td>";	
	rowData += "	<td style='text-align:center;display:none;'>"+basePrice+"</td>";	
	rowData += "	<td style='text-align:center;display:none;'>"+pictureUrl+"</td>";
	rowData += "	<td style='text-align:center;display:none;'>"+description+"</td>";	
	rowData += "	<td style='text-align:center;display:none;'>"+realProductId+"</td>";
	rowData += "	<td style='text-align:center;display:none;'>"+upc+"</td>";
	rowData += "	<td width='10%' style='text-align:center;'><input type='checkbox' class='rowCheck' onclick='clickTest(this)' style='width:20px;height:20px;'></input></td>";
	rowData += "</tr>";
	return rowData;
}

function clickTest(el) {
	var count = countUrl();
	updateClickTest(count);
	
	if($(el).is(':checked') && count <= CHECKED_LIMIT) {
		$(el).parent().css('background-color', 'rgba(50, 236, 50, 0.33)');
	} else {
		$(el).parent().css('background-color', '');
		$(el).attr('checked', false);
	}
}

function updateClickTest(count) {
	if(count > 0) {
		if(count <= CHECKED_LIMIT) {
			$('#btnTopRunTest').text('COMPARE TEST (' + count + ')');				
		} else {
			modalAlert('Product Data Record : Limit ' + CHECKED_LIMIT + ' URLs');
		}
	} else {
		$('#btnTopRunTest').text('Compare Test');
	}
}

function countUrl() {
	var checkedRows = $('.compareTab#productDataContent').find('.rowCheck:checked').closest('tr');
	return checkedRows.length;
}

function setButton(type){
	$('#btnSearch').prop("disabled", type);
	$('#btnSave').prop("disabled", type);
	$('#btnTopSave').prop("disabled", type);
	$('#btnRunTest').prop("disabled", type);
	$('#btnTopRunTest').prop("disabled", type);
	
	if(type){
		$('.iconSpin').parent().show();
	}else{
		$('.iconSpin').parent().hide();
	}
}

function addUrlRow(ele,data){
	var result = "<div class='row'>";
	result += "<div class='col-md-1 textUrl'>URL :</div>";
	result += "<div class='col-md-9'>";
	result += "<input type='text' name='url[]' class='form-control testUrl' value='"+(data!=null?data:"")+"'>";
	result += "</div>";
	result += "<div class='col-md-2'>";
	result += "<button type='button' class='btn btn-md btn-primary addUrlButton' onclick='addUrlRow(this)'>ADD</button> ";
	result += "<button type='button' class='btn btn-md btn-danger deleteUrlButton' onclick='deleteUrlRow(this)'>DEL<i class='fa fa-trash' aria-hidden='true'></i></button>";
	result += "</div>";
	result += "</div>";
	$(ele).parents('#parserSubTab .row').after(result);
	setUrlNumber();
}


function setUrlNumber(){
	var i = 1;
	$('.textUrl').each(function() {
		$(this).text('URL '+ i +' :');
		i++;
	});
}

function clearInput() {
	$('.clearbutton').addClear({
		  closeSymbol: "",
		  symbolClass: 'glyphicon glyphicon-remove-circle',
		  color: "#9e9e9e",
		  top: 10,
		  right: 0,
		  returnFocus: true,
		  showOnLoad: false,
		  onClear: null,
		  hideOnBlur: false,
		  clearOnEscape: true,
		  wrapperClass: '',
		  zindex: 100

		});
}

function setAllActionValue(ele){
	$(ele).each(function() {
		setActionValue($(this));
	});
}

function disableTable(ele){
	ele = ele.parents('.row');
	ele.find('.filterType, .editText').editable('option', 'disabled', true);
	ele.find('button').prop("disabled", true);
	ele.find('.col-md-10').addClass('tbldisable');	
}

function disableEditTable(){
	
	disableTable($('#tblname'));
	$('#chkName').attr('disabled', 'true');
	
	disableTable($('#tblconcatId'));
	$('#chkConcatId').attr('disabled', 'true');
	
	disableTable($('#tblconcatWord'));
	$('#chkConcatWord').attr('disabled', 'true');
}

function modalAlert(result){
	$('#modalAlertData').html('<span style="color:black;font-size:15px;">'+result+'</span>');
	$('#modalAlert').modal('show');
}

function setStepNumber(ele){
	var i = 1;
	ele.find('.step').each(function() {
		$(this).text(i);
		i++;
	});
}

function replaceTag(value){
	if(value){
		value = value.toString().replace(/\&/g, '&amp;');
		value = value.toString().replace(/</g, '&lt;').replace(/>/g, '&gt;');
	}
	return value;
}

function setShowValue(filter, value){
	value = replaceTag(value);
	var result = "";
	if(jQuery.inArray(filter, filterEmpty) > -1){
		if(value.length >0 && value.trim()==""){
			value = "'"+value+"'";
		}
		result = "<a class='editText'>"+ value +"</a>";
	}else if(jQuery.inArray(filter, filterTwoValue) > -1){
		var arr = [ '', ''];
		if(value != ''){
			var tmp = value.split("|,|");
			if(tmp.length == 2){
				for(var i = 0; i< tmp.length ; i++){
					if(tmp[i].length >0 && tmp[i].trim()==""){
						tmp[i] = "'"+tmp[i]+"'";
					}
				}
				arr = tmp;
			}
		}
		result = "<a class='editText'>"+ arr[0] +"</a><span style='padding-left:7%;padding-right:7%'>,</span><a class='editText'>"+ arr[1] +"</a>";
	} else if(jQuery.inArray(filter, filterNonValue) > -1){
		result = "-";
	}
	return result;
	
}

function genRow( field, filter, value){
	var namestepResult = field.replace('row','');
	var result = "<tr>";
	result += "<td class='step'></td>";
	result += "<td><a class='filterType'>"+ pConfigDisplay[filter] +"</a></td>";
	result += "<td style='max-width: 200px; word-wrap: break-word;'>"+ setShowValue(filter, value) +"</td>";
	result += "<td>";
	result += "<button type='button' class='btn btn-md btn-danger btn-circle' onclick='deleteRow(this);'><i class='fa fa-minus' style='margin-left: -4px;' aria-hidden='true'></i></button>";
	result += "<button type='button' class='btn btn-md btn-primary btn-circle' onclick='addRow(this);'><i class='fa fa-plus' style='margin-left: -4px;'></i></button>";
	result += "</td>";
	result += "<td style='max-width: 200px; word-wrap: break-word;'><textarea class='form-control "+namestepResult+"StepResult' readonly style='resize: vertical;'></textarea></td>";
	result += "</tr>";
	
	return result;
}

function search(checkClear){
	setButton(true);
	if(checkClear){
		clearAllTable();		
	}
	
	var merchantId = $('#merchantId').val().trim();
	if(Math.floor(merchantId) != merchantId || !$.isNumeric(merchantId)){
		modalAlert("Merchant Data Not Found.");
		setButton(false);
		return;
	}
	var param = "merchantId="+ merchantId;
	$.ajax({
		type: "GET",
		url: 'searchparser',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		var result = "";
		  		var sessionField = "";
		  		var active = data.merchant.active;
		  		var parser = data.parserClass;
				
		  			if(data.plist){
		  			
					$.each(data.plist, function(index, row) {
			  			
						var field = row.fieldType;
						var filter = row.filterType;
						var value = row.value;

						if(field != sessionField){
							if(sessionField != ""){
								var eleRowSession = $('#row' + sessionField);
						  		$(eleRowSession).html(result);
						  		setStepNumber($(eleRowSession).parents('table'));
							}
							sessionField = field;
							result = "";
						}
	
						result += genRow(field, filter, value);
			  		});
					
					var eleRowSession = $('#row' + sessionField);
			  		$(eleRowSession).html(result);
			  		setStepNumber($(eleRowSession).parents('table'));
			  		assignEditable();
			  		$('table > tbody:has(tr)').each(function () {
			  			$(this).parents(".row").find("input[type=checkbox]").attr('checked', 'checked').val(1).prop('checked', true );		  			
			  			$(this).parents('table').each(function() {
			  				enableTable($(this));
			  			});
			  		});
		  		}
		  		if(data.parserClass!='web.parser.DefaultUserHTMLParser'){
		  			parser = data.parserClass;
		  			$('#parserClass').val(parser);
		  			openTab("Parser");
		  			openTabParser('pName');
		  		}else{
		  			$('#parserClass').val("");
		  			if(result == '' && data.parserClass!='web.parser.DefaultUserHTMLParser'){
		  				openTab("Tool");		  				
		  			}else{
		  				openTab("Parser");
		  				openTabParser('pName');
		  			}
		  		}
		  		
		  		if(active == "1" || active == "3" || active == "4" || active == "11" || active == "14" ){
		  			disableEditTable();
		  		}
		  		
		  		$('#merchantName').html("<span><span>ID : "+ data.merchant.id +"</span> <span>Name : " + data.merchant.name + "</span> <span id='parser'>Parser : " + parser + "</span></span>");
		  		$('#hdMerchantId').val(data.merchant.id);
		  		$('#tabArea').show();

		  		setAllActionValue($('.containConfig').find('table'));
		  	} else if(data.header == 'notFoundMerchant'){
		  		$('#tabArea').hide();
		  		$('#process-tab').css("display", "none");
		  		modalAlert('Merchant Data Not Found.');
		  	} 
		  	$('#buttonResult ul').html("");
		  	$('#buttonResult button').html("Result");
		  	setButton(false);
	  	},error: function (textStatus, errorThrown) {
	  		setButton(false);
	  		$('#tabArea').hide();
	  		$('#process-tab').css("display", "none");
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

function disableTable(ele){
	ele = ele.parents('.row');
	ele.find('.filterType, .editText').editable('option', 'disabled', true);
	ele.find('button').prop("disabled", true);
	ele.find('.col-md-10').addClass('tbldisable');	
}

function clearAllTable(){	
	$('table > tbody').html('');
	$('.col-md-10').addClass('tbldisable');
	$('input[type=checkbox]').removeAttr('checked').val(0).prop('checked', false).removeAttr("disabled");
	deleteUrlAllRow();
	$('table').each(function() {
		disableTable($(this));
	});
	
	$('#hdMerchantId').val('');
	$('#merchantName').html('');
}

function enableTable(ele){
	ele = ele.parents('.row');
	ele.find('.filterType, .editText').editable('option', 'disabled', false);
	ele.find('button').prop("disabled", false);
	ele.find('.col-md-10').removeClass('tbldisable');	
}

function tool0AddUrlBox(){
	var dataBox = ""+
				"<div class='tool0ItemDataBox'>" +
				"	<div class='row'>" +
				"		<div class='tool0Url'>" +
				"			<div class='col-md-2 label-field'>URL : </div>" +
				"			<div class='col-md-7'>" +
				"				<input type='text' class='form-control tool0Url clearbutton' name='tool0Url' value='' placeholder='Require.''>" +
				"			</div>" +
				"		</div>" +
				"		<div class='tool0Delete'>" +
				"			<div class='col-md-1'>" +
				"				<button type='button' class='btn btn-sm btn-danger tool0DeleteBox' >Delete&nbsp;<span class='fa fa-trash-o'></span></button>" +
				"			</div>" +
				"		</div>" +
				"	</div>" +
				"</div>"+
				"";
	$("#tool0ItemDataArea").append(dataBox);
	tool0BindFunction();
	clearInput();
}

function tool0GetData(){
	var jsonString;
	var jsonObj = [];
	$('.tool0ItemDataBox').each(function(){
		var item = {};
		$(this).find(":input").each(function(){
			var name = $(this).attr("name");
			var value = $(this).val();
			if(name != null && value != null){
				item [name] = value;
			}
		});
		jsonObj.push(item);
	});
	jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	return jsonString;
}

function tool0ValidateInputData(){
	var isValidate = true;
	$('input[name=tool0Url]').each(function(){
		if($(this).val() == null || $(this).val() == ""){
			$('#tool0AlertText').text("Require product url in every box.");
			isValidate = false;
		}
	});
	if(isValidate){
		$('#tool0AlertText').text("");
	}
	return isValidate;
}

function tool0Start(typeFunction){
	var mId = $('#merchantId').val();
	var jsonData = tool0GetData();
	var typeFunction = typeFunction;
	if(tool0ValidateInputData() == false) return;
	
	if(mId <= 0){
		modalAlert("Merchant ID Not Found.");
		return;
	}

	$('#modalData').html('<h1 style="text-align:center;">Running, please wait...</h1>');
	$('#modalRun').modal('show');
	$("body").css("cursor", "wait");
	
	var param = "merchantId=" + mId + "&jsonData=" + jsonData ;
	
	$.ajax({
		type: "POST",
		url: 'tool0GetParser',
		data: param,
		cache: false,
		async: true,
		/*beforeSend: function (xhr) {
		    xhr.setRequestHeader("Content-Length", jsonData.length);
		    xmlHttp.setRequestHeader("Connection", "close");
		},*/
	  	success: function(data) {

	  		if(data.header == 'success'){
		  		if(data.result != null){
		  			var result = data.result;
		  			if(typeFunction == "Intersection"){
		  				$("#tool1ItemDataArea").text("");
		  				for(var i=0; i<result.length; i++){
		  					tool1AddProductBox(JSON.stringify(result[i]));
		  				}
		  			}else if(typeFunction == "Union"){
		  				$("#tool2ItemDataArea").text("");
		  				for(var i=0; i<result.length; i++){
		  					tool2AddProductBox(JSON.stringify(result[i]));
		  				}
		  			}
		
		  		}else{
		  			$('#modalData').html("Success, but no result generated.");
		  		}
		  		
		  	}else {	
		  		
		  		$('#modalData').html("Error, please check input data.");
		  		$('#modalRun').modal('show');
		  		
		  	}
		  	setButton(false);
		  	$('#modalRun').modal('hide');
	  		$("body").css("cursor", "default");
	  	},error: function (textStatus, errorThrown) {
	  		setButton(false);
	  		$("body").css("cursor", "default");
	  		$('#modalData').html("Error, server exception. <br />Error Status - " + textStatus + " <br />" +  "Error Thrown - " + errorThrown);
	  		$('#modalRun').modal('show');
	  	}
	});
}

function tool0BindFunction(){
	$(".tool0DeleteBox").bind("click", function(btn){
		$(btn.target).closest(".tool0ItemDataBox").remove();
	})
}

function tool0to1Start(){
	var typeFunction ='Intersection';
	tool0Start(typeFunction);
}

function tool0to2Start(){
	var typeFunction ='Union';
	tool0Start(typeFunction);
}

function tool1AddProductBox(dataInput){
	
	var data = null;
	if(dataInput != null){
		data = JSON.parse(dataInput);
	}
	
	var dataBox = "" +
			"<div class='tool1ItemDataBox'>" +
			"	<div class='row'>" +
			"		<div class='tool1Url'>" +
			"			<div class='col-md-2 label-field'>URL : </div>" +
			"			<div class='col-md-7'>" +
			"				<input type='text' class='form-control tool1Url clearbutton' name='tool1Url' value='"+(data!=null && data.url!=null?data.url:"")+"' placeholder='Require.'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1Type'>" +
			"			<div class='col-md-2'>" +
			"				<div class='input-group'>" +
			"					<div class='input-group-btn'>" +
			"						<button type='button' class='btn dropdown-toggle tool1TypeFilter' data-toggle='dropdown' >General&nbsp;&nbsp;<span class='fa fa-caret-down'></span></button>" +
			"						<ul class='dropdown-menu'>" +
			"							<li><a set-type='0' class='tool1TypeSelector' >General</a></li>" +
			"							<li><a set-type='1' class='tool1TypeSelector' >Have BasePrice</a></li>" +
			"							<li><a set-type='2' class='tool1TypeSelector' >Have Expire</a></li>" +
			"						</ul>" +
			"						<input type='hidden' class='tool1type' name='tool1Type' value='0'>" +
			"	                </div>" +
			"	            </div>" +
			"	        </div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool1Name tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>Name : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1Name' value='"+(data!=null && data.name!=null?data.name:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1Price tool1PriceData'>" +
			"			<div class='col-md-2 label-field'>Price : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1Price' value='"+(data!=null && data.price!=null?data.price:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool1Desc tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>Desc : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1Desc' value='"+(data!=null && data.desc!=null?data.desc:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1BasePrice tool1BasePriceData'>" +
			"			<div class='col-md-2 label-field'>BasePrice : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1BasePrice' value='"+(data!=null && data.baseprice!=null?data.baseprice:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool1PictureUrl tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>PictureUrl : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1PictureUrl' value='"+(data!=null && data.picurl!=null?data.picurl:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1UPC tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>UPC : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1UPC' value=''>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool1RealProductId tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>RealProductId : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1RealProductId' value='"+(data!=null && data.realproductid!=null?data.realproductid:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1ConcatProductId tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>Concat Id : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1ConcatProductId' value=''>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool1ConcatWord tool1GeneralData'>" +
			"			<div class='col-md-2 label-field'>ConcatWord : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1ConcatWord' value=''>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1Expire tool1ExpireData'>" +
			"			<div class='col-md-2 label-field'>Expire : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool1Expire' value=''>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool1Delete'>" +
			"			<div class='col-md-1'>" +
			"				<button type='button' class='btn btn-sm btn-danger tool1DeleteBox' >Delete&nbsp;<span class='fa fa-trash-o'></span></button>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"</div>" +
			"";
	
	if($("#tool1ItemDataArea").text().trim().length > 0){
		dataBox = dataBox.replace("<div class='tool1ItemDataBox'>","<div class='tool1ItemDataBox'><br /><br />"); 
	}
	
	$("#tool1ItemDataArea").append(dataBox);
	$('#tool1AlertText').text("");

	tool1BindFunction();
	tool1SetInputForm();
	clearInput();
}

function tool1BindFunction(){
	$(".tool1TypeSelector").bind("click", function(btn){
		var txt = $(btn.target).text();
		var val = $(btn.target).attr('set-type');
		$(btn.target).closest(".input-group-btn").find(".tool1TypeFilter").text(txt).append('&nbsp;<span class="fa fa-caret-down"></span>');;
		$(btn.target).closest(".input-group-btn").find(".tool1type").val(val);
		tool1SetInputForm();
	})
	$(".tool1DeleteBox").bind("click", function(btn){
		$(btn.target).closest(".tool1ItemDataBox").remove();
	})
}

function tool1SetInputForm(){
	$('.tool1ItemDataBox').each(function(){
		var type = $(this).find(".tool1type").val();
		if(type == 0){ // General
			$(this).find(".tool1GeneralData :input").prop('disabled', false);
			$(this).find(".tool1PriceData :input").prop('disabled', false);
			$(this).find(".tool1BasePriceData :input").prop('disabled', true).val('');
			$(this).find(".tool1ExpireData :input").prop('disabled', true).val('');
		}else if(type == 1){ // Have BasePrice
			$(this).find(".tool1GeneralData :input").prop('disabled', true).val('');
			$(this).find(".tool1PriceData :input").prop('disabled', false);
			$(this).find(".tool1BasePriceData :input").prop('disabled', false);
			$(this).find(".tool1ExpireData :input").prop('disabled', true).val('');
		}else if(type == 2){ // Have Expire
			$(this).find(".tool1GeneralData :input").prop('disabled', true).val('');
			$(this).find(".tool1PriceData :input").prop('disabled', true).val('');
			$(this).find(".tool1BasePriceData :input").prop('disabled', true).val('');
			$(this).find(".tool1ExpireData :input").prop('disabled', false);
		}
	});
}

function tool1GetData(){
	var jsonString;
	var jsonObj = [];
	$('.tool1ItemDataBox').each(function(){
		var item = {};
		$(this).find(":input").each(function(){
			var name = $(this).attr("name");
			var value = $(this).val();
			if(name != null && value != null){
				item [name] = value;
			}
		});
		jsonObj.push(item);
	});
	jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	return jsonString;
}

function tool1ValidateInputData(){
	var isValidate = true;
	$('input[name=tool1Url]').each(function(){
		if($(this).val() == null || $(this).val() == ""){
			$('#tool1AlertText').text("Require product url in every box.");
			isValidate = false;
		}
	});
	if(isValidate){
		$('#tool1AlertText').text("");
	}
	return isValidate;
}

function getLength(data){
	if(data != null){
		return data.length;
	}else{
		return 0;
	}
}

function tool1Start(){
	
	var mId = $('#merchantId').val();
	var jsonData = tool1GetData();
	
	if(tool1ValidateInputData() == false) return;
	
	if(mId <= 0){
		modalAlert("Merchant ID Not Found.");
		return;
	}

	$('#modalData').html('<h1 style="text-align:center;">Running, please wait...</h1>');
	$('#modalRun').modal('show');
	$("body").css("cursor", "wait");
	$('#btnTool1Start').button('loading');
	var param = "merchantId=" + mId + "&jsonData=" + jsonData;
	
	$.ajax({
		type: "POST",
		url: 'tool1GetParser',
		data: param,
		cache: false,
		async: true,
//		beforeSend: function (xhr) {
//		    xhr.setRequestHeader("Content-Length", jsonData.length);
//		},
	  	success: function(data) {

	  		if(data.header == 'success'){
		  		
		  		if(data.result != null){
		  			var rowText = "";
		  			rowText += "<div class='row'>";                                                                                 
		  			rowText += "	<div class='col-md-22'>";                                                                        
		  			rowText += "		<table id='tool1ResultTable' class='table table-striped table-bordered'>";                  
		  			rowText += "			<thead>";                                                                               
		  			rowText += "				<tr class='heading'>";                                                              
		  			rowText += "					<th style='text-align:center;'> Field Name </th>";                              
		  			rowText += "					<th style='text-align:center;'> Success </th>";                                 
		  			rowText += "					<th style='text-align:center;'> Action </th>";                                  
		  			rowText += "					<th style='text-align:center;'> Value </th>";                                   
		  			rowText += "				</tr>";                                                                             
		  			rowText += "			</thead>";                                                                              
		  			rowText += "			<tbody>";                                                                               
		  			rowText += tool1GenerateResultBox("name", data.result.tool1Name, getLength(data.result.tool1Name)) ;
		  			rowText += tool1GenerateResultBox("description", data.result.tool1Desc, getLength(data.result.tool1Desc)) ;
		  			rowText += tool1GenerateResultBox("price", data.result.tool1Price, getLength(data.result.tool1Price)) ;
		  			rowText += tool1GenerateResultBox("basePrice", data.result.tool1BasePrice, getLength(data.result.tool1BasePrice)) ;
		  			rowText += tool1GenerateResultBox("pictureUrl", data.result.tool1PictureUrl, getLength(data.result.tool1PictureUrl)) ;
		  			rowText += tool1GenerateResultBox("upc", data.result.tool1UPC, getLength(data.result.tool1UPC)) ;
		  			rowText += tool1GenerateResultBox("realProductId", data.result.tool1RealProductId, getLength(data.result.tool1RealProductId)) ;
		  			rowText += tool1GenerateResultBox("concatId", data.result.tool1ConcatProductId, getLength(data.result.tool1ConcatProductId)) ;
		  			rowText += tool1GenerateResultBox("concatWord", data.result.tool1ConcatWord, getLength(data.result.tool1ConcatWord)) ;
		  			rowText += tool1GenerateResultBox("expire", data.result.tool1Expire, getLength(data.result.tool1Expire)) ;
		  			rowText += "			</tbody>";                                                                                                                                          
		  			rowText += "			<tfoot>";                                                                                                                                           
		  			rowText += "				<tr class='footing'>";                                                                                                                          
		  			rowText += "					<td colspan='4' style='text-align:center;'>";                                                                                               
		  			rowText += "						<button type='button' class='btn btn-default btn-lg' data-dismiss='modal'>Close</button> ";                                   
		  			rowText += "						<button type='button' class='btn btn-info btn-lg' onclick='tool1SaveResult()' id='btnTool1SaveResult' data-loading-text='<i class='fa fa-spinner fa-spin'></i>Save</button>";                                         
		  			rowText += "					</td>";                                                                                                                                     
		  			rowText += "					<div id='saveResult'></div>";                                                                                                                                     
		  			rowText += "				</tr>";                                                                                                                                         
		  			rowText += "			</tfoot>";                                                                                                                                          
		  			rowText += "		</table>";                                                                                                                                              
		  			rowText += "	</div>";                                                                                                                                                    
		  			rowText += "</div>";                                                                                                                                                        
		  			$('#modalData').html(rowText);
		  			
		  		}else{
		  			$('#modalData').html("Success, but no result generated.");
		  		}
		  		
		  		$('#modalRun').modal('show');
		  		$('#btnTool1Start').button('reset');
		  	}else {	
		  		
		  		$('#modalData').html("Error, please check input data.");
		  		$('#modalRun').modal('show');
		  		$('#btnTool1Start').button('reset');
		  	}
		  	setButton(false);
	  		$("body").css("cursor", "default");
	  	},error: function (textStatus, errorThrown) {
	  		setButton(false);
	  		$("body").css("cursor", "default");
	  		$('#modalData').html("Error, server exception. <br />Error Status - " + textStatus + " <br />" +  "Error Thrown - " + errorThrown);
	  		$('#modalRun').modal('show');
	  	}
	});
}

function tool1GenerateResultBox(rowName, rowData, rowLength){
	var rtnText = "";
	
	if(rowLength == 0){
		rtnText += "<tr class='rowPattern'>";
		rtnText += "	<td style='text-align:center; color:red;'>"+rowName+"</td>";
		rtnText += "	<td style='text-align:center; color:red;'><i class='fa fa-times'></i></td>";
		rtnText += "	<td style='text-align:center;'></td>";
		rtnText += "	<td style='text-align:left;'></td>";
		rtnText += "</tr>";
	}else{
		for(var i=0; i<rowLength; i++){
			rtnText += "<tr class='rowPattern'>";
			rtnText += "	<td style='text-align:center;'>"+rowName+"</td>";
			rtnText += "	<td style='text-align:center;'><i class='fa fa-check'></i></td>";
			rtnText += "	<td style='text-align:center;'>"+(rowData[i].indexOf("|,|") == -1?(rowName!="price" && rowName!="basePrice" ?(rowName!="description" ?"JSoupSelectorTxt":"JSoupSelectorLongTxt"):"JSoupSelectorNum"):"JSoupSelectorAttr")+"</td>";
			rtnText += "	<td style='text-align:left;'>"+rowData[i]+"</td>";
			rtnText += "</tr>";
		}
	}
	
	return rtnText;
}

function tool1GetResultData(){

	var jsonObj = {};
	var fieldObj = [];
	var table = document.getElementById("tool1ResultTable");
	var startField = "";
	for (var r = 1, n = table.rows.length; r < n-1; r++) {
		var fieldName = table.rows[r].cells[0].textContent;
		if(jsonObj[fieldName] != null){
			fieldObj = jsonObj[fieldName];
		}else{
			fieldObj = [];
		}
		
		if(!table.rows[r].cells[2].textContent && !table.rows[r].cells[3].textContent){
			continue;
		}
		
        var item = {};        	    	
    	item ["filter"] = table.rows[r].cells[2].textContent;
    	item ["value"] 	= table.rows[r].cells[3].textContent;
        
    	fieldObj.push(item);
        jsonObj[fieldName] = fieldObj;
    }
    jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	
	return jsonString;
}

function tool1SaveResult(){

	var mId = $('#merchantId').val();
    var jsonString = tool1GetResultData();
	var param = "merchantId=" + mId + "&data=" + jsonString;
	$('#btnTool1SaveResult').button('loading');
    $.ajax({
		type: "POST",
		url: 'savefield',
		data: param,
		cache: false,
		async: true,
//		beforeSend: function (xhr) {
//		    xhr.setRequestHeader("Content-Length", jsonString.length);
//		},
	  	success: function(json) {
	
		  	if(json.header == 'success'){
		  		var noValue = true;
		  		$('.testUrl').each(function(){
		  			if($(this).val() != null && $(this).val() != ""){
		  				noValue = false;
		  			}
		  		});
		  		if(noValue){
		  			$('.deleteUrlButton').each(function(){
		  				deleteUrlRow($(this));
		  			});
		  			$('input[name=tool1Url]').each(function(){
		  				if($(this).val() != null && $(this).val() != ""){
		  					var target = $('.addUrlButton').last();
		  					addUrlRow($(target), $(this).val());
		  				}
		  			});
		  			$('.addUrlButton').first().closest('.row').remove();
//		  			$('#parserSubTab').trigger('click');
		  			setUrlNumber();
		  		}
		  		
		  		search(false);
		  		openTab("Parser");
		  		$('#modalRun').modal('hide');
		  		$('#btnTool1SaveResult').button('reset');
		  	}
	  	},
	  	error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
    
	return jsonString;
}

function tool2AddProductBox(dataInput){
	
	var data = null;
	if(dataInput != null){
		data = JSON.parse(dataInput);
	}
	
	var dataBox = "" +
			"<div class='tool2ItemDataBox'>" +
			"	<div class='row'>" +
			"		<div class='tool2Url'>" +
			"			<div class='col-md-2 label-field'>URL : </div>" +
			"			<div class='col-md-7'>" +
			"				<input type='text' class='form-control tool2Url clearbutton' name='tool2Url' value='"+(data!=null && data.url!=null?data.url:"")+"' placeholder='Require.'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2Type'>" +
			"			<div class='col-md-2'>" +
			"				<div class='input-group'>" +
			"					<div class='input-group-btn'>" +
			"						<button type='button' class='btn dropdown-toggle tool2TypeFilter' data-toggle='dropdown' >General&nbsp;&nbsp;<span class='fa fa-caret-down'></span></button>" +
			"						<ul class='dropdown-menu'>" +
			"							<li><a set-type='0' class='tool2TypeSelector' >General</a></li>" +
			"							<li><a set-type='1' class='tool2TypeSelector' >Have BasePrice</a></li>" +
			"							<li><a set-type='2' class='tool2TypeSelector' >Have Expire</a></li>" +
			"						</ul>" +
			"						<input type='hidden' class='tool2type' name='tool2Type' value='0'>" +
			"	                </div>" +
			"	            </div>" +
			"	        </div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool2Name tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>Name : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2Name' value='"+(data!=null && data.name!=null?data.name:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2Price tool2PriceData'>" +
			"			<div class='col-md-2 label-field'>Price : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2Price' value='"+(data!=null && data.price!=null?data.price:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool2Desc tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>Desc : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2Desc' value='"+(data!=null && data.desc!=null?data.desc:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2BasePrice tool2BasePriceData'>" +
			"			<div class='col-md-2 label-field'>BasePrice : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2BasePrice' value='"+(data!=null && data.baseprice!=null?data.baseprice:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool2PictureUrl tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>PictureUrl : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2PictureUrl' value='"+(data!=null && data.picurl!=null?data.picurl:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2UPC tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>UPC : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2UPC' value=''>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool2RealProductId tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>RealProductId : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2RealProductId' value='"+(data!=null && data.realproductid!=null?data.realproductid:"")+"'>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2ConcatProductId tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>Concat Id : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2ConcatProductId' value=''>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"	<div class='row'>" +
			"		<div class='tool2ConcatWord tool2GeneralData'>" +
			"			<div class='col-md-2 label-field'>ConcatWord : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2ConcatWord' value=''>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2Expire tool2ExpireData'>" +
			"			<div class='col-md-2 label-field'>Expire : </div>" +
			"			<div class='col-md-3'>" +
			"				<input type='text' class='form-control clearbutton' name='tool2Expire' value=''>" +
			"			</div>" +
			"		</div>" +
			"		<div class='tool2Delete'>" +
			"			<div class='col-md-2'>" +
			"				<button type='button' class='btn btn-sm btn-danger tool2DeleteBox' >Delete&nbsp;<span class='fa fa-trash-o'></span></button>" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"</div>" +
			"";
	
	if($("#tool2ItemDataArea").text().trim().length > 0){
		dataBox = dataBox.replace("<div class='tool2ItemDataBox'>","<div class='tool2ItemDataBox'><br /><br />"); 
	}
	
	$("#tool2ItemDataArea").append(dataBox);
	$('#tool2AlertText').text("");
	tool2BindFunction();
	tool2SetInputForm();
	clearInput();
}

function tool2BindFunction(){
	$(".tool2TypeSelector").bind("click", function(btn){
		var txt = $(btn.target).text();
		var val = $(btn.target).attr('set-type');
		$(btn.target).closest(".input-group-btn").find(".tool2TypeFilter").text(txt).append('&nbsp;<span class="fa fa-caret-down"></span>');;
		$(btn.target).closest(".input-group-btn").find(".tool2type").val(val);
		tool2SetInputForm();
	})
	$(".tool2DeleteBox").bind("click", function(btn){
		$(btn.target).closest(".tool2ItemDataBox").remove();
	})
}

function tool2SetInputForm(){
	$('.tool2ItemDataBox').each(function(){
		var type = $(this).find(".tool2type").val();
		if(type == 0){ // General
			$(this).find(".tool2GeneralData :input").prop('disabled', false);
			$(this).find(".tool2PriceData :input").prop('disabled', false);
			$(this).find(".tool2BasePriceData :input").prop('disabled', true).val('');
			$(this).find(".tool2ExpireData :input").prop('disabled', true).val('');
		}else if(type == 1){ // Have BasePrice
			$(this).find(".tool2GeneralData :input").prop('disabled', true).val('');
			$(this).find(".tool2PriceData :input").prop('disabled', false);
			$(this).find(".tool2BasePriceData :input").prop('disabled', false);
			$(this).find(".tool2ExpireData :input").prop('disabled', true).val('');
		}else if(type == 2){ // Have Expire
			$(this).find(".tool2GeneralData :input").prop('disabled', true).val('');
			$(this).find(".tool2PriceData :input").prop('disabled', true).val('');
			$(this).find(".tool2BasePriceData :input").prop('disabled', true).val('');
			$(this).find(".tool2ExpireData :input").prop('disabled', false);
		}
	});
}

function tool2GetData(){
	var jsonString;
	var jsonObj = [];
	$('.tool2ItemDataBox').each(function(){
		var item = {};
		$(this).find(":input").each(function(){
			var name = $(this).attr("name");
			var value = $(this).val();
			if(name != null && value != null){
				item [name] = value;
			}
		});
		jsonObj.push(item);
	});
	jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	return jsonString;
}

function tool2ValidateInputData(){
	var isValidate = true;
	$('input[name=tool2Url]').each(function(){
		if($(this).val() == null || $(this).val() == ""){
			$('#tool2AlertText').text("Require product url in every box.");
			isValidate = false;
		}
	});
	if(isValidate){
		$('#tool2AlertText').text("");
	}
	return isValidate;
}

function tool2Start(){
	
	var mId = $('#merchantId').val();
	var jsonData = tool2GetData();
	
	if(tool2ValidateInputData() == false) return;
	
	if(mId <= 0){
		modalAlert("Merchant ID Not Found.");
		return;
	}
	
	$('#modalData').html('<h1 style="text-align:center;">Running, please wait...</h1>');
	$('#modalRun').modal('show');
	$("body").css("cursor", "wait");
	
	var param = "merchantId=" + mId + "&jsonData=" + jsonData;
	
	$.ajax({
		type: "POST",
		url: 'tool2GetParser',
		data: param,
		cache: false,
		async: true,
//		beforeSend: function (xhr) {
//			xhr.setRequestHeader("Content-Length", jsonData.length);
//		},
		success: function(data) {
			
			if(data.header == 'success'){
				
				if(data.result != null){
					var rowText = "";
					rowText += "<div class='row'>";                                                                                 
					rowText += "	<div class='col-md-12'>";                                                                        
					rowText += "		<table id='tool2ResultTable' class='table table-striped table-bordered'>";                  
					rowText += "			<thead>";                                                                               
					rowText += "				<tr class='heading'>";                                                              
					rowText += "					<th style='text-align:center;'> Field Name </th>";                              
					rowText += "					<th style='text-align:center;'> Success </th>";                                 
					rowText += "					<th style='text-align:center;'> Action </th>";                                  
					rowText += "					<th style='text-align:center;'> Value </th>";                                   
					rowText += "				</tr>";                                                                             
					rowText += "			</thead>";                                                                              
					rowText += "			<tbody>";                                                                               
					rowText += tool2GenerateResultBox("name", data.result.tool2Name, getLength(data.result.tool2Name)) ;
					rowText += tool2GenerateResultBox("description", data.result.tool2Desc, getLength(data.result.tool2Desc)) ;
					rowText += tool2GenerateResultBox("price", data.result.tool2Price, getLength(data.result.tool2Price)) ;
					rowText += tool2GenerateResultBox("basePrice", data.result.tool2BasePrice, getLength(data.result.tool2BasePrice)) ;
					rowText += tool2GenerateResultBox("pictureUrl", data.result.tool2PictureUrl, getLength(data.result.tool2PictureUrl)) ;
					rowText += tool2GenerateResultBox("upc", data.result.tool2UPC, getLength(data.result.tool2UPC)) ;
					rowText += tool2GenerateResultBox("realProductId", data.result.tool2RealProductId, getLength(data.result.tool2RealProductId)) ;
					rowText += tool2GenerateResultBox("concatId", data.result.tool2ConcatProductId, getLength(data.result.tool2ConcatProductId)) ;
					rowText += tool2GenerateResultBox("concatWord", data.result.tool2ConcatWord, getLength(data.result.tool2ConcatWord)) ;
					rowText += tool2GenerateResultBox("expire", data.result.tool2Expire, getLength(data.result.tool2Expire)) ;
					rowText += "			</tbody>";                                                                                                                                          
					rowText += "			<tfoot>";                                                                                                                                           
					rowText += "				<tr class='footing'>";                                                                                                                          
					rowText += "					<td colspan='4' style='text-align:center;'>";                                                                                               
					rowText += "						<button type='button' class='btn btn-default btn-lg' data-dismiss='modal'>Close</button> ";                                   
					rowText += "						<button type='button' class='btn btn-info btn-lg' onclick='tool2SaveResult()'>Save</button>";                                         
					rowText += "					</td>";                                                                                                                                     
					rowText += "					<div id='saveResult'></div>";                                                                                                                                     
					rowText += "				</tr>";                                                                                                                                         
					rowText += "			</tfoot>";                                                                                                                                          
					rowText += "		</table>";                                                                                                                                              
					rowText += "	</div>";                                                                                                                                                    
					rowText += "</div>";                                                                                                                                                        
					$('#modalData').html(rowText);
					
				}else{
					$('#modalData').html("Success, but no result generated.");
				}
				
				$('#modalRun').modal('show');
				
			}else {	
				
				$('#modalData').html("Error, system failed.");
				$('#modalRun').modal('show');
				
			}
			setButton(false);
			$("body").css("cursor", "default");
		},error: function (textStatus, errorThrown) {
			setButton(false);
			$("body").css("cursor", "default");
			$('#modalData').html("Error, server exception. <br />Error Status - " + textStatus + " <br />" +  "Error Thrown - " + errorThrown);
			$('#modalRun').modal('show');
		}
	});
}

function tool2GenerateResultBox(rowName, rowData, rowLength){
	var rtnText = "";
	
	if(rowLength == 0){
		rtnText += "<tr class='rowPattern'>";
		rtnText += "	<td style='text-align:center; color:red;'>"+rowName+"</td>";
		rtnText += "	<td style='text-align:center; color:red;'><i class='fa fa-times'></i></td>";
		rtnText += "	<td style='text-align:center;'></td>";
		rtnText += "	<td style='text-align:left;'></td>";
		rtnText += "</tr>";
	}else{
		for(var i=0; i<rowLength; i++){
			rtnText += "<tr class='rowPattern'>";
			rtnText += "	<td style='text-align:center;'>"+rowName+"</td>";
			rtnText += "	<td style='text-align:center;'><i class='fa fa-check'></i></td>";
			rtnText += "	<td style='text-align:center;'>"+(rowData[i].indexOf("|,|") == -1?(rowName!="price" && rowName!="basePrice" ?(rowName!="description" ?"JSoupSelectorTxt":"JSoupSelectorLongTxt"):"JSoupSelectorNum"):"JSoupSelectorAttr")+"</td>";
			rtnText += "	<td style='text-align:left;'>"+rowData[i]+"</td>";
			rtnText += "</tr>";
		}
	}
	
	return rtnText;
}

function tool2GetResultData(){
	
	var jsonObj = {};
	var fieldObj = [];
	var table = document.getElementById("tool2ResultTable");
	var startField = "";
	for (var r = 1, n = table.rows.length; r < n-1; r++) {
		var fieldName = table.rows[r].cells[0].textContent;
		if(jsonObj[fieldName] != null){
			fieldObj = jsonObj[fieldName];
		}else{
			fieldObj = [];
		}
		
		if(!table.rows[r].cells[2].textContent && !table.rows[r].cells[3].textContent){
			continue;
		}
		
		var item = {};        	    	
		item ["filter"] = table.rows[r].cells[2].textContent;
		item ["value"] 	= table.rows[r].cells[3].textContent;
		
		fieldObj.push(item);
		jsonObj[fieldName] = fieldObj;
	}
	jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	
	return jsonString;
}

function tool2SaveResult(){
	
	var mId = $('#merchantId').val();
	var jsonString = tool2GetResultData();
	var param = "merchantId=" + mId + "&data=" + jsonString;
	
	$.ajax({
		type: "POST",
		url: 'savefield',
		data: param,
		cache: false,
		async: true,
//		beforeSend: function (xhr) {
//			xhr.setRequestHeader("Content-Length", jsonString.length);
//		},
		success: function(json) {
			if(json.header == 'success'){
				var noValue = true;
				$('.testUrl').each(function(){
					if($(this).val() != null && $(this).val() != ""){
						noValue = false;
					}
				});
				if(noValue){
					$('.deleteUrlButton').each(function(){
						deleteUrlRow($(this));
					});
					$('input[name=tool2Url]').each(function(){
						if($(this).val() != null && $(this).val() != ""){
							var target = $('.addUrlButton').last();
							addUrlRow($(target), $(this).val());
						}
					});
					$('.addUrlButton').first().parents('.row').remove();
					setUrlNumber();
				}
				
				search(false);
				openTab("Parser");
				$('#modalRun').modal('hide');
				
			}
		},
		error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});
	
	return jsonString;
}
