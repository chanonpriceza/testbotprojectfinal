function getNotification(){
	var country 	= $('#country').val();
	var notiType 	= $('#notiType').val();
	var page 		= $('#page').val();
	var context		= $('#context').val();
	$.ajax({
		type: "POST",
		url: 'service/getNotificationAjax',
		data: 'country='+country+"&"+"notiType="+notiType+"&page="+page,
		cache: false,
		async: true,
		success: function(data) {
			var result = "";
			var notiBeans = data.notiBeans;
			var paging = data.pagingBean;
			var notitype = data.notiType;
			if(notiBeans!=null){
				notiBeans.forEach(function(noti,index, array) {
					result = result+generateTable(noti,notitype,context);
				});
			}
			if(paging!=null){
				genPaging(data);
			}
			$('#tableBody').html(result);
		},
		error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});	
}

function setNotitype(type){
	$('#notiType').val(type);
	getNotification();
}

function setCountry(country){
	$('#country').val(country);
	getNotification();
}

function generateTable(noti,notitype,context){
	var result = "<tr class='rowPattern'>"+
	"<td width='5%'  style='text-align:center;'>"+
	"<div class=\"row\">"+
	"<img src=\""+context+"/assets/global/img/manwhite.png\" class=\"avatar\" alt=\"...\" style=\"width: 30px; height: 30px\">"+
	"</div>"+
	"<div class=\"row\" style=\"font-size: x-small;\">"+
		noti.source+
	"<div>"+
	"</td>"+
	"<td width='20%'  style='text-align: left: ;'>"+noti.message+"</td>"+													
	"<td width='20%'  style='text-align: left: ;'>"+notitype[noti.type].replace("_", " ")+"</td>"+
	"<td width='10%'  style='text-align:center;'>"+printDate(new Date(noti.createDate));+"</td></tr>";	
	return result;
}

function printDate(cDate,notPrintHour){
	var hourString = "";
	if(notPrintHour==null){
		var min = cDate.getMinutes();
		var second = cDate.getSeconds();
		if(min < 10){
			min = "0"+min;
		}
		
		if(second < 10){
			second = "0"+second;
		}
		hourString = " "+cDate.getHours()+":"+min+":"+second+"";
	}
	
	if(!cDate){
		return "-";
	}
	var month = cDate.getMonth()+1;
	if(month < 10){
		month = "0"+month;
	}
	const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	return cDate.getFullYear()+"-"+month+"-"+cDate.getDate()+"-"+hourString;
}

$(document).on("click",'button[name="notiTypeSelector"]',function() {
	$("button[name=\"notiTypeSelector\"].active").removeClass("active");
	$(this).addClass("active");
});

function genPaging(data){
	var pagingBean = data.pagingBean;
	if(!pagingBean){
		$("#pagingDiv").html('');
		return false;
	}
	var pagingList = pagingBean.pagingList;
	if(!pagingList){
		$("#pagingDiv").html('');
		return false;
	}

	var pagingGen = "";
	pagingGen +="<div class=\"dataTables_paginate paging_bootstrap\" style=\"color:red\">";
	pagingGen +="<ul class=\"pagination\">";
	if(pagingBean.currentPage == 1){
		pagingGen +="<li class=\"prev disabled\"><a href=\"#\" class=\"pagiation\">← Previous</a></li>";
	}else{
		pagingGen +="<li class=\"prev\"><a href=\"#\" data-element-id=\""+(pagingBean.currentPage-1)+"\" name=\"pagination\">← Previous</a></li>";
	}
	pagingList.forEach(function(pageNo, index, array) {
		if(pageNo==pagingBean.currentPage){
			pagingGen +="<li><a href=\"#\" style=\"background-color: #ccc;\" class=\"pagination\">"+pageNo+"</a></li>";
		}else if(pageNo=='...'){
			pagingGen +="<li><a href=\"#\" class=\"pagiation\">...</a></li>";
		}else{
			pagingGen +="<li><a href=\"#\" data-element-id=\""+pageNo+"\" name=\"pagination\">"+pageNo+"</a></li>";
		}
	});
	if(pagingBean.currentPage == pagingBean.totalPage){
		pagingGen +="<li class=\"next disabled\"><a href=\"#\">Next → </a></li>";
	}else{
		pagingGen +="<li class=\"next\"><a href=\"#\" name=\"pagination\" data-element-id=\""+(pagingBean.currentPage+1)+"\">Next → </a></li>";
	}
	pagingGen +="</ui>";
	pagingGen +="</div>";  	
	$("#currentPage").val(pagingBean.currentPage);
	$("#pagingDiv").html(pagingGen);

}

$(document).on("click",'a[name="pagination"]',function() {
	var pageId = $(this).attr("data-element-id");
	$('#page').val(pageId);
	getNotification();
});

$(document).on("click",'button[name="countrySelector"]',function() {
	$("button[name=\"countrySelector\"].active").removeClass("active");
	$(this).addClass("active");
});


