$(document).ready(function () {
	$('#inputType').on('change', function() {
		$('input[name=cmd]').val("searchLnwShopMerchant");
		$('#activeType').val($('#inputType').val());
		$('#searchLnwShopMerchant').submit();
	});
	
	$('a[name="btnEditMerchant"]').click(function() {
		formEditMerchant($(this));	
	});
	
	$('#editLnwShopMerchantModal').on('hide.bs.modal', function() {
		clearForm();
		$('#addFormLabel').text('Edit LnwShop Merchant Manager');
	});
	
	$('#submitbtn').click(function(){
	    $("form#editLnwShopMerchantForm :input").each(function() {
	    	validateInput(this)
	    });
	});
	
	$('a[name="idRefLnw"]').click(function() {
		var data = $(this).attr("data-element-id");
		var lnwId = data.split(",")[0];
		var lnwName = data.split(",")[1];
		$('#cLnwID').val(lnwId);
		$('#txtNameMerchant').text(lnwName);
		getDataBylnwId();
		$("#showProductData").show();
		$("#lnwshopManagerTable").hide();
	});
	
	$("#buttonback").click(function() {
		$("#showProductData").hide();
		$("#lnwshopManagerTable").show();
		$("#cLnwID").val('');
		$("#startData").val('');
		$('#dataTable > tbody').html('');
	});
		
 });

function getDataBylnwId(){

	var lnwId = $('#cLnwID').val();
	var start = $('#startData').val(); 
	if(start == ''){
		start = 0;
	}
	$('#getDataBylnwId').button('loading');
	var param = "lnwID="+ lnwId+"&start="+start;
	$.ajax({
		type: "POST",
		url: 'checkdatalnwshopajax',
		data: param,
		cache: false,
		async: true,
		success: function(json) {
			if(json.header == 'success') {
		  		var listData = json.listData;
		  			assignDataTable(json, listData,start);												
					var nextStart = json.start;
					$('#startData').val(nextStart);
					
					if(listData.length >= 100){
						$("#btnLoadmoreData").css("display", "block");						
					}else{
						$("#btnLoadmoreData").css("display", "none");
					}
			}else{
				$("#btnLoadmoreData").css("display", "none");
				if(start == 0){
					modalAlert("Do not have this data in the system", false);					
				}
			}
			$('#getDataBylnwId').button('reset');
		},
		error: function (textStatus, errorThrown) {
			$("#dataContent").css("display", "none");
			modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
		}
	});
}

function clearForm(){
	$('#eActiveType').prop('disabled', false);
	$('#eAddParentName').prop('disabled', false);
	$('#eAddProductId').prop('disabled', false);
	$('#eAddContactPrice').prop('disabled', false);
	$('#eForceContactPrice').prop('disabled', false);
	$('#ePriceMultiply').val('');
	$('#ePriceMultiply').prop('readonly', false);
	$('#eMerchantID').val('');
	$('#eMerchantID').prop('readonly', false);
	$('.errorMsg').remove();
}

function formEditMerchant(value){
	
	$('#eMerchantID').prop('readonly', false);
	var data = value.attr("data-element-id");
	var mMerchantId = data.split(",")[0];
	var lnw_merchant_id = data.split(",")[1];
	var lnwName = data.split(",")[2];
	var param = "lnw_merchant_id="+ lnw_merchant_id;

	$('#addFormLabel').text("Edit LnwShop Merchant : "+lnwName);
	$.ajax({
		type: "POST",
		url: 'getdatabylnwid',
		data: param,
		cache: false,
		async: true,
		success: function(redata) {
			if(redata.header == "success"){
				var merchantID = redata.merchantId;
				var lnw_id = redata.lnw_id;
				var active = redata.active;
				var addParentName = redata.addParentName;
				var addProductId = redata.addProductId;
				var addContactPrice = redata.addContactPrice;
				var forceContactPrice = redata.forceContactPrice;
				var priceMultiply = redata.priceMultiply;
				$('#eMerchantID').val(merchantID);
				$('#eMerchantidNew').val(merchantID);
				$('#eActiveType').val(active);
				$('#eAddParentName').val(addParentName);
				$('#eAddProductId').val(addProductId);
				$('#eAddContactPrice').val(addContactPrice);
				$('#eForceContactPrice').val(forceContactPrice);
				$('#ePriceMultiply').val(priceMultiply);
				$('#eLnwID').val(lnw_merchant_id);
				
				if(active == 1){
					$('#eAddParentName').attr('disabled',true);
					$('#eAddProductId').attr('disabled',true);
				}
				
				if(merchantID != 0){
					$('#eMerchantID').prop('readonly', true);
				}

				if(priceMultiply != 1.0){
					$('#ePriceMultiply').attr('readonly',true);
				}
			
			}

			$('#editLnwShopMerchantModal').modal('show');
		}
	});	
}


function getCountProduct(lnw_merchant_id, lnw_name){
	$('#btnCountProduct'+lnw_name).button('loading');
	if(lnw_merchant_id != null && lnw_merchant_id.length > 0){
		var param = "lnw_merchant_id="+ lnw_merchant_id;
		
		$.ajax({
			type: "POST",
			url: 'countlnwshopproduct',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
					setTimeout(function(){
					var countproduct = json.count;
					$('#COUNT_PRODUCTONBOT'+lnw_name).text(countproduct);	
					$('#btnCountProduct'+lnw_name).button('reset');
					$('#btnCountProduct'+lnw_name).remove();
				 	}, 2500);
				}else{
					$('#COUNT_PRODUCTONBOT'+lnw_name).text(0);																			
					$('#btnCountProduct'+lnw_name).remove();
				}
			},
			error: function (errorThrown) {
			}
		});	
	}
}

function isChangeBtn(){
	if($('.errorMsg').size() > 0){
		$('#submitbtn').prop('disabled', true);
	}else{
		$('#submitbtn').attr('type', 'submit');
		$('#submitbtn').prop('disabled', false);
		
		$('#eActiveType').prop('disabled', false);
		$('#eAddParentName').prop('disabled', false);
		$('#eAddProductId').prop('disabled', false);
		$('#eAddContactPrice').prop('disabled', false);
		$('#eForceContactPrice').prop('disabled', false);
	}
}

function validateNull(value){
	if(value == ''){
		return true;
	}else{
		return false;
	}
}

function validateInteger(value){
	var regex = new RegExp('^[0-9]+$');
    if (regex.test(value)) {
    	return true;
    }else{
    	return false;
    }
}

function validateInput(btn){
	var btnVal =  $(btn).val();
	var tagName    = '.'+$(btn).attr('name');
	var errormsg = '';
	$(tagName).remove();
	
     if ($(btn).attr('name') == 'eMerchantID') {
    	 if("eMerchantID".indexOf($(btn).attr('name')) != -1){
    			if(!$.isNumeric(btnVal)){
    	   		 errormsg = errormsg +"MerchantID Type must be number"
    	   	 	}
    	 }
    	 
     }else if($(btn).attr('name') == 'ePriceMultiply'){
    	 if("ePriceMultiply".indexOf($(btn).attr('name')) != -1){
 			if(!$.isNumeric(btnVal)){
 	   		 errormsg = errormsg +"PriceMultiply Type must be number"
 	   	 	}
 	 }
     }
     
	if(errormsg == ''){
		$(tagName).remove();
	}else{
		 if($(tagName).size() == 0){
			 $(btn).parent().append('<span class="errorMsg '+$(btn).attr('name')+'" style="color:red;" >'+errormsg+'</span>');					 
		 }	
	}
	isChangeBtn();
	
}

function assignDataTable(json, listData,start){
	var merchantId = json.merchantId;
	if(listData != null && listData.length > 0){
		for(var i=0; i<listData.length; i++){
			var jObject = listData[i];
			var parent_name = (jObject.parent_product_name ? jObject.parent_product_name : '-');
			var name = (jObject.name ? jObject.name : '-');
			var price = (jObject.price ? jObject.price : '-');
			var description = (jObject.description ? jObject.description : '-');
			var picture_url = (jObject.picture_url ? jObject.picture_url : '-');
			var product_url = (jObject.product_url ? jObject.product_url : '-');
			var status = (jObject.status ? jObject.status : '-');
			var updateDate = (jObject.updateDate ? jObject.updateDate : '0');
			
			var rowData = generateRowData(parent_name, name, price, description, picture_url, product_url, status, updateDate)
			if(start == 0){
				$('#dataTable > tbody').append(rowData);					
			}else{
				$('#dataTable > tbody > tr:last').after(rowData);										
			}
		}
	}else {
		var rowData = generateRowData('-', '-', '-', '-', '-', '-', '-');
		$('#dataTable > tbody').append(rowData);
	}
}

function generateRowData(rowParent_name, rowName, rowPrice, rowDescription, rowPicture_url, rowProduct_url, rowStatus, rowUpdateDate){
	var rowData = "";
	rowData += "<tr class='rowData'>";
	rowData += "	<td width='10%' style='text-align:center; '>"+rowParent_name+"</td>";
	rowData += "	<td width='8%' style='text-align:center; '>"+rowName+"</td>";
	rowData += "	<td width='8%' style='text-align:center; '>"+rowPrice+"</td>";
	rowData += "	<td width='15%' style='text-align:center; '>"+rowDescription+"</td>";
	rowData += "	<td width='15%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank' href='"+rowPicture_url+"' class='rowPicture_url'>"+rowPicture_url+"</a></td>";
	rowData += "	<td width='15%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank' href='"+rowProduct_url+"' class='rowProduct_url'>"+rowProduct_url+"</a></td>";
	rowData += "	<td width='15%' style='text-align:center; '>"+rowStatus+"</td>";
	
	var dateStr = EpochToDate(rowUpdateDate).toString();
	dateStr = dateStr.substring(0, dateStr.indexOf("GMT"));
	rowData += "	<td width='5%' style='text-align:left; '>"+dateStr+"</td>";
	rowData += "</tr>";
	
	return rowData;
}	

function modalAlert(result, success){
	if(success){
		$('#modalAlertTitle').html('<h3 style="color:#64FE2E;"><i class="fa fa-check"></i>&nbsp;Success</h3>');
	}else{
		$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
	}
	$('#modalAlertData').html('<span style="color:black;font-size:17px;">'+result+'</span>');
	$('#modalAlert').modal('show');
}

function EpochToDate(epoch) {
    if (epoch < 10000000000)
        epoch *= 1000; // convert to milliseconds (Epoch is usually expressed in seconds, but Javascript uses Milliseconds)
    var epoch = epoch + (new Date().getTimezoneOffset() * -1); //for timeZone        
    return new Date(epoch);
}