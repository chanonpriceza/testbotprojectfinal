$(document).ready(function () {

	$('#dataForm').bootstrapValidator({
		excluded: ':disabled',
		fields: {
        	url: {
        		 validators: {
  	    			notEmpty: {
  		                message: 'Url is required.'
  		            },
  	            }
             }
        }
    }).on('success.form.bv', function(e){
    	$('#load').button('loading');
    	e.preventDefault();
    	runTest();
    });

	$("input").on("change keyup paste", function(){
		var inputclear = $(this).parent().find('span[class="inputclear glyphicon glyphicon-remove-circle"]'); 
		if($(this).val()!=''){
			inputclear.show();
		}else{
			inputclear.hide();
		}
	})
 });
var success = {
		"passSSL":"Pass SSL",
		"available":"This website avaliable",
		"nextpage":"Can continue next page",
		"productlink":"Can match product url",
		"producthtml":"Product page available",
		"nameMatch":"Can parse product name",
		"priceMatch":"Can parse price"		
};

var warning = {
		"nextpage":"No data for next page",
		"productlink" : "No data for product url",
		"producthtml":"Product page unavailable",
		"nameMatch":"No data for product name",
		"priceMatch":"No data for price"		
};

var fail = {
		"passSSL":"Cannot Pass SSL",
		"available":"This website unavaliable",
		"nextpage":"Cannot continue next page",
		"productlink":"Cannot match product url",
		"producthtml":"Product page unavailable",
		"nameMatch":"Cannot parse product name",
		"priceMatch":"Cannot parse price"		
};

function runTest(){
	var url = $('#url').val();
	var nextPage = $('#nextPageUrl').val();
	var pdUrl = $('#productUrl').val();
	var pdName = $('#productName').val();
	var pdPrice = $('#productPrice').val();
	var addTaskButton = false;
	var param = "url="+encodeURIComponent(url)+"&nextPageUrl="+encodeURIComponent(nextPage)+"&productUrl="+encodeURIComponent(pdUrl)+"&productName="+encodeURIComponent(pdName)+"&productPrice="+encodeURIComponent(pdPrice);

	var header = "Sorry!"
  	var icon = "&#xf00d;";
	var status = "danger";
	var message = "";
		$.ajax({
			type: "POST",
			url: 'runcheck',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success') {
			  		status = json.status;
			  		var set = ["passSSL",
			  			"available",
			  			"nextpage",
			  			"productlink",
			  			"producthtml",
			  			"nameMatch",
			  			"priceMatch"]
			  		var results = json.result;
			  		 for (var i in set){
			  			var key = set[i];
			  	        var value = results[key];
			  	        if(value == undefined){
			  	        	continue;
			  	        }
			  	        if(value == "true"){
			  				message += "<div class ='modal-detail' style='color:#82ce34'><i class='fa fa-check fa-fw'></i>"+success[key]+"</div>";
			  			}else if( value == "false"){
			  				message += "<div class ='modal-detail' style='color:#ef513a'><i class='fa fa-times fa-fw'></i>"+fail[key]+"</div>";
			  				addTaskButton = true;
			  			}else{
			  				message += (warning[key]!= undefined)? "<div class ='modal-detail' style='color:#FCCB46'><i class='fa fa-info fa-fw'></i>"+warning[key]+"</div>" : "";
			  				addTaskButton = true;
			  			}
			  	    }
			  		if(status == "success"){
			  			header = "Awesome!"
					  	icon = "&#xf00c;";
			  		}else if(status == "danger"){
			  			header = "Sorry!";
			  			icon = "&#xf00d;";
			  		}else{
			  			header = "Warning!";
			  			icon = "&#xf129;";
			  		}
			
			  		
			  	}else if(json.header == 'error' && json.header != undefined ){
			  		message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>"+json.result+"</p></div>";
			  	}else{
			  		message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>Something went wrong, Please try again</p></div>";
			  	}
		  	},
		  	error: function(xhr, textStatus, errorThrown){
		  		message = "<div class ='modal-detail' style='color:#ef513a'><p class='text-center'>Something went wrong, Please try again</p></div>";
		     },
		  	complete: function() {
		  		generateModalResponse(icon, header, message, status,addTaskButton)
			  	$('#responseModal').modal('show');
		  		$('#load').button('reset');
		    },
		});
		$("#dataForm").data('bootstrapValidator').resetForm();
}

function generateModalResponse(icon, header, message, status,addTaskButton){
	var rowData = "";
		rowData += "<div class='modal-header'>";
		rowData += "	<div class='icon-box "+status+"'>";
		rowData += "		<i class='fa material-icons'>"+icon+"</i>";
		rowData += "	</div>";
		rowData += "	<h4 class='modal-title'>"+header+"</h4>";
		rowData += "</div>";
		rowData += message;
		rowData += "<div class='modal-footer'>";
		rowData += "<button class='btn btn-"+status+" btn-block' data-dismiss='modal'>OK</button>";
		if(addTaskButton){
			rowData += "<button id='addTask' class='btn btn-dark btn-block' data-dismiss='modal' data-status='"+status+"' onclick='addTaskForCheck(this);'>Add Task FOR CHECK</button>";  
  		}
		rowData += "</div>";
		
	$('.modal-content').html(rowData);
}

function addTaskForCheck(value){

	var status = $(value).data("status");
	var context = $("#contextPath").val();
	var data = "";
	 data += "-- Detail --";
	 $("form#dataForm input[type=text]").each(function() {
		 var input = $(this);
		 if(input.val() != "")
		    data += "\n"+input.attr('name') + ' : ' + input.val();	    	
	 });
	 
	 data += "\n -- Status :"+status+" --";		 
	 var set = ["passSSL",
			"available",
			"nextpage",
			"productlink",
			"producthtml",
			"nameMatch",
			"priceMatch"]
		 for (var i in set){
			  var key = set[i];
			  if(status == "danger"){
					data += "\n" + fail[key];
				}else if(status == "warning"){
					if(warning[key] != undefined)
					data += "\n" +key +":"+ warning[key];					
				}
		 	}

		$.ajax({
		    url:'addTaskForCheck',
		    type:'POST',
		    method: 'POST',
		    data:'&data='+data,
		    success:function(data){
		    	if(data.success){
		    		var taskID = data.taskID;
		    		var win = window.open(context+'/admin/taskmanager?id='+taskID, '_self');
			    	if (win) {
			    	    win.focus();
			    	} else {
			    	    //Browser has blocked it
			    	    alert('Please allow popups for this website');
			    	}
		    	}
		    }
		 });

}


$('#dataFormReset').click(function() {
	$(".inputclear").hide();
	$("#dataForm").bootstrapValidator('resetForm', true);
	$("#productName").val('');
	$("#productPrice").val('');
	$("#nextPageUrl").val('');
	$("#productUrl").val('');
});

$(".inputclear").click(function() {
	var input = $(this).parent().find("input");
	input.val('');
	if(input.prop('name') == "url"){
		$('#dataForm').bootstrapValidator('revalidateField', input.prop('name'));
	}
	$(this).hide();
});

