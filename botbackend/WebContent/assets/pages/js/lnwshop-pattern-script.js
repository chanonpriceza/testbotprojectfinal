window.onload = function(e){ 		
	var parameter_index = window.location.href.indexOf('=');
	if(parameter_index != -1){
		var merchantId = window.location.href.slice(parameter_index + 1);
		$('#merchantId').val(merchantId);
		getDataByMerchantId();
	}
}

$(document).ready(function() {
	
	$('#merchantId').keypress(function (e) {
		  if (e.which == 13) {
			  getDataByMerchantId();
		  }
	});
	    
	$.fn.editable.defaults.mode = 'popup';
	
	$("#importPatternForm").submit(function (event) {
		importPattern(event);
    });
	
	$('.file-upload-wrap').bind('dragover', function () {
		$('.file-upload-wrap').addClass('image-dropping');
	});
	
	$('.file-upload-wrap').bind('dragleave', function () {
		$('.file-upload-wrap').removeClass('image-dropping');
	});
	
	$("#inputPatternFile").on('change',function(){
	    readURL(this);
	});
	
	$("#btnGenerateContinue").on('click',function(){
		generateUrl(this,"CONTINUE");
	});
	
	$("#btnGenerateMatch").on('click',function(){
		generateUrl(this,"MATCH");
	});
	
	$('#showBtnGenerate').css("display", "none");
});

var actions = {
		'LNWSHOP' : 1,
		'ALLOW' : 2,
		'BLOCK' : 3
}

function removeUpload() {
  $('.file-upload-input').val('').clone(true);
  $('.file-upload-content').hide();
  $('.file-upload-wrap').show();
  $('.file-upload-wrap').removeClass('image-dropping');
}

function assignEditable(){
	
    $('.rowValue').editable({
    	type: 'text',
        title: 'Enter Value',
        emptyclass: 'text-danger',
    	validate: function(value) {
     		if($.trim(value) == '') return 'This field is required';
    	}
    });
    
    $('.rowAction').editable({
    	type: 'select',
        title: 'Select action',
        emptyclass: 'text-danger',
        value: 1,
        source: [
        	{value: 1, text: 'LNWSHOP'},
            {value: 2, text: 'ALLOW'},
            {value: 3, text: 'BLOCK'},
        ],
        validate: function(value) {
     		if($.trim(value) == '') return 'This field is required';
    	}
    });
    
    $('.rowCatId').editable({
    	type: 'text',
        title: 'Enter CatId',
        emptyclass: 'text-danger',
    	validate: function(value) {
     		if($.trim(value) == '') return 'This field is required';
    	}
    });
    
    $('.rowKeyword').editable({
    	type: 'text',
        title: 'Enter Keyword',
        emptyclass: 'text-danger'
    });
    
}

function getDataByMerchantId(){
	
	var merchantId = $('#merchantId').val().trim();
	var param = "cmd=getDataByMerchantId&merchantId="+ merchantId;
	$('#lnwshopPatternTable > tbody').html('');
	$('#tableMerchantId').val(merchantId);
	$("#saveResult").html("");
	removeUpload();
	$.ajax({
		type: "POST",
		url: 'lnwshoppatternAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(json) {
		  	if(json.header == 'success') {
		  		if(json.mode == 'update') {
					$("#lnwshopPatternContent").css("display", "block");
					$("#testUrlContent").css("display", "block");
			  		var data = json.result;
			  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
 					for(var i=0; i<data.length; i++){
						var jObject = data[i];
						
 						var condition = (jObject.condition ? jObject.condition : '');
 						var group = (jObject.group ? jObject.group : '0');
 						var value = (jObject.value ? jObject.value : '');
 						var action = (jObject.action ? jObject.action : 'LNWSHOP');
 						var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
 						var keyword = (jObject.keyword ? jObject.keyword : '');
 						
 						var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
 						$('#lnwshopPatternTable > tbody').append(rowData);
 					}
		  		}else if(json.mode == 'insert') {
		  			$("#lnwshopPatternContent").css("display", "block");
					$("#testUrlContent").css("display", "block");
					
					var rowData = generateRowPattern('', '0', '', 'LNWSHOP', '0', '');
					$('#lnwshopPatternTable > tbody').append(rowData);
			  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
					
		  		}
		  		$('#hdMerchantId').val(merchantId);
				assignEditable();
		  	}else{
			  	$("#lnwshopPatternContent").css("display", "none");
			  	$("#testUrlContent").css("display", "none");
		  		$("#targetMerchantIdTxt").html("");
		  		$('#hdMerchantId').val("");
			  	alert("Fail to get merchant pattern data.");
		  	}
	  	},
	  	error: function (textStatus, errorThrown) {
		  	$("#lnwshopPatternContent").css("display", "none");
		  	$("#testUrlContent").css("display", "none");
		  	$('#hdMerchantId').val("");
		  	alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
	
}

function generateRowPattern(rowCon, rowGro, rowVal, rowAct, rowCat, rowKey){
	
	var rowData = "";
	rowData += "<tr class='rowPattern'>";
	rowData += "	<td width='7%' data-col-name=\"Condition\" style='text-align:center;'><a href='#' class='rowCondition'>"+rowCon+"</a></td>";
	rowData += "	<td width='5%'  data-col-name=\"Group\" style='text-align:center;'><a href='#' class='rowGroup'>"+rowGro+"</a></td>";
	rowData += "	<td width='33%' data-col-name=\"Value\" style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowValue'>"+rowVal+"</a></td>";
	rowData += "	<td width='8%'  data-col-name=\"Action\" style='text-align:center;'><a href='#' class='rowAction':>"+rowAct+"</a></td>";
	rowData += "	<td width='5%'  data-col-name=\"CatTd\" style='text-align:center;'><a href='#' class='rowCatId'>"+rowCat+"</a></td>";
	rowData += "	<td width='26%' data-col-name=\"Keyword\" style='text-align:left; max-width: 200px; word-wrap: break-word;'><a href='#' class='rowKeyword'>"+rowKey+"</a></td>";
	rowData += "	<td width='4%' style='text-align:center;'><input type='checkbox' class='rowCheck' onclick='clickTest(this)'></input></td>";
	rowData += "	<td width='12%' style='text-align:center;'>";
	rowData += 			"<button type='button' class='btn btn-md btn-primary' onclick='insertPattern(this)'>Insert</button>";
	rowData += 			"<button type='button' class='btn btn-md btn-danger' onclick='deletePattern(this)'>Del<i class='fa fa-trash' aria-hidden='true'></i></button></button>";
	rowData += "	</td>";
	rowData += "</tr>";
	
	return rowData;
}

function getAllData(){
	var jsonObj = [];
    var jsonString;
    var table = document.getElementById("lnwshopPatternTable");
    for (var r = 1, n = table.rows.length; r < n-1; r++) {

        var item = {};        	    	
    	
    	item ["condition"] 	= ( table.rows[r].cells[0].textContent && table.rows[r].cells[0].textContent != 'Empty' ? table.rows[r].cells[0].textContent : "");
    	item ["group"] 		= ( table.rows[r].cells[1].textContent && table.rows[r].cells[1].textContent != 'Empty' ? table.rows[r].cells[1].textContent : "0");
    	item ["value"] 		= ( table.rows[r].cells[2].textContent && table.rows[r].cells[2].textContent != 'Empty' ? table.rows[r].cells[2].textContent : "");
    	item ["action"] 	= ( table.rows[r].cells[3].textContent && table.rows[r].cells[3].textContent != 'Empty' ? table.rows[r].cells[3].textContent : "LNWSHOP");
    	item ["categoryId"] = ( table.rows[r].cells[4].textContent && table.rows[r].cells[4].textContent != 'Empty' ? table.rows[r].cells[4].textContent : "0");
    	item ["keyword"] 	= ( table.rows[r].cells[5].textContent && table.rows[r].cells[5].textContent != 'Empty' ? table.rows[r].cells[5].textContent : "");

        jsonObj.push(item);
    }
    jsonString = JSON.stringify(jsonObj);
	jsonString = encodeURIComponent(jsonString);
	
	return jsonString;
}

function importPattern(event){
	
	event.preventDefault();
	$('#submitImportPattern').button('loading');
	
    var data = new FormData();
    var merchantId = $('#tableMerchantId').val();
    jQuery.each(jQuery('#inputPatternFile')[0].files, function(i, file) {
        data.append('inputPatternFile', file);
    });
    
    $.ajax({
        url: 'lnwshoppatternAjax',
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function (json) {
        	if(json.header == 'success') {
	  			var data = json.result;
	  			$('#lnwshopPatternTable > tbody').html('');
				$("#lnwshopPatternContent").css("display", "block");
				$("#testUrlContent").css("display", "block");
		  		$("#targetMerchantIdTxt").html("<span class='text-danger'>(Merchant Id : <span id='merchantId'>"+merchantId+")</span></span>");
				for(var i=0; i<data.length; i++){
					var jObject = data[i];
					
					var condition = (jObject.condition ? jObject.condition : '');
					var group = (jObject.group ? jObject.group : '0');
					var value = (jObject.value ? jObject.value : '');
					var action = (jObject.action ? jObject.action : 'LNWSHOP');
					var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
					var keyword = (jObject.keyword ? jObject.keyword : '');
					
					var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
					$('#lnwshopPatternTable > tbody').append(rowData);
				}
				assignEditable();
				modalAlert('Import pattern successful, do not forget to save.');
		  	}else{
		  		modalAlert(json.errorMsg);
		  	}
        	$('#submitImportPattern').button('reset');
        },
        error: function (textStatus, errorThrown) {
		  	alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	$('#submitImportPattern').button('reset');
	  	}
    });

    return false;
}

function exportPattern(){
	var merchantId = $('#tableMerchantId').val();
    var jsonString = getAllData();
    var param = "cmd=exportPattern&merchantId="+ merchantId+"&jsonData="+jsonString;
    var filename = "Pattern-"+merchantId+" @ "+generateDate()+".csv";
    var d = new Date();
    
    $.ajax({
		type: "POST",
		url: 'lnwshoppatternAjax',
		data: param,
		cache: false,
		async: true,
		headers: {'Accept': 'application/csv'},
	  	success: function(data) {
	  		downloadToFile(data, filename, 'application/csv')
	  	},
	  	error: function (textStatus, errorThrown) {
			alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
	
}

function saveTable(dataTable) {
	
	$("#saveResult").html("");
	
	var merchantId = $('#tableMerchantId').val();
    var jsonString = getAllData();
    var param = "cmd=saveTable&merchantId="+merchantId+"&jsonData="+jsonString;
    
    $('#saveTableBtn').button('loading');
    var hdmerchantId = $('#hdMerchantId').val();
	if(!hdmerchantId){
		modalAlert("Merchant Data Not Found.");
		setButton(false);
		$('#saveTableBtn').button('reset');
		return;
	}
    $.ajax({
		type: "POST",
		url: 'lnwshoppatternAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(json) {
		  	if(json.header == 'success-empty'){
		  		$("#saveResult").html("<br><p class='text-success'>Save complete. Row with 'Empty' value were not save.</p>");
		  		modalAlert('Save complete. Row with \'Empty\' value were not save.');
		  	}else if(json.header == 'success') {
		  		$("#saveResult").html("<br><p class='text-success'>Save complete</p>");
		  		modalAlert('Save complete');
		  	}else if(json.header == 'fail-not-allow'){
		  		if(json.detail != null){
		  			var detail = "<table class=\"table table-bordered\"><thead><tr class=\"heading\"><th style=\"text-align:center;\">Value</th><th style=\"text-align:center;\">CategoryId</th></tr></thead><tbody>";
		  			var detailArr = json.detail;
		  			for(var i=0; i<detailArr.length; i++){
		  				var detailObj = detailArr[i];
		  				detail += "<tr>";
		  				detail += "    <td style=\"text-align:center;\">" + detailObj.value + "</td>";
		  				detail += "    <td style=\"text-align:center;\">" + detailObj.categoryId + "</td>";
		  				detail += "</tr>";
		  			}
		  			detail += "</tbody></table>";
		  			modalAlert('<p class="text-danger"><b>Error, Save incomplete !!</b></p>Found Category was not allow <br>Please edit data and submit again.<br><br>' + detail);
		  		}
		  	}else{
		  		$("#saveResult").html("<br><p class='text-danger'><b>Error, Save incomplete !!</b></p>");
		  		modalAlert('<b>Error, Save incomplete !!</b>');
		  	}
		  	window.setTimeout(function(){
		  		$("#saveResult").html("");
		 	}, 10000);
		  	$('#saveTableBtn').button('reset');
	  	},
	  	error: function (textStatus, errorThrown) {
	  		$("#saveResult").html("<br><p class='text-danger'>Error, Save incomplete !!</p>");
			alert(	'Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
			$('#saveTableBtn').button('reset');
	  	}
	});
}

function addPattern(){
	var rowData = generateRowPattern('', '0', '', 'LNWSHOP', '0', '');
	$('#lnwshopPatternTable > tbody').append(rowData);
	assignEditable();
}

function insertPattern(btn){
	var rowData = generateRowPattern('', '0', '', 'LNWSHOP', '0', '');
	$(btn).parent().parent().before(rowData)
	assignEditable();
}

function deletePattern(btn){
	$(btn).parent().parent().remove();
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
	reader.onload = function(e) {
	    $('.file-upload-wrap').hide();
	    $('.file-upload-content').show();
		$('.image-title').html(input.files[0].name);
	};
	reader.readAsDataURL(input.files[0]);
	} else {
		removeUpload();
	}
}

function downloadToFile(content, filename, contentType){
	if(!contentType) contentType = 'application/csv';
	var a = document.createElement('a');
    var blob = new Blob([content], {'type':contentType});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.click();
}

function generateDate(){
	var currentdate = new Date(); 
	var datetime = "";										datetime += currentdate.getFullYear();
	if((currentdate.getMonth()+1)<10)	datetime += "0" ; 	datetime += (currentdate.getMonth()+1);
	if(currentdate.getDate()<10)		datetime += "0" ; 	datetime += (currentdate.getDate());
	datetime += " " ;  
	if(currentdate.getHours < 10)		datetime += "0" ; 	datetime += currentdate.getHours();  
	if(currentdate.getMinutes < 10)		datetime += "0" ; 	datetime += currentdate.getMinutes();  
	if(currentdate.getSeconds < 10)		datetime += "0" ; 	datetime += currentdate.getSeconds();  
	return datetime;
}

function modalAlert(result) {
	$('#modalAlertData').html('<center><span style="color:black;font-size:15px;">'+result+'</span></center>');
	$('#modalAlert').modal('show');
}


function setButton(type) {
	$('#search').prop("disabled", type);
	$('#btnTopSave').prop("disabled", type);
}



