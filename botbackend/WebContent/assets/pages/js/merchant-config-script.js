	var listCheckfieldBoolean = "enableProxy,enableCookies,disableParserCookies,ignoreHttpError,skipEncodeURL,wceClearProductUrl,forceUpdateImage,forceUpdateCategory,forceUpdateKeyword,forceDelete,enableUpdateBasePrice,enableUpdateDesc,enableUpdatePic,enableUpdateUrl,userOldProductUrl";
	var listCheckfieldHaveDropdownNotfixClass = "parserClass,urlCrawlerClass,feedCrawlerClass";
	var listCheckfieldHaveDropdownfixClass = "initCrawlerClass,imageParserClass";
	var listInputfieldCheckNumber = "maxDepth,productUpdateLimit,deleteLimit,hourRate,dueHourRate,wceHourRate,maxRuntime,wceParserThreadNum,dueParserThreadNum,wceSleepTime,dueSleepTime,updatePricePercent";
	var listParser = [
			"userConfig"
			,"web.parser.filter.TemplateLazadaJSIncludeIdHTMLParser"
			,"web.parser.filter.TemplateTaradIncludeIDHTMLParser"
			,"web.parser.filter.TemplateTaradIncludeIDNoPriceHTMLParser"
			,"web.parser.filter.TemplateWeLoveWithIDHTMLParser"
			,"web.parser.filter.TemplateWeLoveWithIDContactPriceHTMLParser"
			,"web.parser.filter.TemplateWeLovePlatinumWithIDHTMLParser"
			,"web.parser.filter.TemplateWeLovePlatinumWithIDContactPriceHTMLParser"
			,"web.parser.filter.KaideeMarketplaceHTMLParser"
			,"web.parser.filter.ReadyPlanetHTMLParser"
	];
	var listFeedCrawler = [
			"feed.crawler.TemplateShopeeItemApiShortLinkFeedCrawler",
			"feed.crawler.TemplateShopeeItemApiShortLinkCustomUTMFeedCrawler",
			"feed.crawler.TemplateJDCentralFeedCrawler",
	];
	
	$(document).ready(function() {
		startButton();
		
		if($('#merchantId').val() != ''){
			getDataByMerchantId();
		}
		
		$('#merchantId').keypress(function (e) {
			  if (e.which == 13) {
				  getDataByMerchantId();
			  }
		});
				
		$("#btnTopSave").on("click", function (e) {
			
			$("form#submitMerchantConfig > input:text").each(function() {
		    	validateInput(this);
		    });
			
			if(!$.isNumeric($('#merchantId').val())){
				$('#modalAlert').modal('show');
				$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
				$('#modalAlertData').html('<span style="color:black;font-size:15px;"> Not found merchant id</span>');
	       	 }else{
	       		
	       		$('#currencyRate').val(($('#currencyRate').val().trim() === '') ? '1.0' :$('#currencyRate').val());
				$('#maxDepth').val(($('#maxDepth').val().trim() === '') ? '5' :$('#maxDepth').val());
				$('#productUpdateLimit').val(($('#productUpdateLimit').val().trim() === '') ? '7' :$('#productUpdateLimit').val());
				$('#deleteLimit').val(($('#deleteLimit').val().trim() === '') ? '200' :$('#deleteLimit').val());
				$('#hourRate').val(($('#hourRate').val().trim() === '') ? '24' :$('#hourRate').val());
				$('#wceHourRate').val(($('#wceHourRate').val().trim() === '') ? '24' :$('#wceHourRate').val());
				$('#dueHourRate').val(($('#dueHourRate').val().trim() === '') ? '24' :$('#dueHourRate').val());
				$('#maxRuntime').val(($('#maxRuntime').val().trim() === '') ? '24' :$('#maxRuntime').val());
				$('#parserCharset').val(($('#parserCharset').val().trim() === '') ? 'UTF-8' :$('#parserCharset').val());

			    e.preventDefault();    
			    $('#submitMerchantConfig').submit();	
	       	 }	    	 
		    
		});
		
		$("input").blur(function(){
			validateInput(this);
		});
		
		$("input").on("change keyup paste", function(){
			var inputclear = $(this).parent().find('span[class="inputclear glyphicon glyphicon-remove-circle"]'); 
			if($(this).val()!=''){
				inputclear.show();
			}else{
				inputclear.hide();
			}
		});
		
		$('#parserClass').on('change', function() {
		    var valueSelected = this.value;
		    var valueCrawlerClass = '';
		    var checkFiieldIsBank = true;
		    
		    $('#urlCrawlerClass option').each(function() {
		        if ( $(this).val() != '' ) {
		            $(this).remove();
		        }
		    });
		    
		    if(valueSelected != null && valueSelected != ''){
		    	if(valueSelected == 'web.parser.filter.TemplateWeLoveWithIDHTMLParser' 
		    		|| valueSelected == 'web.parser.filter.TemplateWeLoveWithIDContactPriceHTMLParser'
		    			|| valueSelected == 'web.parser.filter.TemplateWeLovePlatinumWithIDHTMLParser'
		    				|| valueSelected == 'web.parser.filter.TemplateWeLovePlatinumWithIDContactPriceHTMLParser'){
		    		
		    		valueCrawlerClass = 'web.crawler.impl.TemplateWeLoveShoppingURLCrawler';
		    
			    }else{
			    	checkFiieldIsBank = false;
			    }
		    	
		    	if(checkFiieldIsBank){
		    		$('#urlCrawlerClass').append('<option value="'+valueCrawlerClass+'" class="dbvalue" selected="selected">'+valueCrawlerClass+'</option>');		    		
		    	}
		    }
		});
		
	});
	
	function startButton (){
		$('#topSaveConfig').hide();
		$('.iconSpin').parent().hide();
		
	}
	
	function clearAllInput(){
		
		$(".dbvalue").remove();
		$('input:checkbox').removeAttr('checked');
		
		$('#currencyRate').val("");
		$('#maxDepth').val("");
		$('#productUpdateLimit').val("");
		$('#deleteLimit').val("");
		$('#hourRate').val("");
		$('#wceHourRate').val("");
		$('#dueHourRate').val("");
		$('#maxRuntime').val("");
		
		$('#wceParserThreadNum').val("");
		$('#dueParserThreadNum').val("");
		$('#wceSleepTime').val("");
		$('#dueSleepTime').val("");
		
		$('#urlCrawlerCharset').val("");
		$('#parserCharset').val("");
		
		$('#feedCrawlerParam').val("");
		$('#merchantName').val("");
		$('#feedType').val("");
		$('#feedUsername').val("");
		$('#feedPassword').val("");
		$('#feedXmlMegaTag').val("");
		$('#fixCategoryId').val("");
		$('#fixKeyword').val("");
		
		$('#updatePricePercent').val("");
	}

	function getDataByMerchantId(){
		
		var merchantId = $('#merchantId').val().trim();
		var param = "cmd=getDataByMerchantId&merchantId="+ merchantId;
		clearAllInput();
		$.ajax({
			type: "POST",
			url: 'merchantconfigAjax',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success') {
		  			$("#urlConfigContent").css("display", "block");
					$("#testUrlContent").css("display", "block");
					var merchantId = json.merchantId;
					var merchantName = json.merchantName;
			  		var data = json.result;
			  		if(data != null && data.length > 0){
				  		var data = json.result;
				  		var rateField = false;
	 					for(var i=0; i<data.length; i++){
							var jObject = data[i];
	 						var field = (jObject.field ? jObject.field : '');
	 						var value = (jObject.value ? jObject.value : '');
	 						if(listCheckfieldBoolean.indexOf(field) != -1){
	 							$('#'+field).prop('checked', (value == 'true'));
	 						}else{
	 							if(listCheckfieldHaveDropdownNotfixClass.indexOf(field) != -1){
	 								if(listParser.indexOf(value) != -1){
	 									$('select[id="'+field+'"]').val(value);	
	 								}else if(value == "web.parser.DefaultUserHTMLParser"){
	 									$('select[id="'+field+'"]').val("userConfig");
	 								}else{
		 								$('#'+ field).append('<option value="'+value+'" class="dbvalue" selected="selected">'+value+'</option>');									
	 								}                                             
	 								
	 							}else if(listCheckfieldHaveDropdownfixClass.indexOf(field) != -1){
	 								$('#'+ field).append('<option value="'+value+'" class="dbvalue" selected="selected">'+value+'</option>');	 
	 							}else{
	 								if(field == 'currencyRate' && (value != '1' && value != '1.0'&& value != '1.00')){
	 									$('#'+field).val(value);
	 									$('#'+field).prop('readonly', true);
	 								}else{
	 									$('#'+field).val(value);	 									
	 								}
	 							}

	 						}
	 					}
			  		}
			 
			  	}else {
			  		$('#modalAlert').modal('show');
					$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
					$('#modalAlertData').html('<span style="color:black;font-size:15px;"> Not found merchant id</span>');
			  	}
		  	},
		  	error: function (textStatus, errorThrown) {
		  	}
		});
		$("#merchantHidden").val(merchantId);
		$('#topSaveConfig').show();
		
	}
	
	$(".inputclear").click(function() {
		var input = $(this).parent().find("input");
		input.val('');
		if(input.prop('name') == "url"){
			$('#dataForm').bootstrapValidator('revalidateField', input.prop('name'));
		}
		validateInput(input);
		$(this).hide();
	});
	
	function validateInteger(value){
		var regex = new RegExp('^[0-9]+$');
	    if (regex.test(value)) {
	    	return true;
	    }else{
	    	return false;
	    }
	}

	function validateNull(value){
		if(value == ''){
			return true;
		}else{
			return false;
		}
	}
	
	function isChangeBtn(){
		if($('.errorMsg').size() > 0){
			$('#btnTopSave').prop('disabled', true);
		}else{
			$('#btnTopSave').prop('disabled', false);

		}
	}
	
	function validateInput(data){
		var dataVal =  $(data).val();
		var tagName    = '.'+$(data).attr('name');
		var errormsg = '';
		$(tagName).remove();
		if("currencyRate".indexOf($(data).attr('name')) != -1){
			if(!validateNull(dataVal)){
				if(!$.isNumeric(dataVal)){
	       		 errormsg = errormsg + $(data).attr('name') +" Type must be number"
	       	 	}
			}
		}else if(listInputfieldCheckNumber.indexOf($(data).attr('name')) != -1){
	    	 if(!validateNull(dataVal)){
	    		 if(!validateInteger(dataVal)){
	        		 errormsg = errormsg + $(data).attr('name') +" Type must be number"
	        	 }
	    	 }
	     }
	     
		if(errormsg == ''){
			$(tagName).remove();
		}else{
			 if($(tagName).size() == 0){
				 $(data).parent().append('<span class="errorMsg '+$(data).attr('name')+'" style="color:red;" >'+errormsg+'</span>');		
			 }	
		}
		isChangeBtn();
	}
	
	function addTemplateConfig(){
		var merchantId = $("#merchantHidden").val();
		var templateConfig = $("#templateConfig").val();
		var contextPath = $("#contextPathHidden").val();

		var form = document.createElement("form");
	    var element1 = document.createElement("input"); 
	    var element2 = document.createElement("input");  

	    form.action = contextPath + "/admin/addTemplateConfig";   
	    form.method = "POST";

	    element1.name="merchantId";
	    element1.value=merchantId;
	    form.appendChild(element1);  
	    
	    element2.name="templateConfig";
	    element2.value=templateConfig;
	    form.appendChild(element2);
	    
	    document.body.appendChild(form);
	    form.submit();
	}
	
	