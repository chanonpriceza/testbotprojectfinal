
$(document).ready(function() {
    $('.sidebar ul li a').each(function(){
        if ($(this).prop('href') == window.location.href) {
            $(this).addClass('active'); 
            $(this).parents('li').addClass('active');
            $(this).parents('li').parents('ul').parents('li').children('a').attr("aria-expanded","true");
            $(this).parents('li').parents('ul').parents('li').children('ul').addClass('list-unstyled collapse in'); 
        }
    });
    
    $('[data-toggle="tooltip"]').tooltip();
});