$(document).ready(function () {
	
	$('#reportAllCount').on('change', function() {
		$('#hiddenAllCount').val($('#reportAllCount').val());
		$('#searchReport').submit();
	});
	
	$('#reportTime').on('change', function() {
		$('#hiddenReportTime').val($('#reportTime').val());
		$('#searchReport').submit();
	});
	
	$('#reportActive').on('change', function() {
		$('#hiddenActiveType').val($('#reportActive').val());
		$('#searchReport').submit();
	});	
	
	$('#filterSortBy').on('change', function() {
		$('#hiddenSortBy').val($('#filterSortBy').val());
		$('#searchReport').submit();
	});	
 });



