$(document).ready(function() {
	$('.iconSpin').parent().hide();
});

function deleteItem(btn){
	
	$(btn).button('loading');
	var id = $(btn).closest(".recordRow").find(".id").text();
	var param = "deleteId="+ id;
	$.ajax({
		type: "GET",
		url: 'manualproductmanager-deleteproduct',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		modalAlert('Delete completed');
		  		$(btn).parent().parent().remove();
		  	} else if(data.header == 'not-found-product-in-bot'){
		  		modalAlert('ไม่พบ Product ตัวดังกล่าวบน Server Bot แล้ว ระบบจะทำการลบ record นี้ทันที');
		  		$(btn).parent().parent().remove();
		  	} else if(data.header == 'error-query-delete-get-exception'){
		  		modalAlert('Error : ไม่สามารถบันทึกข้อมูลลงใน Database ได้');
		  	} else if(data.header == 'error-cannot-get-data-server-by-merchant-id'){
		  		modalAlert('Error : ร้านค้าไม่ได้ระบุ Server สำหรับเก็บข้อมูลไว้');
		  	} else if(data.header == 'error-cannot-get-merchant-data-by-merchant-id'){
		  		modalAlert('Error : ไม่พบร้านค้าของ MerchantId นี้');
		  	} else if(data.header == 'error-cannot-get-config-data'){
		  		modalAlert('Error : ไม่พบข้อมูล Server สำหรับส่ง Sync');
		  	} else if(data.header == 'error-cannot-get-manual-bean-by-id'){
		  		modalAlert('Error : ข้อมูลไม่ถูกต้อง ไม่พบ ID ของ Record ที่ต้องการลบใน Database');
		  	} else if(data.header == 'error-input-delete-target-id'){
		  		modalAlert('Error : ข้อมูลไม่ถูกต้อง ไม่พบ ID ของ Record ที่ต้องการลบ');
		  	} 
		  	$(btn).button('reset');
	  	},error: function (textStatus, errorThrown) {
	  		$(btn).button('reset');
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

function modalAlert(result) {
	$('#modalAlertData').html('<center><span style="color:black;font-size:15px;">'+result+'</span></center>');
	$('#modalAlert').modal('show');
}



