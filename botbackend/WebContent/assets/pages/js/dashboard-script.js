/*-----------------------------------/
/*	SET VALUE
/*----------------------------------*/
var setPosition ='';

/*-----------------------------------------------------------------------------------------------------------------------------/
/*	                                                   OPEN DOCUMENT READY
/*----------------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
	
/*-----------------------------------/
/*	CHANGE VALUE
/*----------------------------------*/
	
	
	$('.iconSpin').parent().hide();
	
	$("#topButton").click(function(){
		$("#groupButton").slideToggle("slow");
	});
	
	$('#btnSetting').click(function() {
		$('#modalAdd').modal('show');
	});

	$('#btnTopSave').click(function() {
		$('#modalSave').modal('show');
	});
	
	$('.deletePlugin').click(function() {
		var value = $(this).attr("data-item-id");
		deletePlugin(value);
	});
	
	$('.favoriteMerchant').click(function() {
		ajaxFavoriteMerchant($(this));
	});
	
	$('a[name="btntoTask"]').click(function() {
		var id = $(this).attr("data-element-id");
		window.open("taskmanager?id=" + id, '_blank');
	});
	
	$('#modalAlert').on('hidden.bs.modal', function () {
		var actionAlert = $('#actionAlert').val();
		if(actionAlert != 'save'){
			location.reload();			
		}
	});
	
	$('[data-toggle="tooltip"]').tooltip();
	packery();
	
/*-----------------------------------/
/*	SIDEBAR NAVIGATION
/*----------------------------------*/

$('.sidebar a[data-toggle="collapse"]').on('click', function() {
	if($(this).hasClass('collapsed')) {
		$(this).addClass('active');
	} else {
		$(this).removeClass('active');
	}
});

if( $('.sidebar-scroll').length > 0 ) {
	$('.sidebar-scroll').slimScroll({
		height: '95%',
		wheelStep: 2,
	});
}


/*-----------------------------------/
/*	PANEL FUNCTIONS
/*----------------------------------*/

// panel remove
$('.panel .btn-remove').click(function(e){
	e.preventDefault();
	$(this).parents('.panel').fadeOut(300, function(){
		$(this).remove();
	});
});


});
/*-----------------------------------------------------------------------------------------------------------------------------/
/*	                                                 CLOSE DOCUMENT READY
/*----------------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------/
/*	PACKERY DRAG DROP
/*----------------------------------*/
function packery(){

		// get JSON-friendly data for items positions
		Packery.prototype.getShiftPositions = function( attrName ) {
		  attrName = attrName || 'id';
		  var _this = this;
		  return this.items.map( function( item ) {
		    return {
		      attr: item.element.getAttribute( attrName ),
		      x: item.rect.x / _this.packer.width
		    }
		  });
		};

		Packery.prototype.initShiftLayout = function( positions, attr ) {
		  if ( !positions ) {
		    // if no initial positions, run packery layout
		    this.layout();
		    return;
		  }
		  // parse string to JSON
		  if ( typeof positions == 'string' ) {
		    try {
		      positions = JSON.parse( positions );
		    } catch( error ) {
		      console.error( 'JSON parse error: ' + error );
		      this.layout();
		      return;
		    }
		  }
		  
		  attr = attr || 'id'; // default to id attribute
		  this._resetLayout();
		  // set item order and horizontal position from saved positions
		  this.items = positions.map( function( itemPosition ) {
		    var selector = '[' + attr + '="' + itemPosition.attr  + '"]'
		    var itemElem = this.element.querySelector( selector );
		    var item = this.getItem( itemElem );
		    item.rect.x = itemPosition.x * this.packer.width;
		    return item;
		  }, this );
		  this.shiftLayout();
		};

		// -----------------------------//

		// init Packery
		var $grid = $('.grid').packery({
		  itemSelector: '.grid-item',
		  columnWidth: '.grid-sizer',
		  percentPosition: true,
		  initLayout: false // disable initial layout
		});

		// get saved dragged positions
//		var initPositions = localStorage.getItem('dragPositions');

		var $pluginPositionTags = $('[data-position]');
		if($pluginPositionTags != null){
			var initPositions = [];
			$.each($pluginPositionTags, function(i, tag) {
				initPositions.push($(tag).data("position"));
			});			
			setPosition = JSON.stringify(initPositions);
		}
		

		// init layout with saved positions
		$grid.packery( 'initShiftLayout', initPositions, 'data-item-id' );

		// make draggable
		$grid.find('.grid-item').each( function( i, itemElem ) {
		  var draggie = new Draggabilly( itemElem );
		  $grid.packery( 'bindDraggabillyEvents', draggie );
		});

		// save drag positions on event
		$grid.on('dragItemPositioned', function() {
		  // save drag positions
		  var positions = $grid.packery( 'getShiftPositions', 'data-item-id' );
//		  localStorage.setItem( 'dragPositions', JSON.stringify( positions ) );
		  setPosition = JSON.stringify(positions);
		});
}



/*-----------------------------------/
/*	SAVE POSITION PLUGIN
/*----------------------------------*/
function savePosition(){
	$('#btnSavePosition').button('loading');
	var userID = $('#userID').val();
	if(userID == "" && userID.length <= 0){
		$('#btnSavePosition').button('reset');
		modalAlert("Not found user id");
		return;
	}
	
	var jSonPositions = setPosition;
	if(jSonPositions == null && jSonPositions.lenght <= 0){
		modalAlert("Not found position plugin");
		return;
	}
	var param = "userID="+ userID +"&jSonPositions="+jSonPositions;
	$('#actionAlert').val('save');
	$.ajax({
		type: "POST",
		url: 'savePositionAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		modalAlert("Save complete");
		  	}else if(data.header == 'error'){
		  		modalAlert("Error save position");
		  	}
		  	$('#btnSavePosition').button('reset');
	  	},
	  	error: function (textStatus, errorThrown) {
	  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  		$('#btnSavePosition').button('reset');
	  	}
	});
}


/*-----------------------------------/
/*	CREATE PLUGIN
/*----------------------------------*/
function createPlugin(){
	$('#btnAdd').button('loading');
	var typePlugin = $("#pluginType").val();

	if(typePlugin == "" && typePlugin.length <= 0){
		modalAlert("Cannot setting plugin");
		return;
	}
	
	var param = "typePlugin="+typePlugin;
	$('#actionAlert').val('create');
	$.ajax({
		type: "POST",
		url: 'createPluginAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		modalAlert("Create Success");
		  	}else if(data.header == 'error') {
		  		modalAlert("Cannot create plugin");
		  	}
		  	$('#btnAdd').button('reset');
	  	},
	  	error: function (textStatus, errorThrown) {
	  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  		$('#btnAdd').button('reset');
	  	}
	});
}

/*-----------------------------------/
/*	DELETE PLUGIN
/*----------------------------------*/
function deletePlugin(idPosition){

	var idPlugin = idPosition;

	if(idPlugin == "" && idPlugin.length <= 0){
		modalAlert("Cannot delete plugin");
		return;
	}
	
	var param = "idPlugin="+idPlugin;
	$('#actionAlert').val('delete');
	$.ajax({
		type: "POST",
		url: 'deletePluginAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(data) {
		  	if(data.header == 'success'){
		  		modalAlert("Delete complete");
		  	}else if(data.header == 'error') {
		  		modalAlert("Can not delete ");
		  	}
	  	},
	  	error: function (textStatus, errorThrown) {
	  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}
/*-----------------------------------/
/*	DELETE Delete Favorite
/*----------------------------------*/
function ajaxFavoriteMerchant(value){
	var data = value.attr("data-element-id");
	var merchantId = data.split(",")[0];
	var currentServer = data.split(",")[1];
	var statusFavorite = "false";
	
	var param = "merchantId="+ merchantId +"&currentServer="+currentServer+"&statusFavorite="+statusFavorite;
	$.ajax({
		type: "POST",
		url: 'favoritemerchant',
		data: param,
		cache: false,
		async: true,
		success: function(data) {
			if(data.header == 'success'){
				$(value).parent().parent().remove();
			}
		},
		error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});	

}

/*-----------------------------------/
/*	MODAL ALERT
/*----------------------------------*/
function modalAlert(result){
	$('#modalAlertData').html('<span style="color:black;font-size:15px;">'+result+'</span>');
	$('#modalAlert').modal('show');
}

function commentTemplateTask(btn,status){
	var taskId = $(btn).attr("data-element-id");
	var setMessage = "";
	var setStatus = "";
	if(status == "doing"){
		setMessage = "กำลังทำ";
		setStatus = "DOING";
	}else if(status == "waitsync"){
		setMessage = "สินค้ารอซิงค์";
		setStatus = "DOING";
	}else if(status == "syncsuccess"){
		setMessage = "สินค้าซิงค์เสร็จแล้ว";
		setStatus = "RESOLVE";
	}else if(status == "deletesuccess"){
		setMessage = "ลบสินค้าแล้ว";
		setStatus = "RESOLVE";
	}else if(status == "isactive"){
	setMessage = "ปรับ status Active";
	setStatus = "RESOLVE";
}

	commentTaskDashBoardAjax(btn,setStatus,setMessage,taskId);

}


function commentTaskDashBoardAjax(ele,sStatus,sMessage,taskId){
	var param =  "cmd=addNewComment&taskId="+taskId+"&message=" + encodeURIComponent(sMessage) +"&status="+ sStatus;
	var url = "commentManager";
	$.ajax({
		type: "POST",
		url: url,
		data: param,
		xhrFields: {
		      withCredentials: true
		},
		cache: false,
		async: true,
	  	success: function(data) {
	  		if(data.header == 'success'){
	  			if(sStatus == 'DOING'){
	  				$('#status'+taskId).addClass("badge-warning");
	  				$('#status'+taskId).text(sStatus);
	  				$('#btnComment'+taskId).button('loading');
	  				$('[data-toggle="dropdown"]').parent().removeClass('open');
	  				$('#tooltipcomment'+taskId).attr('data-original-title', sMessage);
	  				
	  				setTimeout(function(){
	  					$('#btnComment'+taskId).button('reset');
	  				}, 1500);
	  				
	  			}else if(sStatus == 'DONE'){
	  				$('#status'+taskId).removeClass("badge-warning").addClass("badge-success");
	  				$('#status'+taskId).text(sStatus);
	  				$('#tooltipcomment'+taskId).attr('data-original-title', sMessage);
	  				setTimeout(function(){
	  					$(ele).parents('tr').remove();	 
	  				}, 1500);
	  				
	  			}else if(sStatus == 'RESOLVE'){
	  				$('#status'+taskId).removeClass("badge-warning").addClass("badge-success");
	  				$('#status'+taskId).text(sStatus);
	  				$('#tooltipcomment'+taskId).attr('data-original-title', sMessage);
	  				setTimeout(function(){
	  					$(ele).parents('tr').remove();	 
	  				}, 1500);
	  				
	  			}
	  		}else{
	  			alert('Error Status');
	  		}
	  	},error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}
