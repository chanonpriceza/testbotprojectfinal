$(document).ready(function () {
	$('#reportType').on('change', function() {
		$('#searchReport').submit();
	});
	
	$('#reportStatus').on('change', function() {
		$('#searchReport').submit();
	});
	
	$('#reportServer').on('change', function() {
		$('#searchReport').submit();
	});
	$('#reportTime').on('change', function() {
		$('#searchReport').submit();
	});
	$('#reportRecord').on('change', function() {
		$('#searchReport').submit();
	});

 });

$('a[name="idRefServers"]').click(function() {
	var value = $(this).attr("data-element-id");
	var server = $(this).parents('.rowPattern').find('td[name="showServer"]').attr("data-element-server");
	var type = $(this).parents('.rowPattern').find('td[name="showType"]').attr("data-element-type");
	var permission_all = $(this).data("value")

	if(value == null || value == ''){
		alert("error");
	}
	else {
		getDataById(value,server,type,permission_all);
	}
	
});

window.onload = function(e){ 		
	$("#showMerchantReport").hide();
}

$("#buttonback").click(function() {
	$("#showMerchantReport").hide();
	$("#ServerReport").show();
});

function changeToNumberFormat(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getDataById(id,server,type,permission_all){
	if(id == ''){ id = 0;}
	var param = "id="+ id +"&server="+server+"&type="+type;
	$('#reportContentMerchant > tbody').html('');
	$('.paginationMerchant').html('');
	
	$.ajax({
		type: "POST",
		url: 'serverReportAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(json) {
		  	if(json.header == 'success') {
		  		$("#showMerchantReport").show();
		  		$("#reportContentMerchant").show();
		  		$("#ServerReport").hide();
		  		var data = json.result;
		  		if(data != null && data.length > 0){
			  		var data = json.result;
			  		var totalpage = json.pageTotal;
			  		var curpage = json.pageCurrent;
			  		var paglist = json.pageList;
			  		var totalList = json.totalList;
			  		var paglist = json.pageList;
			  		var totalList = json.totalList;
			  		var showType = json.showType;
			  		var showServer = json.showServer;
			  		
			  		table = $('#reportContentMerchant').DataTable( {
			  			destroy: true,
						data: data,
						dataType: "json",
                        paging: false,
                        info: false,
                        searching: false,
				        "columns": [
				            { "data": "merchantId" },
				            { "data": "merchantName"},
				            { "data": "Package"},
				            { "data": "CountOld"},
				            { "data": "CountNew"},
				            { "data": "SuccessCount"},
				            { "data": "ErrorCount"},
				            { "data": "Add"},
				            { "data": "Update"},
				            { "data": "UpdatePicture"},
				            { "data": "Delete"},
				            { "data": "StartDate"},
				            { "data": "EndDate"},
				            { "data": "RunTime"},
				            { "data": "Status"}
				        ]
				    } );
			  		
 					totalList = changeToNumberFormat(totalList);
 					$(".txtResult > span").html('<h4 class="txtResult"><span >All Report result ' +totalList+ ' record</h4>')
 					$(".txtTitle").html('<span class="txtTitle">Server : '+ showServer +' > Type : '+ showType +'</span>')
 					
		  		}else{
			  		$("#reportContentMerchant").hide();
 				  	$(".txtResult > span").html('<h4 class="txtResult"><span style="color:red">No Report Result record</h4>')
			  	}
		  	}
	  	},
	  	error: function (textStatus, errorThrown) {
		  	$("#reportContentMerchant").hide();
	  	}
	});
	
}

function generateRow(rowMerchantId,rowMerchantName,rowType,rowPackage,rowCountOld,rowCountNew,rowSuccessCount,rowErrorCount,rowAdd,rowUpdate,rowUpdatePicture,rowDelete,rowServer,rowStartDate,rowEndDate,rowRunTime,rowStatus,permission_all){
	var rowData = "";
		rowData += "<tr class='rowData'>";
		rowData += "	<td style='text-align:center;'>"+rowMerchantId+"</td>";
		rowData += "	<td style='text-align:center;'>"+rowMerchantName+"</td>";
		rowData += "	<td style='text-align:center;'>"+rowPackage+"</td>";
		if(permission_all){
			rowData += "	<c:if test=\"${sessionScope.Permission.containsKey('all') }\">";
			rowData += "	<td style='text-align:center;'>"+rowCountOld+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowCountNew+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowSuccessCount+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowErrorCount+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowAdd+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowUpdate+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowUpdatePicture+"</td>";
			rowData += "	<td style='text-align:center;'>"+rowDelete+"</td>";
		}
		rowData += "	<td style='text-align:center;'>"+rowStartDate+"</td>";
		rowData += "	<td style='text-align:center;'>"+rowEndDate+"</td>";
		rowData += "	<td style='text-align:center;'>"+rowRunTime+"</td>";
		rowData += setColor(rowStatus);
		rowData += "</tr>";
	return rowData;
}

function setColor(rowStatus) {
	var rowData="";
	if(rowStatus != null && rowStatus=='PASSED'){
		rowData += "	<td style='text-align: center; border-radius: 10px;' bgcolor='#ABEBC6'>"+rowStatus+"</td>";
	}else if(rowStatus != null && rowStatus=='FAILED'){
		rowData += "	<td style='text-align: center; border-radius: 10px;' bgcolor='#EC7063'>"+rowStatus+"</td>";
	}else if(rowStatus != null && rowStatus=='RUNNING'){
		rowData += "	<td style='text-align: center; border-radius: 10px;' bgcolor='#AED6F1'>"+rowStatus+"</td>";
	}else{
		rowData += "	<td style='text-align: center; border-radius: 10px;' bgcolor='#ABB2B9'>"+rowStatus+"</td>";
	}
	return rowData;
}