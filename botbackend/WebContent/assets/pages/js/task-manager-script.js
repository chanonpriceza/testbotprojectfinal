
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var addURL =  '<div class="row" style="margin-top:3px;">';
	addURL += '<div class="col-xs-10">';
	addURL += '<input type="text" name="productURLs" onblur="validateInput(this)"class="productURLs form-control">';
	addURL += '</div>';
	addURL += '<button type="button" class="removeURL btn btn-md btn-danger" onclick="removeURL(this)">Remove</button>';
	addURL += '</div>';
	
var addFile  = '<div class="row" style="margin-top:3px;">';                                                          
	addFile += '<div class="col-xs-10">';                                                                             
	addFile += '<input type="file" name="productFiles" onblur="validateInput(this)"class="productFiles form-control">';
	addFile += '</div>';                                                                                             
	addFile += '<button type="button" class="removeFile btn btn-md btn-danger" onclick="removeFile(this)">Remove</button>';  
	addFile += '</div>';                                                                                             

	
	
var AllFieldBox = [
	"issueBox",
	"merchantIdBox",
	"duedateBox",
	"feedType",
	"merchantTypeBox",
	"feedUsernameBox",
	"feedPasswordBox",
	"productTagBox",
	"tagBox",
	"productURLBox",  
	"expectDataBox", 
	"detailBox",
	];

var redMerchantActive = [0,2,6,7,8,14,15];

$(document).ready(function () {
		getTask();
		$("#addTaskForm").keypress(function(e) {
			// Enter key;
			if (e.which == 13 && event.target.nodeName!='TEXTAREA') {
				e.preventDefault();
			}
		});
	$('#checkAll').click(function () {    
		$('input:checkbox').not(this).prop('checked', this.checked); 
	 });
	
	 $('[data-checkbox-target]').on('click', function (e) {
         var $target = $(e.target);
         var checkGroupName = $target.data('checkbox-target')
         var $checkGroup = $(checkGroupName);
         $checkGroup
             .find('[type="checkbox"]')
             .prop('checked', $target.prop('checked'));
     });
	
	$("#showTaskDetail").hide();
	$('#submitbtn').click(function(){
	    $("form#addTaskForm input").each(function() {
	    	validateInput(this)
	    });
	});
	
	Date.prototype.toDateInputValue = (function() {
	    var local = new Date(this);
	    var minutes = this.getMinutes();
	    var timeZone = this.getTimezoneOffset();
	    local.setMinutes((minutes - timeZone)+5*24*60);
	    return local.toJSON().slice(0,10);
	});
	
	$('#addNewTask').click(function() {
		$("#feedType").val('xml');
		$("#actionType").val("checkExceedProduct");
		$('#addForm').modal('show');
		
		$('#productFileBox div').empty();
		if($('.productFiles').size() == 0){
			$('#productFileBox').append(addFile);
		}
		
		$('#productURLBox div').empty();
		if($('.productURLs').size() == 0){
			$('#productURLBox').append(addURL);
		}
		
		$('#duedate').val(new Date().toDateInputValue());

		showField();
	});

	
	$('#actionType').change(function() {
		showField();
	});
	
//	$('#merchantType').change(function() {
//		if($('#merchantType').val() == "none"){
//			$("#package").val('');
//			$("#packageBox").parent().hide(); 
//		}else{
//			$("#package").val(1);
//			$("#packageBox").parent().show(); 
//		}
//	});
	
	$('#taskFormReset').click(function() {
		clearForm();
	});
   
	$('#addForm').on('hide.bs.modal', function() {
		clearForm();
	});

	$('#addForm').submit(function(e) {
		   
		if ($("#feedType").is(':hidden')) {
			$("#feedType").val('');
		}

	});
	

	$('#confirm-delete').on('hide.bs.modal', function() {
		 $("#commentId").val('');
	});
	
	$("#buttonback").click(function() {
		$("#showTaskDetail").hide();
		$("#taskManagerTable").show();
		
		var checkurl = window.location.href;
		if(checkurl.indexOf("taskmanager") > -1 && checkurl.indexOf("id=") > -1){
			var newURL = checkurl.split("?")[0];
			newURL = newURL.substring(newURL.indexOf("botbackend"), newURL.length);
		    window.history.replaceState(null, null, "/"+newURL );
		}
		
		var taskId = $("#taskId").val();
		if(taskId != ""){
			getTaskById(taskId);
		}
		$("#taskId").val('');
		
		getTask($('#currentCountry').val(),$('#currentPage').val());
	});
	
	$("#confirmbtn").click(function() {
		var commentId = $("#commentId").val();
		var taskId = $("#taskId").val();
		var param =  "cmd=removeComment&taskId="+taskId+"&commentId="+commentId;
		$('#confirm-delete').modal('hide');
		commentAjax(param);
	});
	
	$(document).on("click",'a[name="idRefTask"]',function() {
		var taskId = $(this).attr("data-element-id");
		var param =  "cmd=getCommentById&taskId="+taskId;
		
		$("#taskId").val(taskId);
		var checkurl = window.location.href;
		if(checkurl.indexOf("taskmanager") > -1){
		    window.history.pushState(null, null, "?id="+taskId );
		}
		commentAjax(param);
		
		$("#showTaskDetail").show();
		$("#taskManagerTable").hide();
	});
	
	$(document).on("click",'a[name="pagination"]',function() {
		var pageId = $(this).attr("data-element-id");
		getTask($('#currentCountry').val(),pageId);
	});
	
	$(document).on("click",'button[name="countrySelector"]',function() {
		$(".active ,button[name=\"countrySelector\"]").removeClass("active");
		//addNewTask
		var currentServer = $('#currentServer').val().toUpperCase();
		var country = $(this).text().toUpperCase();

		$(this).addClass("active");
	});
	
	$("#commentSubmit").click(function() {
		var message = $("#commentBox").val();
		var status = $("#commentStatus").val();
		var taskId = $("#taskId").val();
		if(message == ""){
			alert("Plesea write a comment!");
			$("#commentBox").focus();
			return;
		}
		var param =  "cmd=addNewComment&taskId="+taskId+"&message=" + encodeURIComponent(message) +"&status="+ status;
		commentAjax(param);
	});
	
	
	$('#inputType').on('change', function() {
		$('input[name=cmd]').val("searchTaskManager");
		$('#processType').val($('#inputType').val());
		getTask();
	});

	$('#inputStatus').on('change', function() {
		$('input[name=cmd]').val("searchTaskManager");
		$('#statusType').val($('#inputStatus').val());
		getTask();
	});
	
	$('#inputOwner').on('change', function() {
		$('input[name=cmd]').val("searchTaskManager");
		$('#ownerType').val($('#inputOwner').val());
		getTask();
	});
	
	$('[data-toggle="tooltip"]').tooltip();

	$('#searchTaskManager').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    var currentPage = $('#currentPage').val();
		    getTask($('#currentCountry').val(),currentPage);
		    return false;
		  }
	});
	
	$('#searchfilterMerchant').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    var currentPage = $('#currentPage').val();
		    getTask($('#currentCountry').val(),currentPage);
		    return false;
		  }
	});
 });

function loadWindowSystem(){
	var parameter_index = window.location.href.indexOf('=');
	if(parameter_index != -1){
		var taskId = window.location.href.slice(parameter_index + 1);
		var param =  "cmd=getCommentById&taskId="+taskId;
		
		$("#taskId").val(taskId);
		commentAjax(param);
		
		$("#showTaskDetail").show();
		$("#taskManagerTable").hide();
	}
}

function setTemplateComment(command){
	var setMsg = "";
	var setStatus = "";
	if(command == "doing"){
		setMsg = "กำลังทำ";
		setStatus = "DOING";
	}else if(command == "waitsync"){
		setMsg = "สินค้ารอซิงค์";
		setStatus = "DOING";
	}else if(command == "syncsuccess"){
		setMsg = "สินค้าซิงค์เสร็จแล้ว";
		setStatus = "RESOLVE";
	}else if(command == "deletesuccess"){
		setMsg = "ลบสินค้าแล้ว";
		setStatus = "RESOLVE";
	}else if(command == "checktaskagain"){
		setMsg = "พบปัญหา ตรวจสอบอีกรอบ";
		setStatus = "DOING";
	}else if(command == "done"){
		setMsg = "OK. ปิด tasks ";
		setStatus = "DONE";
	}else if(command == "isactive"){
		setMsg = "ปรับ status Active";
		setStatus = "RESOLVE";
	}

	$("#commentBox").val(setMsg);
	$("#commentStatus").val(setStatus);
}

function setTypes (type, name) {
	$('#searchType').val(type);
	$('#filter').text(name).append('&nbsp;<span class="fa fa-caret-down"></span>');
}

function convertStringArraytoString(value){
	var strvalue ="";
	if(value.length > 0 && value != ""){
		
		for (var i=0;i<value.length;i++) 
		{
			if (value[i].checked){
				strvalue += "'"+value[i].value+"'"+",";
			}
		}
		strvalue = strvalue.substring(0,strvalue.length-1);
		if(strvalue != ""){
			strvalue = "("+strvalue+")";					
		}	
	}
	return strvalue;
}

function submitFilterSearch() {
	if ($('#checkAll').prop('checked') == false) {
		event.preventDefault();
		var checkStatus = document.getElementsByName('filterCheckStatus[]');
		var checkOwner = document.getElementsByName('checkOwner[]');
		var checkType = document.getElementsByName('checkProcess[]');

		var status = convertStringArraytoString(checkStatus);
		var owner = convertStringArraytoString(checkOwner);
		var type = convertStringArraytoString(checkType);
	}

	$('#filterStatusType').val(status);
	$('#filterOwnerType').val(owner);
	$('#filterProcessType').val(type);

	$('#searchfilterMerchant').submit();
}

function clearForm(){
	$("#feedType").val("xml");
	$("#package").val('1');
	$("#issue").val('');

	$('#merchantId').val('');

	$('#feedUsername').val('');
	$('#feedPassword').val('');

	$('#productTag').val('');
	$('input[name=tags]').val('');
	$('input[name=productURLs]').val('');
	$('#actualData').val('');
	$('#expectData').val('');
	$('#detail').val('');
	$('#duedate').val('');
	$('.errorMsg').remove();  
	$('#submitbtn').prop('disabled', false);
	document.getElementById('productURLTooltip').setAttribute('data-original-title', 'link สิ้นค้าที่ไม่ถูกดึงเข้ามา');
	
	$('#merchantType option').each(function () {
	    if ($(this).css('display') != 'none') {
	        $(this).prop("selected", true);
	        return false;
	    }
	});
	
//	if($("#merchantType").val() == "none"){
//		$("#packageBox").parent().hide(); 
//	}else{
//		$("#packageBox").parent().show(); 
//	}
	
}

 
 function confirmDeleteComment(btn){
	 $("#commentId").val($(btn).attr("data-element-id"));
	 $('#confirm-delete').modal('show');
 }
 
function toggleShow(btn){
	var comment = $(btn).parent().parent().find(".comment-content");
	var status = $(btn).parent().parent().find(".comment-status");
	if($(btn).hasClass("fa fa-angle-down")){
		$(btn).removeClass("fa fa-angle-down").addClass("fa fa-angle-right");
		$(comment).css("display","none");
		$($(btn).parent()).append(status);
	 }else{
		 $(btn).removeClass("fa fa-angle-right").addClass("fa fa-angle-down");
		 $(comment).append(status);
		 $(comment).css("display","block");
	 }
	
}

function removeTag(btn) {
	$(btn).parent().remove();
	var newTag =  addTag;
	if($('.tags').size() == 0){
		$('#tagBox').append(newTag);
	}
}

function removeURL(btn) {
	$(btn).parent().remove();
	var newTag =  addURL;
	if($('.productURLs').size() == 0){
		$('#productURLBox').append(newTag);
	}
}

function addNewURL() {
	var errormsg = '';
	var isAppend = true;
	var type = $('#feedType').val();
	$('.productURL').remove();
	$('.productURLs').each(function(){
//		var isTag = validateTag($(this).val());
		if($(this).val() == ""){
			errormsg = errormsg + 'Tag  is required';
			isAppend = false;
			
		}else if(!validateUrlPattern($(this).val())){
   		 errormsg = errormsg + "Url must start with http:// or  https:// "
   		isAppend = false;
   	 }
	});
	
	if(isAppend){
		$('#productURLBox').append(addURL);
	}
	isPermittedBtn();
	
}
function addNewTag() {
	var errormsg = '';
	var isAppend = true;
	var type = $('#feedType').val();
	$('.tag').remove();
	$('.tags').each(function(){
		var isTag = validateTag($(this).val());
		if($(this).val() == ""){
			errormsg = errormsg + 'Tag  is required';
			isAppend = false;
			
		}else if(!(isTag) && type == 'xml'){
			errormsg = errormsg + "Tag must start with  <  and end with  > "
			isAppend = false;
		}
	});
	
	if(isAppend){
		$('#tagBox').append(addTag);
	}
	isPermittedBtn();
	
}

function removeFile(btn) {
	$(btn).parent().remove();
	var newTag =  addFile;
	if($('.productFiles').size() == 0){
		$('#productFileBox').append(newTag);
	}
}

function addNewFile() {
	var errormsg = '';
	var isAppend = true;
	$('.productFile').remove();
	$('.productFiles').each(function(){
		if($(this).val() == ""){
			errormsg = errormsg + 'File  is required';
			isAppend = false;		
		}
	});
	
	if(isAppend){
		$('#productFileBox').append(addFile);
	}
	isPermittedBtn();
}

function isPermittedBtn(){
	if($('.errorMsg').size() > 0){
		$('#submitbtn').prop('disabled', true);
	}else{
		$('#submitbtn').attr('onclick','sendAddTaskData()');
		$('#submitbtn').prop('disabled', false);
	}
}

function validateInput(btn){
	var type = $('#feedType').val();
	var actionType = $('#actionType').val();
	var btnVal =  $(btn).val();
	var tagName    = '#errorMsg'+$(btn).attr('name');
	var errormsg = '';
	$(tagName).remove();
	
	if ($(btn).is(':hidden')) {
		return;
	}
     if ($(btn).attr('name') == 'urlLink' || $(btn).attr('name') == 'productURLs') {
    	 if(validateNull(btnVal)){
    		 errormsg = errormsg + "Url link is required"
    	 }else if(!validateUrlPattern(btnVal)){
        		 errormsg = errormsg + "Url must start with http:// or  https:// "
         }
    }else if($(btn).attr('name') == 'merchantId'){
    	if(actionType == 'checkCrawlerWeb' || actionType == 'checkCrawlerFeed' || actionType == 'others' ){
    	}else{
    		if(validateNull(btnVal)){
    			errormsg = errormsg + "Merchant ID is required"
    		}else if(!validateInteger(btnVal)){
    			errormsg = errormsg + "Merchant ID must be number"
    		}    		
    	}
     }else if($(btn).attr('name') == 'productTag'){
    	 if(validateNull(btnVal) && type == 'xml'){
    		 errormsg = errormsg + "Product Tag is required"
    	 }else{
    		 if(!validateTag(btnVal) && type == 'xml'){
        		 errormsg = errormsg + "Tag must start with  <  and end with  > "
        	 }
    	 }
     }else if($(btn).attr('name') == 'tags'){
    	 if(actionType == 'feedCatList'){
	    	 if(validateNull(btnVal) ){
	    		 if(type == 'xml' ){
    				 errormsg = errormsg + "Input Tag is required"
	    		 }else if (type == 'json'){
    				 errormsg = errormsg + "Input Tag is required"
	    		 }
	    	 }else{
	    		 if(type == 'xml' && !validateTag(btnVal)){
	        		 errormsg = errormsg + "Tag must start with  <  and end with  > "
	        	 }
	    	 }
    	 }
    	 
     }else if($(btn).attr('name') == 'date'){
		 if(validateNull(btnVal)){
			 errormsg = errormsg + "Date is required"
		 }
     }else if($(btn).attr('name') == 'detail'){
		 if(validateNull(btnVal) && actionType == 'others' ){
			 errormsg = errormsg + "Detail is required"
		 }
     }else if($(btn).attr('name') == 'issue'){
		if(validateNull(btnVal)){
			errormsg = errormsg + "Issue is required"
		}
     }
     

	if(errormsg == ''){
		$(tagName).remove();
	}else{
		 if($(tagName).size() == 0){
			 $(btn).parent().append('<span class="errorMsg" id="errorMsg'+$(btn).attr('name')+'" style="color:red;" >'+errormsg+'</span>');	
		 }	
	}
	isPermittedBtn();
	
}

function validateUrlPattern(value){
	var regex = new RegExp("^(http|https)://.*$");
    if (regex.test(value)) {
    	return true;
    }else{
    	return false;
    }
	
}

function validateTag(value){
	var regex = new RegExp("^<.*>$");
    if (regex.test(value)) {
    	return true;
    }else{
    	return false;
    }
	
}

function validateInteger(value){
	var regex = new RegExp('^[0-9]+$');
    if (regex.test(value)) {
    	return true;
    }else{
    	return false;
    }
}

function validateNull(value){
	return value == '';
}
function showField(){
	var actionType = $('#actionType').val();
	var field = [];
	$('#productFileBox').show();
	$('#productFileBox').parent().show();
	if(actionType == 'checkExceedProduct'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox","detailBox" ];
		$(".text-danger").show();
		document.getElementById('productURLTooltip').setAttribute('data-original-title', 'link สิ้นค้าที่มีอยู่บนหน้าเว็บ priceza แต่ไม่มีในหน้าเว็บลูกค้า');
		
	}else if(actionType == 'checkDuplicateProduct'){
		field = ["merchantTypeBox", "issueBox","duedateBox", "packageBox", "merchantIdBox", "productURLBox", "detailBox" ];
		$(".text-danger").show();
		
		document.getElementById('productURLTooltip').setAttribute('data-original-title', 'link เช็คสินค้าซ้ำ');
	}else if(actionType == 'removeProductPermanent'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox","detailBox" ];
		$(".text-danger").show();	
		
	}else if(actionType == 'addAndRemoveProduct'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox","detailBox" ];
		$(".text-danger").show();
		
	}else if(actionType == 'checkExpireProduct'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox"];
		$(".text-danger").show();
		
	}else if(actionType == 'changeDetailCrawlingData'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox", "expectDataBox"];
		$(".text-danger").show();
		
	}else if(actionType == 'newMerchant'){
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox"];
		$(".text-danger").show();
		
	}else if(actionType == 'checkCrawlerWeb'){
		field = ["merchantTypeBox", "issueBox","duedateBox", "merchantIdBox", "detailBox"];
		$(".text-danger").hide();

	}else if(actionType == 'checkCrawlerFeed'){
		field = ["merchantTypeBox", "issueBox","duedateBox", "merchantIdBox", "detailBox","feedUsernameBox", "feedPasswordBox"];
		$(".text-danger").hide();
		
	}else{
		field = ["merchantTypeBox", "issueBox","duedateBox","merchantIdBox", "detailBox" ];
		$(".text-danger").hide();
	}
//	else if(actionType == 'getSupportPerformanceReport'){
//		field = ["getSupportPerformanceReport","issueBox"];
//		$('#productFileBox').hide();
//		$('#productFileBox').parent().hide();
//		$("#merchantType option[value='none']").hide();
//		$("#merchantType").val('test');
//	}
//	else if(actionType == 'reCrawling' || actionType == 'merchantFeedParser' || actionType == 'merchantParser'){
//		field = ["merchantTypeBox", "packageBox", "merchantIdBox", "detailBox" ];
//	}
//	else{
//		field = ["merchantTypeBox", "issueBox","duedateBox",  "packageBox", "merchantIdBox", "detailBox" ];
//		$("#merchantType option[value='none']").hide();
//		$("#merchantType").val('test');
//	}
	setFieldShow(field);
}



function setFieldShow(showedField){
	for (i = 0; i < AllFieldBox.length; i++) {
		var field = $("#"+AllFieldBox[i]).parent().parent(); 
		if(AllFieldBox[i] == "merchantTypeBox" || AllFieldBox[i] == "packageBox"){
			field = $("#"+AllFieldBox[i]).parent();
		}if(AllFieldBox[i] == "feedUsernameBox" || AllFieldBox[i] == "feedPasswordBox"){
			field = $("#"+AllFieldBox[i]).parent().parent().parent(); 
		}
		
		if(showedField.includes(AllFieldBox[i])){
			if('feedType'==AllFieldBox[i]){
				$("#feedType").prop( "disabled", false );
			}
			field.show();
		}else{
			if(field.attr('class')!='portlet light'){
				if('feedType'==AllFieldBox[i]){
					$("#feedType").prop( "disabled", true );
				}
				field.hide();
			}
	
			 $("#"+AllFieldBox[i]).find('input').val('');   
			 $("#"+AllFieldBox[i]).find('textarea').val('');    
			 $('.errorMsg').remove();
			 $('#submitbtn').prop('disabled', false);
			 
		}
		
		
	}
}

function addCommentHis(id, message, name, date, status, currentUser){
	
	//convert to String
	var str = message;
	var p = document.createElement("p");
	p.textContent = str;
	var converted = p.innerHTML;
	
	converted = converted.replace(new RegExp("&lt;br /&gt;",'g'), "</br>");
	
	var commentBox = "";
	
	commentBox += "<div class=\"clearfix\">";
	commentBox += " <div class=\"comment-block\">";
	commentBox += " 	<div class=\"comment-header clearfix\">";
	commentBox += " 		<i class=\"fa fa-angle-down\" onclick=\"toggleShow(this)\"></i>";
	commentBox += " 		<b> "+name +"</b> - " + date;
	if(name == currentUser ){
		commentBox += "      	<div class=\"comment-more\">";
		commentBox += "      		<i class=\"fa fa-trash dropdown-toggle\" data-element-id=\""+id+"\"onclick=\"confirmDeleteComment(this)\" id=\"removeComment\"></i>";
		commentBox += "        </div>	";
	}
	commentBox += "    </div>";
	commentBox += " 	<div class=\"comment-content\">";
	commentBox += "      	<div id = \"message\">";
	commentBox += 				converted;
	commentBox += "      	</div>";
	commentBox += "      	<div class=\"comment-status\">";
	commentBox += "      		<b>Status :</b> " + status;
	commentBox += "      	</div>";
	commentBox += " 	</div>";
	commentBox += "</div>";
	commentBox += "</div>";
	
	$(".list-comments").prepend(commentBox);
	
}

function commentAjax(param){
	
	var contextPath = genServiceUrl("taskmanager");
	var url = genServiceUrl("commentManager");
	
	$.ajax({
		type: "POST",
		url: url,
		data: param,
		xhrFields: {
		      withCredentials: true
		},
		cache: false,
		async: true,
	  	success: function(data) {
	  		if(data.header == 'success'){
	  			
	  			var message = $("#commentBox").val('');
	  			var taskStatus = 'WAITING';
	  			var row = data.currentRole;
	  			if(row=='dev'){
	  				taskStatus = 'DOING';
	  			}
	  			var status = $("#commentStatus").val(taskStatus);
	  			$(".list-comments").empty();
	  			var commentList = data.commentList;
	  			var taskData = JSON.parse(data.taskData.data);
	  			var taskIssue = data.taskData.issue;
	  			var dummyData = new Object();
	  			
	  			var active = data.active;
	  			console.log(active);
	  			if(active!=1){
	  				$('#statusactive').css('background','#ff0000');
	  				$('#detailId').val(taskData.merchantId);
	  			}else{
	  				$('#statusactive').css('background','#48ff5f');
	  				$('#statusactive').attr('disabled','true');
	  				$('#statusactive').html('Actived');
	  			}
	  			
	  			var i = 1;
	  			$("#detailBox").empty();
	  			$("#downloadBox").empty();
	  			
	  			//dummyData. = taskData.;
	  			
	  			dummyData.detail = taskData.detail;
	  			
	  			if(dummyData.detail!=null){
	  				delete taskData.detail;
	  			}
	  			
	  			if(taskIssue!=null){
	  				dummyData.issue = taskIssue;
	  			}
	  			
	  			dummyData.duedate = taskData.duedate;
	  			if(dummyData.duedate!=null){
	  				delete taskData.duedate;
	  			}
	  			
	  			dummyData.merchantName = taskData.merchantName;
	  			
	  			if(dummyData.merchantName!=null){
	  				delete taskData.merchantName;
	  			}
	  			
	  			
	  			dummyData.merchantId = taskData.merchantId;
	  			if(dummyData.merchantId!=null){
	  				delete taskData.merchantId;
	  			}
	  			
	  				  				  			  			
	  			for (var key in taskData) {
	  				if(taskData[key]==null){
	  					continue;
	  				}
	  				var value;
	  				var tmp ;
	  				if(key=='urls'){
	  					value = taskData[key].toString().replace(/\|/g,"<br />");
	  				}else if(key=='tag'){ 
	  					value = taskData[key].toString().replace(/\</g,"&lt;").replace(new RegExp(/\>/g, 'gi'),"&gt;");	  					
	  				}else{
	  					value = taskData[key].toString().replace("<","&lt;").replace(">","&gt;");
	  				}
	  				
  					if(key == 'fileUpload' && (value != null && value.length > 0)){
  						var mfile = (value.substring(0, value.length - 1)).split('|');
  						for (var i = mfile.length-1; i >=0; i--) {	
  							tmp  = "<div class=\"col-md-12\">";  						
  	  						tmp += "<div class=\"col-md-2\" style=\"padding:10px;word-break:break-all;\"><b>" + key +" "+(i+1)+" </b></div>";
  	  						tmp += "<div class=\"col-md-10\" style=\"padding:10px;white-space: pre;word-break:break-all;\">  "	
  	  						tmp	+= "<a target='_blank' onclick='return popUnder(this);' href='"+contextPath+"?cmd=exportCSV&fileName="+mfile[i]+"'><button type='button' class='btn btn-circle' style='background-color: #fafcfb;float: initial;width: 30px;height: 30px;padding: 6px 0px;border-radius: 15px;text-align: center;font-size: 12px;line-height: 1.42857;'><i class='fa fa-download fa-lg' aria-hidden='true' style='color: royalblue;'></i></button></a>";  						
  	  						tmp	+=" &nbsp;"+ mfile[i] +"</div><br>";  						
  	  						tmp += "</div>";  						
  	  						$("#detailBox").prepend(tmp);

  						}
  					}else{
  						var order = []; 
  						tmp = "<div class=\"col-md-12\">";  						
  						tmp += "<div class=\"col-md-2\" style=\"padding:10px;word-break:break-all;\"><b>" + key + " </b></div>"; 
  						tmp	+="<div class=\"col-md-10\" style=\"padding:10px;white-space: pre;word-break:break-all;\"> &nbsp;"+ value +"</div><br>";  						
  						tmp += "</div>";  	
  						$("#detailBox").prepend(tmp);
  					}
	  				
	  		    }
	  			for (var key in dummyData) {
	  				if(dummyData[key]==null){
	  					continue;
	  				}
	  				var value;
	  				var tmp ;
	  				if(key=='urls'){
	  					value = dummyData[key].toString().replace(/\|/g,"<br />");
	  				}else if(key=='tag'){ 
	  					value = dummyData[key].toString().replace(/\</g,"&lt;").replace(new RegExp(/\>/g, 'gi'),"&gt;");	  					
	  				}else{
	  					value = dummyData[key].toString().replace("<","&lt;").replace(">","&gt;");
	  				}
	  				
  					if(key == 'fileUpload' && (value != null && value.length > 0)){
  						var mfile = (value.substring(0, value.length - 1)).split('|');
  						for (var i = mfile.length-1; i >=0; i--) {	
  							tmp  = "<div class=\"col-md-12\">";  						
  	  						tmp += "<div class=\"col-md-2\" style=\"padding:10px;word-break:break-all;\"><b>" + key +" "+(i+1)+" </b></div>";
  	  						tmp += "<div class=\"col-md-10\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">  "	
  	  						tmp	+= "<a target='_blank' onclick='return popUnder(this);' href='"+contextPath+"?cmd=exportCSV&fileName="+mfile[i]+"'><button type='button' class='btn btn-circle' style='background-color: #fafcfb;float: initial;width: 30px;height: 30px;padding: 6px 0px;border-radius: 15px;text-align: center;font-size: 12px;line-height: 1.42857;'><i class='fa fa-download fa-lg' aria-hidden='true' style='color: royalblue;'></i></button></a>";  						
  	  						tmp	+=" &nbsp;"+ mfile[i] +"</div><br>";  						
  	  						tmp += "</div>";  						
  	  						$("#detailBox").prepend(tmp);

  						}
  					}else{
  						var order = []; 
  						var context = $("#contextPath").val();
  						tmp = "<div class=\"col-md-12\">";  						
  						tmp += "<div class=\"col-md-2\" style=\"padding:10px;word-break:break-all;\"><b>" + key + " </b></div>"; 	
  						if(key ==  'merchantId'){
  							if(value == '0'){
  								tmp	+="<div class=\"col-md-10\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">"+ value;
  							}else{
  								tmp	+="<div class=\"col-md-10\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">";  
  								tmp	+= "<a target='_blank' href='"+context+"/admin/getmerchantdata?searchParam="+value+"'>"+value+"</a>";
  								tmp	+= "<a target='_blank' href='"+context+"/admin/merchantruntimereport?searchParam="+value+"'><i class='fa fa-desktop fa-lg' aria-hidden='true' style='color: royalblue;margin-left: 10px;'></i></a>";   								
  							}
  						}else{
  							tmp	+="<div class=\"col-md-10\" style=\"padding:10px;white-space: pre-wrap;word-break:break-all;\">"+ value;  
  						}	
  						tmp += "</div><br>";  	
  						tmp += "</div>";  	
  						$("#detailBox").prepend(tmp);
  					}
	  				
	  		    }
	  			
	  			
	  			for(i=0; i<commentList.length; i++){
	  				var comment = commentList[i];
	  				var jsonComment = JSON.parse(comment.message);
	  				var createDate = new Date(comment.createDate);
	  				var date = createDate.getDate() + " "+months[createDate.getMonth()]+" "+createDate.getFullYear()+" "+createDate.getHours()+":"+createDate.getMinutes()+":"+createDate.getSeconds();
	  				addCommentHis(comment.id, jsonComment.message.split('\n').join('<br />'), comment.owner, date, comment.status, data.currentUser);
	  			}
	  			  			
	  		}else{
	  			alert('Error Status');
	  		}
	  	},error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
	  	}
	});
}

	function setColor(rowStatus) {
		var rowData="";
		if(rowStatus != null && rowStatus=='WAITING'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#AED6F1;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='DOING'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#F9E79F;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='REOPEN'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#EBDEF0;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='HOLD'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#EC7063;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='DONE'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#ABEBC6;'>"+rowStatus+"</button>";
		}else if(rowStatus != null && rowStatus=='AUTOREPLY'){
			rowData += "<button type='button' class='btn' style='width: 100%; background-color:#ABB2B9;'>"+rowStatus+"</button>";
		}
		return rowData;
	}
	
	function popUnder(node) {
	    var newWindow = window.open("about:blank", node.target, "width=500,height=500");
	    newWindow.blur();
	    window.focus();
	    newWindow.location.href = node.href;
	    return false;
	}
	
	function isEmpty(str) {
	    return (!str || 0 === str.length);
	}
	
	function getTaskById(taskId){
		var contextPath = genServiceUrl("taskmanager");
		url = genServiceUrl("taskmanagerAjax");
		var param =  "cmd=getTaskById&taskId="+taskId;
		$.ajax({
			type: "POST",
			url: url,
			data: param,
			cache: false,
			async: true,
			xhrFields: {
			      withCredentials: true
			},
		  	success: function(data) {
		  		if(data.header == 'success'){
		  			var task = data.data;
		  			
		  			var status = task.status;
		  			var updated = task.updatedBy;
		  			var fileName = null;
		  			if(status != null || status != ""){
		  				$("#status"+taskId).html(setColor(status));
		  			}
		  			if(updated == null || updated == ""){
		  				$("#updated"+taskId).text('-');
		  			}else{
		  				$("#updated"+taskId).text(updated);
		  			}
		  			
		  			var lastResult = (task.result == null)? null:JSON.parse(task.result);
		  	
	  				if(lastResult&&isEmpty(lastResult.fileName) && (isEmpty(lastResult) || isEmpty(lastResult.message))){
		  				$("#lastResult"+taskId).text("-");
		  			}else if(lastResult&&isEmpty(lastResult.fileName) && !isEmpty(lastResult) && !isEmpty(lastResult.message))
		  				$("#lastResult"+taskId).html('<a class="fa fa-commenting fa-lg" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="'+encodeHTML(lastResult.message)+'"></a>');
		  			}else{
		  				fileName = lastResult.fileName.replace("<","&lt;").replace(">","&gt;");
		  				var tmp = "<a target='_blank'  onclick='return popUnder(this);' href='"+contextPath+"?cmd=exportCSV&fileName="+fileName+"'><button type='button' class='btn' style='background-color: #62D528;float: right;'><i class='fa fa-download fa-lg' aria-hidden='true' style='color: royalblue;'></i></button></a>";
  		  				$("#lastResult"+taskId).html(tmp);
		  			}
		  			$('[data-toggle="tooltip"]').tooltip();
		  			
		  		
		},error: function (textStatus, errorThrown) {
			alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		}
	});
	}
	
	function getTask(country,page){
		var bot_token = Cookies.get('bot_token');
		if ($('#checkAll').prop('checked') == false) {
			event.preventDefault();
			var checkStatus = document.getElementsByName('filterCheckStatus[]');
			var checkOwner = document.getElementsByName('checkOwner[]');
			var checkType = document.getElementsByName('checkProcess[]');

			var status = convertStringArraytoString(checkStatus);
			var owner = convertStringArraytoString(checkOwner);
			var type = convertStringArraytoString(checkType);
		}
		
		var contextPath = $("#contextPath").val();
		
		if(!page){
			page = $('#currentPage').val();
			if(!page){
				page = 1;
			}
		}
		
		if(!country){
			country = $("#currentCountry").val();
			if(!country){
				country = $('#currentServer').val();
				if(!country){
					country = 'TH';
				}
			}
			
		}
		var formParam = $('#searchTaskManager').serialize().replace("&statusType=","&statusType="+status).replace("&ownerType=","&ownerType="+owner).replace("&processType=","&processType="+type)+"&bot_token="+bot_token;
		country = country.toLowerCase();
		var url = $("#service-url-"+country).val();
		
		if(!url){
			url = "taskDataAjax";
		}else{
			url = url+"/admin/taskDataAjax";
			var addTaskManagerUrl = url.replace('taskDataAjax','taskmanager');
		}
		
		var param = "country="+country+"&page="+page+"&"+formParam;
			$.ajax({
				type: "POST",
				url: url,
				data: param,
				xhrFields: {
				      withCredentials: true
				},
				crossDomain:true,
				cache: false,
				beforeSend: function(){
					showLoading();
			    },
			  	success: function(data) {
			  		genTaskRow(data);
			  		addServiceUrl(data);
			  		genPaging(data);
					$("#currentCountry").val(country);
					$("#addTaskForm").attr('action', addTaskManagerUrl);
					$('[data-toggle="tooltip"]').tooltip();
					loadWindowSystem();
			  	}	
			 });
	}
	
	function printDate(cDate,notPrintHour){
		var hourString = "";
		if(notPrintHour==null){
			var min = cDate.getMinutes();
			var second = cDate.getSeconds();
			if(min < 10){
				min = "0"+min;
			}
			
			if(second < 10){
				second = "0"+second;
			}
			hourString = " "+cDate.getHours()+":"+min+":"+second+"";
		}
		
		if(!cDate){
			return "-";
		}
		const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		return cDate.getDate()+" "+months[cDate.getMonth()]+" "+cDate.getFullYear()+hourString;
	}
	
	function checkString(input){
		if(input){
			return input;
		}
		return "-";
	}
	
	function genTaskRow(data){
		var contextPath = genServiceUrl("taskmanager");
		var result = "";
		var getData = data.model.taskList;
		if(!getData){
			$("#taskBoxCotainer").html('');
			return false;
		}
		getData.forEach(function(task, index, array) {
  			var obj = JSON.parse(task.data);
  			var active = task.active;
  			var cDate = new Date(task.createDate);
  			var eDate = new Date(obj.duedate);
  			var issueType = task.issueType;
  			var statusColor = "";
  			if(task.status=="WAITING"){
  				statusColor = "#AED6F1";
  			}else if(task.status=="DOING"){
  				statusColor = "#F9E79F";
  			}else if(task.status=="REOPEN"){
  				statusColor = "#EBDEF0";
  			}else if(task.status=="HOLD"){
  				statusColor = "#EC7063";
  			}else if(task.status=="DONE"){
  				statusColor = "#ABEBC6";
  			}else if(task.status=="AUTOREPLY"){
  				statusColor = "#ABB2B9";
  			}else if(task.status=="RESOLVE"){
  				statusColor = "#ffc0cb";
  			}

  			result+="<tr class='rowPattern'>";
  			result+="<td width='5%' style='text-align: center;'>"+checkString(task.id)+"</td>";
  			result+="<td width='8%' style='text-align: center;'>"
  			result+="<div class=\"row\">";
  			result+=checkString(task.merchantId);
  			result+="</div>";
  
  			if(!redMerchantActive.includes(active)){
  				result+="<div class=\"row\" style=\"font-size: x-small;\">";
  				result+= checkString(obj.merchantName);
  				result+="</div>";
  			}else{
  				result+="<div class=\"row\" style=\"font-size: x-small;color:red\">";
  				result+= checkString(obj.merchantName);
  				result+="</div>";
  			}

  			result+="</td>";
  			
  			result+="<td width='15%' style='text-align: left;'>";
  			
  			result+=checkString(task.issue);
  			if(issueType == 'customer'){
  				result += "<i class='fa fa-flag' aria-hidden='true'  style='margin-left: 5px;color: #ff8b7d;'></i>";
  			}
  			
  			result+="</td>";
  			result+="<td width='10%' style='text-align: center;'>";
  			result+=printDate(cDate);
  			result+="</td>";
  			result+="<td width='10%' style='text-align: center;'>";
  			if((eDate instanceof Date && !isNaN(eDate))){
  				result+=printDate(eDate,false);
			}else{
				result+="-";
			}
  			result+="</td>";
  			result+="<td width='10%' style='text-align: center;'>";
  			result+=checkString(task.process);
  			result+="</td>";
  			result+="<td width='5%' id="+1+" style=\"text-align: center;\">";
  			result+="<button type=\"button\" class=\"btn\"";
  			result+="style=\"width: 100%; background-color:"+checkString(statusColor)+"\">"+checkString(task.status)+"</button>";
  			result+="</td>";
  			result+="<td width='8%' style='text-align: center;'>";
  			result+=checkString(task.owner)+"</td>"
  			result+="<td width='8%' id=\"task.id\" style='text-align: center;'>";
  			result+=checkString(task.updatedBy);
  			result+="</td>";
  			result+="<td width='5%' id="+1+" style='text-align: center;'>";
  			
  			var detail = JSON.parse(task.result);
  			
  			if(detail==null||detail.message==null&&detail.fileName==null){
  					result+="-";
  			}
  			else if(detail.message!=null&&detail.fileName==null){
  						result+="<a class='fa fa-commenting fa-lg' data-toggle='tooltip' data-placement='right' style=\"text-decoration: none;\" title="+encodeHTML(detail.message)+"></a>"; 			
  			}else if(detail.fileName!=null){
						result += "<a href=\""+contextPath+"?cmd=exportCSV&fileName="+detail.fileName+"\">";
						result += "<button type=\"button\" class=\"btn\" style='background-color: #fdfdfd; float: right;'>";
						result += "<i class=\"fa fa-download fa-lg\" aria-hidden=\"true\" style=\"color: royalblue;\"></i>";
						result += "</button>";
						result += "</a>";
  			}

  			result+="</td>";
  			result+="<td width='5%' style='text-align: center;'>";
  			result+="<a class=\"idRefTask\" data-element-id="+task.id+" name=\"idRefTask\">";
  			result+="<button class=\"btn btn-info\">→</button></a></td>";
  			result+="</tr>";
  		});
  		$("#taskBoxCotainer").html(result);
	}
	
	function genPaging(data){
		
		var pagingBean = data.model.pagingBean;
		if(!pagingBean){
			$("#pagingDiv").html('');
			return false;
		}
		var pagingList = pagingBean.pagingList;
		if(!pagingList){
			$("#pagingDiv").html('');
			return false;
		}

		var pagingGen = "";
		pagingGen +="<div class=\"dataTables_paginate paging_bootstrap\">";
		pagingGen +="<ul class=\"pagination\">";
		if(pagingBean.currentPage == 1){
			pagingGen +="<li class=\"prev disabled\"><a href=\"#\" class=\"pagiation\">← Previous</a></li>";
		}else{
			pagingGen +="<li class=\"prev\"><a href=\"#\" data-element-id=\""+(pagingBean.currentPage-1)+"\" name=\"pagination\">← Previous</a></li>";
		}
		pagingList.forEach(function(pageNo, index, array) {
			if(pageNo==pagingBean.currentPage){
				pagingGen +="<li><a href=\"#\" style=\"background-color: #ccc;\" class=\"pagination\">"+pageNo+"</a></li>";
			}else if(pageNo=='...'){
				pagingGen +="<li><a href=\"#\" class=\"pagiation\">...</a></li>";
			}else{
				pagingGen +="<li><a href=\"#\" data-element-id=\""+pageNo+"\" name=\"pagination\">"+pageNo+"</a></li>";
			}
		});
		if(pagingBean.currentPage == pagingBean.totalPage){
			pagingGen +="<li class=\"next disabled\"><a href=\"#\">Next → </a></li>";
		}else{
			pagingGen +="<li class=\"next\"><a href=\"#\" name=\"pagination\" data-element-id=\""+(pagingBean.currentPage+1)+"\">Next → </a></li>";
		}
		pagingGen +="</ui>";
		pagingGen +="</div>";  	
		$("#currentPage").val(pagingBean.currentPage);
		$("#pagingDiv").html(pagingGen);

	}
 
	function addServiceUrl(data){
		var serviceUrls = data.model.serviceUrls;
		for (var index in serviceUrls)  {
			$("#service-url-"+index).val(serviceUrls[index]);
		};
			
	}
	
	function sendAddTaskData(){
		if($('.errorMsg').size() > 0){
			$('#submitbtn').prop('disabled', true);
			return;
		}
		var addTaskManagerUrl = genServiceUrl("taskmanager");
		var fd = new FormData($('#addTaskForm')[0]);    
		$( "input[name='productFiles']" ).val("");
		$.ajax({
		    url:addTaskManagerUrl,
		    type:'POST',
		    method: 'POST',
	        processData: false,  
	        contentType: false,
		    enctype: 'multipart/form-data; charset=utf-8',
			xhrFields: {
			      withCredentials: true
			},
		    data:fd,
		    beforeSend:function(data){
		    		$('#addForm').modal('hide');
		    		showLoading();
		    },
		    success:function(data){
		    	getTask();
		    }
		 });

	}
	
	function genServiceUrl(path){
		var currentCountry = $("#currentCountry").val();
		var url = $("#service-url-"+currentCountry).val();
		url = url+"/admin/"+path;
		return url;
	}
	
	function showLoading(){
		   $('#taskBoxCotainer').html(	"	<tr>\r\n" + 
					"<td >\r\n" + 
					"</td>\r\n" + 
					"	<td align=\"center\" colspan=\"12\" style=\"padding-top:40px\">\r\n" + 
					"		<div id=\"ajaxLoading\">\r\n" + 
					"<i class=\"fa fa-spinner fa-spin iconSpin fa-5x\"></i>"+
					"		</div>\r\n" + 
					"	</td>\r\n" + 
					"<td>\r\n" + 
					"</td>\r\n" + 
					"</tr>");
	}
	
	function encodeHTML(input){
		var str = input;
		var p = document.createElement("p");
		p.textContent = str;
		var converted = p.innerHTML;
		
		converted = converted.replace(new RegExp("&lt;br /&gt;",'g'), "</br>");
		return converted;
	}
	
	function activeMerchant(){
		var merchantId = $('#detailId').val();
		$.ajax({
		    url:'updateMerchantActive',
		    type:'POST',
		    method: 'POST',
		    data:'&merchantId='+merchantId,
		    success:function(data){
		    	if(data.success){
	  				$('#statusactive').css('background','#48ff5f');
	  				$('#statusactive').attr('disabled','true');
	  				$('#statusactive').html('Actived');
		    	}else{
		    		console.log("Error");
		    	}
		    }
		 });
		
	}
 
 	