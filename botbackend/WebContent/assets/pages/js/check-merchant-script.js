$(document).ready(function() {
		
		openTab("Config");
		
		$("#showCountSummary").css("display", "none");
	});


	var action = {"Approve":1, "Reject":2, "Retest":3}
	/*config tab */
	var listConfig = "'maxDepth','parserClass','deleteLimit','productUpdateLimit','forceUpdateKeyword','forceUpdateCategory','enableUpdateDesc','enableCookies','enableUpdateBasePrice','merchantName','currencyRate'";
	var configDisplay = {
			'maxDepth'				: 'Max depth of product page',
			'parserClass'			: 'Template Parser class name',
			'deleteLimit' 			: 'Max number of deleted product',
			'productUpdateLimit' 	: 'Limit count of error updated product',
			'forceUpdateKeyword' 	: 'Enable update product keyword',
			'forceUpdateCategory' 	: 'Enable update product category',
			'enableUpdateDesc' 		: 'Enable update product description',
			'enableCookies' 		: 'Enable to use cookie',
			'enableUpdateBasePrice'	: 'Enable update product baseprice',
			'merchantName'			: 'Merchant name',
			'currencyRate'			: 'Price rate (for multiply)'
    };	
	var config = {
			'Max depth of product page' 			: 'maxDepth',
			'Template Parser class name' 			: 'parserClass',
			'Max number of deleted product'		 	: 'deleteLimit',
			'Limit count of error updated product' 	: 'productUpdateLimit',
			'Enable update product keyword'			: 'forceUpdateKeyword',
			'Enable update product category' 		: 'forceUpdateCategory',
			'Enable update product description' 	: 'enableUpdateDesc',
			'Enable to use cookie' 					: 'enableCookies',
			'Enable update product baseprice' 		: 'enableUpdateBasePrice',
			'Merchant name'							: 'merchantName',
			'Price rate (for multiply)'				: 'currencyRate'
    };	
	var parserDisplay = {
			'com.ha.bot.parser.filter.TemplateTaradHaHTMLParser':'Tarad (normal)',
			'com.ha.bot.parser.filter.TemplateTaradIncludeIDHaHTMLParser':'Tarad (Include real product Id in name)',
			'com.ha.bot.parser.filter.TemplateTaradNoPriceHaHTMLParser':'Tarad (If no price, will assign contact price automatically)',
			'com.ha.bot.parser.filter.TemplateTaradIncludeIDNoPriceHaHTMLParser':'Tarad (Include real product Id in name , If no price, will assign contact price automatically)',
			'com.ha.bot.parser.filter.WeLoveShoppingHaHTMLParser':'WeLoveShopping (Normal)',
			'com.ha.bot.parser.filter.WeLoveShoppingPlatinumFilter':'WeLoveShopping (Platinum)',
			'com.ha.bot.parser.filter.TemplateWeLoveShoppingNoCatIncludeIdHaHTMLParser':'WeLoveShopping (Not map category)',
			'com.ha.bot.parser.filter.WeLoveShoppingPlatinumNoPriceFilter':'WeLoveShopping (Platinum, If no price, will assign contact price automatically)',
			'com.ha.bot.parser.filter.WeLoveShoppingPlatinumNoPriceIncludeIdFilter':'WeLoveShopping (Include real product Id in name,  , If no price, will assign contact price automatically) ',
			'com.ha.bot.parser.filter.KaideeMarketplaceHaHTMLParser':'KaideeMarketplace (normal)',
			'com.ha.bot.parser.filter.ReadyPlanetHaHTMLParser':'ReadyPlanet (normal)',
			'com.ha.bot.parser.filter.TemplateLazadaHaHTMLParser':'Lazada (normal)'

    };
	var parserConfig = {
			'Tarad (normal)':'com.ha.bot.parser.filter.TemplateTaradHaHTMLParser',
			'Tarad (Include real product Id in name)':'com.ha.bot.parser.filter.TemplateTaradIncludeIDHaHTMLParser',
			'Tarad (If no price, will assign contact price automatically)':'com.ha.bot.parser.filter.TemplateTaradNoPriceHaHTMLParser',
			'Tarad (Include real product Id in name , If no price, will assign contact price automatically)':'com.ha.bot.parser.filter.TemplateTaradIncludeIDNoPriceHaHTMLParser',
			'WeLoveShopping (Normal)':'com.ha.bot.parser.filter.WeLoveShoppingHaHTMLParser',
			'WeLoveShopping (Platinum)':'com.ha.bot.parser.filter.WeLoveShoppingPlatinumFilter',
			'WeLoveShopping (Not map category)':'com.ha.bot.parser.filter.TemplateWeLoveShoppingNoCatIncludeIdHaHTMLParser',
			'WeLoveShopping (Platinum, If no price, will assign contact price automatically)':'com.ha.bot.parser.filter.WeLoveShoppingPlatinumNoPriceFilter',
			'WeLoveShopping (Include real product Id in name,  , If no price, will assign contact price automatically) ':'com.ha.bot.parser.filter.WeLoveShoppingPlatinumNoPriceIncludeIdFilter',
			'KaideeMarketplace (normal)':'com.ha.bot.parser.filter.KaideeMarketplaceHaHTMLParser',
			'ReadyPlanet (normal)':'com.ha.bot.parser.filter.ReadyPlanetHaHTMLParser',
			'Lazada (normal)':'com.ha.bot.parser.filter.TemplateLazadaHaHTMLParser'
    };
	var validateConfigValue = {
			'maxDepth'				: 'number',
			'parserClass'			: 'optionText',
			'deleteLimit' 			: 'number',
			'productUpdateLimit' 	: 'number',
			'forceUpdateKeyword' 	: 'option',
			'forceUpdateCategory' 	: 'option',
			'enableUpdateDesc' 		: 'option',
			'enableCookies' 		: 'option',
			'enableUpdateBasePrice' : 'option',
			'merchantName' 			: 'text',
			'currencyRate' 			: 'number'
    };
	var valueOptionList = {
			'true' : 'true',
			'false' : 'false'
	}
	
	function setShowOption(value){
		var htmlResult = "";
		var showType  = validateConfigValue[value];
		if(showType == 'option'){
			htmlResult = "<a class='rowValueOption'></a>";
		} else if(showType =='optionText'){
			htmlResult = "<a class='rowValueOptionText'></a>";
		} else {
			htmlResult = "<a class='rowValue'></a>";
		}
		return htmlResult;
		
	}
	
	function generateRowFix(rowField, rowVal){
		 var rowData = "";
		 
			var fieldDisplay = configDisplay[rowField];
			var typeVal = validateConfigValue[rowField];
			if(fieldDisplay != undefined && typeVal != undefined){  
				rowData += "<tr class='rowConfig'>";
				rowData += "	<td width='30%' height='45px' style='text-align:center;'>"+fieldDisplay+"</td>";
				
				if(rowVal != null && rowVal != "1" && rowVal != "1.0" ){
					rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+rowVal+"</a></td>";
				}else{
					rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+rowVal+"</td>";
				}
		  		rowData += "</tr>";
			}
			return rowData;
	 }
	
	function generateRowConfig(rowField, rowVal){
		var rowData = "";
		var fieldDisplay = configDisplay[rowField];
		var typeVal = validateConfigValue[rowField];
		if(fieldDisplay != undefined && typeVal != undefined){  
			rowData += "<tr class='rowConfig'>";
			rowData += "	<td width='30%'  style='text-align:center;'>"+fieldDisplay+"</td>";
			
			if(typeVal == 'option'){
	  			rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+rowVal+"</td>";
	  		}else if(typeVal == 'optionText'){
	  			var rowValOptionText  =  parserDisplay[rowVal];
	  			if(rowValOptionText == undefined){		
	  				rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+rowVal+"</td>";
	  			}else{
	  	  			rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+parserDisplay[rowVal]+"</td>";
	  			}
	
	  		}else{
	  			rowData += "	<td width='40%'  style='text-align:left;' class='rows'>"+rowVal+"</td>";
	  		}
			
	  		rowData += "</tr>";
		}
		return rowData;
	}
	function assignConfigTable(json , configData){
		
		var merchantId = json.merchantId;
		var merchantName = json.merchantName;
  		
  		if(configData != null && configData.length > 0){
	  		var rateField = false;
				for(var i=0; i<configData.length; i++){
				var jObject = configData[i];
					var field = (jObject.field ? jObject.field : '');
					var value = (jObject.value ? jObject.value : '');
					if(field == 'currencyRate'){
						var rowData = generateRowFix(field, value);
						$('#urlConfigTable > tbody').prepend(rowData);
						rateField = true;
					}else{
						var rowData = generateRowConfig(field, value);
						$('#urlConfigTable > tbody').append(rowData);
					}
				}
				if(!rateField){
					var rowData = generateRowFix('currencyRate','1');
					$('#urlConfigTable > tbody').prepend(rowData);
				}
  		}else{
			var rowData = generateRowFix('currencyRate','1');
				$('#urlConfigTable > tbody').prepend(rowData);
  		}
	}
	
	function assignPatternTable(json, patternData){
		var merchantId = json.merchantId;
		if(patternData != null && patternData.length > 0){
			for(var i=0; i<patternData.length; i++){
			var jObject = patternData[i];
				var condition = (jObject.condition ? jObject.condition : '-');
				var group = (jObject.group ? jObject.group : '0');
				var value = (jObject.value ? jObject.value : '-');
				var action = (jObject.action ? jObject.action : 'STARTURL');
				var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
				var keyword = (jObject.keyword ? jObject.keyword : '-');
				
				var rowData = generateRowPattern(condition, group, value, action, categoryId, keyword);
				$('#urlPatternTable > tbody').append(rowData);
			}
  		}else {
			var rowData = generateRowPattern('', '0', '', 'STARTURL', '0', '');
				$('#urlPatternTable > tbody').append(rowData);
  		}
	}	
	
	function generateRowPattern(rowCon, rowGro, rowVal, rowAct, rowCat, rowKey){
		var rowData = "";
  		rowData += "<tr class='rowPattern'>";
  		rowData += "	<td width='7%'  style='text-align:center;'>"+rowCon+"</td>";
  		rowData += "	<td width='5%'  style='text-align:center;'>"+rowGro+"</td>";
  		rowData += "	<td width='33%' style='text-align:left; max-width: 200px; word-wrap: break-word;'>"+rowVal+"</td>";
  		rowData += "	<td width='8%'  style='text-align:center;'>"+rowAct+"</a></td>";
  		rowData += "	<td width='5%'  style='text-align:center;'>"+rowCat+"</a></td>";
  		rowData += "	<td width='30%' style='text-align:left; max-width: 200px; word-wrap: break-word;'>"+rowKey+"</td>";
  		rowData += "</tr>";
  		
		return rowData;
	}
	
	function assignWorkloadTable(json, workloadData,start){
		var merchantId = json.merchantId;
		if(workloadData != null && workloadData.length > 0){
			for(var i=0; i<workloadData.length; i++){
				var jObject = workloadData[i];
				var url = (jObject.url ? jObject.url : '-');
				var depth = (jObject.depth ? jObject.depth : '0');
				var status = (jObject.status ? jObject.status : '-');
				var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
				var keyword = (jObject.keyword ? jObject.keyword : '-');
				
				var rowData = generateRowWorkload(url, depth, status, categoryId, keyword);
				if(start == 0){
					$('#workloadTable > tbody').append(rowData);					
				}else{
					$('#workloadTable > tbody > tr:last').after(rowData);										
				}
			}
		}else {
			var rowData = generateRowWorkload('', '0', '', 'STARTURL', '0', '');
			$('#urlPatternTable > tbody').append(rowData);
		}
	}
	
	function assignProductUrlTable(json, productUrl,start){
		var merchantId = json.merchantId;
		if(productUrl != null && productUrl.length > 0){
			for(var i=0; i<productUrl.length; i++){
				var jObject = productUrl[i];
				var url = (jObject.url ? jObject.url : '-');
				var state = (jObject.state ? jObject.state : '-');
				var status = (jObject.status ? jObject.status : '-');
				var categoryId = (jObject.categoryId ? jObject.categoryId : '0');
				var keyword = (jObject.keyword ? jObject.keyword : '-');
				
				var rowData = generateRowProductUrl(url, state, status, categoryId, keyword);
				if(start == 0){
					$('#productUrlTable > tbody').append(rowData);					
				}else{
					$('#productUrlTable > tbody > tr:last').after(rowData);										
				}
			}
		}else {
			var rowData = generateRowProductUrl('-', '-', '-', '-', '-', '-');
			$('#urlPatternTable > tbody').append(rowData);
		}
	}
	
	function assignProductDataTable(json, productData,start){
		var merchantId = json.merchantId;
		if(productData != null && productData.length > 0){
			for(var i=0; i<productData.length; i++){
				var jObject = productData[i];
				var name = (jObject.name ? jObject.name : '-');
				var price = (jObject.price ? jObject.price : '0');
				var basePrice = (jObject.basePrice ? jObject.basePrice : '0');
				var url = (jObject.url ? jObject.url : '-');
				var description = (jObject.description ? jObject.description : '-');
				var pictureUrl = (jObject.pictureUrl ? jObject.pictureUrl : '-');
				var realProductId = (jObject.realProductId ? jObject.realProductId : '-');
				
				var rowData = generateRowProductData(name, price, basePrice, url, description, pictureUrl,realProductId);
				if(start == 0){
					$('#productDataTable > tbody').append(rowData);					
				}else{
					$('#productDataTable > tbody > tr:last').after(rowData);										
				}
			}
		}else {
			var rowData = generateRowProductData('-', '-', '-', '-', '-');
			$('#urlPatternTable > tbody').append(rowData);
		}
	}
	
	function generateRowWorkload(rowUrl, rowDept, rowStatus, rowCat, rowKey){
		var rowData = "";
		rowData += "<tr class='rowWorkload'>";
		rowData += "	<td width='55%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank' href='"+rowUrl+"' class='rowUrl'>"+rowUrl+"</a></td>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+rowDept+"</td>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+rowStatus+"</td>";
		rowData += "	<td width='8%'  style='text-align:center;'>"+rowCat+"</td>";
		rowData += "	<td width='22%' style='text-align:center;'>"+rowKey+"</td>";
		rowData += "</tr>";
		
		return rowData;
	}	
	
	function generateRowProductUrl(rowUrl, rowState, rowStatus, rowCat, rowKey){
		var rowData = "";
		rowData += setProductUrlColor(rowState);
		rowData += "	<td width='55%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank' href='"+rowUrl+"' class='rowUrl'>"+rowUrl+"</td>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+rowState+"</td>";
		rowData += "	<td width='5%'  style='text-align:center;'>"+rowStatus+"</td>";
		rowData += "	<td width='8%'  style='text-align:center;'>"+rowCat+"</td>";
		rowData += "	<td width='22%' style='text-align:left; max-width: 200px; word-wrap: break-word;'>"+rowKey+"</td>";
		rowData += "</tr>";
		
		return rowData;
	}
	//	N, S ,F, D ,L,X
	function setProductUrlColor(rowAct) {
		var rowData="";
		if(rowAct != null && rowAct=='N'){
			rowData += "<tr class='rowProductUrl' bgcolor='#F7F5E8'>";
		}else if(rowAct != null && rowAct == 'S'){
			rowData += "<tr class='rowProductUrl' bgcolor='#85F0D7'>";
		}else if(rowAct != null && rowAct == 'F'){
			rowData += "<tr class='rowProductUrl' bgcolor='#EBB5AF'>";
		}else if(rowAct != null && rowAct == 'D'){
			rowData += "<tr class='rowProductUrl' bgcolor='#F0E09C'>";
		}else if(rowAct != null && rowAct == 'L'){
			rowData += "<tr class='rowProductUrl' bgcolor='#D9BAEC'>";
		}else{
			rowData += "<tr class='rowProductUrl'>";
		}
		return rowData;
	}

	function generateRowProductData(rowName, rowPrice, rowBaseprice,rowUrl,rowDescription,rowPictureUrl,rowRealProductId){
		var rowData = "";
		rowData += "<tr class='rowProductData'>";
		rowData += "	<td width='28%' style='text-align:left;'>"+rowName+"</td>";
		rowData += "	<td width='10%' style='text-align:center;'>"+rowPrice+"</td>";
		rowData += "	<td width='10%' style='text-align:center;'>"+rowBaseprice+"</td>";
		rowData += "	<td width='12%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank'  href='"+rowUrl+"' class='rowUrl'>"+rowUrl+"</a></td>";
		rowData += "	<td width='12%' style='text-align:center;'>"+rowDescription+"</td>";
		rowData += "	<td width='12%' style='text-align:left; max-width: 200px; word-wrap: break-word;'><a target=_blank' href='"+rowPictureUrl+"' class='rowPictureUrl'>"+rowPictureUrl+"</a></td>";
		rowData += "	<td width='5%' style='text-align:center;'>"+rowRealProductId+"</td>";
		rowData += "</tr>";
		
		return rowData;
	}	
	
	function getConfigByMerchantId(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		$('#urlConfigTable > tbody').html('');
		$.ajax({
			type: "POST",
			url: 'checkmerchantconfigajax',
			data: param,
			cache: false,
			async: true,
		  	success: function(json) {
			  	if(json.header == 'success') {
			  		var configData = json.configData;
			  		assignConfigTable(json, configData);
			  		
			  	}else{
 				  	$("#urlConfigContent").css("display", "none");
			  		modalAlert("Do not have this merchant in the system", false);
			  	}
		  	},
		  	error: function (textStatus, errorThrown) {
			  	$("#urlConfigContent").css("display", "none");
			  	modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
		  	}
		});
		
	}
	
	function getPatternByMerchantId(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		$('#urlPatternTable > tbody').html('');
		$.ajax({
			type: "POST",
			url: 'checkmerchantpatternajax',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
			  		var patternData = json.patternData;
			  		assignPatternTable(json, patternData)
					
				}else{
					$("#urlPatternContent").css("display", "none");
					modalAlert("Do not have this merchant in the system", false);
				}
			},
			error: function (textStatus, errorThrown) {
				$("#urlPatternContent").css("display", "none");
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});
		
	}
	
	function getWorkloadByMerchantId(){
		var merchantId = $('#merchantId').val();
		var start = $('#startWorkload').val(); 
		if(start == ''){
			start = 0;
		}
		$('#getWorkloadByMerchantId').button('loading');
		var param = "merchantId="+ merchantId+"&start="+start;
		$.ajax({
			type: "POST",
			url: 'checkworkloadajax',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
			  		var listData = json.listData;
			  		if(listData != null && listData.length > 0){
			  			assignWorkloadTable(json, listData,start);												
						var nextStart = json.start;
						$('#startWorkload').val(nextStart);
					}else{
						$("#btnLoadmoreWorkload").remove();
					}
					
				}else{
					$("#urlWorkloadContent").css("display", "none");
					modalAlert("Do not have this merchant in the system", false);
				}
				$('#getWorkloadByMerchantId').button('reset');
			},
			error: function (textStatus, errorThrown) {
				$("#urlWorkloadContent").css("display", "none");
				$('#getWorkloadByMerchantId').button('reset');
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});
	}
	
	function getProductUrlByMerchantId(){
		var merchantId = $('#merchantId').val();
		var start = $('#startProductUrl').val(); 
		if(start == ''){
			start = 0;
		}
		$('#getProductUrlByMerchantId').button('loading');
		var param = "merchantId="+ merchantId+"&start="+start;
		$.ajax({
			type: "POST",
			url: 'checkproducturlajax',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
			  		var listData = json.listData;
			  		if(listData != null && listData.length > 0){
			  			assignProductUrlTable(json, listData,start);												
						var nextStart = json.start;
						$('#startProductUrl').val(nextStart);
					}else{
						$("#btnLoadmoreProductUrl").remove();
					}
					
				}else{
					$("#urlProductUrlContent").css("display", "none");
					modalAlert("Do not have this merchant in the system", false);
				}
				$('#getProductUrlByMerchantId').button('reset');
			},
			error: function (textStatus, errorThrown) {
				$("#urlProductUrlContent").css("display", "none");
				$('#getProductUrlByMerchantId').button('reset');
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});
	}
	
	function getProductDataByMerchantId(){
		var merchantId = $('#merchantId').val();
		var start = $('#startProductData').val(); 
		if(start == ''){
			start = 0;
		}
		$('#getProductDataByMerchantId').button('loading');
		var param = "merchantId="+ merchantId+"&start="+start;
		$.ajax({
			type: "POST",
			url: 'checkproductdataajax',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
			  		var listData = json.listData;
			  		if(listData != null && listData.length > 0){
			  			assignProductDataTable(json, listData,start);												
						var nextStart = json.start;
						$('#startProductData').val(nextStart);
					}else{
						$("#btnLoadmoreProductData").remove();
					}
				}else{
					$("#urlProductDataContent").css("display", "none");
					modalAlert("Do not have this merchant in the system", false);
				}
				$('#getProductDataByMerchantId').button('reset');
			},
			error: function (textStatus, errorThrown) {
				$("#urlProductDataContent").css("display", "none");
				$('#getProductDataByMerchantId').button('reset');
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});
	}
	
	function getCountProductUrl(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;

		$.ajax({
			type: "POST",
			url: 'countproducturl',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
					var countStateNew = (json.countStateNew ? json.countStateNew :'');
					var countStateLanguageError = (json.countStateLanguageError ? json.countStateLanguageError :'');
					var countStateSuccess = (json.countStateSuccess ? json.countStateSuccess :'');
					var countStateDuplicate = (json.countStateDuplicate ? json.countStateDuplicate :'');
					var countStateFailure = (json.countStateFailure ? json.countStateFailure :'');
					var countStateExpire = (json.countStateExpire ? json.countStateExpire :'');
					$('#COUNT_SUCCESS').text(countStateSuccess);
					$('#COUNT_FAILURE').text(countStateFailure);
					$('#COUNT_DUPLICATE').text(countStateDuplicate);
					$('#COUNT_LANGUAGEERROR').text(countStateLanguageError);
					$('#COUNT_EXPIRE').text(countStateExpire);
					$('#COUNT_NEW').text(countStateNew);
					$('#btnCountProductUrl').remove();
		
				}else if(json.header == 'error limit'){
					modalAlert('Error count over limit product url');
				}
			},
			error: function (textStatus, errorThrown) {
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});	
	}
	function getCountOnBot(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		
		$.ajax({
			type: "POST",
			url: 'countproductonbot',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
					var countproduct = json.count;
					var reportdate = json.reportdate;
					if(reportdate == ""){
						$('#COUNT_PRODUCTONBOT').text(countproduct);																			
					}else{
						$('#COUNT_PRODUCTONBOT').text(countproduct +" ("+reportdate+")");																									
					}
				}else{
					$('#COUNT_PRODUCTONBOT').text("Not found product");							
				}
				$('#btnCountOnBot').remove();
			},
			error: function (textStatus, errorThrown) {
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});	
	}
	
	function getCountWaitingDelete(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		
		$.ajax({
			type: "POST",
			url: 'countwaitingtdelete',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
					var countproduct = json.count;
					if(countproduct == 1000){
						$('#COUNT_PRODUCTWAITING_DELETE').text("Product waiting delete more than 1000");		
					}else{						
						$('#COUNT_PRODUCTWAITING_DELETE').text(countproduct);							
					}
				}else{
					$('#COUNT_PRODUCTWAITING_DELETE').text("Not found product waiting delete");							
				}
				$('#btnCountProductWaitingDelete').remove();
			},
			error: function (textStatus, errorThrown) {
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});	
	}
	
	function getCountWaitingSync(){
		var merchantId = $('#merchantId').val();
		var param = "merchantId="+ merchantId;
		
		$.ajax({
			type: "POST",
			url: 'countwaitingtsync',
			data: param,
			cache: false,
			async: true,
			success: function(json) {
				if(json.header == 'success') {
					var countproduct = json.count;
					if(countproduct > 0){
						$('#COUNT_PRODUCTWAITING_SYNC').text("Syncing");									
					}else{
						$('#COUNT_PRODUCTWAITING_SYNC').text("Sync done");
					}
				}else{
					$('#COUNT_PRODUCTWAITING_SYNC').text("Not found product");							
				}
				$('#btnCountProductWaitingSync').remove();
			},
			error: function (textStatus, errorThrown) {
				modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
			}
		});	
	}
	
	function modalAlert(result, success){
		if(success){
			$('#modalAlertTitle').html('<h3 style="color:#64FE2E;"><i class="fa fa-check"></i>&nbsp;Success</h3>');
		}else{
			$('#modalAlertTitle').html('<h3 style="color:#DF0101;"><i class="fa fa-times">&nbsp;Error</h3>');
		}
		$('#modalAlertData').html('<span style="color:black;font-size:17px;">'+result+'</span>');
		$('#modalAlert').modal('show');
	}

	function toggleShow(btn){
		var buttonText = $(btn).text();
		var bodyDiv = $(btn).closest(".portlet-title").next();
		if(buttonText == "HIDE"){
			$(bodyDiv).css("display","none");
			$(btn).text("SHOW");
		}else if(buttonText == "SHOW"){
			$(bodyDiv).css("display","block");
			$(btn).text("HIDE");
		}
	}
	function openTab(value){
		var tab = value;
		if(tab == 'Config'){
			$('#cTab').addClass('active');
			$('#pTab').removeClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').removeClass('active');
			$('.configTab').show();
			$('.patternTab').hide();
			$('.workloadTab').hide();
			$('.productUrlTab').hide();
			$('.productDataTab').hide();
			if($('#urlConfigTable > tbody > tr').length == 0){
				getConfigByMerchantId();				
			}
		}else if(tab ==  'Pattern'){
			$('#cTab').removeClass('active');
			$('#pTab').addClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').removeClass('active');
			$('.configTab').hide();
			$('.patternTab').show();
			$('.workloadTab').hide();
			$('.productUrlTab').hide();
			$('.productDataTab').hide();
			if($('#urlPatternTable > tbody > tr').length == 0){
				getPatternByMerchantId();				
			}
		}else if(tab == 'Workload'){
			$('#cTab').removeClass('active');
			$('#pTab').removeClass('active');
			$('#wTab').addClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').removeClass('active');
			$('.configTab').hide();
			$('.patternTab').hide();
			$('.workloadTab').show();
			$('.productUrlTab').hide();
			$('.productDataTab').hide();
			if($('#workloadTable > tbody > tr').length == 0){
				getWorkloadByMerchantId();				
			}
	
		}
		else if(tab == 'productUrl'){
			$('#cTab').removeClass('active');
			$('#pTab').removeClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').addClass('active');
			$('#dTab').removeClass('active');
			$('.configTab').hide();
			$('.patternTab').hide();
			$('.workloadTab').hide();
			$('.productUrlTab').show();
			$('.productDataTab').hide();
			if($('#productUrlTable > tbody > tr').length == 0){
				getProductUrlByMerchantId();				
			}
		}
		else if(tab == 'productData'){
			$('#cTab').removeClass('active');
			$('#pTab').removeClass('active');
			$('#wTab').removeClass('active');
			$('#uTab').removeClass('active');
			$('#dTab').addClass('active');
			$('.configTab').hide();
			$('.patternTab').hide();
			$('.workloadTab').hide();
			$('.productUrlTab').hide();
			$('.productDataTab').show();
			if($('#productDataTable > tbody > tr').length == 0){
				getProductDataByMerchantId();				
			}
		}
	}