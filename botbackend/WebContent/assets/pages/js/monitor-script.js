$(document).ready(function () {

	getDataByMerchantId();
	
	$('#resultType').on('change', function() {
		getDataByMerchantId();
	});
	
	$('#searchBtn').click(function() {
		getDataByMerchantId();
	});
	$('#searchParam').keypress(function (e) {
		 var key = e.which;
		 if(key == 13) 
		  {
			 getDataByMerchantId();
		  }
	});
	
	$('.confirmBtn').click(function(){
		saveMonitorFix();
	});
	
	$(".pagination").on("click",".pageAction", function(){
		var page  = $(this).attr("data-id");
		$('#pageCurrent').val(page);
		getDataByMerchantId();
	});

	
	$("input:checkbox").on("click", function() {
    	if($(this).is(':checked')){
			$(this).attr('checked', 'checked');
			$(this).val(1);
		}else{
			$(this).removeAttr('checked');
			$(this).val(0);
		}
	 });
 });


function getDataByMerchantId(){
	var merchantId = $('#searchParam').val().trim();
	var analyzeResult = $('#resultType').val();
	var chkHistory = $('#chkHistory').val();
	var pageCur = $('#pageCurrent').val();
	
	if(merchantId == ''){ merchantId = 0;}
	var param = "merchantId="+ merchantId+"&analyzeResult="+analyzeResult+"&chkHistory="+chkHistory+"&page="+pageCur;
	$('#monitorTable > tbody').html('');
	$('.pagination').html('');
	
	$.ajax({
		type: "POST",
		url: 'monitorAjax',
		data: param,
		cache: false,
		async: true,
	  	success: function(json) {
		  	if(json.header == 'success') {
		  		$("#monitorTable").show();
		  		var data = json.result;
		  		if(data != null && data.length > 0){
			  		var data = json.result;
			  		var totalpage = json.pageTotal;
			  		var curpage = json.pageCurrent;
			  		var paglist = json.pageList;
			  		var totalList = json.totalList;
 					for(var i=0; i<data.length; i++){
						var jObject = data[i];
						var id = (jObject.id ? jObject.id : '');
 						var mId = (jObject.mId ? jObject.mId : '');
 						var mName = (jObject.mName ? jObject.mName : '');
 						var mServer = (jObject.mServer ? jObject.mServer : '');
 						var analyzeresult = (jObject.analyzeResult ? jObject.analyzeResult : '');
 						var updateDate = (jObject.updateDate ? jObject.updateDate : '');
 						var status = (jObject.status ? jObject.status : '');
 						var rowData = generateRow(id, mId, mName, analyzeresult, updateDate, mServer, status);
 						$('#monitorTable > tbody').append(rowData);	
 					}	
 					var pagination  = getPaging(curpage, totalpage, paglist);
 					$('.pagination').append(pagination);
 					totalList = totalList.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
 					$(".txtResult > span").html('<h4 class="txtResult"><span >All Monitor result ' +totalList+ ' record</h4>')
		  		}else{
			  		$("#monitorTable").hide();	
 				  	$(".txtResult > span").html('<h4 class="txtResult"><span style="color:red">No Monitor Result record</h4>')
			  	}
		  	}
	  	},
	  	error: function (textStatus, errorThrown) {
		  	$("#monitorTable").hide();	
		  	modalAlert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown, false);
	  	}
	});
	
}

function generateRow(rowId, rowMerchantId, rowMerchantName, rowAnalyzeResult, rowUpdateDate, rowServer, rowStatus){
	var rowData = "";
		rowData += "<tr class='rowData' id='"+rowId+"'>";
		rowData += "	<td style='text-align:center;' class='mId'>"+rowMerchantId+"</td>";
		rowData += "	<td style='text-align:center;' class='mName'>"+rowMerchantName+"</td>";
		rowData += "	<td style='text-align:center;' class='mServer'>"+rowServer+"</td>";
		rowData += "	<td style='text-align:center;' class='mAnalyze'>"+rowAnalyzeResult+"</td>";
		rowData += "	<td style='text-align:center;' class='mDate'>"+rowUpdateDate+"</td>";
		if(rowStatus == '1' && (rowAnalyzeResult == 'Parser' || rowAnalyzeResult == 'Parser_Delete_Limit')){
			rowData += "	<td style='text-align:center;'><button type='button' class='btn btn-primary btnFix' onclick='monitorFix(\""+rowMerchantId+"||"+rowId+"||"+rowServer+"||"+rowAnalyzeResult+"\")'  id='load"+rowId+"' data-loading-text='<i class=\"fa fa-spinner fa-spin \"></i> Loading'>Recover</button></td>";
		}else{
			rowData += "	<td style='text-align:center;' class='mDate'></td>";
		}
		
		rowData += "</tr>";
	return rowData;
}

function getPrevios(current){
	var pPage = current -1;
	return pPage;
}

function getNext(current){
	var nPage = current +1;
	return nPage;
}

function modalAlert(result){
	$('#modalAlertData').html('<span style="color:black;font-size:15px;">'+result+'</span>');
	$('#modalAlert').modal('show');
}


function getPaging(current, total, pagelist){
	var pageData  = "";
	if(current == 1){
		pageData +=	"<li class='prev disabled'><a>← Previous</a></li>";
	}
	else{
		pageData += "<li class='prev pageAction' data-id='"+getPrevios(current)+"'><a>← Previous</a></li>";		
	}
	if(pagelist.length > 0){
		for(var pageNo = 0 ; pageNo < pagelist.length;pageNo++){
			if(pagelist[pageNo] == current){
				pageData += "<li class='pageAction' data-id='"+pagelist[pageNo]+"'><a style='background-color: #ccc;'>"+pagelist[pageNo]+"</a></li>";
			}
			else if(pagelist[pageNo] == '...'){
				pageData +="<li><a>...</a></li>";
			}
			else{
				pageData += "<li class='pageAction' data-id='"+pagelist[pageNo]+"'><a>"+pagelist[pageNo]+"</a></li>";
			}
		}	
	}
	if(current == total){
		pageData +="<li class='next disabled'><a>Next →</a></li>";
	}else{
		pageData += "<li  class='next pageAction' data-id='"+getNext(current)+"'><a>Next → </a></li>";
	}
					
return pageData;
}


function monitorFix(input){

	var merchantId    = input.split("||")[0];
	var id = input.split("||")[1];
	var server = input.split("||")[2];
	var analyzeResult = input.split("||")[3];
	
	$('#load'+id).button('loading');
	var param = "merchantId="+ merchantId+"&analyzeResult="+analyzeResult;
	$.ajax({
		  type: "POST",
		  url: "monitorFix",
		  data: param,
		  cache: false,
		  async: true,
		 success: function(data) {
			  	if(data.header == 'success'){
			  		var result = "";
			  		var index = 0;
			  		var parserClass = false;
			  		var imgDefault = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooqlqGo2WlWVzqOo3MVlY2UTT3V1cNshhhQHc7sR0HAAGWJIAByAwBdor5b1X9qrwjaXktvpmg67rVvCWVr6I2lnESpwSIp5XmCHnYzIA4+YBRnGaP2tNAPI8F+IiOOl1pZA9RkTn14J2n1ReNwB9bUV8lf8ADWmgf9CV4j/8CtM/+O0f8NaaB/0JXiP/AMCtM/8AjtAH1rRXyV/w1poH/QleI/8AwK0z/wCO0f8ADWmgf9CV4j/8CtM/+O0AfWtFfJX/AA1poH/QleI//ArTP/jtH/DWmgf9CV4j/wDArTP/AI7QB9a0V8lf8NaaB/0JXiP/AMCtM/8AjtH/AA1poH/QleI//ArTP/jtAH1rRXyV/wANaaB/0JXiP/wK0z/47R/w1poH/QleI/8AwK0z/wCO0AfWtFfLel/tV+D7u8it9T0HXtEgmdEW9nW0vIEZ22/vUtZvPRFB3s6RzbVU7lXKk/S2n6haarZWuo6fcQ3ljewrcWt1buHhmhdco6MCTzzuUgNG2UcbkagC9RRRQAUUUUAFFFFABRRRQAUUUUAFfLX7VeqXdp4Q0DTbeVooNZ10xXrKSMxWdnNPEGwRlBMysych9o3AhQV+pa+Sf2tcHw/4LBxz4iuxk9gdLnBxyCeoOORkKeowQD6M8JeEdE8HaJY6No1jb2sVrbxxzzRRgXF5cKoE91dThVlnuJpAzPI5HUIipEkcSdTgegP15/nQOQCfQf0NLQA0jnAC/iOmc8+44PHB96AAc/KBzjBAyOnXBP8A+r25OP4juZ7PQNcu7Z/LubXRtTubaQKrNHcQWU8kLqrZDFXUEKQcnjkE4/NKL44/FZo4z/wmN8DsTJ+y6eNxCqCSq2owf4eWZsAEsewB+oh4PIX24689OgAJyNo5zz9aUAc/KPbgenI4z3/HOeMAE/l43xx+Kyq5/wCEzvV+ViCbewYnCPu+UWjOAmVbcABk47cfpP4Vuri+8MeHb67kM13eaFpF1dSsFDS3Fxp9vLNIwX5QXkdnOABz0FAG7geg/IUYHoPyFLRQAmB6D8hRgeg/IUtFACYHoPyFGB6D8hS0UAct4v8ACei+MNFvdG1iyt7iO6tZ4beeSNfPsriZPLiurWfaZIJopWjkDJkMUCurplG8D/ZX1S6u/B+u6ZJJ5lto2u+VZZyQkN9aRXk0aKWKrH9oaV0REjCtJISHJAP1E444JBwcEY9M55B5BGRwcngjBr5N/ZLx/YHjTAAH/CRWZAAwB/xK4enXFAH1oM4GcZ79vy6/z/wpaOlFABRRRQAUUUUAFFFFABRRRQAV8lftaf8AIA8Ff9jHdf8Apslr61r5K/a0/wCQB4K/7GO6/wDTZLQB9aL0H0H8qazYIHU9h0J4boScE8Dg4xyScchy9B9B/KvnP9on4j3ng3w/Z6Jotw8Gt+IxcBrqI7ZrHSrZP9JniOR5c1xIyQQyZPAmXYx2sgB3fjP4nfDjQUvtE8SeILVJrm3ms7zT7Mz3V6IZ4pIZo3SxV5YXKu6hQ6yI3Jxnn5jWX9kxAoWx8QhQuF2xeN8EAYJB8wA5KknBK5Y4wBhflZmd2eRmkaWSV5JJZHaWV5HO53klkLyF2OQSzsTg4IARQlAH1UZ/2TCCDZeI8EEHCeOlBB+8p2zBSGAG5SSGwOOK9n0/9of4PaXY2em2ep6xFZ2FrBZ2sT+H9dkMdvbRLDCnmSWjSPtjRV3OzEkHJJwB+eMaPK6xQxSSyu6pHHGru8ju21Y0SNXd5SRhI0RmkY4GMc/QvhT9m/xhrVrFqfiO9s/CGmsgndL/AGT6kIHG5Xkto5vs9o+35mE93kcb1RleMAH0mP2lvhPn/kLauev/ADLmtH8ttl/PH1BwC4ftK/Ccn/kLat/4Teucf+SX88Hpx1rxE/Aj4YEran4x6aL0naCJ9CEbuGwVWP8AtIk8cbROTuzznKpzHir9m/xjo9o+p+Gryw8Y6cI2lCWH7nU2gXG2S3t3drW8dlLHyoLsOxDbSxWgD6V/4aV+E/8A0FdX/wDCc1v/AOQ6T/hpX4Tj/mLar7g+HtaVvr81mFx7lgBg9MDP5yyRyRSNFLG8UkcrQzJKrxyROhYMksUiRvHKj/u5YmUMjKcMysjU0jjkFSQpywCtgdc4LEDO7Ck9CM8gNQB+p/hn4t+APF9wtlofiK0mvpP9VZXKzWN3Kep8qG8S3Mp7bY2L7gQFOOfSa/GhS8bRyK7o8TCSOVCUkhZDuWWGZSHiljOG3RlWIH7xigUV+hf7PHxIvfGPh+80PW5zca74c+zILtyWk1HS7kFbe6kOfnmgmjkiuGG0bDB8oO5iAfRTdPz/APQWr5M/ZL/5AHjT/sYbP/01Q19ZE5GcEZycHgjKE46np0PuDXyb+yX/AMgDxp/2MNn/AOmqGgD61ooooAKKKKACiiigAooooAKKKKACvkr9rT/kAeCv+xjuv/TZLX1rXyV+1p/yAPBX/Yx3X/psloA+tF6D6D+VfAf7Vbt/wnehKeVTwzEVBJx8+q3rNkAgduOmQOegavvxeg+g/lXwD+1X/wAj7ov/AGLFv/6c76gD5nPYZycZJ4yWY7iTj6+gA4GOppvIPqMdOBySFUbicDc7Lk4wqhic5yjj1/Bf5Cug8J6Quv8Aijw5ocgDw6rrWnWM6E7Qba4uY1vAD2f7MZthOQHCsVKhhQB9NfDrw74f+EvgpPix4ytftmu6hEreGNKceXNHFcR/6EIRIriG91LIuWvJI3XTrF4xjzy/meQ3mvfEb43+J4dKSeS7e5knms9ItZXtNE0m3U4uLh4VBQrbFkSS9upJ5r6SQMj+XsEfc/tPa7Jd+NNM8NRN5eneH9Hgmjt4xtiW71ETCRwoBQmOzS2hhXZtVRgo7YKea/DT4mX/AMM73Ur/AE7R9P1OXVLe1tpXv3u0ktre0kkdorVreVQonMkHmiRJEb7PbhUUq/nAHt8f7Jt49ohfxrZrfOqsFi0mR7FumcE3SzTRtuHzbAig7yGzgeVLqnxJ+AvilNKe+AULBctpzzvceH9asJTs82OJ3C2/mMpgN1Abe4sZIP3hdGCVueAb74g/Ez4sweIbK+vrdrfUodS1ZkurxNK0rRkbyzpcUTyCORHjHlW1uNrO3+tYq7yp0v7VOr6ZfeJfDelWksMt9oumaj/aZjKOYf7RnsGtrOcqvyyCOCa4MLHASeNiFLFaANP4jeHvD/xZ8FSfFnwbai18Q6fE7eJtKjAa4ngtYd9/DcRoqrNfWluBdW14qL9rso1QhpNpT5FG3kpt2nJG3lT2yCckhvvKWJO0rzwK+lv2Ytee18Z6j4ZkZm07xFo9xLJbuA0Ru9PEZjYqQF3S2k08cgKDcqIuAgK14V4t0lNB8U+I9FjAEWl63qVlAAQQLaG7l+yLnAyyWhgRyBguGICqQoAOe64PcZAx2zgHjoc4Gcg9BX05+yozDx1rqAkI/hiXcoJUHGpWOCQCMkbcL1wCSAMk18xnqe3t6V9N/sq/8j5rf/YsS/8ApxsqAPvtun5/+gtXyZ+yX/yAPGn/AGMNn/6aoa+s26fn/wCgtXyZ+yX/AMgDxp/2MNn/AOmqGgD61ooooAKKKKACiiigAooooAKKKKACvkr9rT/kAeCv+xjuv/TZLX1rXyV+1p/yAPBX/Yx3X/psloA+tF6D6D+VfAP7Vf8AyPui/wDYsW//AKc76vv5eg+g/lXwF+1Wp/4T3ROOvheFsgjIC6reA5Bz23YwD057UAfMx6/gv8hXWeAdRg0nxv4T1G5dY7a08Q6VJcSseIYmu0hkmYDkJHHK7McgcKSQAQ3JkYOM7scZxjOOmPwx6c54xglpPUe2c4xkAgHLHkcMSoHIZQeBkkA+hv2mtJubH4k/b3U/Z9c0SwntXYgKZrJ5bO6iVuhaFVtnIBc7HPyZK4+ePlwSQuMbkbadzIfmUgJ8+TiP92HG4biDnAr7Ssks/wBoT4ZW2lvdwW3xE8GBBG9ycPcyLC0MU05JaQ2urRRhbiWM7Y76AykRwlYz8gavo+p6Ff3WlatY3Gl6jayNHcW11EYpY5IgVikRjujuIwTG0Uto7oYXWRw5cLGAfUPhH4n+EPA/wVn0/QdTsrbx5PbXTPAbaV55tUurp4opZbryTbSTQ2DB7eOSYeXIih4pEdxJ8p3FxcXk093ezzXV1cyPNcXVy5kuriWVsvJIzDcZvMyWZmfkcKUwsf0v/wALP+G3/ClV8GrozDxH/ZQ08WY05CBqrNtOrnVGjVDggyrMzi4JBiMQiVGb5z0jSNU17UrTSdGs59T1O5cQwWtqheV5CdrtICQLaFATJNdTuIYY9rgzM/lxgHun7M2lXN98SG1CMbbbRNEv5rmT7yiW9aGxso2IAG5zNO+OCI4Hycc15P491GDVvG/i3UrVg9teeIdVlt2UYDwrdSRRuBknZIsYdSTyGBB7V9RXy2P7PPw0udJjuYLv4g+MopBI0DBHtd0EkD3UWVEn2PSozIY5XYiW+lkkjzGoiPxh78nPO4gfMTg59DwcHOTkYbJyaACvpv8AZV/5HzW/+xYl/wDTjZV8yV9NfsrZ/wCE81sjt4WnbvxjUrAc9uSwx34agD78bp+f/oLV8mfsl/8AIA8af9jDZ/8Apqhr6ybp+f8A6Cfc96+Tf2S/+QB40/7GGz/9NcP1oA+taKKKACiiigAooooAKKKKACiiigAr5K/a0/5AHgr/ALGO6/8ATZLX1rXyV+1p/wAgDwV/2Md1/wCmyWgD60HQfQV8k/tSeCrvUdP0nxpYRPMNER9P1kRoXeDTbmUyQX2FK5t7S5Mi3mWUqlxFIpCpID9bL0H0H8qhngiuI5IJ0SWGWN4pIZUSSKRJFZGSSOQMkiMpIaN1ZGwNykUAfjcdwUZUgj5clgyrg9C6jDADgFFcMCCHBZVBn2BHTrkMPXIxxn6+2a/QDxP+zD4J1e8mvtDvtS8MzTSGVrS08i805XfG8w2t1+8hRmIJiS4ESkny0XIB5Ifsl2mD/wAVvfEdgmk23Oe43XeFx1xuPHIOeoB8i6Fr+seGdTg1rQtQn03U7bIiuoMNtRtoeO4icrDPbzKvlvHLvBUsAFYhq+mLX45eAfG1jFp/xa8FQ3F1BGsS6vpduLuM5IG6DypotT09WYM8iw3TwfxKCdyr0R/ZKsycHxxfH3/se1JGemQ1yRj3559gdqD9kq0I+bxxenAwc6TasD1O0/6QAigEcYdRzhehoA5z7B+yuH+0nVtbCht4sidaCgYy0AzY+YwYNgsLktwNswKk0+8+OXgHwTYS6f8ACbwTFbXMsflNrGq232aNFAKh5GkkuNT1MLhZIkmu0iL5EgKgg7//AAyTZDB/4TW8PGD/AMSm2UAdsFJwG5OdvyjJPPXKj9kixUZHje+BB+XGjWaAAjk70ufmDYA2yK4HXHNAHyJr2vax4m1ObWdevbnUdTu8Gae7K5CLgLDBGm2K1t1RYxHDAsUYRUJ3Oz78cnGANoAH8TEcnknAVupJPUDJPA4r7UH7JVpzjxxfAHuNHtucjqF+1ADrn7xGcggAZIf2SrPjHje/PU4bSbQZ9OftDEYwCfl5GOnIoA+Kxz1Khc43bzt6E5ZiqqgQLkl9qvkKrLhiPuP9lvwVeadp2q+NL+GS3GuQw6fpEcqeW82nwSia5vSp3N5c1wEjgw2CsEhYtuUrveGf2YfBei3cV7rV/qPiaSBkkW0ult7PTmdTlPOt7dfMuERskJLcBCeqHkN9IwW8NvFHDBGkUMahI4YkSOGJFACpFEgCRoFACpGAo256klgB5+6MnPX8wpzkj3yffntXyd+yX/yAPGn/AGMNn/6aoa+s26D8evX7p69P5Cvkz9kv/kAeNP8AsYbP/wBNUNAH1rRRRQAUUUUAFFFFABRRRQAUUUUAFfJX7Wn/ACAPBX/Yx3X/AKbJa+ta+Sv2tP8AkAeCv+xjuv8A02S0AfWi9B9B/KqOpzyWmn393FgyWtjd3CBgWXfDC8qZUdeUI9e3OavL0H0H8qo6lbyXdjfWkJRZbqyubeNnJCrJLDJHGZCgLiMOw3FQx27gFzzQB8j6V+0J4hv/AIbavrMkOlReMNJ1Hw8BH5E/9nX2ja5qlvYrfQQvPHIxty1zDMqyttkjjZsgsieoa1448Y6343m8AeA/7HsbvSNGstW8R65rMM1zFbPqMME9lZ2dlA4Lu0U8LTu/BExKtEIGJ881L9nfVr7wD4O0WPUtKh8V+GhPZ395vu107VNKl1KTU4LWV1tjO72c5jktXeHAd51ZmGwx+ieIfAPi/T/GR8e/D7UNETVb/R7XR/EOk+IIrt9O1JbSOGK2ug9iPtUcyxpCrqAmUt1beBIyUAbnw28dav4mufE3hvxPY2dl4o8H6gljqn9mu72F5DLH5lre2qyu8kQmCklHb5uJFjjVgo5uD4k+IZNa+N2nulh5Hw906G70I+SweSSTS769YXxMzCYB7eMIsccJKCRTuZg69V8NvAd/4U/4SDWfEGpQav4t8Val/aWtXlpEIrSJVDJb2dlEyrIsEKFwrSkkghVjTyyX5u2+GGuRa38aNRN3pfkfEewtLbR0V7lmtZbfTLyyLX5MA2pvuAx+zs/DSDBypoA4XRPjl4juvhx4g1q9g02XxdaahoFpokNvBIttf23idbZ9JumtZJFdsgai0ixugKW6E7er9LafFvXE+DZ8b3sFlN4pudSn0PTLSGB47S61VtYk0uyiSDzJJGMwjMjKjMxIJChBxlaX8DNWtdX+H9/eahpr2nh3w3pVh4htYZLpl1LWdCtdSh0q7t1khAnhhS8ijKv5Dp5btGWLYSG1+CPiyfR/Avhy+1+ysNL8M6zrmv6hc6S851CTUru+mudGfT2ubNoP+JejI7PKpYSl1WMKgeQA6rSfi/dQ/DDXvFfiGygPiXwte32iavpFrmGOXWYrlbe0hRJWaeKF1nhad3XcEt7yVV2JhH6H4k+KFtqHh268T3vw/Njr/wBnmvtBS+bSdW0KwuomkiurR72VjqTRny4poQjGSVnjRlZCwwV+Bmqib4haRceIW1Lw3460+1uWvb/y5tet/E1jdLdw6hcJHaW9pNFK5ZZijJLJE8kBRV2SGhefCX4heI5/CkviKTwBJc+EdR0Xy9dsrLUF1/WNL0uXIguLt0eO2g8pnfyIVCtdBpCSXkDgFHUPjD4pj8W+NdFfxj8OvCVn4c1l7DT/APhLI7q3nvrZC5aWF4WxK0So3nHaoVpYiABuFfU+lvcSadYy3U9rdXUlpbSXFzZAiznmeFHkmtd2X+zuTug3/MYim7nJr51/4Vn4/wBK8V+N9c0e1+GWqWXi3WjqcUfiyx1a/urFTuCRp5K26QvMZc3CfvRKIogJAEVF+htFXU00nT11v7ANXFpANSGmLNHpy3vlr562KXDyTLbCTcIlkkkZVwu4gCgDRbp+f/oLV8mfsl/8gDxp/wBjDZ/+mqGvrNun5/8AoLV8mfsl/wDIA8af9jDZ/wDpqhoA+taKKKACiiigAooooAKKKKACiiigAr5b/aq0u7u/CGganBG8ltomvCe+EaM8iw3lpNaxyAKDtRZzFG7OFQGdG3fKwP1JVLUNPs9UtLnT9RtIL2xvIWt7q2uFEkU8L/eR0IIIHDK2Q6MNyFWAagDD8J+L9E8YaLZaxo99a3MVxBG80EVxHJPZXGxfOtLmMNvinhclWSRUYoUkClJEY9MTzz1GTkFeBnPfGMjrgcDv0NfLmq/sreDru7km0vXdf0a3lIP2OI2l9Gg3EBUnuovtAiRSqRxtNKVCZ3Mzlaof8Ml6B/0OfiH2xZaV09DmHJPqT1+tAH1hwcjkAkn/AJZkZ557gnJBG7PQehFKMA55wBwMggZ6474/3iMdABwK+Tv+GS9A/wCh08Q/+Aelf/GaP+GS9A/6HTxD/wCAelf/ABmgD6xyOgzgc8EH9CPXpnv/AMByuR1J59Mr2wD16c9xjPrkgV8m/wDDJegf9Dp4h/8AAPSv/jNH/DJegf8AQ6eIf/APSv8A4zQB9YnaeoyOOpU9OVGScnBBIycgnI6ZVCAME5bsclSfcc8EnHOMHgAHjFfJ/wDwyXoH/Q6eIf8AwD0r/wCM0f8ADJegf9Dp4h/8A9K/+M0AfWOR7jtgMB06AYIxj2wCOpJ4pPl5Az37pjJxnAJK8gdhjlu5NfJ//DJegf8AQ6eIf/APSv8A4zR/wyXoH/Q6eIf/AAD0r/4zQB9Y/L6dtv8ADkDsMnkYPzAEjvjoBS7h6cdRkr/8Vnrnn1z7V8m/8Ml6B/0OniH/AMA9K/8AjNH/AAyXoH/Q6eIf/APSv/jNAH0V4w8YaL4L0S91rWry3toba2nkgglnjS4vrlI2MNnZxFt08877Ywq52FgzfLll8H/ZW0q7svCGvalMhS31jXy9nuGGkisbOG1knXn/AFTyhgmVBJU9sEt0v9lXwfZXiXGpa9r2swRlG+xv9jsI5GVlcCea0gEzx5VT5YdQeeucH6Y0/T7HSrK107TrWGysbKFLe0tbdAkMEKKAqIo7cZZySzsWZ2ZmJIBcHIB9QKWiigAooooAKKKKACiiigAooooAKTAyD6f55/Hn60tFABgZzgZ9e/r/AD5ooooAKKKKACiiigAooooAKKKKACiiigAwPSiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/Z';
			  		var fieldList = ["name","price","basePrice","description","pictureUrl","expire","realProductId","upc","concatId","concatWord"];
			  		var fieldParserList =  data.fieldParserList;
			  		if(fieldParserList.length == 0){
			  			fieldParserList = fieldList;
			  		}
			  		var fieldJsoupList =  data.fieldJsoupList;
			  		var jsondata   =  data.jsonData;
			  		$('#jsoupConfig').val(jsondata);
			  		$('#merchantIdParam').val(merchantId);
			  		$('#idParam').val(id);
			  		$('#serverParam').val(server);
			  		
			  		if(fieldJsoupList != null && fieldJsoupList.length > 0){
			  			var resultHead = "";
			  			resultHead += "Auto Parser Recovery <h5 style='border-style: solid;border-color: rgba(23, 0, 0, 0.15);border-width: 2px;border-radius: 5px;background-color: rgba(255, 244, 41, 0.27);padding: 10px;'><b>Recover Field:</b> ";
			  			for(var fieldJsoup in fieldJsoupList){
			  				var textVal = fieldJsoupList[fieldJsoup];
			  				resultHead += "<span style='padding-right: 10px;'>"+textVal.toUpperCase() +"</span>";
			  				
			  			}
			  			resultHead += "</h5>";
			  			$('#modalTitle').html(resultHead);
			  		}
			  		
			  		for(pData in data.pdList){
			  			var row  = data.pdList[pData];
			  			var recoverFlag =  false;
			  			result += "<div class='row' style='margin-bottom:35px;'>";
			  			var pictureData = row.pictureData;
			  			if(!pictureData){
			  				pictureData = imgDefault;
			  			}
			  			
			
			  			result += "<div class='col-md-12'>";
			  			result += "<div style='padding:20px; background-color:#d4efdf; border-bottom:4px solid #7DCEA0;'>";
		  				result += "<img src=data:image/png;base64,"+ pictureData +" style='max-width:150px;'>";
			  			result += "<span style='vertical-align:bottom; margin-left:20px;font-weight:bold;'>URL "+ (index + 1) +" : </span><span><a href='"+ row.url +"' target='_blank' style='vertical-align:bottom;'>" + row.url + "</a></span>";
			  			result += "</div>";
			  			result += "<div style='padding-bottom:20px; background-color:#E9F7EF;'></div>";
			  			result += "</div>";
			  		
			  			for (field in fieldList){			   
			  				var fieldType = fieldList[field];
			  				if (fieldParserList != null && fieldParserList.indexOf(fieldType) == -1){
			  					continue;
			  				}
			 
			  				var value = replaceTag(row[fieldType]);					
			  				result += "<div class='col-md-12'>";
			  				if(fieldJsoupList.indexOf(fieldType) > -1){
			  					result += "<div style='padding:0px 10px 10px 10px; background-color:#E9F7EF;'>";
			  				}else{
			  					result += "<div style='padding:0px 10px 10px 10px; background-color:#E9F7EF;color: #4b525291;'>";
			  				}
			  				result += "<b>"+ fieldType.toUpperCase() +"</b> : " + (value != null ? value : "");
				  			result += "</div>";
				  			result += "</div>";
			  				
			  			}
			  			result += "</div>";
			  		}
			  					 
			  		$('#modalData').html(result);
			  		$('#modalRun').modal('show');
			  	}else if(data.header == 'noData'){
			  		modalAlert('No Sample Data for this Merchant ID');
			  	}
			  	else {
			  		modalAlert('Error : Cannot Run for Recover');
			  	}
			  	
			  	$('#load'+id).button('reset');
		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
}

function saveMonitorFix(){
	
	var data  = $('#jsoupConfig').val();
	var monitorId = $('#idParam').val();
	var merchantId  = $('#merchantIdParam').val();
	var server  = $('#serverParam').val();
	var param = "merchantId="+ merchantId+"&data="+data;
	$.ajax({
		  type: "POST",
		  url: "savefield",
		  data: param,
		  cache: false,
		  async: true,
		 	success: function(data) {
			  	if(data.header == 'success'){
			  		$(".alertBox").html("<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Recovery Parser for merchantId: "+merchantId+"</div>");
			  		$(".alertBox").show();
			  	}else if(data.header == 'error'){
			  		modalAlert('Error : Cannot Run for Recover');
			  	}

		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
	
	var param = "merchantId="+ merchantId+"&monitorId="+monitorId+"&server="+server;
	$.ajax({
		  type: "POST",
		  url: "rocoverMonitor",
		  data: param,
		  cache: false,
		  async: true,
		 	success: function(data) {
			  	if(data.header == 'success'){  		
			  		$('#' + monitorId).remove();
			  	}else if(data.header == 'error'){
			  		modalAlert('Error : Cannot Run for fieldType');
			  	}

		  	},error: function (textStatus, errorThrown) {
		  		setButton(false);
		  		alert('Error Status - ' + textStatus + " \n" +  'Error Thrown - ' + errorThrown);
		  	}
		});
	$('#modalRun').modal('hide');

}


function replaceTag(value){
	if(value){
		value = value.replace(/\&/g, '&amp;');
		value = value.replace(/</g, '&lt;').replace(/>/g, '&gt;');
	}
	return value;
}




