<%@ page import="botbackend.bean.UserBean, botbackend.utils.Util,botbackend.utils.ViewUtil, botbackend.system.BaseConfig,utils.DateTimeUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="priceza" prefix="pz"%>

<% 
HttpSession ses = request.getSession(false);
String contextPath = request.getContextPath(); 
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
</head>
<style>

.orangeButton{
	color: #fff;
	background-color: #ffb266;
	border-color : #4cae4c;
	min-width: 85px;
}
.orangeButton:hover{
	color: #fff;
	background-color: #eb923b;
	border-color : #4cae4c;
	min-width: 85px;
}
.inputclear {
    position: absolute;
    right: 20px;
    margin: 10px 5px 20px 20px;
    font-size: 14px;
    cursor: pointer;
    color: #ccc;
    display:none;
}
#topSaveConfig{
	display: block;
	z-index: 9999;
	position: fixed;
	bottom: 15px;
	right: 10px;
}

#topButton {
    font-size: 2rem;
    background-color: #777;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    color: #fff;
    text-align: center;
    margin-top: 10px;
    margin-left: 52px;
    float: right;
}

#topButton > i {
			line-height: 38px;
		}
		
		#topButton:hover {
			cursor: pointer;
		}

#topSaveConfig > div{
	z-index: 999
}

#topSaveConfig > div:first-child{
	margin-bottom: 25px;
	margin-left: 40px;
}
.heading-single{
	border-bottom: 1px solid #e9e9e9;
    padding-bottom: 5px;
    margin-left: 25px
}
	
.ul_set_two_columns {
  columns: 2;
  -webkit-columns: 2;
  -moz-columns: 2;
}
.scroll_server {
   overflow: scroll;
   height: 360px;
   margin-bottom: 25px; 
   overflow-x: hidden;
   overflow-y: auto;
}
.scroll_name {
   overflow: scroll;
   height: 360px;
   margin-bottom: 25px;
   overflow-x: hidden;
   overflow-y: auto;
}
.div-head-topic{
	position: relative; 
	margin-top: 3%;
}
.div-colum-content{
	position: relative; 
	margin-left: 30px
}
.title-colum{
	margin-top: 8px
}
#merchantName-title > span{
	border: 1px solid #cccccc;
	margin-left: 20px;
	padding: 9px 14px 8px;
	font-size: 14px;
	font-weight: 600;
	border-radius: 2px;
}
		
#merchantName-title > span > span:not(:first-child){
	margin-left: 20px;
}
</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	<input type="hidden" id="contextPathHidden" value="<%=contextPath %>">
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
			<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom:2%;">
                    	<div class="col-md-6">
	                    	<h3 class="page-title"><i class="fa fa-th" style="padding-right: 10px;"></i> Merchant Config Page<span id="merchantName-title"></span></h3>
		                </div>
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="dashboard">Dashboard</a> > <a href="#">Merchant config</a> <i class="fa fa-circle"></i>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->

                   	<!-- BEGIN HEADER SEARCH BOX -->
                   	<div class="modal fade" id="modalAlert" style="top: 5%;">
	    				<div class="modal-dialog" style="width: 40%;">
	    					<div class="modal-content">
		        				<div class="modal-header">
		          					<button type="button" class="close" data-dismiss="modal">&times;</button>
		          					<h4 class="modal-title" id="modalAlertTitle"></h4>
		        				</div>
		       					<div class="modal-body" id="modalAlertData"></div>
		    					<div class="modal-footer">
		          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        				</div>
	    					</div>
	    				</div>
  					</div>
   					<form id="submitMerchantConfig" action="<%=contextPath %>/admin/submitmerchantconfig" method="post">
                    <div class="row">
	                    <div class="col-md-12">
	                    	 <div class="col-md-6" style="margin: 0 0 25px;">
			                    <div class="input-group">
			                        <input type="text" id="merchantId" name="merchantId" class="form-control"  placeholder="Search by merchant ID..." value="${merchantId}" >
			                        <span class="input-group-btn">
			                            <a id="search" class="btn btn-primary submit" onclick="getDataByMerchantId()">
			                                <i class="icon-magnifier"></i>
			                            </a>
			                        </span>
			                    </div>
		                    </div>
	                    </div>
                    </div>
	                <!-- END HEADER SEARCH BOX -->
                 	<!-- START CONTENT -->
                 	<!-- START MERCHANT CONFIG TABLE -->
                    <div class="row" id="urlConfigContent" style="display:none;">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                            	<div class="portlet-title">
                            		<div class="caption">
                            			<div class="col-md-12">
                            				<div class="col-md-10" style="margin-top:12px;">
                            					<h4 id="group-productName"><span>Merchant&nbsp;Config&nbsp;Table</span></h4>
                            				</div>
	                            		</div>
                            		</div>               	
                            	</div> 
	                            <div class="portlet-body clearfix ">
	                               <div class="row">                        
										<div class="col-md-6">
										
                                     		<c:set var="showAllConfig" value="style='display:none'"></c:set>
											<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantconfig.showallconfig') }">
                                       			<c:set var="showAllConfig" value="style='display:block'"></c:set>
                                       		</c:if>
<!-- ******************************************************* START TEMPLATE CONFIG ******************************************************************** -->
											<div class="div-head-topic">
												<h5 class="heading-single"><b>Auto fill by template</b></h5>
											</div>        
											<div class="div-colum-content">
												<div class="row">
													<div class="col-md-5 title-colum">Fill standard config for template merchant</div>
													<div class="col-md-6" id="templateConfigBox input-group">
														<div class="input-group">
															<select id="templateConfig" class="form-control form-control-sm">
													      		<option value="none">Select</option>
													      		<c:if test="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'}">
		                                        					<option value="shopeeDistribute">Shopee distribute merchant</option>
																	<option value="lazadaDistribute">Lazada distribute merchant</option>
																	<option value="jdcentralDistribute">JDCentral distribute merchant</option>
																</c:if>
													    	</select>
														    <span class="input-group-btn">
														        <input type="button" class="btn btn-info" onclick="addTemplateConfig()" value="GO →">
														    </span>
														</div>
													</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="สำหรับใส่ Config เบื้องต้นสำหรับร้านค้า เช่น urlCrawlerClass, parserClass"></a>	                                       		
	                                       		</div>
											</div>
<!-- ******************************************************* END TEMPlATE CONFIG ******************************************************************** -->
<!-- ******************************************************* START WEB COMMON ******************************************************************** -->
											<div class="div-head-topic">
												<h5 class="heading-single"><b>Common</b></h5>
											</div>
											<div class="div-colum-content">
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Currency Rate</div>
		                                            <div class="col-md-6" id="currencyRateBox">
		                                            	<input type="text" id="currencyRate" name="currencyRate"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 1.0">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ราคา คูณ เรทเท่าไหร่"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Max Depth</div>
		                                            <div class="col-md-6" id="maxDepthBox">
		                                            	<input type="text" id="maxDepth" name="maxDepth"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 5">
		                                            	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="หน้าหมวดจะไปหน้าถัดไป กี่ครั้ง"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Product Update Limit</div>
		                                            <div class="col-md-6" id="productUpdateLimitBox">
		                                            	<input type="text" id="productUpdateLimit" name="productUpdateLimit"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 7">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="สินค้าตัวนั้น อัพเดทไม่ได้กี่ครั้งแล้วจะลบ"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Delete Limit</div>
		                                            <div class="col-md-6" id="deleteLimitBox">
		                                            	<input type="text" id="deleteLimit" name="deleteLimit"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 200">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ปรับเป็น error แล้วหยุดรัน ถ้าลบเกินกี่ตัว"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		
	                                      		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">HourRate</div>
		                                            <div class="col-md-6" id="hourRateBox">
		                                            	<input type="text" id="hourRate" name="hourRate"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 24">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="บอทรันทุกกี่ชม. นับจาก start date"></a>
	                                       		</div>
	                                       		
	                                      		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">HourRate-WCE</div>
		                                            <div class="col-md-6" id="wceHourRateBox">
		                                            	<input type="number" id="wceHourRate" name="wceHourRate"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 24">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="บอทรัน WCE ทุกกี่ชม. นับจาก start date"></a>
	                                       		</div>
	                                       		
	                                      		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">HourRate-DUE</div>
		                                            <div class="col-md-6" id="dueHourRateBox">
		                                            	<input type="number" id="dueHourRate" name="dueHourRate"class="form-control form-control-sm " onblur="validateInput(this)" placeholder="Default : 24">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="บอทรัน due ทุกกี่ชม. นับจาก start date"></a>
	                                       		</div>
	                                       		
	                                      		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Max Runtime</div>
		                                            <div class="col-md-6" id="maxRuntimeBox">
		                                            	<input type="text" id="maxRuntime" name="maxRuntime"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="Default : 24">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="บอทรันได้นานสุดกี่ชม. ถ้าเกิน ตัดจบ ขึ้น Timeout"></a>
	                                       		</div>
	                                       		
	                                       		
											</div>
											
<!-- ******************************************************* END WEB COMMON ******************************************************************** -->
<!-- ******************************************************* START BOT CONFIG ******************************************************************** -->
										
											<div class="div-head-topic" ${showAllConfig}>
												<h5 class="heading-single"><b>Bot config</b></h5>
											</div>
											
											<div class="div-colum-content" ${showAllConfig}>
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Enable Proxy</div>
		                                            <div class="col-md-2" id="enableProxyBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="enableProxy" name="enableProxy" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="วิธีการยิง request ว่าจะให้ header ติด proxy ไหม"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Enable Cookies</div>
		                                            <div class="col-md-2" id="enableCookiesBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="enableCookies" name="enableCookies" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="วิธีการยิง request"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Disable Parser Cookies</div>
		                                            <div class="col-md-2" id="disableParserCookiesBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="disableParserCookies" name="disableParserCookies" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="วิธีการยิง request"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Ignore Http Error</div>
		                                            <div class="col-md-2" id="ignoreHttpErrorBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="ignoreHttpError" name="ignoreHttpError" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="วิธีการยิง request"></a>	                                       		
	                                        		</div>
	                                       		</div>

											</div>
									
<!-- ******************************************************* END BOT CONFIG ********************************************************** -->																					
<!-- ******************************************************* START PERFORMANCE CONFIG ********************************************************** -->																				
									
											<div class="div-head-topic" ${showAllConfig}>
												<h5 class="heading-single"><b>Performance config</b></h5>
											</div>
											<div class="div-colum-content" ${showAllConfig}>
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Wce Parser Thread Num</div>
		                                            <div class="col-md-6" id="wceParserThreadNumBox">
		                                            	<input type="text" id="wceParserThreadNum" name="wceParserThreadNum"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="wce รันกี่ thread"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Due Parser ThreadNum</div>
		                                            <div class="col-md-6" id="dueParserThreadNumBox">
		                                            	<input type="text" id="dueParserThreadNum" name="dueParserThreadNum"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="due รันกี่ thread"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Wce SleepTime</div>
		                                            <div class="col-md-6" id="wceSleepTimeBox">
		                                            	<input type="text" id="wceSleepTime" name="wceSleepTime"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="wce รันแล้ว sleep กี่ millisec"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Due SleepTime</div>
		                                            <div class="col-md-6" id="dueSleepTimeBox">
		                                            	<input type="text" id="dueSleepTime" name="dueSleepTime"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="due รันแล้ว sleep กี่ millisec"></a>	                                       		
	                                       		</div>                                       	

											</div>
																														
									</div>
<!-- ******************************************************* END PERFORMANCE CONFIG ********************************************************** -->				
<!-- ******************************************************* START WEB CONFIG ******************************************************************** -->											
									<div class="col-md-6">
											<div class="div-head-topic">
												<h5 class="heading-single"><b>Web config</b></h5>
											</div>
											<div class="div-colum-content">
												<div class="row">
		                                            <div class="col-md-5 title-colum">Parser Class</div>
		                                            <div class="col-md-6" id="parserClassBox">
                                            			<select id="parserClass" name="parserClass" class="form-control form-control-sm">
                                        					<option value="userConfig">User Config</option>
		                                            		<c:if test="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'}">
	                                        					<option value="web.parser.filter.TemplateLazadaJSIncludeIdHTMLParser">					Lazada (with ID)</option>
	                                        					<option value="web.parser.filter.TemplateTaradIncludeIDHTMLParser">						Tarad (with ID)</option>
	                                        					<option value="web.parser.filter.TemplateTaradIncludeIDNoPriceHTMLParser">				Tarad (with ID, ContactPrice)</option>
	                                        					<option value="web.parser.filter.TemplateWeLoveWithIDHTMLParser">						Welove (with ID)</option>
	                                        					<option value="web.parser.filter.TemplateWeLoveWithIDContactPriceHTMLParser">			Welove (with ID, ContactPrice)</option>
	                                        					<option value="web.parser.filter.TemplateWeLovePlatinumWithIDHTMLParser">				Welove platinum (with ID)</option>
	                                        					<option value="web.parser.filter.TemplateWeLovePlatinumWithIDContactPriceHTMLParser">	Welove platinum (with ID, ContactPrice)</option>
	                                        					<option value="web.parser.filter.KaideeMarketplaceHTMLParser">							KaideeMarketplace</option>
	                                        					<option value="web.parser.filter.ReadyPlanetHTMLParser">								ReadyPlanet</option>
		                                            		</c:if>
														</select>     
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ชื่อแพคเกจและคลาสของ parserClass"></a>	                                       		
	                                       		</div>
	                                       		
												<div class="row">
		                                            <div class="col-md-5 title-colum">Url Crawler Class</div>
		                                            <div class="col-md-6" id="urlCrawlerClassBox">
		                                            	<select id="urlCrawlerClass" name="urlCrawlerClass" class="form-control form-control-sm">
															<option value="">No select</option>
														</select>   
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="คลาสพิเศษ ที่ใช้ดัดแปลงโค้ดก่อนรันหน้าหมวด"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Init Crawler Class</div>
		                                            <div class="col-md-6" id="initCrawlerClassBox">		   
		                                            	<select id="initCrawlerClass" name="initCrawlerClass" class="form-control form-control-sm">
															<option value="">No select</option>
														</select>   
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="คลาสพิเศษ ที่จำเป็นต้องใช้ ( มีใช้เฉพาะ facebook ) "></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Url Crawler Charset</div>
		                                            <div class="col-md-6" id="urlCrawlerCharsetBox">
		                                            	<input type="text" id="urlCrawlerCharset" name="urlCrawlerCharset"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ตัวกำหนด charset ตอนรันหน้าหมวด"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Parser Charset</div>
		                                            <div class="col-md-6" id="parserCharsetBox">
		                                            	<input type="text" id="parserCharset" name="parserCharset"class="form-control form-control-sm" placeholder="Default : UTF-8">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ตัวกำหนด charset ตอนรันหน้าสินค้า"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Skip Encode Url</div>
		                                            <div class="col-md-2" id="skipEncodeUrlBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="skipEncodeURL" name="skipEncodeURL" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ถ้าบอทยิงแล้ว http พังก็ข้ามเลย"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                       		
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Wce Clear ProductUrl</div>
		                                            <div class="col-md-2" id="wceClearProductUrlBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="wceClearProductUrl" name="wceClearProductUrl" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="จะเคลียร์ลิงค์สินค้าเก่าทุกรอบที่รัน wce ทำให้รันจบช้าขึ้นมากๆ )"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                       		
											</div>
<!-- ******************************************************* END WEB CONFIG ******************************************************************** -->											
<!-- ******************************************************* START FEED CONFIG ******************************************************************** -->									
											<div class="div-head-topic">
												<h5 class="heading-single"><b>Feed config</b></h5>
											</div>
											<div class="div-colum-content">
												<div class="row">
		                                            <div class="col-md-5 title-colum">Feed Crawler Class</div>
		                                            <div class="col-md-6" id="feedCrawlerClassBox">
		                                            	<select id="feedCrawlerClass" name="feedCrawlerClass" class="form-control form-control-sm">
															<option value="">No select</option>
															<c:if test="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th' || BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'id' }">
																<option value="feed.crawler.TemplateShopeeItemApiShortLinkFeedCrawler">				Shopee Distribute (normal)</option>
																<option value="feed.crawler.TemplateShopeeItemApiShortLinkCustomUTMFeedCrawler">	Shopee Distribute (no utm)</option>
																<option value="feed.crawler.TemplateJDCentralFeedCrawler">							JD Central</option>
		                                            		</c:if>
														</select>   
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ชื่อแพคเกจและคลาสของ feed ถ้ามีค่า จะมองข้าม parserClass"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Feed Crawler Param</div>
		                                            <div class="col-md-6" id="feedCrawlerParamBox">
		                                            	<input type="text" id="feedCrawlerParam" name="feedCrawlerParam"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="สำหรับใส่ shopID ของร้าน template shopee"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Fix CategoryId</div>
		                                            <div class="col-md-6" id="fixCategoryIdBox">
		                                            	<input type="number" id=fixCategoryId name="fixCategoryId"class="form-control form-control-sm">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="category เฉพาะ shopee"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		   <div class="row">
		                                            <div class="col-md-5 title-colum">Fix Keyword</div>
		                                            <div class="col-md-6" id="fixKeywordBox">
		                                            	<input type="text" id=fixKeyword name="fixKeyword"class="form-control form-control-sm">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="category เฉพาะ shopee"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Realtime Parser Class</div>
		                                            <div class="col-md-6" id="parserRealtimeClassBox">
		                                            	<input type="text" id="parserRealtimeClass" name="parserRealtimeClass"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ชื่อแพคเกจและคลาสของ Realtime Parser"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Realtime Detail Class</div>
		                                            <div class="col-md-6" id="parserDetailClassBox">
		                                            	<input type="text" id="parserDetailClass" name="parserDetailClass"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ชื่อแพคเกจและคลาสของ Detail Parser"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">MerchantName</div>
		                                            <div class="col-md-6" id="merchantNameBox">
		                                            	<input type="text" id="merchantName" name="merchantName"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="สำหรับใส่ merchantName ของร้าน Lazada"></a>	                                       		
	                                       		</div>
	                                       		
                                      			
                                    				<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Feed Type</div>
		                                            <div class="col-md-6" id="feedTypeBox">
		                                            	<input type="text" id="feedType" name="feedType"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ประเภทของฟีด"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Feed Username</div>
		                                            <div class="col-md-6" id="feedUsernameBox">
		                                            	<input type="text" id="feedUsername" name="feedUsername"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="username สำหรับใช้โหลดฟีด"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Feed Password</div>
		                                            <div class="col-md-6" id="feedPasswordBox">
		                                            	<input type="text" id="feedPassword" name="feedPassword"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="password สำหรับใช้โหลดฟีด"></a>	                                       		
	                                       		</div>
	                                       		
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Feed Xml MegaTag</div>
		                                            <div class="col-md-6" id="feedXmlMegaTagBox">
		                                            	<input type="text" id="feedXmlMegaTag" name="feedXmlMegaTag"class="form-control form-control-sm" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="วิธีดึงฟีดแบบ xml - Tag ที่ครอบสินค้าแต่ละตัว"></a>	                                       		
	                                       		</div>
	                                       		
										</div>	
<!-- ******************************************************* END FEED CONFIG ******************************************************************** -->								
<!-- ******************************************************* START PRODUCT CONFIG ******************************************************************** -->										
										
											<div class="div-head-topic">
												<h5 class="heading-single"><b>Product config</b></h5>
											</div>
											<div class="div-colum-content">
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Image Parser Class</div>
		                                            <div class="col-md-6" id="imageParserClassBox">
		                                            	<select id="imageParserClass" name="imageParserClass" class="form-control form-control-sm">	                               
	                                            			<option value="">No select</option>	                                            		                                            	
														</select>  
	                                        		</div>
													<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ชื่ื่อแพคเกจกับคลาสของตัวดึงรูป"></a>	                                       		
	                                       		</div>
	                                       		
	                                      		<div class="row">
		                                            <div class="col-md-5 title-colum">Update Price Percent</div>
		                                            <div class="col-md-6" id="updatePricePercentNumBox">
		                                            	<input type="text" id="updatePricePercent" name="updatePricePercent"class="form-control form-control-sm" onblur="validateInput(this)" placeholder="">
	                                        			<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        		</div>
	                                        		<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;" title="" data-original-title="ทำให้สินค้าที่ราคาเปลี่ยนเกิน xx % ไม่สามารถอัพเดทได้"></a>	                                       		
	                                       		</div>
	                                       		<!-- ********************** CHECKBOX *********************** -->
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Force Update Image</div>
		                                            <div class="col-md-2" id="forceUpdateImageBox" style="margin-top: 5px">
			                                            	<input type="checkbox" id="forceUpdateImage" name="forceUpdateImage" value="true">
															<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="บังคับเปลี่ยน image ( ลิงค์เดิมก็ต้องดึงรูปใหม่ effect กับ sync speed)"></a>	                                       					                                           
	                                        		</div>
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Force Update Category</div>
		                                            <div class="col-md-2" id="forceUpdateCategoryBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="forceUpdateCategory" name="forceUpdateCategory" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="บังคับเปลี่ยน category  ( effect กับ sync speed )"></a>	                                       					                                            
	                                        		</div>
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Force Update Keyword</div>
		                                            <div class="col-md-2" id="forceUpdateKeywordBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="forceUpdateKeyword" name="forceUpdateKeyword" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="บังคับเปลี่ยน keyword  ( effect กับ sync speed )"></a>	                                       					                                            
	                                        		</div>
	                                       		</div>
	                                       		
	                                       		<div class="row">
		                                            <div class="col-md-5 title-colum">Force Delete</div>
		                                            <div class="col-md-2" id="forceDeleteBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="forceDelete" name="forceDelete" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ถ้าอัพเดทไม่ได้ให้ลบทันที "></a>	                                       			                                            
	                                        		</div>
	                                       		</div>
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Enable Update BasePrice</div>
		                                            <div class="col-md-2" id="enableUpdateBasePriceBox" style="margin-top: 10px">
		                                            	<input type="checkbox" id="enableUpdateBasePrice" name="enableUpdateBasePrice" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ทำให้ตอนซิงค์ไม่สนใจ price history ( effect กับ sync speed มาก )"></a>	                                       				                                            
	                                        		</div>
	                                       		</div>
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Enable Update Description</div>
		                                            <div class="col-md-2" id="enableUpdateDescBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="enableUpdateDesc" name="enableUpdateDesc" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ทำให้อัพเดท description ได้ตลอดเวลา ( effect กับ sync speed มาก )"></a>	                                       					                                            
	                                        		</div>
	                                       		</div>
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Enable Update Picture</div>
		                                            <div class="col-md-2" id="enableUpdatePicBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="enableUpdatePic" name="enableUpdatePic" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ทำให้อัพเดท pictureUrl ได้ตลอดเวลา ถ้าเช็คเจอว่าลิงค์เปลี่ยน ( effect กับ sync speed มาก )"></a>	                                       					                                           
	                                        		</div>
	                                       		</div>
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">Enable Update Url</div>
		                                            <div class="col-md-2" id="enableUpdateUrlBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="enableUpdateUrl" name="enableUpdateUrl" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ทำให้อัพเดท url ได้ตลอดเวลา ถ้า urlForUpdate ยังเหมือนเดิม ( effect กับ sync speed มาก)"></a>	                                       					                                            
	                                        		</div>
	                                       		</div>
	                                       		<div class="row" ${showAllConfig}>
		                                            <div class="col-md-5 title-colum">User Old ProductUrl</div>
		                                            <div class="col-md-2" id="userOldProductUrlBox" style="margin-top: 5px">
		                                            	<input type="checkbox" id="userOldProductUrl" name="userOldProductUrl" value="true">
														<a class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" style="text-decoration:none;margin-top: 8px;margin-left: 8px;" title="" data-original-title="ใช้ url ที่ดึงมาได้ในครั้งแรกสุด ไม่ให้อัพเดท url"></a>	                                       		
	                                        		</div>
	                                       		</div>
	                                  
		                              
										</div>											
								</div>									
							</div>	

                            </div>	
                        </div>
                	</div>
                	<!-- END MERCHANT CONFIG TABLE -->
                    <!-- END CONTENT -->
                </div>
                <input type="hidden" id="merchantHidden" name="merchantHidden" value="">
                </form>
                <div class="modal fade" id="modalAlert" style="top: 5%;">
	    				<div class="modal-dialog" style="width: 40%;">
	    					<div class="modal-content">
		        				<div class="modal-header">
		          					<button type="button" class="close closeBtn" data-dismiss="modal">&times;</button>
		          					<h4 class="modal-title" id="modalAlertTitle"></h4>
		        				</div>
		       					<div class="modal-body" id="modalAlertData"></div>
		    					<div class="modal-footer">
		          					<button type="button" class="btn btn-default closeBtn" data-dismiss="modal">Close</button>
		        				</div>
	    					</div>
	    				</div>
  				</div>
                <!-- END CONTENT BODY -->
                <div id="topSaveConfig" style="float: right;">
 			<div>
  				<i class="fa fa-spinner fa-spin iconSpin"></i>
			</div> 
			<div>
  				<button type="submit" class='btn orangeButton' id="btnTopSave" style="margin-bottom: 10px;">Save Config</button>
			</div>   					
			<div id="topButton">
				<i class="fa fa-arrow-up"></i>
			</div>
		</div>  
            </div>
            <!-- END CONTENT -->
		</div>
	</div>
</div>

	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/merchant-config-script.js"></script>
	
</body>
</html>