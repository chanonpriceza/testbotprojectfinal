<%@ page
	import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig, botbackend.utils.ViewUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%
	String contextPath = request.getContextPath();
// 	UserBean userBean;
// 	HttpSession ses = request.getSession(false);
// 	userBean = (UserBean) ses.getAttribute("userData");
// 	String userName = userBean.getUserName();
// 	request.setAttribute("userName", userName);
// 	int userID = userBean.getUserId();
%>

<c:set var="userName" value="${sessionScope['userData'].userName}" />
<c:set var="userId" value="${sessionScope['userData'].userId}" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>

<title>Bot Backend</title>
<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />
<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />

<style>
.tooltip-inner {
    max-width: 130px;
    /* If max-width does not work, try using width instead */
    width: 130px; 
}
.dropcolor {
	color: #fff;
	background: #d86213;
}

.dropcolor:hover {
	color: #fff;
	background: #b35900;
}

.colorpanel {
	background: #f5f5fa;
}

.colorbordered {
	background: #FFFFFF;
}

.block__header {
	width: 100%
}

.block__header--left {
	display: inline-block;
	text-align: left;
	margin-left: 10px;
	width: 50%;
}

.block__header--right {
	display: inline-block;
	text-align: right;
	width: 45%;
}

* {
	box-sizing: border-box;
}

#groupSettings {
	display: block;
	z-index: 9999;
	position: fixed;
	bottom: 15px;
	right: 15px;
}

.panel {
	margin-bottom: 0px;
}

.btn-circle-dashboard {
	font-size: 2rem;
	background-color: #777;
	border-radius: 50%;
	width: 40px;
	height: 40px;
	color: #fff;
	text-align: center;
	margin-top: 10px;
	margin-left: 40px;
}

.btn-circle-dashboard>i {
	margin-left: 0px;
	margin-top: 13px;
}

#groupSettings>div {
	z-index: 999
}

#groupSettings>div:first-child {
	margin-bottom: 0px;
	margin-left: 0px;
}

#topButton:hover {
	cursor: pointer;
}

body {
	font-family: sans-serif;
}

/* ---- grid ---- */
.grid {
	background: #f5f5fa;
	max-width: 100%;
}

/* clear fix */
.grid:after {
	content: '';
	display: block;
	clear: both;
}

/* ---- .grid-item ---- */
.grid-item {
	float: left;
	width: auto;
	height: auto;
	background: transparent;
	border: 0px solid hsla(0, 0%, 0%, .4);
	padding-bottom: 10px;
}

.grid-item--width2 {
	width: 200px;
}

.grid-item--height2 {
	height: 200px;
}

.grid-item:hover {
	border-color: hsla(0, 0%, 100%, 0.5);
	cursor: move;
}

.grid-item.is-dragging, .grid-item.is-positioning-post-drag {
	background: transparent;
	z-index: 2;
}

.packery-drop-placeholder {
	outline: 3px dashed hsla(0, 0%, 0%, 0.5);
	outline-offset: -6px;
	-webkit-transition: -webkit-transform 0.2s;
	transition: transform 0.2s;
}
</style>
</head>

<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md ">

	<jsp:include page="/WEB-INF/jsp/template/header.jsp" />
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="wrapper">

			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page="/WEB-INF/jsp/template/menuside.jsp" />

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content colorpanel">
					<!-- BEGIN PAGE HEADER-->

					<!-- MAIN CONTENT -->
					<div class="main-content">
						<div class="container-fluid">
							<div class="col-md-12 grid">
								<div class="grid-sizer" style="width: 8.33%"></div>
								<c:if test="${not empty allPlugin}">
									<c:forEach var="plugin" items="${allPlugin}">
										<c:if test="${not empty plugin}">
<%-- 											<c:if test="${plugin.pluginType == 1 }"> --%>
<%-- 												<c:set var="showSetting" value="${plugin.showSetting }" scope="request" /> --%>
<%-- 												<c:import --%>
<%-- 													url="/WEB-INF/jsp/template/plugin-dashboard-type1.jsp"> --%>
<%-- 													<c:param name="id" value="${plugin.id }" /> --%>
<%-- 													<c:param name="pluginPosition" value="${plugin.pluginPosition }"/> --%>
<%-- 												</c:import> --%>
<%-- 											</c:if> --%>
											
<%-- 											<c:if test="${plugin.pluginType == 2 }"> --%>
<%-- 												<c:set var="showSetting" value="${plugin.showSetting }" scope="request" /> --%>
<%-- 												<c:import --%>
<%-- 													url="/WEB-INF/jsp/template/plugin-dashboard-type2.jsp"> --%>
<%-- 													<c:param name="id" value="${plugin.id }" /> --%>
<%-- 													<c:param name="pluginPosition" value="${plugin.pluginPosition }"/> --%>
<%-- 												</c:import> --%>
<%-- 											</c:if> --%>
											
											<c:if test="${plugin.pluginType == 3 }">
												<c:set var="showSetting" value="${plugin.showSetting }" scope="request" />
												<c:import
													url="/WEB-INF/jsp/template/plugin-dashboard-type3.jsp">
													<c:param name="id" value="${plugin.id }" />
													<c:param name="pluginPosition" value="${plugin.pluginPosition }"/>
													<c:param name="userName" value="${userName}"/>
												</c:import>
											</c:if>
											
											<c:if test="${plugin.pluginType == 4 }">
												<c:set var="showSetting" value="${plugin.showSetting }" scope="request" />
												<c:import
 													url="/WEB-INF/jsp/template/plugin-dashboard-type4.jsp"> 
 													<c:param name="id" value="${plugin.id }" /> 
 													<c:param name="pluginPosition" value="${plugin.pluginPosition }"/> 
 												</c:import> 
 											</c:if>

											<c:if test="${plugin.pluginType == 5 }">
												<c:set var="showSetting" value="${plugin.showSetting }" scope="request" />
												<c:import
													url="/WEB-INF/jsp/template/plugin-dashboard-type5.jsp">
													<c:param name="id" value="${plugin.id }" />
													<c:param name="pluginPosition" value="${plugin.pluginPosition }"/>
													<c:param name="userName" value="${userName}"/>
												</c:import>
											</c:if>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${empty allPlugin}">
									<div class="" style="color: #999">
								    	<h3>Announcement : Manage  your dashboard by click<i class="fa fa-cog" style="padding-left: 8px;"></i></h3>
								  	</div>
								</c:if>
								<div id="groupSettings">
									<div id="groupButton" style="display: none;">
										<div class='btn-circle-dashboard' id="btnTopSave"><i class="fa fa-floppy-o"></i></div>
										<div class='btn-circle-dashboard' id="btnSetting"><i class="fa fa-plus"></i></div>
									</div>
									<div>
										<div class='btn-circle-dashboard' id="topButton">
											<i class="fa fa-cog"></i>
										</div>
									</div>
								</div>


								<div class="modal fade" id="modalAdd">
									<div class="modal-dialog" style="width: 20%;">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title" style="color: #FFC300;">Add Plugin</h4>
											</div>
											<div class="modal-body" id="modalAlertAdd">
												<div class="row">
													<div class="col-md-12">
														<select id="pluginType" class="form-control">
<!-- 															<option value="1">Task of Content</option> -->
<!-- 															<option value="2">Product waiting sync chart</option> -->
															<option value="3">Core Merchant</option>
															<option value="4">Bot Quality chart</option>
															<option value="5">Show Task</option>
														</select>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAdd" onclick='createPlugin();'>Create</button>
											</div>
										</div>
									</div>
								</div>

								<div class="modal fade" id="modalSave">
									<div class="modal-dialog" style="width: 20%;">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title" style="color: #FFC300;">Save Position</h4>
											</div>
											<div class="modal-body" id="modalAlertSave">
												<label class="control-label" id="topicModalSetting">Do you want to save plugin position ?</label> 

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSavePosition" onclick='savePosition();'>Save changes</button>
											</div>
										</div>
									</div>
								</div>

								<div class="modal fade" id="modalAlert">
									<div class="modal-dialog" style="width: 20%;">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title" style="color: #FFC300;">Alert</h4>
											</div>
											<div class="modal-body" id="modalAlertData"></div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" id="userID" value="${userId}">
							<input type="hidden" id="actionAlert" value="">
							
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
		</div>
	</div>

	<script type="text/javascript" src="<%=contextPath%>/assets/pages/js/dashboard-script.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/assets/pages/js/plugin-script.js"></script>
</body>
</html>