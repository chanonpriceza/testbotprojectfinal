<%@ page
	import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig,utils.DateTimeUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%
	String contextPath = request.getContextPath();
	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>

<title>Bot Backend</title>

<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />
<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />

</head>
<style>
</style>
<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">
   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                   	 <!-- BEGIN PAGE BAR -->
		                   	 <div class="page-bar" style="margin-bottom:2%;">
						<div class="col-md-6">
							<h3 class="page-title">
								<i class="fa fa-desktop" style="padding-right: 10px;"></i> Server Runtime Report
							</h3>
						</div>
						<ul class="page-breadcrumb">
							<li><a href="dashboard">Dashboard</a> > <a href="#"> Server Runtime Report</a> <i class="fa fa-circle"></i></li>
						</ul>
					</div>
					<!-- END PAGE BAR -->
					<!-- START CONTENT -->
					<!--------------------------Start Server Report----------------------------------------------------------------------------------->
					<!-- START Server Report TABLE -->
					<div class="row" id="ServerReport">
						<div class="col-md-12">
							<div class="portlet light bordered">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-10" style="margin-top: 12px;">
											<h4 id="group-productName">
												<span>All Report result <c:if test="${not empty totalReportList}">
												<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${totalReportList}"/>
												</c:if> record</span>
											</h4>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin: 25px 0;">
										<div class="col-md-12">
											<form id="searchReport"action="<%=contextPath%>/admin/serversruntimereport"method="post">
												<div class="row">
													<div class="col-md-2" style="display: inline-flex;">												
																<span style="font-size: 16px; font-weight: bold;"><label class="control-label"> Server </label></span>&nbsp;&nbsp;<select id="reportServer" class="form-control" name="reportServer"style="padding: 2px 5px;">
														
																<c:choose>
																	<c:when test="${reportServer == '' || reportServer == null}">
																		<option value="${''}"><c:out value="${'ALL'}" /></option>
																	</c:when>
																	<c:otherwise>
																		<option value="${reportServer}">${reportServer}</option>
																	</c:otherwise>
																</c:choose>
																<c:if test="${not empty BaseConfig.BOT_ACTIVE_SERVER_LIST}">
																	<c:if test="${reportServer != ''}"><option value="${''}"><c:out value="${'ALL'}" /></option></c:if>
																	<c:forEach items="${BaseConfig.BOT_ACTIVE_SERVER_LIST}" var="server">
																		<option value="${server}"><c:out value="${server}" /></option>
																	</c:forEach>
																</c:if>
															</select>
														
													</div>
													<div class="col-md-2" style="display: inline-flex;">
														<c:choose>
															<c:when
																test="${not empty reportType && reportType == 'WCE'}">
																<c:set var="typeWCE" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportType && reportType == 'WCE_TEST'}">
																<c:set var="typeWCETEST" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportType && reportType == 'DUE_TEST'}">
																<c:set var="typeDUETEST" value="${selected='selected'}" />
															</c:when>
															<c:otherwise>
																<c:set var="typeDUE" value="${selected='selected'}" />
															</c:otherwise>
														</c:choose>
														<span style="font-size: 16px; font-weight: bold;"><label class="control-label"> Type </label></span>&nbsp;&nbsp; <select name="reportType" class="form-control" id="reportType" style="padding: 2px 10px;">
															<option value="DUE" ${typeDUE}>DUE (อัพเดทสินค้าที่มีอยู่)</option>
															<option value="DUE_TEST" ${typeDUETEST}>DUE_TEST (ทดสอบอัพเดทสินค้าที่มีอยู่)</option>
															<option value="WCE" ${typeWCE}>WCE (ดึงสินค้าใหม่)</option>
															<option value="WCE_TEST" ${typeWCETEST}>WCE_TEST (ทดสอบดึงสินค้าใหม่)</option>
														</select>
													</div>
													<div class="col-md-2" style="display: inline-flex;">
														<c:choose>
															<c:when
																test="${not empty reportStatus && reportStatus == 'DONE'}">
																<c:set var="statusDONE" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportStatus && reportStatus == 'FAILED'}">
																<c:set var="statusFailed" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportStatus && reportStatus == 'RUNNING'}">
																<c:set var="statusRunning"
																	value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportStatus && reportStatus == 'WARNING'}">
																<c:set var="statusWarning"
																	value="${selected='selected'}" />
															</c:when>
															<c:otherwise>
																<c:set var="statusAll" value="${selected='selected'}" />
															</c:otherwise>
														</c:choose>
														<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Status</label></span>&nbsp;&nbsp; <select name="reportStatus" class="form-control" id="reportStatus" style="padding: 2px 10px;">
															<option value="" ${statusAll}>ALL</option>
															<option value="DONE" ${statusDONE}>PASSED</option>
															<option value="FAILED" ${statusFailed}>FAILED</option>
															<option value="RUNNING" ${statusRunning}>RUNNING</option>
														</select>
													</div>

													<div class="col-md-2" style="display: inline-flex;">
														<c:choose>
															<c:when
																test="${not empty reportTime && reportTime == '1'}">
																<c:set var="time1" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportTime && reportTime == '2'}">
																<c:set var="time2" value="${selected='selected'}" />
															</c:when>
															<c:when
																test="${not empty reportTime && reportTime == '3'}">
																<c:set var="time3" value="${selected='selected'}" />
															</c:when>
															<c:otherwise>
																<c:set var="timeAll" value="${selected='selected'}" />
															</c:otherwise>
														</c:choose>
														<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Time</label></span>&nbsp;&nbsp; <select name="reportTime" class="form-control" id="reportTime"style="padding: 2px 10px;">
															<option value="0" ${timeAll}>ALL</option>
															<option value="1" ${time1}>< 24</option>
															<option value="2" ${time2}>24 - 72</option>
															<option value="3" ${time3}>> 72</option>
														</select>
													</div>
													
													<div class="col-md-2" style="display: inline-flex;">
														<c:choose>
															<c:when
																test="${not empty reportRecord && reportRecord == '1'}">
																<c:set var="LastRecord" value="${selected='selected'}" />
															</c:when>
															<c:otherwise>
																<c:set var="All" value="${selected='selected'}" />
															</c:otherwise>
														</c:choose>
														<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Record</label></span>&nbsp;&nbsp; <select name="reportRecord" class="form-control" id="reportRecord"style="padding: 2px 10px;">
															<option value="0" ${All}>All</option>
															<option value="1" ${LastRecord}>Last Record</option>
														</select>
													</div>
													<input type="hidden" id="cmd" name="cmd"
														value="searchServerReport">
												</div>
											</form>


										</div>
									</div>
								</div>

								<div class="portlet-body">
									<div style="margin-top: 10px;">
										<table id="reportContent" class="table table-striped">
											<thead>
												<tr class="heading">
													<th style="text-align: center;">Server</th>
													<th style="text-align: center;">Type</th>
													<th style="text-align: center;">Start Date</th>
													<th style="text-align: center;">End Date</th>
													<th style="text-align: center;">Worktime(hr/mm)</th>
													<th style="text-align: center;">Status</th>
													<th style="text-align: center;">info</th>
												</tr>
											</thead>
											<tbody>
												<c:if test="${not empty reportList}">
													<c:forEach items="${reportList}" var="report">
														<tr class='rowPattern'>
															<td class=showServer data-element-server="${report.server}"name="showServer" style='text-align: center;'><c:out value="${report.server}" /></td>
															<td class=showType data-element-type="${report.type}"name="showType"style='text-align: center;'><c:out value="${report.type}" /></td>
															<td style='text-align: center;'><c:choose>
																	<c:when test="${not empty report.startDate}">${DateTimeUtil.generateStringDisplayDateTime(report.startDate)}</c:when>
																	<c:otherwise> - </c:otherwise>
																</c:choose></td>
															<td style='text-align: center;'><c:choose>
																	<c:when test="${not empty report.endDate}">${DateTimeUtil.generateStringDisplayDateTime(report.endDate)}</c:when>
																	<c:otherwise> - </c:otherwise>
																</c:choose></td>
															<td style='text-align: center;'><c:choose><c:when
																		test="${not empty report.startDate && not empty report.endDate}">${DateTimeUtil.calcurateHourDiff(report.startDate, report.endDate)}</c:when>
																	<c:otherwise> - </c:otherwise>
																</c:choose></td>
															<c:choose>
																<c:when test="${not empty report.status && report.status == 'DONE'}"><c:set var="statusColor" value="${'#ABEBC6'}" /><c:set var="status" value="PASSED" /></c:when>
																<c:when test="${not empty report.status && report.status == 'FAILED'}"><c:set var="statusColor" value="${'#EC7063'}" /><c:set var="status" value="FAILED" /></c:when>
																<c:when test="${not empty report.status && report.status == 'RUNNING'}"><c:set var="statusColor" value="${'#AED6F1'}" /><c:set var="status" value="RUNNING" /></c:when>
																<c:when test="${not empty report.status && report.status == 'WARNING'}"><c:set var="statusColor" value="${'#ABB2B9'}" /><c:set var="status" value="PASSED" /></c:when>
															</c:choose>
															<td style='text-align: center; border-radius: 10px;'
																bgcolor="${statusColor}"><c:out value="${status}" />
																<c:if
																	test="${report.msgData != null && report.msgData != ''}">
																	<a class='glyphicon glyphicon-info-sign'data-toggle='tooltip' data-placement='right'style="text-decoration: none;"title="<c:out value="${report.msgData}"></c:out>"></a>
																</c:if></td>
															<td style='text-align: center;'><a class=idRefServers data-element-id="${report.id}"name="idRefServers" data-value="${sessionScope.Permission.containsKey('all') }"><button class="btn btn-info" >→</button></a></td>
														</tr>
													</c:forEach>
												</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
					<!--  paging -->
						<div class="row" style="float: right;">
							<div class="col-md-12">
								<c:if test="${not empty pagingBean}">
									<c:set var="pagingBean" value="${pagingBean}" />
									<div class="dataTables_paginate paging_bootstrap">
										<ul class="pagination">
											<c:choose>
												<c:when test="${pagingBean.getCurrentPage() == 1}">
													<li class="prev disabled"><a href="#">← Previous</a></li>
												</c:when>
												<c:otherwise>
													<li class="prev"><a
														href="<%=contextPath%>/admin/serversruntimereport&page=${pagingBean.getCurrentPage()-1}&reportType=${reportType}&reportStatus=${reportStatus}&reportServer=${reportServer}&reportTime=${reportTime}&reportRecord=${reportRecord}">←
															Previous</a></li>
												</c:otherwise>
											</c:choose>
											<c:forEach items="${pagingBean.getPagingList()}" var="pageNo">
												<c:choose>
													<c:when
														test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
														<li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
													</c:when>
													<c:when test="${pageNo.equals('...')}">
														<li><a href="#">...</a></li>
													</c:when>
													<c:otherwise>
														<li><a
															href="<%=contextPath%>/admin/serversruntimereport&page=${pageNo}&reportType=${reportType}&reportStatus=${reportStatus}&reportServer=${reportServer}&reportTime=${reportTime}&reportRecord=${reportRecord}">${pageNo}</a></li>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											<c:choose>
												<c:when
													test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
													<li class="next disabled"><a href="#">Next → </a></li>
												</c:when>
												<c:otherwise>
													<li class="next"><a
														href="<%=contextPath%>/admin/serversruntimereport&page=${pagingBean.getCurrentPage()+1}&reportType=${reportType}&reportStatus=${reportStatus}&reportServer=${reportServer}&reportTime=${reportTime}&reportRecord=${reportRecord}">Next
															→ </a></li>
												</c:otherwise>
											</c:choose>
										</ul>
									</div>
								</c:if>
							</div>
						</div>
						<!--  End paging -->
					</div>
					</div>
					<!-- END Server Report TABLE -->
					<!--------------------------End Server Report----------------------------------------------------------------------------------->
<!--------------------------Start Merchant In Server Report----------------------------------------------------------------------------------->
					<div class="row" id="showMerchantReport">
						<div class="col-md-12">
							<div class="portlet light bordered">
							<!-- <button id="buttonback" class="btn btn-default" type="button" style="margin-left: 0px">BACK</button> -->
								<div class="portlet-title">
									<div class="caption">
										<div class="col-md-12">
											<div class="col-md-10" style="margin-top: 12px; width: 100%; display: inline;">
												<h3 id="group-productName">
													<!-- <button id="buttonback" class="btn btn-default btn-lg" type="button" style="margin-right: 5px"><i class="fa fa-chevron-left"></i></button> -->
													<button id="buttonback" class="btn btn-default btn-lg" type="button" style="margin-right: 5px; color: #fff; background: #d8691e">  BACK </button>
													<span class="txtTitle">Report Table</span>
												</h3>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-10" id="topBar">
											<h4 class="txtResult">
												<span>All Report result record</span>
											</h4>
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-6">
										<div class="input-group">
											<input type="hidden" id="searchMerchantRef" name="searchMerchantRef" value=''> 
											<input type="hidden" id="pageCurrent" value="0" /> 
											<input type="hidden" id="cmd" name="cmd" value="searchMerchantinServerReport">
										</div>
									</div>
								</div>
								<div class="portlet-body">
									<div style="margin-top: 10px;">
										<table id="reportContentMerchant"class="table table-striped table-bordered" style="text-align: center;">
											<thead>
												<tr class="heading">
													<th width="10%" style="text-align: center;">Merchant Id</th>
													<th width="20%" style="text-align: center;">Merchant Name</th>
													<th width="3%" style="text-align: center;">Package</th>
													<c:if test="${sessionScope.Permission.containsKey('all') }">
														<th width="3%" style="text-align: center;">Count Old</th>
														<th width="3%" style="text-align: center;">Count New</th>
														<th width="3%" style="text-align: center;">Success Count</th>
														<th width="3%" style="text-align: center;">Error Count</th>
														<th width="3%" style="text-align: center;">Add</th>
														<th width="3%" style="text-align: center;">Update</th>
														<th width="3%" style="text-align: center;">Update Picture</th>
														<th width="3%" style="text-align: center;">Delete</th>
													</c:if>
													<th width="11%" style="text-align: center;">Start Date</th>
													<th width="11%" style="text-align: center;">End Date</th>
													<th width="11%" style="text-align: center;">Run Time(hr/mm)</th>
													<th width="10%" style="text-align: center;">Status</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!--  start paging -->
						<div class="row" style="float: right; padding-bottom: 20px;">
							<div class="col-md-12">
								<div class="dataTables_paginate paging_bootstrap">
									<ul class='paginationMerchant pagination' style='margin-right: 0px;'></ul>
								</div>
							</div>
						</div>
						<!--  end paging -->
					</div>
					<!--------------------------End Merchant In Server Report------------------------------------------------------------------------------------->
				

					</div>
			</div>
	</div>
</div>



	<script type="text/javascript"
		src="<%=contextPath%>/assets/pages/js/serverreport-script.js"></script>

</body>
</html>