<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var ="contextPath" value="${pageContext.servletContext.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	
	<style type="text/css">
	.modal-confirm {		
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}	
	.modal-confirm .icon-box {
		color: #fff;		
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm  .danger{
		background: #ef513a;
	}
	.modal-confirm  .success{
		background: #82ce34;
	}
	
	.modal-confirm  .warning{
		background: #FCCB46;
	}
	
	.modal-confirm .icon-box i {
		font-size: 56px;
		position: relative;
		top: 4px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
     .modal-confirm .btn-danger {
		background: #ef513a;
    }
     .modal-confirm .btn-success {
		background: #82ce34;
    }
    
    .modal-confirm .btn-dark{
		background: #6c757d;
		color: #343a40;
	}
	.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
		background: #da2c12;
		outline: none;
	}
	.modal-confirm .btn-success:hover, .modal-confirm .btn-success:focus {
		background: #6fb32b;
		outline: none;
	}
	.modal-confirm .btn-dark:hover, .modal-confirm .btn-dark:focus {
		background: #343a40;
		color: #fff;
		outline: none;
	}

	.modal-confirm .modal-detail {
		font-size: 15px;
		margin-bottom: 20px;
	}
	.row {
	    margin-left: -15px;
	    margin-right: -15px;
	    margin-bottom: 20px;
	}

	.inputclear {
	    position: absolute;
	    right: 20px;
	    margin: 10px 5px 20px 20px;
	    font-size: 14px;
	    cursor: pointer;
	    color: #ccc;
	    display:none;
	}
	.form-control{
		padding: 6px 30px 6px 12px;
	}
	.modal .modal-content .modal-title {
	 	margin-bottom: 10px;
	}

</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
  		
				
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-file" style="padding-right: 10px;"></i> Crawler Checking</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Crawler Check</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
							<form id="dataForm" role="form" data-toggle="validator" onsubmit="return false;">
								<div class="portlet light bordered">
									<div style="margin-left: 10px; margin-right: 10px;">
										<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="url">URL(Catergory Link) </label>&nbsp;<span class="text-danger">*</span> :
	                                            </div>
	                                            <div class="col-xs-9">
	                                            	<input type="text" id="url" name="url"  class="form-control clearbutton" placeholder="https://www.google.co.th">
	                                            	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                            </div>
	                                            <a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ลิงค์หน้้าหมวดสินค้า"></a>
                                        	</div>
                                      	</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="productUrl">Product URL </label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                        	<input type="text" id="productUrl" name="productUrl"  class="form-control clearbutton" placeholder="https://www.google.co.th/product/gg">
		                                        	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
		                                        </div>
		                                        <a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ลิงค์หน้าสินค้า"></a>
	                                         </div>
                                        </div>
                                      	<div class="row">
                                      		<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="productName">Product Name</label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                            <input type="text" id="productName" name="productName" class="form-control clearbutton" placeholder="Ex. Google product">
		                                        	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
		                                        </div>
		                                        <a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ชื่อสินค้า"></a>
		                                    </div>
		                               </div>
		                               <div class="row">
                                        	<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="productPrice">Price</label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                            <input type="text" id="productPrice" name="productPrice" class="form-control clearbutton" placeholder="Ex. 78,888">
	                                        		<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        	</div>
	                                        	<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ราคาสินค้า"></a>
	                                        </div>
                                        </div>
                                      	<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="nextPageUrl">NextPage URL </label> :
	                                            </div>
		                                        <div class="col-xs-9">
		                                            <input type="text" id="nextPageUrl" name="nextPageUrl"  class="form-control clearbutton" placeholder="https://www.google.co.th?page=2">
	                                        		<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        	</div>
	                                        	<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ลิงค์เปลี่ยนหน้าสินค้า"></a>
                                        	</div>
                                      	</div>
                                      	<div class="row">
                                     		<div class="col-xs-11" align="right" style="margin: 10px">
							      				<input type="hidden" id="cmd" name="cmd" value="runTest">
									   			<button type="button" id="dataFormReset" class="btn btn-primary">Clear</button>
									 			<button type="submit" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">Submit</button>
											 </div>
									 	</div>
								   </div>	
								</div>
							</form> 
						</div>				
					</div>
		            <!-- END CONTENT -->
			
			<!-- Modal HTML -->
			<div id="responseModal" class="modal fade">
				<div class="modal-dialog modal-confirm">
					<div class="modal-content">
					</div>
				</div>
			</div>     									
		</div>								
  	</div>
  	<input type="hidden" id="contextPath" name="contextPath" value="${contextPath}">
  	<script type="text/Javascript" src="${contextPath}/assets/pages/js/crawler-checking-script.js"></script>
		
</body>
</html>