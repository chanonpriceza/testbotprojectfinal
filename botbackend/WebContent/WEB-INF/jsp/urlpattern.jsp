<%@ page import="botbackend.bean.UserBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%String contextPath = request.getContextPath();%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
</head>
<style>
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}
table td.highlighted {
  background-color:#ccc;
}

/* #urlPatternTable tr:hover {
     background-color: #e5e5e5;
} */
		#topRunPattern{
			display: block;
			z-index: 9999;
			position: fixed;
			bottom: 15px;
			right: 15px;
		}
		
		
		#topRunParser > div{
			z-index: 999
		}
		
		.greenButton{
			color: #fff;
			background-color: #5cb85c;
			border-color : #4cae4c;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		
		.buttonAnalysis{
			color: #fff;
			background-color: #8e44ad;
			border-color : #8e44ad;
			margin-bottom : 10px;
		}
		
		.orangeButton{
			color: #fff;
			background-color: #ffb266;
			border-color : #4cae4c;
			min-width: 85px;
		}
		
		input[type=checkbox].rowCheck {
  			transform: scale(1.5);
		}
		
		div.tab {
		    overflow: hidden;
/* 		    border: 1px solid #ccc; */
/* 		    background-color: #f1f1f1; */
		}
		
		div.tab button {
		    float: left;
		    border: none;
		    outline: none;
		    cursor: pointer;
		    padding: 10px 20px;
		    transition: 0.3s;
		    border: 1px solid #ccc;
		    background-color: #f1f1f1;
		}
		
		div.tab button:hover {
		    background-color: #ddd;
		}
		
		div.tab button.active {
		    background-color: #ccc;
		}
		
		.tabContent {
 		    display: none; 
		    padding: 6px 12px;
		    border: 1px solid #ccc;
		    height: 600px;
    		overflow-y: scroll;
    		width: 100%;
		}
		
		div.runtime {
			float: right;
			position: relative;
			background-color: #292929;
			color: #eee;
			padding: 7px 10px;
			border: 1px solid #292929;
		}
		.file-upload {
  background-color: #ffffff;
  width: 600px;
  margin: 0 auto;
  padding: 20px;
}

.file-upload-btn {
  width: 100%;
  margin: 0;
  color: #fff;
  background: #1FB264;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #15824B;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.file-upload-btn:hover {
  background: #1AA059;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.file-upload-btn:active {
  border: 0;
  transition: all .2s ease;
}

.file-upload-content {
  display: none;
  text-align: center;
}

.file-upload-input {
  position: absolute;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  outline: none;
  opacity: 0;
  cursor: pointer;
}

.file-upload-wrap {
  border: 3px dashed #b3e0c8;
  position: relative;
  width: 80%;
  height: 80px;
}

.image-dropping,
.file-upload-wrap:hover {
  background-color: #b3e0c8;
  border: 3px dashed #ffffff;
}

.image-title-wrap {
  padding: 0 15px 15px 15px;
  color: #222;
}

.drag-text {
  text-align: center;
}

.drag-text h4 {
  font-weight: 100;
  text-transform: uppercase;
  padding: 18px 0;
}

.file-upload-csv {
  max-height: 200px;
  max-width: 200px;
  margin: auto;
  padding: 20px;
}

.remove-image {
  width: 200px;
  margin: 0;
  color: #fff;
  background: #cd4535;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #b02818;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.remove-image:hover {
  background: #c13b2a;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.remove-image:active {
  border: 0;
  transition: all .2s ease;
}
</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>	
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
			<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->+
                     <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom:2%;">
                    	<div class="col-md-6">
		                    	<h3 class="page-title"><i class="fa fa-wrench" style="padding-right: 10px;"></i> Manage Pattern </h3>
		                </div>
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="dashboard">Dashboard</a> > <a href="#">URL Pattern</a> <i class="fa fa-circle"></i>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                   	<!-- BEGIN HEADER SEARCH BOX -->
                    <div class="row">
	                    <div class="col-md-12">
	                    	 <div class="col-md-9" style="margin: 0 0 25px;">
			                    <div class="input-group">
			                        <input type="text" id="merchantId" class="form-control" placeholder="Search by merchant ID..." >
			                        <span class="input-group-btn">
			                            <a id="search" class="btn btn-primary submit" onclick="getDataByMerchantId()">
			                                <i class="icon-magnifier"></i>
			                            </a>
			                        </span>
			                    </div>
		                    </div>
	                    </div>
                    </div>
	                <!-- END HEADER SEARCH BOX -->
                 	<!-- START CONTENT -->
                 	<!-- START URL PATTERN TABLE -->
                    <div class="row" id="urlPatternContent" style="display:none;">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                            	<div class="portlet-title">
<!--                             		<div class="row"> -->
<!--                             			<div class="col-md-12"> -->
<!--                             				<div class="col-md-10" style="margin-top:12px;"> -->
                           					<h4 id="group-titleName"><span>URL&nbsp;Pattern&nbsp;Table <span id="targetMerchantIdTxt"></span></span></h4>
<!--                             				</div> -->	
<!-- 	                            		</div> -->
<!-- 									</div> -->
									
                            	</div>
                                <div class="portlet-body">
                                	<div class="row">
										<form name="importPattern" id="importPatternForm" action="javascript:;" enctype="multipart/form-data" method="post" accept-charset="utf-8">

										<div class="col-md-12">
											<div class="file-upload-wrap">
										    	<input class="file-upload-input" type="file" id="inputPatternFile" name="inputPatternFile"/>
											     <div class="drag-text">
											     	<h4>Click or Drag & Drop file CSV.</h4>
											     </div>
									     	</div>
										    <div class="file-upload-content">
											  	<div class="row">
											  		<i class="file-upload-csv fa fa-file fa-2x"></i>
											  		<span class="image-title"></span>
											  	</div>
											  	<div class="row">
												  <button type="button" onclick="removeUpload()" class="btn btn-danger">Remove</button>		
												  <button type="submit" class="btn btn-primary" id="submitImportPattern" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading"><i class="fa fa-upload"></i>Upload</button>							  	
											  	</div>
										  	</div>	
										</div>
										</form>
									</div>
                                    <div>
                                    	<button class="btn btn-primary" id="exportPattern" onclick="exportPattern()" style="float: right;margin-bottom: 5px"><i class="fa fa-download"></i>DOWLOAD</button>
                                        <table id="urlPatternTable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr class="heading">
                                                	<th width='7%'  style="text-align:center;"> Condition </th>
                                                	<th width='5%'  style="text-align:center;"> Group </th>
                                                	<th width='33%' style="text-align:center;"> Value </th>
                                                    <th width='8%'  style="text-align:center;"> Action </th>
                                                    <th width='5%'  style="text-align:center;"> CatId </th>
                                                    <th width='26%' style="text-align:center;"> Keyword </th>
                                                    <th width='4%'  style="text-align:center;"> Test </th>
                                                    <th width='12%' style="text-align:center;"> Command </th>
                                               	</tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                            	<tr class="footing">
                                            		<td colspan="8" style="text-align:center;">
                                            			<button type="button" class="btn btn-info btn-lg" onclick="addPattern()">Add Row</button> 
                                            			<button type="button" class="btn btn-info btn-lg" onclick="saveTable()" id="saveTableBtn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</button> 
		                                           		<input type="hidden" id="tableMerchantId" name="tableMerchantId" value="0" />
                                            			<div id="saveResult"></div>
                                            		</td>
                                            	</tr>
                                           	</tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>	
                        </div>
                	</div>
                	<!-- END URL PATTERN TABLE -->
                	
                	<div class="modal fade" id="modalRun">
    					<div class="modal-dialog" style="width: 65%;">
    						<div class="modal-content">
	        					<div class="modal-header">
	          						<button type="button" class="close" data-dismiss="modal">&times;</button>
	          						<h4 id="titleModalUrl" class="modal-title">Run URL Pattern Test</h4>
	        					</div>
	       						<div class="modal-body" id="modalData"></div>
	    						<div class="modal-footer">
	    							<div id="groupBtnCount">
	    								<span id="countAll"></span>
	    							</div>
	          						<div id="showBtnGenerate" style="text-align: center;">
	    								<button type="button" class="btn btn-warning" data-dismiss="modal" id="btnGenerateContinue">Generate Continue</button>
	    								<button type="button" class="btn btn-success" data-dismiss="modal" id="btnGenerateMatch">Generate Match</button>
	    							</div>
	        					</div>
    						</div>
    					</div>
    				</div>
    				
                	<div class="modal fade" id="modalGenerate">
    					<div class="modal-dialog" style="width: 65%;">
    						<div class="modal-content">
	        					<div class="modal-header">
	          						<button type="button" class="close" data-dismiss="modal">&times;</button>
	          						<h4 id="titleModalUrl" class="modal-title">Run Generate Url</h4>
	        					</div>
	       						<div class="modal-body" id="modalDataGenerate"></div>
	    						<div class="modal-footer">
	    							<div id="showbtnAddValueGenerate" style="text-align: center;">
	    								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    								<button type="button" class="btn btn-success" data-dismiss="modal" id="btnAddValueGenerate" onclick='selectValueinTable();'>Add</button>
	    							</div>
	        					</div>
    						</div>
    					</div>
    				</div>
                	
                	<div class="modal fade" id="modalAlert">
    					<div class="modal-dialog" style="width: 30%;">
    						<div class="modal-content">
	        					<div class="modal-header">
	          						<button type="button" class="close" data-dismiss="modal">&times;</button>
	          						<h4 class="modal-title" style="color:#FFC300;" >Alert</h4>
	        					</div>
	       						<div class="modal-body" id="modalAlertData"></div>
	    						<div class="modal-footer">
	          						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        					</div>
    						</div>
    					</div>
    				</div>
    				
                	<div id="topRunPattern">
                		<div>  					
    						<button type="button" class='btn orangeButton' id="btnTopSave" onclick="saveTable()" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</button>
    					</div>
    					<div>
    						<button type='button' class='btn greenButton'  id="btnTopRunTest" onclick='runTestPattern();'>Run Test</button>
						</div>
						<div>
    						<button type='button' class='btn buttonAnalysis'  id="btnTopUrlAnalysis" onclick='runUrlAnalysis();'>Url analysis</button>
						</div>
    				</div>
    				<input type="hidden" id="hdMerchantId">
   	                <!-- END CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            <!-- END CONTENT -->
		</div>
	</div>
	
	<script type="text/JavaScript" src="<%=contextPath %>/assets/pages/js/urlpattern-script.js"></script>
	
</body>
</html>
