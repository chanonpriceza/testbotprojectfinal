<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% 
	String contextPath = request.getContextPath(); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	
	<style type="text/css">
	.modal-confirm {		
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}	
	.modal-confirm .icon-box {
		color: #fff;		
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm  .danger{
		background: #ef513a;
	}
	.modal-confirm  .success{
		background: #82ce34;
	}
	
	.modal-confirm  .warning{
		background: #FCCB46;
	}
	
	.modal-confirm .icon-box i {
		font-size: 56px;
		position: relative;
		top: 4px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
     .modal-confirm .btn-danger {
		background: #ef513a;
    }
     .modal-confirm .btn-success {
		background: #82ce34;
    }
	.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
		background: #da2c12;
		outline: none;
	}
	.modal-confirm .btn-success:hover, .modal-confirm .btn-success:focus {
		background: #6fb32b;
		outline: none;
	}

	.modal-confirm .modal-detail {
		font-size: 15px;
		margin-bottom: 20px;
	}
	.row {
	    margin-left: -15px;
	    margin-right: -15px;
	    margin-bottom: 20px;
	}

	.inputclear {
	    position: absolute;
	    right: 20px;
	    margin: 10px 5px 20px 20px;
	    font-size: 14px;
	    cursor: pointer;
	    color: #ccc;
	    display:none;
	}
	.form-control{
		padding: 6px 30px 6px 12px;
	}
	.modal .modal-content .modal-title {
	 	margin-bottom: 10px;
	}
.file-upload-input {
	  position: absolute;
	  margin: 0;
	  padding: 0;
	  width: 100%;
	  height: 100%;
	  outline: none;
	  opacity: 0;
	  cursor: pointer;
	}
	.file-upload-wrap {
		  border: 3px dashed #b3e0c8;
		  position: relative;
		  height: 80px;
	}
	
	.image-dropping,
	.file-upload-wrap:hover {
	  background-color: #b3e0c8;
	  border: 3px dashed #ffffff;
	}
	.image-title-wrap {
	  padding: 0 15px 15px 15px;
	  color: #222;
	}
	
	.drag-text {
	  text-align: center;
	}
	
	.drag-text h4 {
	  font-weight: 100;
	  text-transform: uppercase;
	  padding: 18px 0;
	}
	
	.file-upload-csv {
	  max-height: 200px;
	  max-width: 200px;
	  margin: auto;
	  padding: 20px;
	}
	
	.remove-image {
	  width: 200px;
	  margin: 0;
	  color: #fff;
	  background: #cd4535;
	  border: none;
	  padding: 10px;
	  border-radius: 4px;
	  border-bottom: 4px solid #b02818;
	  transition: all .2s ease;
	  outline: none;
	  text-transform: uppercase;
	  font-weight: 700;
	}
	
	.remove-image:hover {
	  background: #c13b2a;
	  color: #ffffff;
	  transition: all .2s ease;
	  cursor: pointer;
	}
	
	.remove-image:active {
	  border: 0;
	  transition: all .2s ease;
	}
	.file-upload-content {
  		display: none;
 	 	text-align: center;
	}
	table, th, td {
  		border: 1px solid black;
  		text-align:center;
  		padding: 5px;
	}	
	td.left{
		text-align:left;
	}
	#resultTable tr:hover {
     background-color: #e5e5e5;
}

</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
  		
				
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-ban" aria-hidden="true"></i> Check BanKeyWord</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Crawler Check</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                    <form name="importPattern" id="importCSVForm" action="javascript:;" enctype="multipart/form-data" method="post" accept-charset="utf-8" style="display:none">
								<div class="col-md-12 portlet light bordered">
								<div class="row">
									<div class="col-md-10">
										<button type="button" id="showInputMode" class="btn btn-success" style="margin-bottom:11px;margin-top:11px">Input Mode</button>
									</div>
									<div class="col-md-2">
										<a class="btn btn-success" style="background-color: #9d00ff;margin-top:11px" href="${contextPath}/botbackend/admin/getBanKeywordTemplateCSV"><i class="fa fa-download"></i>Template CSV</a>
									</div>
								</div>
									<div class="file-upload-wrap">
								    	<input class="file-upload-input" type="file" id="inputPatternFile" name="inputPatternFile"/>
									     <div class="drag-text">
									     	<h4>Click or Drag & Drop file CSV.</h4>
									     </div>
							     	</div>
								    <div class="file-upload-content">
									  	<div class="row">
									  		<i class="file-upload-csv fa fa-file fa-2x"></i>
									  		<span class="image-title"></span>
									  	</div>
									  	 <div class="row">
										  <button type="button" onclick="removeUpload()" class="btn btn-danger">Remove</button>		
										  <button type="submit" class="btn btn-primary" id="submitImportPattern" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading"><i class="fa fa-upload"></i>Upload</button>							  	
									  	</div>
								  	</div>	
								</div>
								<div id="displayTable" class="col-md-12 portlet light bordered" style="display:none">
								    <div class="file-upload-content">
									  	<div class="row" style="margin-top:20px">
									  		<table width="1200" align="center" id="resultTable">
											<tr>
												<th>ProductName</th>
												<th>MerchantId</th>
												<th>Price</th>
												<th>CategoryId</th>
												<th>Result</th>
												<th>Detail</th>
											</tr>
										</table>
									  	</div>
								  	</div>	
								</div>
							</form>
							<form id="dataForm" role="form" data-toggle="validator" onsubmit="return false;">
								<div class="portlet light bordered">
									<div style="margin-left: 10px; margin-right: 10px;">
										<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="word"> Check Keyword </label>&nbsp;<span class="text-danger">*</span> :
	                                            </div>
	                                            <div class="col-xs-9">
	                                            	<input type="text" id="word" name="word"  class="form-control clearbutton" placeholder="ไฟแช็ค">
	                                            	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                            </div>
	                                            <a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="คำที่ต้องการจะตรวจสอบ"></a>
                                        	</div>
                                      	</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="merchantId">MerchantID </label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                        	<input type="number" id="merchantId" name="merchantId"  class="form-control clearbutton" placeholder="70045">
		                                        	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
		                                        </div>
	                                         </div>
                                        </div>
                                      	<div class="row">
                                      		<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="price">Price</label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                            <input type="text" id="price" name="price" class="form-control clearbutton" placeholder="15">
		                                        	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
		                                        </div>
		                                      
		                                    </div>
		                               </div>
		                               <div class="row">
                                        	<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="categoryId">CategoryID</label> :
	                                            </div>
	                                            <div class="col-xs-9">
		                                            <input type="text" id="categoryId" name="categoryId" class="form-control clearbutton" placeholder="Ex. 78,888">
	                                        		<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                        	</div>
	                                        </div>
                                        </div>
                                      	<div class="row">
                                      		<div class="col-xs-1" style="margin: 10px">
                                      		 	<button type="button" id="showCSV"  class="btn" style="background-color: #5cb85c;border-color: #4cae4c;color:white">CSV MODE</button>
                                      		</div>
                                     		<div class="col-xs-10" align="right" style="margin: 10px">
							      				<input type="hidden" id="cmd" name="cmd" value="runTest">
									   			<button type="button" id="dataFormReset" class="btn btn-primary">Clear</button>
									 			<button type="submit" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">Submit</button>
											 </div>
									 	</div>
								   </div>	
								</div>
							</form> 
						</div>				
					</div>
		            <!-- END CONTENT -->
			
			<!-- Modal HTML -->
			<div id="responseModal" class="modal fade">
				<div class="modal-dialog modal-confirm">
					<div class="modal-content">
					</div>
				</div>
			</div>     									
		</div>								
  	</div>
  	<script type="text/Javascript" src="<%=contextPath %>/assets/pages/js/check-bankeyword-page.js"></script>
		
</body>
</html>