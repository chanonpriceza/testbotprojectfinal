<%@ page import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig, utils.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%
	String contextPath = request.getContextPath();
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<title>Bot Backend</title>

	<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />
	<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page="/WEB-INF/jsp/template/header.jsp" />
	
<!-- 	<div class="clearfix"></div>
	END HEADER & CONTENT DIVIDER
 -->
 
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="wrapper">
			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page="/WEB-INF/jsp/template/menuside.jsp" />

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					
					<!-- BEGIN PAGE BAR -->
					<div class="page-bar" style="margin-bottom: 2%;">
						<div class="col-md-6">
							<h3 class="page-title">
								<i class="fa fa-desktop" style="padding-right: 10px;"></i>
								Product Sync Waiting Qty
							</h3>
						</div>
						<ul class="page-breadcrumb">
							<li>
								<a href="dashboard">Dashboard</a> >  
								<a href="#"> Product Sync Waiting Qty</a>
								<i class="fa fa-circle"></i>
							</li>
						</ul>
					</div>
					<!-- END PAGE BAR -->
					
					<!-- START CONTENT -->
					<!-- START New Report TABLE -->
					<div class="row" id="productWaitingsyncContent">
						<div class="col-md-12">
							<div class="portlet light bordered">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-10" style="margin-top: 12px;">
											<h4 id="group-productName">
												<span>All merchant result <c:if
														test="${not empty totalRecord}">
														<fmt:formatNumber type="number" maxFractionDigits="3"
															value="${totalRecord}" />
													</c:if> record
												</span>
											</h4>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12" style="margin: 25px 0;">
										<div class="col-md-2">
											View By :
											<form id="search" action="<%=contextPath%>/admin/productwaitsyncsearch" method="post" style="display: inline-flex; padding: 2px 20px;">
												<c:choose>
													<c:when test="${not empty selected && selected == 'all'}">
														<c:set var="type" value="${selected}" />
														<c:set var="all" value="${selected='selected'}" />
													</c:when>
													<c:when test="${not empty selected && selected == 'date'}">
														<c:set var="type" value="${selected}" />
														<c:set var="date" value="${selected='selected'}" />
													</c:when>
												</c:choose>

												<select class="form-control" id="viewBy" name="viewBy">
													<option value="all" ${all}>All</option>
													<option value="date" ${date}>Date</option>
												</select>
												
												<input type="hidden" name="page" value="1">
											</form>
										</div>
									</div>
								</div>

								<div class="portlet-body">
									<div style="margin-top: 10px;">
										<table id="reportContent" class="table table-striped table-bordered">
											<thead>
												<tr class="heading">
													<th style="text-align: center;">Add Date</th>
													<th style="text-align: center;">Sync Date</th>
													<th style="text-align: center;">Server</th>
													<th style="text-align: center;">Total</th>
													<th style="text-align: center;">Sync Complete</th>
													<th style="text-align: center;">Remain</th>
													<th style="text-align: center;">Status</th>
												</tr>
											</thead>
											
											<tbody>
												<c:if test="${not empty productWaitSyncList}">
													<c:forEach items="${productWaitSyncList}" var="report">
														<tr class='rowPattern'>
															<td style='text-align: center;'>
																<c:out value="${DateTimeUtil.generateStringDisplayDate(report.addDate)}" />
															</td>
															<c:choose>
																<c:when test="${not empty report.syncDate}">
																	<td style='text-align: center;'><c:out
																	value="${DateTimeUtil.generateStringDisplayDate(report.syncDate)}" /></td>
																</c:when>
																<c:otherwise>
																	<td style="text-align: center;">-</td>
																</c:otherwise>
															</c:choose>
															<td style='text-align: center;'><c:out value="${report.dataServer}" /></td>
															<td style='text-align: center;'>
																<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.allCount}"/>
															</td>

															<c:choose>
																<c:when test="${report.done!=0}">
																	<td style='text-align: center;'>
																	<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.done}"/></td>
																</c:when>
																<c:otherwise>
																	<td style="text-align: center;">0</td>
																</c:otherwise>
															</c:choose>
															<c:choose>
																<c:when test="${report.done!=0}">
																	<td style='text-align: center;'>
																	<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.allCount-report.done}"/></td>
																</c:when>
																<c:otherwise>
																	<td style='text-align: center;'>
																		<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.allCount}"/>
																	</td>
																</c:otherwise>
															</c:choose>
															
															<c:choose>
 																<c:when test="${report.status=='C' && report.allCount > 0}">
																	<td style="text-align: center; border-radius: 10px;" bgcolor="#ABEBC6">
																		<fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${report.done/report.allCount*100}"/>%
																	</td>
																</c:when>
																<c:when test="${report.status=='W' && report.allCount > 0}">
																	<td style="text-align: center; border-radius: 10px;" bgcolor="#AED6F1">
																		<fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${report.done/report.allCount*100}"/>%
																	</td>
																</c:when>
																<c:otherwise>
																	<td style="text-align: center; border-radius: 10px;" bgcolor="#ABB2B9">100%</td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  paging -->
					<div class="row" style="float: right; padding-bottom: 20px;">
						<div class="col-md-12">
							<c:if test="${not empty pagingBean}">
								<c:set var="pagingBean" value="${pagingBean}" />
								<div class="dataTables_paginate paging_bootstrap">
									<ul class="pagination">
										<c:choose>
											<c:when test="${pagingBean.getCurrentPage() == 1}">
												<li class="prev disabled"><a href="#">← Previous</a></li>
											</c:when>
											<c:otherwise>
												<li class="prev"><a
													href="<%=contextPath%>/admin/productwaitsyncsearch?page=${pagingBean.getCurrentPage()-1}&viewBy=${type}">←
														Previous</a></li>
											</c:otherwise>
										</c:choose>
										<c:forEach items="${pagingBean.getPagingList()}" var="pageNo">
											<c:choose>
												<c:when
													test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
													<li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
												</c:when>
												<c:when test="${pageNo.equals('...')}">
													<li><a href="#">...</a></li>
												</c:when>
												<c:otherwise>
													<li>
													<a href="<%=contextPath%>/admin/productwaitsyncsearch?page=${pageNo}&viewBy=${type}">${pageNo}</a>
													</li>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										<c:choose>
											<c:when
												test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
												<li class="next disabled"><a href="#">Next → </a></li>
											</c:when>
											<c:otherwise>
												<li class="next"><a
													href="<%=contextPath%>/admin/productwaitsyncsearch?page=${pagingBean.getCurrentPage()+1}&viewBy=${type}">Next
														→ </a></li>
											</c:otherwise>
										</c:choose>
									</ul>
								</div>
							</c:if>
						</div>
					</div>
					<!--  End paging -->

					<!-- END New Report TABLE -->
					<!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
	
<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/product-wait-sync-script.js"></script>
	
</body>
</html>