<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="botbackend.bean.UserBean, botbackend.system.BaseConfig"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% 
	String contextPath = request.getContextPath(); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend : Manage Parser</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	<style type="text/css">
		 #tabArea > .tab  button:hover {
		    background-color: #ddd;
		}
		
		#tabArea > .tab button.active {
		    background-color: #ccc;
		}
		#buttonResult:hover{
			display: block;
    		margin-top: 0;
		}
		.borderedModal{
		    margin: 0px;
    		padding: 5px;
    		border: 1px solid #a8b1ab;
		}
		
		.btn-modalTab {
			width: 100%;
	    	background-color: #a5e2be;
		}
		.btn-modalTab:active {
	    	background-color: #7dcea0;
		}
		.btn-modalTab:hover {
	    	background-color: #7dcea0;
		}
		  .btn-circle {
		  width: 30px;
		  height: 30px;
		  text-align: center;
		  padding: 6px 0;
		  font-size: 12px;
		  line-height: 1.428571429;
		  border-radius: 15px;
		}
		.btn-circle.btn-lg {
		  width: 50px;
		  height: 50px;
		  padding: 10px 16px;
		  font-size: 18px;
		  line-height: 1.33;
		  border-radius: 25px;
		}
		.btn-circle.btn-xl {
		  width: 70px;
		  height: 70px;
		  padding: 10px 16px;
		  font-size: 24px;
		  line-height: 1.33;
		  border-radius: 35px;
		}

		.numberCircle {
		  border-radius: 50%;
		  behavior: url(PIE.htc);
		  /* remove if you don't care about IE8 */
		  width: 32px;
		  height: 32px;
		  padding: 6px;
		  background: #fff;
		  border: 2px solid #666;
		  color: #666;
		  text-align: center;
		  font: 14px Arial, sans-serif;
		  margin-left: 5px;
    	  margin-right: 5px;
		}
		.tbldisable{
			pointer-events: node;
			opacity: 0.4;
		}
		
		.label-field{
			margin-top: 9px;
			font-weight: 600;
			white-space: nowrap;
		}
		
		.textUrl, .productLinkText{
			font-weight: 600;
			line-height: 34px;
			display: inline-flex;
		}
		
		.darkGreenButton{
			color: #fff;
			background-color: #00E09C;
			border-color : #007C56;
			min-width: 85px;
		}
		
		.row{
			margin-top: 10px;
		}
		
		.table > tbody > tr > td{
			padding: 5px;
		}
		ุ
		.containConfig > .row {
			border-top: 3px solid #eeeeee;
			padding-top: 10px;
		}
		
		.containConfig{
			margin-top: 20px;
		}
		
		#tool0AlertText,#tool1AlertText,#tool2AlertText{
			color: red;
			font-size: 14px;
			font-weight: 900;
			margin-right: 15px;
		}
		
		#merchantName > span{
			border: 1px solid #cccccc;
			margin-left: 20px;
			padding: 9px 14px 8px;
			font-size: 14px;
			font-weight: 600;
			border-radius: 2px;
		}
		
		#merchantName > span > span:not(:first-child){
			margin-left: 20px;
		}
		
		.iconSpin{
			font-size: 40px;
		}
		
		#topRunParser{
			display: block;
			z-index: 9999;
			position: fixed;
			bottom: 15px;
			right: 10px;
		}
		
		#topRunParser > div{
			z-index: 999
		}
		
		#topRunParser > div:first-child{
			margin-bottom: 25px;
			margin-left: 40px;
		}
		
		#topButton{
			font-size: 2rem;
			background-color: #777;
			border-radius: 50%;
			width: 40px;
			height: 40px;
			color: #fff;
			text-align: center;
			margin-top: 10px;
			margin-left: 52px;
			float: right;
		}
		
		#topButton > i {
			line-height: 38px;
		}
		
		#topButton:hover {
			cursor: pointer;
		}
		
		#btnSearch{
			color: darksategray;
			font-size: 18px;
			background-color: #3399ff;
			height: 34px;
		}
		
		#btnSave{
			color: darksategray;
			font-size: 18px;
			background-color: #ffb266;
			height: 34px;
		}
		
		#modalRun .redValidate{
			font-weight: 600;
			color: red;
		}
			
		.tab{
			border-bottom: 1px solid #c2cad8;
		}
		
		div.tab .groupTabButton button {
		    float: left;
		    border: none;
		    outline: none;
		    cursor: pointer;
		    padding: 10px 20px;
		    transition: 0.3s;
		    border: 1px solid #ccc;
		    background-color: #f1f1f1;
		}
		
		div.tab .groupTabButton button:hover {
		    background-color: #ddd;
		}
		
		div.tab .groupTabButton button.active {
		    background-color: #ccc;
		}
		
		.rightGroupButton{
			background-color: transparent;	
			float: right;	
		}
		.design-process-section .text-align-center {
		    line-height: 25px;
		    margin-bottom: 12px;
		}
		.greenButton{
			color: #fff;
			background-color: #5cb85c;
			border-color : #4cae4c;
		}
		.greenButton:hover{
			color: #fff;
			background-color: #63ad63;
			border-color : #4cae4c;
		}
		
		.orangeButton{
			color: #fff;
			background-color: #ffb266;
			border-color : #4cae4c;
			min-width: 85px;
		}
		.orangeButton:hover{
			color: #fff;
			background-color: #eb923b;
			border-color : #4cae4c;
			min-width: 85px;
		}
.design-process-content {
/*     border: 1px solid #e9e9e9;
    position: relative;
    padding: 16px 34% 30px 30px; */
}
.design-process-content img {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 0;
    max-height: 100%;
}
.design-process-content h3 {
    margin-bottom: 16px;
}
.design-process-content p {
    line-height: 26px;
    margin-bottom: 12px;
}
.process-model {
    list-style: none;
    max-width: 600px;
    margin: 10px auto 10px;
    z-index: 10000;
    position: fixed;
    right: 0;
    left: 270px;
    bottom: 15px;
    width: 340px;
    zoom: 0.8;
    border: 2px solid #57b87b;
    border-radius: 12px;
    height: 110px;
    background-color: white;
    padding: 14px;
}
.process-model li::after {
    background: #e5e5e5 none repeat scroll 0 0;
    bottom: 0;
    content: "";
    display: block;
    height: 4px;
    margin: 0 auto;
    position: absolute;
    right: -30px;
    top: 33px;
    width: 85%;
    z-index: -1;
}
.process-model li.visited::after {
    background: #57b87b;
}
.process-model li:last-child::after {
    width: 0;
}
.process-model li {
    display: inline-block;
    width: 100px;
    text-align: center;
    float: none;
}
.nav-tabs.process-model > li.active > a, .nav-tabs.process-model > li.active > a:hover, .nav-tabs.process-model > li.active > a:focus, .process-model li a:hover, .process-model li a:focus {
    border: none;
    background: transparent;

}
.process-model li a {
    padding: 6px;
    border: none;
    color: #606060;
}
.process-model li.active,
.process-model li.visited {
    color: #57b87b;
}
.process-model li.active a,
.process-model li.active a:hover,
.process-model li.active a:focus,
.process-model li.visited a,
.process-model li.visited a:hover,
.process-model li.visited a:focus {
    color: #57b87b;
}
.process-model li.active p,
.process-model li.visited p {
    font-weight: 600;
}
.process-model li i.fa {
    display: block;
    height: 50px;
    width: 50px;
    text-align: center;
    margin: 0 auto;
    background: #f5f6f7;
    border: 2px solid #e5e5e5;
    line-height: 46px;
    font-size: 25px;
    border-radius: 50%;
}
.process-model li.active i, .process-model li.visited i  {
    background: #fff;
    border-color: #57b87b;
}
.process-model li p {
    font-size: 14px;
    margin-top: 11px;
}
.process-model.contact-us-tab li.visited a, .process-model.contact-us-tab li.visited p {
    color: #606060!important;
    font-weight: normal
}
.process-model.contact-us-tab li::after  {
    display: none; 
}
.process-model.contact-us-tab li.visited i {
    border-color: #e5e5e5; 
}



@media screen and (max-width: 560px) {
  .more-icon-preocess.process-model li span {
        font-size: 23px;
        height: 50px;
        line-height: 46px;
        width: 50px;
    }
    .more-icon-preocess.process-model li::after {
        top: 24px;
    }
}
@media screen and (max-width: 380px) { 
    .process-model.more-icon-preocess li {
        width: 16%;
    }
    .more-icon-preocess.process-model li span {
        font-size: 16px;
        height: 35px;
        line-height: 32px;
        width: 35px;
    }
    .more-icon-preocess.process-model li p {
        font-size: 8px;
    }
    .more-icon-preocess.process-model li::after {
        top: 18px;
    }
    .process-model.more-icon-preocess {
        text-align: center;
    }
}
	</style>
		
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>

	<div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="wrapper">
			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
		<!-- BEGIN CONTENT -->
	    <div class="page-content-wrapper">
	        <!-- BEGIN CONTENT BODY -->
	        <div class="page-content">
	            <!-- BEGIN PAGE HEADER-->
	            <!-- BEGIN PAGE BAR -->
	            <div class="page-bar" style="margin-bottom: 2%;">
	            	<div class="col-md-9">
          				<h3 class="page-title" style="white-space: nowrap;"><i class="fa fa-wrench" style="padding-right: 10px;"></i> Manage Parser Page<span id="merchantName"></span></h3>
          				<input type="hidden" id="parserClass" value=""/>
         			</div>
         			<div class="col-md-1">
         				<!-- <i class="fa fa-spinner fa-spin iconSpin" style="margin-top: 20px;"></i> -->
         			</div>
		            <ul class="page-breadcrumb">
		            	<li><a href="dashboard">Dashboard</a> > <a href="#">Parser</a> <i class="fa fa-circle"></i></li>
		            </ul>
	        	</div>

				<div class="row">
					<div class="col-md-12">
						<div class="box-tools">
							<div class="form-group">
								<div class="input-group">
									<input type="text" id="merchantId" class="form-control" placeholder="Fill Merchant ID">
									<div class="input-group-btn">
										<button type="button" class="btn btn-sm btn-default" id="btnSearch" onclick="search(true);">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="tabArea">
				<!-- START TAB -->
				<div class="tab">
				  <button id="pTab" data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้ในการสร้างรูปแบบการดึงสินค้าแบบ  Manual"></c:out>" class="btn tablinks" onclick="openTab('Parser')">Parser</button>
				  <button id="tTab" data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้ในการสร้างรูปแบบในการเก็บข้อมูลสินค้าแบบ  Automatic"></c:out>"class="btn tablinks" onclick="openTab('Tool')">Tool</button>
				  <button id="cTab" data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้ในการเปรียบเทียบการทำงานของ parser ที่ใช้งานอยู่ปัจจุบันกับข้อมูลในระบบ"></c:out>" class="btn tablinks" onclick="openTab('Compare')">Compare</button>
				</div>
				<!-- END TAB -->
				<!-- START PARSER PRODUCT DATA TABLE -->
				<div class="parserTab portlet light bordered" style="padding-bottom: 50px">
					    <div class="row">
					      <div class="col-xs-12"> 
					        <!-- Tab panes -->
					        <div class="tab-content">
					          	<div class="tabpanel" id="toolSubTab">
						            <div class="design-process-content">
						              
						               
						            </div>
					          	</div>
					          	<div class="tabpanel" id="parserSubTab">
										<div class="row">
											<div class="col-md-12 productLinkText">Step <div class="numberCircle">1</div>  Select Url</div>
										</div>
										<div class="row">
											<div id="rowUrl" class="col-md-10"></div>
										</div>
						            
							            <div class="containConfig">
								            <div class="row">
												<div class="col-md-12 productLinkText" style="margin-bottom: 10px">Step <div class="numberCircle">2</div>  Select Tag of product</div>
											</div>
							             		<div class="tab clearfix">
							             		  <div class="groupTabButton">
													  <button id="pName" class="btn tablinks" onclick="openTabParser('pName')">Name</button>
													  <button id="pPrice" class="btn tablinks" onclick="openTabParser('pPrice')">Price</button>
													  <button id="pDes" class="btn tablinks" onclick="openTabParser('pDes')">Decscript</button>
													  <button id="pPic" class="btn tablinks" onclick="openTabParser('pPic')">Picture url</button>
													  <button id="pRealproductAndConcat" class="btn tablinks" onclick="openTabParser('pRealproductAndConcat')">Real product id & Concat Word</button>
													  <button id="pUpc" class="btn tablinks" onclick="openTabParser('pUpc')">Upc</button>
													  <button id="pExpire" class="btn tablinks" onclick="openTabParser('pExpire')">Expire</button>
												  </div>
												  <div class="rightGroupButton">
												  	  <div id="buttonResult" class="btn-group dropup" style="float: left;margin-right: 10px">
													  	  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Result</button>
														  <ul class="dropdown-menu pull-right" role="menu" style="z-index: 1; min-width: 120px;"></ul>
													  </div>
													  <button type='button' class='btn orangeButton' id="btnSaveTest"    onclick='save();' style="margin-right: 10px">Save</button>
													  <button type='button' class='btn greenButton'  id="btnRunTest" onclick='runTest();'>Run Test</button>									
												  </div>
												</div>
						               </div>
					          	</div>
					      	</div>
					  </div>
				  </div>
				<div class="containConfig" style="margin-bottom: 1%">
				<!-- START NAME TAB-->
				<div class="pNameTab">
					 <div class="row" id="productNameContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkName" value="0">Product Name
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblname" class="table table-striped" val="name">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowname"></tbody>
		                        </table>
							</div>
		             </div>
				</div>
				<!-- END NAME TAB-->
				
				<!-- START PRICE TAB-->
				<div class="pPriceTab">
					 <div class="row" id="productPriceContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkPrice" value="0">Price
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblprice" class="table table-striped" val="price">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowprice"></tbody>
		                        </table>
							</div>
		             </div>
		             <div class="row" id="productBasePriceContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkBasePrice" value="0">Base Price
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblbasePrice" class="table table-striped" val="basePrice">
					            	<thead>
			                        	<tr class="heading">
			                        			<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
		                               		</tr>
			                         </thead>
			                         <tbody id="rowbasePrice"></tbody>
		                        </table>
							</div>
					</div>
				</div>
				<!-- END PRICE TAB-->
				
				<!-- START DESCRIPTION TAB-->
				<div class="pDesTab">
					 <div class="row" id="productDesContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkDescription" value="0">Description
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tbldescription" class="table table-striped" val="description">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowdescription"></tbody>
		                        </table>
							</div>
		             </div>
				</div>
				<!-- END DESCRIPTION TAB-->
				
				<!-- START PICTURE TAB-->
				<div class="pPicTab">
					 <div class="row" id="productPicContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkPictureUrl" value="0">Picture Url
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblpictureurl" class="table table-striped" val="pictureUrl">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowpictureUrl"></tbody>
		                        </table>
							</div>
		             </div>
				</div>
				<!-- END PICTURE TAB-->
				
				<!-- START RealproductAndConcat TAB-->
				<div class="pRealproductAndConcatTab">
					 <div class="row" id="productRealProductIDTabContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkRealProductId" value="0">Real Product ID
							</div>
								<div class="col-md-10" style="margin-left: 10%">
									<table id="tblrealproductId" class="table table-striped" val="realProductId">
							            	<thead>
					                        	<tr class="heading">
					                        		<th width="5%">Step</th>
					                                <th width="20%">Action</th>
					                                <th width="30%">Value</th>
					                                <th >Add / Delete</th>
					                                <th width="30%">Result</th>
				                               	</tr>
					                         </thead>
					                         <tbody id="rowrealProductId"></tbody>
			                        </table>
								</div>
		             </div>
		             <div class="row" id="productConCatIdTabContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkConcatId" value="0">Concat Word (As Suffix)
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblconcatId" class="table table-striped" val="concatId">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowconcatId"></tbody>
		                        </table>
							</div>
		             </div>
		             <div class="row" id="productConCatWordTabContent" >
		             		<div class="col-md-2 label-field">
								<input type="checkbox" id="chkConcatWord" value="0">Concat Word (As Prefix)
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblconcatWord" class="table table-striped" val="concatWord">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowconcatWord"></tbody>
		                        </table>
							</div>
		             </div>
				</div>
				<!-- END RealproductAndConcat TAB-->
				
				<!-- START UPC TAB-->
				<div class="pUpcTab">
					 <div class="row" id="productUpcContent" >
							<div class="col-md-2 label-field">
								<input type="checkbox" id="chkUpc" value="0">UPC
							</div>
							<div class="col-md-10" style="margin-left: 10%">
								<table id="tblupc" class="table table-striped" val="upc">
						            	<thead>
				                        	<tr class="heading">
				                        		<th width="5%">Step</th>
				                                <th width="20%">Action</th>
				                                <th width="30%">Value</th>
				                                <th >Add / Delete</th>
				                                <th width="30%">Result</th>
			                               	</tr>
				                         </thead>
				                         <tbody id="rowupc"></tbody>
		                        </table>
							</div>
		             </div>
				</div>
				<!-- END UPC TAB-->
				
				<div class="pExpireTab">
					<div class="row" id="productExpireTabContent" >
						<div class="col-md-2 label-field">
							<input type="checkbox" id="chkExpire" value="0">Expire
						</div>
						<div class="col-md-10" style="margin-left: 10%">
							<table id="tblexpire" class="table table-striped" val="expire">
				            	<thead>
		                        	<tr class="heading">
		                        		<th width="5%">Step</th>
		                                <th width="20%">Action</th>
		                                <th width="30%">Value</th>
		                                <th >Add / Delete</th>
		                                <th width="30%">Result</th>
	                               	</tr>
		                         </thead>
		                         <tbody id="rowexpire"></tbody>
	                        </table>
						</div>
		            </div>
	            </div>
				
				</div>
				</div>
				<!-- END PARSER PRODUCT DATA TABLE -->
				
				
				<!-- START PARSER TOOL -->
				<div class="row toolTab" id="productDataContent" style="margin-bottom: 50px;">
					<div class="col-md-12">
					<c:if test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'}">
						<div class="portlet light bordered" style="display:none">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" onclick="toggleTool(this)">HIDE</button>
									<span>&nbsp;Tool&nbsp;#0&nbsp;:&nbsp;Product&nbsp;Predication&nbsp;</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<div id="tool0ItemData">
									<form>
										<div class="row">
											<div class="col-md-12" id="tool0ItemDataArea">
												
											</div>
										</div>
									</form>
								</div>
								<div class="row" style="text-align: right; margin-right: 0px;">
									<span id="tool0AlertText"></span>
									<button type='button' class='btn darkGreenButton' id="btnTool0AddUrl" onclick='tool0AddUrlBox();'>Add Url</button>
									<button type='button' class='btn orangeButton' id="btnTool0to1Start" onclick='tool0to1Start();'>Predict tool 1</button>
									<button type='button' class='btn orangeButton' id="btnTool0to2Start" onclick='tool0to2Start();'>Predict tool 2</button>
								</div>
							</div>
						</div>
					</c:if> 
						<div class="portlet light bordered">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" onclick="toggleTool(this)">HIDE</button>
									<span>&nbsp;Tool&nbsp;#1&nbsp;:&nbsp;JSoup&nbsp;Selector&nbsp;(Intersection&nbsp;Mode) 
										<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="ถ้าใส่ไปมากกว่า 1 ตัว parser ที่  Generate จะได้ใช้งานทุกตัว"></c:out>"></a>
									</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<div id="tool1ItemData">
									<form>
										<div class="row">
											<div class="col-md-12" id="tool1ItemDataArea">
											</div>
										</div>
									</form>
								</div>
								<div class="row" style="text-align: right; margin-right: 0px;">
									<span id="tool1AlertText"></span>
									<button type='button' class='btn darkGreenButton' id="btnTool1AddProductBox" onclick='tool1AddProductBox();'>Add Product</button>
									<button type='button' class='btn orangeButton' id="btnTool1Start" onclick='tool1Start();' data-loading-text="<i class='fa fa-spinner fa-spin '></i>">Generate</button>
								</div>
							</div>
						</div>
						<div class="portlet light bordered">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" onclick="toggleTool(this)">HIDE</button>
									<span>&nbsp;Tool&nbsp;#2&nbsp;:&nbsp;JSoup&nbsp;Selector&nbsp;(Union&nbsp;Mode) 
										<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="ถ้าใส่ไปมากกว่า 1 ตัว parser ที่  Generate จะได้ทุกเคสที่เป็นไปได้"></c:out>"></a>
									</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<div id="tool2ItemData">
									<form>
										<div class="row">
											<div class="col-md-12" id="tool2ItemDataArea">
											</div>
										</div>
									</form>
								</div>
								<div class="row" style="text-align: right; margin-right: 0px;">
									<span id="tool2AlertText"></span>
									<button type='button' class='btn darkGreenButton' id="btnTool2AddProductBox" onclick='tool2AddProductBox();'>Add Product</button>
									<button type='button' class='btn orangeButton' id="btnTool2Start" onclick='tool2Start();'>Generate</button>
								</div>
							</div>
						</div>
						<div class="portlet light bordered">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default">Waiting</button>
									<span>&nbsp;Tool&nbsp;#3&nbsp;:&nbsp;Under&nbsp;Construction </span>
								</h4>
							</div>
						</div>
					</div>
				</div>
				<!-- END PARSER TOOL -->
				
				
				<!-- START COMPARE PRODUCT DATA TABLE -->
				<div class="row compareTab" id="productDataContent" style="display:none;">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<span>Compare&nbsp;Table</span>
								</h4>
							</div>
							<div class="portlet-body">
								<div style="margin-top: 20px;">
									<table id="comparetbl" class="table table-striped table-bordered">
										<thead>
											<tr class="heading">
												<th width='5%'  style="text-align: center;">No.</th>
												<th width='45%' style="text-align: center;">Name</th>
												<th width='40%' style="text-align: center;">Url</th>
												<th width='10%' style="text-align: center;">Test
													<span>
												  		<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="ทำการคลิ๊ก เมื่อต้องการทดสอบดึงสินค้า"></c:out>"></a>		
												  	</span>
												</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				<!-- END COMPARE PRODUCT DATA TABLE -->
				
				
			<div class="modal fade" id="modalRun">
   				<div class="modal-dialog" style="width: 70%;">
   					<div class="modal-content">
        				<div class="modal-header">
          					<button type="button" class="close" data-dismiss="modal">&times;</button>
          					<h4 class="modal-title" id="modalTitle">Run Parser Test</h4>
        				</div>
       					<div class="modal-body" id="modalData">
	 
       					</div>
    					<div class="modal-footer">
          					<button type="button" class="btn btn-default"  id="btnClose" data-dismiss="modal">Close</button>
        				</div>
   					</div>
   				</div>
   			</div>	
   			
   			<div class="modal fade" id="modalAlert">
    				<div class="modal-dialog" style="width: 40%;">
    					<div class="modal-content">
	        				<div class="modal-header">
	          					<button type="button" class="close" data-dismiss="modal">&times;</button>
	          					<h4 class="modal-title" style="color:#FFC300;" >Alert</h4>
	        				</div>
	       					<div class="modal-body" id="modalAlertData"></div>
	    					<div class="modal-footer">
	          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        				</div>
    					</div>
    				</div>
    			</div>
				

   			
   	 	<div id="topRunParser" style="float: right;">
   				<div>
   					<i class="fa fa-spinner fa-spin iconSpin"></i>
				</div> 
				<div>
   					<button type='button' class='btn orangeButton' id="btnTopSave" onclick='save();' style="margin-bottom: 10px;">Save</button>
				</div>   					
   				<div>
   					<button type='button' class='btn greenButton' id="btnTopRunTest" onclick='runTest();' style="margin-bottom: 10px;">Run Test</button>
				</div>
				<div id="topButton">
					<i class="fa fa-arrow-up"></i>
				</div>
   			</div>  
   
	        <!-- end design process steps--> 
   			<div id="stepResultHidden">
   			
   			</div>	
			<input type="hidden" id="hdMerchantId">
		  	<input type="hidden" id="hdTabType">
		  	<input type="hidden" id="hdIsNewVersion">
			</div>		
		</div>	
	</div>		
</div>
	<script>
	</script>
	<script type="text/JavaScript" src="<%=contextPath %>/assets/pages/js/parser-script.js"></script>
	
</body>
</html>