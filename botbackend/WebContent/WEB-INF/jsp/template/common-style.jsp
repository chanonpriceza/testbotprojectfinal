<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<% String contextPath = request.getContextPath(); %>

<meta charset="utf-8" />
    <title>Metronic | Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="<%=contextPath %>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<%=contextPath %>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/chart/Chart.min.css" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<%=contextPath %>/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<%=contextPath %>/assets/global/css/googleFont.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<%=contextPath %>/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<%=contextPath %>/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=contextPath %>/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN VENDOR  -->
    
    <link href="<%=contextPath %>/assets/global/css/icon-font.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/vendor/chartist/css/chartist-custom.css" type="text/css" />

    <!-- END VENDOR  -->
    
    <!-- BEGIN LOGIN  -->
    <link href="<%=contextPath %>/assets/global/plugins/animate/animate.min.css" type="text/css" />
    <link href="<%=contextPath %>/assets/global/plugins/css-hamburgers/hamburgers.min.css" type="text/css" />
    <!-- BEGIN LOGIN  -->