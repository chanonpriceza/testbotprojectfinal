<%@ page import="botbackend.bean.UserBean, botbackend.system.BaseConfig"%>
<%@ page session="true" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%
	String contextPath = request.getContextPath();
%>

<style>
	/*
    DEMO STYLE
*/
.nav-item{
     background-color:rgb(255, 218, 153);
}

body {
    background: #fafafa;
}

p {
    font-size: 1.1em;
    font-weight: 300;
    line-height: 1.7em;
    color: #999;
}

div ul li a, a:hover, a:focus {
    color: inherit; 
    text-decoration: none;
    transition: all 0.3s;
}

.navbar {
    padding: 15px 10px;
    background: #fff;
    border: none;
    border-radius: 0;
    margin-bottom: 40px;
    box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
}

.navbar-btn {
    box-shadow: none;
    outline: none !important;
    border: none;
}

.line {
    width: 100%;
    height: 1px;
    border-bottom: 1px dashed #ddd;
    margin: 40px 0;
}

/* ---------------------------------------------------
    SIDEBAR STYLE
----------------------------------------------------- */
.wrapper {
    display: flex;
    align-items: stretch;
}

.list-unstyled li>.list-unstyled {
    margin-left: 0px;
}

.sidebar {
    min-width: 235px;
    max-width: 250px;
    background: linear-gradient(-266deg, #e7ecf1, #e5e5e5);
    color: #555;
    transition: all 0.3s;
}

.sidebar.active {
    margin-left: -235px;
}

.sidebar .sidebar-header {
    padding: 20px;
    background: #6d7fcc;
}

.sidebar ul.components {
    padding: 0px 0;
}

.sidebar ul p {
    color: #fff;
    padding: 10px;
}

.sidebar ul li a {
    padding: 10px;
    font-size: 1.1em;
    display: block;
}
.sidebar ul li a:hover {
    color: #595966;
    background: #c9ffeb;
}



 .sidebar ul li.active > a, a[aria-expanded="true"].dropdown-menu-home {
    color: #595966;
    background: #85e4c0;
} 

.sidebar ul li ul li a.active {
    color: #595966;
    background: #fff;
}

a[data-toggle="collapse"] {
    position: relative;
}

ul.CTAs {
    padding: 20px;
}

ul.CTAs a {
    text-align: center;
    font-size: 0.9em !important;
    display: block;
    border-radius: 5px;
    margin-bottom: 5px;
}

a.download {
    background: #fff;
    color: #7386D5;
}

a.article, a.article:hover {
    background: #6d7fcc !important;
    color: #fff !important;
}



/* ---------------------------------------------------
    CONTENT STYLE
----------------------------------------------------- */
#content {
    padding: 20px;
    min-height: 100vh;
    transition: all 0.3s;
}
/* 
/* ---------------------------------------------------
    MEDIAQUERIES
----------------------------------------------------- */
@media (max-width: 991px){
	.page-header.navbar .menu-toggler.responsive-toggler {
	    display: none;
	}
	
	.page-header.navbar {
    padding: 0 0px;
    position: relative;
    clear: both;
}
}

@media (max-width: 768px) {
    .sidebar {
        margin-left: 0px;
        min-width: -webkit-fill-available;
    }
    .sidebar.active {
        margin-left: 0px;
    }
   
   #sidebarCollapse {
        display: none;
    }
    
    .collapse.in {
    display: block;
	}
	
	.wrapper {
    display: block;
    align-items: stretch;
	} 
	
	.page-header.navbar .menu-toggler.responsive-toggler {
    display: inline-block;
	}
	.set-padding-submenu{
	padding-right: 5%;
	}
 }
 
 @media (min-width: 768px) {
	/* .sidebarCollapse{
		display: none;
	} */
    
 }
  .navbar-collapse {
    padding-right: 0px;
    padding-left: 0px;
}

.main-menu__fixed {
	padding: 65px 0 0;
	position: fixed;
	background: #595966;
	top: 0;
	bottom: 0;
	height: 100%;
	left: 0;
}

.set-padding-submenu{
	padding-right: 10%;
}
.set-icon-submenu{
	margin-left: 9px;
}
	
</style>

<div class ="sidebar navbar-collapse collapse">
       <!-- <div class="sidebar-header">
           <h3>Priceza Admin</h3>
       </div> -->

	<ul class="list-unstyled components page-sidebar-menu  page-header-fixed">
		<!-- <p>Dummy Heading</p> -->
	
		<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value=""></c:out>">
			<a href="${pageContext.servletContext.contextPath}/admin/dashboard"><i class="fa fa-tachometer set-padding-submenu"></i><span>Dashboard</span></a>
		</li>
				
		 <c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantmanager')||sessionScope.Permission.containsKey('merchant.merchantmanager')&&!sessionScope.Permission.containsKey('-merchant.merchantmanager')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantconfig')||sessionScope.Permission.containsKey('merchant.merchantconfig')&&!sessionScope.Permission.containsKey('-merchant.merchantconfig')}">
			<li><a href="#merchantPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-th set-padding-submenu"></i><span class="title">Merchant</span></a>
                <ul class="collapse list-unstyled" id="merchantPage">
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantmanager')||sessionScope.Permission.containsKey('merchant.merchantmanager')&&!sessionScope.Permission.containsKey('-merchant.merchantmanager')}">
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับจัดการร้านค้าในระบบ"></c:out>">
							<a href="${pageContext.servletContext.contextPath}/admin/merchantmanager"><i class="fa fa-caret-right set-icon-submenu"></i>Merchant Manager</a>
						</li>
					</c:if>
					<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantconfig')||sessionScope.Permission.containsKey('merchant.merchantconfig')&&!sessionScope.Permission.containsKey('-merchant.merchantconfig')}">
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับตั้งค่าการดึงสินค้า"></c:out>">
							<a href="${pageContext.servletContext.contextPath}/admin/merchantconfig"><i class="fa fa-caret-right set-icon-submenu"></i>Merchant Config</a>
						</li>
					</c:if>
                </ul>
            </li>
		</c:if>		
				
				
		<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-crawlerconfig.urlpattern')
		||sessionScope.Permission.containsKey('crawlerconfig.urlpattern')&&!sessionScope.Permission.containsKey('-crawlerconfig.urlpattern')
		||sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-crawlerconfig.parser')
		||sessionScope.Permission.containsKey('crawlerconfig.parser')&&!sessionScope.Permission.containsKey('-crawlerconfig.parser')
		||sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-crawlerconfig.feedparser')
		||sessionScope.Permission.containsKey('crawlerconfig.feedparser')&&!sessionScope.Permission.containsKey('-crawlerconfig.feedparser')
		}">
		<li><a href="#crawlerPage" data-toggle="collapse"aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-wrench set-padding-submenu"></i><span class="title">Crawler Config</span></a>
			<ul class="collapse list-unstyled" id="crawlerPage">
				<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-crawlerconfig.urlpattern')||sessionScope.Permission.containsKey('crawlerconfig.urlpattern')&&!sessionScope.Permission.containsKey('-crawlerconfig.urlpattern')}">
					<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับจัดการรูปแบบลิงค์หมวดสินค้า "></c:out>">
						<a href="${pageContext.servletContext.contextPath}/admin/urlpattern"><i class="fa fa-caret-right set-icon-submenu"></i>Manage Pattern</a>
					</li>
				</c:if>
				<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-crawlerconfig.parser')||sessionScope.Permission.containsKey('crawlerconfig.parser')&&!sessionScope.Permission.containsKey('-crawlerconfig.parser')}">
					<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับจัดการรูปแบบการดึงสินค้าแบบ Parser"></c:out>">
						<a href="${pageContext.servletContext.contextPath}/admin/parser"><i class="fa fa-caret-right set-icon-submenu"></i>Manage Parser</a>
					</li>			
				</c:if>
			</ul>
		</li>
		</c:if>
		
		<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-task.taskmanager')||sessionScope.Permission.containsKey('task.taskmanager')&&!sessionScope.Permission.containsKey('-task.taskmanager')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-task.taskmanager')||sessionScope.Permission.containsKey('task.taskmanager')&&!sessionScope.Permission.containsKey('-task.taskmanager')}">
		<li><a href="#taskPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-file set-padding-submenu"></i><span class="title">Task & Issue</span></a>
            <ul class="collapse list-unstyled" id="taskPage">
	             <c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-task.taskmanager')||sessionScope.Permission.containsKey('task.taskmanager')&&!sessionScope.Permission.containsKey('-task.taskmanager')}">
					<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับแจ้งร้านค้าที่มีปัญหา"></c:out>">
						<a href="${pageContext.servletContext.contextPath}/admin/taskmanager"><i class="fa fa-caret-right set-icon-submenu"></i>Task Manager</a>
					</li>
				</c:if>
            </ul>
        </li>
        </c:if>	
        
        <c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')||sessionScope.Permission.containsKey('merchant.merchantmonitoring')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')||sessionScope.Permission.containsKey('merchant.merchantmonitoring')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')}">
		<li><a href="#monitoringPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-desktop set-padding-submenu"></i><span class="title">Monitoring</span></a>
            <ul class="collapse list-unstyled" id="monitoringPage">
	             <c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')||sessionScope.Permission.containsKey('merchant.merchantmonitoring')&&!sessionScope.Permission.containsKey('-merchant.merchantmonitoring')}">
					<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับ mornitor ร้านค้าที่มีปัญหา"></c:out>">
						<a href="${pageContext.servletContext.contextPath}/admin/merchantmonitoring"><i class="fa fa-caret-right set-icon-submenu"></i>Merchant Monitoring</a>
					</li>
				</c:if>
            </ul>
        </li>
        </c:if>	
		
		<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualproductmanager')||sessionScope.Permission.containsKey('product.manualproductmanager')&&!sessionScope.Permission.containsKey('-product.manualproductmanager')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualaddproduct')||sessionScope.Permission.containsKey('product.manualaddproduct')&&!sessionScope.Permission.containsKey('-product.manualaddproduct')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualupdateproduct')||sessionScope.Permission.containsKey('product.manualupdateproduct')&&!sessionScope.Permission.containsKey('-product.manualupdateproduct')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.focusupdate')||sessionScope.Permission.containsKey('product.focusupdate')&&!sessionScope.Permission.containsKey('-product.focusupdate')}">
			<li><a href="#productPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-archive set-padding-submenu"></i><span class="title">Product</span></a>
                <ul class="collapse list-unstyled" id="productPage">
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualproductmanager')||sessionScope.Permission.containsKey('product.manualproductmanager')&&!sessionScope.Permission.containsKey('-product.manualproductmanager')}">
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="หน้าสำหรับดูลิสสินค้าที่เข้าผา่นระบบ Manual และฟังก์ชั่นสำหรับเพิ่ม/ลบสินค้ากลุ่มนี้"></c:out>">
							<a href="${pageContext.servletContext.contextPath}/admin/manualproductmanager"><i class="fa fa-caret-right set-icon-submenu"></i>Manual Product Manager</a>
						</li>
					</c:if>
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualaddproduct')||sessionScope.Permission.containsKey('product.manualaddproduct')&&!sessionScope.Permission.containsKey('-product.manualaddproduct')}">
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="หน้าเพิ่มสินค้าเข้าระบบ อัตโนมัติผ่านระบบ Bot หมายเหตุ * สินค้าจะได้รับการเพิ่มเข้าระบบ ใช้เวลาประมาณ 15 นาที"></c:out>">
							<a href="${pageContext.servletContext.contextPath}/admin/manualaddproduct"><i class="fa fa-caret-right set-icon-submenu"></i>Manual Add Product</a>
						</li>
					</c:if>
					<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.manualupdateproduct')||sessionScope.Permission.containsKey('product.manualupdateproduct')&&!sessionScope.Permission.containsKey('-product.manualupdateproduct')}">
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="หน้าอัพเดตสินค้า อัติโนมัติผ่านระบบ Bot หมายเหตุ * สินค้าจะได้รับการอัพเดต ใช้เวลาประมาณประมาณ 15 นาที"></c:out>">
							<a href="${pageContext.servletContext.contextPath}/admin/manualupdateproduct"><i class="fa fa-caret-right set-icon-submenu"></i>Manual Update Product</a>
						</li>
					</c:if>
					<%--
						<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-product.focusupdate')||sessionScope.Permission.containsKey('product.focusupdate')&&!sessionScope.Permission.containsKey('-product.focusupdate')}">
							<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="หน้าจัดการสินค้า สำหรับสินค้าที่ต้องใช้การอัพเดตบ่่อยครั้ง"></c:out>">
								<a href="${pageContext.servletContext.contextPath}/admin/focusupdate"><i class="fa fa-caret-right set-icon-submenu"></i>Focus Update</a>
							</li>
						</c:if>
					--%>
                </ul>
            </li>
		</c:if>	
		
		<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-report.merchant')||sessionScope.Permission.containsKey('report.merchant')&&!sessionScope.Permission.containsKey('-report.merchant')||
		sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-report.summary.botquality')||sessionScope.Permission.containsKey('report.summary.botquality')&&!sessionScope.Permission.containsKey('-report.summary.botquality')
		}">
		
		<li><a href="#reportPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-desktop set-padding-submenu"></i> <span class="title">Report</span></a>
                <ul class="collapse list-unstyled" id="reportPage">
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-report.merchant')||sessionScope.Permission.containsKey('report.merchant')&&!sessionScope.Permission.containsKey('-report.merchant')}">
						 <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="แสดงรายงานการทำงานของร้านค้า"></c:out>">
						 	<a href="${pageContext.servletContext.contextPath}/admin/merchantruntimereport"><i class="fa fa-caret-right set-icon-submenu"></i>Merchant Runtime Report</a>
						 </li>
				 	</c:if>
					<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-report.summary.botquality')||sessionScope.Permission.containsKey('report.summary.botquality')&&!sessionScope.Permission.containsKey('-report.summary.botquality')}">	
						<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="แสดงคุณภาพของบอทในการรันแต่ละร้าน"></c:out>"> 
						 	<a href="${pageContext.servletContext.contextPath}/admin/summarybotquality"><i class="fa fa-caret-right set-icon-submenu"></i>Summary Bot Quality</a>
			 			</li>
				 	</c:if>
					<%-- 
				 		<li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="แสดงคุณภาพของงานบอท"></c:out>"> 
							<a href="${pageContext.servletContext.contextPath}/admin/taskqualityreport"><i class="fa fa-caret-right set-icon-submenu"></i>Task Quality Report</a>
						</li>
					 --%>  
                </ul>
             </li>
		</c:if>	
		<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-tools.crawlerchecking')||sessionScope.Permission.containsKey('tools.crawlerchecking')&&!sessionScope.Permission.containsKey('-tools.crawlerchecking')}">
			<li><a href="#toolsPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-cog set-padding-submenu"></i> <span class="title">Tools</span></a>
                <ul class="collapse list-unstyled" id="toolsPage">
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-tools.crawlerchecking')||sessionScope.Permission.containsKey('tools.crawlerchecking')&&!sessionScope.Permission.containsKey('-tools.crawlerchecking')}">
	                    <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับตรวจสอบการดึงหน้าเว็บ"></c:out>">
	                    	<a href="${pageContext.servletContext.contextPath}/admin/crawlerchecking"><i class="fa fa-caret-right set-icon-submenu"></i>Check Web</a>
	                    </li>
					</c:if>
					<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-tools.checkbankeyword')||sessionScope.Permission.containsKey('tools.checkbankeyword')&&!sessionScope.Permission.containsKey('-tools.checkbankeyword')}">
	                    <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับตรวจสอบการบลอค keyword จังหวะซิงค์สินค้า"></c:out>">
	                    	<a href="${pageContext.servletContext.contextPath}/admin/checkBanKeywordPage"><i class="fa fa-caret-right set-icon-submenu"></i>Check BanKeyword</a>
	                    </li>
					</c:if>
					<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-tools.checkimage')||sessionScope.Permission.containsKey('tools.checkimage')&&!sessionScope.Permission.containsKey('-tools.checkimage')}">
	                    <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับตรวจสอบการดึงรูป"></c:out>">
	                    	<a href="${pageContext.servletContext.contextPath}/admin/checkImage"><i class="fa fa-caret-right set-icon-submenu"></i>Check Image</a>
	                    </li>
					</c:if>
				</ul>
            </li>
		</c:if>
		
		<c:if test="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'&& sessionScope.Permission.containsKey('all') &&!sessionScope.Permission.containsKey('-partnership.lnwshopmanager')||sessionScope.Permission.containsKey('partnership.lnwshopmanager')||
		BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'&& sessionScope.Permission.containsKey('all') &&!sessionScope.Permission.containsKey('-partnership.lnwshoppattern')||sessionScope.Permission.containsKey('partnership.lnwshoppattern')}">
			<li><a href="#partnershipPage" data-toggle="collapse" aria-expanded="false" class="dropdown-menu-home"><i class="fa fa-users set-padding-submenu"></i> <span class="title">Partnership</span></a>
                <ul class="collapse list-unstyled" id="partnershipPage">
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-partnership.lnwshopmanager')||sessionScope.Permission.containsKey('partnership.lnwshopmanager')&&!sessionScope.Permission.containsKey('-partnership.lnwshopmanager')}">
	                    <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับจัดการร้านค้า lnwShop"></c:out>">
	                    	<a href="${pageContext.servletContext.contextPath}/admin/lnwshopmanager"><i class="fa fa-caret-right set-icon-submenu"></i>lnwshop Merchant</a>
	                    </li>
					</c:if>
                	<c:if test="${sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('-partnership.lnwshoppattern')||sessionScope.Permission.containsKey('partnership.lnwshoppattern')&&!sessionScope.Permission.containsKey('-partnership.lnwshoppattern')}">
	                    <li data-toggle='tooltip' data-placement='right' style="text-decoration:none;"  title="<c:out value="ใช้สำหรับจัดการร้านค้า lnwShop"></c:out>">
	                    	<a href="${pageContext.servletContext.contextPath}/admin/lnwshoppattern"><i class="fa fa-caret-right set-icon-submenu"></i>lnwshop Pattern</a>
	                    </li>
					</c:if>
				</ul>
            </li>
		</c:if>

	</ul>
</div>


<script type="text/javascript" src="<%=contextPath%>/assets/pages/js/menuside-script.js"></script>

