<%@page import="org.apache.commons.text.StringEscapeUtils,botbackend.bean.UserBean,botbackend.bean.TaskBean,utils.DateTimeUtil, botbackend.system.BaseConfig, botbackend.utils.ViewUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="priceza" prefix="pz"%>
<%
  UserBean userBean;
  HttpSession ses = request.getSession(false);
  String contextPath = request.getContextPath();
%>

		<c:set var="id" value="${param.id }"/>
		<c:set var="pluginPosition" value="${param.pluginPosition }"/>
		<c:set var="userName" value="${param.userName}"/>
		<c:set var="showSetting" value="${requestScope.showSetting }" />
		
		<div class="grid-item" data-item-id="${id}" data-position="${StringEscapeUtils.escapeXml10(pluginPosition)}">
			<div class="panel">
				<div class="panel-heading colorbordered  block__header">
					<h3 class="panel-title  block__header--left ">Show Task</h3>
					<div class="block__header--right">
						<div><i class="deletePlugin fa fa-times" data-item-id="${id}" style="cursor: pointer;"></i></div>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table">
						<thead>
							<tr class="heading">
								<th style="text-align: center;width: 15%;">M.Id</th>
								<th style="text-align: center;width: 5%;">Status</th>
								<th style="text-align: center;max-width: 200px; word-wrap: break-word;">Issue</th>
								<th style="text-align: center;width: 11%;">Create Date</th>
								<c:if test="${!sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('task.taskmanager.calculate.workday')}">
									<th style="text-align: center;width: 11%;">Due Date</th>								
								</c:if>
								
								<th style="text-align: center;width: 11%;">Owner &Update</th>
								<th style="text-align: center;width: 11%;">Count down</th>
								<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('task.taskmanager.calculate.workday')}">
									<th style="text-align: center;">Comment</th>								
								</c:if>
							</tr>
						</thead>
					 	<tbody>
							<c:set var="count" value="0"/>
							<c:forEach var="task" items="${showSetting}">
								<c:if test="${not empty task}" >
									<tr class='rowPattern'>
										<!-- SET VARIABLE -->
										<c:if test="${not empty task.data}">
											<c:set var ="data" value="${pz:convertJSONStringToMap(task.data)}"/>
										</c:if>
										
										<c:if test="${not empty task.result}">
											<c:set var ="result" value="${pz:convertJSONStringToMap(task.result)}"/>
										</c:if>
										
										<c:set var ="merchantId" value="${task.merchantId}"/>
										<c:set var ="active" 	 value="${task.active}"/>
										<c:set var ="merchantName" value="${task.merchantName}"/>
										<c:set var ="issue" value="${task.issue}"/>
										<c:set var ="createDate" value="${task.createDate}"/>
										<c:set var ="owner" value="${task.owner}"/>
										<c:set var ="issueType" value="${task.issueType}"/>
										<c:set var ="status" value="${task.status}"/>
										<c:set var="redActive"	 value=""/>
										<c:if test="${active == 0 || active == 2 || active == 6 || active == 7 || active == 8 || active == 14 ||  active == 15   }">
											<c:set var="redActive"	 value="color:red"/>
										</c:if>
										
										<!-- METCHANT ID -->
										<td style='text-align:center;'>
											<div class="row">
										    	<c:choose>
												   	<c:when test="${empty merchantId || merchantId == 0}">-</c:when>
													<c:otherwise>
														<a href="<%=contextPath%>/admin/getmerchantdata?searchParam=${merchantId}" target="_blank" id="btntoMerchantManager" data-element-id="${task.merchantId}" name="btntoMerchantManager" style="color: #666;">
															<c:out value="${merchantId}"/>
														</a>	
													</c:otherwise>
												</c:choose>				    											    
											</div>
										    <c:if test="${not empty data.merchantName}">
										    	<div class="row" style="font-size: x-small; ${redActive}">
											    	<c:choose>
	   											   		<c:when test="${not empty data.merchantName}">${data.merchantName}</c:when>
														<c:otherwise> - </c:otherwise>
													</c:choose>				    											    
										    	</div>
										    </c:if>
										</td>
										
										<td>
											<c:choose>
								    			<c:when test="${status == 'WAITING'}">
								    				<span id="status${task.id}" class="badge badge-primary" style="width: 100%;"><c:out value="${task.status}"></c:out></span>
								    			</c:when>
								    			<c:when test="${status == 'DOING'}">
								    				<span id="status${task.id}" class="badge badge-warning" style="width: 100%;"><c:out value="${task.status}"></c:out></span>
								    			</c:when>
								    			<c:otherwise>
								    				<span id="status${task.id}" class="badge badge-secondary" style="width: 100%;"><c:out value="${task.status}"></c:out></span>
								    			</c:otherwise>
								    		</c:choose>
										</td>
										
										
										<td style="max-width: 200px; word-wrap: break-word;">
										<a class="fa fa-desktop" href="<%=contextPath%>/admin/merchantruntimereport?searchParam=${task.merchantId}" target="_blank" id="btntoMerchantReport" style="color: #666" data-element-id="${task.merchantId}" name="btntoMerchantReport"></a>
											<c:set var="country" value="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}"></c:set> 
											<c:if test="${not empty country && country != null && task.merchantId != 0}">
												<c:set var="country_path" value="${ViewUtil.generateDomain(country)}"></c:set> 											
												<a class="fa fa-clone" href="${country_path}rating?cmd=tabAllProduct&merchantId=${merchantId}" target="_blank" style="color: #666"></a>
											</c:if>
											<a href="#" id="btntoTask" data-element-id="${task.id}" name="btntoTask" style="color: #666;">
												<c:out value="${issue}"/>
												<c:if test="${not empty issueType && issueType == 'customer' }">
													<i class='fa fa-flag' aria-hidden='true'  style='margin-left: 5px;color: #ff8b7d;'></i>
												</c:if>
											</a>																													    					
										</td>
											
										
										
										<td style='text-align:center;'>
								    		<c:choose>
												<c:when test="${not empty createDate}">${DateTimeUtil.generateStringDisplayDate(DateTimeUtil.generateDate(createDate,"yyyy-MM-dd"))}</c:when>
												<c:otherwise> - </c:otherwise>
											</c:choose>
										</td>
										
										<c:if test="${!sessionScope.Permission.containsKey('all')&&!sessionScope.Permission.containsKey('task.taskmanager.calculate.workday')}">
											<td style='text-align:center;'>
									    		<c:choose>
													<c:when test="${not empty data.duedate}">${DateTimeUtil.generateStringDisplayDate(DateTimeUtil.generateDate(data.duedate,"yyyy-MM-dd"))}</c:when>
													<c:otherwise> - </c:otherwise>
												</c:choose>
											</td>
										</c:if>
										
										<td>
									    	<i style="width:18px; text-align:center;" class="fa fa-user"></i> <c:out value="${owner}" /><br/>
									    	<c:choose>
									    		<c:when test="${empty task.updatedBy}">
									    			<a id="tooltipcomment${task.id}" style="width:18px; text-align:center; color:black;" class='fa fa-commenting-o' data-toggle='tooltip' data-placement='top' style="text-decoration:none;" title="<c:out value="${result.message}" />"></a>
									    			<c:out value="-"/>
									    		</c:when>
									    		<c:otherwise>
									    			<a id="tooltipcomment${task.id}" style="width:18px; text-align:center; color:black;" class='fa fa-commenting-o' data-toggle='tooltip' data-placement='top' style="text-decoration:none;" title="<c:out value="${result.message}" />"></a>
											    	<c:out value="${task.updatedBy}"/>
									    		</c:otherwise>
									    	</c:choose>
									    </td>
									    
										<td style="text-align: center;">
											<c:choose>
												<c:when test="${not empty data.duedate}">
													<c:choose>
														<c:when test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('task.taskmanager.calculate.workday')}">
															<c:set var="countdown" value="${ViewUtil.getWorktimeDay(ViewUtil.calcurateTargetDate(createDate))}"></c:set>
														</c:when>
														<c:otherwise>
															<c:set var="countdown" value="${DateTimeUtil.calcurateDayDiff(createDate,DateTimeUtil.generateDate(data.duedate,'yyyy-MM-dd'))+1}"></c:set>						
														</c:otherwise>
													</c:choose>
													<c:choose>
												   		<c:when test="${countdown <= 3}"><c:set var="colorcountdown" value="#e43a45"></c:set></c:when>
												   		<c:when test="${countdown <= 5}"><c:set var="colorcountdown" value="#eca80e"></c:set></c:when>
												   		<c:when test="${countdown <= 7}"><c:set var="colorcountdown" value="#26c281"></c:set></c:when>
														<c:otherwise><c:set var="colorcountdown" value=""></c:set></c:otherwise>
													</c:choose>	
													<span class="fa-stack fa-lg" style="padding: 10%">
													   <i class="fa fa-circle-o-notch fa-stack-2x fa-spinner fa-spin" style="color: ${colorcountdown}"></i>
													   <span class="fa-stack-1x" style="font-size:70%;">
													        <c:out value="${countdown}"></c:out>
													    </span> 
													</span>
													<c:out value="Day"></c:out>
												</c:when>
												<c:otherwise>-</c:otherwise>
											</c:choose>
										</td>
										
										<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('task.taskmanager.comment.template')}">
											<td style='text-align:center;'>
									    		<div class="btn-group">																						    			
	                                           		<button id="btnComment${task.id}" class="btnComment btn btn-primary dropdown-toggle" data-element-id="${task.id}" data-toggle="dropdown" data-loading-text="<i class='fa fa-spinner fa-spin '></i>"><i class="fa fa-pencil"></i><span class="sr-only">Toggle Dropdown</span></button>
	                                           		<ul class="dropdown-menu" role="menu">
														<li><a class = "commentTemplate" data-element-id="${task.id}" 		name="commentTemplate" 		onclick="commentTemplateTask(this,'doing');">DOING</a></li>
														<li><a class = "commentTemplate" data-element-id="${task.id}" 		name="commentTemplate" 		onclick="commentTemplateTask(this,'waitsync');">WAITING SYNC</a></li>
														<li><a class = "commentTemplate" data-element-id="${task.id}" 		name="commentTemplate" 		onclick="commentTemplateTask(this,'syncsuccess');">SYNC SUCCESS</a></li>
														<li><a class = "commentTemplate" data-element-id="${task.id}" 		name="commentTemplate" 		onclick="commentTemplateTask(this,'deletesuccess');">DELETE SUCCESS</a></li>
														<li><a class = "commentTemplate" data-element-id="${task.id}" 		name="commentTemplate" 		onclick="commentTemplateTask(this,'isactive');">STATUS ACTIVE</a></li>
													</ul>
												</div>	
											</td>
										</c:if>				
									</tr> 								
								</c:if>
							</c:forEach>  
						</tbody>
					</table>
				</div>	
			</div>
		</div>