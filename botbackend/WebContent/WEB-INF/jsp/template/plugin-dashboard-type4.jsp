<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%
	String contextPath = request.getContextPath();
	
%>

	 	<c:set var="id" value="${param.id}"/>
		<c:set var="pluginPosition" value="${param.pluginPosition }"/>
		<c:set var="showSetting" value="${requestScope.showSetting}"/>
		
		
		<div class="grid-item" data-item-id="${id}"
			data-position="${StringEscapeUtils.escapeXml(pluginPosition)}">
			<div class="panel">
				<div class="panel-heading colorbordered  block__header">
					<h3 class="panel-title  block__header--left ">Bot Quality chart</h3>
					<div class="block__header--right">
						<div>
							<i class="deletePlugin fa fa-times" data-item-id="${id}"
								style="cursor: pointer;"></i>
						</div>
					</div>
				</div>
				<div class="panel-body no-padding">
					<canvas id="myChart${id}" width="600" height="400"></canvas>
				</div>
			</div>
		</div>
		
		
		<script>
			

		window.onload = function() {
			
			var id = "<c:out value="${id}"/>"; 
		    var obj = JSON.parse("<c:out value="${showSetting}"/>".replace(/&#034;/g,'"')); 	

		    var dateList = [];
			var dataQualityPercent = [];
			var dataProductall = [];
			var dataProductNotUpdate = [];
			var dataProductWaittoFix = [];
			var dataProductUpdateWithin1Day = [];
			var dataProductUpdateWithin3Day = [];
			var dataProductUpdateMorethan3Day = [];
		                    
			for(var i = 0; i < obj.length; i++) {
				dateList.push(obj[i].date.replace("-",","));
				dataQualityPercent.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].qualityPercent});
				dataProductall.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductall});
				dataProductNotUpdate.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductNotUpdate});
				dataProductWaittoFix.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductWaittoFix});
				dataProductUpdateWithin1Day.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductUpdateWithin1Day});
				dataProductUpdateWithin3Day.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductUpdateWithin3Day});
				dataProductUpdateMorethan3Day.push({x: new Date(obj[i].date.replace("-",",")), y: obj[i].countProductUpdateMorethan1Day});
			}
			
			var config = {
				type: 'line',
				data: {
					labels: dateList,
					datasets: [{
						label: 'Quality Percent',
						backgroundColor: '#4dc9f6',
						borderColor: '#4dc9f6',
						data: dataQualityPercent,
						yValueFormatString: "#0.##",
						fill: false,
						hidden: false,
					}, {
						label: 'Product All',
						backgroundColor: '#f67019',
						borderColor: '#f67019',
						data: dataProductall,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}, {
						label: 'Product Not Update',
						backgroundColor: '#f53794',
						borderColor: '#f53794',
				    	data: dataProductNotUpdate,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}, {
						label: 'Product Wait Fix',
						backgroundColor: '#537bc4',
						borderColor: '#537bc4',
				    	data: dataProductWaittoFix,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}, {
						label: 'Product Done in 1 Day',
						backgroundColor: '#acc236',
						borderColor: '#acc236',
				    	data: dataProductUpdateWithin1Day,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}, {
						label: 'Product Done in 1-3 Day',
						backgroundColor: '#166a8f',
						borderColor: '#166a8f',
				    	data: dataProductUpdateWithin3Day,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}, {
						label: 'Product waste more than 3 Day',
						backgroundColor: '#00a950',
						borderColor: '#00a950',
				    	data: dataProductNotUpdate,
						yValueFormatString: "#,##0",
						fill: false,
						hidden: true,
					}]
				},
				options: {
					responsive: true,
					title: {
						display: false,
						text: 'Bot quality history chart'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Date'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Value'
							}
						}]
					}
				}
			};

			var ctx = document.getElementById('myChart' + id).getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		</script>
		
		
		
		
		

