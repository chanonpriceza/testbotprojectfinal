<%@page import="org.apache.commons.lang3.StringEscapeUtils,botbackend.utils.ViewUtil,botbackend.bean.UserBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

		<c:set var="id" value="${param.id }"/>
		<c:set var="pluginPosition" value="${param.pluginPosition }"/>
		<c:set var="userName" value="${param.userName}"/>
		<c:set var="showSetting" value="${requestScope.showSetting }" />
		
		<div class="grid-item" data-item-id="${id}" data-position="${StringEscapeUtils.escapeXml(pluginPosition)}">
			<div class="panel">
				<div class="panel-heading colorbordered  block__header">
					<h3 class="panel-title  block__header--left ">Core Merchant for ${userName}</h3>
					<div class="block__header--right">
						<div><i class="deletePlugin fa fa-times" data-item-id="${id}" style="cursor: pointer;"></i></div>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table table-striped">
						<thead>
							<tr class="heading">
								<!-- <th style="text-align: center;">#</th> -->
								<th style="text-align: center;">M.Id</th>
								<th style="text-align: center;">MerchantName</th>
								<th style="text-align: center;">Status</th>
								<th style="text-align: center;">UpdateBy</th>
								<th style="text-align: center;">Last DUE 
									<span>
								  		<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="Show last time update product"></c:out>"></a>		
								  	</span>
								</th>
								<th style="text-align: center;">Last WCE
									<span>
								  		<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="Show last time add new product  "></c:out>"></a>		
								  	</span>
								</th>
							</tr>
						</thead>
					 	<tbody>
							<c:set var="count" value="0"/>
							<c:forEach var="task" items="${showSetting}"> 
								<c:if test="${not empty task}" >
									<tr class='rowPattern'>
										<!-- SET VARIABLE -->
										<c:set var ="merchantId" value="${task[0]}"/>
										<c:set var ="merchantName" value="${task[1]}"/>
										<c:set var ="active" value="${task[2]}"/>
										<c:set var ="server" value="${task[3]}"/>
										<c:set var ="lastupdate" value="${task[4]}"/>
										<c:set var ="errorMessage" value="${task[15]}"/>
										
										<!-- SET Time WCE -->
										<c:set var ="typeWCE" value="${task[5]}"/>
										<c:set var ="wceFirstTime" value="${task[6]}"/>
										<c:set var ="wceSecondTime" value="${task[7]}"/>
										<c:set var ="wceThirdTime" value="${task[8]}"/>
										<c:set var ="lastUpdateWCE" value="${task[9]}"/>
										
										<!-- SET Time DUE -->
										<c:set var ="typeDUE" value="${task[10]}"/>
										<c:set var ="dueFirstTime" value="${task[11]}"/>
										<c:set var ="dueSecondTime" value="${task[12]}"/>
										<c:set var ="dueThirdTime" value="${task[13]}"/>
										<c:set var ="lastUpdateDUE" value="${task[14]}"/>
										
										
										<!-- METCHANT ID -->
										<td style='text-align:center;'><c:out value="${merchantId}"/></td>
										
										<!-- SERVER -->
										<td style='text-align:start;'>
											<a id="favoritemerchant${task[0]}${task[3]}"   class="favoriteMerchant fa fa-minus-circle"     style="display: inline-block; text-decoration:none; color: #F68B0B;" aria-hidden="true" data-placement='right'  data-element-id="${task[0]},${task[3]}"></a>
											<c:if test="${server != null && server != ''}">
									  			<a class='fa fa-desktop' data-toggle='tooltip' data-placement='right' style="text-decoration:none; color: #d86213" title="<c:out value="${server}"></c:out>"></a>
									  		</c:if>
									  		<c:choose>  
										    	<c:when test="${empty errorMessage || fn:contains(errorMessage,'null')}">
										    		<c:out value="${merchantName}"/>
										    	</c:when>  
										    	<c:otherwise>
										    		<span data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="${errorMessage}"></c:out>">
								  						<c:out value="${merchantName}"/>		
								  					</span>
								  				</c:otherwise>
										    </c:choose>
											
										</td>
										
										<!-- ACTIVE -->
										<td style='text-align:center;'>
											<c:out value="${ViewUtil.generateShowActive(active)}"/>
										</td>	
										<!-- LAST UPDATE -->
										<td style='text-align:center;'>
											<c:choose>
												<c:when test="${empty lastupdate || lastupdate=='null' }"><c:out value=""/></c:when>
												<c:otherwise><c:out value="${lastupdate}"/></c:otherwise>
											</c:choose>
										</td>
										
										<!-- Time DUE -->
										<td style='text-align:center;'>
											<div class="row">
												<c:choose>  
											    	<c:when test="${empty lastUpdateDUE || lastUpdateDUE=='Unknown'}"><c:out value="${lastUpdateDUE}"></c:out></c:when>  
											    	<c:otherwise><c:out value="${lastUpdateDUE}"></c:out></c:otherwise>
											    </c:choose>
											</div>
											<div class="row">
												<c:choose>  
											    	<c:when test="${empty dueFirstTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${dueFirstTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(dueFirstTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-circle-o-notch fa-spin' data-toggle='tooltip' style="color: #eca80e" title="<c:out value="${dueFirstTime}"></c:out>"></a>
											    	</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${dueFirstTime}"></c:out>"></a>
											    	</c:otherwise>
											    </c:choose>
												<c:choose>  
											    	<c:when test="${empty dueSecondTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${dueSecondTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(dueSecondTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #e43a45" title="<c:out value="${fn:replace(dueSecondTime,'Unknown',' Error')}"></c:out>"></a>
											    	</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${dueSecondTime}"></c:out>"></a>
											    	</c:otherwise>
											    </c:choose>
												<c:choose>  
											    	<c:when test="${empty dueThirdTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${dueThirdTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(dueThirdTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #e43a45" title="<c:out value="${fn:replace(dueThirdTime,'Unknown',' Error')}"></c:out>"></a>
											    	</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeDUE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${dueThirdTime}"></c:out>"></a>
											    	</c:otherwise>
											    </c:choose>
											</div>  
										</td>
										
										<!-- Time WCE -->
										<td style='text-align:center;'>
											<div class="row">
												<c:choose>  
											    	<c:when test="${empty lastUpdateWCE || lastUpdateWCE=='Unknown'}"><c:out value="${lastUpdateWCE}"></c:out></c:when>  
											    	<c:otherwise><c:out value="${lastUpdateWCE}"></c:out></c:otherwise>
											    </c:choose>										
											</div>
											<div class="row">
										  		<c:choose>  
											    	<c:when test="${empty wceFirstTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${wceFirstTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(wceFirstTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-circle-o-notch fa-spin' data-toggle='tooltip' style="color: #eca80e" title="<c:out value="${wceFirstTime}"></c:out>"></a>
										    		</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${wceFirstTime}"></c:out>"></a>
										    		</c:otherwise>
											    </c:choose>
												<c:choose>  
											    	<c:when test="${empty wceSecondTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${wceSecondTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(wceSecondTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #e43a45" title="<c:out value="${fn:replace(wceSecondTime,'Unknown',' Error')}"></c:out>"></a>
											    	</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${wceSecondTime}"></c:out>"></a>
											    	</c:otherwise>
											    </c:choose>
												<c:choose>  
											    	<c:when test="${empty wceThirdTime}"><a class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #999" title="<c:out value="${wceThirdTime}"></c:out>"></a></c:when>  
											    	<c:when test="${fn:contains(wceThirdTime,'Unknown')}">
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-times-circle-o fa-lg' data-toggle='tooltip' style="color: #e43a45" title="<c:out value="${fn:replace(wceThirdTime,'Unknown',' Error')}"></c:out>"></a>
											    	</c:when>  
											    	<c:otherwise>
											    		<a href="merchantruntimereport?searchParam=${merchantId}&reportType=${typeWCE}&reportTime=${time}" target="_blank" class='fa fa-clock-o fa-lg' data-toggle='tooltip' style="color: #26c281" title="<c:out value="${wceThirdTime}"></c:out>"></a>
											    	</c:otherwise>
											    </c:choose>										
											</div>
										</td>	
															
									</tr> 								
								</c:if>
							</c:forEach>  
						</tbody>
					</table>
				</div>	
			</div>
		</div>