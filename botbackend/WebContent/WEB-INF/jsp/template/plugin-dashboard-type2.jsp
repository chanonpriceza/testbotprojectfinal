<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%
	String contextPath = request.getContextPath();
	
%>

	 	<c:set var="id" value="${param.id}"/>
		<c:set var="pluginPosition" value="${param.pluginPosition }"/>
		<c:set var="showSetting" value="${requestScope.showSetting}"/>
		
		
		<div class="grid-item" data-item-id="${id}"
			data-position="${StringEscapeUtils.escapeXml(pluginPosition)}">
			<div class="panel">
				<div class="panel-heading colorbordered  block__header">
					<h3 class="panel-title  block__header--left ">Product Sync</h3>
					<div class="block__header--right">
						<div>
							<i class="deletePlugin fa fa-times" data-item-id="${id}"
								style="cursor: pointer;"></i>
						</div>
					</div>
				</div>
				<div class="panel-body no-padding">
					<!-- <div id="chartContainer" style="width: 480px;"></div> -->
					<div id="chart${id}" style="height: 300px; width: 480px;"></div>
				</div>
				<div class="panel-footer">
					<div class="row" style="margin-right: 0.1px">
						<span class="col-md-12">
							<a href="<%=contextPath%>/admin/productwaitsync" class="btn dropcolor pull-right">View All Report</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		
<script type="text/javascript">
$(document).ready(function() {
	var id = "<c:out value="${id}"/>"; 
	
    var obj = JSON.parse("<c:out value="${showSetting}"/>".replace(/&#034;/g,'"')); 
	var allCount = obj.allCount;
	var done = obj.done;
	
 	var chart = new CanvasJS.Chart("chart"+id,
	{
			axisY : {
				title : "Product",
				interlacedColor : "Azure",
				tickColor : "navy",
				gridColor : "navy",
				tickLength : 10
			},
			data : [
				{
					showInLegend : true,
					legendText : "All product",
					dataPoints : allCount
				},
				{
					showInLegend : true,
					legendText : "Sync Complete",
					dataPoints : done
				}
			]
		});

		chart.render();
	});
</script>