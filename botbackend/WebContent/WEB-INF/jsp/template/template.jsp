<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String contextPath = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

<title>Priceza Admin</title>

<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />

</head>
<body class="skin-blue">
	<jsp:include page="/WEB-INF/jsp/template/header.jsp" />

	<div class="wrapper row-offcanvas row-offcanvas-left">

		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="/WEB-INF/jsp/template/menuside.jsp" />

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side"> <!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			Dashboard <small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
		</section> <!-- Main content --> <section class="content"> <!-- Content -->




		<!-- /Content --> </section><!-- /.content --> </aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />
</body>
</html>