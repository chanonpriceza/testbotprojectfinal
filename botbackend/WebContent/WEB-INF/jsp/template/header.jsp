<%@page import="botbackend.bean.NotificationApiBean.NOTITYPE"%>
<%@page import="java.util.List"%>
<%@page import="botbackend.bean.NotificationApiBean"%>
<%@ page import="botbackend.bean.UserBean,utils.DateTimeUtil, botbackend.system.BaseConfig, java.net.URLDecoder" %>
<%@page import="botbackend.bean.UserBean"%>
<%@page import="botbackend.component.MonitorConfigComponent"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8" import="org.apache.commons.lang3.StringUtils"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="priceza" prefix="pz"%>
<%
  UserBean userBean;
  HttpSession ses = request.getSession(false);
  String contextPath = request.getContextPath();
  userBean = (UserBean) ses.getAttribute("userData");
  String userName = userBean.getUserName();
  List<NotificationApiBean> notiAttr = (List<NotificationApiBean>)session.getAttribute("notiList");
%>
<style>

.contentNotificationStyle{
	display: inline-flex;
    padding-bottom: 5px;
    padding-top: 5px;
    border-bottom: 1px solid #ddd;
}

.heading-single2 {
    border-bottom: 1px solid #e9e9e9;
    padding: 5px;
}

.readAllNoti {
    border-top: 1px solid #e9e9e9;
    padding: 5px;
    text-align: center;
}

.navbar-default .dropdown-menu.notify-drop .notify-drop-title {
    border-bottom: 1px solid #e2e2e2;
    padding: 5px 15px 10px 15px;
}

.notification-list, .n-user-list {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}

.notification-list > li {
    border-bottom: 1px solid #eee;
    margin-bottom: 5px;
    padding: 5px 0;
}

.notification-list .cat-icon {
    width: 20px;
}

.notification-list .avatar {
    width: 30px;
}

.rounded {
    border-radius: 0.25rem;
    width: 20%
}
.n-user-list{margin-bottom:5px;}
.n-user-list:after{clear:both; content:''; display:table;}
.n-user-list li{float:left;}
.n-user-list li + li{margin-left:3px;}
.n-user-list li a, .n-user-list li a:hober{text-decoration:none;}

.badge-notify{
   background:	#41A43C;
   position:relative;
   top: 500px;
   right: -10px;
   margin-top: -10px; 
   margin-right: -14px;
}

.page-header.navbar {
  background: #d86213;
  background: -webkit-linear-gradient(top, #d86213, #d86c24);
  background: -moz-linear-gradient(top, #d86213, #d86c24);
  background: -ms-linear-gradient(top, #d86213, #d86c24);
  background: -o-linear-gradient(top, #d86213, #d86c24);
  background: linear-gradient(-300deg, #73d0ad, #49c596);
}

body {
  background: #29294d;
}

.logoText {
  font-family: Impact;
  font-size: 22px;
  margin-top: -8px;
  margin-bottom: 0px;
  margin-left: 0px;
  text-align: center;
  color: white;
  width: 235px;
  height: 50px;
}

a.logoText:hover {
  color: white;
  text-decoration: none;
}

 .btn-circle {
  width: 30px; 
  height: 30px;
  border-radius: 15px;
  color: #fff;
} 

.btn-spe {
  padding: 0px 0px 0px!important;
}

.bg {
  padding: 0px 0px 0px!important;
  width: 30px;
    height: 30px;
    border-radius: 15px;
}

.dropdown-spe {
  min-width: 0px;
}

.page-bar-sidebar {
    width: 65px;
    height: 65px;
    float: left;
    display: block;
    padding-right: 4px;
    padding-top: 25px;
    text-align: center;
    color: white;
}

.page-bar-sidebar:hover {
    background: #81dcba;
}

.logo-image{
	width: 200px;
}

#notification_li{
	position:relative
}

#notificationContainer {
	background-color: #fff;
	border: 1px solid rgba(100, 100, 100, .4);
	-webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
	overflow: visible;
	position: absolute;
	top: 30px;
	margin-left: -170px;
	width: 400px;
	z-index: -1;
	display: none; // Enable this after jquery implementation 
}
// Popup Arrow
#notificationContainer:before {
	content: '';
	display: block;
	position: absolute;
	width: 0;
	height: 0;
	color: transparent;
	border: 10px solid black;
	border-color: transparent transparent white;
	margin-top: -20px;
	margin-left: 188px;
}

#notificationTitle{
	font-weight: bold;
	padding: 8px;
	font-size: 13px;
	background-color: #ffffff;
	position: fixed;
	z-index: 1000;
	width: 384px;
	border-bottom: 1px solid #dddddd;
}

#notificationsBody{
	padding: 33px 0px 0px 0px !important;
	min-height:300px;
}

#notificationFooter{
	background-color: #e9eaed;
	text-align: center;
	font-weight: bold;
	padding: 8px;
	font-size: 12px;
	border-top: 1px solid #dddddd;
}
.notiTablinks.active{
	background-color:#caccce;
}
#thTab.active {
	background-color:#0080ff;
}
#idTab.active {
	background-color:#ff8c1a;
}
#myTab.active {
	background-color:#4f9a94;
}
#phTab.active {
	background-color:#ff5c8d;
}
#sgTab.active {
	background-color:#002071;
	color:white;
}
#vnTab.active {
	background-color:#8e0000;
	color:white;
}
#taskContent{
	word-break:break-word;
}

.custom-catption{
    font-size: 18px;
    line-height: 18px;
    padding: 10px 0;
}


</style>


<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
  <!-- BEGIN HEADER INNER -->
  <div class="page-header-inner ">
  
    <!-- BEGIN LOGO -->
    <div class="page-logo">
      <a href="dashboard" class="logoText"><img  class="logo-image" src="<%=contextPath%>/assets/global/img/logoBot_notext_white.png" /></a>
    </div>
    
    <span class="page-bar-sidebar" id="sidebarCollapse">
        <i class="fa fa-bars fa-lg"></i>
    </span>

    <!-- END LOGO -->
    
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    	<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    
    <!-- BEGIN TOP NAVIGATION MENU -->
    <c:choose>
    	<c:when test="${not empty notiList}">
    		<c:set var="notificationValue" value="${notiList}"></c:set>
    	</c:when>
    	<c:when test="${not empty cookie.checkCookieNoti.value}">
    		<c:set var="notificationValue" value="${cookie.checkCookieNoti.value}"></c:set>
    	</c:when>
    	<c:otherwise>
    		<c:set var="notificationValue" value=""></c:set>
    	</c:otherwise>
    </c:choose>
    
    <div class="top-menu">
      <ul class="nav navbar-nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <c:set var="cookieNoti" value="${notificationValue}"></c:set>
        <c:set var="countRead" value="${notRead}"></c:set>
        <c:set var="cookieList" value="${notificationValue}"></c:set>
        <li class="dropdown dropdown-user" style="margin-left: -165px; margin-top: 5px;">
          <c:forEach var="id" items="${cookieList}">
          	<c:choose>
          		<c:when test="${not id.isRead}">
          			<c:set var="idList" value="${id.id},${idList}"/>
          		</c:when>
          	</c:choose>
          </c:forEach>
          <input id="unreadList" type="hidden" value="${idList}">
          <a href="javascript:;" onclick="markRead()" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="display:inline-block; height: 50px;">
          		<img alt="" class="img-circle" src="<%=contextPath %>/assets/layouts/layout/img/notification.png" />
          		<c:choose>
          			<c:when test="${countRead > 0 && countRead < 9 }">
               		 <span id="countReadOnBaddge" class="badge badge-notify" style="background-color:#e7505a">${countRead}</span>
               		 </c:when>
               		 <c:when test="${countRead >= 9}">
               			 <span id="countReadOnBaddge" class="badge badge-notify" style="background-color:#e7505a">9+</span>
               		 </c:when>
          		</c:choose>
          </a>
          <ul class="dropdown-menu notify-drop">
       		<li class="titleNotification">
     			<c:if test="${cookieList != null && cookieList !=''}">
			        <div class="heading-single2">
			        	<span id="countReadOnTitleNoti">Notification (${countRead})</span>
			        </div>       				
  				</c:if>
        	</li>
			<c:choose>
				<c:when test="${cookieList == null || cookieList == '' }">
					<div class="contentNotificationStyle" style="margin-left: 5px">
		           		<c:out value="No Notification"></c:out>
		           	</div>
				</c:when>
				<c:when test="${cookieList !=null && cookieList != '' }">
					<c:forEach var="item" items="${cookieList}">
						<c:set var="noti_id" value="${item.id}" />
						<c:set var="noti_msg" value="${item.message}" />
						<c:set var="noti_status" value="${item.isRead}" />
						<c:set var="noti_creatDate" value="${item.createDate}"/>
						<c:set var="noti_country" value="${item.country}"/>
						<c:choose>  
					    	 <c:when test="${not noti_status}"><c:set var ="colorStatus" value="width: 100%; background-color: #cff7e8;"/></c:when>  
							 <c:otherwise><c:set var ="colorStatus" value="width: 100%;"/></c:otherwise>
						</c:choose>
												<c:choose>  
							<c:when test="${ noti_country == 'TH'}">
								<c:set var ="contrycolor" value="color:#0080ff"/>
							</c:when>
							<c:when test="${noti_country == 'ID'}">
								<c:set var ="contrycolor" value="color:#ff8c1a"/>
							</c:when>
							<c:when test="${noti_country == 'SG'}">
								<c:set var ="contrycolor" value="color:#002071"/>
							</c:when>
							<c:when test="${noti_country == 'MY'}">
								<c:set var ="contrycolor" value="color:#4f9a94"/>
							</c:when>
							<c:when test="${noti_country == 'VN'}">
								<c:set var ="contrycolor" value="color:#8e0000"/>
							</c:when>
							<c:when test="${noti_country == 'PH'}">
								<c:set var ="contrycolor" value="color:#ff5c8d"/>
							</c:when>
						</c:choose>
						<li class="itemNoti" style="${colorStatus} ">
				           	<div class="contentNotificationStyle" style="width: 100%">
				           		<div class="col-md-2">
				           			<img src="<%=contextPath%>/assets/global/img/manwhite.png" class="avatar" alt="..." style="width: 30px; height: 30px">		
				           		</div>
				           		<div class="col-md-8"> 
					           		<div class="row">
						           		<a target="_blank" style="${contrycolor}" href="${serviceUrl[fn:toLowerCase(item.country)]}${item.link}"><div style="font-size: small;"><c:out value="${noti_msg}"></c:out></div></a>
						           		<div style="font-size: smaller;"><i class="fa fa-clock-o"></i><c:out value="${noti_creatDate}"></c:out></div>
					           		</div>  
				           		</div>
				           		<div class="col-md-2">				           	
				           			<div style="${contrycolor}" >${noti_country}</div>         
				           		</div>
				           	</div>
		           		</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<div class="contentNotificationStyle" style="margin-left: 5px">
		           		<c:out value="Refresh and try again"></c:out>
		           	</div>
				</c:otherwise>
			</c:choose>
           	        
           	 <li class="footerNotification">
           		<a class="readAllNoti" href="readAllNotification" name="readAllNoti">Read All</a>
           	</li> 
          </ul>
         
          <!-- END NOTIFICATION DROPDOWN -->
          <input type="hidden" id="getCountNoti" name="getCountNoti" value="${countRead}">
          <!-- BEGIN USER -->
          <li class="dropdown dropdown-user" style="margin-left: -60px; margin-top: 5px;">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-left: -50px; height: 50px; ">
              <img alt="" class="img-circle" src="<%=contextPath%>/assets/global/img/userprofile.png" />
              <span class="username username-hide-on-mobile"> <%=userName%></span> 
              <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default" style="width: 150px;">
              <li>
                <a href="<%=contextPath%>/userlogin?cmd=getLogout">
                  <i class="icon-key"></i> Log Out
                </a>
              </li>
            </ul>
          </li>
          <!-- END USER -->
         
          <!-- BEGIN COUNTRY -->
           <li class="dropdown dropdown-user" style="margin-top: -2px;">
            <a class="dropdown-toggle bg" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-top: 15px; margin-left: 5px;">
              <c:choose>
         
                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'}">
                        <c:set var = "color" value = "#0080ff"/>
                     </c:when>
                     
                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'id'}">
                        <c:set var = "color" value = "#ff8c1a"/>
                     </c:when>
                     
                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'my'}">
                        <c:set var = "color" value = "#4f9a94"/>
                     </c:when>
                     
                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'ph'}">
                        <c:set var = "color" value = "#ff5c8d"/>
                     </c:when>
                     
                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'sg'}">
                        <c:set var = "color" value = "#002071"/>
                     </c:when>
                     
                     <c:otherwise>
                        <c:set var = "color" value = "#8e0000"/>
                     </c:otherwise>
                  </c:choose>
              <button type="button" class="btn btn-default btn-circle btn-spe" style="background: <c:out value = "${color}"/>;">
                <strong style="margin-left: 0px; margin-bottom: 15px;">${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}</strong>
              </button>
            </a>
          
            <ul class="dropdown-menu dropdown-menu-default dropdown-spe" style="width: 50px;">
              <c:forEach items="${BaseConfig.BOT_SERVER_LIST_COUNTRY}" var="country">
                <li>
                <c:choose>
         
                     <c:when test = "${country == 'th'}">
                        <c:set var = "country_path" value = ""/>
                     </c:when>
                     
                     <c:when test = "${country == 'id'}">
                        <c:set var = "country_path" value = "id"/>
                     </c:when>
                     
                     <c:when test = "${country == 'my'}">
                        <c:set var = "country_path" value = "my"/>
                     </c:when>
                     
                     <c:when test = "${country == 'ph'}">
                        <c:set var = "country_path" value = "ph"/>
                     </c:when>
                     
                     <c:when test = "${country == 'sg'}">
                        <c:set var = "country_path" value = "sg"/>
                     </c:when>
                     
                     <c:otherwise>
                        <c:set var = "country_path" value = "vn"/>
                     </c:otherwise>
                  </c:choose>
                  <a target="_blank" href="http://27.254.65.119:8080/botbackend${country_path}/admin/dashboard">${country}</a>
                </li>
              </c:forEach>
            </ul>
          </li> 
          <!-- END COUNTRY -->
        <!-- END USER LOGIN DROPDOWN -->
        <!-- END QUICK SIDEBAR TOGGLER -->
      </ul>
    </div>
    <!-- END TOP NAVIGATION MENU -->
  </div>
  <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<script type="text/javascript">
$('a[name="editSettings"]').click(function() {
	$('input[name=cmd]').val("editSettings");
	$('#addFormMerchant').modal('show'); 
	
});
</script>
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/header-script.js"></script>