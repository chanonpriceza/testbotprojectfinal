<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<% String contextPath = request.getContextPath(); %>

<!-- BEGIN CORE PLUGINS -->
<script src="<%=contextPath %>/assets/global/scripts/jquery-1.12.4.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<%=contextPath %>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/scripts/datatable.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/bootstrap-add-clear-master/bootstrap-add-clear-master/bootstrap-add-clear.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/datatables-1.10.18/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/jquery.bootstrapvalidator/0.5.3/bootstrapValidator.min.js"></script>
<script src="<%=contextPath %>/assets/global/plugins/chart/Chart.min.js"></script>
<script src="<%=contextPath %>/assets/global/plugins/chart/ChartUtil.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<%=contextPath %>/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<%=contextPath %>/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<!-- BEGIN PACKERY  -->
<script src="<%=contextPath %>/assets/global/plugins/packery/packery.pkgd.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/packery/draggabilly.pkgd.min.js" type="text/javascript"></script>
<script src="<%=contextPath %>/assets/global/plugins/tilt/tilt.jquery.min.js" type="text/javascript"></script>
<!-- END PACKERY  -->

<!-- Page specific script -->
<script type="text/javascript">

function startUi(id){
	App.blockUI({
        target: '#'+id+'',
        animate: true
    });
}

function stopUi(id){
	window.setTimeout(function() {
        App.unblockUI('#'+id+'');
    }, 50); 
}

    $(function() {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {//This is to add icons to the visible buttons
                prev: "<span class='fa fa-caret-left'></span>",
                next: "<span class='fa fa-caret-right'></span>",
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random default events
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1),
                    backgroundColor: "#f56954", //red
                    borderColor: "#f56954" //red
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: "#f39c12", //yellow
                    borderColor: "#f39c12" //yellow
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    backgroundColor: "#0073b7", //Blue
                    borderColor: "#0073b7" //Blue
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    backgroundColor: "#00c0ef", //Info (aqua)
                    borderColor: "#00c0ef" //Info (aqua)
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false,
                    backgroundColor: "#00a65a", //Success (green)
                    borderColor: "#00a65a" //Success (green)
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    backgroundColor: "#3c8dbc", //Primary (light-blue)
                    borderColor: "#3c8dbc" //Primary (light-blue)
                }
            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

        /* ADDING EVENTS */
        var currColor = "#f56954"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function(e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            colorChooser
                    .css({"background-color": currColor, "border-color": currColor})
                    .html($(this).text()+' <span class="caret"></span>');
        });
        $("#add-new-event").click(function(e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create event
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
        
        $('.back-drop').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
        });
    });
</script>

<script type="text/javascript">
  // fig error modal and ck editor for ie Layout
	$.fn.modal.Constructor.prototype.enforceFocus = function() {
        var $modalElement = this.$element;
        $(document).on('focusin.modal',function(e) {
                var $parent = $(e.target.parentNode);
                if ($modalElement[0] !== e.target
                                && !$modalElement.has(e.target).length
                                && $(e.target).parentsUntil('*[role="dialog"]').length === 0) {
                        $modalElement.focus();
                }
        });       
};

</script>

<script type="text/javascript">
   $(document).ready(function () {
       $('#sidebarCollapse').on('click', function () {
           $('.sidebar').toggleClass('active');
       });
   });
</script>