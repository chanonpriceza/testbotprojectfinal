<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String contextPath = request.getContextPath();
%>

		<c:set var="id" value="${param.id }"/>
		<c:set var="pluginPosition" value="${param.pluginPosition }"/>
		<c:set var="showSetting" value="${requestScope.showSetting }" />
		
		<div class="grid-item" data-item-id="${id}" data-position="${StringEscapeUtils.escapeXml(pluginPosition)}">
			<div class="panel">
				<div class="panel-heading colorbordered  block__header">
					<h3 class="panel-title  block__header--left ">Tasks</h3>
					<div class="block__header--right">
						<div><i class="deletePlugin fa fa-times" data-item-id="${id}" style="cursor: pointer;"></i></div>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table table-striped">
						<thead>
							<tr class="heading">
								<th style="text-align: center;">#</th>
								<th style="text-align: center;">Owner</th>
								<th style="text-align: center;">Working</th>
								<th style="text-align: center;">Issues</th>
								<th style="text-align: center;">%Problem</th>
							</tr>
						</thead>
					 	<tbody>
							<c:set var="count" value="0"/>
							<c:forEach var="task" items="${showSetting}"> 
								<c:if test="${not empty task}" >
									<tr class='rowPattern'> 
										<td style='text-align:center;'><c:out value="${count = count+1}"/></td>
										<td style='text-align:center;'><c:out value="${task['owner']}"/></td>
										<td style='text-align:center;'><c:out value="${task['total']}"/></td>
										<td style='text-align:center;'><c:out value="${task['issue']}"/></td>
										<td style='text-align:center;'><c:out value="${task['percentage']}"/></td>
									</tr> 								
								</c:if>
							</c:forEach>  
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<div class="row" style=" margin-right: 0.1px">
						<span class="col-md-12">
						<a href="<%=contextPath%>/admin/monitor"class="btn dropcolor pull-right">View All Monitor</a></span>
					</div>
				</div>		
			</div>
		</div>
	
<%-- 		<input type="hidden" class="setPosition" value="${StringEscapeUtils.escapeXml(pluginPosition)}"> --%>
