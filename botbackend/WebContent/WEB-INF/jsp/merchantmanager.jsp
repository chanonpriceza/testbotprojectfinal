<%@ page import="botbackend.bean.UserBean, botbackend.utils.UserUtil, botbackend.utils.Util, botbackend.utils.ViewUtil, botbackend.system.BaseConfig, utils.DateTimeUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="priceza" prefix="pz"%>

<%
  UserBean userBean;
  HttpSession ses = request.getSession(false);
  String contextPath = request.getContextPath();
  userBean = (UserBean) ses.getAttribute("userData");
  String userId = Integer.toString(userBean.getUserId());
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	
	<title>Bot Backend</title>
	
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
	<style>	
		.div-head-topic{
			position: relative; 
			margin-left: 25px
		}
		
		.filter-full-secondary {
		    background: #f6f7f8;
		    position: relative;
		}
		.row-no-padding-margin{
			padding: 0px;
			margin: 0px;
		}
		.scroll_server {
		    overflow: scroll;
		    height: 360px;
		    margin-bottom: 25px; 
		    overflow-x: hidden;
		    overflow-y: auto;
		}
		.scroll_name {
		    overflow: scroll;
		    height: 360px;
		    margin-bottom: 25px;
		    overflow-x: hidden;
		    overflow-y: auto;
		}
		.modal-body {
		    max-height: calc(100vh - 210px);
		    overflow-y: auto;
		}
		.heading-single{
			border-bottom: 1px solid #e9e9e9;
		    padding-bottom: 5px;
		    margin-left: 25px
		}
		
		.ul_set_two_columns {
		  columns: 2;
		  -webkit-columns: 2;
		  -moz-columns: 2;
		}
	</style>
	
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	
	<c:set var ="userId" value= "<%=userId%>"/>
	
	<div class="clearfix"> </div>
    	<div class="page-container">
           	<div class="wrapper">
				<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
	            <div class="page-content-wrapper">
	                <div class="page-content">
	                    <div class="page-bar" style="margin-bottom:2%;">
	                    	<div class="col-md-6">
		                    	<h3 class="page-title"><i class="fa fa-th" style="padding-right: 10px;"></i> Merchant Manager </h3>
			                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Merchant Manager</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    
		                    <c:if test="${not empty warning}">
								<div class="alert alert-danger alert-dismissable fade in">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    	<strong>Error!</strong> <c:out value="${warning }"></c:out>
							  	</div>
							</c:if>
		                    
							<!-- SEARCH & TABLE -->
		                    <div class="row" id="newMerchantContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                            	 <div class="row">
		                            	 	<div class="col-md-12">
	                            				<div class="col-md-10" id= "topBar" >
	                            					<h4 class="txtResult"><span>All result <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${totalmerchantList}" /> record</span></h4>
	                            				</div>
		                            		</div>
		                   				 </div>
		                            	 <div class="row">
		                            	    <div class="col-md-12" style="margin: 25px 20px;">
		                            	   		<div class="row " >
							                   		<div class="col-md-6">
							                   		
							                   			<!-- ######################################################## -->
														<!-- ################ HEADER SEARCH AREA #################### -->
														<!-- ######################################################## -->
							                   		
							                    	   	<form id="headerSearchMerchant" action="<%=contextPath %>/admin/getmerchantdata" method="post">
							                    	   	   	<div class="row" >
									                    	   	<div class="input-group">
									                    	   	   	<div class="input-group-btn">
										         						<button type="button" id="filter" class="btn">Merchant</button>
										                                <input type="hidden" id="headerSearchActiveType" 	class ="activeType" 	name="activeType" value="${activeType}"/> 
										                                <input type="hidden" id="headerSearchPackageType"   class ="packageFilter" 	name="packageFilter" value="${packageFilter}"/>  
									                                </div>                    
												                    <input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:if test="${not empty searchParam}">${searchParam}</c:if>'>
												                    <span class="input-group-btn"><a><button type="submit" id ="searchhBtn" class="btn btn-primary"><i class="icon-magnifier"></i></button></a></span>
												                    
									                            </div>
								                            </div>
									                    </form>     
								                    </div>
								                   
								                    <div class="row">                
									                    <div class="col-md-2">
										            		<button id="addNewMerchant" class="btn btn-primary btn-block">Add New Merchant</button>
										            	</div>  
								                    	<div class="col-md-2">  
									                    	<button id ="btnFilterSearch" class="btn btn-primary" data-toggle="collapse" data-target="#refine-result"><i class="fa fa-filter"></i></button>
										            	</div>  									                    
								                    	<div class="col-md-1">  
								                    		<button id="exportMerchant" class="btn btn-primary" style="float: right;" onclick="exportMerchant()"><i class="fa fa-download"></i>Export Merchant</button>
										            	</div>  									                    
								                	</div>  
							                	</div>                
		                                    </div>
                   						</div>
			                          	
			                          	<!-- ######################################################## -->
										<!-- ##################### SEARCH FILTER #################### -->
										<!-- ######################################################## -->
			                          	
		    							<div id="refine-result" class="collapse">
											<h4 class="modal-title" id="addFormLabel"><i class="fa fa-filter" style="margin-right: 5px"></i>Filter Merchant</h4> 
							        		<div style="text-align: right;"><input type="checkbox" id="checkAll"class="form-check-input"> Check All</div>
							        			<form id="searchfilterMerchant" action="<%=contextPath %>/admin/getmerchantdata" method="post">
													<div style="margin-left: 25px; margin-right: 25px; color: #5b6770 font-size: 13px;">
														<div class="row">
															<div class="col-md-3">
																<h5 class="heading-single"><b>Package</b></h5>														
																<div class="form-check" style="display: flex; margin-left: 45px;">							
								                                	<c:if test="${not empty packageFilter and fn:contains(packageFilter, ',') }">
								                                		<c:set var="firstPackage" value="${pz:getStringBetween(fn:replace(packageFilter, \"'\", \"\"), \"(\", \",\") }" />
								                                		<c:set var="lastPackage" value="${pz:getStringBetween(fn:replace(packageFilter, \"'\", \"\"), \",\", \")\") }" />
							                                		</c:if>
																												
																	<select id="inputFirstPackage" name="inputRangePackage" class="form-control" style="text-align-last: center;width:40%; margin-right: 10px">
																		<c:forEach begin="0" end="20" varStatus="loop">
																			<option value="${loop.index}" <c:if test="${not empty firstPackage && firstPackage == loop.index}">selected="selected"</c:if>>${loop.index}</option>
																		</c:forEach>
		                                                			</select>
	
																	<label class="control-label" for="merchantName" style="margin-right: 10px; margin-top: 5px;"><b> to </b></label>
							                                        
							                                        <select id="inputLastPackage" name="inputRangePackage" class="form-control" style="text-align-last: center;width:40%;">
							                                        <option value="${'20'}">20</option>
							                                        
																		<c:forEach begin="0" end="20" varStatus="loop">
																			<option value="${loop.index}" <c:if test="${not empty lastPackage && lastPackage == loop.index}">selected="selected"</c:if>>${loop.index}</option>
																		</c:forEach>
		                                                			</select>    																
																</div>
																
																<div class="div-head-topic">
																	<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkStatusAll"></div>
																	<h5 class="heading-single"><b>Status</b></h5>
																</div>
															
																<div id="checkStatusAll" class="form-check">
																	<ul style="list-style: none;">
																		<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
																			<li><input type="checkbox" class="checkStatus" name="checkStatus[]" value="${loop.index}" <c:if test="${not empty activeType and fn:contains(activeType, \"'\".concat(loop.index).concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">${ViewUtil.generateShowActive(loop.index)}</label></li>														
																		</c:forEach>
																	</ul>
																</div>
															</div>	
	
															<div class="col-md-9">
																<div class="col-md-12 row-no-padding-margin">
																	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.configserver') }"> 
																		<div class="col-md-3 row-no-padding-margin">
																			<div class="div-head-topic">
																				<h5 class="heading-single"><b>Server</b></h5>
																				<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkServerAll"></div>
																			</div>
																			<c:if test="${not empty BaseConfig.BOT_ACTIVE_SERVER_LIST}" >
																				<div id="checkServerAll" class="form-check scroll_server">
																				 	<ul style="list-style: none;">
																					 	<c:forEach items="${BaseConfig.BOT_ACTIVE_SERVER_LIST}" var="servername"> 
																							<li><input type="checkbox" name="checkServername[]" value="${servername}" <c:if test="${not empty serverType and fn:contains(serverType, \"'\".concat(servername).concat(\"'\"))}">checked="checked"</c:if>><c:out value="${servername}"/></li>
																						</c:forEach>
																					</ul>
																				</div>
																			</c:if>		
																		</div>	
																	</c:if>
																
																	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.configserver') }"> 
																		<div class="col-md-3 row-no-padding-margin">
																			<div class="div-head-topic">
																				<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkDataServerAll"></div>
																				<h5 class="heading-single"><b>Dataserver</b></h5>
																			</div>			
																			<c:if test="${not empty BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" >
																			<div id="checkDataServerAll"class="form-check scroll_server">
																				<ul style="list-style: none;">
																				 	<c:forEach items="${BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" var="dataservername"> 
																						<li><input type="checkbox" name="checkDataServername[]" value="${dataservername}" <c:if test="${not empty dataserverType and fn:contains(dataserverType, \"'\".concat(dataservername).concat(\"'\"))}">checked="checked"</c:if>><c:out value="${dataservername}"/></li>
																					</c:forEach>
																				</ul>
																			</div>
																			</c:if>	
																		</div>
																	</c:if>	
																
																	<div class="col-md-3 row-no-padding-margin">
																		<div class="div-head-topic">
																			<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkOwnerAll"></div>
																			<h5 class="heading-single"><b>Owner</b></h5>
																		</div>
																		<div id ="checkOwnerAll" class="form-check scroll_name">
																			<ul class="" style="list-style: none;">
																			    <c:forEach items="${userList}" var="userItem">
																				    <li><input type="checkbox" class="form-check-input" name="checkOwner[]" value="${userItem.userName}" <c:if test="${not empty userFilter and fn:contains(userFilter, \"'\".concat(userItem.userName).concat(\"'\"))}">checked="checked"</c:if>><c:out value="${userItem.userName}"/></li>
																				</c:forEach>
																			</ul>
																		</div>
																	</div>	
																
																	<div class="col-md-3 row-no-padding-margin">
																		<div class="div-head-topic">
																			<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkLastUpdateAll"></div>
																			<h5 class="heading-single"><b>Last Update</b></h5>
																		</div>
																		<div id="checkLastUpdateAll"class="form-check scroll_name">
																			<ul class="" style="list-style: none;">
																			    <c:forEach items="${userList}" var="userItem">
																				    <li><input type="checkbox" class="form-check-input" name="checkLastUpdate[]" value="${userItem.userName}" <c:if test="${not empty userUpdate and fn:contains(userUpdate, \"'\".concat(userItem.userName).concat(\"'\"))}">checked="checked"</c:if>><c:out value="${userItem.userName}"/></li>
																				</c:forEach>
																			</ul>
																		</div>
																	</div>
																</div>
		                                    				</div>																						
	                                    				</div>								
		                                        	</div>								
					                                <input type="hidden" id="filterActiveType" 		class ="activeType" 	name="activeType" 		value=""/> 
					                                <input type="hidden" id="filterPackageFilter" 	class ="packageFilter" 	name="packageFilter" 	value=""/>  
					                                <input type="hidden" id="filterServerType" 		class ="serverType" 	name="serverType" 		value=""/>
					                                <input type="hidden" id="filterDataserverType"	class ="dataserverType"	name="dataserverType" 	value=""/>  
					                                <input type="hidden" id="filterUserFilter" 		class ="userFilter" 	name="userFilter" 		value=""/>  
					                                <input type="hidden" id="filterUserUpdate" 		class ="userUpdate" 	name="userUpdate" 		value=""/>  
											      	<input type="hidden" id="cmd" 					name="cmd" 				value="searchNewMerchant">
										      		<div style="text-align: right;"> 
									        			<button id ="btnCloseFilter" type="button" class="btn btn-default" data-toggle="collapse" data-target="#refine-result">Cancel</button>
														<button id ="btnConfirmFilter"class="btn btn-primary" onclick="submitFilterSearch();">Confirm</button>
									        		</div>
											      		
												</form>	
											</div>
	    							
								        <!--END FILTER MERCHANT-->	
										
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="newMerchantContent" class="table table-striped table-bordered">
		                                        
					                                <!-- ######################################################## -->
													<!-- ##################### TABLE HEADER ##################### -->
													<!-- ######################################################## -->
									
		                                            <thead>
		                                                <tr class="heading">
		                                                    <th style="width:auto; text-align:center;"> Edit </th>
		                                               		<th style="width:5%;   text-align:center;"> Id </th>
		                                                	<th style="width:auto; text-align:center;"> Merchant Name </th>
		                                                	<th style="width:auto; text-align:center;"> 
																<select name="inputActiveTableHeader" id="inputActiveTableHeader" class="form-control" style="width:auto; padding: 2px 10px;text-align-last: center;width:100%;">
																	<c:if test="${not empty activeType && pz:isContainBracket(activeType) }">
																		<option value="" ${typeFilter }> ${typeFilter } By Filter</option>
																	</c:if>
																	<option value="">Status(All)</option> 
																	<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
																		<c:if test="${(not empty activeType) && not(pz:isContainBracket(activeType))}"><fmt:parseNumber var = "activeTypeNum" type = "number" value = "${activeType}" /></c:if>													
																		<option value="${loop.index}" ${activeTypeNum == loop.index ? 'selected=\"selected\"' : ''}>${ViewUtil.generateShowActive(loop.index)}</option>
																	</c:forEach>
																</select>			
	                                                	 	</th>
		                                                	<th style="width:5%;   text-align:center;">
		                                                		<select id="inputPackageTableHeader" name="inputPackageTableHeader" class="form-control" style="width:auto; padding: 2px 10px;text-align-last: center;width:100%;">
								                                	<c:if test="${not empty packageFilter and !fn:contains(packageFilter, ',') }">
								                                		<c:set var="currentPackage" value="${packageFilter}" />
								                                	</c:if>
								                                	<c:if test="${not empty packageFilter and fn:contains(packageFilter, ',') }">
								                                		<c:set var="currentPackageRange" value="${fn:replace(pz:removeBracketAndQuote(packageFilter), \",\", \"-\")}" />
								                                	</c:if>
																	<c:if test="${not empty currentPackageRange}">
																		<option value="" selected="selected">${currentPackageRange }</option>
																	</c:if>
																	<option value="" <c:if test="${empty packageFilter and empty currentPackageRange}">selected="selected"</c:if>>Package(All)</option>
																	<c:forEach begin="0" end="20" varStatus="loop">
																		<option value="${loop.index}" <c:if test="${not empty currentPackage && currentPackage == loop.index}">selected="selected"</c:if>>${loop.index}</option>
																	</c:forEach>
		                                                		</select>
		                                                	</th>
		                                                	<th style="width:5%;   text-align:center;"> Web Count </th>
		                                                	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.configserver') }">
		                                                		<th style="width:auto; text-align:center;"> Server </th>
	                                                		</c:if>
		                                                	<th style="width:auto; text-align:center;"> Note </th>
		                                                	<th style="width:auto; text-align:center;"> Owner & Update </th>
		                                                	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.submit') }">
		                                                    	<th style="width:auto; text-align:center;"> Action </th> 
		                                                    </c:if>
		                                                	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.command') }">
		                                                    	<th style="width:auto; text-align:center;"> WCE Queue </th> 
		                                                    	<th style="width:auto; text-align:center;"> Due Queue </th> 
		                                                    </c:if>
		                                                    <c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.command.delete') }">
		                                                    	<th style="width:auto; text-align:center;"> Delete </th> 
		                                                    </c:if>
		                                               	</tr>        
		                                            </thead>
		                                            
	                                            	<!-- ######################################################## -->
													<!-- ##################### TABLE HEADER ##################### -->
													<!-- ######################################################## -->
													
		                                            <c:choose>
		                                            	<c:when test="${not empty merchantList}">
		                                            		<tbody>
				                                        		<c:forEach items="${merchantList}" var="merchant"> 
				                                        			<tr class='rowMerchant'>
				                                        				<td style="text-align:center;">
				                                        					<div class="btn-group">
								                                           		<button class="editNewMerchantButton btn btn-primary btn-info dropdown-toggle" data-element-id="${merchant.id},${merchant.server},${merchant.countWebProduct}" data-toggle="dropdown">Edit<i class="fa fa-wrench"></i><span class="sr-only">Toggle Dropdown</span></button>
								                                           		<ul class="dropdown-menu" role="menu">
																					<li><a class = "editNewMerchant" onclick="showModalEditMerchant('${merchant.id}','${merchant.server}','${merchant.countWebProduct}')" name="editNewMerchant">Edit Merchant</a></li>
																					<li><a class = "managePattern" data-element-id="${merchant.id}" name="managePattern">Manage Pattern</a></li>
																					<li><a class = "manageParser" data-element-id="${merchant.id}" name="manageParser">Manage Parser</a></li>
								                                                	<c:if test="${sessionScope.Permission.containsKey('all') || sessionScope.Permission.containsKey('merchant.merchantmanager.checksummary') }">
								                                               			<li><a class = "checkMerchant" data-element-id="${merchant.id}" name="checkMerchant">Check Summary</a></li>
								                                               		</c:if>
								                                               		<c:if test="${sessionScope.Permission.containsKey('all') || sessionScope.Permission.containsKey('merchant.merchantmanager.checkapproval') }">
								                                               			<li><a class = "checkApproval" data-element-id="${merchant.id}" name="checkApproval">Check Approve</a></li>
								                                               		</c:if>
																				</ul>
																			</div>	
				                                        				</td>
				                                        				<td style="text-align:center;"><c:out value="${merchant.id}"/></td>
				                                        				<td style="text-align:left;">
				                                        					<c:choose>
				                                        						<c:when test="${fn:contains(\"|\".concat(merchant.coreUserConfig ),\"|\".concat(userId).concat(\"|\"))}">
				                                        							<c:set var ="favoriteMerchant" value="display: inline-block;"/>
				                                        							<c:set var ="nofavoriteMerchant" value="display: none;"/>
				                                        						</c:when>
				                                        						<c:otherwise>																								
				                                        							<c:set var ="favoriteMerchant" value="display: none;"/>
				                                        							<c:set var ="nofavoriteMerchant" value="display: inline-block;"/>
				                                        						</c:otherwise>
				                                        					</c:choose>
				                                        					<a id="favoriteMerchant${merchant.id}${merchant.server}"   class="favoriteMerchant fa fa-star"     style="${favoriteMerchant} 	text-decoration:none; color: #F68B0B;" aria-hidden="true" data-placement='right'  data-element-id="${merchant.id},${merchant.server}"></a>
															   				<a id="nofavoriteMerchant${merchant.id}${merchant.server}" class="nofavoriteMerchant fa fa-star-o" style="${nofavoriteMerchant} text-decoration:none; color: #F68B0B;" aria-hidden="true" data-placement='right'  data-element-id="${merchant.id},${merchant.server}"></a>
															  				<c:out value="${merchant.name}"/>
																		</td>
																		<td style="text-align:center;">
																			<span id="textstatus${merchant.id}${merchant.server}"><c:out value="${ViewUtil.generateShowActive(merchant.active)}"/></span>
																		</td>
																		<td style="text-align:center;">
																			<c:out value="${merchant.packageType}"/>
																		</td>
																		<td style='text-align:center;'>															  		
																	  		<c:choose>
					                                                			<c:when test="${empty merchant.countWebProduct && merchant.countWebProduct == null}"><c:out value="${'Unknown'}"/></c:when>
					                                                			<c:otherwise><c:out value="${merchant.countWebProduct}"/></c:otherwise>
				                                                			</c:choose>
																	    </td>
																	    <c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.configserver') }">
																		    <td style="text-align:left;">
																		    	<i style="width:18px; text-align:center;" class="fa fa-television"></i> <c:out value="${empty merchant.server ? '-' : merchant.server}"/><br/>
																		    	<i style="width:18px; text-align:center;" class="fa fa-database"></i> <c:out value="${empty merchant.dataServer ? '-' : merchant.dataServer}"/>
																		    </td>
																	    </c:if>
																	    <td>
																	    	<a style="width:18px; text-align:center; color:black;" class='fa fa-user' data-toggle='tooltip' data-placement='top' style="text-decoration:none;" title="<c:out value="${empty merchant.noteDate ? '' : DateTimeUtil.generateStringDateTime(merchant.noteDate)}" />"></a>
																			<a class="rowNote" data-name="note" data-pk="<c:out value="${merchant.id},${merchant.server}"/>" data-url="savenotemerchant" data-original-title="Enter note" tabindex="-1">
																	    		<c:out value="${merchant.note}"/>
																	    	</a><br/>
																	    	<i style="width:18px; text-align:center;" class="fa fa-info-circle"></i><span id="textErrorMsg${merchant.id}${merchant.server}"><c:out value="${empty merchant.errorMessage ? '' : merchant.errorMessage }" /></span>
																	    </td>
																	    <td>
																	    	<i style="width:18px; text-align:center;" class="fa fa-user"></i> <c:out value="${merchant.ownerName }" /><br/>
																	    	<c:choose>
																	    		<c:when test="${empty merchant.lastUpdate }">
																	    			<a style="width:18px; text-align:center;" class='glyphicon glyphicon-time'></a>
																	    		</c:when>
																	    		<c:otherwise>
																			    	<a style="width:18px; text-align:center; color:black;" class='glyphicon glyphicon-time' data-toggle='tooltip' data-placement='bottom' style="text-decoration:none;" title="<c:out value="${DateTimeUtil.generateStringDateTime(merchant.lastUpdate)}" />"></a>
																			    	<c:out value="${merchant.userUpdate }" />
																	    		</c:otherwise>
																	    	</c:choose>
																	    </td>
																	    <c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.submit')||sessionScope.Permission.containsKey('merchant.merchantmanager.command') }">
																	    	<td style="text-align:center;">
																	    		<center>
																		    		<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.submit')}">
																						<c:choose>  
																		    				<c:when test="${merchant.active == 1 || merchant.active == 3}">
																		    					<c:set var ="btnSubmitMerchant" value="display: none;"/>
																		    					<c:set var ="btnStopMerchant" value="display: block;"/>
																		    				</c:when>  
																				 			<c:otherwise>
																				 				<c:set var ="btnSubmitMerchant" value="display: block;"/>
																				 				<c:set var ="btnStopMerchant" value="display: none;"/>
																				 			</c:otherwise>
																			 			</c:choose>
																				    	<button id="submitNewMerchant${merchant.id}${merchant.server}" style="${btnSubmitMerchant} background-color:#ABEBC6; width:75px;" onclick="submitMerchant('${merchant.id}','${merchant.server}')" type="button" class="btn submitNewMerchant">Submit</button>
																						<button id="stopNewMerchant${merchant.id}${merchant.server}"   style="${btnStopMerchant} background-color:#EC7063; width:75px;" onclick="stopMerchantShowModal('${merchant.id}','${merchant.server}','${merchant.errorMessage}')" type="button" class="btn stopNewMerchant">Stop</button>
																		    		</c:if>
																		    	</center>
																		    </td>
																	    </c:if>
															    		<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.command')}">
															    		<td style="text-align:center;">
															    				<c:if test="${not empty merchant.wceNextStart || not empty merchant.dueNextStart  }">
																    				<center>
																		    			<c:choose>  
																					    	<c:when test="${merchant.state == 'BOT_FINISH' && merchant.status == 'FINISH'}">
																					    		<c:choose>  
																							    	<c:when test="${fn:contains(merchant.command, 'PRIMARY_WCE') }">
																										<button id="btnPrimaryQueueWCEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:block; 	width 85px ;background-color: #55dbe6;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnPrimaryQueueWCEWithComamnd','${merchant.id}','${merchant.server}','PRIMARY_WCE','REMOVE');">RUN_WCE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnPrimaryQueueWCE${merchant.id}${merchant.server}" 			class="btn" style="display:none; 	width 85px ;background-color: #55dbe6;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnPrimaryQueueWCE','${merchant.id}','${merchant.server}','PRIMARY_WCE','ADD');">RUN_WCE</button>
																									</c:when>  
																									<c:otherwise>
																										<button id="btnPrimaryQueueWCEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:none; 	width 85px ;background-color: #55dbe6;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnPrimaryQueueWCEWithComamnd','${merchant.id}','${merchant.server}','PRIMARY_WCE','REMOVE');">RUN_WCE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnPrimaryQueueWCE${merchant.id}${merchant.server}" 			class="btn" style="display:block; 	width 85px ;background-color: #55dbe6;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnPrimaryQueueWCE','${merchant.id}','${merchant.server}','PRIMARY_WCE','ADD');">RUN_WCE</button>
																									</c:otherwise>
																								</c:choose>
																					    	</c:when>  
																							<c:otherwise>
																								<c:choose>  
																							    	<c:when test="${fn:contains(merchant.command, 'KILL_WCE') }">
																										<button id="btnKillQueueWCEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:block; 	width 85px;background-color: #ef9c62;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnKillQueueWCEWithComamnd','${merchant.id}','${merchant.server}','KILL_WCE','REMOVE');">KILL_WCE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnKillQueueWCE${merchant.id}${merchant.server}" 				class="btn" style="display:none; 	width 85px;background-color: #ef9c62;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnKillQueueWCE','${merchant.id}','${merchant.server}','KILL_WCE','ADD');">KILL_WCE</button>
																									</c:when>  
																									<c:otherwise>
																										<button id="btnKillQueueWCEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:none; 	width 85px;background-color: #ef9c62;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnKillQueueWCEWithComamnd','${merchant.id}','${merchant.server}','KILL_WCE','REMOVE');">KILL_WCE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnKillQueueWCE${merchant.id}${merchant.server}" 				class="btn" style="display:block; 	width 85px;background-color: #ef9c62;" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandWCE('btnKillQueueWCE','${merchant.id}','${merchant.server}','KILL_WCE','ADD');">KILL_WCE</button>
																									</c:otherwise>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</center>
																				</c:if>
																			<td style="text-align:center;">
																				<c:if test="${not empty merchant.wceNextStart || not empty merchant.dueNextStart  }">
																					<center>
																						<c:choose>  
																					    	<c:when test="${merchant.dataUpdateState == 'BOT_FINISH' && merchant.dataUpdateStatus == 'FINISH'}">
																					    		<c:choose>  
																							    	<c:when test="${fn:contains(merchant.command, 'PRIMARY_DUE') }">
																										<button id="btnPrimaryQueueDUEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:block; width 85px ;background-color: #55dbe6;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>"  onclick="updateCommandDUE('btnPrimaryQueueDUEWithComamnd','${merchant.id}','${merchant.server}','PRIMARY_DUE','REMOVE');">RUN_DUE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnPrimaryQueueDUE${merchant.id}${merchant.server}" 			class="btn" style="display:none;width 85px ;background-color: #55dbe6;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnPrimaryQueueDUE','${merchant.id}','${merchant.server}','PRIMARY_DUE','ADD');">RUN_DUE</button>
																									</c:when>  
																									<c:otherwise>
																										<button id="btnPrimaryQueueDUEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px ;background-color: #55dbe6;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnPrimaryQueueDUEWithComamnd','${merchant.id}','${merchant.server}','PRIMARY_DUE','REMOVE');">RUN_DUE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnPrimaryQueueDUE${merchant.id}${merchant.server}" 			class="btn" style="display:block;width 85px ;background-color: #55dbe6;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnPrimaryQueueDUE','${merchant.id}','${merchant.server}','PRIMARY_DUE','ADD');">RUN_DUE</button>
																									</c:otherwise>
																								</c:choose>
																					    	</c:when>  
																							<c:otherwise>
																								<c:choose>  
																							    	<c:when test="${fn:contains(merchant.command, 'KILL_DUE') }">
																										<button id="btnKillQueueDUEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:block; width 85px;background-color: #ef9c62;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnKillQueueDUEWithComamnd','${merchant.id}','${merchant.server}','KILL_DUE','REMOVE');">KILL_DUE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnKillQueueDUE${merchant.id}${merchant.server}" 				class="btn" style="display:none; width 85px;background-color: #ef9c62;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnKillQueueDUE','${merchant.id}','${merchant.server}','KILL_DUE','ADD');">KILL_DUE</button>
																									</c:when>  
																									<c:otherwise>
																										<button id="btnKillQueueDUEWithComamnd${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: #ef9c62;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnKillQueueDUEWithComamnd','${merchant.id}','${merchant.server}','KILL_DUE','REMOVE');">KILL_DUE<i class="fa fa-check-circle-o"></i></button>																				
																										<button id="btnKillQueueDUE${merchant.id}${merchant.server}" 				class="btn" style="display:block; width 85px;background-color: #ef9c62;" 	type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i>" onclick="updateCommandDUE('btnKillQueueDUE','${merchant.id}','${merchant.server}','KILL_DUE','ADD');">KILL_DUE</button>
																									</c:otherwise>	
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																	    			</center>
																	    		</c:if>
																    			</td>
																			  </c:if>																	    	
																    		<c:if test="${sessionScope.Permission.containsKey('all') ||sessionScope.Permission.containsKey('merchant.merchantmanager.command.delete')  && not empty merchant.wceNextStart && not empty merchant.dueNextStart}">
																    			<td style="text-align:center;">
																				<div class="btn-group">
																    			<c:if test="${not empty merchant.wceNextStart || not empty merchant.dueNextStart }">
																    				<c:choose>  
																						<c:when test="${!fn:contains(merchant.command, 'DELETE_ALL') && !fn:contains(merchant.command, 'DELETE_NOT_MAP') }">
																    						<button id="btnCheckDelete${merchant.id}${merchant.server}"  	class="btn dropdown-toggle" style="display:block; width 85px;background-color: #e6e6e6;width:96%;" 	type="button" data-toggle="dropdown" data-element-id="${merchant.id},${merchant.server},${merchant.countWebProduct}"  > check <span class="sr-only">Toggle Dropdown</span></button>
																    						<ul class="dropdown-menu">
																								<li><a onclick="checkDeleteCommand('btnCheckDelete','${merchant.id}','${merchant.dataServer}','${merchant.server}','DELETE_ALL','ADD')" >DELETE ALL</a></li>
																								<li><a onclick="checkDeleteCommand('btnCheckDelete','${merchant.id}','${merchant.dataServer}','${merchant.server}','DELETE_NOT_MAP','ADD')">DELETE NOT MAP</a></li>
																							</ul>
																    						<button id="btnDeleteProduct${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: #EC7063; width:96%;" 	type="button"  onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_ALL','ADD');">Delete <i class="fa fa-trash-o"></i></button>
																    						<button id="btnDeleteNonCat${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: red; width:96%;" 	type="button"  onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_NOT_MAP','ADD');">Not Map<i class="fa fa-trash-o"></i></button>	
															    							<c:choose> 
															    								<c:when test="${merchant.dataUpdateState == 'BOT_FINISH' && merchant.dataUpdateStatus == 'FINISH' }">
																    								<button id="btnWaitingDeleteProduct${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: #ffff66;width:96%;" 	type="button" onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_ALL','REMOVE');">Delete<i class="fa fa-check-circle-o"></i></button>
																    							</c:when>
																    							<c:otherwise>
																    								<button id="btnWaitingDeleteProduct${merchant.id}${merchant.server}" disabled="disabled" class="btn" style="display:none; width 85px;background-color: #ffff66;width:96%;" 	type="button" >Deleting <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i></button>
																    								<button id="btnWaitingDeleteProduct${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: #ffff66;width:96%;" 	type="button" onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_ALL','REMOVE');">Delete<i class="fa fa-check-circle-o"></i></button>												    						
																    							</c:otherwise>
																    						</c:choose>
																    					</c:when>
																    					<c:otherwise>
																    						<button id="btnCheckDelete${merchant.id}${merchant.server}" class="btn dropdown-toggle" style="display:none; width 85px;background-color: #e6e6e6;width:96%;" 	type="button" data-toggle="dropdown" data-element-id="${merchant.id},${merchant.server},${merchant.countWebProduct}"  > check <span class="sr-only">Toggle Dropdown</span></button>
																    						<ul class="dropdown-menu">
																								<li><a onclick="checkDeleteCommand('btnCheckDelete','${merchant.id}','${merchant.dataServer}','${merchant.server}','DELETE_ALL','ADD')" >DELETE ALL</a></li>
																								<li><a onclick="checkDeleteCommand('btnCheckDelete','${merchant.id}','${merchant.dataServer}','${merchant.server}','DELETE_NOT_MAP','ADD')">DELETE NOT MAP</a></li>
																							</ul>
																    						<button id="btnDeleteProduct${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: #EC7063; width:96%;" 	type="button"  onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_ALL','ADD');">Delete <i class="fa fa-trash-o"></i></button>
																    						<button id="btnDeleteNonCat${merchant.id}${merchant.server}" 	class="btn" style="display:none; width 85px;background-color: red; width:96%;" 	type="button"  onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_NOT_MAP','ADD');">Not Map<i class="fa fa-trash-o"></i></button>	
															    							<c:choose>  
																    							<c:when test="${merchant.dataUpdateState == 'BOT_FINISH' && merchant.dataUpdateStatus == 'FINISH' }">
																    								<button id="btnWaitingDeleteProduct${merchant.id}${merchant.server}" 	class="btn" style="display:block; width 85px;background-color: #ffff66;width:96%;" 	type="button" onclick="deleteProduct('btnDeleteProduct','${merchant.id}','${merchant.server}','DELETE_ALL','REMOVE');">Delete<i class="fa fa-check-circle-o"></i></button>
																    							</c:when>
																    							<c:otherwise>
																    								<button id="btnWaitingDeleteProduct${merchant.id}${merchant.server}" disabled="disabled" class="btn" style="display:block; width 85px;background-color: #ffff66;width:96%;" type="button" >Deleting <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i></button>												    						
																    							</c:otherwise>
																    						</c:choose>
																    					</c:otherwise>
																    				</c:choose>
																	   		 	</c:if>
																	   		 	</div>
																	   		 </td>
																    	</c:if>
				                                        			</tr>
																</c:forEach>
			                                            	</tbody> 
			                                            </c:when>
				                                        <c:otherwise>
				                                        	<h4>No Merchant Data</h4>
				                                        </c:otherwise>
		                                       		</c:choose>	   	
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
 
							<!-- PAGING -->
					        <div class="row" style="float: right;">
					          	<div class="col-md-12">
					                <c:if test="${not empty pagingBean}" >
					                <c:set var ="pagingBean" value="${pagingBean}"/>
									<div class="dataTables_paginate paging_bootstrap">
										<ul class="pagination">
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == 1}">
											       <li class="prev disabled"><a href="#">← Previous</a></li>
											    </c:when>    
											    <c:otherwise>
											     <li class="prev"><a href="<%=contextPath%>/admin/getmerchantdata?page=${pagingBean.getCurrentPage()-1}&searchParam=${searchParam}&activeType=${activeType}&serverType=${serverType}&dataserverType=${dataserverType}&userFilter=${userFilter}&packageFilter=${packageFilter}&userUpdate=${userUpdate}">← Previous</a></li>			
											    </c:otherwise>
											</c:choose>
											<c:forEach items="${pagingBean.getPagingList()}" var="pageNo"> 
												<c:choose>
											    <c:when test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
											       <li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
											    </c:when>
											    <c:when test="${pageNo.equals('...')}">
											       <li><a href="#">...</a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li><a href="<%=contextPath%>/admin/getmerchantdata?page=${pageNo}&searchParam=${searchParam}&activeType=${activeType}&serverType=${serverType}&dataserverType=${dataserverType}&userFilter=${userFilter}&packageFilter=${packageFilter}&userUpdate=${userUpdate}">${pageNo}</a></li>
											    </c:otherwise>
											</c:choose>
											
											</c:forEach>
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
											       <li class="next disabled"><a href="#">Next → </a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li class="next"><a href="<%=contextPath%>/admin/getmerchantdata?page=${pagingBean.getCurrentPage()+1}&searchParam=${searchParam}&activeType=${activeType}&serverType=${serverType}&dataserverType=${dataserverType}&userFilter=${userFilter}&packageFilter=${packageFilter}&userUpdate=${userUpdate}">Next → </a></li>
											    </c:otherwise>
											</c:choose>
										</ul>
									</div>
								</c:if>
					       	</div>
					    </div>
		               	
		                	<!--START ADD MODAL -->
							<div class="row">
				       			<div id="addFormMerchant" class="modal fade back-drop" >
									<div class="modal-dialog modal-xs">
									 	<div class="modal-content">
									      	<div class="modal-header">
									        	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
									        	<h4 class="modal-title" id="addFormLabel"><b>New Merchant Manager</b></h4>
									      	</div>
									      	<form id="addNewMerchantForm" action="<%=contextPath %>/admin/addnewmerchant" method="post">
									      		<div class="modal-body">
													<div style="margin-left: 25px; margin-right: 25px;">
														<div class="row">
															<div class="form-group">
					                                            <label class="control-label" for="addMerchantName"><b>Merchant Name</b></label>&nbsp;<span class="text-danger">*</span>
					                                            <div class="row">
						                                            <div class="col-md-12">
						                                            	<input type="text" id="addMerchantName" name="merchantName" class="form-control" >
					                                        		</div>
				                                        		</div>
				                                        	</div>
		                                        		</div>
		                                        		<div class="row" style="margin-bottom: 10px">
			                                        		<div class="form-group">
		                                        				<label class="control-label" for="addSiteUrl"><b>Site URL</b></label>
		                                        				<div class="row">
		                                        					<div class="col-md-12">
		                                        						<input type="text" id="addSiteUrl" name="siteUrl" class="form-control">
		                                        					</div>
		                                        				</div> 
			                                        		</div>
				                                        	<div class="col-md-12" style="padding: 0px">
																<div class="col-md-6" style="padding: 0px">
						                                            <label class="control-label" for="addPackageType"><b>Package Type</b></label>
							                                        <input type="text" id="addPackageType" name="packageType" class="form-control" style="width: 95%;">
						                                        </div>
						                                        <div class="col-md-6" style="padding: 0px">
				                                        			<label class="control-label" for="addSaleName"><b>Sale Name</b></label>
					                                        		<input type="text" id="addSaleName" name="saleName" class="form-control" style="width: 100%;">
						                                        </div>
					                                        </div>	
					                                        <div class="col-md-12" style="padding: 0px">
						                                        <div class="col-md-6" style="padding: 0px">
			                                        		        <label class="control-label" for="addMerchantType"><b>Merchant Type</b></label>	
							                                         <div>
																		<select id="addMerchantType" name="merchantType" class="form-control" style="width:95%;">
																			<option value="test">Test merchant</option>
				                                        					<option value="customer">Customer</option>
																		</select>
																	</div>
																</div>
						                                        <div class="col-md-6" style="padding: 0px">
				                                        			<label class="control-label" for="addNote"><b>Note</b></label>
					                                        		<input type="text" id="addNote" name="note" class="form-control" style="width: 100%;">
						                                        </div>
															</div>
														</div>
		                                        	</div>								
									      		</div>
										      	<div class="modal-footer">
												   	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												   	<button type="button" id="addFormReset" class="btn btn-primary">Clear</button>
													<button type="submit" id ="addSubmitbtn"class="btn btn-primary">Submit</button>
										    	</div>
											</form>
									 	</div>
									</div>
								</div>
			       			</div>
		               	
		                	<!--START EDIT MODAL -->
							<div class="row">
				       			<div id="editFormMerchant" class="modal fade back-drop" >
									<div class="modal-dialog modal-xs">
									 	<div class="modal-content">
									      	<div class="modal-header">
									        	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
									        	<h4 class="modal-title" id="addFormLabel"><b>Edit Merchant Manager</b></h4>
									      	</div>
									      	<form id="editMerchantForm" action="<%=contextPath %>/admin/editmerchant" method="post">
									      		<div class="modal-body">
													<div style="margin-left: 25px; margin-right: 25px;">
														<div class="row">
															<div class="form-group">
					                                            <div class="col-md-12">
					                                            <label class="control-label" for="editMerchantName"><b>Merchant Name</b></label>&nbsp;<span class="text-danger">*</span>
					                                            	<input type="text" id="editMerchantName" name="merchantName" class="form-control" >
				                                        		</div>
				                                        	</div>
		                                        		</div>
		                                        		<div class="row">
		                                        			<div class="form-group">
		                                        				<div class="col-md-12">
																	<div class="col-md-6" style="padding: 0px">
							                                            <label class="control-label" for="editPackageType"><b>Package Type</b></label>
								                                        <input type="text" id="editPackageType" name="packageType" class="form-control" style="width: 95%;">
							                                        </div>
							                                        <div class="col-md-6" style="padding: 0px">
					                                        			<label class="control-label" for="note"><b>Note</b></label>
						                                        		<input type="text" id="editMerchantNote" name="note" class="form-control" style="width: 100%;">
							                                        </div>
						                                        </div>
					                                        </div>
			                                            </div>  
			                                                   
			                                       		<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.configserver') }">  
				                                       		<div class="row">
				                                       			<div class="form-group">
				                                       				<div class="col-md-12">
			                                      		         		<div class="col-md-6" style="padding: 0px">
						                                      		        <label class="control-label"><b>Server</b><span class="text-danger">*</span></label>
							                                        		<div>
							                                        			<select id="editServername" name="servername" class="form-control" style="width: 95%;">
																				 <c:if test="${not empty BaseConfig.BOT_ACTIVE_SERVER_LIST}" >
																				 	<c:forEach items="${BaseConfig.BOT_ACTIVE_SERVER_LIST}" var="server"> 
																						<option value="${server}"><c:out value="${server}"/></option>									
																					</c:forEach>
																				 </c:if>
																				</select>
							                                        		</div>
					                                      		 		</div>		                               
									                                 	<div class="col-md-6" style="padding: 0px">
						                                      		    	<label class="control-label"><b>DataServer</b><span class="text-danger">*</span></label>
							                                        		<div>
																				<select id="editDataservername" name="dataservername" class="form-control" style="width:100%;">
																					 <c:if test="${not empty BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" >
																					 	<c:forEach items="${BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" var="dataservername"> 
																							<option value="${dataservername}"><c:out value="${dataservername}"/></option>									
																						</c:forEach>
																					 </c:if>
																				</select>
																			</div>
																		</div>
				                                       				</div>
				                                       			</div>
															</div>
														</c:if> 
														<div class="row">
			                                       			<div class="form-group">
			                                       				<div class="col-md-12">
			                                       					<div class="col-md-6" style="padding: 0px">
				                                       		   			<label class="control-label"><b>Status</b><span class="text-danger">*</span></label>	
								                                        <div>
																			<select id="editActive" name="active" class="form-control" style="width:95%;">
																				<c:choose>
																					<c:when test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('merchant.merchantmanager.analyzeactive') }">
																						<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
																							<option value="${loop.index}"><c:out value="${ViewUtil.generateShowActive(loop.index)}"></c:out></option>
																						</c:forEach>
																					</c:when>
																					<c:otherwise>
																						<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
																							<c:if test="${loop.index != 1 && loop.index != 4}">
																								<option value="${loop.index}" ><c:out value="${ViewUtil.generateShowActive(loop.index)}"></c:out></option>
																							</c:if>
																							<c:if test="${loop.index == 1 || loop.index == 4}">
																								<option value="${loop.index}" style="display: none;"><c:out value="${ViewUtil.generateShowActive(loop.index)}"></c:out></option>
																							</c:if>
																						</c:forEach>
																					</c:otherwise>
																				</c:choose>
																			</select>
																		</div>
																	</div>
																  	<div class="col-md-6" style="padding: 0px">
					                                      		         <label class="control-label"><b>Owner</b><span class="text-danger">*</span></label>
						                                        		<div>
																			<select id="editOwnerName" name="ownerName" class="form-control" style="width:100%;">
																			<c:if test="${not empty userList}">
																				<c:forEach items="${userList}" var="ownerName">
																					<option value="${ownerName.userName}">${ownerName.userName}</option>
																				</c:forEach>
																			</c:if>
																			</select>
																		</div>
																	</div>
			                                       				</div>
			                                       			</div>
		                                       			</div>
		                                        	</div>								
									      		</div>
										      	<div class="modal-footer">
										      		<input type="hidden" id="editMerchantID" name="merchantID">
										      		<input type="hidden" id="editCurrentserver" name="currentserver">
										      		<input type="hidden" id="editCurrentDataserver" name="currentdataserver">
												   	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												   	<button type="button" id="editFormReset" class="btn btn-primary">Clear</button>
													<button type="submit" id ="editSubmitbtn"class="btn btn-primary">Submit</button>
										    	</div>
											</form>
									 	</div>
									</div>
								</div>
			       			</div>
							
							<!--START MODAL STOP MERCHANT-->
							<div class="modal fade" id="modalStop">
								<div class="modal-dialog" style="width: 25%;">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<!-- <h4 class="modal-title" style="color: #FFC300;">Add Error Message</h4> -->
										</div>
										<div class="modal-body" id="modalAlertAdd">
											<div class="row">
												<div class="col-md-12">											
		                                            <label class="control-label" for="merchantName"><b>Please enter problems </b></label>&nbsp;<span class="text-danger">*</span>
			                                        <input type="text" id="txtErrorMsg" name="txtErrorMsg" class="form-control" >
                                        		</div>	
											</div>
										</div>
										<div class="modal-footer">
											<input type="hidden" id="stopMerchantId" name="stopMerchantId">
											<input type="hidden" id="stopServer" name="stopServer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveErrorMsg" onclick="confirmStop()" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Confirm</button>
										</div>
									</div>
								</div>
							</div>
									
							<!--START MODAL ALERT-->			
							<div class="modal fade" id="modalAlert" style="top: 5%;">
			    				<div class="modal-dialog" style="width: 40%;">
			    					<div class="modal-content">
				        				<div class="modal-header">
				          					<button type="button" class="close closeBtn" data-dismiss="modal">&times;</button>
				          					<h4 class="modal-title" id="modalAlertTitle"></h4>
				        				</div>
				       					<div class="modal-body" id="modalAlertData"></div>
				    					<div class="modal-footer">
				          					<button type="button" class="btn btn-default closeBtn" data-dismiss="modal">Close</button>
				        				</div>
			    					</div>
			    				</div>
    						</div>	
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
			
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/merchantmanager-script.js"></script>

</body>
</html>