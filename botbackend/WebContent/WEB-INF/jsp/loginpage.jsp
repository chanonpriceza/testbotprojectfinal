<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% String contextPath = request.getContextPath(); %>
<%@ page import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
	<head>
		
		<title>Kuy</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
<style>
.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
    text-decoration: none;
    color: #262626;
    background-color: #bfbfbf;
}
#login-box{
	margin-top: 10%;
}

.TitleLogin-text{
 	float:left;
    width:50%;
    height:100%;
    text-align:center;
    display: inline-block;
}

.dropdown-menu {
    min-width: 54px;
}

.limiter {
  width: 100%;
  margin: 0 auto;
}

.container-login100 {
  width: 100%;  
  min-height: 100vh;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  padding: 15px;
  background: #9053c7;
  background: -webkit-linear-gradient(-135deg, #c850c0, #4158d0);
  background: -o-linear-gradient(-135deg, #c850c0, #4158d0);
  background: -moz-linear-gradient(-135deg, #c850c0, #4158d0);
  /* background: linear-gradient(-135deg, #c850c0, #4158d0); */
  background: linear-gradient(-135deg, #b5e8d5, #ffffba);
}

.wrap-login100 {
  width: 85%;
/*   width: 960px; */
  background: #fff;
  border-radius: 10px;
  overflow: hidden;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  /* padding: 177px 130px 33px 95px; */
  padding: 10%;
}
.login100-pic {
  width: 316px;
}

.login100-pic img {
  max-width: 100%;
}


.login100-form {
  width: 290px;
}

.login100-form-title {
  /* font-family: Poppins-Bold; */
  font-size: 24px;
  color: #333333;
  line-height: 1.2;
  text-align: center;

  width: 100%;
  display: block;
  padding-bottom: 54px;
}


/*---------------------------------------------*/
.wrap-input100 {
  position: relative;
  width: 100%;
  z-index: 1;
  margin-bottom: 10px;
}

.input100 {
  /* font-family: Poppins-Medium; */
  font-size: 15px;
  line-height: 1.5;
  color: #666666;

  display: block;
  width: 100%;
  background: #e6e6e6;
  height: 40px;
  border-radius: 25px;
  padding: 0 30px 0 68px;
  outline: none;
  border: none;
}




/*------------------------------------------------------------------
[ Focus ]*/
.focus-input100 {
  display: block;
  position: absolute;
  border-radius: 25px;
  bottom: 0;
  left: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
  box-shadow: 0px 0px 0px 0px;
  color: rgba(87,184,70, 0.8);
}

.input100:focus + .focus-input100 {
  -webkit-animation: anim-shadow 0.5s ease-in-out forwards;
  animation: anim-shadow 0.5s ease-in-out forwards;
}

@-webkit-keyframes anim-shadow {
  to {
    box-shadow: 0px 0px 70px 25px;
    opacity: 0;
  }
}

@keyframes anim-shadow {
  to {
    box-shadow: 0px 0px 70px 25px;
    opacity: 0;
  }
}

.symbol-input100 {
  font-size: 15px;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  align-items: center;
  position: absolute;
  border-radius: 25px;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding-left: 35px;
  pointer-events: none;
  color: #666666;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;
}

.input100:focus + .focus-input100 + .symbol-input100 {
  color: #57b846;
  padding-left: 28px;
}

/*------------------------------------------------------------------
[ Button ]*/
.container-login100-form-btn {
  width: 100%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding-top: 20px;
}

.login100-form-btn {
/*   font-family: Montserrat-Bold; */
  font-size: 12px;
  line-height: 1.5;
  color: #fff;
  text-transform: uppercase;
  width: 100%;
  height: 40px;
  border-radius: 25px;
 /*  background: #9053c7; */
  background: #26c281;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 25px;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;
  outline: none;
  border: none;
  font-weight: bold;
}

.login100-form-btn:hover {
  background: #62d0a2;
}


@media (min-width: 1300px) {
  .wrap-login100 {
  	width: 70%;
  }

  .login100-pic {
    width: 50%;
}
  .login100-form {
    width: 50%;
  }
}

@media (max-width: 1300px) {
  .wrap-login100 {
  	width: 80%;
  }

  .login100-pic {
    width: 50%;
}
  .login100-form {
    width: 50%;
  }
}

@media (max-width: 992px) {
  .wrap-login100 {
  	width: 100%;
  }

  .login100-pic {
    width: 50%;
}
  .login100-form {
    width: 50%;
  }
}

@media (max-width: 768px) {
  .wrap-login100 {
    width: 60%;
  }

  .login100-pic img {
    display: none;
  }

  .login100-form {
    width: 100%;
  }
}



/*------------------------------------------------------------------
[ Alert validate ]*/

.validate-input {
  position: relative;
}

.alert-validate::before {
  content: attr(data-validate);
  position: absolute;
  max-width: 70%;
  background-color: white;
  border: 1px solid #c80000;
  border-radius: 13px;
  padding: 4px 25px 4px 10px;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -moz-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  -o-transform: translateY(-50%);
  transform: translateY(-50%);
  right: 8px;
  pointer-events: none;

  font-family: Poppins-Medium;
  color: #c80000;
  font-size: 13px;
  line-height: 1.4;
  text-align: left;

  visibility: hidden;
  opacity: 0;

  -webkit-transition: opacity 0.4s;
  -o-transition: opacity 0.4s;
  -moz-transition: opacity 0.4s;
  transition: opacity 0.4s;
}

.alert-validate::after {
  content: "\f06a";
  font-family: FontAwesome;
  display: block;
  position: absolute;
  color: #c80000;
  font-size: 15px;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -moz-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  -o-transform: translateY(-50%);
  transform: translateY(-50%);
  right: 13px;
}

.alert-validate:hover:before {
  visibility: visible;
  opacity: 1;
}

@media (max-width: 992px) {
  .alert-validate::before {
    visibility: visible;
    opacity: 1;
  }
}

.img-logo{
	width: 300px
}

input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {
    background-color: rgb(250, 255, 189) !important;
    background-image: none !important;
    color: rgb(0, 0, 0) !important;
}
</style>


</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img alt="IMG"  src="<%=contextPath%>/assets/global/img/img-001.png" />
				</div>
				<form:form class="login-form" id="frmLogin" action="userlogin" method="post">
					<span class="login100-form-title">
						<img  class="img-logo" src="<%=contextPath%>/assets/global/img/logoBot5.png" />
					</span>
					<div class="wrap-input100 validate-input">
					<input class="input100" type="text" autocomplete="off" placeholder="Username" name="UserName" /> 
						
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" autocomplete="off" placeholder="Password" name="UserPass" />
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
					<button type="submit" class="login100-form-btn">Login</button>
                    <input type="hidden" name="cmd" value="getLogin" />
                    
                    
                    <div class="text-center" style="margin-top: 10px">				
							Please select country	
					</div>
                    
                     <!-- BEGIN COUNTRY -->
		           	<div class="dropdown dropdown-user dropup" style="width: 100% ;margin-top: 5px">
		            <a class="dropdown-toggle bg" data-toggle="dropdown" data-close-others="true">
		              <c:choose>
		         
		                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'th'}">
		                        <c:set var = "color" value = "#0080ff"/>
		                     </c:when>
		                     
		                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'id'}">
		                        <c:set var = "color" value = "#ff8c1a"/>
		                     </c:when>
		                     
		                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'my'}">
		                        <c:set var = "color" value = "#4f9a94"/>
		                     </c:when>
		                     
		                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'ph'}">
		                        <c:set var = "color" value = "#ff5c8d"/>
		                     </c:when>
		                     
		                     <c:when test = "${BaseConfig.BOT_SERVER_CURRENT_COUNTRY == 'sg'}">
		                        <c:set var = "color" value = "#002071"/>
		                     </c:when>
		                     
		                     <c:otherwise>
		                        <c:set var = "color" value = "#8e0000"/>
		                     </c:otherwise>
		                  </c:choose>
		              <button type="button" class="btn btn-default btn-circle btn-spe" style="width: 100%; background: <c:out value = "${color}"/>;">
		                <strong style="color: white;margin-left: -5px;">${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}</strong>
		              </button>
		            </a>
		          
		            <ul class="dropdown-menu dropdown-menu-default dropdown-spe" style="width: 100%">
		              <c:forEach items="${BaseConfig.BOT_SERVER_LIST_COUNTRY}" var="country">
		                <li>
		                <c:choose>
		         
		                     <c:when test = "${country == 'th'}">
		                        <c:set var = "country_path" value = ""/>
		                     </c:when>
		                     
		                     <c:when test = "${country == 'id'}">
		                        <c:set var = "country_path" value = "id"/>
		                     </c:when>
		                     
		                     <c:when test = "${country == 'my'}">
		                        <c:set var = "country_path" value = "my"/>
		                     </c:when>
		                     
		                     <c:when test = "${country == 'ph'}">
		                        <c:set var = "country_path" value = "ph"/>
		                     </c:when>
		                     
		                     <c:when test = "${country == 'sg'}">
		                        <c:set var = "country_path" value = "sg"/>
		                     </c:when>
		                     
		                     <c:otherwise>
		                        <c:set var = "country_path" value = "vn"/>
		                     </c:otherwise>
		                  </c:choose>
		                  <a target="_blank" href="http://27.254.65.119:8080/botbackend${country_path}/admin/dashboard">
		                    ${country}
		                  </a>
		                </li>
		              </c:forEach>
		            </ul>
		          </div> 
		          <!-- END COUNTRY -->
					
					</div>

				</form:form>
			</div>
		</div>
	</div>

<!--=======================================Java Script========================================================-->
<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
<script type="text/javascript">
$(document).ready(function() {
	$('.login').keypress(function (e) {
		  if (e.which == 13) {
		    $('form#frmLogin').submit();
		    return false;    
		  }
	});
	
	$('.dropdown').click(function() {
	      $('.dropdown-menu').slideToggle();
	  });
});
$('.js-tilt').tilt({
	scale: 1.1
})
</script>

</body>
</html>