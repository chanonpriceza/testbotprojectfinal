<%@ page import="botbackend.bean.UserBean, botbackend.utils.UserUtil, botbackend.utils.Util, botbackend.utils.ViewUtil, botbackend.system.BaseConfig, utils.DateTimeUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="priceza" prefix="pz"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />
		<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />
		
		<style>
			.div-head-topic {
				position: relative;
				margin-left: 25px
			}
			
			.filter-full-secondary {
				background: #f6f7f8;
				position: relative;
			}
			
			.row-no-padding-margin {
				padding: 0px;
				margin: 0px;
			}
			
			.scroll_server {
				overflow: scroll;
				height: 360px;
				margin-bottom: 25px;
				overflow-x: hidden;
				overflow-y: auto;
			}
			
			.scroll_name {
				overflow: scroll;
				height: 360px;
				margin-bottom: 25px;
				overflow-x: hidden;
				overflow-y: auto;
			}
			
			.modal-body {
				max-height: calc(100vh - 210px);
				overflow-y: auto;
			}
			
			.heading-single {
				border-bottom: 1px solid #e9e9e9;
				padding-bottom: 5px;
				margin-left: 25px
			}
			
			.ul_set_two_columns {
				columns: 2;
				-webkit-columns: 2;
				-moz-columns: 2;
			}
		</style>
	
	</head>
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	
		<jsp:include page="/WEB-INF/jsp/template/header.jsp" />
	
		<div class="clearfix"></div>
		<div class="page-container">
			<div class="wrapper">
				<jsp:include page="/WEB-INF/jsp/template/menuside.jsp" />
				<div class="page-content-wrapper">
					<div class="page-content">
						<div class="page-bar" style="margin-bottom: 2%;">
							<div class="col-md-6">
								<h3 class="page-title">
									<i class="fa fa-list-alt" style="padding-right: 10px;"></i> Summary Botquality
								</h3>
							</div>
							<ul class="page-breadcrumb">
								<li>
									<a href="dashboard">Dashboard</a> > <a href="#">SummaryBotquality</a> 
									<i class="fa fa-list-alt"></i>
								</li>
							</ul>
						</div>
	
						<c:if test="${not empty warning}">
							<div class="alert alert-danger alert-dismissable fade in">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Error!</strong> <c:out value="${warning }"></c:out>
							</div>
						</c:if>
	
						<!-- SEARCH & TABLE -->
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light bordered">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-10" id="topBar">
												<h4 class="txtResult">
													<span>All result 
														<c:out value="${countDataByFilter}"></c:out> record
													</span>
												</h4>
											</div>
										</div>
									</div>

									<!-- ######################################################## -->
									<!-- ################ HEADER SEARCH AREA #################### -->
									<!-- ######################################################## -->
									
									<div class="row">
										<div class="col-md-12" style="margin: 25px 20px;">
											<div class="row ">
												<div class="col-md-6">
													<form id="searchReport"action="${pageContext.servletContext.contextPath}/admin/searchsummarybotquality" method="post">
														<div class="row">
															<div class="input-group">
																<input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:if test="${not empty searchParam}">${searchParam}</c:if>'>
																<input name="inputProductCountRange" id="hiddenAllCount" type="hidden" value="${inputProductCountRange}"> 
																<input name="inputTimeRange" id="hiddenReportTime" type="hidden" value="${inputTimeRange}"> 
																<input name="activeType" id="hiddenActiveType" type="hidden" value="${activeType}"> 
																<input name="sortBy" id="hiddenSortBy" type="hidden"	value=""> 
																<span class="input-group-btn">
																	<a>
																		<button type="submit" id="searchBtn" class="btn btn-primary"><i class="icon-magnifier"></i></button>
																	</a>
																</span>
															</div>
														</div>
													</form>
												</div>
	
												<div class="row">
													<div class="col-md-2">
														<button id="btnFilterSearch" class="btn btn-primary" data-toggle="collapse" data-target="#refine-result"><i class="fa fa-fw fa-sort"></i> ShowSummary</button>
													</div>
													<c:choose>
														<c:when test="${not empty sortBy && sortBy == 1}">
															<c:set var="merchantidasc" value="${selected='selected'}" />
														</c:when>
														<c:when test="${not empty sortBy && sortBy == 2}">
															<c:set var="merchantiddesc" value="${selected='selected'}" />
														</c:when>
														<c:when test="${not empty sortBy && sortBy == 3}">
															<c:set var="allcountasc" value="${selected='selected'}" />
														</c:when>
														<c:when test="${not empty sortBy && sortBy == 4}">
															<c:set var="allcountdesc" value="${selected='selected'}" />
														</c:when>
													</c:choose>
													<select name="filterSortBy" id="filterSortBy" class="form-control" style="text-align-last: center; width: 15%;">
														<option value="1" ${merchantidasc}>MerchantId ASC</option>
														<option value="2" ${merchantiddesc}>MerchantId DESC</option>
														<option value="3" ${allcountasc}>AllCount ASC</option>
														<option value="4" ${allcountdesc}>AllCOunt DESC</option>
													</select>
												</div>
											</div>
										</div>
									</div>

									<!-- ######################################################## -->
									<!-- ################ SUMMARY TABLE AREA #################### -->
									<!-- ######################################################## -->
									
									<c:if test="${showSummary}">
										<c:set var="showsummary" value="in" />
									</c:if>
	
									<div id="refine-result" class="collapse ${showsummary}">
										<h4 class="modal-title" id="addFormLabel"></h4>
										<table id="newSummary" class="table table-striped table-bordered" style="width: 50%;">
											<thead>
												<tr class="heading">
													<th style="width: 15%; text-align: center">Name</th>
													<th style="width: 15%; text-align: center">allCount</th>
												</tr>
											</thead>
											<c:if test="${not empty reportSummaryList}">
												<tbody>
													<c:forEach items="${reportSummaryList}" var="summaryItem">
														<tr>
															<td style="text-align: left"><c:out value="${summaryItem.name }" /></td>
															<td style="text-align: right"><fmt:formatNumber value="${summaryItem.allCount }" /></td>
														</tr>
													</c:forEach>
												</tbody>
											</c:if>
										</table>
									</div>
								</div>
	
								<!--END FILTER MERCHANT-->
	
								<div class="portlet-body">
									<div style="margin-top: 10px;">
										<table id="newSummaryTableTwo" class="table table-striped table-bordered">
											<thead>
												<tr class="heading">
													<th style="width: auto; text-align: center;">MerchantId</th>
													<th style="width: auto; text-align: center;">Name</th>
													<th style="width: 20%; text-align: center;">
														<select name="reportActive" id="reportActive" class="form-control" style="width: auto; text-align-last: center; width: 100%;">
															<option value="">Status(All)</option>
															<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
																<c:if test="${(not empty activeType)}">
																	<fmt:parseNumber var="activeTypeNum" type="number"	value="${activeType}" />
																</c:if>
																<option value="${loop.index}" ${activeTypeNum  == loop.index ? 'selected=\"selected\"' : ''}>${ViewUtil.generateShowActive(loop.index)}</option>
															</c:forEach>
														</select>
													</th>
													<th style="width: 20%; text-align: center;">
														<c:choose>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '0'}">
																<c:set var="count0" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '1'}">
																<c:set var="count1" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '2'}">
																<c:set var="count2" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '3'}">
																<c:set var="count3" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '4'}">
																<c:set var="count4" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputProductCountRange && inputProductCountRange == '5'}">
																<c:set var="count5" value="${selected='selected'}" />
															</c:when>
														</c:choose> 
														<select name="reportAllCount" id="reportAllCount" class="form-control" style="text-align-last: center; width: 100%;">
															<option value="">allCount</option>
															<option value="0" ${count0}>0</option>
															<option value="1" ${count1}>1-10000</option>
															<option value="2" ${count2}>10001-50000</option>
															<option value="3" ${count3}>50001-100000</option>
															<option value="4" ${count4}>100001-300000</option>
															<option value="5" ${count5}>300000++</option>
														</select>
													</th>
													<th style="width: 20%; text-align: center;">
														<c:choose>
															<c:when test="${not empty inputTimeRange && inputTimeRange == 1}">
																<c:set var="time1" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputTimeRange && inputTimeRange == 2}">
																<c:set var="time2" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputTimeRange && inputTimeRange == 3}">
																<c:set var="time3" value="${selected='selected'}" />
															</c:when>
															<c:when test="${not empty inputTimeRange && inputTimeRange == 4}">
																<c:set var="time4" value="${selected='selected'}" />
															</c:when>
														</c:choose> 
														<select name="reportTime" id="reportTime" class="form-control" style="text-align-last: center; width: 100%;">
															<option value="">ทั้งหมด</option>
															<option value="1" ${time1}>น้อยกว่า 1 วัน</option>
															<option value="2" ${time2}>ระหว่าง 1-3 วัน</option>
															<option value="3" ${time3}>มากกว่า 3 วัน</option>
															<option value="4" ${time4}>อื่นๆ</option>
														</select>
													</th>
												</tr>
											</thead>
											<c:if test="${not empty getReportMerchantByFilterList}">
												<tbody>
													<c:forEach items="${getReportMerchantByFilterList}" var="summary">
														<tr>
															<td style="text-align: center">
																<c:out value="${summary.merchantId }" />
															</td>
															<td style="text-align: center">
															 	<c:out value="${summary.name }" />
															 </td>
															<td style="text-align: center">
																<c:out value="${summary.status}" />
															</td>
															<td style="text-align: center">
																<c:out value="${summary.allCount }" />
															</td>
															<td style="text-align: center">
																<c:out value="${summary.time }" />
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</c:if>
										</table>
	
									</div>
								</div>
							</div>
						</div>
					</div>
	
					<!--  paging -->
					<div class="row" style="float: right; padding-bottom: 20px;">
						<ul class="pagination">
							<c:choose>
								<c:when test="${pagingBean.getCurrentPage() == 1}">
									<li class="prev disabled"><a href="#">← Previous</a></li>
								</c:when>
								<c:otherwise>
									<li class="prev">
										<a href="${pageContext.servletContext.contextPath}/admin/searchsummarybotquality?page=${pagingBean.getCurrentPage()-1}&searchPram=${searchParam}&inputProductCountRange=${inputProductCountRange}&inputTimeRange=${inputTimeRange}&activeType=${activeType}&sortBy=${sortBy}">←Previous</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:forEach items="${pagingBean.getPagingList()}" var="pageNo">
								<c:choose>
									<c:when test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
										<li>
											<a href="#" style="background-color: #ccc;">${pageNo}</a>
										</li>
									</c:when>
									<c:when test="${pageNo.equals('...')}">
										<li>
											<a href="#">...</a>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<a href="${pageContext.servletContext.contextPath}/admin/searchsummarybotquality?page=${pageNo}&searchPram=${searchParam}&inputProductCountRange=${inputProductCountRange}&inputTimeRange=${inputTimeRange}&activeType=${activeType}&sortBy=${sortBy}">${pageNo}</a>
										</li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:choose>
								<c:when test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
									<li class="next disabled">
										<a href="#">Next → </a>
									</li>
								</c:when>
								<c:otherwise>
									<li class="next">
										<a href="${pageContext.servletContext.contextPath}/admin/searchsummarybotquality?page=${pagingBean.getCurrentPage()+1}&searchPram=${searchParam}&inputProductCountRange=${inputProductCountRange}&inputTimeRange=${inputTimeRange}&activeType=${activeType}&sortBy=${sortBy}">Next→ </a>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
					<!--  End paging -->
					<!-- END CONTENT -->
				</div>
			</div>
		</div>
		<script type="text/javascript"src="${pageContext.servletContext.contextPath}/assets/pages/js/summarybotquality-script.js"></script>
	</body>
</html>