<%@ page import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig,utils.DateTimeUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<% String contextPath = request.getContextPath();  

UserBean userBean;
HttpSession ses = request.getSession(false);
userBean = (UserBean) ses.getAttribute("userData");
String userId = Integer.toString(userBean.getUserId());
boolean isDev = userBean.getUserPermission().contains("content") ;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>BotBackEnd</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>

</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                   	 <!-- BEGIN PAGE BAR -->
		                   	 <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-desktop" style="padding-right: 10px;"></i> Merchant Runtime Report </h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#"> Merchant Runtime Report</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<!-- START CONTENT -->
		                 	
		                 	<!-- START New Report TABLE -->
		                    <div class="row" id="newMerchantContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">       
		                            	 <div class="row">
		                            	 	<div class="col-md-12">
		                            				<div class="col-md-10" style="margin-top:12px;">
		                            					<h4 id="group-productName"><span>All Report result <c:if test="${not empty totalRepotList}" >
		                            					<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${totalRepotList}" />
		                            					</c:if> record</span></h4>
		                            				</div>
			                            	</div>
		                            	 </div>
		                            	 <div class="row">
		                            	    <div class="col-md-12" style="margin: 25px 0;">
						                    	 <div class="col-md-12">
						                    	   <form id="searchReport" action="<%=contextPath %>/admin/merchantruntimereport" method="post">
						                    	   <div class="row" style="margin-bottom: 10px" >
						                    	  	  <div class="col-md-3" style="display: inline-flex;">	
							                    	   		<c:choose>
							                            		<c:when test="${not empty reportType && reportType == 'WCE'}">
							                            	 		<c:set var ="typeWCE" value="${selected='selected'}"/>
							                            	 	</c:when>
							                            	 	<c:when test="${not empty reportType && reportType == 'DUE'}">
							                            	 		<c:set var ="typeDUE" value="${selected='selected'}"/>
							                            	 	</c:when>
							                  					<c:otherwise>
							                  						<c:set var ="typeALL" value="${selected='selected'}"/>
							                  					</c:otherwise>   
				                            	 			</c:choose>
															<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Type </label></span>&nbsp;&nbsp;
															<select name="reportType" id="reportType" class="form-control" style="padding: 2px 10px;">
																<option value="DUE" ${typeDUE}>DUE (อัพเดทสินค้าที่มีอยู่)</option>
																<option value="WCE" ${typeWCE}>WCE (ดึงสินค้าใหม่)</option>
																<option value="ALL" ${typeALL}>ALL (ดึงสินค้าใหม่ เละ อัพเดทสินค้าที่มีอยู่)</option>
															</select>							
														</div>
														<div class="col-md-3" style="display: inline-flex;">		
															<c:choose>
							                            	 	<c:when test="${not empty reportStatus && reportStatus == 'RUNNING'}"><c:set var ="statusRunning" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportStatus && reportStatus == 'PASSED'}"><c:set var ="statusPass" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportStatus && reportStatus == 'KILLED'}"><c:set var ="statusKilled" value="${selected='selected'}"/></c:when>
							                            		<c:when test="${not empty reportStatus && reportStatus == 'TIMEOUT'}"><c:set var ="statusTimeout" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportStatus && reportStatus == 'EXCEED'}"><c:set var ="statusExceed" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportStatus && reportStatus == 'FAILED'}"><c:set var ="statusFailed" value="${selected='selected'}"/></c:when>
							                            	 	<c:otherwise><c:set var ="statusAll" value="${selected='selected'}"/></c:otherwise>   
				                            	 			</c:choose>
															<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Status</label></span>&nbsp;&nbsp;
															<select name="reportStatus" id="reportStatus"  class="form-control" style="padding: 2px 10px;">
																<option value="" ${statusAll}>ALL</option>
																<option value="RUNNING" ${statusPass}>RUNNING</option>
																<option value="PASSED" ${statusPass}>PASSED</option>
																<option value="KILLED" ${statusKilled}>KILLED</option>
																<option value="TIMEOUT" ${statusTimeout}>TIMEOUT</option>
																<option value="EXCEED" ${statusExceed}>EXCEED</option>
																<option value="FAILED" ${statusFailed}>FAILED</option>
															</select>							
														</div>
														<div class="col-md-3" style="display: inline-flex;">	
							                    	   		<c:choose>
							                            	 	<c:when test="${not empty reportType && reportType == 'WCE'}"><c:set var ="typeWCE" value="${selected='selected'}"/></c:when>
							                  					<c:otherwise><c:set var ="typeDUE" value="${selected='selected'}"/></c:otherwise>   
				                            	 			</c:choose>
				                            	 			<c:choose>
				                            	 			<c:when test="${not empty reportTime && reportTime == 1}"><c:set var ="statusLessDay" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportTime && reportTime == 2}"><c:set var ="statusBetweenDay" value="${selected='selected'}"/></c:when>
							                            	 	<c:when test="${not empty reportTime && reportTime == 3}"><c:set var ="statusMoreDay" value="${selected='selected'}"/></c:when>
															</c:choose>
															<span style="font-size: 16px; reportTime-weight: bold;"><label class="control-label">Time </label></span>&nbsp;&nbsp;
															<select name="reportTime" id="reportTime" class="form-control" style="padding: 2px 10px;">
																<option value="" ${statusLessDay}>ทั้งหมด </option>
																<option value="1" ${statusLessDay}>น้อยกว่า 1 วัน </option>
																<option value="2" ${statusBetweenDay}> ระหว่าง 1-3 วัน</option>
																<option value="3" ${statusMoreDay}> มากกว่า 3 วัน </option>
															
															</select>							
														</div>
														
													</div>
													
													<div class="row col-md-10" >
									                    <div class="input-group">
										                    <div class="input-group-btn">
					                                		  <c:choose>
											                        <c:when test="${not empty searchType && searchType == 1}"><c:set var ="searchTypeShow" value="${'Merchant Name'}"/></c:when>
											                        <c:otherwise><c:set var ="searchTypeShow" value="${'Merchant'}"/></c:otherwise>
				                            	 			  </c:choose>
						                                      <button type="button" id="filter" class="btn dropdown-toggle" data-toggle="dropdown" disabled="true" style="color: black;">${searchTypeShow}</button>
						                                      	  <ul class="dropdown-menu">
						                                             <!-- <li><a onclick="setTypes('0', 'Merchant Id');" class="typeFilter" >Merchant Id</a></li> -->
						                                             <!-- <li><a onclick="setTypes('1', 'Merchant Name');" class="typeFilter" >Merchant Name</a></li> -->
						                                          </ul>
						                                       <input type="hidden" id="searchType" name="searchType" value="${searchType}"/>
						                                  	</div>
									                        <input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:choose><c:when test="${not empty searchParam}">${searchParam}</c:when><c:otherwise></c:otherwise></c:choose>'>
									                        <input type="hidden" id="cmd" name="cmd" value="searchMerchantReport">
									                        <span class="input-group-btn">
									                            <a>
									                                <button type="submit" class="btn btn-primary"><i class="icon-magnifier"></i></button>
									                            </a>
									                        </span>
									                    </div>
								                    </div>
								                    </form>     
								                    
								                    
							                    </div>                 
		                                    </div>
                   						 </div>
		                          
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="reportContent" class="table table-striped">
		                                            <thead>
		                                                <tr class="heading">
		                                               		<th style="text-align:center;"> Merchant Id </th>
		                                                	<th style="text-align:center;"> Merchant Name </th>
		                                                	<th style="text-align:center;"> Type </th>

															<c:choose>
					    									 	<c:when test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('report.merchant.showall')}">		
				                                                	<th style="text-align:center;"> Product Count </th>
				                                                	<th style="text-align:center;"> Success </th>
				                                                	<th style="text-align:center;"> Error </th>
				                                                	<th style="text-align:center;"> Add </th>
				                                                	<th style="text-align:center;"> Update </th>
				                                                	<th style="text-align:center;"> UpdatePic </th>
				                                                	<th style="text-align:center;"> Expire </th>
				                                                	<th style="text-align:center;"> Duplicate </th>
				                                                	<th style="text-align:center;"> Delete </th>
				                                                	<th style="text-align:center;"> DefinetDate </th>
			                                                	</c:when>
			                                                	<c:otherwise>
			                                                		<c:if test="${not empty reportType && (reportType == 'WCE')}"> 
					                                                	<th style="text-align:center;"> Product Count</th>  
					                                                	<th style="text-align:center;">Add</th>             
					                                                	<th style="text-align:center;">Expire </th>         
					                                                	<th style="text-align:center;">Duplicate</th>       
					                                                </c:if>                                                 
					                                                <c:if test="${not empty reportType && (reportType == 'DUE')}"> 
					                                                	<th style="text-align:center;"> Product Count</th>     
					                                                	<th style="text-align:center;"> Success</th>           
					                                                	<th style="text-align:center;"> Failed</th>            
					                                                	<th style="text-align:center;"> Expire </th>           
					                                                    <th style="text-align:center;"> Delete </th>           
					                                                </c:if>                                                    
					                                            	<c:if test="${not empty reportType && (reportType == 'ALL')}">
					                                                	<th style="text-align:center;"> Product Count</th>
					                                                </c:if>
			                                                	</c:otherwise>
		                                                	</c:choose>
		                                                	
		                                                    <th style="text-align:center;"> StartDate </th>
		                                                	<th style="text-align:center;"> End Date </th>
		                                                	<th style="text-align:center;"> Run Time(hr/mm) </th>
		                                                	<th style="text-align:center;"> Status </th>
		                                               	</tr>        
		                                            </thead>
		                                            <tbody>
			                                            <c:if test="${not empty reportList}" >
			                                               	<c:forEach items="${reportList}" var="report"> 
			                                               		<c:choose>
						    										<c:when test="${report.type == 'DUE' || report.type == 'DUE_MANUAL'}">
						    											<c:set var ="productCount" value="${report.successCount+report.errorCount}"/>
						    										</c:when>
						    								 		<c:when test="${report.type == 'WCE' || report.type == 'WCE_MANUAL'}">
									    								<c:set var ="productCount" value="${report.countOld+report.add}"/>
						    								 		</c:when>
						    								 	</c:choose>  
				    											<tr class='rowPattern'>
				    												<td style='text-align:center;'><c:out value="${report.merchantId}"/></td>
			    													<td style='text-align:center;'><c:out value="${report.name}"/></td>
			    												    <td style='text-align:center;'><c:out value="${report.type}"/></td>	
			    												    
		    												    	<c:choose>
					    											 	<c:when test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('report.merchant.showall')}">		
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${productCount}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.successCount}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.errorCount}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.add}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.update}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.updatePic}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.expire}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.duplicate}" /></td> 				
						    											    <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.delete}" /></td> 		
						    											    <td style='text-align:center;'><c:choose><c:when test="${not empty report.defineDate}">${DateTimeUtil.generateStringDisplayDateTime(report.defineDate)}</c:when><c:otherwise> - </c:otherwise></c:choose></td> 		
				    											   		</c:when>
				    											   		<c:otherwise>
				    											   			<c:if test="${not empty reportType && (reportType == 'WCE')}"> 
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${productCount}" /> </td>
						    											        <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.add}" /></td>
						    											        <td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.expire}" /></td> 
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.duplicate}" /></td>
			    												    		</c:if>
			    												    		<c:if test="${not empty reportType && (reportType == 'DUE')}"> 
			    												    			<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${productCount}"/></td>
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.successCount}"/></td>
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.errorCount}"/></td>
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.expire}" /></td> 
						    											     	<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${report.delete}" /></td> 	
			    												    		</c:if>
			    												    		<c:if test="${not empty reportType && (reportType == 'ALL')}"> 
			    												    			<td style='text-align:center;'><fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${productCount}"/></td>
			    												    		</c:if>
				    											   		</c:otherwise>
		    												    	</c:choose>
			    											   		
				    											    <td style='text-align:center;'><c:choose><c:when test="${not empty report.startDate}">${DateTimeUtil.generateStringDisplayDateTime(report.startDate)}</c:when><c:otherwise> - </c:otherwise></c:choose></td>	
				    											    <td style='text-align:center;'><c:choose><c:when test="${not empty report.endDate}">${DateTimeUtil.generateStringDisplayDateTime(report.endDate)}</c:when><c:otherwise> - </c:otherwise></c:choose></td>
				    											    	
				    											    <c:choose>
									                            	 	<c:when test="${not empty report.status && report.status == 'PASSED'}"><c:set var ="statusColor" value="${'#ABEBC6'}"/><c:set var ="status" value="PASSED"/></c:when>
									                            	 	<c:when test="${not empty report.status && report.status == 'FAILED'}"><c:set var ="statusColor" value="${'#EC7063'}"/><c:set var ="status" value="FAILED"/></c:when>
									                            	 	<c:when test="${not empty report.status && report.status == 'RUNNING'}"><c:set var ="statusColor" value="${'#AED6F1'}"/><c:set var ="status" value="RUNNING"/></c:when>
									                            	 	<c:when test="${not empty report.status && report.status == 'TIMEOUT'}"><c:set var ="statusColor" value="${'#ffc61a'}"/><c:set var ="status" value="TIMEOUT"/></c:when>
									                            	 	<c:when test="${not empty report.status && report.status == 'KILLED'}"><c:set var ="statusColor" value="${'#ff3300'}"/><c:set var ="status" value="KILLED"/></c:when>
									                            	 	<c:when test="${not empty report.status && report.status == 'EXCEED'}"><c:set var ="statusColor" value="${'#DDA0DD'}"/><c:set var ="status" value="EXCEED"/></c:when>
		                            	 							</c:choose>
		                            	 							<td style='text-align:center;'><c:choose><c:when test="${not empty report.startDate && not empty report.endDate}">${DateTimeUtil.calcurateHourDiff(report.startDate, report.endDate)}</c:when><c:otherwise> - </c:otherwise></c:choose></td>
				    											    <td style='text-align:center;border-radius: 10px;'  bgcolor="${statusColor}"><c:out value="${status}"/> </td> 
														  		</tr>
															</c:forEach>
														</c:if>
		                                            </tbody>    	
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                      </div>
							<!--  paging -->
					        <div class="row" style="float: right; padding-bottom: 20px;">
					          	<div class="col-md-12">
					                <c:if test="${not empty pagingBean}" >
					                <c:set var ="pagingBean" value="${pagingBean}"/>
									<div class="dataTables_paginate paging_bootstrap">
										<ul class="pagination">
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == 1}">
											       <li class="prev disabled"><a href="#">← Previous</a></li>
											    </c:when>    
											    <c:otherwise>
											     <li class="prev"><a href="<%=contextPath%>/admin/merchantruntimereport?page=${pagingBean.getCurrentPage()-1}&searchParam=${searchParam}&reportType=${reportType}&reportStatus=${reportStatus}&searchType=${searchType}&reportTime=${reportTime}">← Previous</a></li>			
											    </c:otherwise>
											</c:choose>
											<c:forEach items="${pagingBean.getPagingList()}" var="pageNo"> 
												<c:choose>
											    <c:when test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
											       <li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
											    </c:when>
											    <c:when test="${pageNo.equals('...')}">
											       <li><a href="#">...</a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li><a href="<%=contextPath%>/admin/merchantruntimereport?page=${pageNo}&searchParam=${searchParam}&reportType=${reportType}&reportStatus=${reportStatus}&searchType=${searchType}&reportTime=${reportTime}">${pageNo}</a></li>
											    </c:otherwise>
											</c:choose>
											</c:forEach>
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
											       <li class="next disabled"><a href="#">Next → </a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li class="next"><a href="<%=contextPath%>/admin/merchantruntimereport?page=${pagingBean.getCurrentPage()+1}&searchParam=${searchParam}&reportType=${reportType}&reportStatus=${reportStatus}&searchType=${searchType}&reportTime=${reportTime}">Next → </a></li>
											    </c:otherwise>
											</c:choose>
										</ul>
									</div>
								</c:if>
					       	</div>
					    </div>
					     <!--  End paging --> 
 
		                 <!-- END New Report TABLE -->    	
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
			
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/merchantreport-script.js"></script>

</body>
</html>