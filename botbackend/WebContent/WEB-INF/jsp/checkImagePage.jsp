<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% 
	String contextPath = request.getContextPath(); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Tool: ChecKImage</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	
	<style type="text/css">
	.modal-confirm {		
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}	
	.modal-confirm .icon-box {
		color: #fff;		
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm  .danger{
		background: #ef513a;
	}
	.modal-confirm  .success{
		background: #82ce34;
	}
	
	.modal-confirm  .warning{
		background: #FCCB46;
	}
	
	.modal-confirm .icon-box i {
		font-size: 56px;
		position: relative;
		top: 4px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
     .modal-confirm .btn-danger {
		background: #ef513a;
    }
     .modal-confirm .btn-success {
		background: #82ce34;
    }
	.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
		background: #da2c12;
		outline: none;
	}
	.modal-confirm .btn-success:hover, .modal-confirm .btn-success:focus {
		background: #6fb32b;
		outline: none;
	}

	.modal-confirm .modal-detail {
		font-size: 15px;
		margin-bottom: 20px;
	}
	.row {
	    margin-left: -15px;
	    margin-right: -15px;
	    margin-bottom: 20px;
	}
	.center{
		text-align:center;
		word-wrap: break-word;
	}

	.inputclear {
	    position: absolute;
	    right: 20px;
	    margin: 10px 5px 20px 20px;
	    font-size: 14px;
	    cursor: pointer;
	    color: #ccc;
	    display:none;
	}
	.form-control{
		padding: 6px 30px 6px 12px;
	}
	.modal .modal-content .modal-title {
	 	margin-bottom: 10px;
	}
.file-upload-input {
	  position: absolute;
	  margin: 0;
	  padding: 0;
	  width: 100%;
	  height: 100%;
	  outline: none;
	  opacity: 0;
	  cursor: pointer;
	}
	.file-upload-wrap {
		  border: 3px dashed #b3e0c8;
		  position: relative;
		  height: 80px;
	}
	
	.image-dropping,
	.file-upload-wrap:hover {
	  background-color: #b3e0c8;
	  border: 3px dashed #ffffff;
	}
	.image-title-wrap {
	  padding: 0 15px 15px 15px;
	  color: #222;
	}
	
	.drag-text {
	  text-align: center;
	}
	
	.drag-text h4 {
	  font-weight: 100;
	  text-transform: uppercase;
	  padding: 18px 0;
	}
	
	.file-upload-csv {
	  max-height: 200px;
	  max-width: 200px;
	  margin: auto;
	  padding: 20px;
	}
	
	.remove-image {
	  width: 200px;
	  margin: 0;
	  color: #fff;
	  background: #cd4535;
	  border: none;
	  padding: 10px;
	  border-radius: 4px;
	  border-bottom: 4px solid #b02818;
	  transition: all .2s ease;
	  outline: none;
	  text-transform: uppercase;
	  font-weight: 700;
	}
	
	.remove-image:hover {
	  background: #c13b2a;
	  color: #ffffff;
	  transition: all .2s ease;
	  cursor: pointer;
	}
	
	.remove-image:active {
	  border: 0;
	  transition: all .2s ease;
	}
	.file-upload-content {
  		display: none;
 	 	text-align: center;
	}
	table, th, td {
  		border: 1px solid black;
  		text-align:center;
  		padding: 5px;
	}	
	td.left{
		text-align:left;
	}
	#resultTable tr:hover {
     background-color: #e5e5e5;
}

.text-center{
	word-wrap: break-word;
}

</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
  		
				
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-file-image-o" ></i> Check Image</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Tool</a> > <a href="#">Image Check</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
							<form id="dataForm" role="form" data-toggle="validator" onsubmit="return false;">
								<div class="portlet light bordered">
									<div style="margin-left: 10px; margin-right: 10px;">
										<div class="row">
											<div class="form-group">
												<div class="col-xs-2">
	                                            	<label class="control-label" for="word"> Image URL </label>&nbsp;<span class="text-danger">*</span> :
	                                            </div>
	                                            <div class="col-xs-9">
	                                            	<input type="text" id="url" name="url"  class="form-control clearbutton">
	                                            	<span class="inputclear glyphicon glyphicon-remove-circle"></span>
	                                            </div>
	                                            <a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="ลิ้งรูปภาพ"></a>
                                        	</div>
                                      	</div>
                                      	<div class="row center">
                                      		<button type="button" onclick="testImage()" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order" >Submit</button>
                                      		<button type="button" onclick="clearUrl()" class="btn btn-danger" id="clear" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order" >Clear</button>
                                      		
                                      	</div>
								   </div>	
								</div>
							</form> 
						</div>				
					</div>
		            <!-- END CONTENT -->
			
			<!-- Modal HTML -->
			<div id="responseModal" class="modal fade" style="text-align:center">
				<div class="modal-dialog modal-confirm">
					<div class="modal-content">
					</div>
				</div>
			</div>     									
		</div>								
  	</div>
  	<script type="text/Javascript" src="<%=contextPath %>/assets/pages/js/check-image-page.js"></script>
		
</body>
</html>