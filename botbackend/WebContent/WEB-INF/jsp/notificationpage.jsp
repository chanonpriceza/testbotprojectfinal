<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="botbackend.bean.NotificationApiBean.NOTITYPE"%>
<%@ page import="botbackend.bean.UserBean, botbackend.utils.Util,botbackend.utils.ViewUtil, botbackend.system.BaseConfig,utils.DateTimeUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="priceza" prefix="pz"%>

<%
  UserBean userBean;
  HttpSession ses = request.getSession(false);
  String contextPath = request.getContextPath();
  userBean = (UserBean) ses.getAttribute("userData");
  String userId = Integer.toString(userBean.getUserId());
  Map<Integer,String> types =  NOTITYPE.getNotiTypeMap();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	
	<title>Bot Backend</title>
	
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>	
	.div-head-topic{
		position: relative; 
		margin-left: 25px
	}
	
	.filter-full-secondary {
	    background: #f6f7f8;
	    position: relative;
	}
	.row-no-padding-margin{
		padding: 0px;
		margin: 0px;
	}
	.scroll_server {
	    overflow: scroll;
	    height: 360px;
	    margin-bottom: 25px; 
	    overflow-x: hidden;
	    overflow-y: auto;
	}
	.scroll_name {
	    overflow: scroll;
	    height: 360px;
	    margin-bottom: 25px;
	    overflow-x: hidden;
	    overflow-y: auto;
	}
	.modal-body {
	    max-height: calc(100vh - 210px);
	    overflow-y: auto;
	}
	.heading-single{
		border-bottom: 1px solid #e9e9e9;
	    padding-bottom: 5px;
	    margin-left: 25px
	}
	
	.ul_set_two_columns {
	  columns: 2;
	  -webkit-columns: 2;
	  -moz-columns: 2;
	}

</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-bell" style="padding-right: 10px;"></i> Notification page </h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="notificationpage">Dashboard</a> > <a href="#">Notification page</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<span class="tab" style="float:left;margin-top:3px">
		                 	<button id="AllnotiType" name="notiTypeSelector"  style="text-decoration:none;font-size:10px" title="" class="btn notiTablinks tabNotitype active" onclick="setNotitype(0)" >ALL</button>
		                 	<% for(Entry<Integer,String> x:types.entrySet()){
		                 		if(x.getKey()==2||x.getKey()==4) {%>
		                 		<button id="<%=x.getKey()%>notiType" name="notiTypeSelector"  style="text-decoration:none;font-size:10px" title="" class="btn notiTablinks tabNotitype" onclick="setNotitype('<%= x.getKey() %>')" ><%= x.getValue().replace("_"," ") %></button>
		                 	<%}}%>
		                 	</span>
		                    <span class="tab" style="float:right">
		                        <c:forTokens items = "TH,ID,SG,MY,VN,PH" delims = "," var = "name">
		                         <c:choose>
		                            	<c:when test="${fn:toLowerCase(BaseConfig.BOT_SERVER_CURRENT_COUNTRY) != fn:toLowerCase(name)}">
        									<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks" onclick="setCountry('${name}')" >${ name }</button>
        								</c:when>
        								<c:otherwise>
        									<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks active" onclick="setCountry('${name}')">${ name }</button>	
        								</c:otherwise>
        							</c:choose>
     							</c:forTokens> 
							</span>
							</span>
    						<input id="notiType" type="hidden" name="notiType" value="${notiType}">
    						<input id="country" type="hidden" name="country" value="${country}">
    						<input id="page" type="hidden" name="page" value="${pagingBean.getCurrentPage()}">
    						<input id="context" type="hidden" name="context" value="<%=contextPath %>">
		                 	
		                 	<!-- START NOTIFICATION TABLE -->
		                    <div class="row" id="notificationContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="NotificationContent" class="table">
		                                            <thead>
		                                                <tr class="heading">
		                                               		<th width="5%"  style="text-align:center;">User</th>
		                                               		<th width="20%"  style="text-align:left;"> Message </th>
		                                               		<th width="20%"  style="text-align:left;"> Type </th>
		                                                	<th width="10%"  style="text-align:center;"> Date </th>
		                                               	</tr>        
		                                            </thead>
		                                            <c:choose>
		                                            <c:when test="${not empty notificationList}">
		                                            <tbody id="tableBody">
				                                        <c:forEach items="${notificationList}" var="notificationData"> 
				                                        
				                                        <c:set var="userComment" value="${notificationData.source}"></c:set>
				                                        <c:set var="message" value="${notificationData.message}"></c:set>
				                                        <c:set var="type" value="${notificationData.type}"></c:set>
				                                       <c:set var="creatDate" value="${DateTimeUtil.generateStringDateTime(notificationData.createDate)}"></c:set>
				    										<tr class='rowPattern'>		
			    											<td width='5%'  style='text-align:center;'>
			    												<div class="row">
			    											    	<img src="<%=contextPath%>/assets/global/img/manwhite.png" class="avatar" alt="..." style="width: 30px; height: 30px">		    											    
			    											    </div>
			    											    <c:if test="${not empty userComment}">
			    											    	<div class="row" style="font-size: x-small;">
				    											    	<c:choose>
	 				    											   		<c:when test="${not empty userComment}">${userComment}</c:when>
																			<c:otherwise> - </c:otherwise>
																		</c:choose>				    											    
			    											    	</div>
			    											    </c:if>
			    											</td>		
			    											<% String type = types.get((int)pageContext.getAttribute("type"));%>
			    											<td width='20%'  style='text-align: left: ;'><a target="_blank" href="${serviceUrl[fn:toLowerCase(notificationData.country)]}${notificationData.link}"><c:out value="${message}"/></a></td>														
			    											<td width='20%'  style='text-align: left: ;'><%=type.replace("_"," ")%></td>			
			    											<td width='10%'  style='text-align:center;'><c:out value="${creatDate}"/></td>					 
														  	</tr>
														</c:forEach>
		                                            </tbody> 
		                                            </c:when>
			                                        <c:otherwise>
			                                        	<h4>No Notification Data</h4>
			                                        </c:otherwise>
		                                       		 </c:choose>	   	
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
 
							<!--  paging -->
					        <div class="row" style="float: right;">
					          	<div class="col-md-12" id="pagingDiv">
					                <c:if test="${not empty pagingBean}" >
					                <c:set var ="pagingBean" value="${pagingBean}"/>
									<div class="dataTables_paginate paging_bootstrap">
										<ul class="pagination">
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == 1}">
											       <li class="prev disabled"><a href="#">← Previous</a></li>
											    </c:when>    
											    <c:otherwise>
											     <li class="prev"><a href="<%=contextPath%>/admin/notificationpagesearch?page=${pagingBean.getCurrentPage()-1}">← Previous</a></li>			
											    </c:otherwise>
											</c:choose>
											<c:forEach items="${pagingBean.getPagingList()}" var="pageNo"> 
												<c:choose>
											    <c:when test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
											       <li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
											    </c:when>
											    <c:when test="${pageNo.equals('...')}">
											       <li><a href="#">...</a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li><a href="#" data-element-id="${pageNo}" name="pagination">${pageNo}</a></li>
											    </c:otherwise>
											</c:choose>
											
											</c:forEach>
											<c:choose>
											    <c:when test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
											       <li class="next disabled"><a href="#">Next → </a></li>
											    </c:when>    
											    <c:otherwise>
											     	<li class="next"><a href="#" data-element-id="${pagingBean.getCurrentPage()+1}" name="pagination">${pageNo}>Next → </a></li>
											    </c:otherwise>
											</c:choose>
										</ul>
									</div>
								</c:if>
					       	</div>
					    </div>
					     <!--  End paging --> 
		                 <!-- END NOTIFICATION TABLE -->    	
						<script type="text/javascript" src="<%=contextPath%>/assets/pages/js/notification-script.js"></script>						
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>		

</body>
</html>