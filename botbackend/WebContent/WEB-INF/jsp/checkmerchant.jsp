<%@ page import="botbackend.bean.MerchantBean, botbackend.utils.*, botbackend.system.BaseConfig, botbackend.utils.ViewUtil,  utils.DateTimeUtil, java.util.HashMap, botbackend.bean.MerchantConfigBean" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<% 
	String contextPath = request.getContextPath(); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend : Check Merchant</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	<style type="text/css">
		#merchantName > span{
			border: 1px solid #cccccc;
			margin-left: 20px;
			padding: 9px 14px 8px;
			font-size: 14px;
			font-weight: 600;
			border-radius: 2px;
		}
		#merchantName > span > span:not(:first-child){
			margin-left: 20px;
		}
		#btnSearch{
			color: darksategray;
			font-size: 18px;
			background-color: #3399ff;
			height: 34px;
		}
		#overview-header{
			font-size: 1.3em;
		}
		#overview-order li{
		    line-height: 40px;
			font-size: 1.3em;
		}
		.label-field{
			margin-top: 9px;
			font-weight: 600;
			white-space: nowrap;
		}
		.orangeButton{
			color: #fff;
			background-color: #ffb266;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		
		.darkGreenButton{
			color: #fff;
			background-color: #00E09C;
			border-color : #007C56;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		
		.greenButton{
			color: #fff;
			background-color: #5cb85c;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		.redButton{
			color: #fff;
			background-color: #ff687f;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		.yellowButton{
			color: #fff;
			background-color: #fdc826;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		div.tab button {
		    float: left;
		    border: none;
		    outline: none;
		    cursor: pointer;
		    padding: 10px 20px;
		    transition: 0.3s;
		    border: 1px solid #ccc;
		    background-color: #f1f1f1;
		}
		
		div.tab button:hover {
		    background-color: #ddd;
		}
		
		div.tab button.active {
		    background-color: #ccc;
		}
		

		#productDataTable  td {
    		min-width: 10px;
    		max-width: 20px;
    		overflow: hidden;
    		text-overflow: ellipsis;
    		white-space: nowrap;
    		word-wrap:break-word;
		}
		
  		#productDataTable  td:hover{
	    	overflow: visible; 
	   		white-space: normal;
	    	height:auto;  /* just added this line */
		}    
		
		/* Create two equal columns that floats next to each other */
		.column {
		    float: left;
		    width: 50%;
		}
		
		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}
		.wrap{
		    font-size: 16px;
 		    margin: 10px; 
		}
		
		#topBtn{
			display: block;
			z-index: 9999;
			position: fixed;
			bottom: 15px;
			right: 15px;
		}
	</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	
	<div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="wrapper">
			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			<!-- BEGIN CONTENT -->
		    <div class="page-content-wrapper">
		        <!-- BEGIN CONTENT BODY -->
		        <div class="page-content">
		            <!-- BEGIN PAGE HEADER-->
		            <!-- BEGIN PAGE BAR -->
		            <div class="page-bar" style="margin-bottom: 2%;">
		            	<div class="col-md-9">
	          				<h3 class="page-title" style="white-space: nowrap;"><i class="fa fa-wrench" style="padding-right: 10px;"></i>Check Merchant Summary</h3>
	         			</div>
	         			<div class="col-md-1"></div>
		        	</div>
					<div class="modal fade" id="modalAlert" style="top: 5%;">
	    				<div class="modal-dialog" style="width: 40%;">
	    					<div class="modal-content">
		        				<div class="modal-header">
		          					<button type="button" class="close" data-dismiss="modal">&times;</button>
		          					<h4 class="modal-title" id="modalAlertTitle"></h4>
		        				</div>
		       					<div class="modal-body" id="modalAlertData"></div>
		    					<div class="modal-footer">
		          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        				</div>
	    					</div>
	    				</div>
   					</div>
					<!-- BEGIN MERCHANT DETAIL -->
					<c:if test="${merchantData != null}">
						<c:set var = "merchant" value = "${merchantData.id}"/>
						<input type="hidden" id="merchantId" value="${merchant}">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light bordered">
		 							<div class="portlet-title">
										<h4 id="group-titleName">
											<button type="button" class="btn btn-sm btn-default" onclick="toggleShow(this)">HIDE</button>
											<span id="overview-header">&nbsp;<c:out value="${merchantData.id} : ${merchantData.name}"/></span>
										</h4>
									</div> 
									<div class="portlet-body" style="display:block; padding-top: 0;">
										<div class="row">
											<div class="column">
											    <div class="col-md-4" style="padding:10px;"><b> Merchant Id</b>		</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchantData.id}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Merchant Name</b>	</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchantData.name}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Status</b>			</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${ViewUtil.generateShowActive(merchantData.active)}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Package</b>			</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchantData.packageType}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Owner</b>			</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchantData.ownerName}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Last Update By</b>	</div>
											    <div class="col-md-6" id= "lastUpdate" style="padding:10px;">: &nbsp;
											    	<c:choose>
											    		<c:when test="${merchantData.lastUpdate != null}">
											    			<c:out value="${merchantData.userUpdate} (${DateTimeUtil.generateStringDateTime(merchantData.lastUpdate)})"/>
											    		</c:when>
											    		<c:otherwise>
											    			<c:out value="-"/>
											    		</c:otherwise>
										    		</c:choose>
										    	</div> 
											    <div class="col-md-4" style="padding:10px;"><b> Error Message</b>	</div>	
										    	<div class="col-md-6" id= "errormsg"style="padding:10px;">: &nbsp;
											    	<c:out value="${merchantData.errorMessage ? '' : merchantData.errorMessage}" />
										   	 	</div>
										   	 	
										   	 	<c:if test= "${not empty reportWceMerchantBean}">
										   	 	<div class="col-md-12" style="padding:10px;"><b> Wce Detail </b></div>			
										   	 	<div class="col-md-4" style="padding:10px; margin-left: 10px"><b>- Current Status</b></div>		
										   	 	<div class="col-md-6" style="padding:10px;"> :&nbsp;
											   	 	<c:choose>
												   	 	<c:when test="${not empty reportWceMerchantBean.startDate}"><c:set var= "startDateDisplay" value="${DateTimeUtil.generateStringDisplayDateTime(reportWceMerchantBean.startDate)}"></c:set></c:when><c:otherwise> <c:set var= "startDateDisplay" value=""></c:set></c:otherwise>
												   	</c:choose>
												   	<c:choose>	
												   	 	<c:when test="${not empty reportWceMerchantBean.endDate}"><c:set var= "endDateDisplay" value="${DateTimeUtil.generateStringDisplayDateTime(reportWceMerchantBean.endDate)}"></c:set></c:when><c:otherwise><c:set var= "endDateDisplay" value=""></c:set></c:otherwise>
												   	</c:choose>
												   	<c:out value="${reportWceMerchantBean.status} (${startDateDisplay} - ${endDateDisplay})"></c:out>
											   	</div>
										   	 	<div class="col-md-4" style="padding:10px; margin-left: 10px"><b>- Last success Runtime</b></div>		
										   	 	<div class="col-md-6" style="padding:10px;"> :&nbsp;
											   	 	<c:choose>
											   	 		<c:when test="${not empty reportWceMerchantBean.endDate}">
											   	 			${DateTimeUtil.calcurateDiff(reportWceMerchantBean.startDate,reportWceMerchantBean.endDate)}
											   	 		</c:when>
											   	 		<c:otherwise><c:out value="${reportWceMerchantBean.status}"/></c:otherwise>
											   	 	</c:choose>
											   	</div>
										 		</c:if>
										 		
										 		<c:if test= "${not empty reportDueMerchantBean}">
										 			<div class="col-md-12" style="padding:10px;"><b> Due Detail </b></div>
										 			<div class="col-md-4" style="padding:10px; margin-left: 10px"><b>- Current Status</b></div>		
											   	 	<div class="col-md-6" style="padding:10px;"> :&nbsp;
												   	 	<c:choose>
													   	 	<c:when test="${not empty reportDueMerchantBean.startDate}"><c:set var= "startDateDisplay" value="${DateTimeUtil.generateStringDisplayDateTime(reportDueMerchantBean.startDate)}"></c:set></c:when><c:otherwise> <c:set var= "startDateDisplay" value=""></c:set></c:otherwise>
													   	</c:choose>
													   	<c:choose>	
													   	 	<c:when test="${not empty reportDueMerchantBean.endDate}"><c:set var= "endDateDisplay" value="${DateTimeUtil.generateStringDisplayDateTime(reportDueMerchantBean.endDate)}"></c:set></c:when><c:otherwise><c:set var= "endDateDisplay" value=""></c:set></c:otherwise>
													   	</c:choose>
													   	<c:out value="${reportDueMerchantBean.status} (${startDateDisplay} - ${endDateDisplay})"></c:out>
												   	</div>	
											 		<div class="col-md-4" style="padding:10px; margin-left: 10px"><b>- Last success Runtime</b></div>	
											 		<div class="col-md-6" style="padding:10px;"> :&nbsp;
											   	 	<c:choose>
											   	 		<c:when test="${not empty reportDueMerchantBean.endDate}">
											   	 			${DateTimeUtil.calcurateDiff(reportDueMerchantBean.startDate,reportDueMerchantBean.endDate)}
											   	 		</c:when>
											   	 		<c:otherwise><c:out value="${reportDueMerchantBean.status}"/> </c:otherwise>
											   	 	</c:choose>
											   	</div>
										 		</c:if>
											</div>
											<div class="column">
											    <div class="col-md-12" style="padding:10px;"><b> Count Summary Detail </b>
											    	<button type="button" id="btnCountProductUrl" class="btn btn-xs btn-default" style="margin-left: 10px" onclick="getCountProductUrl();">SHOW COUNT</button>
											    </div>
										
											    <div class="showCountSummary">
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- New</b></div>				<div class="col-md-6" style="padding:10px;">:<span id="COUNT_NEW" style="margin-left: 10px;"></span></div> 
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- Language Error</b></div>	<div class="col-md-6" style="padding:10px;">:<span id="COUNT_LANGUAGEERROR" style="margin-left: 10px;"></span></div> 
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- Success</b></div>			<div class="col-md-6" style="padding:10px;">:<span id="COUNT_SUCCESS" style="margin-left: 10px;"></span></div> 
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- Duplicate</b></div>		<div class="col-md-6" style="padding:10px;">:<span id="COUNT_DUPLICATE" style="margin-left: 10px;"></span></div> 
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- Failure</b></div>			<div class="col-md-6" style="padding:10px;">:<span id="COUNT_FAILURE" style="margin-left: 10px;"></span></div> 
												    <div class="col-md-4" style="padding:10px;margin-left: 10px"><b>- Expire</b></div>			<div class="col-md-6" style="padding:10px;">:<span id="COUNT_EXPIRE" style="margin-left: 10px;"></span></div> 
											    </div>	
											    		
											    <div class="col-md-12">
													<div class="col-md-5" style="padding:10px;"><b> Count Product On Web</b></div>			
													<div class="col-md-6" style="padding:10px; margin-left: 10px;">:
														<c:set var="country" value="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}"></c:set> 
														<c:if test="${not empty country && country != null}">
															<c:set var="country_path" value="${ViewUtil.generateDomain(country)}"></c:set> 
															<a target="_blank" href="${country_path}rating?cmd=tabAllProduct&merchantId=${merchant}">Click</a>												
														</c:if>
													</div>
	
													<div class="col-md-5" style="padding:10px;"><b> Count Product On Bot</b></div>			
													<div class="col-md-6" style="padding:10px;">:
														<button type="button" id="btnCountOnBot" class="btn btn-xs btn-default" style="margin-left: 10px" onclick="getCountOnBot();">SHOW COUNT</button> 
														<span id="COUNT_PRODUCTONBOT" style="margin-left: 10px;"></span>
													</div>
												
							
												<div class="col-md-5" style="padding:10px;"><b> Product Waiting Delete</b></div>		
												<div class="col-md-6" style="padding:10px;">:
													<button type="button" id="btnCountProductWaitingDelete" class="btn btn-xs btn-default" style="margin-left: 10px" onclick="getCountWaitingDelete();">SHOW COUNT</button> 
													<span id="COUNT_PRODUCTWAITING_DELETE" style="margin-left: 10px;"></span>
												</div>
												<div class="col-md-5" style="padding:10px;"><b> Sync Done All</b></div>			
												<div class="col-md-6" style="padding:10px;">:
													<button type="button" id="btnCountProductWaitingSync" class="btn btn-xs btn-default" style="margin-left: 10px" onclick="getCountWaitingSync();">SHOW COUNT</button> 
													<span id="COUNT_PRODUCTWAITING_SYNC" style="margin-left: 10px;"></span>
												</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
					    <div class="tab">
						  <button id="cTab" class="btn tablinks" onclick="openTab('Config')">Config</button>
						  <button id="pTab" class="btn tablinks" onclick="openTab('Pattern')">URLPattern</button>
						  <button id="wTab" class="btn tablinks" onclick="openTab('Workload')">Workload</button>
						  <button id="uTab" class="btn tablinks" onclick="openTab('productUrl')">Product URL</button>
						  <button id="dTab" class="btn tablinks" onclick="openTab('productData')">Product Data</button>
						</div>	
						<!-- START CONFIG TAB-->
						<div class="configTab">
						 <div class=" row" id="urlConfigContent" >
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
							    	 	<div class="portlet-body">
			                                 <div style="margin-top:10px;">
			                                     <table id="urlConfigTable" class="table table-striped table-bordered table-hover" >
			                                         <thead>
			                                             <tr class="heading">
			                                             	<th width="30%"  style="text-align:center;"> Field </th>
			                                             	<th width="50%" style="text-align:center;"> Value </th>
			                                            	</tr>
			                                         </thead>
			                                         <tbody></tbody>
			                                         <tfoot>
			                                         	<tr class="footing">
			                                         		<td colspan="7" style="text-align:center;">
			                                          		<input type="hidden" id="tableMerchantId" name="tableMerchantId" value="0" />
			                                         		</td>
			                                         	</tr>
			                                     </table>
			                                 </div>
			                             </div>
			                        </div>
			                    </div>
			             </div>
						</div>
						<!-- END CONFIG TAB-->
						
						<!-- START PATTERN TAB-->
						<div class="patternTab" style="display:none;">
		                    <div class=" row" id="urlPatternContent" >
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="urlPatternTable" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='7%'  style="text-align:center;"> Condition </th>
		                                                	<th width='5%'  style="text-align:center;"> Group </th>
		                                                	<th width='45%' style="text-align:center;"> Value </th>
		                                                    <th width='8%'  style="text-align:center;"> Action </th>
		                                                    <th width='5%'  style="text-align:center;"> CatId </th>
		                                                    <th width='30%' style="text-align:center;"> Keyword </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                            <tfoot>
		                                        </table>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
						</div>
						<!-- END PATTERN TAB-->
						<!-- START WORKLOAD TAB-->
						<div class="workloadTab" style="display:none;">
		                    <div class=" row" id="urlWorkloadContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="workloadTable" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='55%'  style="text-align:center;"> URL </th>
		                                                	<th width='5%'  style="text-align:center;"> Depth </th>
		                                                	<th width='5%' style="text-align:center;"> Status </th>
		                                                    <th width='8%'  style="text-align:center;"> Category </th>
		                                                    <th width='22%' style="text-align:center;"> Keyword </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody>
		                                            </tbody>
		                                        </table>
		                                        <button id="btnLoadmoreWorkload" class="btn btn btn-primary " type="button" onclick="getWorkloadByMerchantId();" style="width: 100%" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading">Load More</button>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
		                	<input type="hidden" id="startPattern" value="">							
		                	<input type="hidden" id="startWorkload" value="">							
		                	<input type="hidden" id="startProductUrl" value="">							
		                	<input type="hidden" id="startProductData" value="">							
						</div>
						<!-- END WORKLOAD TAB-->
						<!-- START PRODUCT URL TAB-->
						<div class="productUrlTab" style="display:none;">
							<div class="row" id="urlProductUrlContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="productUrlTable" class="table table-hover">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='55%'  style="text-align:center;"> URL </th>
		                                                	<th width='5%'  style="text-align:center;"> State </th>
		                                                	<th width='5%' style="text-align:center;"> Status </th>
		                                                    <th width='8%'  style="text-align:center;"> Category </th>
		                                                    <th width='22%' style="text-align:center;"> Keyword </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                        </table>
		                                        <button id="btnLoadmoreProductUrl" class="btn btn btn-primary " type="button" onclick="getProductUrlByMerchantId();" style="width: 100%" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading">Load More</button>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>		
						</div>
						<!-- END PRODUCT URL TAB-->
						<!-- START PRODUCT DATA TAB-->
						<div class="productDataTab" style="display:none;">
							<div class=" row" id="urlProductDataContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table  id="productDataTable" class="table table-hover">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width="28%" style="text-align:center;"> Name </th>
		                                                	<th width="10%" style="text-align:center;"> Price </th>
		                                                	<th width="10%" style="text-align:center;"> BasePrice </th>
		                                                	<th width="12%" style="text-align:center;"> URL </th>
		                                                    <th width="12%" style="text-align:center;"> Description </th>
		                                                    <th width="12%" style="text-align:center;"> PictureUrl </th>
		                                                    <th width="5%" style="text-align:center;"> Real ProductId </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                        </table>
		                                        <button id="btnLoadmoreProductData" class="btn btn btn-primary " type="button" onclick="getProductDataByMerchantId();" style="width: 100%" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading">Load More</button>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
						</div>
						<!-- END PRODUCT DATA TAB-->
				    </c:if>	        
		        </div>
 
		    </div> <!-- end page-content-wrapper -->
	   </div> <!-- end wrapper  -->
	</div> <!-- end page-container -->
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/check-merchant-script.js"></script>
</body>
</html>