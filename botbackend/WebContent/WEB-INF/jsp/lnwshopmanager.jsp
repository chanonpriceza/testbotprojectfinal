<%@ page import="botbackend.system.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:set var ="contextPath" value="${pageContext.servletContext.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>
#taskContent tr:hover {
     background-color: #e5e5e5;
}
.color-check-icon{
	color: cadetblue;
}
.scroll_name {
    overflow: scroll;
    height: 320px;
    margin-bottom: 25px;
    overflow-x: hidden;
    overflow-y: auto;
}
.heading-single{
	border-bottom: 1px solid #e9e9e9;
    padding-bottom: 5px;
    margin-left: 25px
}
.div-head-topic{
	position: relative; 
	margin-left: 25px
}
.scroll_model {
    overflow: scroll;
    height: auto;
    margin-bottom: 25px; 
    overflow-x: hidden;
    overflow-y: auto;
}


.content{
  width: 100%;
}

.comments{
  width: 80%;
  margin: 30px auto;
}

.comment-content{
	margin: 10px 15px 5px;
}

.comment-header > .fa{
	margin-right: 10px;
}

.comment-more{
  float: right;
  margin: 4px;
  font-size :20px;
  cursor: pointer;
}

.comment-status{
  float: right;
  margin-right: 10px;
  margin-top: 2px;
}

.insert-text{
  position: relative;
}

.insert-text .loading{
  position: absolute;
  bottom: -25px;
  display: none;
}

.insert-text .total-comment{
  position: absolute;
  bottom: -25px;
  right: 0px;
}

.insert-text .total-comment:before{
  content: "Total comment: ";
  font-weight: bold;
}

.list-comments{
  margin-top: 30px;
  background: #fdfdfd;
}

.list-comments > div{
  padding: 10px;
  border-bottom: 1px solid #ccc;
}

.list-comments > div:last-child{
  border-bottom: none;
}

.editor{
  border: 1px solid #ccc;
  border-radius: 5px;
}

.editor-header{
  border-bottom: 1px solid #ccc;
}

.editor-header a{
  display: inline-block;
  padding: 10px;
  color: #666;
}

.editor-header a:hover{
  color: #000;
}

.editor-content{
  border: none;
  resize: none;
  width: 100%;
  padding: 10px;
  min-height: 150px;
  background: #f9f9f9;
  border-radius: 0px 0px 5px 5px;
}

.editor-content:focus{
  background: #fff;
}

.editor-footer{
  float: right;
  background: #fff;
  display: inline-flex;
  padding: 5px;

}

b{
  font-weight: bold;
}

i{
  font-style: italic;
}

p{
  line-height: 20px;
}

a{
  text-decoration: none;
}

[data-role="bold"]{
  font-weight: bold;
}

[data-role="italic"]{
  font-style: italic;
}

[data-role="underline"]{
  text-decoration: underline;
}

[class^="menu"] {
  position: relative;
  top: 6px;
  display: block;
  width: 27px;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:before {
  content: '';
  top: -5px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:after {
  content: '';
  top: 3px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

.menu-left {
  margin-right: 5px;
}

.menu-left:before{
  margin-right: 5px;
}

.menu-left:after{
  margin-right: 5px;
}

.menu-right {
  margin-left: 5px;
}

.menu-right:before{
  margin-left: 5px;
}

.menu-right:after{
  margin-left: 5px;
  
}

.page-container {
    margin: 0;
    padding: 0;
    position: relative;
    overflow: scroll;
}

#taskContent{
	word-break:break-word;
}
</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-users" style="padding-right: 10px;"></i> LnwShop Merchant Manager</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="home">Home</a> > <a href="#">LnwShop page</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<!-- START CONTENT -->
		                 	<c:if test="${warning == 'delete_data'}">
								<div class="alert alert-warning alert-dismissable fade in">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    	<strong>Warning!</strong> Can't edit merchant. Please add task to delete product. <br /> ไม่สามารถแก้ไขร้านค้าได้ในขณะนี้ ให้ติดต่อ Dev เพื่อลบสินค้าเก่าออกก่อน
							  	</div>
							</c:if>

		                 	<c:if test="${fn:contains(warning, 'contact_dev')}">
								<div class="alert alert-danger alert-dismissable fade in">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    	<strong>Error!</strong> Can't edit merchant. Please contact dev <br /> ไม่สามารถแก้ไขร้านค้าได้ในขณะนี้ ให้ติดต่อ Dev เพื่อตรวจสอบ
							  	</div>
							</c:if>
							
							
		                 	<c:if test="${fn:contains(warning, 'not_found_config')}">
								<div class="alert alert-danger alert-dismissable fade in">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    	<strong>Error!</strong> Please create merchantId first. <br /> กรุณาใส่  merchantId ก่อน
							  	</div>
							</c:if>
							
		                 	<c:if test="${fn:contains(warning, 'data-server-incorrect')}">
								<div class="alert alert-danger alert-dismissable fade in">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    	<strong>Error!</strong> Can't edit merchant. Please contact dev <br /> ไม่สามารถแก้ไขร้านค้าได้ในขณะนี้ ให้ติดต่อ Dev เพื่อแก้ไข dataserver
							  	</div>
							</c:if>
							<div class="modal fade" id="modalAlert" style="top: 5%;">
			    				<div class="modal-dialog" style="width: 40%;">
			    					<div class="modal-content">
				        				<div class="modal-header">
				          					<button type="button" class="close closeBtn" data-dismiss="modal">&times;</button>
				          					<h4 class="modal-title" id="modalAlertTitle"></h4>
				        				</div>
				       					<div class="modal-body" id="modalAlertData"></div>
				    					<div class="modal-footer">
				          					<button type="button" class="btn btn-default closeBtn" data-dismiss="modal">Close</button>
				        				</div>
			    					</div>
			    				</div>
    						</div>
		                 	<!-- START Task Monitor TABLE -->
		                    <div class="row" id="lnwshopManagerTable">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                           
		                            	 <div class="row">
		                            	    <div class="col-md-12" style="margin: 25px 20px;">
		                            	   		<div class="row " >
							                   		<div class="col-md-6">
							                    	   	<form id="searchLnwShopMerchant" action="${contextPath}/admin/lnwshopmanager" method="post">
							                    	   	   	<div class="row " >
									                    	   	<div class="input-group">
									                    	   	   	<div class="input-group-btn">
										         						<button type="button" id="filter" class="btn">Merchant</button>
										                                <input type="hidden" id="activeType" 	class ="activeType" 		name="activeType" value="${activeType}"/> 
									                                </div>                    
									                                <input type="hidden" id="cmd" name="cmd" value="searchLnwShopMerchant">
												                    <input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:if test="${not empty searchParam}">${searchParam}</c:if>'>
												                    <span class="input-group-btn"><a><button type="submit" id ="searchhBtn" class="btn btn-primary"><i class="icon-magnifier"></i></button></a></span>
									                            </div>
								                            </div>
									                    </form>     
								                    </div> 
								     
							                       	 
							                       	 
							                	</div>                
		                                    </div>
                   						 </div>		
                   						    	
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="lnwshowContent" class="table table-striped">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width="8%" style="text-align:center;"></th>
		                                                	<th width="8%" style="text-align:center;"> MerchantID </th>
		                                                	<th width="8%" style="text-align:center;"> Merchant Domain</th>
		                                                	<th width="10%" style="text-align:center;"> 
	                                                
	                                                			<c:choose>
								                            	 	<c:when test="${not empty activeType && activeType == '0'}"><c:set var ="typeInactive" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty activeType && activeType == '1'}"><c:set var ="typeActive" value="${selected='selected'}"/></c:when>
								                            	 	<c:otherwise><c:set var ="typeAll" value="${selected='selected'}"/></c:otherwise>   
					                            	 			</c:choose>
																<select name="inputType" id="inputType" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																	<option value="-1" ${typeAll}>Status(All)</option>
																	<option value="0" ${typeInactive}>Inactive</option>
															  		<option value="1" ${typeActive}>Active</option>
																</select>		
	                                                	 	</th>
		                                                	<th width="8%" style="text-align:center;"> Add ParentName </th>
		                                                	<th width="8%" style="text-align:center;"> Add ProductId </th>
		                                                	<th width="8%" style="text-align:center;"> Add ContactPrice </th>
		                                                	<th width="8%" style="text-align:center;"> Force ContactPrice </th>
		                                                	<th width="8%" style="text-align:center;"> Re-sync Category </th>
		                                                	<th width="8%" style="text-align:center;"> PriceMultiply </th>
		                                                	<th width="5%" style="text-align:center;"> CountProduct </th>
		                                                    <th width="10%"  style="text-align:center;"> Info </th>
		                                                    
		                                               	</tr>        
		                                            </thead>
		                                            <tbody>
			                                            <c:if test="${not empty lnwShopMerchantList}" >
			                                               	<c:forEach items="${lnwShopMerchantList}" var="lnwShopMerchant"> 
				    											<tr class='rowPattern'>
				    												<c:set var ="data" value=""/>
				    												<c:set var ="result" value=""/>
				    												
				    											    <td width='8%'  style='text-align:center;'>
					    											<!-- ------------------------------------------------------------------ -->
					    											 <div class="btn-group">
								                                           <button id="btnEditNewMerchant" data-element-id="${lnwShopMerchant.id},${lnwShopMerchant.lnw_id},${lnwShopMerchant.name}" type="button" class="btn btn-info btn-primary dropdown-toggle" data-toggle="dropdown">Edit
								                                               <i class="fa fa-wrench"></i>
								                                               <span class="sr-only">Toggle Dropdown</span>
								                                           </button>
								                                           <ul class="dropdown-menu" role="menu">
								                                               <li><a class = "btnEditMerchant" data-element="btnEditMerchant" data-element-id="${lnwShopMerchant.id},${lnwShopMerchant.lnw_id},${lnwShopMerchant.name}" name="btnEditMerchant">Edit Merchant</a></li>
								                                           </ul>
								                                       </div>	
								                                       <!-- ------------------------------------------------------------------ -->	
					    											</td>
				    											    <td width='8%'  style='text-align:center;'>
					    											    <div class="row">
					    											    	<c:choose>
		 				    											   		<c:when test="${empty lnwShopMerchant.id || lnwShopMerchant.id == 0}">-</c:when>
																				<c:otherwise>${lnwShopMerchant.id}</c:otherwise>
																			</c:choose>				    											    
					    											    </div>
				    											    </td>
				    											    
				    											    <td width='8%'  style='text-align:center;'><c:out value="${lnwShopMerchant.name}"/></td>
					    						
				    											    <td width='10%'  style='text-align:center;'>
																	  	<span id="activeType">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.active == 0}"><c:out value="Inactive"/></c:when> 
																				<c:when test="${lnwShopMerchant.active == 1}"><c:out value="Active"/></c:when> 
																			</c:choose>
																		</span>
																  	</td>
				    											    
				    											    <td width='8%'  style='text-align:center;'>
																	  	<span id="addParentName">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.addParentName == 0}"><c:out value="-"/></c:when> 
																				<c:when test="${lnwShopMerchant.addParentName == 1}"><i class="fa fa-check-circle color-check-icon" aria-hidden="true"></i></c:when> 
																			</c:choose>
																		</span>
																  	</td>
																  	
				    											    <td width='8%'  style='text-align:center;'>
																	  	<span id="addProductId">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.addProductId == 0}"><c:out value="-"/></c:when> 
																				<c:when test="${lnwShopMerchant.addProductId == 1}"><i class="fa fa-check-circle color-check-icon" aria-hidden="true"></i></c:when> 
																			</c:choose>
																		</span>
																  	</td>
																  	
																  	<td width='8%'  style='text-align:center;'>
																	  	<span id="addContactPrice">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.addContactPrice == 0}"><c:out value="-"/></c:when> 
																				<c:when test="${lnwShopMerchant.addContactPrice == 1}"><i class="fa fa-check-circle color-check-icon" aria-hidden="true"></i></c:when> 
																			</c:choose>
																		</span>
																  	</td>
																  	
																  	<td width='8%'  style='text-align:center;'>
																	  	<span id="forceContactPrice">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.forceContactPrice == 0}"><c:out value="-"/></c:when> 
																				<c:when test="${lnwShopMerchant.forceContactPrice == 1}"><i class="fa fa-check-circle color-check-icon" aria-hidden="true"></i></c:when> 
																			</c:choose>
																		</span>
																  	</td>
																  	
																  	<td width='8%'  style='text-align:center;'>
																	  	<span id="resyncCategory">
																	  		<c:choose>
																				<c:when test="${lnwShopMerchant.forceUpdateCat == 0}"><c:out value="-"/></c:when> 
																				<c:when test="${lnwShopMerchant.forceUpdateCat == 1}"><i class="fa fa-check-circle color-check-icon" aria-hidden="true"></i></c:when> 
																			</c:choose>
																		</span>
																  	</td>
																  	
																  	<td width='8%'  style='text-align:center;'>
																	  	<span id="priceMultiply"><c:out value="${lnwShopMerchant.priceMultiply}"/></span>
																  	</td>
																  	
																  	
															  		<td width='5%'  style='text-align:center;'>
															  			<button id="btnCountProduct${lnwShopMerchant.name}" 	class="btn btn-default" style="width: 100% ;" type="button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" onclick="getCountProduct('${lnwShopMerchant.lnw_id}','${lnwShopMerchant.name}');">CHECK</button>																				
															  			<span id="COUNT_PRODUCTONBOT${lnwShopMerchant.name}" style="margin-left: 10px;"></span>
															  		</td> 
															 		<td width='10%' style='text-align: center;'><a class=idRefLnw data-element-id="${lnwShopMerchant.lnw_id},${lnwShopMerchant.name}"name="idRefLnw" ><button class="btn btn-info" >→</button></a></td>
														  		</tr>
															</c:forEach>
														</c:if>
		                                            </tbody>    	 
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
 
								<!--  paging -->
						        <div class="row" style="float: right;">
						          	<div class="col-md-12">
						                <c:if test="${not empty pagingBean}" >
							                <c:set var ="pagingBean" value="${pagingBean}"/>
											<div class="dataTables_paginate paging_bootstrap">
												<ul class="pagination">
													<c:choose>
													    <c:when test="${pagingBean.getCurrentPage() == 1}">
													       <li class="prev disabled"><a href="#">← Previous</a></li>
													    </c:when>    
													    <c:otherwise>
													     <li class="prev"><a href="${contextPath}/admin/searchlnwshopmanager?page=${pagingBean.getCurrentPage()-1}&searchParam=${searchParam}&activeType=${activeType}">← Previous</a></li>			
													    </c:otherwise>
													</c:choose>
													<c:forEach items="${pagingBean.getPagingList()}" var="pageNo"> 
														<c:choose>
													    <c:when test="${pageNo.equals(String.valueOf(pagingBean.getCurrentPage()))}">
													       <li><a href="#" style="background-color: #ccc;">${pageNo}</a></li>
													    </c:when>
													    <c:when test="${pageNo.equals('...')}">
													       <li><a href="#">...</a></li>
													    </c:when>    
													    <c:otherwise>
													     	<li><a href="${contextPath}/admin/searchlnwshopmanager?page=${pageNo}&searchParam=${searchParam}&activeType=${activeType}">${pageNo}</a></li>
													    </c:otherwise>
													</c:choose>
													</c:forEach>
													<c:choose>
													    <c:when test="${pagingBean.getCurrentPage() == pagingBean.getTotalPage()}">
													       <li class="next disabled"><a href="#">Next → </a></li>
													    </c:when>    
													    <c:otherwise>
													     	<li class="next"><a href="${contextPath}/admin/searchlnwshopmanager?page=${pagingBean.getCurrentPage()+1}&searchParam=${searchParam}&activeType=${activeType}">Next → </a></li>
													    </c:otherwise>
													</c:choose>
												</ul>
											</div>
										</c:if>
							       	</div>
							    </div>
						    </div>
					     <!--  End paging -->  	
					     
					     <!--------------------------Start Task Detail--------------------------->
					<div class="row" id="showProductData" style="display:none;">
		                    <div class=" row" id="dataContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
			                                    <div class="portlet-title">
													<div class="caption">
														<div class="col-md-12">
															<div class="col-md-10" style="margin-top: 12px; width: 100%; display: inline;">
																<h3 id="group-productName">
																	<button id="buttonback" class="btn btn-primary" type="button" style="margin-right: 5px;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
																	<span class="txtTitle">Merchant Data : </span>
																	<span class="txtTitle" id="txtNameMerchant"></span>
																</h3>
															</div>
														</div>
													</div>
												</div>
		                                        <table id="dataTable" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='10%'  style="text-align:center;"> Parent Name </th>
		                                                	<th width='8%'  style="text-align:center;"> Name </th>
		                                                	<th width='8%' style="text-align:center;"> Price </th>
		                                                    <th width='15%'  style="text-align:center;"> Description </th>
		                                                    <th width='15%' style="text-align:center;"> Picture Url </th>
		                                                    <th width='15%' style="text-align:center;"> Product Url </th>
		                                                    <th width='15%' style="text-align:center;"> Product Status </th>
		                                                    <th width='5%' style="text-align:center;"> UpdateDate </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody>
		                                            </tbody>
		                                        </table>
		                                        <button id="btnLoadmoreData" class="btn btn btn-primary " type="button" onclick="getDataBylnwId();" style="width: 100%" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading">Load More</button>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>		
		                	<input type="hidden" id="startData" value="">							
		                	<input type="hidden" id="cLnwID" value="">							
					</div>
	
	
				</div>
			</div>
		</div>
	</div>
	
		
	
	<!--START MODAL -->
							<div class="row">
			       			<div id="editLnwShopMerchantModal" class="modal fade back-drop" >
								<div class="modal-dialog modal-xs">
								 	<div class="modal-content">
								      	<div class="modal-header">
								        	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
								        	<h4 class="modal-title" id="addFormLabel"><b>Edit LnwShop Merchant Manager</b></h4>
								      	</div>
								      	<form id="editLnwShopMerchantForm" action="${contextPath}/admin/lnwshopmanager" method="post">
								      		<div class="modal-body">
												<div style="margin-left: 25px; margin-right: 25px;">
	                                        		<div class="row" style="padding: 0px">
				                                        <div class="col-md-12" style="padding: 0px">
	                                        		        <label class="control-label"><b>MerchantID</b></label>&nbsp;<span class="text-danger">*</span>
					                                         <div class="row">
					                                            <div class="col-md-12">
					                                            	<input type="text" id="eMerchantID" name="eMerchantID" value="" onblur="validateInput(this)"  class="form-control">
				                                        		</div>
			                                        		</div>
														</div>
													</div>
	                                        		
	                                        		<div class="row" style="padding: 0px">
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Active type</b></label>	
					                                         <div>
																<select id="eActiveType" name="eActiveType" class="form-control" style="width:95%;">
																	<option value="0">Inactive</option>
		                                        					<option value="1">Active</option>
																</select>
															</div>
														</div>
														
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Add parent name (before name) </b></label>	
					                                         <div>
																<select id="eAddParentName" name="eAddParentName" class="form-control" style="width:100%;">
																	<option value="0">Not add</option>
		                                        					<option value="1">Add</option>
																</select>
															</div>
														</div>
													</div>
	                                        		<div class="row" style="padding: 0px">
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Add product id (after name)</b></label>	
					                                         <div>
																<select id="eAddProductId" name="eAddProductId" class="form-control" style="width:95%;">
																	<option value="0">Not add</option>
		                                        					<option value="1">Add</option>
																</select>
															</div>
														</div>
														
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Add contact price</b></label>	
					                                         <div>
																<select id="eAddContactPrice" name="eAddContactPrice" class="form-control" style="width:100%;">
																	<option value="0">Not add</option>
		                                        					<option value="1">Add</option>
																</select>
															</div>
														</div>
													</div>
	                                        		<div class="row" style="padding: 0px">
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Force contact price</b></label>	
					                                         <div>
																<select id="eForceContactPrice" name="eForceContactPrice" class="form-control" style="width:95%;">
																	<option value="0">Not force</option>
		                                        					<option value="1">Force</option>
																</select>
															</div>
														</div>
														
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Re-Sync Category</b></label>	
					                                         <div>
																<select id="eForceUpdateCat" name="eForceUpdateCat" class="form-control" style="width:95%;">
																	<option value="0">No</option>
		                                        					<option value="1">Force (Only 1 Round)</option>
																</select>
															</div>
														</div>
													</div>
	                                        		<div class="row" style="padding: 0px">
				                                        <div class="col-md-6" style="padding: 0px">
	                                        		        <label class="control-label"><b>Price Multiply</b></label>	
					                                         <div class="row">
					                                            <div class="col-md-12">
					                                            	<input type="text" id="ePriceMultiply" name="ePriceMultiply" onblur="validateInput(this)"  class="form-control" placeholder="Ex : 1.0">
				                                        		</div>
			                                        		</div>
														</div>
													</div>
													
													
	                                        	</div>								
								      		</div>
									      	<div class="modal-footer">
									      		<input type="hidden" id="cmd" name="cmd" value="editlnwMerchant">
									      		<input type="hidden" id="eLnwID" name="eLnwID" value="">
									            <input type="hidden" id="page" name="page" value="${pagingBean.getCurrentPage()}">
											   	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												<button type="button" id ="submitbtn"class="btn btn-primary">Submit</button>
									    	</div>
										</form>
								 	</div>
								</div>
							</div>
			       			</div>
							<!-- END  MODAl -->
							
	
	<input type="hidden" id="contextPath" name="contextPath" value="${contextPath}">
	<script type="text/javascript" src="${contextPath}/assets/pages/js/lnwshop-manager-script.js"></script>
</body>
</html>