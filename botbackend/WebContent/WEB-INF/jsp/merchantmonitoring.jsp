<%@ page import="botbackend.bean.UserBean, botbackend.system.*, botbackend.utils.Util,utils.DateTimeUtil, botbackend.utils.ViewUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="priceza" prefix="pz"%>

<c:set var ="contextPath" value="${pageContext.servletContext.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>
#commentTable tr:hover {
     background-color: #e5e5e5;
}

.scroll_name {
    overflow: scroll;
    height: 320px;
    margin-bottom: 25px;
    overflow-x: hidden;
    overflow-y: auto;
}
.heading-single{
	border-bottom: 1px solid #e9e9e9;
    padding-bottom: 5px;
    margin-left: 25px
}
.div-head-topic{
	position: relative; 
	margin-left: 25px
}
.scroll_model {
    overflow: scroll;
    height: auto;
    margin-bottom: 25px; 
    overflow-x: hidden;
    overflow-y: auto;
}


.content{
  width: 100%;
}

.insert-text{
  position: relative;
}

.insert-text .loading{
  position: absolute;
  bottom: -25px;
  display: none;
}

.insert-text .total-comment{
  position: absolute;
  bottom: -25px;
  right: 0px;
}

.insert-text .total-comment:before{
  content: "Total comment: ";
  font-weight: bold;
}

.list-comments{
  margin-top: 30px;
  background: #fdfdfd;
}

.list-comments > div{
  padding: 10px;
  border-bottom: 1px solid #ccc;
}

.list-comments > div:last-child{
  border-bottom: none;
}

.editor{
  border: 1px solid #ccc;
  border-radius: 5px;
}

.editor-header{
  border-bottom: 1px solid #ccc;
}

.editor-header a{
  display: inline-block;
  padding: 10px;
  color: #666;
}

.editor-header a:hover{
  color: #000;
}

.editor-content{
  border: none;
  resize: none;
  width: 100%;
  padding: 10px;
  min-height: 150px;
  background: #f9f9f9;
  border-radius: 0px 0px 5px 5px;
}

.editor-content:focus{
  background: #fff;
}

.editor-footer{
  float: right;
  background: #fff;
  display: inline-flex;
  padding: 5px;

}
.editor-footer-template-comment{
  float: left;
  background: #fff;
  display: inline-flex;
  padding: 5px;

}

b{
  font-weight: bold;
}

i{
  font-style: italic;
}

p{
  line-height: 20px;
}

a{
  text-decoration: none;
}

[data-role="bold"]{
  font-weight: bold;
}

[data-role="italic"]{
  font-style: italic;
}

[data-role="underline"]{
  text-decoration: underline;
}

[class^="menu"] {
  position: relative;
  top: 6px;
  display: block;
  width: 27px;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:before {
  content: '';
  top: -5px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:after {
  content: '';
  top: 3px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

.menu-left {
  margin-right: 5px;
}

.menu-left:before{
  margin-right: 5px;
}

.menu-left:after{
  margin-right: 5px;
}

.menu-right {
  margin-left: 5px;
}

.menu-right:before{
  margin-left: 5px;
}

.menu-right:after{
  margin-left: 5px;
  
}

.page-container {
    margin: 0;
    padding: 0;
    position: relative;
    overflow: scroll;
}

#monitoringContent{
	word-break:break-word;
}

.custom-catption{
    font-size: 18px;
    line-height: 18px;
    padding: 10px 0;
}


#thTab.active {
	background-color:#0080ff;
}
#idTab.active {
	background-color:#ff8c1a;
}
#myTab.active {
	background-color:#4f9a94;
}
#phTab.active {
	background-color:#ff5c8d;
}
#sgTab.active {
	background-color:#002071;
	color:white;
}
#vnTab.active {
	background-color:#8e0000;
	color:white;
}


</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper" style="max-width:85%">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-desktop" style="padding-right: 10px;"></i> Merchant Monitoring</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Merchant Monitoring page</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<!-- START CONTENT -->
		                 	
		                 	<!-- START Monitoring Monitor TABLE -->
		                    <div class="row" id="monitoringManagerTable">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                            	<div class="portlet-title">
		                            		<div class="custom-catption">
		                            			<div class="col-md-12">
		                            				<div class="col-md-12">
		                            					<span id="group-productName">Merchant&nbsp;Monitoring&nbsp;Table</span></h4>
		                            					<span class="tab" style="float:right">
		                            					<c:forTokens items = "TH,ID,SG,MY,VN,PH" delims = "," var = "name">
		                            					 	<c:choose>
		                            					 		<c:when test="${fn:toLowerCase(BaseConfig.BOT_SERVER_CURRENT_COUNTRY) != fn:toLowerCase(name)}">
        															<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks" onclick="getMonitor('${name}',1)" >${ name }</button>
        														</c:when>
        														<c:otherwise>
        															<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks active" onclick="getMonitor('${name}',1)">${ name }</button>	
        														</c:otherwise>
        													</c:choose>
     													 </c:forTokens> 
														</span>
		                            				</div>
			                            		</div>
		                            		</div>
		                            	</div>
		                            	 <div class="row">
		                            	    <div class="col-md-12" style="margin: 25px 20px;">
		                            	   		<div class="row " >
							                   		<div class="col-md-6">
							                    	   	<form id="searchMonitorManager" action="${contextPath}/admin/merchantmonitoring" method="post">
							                    	   	   	<div class="row " >
									                    	   	<div class="input-group">
									                    	   	   	<div class="input-group-btn">
										         					 	<c:choose>
													                        <c:when test="${not empty searchType && searchType == 1}"><c:set var ="searchTypeShow" value="${'Merchant Name'}"/></c:when>
													                        <c:when test="${not empty searchType && searchType == 2}"><c:set var ="searchTypeShow" value="${'Issue'}"/></c:when>
													                        <c:when test="${not empty searchType && searchType == 3}"><c:set var ="searchTypeShow" value="${'Comment'}"/></c:when>
													                        <c:otherwise><c:set var ="searchTypeShow" value="${'Merchant Id'}"/></c:otherwise>
						                            	 			  	</c:choose>
								                                      	<button type="button" id="filter" class="btn dropdown-toggle" data-toggle="dropdown" >${searchTypeShow}<span class="fa fa-caret-down"></span></button>
							                                      	  	<ul class="dropdown-menu">
								                                             <li><a onclick="setTypes('0', 'Merchant Id');" class="typeFilter" >Merchant Id</a></li>
								                                             <li><a onclick="setTypes('1', 'Merchant Name');" class="typeFilter" >Merchant Name</a></li>
								                                             <li><a onclick="setTypes('2', 'Monitor Id');" class="typeFilter" >Monitor Id</a></li>
							                                          	</ul>
									                                </div>                    
							                                        <input type="hidden" id="searchType" name="searchType" value="${searchType}"/>
									                                <input type="hidden" id="topicType" name="topicType" value="${topicType}"/> 
									                                <input type="hidden" id="processType" name="processType" value="${processType}"/>
									                                <input type="hidden" id="cmd" name="cmd" value="searchMonitorManager">
												                    <input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:if test="${not empty searchParam}">${searchParam}</c:if>'>
												                    <span class="input-group-btn"><a id="searchhBtn" onclick="getMonitor()" class="btn btn-primary"><i class="icon-magnifier"></i></a></span>
									                            </div>
								                            </div>
									                    </form>     
								                    </div>   
								                    
								                    <div class="col-md-6">     
								                    	<div class="col-md-1">  
									                    	<button id ="btnFilterSearch" class="btn btn-primary" data-toggle="collapse" data-target="#refine-result"><i class="fa fa-filter"></i></button>
										            	</div>  									                    
								                	</div>   
							                	</div>                
		                                    </div>
                   						 </div>		
                   						 
                   						 <!--START FILTER MERCHANT-->
			                          	
		    							<div id="refine-result" class="collapse">
											<h4 class="modal-title" id="addFormLabel"><i class="fa fa-filter" style="margin-right: 5px"></i>Filter Monitoring</h4> 
							        		<div style="text-align: right;"><input style="display:none" type="checkbox" id="checkAll"class="form-check-input"> Check All</div>
							        		<form id="searchfilterMerchant" action="${contextPath}/admin/merchantmonitoring" method="post">
											<div style="margin-left: 25px; margin-right: 25px; color: #5b6770 font-size: 13px;">
												<div class="row">
													<div class="col-md-6">
														<div class="div-head-topic">
															<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkTopicAll"></div>
															<h5 class="heading-single"><b>Type</b></h5>
														</div>
																											
														<div id="checkTopicAll" class="form-check">
															<ul style="list-style: none;">
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="Stable" 				<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('Stable').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Stable</label></li>		
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="ExceedDeleteLimit"		<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('ExceedDeleteLimit').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Exceed Delete Limit</label></li>                  
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="NoUrlPattern"			<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('NoUrlPattern').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">No Url Pattern</label>
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="WorkloadFailedAll"		<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('WorkloadFailedAll').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Workload Failed All</label>
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="CannotMatch"			<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('CannotMatch').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Cannot Match</label>
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="CannotParse"			<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('CannotParse').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Cannot Parse</label>
																<li><input type="checkbox" class="checkTopic" name="checkTopic[]" value="NotFoundProduct"			<c:if test="${not empty topicType and fn:contains(topicType, \"'\".concat('NotFoundProduct').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">Not Found Product</label>
															</ul>
														</div>
													</div>	
													<div class="col-md-3">
														<div class="div-head-topic">
															<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkTypeAll"></div>
															<h5 class="heading-single"><b>Type</b></h5>
														</div>
															
														<div id="checkTypeAll" class="form-check">
															<ul style="list-style: none;">
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckType[]" value="WCE"	<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('WCE').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">WCE</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckType[]" value="DUE"  	<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('DUE').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">DUE</label></li>
															</ul>
														</div>
													</div>	
												</div>	
											</div>			
			                                		
			                                <input type="hidden" id="filtertopicType"		class ="topicType"		name="topicType" 	value=""/>  
			                                <input type="hidden" id="filterprocessType" 	class ="processType" 	name="processType" 	value=""/>  
									      	<input type="hidden" id="cmd" 					name="cmd" 				value="searchMonitorManager">
								      		<div style="text-align: right;"> 
							        			<button id ="btnCloseFilter" type="button" class="btn btn-default" data-toggle="collapse" data-target="#refine-result">Cancel</button>
												<a id ="btnConfirmFilter"class="btn btn-primary" onclick="getMonitor()">Confirm</a>
						        			</div>
									      		
										</form>	
												                                                        
									</div>                                                               
	    							                                                                    
								       <!--END FILTER MERCHANT-->                            	
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="monitoringContent" class="table table-striped">
		                                            <thead>
		                                                <tr class="heading" style="background-color: #e6e8eb;">
		                                                	<th width="5%" style="text-align:center;"> Id </th>
		                                                	<th width="10%" style="text-align:center;"> MerchantId </th>
		                                                	<th width="7%" style="text-align:center;"> 
		                                                		<c:choose>
		                                                			<c:when test="${not empty topicType and pz:isContainBracket(topicType) }"><c:set var ="ByFilter" value=""/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'Stable'}"><c:set var ="topicStable" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'ExceedDeleteLimit'}"><c:set var ="topicExceedDeleteLimit" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'NoUrlPattern'}"><c:set var ="topicNoUrlPattern" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'WorkloadFailedAll'}"><c:set var ="topicWorkloadFailedAll" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'CannotMatch'}"><c:set var ="topicCannotMatch" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'CannotParse'}"><c:set var ="topicCannotParse" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty topicType && topicType == 'NotFoundProduct'}"><c:set var ="topicNotFoundProduct" value="${selected='selected'}"/></c:when>
								                            	 	<c:otherwise><c:set var ="topicAll" value="${selected='selected'}"/></c:otherwise>   
					                            	 			</c:choose>
																<select name="inputTopic" id="inputTopic" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																	<c:if test="${not empty topicTopic && pz:isContainBracket(topicTopic) }">
																		<option value="" ${ByFilter} >By Filter</option>
																	</c:if>
																
																	<option value="" ${topicAll}>Topic(All)</option>
																	<option value="Stable"  			${topicStable}>Stable</option>
																	<option value="ExceedDeleteLimit" 	${topicExceedDeleteLimit}>Exceed Delete Limit</option>
																	<option value="NoUrlPattern" 		${topicNoUrlPattern}>No Url Pattern</option>
																	<option value="WorkloadFailedAll" 	${topicWorkloadFailedAll}>Workload Failed All</option>
																	<option value="CannotMatch" 		${topicCannotMatch}>Cannot Match</option>
																	<option value="CannotParse" 		${topicCannotParse}>Cannot Parse</option>
																	<option value="NotFoundProduct" 	${topicNotFoundProduct}>Not Found Product</option>
																</select>	
		                                                	</th>
		                                           
		                                                    <th width="15%" style="text-align:center;"> Detail </th>
		                                                    <th width="10%"  style="text-align:center;"> 
		                                                    	<c:choose>
		                                                    		<c:when test="${not empty processType and pz:isContainBracket(processType) }"><c:set var ="ByFilter" value=""/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'WCE'}"><c:set var ="processeWCE" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'DUE'}"><c:set var ="processDUE" value="${selected='selected'}"/></c:when>
								                            	 	<c:otherwise><c:set var ="processAll" value="${selected='selected'}"/></c:otherwise>   
					                            	 			</c:choose>
																<select name="inputProcesstype" id="inputProcesstype" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																	<c:if test="${not empty processType && pz:isContainBracket(processType) }">
																		<option value="" ${ByFilter}>By Filter</option>
																	</c:if>
																	<option value="" ${processAll}>Type(All)</option>
																	<option value="WCE" ${processeWCE}>WCE</option>
															  		<option value="DUE" ${processDUE}>DUE</option>
																</select>	
		                                                    </th> 
		                                                	<th width="7%"  style="text-align:center;"> Count </th>
		                                                	<th width="12%"  style="text-align:center;"> First Date </th>
		                                                	<th width="12%"  style="text-align:center;"> Recent Date </th>
		                                                    <th width="5%"  style="text-align:center;"> Info </th>
		                                                    
		                                               	</tr>        
		                                            </thead>  	
		                                            <tbody id="monitoringBoxCotainer" style="padding-top:35px">
														<tr>
														<td >
														</td>
															<td align="center" colspan="12" style="padding-top:40px">
																<div id="ajaxLoading">
																	
																</div>
															</td>
														<td>
														</td>
														</tr>
		                                            </tbody>    	 
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
 
								<!--  paging -->
						        <div class="row" style="float: right;">
						        <input type="hidden" value="" id="currentCountry"/>
						        <input type="hidden" value="" id="currentPage"/>
						        <input type="hidden" value="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}" id="currentServer">
						        <input type="hidden" value="" id="service-url-th"/>
						        <input type="hidden" value="" id="service-url-id"/>
						        <input type="hidden" value="" id="service-url-ph"/>
						        <input type="hidden" value="" id="service-url-my"/>
						        <input type="hidden" value="" id="service-url-sg"/>
						        <input type="hidden" value="" id="service-url-vn"/>
						        <div id="pagingDiv" class="col-md-12">
						               
							       	</div>
							    </div>
						    </div>
					     <!--  End paging --> 
		                 <!-- END Monitoring Monitor TABLE -->    	
		                	
		               	<!--------------------------Start Monitoring Detail--------------------------->
					<div class="row" id="showMonitoringDetail">
						<div class="col-md-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<div class="col-md-12">
											<div class="col-md-10" style="margin-top: 12px; width: 100%; display: inline;">
												<h3 id="group-productName">
													<!-- <button id="buttonback" class="btn btn-default btn-lg" type="button" style="margin-right: 5px"><i class="fa fa-chevron-left"></i></button> -->
													<button id="buttonback" class="btn btn-default" type="button" style="margin-right: 5px; color: #fff; background: #49c596"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
													<span class="txtTitle">Monitoring History</span>
												</h3>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="portlet light">
											<div class="portlet-body" style="display:block; padding-top: 0;padding-left: 30px;">
												<div class="row" id="downloadBox">
												</div>
												<div class="row" id="detailBox">
												</div>
												<div style="margin-top:20px;">
		                                        <table id="commentTable" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                                <tr class="heading" style="background: #85e4c0;">
		                                                	<th width='5%'  style="text-align:center;"> Type </th>
		                                                	<th width='8%'  style="text-align:center;"> Topic </th>
		                                                	<th width='15%' style="text-align:center;"> Detail </th>
		                                                    <th width='5%'  style="text-align:center;"> Count </th>
		                                                    <th width='10%'  style="text-align:center;"> First Date </th>
		                                                    <th width='10%' style="text-align:center;"> Recent Date </th>
		                                                    <th width='15%' style="text-align:center;"> Solve Detail </th>
		                                                    <th width='10%' style="text-align:center;"> Solve By </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                            <tfoot>
		                                        </table>
		                                    </div>
											</div>
										</div>
									</div>
							    </div>
							
								<div class="row">
									<div class="col-md-12">
										<div class="insert-text">
									      <span class="loading">Loading...</span>
									      <span style="display:none;" class="total-comment"></span>
									      <p>
									      </p>
										</div>
									    <div class="list-comments"></div>
										    <input type="hidden" id="monitoringId" name="monitoringId" value="" />
										</div>
									</div>
								</div>
						</div>
						<!--  start paging -->
						<div class="row" style="float: right; padding-bottom: 20px;">
							<div class="col-md-12">
								<div class="dataTables_paginate paging_bootstrap">
									<ul class='paginationMerchant pagination' style='margin-right: 0px;'></ul>
								</div>
							</div>
						</div>
						<!--  end paging -->
					</div>
					<!--end detail  -->
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="contextPath" name="contextPath" value="${contextPath}">
	<script type="text/javascript" src="${contextPath}/assets/pages/js/merchant-monitoring.js"></script>

</body>
</html>