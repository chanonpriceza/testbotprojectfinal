<%@ page import="botbackend.bean.MerchantBean, botbackend.utils.*, botbackend.system.BaseConfig, botbackend.utils.ViewUtil,utils.DateTimeUtil, java.util.HashMap, botbackend.bean.MerchantConfigBean" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<% 
	String contextPath = request.getContextPath(); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend : Check Approval</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	<style type="text/css">
		#merchantName > span{
			border: 1px solid #cccccc;
			margin-left: 20px;
			padding: 9px 14px 8px;
			font-size: 14px;
			font-weight: 600;
			border-radius: 2px;
		}
		#merchantName > span > span:not(:first-child){
			margin-left: 20px;
		}
		#btnSearch{
			color: darksategray;
			font-size: 18px;
			background-color: #3399ff;
			height: 34px;
		}
		#overview-header{
			font-size: 1.3em;
		}
		#overview-order li{
		    line-height: 40px;
			font-size: 1.3em;
		}
		.label-field{
			margin-top: 9px;
			font-weight: 600;
			white-space: nowrap;
		}
		.orangeButton{
			color: #fff;
			background-color: #ffb266;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		
		.darkGreenButton{
			color: #fff;
			background-color: #00E09C;
			border-color : #007C56;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		
		.greenButton{
			color: #fff;
			background-color: #5cb85c;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		.redButton{
			color: #fff;
			background-color: #ff687f;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		.yellowButton{
			color: #fff;
			background-color: #fdc826;
			border-color : #4cae4c;
			min-width: 85px;
			margin-top: 7px;
			margin-bottom : 10px;
		}
		div.tab button {
		    float: left;
		    border: none;
		    outline: none;
		    cursor: pointer;
		    padding: 10px 20px;
		    transition: 0.3s;
		    border: 1px solid #ccc;
		    background-color: #f1f1f1;
		}
		
		div.tab button:hover {
		    background-color: #ddd;
		}
		
		div.tab button.active {
		    background-color: #ccc;
		}
		

		#productDataTable  td {
    		min-width: 10px;
    		max-width: 20px;
    		overflow: hidden;
    		text-overflow: ellipsis;
    		white-space: nowrap;
    		word-wrap:break-word;
		}
		
  		#productDataTable  td:hover{
	    	overflow: visible; 
	   		white-space: normal;
	    	height:auto;  /* just added this line */
		}    
		
		/* Create two equal columns that floats next to each other */
		.column {
		    float: left;
		    width: 50%;
		}
		
		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}
		.wrap{
		    font-size: 16px;
 		    margin: 10px; 
		}
		
		#topBtn{
			display: block;
			z-index: 9999;
			position: fixed;
			bottom: 15px;
			right: 15px;
		}
	</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	
	<div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="wrapper">
			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			<!-- BEGIN CONTENT -->
		    <div class="page-content-wrapper">
		        <!-- BEGIN CONTENT BODY -->
		        <div class="page-content">
		            <!-- BEGIN PAGE HEADER-->
		            <!-- BEGIN PAGE BAR -->
		            <div class="page-bar" style="margin-bottom: 2%;">
		            	<div class="col-md-9">
	          				<h3 class="page-title" style="white-space: nowrap;"><i class="fa fa-wrench" style="padding-right: 10px;"></i>Check Approval</h3>
	         			</div>
	         			<div class="col-md-1"></div>
		        	</div>
					<div class="modal fade" id="modalAlert" style="top: 5%;">
	    				<div class="modal-dialog" style="width: 40%;">
	    					<div class="modal-content">
		        				<div class="modal-header">
		          					<button type="button" class="close" data-dismiss="modal">&times;</button>
		          					<h4 class="modal-title" id="modalAlertTitle"></h4>
		        				</div>
		       					<div class="modal-body" id="modalAlertData"></div>
		    					<div class="modal-footer">
		          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        				</div>
	    					</div>
	    				</div>
   					</div>
					<!-- BEGIN MERCHANT DETAIL -->
					<c:if test="${merchantData != null && merchantData.size() > 0}">
						<c:set var = "merchant" value = "${merchantData[0]}"/>
						<input type="hidden" id="merchantId" value="${merchant.getId()}">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light bordered">
		 							<div class="portlet-title">
										<h4 id="group-titleName">
											<button type="button" class="btn btn-sm btn-default" onclick="toggleShow(this)">HIDE</button>
											<span id="overview-header">&nbsp;<c:out value="${merchant.getId()} : ${merchant.getName()}"/></span>
										</h4>
									</div> 
									<div class="portlet-body" style="display:block; padding-top: 0;">
										<div class="row">
											<div class="column">
											    <div class="col-md-4" style="padding:10px;"><b> Merchant Id</b>		</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchant.getId()}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Merchant Name</b>	</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchant.getName()}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Package</b>			</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchant.getPackageType()}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Owner</b>			</div><div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${merchant.getOwnerName()}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Last Update By</b>	</div>
											    <div class="col-md-6" id= "lastUpdate"style="padding:10px;">: &nbsp;
											    	<c:choose>
											    		<c:when test="${merchant.getLastUpdate() != null}">
											    			<c:out value="${merchant.getUserUpdate()} (${DateTimeUtil.generateStringDateTime(merchant.getLastUpdate())})"/>
											    		</c:when>
											    		<c:otherwise>
											    			<c:out value="-"/>
											    		</c:otherwise>
										    		</c:choose>
										    	</div> 
											    <div class="col-md-4" style="padding:10px;"><b> Error Message</b>	</div>	
										    	<div class="col-md-6" id= "errormsg"style="padding:10px;">: &nbsp;
										    		<c:forEach items = "${merchantData}"  var= "entry">
										    			<c:set var="msg" value="${msg}|${entry.getErrorMessage() ? '' : entry.getErrorMessage()}" />
										    		</c:forEach>
										    		<c:choose>
											    		<c:when test="${merchant.getErrorMessage()!= null && merchant.getErrorMessage() != ''}">
											    			<c:set var = "tmp2" value = "${fn:substring(msg, 1, fn:length(msg))}" />
											    			<c:out value="${tmp2}"/>
											    		</c:when>
											    		<c:otherwise>
											    			<c:out value="-"/>
											    		</c:otherwise>
										    		</c:choose>
										   	 	</div>
										   	 	<c:if test= "${not empty wceReport}">
										   	 	<div class="col-md-4" style="padding:10px;"><b> Wce Runtime</b></div>		
										   	 	<div class="col-md-6" style="padding:10px;"> :&nbsp;
											   	 	<c:choose>
											   	 		<c:when test="${not empty wceReport.getEndDate()}">
											   	 			${DateTimeUtil.calcurateDiff(wceReport.getStartDate(),wceReport.getEndDate())}
											   	 		</c:when>
											   	 		<c:otherwise><c:out value="${wceReport.getStatus()}"/> </c:otherwise>
											   	 	</c:choose>
											   	</div>
										 		</c:if>
										 		<c:if test= "${not empty dueReport}">
											 		<div class="col-md-4" style="padding:10px;"><b> Due Runtime</b></div>		
											 		<div class="col-md-6" style="padding:10px;"> :&nbsp;
											   	 	<c:choose>
											   	 		<c:when test="${not empty dueReport.getEndDate()}">
											   	 			${DateTimeUtil.calcurateDiff(dueReport.getStartDate(),dueReport.getEndDate())}
											   	 		</c:when>
											   	 		<c:otherwise><c:out value="${dueReport.getStatus()}"/> </c:otherwise>
											   	 	</c:choose>
											   	</div>
										 		</c:if>
											</div>
											<div class="column">
											    <div class="col-md-12" style="padding:10px;"><b> Count URL </b></div>			
											    <c:forEach items="${productUrlCountList}" var="productUrlCount" varStatus="index">
											    	<div class="col-md-4" style="padding:0px 10px 10px 30px;"> State ("<c:out value="${productUrlCount.getState()}"/>")</div>
											    	<div class="col-md-6" style="padding:0px 10px 10px 10px;">: &nbsp;<c:out value="${productUrlCount.getTotoalCount()}"/></div> 
											    </c:forEach>
											    <div class="col-md-4" style="padding:10px;"><b> Count WL</b></div>		<div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${workloadCount}"/></div> 
											    <div class="col-md-4" style="padding:10px;"><b> Count Data</b></div>		<div class="col-md-6" style="padding:10px;">: &nbsp;<c:out value="${productCount}"/></div> 
											</div>
										</div>
										<div class="row" style="margin:20px" align="right">
											<button type="button" class="btn btn-primary greenButton" id="ApproveBtn" >Approve</button>
										</div>
									</div>
								</div>
							</div>
					    </div>
					    <div class="tab">
						  <button id="pTab" class="btn tablinks" onclick="openTab('Pattern')">URLPattern</button>
						  <button id="wTab" class="btn tablinks" onclick="openTab('Workload')">Workload</button>
						  <button id="uTab" class="btn tablinks" onclick="openTab('productUrl')">Product URL</button>
						  <button id="dTab" class="btn tablinks" onclick="openTab('productData')">Product Data</button>
						</div>	
						<!-- START PATTERN TAB-->
						<div class="patternTab" style="display:none;">
		                    <div class=" row" id="urlPatternContent" >
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="urlPatternTable" class="table table-striped table-bordered">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='7%'  style="text-align:center;"> Condition </th>
		                                                	<th width='5%'  style="text-align:center;"> Group </th>
		                                                	<th width='33%' style="text-align:center;"> Value </th>
		                                                    <th width='8%'  style="text-align:center;"> Action </th>
		                                                    <th width='5%'  style="text-align:center;"> CatId </th>
		                                                    <th width='30%' style="text-align:center;"> Keyword </th>
		                                                    <th width='12%' style="text-align:center;"> Command </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                            <tfoot>
		                                            	<tr class="footing">
		                                            		<td colspan="8" style="text-align:center;">
		                                            			<button type="button" class="btn btn-info btn-lg" onclick="addPatternRow()">Add Row</button> 
		                                            			<button type="button" class="btn btn-info btn-lg" onclick="savePatternTable()" id="savePatternTableBtn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</button> 
		                                            			<div id="saveResult"></div>
		                                            		</td>
		                                            	</tr>
		                                        </table>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
						</div>
						<!-- END PATTERN TAB-->
						<!-- START WORKLOAD TAB-->
						<div class="workloadTab" style="display:none;">
		                    <div class=" row" >
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="workloadTable" class="table table-striped table-bordered">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='5%'  style="text-align:center;"> No. </th>
		                                                	<th width='55%'  style="text-align:center;"> URL </th>
		                                                	<th width='5%'  style="text-align:center;"> Depth </th>
		                                                	<th width='5%' style="text-align:center;"> Status </th>
		                                                    <th width='8%'  style="text-align:center;"> Category </th>
		                                                    <th width='22%' style="text-align:center;"> Keyword </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody>
		                                            	<c:if test="${not empty workloadData}" >
		                                            		<c:forEach items="${workloadData}" var="workload" varStatus="index">
		                                            			<tr>
		                                            				<td width='5%' style="text-align:center;"> <c:out value="${index.count}"/></td>
		                                            				<td width='55%'> <a href="${workload.getUrl()}" target="_blank"><c:out value="${workload.getUrl()}"/></a></td>
				                                                	<td width='5%' style="text-align:center;"> <c:out value="${workload.getDepth()}"/></td>
				                                                	<td width='5%' style="text-align:center;"> <c:out value="${workload.getStatus()}"/></td>
				                                                    <td width='8%'> <c:out value="${workload.getCategoryId()}"/></td>
				                                                    <td width='22%'> <c:out value="${workload.getKeyword()}"/></td>
		                                            			</tr>
		                                            		</c:forEach>
		                                            	</c:if> 
		                                            </tbody>
		                                        </table>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>							
						</div>
						<!-- END WORKLOAD TAB-->
						<!-- START PRODUCT URL TAB-->
						<div class="productUrlTab" style="display:none;">
							<div class=" row" >
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table id="productUrlTable" class="table table-striped table-bordered">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width='5%'  style="text-align:center;"> No. </th>
		                                                	<th width='55%'  style="text-align:center;"> URL </th>
		                                                	<th width='5%'  style="text-align:center;"> State </th>
		                                                	<th width='5%' style="text-align:center;"> Status </th>
		                                                    <th width='8%'  style="text-align:center;"> Category </th>
		                                                    <th width='22%' style="text-align:center;"> Keyword </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody>
		                                            	<c:if test="${not empty productUrlData}" >
		                                            		<c:forEach items="${productUrlData}" var="productUrl" varStatus="index">
		                                            			<tr>
		                                            				<td width='5%' style="text-align:center;"> <c:out value="${index.count}"/></td>
		                                            				<td width='55%'> <a href="${productUrl.getUrl()}" target="_blank"><c:out value="${productUrl.getUrl()}"/></a></td>
				                                                	<td width='5%' style="text-align:center;"> <c:out value="${productUrl.getState()}"/></td>
				                                                	<td width='5%' style="text-align:center;"> <c:out value="${productUrl.getStatus()}"/></td>
				                                                    <td width='8%'> <c:out value="${productUrl.getCategoryId()}"/></td>
				                                                    <td width='22%'> <c:out value="${productUrl.getKeyword()}"/></td>
		                                            			</tr>
		                                            		</c:forEach>
		                                            	</c:if> 
		                                            </tbody>
		                                        </table>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>		
						</div>
						<!-- END PRODUCT URL TAB-->
						<!-- START PRODUCT DATA TAB-->
						<div class="productDataTab" style="display:none;">
							<div class=" row">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                                <div class="portlet-body">
		                                    <div style="margin-top:20px;">
		                                        <table  id="productDataTable" class="table table-striped table-bordered">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width="4%" style="text-align:center;"> No. </th>
		                                                	<th width="28%" style="text-align:center;"> Name </th>
		                                                	<th width="10%" style="text-align:center;"> Price </th>
		                                                	<th width="10%" style="text-align:center;"> BasePrice </th>
		                                                	<th width="12%" style="text-align:center;"> URL </th>
 		                                                    <th width="12%" style="text-align:center;"> URL For Update </th>
		                                                    <th width="12%" style="text-align:center;"> Description </th>
		                                                    <th width="12%" style="text-align:center;"> PictureUrl </th>
		                                                    <th width="5%" style="text-align:center;"> Real ProductId </th>
		                                               	</tr>
		                                            </thead>
		                                            <tbody>
		                                            	<c:if test="${not empty productData}" >
		                                            		<c:forEach items="${productData}" var="product" varStatus="index">
		                                            			<tr>
		                                            				<td width="4%" style="text-align:center;"> <c:out value="${index.count}"/></td>
		                                            				<td width="28%"> <c:out value="${product.getName()}"/></td>
				                                                	<td width="10%" style="text-align:center;"> <c:out value="${product.getPrice()}"/></td>
				                                                	<td width="10%"style="text-align:center;"> <c:out value="${product.getBasePrice()}"/></td>
				                                                    <td width="12%"> <c:out value="${product.getUrl()}"/></td>
 				                                                    <td width="12%"> <c:out value="${product.getUrlForUpdate()}"/></td>
 				                                                    <c:choose>
 				                                                    	<c:when test="${empty product.getDescription()}">
				                                                    		<td width="12%"> <c:out value=""/></td>
				                                                    	</c:when>
 				                                                    	<c:when test="${product.getDescription().length() > 100}">
				                                                    		<td width="12%"> <c:out value="${product.getDescription().substring(0,100)}"/></td>
				                                                    	</c:when>
				                                                    	<c:otherwise>
				                                                    		<td width="12%"> <c:out value="${product.getDescription()}"/></td>
				                                                    	</c:otherwise>
				                                                    </c:choose>
				                                                    <td width="12%"> <c:out value="${product.getPictureUrl()}"/></td>
				                                                    <td width="5%"> <c:out value="${product.getRealProductId()}"/></td>
		                                            			</tr>
		                                            		</c:forEach>
		                                            	</c:if> 
		                                            </tbody>
		                                        </table>
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
		                	</div>
						</div>
						<!-- END PRODUCT DATA TAB-->
				    </c:if>	        
		        </div>
       		    <!--START MODAL -->
				<div class="row">
       			<div id="selectServer" class="modal fade back-drop" >
					<div class="modal-dialog modal-xs">
					 	<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
					        	<h4 class="modal-title" id="addFormLabel"><b>Select Server</b></h4>
					      	</div>
					      	<form id="selectServerForm">
					      		<div class="modal-body">
									<div style="margin-left: 25px; margin-right: 25px;">
                                      			
                                      		<div class="row additionalDetail" style="margin-bottom: 10px">
                                      		<c:forEach items="${merchantData}" var="merchant"  varStatus="index">
                                      		<div id="Shard"+idex.count>
                                      			<div><h4><b>Shard# <c:out value ="${merchant.getShardNo()}"/></b></h4></div>
                                      			<input type="hidden" id="shardNo" value="${merchant.getShardNo()}"/>
                                     		   <div class="col-md-6" style="padding: 0px">
                                     		       <label class="control-label"><b>Server</b><span class="text-danger">*</span></label>
                                        			<div>
                                        				<select id="servername" name="servername" class="form-control" class="form-control" style="width: 95%;">
	                                                		<c:choose>
																<c:when test="${empty merchant.getServer()}"><option value="${''}"><c:out value=""/></option></c:when>
																<c:otherwise><option value="${merchant.getServer()}">${merchant.getServer()}</option></c:otherwise>
															</c:choose>

															<c:if test="${not empty BaseConfig.BOT_ACTIVE_SERVER_LIST}" >
																<c:if test="${merchant.getServer() != ''}"><option value="${''}"><c:out value=""/></option></c:if>
															  	<c:forEach items="${BaseConfig.BOT_ACTIVE_SERVER_LIST}" var="svName"> 
															  		 <c:if test="${merchant.getServer() != svName}"><option value="${svName}"><c:out value="${svName}"/></option></c:if>								
																</c:forEach>
															</c:if>
														</select>
                                        			</div>
                                     		   </div>
		                                     <div class="col-md-6" style="padding: 0px">
                                     		    <label class="control-label"><b>DataServer</b><span class="text-danger">*</span></label>
                                        		<div>
                                        			<select id="dataservername" name="dataservername" class="form-control" class="form-control" style="width: 95%;">
                                                		<c:choose>
															<c:when test="${empty merchant.getDataServer()}"><option value="${''}"><c:out value=""/></option></c:when>
															<c:otherwise><option value="${merchant.getDataServer()}">${merchant.getDataServer()}</option></c:otherwise>
														</c:choose>

														<c:if test="${not empty BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" >
															<c:if test="${merchant.getDataServer() != ''}"><option value="${''}"><c:out value=""/></option></c:if>
														  	<c:forEach items="${BaseConfig.BOT_ACTIVE_DATA_SERVER_LIST}" var="svName"> 
														  		 <c:if test="${merchant.getDataServer() != svName}"><option value="${svName}"><c:out value="${svName}"/></option></c:if>								
															</c:forEach>
														</c:if>
													</select>
												</div>
											</div>
                                       		<div class="col-md-6" style="padding: 0px; padding-bottom: 20px;">
                                     		     	<label class="control-label"><b>Status</b><span class="text-danger">*</span></label>
	                                           	<div>
	                                           	<select id="active" name="active" value="${merchant.getActive()}" class="form-control" style="width:95%;">
		                                           	<c:forEach begin="0" end="${ViewUtil.getMaxActiveType()}" varStatus="loop">
														<fmt:parseNumber var = "activeTypeNum" type = "number" value = "${merchant.getActive()}" />													
														<option value="${loop.index}" ${activeTypeNum == loop.index ? 'selected=\"selected\"' : ''}>${ViewUtil.generateShowActive(loop.index)}</option>
													</c:forEach>
												</select>
												</div>
											</div>
                                       		<div class="col-md-6" style="padding: 0px; padding-bottom: 20px;">
                                     		     	<label class="control-label"><b>Error Meassage</b></label>
	                                           	<div>
													<input id="message" name="message" value="${merchant.getErrorMessage()}" class="form-control" style="width:95%;" />
														 
												</div>
											</div>
										<input type="hidden" name ="cActive" id="cActive" value="${merchant.getActive()}">
										<input type="hidden" name ="cServername" id="cServername" value="${merchant.getServer()}">
										<input type="hidden" name ="cDataserver" id="cDataserver" value="${merchant.getDataServer()}">
										<input type="hidden" name ="cMessage" id="cMessage" value="${merchant.getErrorMessage()}">
										</div>
										</c:forEach>
									
                                       
                                       	</div>
                                       	</div>
                                       	
                                       	
					      		</div>
						      	<div class="modal-footer">
								   	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								   	<button type="button" id="FormReset" class="btn btn-primary">Clear</button>
									<button type="button" id ="submitbtn"class="btn btn-primary" onclick="updateMerchant()">Submit</button>
						    	</div>
							</form>
					 	</div>
					</div>
				</div>
       			</div>
				<!-- END  MODAl -->
		    </div> <!-- end page-content-wrapper -->
	   </div> <!-- end wrapper  -->
	</div> <!-- end page-container -->
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/check-approval-script.js"></script>
</body>
</html>