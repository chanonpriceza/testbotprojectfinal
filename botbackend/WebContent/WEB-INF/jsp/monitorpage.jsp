<%@ page import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<% String contextPath = request.getContextPath();  
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>

	#topBar {
		margin:12 0 12 0px;
		padding-right: 0px;
		padding-left: 0px;
		padding-top: 15px;
		padding-bottom: 30px;
	}
	#searchBtn{
		padding-top: 10px;
		padding-bottom: 10px;
	}
	.pagination{
		margin-right: 10px;
	}
	
</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                   	 <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-area-chart" style="padding-right: 10px;"></i> Monitor Manager </h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Monitor Page</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<!-- START CONTENT -->
		                 	
		                 	<!-- START MONITOR TABLE -->
		                 	<div class="alertBox"></div>
		                 	<div class="modal fade" id="modalAlert">
			    				<div class="modal-dialog" style="width: 40%;">
			    					<div class="modal-content">
				        				<div class="modal-header">
				          					<button type="button" class="close" data-dismiss="modal">&times;</button>
				          					<h4 class="modal-title" style="color:#FFC300;" >Alert</h4>
				        				</div>
				       					<div class="modal-body" id="modalAlertData"></div>
				    					<div class="modal-footer">
				          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        				</div>
			    					</div>
			    				</div>
    						</div>
		                    <div class="row" id="monitorContent">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                            	 <div class="row">
		                            	 	<div class="col-md-12">
	                            				<div class="col-md-10" id= "topBar" >
	                            					<h4 class="txtResult"><span>All Monitor result  record</span></h4>
	                            				</div>
			                            	</div>
		                            	 </div>
		                          	<div class="row ">
			                          	<div class="col-md-6" style="padding-bottom: 10px;display: inline-flex;">
											<span style="font-size: 16px; font-weight: bold;"><label class="control-label">Analyze Result </label></span>:&nbsp;&nbsp; 
											<select name="resultType" id="resultType" class="form-control" style="padding: 2px 10px;width: 30%;">
												<option value="">All</option>
												<option value="Parser">Parser</option>
												<option value="Network">Network</option>
												<option value="Block">Block</option>
												<option value="Feed_Parser">Feed_Parser</option>
												<option value="Feed_Network">Feed_Network</option>
												<option value="Feed_Block">Feed_Block</option>
												<option value="Feed_UnAuthorize">Feed_UnAuthorize</option>
												<option value="Parser_Delete_Limit">Parser_Delete_Limit</option>
												<option value="Feed_Delete_Limit">Feed_Delete_Limit</option>
											</select>
										</div>
		                          	</div>
									<div class="row ">
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="searchParam" name="searchParam" class="form-control" placeholder="Search By Merchant Id" value=''>
												<input type="hidden" id="cmd" name="cmd" value="searchMonitor">
												<span class="input-group-btn"> <a>
														<button type="button" id="searchBtn" class="btn btn-primary" >
															<i class="icon-magnifier"></i>
														</button>
												</a>
												</span>
											</div>
										</div>
										<div style="padding: 5px;" >
											<input type="checkbox" id="chkHistory" value="0">Show History
										</div>
									</div>
									<div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="monitorTable" class="table table-striped table-bordered">
		                                            <thead>
		                                                <tr class="heading">
		                                               		<th width="10%" style="text-align:center;"> Merchant Id </th>
		                                                	<th width="30%" style="text-align:center;"> Merchant Name </th>
		                                                	<th width="15%" style="text-align:center;"> Server </th>
		                                                	<th width="15%" style="text-align:center;"> Analyze Result </th>
		                                                	<th width="20%" style="text-align:center;"> Update Date </th>	
		                                                	<th width="20%" style="text-align:center;"> Action </th>
		                                               	</tr>        
		                                            </thead>
		                                            <tbody>
		                                            
		                                            </tbody>    	
		                                        </table>	
		                                    </div>
		                                </div>
		                                 	 <input type="hidden" id="analyzeResult" name="analyzeResult" value=""/> 
		                                 	 <input type="hidden" id="jsoupConfig" name="jsoupConfig" value=""/>
		                                 	 <input type="hidden" id="serverParam" name="serverParam" value=""/>
		                                 	 <input type="hidden" id="merchantIdParam" name="merchantIdParam" value=""/>
		                                 	 <input type="hidden" id="idParam" name="idParam" value=""/>
		                                 	 <input type="hidden" id="cmd" name="cmd" value="searchMonitor">
		                                 	 <input type="hidden" id="pageCurrent" value="0" />
		                            </div>	
		                        </div>
		                        </div>
		                        
		                        <div class="modal fade" id="modalRun">
				    				<div class="modal-dialog" style="width: 55%;overflow-y: scroll;max-height: 90%;">
				    					<div class="modal-content">
					        				<div class="modal-header">
					          					<h4 class="modal-title" id="modalTitle"></h4>
					        				</div>
					       					<div class="modal-body" id="modalData"></div>
					    					<div class="modal-footer" style="text-align: center;">
					    						<button type="button" class="btn btn-default confirmBtn" style="background: lightgreen;">Confirm</button>
					          					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancle</button>
					          					
					        				</div>
				    					</div>
				    				</div>
    							</div>
		                        <!--  paging -->
					        <div class="row" style="float: right; padding-bottom: 20px;">
					          	<div class="col-md-12">
									<div class="dataTables_paginate paging_bootstrap">
										<ul class='pagination' style='margin-right: 0px;'>
										</ul>
									</div>
					       		</div>
					    	</div>
					     <!--  End paging -->
		                 <!-- END MONITOR TABLE -->     	
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
				
	<script type="text/javascript" src="<%=contextPath %>/assets/pages/js/monitor-script.js"></script>

</body>
</html>