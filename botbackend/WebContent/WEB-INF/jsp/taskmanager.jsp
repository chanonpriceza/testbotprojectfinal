<%@ page import="botbackend.bean.UserBean, botbackend.system.*, botbackend.utils.Util,utils.DateTimeUtil, botbackend.utils.ViewUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="priceza" prefix="pz"%>

<c:set var ="contextPath" value="${pageContext.servletContext.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>Bot Backend</title>
		
		<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
		<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>
		
</head>
<style>
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}

#taskContent tr:hover {
     background-color: #e5e5e5;
}

.scroll_name {
    overflow: scroll;
    height: 320px;
    margin-bottom: 25px;
    overflow-x: hidden;
    overflow-y: auto;
}
.heading-single{
	border-bottom: 1px solid #e9e9e9;
    padding-bottom: 5px;
    margin-left: 25px
}
.div-head-topic{
	position: relative; 
	margin-left: 25px
}
.scroll_model {
    overflow: scroll;
    height: auto;
    margin-bottom: 25px; 
    overflow-x: hidden;
    overflow-y: auto;
}


.content{
  width: 100%;
}

.comments{
  width: 85%;
  margin: 30px auto;
}

.comment-content{
	margin: 10px 15px 5px;
}

.comment-header > .fa{
	margin-right: 10px;
}

.comment-more{
  float: right;
  margin: 4px;
  font-size :20px;
  cursor: pointer;
}

.comment-status{
  float: right;
  margin-right: 10px;
  margin-top: 2px;
}

.insert-text{
  position: relative;
}

.insert-text .loading{
  position: absolute;
  bottom: -25px;
  display: none;
}

.insert-text .total-comment{
  position: absolute;
  bottom: -25px;
  right: 0px;
}

.insert-text .total-comment:before{
  content: "Total comment: ";
  font-weight: bold;
}

.list-comments{
  margin-top: 30px;
  background: #fdfdfd;
}

.list-comments > div{
  padding: 10px;
  border-bottom: 1px solid #ccc;
}

.list-comments > div:last-child{
  border-bottom: none;
}

.editor{
  border: 1px solid #ccc;
  border-radius: 5px;
}

.editor-header{
  border-bottom: 1px solid #ccc;
}

.editor-header a{
  display: inline-block;
  padding: 10px;
  color: #666;
}

.editor-header a:hover{
  color: #000;
}

.editor-content{
  border: none;
  resize: none;
  width: 100%;
  padding: 10px;
  min-height: 150px;
  background: #f9f9f9;
  border-radius: 0px 0px 5px 5px;
}

.editor-content:focus{
  background: #fff;
}

.editor-footer{
  float: right;
  background: #fff;
  display: inline-flex;
  padding: 5px;

}
.editor-footer-template-comment{
  float: left;
  background: #fff;
  display: inline-flex;
  padding: 5px;

}

b{
  font-weight: bold;
}

i{
  font-style: italic;
}

p{
  line-height: 20px;
}

a{
  text-decoration: none;
}

[data-role="bold"]{
  font-weight: bold;
}

[data-role="italic"]{
  font-style: italic;
}

[data-role="underline"]{
  text-decoration: underline;
}

[class^="menu"] {
  position: relative;
  top: 6px;
  display: block;
  width: 27px;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:before {
  content: '';
  top: -5px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

[class^="menu"]:after {
  content: '';
  top: 3px;
  width: 80%;
  position: relative;
  display: block;
  height: 2px;
  margin: 0 auto;
  background: #999;
}

.menu-left {
  margin-right: 5px;
}

.menu-left:before{
  margin-right: 5px;
}

.menu-left:after{
  margin-right: 5px;
}

.menu-right {
  margin-left: 5px;
}

.menu-right:before{
  margin-left: 5px;
}

.menu-right:after{
  margin-left: 5px;
  
}

.page-container {
    margin: 0;
    padding: 0;
    position: relative;
    overflow: scroll;
}

#taskContent{
	word-break:break-word;
}

.custom-catption{
    font-size: 18px;
    line-height: 18px;
    padding: 10px 0;
}


#thTab.active {
	background-color:#0080ff;
}
#idTab.active {
	background-color:#ff8c1a;
}
#myTab.active {
	background-color:#4f9a94;
}
#phTab.active {
	background-color:#ff5c8d;
}
#sgTab.active {
	background-color:#002071;
	color:white;
}
#vnTab.active {
	background-color:#8e0000;
	color:white;
}


</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->

       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">

   			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper" style="max-width:85%">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                    <!-- BEGIN PAGE HEADER-->
		                     <!-- BEGIN PAGE BAR -->
		                    <div class="page-bar" style="margin-bottom:2%;">
		                    	<div class="col-md-6">
				                    	<h3 class="page-title"><i class="fa fa-file" style="padding-right: 10px;"></i> Task Manager</h3>
				                </div>
		                        <ul class="page-breadcrumb">
		                            <li>
		                                <a href="dashboard">Dashboard</a> > <a href="#">Task page</a> <i class="fa fa-circle"></i>
		                            </li>
		                        </ul>
		                    </div>
		                    <!-- END PAGE BAR -->
		                 	<!-- START CONTENT -->
		                 	
		                 	<!-- START Task Monitor TABLE -->
		                    <div class="row" id="taskManagerTable">
		                        <div class="col-md-12">
		                            <div class="portlet light bordered">
		                            	<div class="portlet-title">
		                            		<div class="custom-catption">
		                            			<div class="col-md-12">
		                            				<div class="col-md-12">
		                            					<span id="group-productName"><span>Task&nbsp;Manager&nbsp;Table</span></h4>
		                            					<span class="tab" style="float:right">
		                            					<c:forTokens items = "TH,ID,SG,MY,VN,PH" delims = "," var = "name">
		                            					 	<c:choose>
		                            					 		<c:when test="${fn:toLowerCase(BaseConfig.BOT_SERVER_CURRENT_COUNTRY) != fn:toLowerCase(name)}">
        															<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks" onclick="getTask('${name}',1)" >${ name }</button>
        														</c:when>
        														<c:otherwise>
        															<button id="${fn:toLowerCase(name)}Tab" name="countrySelector" data-toggle="tooltip" data-placement="right" style="text-decoration:none;" title="" class="btn tablinks active" onclick="getTask('${name}',1)">${ name }</button>	
        														</c:otherwise>
        													</c:choose>
     													 </c:forTokens> 
														</span>
		                            				</div>
			                            		</div>
		                            		</div>
		                            	</div>
		                            	 <div class="row">
		                            	    <div class="col-md-12" style="margin: 25px 20px;">
		                            	   		<div class="row " >
							                   		<div class="col-md-6">
							                    	   	<form id="searchTaskManager" action="${contextPath}/admin/taskmanager" method="post">
							                    	   	   	<div class="row " >
									                    	   	<div class="input-group">
									                    	   	   	<div class="input-group-btn">
										         					 	<c:choose>
													                        <c:when test="${not empty searchType && searchType == 1}"><c:set var ="searchTypeShow" value="${'Merchant Name'}"/></c:when>
													                        <c:when test="${not empty searchType && searchType == 2}"><c:set var ="searchTypeShow" value="${'Issue'}"/></c:when>
													                        <c:when test="${not empty searchType && searchType == 3}"><c:set var ="searchTypeShow" value="${'Comment'}"/></c:when>
													                        <c:otherwise><c:set var ="searchTypeShow" value="${'Merchant Id'}"/></c:otherwise>
						                            	 			  	</c:choose>
								                                      	<button type="button" id="filter" class="btn dropdown-toggle" data-toggle="dropdown" >${searchTypeShow}<span class="fa fa-caret-down"></span></button>
							                                      	  	<ul class="dropdown-menu">
								                                             <li><a onclick="setTypes('0', 'Merchant Id');" class="typeFilter" >Merchant Id</a></li>
								                                             <li><a onclick="setTypes('1', 'Merchant Name');" class="typeFilter" >Merchant Name</a></li>
								                                             <li><a onclick="setTypes('2', 'Issue');" class="typeFilter" >Issue</a></li>
								                                             <li><a onclick="setTypes('3', 'Comment');" class="typeFilter" >Comment</a></li>
								                                             <li><a onclick="setTypes('4', 'Task Id');" class="typeFilter" >Task Id</a></li>
							                                          	</ul>
									                                </div>                    
							                                        <input type="hidden" id="searchType" name="searchType" value="${searchType}"/>
									                                <input type="hidden" id="processType" name="processType" value="${processType}"/> 
									                                <input type="hidden" id="statusType" name="statusType" value="${statusType}"/>
									                                <input type="hidden" id="ownerType" name="ownerType" value="${ownerType}"/>  
									                                <input type="hidden" id="cmd" name="cmd" value="searchTaskManager">
												                    <input type="text" id="searchParam" name="searchParam" class="form-control" value='<c:if test="${not empty searchParam}">${searchParam}</c:if>'>
												                    <span class="input-group-btn"><a id="searchhBtn" onclick="getTask()" class="btn btn-primary"><i class="icon-magnifier"></i></a></span>
									                            </div>
								                            </div>
									                    </form>     
								                    </div>   
								     
							                       	 
							                       	 <div class="col-md-6">                
									                    <div class="col-md-6">
										            		<button id="addNewTask" class="btn btn-primary btn-block">Add</button>
										            		<a id="goOtherCountry" class="btn btn-primary btn-block" style="display:none;margin-top:0px">Go Add Task</a>
										            	</div>  
								                    	<div class="col-md-1">  
									                    	<button id ="btnFilterSearch" class="btn btn-primary" data-toggle="collapse" data-target="#refine-result"><i class="fa fa-filter"></i></button>
										            	</div>  									                    
								                	</div>   
							                	</div>                
		                                    </div>
                   						 </div>		
                   						 
                   						 <!--START FILTER MERCHANT-->
			                          	
		    							<div id="refine-result" class="collapse">
											<h4 class="modal-title" id="addFormLabel"><i class="fa fa-filter" style="margin-right: 5px"></i>Filter Task</h4> 
							        		<div style="text-align: right;"><input style="display:none" type="checkbox" id="checkAll"class="form-check-input"> Check All</div>
							        		<form id="searchfilterMerchant" action="${contextPath}/admin/taskmanager" method="post">
											<div style="margin-left: 25px; margin-right: 25px; color: #5b6770 font-size: 13px;">
												<div class="row">
													<div class="col-md-6">
														<div class="div-head-topic">
															<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkProcessAll"></div>
															<h5 class="heading-single"><b>Type</b></h5>
														</div>
																											
														<div id="checkProcessAll" class="form-check">
															<ul style="list-style: none;">
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="checkExceedProduct" 		<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkExceedProduct').concat(\"'\"))}">checked="checked"</c:if>>		<label class="form-check-label">เช็คสินค้า exceed</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="checkDuplicateProduct" 	<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkDuplicateProduct').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">เช็็คสืนค้าซ้ำ</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="removeProductPermanent " 	<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('removeProductPermanent ').concat(\"'\"))}">checked="checked"</c:if>>	<label class="form-check-label">ลบสินค้าออกถาวร</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="addAndRemoveProduct" 		<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('addAndRemoveProduct').concat(\"'\"))}">checked="checked"</c:if>>				<label class="form-check-label">ลบทิ้งดึงใหม่</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="checkExpireProduct" 		<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkExpireProduct').concat(\"'\"))}">checked="checked"</c:if>>		<label class="form-check-label">เช็คสินค้้าหมด</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="changeDetailCrawlingData" 	<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('changeDetailCrawlingData').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">เปลี่ยนรายละเอียดการดึง</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="newMerchant" 				<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('newMerchant').concat(\"'\"))}">checked="checked"</c:if>>				<label class="form-check-label">ทำร้านค้าใหม่</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="checkCrawlerWeb" 			<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkCrawlerWeb').concat(\"'\"))}">checked="checked"</c:if>>			<label class="form-check-label">ตรวจสอบ การดึงร้านจากหน้า web</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="checkCrawlerFeed" 			<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkCrawlerFeed').concat(\"'\"))}">checked="checked"</c:if>>		<label class="form-check-label">ตรวจสอบ การดึงร้านจาก Feed</label></li>
																<li><input type="checkbox" class="checkProcess" name="checkProcess[]" value="others" 					<c:if test="${not empty processType and fn:contains(processType, \"'\".concat('checkCrawlerFeed').concat(\"'\"))}">checked="checked"</c:if>>		<label class="form-check-label">อื่นๆ</label></li>
															</ul>
														</div>
													</div>	
													<div class="col-md-3">
														<div class="div-head-topic">
															<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkStatusAll"></div>
															<h5 class="heading-single"><b>Status</b></h5>
														</div>
															
														<div id="checkStatusAll" class="form-check">
															<ul style="list-style: none;">
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="WAITING"	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('WAITING').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">WAITING</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="DOING"  	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('DOING').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">DOING</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="REOPEN"	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('REOPEN').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">REOPEN</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="HOLD" 	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('HOLD').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">HOLD</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="DONE"	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('DONE').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">DONE</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="RESOLVE"	<c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('DONE').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">RESOLVE</label></li>
																<li><input type="checkbox" class="filterCheckStatus" name="filterCheckStatus[]" value="AUTOREPLY" <c:if test="${not empty statusType and fn:contains(statusType, \"'\".concat('AUTOREPLY').concat(\"'\"))}">checked="checked"</c:if>><label class="form-check-label">AUTOREPLY</label></li> 
															</ul>
														</div>
													</div>
													<div class="col-md-3">
														<div class="div-head-topic">
															<div style="position: absolute; top: 0;"><input type="checkbox" class="form-check-input" data-checkbox-target="#checkOwnerAll"></div>
															<h5 class="heading-single"><b>Owner</b></h5>
														</div>
														<div id ="checkOwnerAll" class="form-check scroll_name">
															<ul class="" style="list-style: none;">
															    <c:forEach items="${userList}" var="userItem">
																    <li><input type="checkbox" class="form-check-input" name="checkOwner[]" value="${userItem.userName}" <c:if test="${not empty ownerType and fn:contains(ownerType, \"'\".concat(userItem.userName).concat(\"'\"))}">checked="checked"</c:if>><c:out value="${userItem.userName}"/></li>
																</c:forEach>
															</ul>
														</div>
													</div>		
												</div>	
											</div>			
			                                		
			                                <input type="hidden" id="filterProcessType"		class ="processType"	name="processType" 	value=""/>  
			                                <input type="hidden" id="filterStatusType" 		class ="statusType" 	name="statusType" 	value=""/>  
			                                <input type="hidden" id="filterOwnerType" 		class ="ownerType" 		name="ownerType" 		value=""/>  
									      	<input type="hidden" id="cmd" 					name="cmd" 					value="searchTaskManager">
								      		<div style="text-align: right;"> 
							        			<button id ="btnCloseFilter" type="button" class="btn btn-default" data-toggle="collapse" data-target="#refine-result">Cancel</button>
												<a id ="btnConfirmFilter"class="btn btn-primary" onclick="getTask()">Confirm</a>
						        			</div>
									      		
										</form>	
												                                                        
									</div>                                                               
	    							                                                                    
								       <!--END FILTER MERCHANT-->                            	
		                                <div class="portlet-body">
		                                    <div style="margin-top:10px;">
		                                        <table id="taskContent" class="table table-striped">
		                                            <thead>
		                                                <tr class="heading">
		                                                	<th width="5%" style="text-align:center;"> Id </th>
		                                                	<th width="8%" style="text-align:center;"> MerchantId </th>
		                                                	<th width="15%"  style="text-align:center;"> Issue </th>
		                                                	<th width="10%"  style="text-align:center;"> Create Date </th>
		                                                	<th width="10%"  style="text-align:center;"> Due Date </th>
		                                                	<th width="10%" style="text-align:center;"> 
		                                                		<c:choose>
		                                                			<c:when test="${not empty processType and pz:isContainBracket(processType) }">		<c:set var ="ByFilter" value=""/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'checkExceedProduct'}">		<c:set var ="typeCheckExceedProduct"	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'checkDuplicateProduct'}">	<c:set var ="typeCheckDuplicateProduct"	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'removeProductPermanent '}"><c:set var ="typeRemoveProductPermanent "	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'addAndRemoveProduct'}">	<c:set var ="typeAddAndRemoveProduct" 	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'checkExpireProduct'}">		<c:set var ="typeCheckExpireProduct" 	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'changeDetailCrawlingData'}"><c:set var ="typeChangeDetailCrawlingData" 	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'newMerchant'}">			<c:set var ="typeNewMerchant"	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'checkCrawlerWeb'}">		<c:set var ="typeCheckCrawlerWeb" 	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'checkCrawlerFeed'}">		<c:set var ="typeCheckCrawlerFeed"	value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty processType && processType == 'others'}">					<c:set var ="typeCheckOthers"	value="${selected='selected'}"/></c:when>
								                            	 	<c:otherwise><c:set var ="typeAll" value="${selected='selected'}"/></c:otherwise>   
					                            	 			</c:choose>
																<select name="inputType" id="inputType" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																	<c:if test="${not empty processType && pz:isContainBracket(processType) }">
																		<option value="" ${ByFilter} >By Filter</option>
																	</c:if>
																
																	<option value="" ${typeAll}>ประเภท  Issue ทั้งหมด</option>
																	<option value="checkExceedProduct"  		${typeCheckExceedProduct}>เช็คสินค้า exceed</option>
																	<option value="checkDuplicateProduct"  		${typeCheckDuplicateProduct}>เช็็คสืนค้าซ้ำ</option>
																	<option value="removeProductPermanent " 	${typeRemoveProductPermanent}>ลบสินค้าออกถาวร</option>
																	<option value="addAndRemoveProduct" 		${typeAddAndRemoveProduct}>ลบทิ้งดึงใหม่</option>
																	<option value="checkExpireProduct" 			${typeCheckExpireProduct}>เช็คสินค้้าหมด</option>
																	<option value="changeDetailCrawlingData" 	${typeChangeDetailCrawlingDat}>เปลี่ยนรายละเอียดการดึง</option>
																	<option value="newMerchant" 				${typeNewMerchant}>ทำร้านค้าใหม่</option>
																	<option value="checkCrawlerWeb" 			${typeCheckCrawlerWeb}>ตรวจสอบ การดึงร้านจากหน้า web</option>
																	<option value="checkCrawlerFeed" 			${typeCheckCrawlerFeed}>ตรวจสอบ การดึงร้านจาก Feed</option>
																	<option value="others" 						${typeCheckOthers}>อื่นๆ</option>
														
																</select>	
		                                                	</th>
		                                                    <th width="5%"  style="text-align:center;"> 
		                                                    	<c:choose>
		                                                    		<c:when test="${not empty statusType and pz:isContainBracket(statusType) }"><c:set var ="ByFilter" value=""/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'WAITING'}"><c:set var ="typeWaiting" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'DOING'}"><c:set var ="typeDoing" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'REOPEN'}"><c:set var ="typeReOpen" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'HOLD'}"><c:set var ="typeHold" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'DONE'}"><c:set var ="typeDone" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'AUTOREPLY'}"><c:set var ="typeAutoReply" value="${selected='selected'}"/></c:when>
								                            	 	<c:when test="${not empty statusType && statusType == 'RESOLVE'}"><c:set var ="typeRESOLVE" value="${selected='selected'}"/></c:when>
								                            	 	<c:otherwise><c:set var ="typeAll" value="${selected='selected'}"/></c:otherwise>   
					                            	 			</c:choose>
																<select name="inputStatus" id="inputStatus" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																	<c:if test="${not empty statusType && pz:isContainBracket(statusType) }">
																		<option value="" ${ByFilter}>By Filter</option>
																	</c:if>
																	<option value="" ${typeAll}>Status(All)</option>
																	<option value="WAITING" ${typeWaiting}>WAITING</option>
															  		<option value="DOING" ${typeDoing}>DOING</option>
															  		<option value="REOPEN" ${typeReOpen}>REOPEN</option>
															  		<option value="HOLD" ${typeHold}>HOLD</option>
															  		<option value="DONE" ${typeDone}>DONE</option>
															  		<option value="AUTOREPLY" ${typeAutoReply}>AUTOREPLY</option>
															  		<option value="RESOLVE" ${typeRESOLVE}>RESOLVE</option>
																</select>	
		                                                    </th>
		                                                    <th width="8%"  style="text-align:center;"> 
															<select id="inputOwner" name="inputOwner" class="form-control" style="padding: 2px 10px;text-align-last: center;width:100%;">
																<c:if test="${not empty ownerType && pz:isContainBracket(ownerType) }">
																		<option value="" ${ByFilter}>By Filter</option>
																</c:if>
																<option value="" <c:if test="${empty ownerType}">selected="selected"</c:if>>Owner(All)</option>
																<c:forEach items="${userList}" var="userItem">
																	<option value="${userItem.userName}" <c:if test="${not empty ownerType && ownerType == userItem.userName}">selected="selected"</c:if>>${userItem.userName}</option>
																</c:forEach>
	                                                		</select>
															</th>
		                                                    <th width="8%"  style="text-align:center;"> Updated By </th>
		                                                    <th width="5%"  style="text-align:center;"> Last Result </th>
		                                                    <th width="5%"  style="text-align:center;"> Info </th>
		                                                    
		                                               	</tr>        
		                                            </thead>  	
		                                            <tbody id="taskBoxCotainer" style="padding-top:35px">
														<tr>
														<td >
														</td>
															<td align="center" colspan="12" style="padding-top:40px">
																<div id="ajaxLoading">
																	
																</div>
															</td>
														<td>
														</td>
														</tr>
		                                            </tbody>    	 
		                                        </table>	
		                                    </div>
		                                </div>
		                            </div>	
		                        </div>
 
								<!--  paging -->
						        <div class="row" style="float: right;">
						        <input type="hidden" value="" id="currentCountry"/>
						        <input type="hidden" value="" id="currentPage"/>
						        <input type="hidden" value="${BaseConfig.BOT_SERVER_CURRENT_COUNTRY}" id="currentServer">
						        <input type="hidden" value="" id="service-url-th"/>
						        <input type="hidden" value="" id="service-url-id"/>
						        <input type="hidden" value="" id="service-url-ph"/>
						        <input type="hidden" value="" id="service-url-my"/>
						        <input type="hidden" value="" id="service-url-sg"/>
						        <input type="hidden" value="" id="service-url-vn"/>
						        <div id="pagingDiv" class="col-md-12">
						               
							       	</div>
							    </div>
						    </div>
					     <!--  End paging --> 
		                 <!-- END Task Monitor TABLE -->    	
		                	
		               	<!--------------------------Start Task Detail--------------------------->
					<div class="row" id="showTaskDetail">
						<div class="col-md-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<div class="col-md-12">
											<div class="col-md-10" style="margin-top: 12px; width: 100%; display: inline;">
												<h3 id="group-productName">
													<!-- <button id="buttonback" class="btn btn-default btn-lg" type="button" style="margin-right: 5px"><i class="fa fa-chevron-left"></i></button> -->
													<button id="buttonback" class="btn btn-default" type="button" style="margin-right: 5px; color: #fff; background: #d8691e"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
													<span class="txtTitle">Task History</span>
												</h3>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="portlet light">
											<div class="portlet-body" style="display:block; padding-top: 0;padding-left: 30px;">
												<div class="row" id="downloadBox">
												</div>
												<div class="row" id="detailBox">
												</div>
											</div>
										</div>
									</div>
							    </div>
							
								<div class="row">
									<div class="col-md-12">
										<div class="insert-text">
									      <span class="loading">Loading...</span>
									      <span style="display:none;" class="total-comment"></span>
									      <p>
									      </p>
										</div>
									    <h4>Comment</h4>
									    <div class="list-comments"></div>
										    <input type="hidden" id="taskId" name="taskId" value="" />
										</div>
									</div>
								</div>
								<div class="portlet-body">
									<div style="margin-top: 10px;">
										<div class="content">
										  <div class="comments">
										  <span>Comment</span>
										    <div class="editor">
										      <textarea id="commentBox" class="editor-content"></textarea>
										      <div class = "clearfix">
										      	<c:if test="${sessionScope.Permission.containsKey('all')||sessionScope.Permission.containsKey('task.taskmanager.comment.template')}">			
											      	<div class="editor-footer-template-comment">
											      		<button id="doing" 			class="btn btn-default" type="button" style="width:7pc;background: #F9E79F;border-radius: 10px;" onclick="setTemplateComment('doing');"> กำลังทำ </button>
											      		<button id="waitsync" 		class="btn btn-default" type="button" style="width:7pc;margin-left: 5px;background: #F9E79F;border-radius: 10px;" onclick="setTemplateComment('waitsync');"> สินค้ารอซิงค์ </button>
											      		<button id="syncsuccess" 	class="btn btn-default" type="button" style="width:7pc;margin-left: 5px;background: #ABEBC6;border-radius: 10px;" onclick="setTemplateComment('syncsuccess');"> สินค้าถูกซิงค์แล้ว </button>
											      		<button id="deletesuccess" 	class="btn btn-default" type="button" style="width:7pc;margin-left: 5px;background: #ABEBC6;border-radius: 10px;" onclick="setTemplateComment('deletesuccess');"> ลบสินค้าแล้ว </button>
											     		<button id="isactive" 		class="btn btn-default" type="button" style="width:7pc;margin-left: 5px;background: #ABEBC6;border-radius: 10px;" onclick="setTemplateComment('isactive');"> STATUS ACTIVE </button>
											      		<button id="statusactive" 	class="btn btn-default" type="button" style="width:9pc;margin-left: 5px;background: #ff0000;border-radius: 10px;" onclick="activeMerchant();"> Active </button>
											      		<input type="hidden" id="detailId" value=""/>
											      	</div>
											   	</c:if>
											   	<c:if test="${sessionScope.Permission.containsKey('content')||sessionScope.Permission.containsKey('task.taskmanager.comment.template')}">			
											      	<div class="editor-footer-template-comment">
											      		<button id="checktaskagain" 			class="btn btn-default" type="button" style="width:11pc;background: #F9E79F;border-radius: 10px;" onclick="setTemplateComment('checktaskagain');">พบปัญหา ตรวจสอบอีกรอบ </button>
											      		<button id="done" 						class="btn btn-default" type="button" style="width:9pc;margin-left: 5px;background: #ABEBC6;border-radius: 10px;" onclick="setTemplateComment('done');"> OK. ปิด tasks </button>
											      	</div>
											   	</c:if>
											      <div class="editor-footer">
											      	  <div style="margin-right:20px;margin-top:8px"><b>Status:</b> </div> 
												      <select id="commentStatus" class="form-control ">
												  		<option value="WAITING">WAITING</option>
												  		<option value="DOING">DOING</option>
												  		<option value="REOPEN">REOPEN</option>
												  		<option value="HOLD">HOLD</option>
												  		<option value="DONE">DONE</option>
												  		<option value="RESOLVE">RESOLVE</option>
													  </select>
													  <button id="commentSubmit" class="btn btn-default" type="button" style="width:10pc;margin-left: 10px; color: #000; background: #69DCFE;">  Comment </button>
											      </div>
										      </div>
										    </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  start paging -->
						<div class="row" style="float: right; padding-bottom: 20px;">
							<div class="col-md-12">
								<div class="dataTables_paginate paging_bootstrap">
									<ul class='paginationMerchant pagination' style='margin-right: 0px;'></ul>
								</div>
							</div>
						</div>
						<!--  end paging -->
					</div>
					<!--end detail  -->
		                	<!--START MODAL -->
							<div class="row">
			       			<div id="addForm" class="modal fade back-drop" >
								<div class="modal-dialog modal-xs">
								 	<div class="modal-content scroll_model">
								      	<div class="modal-header">
								        	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
								        	<h4 class="modal-title" id="addFormLabel"><b>Task Manager</b></h4>
								      	</div>
								      	<form id="addTaskForm" action="" method="post" enctype="multipart/form-data" accept-charset="utf-8">
								      		<div class="modal-body">
												<div style="margin-left: 30px; margin-right: 30px;">
				
	                                        		
	                                        			<div class="row">
														<div class="form-group">														
			                                            	<div class="row">
																<div class="col-xs-9">
			                                            			<label class="control-label" for="issue">หัวข้อ Issue</label>&nbsp;<span class="text-danger">*</span>
							                                        <div class="row" id="issueBox">
							                                        	<div class="col-xs-12">
								                                           	<input type="text" id="issue" name="issue" onblur="validateInput(this)"  class="form-control" >
							                                       		</div>
							                                       	</div>
																</div>
																
																<div class="col-xs-3">
																	<label class="control-label" for="merchantId">Merchant ID</label>&nbsp;<span class="text-danger">*</span>
						                                            <div class="row" id="merchantIdBox">
							                                            <div class="col-xs-12">
							                                            	<input type="text" id="merchantId" name="merchantId" onblur="validateInput(this)" class="form-control" placeholder="Ex. 300001">
						                                        		</div>
					                                        		</div>
																</div>
                			
			                                        		</div>
		                                        		</div>
													</div>
	                                        		
	 
													<div class="row">
														<div class="form-group">														
			                                            	<div class="row">
																<div class="col-xs-6">
			                                            			<label>ประเภท Issue</label>
			                                            			<div class="row">
																		<div class="col-md-12">
																			<select id="actionType" name="actionType" class="form-control">
																				<option value="checkExceedProduct">เช็คสินค้า exceed</option>
																				<option value="checkDuplicateProduct">เช็คสินค้าซ้ำ</option>
																				<option value="removeProductPermanent">ลบสินค้าออกถาวร</option>
																				<option value="addAndRemoveProduct">ลบทิ้งดึงใหม่</option>
																				<option value="checkExpireProduct">เช็คสินค้าหมด</option>
																				<option value="changeDetailCrawlingData">เปลี่ยนรายละเอียดการดึง</option>
																				<option value="newMerchant">ทำร้านค้าใหม่</option>
																				<option value="checkCrawlerWeb">ตรวจสอบ การดึงร้านจากหน้า web</option>
																				<option value="checkCrawlerFeed">ตรวจสอบ การดึงร้านจาก Feed</option>
																				<option value="others">อื่นๆ</option>
																			</select>
																		</div>
																	</div>
																</div>
																
																<div class="col-xs-3">
																<label>วันกำหนดส่ง </label>
																	<div class="row">
																		<div class="form-group">
								                                            <div class="row" id="duedateBox">
									                                            <div class="col-xs-6" style="width: 96%;">
																	                    <input type="date" id="duedate" name="duedate" onblur="validateInput(this)"  pattern="\d{1,2}/\d{1,2}/\d{4}" class="form-control">
								                                        		</div>
							                                        		</div>
							                                        	</div>
					                                        		</div>
																</div>
																
																
																<div class="col-xs-3">
																<label>ประเภท ร้านค้า</label>
			                                            			<div class="row">
																		<div class="col-md-12">
																			<select id="merchantType" name="merchantType" class="form-control">
																				<option value="">auto</option>
																				<option value="test">test</option>
																				<option value="customer">customer</option>
																			</select>
																		</div>
																	</div>
																</div>
			                                        		</div>
		                                        		</div>
													</div>
													
													<div class="row">
														<div class="form-group">
					                                           <label class="control-label" for="productURLs">Link สิ้นค้าที่มีปัญหา <a href="#" id="productURLTooltip" data-toggle="tooltip" data-placement="top" title="link สิ้นค้าที่ไม่ถูกดึงเข้ามา "><i class="fa fa-exclamation-circle"></i></a></label>&nbsp;<span class="text-danger">*</span>
					                                           <div id ="productURLBox" style="margin-bottom:5px;">
						                                           <div class="row" style="margin-top:5px;">
								                                        <div class="col-xs-10">
								                                           	<input type="text" name="productURLs" onblur="validateInput(this)" class="productURLs form-control">
							                                        	</div>
							                                        	<button type="button" class="removeURL btn btn-md btn-danger" onclick="removeURL(this)">Remove</button>
					                                        		</div>
				                                        	   </div>
	                                        				   <button type="button"  class="addURL btn btn-primary" style="float: right;" onclick="addNewURL()">Add URL</button>
				                                        </div>
		                                        	</div>
												
	                                        		<div class="row">
														<div class="form-group">														
			                                            	<div class="row">
																<div class="col-xs-6">
																	<label class="control-label" for="feedUsername">Feed Username</label>
						                                            <div class="row" id="feedUsernameBox">
							                                            <div class="col-xs-12">
							                                            	<input type="text" id="feedUsername" name="feedUsername" class="form-control" placeholder="Ex. 4114763">
						                                        		</div>
					                                        		</div>
					                                        	</div>
					                                        	
					                                        	<div class="col-xs-6">
			                                            			 <label class="control-label" for="feedPassword">Feed Password</label>
						                                            <div class="row" id="feedPasswordBox">
							                                            <div class="col-xs-6" style="width: 96%;">
							                                            	<input type="text" id="feedPassword" name="feedPassword" class="form-control" placeholder="Ex. zaBr-HkJ">
						                                        		</div>
					                                        		</div>
			                                        			</div>	
															</div>
		
			                                        	</div>
		                                        	</div>		
		                                        	
		                                        	<div class="row">
														<div class="form-group">
					                                           <label class="control-label" for="productFiles">Import File </label>
					                                           <div id ="productFileBox" style="margin-bottom:5px;">
						                                           <div class="row" style="margin-top:5px;">
								                                        <div class="col-xs-10">
								                                           	<input type="file" name="productFiles" onblur="validateInput(this)" class="productFiles form-control">
							                                        	</div>
							                                        	<button type="button" class="removeFile btn btn-md btn-danger" onclick="removeFile(this)">Remove</button>
					                                        		</div>
				                                        	   </div>
	                                        				   <button type="button"  class="addFile btn btn-primary" style="float: right;" onclick="addNewFile()">Add File</button>
				                                        </div>
		                                        	</div>
		                                        	
		                                        	<div class="row">
														<div class="form-group">
														<div class="row" id="expectDataBox">
															<div class="col-xs-6">
					                                            <label class="control-label" for="actualData">ข้อมูลที่ผิดพลาด <a href="#" data-toggle="tooltip" data-placement="top" title="ข้อมูลที่แสดงบนหน้าเว็บ priceza ในขณะนี้"><i class="fa fa-exclamation-circle"></i></a></label>
						                                        <textarea rows="2"  id="actualData"  onblur="validateInput(this)" name="actualData" class="form-control"></textarea>
			                                        		</div>
															<div class="col-xs-6">
					                                            <label class="control-label" for="expectData">ข้อมูลที่ต้องการให้เป็น <a href="#" data-toggle="tooltip" data-placement="top" title="ข้อมูลที่ถูกต้องจากเว็บลูกค้า"><i class="fa fa-exclamation-circle"></i></a></label>
						                                        <textarea rows="2"  id="expectData"  onblur="validateInput(this)" name="expectData" class="form-control"></textarea>
			                                        		</div>
			                                        	</div>
			                                        	</div>
	                                        		</div>	                                        	
	                                        		
		                                        	<div class="row">
														<div class="form-group">
				                                            <label class="control-label" for="detail">ข้อมูลเพิ่มเติม </label>
				                                            <div class="row" id="detailBox">
					                                            <div class="col-xs-12">
					                                            	<textarea rows="3"  id="detail"  onblur="validateInput(this)" name="detail" class="form-control"></textarea>
				                                        		</div>
			                                        		</div>
			                                        	</div>
	                                        		</div>
	                                             	
	                                        	</div>								
								      		</div>
									      	<div class="modal-footer">
									      		<input type="hidden" id="cmd" name="cmd" value="addNewTask">
											   	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											   	<button type="button" id="taskFormReset" class="btn btn-primary">Clear</button>
												<button type="button" id ="submitbtn"class="btn btn-primary">Submit</button>
									    	</div>
										</form>
								 	</div>
								</div>
							</div>
			       			</div>
							<!-- END  MODAl -->
							<!-- CONFIRM  MODAl -->
							<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							    <div class="modal-dialog">
							        <div class="modal-content">
							            <div class="modal-header">
							            	<button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
							                <h4 class="modal-title" id="addFormLabel"><b>Comment Manager</b></h4>
							            </div>
							            <div class="modal-body">
							                Are you sure to delete ?
							            </div>
							            <input type="hidden" id="commentId" value="" />
							            <div class="modal-footer">
							                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							                <button type="button" id ="confirmbtn" class="btn btn-primary">Confirm</button>
							                
							            </div>
							        </div>
							    </div>
							</div>
		            <!-- END CONTENT -->
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="contextPath" name="contextPath" value="${contextPath}">
	<script type="text/javascript" src="${contextPath}/assets/pages/js/task-manager-script.js"></script>

</body>
</html>