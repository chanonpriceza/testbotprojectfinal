<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% 
	String contextPath = request.getContextPath(); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>	
	<title>Bot Backend : Manual Update Product</title>
	<jsp:include page = "/WEB-INF/jsp/template/common-style.jsp"/>
	<jsp:include page = "/WEB-INF/jsp/template/common-script.jsp"/>	
	<style type="text/css">
		#merchantName > span{
			border: 1px solid #cccccc;
			margin-left: 20px;
			padding: 9px 14px 8px;
			font-size: 14px;
			font-weight: 600;
			border-radius: 2px;
		}
		#merchantName > span > span:not(:first-child){
			margin-left: 20px;
		}
		#btnSearch{
			color: darksategray;
			font-size: 18px;
			background-color: #3399ff;
			height: 34px;
		}
		#overview-header{
			font-size: 1.3em;
		}
		#overview-order li{
		    line-height: 40px;
			font-size: 1.3em;
		}
		.label-field{
			margin-top: 9px;
			font-weight: 600;
			white-space: nowrap;
		}
		.orangeButton{
			color: #fff;
			background-color: #ffb266;
			border-color : #4cae4c;
			min-width: 85px;
		}
		
		.darkGreenButton{
			color: #fff;
			background-color: #00E09C;
			border-color : #007C56;
			min-width: 85px;
		}
	</style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"> </div>
		<div class="page-container">
	       <!-- BEGIN SIDEBAR -->
	       <div class="wrapper">
			<!-- Left side column. contains the logo and sidebar -->
			<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
			<!-- BEGIN CONTENT -->
	   		<div class="page-content-wrapper">
	        <!-- BEGIN CONTENT BODY -->
	        <div class="page-content">
	            <!-- BEGIN PAGE HEADER-->
	            <!-- BEGIN PAGE BAR -->
	            <div class="page-bar" style="margin-bottom: 2%;">
	            	<div class="col-md-9">
	         			<h3 class="page-title" style="white-space: nowrap;"><i class="fa fa-wrench" style="padding-right: 10px;"></i> Manual Update Product<span id="merchantName"></span></h3>
	        		</div>
	        		<div class="col-md-1">
	        		</div>
		            <ul class="page-breadcrumb">
		            	<li><a href="dashboard">Dashboard</a> > <a href="#">Product</a> > <a href="#">Update Product</a> <i class="fa fa-circle"></i></li>
		            </ul>
	        	</div>	
				<div class="row">
					<div class="col-md-12">
						<div class="box-tools">
							<div class="form-group">
								<div class="input-group">
									<input type="text" id="merchantId" class="form-control" placeholder="Fill Merchant ID">
									<div class="input-group-btn">
										<button type="button" class="btn btn-sm btn-default" id="btnSearch" onclick="search();">
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" onclick="toggleShow(this)">HIDE</button>
									<span id="overview-header">&nbsp;Step&nbsp;overview</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<ol id="overview-order">
								</ol>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="productUrlBox" class="portlet light bordered" style="display:none;">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" id="toggleProductUrlBoxBtn" onclick="toggleShow(this)">HIDE</button>
									<span>&nbsp;Update&nbsp;:&nbsp;Product&nbsp;by&nbsp;url&nbsp;
										<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="ทำการกรอกลิงค์สินค้าที่ต้องการอัพเดต"></c:out>"></a>
									</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<div id="productUrlData">
									<form>
										<div class="row">
											<div class="col-md-12" id="productUrlDataArea">
											</div>
										</div>
									</form>
								</div>
								<div class="row" style="text-align: right; margin-right: 0px;">
									<span id="productUrlAlertText"></span>
									<!--<button type='button' class='btn darkGreenButton' id="addUrlPredict" onclick='addUrlProduct();'>Add Url</button>-->
									<button type='button' class='btn orangeButton' id="startAddProduct" onclick='startAddProduct();' data-loading-text="<i class='fa fa-spinner fa-spin '></i> Parsing">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div id="sampleProductBox" class="portlet light bordered" style="display:none;">
							<div class="portlet-title">
								<h4 id="group-titleName">
									<button type="button" class="btn btn-sm btn-default" id="toggleSampleProductBoxBtn" onclick="toggleShow(this)">HIDE</button>
									<span>&nbsp;Update&nbsp;:&nbsp;Sample&nbsp;product&nbsp;
										<a class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='right' style="text-decoration:none;" title="<c:out value="ตัวอย่างรูปแบบการดึงสินค้าที่ดึงได้"></c:out>"></a>
									</span>
								</h4>
							</div>
							<div class="portlet-body" style="display:block; padding-top: 0;">
								<div id="sampleProductData">
									<form>
										<div class="row">
										<!--  <input type="checkbox" id = "checkAll" style='width:20px;height:20px;'>-->
											<div class="col-md-12" id="sampleProductDataArea">
											</div>
										</div>
									</form>
								</div>
								<div class="row" style="text-align: right; margin-right: 0px;">
									<span id="sampleProductAlertText"></span>
									<button type='button' class='btn orangeButton' id="confirmUpdateProduct" onclick='confirmUpdateProduct();' data-loading-text="<i class='fa fa-spinner fa-spin '></i> Adding">Confirm</button>
								</div>
							</div>
						</div>
					</div>
				</div>
	
	   			<div class="modal fade" id="modalAlert">
	   				<div class="modal-dialog" style="width: 40%;">
	   					<div class="modal-content">
	        				<div class="modal-header">
	          					<button type="button" class="close" data-dismiss="modal">&times;</button>
	          					<h4 class="modal-title" style="color:#FFC300;" >Alert</h4>
	        				</div>
	       					<div class="modal-body" id="modalAlertData"></div>
	    					<div class="modal-footer">
	          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        				</div>
	   					</div>
	   				</div>
	   			</div>
	   			
	    	</div>
	    </div>
	    <!-- END CONTENT -->
		</div>
	</div>
	<script type="text/JavaScript" src="<%=contextPath %>/assets/pages/js/manual-update-product-script.js"></script>
</body>
</html>