<%@ page import="botbackend.bean.UserBean, botbackend.utils.*, botbackend.system.BaseConfig,utils.DateTimeUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%	String contextPath = request.getContextPath();	%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>

<title>Bot Backend</title>

<jsp:include page="/WEB-INF/jsp/template/common-style.jsp" />
<jsp:include page="/WEB-INF/jsp/template/common-script.jsp" />

</head>
<style>
</style>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	
	<jsp:include page = "/WEB-INF/jsp/template/header.jsp"/>
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
       	<!-- BEGIN CONTAINER -->
    	<div class="page-container">
           	<!-- BEGIN SIDEBAR -->
           	<div class="wrapper">
   				<!-- Left side column. contains the logo and sidebar -->
				<jsp:include page = "/WEB-INF/jsp/template/menuside.jsp"/>
			
				<!-- BEGIN CONTENT -->
	            <div class="page-content-wrapper">
	                <!-- BEGIN CONTENT BODY -->
	                <div class="page-content">
		                <!-- BEGIN PAGE HEADER-->
		                <!-- BEGIN PAGE BAR -->
						<div class="page-bar" style="margin-bottom:2%;">
							<div class="col-md-6">
								<h3 class="page-title">
									<i class="fa fa-desktop" style="padding-right: 10px;"></i> Manual Product Manager
								</h3>
							</div>
							<ul class="page-breadcrumb">
								<li><a href="home">Home</a> > <a href="#"> Manual Product Manager</a> <i class="fa fa-circle"></i></li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
					
						<!-- START CONTENT -->
						<!-- START Focus Update TABLE -->
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light bordered">
									<div class="portlet-title">
										<h4 id="group-titleName">
											<span>Current product list <span id="currentAllCount">${manualProductSize }</span> record</span>
										</h4>
									</div>
									<div class="portlet-body">
										<table id="urlPatternTable" class="table table-striped table-bordered">
											<thead>
                                                <tr class="heading">
                                                	<th width='10%'  style="text-align:center;"> ID </th>
                                                	<th width='15%' style="text-align:center;"> MerchantId </th>
                                                    <th width='30%' style="text-align:center;"> ProductName </th>
                                                    <th width='15%' style="text-align:center;"> Add By </th>
                                                    <th width='15%' style="text-align:center;"> Add Date </th>
                                                    <th width='15%' style="text-align:center;"> Action </th>
                                               	</tr>
                                            </thead>
                                            <tbody>
                                            	<c:forEach items="${manualProductList}" var="manualProductItem">
                                            		<tr class="recordRow">
                                            			<td style='text-align: center;' class='id' id='id-${manualProductItem.id}'>${manualProductItem.id}</td>
                                            			<td style='text-align: center;' class='merchantId' id='merchantId-${manualProductItem.id}'>${manualProductItem.merchantId}</td>
                                            			<td style='text-align: center;' class='productName' id='productName-${manualProductItem.id}'>${manualProductItem.productName}</td>
                                            			<td style='text-align: center;' class='addBy' id='addBy-${manualProductItem.id}'>${manualProductItem.addBy}</td>
                                            			<td style='text-align: center;' class='addDate' id='addDate-${manualProductItem.id}'>${DateTimeUtil.generateStringDisplayDateTime(manualProductItem.addDate)}</td>
                                            			<td style='text-align: center;' class='recordAction'>
                                            				<button type="button" class="deleteItemBtn btn btn-danger" onclick="deleteItem(this)" data-loading-text=" <i class='fa fa-spinner fa-spin '></i>  "><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            			</td>
                                            		</tr> 
                                            	</c:forEach>
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
						
						<div class="modal fade" id="modalAlert">
	    					<div class="modal-dialog" style="width: 30%;">
	    						<div class="modal-content">
		        					<div class="modal-header">
		          						<button type="button" class="close" data-dismiss="modal">&times;</button>
		          						<h4 class="modal-title" style="color:#FFC300;" >Alert</h4>
		        					</div>
		       						<div class="modal-body" id="modalAlertData"></div>
		    						<div class="modal-footer">
		          						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        					</div>
	    						</div>
	    					</div>
	    				</div>
	    				
						<!-- END Focus Update TABLE -->
						<!-- END CONTENT -->
					</div>
				</div>
			</div>
		</div>



	<script type="text/javascript" src="<%=contextPath%>/assets/pages/js/manual-product-manager-script.js"></script>

</body>
</html>