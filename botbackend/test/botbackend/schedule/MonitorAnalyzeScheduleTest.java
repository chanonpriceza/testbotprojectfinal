package botbackend.schedule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import botbackend.bean.MonitorBean;
import botbackend.bean.ProductParserBean;
import botbackend.bean.common.WorkLoadFeedCommonBean;

public class MonitorAnalyzeScheduleTest {

	private MonitorAnalyzeSchedule schedule;
	final String path = System.getProperty("user.dir");
	private String backend_properties_path = path + "/WebContent/assets/test/config/priceza-bot-backend.properties";
	private String server_properties_path = path + "/WebContent/assets/test/config/priceza-bot-server.properties";


//	@Before
//	public void setUp() throws Exception {
//		schedule = new MonitorAnalyzeSchedule(new String[]{ backend_properties_path, server_properties_path});
//		schedule.initailize();
//	}
//
//	@After
//	public void tearDown() throws Exception {
//	}
//
//
//	@Test
//	public void constructor_backendConfiguration_null() {
//		try {
//			new MonitorAnalyzeSchedule(new String[]{null});
//		} catch (ConfigurationException e) {
//			assertEquals("<bot-backend-configuration>", e.getMessage());
//		}
//	}
//	
//	@Test
//	public void constructor_backendConfiguration_space() {
//		try {
//			new MonitorAnalyzeSchedule(new String[]{" "});
//		} catch (ConfigurationException e) {
//			assertEquals("<bot-backend-configuration>", e.getMessage());
//		}
//	}
//	
//	@Test
//	public void constructor_success() throws ConfigurationException {
//		
//		MonitorAnalyzeSchedule _schedule = new MonitorAnalyzeSchedule(new String[]{ backend_properties_path, server_properties_path});
//		Configuration conf = _schedule.getPropConfig().getConfiguration();
//		assertNotEquals(null, conf.getKeys());
//		assertEquals(false, conf.isEmpty());		
//	}
//	
//	@Test
//	public void setMapSSH() throws ConfigurationException {
//		
//		schedule.setMapSSH("");
//		
//		assertNotEquals(null, schedule.getDBTestSSHUrlMap());
//		assertNotEquals(false, schedule.getDBTestSSHUrlMap().isEmpty());
//		
//		assertNotEquals(null, schedule.getDBTestSSHPortMap());
//		assertNotEquals(false, schedule.getDBTestSSHPortMap().isEmpty());
//		
//		assertNotEquals(null, schedule.getDBTestSSHPortMap());
//		assertNotEquals(false, schedule.getDBTestSSHPortMap().isEmpty());
//	}
//	@Test
//	public void groupMerchant() throws ConfigurationException {
//
//		String[][] data = {{"90261",	"Xspeed",	"0", "CRAWLER_START", "WAITING", "DATA_UPDATE_START", "WAITING", "3", "0", "0", null, null, "0", "9", null},
//				{"90262", "sktbike", "1", "PARSER_FINISH", "RUNNING", "DATA_UPDATE_FINISH", "RUNNING", "3", "0", "40", "Bot13-bot", "Parser ราคาผิด", "1", "9", "Bot13-bot"},
//				{"90262", "sktbike", "1", "PARSER_FINISH", "RUNNING", "DATA_UPDATE_FINISH", "RUNNING", "3", "0", "40", "Bot13-bot", "Parser ราคาผิด", "1", "9", "Bot12-bot"},
//				{"220002","AURA GOLD by 11street", "0",	"CRAWLER_START", "WAITING",	"DATA_UPDATE_START", "WAITING",	"3", "0", "0", "Bot12-bot", "exceed delete limit = 2222", "0",	"0", "Bot12-bot"},
//				{"220002","AURA GOLD by 11street", "0",	"CRAWLER_START", "WAITING",	"DATA_UPDATE_START", "WAITING",	"3", "0", "0", "Bot12-bot", "exceed delete limit = 2222", "0",	"0", "Bot13-bot"},
//				{"220003","BTG GADGET by 11street", "0",	"CRAWLER_START", "WAITING",	"DATA_UPDATE_START", "WAITING",	"3", "0", "0", "Bot12-bot", null,	"0", "0", "Bot12-bot"},
//				{"220003", "THAIINK",	"2", "PARSER_FINISH",	"RUNNING", "DATA_UPDATE_FINISH", "RUNNING",	"15", "5", "135",	"Bot13-bot", "exceed delete limit = 200", "1", "7", "Bot13-bot"},
//				{"220003", "THAIINK",	"2", "PARSER_FINISH",	"RUNNING", "DATA_UPDATE_FINISH", "RUNNING",	"15", "5", "135",	"Bot13-bot", "exceed delete limit = 200", "1", "7", "Bot12-bot"},
//				{"220003", "S-eStore",	"2", "PARSER_FINISH",	"RUNNING", "DATA_UPDATE_FINISH", "RUNNING",	"3", "0", "0",	"Bot12-bot", "exceed delete limit = 25", "0", "0", "Bot12-bot"}};
//		
//		String[] server = {"Bot13-bot", "Bot12-bot"};
//		List<MerchantBean> merchantList = new ArrayList<MerchantBean>();
//		for(int i=0 ;i<data.length;i++){
//			
//				MerchantBean tmp = new MerchantBean();
//				tmp.setId(Integer.parseInt(data[i][0]));
//				tmp.setName(data[i][1]);
//				tmp.setActive(Integer.parseInt(data[i][2]));
//				tmp.setState(data[i][3]);
//				tmp.setStatus(data[i][4]);
//				tmp.setDataUpdateState(data[i][5]);
//				tmp.setDataUpdateStatus(data[i][6]);
//				tmp.setPackageType(Integer.parseInt(data[i][7]));
//				tmp.setDueErrorCount(Integer.parseInt(data[i][8]));
//				tmp.setWceErrorCount(Integer.parseInt(data[i][9]));
//				tmp.setErrorMessage(data[i][11]);
//				tmp.setServer(data[i][10]);
//				tmp.setDataServer(data[i][14]);
//				tmp.setNodeTest(Integer.parseInt(data[i][12]));
//				tmp.setOwnerId(Integer.parseInt(data[i][13]));
//				
//				merchantList.add(tmp);
//		}
//		
//		Map<String,Map<String,List<Integer>>> merchantGroup = schedule.groupMerchant(merchantList);
//		assertNotEquals(null, merchantGroup);
//		assertNotEquals(0, merchantGroup.size());
//		int i = 0;
//		for(Entry<String,Map<String,List<Integer>>> merchantServertMap : merchantGroup.entrySet()){
//			if(i > 1){
//				i = -1;
//				break;
//			}
//			String serverName  = merchantServertMap.getKey();
//			assertEquals(server[i], serverName);
//			Map<String,List<Integer>> dataServerList = merchantServertMap.getValue();
//			for(Entry<String, List<Integer>> dataServertMap : dataServerList.entrySet()){
//			assertNotEquals(0, dataServertMap.getValue().size());
//			}
//			i++;
//			
//		}
//		assertEquals(2, i);
//		assertNotEquals(-1, i);
//		schedule.setMapSSH();
//		
//		
//	}
//	@Test
//	public void initiateMerchant(){
//		try {
//			schedule.initiateMerchant();
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//			fail();
//		}
//	}
	
//	@Test
//	public void analyzeNewMerchant(){
//		Map<Integer, MonitorBean> monitorMap = setMonitorListMapData();
//		int[] overAll = {300012,300010,300011,300014};
//		List<Integer> overallMerchant = new ArrayList<Integer>();
//		for(int i=0;i<overAll.length;i++){
//			overallMerchant.add(overAll[i]);
//		}                                                                                                                           
//		List<Integer> oldIMerchantList = new ArrayList<Integer>();
//		List<Integer> newMerchantsList = new ArrayList<Integer>();
//		schedule.analyzeNewMerchant( monitorMap, overallMerchant, oldIMerchantList, newMerchantsList);
//		
//		assertEquals(2, oldIMerchantList.size());
//		assertEquals(1, newMerchantsList.size());
//	}
//	@Test
//	public void addWarningMerchant(){       
//
//		List<Integer> overallMerchant = new ArrayList<Integer>();
//		String[][] merchant = {{"300012","PASSED"},{"300010","WARNING"},{"300010","PASSED"},{"300014","WARNING"}};
//		List<MerchantRuntimeReportBean> reportList = new ArrayList<MerchantRuntimeReportBean>();
//		for(int i = 0; i < merchant.length ; i++){
//			MerchantRuntimeReportBean report = new MerchantRuntimeReportBean();
//			report.setMerchantId(Integer.parseInt(merchant[i][0]));
//			report.setStatus(merchant[i][1]);
//			reportList.add(report);
//		}
//
//		schedule.addWarningMerchant(reportList, overallMerchant);
//		
//		assertEquals(2, overallMerchant.size());
//		
//	}
//	@Test
//	public void getUrlData(){
//		List<String> urlList = new ArrayList<String>();
//		urlList.add("https://test1.com");
//		urlList.add("https://test2.com");
//		urlList.add("https://test3.com");
//		String [] urls = schedule.getUrlData(urlList);
//		
//		assertEquals(urlList.size(), urls.length);
//		
//	}
//	@Test
//	public void getRespondTypeData_Network(){
//		String result  = schedule.getRespondTypeData("000"); 
//		assertEquals("Network", result);
//		
//	}
//	@Test
//	public void getRespondTypeData_Success(){
//		String result  = schedule.getRespondTypeData("200"); 
//		assertEquals("Success", result);
//		
//	}
//	@Test
//	public void getRespondTypeData_Block(){
//		String result  = schedule.getRespondTypeData("403"); 
//		assertEquals("Block", result);
//		
//	}
//	@Test
//	public void getRespondTypeData_Unknown(){
//		String result  = schedule.getRespondTypeData("444"); 
//		assertEquals("Unknown", result);
//		
//	}
//	@Test
//	public void getRespondTypeData_Blank(){
//		String result  = schedule.getRespondTypeData(null); 
//		assertEquals("", result);
//		
//	}
//	@Test
//	public void getRespondTypeData_String(){
//		String result  = schedule.getRespondTypeData("Success"); 
//		assertEquals("", result);
//		
//	}
//	@Test
//	public void getRespondTypeFeed_Network(){
//		String result  = schedule.getRespondTypeFeed("000"); 
//		assertEquals("Feed_Network", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_Success(){
//		String result  = schedule.getRespondTypeFeed("200"); 
//		assertEquals("Feed_Success", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_UnAuthorize(){
//		String result  = schedule.getRespondTypeFeed("401"); 
//		assertEquals("Feed_UnAuthorize", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_Block(){
//		String result  = schedule.getRespondTypeFeed("403");  
//		assertEquals("Feed_Block", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_Network(){
//		String result  = schedule.getRespondTypeFeed("404"); 
//		assertEquals("Feed_Network", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_Unknown(){
//		String result  = schedule.getRespondTypeFeed("444"); 
//		assertEquals("Unknown", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_Blank(){
//		String result  = schedule.getRespondTypeFeed(null); 
//		assertEquals("", result);
//		
//	}
//	@Test
//	public void getResponseTypeFeed_String(){
//		String result  = schedule.getRespondTypeFeed("Unknown"); 
//		assertEquals("", result);
//		
//	}
//	@Test
//	public void countByResult(){
//		String[] data = {"Success", "Network", "Block", "Network"};
//		List<String> serverMap = new ArrayList<String>();
//		for(int i=0;i<data.length;i++){
//			serverMap.add(data[i]);
//		} 
//		Map<String, Integer> mapResult  = schedule.countByResult(serverMap);
//		assertEquals("2", mapResult.get("Network").toString());
//		assertEquals("1", mapResult.get("Success").toString());
//		assertEquals("1", mapResult.get("Block").toString());
//		
//	}
//	
//	@Test
//	public void  analyzeWebParser_Exceed_Delete_Limit(){
//		List<ProductParserBean> dataList = new ArrayList<ProductParserBean>();
//		for(int i = 0 ;i < 3 ; i++){
//			ProductParserBean data = new ProductParserBean();
//			data.setName("name"+i);
//			data.setPrice("33000.00");
//			data.setIsPrice(true);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeWebParser(dataList);	
//		assertEquals("Parser_Delete_Limit", result);
//		
//	}
//	
//	@Test
//	public void  analyzeWebParser_Parser(){
//		List<ProductParserBean> dataList = new ArrayList<ProductParserBean>();
//		for(int i = 0 ;i < 3 ; i++){
//			ProductParserBean data = new ProductParserBean();
//			data.setName("");
//			data.setIsPrice(false);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeWebParser(dataList);	
//		assertEquals("Parser", result);
//		
//	}
//
//	@Test 
//	public void analyzeWebResult(){
//		Map<String, Integer> countCurrentServer = new HashMap<String, Integer>();
//		Map<String, Integer> countTestServer1 = new HashMap<String, Integer>();
//		Map<String, Integer> countTestServer2 = new HashMap<String, Integer>();
//		
//		/* currentSever Block, others can be any status */
//		countCurrentServer.put("Block", 2);
//		
//		String result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("Block", result);
//		
//		/* currentSever Network , others success */
//		countCurrentServer.clear();		
//		countTestServer1.clear();		
//		countTestServer2.clear();
//		
//		countCurrentServer.put("Network", 2);		
//		countTestServer1.put("Success", 2);		
//		countTestServer2.put("Success", 2);
//		
//		result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("Block", result);
//		
//		/* currentSever network , others block */		
//		countCurrentServer.clear();	
//		countTestServer1.clear();		
//		countTestServer2.clear();
//		
//		countCurrentServer.put("Network", 2);
//		countTestServer1.put("Block", 2);		
//		countTestServer2.put("Block", 2);
//		
//		result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("Network", result);
//		
//		/* All Sever network */	
//		countCurrentServer.clear();		
//		countTestServer1.clear();		
//		countTestServer2.clear();
//		
//		countCurrentServer.put("Network", 2);
//		countTestServer1.put("Network", 2);		
//		countTestServer2.put("Network", 2);
//		
//		result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("Network", result);
//		
//		/* countTestServer1 network , countTestServer2 Success */	
//		countCurrentServer.clear();		
//		countTestServer1.clear();		
//		countTestServer2.clear();
//		
//		countTestServer1.put("Network", 2);		
//		countTestServer2.put("Success", 2);
//		countTestServer2.put("Network", 2);
//		
//		result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("Network", result);
//		
//		/* ---------------------------------------*/
//		countCurrentServer.clear();		
//		countTestServer1.clear();		
//		countTestServer2.clear();
//		
//		countCurrentServer.put("Network", 2);
//		countTestServer1.put("Block", 2);		
//		countTestServer2.put("Success", 2);
//		
//		result = schedule.analyzeWebResult(countCurrentServer, countTestServer1, countTestServer2);
//		assertEquals("", result);
//
//	}
//	
//	@Test 
//	public void analyzeFeedResult(){
//		String currentServerMap = "";
//		String testServer1Map = "";
//		String testServer2Map = "";
//		
//		/* currentSever Block, others can be any status */
//		currentServerMap = "Feed_Block";
//		String result = schedule.analyzeFeedResult(currentServerMap, testServer1Map, testServer2Map);
//		assertEquals("Feed_Block", result);
//		
//		/* currentSever Network, others success */
//		currentServerMap = "Feed_Network";	
//		testServer1Map = "Feed_Success";		
//		testServer2Map = "Feed_Success";
//		
//		result = schedule.analyzeFeedResult(currentServerMap, testServer1Map, testServer2Map);
//		assertEquals("Feed_Block", result);
//		
//		/* All UnAuthorize */		
//		currentServerMap = "Feed_UnAuthorize";
//		testServer1Map = "Feed_UnAuthorize";		
//		testServer2Map = "Feed_UnAuthorize";
//		
//		result = schedule.analyzeFeedResult(currentServerMap, testServer1Map, testServer2Map);
//		assertEquals("Feed_UnAuthorize", result);
//		
//		/*--------------------------------*/	
//		currentServerMap = "Feed_Network";
//		testServer1Map = "Feed_UnAuthorize";		
//		testServer2Map = "Feed_Success";
//		
//		result = schedule.analyzeFeedResult(currentServerMap, testServer1Map, testServer2Map);
//		assertEquals("Feed_Network", result);
//		
//		/*All network */			
//		currentServerMap = "Feed_Network";		
//		testServer1Map = "Feed_Network";	
//		testServer2Map = "Feed_Network";	
//		
//		result = schedule.analyzeFeedResult(currentServerMap, testServer1Map, testServer2Map);
//		assertEquals("Feed_Network", result);
//		
//	}
//	
//	@Test
//	public void  analyzeFeedParser_Exceed_Delete_Limit(){
//		List<WorkLoadFeedCommonBean> dataList = new ArrayList<WorkLoadFeedCommonBean>();
//		String url = "http://www.priceza.com/";
//		for(int i = 0 ;i < 3 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("name"+i);
//			data.setOld_name("name"+i);
//			data.setUrl(url);
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Delete_Limit", result);
//		
//	}
//	
//	@Test
//	public void  analyzeFeedParser_Feed_Parser(){
//		
//		List<WorkLoadFeedCommonBean> dataList = new ArrayList<WorkLoadFeedCommonBean>();
//		String url = "http://www.priceza.com/";
//		
//		/*new name is blank */
//		for(int i = 0 ;i < 2 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("");
//			data.setOld_name("name");
//			data.setUrl(url);
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Parser", result);
//		dataList.clear();
//		
//		/*url change*/
//		for(int i = 0 ;i < 2 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("name");
//			data.setOld_name("name");
//			data.setUrl(url+"1");
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Parser", result);
//		dataList.clear();
//		
//		/*url blank*/
//		for(int i = 0 ;i < 2 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("name");
//			data.setOld_name("name");
//			data.setUrl("");
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Parser", result);
//		dataList.clear();
//		
//		/*name change*/
//		for(int i = 0 ;i < 2 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("name");
//			data.setOld_name("name2");
//			data.setUrl("");
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Parser", result);
//		
//		dataList.clear();
//		
//		/*some product have changed name*/
//		for(int i = 0 ;i < 2 ; i++){
//			WorkLoadFeedCommonBean data = new WorkLoadFeedCommonBean();
//			data.setName("name"+i);
//			data.setOld_name("name1");
//			data.setUrl("url");
//			data.setOld_url(url);
//			dataList.add(data);
//		}
//		result = schedule.analyzeFeedParser(dataList);	
//		assertEquals("Feed_Parser", result);
//	}
//	
//	@Test
//	public void  analyzeOnlineFeed_Exceed_Delete_Limit(){
//		List<ProductParserBean> dataList = new ArrayList<ProductParserBean>();
//		String url = "http://www.priceza.com/";
//		for(int i = 0 ;i < 3 ; i++){
//			ProductParserBean data = new ProductParserBean();
//			data.setName("name"+i);
//			data.setUrl(url);
//			data.setIsPrice(true);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeOnlineFeed(dataList);	
//		assertEquals("Feed_Delete_Limit", result);
//		
//	}
//	
//	@Test
//	public void  analyzeOnlineFeed_Parser(){
//		List<ProductParserBean> dataList = new ArrayList<ProductParserBean>();
//		
//		dataList.clear();
//		for(int i = 0 ;i < 3 ; i++){
//			ProductParserBean data = new ProductParserBean();
//			data.setName("");
//			data.setIsPrice(false);
//			dataList.add(data);
//		}
//		String result = schedule.analyzeOnlineFeed(dataList);	
//		assertEquals("Feed_Parser", result);
//		
//	}
//
//
//	private Map<Integer, MonitorBean> setMonitorListMapData(){
//		Map<Integer, MonitorBean> monitorMap = new HashMap<Integer, MonitorBean>();
//		String[][] oldData = {{"1","300012","Bot13-bot"},{"2", "300010", "Bot13-bot"},{"3", "300011", ""}};
//		for(int i=0;i<oldData.length;i++){
//			MonitorBean tmp = new MonitorBean();
//			tmp.setId(Integer.parseInt(oldData[i][0]));
//			tmp.setMerchantId(Integer.parseInt(oldData[i][1]));
//			tmp.setServer(oldData[i][2]);
//			monitorMap.put(tmp.getMerchantId(),tmp);
//		}
//		return monitorMap;
//		
//	}
//	
	
	
	

}
