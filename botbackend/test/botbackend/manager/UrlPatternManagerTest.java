package botbackend.manager;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import botbackend.bean.UrlPatternBean;

public class UrlPatternManagerTest {
	
	private UrlPatternManager urlPatternManager;
	private List<UrlPatternBean> urlPatternBeanList ;
	
	private final String[][] URL_PATTERN = {
			{"haveString", "/product/", "MATCH", "0"},
			{"haveQuery", "page", "CONTINUE", "0"},			
			{"haveString", "tskp", "CONTINUE", "0"},
			{"endsWith", "pro_", "DROP", "0"},			
			{"haveString", "#rx", "DROP", "0"},		
			
			{"haveString", "/p/", "MATCH", "1"},
			{"endsWith", ".html", "MATCH", "1"},
			{"haveQuery", "pid", "REMOVE", "0"},
			{"haveString", "feedUrl", "FEEDMAPPING", "0"}
	};
	@Before
	public void setUp() throws Exception {
		urlPatternBeanList = new ArrayList<UrlPatternBean>();
		for (int i = 0; i < URL_PATTERN.length; i++) {			
			UrlPatternBean  b = new UrlPatternBean();
			b.setCondition(URL_PATTERN[i][0]);
			b.setValue(URL_PATTERN[i][1]);
			b.setAction(URL_PATTERN[i][2]);
			b.setGroup(Integer.parseInt(URL_PATTERN[i][3]));
			
			urlPatternBeanList.add(b);
		}
		urlPatternManager = new UrlPatternManager(urlPatternBeanList);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void allMatchBeanList_count() {
		assertEquals(2, urlPatternManager.allMatchBeanList.size());
	}
	@Test
	public void allContinueBeanList_count() {
		assertEquals(2, urlPatternManager.allContinueBeanList.size());
	}
	@Test
	public void allDropBeanList_count() {
		assertEquals(2, urlPatternManager.allDropBeanList.size());
	}
	@Test
	public void allRemoveBeanList_count() {
		assertEquals(1, urlPatternManager.allRemoveBeanList.size());
	}
	@Test
	public void allFeedmappingBeanList_count() {
		assertEquals(1,urlPatternManager.allFeedmappingBeanList.size());
	}
	
	@Test
	public void isPatternMatch_match_group_0() {
		String url = "http://www.best2home.com/product/2-7-.html";
		boolean isPatternMatch = urlPatternManager.isPatternMatch(url);
		assertEquals(true ,isPatternMatch);
	}
	@Test
	public void isPatternMatch_not_match_group_0() {
		String url = "http://www.best2home.com/2-7-.html";
		boolean isPatternMatch = urlPatternManager.isPatternMatch(url);
		assertEquals(false , isPatternMatch);
	}
	
	@Test
	public void isPatternMatch_match_group_1() {
		String url = "http://www.best2home.com/p/2-7-.html";
		boolean isPatternMatch = urlPatternManager.isPatternMatch(url);
		assertEquals(true ,isPatternMatch);
	}
	
	@Test
	public void isPatternMatch_not_match_group_1() {
		String url = "http://www.best2home.com/p/2-7-";
		boolean isPatternMatch = urlPatternManager.isPatternMatch(url);
		assertEquals(false ,isPatternMatch);
		
		url = "http://www.best2home.com/pd/2-7-pro_.html";
		isPatternMatch = urlPatternManager.isPatternMatch(url);
		assertEquals(false ,isPatternMatch);
	}
	
	@Test
	public void isPatternContinue_match_haveQuery() {
		String url = "http://www.best2home.com/product/2-7-pro_.html?page=1";
		boolean isPatternContinue = urlPatternManager.isPatternContinue(url);
		assertEquals(true ,isPatternContinue);
	}
	
	@Test
	public void isPatternContinue_not_match_haveQuery() {
		String url = "http://www.best2home.com/product/2-7-pro_.html/page/1";
		boolean isPatternContinue = urlPatternManager.isPatternContinue(url);
		assertEquals(false ,isPatternContinue);
	}
	
	@Test
	public void isPatternContinue_match_haveString() {
		String url = "http://www.best2home.com/product/2-7-pro_.html/tskp/1";
		boolean isPatternContinue = urlPatternManager.isPatternContinue(url);
		assertEquals(true ,isPatternContinue);
	}
	
	@Test
	public void isPatternContinue_not_match_haveString_tskp() {
		String url = "http://www.best2home.com/product/2-7-pro_.html?tskp=1";
		boolean isPatternContinue = urlPatternManager.isPatternContinue(url);
		assertEquals(true ,isPatternContinue);
	}
	
	@Test
	public void isPatternDrop_match() {
		String url = "http://www.best2home.com/product/2-7-pro_.html#rx";
		boolean isPatternDrop = urlPatternManager.isPatternDrop(url);
		assertEquals(true ,isPatternDrop);
	}
	
	@Test
	public void isPatternDrop_not_match() {
		String url = "http://www.best2home.com/product/2-7-pro_.html";
		boolean isPatternDrop = urlPatternManager.isPatternDrop(url);
		assertEquals(false ,isPatternDrop);
	}
	
	@Test
	public void processRemoveQuery_first_parameter(){
		String url = "http://www.best2home.com/product/2-7-pro_.html?pid=123354&cat=1";
		String new_url = urlPatternManager.processRemoveQuery(url);
		assertEquals("http://www.best2home.com/product/2-7-pro_.html?cat=1" ,new_url);
	}
	
	@Test
	public void processRemoveQuery_second_parameter(){
		String url = "http://www.best2home.com/product/2-7-pro_.html?cat=1&pid=123354";
		String new_url = urlPatternManager.processRemoveQuery(url);
		assertEquals("http://www.best2home.com/product/2-7-pro_.html?cat=1" ,new_url);
	}
	
	@Test
	public void processRemoveQuery_no_remove_parameter(){
		String url = "http://www.best2home.com/product/2-7-pro_.html?cat=1";
		String new_url = urlPatternManager.processRemoveQuery(url);
		assertEquals("http://www.best2home.com/product/2-7-pro_.html?cat=1" ,new_url);
	}
	
}
