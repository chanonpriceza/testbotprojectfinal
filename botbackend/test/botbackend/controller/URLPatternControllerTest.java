package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.UserBean;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.utils.DatabaseManager;

@RunWith(MockitoJUnitRunner.class)
public class URLPatternControllerTest{

    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    @Mock HttpSession session;    
    @Mock MerchantDB  merchantDB;
    @Mock UrlPatternDB urlPatternDB;
    @Mock MerchantConfigDB mcDB;
    @Mock ConfigDB configDB;
    @Mock private DatabaseManager databaseManager;
    @Mock ServletFileUpload servletFileUpload;
    @Mock private FileItem fileItem;

    private URLPatternController servlet; 
    
    String fileName_csv_success;
    String fileName_csv_error;
    String fileName_csv_firstrecordIsblank;
    String fileName_html ;
    int MerchantID = 111;
    UrlPatternBean urlPatternBean = new UrlPatternBean();
    MerchantConfigBean merchantConfigBean = new MerchantConfigBean();
    UserBean userBean = new UserBean();
    ConfigBean configBean = new ConfigBean();
    List<UrlPatternBean> urlPatternBeanList = new ArrayList<UrlPatternBean>();
    List<MerchantConfigBean> merchantConfigList = new ArrayList<MerchantConfigBean>();
  
    @Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(databaseManager.getUrlPatternDB()).thenReturn(urlPatternDB);
		when(databaseManager.getConfigDB()).thenReturn(configDB);
		when(databaseManager.getMerchantConfigDB()).thenReturn(mcDB);
		
		userBean.setUserName("test");
		when(session.getAttribute("userData")).thenReturn(userBean);
		when(request.getMethod()).thenReturn("POST");
		configBean.setConfigValue("51016,80325");
		when(configDB.getByName("category.allow")).thenReturn(configBean);
		
		
		String path = System.getProperty("user.dir");
		fileName_csv_success = path + "/WebContent/assets/test/GumpPattenTest.csv";
		fileName_csv_error = path + "/WebContent/assets/test/GumpPattenTest_error_firstrecord.csv";
		fileName_csv_firstrecordIsblank = path + "/WebContent/assets/test/GumpPattenTest_error_firstrecord_blank.csv";
		fileName_html = path + "/WebContent/assets/test/GumpTest.html";
		
		servlet = new URLPatternController();
//		servlet = new URLPatternController(databaseManager);
	}
    
    @Test
	public void getModalAndView_all() {
		ModelAndView result = getModalAndView("all");		
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("urlpattern", result.getViewName());
		assertEquals(true, newpage);
	}

	@Test
	public void getModalAndView_all_and_merchantconfig() {
		ModelAndView result = getModalAndView("all|crawlerconfig.urlpattern");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("urlpattern", result.getViewName());
		assertEquals(true, newpage);
	}
	
	@Test
	public void getModalAndView_all_except() {
		ModelAndView result = getModalAndView("task.taskmanager");
		assertEquals("redirect:/admin/dashboard", result.getViewName());
	}
	
	@Test
	public void getModalAndView_merchantconfig() {
		ModelAndView result = getModalAndView("merchant.merchantconfig|crawlerconfig.urlpattern|crawlerconfig.parser");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("urlpattern", result.getViewName());
		assertEquals(true, newpage);
	}
	
	  
    private ModelAndView getModalAndView(String rolePermission) {

		when(request.getSession(false)).thenReturn(session);
		HashMap<String, String> permission = new HashMap<String, String>();
		String[] permissions = rolePermission.split("\\|");
		for (String p : permissions) {
			permission.put(p, "");
		}
		when(session.getAttribute("Permission")).thenReturn(permission);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ModelAndView result = null;
		try {

			when(response.getWriter()).thenReturn(pw);
			result = servlet.getModalAndView(request, response);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		return result;
	}
    
    @Test
    public void getModalAndViewAjax_getDataByMerchantId__succsss() {
    	urlPatternBean.setId(1);
    	urlPatternBean.setAction("MATCH");
    	urlPatternBean.setCategoryId(1);
    	urlPatternBean.setCondition("haveString");
    	urlPatternBean.setGroup(0);
    	urlPatternBean.setKeyword("test");
    	urlPatternBean.setValue("https://www.priceza.com/test");
    	urlPatternBeanList.add(urlPatternBean);
    	
    	getModalAndViewAjax_getDataByMerchantId("getDataByMerchantId",String.valueOf(MerchantID),urlPatternBeanList,"success","update") ;
    }
    
    
// no check condition   merchant > 0 
//  @Test
  public void getModalAndViewAjax_getDataByMerchantId__fail_merchantzero() {
	  urlPatternBean.setId(1);
	  urlPatternBean.setAction("MATCH");
	  urlPatternBean.setCategoryId(1);
	  urlPatternBean.setCondition("haveString");
	  urlPatternBean.setGroup(0);
	  urlPatternBean.setKeyword("test");
	  urlPatternBean.setValue("https://www.priceza.com/test");
	  urlPatternBeanList.add(urlPatternBean);
	  getModalAndViewAjax_getDataByMerchantId("getDataByMerchantId",String.valueOf(0),urlPatternBeanList,"fail","") ;
  }
 

  @Test
  public void getModalAndViewAjax_getDataByMerchantId__success_listdataIsNull_insertData() {
	  urlPatternBean.setId(1);
	  urlPatternBean.setAction("MATCH");
	  urlPatternBean.setCategoryId(1);
	  urlPatternBean.setCondition("haveString");
	  urlPatternBean.setGroup(0);
	  urlPatternBean.setKeyword("test");
	  urlPatternBean.setValue("https://www.priceza.com/test");
	  urlPatternBeanList.add(urlPatternBean);
	  getModalAndViewAjax_getDataByMerchantId("getDataByMerchantId",String.valueOf(1),null,"success","insert");
  }
 
// 
  @Test
  public void getModalAndViewAjax_getDataByMerchantId__success_merchantbeanIsNULL_modeInsert() throws SQLException {
	  getModalAndViewAjax_getDataByMerchantId("getDataByMerchantId",String.valueOf(MerchantID),null,"success","insert") ;
  }
// when result is return correct  but JSONParser is error  
//  @Test
  public void getModalAndViewAjax_getDataByMerchantId__fail_cmd() {
	  urlPatternBean.setId(1);
	  urlPatternBean.setAction("MATCH");
	  urlPatternBean.setCategoryId(1);
	  urlPatternBean.setCondition("haveString");
	  urlPatternBean.setGroup(0);
	  urlPatternBean.setKeyword("test");
	  urlPatternBean.setValue("https://www.priceza.com/test");
	  urlPatternBeanList.add(urlPatternBean);
	  getModalAndViewAjax_getDataByMerchantId("fail",String.valueOf(MerchantID),null,null,"") ;
  }
  
  
  private void getModalAndViewAjax_getDataByMerchantId(String cmd , String merchantId ,List<UrlPatternBean> urlPatternList ,String header ,String mode) {
  	try {
	  		when(request.getSession()).thenReturn(session);
			when(request.getParameter("cmd")).thenReturn(cmd);
			when(request.getParameter("merchantId")).thenReturn(merchantId);
			when(urlPatternDB.getUrlPatternByMerchantId(MerchantID)).thenReturn(urlPatternList);
			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			when(response.getWriter()).thenReturn(pw);
			
			servlet.getModalAndViewAjax(request,response);
			JSONObject jsonObj = new JSONObject();
			String result = sw.getBuffer().toString().trim();
		
	        jsonObj = (JSONObject) new JSONParser().parse(result);
			assertEquals(header, jsonObj.get("header"));
			assertEquals(mode, jsonObj.get("mode"));
			
		} catch (IOException | ServletException | SQLException | ParseException e) {
			e.printStackTrace();
		}
  }
  
  
  private JSONObject setMockPattern(String condition,String group,String value, String action, String categoryId, String keyword) throws Exception{
		JSONObject obj = new JSONObject();

		obj.put("condition", condition);
		obj.put("group", group);
		obj.put("value", value);
		obj.put("action", action);
		obj.put("categoryId", categoryId);
		obj.put("keyword", keyword);
		
		return obj;
		
  }
//  create constructor 
//  @Test
	public void getModalAndViewAjax_saveTable_success() throws Exception {
		String header = "success";		
		JSONArray arr = new JSONArray();
		
		arr.add(setMockPattern("","0","https://testSaveTable.com", "", "0", ""));
		getModalAndViewAjax_saveTable(arr, header,Integer.toString(MerchantID));
	}
// create constructor   
//  @Test
  public void getModalAndViewAjax_saveTable_success1() throws Exception {
	  String header = "success";		
	  JSONArray arr = new JSONArray();
	  
	  arr.add(setMockPattern("","1","https://testSaveTable.com", "", "0", ""));
	  getModalAndViewAjax_saveTable(arr, header,Integer.toString(MerchantID));
  }
  
//	create constructor 
//	@Test
	public void getModalAndViewAjax_saveTable_successEmpty() throws Exception {
		String header = "success-empty";
		JSONArray arr = new JSONArray();
			//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","0","https://testSaveTable.com", "", "0", ""));
		arr.add(setMockPattern("","","", "", "", ""));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_fail_JSONArray_isNull() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
			//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","0","https://testSaveTable.com", "", "0", ""));
		getModalAndViewAjax_saveTable(new JSONArray(), header, "0");
		getModalAndViewAjax_saveTable(new JSONArray(), header, Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_fail_group_isNotNumeric() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","w","https://testSaveTable.com", "", "0", ""));
		getModalAndViewAjax_saveTable(arr, header, "0");
//		getModalAndViewAjax_saveTable(new JSONArray(), header, Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_fail_group_isEmpty() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","","https://testSaveTable.com", "", "x", ""));
		getModalAndViewAjax_saveTable(arr, header, "0");
		getModalAndViewAjax_saveTable(new JSONArray(), header, Integer.toString(MerchantID));
	}
// Unexpected token END OF FILE at position 0. But no condition check 
//	@Test
	public void getModalAndViewAjax_saveTable_fail_group_isNotNumeric_() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","X","https://testSaveTable.com", "", "1", ""));
		getModalAndViewAjax_saveTable(arr, header,  Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_FailNotAllow() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		//condition, group, value, action, categoryId, keyword
		arr.add(setMockPattern("","","https://testSaveTable.com", "", "000", ""));
		getModalAndViewAjax_saveTable(arr, header, "0");
		getModalAndViewAjax_saveTable(new JSONArray(), header, Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_JsonIsNull() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		when(urlPatternDB.insertUrlPatternBarch(arr, MerchantID)).thenReturn(-1);
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(MerchantID));
	}
	
	@Test
	public void getModalAndViewAjax_saveTable_categoryAllowList() throws Exception {
		String header = "fail-not-allow";
		JSONArray arr = new JSONArray();
		arr.add(setMockPattern("","5","https://testSaveTable.com", "", "99999", ""));
		when(urlPatternDB.insertUrlPatternBarch(null, MerchantID)).thenReturn(-1);
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(MerchantID));
	}
	
  
  private void getModalAndViewAjax_saveTable(JSONArray arr, String header, String merchantId) throws Exception{
	    when(request.getContentType()).thenReturn("");
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("saveTable");
		when(request.getParameter("merchantId")).thenReturn(merchantId);
		when(request.getParameter("jsonData")).thenReturn(arr.toString());
	
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		when(response.getWriter()).thenReturn(pw);
		servlet.getModalAndViewAjax(request,response);
		
		String result = sw.getBuffer().toString().trim();
		JSONObject jsonObj = new JSONObject();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(header, jsonObj.get("header"));
	}
  
  
	@Test
	public void getModalAndViewAjax_importPattern() throws Exception{

		when(request.getContentType()).thenReturn("multipart/form-data; boundary=someBoundary");
		when(request.getSession(false)).thenReturn(session);
		DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
		fileItemFactory.setSizeThreshold(50*1024);
		fileUpload.setSizeMax(10485760);
		
		final FileItem item1 = mock(FileItem.class);
        when(item1.getFieldName()).thenReturn("multipart/baa");
        final FileItem item2 = mock(FileItem.class);
        when(item2.getFieldName()).thenReturn("foo");
        List<FileItem> items = Arrays.asList(item1);
 


		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		when(response.getWriter()).thenReturn(pw);
//		servlet.getModalAndViewAjax(request,response);	
	
		
	}
  
  
  
	@Test
	public void getModalAndViewAjax_importPattern_parseCSV_success() throws Exception{
		getModalAndViewAjax_importPattern_parseCSV("",fileName_csv_success);
	}
// 	Mark invalid
//	@Test
	public void getModalAndViewAjax_importPattern_parseCSV_error() throws Exception{
		getModalAndViewAjax_importPattern_parseCSV("Parse data error.",fileName_html);
	}
	
	@Test
	public void getModalAndViewAjax_importPattern_parseCSV_error_firstRecordNotContain() throws Exception{
		getModalAndViewAjax_importPattern_parseCSV("Header error.",fileName_csv_error);
	}
	
	@Test
	public void getModalAndViewAjax_importPattern_parseCSV_error_firstRecordIsBlank() throws Exception{
		getModalAndViewAjax_importPattern_parseCSV("Header error.",fileName_csv_firstrecordIsblank);
	}
// 	no condition check pathfile
//	@Test
	public void getModalAndViewAjax_importPattern_parseCSV_error_noPathFile() throws Exception{
		getModalAndViewAjax_importPattern_parseCSV("Header error.","");
	}
	
	
	private void getModalAndViewAjax_importPattern_parseCSV(String checkErrorMsg ,String file) throws Exception{
		
		String charsetName = "UTF-8";
		FileInputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {	
			
			is = new FileInputStream(file);
			isr = new InputStreamReader(is, charsetName);
			br = new BufferedReader(isr);
			br.mark(1000);
			
			String errorMsg = servlet.parseCSV(br,urlPatternBeanList);
			assertEquals(checkErrorMsg, errorMsg);
			int i = 0;
			br.reset();
			
			boolean first = true;
			if(urlPatternBeanList != null && urlPatternBeanList.size() > 0){
				String line = null;
				while ((line = br.readLine()) != null && urlPatternBeanList.size() > i) {
					if(first){
						first = false;
						continue;
					}
					String[] expect = line.split(",");
					UrlPatternBean result = urlPatternBeanList.get(i);
					assertEquals(Integer.parseInt(expect[0]), result.getMerchantId());
					assertEquals(expect[1], result.getCondition());
					assertEquals(expect[2], result.getValue());
					assertEquals(expect[3], result.getAction());
					assertEquals(Integer.parseInt(expect[4]), result.getGroup());
					assertEquals(expect[5], result.getKeyword());
					assertEquals(Integer.parseInt(expect[6]), result.getCategoryId());
					
					i++;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) { try { br.close(); } catch (IOException e) {} }
			if(isr != null) { try { isr.close(); } catch (IOException e) {} }
			if(is != null) { try { is.close(); } catch (IOException e) {} }
		}		

	}
  
	@Test 
	public void runPattern_success() throws Exception {
		String header = "success"; 
		String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
		runPattern(data, String.valueOf(MerchantID),header);
	}
// no check condition data json
//	@Test 
	public void runPattern_error_dataIsEmpty() throws Exception {
		String header = "error"; 
		String data = "[]";
		runPattern(data, String.valueOf(MerchantID),header);
	}
// no check condition merchant > 0 
//	@Test 
	public void runPattern_error_merchactLessthan() throws Exception {
		String header = "error"; 
		String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
		runPattern(data,"-1",header);
	}
	
// no check condition merchant is Empty
//	@Test 
	public void runPattern_error_merchantIsEmpty() throws Exception {
		String header = "error"; 
		String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
		runPattern(data,"",header);
	}
	 
	
	private void runPattern(String data , String mID ,String header) throws Exception{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);		
		when(response.getWriter()).thenReturn(pw);
		servlet.runPattern(request, response, data, mID);
		
		String result = sw.getBuffer().toString().trim();
		JSONObject jsonObj = new JSONObject();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(header, jsonObj.get("header"));
		if(jsonObj.get("header").equals("success")) {
			// �Դ����͹ 
		}
		
	}
	
	@Test 
	public void runUrlAnalysis_success() throws Exception {
		String header = "success"; 
		String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
		merchantConfigBean.setMerchantId(MerchantID);
		merchantConfigBean.setId(1);
		merchantConfigBean.setField("maxDepth");
		merchantConfigBean.setValue("1");
		merchantConfigList.add(merchantConfigBean);
		
		when(mcDB.findMerchantConfig(MerchantID)).thenReturn(merchantConfigList);
		runUrlAnalysis(data, String.valueOf(MerchantID),header);
	}
	

// no check condition data json
//		@Test 
		public void runUrlAnalysis_error_dataIsEmpty() throws Exception {
			String header = "error"; 
			String data = "[]";
			runUrlAnalysis(data, String.valueOf(MerchantID),header);
		}
// no check condition merchant > 0 
//		@Test 
		public void runUrlAnalysis_error_merchactLessthan() throws Exception {
			String header = "error"; 
			String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
			runUrlAnalysis(data,"-1",header);
		}
		
// no check condition merchant is Empty
//		@Test 
		public void runUrlAnalysis_error_merchantIsEmpty() throws Exception {
			String header = "error"; 
			String data = "[{\"condition\":\"haveQuery\",\"group\":\"0\",\"value\":\"p\",\"action\":\"CONTINUE\"},{\"condition\":\"endsWith\",\"group\":\"0\",\"value\":\".html\",\"action\":\"MATCH\"},{\"condition\":\"haveString\",\"group\":\"0\",\"value\":\"/all-products/\",\"action\":\"MATCH\"},{\"condition\":\"\",\"group\":\"0\",\"value\":\"https://www.gamerig.in.th/all-products/pc-games/steam\",\"action\":\"STARTURL\"}]";
			runUrlAnalysis(data,"",header);
		}
	
	private void runUrlAnalysis(String data , String mID ,String header) throws Exception{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);		
		when(response.getWriter()).thenReturn(pw);
		servlet.runUrlAnalysis(request, response, data, mID);
		
		String result = sw.getBuffer().toString().trim();
		JSONObject jsonObj = new JSONObject();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(header, jsonObj.get("header"));	
	}
 
	
	@Test 
	public void exportPattern_success() throws Exception {
		JSONArray arr = new JSONArray();
		arr.add(setMockPattern("","","", "", "", ""));
		arr.add(setMockPattern("","0","https://testSaveTable.com", "", "0", ""));
		String result = getModalAndViewAjax_exportPattern(String.valueOf(MerchantID),arr);
		
		String[] lines = result.split("\r\n");
		
		assertEquals(3, lines.length);
		assertEquals("MerchantId,Condition,Value,Action,Group,Keyword,CategoryId", lines[0]);
		assertEquals(Integer.toString(MerchantID)+",,,,,,", lines[1]);
		assertEquals(Integer.toString(MerchantID)+",,https://testSaveTable.com,,0,,0", lines[2]);
	}
	
	@Test 
	public void exportPattern_error() throws Exception {
		JSONArray arr = new JSONArray();
		arr.add(setMockPattern("","","", "", "", ""));
		String result = getModalAndViewAjax_exportPattern(String.valueOf(-1),arr);
		
		String[] lines = result.split("\r\n");
		assertEquals(1, lines.length);
		assertEquals("MerchantId,Condition,Value,Action,Group,Keyword,CategoryId", lines[0]);

	} 
	
	
	private String getModalAndViewAjax_exportPattern(String merchantID ,JSONArray arr) throws Exception{
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("exportPattern");
		when(request.getParameter("merchantId")).thenReturn(merchantID);
		when(request.getParameter("jsonData")).thenReturn(arr.toString());
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);		
		when(response.getWriter()).thenReturn(pw);
		
		servlet.getModalAndViewAjax(request,response);	
		
		String result = sw.getBuffer().toString().trim();
		return result;
	}
	
	@Test 
	public void runGenerateUrl_success_haveQuery() throws Exception {
		String header = "success"; 
		String data = "[{\"value\":\"https://www.gamerig.in.th/all-products/?product=111&sub=1\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-products/?product=222&sub=1\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-products/?product=333&sub=1\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
	
	@Test 
	public void runGenerateUrl_success_endWith() throws Exception {
		String header = "success"; 
		String data = "[{\"value\":\"https://www.gamerig.in.th/all-products/productIsTrue\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-products/productIsTrue\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-productsproductIsTrue\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
	
	
	@Test 
	public void runGenerateUrl_success_haveString() throws Exception {
		String header = "success"; 
		String data = "[{\"value\":\"https://www.gamerig.in.th/all-products/product/11111\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-products/product/22222\",\"action\":\"Other\"},{\"value\":\"https://www.gamerig.in.th/all-products/product/33333\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
//	not found variable value in json  result -> error
//	@Test 
	public void runGenerateUrl_error_valueIsBlank() throws Exception {
		String header = "error"; 
		String data = "[{\"action\":\"Other\"},{\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
//	value is empty   result --> error
//	@Test 
	public void runGenerateUrl_error_dataIsBlank() throws Exception {
		String header = "error"; 
		String data = "[{\"value\":\" \",\"action\":\"Other\"},{\"value\":\"\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
// no check condition merchant > 0  result --> error
//	@Test 
	public void runGenerateUrl_error_merchantIDZero() throws Exception {
		// merchantID not check 
		String header = "error"; 
		String data = "[{\"value\":\" \",\"action\":\"Other\"},{\"value\":\"\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(0),header);
	}
	
// varible value wrong
//	@Test 
	public void runGenerateUrl_error_varible_value_wrong() throws Exception {
		String header = "error"; 
		String data = "[{\"eeeee\":\" \",\"action\":\"Other\"},{\"eeeee\":\"\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
//  error parse json not found "value" result --> error
//	@Test 
	public void runGenerateUrl_error_data_() throws Exception {
		// merchantID not check 
		String header = "error"; 
		String data = "[{\"eeeee\":\"www.priceza.com/?test=1\",\"action\":\"Other\"},{\"eeeee\":\"www.priceza.com/?test=1\",\"action\":\"Other\"}]";
		runGenerateUrl(data, String.valueOf(MerchantID),header);
	}
	
	private void runGenerateUrl(String data , String mID ,String header) throws Exception{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);		
		when(response.getWriter()).thenReturn(pw);
		servlet.runGenerateUrl(request, response, data, mID);
		
		String result = sw.getBuffer().toString().trim();
		JSONObject jsonObj = new JSONObject();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(header, jsonObj.get("header"));
	}
  
}
