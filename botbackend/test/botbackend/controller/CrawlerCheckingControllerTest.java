package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import botbackend.db.utils.DatabaseManager;
@RunWith(MockitoJUnitRunner.class)
public class CrawlerCheckingControllerTest {
	@Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    @Mock HttpSession session;   
    @Mock private DatabaseManager databaseManager;
    @Mock HttpURLConnection conn ;
    @Mock InputStream isr ;
    String data ;
    private CrawlerCheckingController servlet;
    
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		servlet = new CrawlerCheckingController();
		
		
	}

	
//##########################################################		getTask 		#########################################################################	
// wrong productUrl is null	
	@Test
	public void getTask_productUrlisNull() throws Exception {
		String url = "https://www.google.co.th/";
		String nextPageUrl = "https://www.google.co.th/page=1";
		String productUrl = null;
		String productName = "";
		String productPrice = "";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"false\",\"available\":\"true\",\"productlink\":\"no data\",\"nameMatch\":\"\",\"priceMatch\":\"\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotMatchinHtml() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"true\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-EURORACK-PRO-RX1602.html";
		String productName = "EURORACK PRO RX1602";
		String productPrice = "7,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"true\",\"nameMatch\":\"true\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
// httpRequest return null not check condition	
//	@Test
	public void getTask_urlisEmpty_getResponseCodeNot200() throws Exception {
		String url = "http://localhost:8080/botbackend/test";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "error";
		String result = null;
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
// httpRequest return null not check condition		
//	@Test
	public void getTask_urlisError_Exception() throws Exception {
		String url = "http://localhost:8080/botbackend/test";
		String nextPageUrl = "http://localhost:8080/botbackend/test";
		String productUrl = "http://localhost:8080/botbackend/test";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "error";
		String result = null;
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_notcharsetUTF8() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "tis-620";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"true\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_productUrlIsEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"no data\",\"nameMatch\":\"false\",\"priceMatch\":\"false\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_nextpageIsEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"no data\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"true\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_productNameIsEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"no data\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_productNameNotMatchIsEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "Behringer XENYX 1002 FXXXXXXX";
		String productPrice = "5,500";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"false\",\"priceMatch\":\"true\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisNotEmpty_productPriceIsEmpty() throws Exception {
		String url = "https://www.audiothailand.com/MIXER/MIXER-Behringer";
		String nextPageUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer_cpage2_view1";
		String productUrl = "https://www.audiothailand.com/MIXER/MIXER-Behringer/Behringer-XENYX-1002-FX.html";
		String productName = "Behringer XENYX 1002 FX";
		String productPrice = "";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"true\",\"available\":\"true\",\"productlink\":\"false\",\"nameMatch\":\"true\",\"priceMatch\":\"no data\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_nameMatch_NoData() throws Exception {
		String url = "https://www.xxxxxxx.com";
		String nextPageUrl = "https://www.xxxxxxx.com";
		String productUrl = "https://www.xxxxxxx.com";
		String productName = "";
		String productPrice = "";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"false\",\"nextpage\":\"false\",\"available\":\"false\",\"productlink\":\"false\",\"nameMatch\":\"\",\"priceMatch\":\"\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_nameMatch_NoData_htmlisBlank() throws Exception {
		String url = "https://www.xxxxxxx.com";
		String nextPageUrl = "";
		String productUrl = "";
		String productName = "";
		String productPrice = "";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"false\",\"nextpage\":\"\",\"available\":\"false\",\"productlink\":\"\",\"nameMatch\":\"\",\"priceMatch\":\"\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_productUrlisEmpty() throws Exception {
		String url = "https://www.google.co.th/";
		String nextPageUrl = "https://www.google.co.th/page=1";
		String productUrl = "";
		String productName = "";
		String productPrice = "";
		String charset = "UTF-8";
		String header = "success";
		String result = "{\"passSSL\":\"true\",\"nextpage\":\"false\",\"available\":\"true\",\"productlink\":\"no data\",\"nameMatch\":\"\",\"priceMatch\":\"\"}";
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_error_Invalid() throws Exception {
		String url = "";
		String nextPageUrl = "";
		String productUrl = "";
		String productName = "";
		String productPrice = "";
		String charset = "";
		String header = "error";
		String result = "URL Invalid";
		
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	@Test
	public void getTask_error_Invalid2() throws Exception {
		String url = "www.google.co.th/";
		String nextPageUrl = "";
		String productUrl = "";
		String productName = "";
		String productPrice = "";
		String charset = "";
		String header = "error";
		String result = "URL Invalid";
		
		getTask(url, nextPageUrl, productUrl, productName, productPrice, charset,header,result);
	}
	
	
	private void getTask(String url,String nextPageUrl,String productUrl,String productName,String productPrice,String charset , String header,String result_test) throws Exception {
		when(request.getParameter("url")).thenReturn(url);
		when(request.getParameter("nextPageUrl")).thenReturn(nextPageUrl);
		when(request.getParameter("productUrl")).thenReturn(productUrl);
		when(request.getParameter("productName")).thenReturn(productName);
		when(request.getParameter("productPrice")).thenReturn(productPrice);
		when(request.getParameter("charset")).thenReturn(charset);
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		when(response.getWriter()).thenReturn(pw);
		
		
		servlet.getTaskf(request, response);
		
		JSONObject jsonObj = new JSONObject();
		String result = sw.getBuffer().toString().trim();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(jsonObj.get("header").toString(),header);
		assertEquals(jsonObj.get("result").toString(),result_test);

	}
//##########################################################		getModalAndView 		#########################################################################	
		
		@Test
		public void getModalAndView_all() throws Exception {
			ModelAndView result = getModalAndView("all");		
			boolean newpage = (boolean) result.getModel().get("newPage");
			assertEquals("crawlerchecking", result.getViewName());
			assertEquals(true, newpage);
		}

		@Test
		public void getModalAndView_all_and_merchantconfig() throws Exception {
			ModelAndView result = getModalAndView("all|merchant.merchantmanager");
			boolean newpage = (boolean) result.getModel().get("newPage");
			assertEquals("crawlerchecking", result.getViewName());
			assertEquals(true, newpage);
		}
		
		@Test
		public void getModalAndView_all_except() throws Exception {
			ModelAndView result = getModalAndView("task.taskmanager");
			assertEquals("redirect:/admin/dashboard", result.getViewName());
		}

		private ModelAndView getModalAndView(String rolePermission) throws Exception {

			when(request.getSession(false)).thenReturn(session);
			HashMap<String, String> permission = new HashMap<String, String>();
			String[] permissions = rolePermission.split("\\|");
			for (String p : permissions) {
				permission.put(p, "");
			}
			when(session.getAttribute("Permission")).thenReturn(permission);

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ModelAndView result = null;
			try {

				when(response.getWriter()).thenReturn(pw);
				result = servlet.getModalAndView(request, response);

			} catch (IOException e) {
				// TODO Auto-generated catch block
			} finally {
				if (sw != null) {
					try {
						sw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (pw != null) {
					pw.close();
				}
			}
			return result;
		}
	
	

}
