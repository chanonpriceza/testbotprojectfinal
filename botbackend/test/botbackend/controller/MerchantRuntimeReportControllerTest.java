package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.utils.DatabaseManager;

@RunWith(MockitoJUnitRunner.class)
public class MerchantRuntimeReportControllerTest {
	@Mock HttpServletRequest request;
	@Mock HttpServletResponse response;
	@Mock HttpSession session;
	@Mock DatabaseManager databaseManager;
	@Mock MerchantRuntimeReportDB merchantruntimereportDB;
	private MerchantRuntimeReportController servlet;
	
	// code For Test
	//	private DatabaseManager databasemanager;
	//	
	//	public MerchantRuntimeReportController(){
	//		databasemanager = DatabaseManager.getDatabaseManager();
	//	}
	//	 
	//	public MerchantRuntimeReportController(DatabaseManager databasemanager){
	//		this.databasemanager = databasemanager;
	//	}
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(databaseManager.getMerchantruntimeReportDB()).thenReturn(merchantruntimereportDB);
		when(merchantruntimereportDB.countReportBySearch("1", 1, "1", "1", "1","")).thenReturn(5);
		//servlet = new MerchantRuntimeReportController(databaseManager);
	}

	
	@Test
	public  void getParamcheckPermissionPass() throws Exception {
		Map<String, String> permission = new HashMap<String, String>();
		permission.put("all","");
		String cmd = "searchMerchantReport";
		String result = "merchantruntimereport";
		getParam(permission,cmd,result,"1");
	}
	
	
	@Test
	public  void testGetMonitorDataHasPage() throws Exception {
		
		Map<String, String> permission = new HashMap<String, String>();
		permission.put("all","");
		String cmd = "searchMerchantReport";
		String result = "merchantruntimereport";
		getParam(permission,cmd,result,"0");
		ModelAndView model = servlet.getMonitorData(100,"","", "", 0, "", request, response);
		assertEquals(model.getViewName(),"merchantruntimereport");
		assertEquals(model.getModel().get("reportList"),new LinkedList<MerchantRuntimeReportBean>());
		
	}
	
	@Test
	public  void testGetMonitorDataNoPermission() throws Exception {
		
		Map<String, String> permission = new HashMap<String, String>();
		permission.put("","");
		ModelAndView model = servlet.getMonitorData(100,"","", "", 0, "", request, response);
		assertEquals(model.getViewName(),"redirect:/admin/dashboard");

	}
	
	@Test 
	public  void testException() throws Exception {
		Map<String, String> permission = new HashMap<String, String>();
		permission.put("all","");
		String cmd = "";
		when(databaseManager.getMerchantruntimeReportDB()).thenReturn(null);
		String result = "merchantruntimereport";
		getParam(permission,cmd,result,"1");
	}
	
	@Test
	public  void getParamCheckNoPermission() throws Exception {
		Map<String, String> permission = new HashMap<String, String>();
		permission.put("","");
		String cmd = "";
		String result = "redirect:/admin/dashboard";
		getParam(permission,cmd,result,"1");
	}
	
	
	private void getParam(Map<String, String> Permission,String cmd,String result,String page) throws Exception {
		
		when(session.getAttribute("Permission")).thenReturn(Permission);
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("searchParam")).thenReturn("1");
		when(request.getParameter("reportType")).thenReturn("1");
		when(request.getParameter("reportStatus")).thenReturn("1");
		when(request.getParameter("searchType")).thenReturn("1");
		when(request.getParameter("reportTime")).thenReturn("1");
		when(request.getParameter("cmd")).thenReturn(cmd);
		when(request.getParameter("page")).thenReturn(page);
		ModelAndView model = servlet.getParam("-1","Test","Test","Test", request, response);
		assertEquals(model.getViewName(),result);
	}
	
	@Test
	public void getModalAndView_all() throws Exception {
		ModelAndView result = getModalAndView("all");	
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantruntimereport", result.getViewName());
		assertEquals(true, newpage);
	}

	@Test
	public void getModalAndView_all_and_merchantconfig() throws Exception {
		ModelAndView result = getModalAndView("all|merchant.merchantmanager");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantruntimereport", result.getViewName());
		assertEquals(true, newpage);
	}

	@Test
	public void getModalAndView_all_except() throws Exception {
		ModelAndView result = getModalAndView("task.taskmanager");
		assertEquals("redirect:/admin/dashboard", result.getViewName());
	}

	private ModelAndView getModalAndView(String rolePermission) throws Exception {	
		when(request.getSession(false)).thenReturn(session);
		HashMap<String, String> permission = new HashMap<String, String>();
		String[] permissions = rolePermission.split("\\|");
		for (String p : permissions) {
			permission.put(p, "");
		}
		when(session.getAttribute("Permission")).thenReturn(permission);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ModelAndView result = null;
		try {

			when(response.getWriter()).thenReturn(pw);
			result = servlet.getModalAndView(request, response);

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		return result;
	}

}
