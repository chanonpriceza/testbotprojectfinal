package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import bean.NotificationBean;
import botbackend.bean.UserBean;
import botbackend.db.utils.DatabaseManager;
import db.NotificationDB;
import utils.BotUtil;
@RunWith(MockitoJUnitRunner.class)
public class NotificationControllerTest {
	@Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    @Mock HttpSession session;    
    @Mock private DatabaseManager databaseManager;
    @Mock NotificationDB notificationDB;
    @Mock BotUtil botUtil;
    
	private static final int numRecordPage = 40;
	private static final int numRecord = 8;
	private static final int start = 0;
    
    private NotificationController servlet;
    UserBean userBean = new UserBean();
    NotificationBean notificationBean = new NotificationBean();
    List<NotificationBean> listNotiBean = new ArrayList<NotificationBean>();
    
    @Before
	public void setUp() throws Exception {
    	MockitoAnnotations.initMocks(this);
		servlet = new NotificationController();
//		servlet = new NotificationController(databaseManager);
		when(databaseManager.getNotificationDB()).thenReturn(notificationDB);
		userBean.setUserId(16);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
	}
  
  //##########################################################		Note  	 #########################################################################	
//	Comment @Test =  because is error or null 
//##########################################################		Solution  	 #########################################################################	
//	1. create constructor in NotificationController 
//	2 Setup	servlet = new NotificationController(databaseManager);

//    private DatabaseManager databaseManager;
//	public NotificationController() {
//		this.databaseManager = databaseManager.getDatabaseManager();
//	}
//	
//	public NotificationController(DatabaseManager databaseManager) {
//		this.databaseManager = databaseManager;
//	}	
    
	@Test
	public void getModalAndView_success() throws Exception {
		int returncountAll = 1;
		when(request.getSession(true)).thenReturn(session);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(1);
		
		ModelAndView result = getModalAndView(returncountAll, userBean);
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals(true, newpage);
		assertEquals("notificationpage", result.getViewName());
		
		int totalnotification = (int) result.getModel().get("totalnotification");
		assertEquals(totalnotification, 1);
	}
	
	@Test
	public void getModalAndView_totalnotification_IsZero() throws Exception {
		int returncountAll = 0;
		when(request.getSession(true)).thenReturn(session);
		userBean.setUserId(16);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(1);
		
		ModelAndView result = getModalAndView(returncountAll, userBean);
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals(true, newpage);
		assertEquals("notificationpage", result.getViewName());
		
		int totalnotification = (int) result.getModel().get("totalnotification");
		assertEquals(totalnotification, 0);

	}
	
	
	private ModelAndView getModalAndView(int returncountAll ,UserBean userBean ) throws Exception {
		when(request.getSession(true)).thenReturn(session);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(returncountAll);
		
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		when(response.getWriter()).thenReturn(pw);
		return servlet.getModalAndView(request, response);
	}
	
	
	@Test
	public void getNotificationData_success() throws Exception {
		int returncountAll = 1;
		int page = 2;
		when(request.getSession(true)).thenReturn(session);
		userBean.setUserId(16);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(1);
		
		ModelAndView result = getNotificationData(page,returncountAll, userBean);
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals(true, newpage);
		assertEquals("notificationpage", result.getViewName());
		
		int totalnotification = (int) result.getModel().get("totalnotification");
		assertEquals(totalnotification, 1);
	}
	
	@Test
	public void getNotificationData_error() throws Exception {
		int returncountAll = 1;
		int page = -1;
		when(request.getSession(true)).thenReturn(session);
		userBean.setUserId(16);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(1);
		
		ModelAndView result = getNotificationData(page,returncountAll, userBean);
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals(true, newpage);
		assertEquals("notificationpage", result.getViewName());
		
		int totalnotification = (int) result.getModel().get("totalnotification");
		assertEquals(totalnotification, 1);
	}
	
	private ModelAndView getNotificationData(int page,int returncountAll ,UserBean userBean ) throws Exception {
		when(request.getSession(true)).thenReturn(session);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		when(notificationDB.countAllNotification(userBean.getUserId(),start,numRecordPage)).thenReturn(returncountAll);
		
		
		return servlet.getNotificationData(page, request, response);
	}
	
	
	@Test
	public void changeStatusNotification_error() throws Exception {
		changeStatusNotification("1", "test","error");
		
	}
	
	@Test
	public void changeStatusNotification_action_READ_error() throws Exception {
		String action = "READ";
		String notiID = "1";
		when(notificationDB.updateStatusNotification(notiID,action)).thenReturn(0);
		changeStatusNotification(notiID, action,"error");
		
	}
	
	@Test
	public void changeStatusNotification_action_READALL_error() throws Exception {
		String action = "READALL";
		String notiID = "1";
		when(notificationDB.updateStatusAllNotification(userBean.getUserId())).thenReturn(0);
		changeStatusNotification(notiID, action,"error");
		
	}
	
	@Test
	public void changeStatusNotification_action_READ_success_NOTLOCALE_TH() throws Exception {
		String action = "READ";
		String notiID = "1";
		
		when(notificationDB.updateStatusNotification(notiID,action)).thenReturn(1);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READALL_success_NOTLOCALE_TH() throws Exception {
		String action = "READALL";
		String notiID = "1";   
		
		when(notificationDB.updateStatusAllNotification(userBean.getUserId())).thenReturn(1);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READ_success_LOCALE_TH() throws Exception {
		String action = "READ";
		String notiID = "1";
		
		notificationBean.setId(1);
		notificationBean.setMessage("test");
		notificationBean.setUser("test");
		notificationBean.setStatus("UNREAD");
		notificationBean.setCreateDate(new Date());
		listNotiBean.add(notificationBean); 
		
		when(notificationDB.updateStatusNotification(notiID,action)).thenReturn(1);
		when(notificationDB.getAllNotificationByUserID(userBean.getUserId(),start,numRecord)).thenReturn(listNotiBean);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READ_success_LOCALE_TH_listNotiBeanIsNull() throws Exception {
		String action = "READ";
		String notiID = "1";
		
		botUtil.LOCALE = "id";
		when(notificationDB.updateStatusNotification(notiID,action)).thenReturn(1);
		when(notificationDB.getAllNotificationByUserID(userBean.getUserId(),start,numRecord)).thenReturn(null);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READ_success_LOCALE_Empty_listNotiBeanIsNull() throws Exception {
		String action = "READ";
		String notiID = "1";
		
		botUtil.LOCALE = "";
		when(notificationDB.updateStatusNotification(notiID,action)).thenReturn(1);
		when(notificationDB.getAllNotificationByUserID(userBean.getUserId(),start,numRecord)).thenReturn(null);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READALL_success_LOCALE_TH() throws Exception {
		String action = "READALL";
		String notiID = "1";
		
		notificationBean.setId(1);
		notificationBean.setMessage("test");
		notificationBean.setUser("test");
		notificationBean.setStatus("READ");
		notificationBean.setCreateDate(new Date());
		listNotiBean.add(notificationBean); 
		
		when(notificationDB.updateStatusAllNotification(userBean.getUserId())).thenReturn(1);
		when(notificationDB.getAllNotificationByUserID(userBean.getUserId(),start,numRecord)).thenReturn(listNotiBean);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	@Test
	public void changeStatusNotification_action_READALL_success_LOCALE_TH_listNotiBeanIsNull() throws Exception {
		String action = "READALL";
		String notiID = "1";
		
		notificationBean.setId(1);
		notificationBean.setMessage("test");
		notificationBean.setUser("test");
		notificationBean.setStatus("READ");
		notificationBean.setCreateDate(new Date());
		listNotiBean.add(notificationBean); 
		
		when(notificationDB.updateStatusAllNotification(userBean.getUserId())).thenReturn(1);
		when(notificationDB.getAllNotificationByUserID(userBean.getUserId(),start,numRecord)).thenReturn(null);
		changeStatusNotification(notiID, action,"success");
		
	}
	
	private void changeStatusNotification(String notiID,String action ,String header) throws Exception {
		when(request.getSession(true)).thenReturn(session);
		when((UserBean) session.getAttribute("userData")).thenReturn(userBean);
		
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		when(response.getWriter()).thenReturn(pw);
		servlet.changeStatusNotification(request, response, notiID, action);
		
		String result = sw.getBuffer().toString().trim();
		JSONObject jsonObj = (JSONObject) new JSONParser().parse(result);

		assertEquals(header, jsonObj.get("header"));

	}
    
}
