package botbackend.controller;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import botbackend.bean.ParserConfigBean;
import botbackend.bean.ProductParserBean;
import botbackend.utils.ParserUtil;


public class ParserUtilTest <MockHttpServletRequestBuilder>{
	String html;
	final String path = System.getProperty("user.dir");
	
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	public String readLineFromFile(String fileName_html) throws IOException {
		File f = new File(fileName_html);
		byte[] bytes = Files.readAllBytes(f.toPath());
		html = new String(bytes,"UTF-8");
		return html;		
	}

	@After
	public void tearDown() throws Exception {
	}
	
//	Filter plainText,unescape,ifNotContain
	@Test
	public void checkConditon_MerchantName_Seed_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h1 class=\\\"book-title\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class='price-display'>\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class='price-start'>\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"between\",\"value\":\"<meta name=\\\"Description\\\" content=\\\"|,|\\\"\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"<meta property=\\\"og:image\\\" content=\\\"|,|\\\">\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"realProductId\":[{\"filter\":\"between\",\"value\":\"<td class='left'>ISB|,|</td></tr>\"},{\"filter\":\"between\",\"value\":\"nbsp;|,|&nbsp;\"}],\"upc\":[{\"filter\":\"between\",\"value\":\"<td class='left'>ISBN|,|</td></tr>\"},{\"filter\":\"between\",\"value\":\"nbsp;|,|&nbsp;\"}],\"concatId\":[{\"filter\":\"between\",\"value\":\"<td class='left'>ISBN|,|</td></tr>\"},{\"filter\":\"between\",\"value\":\"nbsp;|,|&nbsp;\"}],\"concatWord\":[{\"filter\":\"ifNotContain\",\"value\":\"ราคาพิเศษ|,|2\"},{\"filter\":\"inTag\",\"value\":\"<span class='item-type item-selected' >\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 30056;
		String fileName_html = path + "/WebContent/assets/test/SeedTest.html";
		String url ="https://www.se-ed.com/product/%E0%B8%84%E0%B8%B9%E0%B9%88%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%AA%E0%B8%AD%E0%B8%9A%E0%B8%95%E0%B8%B3%E0%B8%A3%E0%B8%A7%E0%B8%88%E0%B8%8A%E0%B8%B1%E0%B9%89%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%97%E0%B8%A7%E0%B8%99%E0%B8%84%E0%B8%A7%E0%B8%9A%E0%B8%84%E0%B8%B8%E0%B8%A1%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%84%E0%B8%AD%E0%B8%A1%E0%B8%9E%E0%B8%B4%E0%B8%A7%E0%B9%80%E0%B8%95%E0%B8%AD%E0%B8%A3%E0%B9%8C-%E0%B8%81%E0%B8%A5%E0%B8%B8%E0%B9%88%E0%B8%A1%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B9%80%E0%B8%97%E0%B8%84%E0%B8%99%E0%B8%B4%E0%B8%84.aspx?no=9786162468650";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("หนังสือ คู่มือสอบตำรวจชั้นประทวนควบคุมเครื่องคอมพิวเตอร์ กลุ่มงานเทคนิค (9786162468650)",list.getName());
		assertEquals("266.00",list.getPrice().toString());
		assertEquals("280.00",list.getBasePrice().toString());
		assertEquals(106,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("https://static.se-ed.com/ws/Storage/Originals/978616/246/9786162468650L.jpg", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals("9786162468650",list.getConcatId().toString());
		assertEquals("หนังสือ",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals("9786162468650",list.getUpc().toString());
		assertEquals("9786162468650",list.getRealProductId().toString());
	}

//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"<meta name=image content=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/PrimonlyTest.html";
		String url ="https://www.primonly.com/product/view/936:%E0%B8%8A%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B9%E0%B8%97%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%A1%E0%B8%B5%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%94%E0%B8%B3-pn927";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("ชุดสูทไม่มีปกพร้อมกางเกงสีดำ (PN927)",list.getName());
		assertEquals("1390.00",list.getPrice().toString());
		assertEquals("1490.00",list.getBasePrice().toString());
		assertEquals(222,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("https://www.primonly.com/images/product_share/product_share_pn927.png", list.getPictureUrl().toString());
		assertEquals("https://www.primonly.com/product/view/936:%E0%B8%8A%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B9%E0%B8%97%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%A1%E0%B8%B5%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%94%E0%B8%B3-pn927",list.getUrl());
		assertEquals("PN927",list.getConcatId().toString());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("PN927",list.getRealProductId());
	}
	
//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_error_expire() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"<meta name=image content=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/SeedTest_trueexpire.html";
		String url ="https://www.primonly.com/product/view/936:%E0%B8%8A%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B9%E0%B8%97%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%A1%E0%B8%B5%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%94%E0%B8%B3-pn927";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals(null,list.getName());
		assertEquals(null,list.getPrice());
		assertEquals(null,list.getBasePrice());
		assertEquals(null,list.getDescription());
		assertEquals("TRUE",list.getExpire().toString());
		assertEquals(null, list.getPictureUrl());
		assertEquals(null,list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_error_urlNull() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"<meta name=image content=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/PrimonlyTest.html";
		String url = null;
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("ชุดสูทไม่มีปกพร้อมกางเกงสีดำ (PN927)",list.getName());
		assertEquals("1390.00",list.getPrice().toString());
		assertEquals("1490.00",list.getBasePrice().toString());
		assertEquals(222,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("https://www.primonly.com/images/product_share/product_share_pn927.png", list.getPictureUrl().toString());
		assertEquals(null,list.getUrl());
		assertEquals("PN927",list.getConcatId().toString());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("PN927",list.getRealProductId());
	}
	
//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_error_picture() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"span=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/PrimonlyTest.html";
		String url ="https://www.primonly.com/product/view/936:%E0%B8%8A%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B9%E0%B8%97%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%A1%E0%B8%B5%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%94%E0%B8%B3-pn927";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("ชุดสูทไม่มีปกพร้อมกางเกงสีดำ (PN927)",list.getName());
		assertEquals("1390.00",list.getPrice().toString());
		assertEquals("1490.00",list.getBasePrice().toString());
		assertEquals(222,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals("PN927",list.getConcatId().toString());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("PN927",list.getRealProductId());
	}
	
//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_error_concatId() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"span=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<div haha\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/PrimonlyTest.html";
		String url ="https://www.primonly.com/product/view/936:%E0%B8%8A%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B9%E0%B8%97%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%A1%E0%B8%B5%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%81%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%94%E0%B8%B3-pn927";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("ชุดสูทไม่มีปกพร้อมกางเกงสีดำ",list.getName());
		assertEquals("1390.00",list.getPrice().toString());
		assertEquals("1490.00",list.getBasePrice().toString());
		assertEquals(222,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("PN927",list.getRealProductId());
	}
	
//	Filter removeNotPrice,expireContain
	@Test
	public void checkConditon_MerchantName_Primonly_error_urlBlank() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h2 itemprop=name class=product-title>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=sale-price>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"base-price strike\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=product-sdesc>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"between\",\"value\":\"<meta name=image content=|,|/>\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<span itemprop=sku class=sku>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"expire\":[{\"filter\":\"expireContain\",\"value\":\"สินค้าหมด\"}]}";
		int merchantId = 300023;
		String fileName_html = path + "/WebContent/assets/test/PrimonlyTest.html";
		String url = "";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("ชุดสูทไม่มีปกพร้อมกางเกงสีดำ (PN927)",list.getName());
		assertEquals("1390.00",list.getPrice().toString());
		assertEquals("1490.00",list.getBasePrice().toString());
		assertEquals(222,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("https://www.primonly.com/images/product_share/product_share_pn927.png", list.getPictureUrl().toString());
		assertEquals("",list.getUrl().toString());
		assertEquals("PN927",list.getConcatId().toString());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("PN927",list.getRealProductId());
	}
	
//	Filter removeNotPrice,encodeUrl
	@Test
	public void checkConditon_MerchantName_Shimono_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h1 itemprop=\\\"name\\\" class=\\\"product_title entry-title\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<ins>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<ins>|,|5\"},{\"filter\":\"between\",\"value\":\"<ins>|,|</ins>\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"woocommerce-Price-amount amount\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"price\\\">\"},{\"filter\":\"between\",\"value\":\"<del>|,|</del>\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"woocommerce-Price-amount amount\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div itemprop=\\\"description\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"images\\\">\"},{\"filter\":\"between\",\"value\":\"<a href=\\\"|,|\\\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}]}";
		int merchantId = 300070;
		String fileName_html = path + "/WebContent/assets/test/ShimonoTest.html";
		String url ="http://www.shimono.in.th/product/shimono-%E0%B8%A3%E0%B8%B8%E0%B9%88%E0%B8%99-svc-1012c/";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("เครื่องดูดฝุ่นในรถยนต์ SHIMONO รุ่น SVC-1012C",list.getName());
		assertEquals("990.00",list.getPrice().toString());
		assertEquals("1290.00",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://www.shimono.in.th/wp-content/uploads/2017/06/%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%94%E0%B8%B9%E0%B8%94%E0%B8%9D%E0%B8%B8%E0%B9%88%E0%B8%99-1012C-500x500.gif", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}

//	Filter before,unescape,ifNotEqual
	@Test
	public void checkConditon_MerchantName_Nanajipata_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"supplier\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span class=\\\"normal-price\\\">|,|6\"},{\"filter\":\"ifNotEqual\",\"value\":\"<span class=\\\"sale-price\\\">|,|4\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"sale-price\\\">|,|</span>\"},{\"filter\":\"before\",\"value\":\">\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-price\\\">\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"normal-price through\\\">|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-detail\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<main>\"},{\"filter\":\"inTag\",\"value\":\"<div class=\\\"frame\\\" style=\\\"height: 555px;\\\">\"},{\"filter\":\"textStep\",\"value\":\"1\"},{\"filter\":\"inTag\",\"value\":\"<div class=\\\"col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 pos-relative\\\">\"},{\"filter\":\"between\",\"value\":\"src=\\\"|,|\\\"\"}],\"realProductId\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"show-size\\\">\"},{\"filter\":\"between\",\"value\":\"<input type=\\\"hidden\\\" id=\\\"item-id\\\" value=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"concatId\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"show-size\\\">\"},{\"filter\":\"between\",\"value\":\"<input type=\\\"hidden\\\" id=\\\"item-id\\\" value=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 300074;
		String fileName_html = path + "/WebContent/assets/test/NanajipataTest.html";
		String url ="https://www.nanajipata.com/%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%A0%E0%B8%B2%E0%B8%A2%E0%B9%83%E0%B8%99%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99---%E0%B8%81%E0%B8%A5%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%AA%E0%B9%88%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B8%9E%E0%B8%B1%E0%B8%9A%E0%B9%84%E0%B8%94%E0%B9%89-26x20x16cm-%E0%B8%AA%E0%B8%B5%E0%B8%9F%E0%B9%89%E0%B8%B2-206.html";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("กล่องใส่ของพับได้ 26x20x16cm สีฟ้า (206)",list.getName());
		assertEquals("60",list.getPrice().toString());
		assertEquals("90",list.getBasePrice().toString());
		assertEquals(161,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("https://www.nanajipata.com/upload/product-images/IC-005977/cc3896ee.jpg", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals("206",list.getConcatId().toString());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals("206",list.getRealProductId().toString());
	}

//	Filter after,unescape
	@Test
	public void checkConditon_MerchantName_AltheaThailand_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-shop\\\">\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"product-name col-xs-12\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"special-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"price\\\" id=\\\"product-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"brand-name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Etude House Wonder Pore Freshner (250ml)",list.getName());
		assertEquals("249",list.getPrice().toString());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Etude House",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}

//	Filter inTag,between,after,unescape	
	@Test
	public void checkConditon_MerchantName_AltheaThailand_error_configImgParser() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-shop\\\">\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"product-name col-xs-12\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"special-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"price\\\" id=\\\"product-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"brand-name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "";
		
		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Etude House Wonder Pore Freshner (250ml)",list.getName());
		assertEquals("249",list.getPrice().toString());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Etude House",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getCategory());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter inTag,between,after,unescape	
	@Test
	public void checkConditon_MerchantName_AltheaThailand_error_name() throws IOException{
		String dataFilter = "{\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"special-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"price\\\" id=\\\"product-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"brand-name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";	

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Etude House null",list.getName());
		assertEquals("249",list.getPrice().toString());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Etude House",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getCategory());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter inTag,between,after,unescape	
	@Test
	public void checkConditon_MerchantName_AltheaThailand_error_price() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-shop\\\">\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"product-name col-xs-12\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"brand-name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";	

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Etude House Wonder Pore Freshner (250ml)",list.getName());
		assertEquals(null,list.getPrice());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Etude House",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getCategory());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter inTag,between,after,unescape	
	@Test
	public void checkConditon_MerchantName_AltheaThailand_error_concatword() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-shop\\\">\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"product-name col-xs-12\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"special-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"price\\\" id=\\\"product-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Wonder Pore Freshner (250ml)",list.getName());
		assertEquals("249",list.getPrice().toString());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals(null,list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter inTag,between,after,unescape
	
	@Test
	public void checkConditon_MerchantName_AltheaThailand_error_picUrlstartsWithHTTP() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product-shop\\\">\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"product-name col-xs-12\\\">|,|<\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"special-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<span class=\\\"price\\\" id=\\\"product-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"old-price\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span>|,|4\"},{\"filter\":\"ifNotContain\",\"value\":\"<span>|,|5\"},{\"filter\":\"between\",\"value\":\"<div class=\\\"price\\\" id=\\\"old-price-3114\\\">|,|</span>\"},{\"filter\":\"after\",\"value\":\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"std\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"item active\\\">\"},{\"filter\":\"between\",\"value\":\"<img src=\\\"|,|\\\"\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"brand-name\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"expire\":[{\"filter\":\"inTag\",\"value\":\"<p class=\\\"availability out-of-stock\\\">\"},{\"filter\":\"between\",\"value\":\"<span>|,|</span>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}]}";
		int merchantId = 90089;
		String fileName_html = path + "/WebContent/assets/test/AltheaThailand_starthttp.html";
		String url ="http://th.althea.kr/wonder-pore-freshner-250ml";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Etude House Wonder Pore Freshner (250ml)",list.getName());
		assertEquals("249",list.getPrice().toString());
		assertEquals("528",list.getBasePrice().toString());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("www://d1g2tbmsc7njs7.cloudfront.net/catalog/product/cache/5/image/600x600/9df78eab33525d08d6e5fb8d27136e95/e/t/etude-house_wonder-pore-freshner_main_2.jpg", list.getPictureUrl().toString());
		assertEquals("http://th.althea.kr/wonder-pore-freshner-250ml",list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Etude House",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter replaceSpace
	@Test
	public void checkConditon_MerchantName_GunnerShop_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h1>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"price\\\">\"},{\"filter\":\"inTag\",\"value\":\"<span class=\\\"price-new\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"tab-content\\\">\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"replaceSpace\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"image\\\">\"},{\"filter\":\"between\",\"value\":\"src=\\\"|,|\\\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<ul class=\\\"socials\\\">\"},{\"filter\":\"between\",\"value\":\"href=\\\"//www.facebook.com/|,|Thailand\"}]}";
		int merchantId = 220145;
		String fileName_html = path + "/WebContent/assets/test/GunnerShopTest.html";
		String url ="http://www.fjallraven.co.th/Bags/Kanken/Kanken_Classic/Kanken_Classic_Ox_Red_Royal_Blue";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("Fjallraven Kanken Classic Ox Red/Royal Blue",list.getName());
		assertEquals("3350",list.getPrice().toString());
		assertEquals(null,list.getBasePrice());
		assertEquals(500,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://www.fjallraven.co.th/image/cache/data/Product/Classic/Classic%20Ox%20Red-Royal%20Blue-1046x1509.jpg", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("Fjallraven",list.getConcatWord());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}
	
//	Filter .textStep,contactPrice
	@Test
	public void checkConditon_MerchantName_Inksub_success() throws IOException{
		String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h1>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<htmll>\"},{\"filter\":\"inTag\",\"value\":\"<div id=\\\"pricePanel\\\">\"},{\"filter\":\"ifNotContain\",\"value\":\"<span class=\\\"price\\\">|,|8\"},{\"filter\":\"ifNotContain\",\"value\":\"ไม่ระบุราคา|,|6\"},{\"filter\":\"contactPrice\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"ifNotContain\",\"value\":\"<table class=\\\"proPriceText\\\">|,|10\"},{\"filter\":\"textStep\",\"value\":\"1\"},{\"filter\":\"inTag\",\"value\":\"<table class=\\\"proPriceText\\\">\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<html>\"},{\"filter\":\"inTag\",\"value\":\"<div class=\\\"productDetail\\\">\"},{\"filter\":\"ifContain\",\"value\":\"<span id=\\\"productDetail\\\" style=\\\"\\\">|,|6\"},{\"filter\":\"textStep\",\"value\":\"1\"},{\"filter\":\"between\",\"value\":\"class=\\\"productDetail\\\">|,|</div>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"unescape\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<html>\"},{\"filter\":\"ifNotContain\",\"value\":\"<div class=\\\"product_img_big\\\">|,|6\"},{\"filter\":\"inTag\",\"value\":\"<div class=\\\"product_img_big\\\">\"},{\"filter\":\"between\",\"value\":\"src=\\\"|,|\\\"\"},{\"filter\":\"ifContain\",\"value\":\"product-image|,|9\"},{\"filter\":\"textStep\",\"value\":\"1\"},{\"filter\":\"between\",\"value\":\"class=\\\"proPhoto\\\">|,|</div>\"},{\"filter\":\"between\",\"value\":\"src=\\\"|,|\\\"\"},{\"filter\":\"encodeUrl\",\"value\":\"\"}],\"concatWord\":[{\"filter\":\"inTag\",\"value\":\"<div class=\\\"Link\\\">\"},{\"filter\":\"inTag\",\"value\":\"<span class=\\\"line2\\\">\"},{\"filter\":\"unescape\",\"value\":\"\"},{\"filter\":\"plainText\",\"value\":\"\"}]}";
		int merchantId = 300084;
		String fileName_html = path + "/WebContent/assets/test/InksubTest.html";
		String url ="http://www.ink-sub.com/p_29820_169334_496444_A4-Swing.htm";
		String imageParserClass = "botbackend.parser.image.DefaultImageParser";

		ProductParserBean list = ParserUtil_getParserConfig(readLineFromFile(fileName_html),url,dataFilter,merchantId,imageParserClass);
		assertEquals("เครื่องสกรีนรีดร้อน A4 Swing",list.getName());
		assertEquals("",list.getPrice().toString());
		assertEquals(null,list.getBasePrice());
		assertEquals(101,list.getDescription().length());
		assertEquals(null,list.getExpire());
		assertEquals("http://www.ink-sub.com/userfiles2/product-image15/29820/55cf8d26-f169-4f38-b054-f53f11a84efd/resizestyle2%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%AA%E0%B8%81%E0%B8%A3%E0%B8%B5%E0%B8%99a4.png", list.getPictureUrl().toString());
		assertEquals(url,list.getUrl());
		assertEquals(null,list.getConcatId());
		assertEquals("เครื่องสกรีนรีดร้อน",list.getConcatWord().toString());
		assertEquals(null,list.getKeyword());
		assertEquals(null,list.getUpc());
		assertEquals(null,list.getRealProductId());
	}


	private ProductParserBean ParserUtil_getParserConfig(String html,String url,String data,int merchantId,String imageParserClass){
		try {
			Map<ParserConfigBean.FIELD, List<String[]>> pConfig = ParserUtil.getParserConfig(data);
			ProductParserBean list = ParserUtil.processProductData(html, url, pConfig, loadMerchantConfig(merchantId,imageParserClass));
			return list;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Properties loadMerchantConfig(int merchantId, String imageParserClass) throws SQLException {
		Properties rtn = new Properties();
		if(imageParserClass.equals("botbackend.parser.image.DefaultImageParser")){
			rtn.setProperty("deleteLimit", ""+merchantId);
			rtn.setProperty("imageParserClass", imageParserClass);
		}else{
			rtn.setProperty("deleteLimit", ""+merchantId);
		}
		return rtn;
	}

}
