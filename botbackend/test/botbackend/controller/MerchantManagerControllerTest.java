package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.UserBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.web.MerchantMappingDB;
import botbackend.utils.Util;
@RunWith(MockitoJUnitRunner.class)
public class MerchantManagerControllerTest {

	@Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    @Mock HttpSession session;    
    @Mock MerchantDB merchantDB;
    @Mock MerchantMappingDB mappingDB;
    @Mock MerchantConfigDB merchantconfigDB;
    @Mock UrlPatternDB urlpatternDB;
    @Mock ParserConfigDB parserConfigDB;
    @Mock UserDB userDB;
    @Mock private DatabaseManager databaseManager;
    @Mock MerchantBean merchantBean;
    @Mock MerchantConfigBean merchantconfigBean;

    private MerchantManagerController servlet;
    UserBean userBean = new UserBean();
    int MerchantID = 30001;
    String server = "Bot-One";
    List<MerchantBean> merchantList = new ArrayList<MerchantBean>();
    
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		servlet = new MerchantManagerController();
//		servlet = new MerchantManagerController(databaseManager);
		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(databaseManager.getMerchantMappingDB()).thenReturn(mappingDB);
		when(databaseManager.getUserDB()).thenReturn(userDB);
		when(databaseManager.getMerchantConfigDB()).thenReturn(merchantconfigDB);
		when(databaseManager.getUrlPatternDB()).thenReturn(urlpatternDB);
		when(databaseManager.getParserConfigDB()).thenReturn(parserConfigDB);
		merchantBean.setActive(1);
		merchantBean.setCoreUserConfig("test");
		merchantBean.setCountWebProduct(2);
		merchantBean.setCurrentserver("test");
		merchantBean.setMerchant_id(MerchantID);
	    when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, server)).thenReturn(merchantBean);
	    when(merchantDB.updateMerchantNode(1, 3,MerchantID, "CRAWLER_START", "DATA_UPDATE_START", "WAITING",server )).thenReturn(1);
	    
		userBean.setUserName("test");
		userBean.setUserId(111);
		when(request.getMethod()).thenReturn("POST");
		when(session.getAttribute("userData")).thenReturn(userBean);
	}
//##########################################################		Note  	 #########################################################################	
//	Comment @Test =  because is error or null 
//##########################################################		Solution  	 #########################################################################	
//	1. create constructor in MerchantManagerController 
//	2 Setup	servlet = new MerchantManagerController(databaseManager);
//	private DatabaseManager databaseManager;
//
//	public MerchantManagerController() {
//		this.databaseManager = databaseManager.getDatabaseManager();
//	}
//	
//	public MerchantManagerController(DatabaseManager databaseManager) {
//		this.databaseManager = databaseManager;
//	}	

//##########################################################		saveNote 		#########################################################################	
// NotificationComponant can't mock    
// MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
	
	
//	@Test
	public void saveNote_true_success() throws Exception {
		String merchant = "300001";
		String server = "test";
		String value = "errorMsgTest";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("true", merchant+","+server, value);
	}	
// HaveComma but check StringUtil isBlank
//	@Test
	public void saveNote_merchantAndserver_IsEmpty_butHaveComma() throws Exception {
		String merchant = "";
		String server = "";
		String value = "errorMsgTest";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", merchant+","+server, value);
	}
	
//	@Test
	public void saveNote_merchantAndserver_IsEmpty_NoComma() throws Exception {
		String merchant = "";
		String server = "";
		String value = "errorMsgTest";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", merchant+server, value);
	}
// return null
//	@Test
	public void saveNote_merchatID_IsNull() throws Exception {
		String merchant = "";
		String server = "test";
		String value = "errorMsgTest";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", null, value);
	}
	
	@Test
	public void saveNote_valueEmpty() throws Exception {
		String merchant = "300001";
		String server = "test";
		String value = "";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", merchant+","+server, value);
	}
	
	@Test
	public void saveNote_nocomma() throws Exception {
		String merchant = "300001";
		String server = "test";
		String value = "";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", merchant+server, value);
	}
	@Test
	public void saveNote_comma() throws Exception {
		String merchant = "300001";
		String server = "test";
		String value = "";
		
		when(merchantDB.editNote(merchant, value, server)).thenReturn(1);
		saveNote("false", merchant+",,,"+server, value);
	}
	
	private void saveNote(String header, String data , String value) throws Exception {
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("pk")).thenReturn(data);
		when(request.getParameter("value")).thenReturn(value);

		
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		when(response.getWriter()).thenReturn(pw);
		
		servlet.saveNote(response, request);
		JSONObject jsonObj = new JSONObject();
		String result = sw.getBuffer().toString().trim();
		jsonObj = (JSONObject) new JSONParser().parse(result);
		assertEquals(jsonObj.get("success").toString(),header);
	
	}
	
	
// Can't unit-test 	
//##########################################################		favoriteMerchant 		#########################################################################	
//##########################################################		addQueueMerchant 		#########################################################################	
//##########################################################		stopMerchant 		    #########################################################################	
	
	
//##########################################################		searchNewMerchant 		#########################################################################	
	@Test
	public void searchNewMerchant_success_pageIsZero() throws SQLException, IOException, ParseException {
		String searchParam = "test";
		String activeType = "1";
		String serverType = "test";
		String dataserverType = "test";
		String userFilter = "test";
		String userUpdate = "test";
		String packageFilter= "5";
		
		int page = 0;
		ModelAndView result = searchNewMerchant(searchParam, activeType, serverType, dataserverType, userFilter, userUpdate, packageFilter ,page);
		assertNotNull(result);
	}	
	
	@Test
	public void searchNewMerchant_success_startIsSetZero() throws SQLException, IOException, ParseException {
		String searchParam = "test";
		String activeType = "1";
		String serverType = "test";
		String dataserverType = "test";
		String userFilter = "test";
		String userUpdate = "test";
		String packageFilter= "5";
		
		int page = 10;
		when(merchantDB.countSearchMerchant(searchParam, activeType, serverType, dataserverType, userFilter, packageFilter, userUpdate)).thenReturn(5);
		merchantBean.setActive(1);
		merchantBean.setCoreUserConfig("test");
		merchantBean.setCurrentserver("test");
		merchantBean.setMerchant_id(MerchantID);
		merchantList.add(merchantBean);
		when(merchantDB.getSearchMerchant(searchParam, activeType, serverType, dataserverType, userFilter, packageFilter, userUpdate, 2, 20)).thenReturn(merchantList);
		ModelAndView result = searchNewMerchant(searchParam, activeType, serverType, dataserverType, userFilter, userUpdate, packageFilter ,page);
		assertNotNull(result);
	}	
	
	private ModelAndView searchNewMerchant(String searchParam ,String activeType ,String  serverType,String dataserverType,String  userFilter ,String userUpdate,String packageFilter,int page) throws IOException, ParseException {
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("searchNewMerchant");
		when(request.getParameter("searchParam")).thenReturn(searchParam);
		when(request.getParameter("activeType")).thenReturn(activeType);
		when(request.getParameter("serverType")).thenReturn(serverType);
		when(request.getParameter("dataserverType")).thenReturn(dataserverType);
		when(request.getParameter("userFilter")).thenReturn(userFilter);
		when(request.getParameter("userUpdate")).thenReturn(userUpdate);
		when(request.getParameter("packageFilter")).thenReturn(packageFilter);
		
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		when(response.getWriter()).thenReturn(pw);
		
		return servlet.getNewMerchantData(page, null, "-1", "", "", "", "", "", request, response);

	}	
	
	
//##########################################################		submitNewMerchant 		#########################################################################	
	
	
//	@Test
//	public void submitNewMerchant_success() throws SQLException, IOException, ParseException {
//		String header = "{\"countParserPrice\":1,\"countActionnMatch\":1,\"isFeedParser\":true,\"countMerchantConfig\":1,\"countActionFeed\":1,\"countParserName\":1,\"countActionStartUrl\":1}";
//		String value = "com.ha.bot.parser.DefaultFeedHTMLParser";
//		when(merchantconfigDB.countByFieldByMerchantId(MerchantID, "parserClass")).thenReturn(1);
//		when(parserConfigDB.countByFieldByMerchantId(MerchantID, "name")).thenReturn(1);
//		when(parserConfigDB.countByFieldByMerchantId(MerchantID, "price")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "MATCH")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "FEED")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "STARTURL")).thenReturn(1);
//		submitNewMerchant(header,value);
//	}
//	
//	@Test
//	public void submitNewMerchant_success_valueIsNotDefaultFeedHTMLParser() throws SQLException, IOException, ParseException {
//		String header = "{\"countParserPrice\":1,\"countActionnMatch\":1,\"isFeedParser\":false,\"countMerchantConfig\":1,\"countActionFeed\":1,\"countParserName\":1,\"countActionStartUrl\":1}";
//		String value = "";
//		when(merchantconfigDB.countByFieldByMerchantId(MerchantID, "parserClass")).thenReturn(1);
//		when(parserConfigDB.countByFieldByMerchantId(MerchantID, "name")).thenReturn(1);
//		when(parserConfigDB.countByFieldByMerchantId(MerchantID, "price")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "MATCH")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "FEED")).thenReturn(1);
//		when(urlpatternDB.countActionByMerchantId(MerchantID, "STARTURL")).thenReturn(1);
//		submitNewMerchant(header,value);
//	}
//	
//	@Test
//	public void submitNewMerchant_success_no_NodeTest() throws SQLException, IOException, ParseException {
//		String header = "{\"countParserPrice\":0,\"countActionnMatch\":0,\"isFeedParser\":true,\"countMerchantConfig\":0,\"countActionFeed\":0,\"countParserName\":0,\"countActionStartUrl\":0}";
//		String value = "com.ha.bot.parser.DefaultFeedHTMLParser";
//		List<MerchantBean> countNode = new ArrayList<MerchantBean>();
//		merchantBean.setNodeTest(1);
//		countNode.add(merchantBean);
//		submitNewMerchant(header,value);
//	}
//	
//	@Test
//	public void submitNewMerchant_success_have_NodeTest() throws SQLException, IOException, ParseException {
//		String header = "{\"countParserPrice\":0,\"countActionnMatch\":0,\"isFeedParser\":true,\"countMerchantConfig\":0,\"countActionFeed\":0,\"countParserName\":0,\"countActionStartUrl\":0}";
//		String value = "com.ha.bot.parser.DefaultFeedHTMLParser";
//		List<MerchantBean> countNode = new ArrayList<MerchantBean>();
//		merchantBean.setNodeTest(1);
//	    merchantBean.setActive(1);
//	    merchantBean.setMerchant_id(MerchantID);
//	    merchantBean.setCurrentserver(server);
//	    countNode.add(merchantBean);
//		when(merchantDB.countNodeTestByActive(3)).thenReturn(countNode);
//		submitNewMerchant(header,value);
//	}
//	private void submitNewMerchant(String header ,String value) throws SQLException, IOException, ParseException {
//		when(request.getSession(false)).thenReturn(session);
//		when(request.getParameter("cmd")).thenReturn("submitNewMerchant");
//		when(request.getParameter("merchantId")).thenReturn(Integer.toString(MerchantID));
//		when(request.getParameter("currentserver")).thenReturn(server);
//		when(merchantconfigDB.findMerchantConfigByIdAndField(MerchantID, "parserClass")).thenReturn(merchantconfigBean);
//		when(merchantconfigBean.getValue()).thenReturn(value);
//		
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		when(response.getWriter()).thenReturn(pw);
//		
//		servlet.getNewMerchantData(1, null, "-1", "", "", "", "", "", request, response);
//		
//		JSONObject jsonObj = new JSONObject();
//		String result = sw.getBuffer().toString().trim();
//		jsonObj = (JSONObject) new JSONParser().parse(result);
//		
//		assertEquals(jsonObj.get("header").toString(),header);
//		
//	}	
	
	
	
//##########################################################		getDataByMerchantId 		#########################################################################	
// **  Create constructor class MerchantManagerController   
//
//	public MerchantManagerController() {
//		this.databaseManager = DatabaseManager.getDatabaseManager();
//	}
//	
//	public MerchantManagerController(DatabaseManager databaseManager) {
//		this.databaseManager = databaseManager;
//	}
	
	
//	@Test
//	public void getDataByMerchantId_errorMerchant() throws Exception {
//		String header = "{}" ;
//		getDataByMerchantId(-1, server,header);
//	}
//	
//	@Test
//	public void getDataByMerchantId_success() throws Exception {
//		String header = "{\"server\":null,\"dataUpdateState\":null,\"note\":null,\"dataserver\":null,\"active\":0,\"ownerId\":0,\"packageType\":0,\"ownerName\":null,\"merchantId\":0,\"name\":null,\"state\":null,\"dataUpdateStatus\":null,\"status\":null}" ;
//		getDataByMerchantId(MerchantID, server,header);
//	}
//	
//	private void getDataByMerchantId(int merchantId,String servername,String header) throws Exception {
//		when(request.getSession(false)).thenReturn(session);
//		when(request.getParameter("cmd")).thenReturn("getDataByMerchantId");
//		when(request.getParameter("merchantId")).thenReturn(Integer.toString(merchantId));
//		when(request.getParameter("currentserver")).thenReturn(servername);
//		
//	
//
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		when(response.getWriter()).thenReturn(pw);
//		
//		
//		servlet.getNewMerchantData(1, null, "-1", "", "", "", "", "", request, response);
//
//		JSONObject jsonObj = new JSONObject();
//		String result = sw.getBuffer().toString().trim();
//        jsonObj = (JSONObject) new JSONParser().parse(result);
//        
//		assertEquals(jsonObj.get("header").toString(),header);
//		
//	}	

//##########################################################		editNewMerchant 		#########################################################################	
	

	@Test
	public void editNewMerchant_success_active_1() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "1" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "one-test" ;
		String dataservername = "one-test" ;
		String currentserver = "one-test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		when(merchantDB.updateNewMerchant(merchantBean)).thenReturn(1);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_3() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "3" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "one-test" ;
		String dataservername = "one-test" ;
		String currentserver = "one-test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		when(merchantDB.updateNewMerchant(merchantBean)).thenReturn(1);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_5() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "5" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "one-test" ;
		String dataservername = "one-test" ;
		String currentserver = "one-test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		when(merchantDB.updateNewMerchant(merchantBean)).thenReturn(1);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "one-test" ;
		String dataservername = "one-test" ;
		String currentserver = "one-test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_AllServerEmpty() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "" ;
		String dataservername = "" ;
		String currentserver = "" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_ServernameAndCurrentserverEmpty() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "test" ;
		String dataservername = "" ;
		String currentserver = "" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_DataServerEmpty() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "test" ;
		String dataservername = "" ;
		String currentserver = "test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_CurrentserverEmpty() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "test" ;
		String dataservername = "test" ;
		String currentserver = "" ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_AllNull() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = null ;
		String dataservername = null ;
		String currentserver = null ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}
	
	@Test
	public void editNewMerchant_success_active_4_setState_PARSER_FINISH_CurrentserverNull() throws SQLException {
		String merchantName = "ADtest" ;
		String packageType = "3" ;
		String active = "4" ;
		String merchantId = Integer.toString(MerchantID);
		String servername = "test" ;
		String dataservername = "test" ;
		String currentserver = null ;
		String ownerName = "test" ;
		int userId = 12;
		
		when(merchantDB.getMerchantByIdAndServerWithOwner(MerchantID, currentserver)).thenReturn(merchantBean);
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}

	@Test
	public void editNewMerchant_AllEmpty() throws SQLException {
		String merchantName = "" ;
		String packageType = "" ;
		String active = "" ;
		String merchantId = "" ;
		String servername = "" ;
		String dataservername = "" ;
		String currentserver = "" ;
		String ownerName = "" ;
		int userId = -1;
		
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNull(result);
	}
	
	@Test
	public void editNewMerchant_merchantListISNull() throws SQLException {
		String merchantName = "test" ;
		String packageType = "3" ;
		String active = "1" ;
		String merchantId = "30001" ;
		String servername = "test" ;
		String dataservername = "test" ;
		String currentserver = "test" ;
		String ownerName = "test" ;
		int userId = 12;
		
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNotNull(result);
	}

	@Test
	public void editNewMerchant_ownerNameNull() throws SQLException {
		String merchantName = "test" ;
		String packageType = "3" ;
		String active = "1" ;
		String merchantId = "30001" ;
		String servername = "test" ;
		String dataservername = "test" ;
		String currentserver = "test" ;
		String ownerName = null ;
		int userId = -1;
		
		ModelAndView result = editNewMerchant(merchantName, packageType, active, merchantId, servername, dataservername, currentserver, ownerName,userId);
		assertNull(result);
	}
	
	private ModelAndView editNewMerchant(String merchantName ,String packageType ,String active , String merchantId,String servername,String dataservername,String currentserver,String ownerName,int userId) throws SQLException {
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("editNewMerchant");
		
		when(request.getParameter("merchantName")).thenReturn(merchantName);
		when(request.getParameter("packageType")).thenReturn(packageType);
		when(request.getParameter("active")).thenReturn(active);
		when(request.getParameter("merchantID")).thenReturn(merchantId);
		when(request.getParameter("servername")).thenReturn(servername);
		when(request.getParameter("dataservername")).thenReturn(dataservername);
		when(request.getParameter("currentserver")).thenReturn(currentserver);
		when(request.getParameter("ownerName")).thenReturn(ownerName);
		userBean.setUserId(userId);
		when(userDB.getUserIDbyUserName(ownerName)).thenReturn(userBean);
		
		return servlet.getNewMerchantData(1, null, "-1", "", "", "", "", "", request, response);
		
	}
//##########################################################		addNewMerchant 		#########################################################################
// warning is not initial 
//	@Test
	public void addNewMerchant_merchantTypeIs() throws SQLException {
		String merchantName = "PlatinumPDA";
		int packageType = 0 ;
		String saleName = "test";
		String siteUrl = "www.priceza.com/test";
		String merchantType = "test";
		int merchantId = 3000011;
		int merchantIdBackEnd = 400000+1;
		when(mappingDB.getMaxMerchantIdByRange(Util.getMerchantIdStartRange(), Util.getMerchantIdEndRange())).thenReturn(merchantId);
		when(merchantDB.getMaxMerchantIdByRange(Util.getMerchantIdStartRange(), Util.getMerchantIdEndRange())).thenReturn(merchantIdBackEnd);
		when(mappingDB.getSlug(Util.formatSlugName(merchantName))).thenReturn(merchantList);
		
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType.toLowerCase());
		boolean newpage = (boolean) result.getModel().get("newPage");
		
		
		List<MerchantBean> mList = (List<MerchantBean>) result.getModel().get("merchantList");
		assertEquals(merchantName, mList.get(0).getName());
		assertEquals("merchantmanager", result.getViewName());
		assertEquals(true, newpage);
		
	}
// 	error	
//	@Test
	public void addNewMerchant_merchantTypeIsTesttoLowerCase() throws SQLException {
		String merchantName = "PlatinumPDA";
		int packageType = 0 ;
		String saleName = "test";
		String siteUrl = "www.priceza.com/test";
		String merchantType = "test";
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType.toLowerCase());
		boolean newpage = (boolean) result.getModel().get("newPage");
		
		
		List<MerchantBean> mList = (List<MerchantBean>) result.getModel().get("merchantList");
		assertEquals(merchantName, mList.get(0).getName());
		assertEquals("merchantmanager", result.getViewName());
		assertEquals(true, newpage);
		
	}
// not set toLowerCase merchantType 
//	@Test
	public void addNewMerchant_merchantTypeIsTesttoUpperCase() throws SQLException {
		String merchantName = "PlatinumPDA" ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "test" ;
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType.toUpperCase());
		boolean newpage = (boolean) result.getModel().get("newPage");
	
		
		List<MerchantBean> mList = (List<MerchantBean>) result.getModel().get("merchantList");
		assertEquals(merchantName, mList.get(0).getName());
		assertEquals("merchantmanager", result.getViewName());
		assertEquals(true, newpage);
	}
//	error
//	@Test
	public void addNewMerchant_merchantTypeIsTesNormalCase() throws SQLException {
		String merchantName = "PlatinumPDA" ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "test";
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType);
	
		
		List<MerchantBean> mList = (List<MerchantBean>) result.getModel().get("merchantList");
		assertEquals(merchantName, mList.get(0).getName());

	}
// error	
//	@Test
	public void addNewMerchant_merchantTypeIsCustomer() throws SQLException {
		String merchantName = "PlatinumPDA" ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "customer" ;
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType);
		boolean newpage = (boolean) result.getModel().get("newPage");
	
		
		List<MerchantBean> mList = (List<MerchantBean>) result.getModel().get("merchantList");
		assertEquals(merchantName, mList.get(0).getName());
		assertEquals("merchantmanager", result.getViewName());
		assertEquals(true, newpage);
	}
//	when merchantName is blank no condition else 
//	@Test
	public void addNewMerchant_merchantNameisBlank() throws SQLException {
		String merchantName = "" ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "customer" ;
		
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType);
		assertNull(result.getModel().get("merchantList"));
	}
//	merchantName is Null
//	@Test
	public void addNewMerchant_merchantNameisNull() throws SQLException {
		String merchantName = null ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "customer" ;
		
		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType);
		assertNull(result);
	}
	
//	merchantName is ,,
	@Test
	public void addNewMerchant_merchantDoubleComma() throws SQLException {
		String merchantName = ",," ;
		int packageType = 0 ;
		String saleName = "test" ;
		String siteUrl = "www.priceza.com/test" ;
		String merchantType = "customer" ;

		ModelAndView result = addNewMerchant(merchantName, packageType, saleName, siteUrl, merchantType);
		assertNotNull(result);
	}
	
	private ModelAndView addNewMerchant(String merchantName ,int packageType ,String saleName,String siteUrl,String merchantType) throws SQLException {
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("addNewMerchant");
		
		when(request.getParameter("merchantName")).thenReturn(merchantName);
		when(request.getParameter("packageType")).thenReturn("");
		when(request.getParameter("saleName")).thenReturn(saleName);
		when(request.getParameter("siteUrl")).thenReturn(siteUrl);
		when(request.getParameter("merchantType")).thenReturn(merchantType);
		
		
		return servlet.getNewMerchantData(1, null, "-1", "", "", "", "", "", request, response);
	}
	
	
//##########################################################		getModalAndView 		#########################################################################	
	@Test
  	public void getModalAndView_all() throws Exception {
	  ModelAndView result = getModalAndView("all");		
	  boolean newpage = (boolean) result.getModel().get("newPage");
	  assertEquals("merchantmanagerpage", result.getViewName());
	  assertEquals(true, newpage);
  }

	@Test
	public void getModalAndView_all_and_merchantconfig() throws Exception {
		ModelAndView result = getModalAndView("all|merchant.merchantmanager");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantmanagerpage", result.getViewName());
		assertEquals(true, newpage);
	}
	
	@Test
	public void getModalAndView_all_except() throws Exception {
		ModelAndView result = getModalAndView("task.taskmanager");
		assertEquals("redirect:/admin/dashboard", result.getViewName());
	}
	
	@Test
	public void getModalAndView_merchantconfig() throws Exception {
		ModelAndView result = getModalAndView("merchant.merchantconfig|merchant.merchantmanager|crawlerconfig.parser");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantmanagerpage", result.getViewName());
		assertEquals(true, newpage);
	}

	private ModelAndView getModalAndView(String rolePermission) {

		when(request.getSession(false)).thenReturn(session);
		HashMap<String, String> permission = new HashMap<String, String>();
		String[] permissions = rolePermission.split("\\|");
		for (String p : permissions) {
			permission.put(p, "");
		}
		when(session.getAttribute("Permission")).thenReturn(permission);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ModelAndView result = null;
		try {

			when(response.getWriter()).thenReturn(pw);
			result = servlet.getModalAndView(request, response);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		return result;
	}
	

}
