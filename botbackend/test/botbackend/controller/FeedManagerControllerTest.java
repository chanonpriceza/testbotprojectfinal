package botbackend.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.TaskBean;
import botbackend.bean.UserBean;
import botbackend.db.TaskDB;

public class FeedManagerControllerTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession session;  
    @Mock 
    TaskDB taskDB;
    
//    private FeedManagerController servlet;
//    private List<TaskBean> taskList ;
//    private String merchantId = "70024";
//    private String urlLink = "https://www.priceza.co.id";
//	private String feedUsername = "priceza";
//	private String feedPassword = "xxxxxxxx";
//	private String chkGz = "1";
//	private String productTag = "<products>";
//	private String[] tags = {"test_1","test_2","test_3","test_4","test_5","test_6"};
//	private String pageParam = "page";
//	private String limitPage = "500";
//	private String pageOffset = "";
//	private String feedType = "xml";
//	private String actionType = "";
//	
//	@Before
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		servlet = new FeedManagerController(taskDB);
//		
//		taskList = new ArrayList<TaskBean>();
//		for(int i=1;i<3;i++){
//			TaskBean tmp = new TaskBean();
//			tmp.setId(i);
//			tmp.setMerchantId(Integer.parseInt(merchantId)+i);
//			tmp.setProcess("feedCatList");
//			tmp.setStatus("DONE");
//			tmp.setResult("0");
//			tmp.setData("{}");
//			
//			taskList.add(tmp);
//		}
//	}
//	
//	@Test
//	public void getModalAndView_all_except(){
//		 ModelAndView result = getModalAndView("all|-feed");		
//		 boolean newpage = (boolean) result.getModel().get("permissionNotMatch");
//		 
//     	 assertEquals("loginpage", result.getViewName());
//	     assertEquals(true, newpage);
//	}
//	
//	@Test
//	public void getModalAndView_urlPattern(){
//		 ModelAndView result = getModalAndView("merchantconfig|urlpattern|parser");
//		 boolean newpage = (boolean) result.getModel().get("permissionNotMatch");
//		 
//     	 assertEquals("loginpage", result.getViewName());
//	     assertEquals(true, newpage);
//	}
//	
//	@Test
//	public void monitorData_addNewTask() {
//		UserBean userBean = new UserBean();
//		userBean.setUserName("test");
//		
//		when(request.getParameter("urlLink")).thenReturn(urlLink);
//		when(request.getParameter("feedUsername")).thenReturn(feedUsername);
//		when(request.getParameter("feedPassword")).thenReturn(feedPassword);
//		when(request.getParameter("chkGz")).thenReturn(chkGz);
//		when(request.getParameter("productTag")).thenReturn(productTag);
//		when(request.getParameterValues("tags")).thenReturn(tags);
//		when(request.getParameter("pageParam")).thenReturn(pageParam);
//		when(request.getParameter("limitPage")).thenReturn(limitPage);
//		when(request.getParameter("pageOffset")).thenReturn(pageOffset);
//		when(request.getParameter("feedType")).thenReturn(feedType);
//		when(request.getParameter("actionType")).thenReturn(actionType);
//		
//		when(request.getSession(false)).thenReturn(session);
//		when(session.getAttribute("userData")).thenReturn(userBean);
//		when(request.getParameter("cmd")).thenReturn("addNewTask");
//		when(request.getParameter("merchantId")).thenReturn(merchantId);
//		try {
//			when(taskDB.countMonitorDataByProcess(Mockito.anyList())).thenReturn(1);
//			when(taskDB.insertTask(Mockito.anyObject())).thenReturn(1);
//			when(taskDB.getMonitorDataByProcess(Mockito.anyList(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(taskList);
//			
//			ModelAndView model =servlet.getMonitorData(1, request, response);			
//			List<TaskBean> result = (List<TaskBean>) model.getModelMap().get("taskList");
//			assertEquals(2, result.size());
//			verify(taskDB).insertTask(Mockito.anyObject());
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	
//	@Test
//	public void exportCSV() {
//		when(request.getParameter("cmd")).thenReturn("exportCSV");
//		when(request.getParameter("merchantId")).thenReturn(merchantId);
//		when(request.getParameter("fileName")).thenReturn("FeedCSV.csv");
//		try {
//			
//			StringWriter sw = new StringWriter();
//			PrintWriter pw = new PrintWriter(sw);
//			when(response.getWriter()).thenReturn(pw);
//			servlet.exportCSV(request, response);
//			
//			String result = sw.getBuffer().toString().trim();
//			assertNotEquals(0, result.length());
//			assertNotEquals("This file now is not exist.", result);
//			assertNotEquals("Can not found File", result);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	@Test
//	public void exportCSV_nonExist() {
//		when(request.getParameter("cmd")).thenReturn("exportCSV");
//		when(request.getParameter("merchantId")).thenReturn(merchantId);
//		when(request.getParameter("fileName")).thenReturn("FeedCSV_X.csv");
//		try {
//			
//			StringWriter sw = new StringWriter();
//			PrintWriter pw = new PrintWriter(sw);
//			when(response.getWriter()).thenReturn(pw);
//			servlet.exportCSV(request, response);
//			
//			String result = sw.getBuffer().toString().trim();
//			assertEquals("This file now is not exist.", result);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	@Test
//	public void exportCSV_fileName_null() {
//		when(request.getParameter("cmd")).thenReturn("exportCSV");
//		when(request.getParameter("merchantId")).thenReturn(merchantId);
//		when(request.getParameter("fileName")).thenReturn(null);
//		try {
//			
//			StringWriter sw = new StringWriter();
//			PrintWriter pw = new PrintWriter(sw);
//			when(response.getWriter()).thenReturn(pw);
//			servlet.exportCSV(request, response);
//			
//			String result = sw.getBuffer().toString().trim();
//			assertEquals("Can not found File", result);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	
//	private ModelAndView getModalAndView(String rolePermission) {
//		
//		when(request.getSession(false)).thenReturn(session);
//		HashMap<String, String> permission  = new HashMap<String, String>();
//		String[] permissions = rolePermission.split("\\|");
//		for(String p : permissions){
//			permission.put(p, "");
//		}
//		when(session.getAttribute("Permission")).thenReturn(permission);
//
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		ModelAndView result = null;
//         try {
//        	
//        	when(response.getWriter()).thenReturn(pw);
//        	result = servlet.getModalAndView(request,response);
//		} catch (Exception e) {
//			fail();  
//			e.printStackTrace();
//		} 
//      return result;
//	}

}
