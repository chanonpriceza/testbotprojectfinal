package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.UserBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.utils.DatabaseManager;
@RunWith(MockitoJUnitRunner.class)
public class MerchantConfigControllerTest {
	@Mock private HttpServletRequest request;
	@Mock private HttpServletResponse response;
	
	@Mock private MerchantConfigController servlet;
	
	@Mock private DatabaseManager databaseManager;
	@Mock private MerchantConfigDB merchantconfigDB;
	@Mock private MerchantDB merchantDB;
	@Mock private HttpSession session;

	final int merchantId = 55555;
	final int active = 1;
	final String errMsg = "test";
	MerchantBean merchantBean = new MerchantBean();
	MerchantConfigBean merchantConfigBean = new MerchantConfigBean(); 
	List<MerchantConfigBean> merchantconfigList = new ArrayList<MerchantConfigBean>();
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		when(databaseManager.getMerchantDB()).thenReturn(merchantDB);
		when(request.getMethod()).thenReturn("POST");
		servlet = new MerchantConfigController();
	}
	
	private ModelAndView getModalAndView(String rolePermission) {

		when(request.getSession(false)).thenReturn(session);
		HashMap<String, String> permission = new HashMap<String, String>();
		String[] permissions = rolePermission.split("\\|");
		for (String p : permissions) {
			permission.put(p, "");
		}
		when(session.getAttribute("Permission")).thenReturn(permission);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ModelAndView result = null;
		try {

			when(response.getWriter()).thenReturn(pw);
			result = servlet.getModalAndView(request, response);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		return result;
	}

	@Test
	public void getModalAndView_all() {
		ModelAndView result = getModalAndView("all");		
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantconfig", result.getViewName());
		assertEquals(true, newpage);
	}

	@Test
	public void getModalAndView_all_and_merchantconfig() {
		ModelAndView result = getModalAndView("all|merchant.merchantconfig");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantconfig", result.getViewName());
		assertEquals(true, newpage);
	}
	
	@Test
	public void getModalAndView_all_except() {
		ModelAndView result = getModalAndView("task.taskmanager");
		boolean newpage = (boolean) result.getModel().get("permissionNotMatch");
		assertEquals("loginpage", result.getViewName());
		assertEquals(true, newpage);
	}
	
	@Test
	public void getModalAndView_merchantconfig() {
		ModelAndView result = getModalAndView("merchant.merchantconfig|crawlerconfig.urlpattern|crawlerconfig.parser");
		boolean newpage = (boolean) result.getModel().get("newPage");
		assertEquals("merchantconfig", result.getViewName());
		assertEquals(true, newpage);
	}


	@Test
	public void getModalAndViewAjax_getDataByMerchantId_success_mode_update() {
		try {
			merchantBean.setId(merchantId);
			merchantBean.setActive(active);
			merchantBean.setErrorMessage(errMsg);
			
			merchantConfigBean.setId(1);
			merchantConfigBean.setField("forceDelete");
			merchantConfigBean.setValue("true");
			merchantconfigList.add(merchantConfigBean);
			

			getModalAndViewAjax_getDataByMerchantId(merchantId, "success",merchantBean,merchantconfigList);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getModalAndViewAjax_getDataByMerchantId_mode_insert() {
		try {
			merchantBean.setId(merchantId);
			merchantBean.setActive(active);
			merchantBean.setErrorMessage(errMsg);
			
			merchantConfigBean.setId(1);
			merchantConfigBean.setField("forceDelete");
			merchantConfigBean.setValue("true");
			merchantconfigList.add(merchantConfigBean);
			
			getModalAndViewAjax_getDataByMerchantId(merchantId, "success" ,merchantBean,merchantconfigList);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

// no condition else check merchantID is empty	
//	@Test
	public void getModalAndViewAjax_getDataByMerchantId_success_noDataConfig() {
		try {
			getModalAndViewAjax_getDataByMerchantId(Integer.parseInt(""), "error", merchantBean, merchantconfigList);
		} catch (NumberFormatException | ParseException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void getModalAndViewAjax_getDataByMerchantId_fail_2() {
		try {
			getModalAndViewAjax_getDataByMerchantId(-1, "error",null,null);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	@Test
	public void getModalAndViewAjax_saveTable_success() throws Exception {
		String header = "success";
		String visibleConfig = "'maxDepth','parserClass','deleteLimit','productUpdateLimit','forceUpdateKeyword','forceUpdateCategory','enableUpdateDesc','enableCookies','enableUpdateBasePrice','merchantName','currencyRate'";
		JSONArray arr = new JSONArray();
		arr.add(setMerchantConfig("maxDepth", "20"));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(merchantId),visibleConfig);
	}
// null json array	
//	@Test
	public void getModalAndViewAjax_saveTable_null_JSONArray() throws Exception {
		String header = "success";
		String visibleConfig = "'maxDepth','parserClass','deleteLimit','productUpdateLimit','forceUpdateKeyword','forceUpdateCategory','enableUpdateDesc','enableCookies','enableUpdateBasePrice','merchantName','currencyRate'";
		JSONArray arr = new JSONArray();
		getModalAndViewAjax_saveTable(null, header, Integer.toString(merchantId),visibleConfig);
	}
	
// not check null string ����� success ����ôѡ���� isNotBlank ���� ArrayList ���� result = [] 
//	@Test
	public void getModalAndViewAjax_saveTable_null_visibleConfig() throws Exception {
		String header = "success";
		String visibleConfig = null;
		JSONArray arr = new JSONArray();
		arr.add(setMerchantConfig("maxDepth", "20"));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(merchantId),visibleConfig);
	}
	 
//	@Test
	public void getModalAndViewAjax_saveTable_empty_visibleConfig() throws Exception {
		String header = "fail";
		String visibleConfig = ",,,,,,,,,,,,,,,,,,,,";
		JSONArray arr = new JSONArray();
		arr.add(setMerchantConfig("maxDepth", "20"));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(merchantId),visibleConfig);
	}


//	case  merchantconfigList is null
//	@Test
	public void getModalAndViewAjax_saveTable_null_visibleConfig_config_isempty() throws Exception {
		String header = "success-empty";
		String visibleConfig = ",,,,,,,,,,,,,,,,,,,,";
		JSONArray arr = new JSONArray();
		// condition, group, value, action, categoryId, keyword
		arr.add(setMerchantConfig("maxDepth", " "));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(merchantId),visibleConfig);
	}

//	case  merchantconfigList is null
//	@Test
	public void getModalAndViewAjax_saveTable_successEmpty_2() throws Exception {
		String header = "success-empty";
		JSONArray arr = new JSONArray();
		// condition, group, value, action, categoryId, keyword
		arr.add(setMerchantConfig("", ""));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(merchantId),"");
	}
	
//	case  merchantconfigList is []
	@Test
	public void getModalAndViewAjax_saveTable_fail_2() throws Exception {
		String header = "fail";
		JSONArray arr = new JSONArray();
		// condition, group, value, action, categoryId, keyword
		arr.add(setMerchantConfig("", ""));
		getModalAndViewAjax_saveTable(arr, header, Integer.toString(-1),"");
	}

	private void getModalAndViewAjax_saveTable(JSONArray arr, String header, String merchantId ,String visibleConfig) throws ParseException {
		UserBean userBean = new UserBean();
		userBean.setUserName("test");
		when(session.getAttribute("userData")).thenReturn(userBean);
		when(request.getSession(false)).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("saveTable");
		when(request.getParameter("merchantId")).thenReturn(merchantId);
		when(request.getParameter("visibleConfig")).thenReturn(visibleConfig);
		
		if(arr != null && arr.size() > 0) {
			when(request.getParameter("jsonData")).thenReturn(arr.toString());			
		}else {
			when(request.getParameter("jsonData")).thenReturn("[]");						
		}

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		try {
			when(response.getWriter()).thenReturn(pw);
			servlet.getModalAndViewAjax(request, response);
			
			String result = sw.getBuffer().toString().trim();
			JSONObject jsonObj = new JSONObject();
			if(StringUtils.isNotBlank(result)){			
				jsonObj = (JSONObject) new JSONParser().parse(result);
				assertEquals(header, jsonObj.get("header"));
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
	}

	private JSONObject setMerchantConfig(String field, String value) throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("field", field);
		obj.put("value", value);
		return obj;

	}

	private void getModalAndViewAjax_getDataByMerchantId(int merchantId, String header ,MerchantBean merchantBean ,List<MerchantConfigBean> merchantconfigList) throws ParseException, NumberFormatException, SQLException {
		
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("cmd")).thenReturn("getDataByMerchantId");
		when(request.getParameter("merchantId")).thenReturn(String.valueOf(merchantId));	
		
		if(merchantId > 0) {
			when(merchantDB.getMerchantByMerchantId(merchantId)).thenReturn(merchantBean);
			assertEquals(merchantId, merchantBean.getId());					
			assertEquals(active, merchantBean.getActive());					
			assertEquals(errMsg, merchantBean.getErrorMessage());
			assertNotNull(merchantconfigList);
		
		}else {
			when(merchantDB.getMerchantByMerchantId(merchantId)).thenReturn(null);
			when(merchantconfigDB.findMerchantConfig(merchantId)).thenReturn(merchantconfigList);
			assertEquals("error", header);										
			assertNull(merchantBean);			
			assertNull(merchantconfigList);			
		}
		
		
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		try {
			
			when(response.getWriter()).thenReturn(pw);
			servlet.getModalAndViewAjax(request, response);
			
			String result = sw.getBuffer().toString().trim();
			JSONObject jsonObj = (JSONObject) new JSONParser().parse(result);
			if (header.equals("success")) {
				if(jsonObj.get("mode").equals("update")){
					assertEquals(header, jsonObj.get("header"));
					assertEquals("update", jsonObj.get("mode"));
				}else{
					assertEquals(header, jsonObj.get("header"));
					assertEquals("insert", jsonObj.get("mode"));
				}
			}else {
				assertEquals("{}", jsonObj.toString());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
	}
}
