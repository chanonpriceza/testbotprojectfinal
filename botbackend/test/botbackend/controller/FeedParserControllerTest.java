package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.ParserConfigBean;
import botbackend.bean.TaskBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.TaskDB;
import botbackend.db.utils.DatabaseManager;


public class FeedParserControllerTest {

	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	@Mock
	HttpSession session;
	
	private MerchantDB  merchantDB;
	private MerchantConfigDB configDB;
	private ParserConfigDB pDB;
	private FeedParserController servlet;
	
	@SuppressWarnings("serial")
	Map<String,Integer> merchant = new HashMap<String,Integer>(){{
		put("xml", 99000002);
		put("csv", 99000003);
		put("json", 99000004);
	}};
	
//	@Before
//	public void setUp() throws Exception {
//		merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
//		configDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
//		pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
//		servlet = new FeedParserController();
//		
//		MockitoAnnotations.initMocks(this);
//		when(request.getMethod()).thenReturn("POST");
//		
//		for(String type: merchant.keySet()){
//			String name = type +"_test";
//			int id = merchant.get(type);
//			insertNewMerchant(id, name, 0);
//			insertFeedConfig(id, type);
//		}
//	   	
//	}
//
//	private void insertNewMerchant(int id, String name, int active) throws NumberFormatException, SQLException{
//		
//		MerchantBean m = new MerchantBean();
//		m.setId(id);
//		m.setName(name);
//		m.setActive(active);
//		m.setState("CRAWLER_START");
//		m.setStatus("WAITING");
//		m.setDataUpdateState("DATA_UPDATE_START");
//		m.setDataUpdateStatus("WAITING");
//		m.setPackageType(0);
//		m.setWceErrorCount(0);
//		m.setDueErrorCount(0);
//		m.setServer("Bot13-bot");
//		m.setNodeTest(0);
//		m.setOwnerId(0);
//		
//		MerchantBean data = merchantDB.getMerchantByMerchantId(id);
//		if(data == null){
//    		merchantDB.insertNewMerchant(m);
//    	}else if (data.getActive() != active){
//    		merchantDB.updateNewMerchant(m);
//    	}
//		
//
//	}
//	
//	private void insertFeedConfig(int id, String type) throws NumberFormatException, SQLException{
//		List<MerchantConfigBean> mConfig =  configDB.findMerchantConfig(id);
//		
//		if(mConfig != null && !mConfig.isEmpty()){
//			for(MerchantConfigBean b : mConfig){
//				if(b.getField().equals("feedType") && b.getValue().equals(type)){
//					return;
//				}else if(b.getField().equals("feedType")){
//					configDB.deleteMerchantConfigByMerchantId(id);
//					break;
//				}
//			}
//		}
//		
//		@SuppressWarnings("serial")
//		Map<String,String> config = new HashMap<String,String>(){{
//			put("feedType", type);
//			put("feedCheckGz", "");
//			put("feedUrl", "http://www.priceza.com/");
//			put("feedUsername", "");
//			put("feedPassword", "");
//			put("feedCrawlerParam", "");
//			put("feedCrawlerClass", "");
//		}};
//		
//		List<MerchantConfigBean> dataList = new ArrayList<MerchantConfigBean>();
//		for(String field : config.keySet()){
//			MerchantConfigBean mc = new MerchantConfigBean();
//			mc.setMerchantId(id);
//			mc.setField(field);
//			mc.setValue(config.get(field));
//			
//			dataList.add(mc);
//		}
//		configDB.insertMerchantConfigList(dataList);
//	}
//	
//	
//	@Test
//	public void getModalAndView_all() {
//		String permission = "all";
//		ModelAndView result = getModalAndView(permission);
//		
//		boolean isBlank = (boolean) result.getModel().get("");
//		assertEquals("feedparser", result.getViewName());
//		assertEquals(true, isBlank);
//	}
//	
//	@Test
//	public void getModalAndView_feedParser() {
//		String permission = "merchantconfig|urlpattern|parser|feedparser";
//		ModelAndView result = getModalAndView(permission);
//		
//		boolean isBlank = (boolean) result.getModel().get("");
//		assertEquals("feedparser", result.getViewName());
//		assertEquals(true, isBlank);
//	}
//
//	@Test
//	public void getModalAndView_all_except(){
//		String permission = "all|-feedparser";
//		ModelAndView result = getModalAndView(permission);
//		
//		boolean isNotMatch = (boolean) result.getModel().get("permissionNotMatch");
//
//		assertEquals("loginpage", result.getViewName());
//		assertEquals(true, isNotMatch);
//	}
//
//	
//	
//	private ModelAndView getModalAndView(String rolePermission) {
//
//		HashMap<String, String> permission = new HashMap<String, String>();
//		String[] permissions = rolePermission.split("\\|");
//		for (String p : permissions) {
//			permission.put(p, "");
//		}
//		
//		when(request.getSession(false)).thenReturn(session);
//		when(session.getAttribute("Permission")).thenReturn(permission);
//
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		ModelAndView result = null;
//		
//		try {
//			when(response.getWriter()).thenReturn(pw);
//			result = servlet.getModelAndView(request);
//			
//		} catch (IOException | ServletException | SQLException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//		return result;
//	}
//	
//	@Test
//	public void getMerchant_exist() {
//		getMerchant(merchant.get("xml"), "success");
//	}
//	
//	@Test
//	public void getMerchant_nonExist() {
//		getMerchant(978585, "notFoundMerchant");
//	}
//	
//	private void getMerchant(int id, String header){
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		
//		try {
//			when(response.getWriter()).thenReturn(pw);			
//			servlet.getMerchant(response,id);		
//			
//			String result = sw.getBuffer().toString().trim();
//			JSONObject jsonObj = new JSONObject(result);
//			
//			assertEquals(header, jsonObj.get("header"));
//			if(header.equals("success")){
//				assertNotEquals(null, jsonObj.get("merchant"));
//			}
//			
//		} catch (IOException |NumberFormatException | SQLException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		}finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//	}
//	
//	@Test
//	public void saveParser_nonActive_merchant() {
//		String[] old_data =  {"xmltag", "Name"};
//		String type = "xml";
//		
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedName", new String[][]{{"xmltag", "Name"}});
//		data.put("feedPrice",new String[][]{{"xmltag", "Price"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"xmltag", "DESC"}});
//		data.put("feedConcatId", new String[][]{{"xmltag", "ID"}});
//		data.put("feedConcatWord", new String[][]{{"xmltag", "Rate"}});
//	
//		
//		try {
//			int id = merchant.get(type);
//			insertNewMerchant(id, type ,0);
//			
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			
//			JSONObject jsonObj =  setMockFeedParser(data,jsonConfigObject, id);
//			assertEquals("success", jsonObj.get("header"));
//			
//			List<ParserConfigBean> new_data = pDB.getByMerchantId(id);
//			for (ParserConfigBean parser : new_data){
//				if(!parser.getFieldType().equals("feedName")){
//					continue;
//				}
//				assertEquals(old_data[0], parser.getFilterType());
//				assertEquals(old_data[1], parser.getValue());
//				break;
//			}
//		} catch (NumberFormatException | SQLException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	

	
//	have no permission
//	@Test
//	public void saveParser_active_merchant() {	
//		Random rand = new Random(new java.util.Date().getTime());
//		int  n = rand.nextInt(50) + 1;
//		
//		String type = "xml";
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedName", new String[][]{{"xmltag", "Name"+n}});
//		data.put("feedPrice",new String[][]{{"xmltag", "Price"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"xmltag", "DESC"}});
//		data.put("feedConcatId", new String[][]{{"xmltag", "ID"+n}});
//		data.put("feedConcatWord", new String[][]{{"xmltag", "Rate"+n}});
//		
//		try {
//			int id = merchant.get(type);
//			insertNewMerchant(id, type , 1);
//			
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			JSONObject jsonObj =  setMockFeedParser(data,jsonConfigObject,id);
//			assertEquals("success", jsonObj.get("header"));
//			
//			List<ParserConfigBean> new_data = pDB.getByMerchantId(id);
//			for (ParserConfigBean parser : new_data){
//				String field = parser.getFieldType();
//				String[][] _data = data.get(field);
//				if(field.equals("feedName") && parser.getFilterType().equals("xmltag")){
//					assertNotEquals(_data[0][1], parser.getValue());
//				}else if(field.equals("feedConcatId") && parser.getFilterType().equals("xmltag")){
//					assertNotEquals(_data[0][1], parser.getValue());
//				}else if(field.equals("feedConcatWord") && parser.getFilterType().equals("xmltag")){
//					assertNotEquals(_data[0][1], parser.getValue());
//				}
//			}
//		} catch (NumberFormatException | SQLException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
	
//	@Test
//	public void saveParser_fieldBlank() {	
//		String type = "xml";
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedName", new String[][]{{"xmltag", "Name"}});
//		data.put("feedPrice",new String[][]{{"xmltag", "' '"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"xmltag", "DESC"}});
//		data.put("feedConcatId", new String[][]{{"xmltag", "ID"}});
//		data.put("feedConcatWord", new String[][]{{"xmltag", "Rate"}});
//		
//		try {
//			int id = merchant.get(type);
//			insertNewMerchant(id, type , 1);
//			
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			JSONObject jsonObj =  setMockFeedParser(data,jsonConfigObject, id);
//			assertEquals("success", jsonObj.get("header"));
//			
//			List<ParserConfigBean> new_data = pDB.getByMerchantId(id);
//			for (ParserConfigBean parser : new_data){
//				String field = parser.getFieldType();
//				if(field.equals("feedPrice") && parser.getFilterType().equals("xmltag")){
//					assertEquals(" ", parser.getValue());
//					break;
//				}
//			}
//		} catch (NumberFormatException | SQLException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	
//	@Test
//	public void runFeedParser_fail() {	
//		String type = "xml";
//		int id = merchant.get(type);
//		
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedGetproduct", new String[][]{{"xmltaglist", "product"}});
//		data.put("feedName", new String[][]{{"xmltag", "name"},{"between","![CDATA[|,|]]"}});
//		data.put("feedPrice",new String[][]{{"xmltag", "price"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"xmltag", "description"},{"between","![CDATA[|,|]]"},{"plainText","-"}});
//		data.put("feedConcatId", new String[][]{{"xmltag", "category_id"},{"between","![CDATA[|,|]]"}});
//		data.put("feedPictureUrl", new String[][]{{"xmltag", "image_url"},{"between","![CDATA[|,|]]"}});
//		data.put("feedProductUrl", new String[][]{{"xmltag", "url"},{"between","![CDATA[|,|]]"}});
//		
//		
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		try {
//			insertTaskDB(id, "inputFeedFile.xml", type);
//			when(response.getWriter()).thenReturn(pw);
//			
//			org.json.simple.JSONObject jsonDataObject =  HashMapDataToJsonObJect(data);
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			servlet.runFeedParser(request, response, jsonDataObject.toString(), jsonConfigObject.toString(), id);
//			
//			String result = sw.getBuffer().toString().trim();
//			JSONObject jsonObj = new JSONObject(result);
//			assertEquals("error", jsonObj.get("header"));
//			
//		} catch (NumberFormatException | SQLException | IOException | ParseException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//	}
//	
//	@Test
//	public void runFeedParser_xml() {	
//		String type = "xml";
//		int id = merchant.get(type);
//		
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedGetproduct", new String[][]{{"xmltaglist", "product"}});
//		data.put("feedName", new String[][]{{"xmltag", "name"},{"between","![CDATA[|,|]]"}});
//		data.put("feedPrice",new String[][]{{"xmltag", "price"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"xmltag", "description"},{"between","![CDATA[|,|]]"},{"plainText","-"}});
//		data.put("feedConcatId", new String[][]{{"xmltag", "category_id"},{"between","![CDATA[|,|]]"}});
//		data.put("feedPictureUrl", new String[][]{{"xmltag", "image_url"},{"between","![CDATA[|,|]]"}});
//		data.put("feedProductUrl", new String[][]{{"xmltag", "url"},{"between","![CDATA[|,|]]"}});
//		
//		
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		try {
//			insertTaskDB(id, "FeedXML.xml", type);
//			when(response.getWriter()).thenReturn(pw);
//			
//			org.json.simple.JSONObject jsonDataObject =  HashMapDataToJsonObJect(data);
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			servlet.runFeedParser(request, response, jsonDataObject.toString(), jsonConfigObject.toString(), id);
//			
//			String result = sw.getBuffer().toString().trim();
//			JSONObject jsonObj = new JSONObject(result);
//			assertEquals("success", jsonObj.get("header"));
//			assertNotEquals(0, jsonObj.getJSONArray("list").length());
//			
//		} catch (NumberFormatException | SQLException | IOException | ParseException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//		
//	}
//	
//	@Test
//	public void runFeedParser_json() {	
//		String type = "json";
//		int id = merchant.get(type);
//		
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedGetproduct", new String[][]{{"jsonobjectkey", "result"}, {"jsonobjectlistkey", "products"}});
//		data.put("feedName", new String[][]{{"jsonobjectkey", "name"}});
//		data.put("feedPrice",new String[][]{{"jsonobjectkey", "price"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"jsonobjectkey", "description"},{"plainText","-"}});
//		data.put("feedConcatId", new String[][]{{"jsonobjectkey", "category_id"}});
//		data.put("feedPictureUrl", new String[][]{{"jsonobjectkey", "image_url"}});
//		data.put("feedProductUrl", new String[][]{{"jsonobjectkey", "url"}});
//		
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		try {
//			insertTaskDB(id, "FeedJson.json", type);						
//			when(response.getWriter()).thenReturn(pw);
//			
//			org.json.simple.JSONObject jsonDataObject =  HashMapDataToJsonObJect(data);
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			servlet.runFeedParser(request, response, jsonDataObject.toString(), jsonConfigObject.toString(), id);
//			
//			String result = sw.getBuffer().toString().trim();
//			JSONObject jsonObj = new JSONObject(result);
//			assertEquals("success", jsonObj.get("header"));
//			assertNotEquals(0, jsonObj.getJSONArray("list").length());
//			
//		} catch (NumberFormatException | SQLException | IOException | ParseException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//		
//	}
//	@Test
//	public void runFeedParser_csv() {	
//		String type = "csv";
//		int id = merchant.get(type);
//		
//		Map<String,String[][]> data = new HashMap<String, String[][]>();
//		data.put("feedName", new String[][]{{"csvcolumn", "3"}});
//		data.put("feedPrice",new String[][]{{"csvcolumn", "6"}, {"removeNotPrice", "-"}});
//		data.put("feedDescription", new String[][]{{"csvcolumn", "8"}});
//		data.put("feedPictureUrl", new String[][]{{"csvcolumn", "5"}});
//		data.put("feedProductUrl", new String[][]{{"csvcolumn", "4"}});
//		
//		
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		try {
//			insertTaskDB(id, "FeedCSV.csv", type);
//			when(response.getWriter()).thenReturn(pw);
//			
//			org.json.simple.JSONObject jsonDataObject =  HashMapDataToJsonObJect(data);
//			org.json.simple.JSONObject jsonConfigObject = setConfig(type);
//			servlet.runFeedParser(request, response, jsonDataObject.toString(), jsonConfigObject.toString(), id);
//			
//			String result = sw.getBuffer().toString().trim();
//			JSONObject jsonObj = new JSONObject(result);
//			assertEquals("success", jsonObj.get("header"));
//			assertNotEquals(0, jsonObj.getJSONArray("list").length());
//			
//		} catch (NumberFormatException | SQLException | IOException | ParseException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		}finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//		
//	}
//	private void insertTaskDB(int id, String fileName, String type) throws SQLException{
//		
//		TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
//		
//		TaskBean taskBeans = new TaskBean();
//		taskBeans.setMerchantId(id);
//		taskBeans.setData("{\"inputFile\":\""+fileName+"\",\"feedPassword\":\"\",\"fileName\":\""+fileName+"\",\"pageOffset\":\"\",\"chkGz\":\"0\",\"limitPage\":\"\",\"type\":\""+type+"\",\"pageParam\":\"\",\"feedUsername\":\"\"}");
//		taskBeans.setResult("0");
//		taskBeans.setStatus("DONE");
//		taskBeans.setProcess("feedCatList");
//		
//		TaskBean recentTask;
//		recentTask = taskDB.getRecentTaskByMerchantId(id);
//		if(recentTask != null){
//			taskDB.updateTaskData(taskBeans.getData(), taskBeans.getStatus(), taskBeans.getResult(), recentTask.getId());
//		}else{
//			taskDB.insertTask(taskBeans);
//		}
//	}
//	
//	@Test 
//	public void compareFeedProductdata_existData(){
//		
//		int merchantId = 1258; 
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//			
//		try {
//			when(response.getWriter()).thenReturn(pw);
//			servlet.compareFeedProductdata(response, merchantId);
//			String result = sw.getBuffer().toString().trim();
//			
//			JSONObject jsonObj = new JSONObject(result);
//			assertEquals("success", jsonObj.get("header"));
//			assertEquals(true, jsonObj.has("compareList"));
//			
//		} catch (SQLException | IOException | JSONException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//	}
//	
//	@Test 
//	public void compareFeedProductdata_nonExistData(){
//		
//		int merchantId = 99999999; 
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		
//		try {
//			
//			when(response.getWriter()).thenReturn(pw);
//			servlet.compareFeedProductdata(request, response, merchantId);
//			String result = sw.getBuffer().toString().trim();
//			
// 			assertEquals("{}", result);
//			
//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//			fail();
//		} finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//	}
//	
//	@SuppressWarnings("unchecked")
//	private  org.json.simple.JSONObject HashMapDataToJsonObJect(Map<String,String[][]> data){
//		org.json.simple.JSONObject jsonDataObject = new org.json.simple.JSONObject();
//		
//		for(String field : data.keySet()){
//			org.json.simple.JSONArray jsonFilterArray = new org.json.simple.JSONArray();
//			String[][] f = data.get(field);
//			for(int i = 0 ;i < f.length; i++){
//				org.json.simple.JSONObject tmpJson = new org.json.simple.JSONObject();
//				tmpJson.put("filter", f[i][0]);
//				tmpJson.put("value", f[i][1]);
//				jsonFilterArray.add(tmpJson);
//			}
//			jsonDataObject.put(field, jsonFilterArray);
//		}
//		return jsonDataObject;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private JSONObject setMockFeedParser(Map<String,String[][]> data,org.json.simple.JSONObject jsonConfigObject, int id) {
//		StringWriter sw = new StringWriter();
//		PrintWriter pw = new PrintWriter(sw);
//		JSONObject jsonObj = null;
//		try {
//			org.json.simple.JSONObject jsonDataObject = new org.json.simple.JSONObject();
//					
//				for(String field : data.keySet()){
//					org.json.simple.JSONArray jsonFilterArray = new org.json.simple.JSONArray();
//					String[][] f = data.get(field);
//					for(int i = 0 ;i < f.length; i++){
//						org.json.simple.JSONObject tmpJson = new org.json.simple.JSONObject();
//						tmpJson.put("filter", f[i][0]);
//						tmpJson.put("value", f[i][1]);
//						jsonFilterArray.add(tmpJson);
//					}
//					jsonDataObject.put(field, jsonFilterArray);
//				}
//			
//			when(response.getWriter()).thenReturn(pw);
//			
//			servlet.saveParser(request, response, id, jsonDataObject.toString(),jsonConfigObject.toString());
//			
//			String result = sw.getBuffer().toString().trim();
//			jsonObj = new JSONObject(result);
//			
//		} catch (IOException | JSONException | NumberFormatException | SQLException | ParseException e) {
//			e.printStackTrace();
//			fail();
//		}finally {
//			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
//			if(pw != null) { pw.close(); }
//		}
//		
//		return jsonObj;
//	}
//	
//	@SuppressWarnings("unchecked")
//	private org.json.simple.JSONObject setConfig(String feedType){
//		org.json.simple.JSONObject jsonConfigObject = new org.json.simple.JSONObject();
//		jsonConfigObject.put("feedPagingParam", "1");
//		jsonConfigObject.put("feedPagingLimit", "3");
//		jsonConfigObject.put("feedPagingOffset", "");
//		jsonConfigObject.put("feedType", feedType);
//		jsonConfigObject.put("feedCheckGz", "");
//		jsonConfigObject.put("feedUrl", "https://www.priceza.com");
//		jsonConfigObject.put("feedUsername", "test");
//		jsonConfigObject.put("feedPassword", "test");
//		
//		return jsonConfigObject;
//	}
}
