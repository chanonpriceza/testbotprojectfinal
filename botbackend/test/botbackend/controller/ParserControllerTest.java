package botbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.ha.bot.utils.ACCPropertyParser;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ParserUtil;
import botbackend.utils.Util;

public class ParserControllerTest<MockHttpServletRequestBuilder> {
	private ParserController servlet;

	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	@Mock
	HttpSession session;
	ParserConfigDB pDB;
	MerchantDB mDB;
	MerchantConfigDB mcDB;
	
	String[] html;
	String fileName_html;
	String path_pricezabotbackend;
	String path_config;

	final String path = System.getProperty("user.dir");
	final int mID = 300000;
	String dataFilter = "{\"name\":[{\"filter\":\"inTag\",\"value\":\"<h1>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"price\":[{\"filter\":\"inTag\",\"value\":\"<div class='belanja-right'>\"},{\"filter\":\"between\",\"value\":\"Harga|,|<span class='coret'>\"},{\"filter\":\"plainText\",\"value\":\"\"},{\"filter\":\"replace\",\"value\":\".|,|,\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"}],\"basePrice\":[{\"filter\":\"inTag\",\"value\":\"<span class='coret'>\"},{\"filter\":\"removeNotPrice\",\"value\":\"\"},{\"filter\":\"replace\",\"value\":\".|,|,\"}],\"description\":[{\"filter\":\"inTag\",\"value\":\"<div class='post'>\"},{\"filter\":\"plainText\",\"value\":\"\"}],\"pictureUrl\":[{\"filter\":\"inTag\",\"value\":\"<div class='photo-frame'>\"},{\"filter\":\"between\",\"value\":\"img src='|,|'\"}]}";
	final String dataTest = "[{\"name\":\"Dompet Pria Branded Panjang Bahan Kulit Asli Warna Hitam(test)\",\"url\":\"https://www.tashawa.com/dompet-pria-branded/\",\"price\":\"170000\",\"basePrice\":\"\",\"pictureUrl\":\"https://www.tashawa.com/wp-content/uploads/2015/07/dompet-pria-branded-uuc-008-300x300.jpg\",\"description\":\"Dompet pria branded – Produk dompet pria panjang model terbaru bahan kulit asli warna hitam keren. Trend harga jual dompet kulit pria branded murah online shop masa kini. \",\"realProductId\":\"\",\"upc\":\"\"}]";
	private QueryRunner queryRunner;
	DataSource dataSource;
	boolean isCheckInsert = true;
	byte[] bytes;

	Scanner sc;

	@Before
	public void setUp() throws Exception {
		pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
		mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		servlet = new ParserController();

		fileName_html = path + "/WebContent/assets/test/TashawaTest.html";
		path_pricezabotbackend = path + "/WebContent/assets/test/priceza-bot-backend.properties";
		path_config = path +"/WebContent/assets/test/priceza-bot-server.properties";
	
		MockitoAnnotations.initMocks(this);
		when(request.getMethod()).thenReturn("POST");
		
		pDB.deleteByMerchantId(mID);
		mcDB.deleteMerchantConfigByMerchantId(mID);
		deleteMerchantConfigByMerchantId(mID);
		dataFilter = dataFilter.replace("'", "\\\"");
		if (isCheckInsert) {
			setTblMerchantDB();
			setTblMerchantConfigDB();
			readLineFromFile();
		}
		
	}
	
	
	public void readLineFromFile() {
		String fileName = fileName_html;
		String charsetName = "UTF-8";
		FileInputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {			
			is = new FileInputStream(fileName);
			isr = new InputStreamReader(is, charsetName);
			br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {		
				html = line.split(" ");
			}		
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) { try { br.close(); } catch (IOException e) {} }
			if(isr != null) { try { isr.close(); } catch (IOException e) {} }
			if(is != null) { try { is.close(); } catch (IOException e) {} }
		}		
	}
	
	
	
	public void setTblMerchantConfigDB() {
		List<MerchantConfigBean> mcfList = new ArrayList<>();
		MerchantConfigBean mcfBean = new MerchantConfigBean();
		mcfBean.setMerchantId(mID);
		mcfBean.setField("parserClass");
		mcfBean.setValue("Test");
		mcfList.add(mcfBean);
		try {
			mcDB.insertMerchantConfigList(mcfList);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setTblMerchantDB() {
		String merchantName = "test_wan_activeZero";
		int packageType = 3;
		if (StringUtils.isNotBlank(merchantName)) {

			MerchantBean merchantBean = new MerchantBean();
			merchantBean.setName(merchantName);
			merchantBean.setActive(0);
			merchantBean.setPackageType(packageType);
			merchantBean.setMerchant_package_runtime(packageType);
			merchantBean.setRedirect_type_runtime(0);
			merchantBean.setSale_name("");
			merchantBean.setState("CRAWLER_START");
			merchantBean.setStatus("WAITING");
			merchantBean.setDataUpdateState("DATA_UPDATE_START");
			merchantBean.setDataUpdateStatus("WAITING");
			merchantBean.setDueErrorCount(0);
			merchantBean.setWceErrorCount(0);
			merchantBean.setServer("Bot13-bot");
			merchantBean.setNodeTest(0);
			merchantBean.setSlug(Util.formatSlugName(merchantName));
			merchantBean.setOwnerId(12);
			merchantBean.setId(mID);
			try {
				mDB.insertNewMerchant(merchantBean);
				isCheckInsert = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@After
	public void tearDown() throws Exception {
		pDB.deleteByMerchantId(mID);
		mcDB.deleteMerchantConfigByMerchantId(mID);
		deleteMerchantConfigByMerchantId(mID);

	}

	public void deleteMerchantConfigByMerchantId(int merchantId) throws SQLException {
		ACCPropertyParser allBotConfig;
		try {
			allBotConfig = new ACCPropertyParser(path_pricezabotbackend);
			dataSource = DatabaseUtil.createDataSource(allBotConfig.getString("web.db.url"),
					allBotConfig.getString("web.db.driver"), allBotConfig.getString("web.db.username"),
					allBotConfig.getString("web.db.password"), 2);
			queryRunner = new QueryRunner(dataSource);
			String DELETE_MERCHANT = "DELETE FROM tbl_merchant WHERE id = ?";
			queryRunner.update(DELETE_MERCHANT, merchantId);

		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getModalAndView_all() {
		ModelAndView result = getModalAndView("all");
		boolean blank = (boolean) result.getModel().get("");

		assertEquals("parser", result.getViewName());
		assertEquals(true, blank);
	}

	@Test
	public void getModalAndView_all_except() {
		ModelAndView result = getModalAndView("all|-parser");
		boolean blank = (boolean) result.getModel().get("permissionNotMatch");

		assertEquals("loginpage", result.getViewName());
		assertEquals(true, blank);
	}

	@Test
	public void getModalAndView_parser() {
		ModelAndView result = getModalAndView("merchantconfig|urlpattern|parser|feed");
		boolean blank = (boolean) result.getModel().get("");
		assertEquals("parser", result.getViewName());
		assertEquals(true, blank);
	}

	private ModelAndView getModalAndView(String rolePermission) {

		when(request.getSession(false)).thenReturn(session);
		HashMap<String, String> permission = new HashMap<String, String>();
		String[] permissions = rolePermission.split("\\|");
		for (String p : permissions) {
			permission.put(p, "");
		}
		when(session.getAttribute("Permission")).thenReturn(permission);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ModelAndView result = null;
		try {

			when(response.getWriter()).thenReturn(pw);
			result = servlet.getModelAndView(request);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
		return result;
	}

	@Test
	public void getMerchant_success() throws SQLException, IOException {
		getMerchant(mID, "success");
	}

	@Test
	public void getMerchant_fail() throws SQLException, IOException {
		getMerchant(-76543, "notFoundMerchant");
	}

	private void getMerchant(int mId, String header) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		try {
			when(response.getWriter()).thenReturn(pw);
			servlet.getMerchant(response, mId);
			String result = sw.getBuffer().toString().trim();
			JSONObject jsonObj = new JSONObject(result);

			if (header.equals("success")) {
				assertEquals(header, jsonObj.get("header"));
			} else {
				assertEquals(header, jsonObj.get("header"));
			}

		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
	}

	@Test
	public void getProductData_success() {
		getProductData(mID, "success", "Bot13-bot");
	}

	@Test
	public void getProductData_fail_caseNoserver() {
		getProductData(-76543, "ffffffff", "");
	}

	private void getProductData(int mId, String header, String serverName) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		try {
			MerchantBean mBean = mDB.getMerchantByMerchantId(mId);
			if (mBean != null) {

				if (StringUtils.isNotBlank(serverName)) {
					when(response.getWriter()).thenReturn(pw);
					servlet.getProductData(request, response, mId);

					String result = sw.getBuffer().toString().trim();
					JSONObject jsonObj = new JSONObject(result);
					if (header.equals("success")) {
						assertEquals(header, jsonObj.get("header"));
						assertEquals(false, jsonObj.get("plist").toString().isEmpty());
						assertEquals("Bot13-bot", serverName);
					}
				}
			} else {
				assertNull(mBean);
				assertEquals("", serverName);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}	

	}

	//
	// @Test
	// public void compareproductdata_success() {
	// compareproductdata(mID, "success", dataFilter, dataTest, "Test");
	// }
	//
	// @Test
	// public void compareproductdata_error() {
	// compareproductdata(55556, "error", dataFilter, "[{\"name\":\"Hello
	// \",\"realProductId\":\"\",\"upc\":\"\"}]",
	// null);
	// }
	//
	// @Test
	// public void compareproductdata_error_dataTestBlank() {
	// compareproductdata(mID, "error", dataFilter, "", "");
	// }
	// @Test
	// public void compareproductdata_error_dataFilterBlank() {
	// compareproductdata(mID, "error", "", dataTest, "");
	// }
	// @Test
	// public void compareproductdata_error_dataFilterBlank1() {
	// compareproductdata(55552, "error", "", dataTest, "");
	// }
	//
	// private void compareproductdata(int mId, String header, String
	// dataFilter, String dataTest, String checkParser) {
	// StringWriter sw = new StringWriter();
	// PrintWriter pw = new PrintWriter(sw);
	// try {
	// ObjectMapper mapper = new ObjectMapper();
	// List<ProductParserBean> oldList = mapper.readValue(dataTest, new
	// TypeReference<List<ProductParserBean>>() {
	// });
	//
	// if (oldList != null && oldList.size() > 0) {
	//
	// when(response.getWriter()).thenReturn(pw);
	// servlet.compareproductdata(response, mId, dataFilter, dataTest);
	// String result = sw.getBuffer().toString().trim();
	// JSONObject jsonObj = new JSONObject(result);
	//
	// String parserClass = checkParser;
	// if (StringUtils.isNotBlank(parserClass)) {
	// assertEquals("Test", parserClass);
	// assertEquals(header, jsonObj.get("header"));
	// assertEquals(25802, jsonObj.get("oldList").toString().length());
	// assertEquals(26128, jsonObj.get("newList").toString().length());
	// }
	//
	// }
	//
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
	//
	@Test
	public void runParser_success() {
		runParser(mID, html, "success", dataFilter);
	}

	private void runParser(int mId, String[] html, String header, String dataFilter) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		try {
			when(response.getWriter()).thenReturn(pw);
			servlet.runParser(request, response, dataFilter, html, mId, "");
			String result = sw.getBuffer().toString().trim();
			JSONObject jsonObj = new JSONObject(result);
			if (header.equals("success")) {
				assertEquals(header, jsonObj.get("header"));
			} else {
				assertEquals(header, jsonObj.get("header"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
	}

	@Test
	public void saveParser_success_haveConfig_ActiveZero() {
		saveParser(mID, dataFilter, "success");
	}

	// @Test
	// public void saveParser_success_haveConfig_ActiveOne() {
	// setDataInDB(55554,"test_wan_activeOne",1);
	// saveParser(55554, dataFilter, "success");
	// }
	//
	// @Test
	// public void saveParser_success_haveConfig_ActiveTwo() {
	// setDataInDB(55553,"test_wan_activeTwo",2);
	// saveParser(55553, dataFilter, "success");
	// }
	//
	// @Test
	// public void saveParser_success_noDataDBL() {
	// saveParser(55552, dataFilter, "success");
	// }

	@Test
	public void saveParser_error_noConfig_noDataFilter() {
		saveParser(55552, "{}", "notfound");
	}

	private void saveParser(int mId, String data, String header) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		JSONObject jsonObj = null;
		try {
			when(response.getWriter()).thenReturn(pw);
			servlet.saveParser(request, response, mId, data);
			String result = sw.getBuffer().toString().trim();
			jsonObj = new JSONObject(result);

			LinkedHashMap<String, String> mValidate = ParserUtil.getConfigValidate(ParserUtil.getParserConfig(data));
			if (mValidate.isEmpty()) {
				if (header.equals("success")) {
					assertEquals(header, jsonObj.get("header"));
				} else {
					assertEquals(header, jsonObj.get("header"));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}finally {
			if(sw != null) { try {sw.close();} catch (IOException e) {e.printStackTrace();} }
			if(pw != null) { pw.close(); }
		}
	}
}
