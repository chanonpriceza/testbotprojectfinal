package botbackend.parser.automate;

import java.util.LinkedHashMap;
import java.util.Map;

public class Rule {
	
	public final static Map<String, Double> ruleSpecificName = ruleSpecificName();
	public final static Map<String, Double> ruleSpecificPrice = ruleSpecificPrice();
	public final static Map<String, Double> ruleSpecificBasePrice = ruleSpecificBasePrice();
	public final static Map<String, Double> ruleSpecificImage = ruleSpecificImage();
	public final static Map<String, Double> ruleSpecificProductID = ruleSpecificProductID();
	
	public final static Map<String, Double> ruleName = ruleName(); 
	public final static Map<String, Double> rulePrice = rulePrice();
	public final static Map<String, Double> ruleBasePrice = ruleBasePrice();
	public final static Map<String, Double> rulePriceSign = rulePriceSign();
	
	public final static Map<String, Boolean> ruleProductBlog = ruleProductBlog();
	public final static Map<String, Boolean> ruleProductContentBlog = ruleProductContentBlog();
	
	private final static Map<String, Double> ruleName() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("h1[itemprop~=name]", 1.0);
		m.put("span[itemprop~=name]", 1.0);
		m.put("h1[class~=(?=.*prod|product)(?=.*(title|name))]", 0.9);
		m.put("h1[class~=(title)|(name)]", 0.8);
		m.put("div[class~=(?=.*prod|product)(?=.*(title|name))]", 0.8);
		m.put("div[class~=(title)|(name)|(h1)]", 0.8);
		m.put("h1[style~=color]", 0.8);
		m.put("h1", 0.8);
		m.put("h2[itemprop~=name]", 0.8);
		m.put("h2", 0.8);
		m.put("h3", 0.8);
		m.put("span[id~=(?=.*Name)(?=.*(Label))]", 0.8);
		
		return m;
	}
	
	private final static Map<String, Double> rulePrice() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("*[itemprop=price]", 1.0);
		m.put("p[class~=(?=.*price)(?=.*(sell|new|special|sale|value))]", 0.9);
		m.put("span[class~=(?=.*price)(?=.*(sell|new|special|sale|value))]", 0.9);
		m.put("div[class~=(?=.*price)(?=.*(sell|new|special|sale|display|value))]", 0.9);
		m.put("p[class=price]", 0.9);
		m.put("ins", 0.8);
		m.put("*[class=productPrice]", 0.7);
		m.put("span[class~=(?=.*woocommerce-Price-amount)]", 0.6);
		m.put("div[class=price]", 0.1);
		m.put("span[class~=(?=.*(price|amount))]", 0.1);
		m.put("span[id~=(?=.*price)(?=.*product)]", 0.9);
		m.put("span[id~=(?=.*price)(?=.*promo)]", 0.9);
		m.put("font[class~=(?=.*price)(?=.*product)]", 0.9);
		m.put("span[id~=(?=.*Price)(?=.*(Label))]", 0.8);
		m.put("p[class~=(?=.*price)(?=.*(normal))]", 0.1);
		
		return m;
	}
	
	private final static Map<String, Double> ruleBasePrice() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("del", 0.9);
		m.put("strike", 0.9);
		m.put("span[class~=(?=.*price)(?=.*(retail|old|base|erase))]", 0.8);
		m.put("p[class~=(?=.*price)(?=.*(retail|old|base|erase))]", 0.8);
		m.put("div[id~=(?=.*price)(?=.*(retail|old|base|erase))]", 0.8);
		m.put("span[id~=(?=.*price)(?=.*(retail|old|base|erase))]", 0.8);
		
		m.put("div[class~=(?=.*price)(?=.*(start|base|old|erase))]", 0.1);
		m.put("span[id~=(?=.*Retail)(?=.*(Label))]", 0.8);
		
		return m;
	}
	
	private final static Map<String, Double> rulePriceSign() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("฿", 1.0);
		m.put("บาท", 1.0);
		
		return m;
	}
	
	private final static Map<String, Double> ruleSpecificName() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("meta[property=og:title]", 1.0);
		
		return m;
	}
	
	private final static Map<String, Double> ruleSpecificPrice() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("meta[property=product:price:amount]", 1.0);
		m.put("meta[property=product:sale_price:amount]", 1.0);
		m.put("meta[itemprop=price]", 1.0);
		
		return m;
	}
	
	private final static Map<String, Double> ruleSpecificBasePrice() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("meta[property=product:original_price:amount]", 1.0);
		m.put("input[class=old_original_price]", 1.0);
		
		return m;
	}
	
	private final static Map<String, Double> ruleSpecificImage() {
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("meta[property=og:image]", 1.0);
		m.put("link[rel=image_src]", 0.8);
		
		return m;
	}
	
	private final static Map<String, Double> ruleSpecificProductID(){
		Map<String, Double> m = new LinkedHashMap<String, Double>();
		m.put("input[type=hidden][name=productId]", 1.0);
		m.put("input[type=hidden][name~=(?=.*(prod|Prod|prd))(?=.*(id|Id|no|No))]", 1.0);
		m.put("input[type=hidden][id~=(?=.*(prod|Prod|prd))(?=.*(id|Id|no|No))]", 1.0);
		
		return m;
	}
	
	private final static Map<String, Boolean> ruleProductBlog() {
		Map<String, Boolean> m = new LinkedHashMap<String, Boolean>();		
		m.put("div[class~=(?=.*suggest)(?=.*item)],[id~=(?=.*suggest)(?=.*item)]", false);
		m.put("*[class~=(?=.*(product|wrap|tab))(?=.*(relate|history|list|category))],[id~=(?=.*(product|wrap|tab))(?=.*(relate|history|list|category))]", false);
		m.put("*[class~=(?=.*(carousel|footer|sale-tag))],[id~=(?=.*(carousel|footer|sale-tag))]", false);
		m.put("*[class~=(percent)]", false);
		m.put("*[id~=(percent)]", false);
		
		return m;
	}
	
	private final static Map<String, Boolean> ruleProductContentBlog() {
		Map<String, Boolean> m = new LinkedHashMap<String, Boolean>();		
		m.put("สินค้าที่เกี่ยวข้อง", false);
		
		return m;
	}
}
