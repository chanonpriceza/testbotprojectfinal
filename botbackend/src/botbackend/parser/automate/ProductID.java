package botbackend.parser.automate;

import java.util.Map.Entry;

import static org.apache.commons.lang3.StringUtils.*;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProductID {
	
	public static Element scrapSpecific(Document doc) {
		for(Entry<String, Double> m : Rule.ruleSpecificProductID.entrySet()) {
			String rule = m.getKey();
			Elements tmp = doc.select(rule);
			
			if(tmp.size() != 1) {
				continue;
			}
			
			Element firstTag = tmp.get(0);
			if(isNotBlank(firstTag.toString())) return firstTag;
		}
		return null;
	}
}