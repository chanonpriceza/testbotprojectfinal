package botbackend.parser.automate;

import static org.apache.commons.lang3.StringUtils.*;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;

public class Image {
	private final static Pattern PATTERN_NO_IMAGE = Pattern.compile("(?=.*no)(?=.*image).+", Pattern.CASE_INSENSITIVE);
	private final static String IMAGE_SELECTOR = "img[src~=(jpg)|(jpeg)|(png)]";
	
	private final static int MAX_IMAGE_FOUND = 3;
	private final static int MAX_IMAGE_DIMENSION = 620;
	private final static int MIN_IMAGE_DIMENSION = 75;
	private final static int MIN_AREA = 10000;
	
	public static Element scrap(Element parent, String currentUrl) {
		NavigableMap<Integer, Object[]> m = screen(parent, currentUrl);
		return choose(m);
	}
	
	private static NavigableMap<Integer, Object[]> screen(Element parent, String currentUrl){
		NavigableMap<Integer, Object[]> m = new TreeMap<Integer,  Object[]>();
		Elements imageUrls = parent.select(IMAGE_SELECTOR);
		
		for(Element img : imageUrls) {
			String url = FilterUtil.getAbsoluteURL(img.attr("src"), currentUrl);
			
			if(isBlank(url) || !url.startsWith("http")) {
				continue;
			}
			int[] dimension = ScrapeUtil.getDimension(url);
			int width = dimension[0];
			int height = dimension[1];
			
			if(width <= MIN_IMAGE_DIMENSION || height <= MIN_IMAGE_DIMENSION 
					|| width > MAX_IMAGE_DIMENSION || height > MAX_IMAGE_DIMENSION 
					|| (width * height <= MIN_AREA)) {
				continue;
			}
			
			int score = (width * height) + width + height;
			if(PATTERN_NO_IMAGE.matcher(url).matches()) {
				score -= 1;
			}
			if(!m.containsKey(score)) {				
				Object[] obj = new Object[3];
				obj[0] = img;
				obj[1] = dimension[0];
				obj[2] = dimension[1];
				m.put(score, obj);
				
				if(m.size() == MAX_IMAGE_FOUND) {
					break;
				}
			}
		}
		return m;
	}
	
	private static Element choose(NavigableMap<Integer, Object[]> m){
		int currentWightScore = 0;
		Element currentElement = null;
		for(Entry<Integer, Object[]> img : m.entrySet()) {
			Object[] value = img.getValue();
			int width = (Integer)value[1];
			int height = (Integer)value[2];
			int score = 0;
			
			if(width >= 200 && height >= 200) {
				
				if(width == height && width <= 400 && width <= 400) {
					score = 20;
				}else if(width <= 400 && width <= 400) {
					score = 19;
				}else if(width == height) {
					score = 18;
				}else if(width <= 400 || width <= 400) {
					score = 17;
				}else {
					score = 16;
				}
				
			}else if(width >= 200 || height >= 200) {
				
				if(width <= 400 && width <= 400) {
					score = 10;
				} else if(width <= 400 || width <= 400) {
					score = 9;
				} else {
					score = 8;
				}
				
			}else {
				score = 1;
			}
			
			if(currentWightScore < score) {
				currentWightScore = score;
				currentElement = (Element)value[0];
			}else if(currentWightScore == score){
				String url = currentElement.attr("src").toString();
				if(PATTERN_NO_IMAGE.matcher(url).matches()) {
					currentWightScore = score;
					currentElement = (Element)value[0];
				}
			}
		}
		
		return currentElement;
	}
	
	public static Element scrapSpecific(Document doc) {
		for(Entry<String, Double> m : Rule.ruleSpecificImage.entrySet()) {
			String rule = m.getKey();
			Elements tmp = doc.select(rule);
			
			if(!tmp.isEmpty()) {
				Element firstTag = tmp.get(0);
				if(isNotBlank(firstTag.toString())) return firstTag;
			}
		}
		return null;
	}
}
