package botbackend.parser.automate;

import static org.apache.commons.lang3.StringUtils.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map.Entry;

import javax.swing.ImageIcon;

import org.jsoup.nodes.Element;

import utils.FilterUtil;

import botbackend.utils.Util;

public class ScrapeUtil {
	
	public static double convertToRealPrice(String value) {
		value = FilterUtil.toPlainTextString(value);
		value = FilterUtil.removeCharNotPrice(value);
		return FilterUtil.convertPriceStr(value);
	}
	
	public static boolean hasText(Element element) {
		if(element != null && isNotBlank(element.text())) {
			return true;
		}
		return false;
	}
	
	public static void removeNonProductBlog(Element element) {
		for(Entry<String, Boolean> m : Rule.ruleProductBlog.entrySet()) {
			if(element == null) {
				return;
			}
			
			String rule = m.getKey();
			try {
				element.select(rule).remove();
			}catch(Exception e) { return;}
		}
		
		for(Entry<String, Boolean> m : Rule.ruleProductContentBlog.entrySet()) {
			String rule = m.getKey();
			try {
				element.getElementsContainingOwnText(rule).parents().remove();
			}catch(Exception e) { return;}
		}
	}
	
	public static int[] getDimension(String imageUrl) { 
	  	URL url = null;
		HttpURLConnection conn = null;
		ByteArrayOutputStream outputStream = null;
		InputStream inputStream = null;
		try {
			url = new URL(imageUrl);
	        conn = (HttpURLConnection) url.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5");
		
			inputStream = conn.getInputStream();				
			outputStream = new ByteArrayOutputStream();		 
			int read = 0;
			byte[] bytes = new byte[1024];			 
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		    
			ImageIcon imageIcon = new ImageIcon(outputStream.toByteArray());
			return new int[] { imageIcon.getIconWidth(), imageIcon.getIconHeight()};
		} catch (Exception e) {
			Util.consumeInputStreamQuietly(inputStream);
		} finally {
			if(outputStream != null) try { outputStream.close(); } catch (IOException e1) {	}
			if(inputStream != null) try { inputStream.close(); } catch (IOException e1) {	}
			conn.disconnect();
		}
		
		return new int[] { -1, -1};
	}
}
