package botbackend.parser.automate;

import static org.apache.commons.lang3.StringUtils.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.FilterUtil;

import botbackend.bean.ProductParserBean;

public class ScrapeProduct {
	private final static int MAX_OUTER_LOOP = 20;
	private final static int MAX_OUTER_LOOP_FOUND_BASEPRICE = 3;
	private final static int MAX_OUTER_LOOP_FOUND_IMAGE = 15;
	private final static int MAX_OUTER_LOOP_TYPE = Collections.max(Arrays.asList(MAX_OUTER_LOOP_FOUND_BASEPRICE, MAX_OUTER_LOOP_FOUND_IMAGE));
	
	public static ProductParserBean predict(String url) {		
		ProductParserBean b = new ProductParserBean();
		try {
			b.setEmptyField();
			b.setUrl(url);
			Document doc = Jsoup.connect(url).get();
			
			Map<Element, Double> name = Name.scrap(doc);
			scanByAlgorithm(doc, name, b);
			scanByTag(doc, b);
			setValue(b);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	
	public static List<ProductParserBean> predict(List<String> urls) {
		List<ProductParserBean> list = new ArrayList<ProductParserBean>();
		for(String url: urls) {
			list.add(predict(url));
		}
		
		int duplicateImage = list.stream().filter(b -> Collections.frequency(list, b.getPictureUrl()) > 1).collect(Collectors.toSet()).size();
		int imageTotal = list.stream().filter(b -> isNotBlank(b.getPictureUrl())).collect(Collectors.toList()).size();
		if(duplicateImage == imageTotal) {
			list.stream().forEach(b -> b.setPictureUrl(""));
		}
		
		return list;
	}
	
	private static void scanByAlgorithm(Document doc, Map<Element, Double> element, ProductParserBean b) {
		if(element.isEmpty()) {
			return;
		}
		
		for(Entry<Element, Double> m : element.entrySet()) {
			int round = 0;
			int roundAfterFoundPrice = 0;
			boolean foundPrice = false;
			boolean foundImg = false;
			boolean foundBasePrice = false;
			Element nowElement = m.getKey();
			Element nameElement = nowElement;
			
			OuterLoop : 
			while((!foundPrice || !foundImg)  
					&& round < MAX_OUTER_LOOP 
					&& roundAfterFoundPrice < MAX_OUTER_LOOP_TYPE) {
				
				round++;
				if(foundPrice) roundAfterFoundPrice++;

				if(nowElement == null) {
					break OuterLoop;
				}
				
				Element parent = nowElement.parent();
				if(parent == null) {
					nowElement = parent;
					continue;
				}
				
				Elements tmp = doc.select(parent.cssSelector());
				if(tmp.size() != 1) {
					nowElement = parent;
					continue;
				}
				parent = tmp.first();

				if(!foundPrice) {
					Element price = Price.scrap(parent.clone());
					if(price != null) {
						foundPrice = true;
						b.setElementPrice(price);
					}
				}
				
				if(foundPrice && !foundBasePrice && roundAfterFoundPrice < MAX_OUTER_LOOP_FOUND_BASEPRICE) {
					Element basePrice = BasePrice.scrap(parent.clone());
					if(basePrice != null) {
						foundBasePrice = true;
						b.setElementBasePrice(basePrice);
					}
				}
				
				if(foundPrice && !foundImg && roundAfterFoundPrice < MAX_OUTER_LOOP_FOUND_IMAGE) {
					Element image = Image.scrap(parent.clone(), b.getUrl());
					if(image != null) {
						foundImg = true;
						b.setElementPictureUrl(image);
					}
				}
				
				nowElement = parent;
			}
			
			if((foundPrice && foundImg)) {
				b.setElementName(nameElement);
				break;
			}
		}
		
		if(b.getElementName() == null) {
			Entry<Element, Double> m = element.entrySet().stream().findFirst().get();
			double weight = m.getValue();
			if(weight >= 0.8) {
				b.setElementName(m.getKey());
			}
		}
	}
	
	private static void scanByTag(Document doc, ProductParserBean b) {
		Element elementPrice = Price.scrapSpecific(doc);
		if(elementPrice != null) b.setElementPrice(elementPrice);
		
		Element elementBasePrice = BasePrice.scrapSpecific(doc);
		if(elementBasePrice != null) b.setElementBasePrice(elementBasePrice);
		
		Element elementImage = Image.scrapSpecific(doc);
		if(elementImage != null) b.setElementPictureUrl(elementImage);
		
		Element elementProductID = ProductID.scrapSpecific(doc);
		if(elementImage != null) b.setElementRealProductId(elementProductID);
	}
	
	private static void setValue(ProductParserBean b) {
		Element element = b.getElementName();
		if(element != null) {
			String nameStr = element.toString();
			String name = FilterUtil.toPlainTextString(nameStr);
			if(isBlank(name)) {
				name = FilterUtil.getStringBetween(nameStr, "content=\"", "\"");
			}
			
			name = FilterUtil.replaceHtmlCode(name);
			name = StringEscapeUtils.unescapeHtml4(name);
			name = FilterUtil.removeNonChar(name);
			name = FilterUtil.removeSpace(name);
			
			if(isNotBlank(name)) {
				b.setName(name);
			}
		}
		
		element = b.getElementPrice();
		if(element != null) {
			String priceStr = element.toString();
			String price = FilterUtil.toPlainTextString(priceStr);
			if(isBlank(price)) {
				price = FilterUtil.getStringBetween(priceStr, "content=\"", "\"");
				if(isBlank(price)) {
					price = FilterUtil.getStringBetween(priceStr, "value=\"", "\"");
				}
			}
			price = FilterUtil.removeCharNotPrice(price);
			if(isNotBlank(price)) {
				b.setPrice(price);
			}
		}
		
		element = b.getElementBasePrice();
		if(element != null) {
			String basePriceStr = element.toString();
			String basePrice = FilterUtil.toPlainTextString(basePriceStr);
			if(isBlank(basePrice)) {
				basePrice = FilterUtil.getStringBetween(basePriceStr, "content=\"", "\"");
				if(isBlank(basePrice)) {
					basePrice = FilterUtil.getStringBetween(basePriceStr, "value=\"", "\"");
				}
			}
			basePrice = FilterUtil.removeCharNotPrice(basePrice);
			
			String price = b.getPrice();
			if(!price.isEmpty() && isNotBlank(basePrice)) {
				double priceVal = FilterUtil.convertPriceStr(price);
				double basePriceVal = FilterUtil.convertPriceStr(basePrice);
				
				if(basePriceVal > priceVal && priceVal > 0) {
					b.setBasePrice(basePrice);
				}
			}
		}
		
		element = b.getElementPictureUrl();
		if(element != null) {
			String pictureUrlStr = element.toString();
			String pictureUrl = FilterUtil.getStringBetween(pictureUrlStr, "src=\"", "\"");
			if(isBlank(pictureUrl)) {
				pictureUrl = FilterUtil.getStringBetween(pictureUrlStr, "content=\"", "\"");
				if(isBlank(pictureUrl)) {
					pictureUrl = FilterUtil.getStringBetween(pictureUrlStr, "href=\"", "\"");
				}
			}
			pictureUrl = FilterUtil.getAbsoluteURL(pictureUrl, b.getUrl());
			
			if(isNotBlank(pictureUrl)) {
				b.setPictureUrl(pictureUrl);
			}
		}
		
		element = b.getElementRealProductId();
		if(element != null) {
			String realProductIdStr = element.toString();
			String realProductId = FilterUtil.getStringBetween(realProductIdStr, "value=\"", "\"");
			
			if(isNotBlank(realProductId)) {
				b.setRealProductId(realProductId);
			}
		}
	}
}
