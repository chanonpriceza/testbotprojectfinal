package botbackend.parser.automate;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import static org.apache.commons.lang3.StringUtils.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Name {
	
	private final static double WEIGHT_STANDARD = 0.8;
	private final static int MAX_NAME_ELEMENT = 3;
	
	public static Map<Element, Double> scrap(Document doc) {
		Map<Element, Double> found = new LinkedHashMap<Element, Double>();
		for(Entry<String, Double> m : Rule.ruleName.entrySet()) {
			String rule = m.getKey();
			Elements select = doc.select(rule);
			if(select.size() != 1) {
				continue;
			}
			
			for(Element tag : select) {
				if(ScrapeUtil.hasText(tag)) {
					Double weight = m.getValue();
					found.put(tag, weight);
					if(weight >= WEIGHT_STANDARD || found.size() == MAX_NAME_ELEMENT) {
						return found;
					}
				}
			}
		}
		return found;
	}
	
	public static Element scrapSpecific(Document doc) {
		for(Entry<String, Double> m : Rule.ruleSpecificName.entrySet()) {
			String rule = m.getKey();
			Elements tmp = doc.select(rule);
			
			if(!tmp.isEmpty()) {
				Element firstTag = tmp.get(0);
				if(isNotBlank(firstTag.toString())) return firstTag;
			}
		}
		return null;
	}
}
