package botbackend.parser.automate;

import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.TreeMap;

import static org.apache.commons.lang3.StringUtils.*;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Price {
	private final static String EXCLUDE_TAG = "del,strike";
	private final static Pattern EXCLUDE_PATTERN = Pattern.compile("(?=.*(discount|old|erase)).+", Pattern.CASE_INSENSITIVE);
	
	public static Element scrap(Element element) {
		ScrapeUtil.removeNonProductBlog(element);
		for(Entry<String, Double> m : Rule.rulePrice.entrySet()) {
			String rule = m.getKey();
			
			Elements tmp = element.select(rule);
			if(tmp.isEmpty()) {
				continue;
			}
			
			Element tag = chooseTag(tmp);
			if(ScrapeUtil.hasText(tag)) {
				return tag;
			}
		}
		return null;
	}
	
	private static Element chooseTag(Elements price) {
		if(price == null) {
			return null;
		}
		removeNeedlessTag(price);
		Map<Double, Element> m = new TreeMap<Double, Element>();
		for(Element now : price) {
			String nowString = now.toString();
			if(EXCLUDE_PATTERN.matcher(nowString).matches()) {
				continue;
			}
			double nowPrice = ScrapeUtil.convertToRealPrice(nowString);
			if(nowPrice > 0) {
				m.put(nowPrice, now);
			}
		}
		
		if(!m.isEmpty()) {
			return m.entrySet().stream().findFirst().get().getValue();
		}
		
		return null;
	}
	
	private static void removeNeedlessTag(Elements price) {
		if(price == null) {
			return;
		}
		price.select(EXCLUDE_TAG).remove();
	}
	
	public static Element scrapSpecific(Document doc) {
		for(Entry<String, Double> m : Rule.ruleSpecificPrice.entrySet()) {
			String rule = m.getKey();
			Elements tmp = doc.select(rule);
			
			if(!tmp.isEmpty()) {
				Element firstTag = tmp.get(0);
				if(isNotBlank(firstTag.toString())) return firstTag;
			}
		}
		return null;
	}
}
