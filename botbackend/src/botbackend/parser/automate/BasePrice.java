package botbackend.parser.automate;

import java.util.Map.Entry;

import static org.apache.commons.lang3.StringUtils.*;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class BasePrice {
	
	public static Element scrap(Element element) {
		ScrapeUtil.removeNonProductBlog(element);
		for(Entry<String, Double> m : Rule.ruleBasePrice.entrySet()) {
			String rule = m.getKey();
			
			Elements tmp = element.select(rule);
			if(tmp.isEmpty()) {
				continue;
			}
			
			Element tag = tmp.first();
			if(ScrapeUtil.hasText(tag)) {
				tag = removeNeedlessTag(tag);
				return tag;
			}
		}
		return null;
	}
	
	private static Element removeNeedlessTag(Element basePrice) {
		if(basePrice == null) {
			return null;
		}
		
		double highBasePrice = 0.00;
		Element chooseElement = null;
		Elements found = basePrice.select("*:containsOwn(฿)");
		for(Element currentElement : found) {
			double elementPrice = ScrapeUtil.convertToRealPrice(currentElement.toString());
			
			if(elementPrice > highBasePrice) {
				highBasePrice = elementPrice;
				chooseElement = currentElement;
			}
		}
		
		if(highBasePrice == 0) {
			return basePrice;
		}
		return chooseElement;
	}
	
	public static Element scrapSpecific(Document doc) {
		for(Entry<String, Double> m : Rule.ruleSpecificBasePrice.entrySet()) {
			String rule = m.getKey();
			Elements tmp = doc.select(rule);
			
			if(!tmp.isEmpty()) {
				Element firstTag = tmp.get(0);
				if(isNotBlank(firstTag.toString())) return firstTag;
			}
		}
		return null;
	}
}
