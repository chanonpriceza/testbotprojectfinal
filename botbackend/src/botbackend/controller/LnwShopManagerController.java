 package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.api.LnwShopMerchantBean;
import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.ConfigDB;
import botbackend.db.LnwShopMerchantDB;
import botbackend.db.MerchantDB;
import botbackend.db.ProductDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;


@Controller
public class  LnwShopManagerController{
	
	final static String PAGE = "lnwshop Merchant";
	final static String permissionName = "partnership.lnwshopmanager";
	private static final int numRecord = 20;
	final static int LIMIT = 100;
	private String warning;
	
  private DatabaseManager databaseManager;
	public LnwShopManagerController() {
		this.databaseManager = databaseManager.getDatabaseManager();
	}
	
	public LnwShopManagerController(DatabaseManager databaseManager) {
		this.databaseManager = databaseManager;
	}	
  

	@RequestMapping(value = {"/admin/lnwshopmanager"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return getLnwshopMerchantData(1, null, "-1", request, response);
	}
	
	@RequestMapping(value = {"/admin/searchlnwshopmanager"})
	public ModelAndView  getLnwshopMerchantData(
			@RequestParam("page") int page,
			@RequestParam("searchParam") String searchParam,
			@RequestParam("activeType") String activeType,
			HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException { 
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		ModelAndView model = new ModelAndView("lnwshopmanager", "newPage", true);
		String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
		int pageSubmit = ServletUtil.getIntParameter(request, "page", 0);
		List<LnwShopMerchantBean> lnwShopMerchantList = new ArrayList<LnwShopMerchantBean>();
		LnwShopMerchantDB lnwShopDB = databaseManager.getLnwshopmerchantDB();
		String message = "";
		warning = "";
		if(cmd.equals("editlnwMerchant")){
			lnwShopMerchantList = editLnwMerchant(request, response);
			if(StringUtils.isNotBlank(warning)) {
				message = warning;				
			}

		}else if(cmd.equals("searchLnwShopMerchant")){
			searchParam = StringUtils.defaultString((String)request.getParameter("searchParam")).trim();
			activeType = StringUtils.defaultIfBlank(((String)request.getParameter("activeType")).trim(),"-1");
		}

		if(pageSubmit > 0 && cmd.equals("editlnwMerchant")) {
			page = pageSubmit;
		}
		
		if(page < 1){ page = 1; }
		int total = lnwShopDB.countSearchMerchant(searchParam, activeType);
		if(total > 0){
			int start = (page - 1) * numRecord;
			if(start >= total) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)total/(double)numRecord));
			
			if(page > totalPage) {
				page = 1;
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, total, numRecord);
			lnwShopMerchantList = lnwShopDB.getSearchMerchant(searchParam, activeType, start, numRecord);

			if (lnwShopMerchantList != null && lnwShopMerchantList.size() > 0) {
				model.addObject("lnwShopMerchantList", lnwShopMerchantList);
				model.addObject("pagingBean", pagingBean);
			}
		}


		model.addObject("activeType", activeType);
		model.addObject("searchParam", searchParam);
		model.addObject("warning", message);
		return model;
	
	}
	
	@RequestMapping(value = "/admin/countlnwshopproduct")
	public @ResponseBody void countlnwShopProduct(@RequestParam("lnw_merchant_id") String lnw_merchant_id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> m = new HashMap<String, Object>();
		LnwShopMerchantDB lnwShopDB = databaseManager.getLnwshopmerchantDB();
		if(lnwShopDB != null ) {
			int countLnwShop = lnwShopDB.checkCountProductDataLnwShop(lnw_merchant_id);
			if(countLnwShop >  0) {
				m.put("header", "success");
				m.put("count", countLnwShop);																				
			}else{
				m.put("header", "error");
			}
			
			LogUtil.info(request, PAGE, "countlnwshopProduct|lnw_merchant_id " + lnw_merchant_id);
			JsonWriter.getInstance().write(response, m);
		}
	}
	
	@RequestMapping(value = "/admin/getdatabylnwid")
	public @ResponseBody void getDataByLnwId(@RequestParam("lnw_merchant_id") String lnw_merchant_id ,HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> datamap = new HashMap<String, Object>();	
		try{
			if(StringUtils.isNotBlank(lnw_merchant_id)) {
				LnwShopMerchantDB lnwShopDB = databaseManager.getLnwshopmerchantDB();
				if(lnwShopDB != null ) {
					try {
						LnwShopMerchantBean lnwShopMerchantBean = lnwShopDB.getMerchantBeanByLnwShopID(lnw_merchant_id);
						
						if(lnwShopMerchantBean != null ){
							datamap.put("merchantId", lnwShopMerchantBean.getId());
							datamap.put("lnw_id", lnwShopMerchantBean.getLnw_id());
							datamap.put("active", lnwShopMerchantBean.getActive());
							datamap.put("addParentName", lnwShopMerchantBean.getAddParentName());
							datamap.put("addProductId", lnwShopMerchantBean.getAddProductId());
							datamap.put("addContactPrice", lnwShopMerchantBean.getAddContactPrice());
							datamap.put("forceContactPrice", lnwShopMerchantBean.getForceContactPrice());
							datamap.put("forceUpdateCat", lnwShopMerchantBean.getForceUpdateCat());
							datamap.put("priceMultiply", lnwShopMerchantBean.getPriceMultiply());
							datamap.put("header", "success");
						}else {
							datamap.put("header", "error");
						}
						
					}catch(Exception e) {
						e.printStackTrace();
					}
				}				
			}else {
				datamap.put("header", "error");
			}

			JsonWriter.getInstance().write(response, datamap);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private @ResponseBody List<LnwShopMerchantBean> editLnwMerchant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		try {
			List<LnwShopMerchantBean> merchantList = new ArrayList<LnwShopMerchantBean>();
			int merchantID = ServletUtil.getIntParameter(request,"eMerchantID",0);
			int activeType =  ServletUtil.getIntParameter(request, "eActiveType", 0);
			int addParentName = ServletUtil.getIntParameter(request, "eAddParentName", 0);
			int addProductId =  ServletUtil.getIntParameter(request, "eAddProductId", 0);
			int addContactPrice =  ServletUtil.getIntParameter(request, "eAddContactPrice", 0);
			int forceContactPrice =  ServletUtil.getIntParameter(request, "eForceContactPrice", 0);
			int forceUpdateCat =  ServletUtil.getIntParameter(request, "eForceUpdateCat", 0);
			String priceMultiply = request.getParameter("ePriceMultiply");
			String lnwID = request.getParameter("eLnwID");
			boolean allowEdit = false;
			LnwShopMerchantDB lnwShopDB = databaseManager.getLnwshopmerchantDB();
			ConfigDB configDB = databaseManager.getConfigDB();
			ConfigBean configBean = configDB.getByName("lnwshop.manager.server");
			if(StringUtils.isBlank(configBean.getConfigValue())){
				warning = "contact_dev";
			}else {
				LnwShopMerchantBean lnwMerchantBean = lnwShopDB.getMerchantBeanByLnwShopID(lnwID);
				if(lnwMerchantBean.getId() != merchantID) { 
					MerchantDB configMerchantDB = databaseManager.getMerchantDB();
					MerchantBean configMerchantBean = configMerchantDB.getMerchantBeanByIdWithOwner(merchantID);
					if(configMerchantBean != null) {
						if(StringUtils.isNotBlank(configMerchantBean.getDataServer())) {
							ProductDataDB productDataDB = databaseManager.getProductDataDB(configMerchantBean.getDataServer());
							if(productDataDB != null) {
								int countProductDataInDataServer = productDataDB.countProduct(merchantID);
								if(countProductDataInDataServer == 0) {
									allowEdit = true;
								}else {
									warning = "delete_data";
								}								
							}else {
								LogUtil.info(request, PAGE, "editLnwMerchant|lnwID:" + lnwID + "|Error,need to create merchantId before.");
								warning = "data-server-incorrect";
							}
						}else {
							allowEdit = true;
						}
					}else {
						LogUtil.info(request, PAGE, "editLnwMerchant|lnwID:" + lnwID + "|Error,need to create merchantId before.");
						warning = "not_found_config";
					}
				}else {
					allowEdit = true;
				}
				
				if(allowEdit) {
					if(lnwMerchantBean != null){
						lnwMerchantBean.setId(merchantID);
						lnwMerchantBean.setActive(activeType);
						lnwMerchantBean.setAddContactPrice(addContactPrice);
						lnwMerchantBean.setAddParentName(addParentName);
						lnwMerchantBean.setAddProductId(addProductId);
						lnwMerchantBean.setForceContactPrice(forceContactPrice);
						lnwMerchantBean.setForceUpdateCat(forceUpdateCat);
						if(StringUtils.isNotBlank(priceMultiply)){
							lnwMerchantBean.setPriceMultiply(Double.parseDouble(priceMultiply));
						}

						try {
							int success = lnwShopDB.updateLnwMerchant(lnwMerchantBean);
							if (success > 0) {
								LogUtil.info(request, PAGE, "editLnwMerchant|lnwID:" + lnwID );
								merchantList.add(lnwMerchantBean);
								return merchantList;
							}
						} catch (Exception e) {
							e.printStackTrace();
							return null;
						}

					}
	
				}else {
					return null;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return null;
	}
	
	@RequestMapping(value = "/admin/checkdatalnwshopajax")
	public @ResponseBody void getMerchantDataLnwShopListAjax(@RequestParam("lnwID") String lnwID,int start, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		if(StringUtils.isNotBlank(lnwID)){
			try{
				LnwShopMerchantDB lnwShopDB = databaseManager.getLnwshopmerchantDB();
				if (lnwShopDB != null) {
					List<LnwShopProductDataBean> listData = lnwShopDB.findProductData(lnwID, start,LIMIT);	
					if(listData != null && listData.size() > 0) {
						start = start + LIMIT;
						m.put("header", "success");
						m.put("start", start);
						m.put("listData", listData);
						LogUtil.info(request, PAGE, "getMerchantDataLnwShopListAjax|LnwShopID " + lnwID);						
					}else {
						m.put("header", "no data");
					}
				} else {
					LogUtil.info(request, PAGE, "getMerchantDataLnwShopListAjax --> Not connect database");
				}
			} catch(Exception e){
				e.printStackTrace();		
			}
		}else{
			LogUtil.info(request, PAGE, "getMerchantDataLnwShopListAjax --> No LnwShopID for this merchant");
		}			
	
		JsonWriter.getInstance().write(response, m);
	}
	
}
