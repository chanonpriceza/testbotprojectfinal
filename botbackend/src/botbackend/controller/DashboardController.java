package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bean.ProductDataBean.STATUS;
import botbackend.bean.BotQualityReportBean;
import botbackend.bean.PluginConfigBean;
import botbackend.bean.ProductWaitSyncBean;
import botbackend.bean.TaskBean;
import botbackend.db.DashboardDB;
import botbackend.db.PluginDB;
import botbackend.db.TaskDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.AjaxUtil;
import botbackend.utils.UserUtil;
import utils.DateTimeUtil;
import utils.FilterUtil;

@Controller
public class DashboardController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/dashboard"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView("dashboard", "newPage", true);

		int userId = UserUtil.getUserSession(request).getUserId();
		if(userId <= 0){
			return model;
		}
		
		PluginDB pluginDB = DatabaseManager.getDatabaseManager().getPluginDB();
		List<PluginConfigBean> configListPlugin = pluginDB.getConfigPluginByUserId(userId);
		
		JSONArray allPlugin = new JSONArray();
		if(configListPlugin != null && configListPlugin.size() > 0){
			for (PluginConfigBean pluginConfigBean : configListPlugin) {
				if (pluginConfigBean.getPluginType() == 1) {
//					pluginConfigBean.setShowSetting(generateObjectPlugin1());
				} else if (pluginConfigBean.getPluginType() == 2) {
//					pluginConfigBean.setShowSetting(generateObjectPlugin2());
				} else if (pluginConfigBean.getPluginType() == 3) {
					pluginConfigBean.setShowSetting(generateObjectPlugin3(userId));
				} else if (pluginConfigBean.getPluginType() == 4) {
					pluginConfigBean.setShowSetting(generateObjectPlugin4());
				} else if (pluginConfigBean.getPluginType() == 5) {
					pluginConfigBean.setShowSetting(generateObjectPlugin5());
			}

				allPlugin.add(pluginConfigBean);
			}	
		}
		if(allPlugin != null && allPlugin.size() > 0){
			model.addObject("allPlugin",allPlugin);
		}
		 
		return model;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/savePositionAjax"})
	private @ResponseBody void savePositionAjax(HttpServletRequest request, HttpServletResponse response, @RequestParam("userID") String userID,@RequestParam("jSonPositions") String jSonPositions) throws IOException, ServletException, SQLException {
		PluginDB pluginDB = DatabaseManager.getDatabaseManager().getPluginDB();
		JSONObject json = new JSONObject();
		boolean success = false;
		try{
			int userid = UserUtil.getUserSession(request).getUserId();
			if(userid > 0){
				if(StringUtils.isNotBlank(jSonPositions) && jSonPositions.length() > 0){
					JSONArray jArr = (JSONArray) new JSONParser().parse(jSonPositions);
					if(jArr != null && jArr.size() > 0){
						Object[][] paramList = new Object[jArr.size()][2];
						int count = 0;
						for (Object obj : jArr) {
							JSONObject jObj = (JSONObject) obj;
							Object[] param = new Object[2];
							param[0] = obj.toString();
							param[1] = Integer.parseInt(jObj.get("attr").toString());
							paramList[count] = param;
							count++;
						}
						int updateCount[] = pluginDB.updateConfigPlugin(paramList);
						if(updateCount.length > 0){
							success = true;
							for(int i = 0; i<updateCount.length; i++){
								if(updateCount[i] <= 0){
									success = false;
								}
							}
						}
					}
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(success){
			json.put("header", "success");
		}else{
			json.put("header", "error");
		}
		
		AjaxUtil.sendJSON(response, json);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/createPluginAjax"})
	private @ResponseBody void createPluginAjax(HttpServletRequest request, HttpServletResponse response, @RequestParam("typePlugin") String typePlugin) throws IOException, ServletException, SQLException {
		
		JSONObject json = new JSONObject();
		boolean success = false;
		try{
			int userid = UserUtil.getUserSession(request).getUserId();
			if(userid > 0){
				if(StringUtils.isNotBlank(typePlugin) && typePlugin.length() > 0){
					PluginDB pluginDB = DatabaseManager.getDatabaseManager().getPluginDB();
					int newId = pluginDB.insertConfigPlugin(userid,typePlugin);
					if(newId > 0){
						Object[][] newObj = new Object[1][2];
						newObj[0][0] = "{\"x\":0,\"attr\":\""+newId+"\"}";
						newObj[0][1] = newId;
						int[] updateCount = pluginDB.updateConfigPlugin(newObj);
						if(updateCount.length > 0){
							success = true;
							for(int i=0; i<updateCount.length; i++){
								if(updateCount[i] <= 0){
									success = false;
								}
							}
						}
					}
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(success){
			json.put("header", "success");
		}else{
			json.put("header", "error");
		}
		
		AjaxUtil.sendJSON(response, json);
	}
		
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/deletePluginAjax"})
	private @ResponseBody void deletePluginAjax(HttpServletRequest request, HttpServletResponse response, @RequestParam("idPlugin") String idPlugin) throws IOException, ServletException, SQLException {
		JSONObject json = new JSONObject();
		boolean success = false;
		try{
			int userid = UserUtil.getUserSession(request).getUserId();
			if(userid > 0){
				if(StringUtils.isNotBlank(idPlugin) && idPlugin.length() > 0){
					PluginDB pluginDB = DatabaseManager.getDatabaseManager().getPluginDB();
					int checkDelete = pluginDB.deletePluginById(userid,Integer.parseInt(idPlugin));
					if(checkDelete >= 0){
						success = true;								
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		if(success){
			json.put("header", "success");
		}else{
			json.put("header", "error");
		}
		
		AjaxUtil.sendJSON(response, json);
	}
	
	private Object generateObjectPlugin1() throws SQLException {
		DashboardDB dashboardDB = DatabaseManager.getDatabaseManager().getDashboardDB();
		List<Object[]> resultSet = dashboardDB.getPluginType1DataList();
		List<Object> result = new ArrayList<>();
		if(resultSet == null || resultSet.size() == 0) return null;
		for(int i = 0; i<resultSet.size(); i++){
			Object[] obj = resultSet.get(i);
			if(obj.length == 4){
				Map<String, Object> dataMap = new HashMap<>();
				dataMap.put("owner", obj[0]);
				dataMap.put("total", obj[1]);
				dataMap.put("issue", obj[2]);
				dataMap.put("percentage", obj[3]);
				result.add(dataMap);
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private Object generateObjectPlugin2() throws SQLException {
		DashboardDB dashboardDB = DatabaseManager.getDatabaseManager().getDashboardDB();
		Calendar calAll = Calendar.getInstance();
		calAll.add(Calendar.DAY_OF_YEAR, -7);
		
		List<ProductWaitSyncBean> listTask = dashboardDB.getPluginType2DataList(DateTimeUtil.clearTime(calAll.getTime()));
		
		Map<String, Map<String, Integer[]>> m = new HashMap<String, Map<String, Integer[]>>();
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		
		
		for (int count=1; count<8; count++) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR, -count);
			String d = dt.format(cal.getTime());
			m.put(d, null);
		}
		
		for (ProductWaitSyncBean bean : listTask) {
			
			Map<String, Integer[]> mDate = m.get(bean.getAddDate().toString());
			
			
			if (mDate != null) {
				if (mDate.get(bean.getDataServer())!=null) {
					Integer[] dataList = mDate.get(bean.getDataServer());

					if (bean.getAllCount()>dataList[0]) {
						dataList[0] = bean.getAllCount();
					}
					dataList[1] = dataList[1]+bean.getDone();
					mDate.put(bean.getDataServer(), dataList);
				} else {
					Integer[] newDataList = new Integer[3];
					newDataList[0] = bean.getAllCount();
					if (bean.getStatus().equals(STATUS.C.toString())) {
						newDataList[1] = bean.getDone();
					} else {
						newDataList[1] = 0;
					}
					mDate.put(bean.getDataServer(), newDataList);
				}
			} else {
				Map<String, Integer[]> resultList = new HashMap<String, Integer[]>();
				
				Integer[] newDataList = new Integer[3];
				newDataList[0] = bean.getAllCount();
				if (bean.getStatus().equals(STATUS.C.toString())) {
					newDataList[1] = bean.getDone();
				} else {
					newDataList[1] = 0;
				}
					
				resultList.put(bean.getDataServer(), newDataList);
				m.put(bean.getAddDate().toString(), resultList);
			}
		}
		
		JSONObject obj = new JSONObject();
		JSONArray allCountArr = new JSONArray();
		JSONArray doneArr = new JSONArray();
		
		TreeMap<String, Map<String, Integer[]>> treeMap = new TreeMap<String, Map<String, Integer[]>>(m);
		
		for (Entry<String, Map<String, Integer[]>> mDate: treeMap.entrySet()) {
			String d = mDate.getKey();
			int allCount=0;
			int done=0;
			
			if (mDate.getValue()!=null) {
				for (Entry<String, Integer[]> e: mDate.getValue().entrySet()) {
					allCount += e.getValue()[0];
					done += e.getValue()[1];
				}
			}
			
			JSONObject allCountObj = new JSONObject();
			allCountObj.put("label", d);
			allCountObj.put("y", allCount);
			allCountArr.add(allCountObj);
			
			JSONObject doneObj = new JSONObject();
			doneObj.put("label", d);
			doneObj.put("y", done);
			doneArr.add(doneObj);
		}
		
		obj.put("allCount", allCountArr);
		obj.put("done", doneArr);
		return obj;
	}
	
	private Object generateObjectPlugin3(int userId) throws SQLException {
		DashboardDB dashboardDB = DatabaseManager.getDatabaseManager().getDashboardDB();
		List<Object[]> resultSet1 = dashboardDB.getUserCoreMerchant(userId);
		if(resultSet1 == null || resultSet1.size() == 0) return null;
		List<Object> result = new ArrayList<>(); // last result
		
		Map<String, Object[]> resultMap = new LinkedHashMap<>();
		Map<String, List<String>> timeMap = new LinkedHashMap<>();
		Map<String, String> recentTimeMap = new LinkedHashMap<>();
		List<String> merchantIdList = new ArrayList<>();
		List<String> merchantKeyList = new ArrayList<>();
		String lastStartTime = "Unknown";
		
		for(int i=0; i<resultSet1.size(); i++) {
			Object[] obj = resultSet1.get(i);
			if(obj.length == 6){
				String[] newObj = new String[16];
				newObj[0] = String.valueOf(obj[0]);
				newObj[1] = String.valueOf(obj[1]);
				newObj[2] = String.valueOf(obj[2]);
				newObj[3] = String.valueOf(obj[3]);
				newObj[4] = String.valueOf(obj[4]);
				newObj[15] = String.valueOf(obj[5]);
				
				String merchantId = String.valueOf(obj[0]);
				String server = String.valueOf(obj[3]);
				
				merchantIdList.add(merchantId);
				if(StringUtils.isNotBlank(server)) {
					merchantKeyList.add(merchantId+server);
					resultMap.put(merchantId+server, newObj);				
				}else {
					merchantKeyList.add(merchantId);
					resultMap.put(merchantId, newObj);
				}
			}
		}
		
		if(merchantIdList != null && merchantIdList.size() > 0){
			List<Object[]> resultSet2 = dashboardDB.getPluginType3DataList(merchantIdList);
			if(resultSet2 != null && resultSet2.size() > 0) {
				for(int i=0; i<resultSet2.size(); i++) {
					Object[] obj = resultSet2.get(i);
					if(obj.length == 4) {
						String merchantId 	= String.valueOf(obj[0]);
						String type 		= String.valueOf(obj[1]);
						String startDate	= DateTimeUtil.generateStringDisplayDateTime((Date) obj[2]);
						String endDate 		= ((Date) obj[3] != null?DateTimeUtil.generateStringDisplayDateTime((Date) obj[3]):"Unknown");
						String recentTime	= ((Date) obj[3] != null?DateTimeUtil.calcurateDiff((Date) obj[3],new Date()):"Unknown");
						
						if(recentTime.contains("d")){
							recentTime = recentTime.replace(FilterUtil.getStringAfter(recentTime, "h", ""), "");
						}
						List<String> wceTimeList = timeMap.getOrDefault("WCE"+merchantId, new ArrayList<>());
						if(type.equals("WCE") && wceTimeList.size() < 3) {
							wceTimeList.add(startDate  + " - " + endDate);
							timeMap.put("WCE"+merchantId, wceTimeList);
						}
						List<String> dueTimeList = timeMap.getOrDefault("DUE"+merchantId, new ArrayList<>());
						if(type.equals("DUE") && dueTimeList.size() < 3) {
							dueTimeList.add(startDate  + " - " + endDate);
							timeMap.put("DUE"+merchantId,dueTimeList);
						}
						String recentWce = recentTimeMap.getOrDefault("WCE"+merchantId, "");
						if(StringUtils.isBlank(recentWce) && StringUtils.isNotBlank(endDate) && !endDate.equals("Unknown") && type.equals("WCE")){
							recentTimeMap.put("WCE"+merchantId, recentTime);
						}
						String recentDue = recentTimeMap.getOrDefault("DUE"+merchantId, "");
						if(StringUtils.isBlank(recentDue) && StringUtils.isNotBlank(endDate) && !endDate.equals("Unknown") && type.equals("DUE")){
							recentTimeMap.put("DUE"+merchantId, recentTime);
						}
						if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate) && obj[3] != null){
							lastStartTime = "> " + DateTimeUtil.calcurateSecondDiff((Date) obj[3],new Date()) / 86400 +" day";
						}
						
					}
				}
			}
		}
		
		for(Entry<String, Object[]> entry : resultMap.entrySet()) {
			Object[] finalResult = entry.getValue();
			
			String merchantId = String.valueOf(finalResult[0]);
			String server = String.valueOf(finalResult[3]);
			List<String> wceData = timeMap.get("WCE"+merchantId+server);
			if(wceData != null && wceData.size() > 0){
				finalResult[5] = "WCE";
				for(int i=0; i<wceData.size(); i++) {
					if(StringUtils.isNotBlank(wceData.get(i))) finalResult[6+i] = wceData.get(i);
				}
			}
			List<String> dueData = timeMap.get("DUE"+merchantId+server);
			if(dueData != null && dueData.size() > 0){
				finalResult[10] = "DUE";
				for(int i=0; i<dueData.size(); i++) {
					if(StringUtils.isNotBlank(dueData.get(i))) finalResult[11+i] = dueData.get(i);
				}
			}
			finalResult[9] = recentTimeMap.getOrDefault("WCE"+merchantId+server, lastStartTime);
			finalResult[14] = recentTimeMap.getOrDefault("DUE"+merchantId+server, lastStartTime);
			
			result.add(finalResult);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private Object generateObjectPlugin4() throws SQLException {
		DashboardDB dashboardDB = DatabaseManager.getDatabaseManager().getDashboardDB();
		List<BotQualityReportBean> listTask = dashboardDB.getPluginType4DataList();
		Map<String, Map<String, Integer>> m = new HashMap<String, Map<String, Integer>>();
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		
		List<String> dateList = new ArrayList<>();
		for (BotQualityReportBean bean : listTask) {	
			String date = dt.format(bean.getReportDate());
			if(StringUtils.isNotBlank(date)) {
				Map<String, Integer> mData = m.getOrDefault(date, new HashMap<String, Integer>());
				mData.put(bean.getName(), bean.getAllCount());
				m.put(date, mData);				
				
				if(!dateList.contains(date)) {
					dateList.add(date);
				}
			}
		}
		JSONArray  result = new JSONArray();
		 
		for (int count=0 ; count<dateList.size(); count++) {
			String date = dateList.get(count);
			Map<String, Integer> mDataDetail = m.getOrDefault(date, new HashMap<String,Integer>());
			int countProductall = mDataDetail.getOrDefault("Product all", 0);
			int countProductNotUpdate = mDataDetail.getOrDefault("Product not update", 0);
			int countProductWaittoFix = mDataDetail.getOrDefault("Product wait to fix", 0);
			int countProductUpdateWithin1Day = mDataDetail.getOrDefault("Product update within 1Day", 0);
			int countProductUpdateWithin3Day = mDataDetail.getOrDefault("Product update within 3Day", 0);
			int countProductUpdateMorethan3Day = mDataDetail.getOrDefault("Product update more than 3Day", 0);
			Double qualityPercent = (countProductall == 0)?0:(countProductUpdateWithin1Day/Double.valueOf(countProductall)*100);
			
			JSONObject obj = new JSONObject();
			
			obj.put("date", date);
			obj.put("countProductall", countProductall);
			obj.put("countProductNotUpdate", countProductNotUpdate);
			obj.put("countProductWaittoFix", countProductWaittoFix);
			obj.put("countProductUpdateWithin1Day", countProductUpdateWithin1Day);
			obj.put("countProductUpdateWithin3Day", countProductUpdateWithin3Day);
			obj.put("countProductUpdateMorethan1Day", countProductUpdateMorethan3Day);
			obj.put("qualityPercent", new DecimalFormat("##.##").format(qualityPercent));
			result.add(0,obj);
		
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private Object generateObjectPlugin5() throws SQLException {
		TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
		List<TaskBean> taskList = taskDB.getTaskData(null, null, "('WAITING','DOING','REOPEN')", null,0, 0, 100);
		return taskList;
	}
}


