package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import bean.SendDataBean;
import botbackend.bean.ConfigBean;
import botbackend.bean.ManualProductBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.MerchantConfigBean.FIELD;
import botbackend.bean.ProductParserBean;
import botbackend.bean.UserBean;
import botbackend.db.ConfigDB;
import botbackend.db.ManualProductDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.ProductDataDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.parser.automate.ScrapeProduct;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ParserUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;
import utils.BotUtil;

@Controller
public class ManualAddProductController {
	
	final static String PAGE = "Manual Add Product";
	final static String permissionName = "product.manualaddproduct";
	final static String DEFAULT_CHARSET = "UTF-8";
	final static int DEFAULT_IMAGE_WIDTH = 200;
	final static int DEFAULT_IMAGE_HEIGHT = 200;
	
	@RequestMapping(value = "/admin/manualaddproduct")
	public ModelAndView getModelAndView(HttpServletRequest request) throws IOException, ServletException, SQLException {	
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("manualaddproduct", "", true);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/addproduct-searchmerchant")
	public @ResponseBody void searchMerchant(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId){
		try {
			JSONObject json = new JSONObject();
			
			MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
			
			MerchantBean mBean = null;
			if(mList != null && mList.size() > 0){
				for(MerchantBean merchant : mList){
					if(merchant.getActive() == 1){ mBean = merchant; break;}
					if(mBean == null){ mBean = merchant; }
				}
			}
			
			if(mBean == null){
				json.put("header", "error-not-found-merchant");
			}else{
				ObjectMapper mapper = new ObjectMapper();
				JsonNode mNode = mapper.valueToTree(mBean);
				json.put("merchant", mNode);
				
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				MerchantConfigBean mcBean = mcDB.findMerchantConfigByIdAndField(merchantId, FIELD.parserClass.toString());
				if(mcBean == null){
					ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
					List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
					if(pList.size() <= 0) {
						json.put("header", "error-not-found-merchant-parser");
					} else {
						json.put("parserClass", "User Config");
						json.put("header", "success");
					}
				}else{
					if(mcBean.getValue().equals("com.ha.bot.parser.DefaultFeedHTMLParser")){
						ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
						List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
						if(pList.size() <= 0) {
							MerchantConfigBean mcrBean = mcDB.findMerchantConfigByIdAndField(merchantId, FIELD.realtimeParserClass.toString());
							if(mcrBean != null) {
								json.put("parserClass", mcrBean.getValue());
								json.put("header", "success");
							}else {
								json.put("header", "error-feed-without-parser");
							}
						} else {
							json.put("parserClass", "Feed with Parser Config");
							json.put("header", "error-feed-with-parser");
						}
					}else{
						json.put("parserClass", mcBean.getValue());
						json.put("header", "success");
					}
				}
				
			}
			
			AjaxUtil.sendJSON(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LogUtil.info(request, PAGE, "searchMerchant|MerchantId:" + merchantId);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/addproduct-predictandsaveparser")
	public @ResponseBody void predictAndSaveParser(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("jsonData") String jsonData) throws SQLException, IOException{
		JSONObject json = new JSONObject();
		try{
			if(merchantId > 0 && StringUtils.isNotBlank(jsonData)){
				List<ProductParserBean> resultList = new ArrayList<ProductParserBean>();
				JSONArray jArr = (JSONArray) new JSONParser().parse(jsonData);
				if(jArr == null || jArr.size() == 0){
					json.put("header", "error-after-parse-jsonData");
				}else{
					List<JSONObject> urlListJson = new ArrayList<>();
					for (Object jObj : jArr) {
						JSONObject obj = (JSONObject) jObj;
						urlListJson.add(obj);
					}
					
					if(urlListJson != null && urlListJson.size() > 0){
						try{
							List<String> urlList = new ArrayList<>();
							for(int i=0; i<urlListJson.size(); i++){
								JSONObject obj = urlListJson.get(i);
								String predictUrl = (String) obj.get("predictUrl");
								if(StringUtils.isNotBlank(predictUrl)){
									urlList.add(predictUrl);
								}else {
									LogUtil.info(request, PAGE, "manualAddProductPredict -->  Url is Blank|MerchantId:"+ merchantId );
									continue;
								}
							}
							resultList = ScrapeProduct.predict(urlList);
						}catch(Exception e){
							json.put("header", "error-predict-url");
							e.printStackTrace();
						}
					}
					
					if(resultList != null && resultList.size() > 0){
						JSONArray resultArr = new JSONArray();
						for (ProductParserBean resultBean : resultList) {
							try{
								JSONObject pObj  = new JSONObject();
								
								pObj.put("tool2Name", resultBean.getName());
								pObj.put("tool2Desc", resultBean.getDescription());
							    pObj.put("tool2Price", resultBean.getPrice());	
								pObj.put("tool2PictureUrl", resultBean.getPictureUrl());
								pObj.put("tool2Url", resultBean.getUrl());
								pObj.put("tool2RealProductId", resultBean.getRealProductId());
								pObj.put("tool2Type", "0");	
								
								resultArr.add(pObj);
								if(StringUtils.isNumeric(resultBean.getBasePrice()) && Double.parseDouble(resultBean.getBasePrice()) > 0){
									JSONObject pObjBasePrice  = (JSONObject) pObj.clone();
									pObjBasePrice.put("tool2BasePrice", resultBean.getBasePrice());
									pObjBasePrice.put("tool2Type", "1");
									resultArr.add(pObjBasePrice);
								}
							}catch(Exception e){
								json.put("header", "error-parse-predict-result-to-json-array-exception");
								e.printStackTrace();
							}
						}
						
						if(resultArr != null && resultArr.size() > 0){
							try{
								Map<String, List<String>> resultMap  = ParserUtil.awakeTool2(merchantId, resultArr);
								if(resultMap != null && resultMap.size() > 0){
									
									List<Object[]> insertConfigList = new ArrayList<>();
									JSONArray fieldList = new JSONArray();
									for(Entry<String, List<String>> jsoupKey : resultMap.entrySet()){
										
										String field = "";
										String filter = "";										
										String toolKey = jsoupKey.getKey();
										List<String> value =  jsoupKey.getValue();				
										switch(toolKey) {
										case "tool2Name" 		   :  field = "name";  break;
										case "tool2Desc" 		   :  field = "description";  break;
										case "tool2Price" 		   :  field = "price";  break;
										case "tool2BasePrice" 	   :  field = "basePrice";  break;
										case "tool2PictureUrl" 	   :  field = "pictureUrl";  break;
										case "tool2UPC" 	           :  field = "upc";  break;
										case "tool2RealProductId"   :  field = "realProductId";  break;
										case "tool2ConcatProductId" :  field = "concatId";  break;
										case "tool2Expire"          :  field = "expire";  break;
										default : continue;
										}
										
										for(String val : value){
											if(StringUtils.isNotBlank(val)){
												if(val.indexOf("|,|") >=1){
													filter = "JSoupSelectorAttr";
												}else{
													if(field != "price" && field != "basePrice"){
														if(field != "description"){ filter  = "JSoupSelectorTxt"; }
														else{ filter  = "JSoupSelectorLongTxt"; }
													}else{
														filter  = "JSoupSelectorNum";
													}
												}
											}else{
												continue;
											}													
											
											String[] split_value = val.split(",");
											for(String sVal : split_value){
												sVal = sVal.replace("|", "");
												if(!sVal.startsWith("'") && !sVal.endsWith("'")){
													continue;
												}
												String tmp = sVal.substring(1, sVal.length()-1);
												if(tmp.trim().isEmpty()){
													val = val.replace(sVal, tmp);
												}
											}
											
											Object[] oArr = new Object[]{ merchantId, field, filter, val};
											insertConfigList.add(oArr);
											if(!fieldList.contains(field)){
												fieldList.add(field);
											}
										}
										
									}	
									
									if(insertConfigList != null && insertConfigList.size() > 0){
										Object[][] obj = new Object[insertConfigList.size()][4];
										for(int i = 0; i < insertConfigList.size(); i++){
											obj[i] = insertConfigList.get(i);
										}
										try { 
											
											if(fieldList.contains("name") && fieldList.contains("price")){
												ParserConfigDB pcDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
												pcDB.insert(obj);
												LogUtil.info(request, PAGE, "saveParser|MerchantId:" + merchantId);
												json.put("header", "success");
												json.put("fieldList", fieldList);
											}else{
												json.put("header", "error-cannot-predict-name-and-price");
											}
										} catch (Exception e) {
											e.printStackTrace();
											json.put("header", "error-insert-parser-config-exception");
										}
									}else{
										json.put("header", "error-no-list-to-insert-db");
									}
								}else{
									json.put("header", "error-predict-no-result");
								}
							}catch(Exception e){
								json.put("header", "error-awakeTool2-exception");
								e.printStackTrace();
							}
						}else{
							json.put("header", "error-predict-no-result");
						}
					}else{
						json.put("header", "error-predict-no-result");
					}
				}
			}else{
				json.put("header", "error-input-merchantId-or-jsonData");
			}
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
		LogUtil.info(request, PAGE, "predictAndGetParser|MerchantId:" + merchantId);
	}
	
	@SuppressWarnings({"unchecked" })
	@RequestMapping(value = "/admin/addproduct-get-parse-result")
	@ResponseBody
	public void parseProductUrl(@RequestBody Map<String, Object> json, HttpServletRequest request, HttpServletResponse response)throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();

		try{
			
			String inputMerchantId = (String) json.get("merchantId");
			List<String> inputUrlList = (List<String>) json.get("urlList");
			String parserClass = (String) json.get("parserClass");
			
			if(!StringUtils.isNumeric(inputMerchantId)) return;
			int merchantId = Integer.valueOf(inputMerchantId);
			
			if(inputUrlList == null || inputUrlList.size() <= 0) return;
			String[] urlList = inputUrlList.toArray(new String[inputUrlList.size()]);
			
			if(StringUtils.isBlank(parserClass)){
				parserClass = Util.DEFAULT_PARSER_CLASS;
			}
			
			ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
			List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
			if("User Config".contentEquals(parserClass)) {
				parserClass = Util.DEFAULT_PARSER_CLASS;
			}
			List<ProductDataBean> list = ParserUtil.parse(parserClass,  pList, urlList);
			m.put("list", list);
			m.put("header", "success");
			LogUtil.info(request, PAGE, "parseProduct|Result:Success|MerchantId:" + merchantId);
			
		}catch(Exception e){
			e.printStackTrace();
			m.put("header", "error");
			m.put("errorDetail", "Error : " + e.getMessage());
			LogUtil.info(request, PAGE, "parseProduct|Result:Error");
		}
		
		JsonWriter.getInstance().write(response, m);
	}

	@SuppressWarnings({ "unchecked"})
	@RequestMapping(value = "/admin/addproduct-confirm-result")
	@ResponseBody
	public void confirmAddProduct(@RequestBody Map<String, Object> json, HttpServletRequest request, HttpServletResponse response)throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();
		try{
			
			UserBean userBean = UserUtil.getUserSession(request);
			
			String inputMerchantId = (String) json.get("merchantId");
			List<String> inputProductDataList = (List<String>) json.get("productDataList");
			
			if(!StringUtils.isNumeric(inputMerchantId)) return;
			int merchantId = Integer.valueOf(inputMerchantId);
			
			if(inputProductDataList == null || inputProductDataList.size() <= 0) return;
			
			JSONObject dynamicNote = new JSONObject();
			dynamicNote.put("method", "manual-add-botbackend");
			
			MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			ConfigDB cDB = DatabaseManager.getDatabaseManager().getConfigDB();

			List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
			ConfigBean cBean = cDB.getByName("manual.addproduct.senddata.default.server");
			
			MerchantBean mBean = null;
			if(mList != null && mList.size() > 0){
				for(MerchantBean merchant : mList){
					if(merchant.getActive() == 1){ mBean = merchant; break;}
					if(mBean == null){ mBean = merchant; }
				}
			}
			
			String dataServer = mBean.getDataServer();
			String sendServer = cBean.getConfigValue();
			
			ProductDataDB pdDB = DatabaseManager.getDatabaseManager().getProductDataDB(dataServer);
			SendDataDB sdDB = DatabaseManager.getDatabaseManager().getSendDataDB(sendServer);
			ManualProductDB mpDB = DatabaseManager.getDatabaseManager().getManualProductDB();
			
			List<ProductDataBean> pdList = new ArrayList<>();
			
			try {
				if(merchantId > 0) {
					for(Object productDataList : inputProductDataList) {
						try {
							
							if(!(productDataList instanceof Map<?,?>)) return;
							
								String name 			= (String) ((LinkedHashMap<Object, Object>) productDataList).get("name");
								String price 			= (String) ((LinkedHashMap<Object, Object>) productDataList).get("price");
								String basePrice 		= (String) ((LinkedHashMap<Object, Object>) productDataList).get("basePrice");
								String description 		= (String) ((LinkedHashMap<Object, Object>) productDataList).get("description");
								String pictureUrl 		= (String) ((LinkedHashMap<Object, Object>) productDataList).get("pictureUrl");
								String realProductId 	= (String) ((LinkedHashMap<Object, Object>) productDataList).get("realProductId");
								String upc 				= (String) ((LinkedHashMap<Object, Object>) productDataList).get("upc");
								String url 				= (String) ((LinkedHashMap<Object, Object>) productDataList).get("url");
								
								description = "null".equals(description) ? null:description;
								pictureUrl = "null".equals(pictureUrl) ? null:pictureUrl;
								realProductId = "null".equals(realProductId) ? null:realProductId;
								upc = "null".equals(upc) ? null:upc;
								
								ProductDataBean pdBean = new ProductDataBean();
								pdBean.setMerchantId(merchantId);
								pdBean.setName(name);
								pdBean.setPrice((StringUtils.isNotBlank(price) && Util.checkPrice(price))?Util.revertPrice(price):BotUtil.CONTACT_PRICE);
								pdBean.setBasePrice((StringUtils.isNotBlank(basePrice) && Util.checkPrice(basePrice))?Util.revertPrice(basePrice):BotUtil.CONTACT_PRICE);
								pdBean.setDescription(StringUtils.isNotBlank(description)?description:null);
								pdBean.setPictureUrl(StringUtils.isNotBlank(pictureUrl)?pictureUrl:null);
								pdBean.setRealProductId(StringUtils.isNotBlank(realProductId)?realProductId:null);
								pdBean.setUpc(StringUtils.isNotBlank(upc)?upc:null);
								pdBean.setUrl(url);
								pdBean.setUrlForUpdate(url);
								pdBean.setUpdateDate(new Timestamp(System.currentTimeMillis()));
								pdBean.setDynamicField(dynamicNote.toJSONString());
								
								pdList.add(pdBean);
								
						}catch(Exception e) {
							e.printStackTrace();
						}
					
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			int insertSendDataCount = 0;
			if(pdList != null && pdList.size() > 0){
				for (ProductDataBean pdBean : pdList) {
					try{

						byte[] imageByte = ParserUtil.parseImage(null, pdBean.getPictureUrl(), DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
						
						SendDataBean sdBean = SendDataBean.createSendDataBean(pdBean, imageByte, SendDataBean.ACTION.ADD);
						
						if (StringUtils.isBlank(pdBean.getName()) || (pdBean.getPrice() <= 0)) {
							continue;
						}
						
						int insertProductCount  = pdDB.insertProductData(pdBean);
						if(insertProductCount > 0){
							insertSendDataCount += sdDB.insertSendData(sdBean);
							
							ManualProductBean mpBean = new ManualProductBean();
							mpBean.setMerchantId(pdBean.getMerchantId());
							mpBean.setProductName(pdBean.getName());
							mpBean.setAddBy(userBean.getUserName());
							mpDB.insertManual(mpBean);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			if(pdList.size() != insertSendDataCount){
				m.put("header", "error-insert-statement");
				m.put("errorDetail", (pdList.size() - insertSendDataCount) + " products was not add.");
			}else{
				m.put("header","success");
				m.put("resultcount",pdList.size());
			}
			LogUtil.info(request, PAGE, "addProduct|Result:Success|MerchantId:" + merchantId);
			
		}catch(Exception e){
			e.printStackTrace();
			m.put("header", "error");
			m.put("errorDetail", e.getMessage());
			LogUtil.info(request, PAGE, "addProduct|Result:Error");
		}
		
		JsonWriter.getInstance().write(response, m);
	}



}
