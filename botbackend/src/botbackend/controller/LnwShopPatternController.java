package botbackend.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import botbackend.bean.ConfigBean;
import botbackend.bean.UrlPatternBean;
import botbackend.db.ConfigDB;
import botbackend.db.LnwShopPatternDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.AjaxUtil;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import utils.DateTimeUtil;

@Controller
public class LnwShopPatternController {
	
	final static String PAGE = "URL Pattern";
	final static String permissionName = "partnership.lnwshoppattern";
	static Logger trackLogger = Logger.getLogger("trackLogger");
	static final ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = {"/admin/lnwshoppattern"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("lnwshoppattern", "newPage", true);
	}
	
	
	@RequestMapping(value = {"/admin/lnwshoppatternAjax"})
	public @ResponseBody void getModalAndViewAjax(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(isMultipart) {
			try {
				importPattern(request, response);
			} catch (Exception e) {
				e.printStackTrace();		
			}
		}else{
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			if(cmd.equals("getDataByMerchantId")){
				getDataByMerchantId(request, response);
			}else if(cmd.equals("saveTable")){
				saveTable(request, response);
			}else if(cmd.equals("exportPattern")){
				exportPattern(request, response);
			}
		}
		
		return ;
		
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getDataByMerchantId(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			JSONObject json = new JSONObject();
			if(merchantId > 0) {
				try {
					LnwShopPatternDB lnwshopPatternDB = DatabaseManager.getDatabaseManager().getLnwshopPatternDB();
					List<UrlPatternBean> urlPatternBeanList = lnwshopPatternDB.getLnwshopPatternByMerchantId(merchantId);
					
					if(urlPatternBeanList != null && urlPatternBeanList.size() > 0){
						
						JSONArray jsonNodeList = new JSONArray();
						
						for (UrlPatternBean urlPatternBean : urlPatternBeanList) {
							JsonNode jsonNode = mapper.valueToTree(urlPatternBean);
							jsonNodeList.add(jsonNode);
						}
						
						json.put("header", "success");
						json.put("result", jsonNodeList);
						json.put("mode", "update");
					}else{
						json.put("header", "success");
						json.put("mode", "insert");
					}
					
				}catch(Exception e) {
					json.put("header", "fail");
					e.printStackTrace();
				}
			}
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings({ "unchecked" })
	private @ResponseBody void saveTable(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		try{
			boolean success = false;
			boolean successEmpty = false;
			boolean allowAccessData = true;
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			String jsonString = StringUtils.defaultString((String) request.getParameter("jsonData"));
			JSONObject json = new JSONObject();
			JSONArray errorDataList = new JSONArray();
			if(merchantId > 0 && StringUtils.isNotBlank(jsonString)) {
				JSONArray jArr = (JSONArray)new JSONParser().parse(jsonString.toString());
				if(jArr != null && jArr.size() > 0){
					
					ConfigDB configDB = DatabaseManager.getDatabaseManager().getConfigDB();
					ConfigBean configBean = configDB.getByName("category.allow");
					List<String> categoryAllowList = Arrays.asList((configBean.getConfigValue()).split(","));
					
					List<UrlPatternBean> urlPatternList = new ArrayList<UrlPatternBean>();
					for(Object jObject : jArr){
						JSONObject jObj = (JSONObject) jObject;
						String condition = (String) jObj.get("condition");
						String group = (String) jObj.get("group");
						String value = (String) jObj.get("value");
						String action = (String) jObj.get("action");
						String categoryId = (String) jObj.get("categoryId");
						String keyword = (String) jObj.get("keyword");
						
						if(StringUtils.isBlank(value)){
							successEmpty = true;
							continue;
							
						}else if(!StringUtils.isNumeric(group)){
							continue;
							
						}else if(((StringUtils.isNotBlank(categoryId)) && (!categoryId.equals("0")) && (!categoryAllowList.contains(categoryId))) 
								|| (!StringUtils.isNumeric(categoryId))){
							JSONObject errorDataObj = new JSONObject();
							errorDataObj.put("value", value);
							errorDataObj.put("categoryId", categoryId);
							errorDataList.add(errorDataObj);
							allowAccessData = false;
							
						}else{
							UrlPatternBean ubBean = new UrlPatternBean();
							ubBean.setCondition(condition.trim());
							ubBean.setGroup(Integer.parseInt(group.trim()));
							ubBean.setValue(value.trim());
							ubBean.setAction(action.trim());
							ubBean.setCategoryId(Integer.parseInt(categoryId.trim()));
							ubBean.setKeyword(keyword.trim());
							urlPatternList.add(ubBean);
						}
					}
					
					if(allowAccessData){
						LnwShopPatternDB lnwshopPatternDB = DatabaseManager.getDatabaseManager().getLnwshopPatternDB();
						
						int delete = lnwshopPatternDB.deleteLnwshopPatternByMerchantId(merchantId);
						int insert = lnwshopPatternDB.insertLnwshopPatternBarch(urlPatternList,merchantId);
						if(insert > 0){
							success = true;
						}
						LogUtil.info(request, PAGE, "saveTable|Delete:" + delete + "|Insert:"+ insert + "|MerchantId:"+ merchantId);
					}
				}
			}
			
			if(success){
				if(successEmpty){
					json.put("header", "success-empty");
				}else{
					json.put("header", "success");
				}
			}else{
				if(!allowAccessData){
					json.put("header", "fail-not-allow");
					json.put("detail", errorDataList);
				}else{
					json.put("header", "fail");
				}
			}
			
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void importPattern(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			
			JSONObject json = new JSONObject();
			
			List<UrlPatternBean> urlPatternBeanList = new ArrayList<UrlPatternBean>();
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			fileItemFactory.setSizeThreshold(50*1024);
			fileUpload.setSizeMax(10485760);
			List<FileItem> multiParts = fileUpload.parseRequest(request);
			String errorMsg = "";
			boolean error = false;
			if(multiParts != null && multiParts.size() > 0){
				for(FileItem item : multiParts) {
					if (item.getFieldName().equals("inputPatternFile")) {
						if (StringUtils.isNotBlank(item.getName())) {
							
							InputStreamReader isr = new InputStreamReader(item.getInputStream(), "UTF-8");
							BufferedReader br = new BufferedReader(isr);
													
							errorMsg = parseCSV(br,urlPatternBeanList);
							if(!errorMsg.isEmpty()){
								error = true;
							}
							item.delete();
						}
					}
				}
			}else{
				errorMsg = "Cannot detect upload file.";
				error = true; 
			}
			multiParts.clear();
			
			if(urlPatternBeanList != null && urlPatternBeanList.size() > 0 && !error){
				JSONArray jsonNodeList = new JSONArray();
				ObjectMapper mapper = new ObjectMapper();
				for (UrlPatternBean urlPatternBean : urlPatternBeanList) {
					JsonNode jsonNode = mapper.valueToTree(urlPatternBean);
					jsonNodeList.add(jsonNode);
				}
				json.put("header", "success");
				json.put("result", jsonNodeList);
			}else{
				json.put("header", "fail");
				json.put("errorMsg", errorMsg);
			}
			
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public String parseCSV(BufferedReader br,List<UrlPatternBean> urlPatternBeanList) throws IOException{
		String errorMsg = "";
		String line = null;
		boolean first = true;
		while ((line = br.readLine()) != null) {
			try{
				StringReader lineReader = new StringReader(line);
				CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
				for (CSVRecord csvRecord : record) {
					if(first){
						first = false;
						
						String colum0Name = csvRecord.get(0); if(!colum0Name.contains("MerchantId")){ errorMsg = "Header error." ;break; } // utf8 - first colum have something before first char.
						String colum1Name = csvRecord.get(1); if(!colum1Name.equals("Condition"))	{ errorMsg = "Header error." ;break; }
						String colum2Name = csvRecord.get(2); if(!colum2Name.equals("Value"))		{ errorMsg = "Header error." ;break; }
						String colum3Name = csvRecord.get(3); if(!colum3Name.equals("Action"))		{ errorMsg = "Header error." ;break; }
						String colum4Name = csvRecord.get(4); if(!colum4Name.equals("Group"))		{ errorMsg = "Header error." ;break; }
						String colum5Name = csvRecord.get(5); if(!colum5Name.equals("Keyword"))		{ errorMsg = "Header error." ;break; }
						String colum6Name = csvRecord.get(6); if(!colum6Name.equals("CategoryId"))	{ errorMsg = "Header error." ;break; }
						
						continue;
					}
					try{
						String inputMerchantId = csvRecord.get(0);
						String inputCondition = csvRecord.get(1);
						String inputValue = csvRecord.get(2);
						String inputAction = csvRecord.get(3);
						String inputGroup = csvRecord.get(4);
						String inputKeyword = csvRecord.get(5);
						String inputCategoryId = csvRecord.get(6);
						
						if(StringUtils.isBlank(inputMerchantId) && StringUtils.isBlank(inputValue) && StringUtils.isBlank(inputAction) && StringUtils.isBlank(inputGroup) && StringUtils.isBlank(inputCategoryId)
								&& ( StringUtils.isBlank(inputCondition) || StringUtils.isBlank(inputKeyword)) ) {
							continue;
						}
						
						UrlPatternBean patternBean = new UrlPatternBean();
						patternBean.setMerchantId(Integer.parseInt(inputMerchantId));
						patternBean.setCondition(inputCondition);
						patternBean.setValue(inputValue);
						patternBean.setAction(inputAction);
						patternBean.setGroup(Integer.parseInt(inputGroup));
						patternBean.setKeyword(inputKeyword);
						patternBean.setCategoryId(Integer.parseInt(inputCategoryId));
						
						urlPatternBeanList.add(patternBean);
						
					}catch(Exception e){
						errorMsg = "Parse data error.";
						break;
					}
				}
				record.close();
			}catch(Exception e){
				trackLogger.warn("Can't parse import url pattern line data : " + line);
				e.printStackTrace();
			}
		}
		return errorMsg;
	}
	
	
	private @ResponseBody void exportPattern(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String file = "";
		try{
			
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			String jsonString = StringUtils.defaultString((String) request.getParameter("jsonData"));
			String dateFile = DateTimeUtil.generateStringDate(new Date(), "yyyyMMdd-HHmmss");
			file = "Pattern-" + merchantId + "-" + dateFile;
			
			out.println("MerchantId,Condition,Value,Action,Group,Keyword,CategoryId");
			
			if(merchantId > 0 && StringUtils.isNotBlank(jsonString)) {
				JSONArray jArr = (JSONArray)new JSONParser().parse(jsonString.toString());
				int recNum = 0;
				if(jArr != null && jArr.size() > 0){
					for(Object jObject : jArr){
						JSONObject jObj = (JSONObject) jObject;
						String condition = (String) jObj.get("condition");
						String value = (String) jObj.get("value");
						String action = (String) jObj.get("action");
						String group = (String) jObj.get("group");
						String keyword = (String) jObj.get("keyword");
						String categoryId = (String) jObj.get("categoryId");
						
						out.print("\""+merchantId +"\",");
						out.print("\""+condition +"\",");
						out.print("\""+value +"\",");
						out.print("\""+action +"\",");
						out.print("\""+group +"\",");
						out.print("\""+keyword +"\",");
						out.println("\""+categoryId+"\"");
						recNum++;
					}
				}
			    LogUtil.info(request, PAGE, "exportPattern-->" + recNum +" record(s)|MerchantId:"+ merchantId);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			response.setContentType("application/csv");
	        response.addHeader("Content-Disposition", "attachment; filename = " + file); 
	        response.addHeader("X-Frame-Options", "ALLOWALL");
	        response.flushBuffer();
	        
			out.flush();  
		    out.close();
		}
	}
	
}
