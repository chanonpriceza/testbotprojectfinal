package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bean.MerchantConfigBean.FIELD;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.system.BaseConfig;
import botbackend.utils.AjaxUtil;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;

@Controller
public class MerchantConfigController {
	
	final static String PAGE = "Merchant Config";
	final static String permissionName = "merchant.merchantconfig";
	private static final String[] BOT_5_COUNTRY = new String[] {"th"};
	private static String BOT_SERVER_CURREBT_COUNTRY = BaseConfig.BOT_SERVER_CURRENT_COUNTRY;
	
	@RequestMapping(value = {"/admin/merchantconfig"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("merchantconfig", "newPage", true);
	}
	
	@RequestMapping(value = {"/admin/merchantconfigAjax"})
	public @ResponseBody void getModalAndViewAjax(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
		if(cmd.equals("getDataByMerchantId")){
			getDataByMerchantId(request, response);
		}
		return ;
		
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getDataByMerchantId(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			JSONObject json = new JSONObject();
			if(merchantId > 0) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
					MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
					
					if(mBean != null){
						MerchantConfigDB merchantconfigDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
						List<MerchantConfigBean> merchantconfigList = merchantconfigDB.findMerchantConfig(merchantId);
						
						if(merchantconfigList != null && merchantconfigList.size() > 0){
							
							JSONArray jsonNodeList = new JSONArray();
							
							for (MerchantConfigBean mcfBean : merchantconfigList) {
								JsonNode jsonNode = mapper.valueToTree(mcfBean);
								jsonNodeList.add(jsonNode);
							}
							
							json.put("result", jsonNodeList);
						}
						
						json.put("header", "success");
						json.put("merchantId", mBean.getId());
						json.put("merchantName", mBean.getName());
						
					}else{
						json.put("header", "error");
					}
					
				}catch(Exception e) {
					json.put("header", "error");
					e.printStackTrace();
				}
			}
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = {"/admin/submitmerchantconfig"})
	public @ResponseBody ModelAndView submitMerchantConfig(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, SQLException {
		String merchantId = request.getParameter("merchantHidden");
		ModelAndView model = new ModelAndView("merchantconfig", "newPage", true);
		if(StringUtils.isNotBlank(merchantId)) {
			Map<String, String> fieldValueMap = new HashMap<>();
			for (FIELD field : FIELD.values()) { 
				fieldValueMap.put(field.toString(), StringUtils.defaultString((String) request.getParameter(field.toString())).trim());
			}
			
			List<String[]> insertList = new ArrayList<>();
			for (Entry<String, String> entry : fieldValueMap.entrySet()) {
				if(StringUtils.isNotBlank(entry.getValue())) {
					if(entry.getKey().equals("parserClass") && entry.getValue().equals("userConfig")) {
						if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
							entry.setValue("web.parser.DefaultUserHTMLParser");
						} else {
							continue;
						}
					}
					insertList.add(new String[] {entry.getKey() ,entry.getValue()});
				}
			}
			
			if(insertList != null && insertList.size() > 0) {
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
					mcDB.deleteMerchantConfigByMerchantId(Integer.parseInt(merchantId));
				} else {
					List<String> exceptList = new ArrayList<>();
					exceptList.addAll(Arrays.asList(new String[] {"disableDUEContinue", "dueParserClass", "enableLargeImage", "enableSmallImage", "enableVeryLargeImage", "feedCheckGz", "feedUrl", "forceUpdateLimit", "wceParserClass"}));
					mcDB.deleteMerchantConfigByMerchantIdExceptField(Integer.parseInt(merchantId), exceptList);
				}
				mcDB.insertMerchantConfigBatch(merchantId, insertList);		
				LogUtil.info(request, PAGE, "Submit config, merchantId = " + merchantId);
			}
		}
		model.addObject("merchantId", merchantId);
		return model;
	}
	
	@RequestMapping(value = {"/admin/addTemplateConfig"})
	public @ResponseBody ModelAndView addTemplateConfigSet(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("merchantconfig", "newPage", true);
		String merchantId = request.getParameter("merchantId");
		String templateConfig = request.getParameter("templateConfig");
		List<String> configList = new ArrayList<>();
		List<String[]> insertList = new ArrayList<>();
		try {
			switch(templateConfig){
				case "lazadaDistribute": {
					configList.addAll(Arrays.asList(new String[] {"parserClass", "urlCrawlerClass", "feedCrawlerClass", "feedType", "feedXmlMegaTag"}));
					configList.addAll(Arrays.asList(new String[] {"enableUpdateUrl", "disableParserCookies", "enableProxy"}));
					insertList.add(new String[] {"enableUpdateUrl", "true"});
					insertList.add(new String[] {"disableParserCookies", "true"});
					insertList.add(new String[] {"enableProxy", "true"});
					insertList.add(new String[] {"urlCrawlerClass", "web.crawler.impl.LazadaURLCrawler"});
					insertList.add(new String[] {"parserClass", "web.parser.filter.TemplateLazadaJSIncludeIdHTMLParser"});
					break;
				}
				case "shopeeDistribute": {
					configList.addAll(Arrays.asList(new String[] {"parserClass", "urlCrawlerClass", "feedCrawlerClass", "feedType", "feedXmlMegaTag"}));
					configList.addAll(Arrays.asList(new String[] {"disableParserCookies", "enableProxy"}));
					insertList.add(new String[] {"feedCrawlerClass", "feed.crawler.TemplateShopeeItemApiShortLinkFeedCrawler"});
					insertList.add(new String[] {"feedType", "xml"});
					insertList.add(new String[] {"feedXmlMegaTag", "product"});
					break;
				}
				case "jdcentralDistribute": {
					configList.addAll(Arrays.asList(new String[] {"parserClass", "urlCrawlerClass", "feedCrawlerClass", "feedType", "feedXmlMegaTag"}));
					configList.addAll(Arrays.asList(new String[] {"disableParserCookies", "enableProxy"}));
					insertList.add(new String[] {"feedCrawlerClass", "feed.crawler.TemplateJDCentralFeedCrawler"});
					insertList.add(new String[] {"feedType", "json"});
					break;
				}
				default: break;
			}
			
			if(insertList != null && insertList.size() > 0) {
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				mcDB.deleteMerchantConfigByMerchantIdSpecificField(Integer.parseInt(merchantId), configList);
				mcDB.insertMerchantConfigBatch(merchantId, insertList);	
				LogUtil.info(request, PAGE, "Submit template config, merchantId = " + merchantId + ", type = " + templateConfig);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		model.addObject("merchantId", merchantId);
		return model;
	}
	
}
