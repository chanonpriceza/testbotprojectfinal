package botbackend.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.NotificationApiBean;
import botbackend.bean.NotificationApiBean.COUNTRYLIST;
import botbackend.bean.NotificationApiBean.DESTYPE;
import botbackend.bean.NotificationApiBean.NOTITYPE;
import botbackend.bean.TaskBean;
import botbackend.bean.TaskBean.STATUS;
import botbackend.bean.TaskDetailBean;
import botbackend.bean.UserBean;
import botbackend.bean.common.PagingBean;
import botbackend.component.NotificationAPIComponent;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.TaskDB;
import botbackend.db.TaskDetailDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.system.BaseConfig;
import botbackend.utils.CentralizeUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;
import utils.DateTimeUtil;

@Controller
public class TaskManagerController {
	
	final static String PAGE = "Task Manger";
	final static String permissionName = "task.taskmanager";
	private static Logger trackLogger = Logger.getLogger("trackLogger");
	private static final int numRecord = 20;
	private static final int USER_STATUS = 1;
	final static String SERVICE_URL = "task.service.url.";
	final static String[] COUNTRY_LIST = new String[] {"TH","VN","ID","SG","MY","PH"};
	private static String imagePath = BaseConfig.UPLOAD_FILE_PATH;;


	@RequestMapping(value = {"/admin/taskmanager"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");

		return getTaskData(request, response);
	}
	
	public ModelAndView  getTaskData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("taskmanager", "newPage", true);
		TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
		String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
		int page = ServletUtil.getIntParameter(request, "page", 0);
		String searchParam = "";
		String ownerType = "";
		String statusType = "";
		String processType = "";
		int searchType = 0;
		UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
		List<UserBean> userList = userDB.getUserList(USER_STATUS);
		
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		if(isMultipart){
			addNewtask(request, response);
		}else if(cmd.equals("exportCSV")){
			exportCSV(request, response);
		}else {
			searchParam = StringUtils.defaultString((String)request.getParameter("searchParam")).trim();
			processType = StringUtils.defaultString((String)request.getParameter("processType")).trim();
			statusType = StringUtils.defaultString((String)request.getParameter("statusType")).trim();
			ownerType = StringUtils.defaultString((String)request.getParameter("ownerType")).trim();
			ownerType = StringUtils.defaultString((String)request.getParameter("ownerType")).trim();
			searchType = ServletUtil.getIntParameter(request, "searchType", 0);
		}
		
		if(page < 1){ page = 1; }
		int totalTask = taskDB.countTaskData(searchParam, processType, statusType, ownerType,searchType);
		if(totalTask > 0){
			int start = (page - 1) * numRecord;
			if(start >= totalTask) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)totalTask/(double)numRecord));
			
			if(page > totalPage) {
				page = 1;
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, totalTask, numRecord);
			List<TaskBean> taskList = taskDB.getTaskData(searchParam, processType, statusType, ownerType,searchType, start, numRecord);
			if (taskList != null && taskList.size() > 0) {
				model.addObject("taskList", taskList);
				model.addObject("pagingBean", pagingBean);
			}
		}
		
		model.addObject("userList", userList);
		model.addObject("searchType", searchType);
		model.addObject("statusType", statusType);
		model.addObject("processType", processType);
		model.addObject("ownerType", ownerType);
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		model.addObject("searchParam", searchParam);
		return model;
	
	}
	
	@RequestMapping(value = {"/admin/taskDataAjax"})
	public void  getTaskDataAjax(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ModelAndView model = new ModelAndView("taskmanager", "newPage", true);
		TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
		ConfigDB configDB = DatabaseManager.getDatabaseManager().getConfigDB();
		String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
		int page = ServletUtil.getIntParameter(request, "page", 0);
		String searchParam = "";
		String ownerType = "";
		String statusType = "";
		String processType = "";
		int searchType = 0;
		UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
		List<UserBean> userList = userDB.getUserList(USER_STATUS);
		
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		if(isMultipart){
			addNewtask(request, response);
		}else if(cmd.equals("exportCSV")){
			exportCSV(request, response);
		}else {
			searchParam = StringUtils.defaultString((String)request.getParameter("searchParam")).trim();
			processType = (String)request.getParameter("processType");
			statusType = StringUtils.defaultString((String)request.getParameter("statusType")).trim();
			ownerType = StringUtils.defaultString((String)request.getParameter("ownerType")).trim();
			searchType = ServletUtil.getIntParameter(request, "searchType", 0);
		}
		

		if(page < 1){ page = 1; }
		int totalTask = taskDB.countTaskData(searchParam, processType, statusType, ownerType,searchType);
		if(totalTask > 0){
			int start = (page - 1) * numRecord;
			if(start >= totalTask) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)totalTask/(double)numRecord));
			
			if(page > totalPage) {
				page = 1;
			}
			Map<String,String> serviceUrls = new HashMap<String,String>();
			
			for(String s :COUNTRY_LIST) {
				ConfigBean bean = configDB.getByName(SERVICE_URL+s.toLowerCase());
				String url = null;
				if(bean!=null) {
					url = bean.getConfigValue();
				}
				if(url!=null) {
					serviceUrls.put(s.toLowerCase(),url);
				}
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, totalTask, numRecord);
			List<TaskBean> taskList = taskDB.getTaskData(searchParam, processType, statusType, ownerType,searchType, start, numRecord);
			if (taskList != null && taskList.size() > 0) {
				model.addObject("taskList", taskList);
				model.addObject("pagingBean", pagingBean);
				model.addObject("serviceUrls",serviceUrls);
			}
		}
		model.addObject("userList", userList);
		model.addObject("searchType", searchType);
		model.addObject("statusType", statusType);
		model.addObject("processType", processType);
		model.addObject("ownerType", ownerType);
		model.addObject("searchParam", searchParam);
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		JsonWriter.getInstance().write(response,model);
	
	}
	
	@RequestMapping(value = {"/admin/taskmanagerAjax"})
	private @ResponseBody void  taskmanagerAjax(@RequestParam("taskId") int taskId,HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			
			if(cmd.equals("getCommentByTaskId")){
				getComment(taskId , request, response);
			}else if(cmd.equals("getTaskById")){
				getTaskById(taskId,request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/updateMerchantActive"})
	public  @ResponseBody void  updateMerchantActive(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj  = new JSONObject();
		try {
			int merchantId =  ServletUtil.getIntParameter(request, "merchantId", 0);
			MerchantDB merDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			MerchantBean bean = merDB.getMerchantByMerchantId(merchantId);
			if(bean!=null) {
				bean.setActive(1);
				int i = merDB.updateNewMerchant(bean);
				if(i>0)
					obj.put("success", true);
				else
					obj.put("success", false);
			}else
				obj.put("success", false);
		}catch (Exception e) {
			e.printStackTrace();
			obj.put("success", false);
		}
		JsonWriter.getInstance().write(response,obj);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/commentManager"})
	public  @ResponseBody void  commentManager(@RequestParam("taskId") int taskId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			JSONObject json = new JSONObject();
			
			TaskDB task = DatabaseManager.getDatabaseManager().getTaskDB();
			TaskDetailDB taskDetail = DatabaseManager.getDatabaseManager().getTaskDetailDB();
			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			if(cmd.equals("addNewComment")){
				String message = ServletUtil.getStringParam(request, "message", "");
				String status = ServletUtil.getStringParam(request, "status", "");
				if(message.isEmpty() || status.isEmpty()){
					json.put("header", "error");
					return;
				}
				JSONObject comment = new JSONObject();
				comment.put("message", message);
				int success = taskDetail.insertComment(taskId, comment.toString(),  status.toUpperCase(), userBean.getUserName());
				if(success > 0){
					task.updateTaskStatus( status, taskId);
				}else{
					json.put("header", "error");
					return;
				}
			}else if(cmd.equals("removeComment")){
				int commentId = ServletUtil.getIntParameter(request, "commentId", 0);
				int success = taskDetail.deleteComment(commentId, taskId, userBean.getUserName());
				if(success < 0){
					json.put("header", "error");
					return;
				}
				TaskDetailBean tmp = taskDetail.getDataByTaskId(taskId,1);
				if(tmp == null){
					task.updateTaskStatus(STATUS.WAITING.toString(), taskId);
				}else{
					task.updateTaskStatus(tmp.getStatus(), taskId);
				}
				
				
			}
			TaskBean taskData = task.getTaskById(taskId);
			if(cmd.equals("addNewComment")){
				Set<String> olderCommentList = taskDetail.getDataByTaskId(taskId).stream().filter(b->!b.getOwner().equals(userBean.getUserName())).map(x->x.getOwner()).collect(Collectors.toSet());
				if(!taskData.getOwner().equals(userBean.getUserName()))
					olderCommentList.add(taskData.getOwner());
				for(String c:olderCommentList) {
					if(!NotificationAPIComponent.isLocal)
						NotificationAPIComponent.getInstance().putNoti(new NotificationApiBean(NotificationApiBean.generateMessageAddNewComment(userBean.getUserName(), taskId),userBean.getUserName(),c,DESTYPE.INDIVIDUAL,NOTITYPE.TASK_COMMENT,NotificationApiBean.generateLinkAddNewComment(taskId), COUNTRYLIST.TH, taskData.getMerchantId()));
					else {
						NotificationAPIComponent.putNotificationDirect(new NotificationApiBean(NotificationApiBean.generateMessageAddNewComment(userBean.getUserName(), taskId),userBean.getUserName(),c,DESTYPE.INDIVIDUAL,NOTITYPE.TASK_COMMENT,NotificationApiBean.generateLinkAddNewComment(taskId), COUNTRYLIST.TH, taskData.getMerchantId()));
					}
				}
			}
			List<TaskDetailBean> bean =  taskDetail.getDataByTaskId(taskId);
			MerchantDB merchatnDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			MerchantBean merchantBean = merchatnDB.getMerchantByMerchantId(taskData.getMerchantId());
			int active = -1;
			if(merchantBean!=null) 
				active = merchantBean.getActive();
			json.put("header", "success");
			json.put("commentList", bean);
			json.put("taskData", taskData);
			if(active>-1)
				json.put("active", active);
			json.put("taskId", taskId);
			json.put("currentUser", userBean.getUserName());
			json.put("currentRole",userBean.getUserPermission());
			String origin = request.getHeader("Origin");
			CentralizeUtil.checkAllowOrigin(origin, response);
			JsonWriter.getInstance().write(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void addNewtask(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		try {
			
			int filesize = 1024*1024*100;
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			fileItemFactory.setSizeThreshold(50*1024);
			fileItemFactory.setRepository(new File(imagePath));
			
			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			fileUpload.setSizeMax(filesize);
	
			List<FileItem> multiParts = fileUpload.parseRequest(request);
			
			String cmd = "";
			String issue = "";
			String productFile = null;
			int packageType = 0;
			String merchantType = "";
            int merchantId = 0;; 
			String actionType = "";
			String duedate    ="";

			String feedUsername ="";
			String feedPassword ="";

			String actualData ="";
			String expectData ="";
			String detail     ="";
	
		    String allURL = "";
		    String allFile = "";

		    int countfile = 0;
			for (FileItem item : multiParts) {
				
				if(item.getFieldName().equals("cmd")) {
					cmd = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("issue") && StringUtils.isNotBlank(item.getString())){
					issue = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("duedate") && StringUtils.isNotBlank(item.getString())){
					duedate = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("feedUsername") && StringUtils.isNotBlank(item.getString())){
					feedUsername = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("feedPassword") && StringUtils.isNotBlank(item.getString())){
					feedPassword = item.getString("UTF-8");
				}

				if(item.getFieldName().equals("actualData") && StringUtils.isNotBlank(item.getString())){
					actualData = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("expectData") && StringUtils.isNotBlank(item.getString())){
					expectData = item.getString("UTF-8");
				}
				if(item.getFieldName().equals("detail") && StringUtils.isNotBlank(item.getString())){
					detail = item.getString("UTF-8");
				}
				
				if(item.getFieldName().equals("actionType") && StringUtils.isNotBlank(item.getString())){
					actionType = item.getString("UTF-8");
				}
				
				if(item.getFieldName().equals("merchantType") && StringUtils.isNotBlank(item.getString())){
					merchantType = item.getString("UTF-8");
				}
				
				if(item.getFieldName().equals("package") && StringUtils.isNotBlank(item.getString()) && NumberUtils.isNumber(item.getString())){
					packageType = Integer.parseInt(item.getString("UTF-8"));
				}
				
				if(item.getFieldName().equals("merchantId") && StringUtils.isNotBlank(item.getString()) && NumberUtils.isNumber(item.getString())){
					merchantId = Integer.parseInt(item.getString("UTF-8"));
				}
				
				if(item.getFieldName().equals("productURLs") && StringUtils.isNotBlank(item.getString()) ){
					String getTags = item.getString("UTF-8");
					getTags = getTags.replaceAll("\\[", "").replaceAll("\\]","");
					if(StringUtils.isNotBlank(getTags)){						
						allURL += getTags +"|";						
					}
				}
				
				if(!item.isFormField()) {
					if(item.getFieldName().equals("productFiles") && StringUtils.isNotBlank(item.getString())) {
						if(item.getSize() <= filesize){
							String[] typeOfFile = Util.convertISOtoUTF8(item.getName()).split("\\.");
							if(typeOfFile != null && typeOfFile.length > 1) {
								String nameLogoBuff = DateTimeUtil.generateStringDate(new Date(), "yyyyMMddHHmm")+"_"+countfile+1+"_"+typeOfFile[typeOfFile.length - 2 ]+"."+typeOfFile[typeOfFile.length - 1];
								productFile = nameLogoBuff;
								item.write( new File(imagePath + File.separator + productFile));
								Util.optimizedImage(imagePath + File.separator + productFile);
								if(StringUtils.isNotBlank(productFile)){						
									allFile += productFile +"|";
									countfile++;
								}
							}
						}
					}
				}
				
			}
			

			TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		    
		    if(StringUtils.isBlank(actionType)){
		    	trackLogger.error("Error ! actionType can't be blank");
		    	return;
		    }
		    
		    if(StringUtils.isBlank(issue)){
		    	trackLogger.error("Error ! Issue can't be blank");
		    	return;
		    }
		    
		    if(StringUtils.isBlank(cmd)){
		    	trackLogger.error("Error ! cmd can't be blank");
		    	return;
		    }
		    
		    HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			
		    TaskBean taskBean  = new TaskBean();
		    JSONObject json = new JSONObject();	
		    
		    if(StringUtils.isNotBlank(merchantType)){
		    	
		    }else {
		    	MerchantBean merchantBean = merchantDB.getMerchantByMerchantId(merchantId); 
		    	if(merchantBean!=null ){
		    		merchantType =   merchantBean.getPackageType() > 4 ? "customer":"test";
		    	}
		    	
		    }
		    
		    
			json.put("merchantId",merchantId);
			
	
			  if(merchantId > 0){
				   	List<TaskBean> listMerchantName = taskDB.selectMerchantNameById(merchantId);
				   	if(listMerchantName != null && listMerchantName.size() > 0) {
					   	if(StringUtils.isNotBlank(listMerchantName.get(0).getMerchantName())) {
					   		json.put("merchantName", listMerchantName.get(0).getMerchantName());
					   	}
				   	}
				}
			
			if(StringUtils.isNotBlank(allFile)){
				json.put("fileUpload", allFile);
			}
			if(StringUtils.isNotBlank(duedate)){
				json.put("duedate",duedate);
			}
			if(StringUtils.isNotBlank(feedUsername)){
				json.put("feedUsername",feedUsername);
			}
			if(StringUtils.isNotBlank(feedPassword)){
				json.put("feedPassword",feedPassword);
			}
			if(StringUtils.isNotBlank(actualData)){
				json.put("actualData",actualData);
			}
			if(StringUtils.isNotBlank(expectData)){
				json.put("expectData",expectData);
			}
			if(StringUtils.isNotBlank(detail)){
				json.put("detail",detail);
			}
			if(StringUtils.isNotBlank(allURL)){
				json.put("urls",allURL);
			}
			taskBean.setData(json.toString());
			taskBean.setIssue(issue);
			taskBean.setProcess(actionType);
			taskBean.setMerchantId(merchantId);
			taskBean.setPackageType(packageType);
			taskBean.setOwner(userBean.getUserName());
			taskBean.setStatus(STATUS.WAITING.toString());
			taskBean.setIssueType(merchantType);
			int success = taskDB.insertTask(taskBean);
			if(success > 0){
				LogUtil.info(request, PAGE, "addNewtask --> "+ actionType);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getComment(int taskId, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		JSONObject json = new JSONObject();
		try {
			TaskDetailDB taskDetail = DatabaseManager.getDatabaseManager().getTaskDetailDB();
			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			int toltalList = 0;
			JSONArray jsonNodeList = null;
			JSONObject jsonObj = null;
			List<TaskDetailBean> dataList = taskDetail.getDataByTaskId(taskId);
			if(dataList != null && dataList.size() > 0){
				jsonNodeList = new JSONArray();	
				for (TaskDetailBean data : dataList) {
					jsonObj = new JSONObject();	
					jsonObj.put("Id", data.getId());
					jsonObj.put("Message", data.getMessage());
					jsonObj.put("Owner", data.getOwner());
					jsonObj.put("status", data.getStatus());
					jsonObj.put("createDate", DateTimeUtil.generateStringDisplayDateTime(data.getCreateDate()));
					
					jsonNodeList.add(jsonObj);
				}
				toltalList = dataList.size();
			}
				json.put("totalList", toltalList);
				json.put("header", "success");
				json.put("result", jsonNodeList);
				json.put("taskId", taskId);
				json.put("currentUser", userBean.getUserName());
				
		}catch(Exception e) {
			json.put("header", "error");
			e.printStackTrace();
		}
		JsonWriter.getInstance().write(response, json);
		
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getTaskById(int taskId, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		JSONObject json = new JSONObject();
		try {
			TaskDB task = DatabaseManager.getDatabaseManager().getTaskDB();
			TaskBean data = task.selectTaskById(taskId);

			json.put("data", data);
			json.put("header", "success");
			json.put("taskId", taskId);
			
		}catch(Exception e) {
			json.put("header", "error");
			e.printStackTrace();
		}
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		JsonWriter.getInstance().write(response, json);
		
	}
	
	public void exportCSV(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		File file = null; 
		FileInputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		OutputStream out = response.getOutputStream();
		String fileName = StringUtils.defaultString((String)request.getParameter("fileName"));
		
		try{
			if(fileName.toLowerCase().contains(".jpg")){
				response.setContentType("image/jpg");												
			}else if(fileName.toLowerCase().contains(".png")){
				response.setContentType("image/png");												
			}else {
				response.setContentType("application/");								
			}
			
			response.setCharacterEncoding("UTF-8");
			response.setHeader("content-disposition","filename="+ fileName);	
			imagePath = (!imagePath.endsWith("/")? imagePath+"/":imagePath);
			if(StringUtils.isNotBlank(fileName)){
				file = new File(imagePath + fileName);
				if (file.exists()) {
					is = new FileInputStream(file);
					isr = new InputStreamReader(is);
					byte[] b = IOUtils.toByteArray(is);
					out.write(b);
					LogUtil.info(request, PAGE, "exportCSV|FileName:"+fileName);
					
				}else{
					LogUtil.info(request, PAGE, "exportCSV --> This file now is n ot exist.|FileName:"+fileName );
					out.write("This file now is not exist.".getBytes());
				}

			}else{
				LogUtil.info(request, PAGE, "exportCSV --> Can not found File.|FileName:"+fileName );
				out.write("Can not found File".getBytes());
				
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(br != null) { try { br.close(); } catch (IOException e) {} }
			if(isr != null) { try { isr.close(); } catch (IOException e) {} }
			if(is != null) { try { is.close(); } catch (IOException e) {} }
			out.flush();  
		    out.close();
		}
	}
}
