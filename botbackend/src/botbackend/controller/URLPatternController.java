package botbackend.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import utils.DateTimeUtil;
import web.crawler.bot.Attribute;
import web.crawler.bot.HTMLParser;
import web.crawler.bot.HTMLTag;
import web.crawler.bot.URLUtility;

import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.UrlPatternHistoryBean;
import botbackend.bean.UrlPatternHistoryBean.ACTION;
import botbackend.bean.UserBean;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.UrlPatternHistoryDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.manager.UrlPatternManager;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.URLCrawlerUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;

@Controller
public class URLPatternController {
	
	final static String PAGE = "URL Pattern";
	final static String permissionName = "crawlerconfig.urlpattern";
	static Logger trackLogger = Logger.getLogger("trackLogger");
	static final ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = {"/admin/urlpattern"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("urlpattern", "newPage", true);
	}
	
	
	@RequestMapping(value = {"/admin/urlpatternAjax"})
	public @ResponseBody void getModalAndViewAjax(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(isMultipart) {
			try {
				importPattern(request, response);
			} catch (Exception e) {
				e.printStackTrace();		
			}
		}else{
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			if(cmd.equals("getDataByMerchantId")){
				getDataByMerchantId(request, response);
			}else if(cmd.equals("saveTable")){
				saveTable(request, response);
			}else if(cmd.equals("exportPattern")){
				exportPattern(request, response);
			}
		}
		
		return ;
		
	}
	
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getDataByMerchantId(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			JSONObject json = new JSONObject();
			if(merchantId > 0) {
				try {
					UrlPatternDB urlPatternDB = DatabaseManager.getDatabaseManager().getUrlPatternDB();
					List<UrlPatternBean> urlPatternBeanList = urlPatternDB.getUrlPatternByMerchantId(merchantId);
					
					if(urlPatternBeanList != null && urlPatternBeanList.size() > 0){
						
						JSONArray jsonNodeList = new JSONArray();
						
						for (UrlPatternBean urlPatternBean : urlPatternBeanList) {
							JsonNode jsonNode = mapper.valueToTree(urlPatternBean);
							jsonNodeList.add(jsonNode);
						}
						
						json.put("header", "success");
						json.put("result", jsonNodeList);
						json.put("mode", "update");
					}else{
						json.put("header", "success");
						json.put("mode", "insert");
					}
					
				}catch(Exception e) {
					json.put("header", "fail");
					e.printStackTrace();
				}
			}
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings({ "unchecked" })
	private @ResponseBody void saveTable(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		try{
			boolean success = false;
			boolean successEmpty = false;
			boolean allowAccessData = true;
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			String jsonString = StringUtils.defaultString((String) request.getParameter("jsonData"));
			JSONObject json = new JSONObject();
			JSONArray errorDataList = new JSONArray();
			if(merchantId > 0 && StringUtils.isNotBlank(jsonString)) {
				JSONArray jArr = (JSONArray)new JSONParser().parse(jsonString.toString());
				if(jArr != null && jArr.size() > 0){
					
					ConfigDB configDB = DatabaseManager.getDatabaseManager().getConfigDB();
					ConfigBean configBean = configDB.getByName("category.allow");
					if(StringUtils.isBlank((configBean.getConfigValue())))
						return;
					if((configBean.getConfigValue()).split(",")==null)
						return;
					List<String> categoryAllowList = Arrays.asList((configBean.getConfigValue()).split(","));
					
					List<UrlPatternBean> urlPatternList = new ArrayList<UrlPatternBean>();
					for(Object jObject : jArr){
						JSONObject jObj = (JSONObject) jObject;
						String condition = (String) jObj.get("condition");
						String group = (String) jObj.get("group");
						String value = (String) jObj.get("value");
						String action = (String) jObj.get("action");
						String categoryId = (String) jObj.get("categoryId");
						String keyword = (String) jObj.get("keyword");
						
						if(StringUtils.isBlank(value)||StringUtils.isBlank(Util.removeEmoji(value.trim()))){
							successEmpty = true;
							continue;
							
						}else if(!StringUtils.isNumeric(group)){
							continue;
							
						}else if(((StringUtils.isNotBlank(categoryId)) && (!categoryId.equals("0")) && (!categoryAllowList.contains(categoryId))) 
								|| (!StringUtils.isNumeric(categoryId))){
							JSONObject errorDataObj = new JSONObject();
							errorDataObj.put("value", value);
							errorDataObj.put("categoryId", categoryId);
							errorDataList.add(errorDataObj);
							allowAccessData = false;
							
						}else{
							UrlPatternBean ubBean = new UrlPatternBean();
							ubBean.setCondition(condition.trim());
							ubBean.setGroup(Integer.parseInt(group.trim()));
							ubBean.setValue(value.trim());
							ubBean.setValue(Util.removeEmoji(value.trim()));
							ubBean.setAction(action.trim());
							ubBean.setCategoryId(Integer.parseInt(categoryId.trim()));
							ubBean.setKeyword(keyword.trim());
							urlPatternList.add(ubBean);
						}
					}
					
					if(allowAccessData){
						UrlPatternDB urlPatternDB = DatabaseManager.getDatabaseManager().getUrlPatternDB();
						List<UrlPatternBean> oldPatternList = urlPatternDB.getUrlPatternByMerchantId(merchantId);
						
						int delete = urlPatternDB.deleteUrlPatternByMerchantId(merchantId);
						int insert = urlPatternDB.insertUrlPatternBarch(urlPatternList,merchantId);
						if(insert > 0){
							success = true;
							HttpSession session = request.getSession(false);
							UserBean userBean = (UserBean)session.getAttribute("userData");
							insertHistory(urlPatternList, oldPatternList, merchantId, userBean);
							}
						LogUtil.info(request, PAGE, "saveTable|Delete:" + delete + "|Insert:"+ insert + "|MerchantId:"+ merchantId);
					}else{
						
					}
					
				}
			}
			
			if(success){
				if(successEmpty){
					json.put("header", "success-empty");
				}else{
					json.put("header", "success");
				}
			}else{
				if(!allowAccessData){
					json.put("header", "fail-not-allow");
					json.put("detail", errorDataList);
				}else{
					json.put("header", "fail");
				}
			}
			
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void importPattern(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			
			JSONObject json = new JSONObject();
			
			List<UrlPatternBean> urlPatternBeanList = new ArrayList<UrlPatternBean>();
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			fileItemFactory.setSizeThreshold(50*1024);
			fileUpload.setSizeMax(10485760);
			List<FileItem> multiParts = fileUpload.parseRequest(request);
			String errorMsg = "";
			boolean error = false;
			if(multiParts != null && multiParts.size() > 0){
				for(FileItem item : multiParts) {
					if (item.getFieldName().equals("inputPatternFile")) {
						if (StringUtils.isNotBlank(item.getName())) {
							
							InputStreamReader isr = new InputStreamReader(item.getInputStream(), "UTF-8");
							BufferedReader br = new BufferedReader(isr);
													
							errorMsg = parseCSV(br,urlPatternBeanList);
							if(!errorMsg.isEmpty()){
								error = true;
							}
							item.delete();
						}
					}
				}
			}else{
				errorMsg = "Cannot detect upload file.";
				error = true; 
			}
			multiParts.clear();
			
			if(urlPatternBeanList != null && urlPatternBeanList.size() > 0 && !error){
				JSONArray jsonNodeList = new JSONArray();
				ObjectMapper mapper = new ObjectMapper();
				for (UrlPatternBean urlPatternBean : urlPatternBeanList) {
					JsonNode jsonNode = mapper.valueToTree(urlPatternBean);
					jsonNodeList.add(jsonNode);
				}
				json.put("header", "success");
				json.put("result", jsonNodeList);
			}else{
				json.put("header", "fail");
				json.put("errorMsg", errorMsg);
			}
			
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public String parseCSV(BufferedReader br,List<UrlPatternBean> urlPatternBeanList) throws IOException{
		String errorMsg = "";
		String line = null;
		boolean first = true;
		while ((line = br.readLine()) != null) {
			try{
				StringReader lineReader = new StringReader(line);
				CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
				for (CSVRecord csvRecord : record) {
					if(first){
						first = false;
						
						String colum0Name = csvRecord.get(0); if(!colum0Name.contains("MerchantId")){ errorMsg = "Header error." ;break; } // utf8 - first colum have something before first char.
						String colum1Name = csvRecord.get(1); if(!colum1Name.equals("Condition"))	{ errorMsg = "Header error." ;break; }
						String colum2Name = csvRecord.get(2); if(!colum2Name.equals("Value"))		{ errorMsg = "Header error." ;break; }
						String colum3Name = csvRecord.get(3); if(!colum3Name.equals("Action"))		{ errorMsg = "Header error." ;break; }
						String colum4Name = csvRecord.get(4); if(!colum4Name.equals("Group"))		{ errorMsg = "Header error." ;break; }
						String colum5Name = csvRecord.get(5); if(!colum5Name.equals("Keyword"))		{ errorMsg = "Header error." ;break; }
						String colum6Name = csvRecord.get(6); if(!colum6Name.equals("CategoryId"))	{ errorMsg = "Header error." ;break; }
						
						continue;
					}
					try{
						String inputMerchantId = csvRecord.get(0);
						String inputCondition = csvRecord.get(1);
						String inputValue = csvRecord.get(2);
						String inputAction = csvRecord.get(3);
						String inputGroup = csvRecord.get(4);
						String inputKeyword = csvRecord.get(5);
						String inputCategoryId = csvRecord.get(6);
						
						if(StringUtils.isBlank(inputMerchantId) && StringUtils.isBlank(inputValue) && StringUtils.isBlank(inputAction) && StringUtils.isBlank(inputGroup) && StringUtils.isBlank(inputCategoryId)
								&& ( StringUtils.isBlank(inputCondition) || StringUtils.isBlank(inputKeyword)) ) {
							continue;
						}
						
						UrlPatternBean patternBean = new UrlPatternBean();
						patternBean.setMerchantId(Integer.parseInt(inputMerchantId));
						patternBean.setCondition(inputCondition);
						patternBean.setValue(Util.removeEmoji(inputValue));
						patternBean.setAction(inputAction);
						patternBean.setGroup(Integer.parseInt(inputGroup));
						patternBean.setKeyword(inputKeyword);
						patternBean.setCategoryId(Integer.parseInt(inputCategoryId));
						
						urlPatternBeanList.add(patternBean);
						
					}catch(Exception e){
						errorMsg = "Parse data error.";
						break;
					}
				}
				record.close();
			}catch(Exception e){
				trackLogger.warn("Can't parse import url pattern line data : " + line);
				e.printStackTrace();
			}
		}
		return errorMsg;
	}
	
	
	private @ResponseBody void exportPattern(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String file = "";
		try{
			
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0);
			String jsonString = StringUtils.defaultString((String) request.getParameter("jsonData"));
			String dateFile = DateTimeUtil.generateStringDate(new Date(), "yyyyMMdd-HHmmss");
			file = "Pattern-" + merchantId + "-" + dateFile;
			
			out.println("MerchantId,Condition,Value,Action,Group,Keyword,CategoryId");
			
			if(merchantId > 0 && StringUtils.isNotBlank(jsonString)) {
				JSONArray jArr = (JSONArray)new JSONParser().parse(jsonString.toString());
				int recNum = 0;
				if(jArr != null && jArr.size() > 0){
					for(Object jObject : jArr){
						JSONObject jObj = (JSONObject) jObject;
						String condition = (String) jObj.get("condition");
						String value = (String) jObj.get("value");
						String action = (String) jObj.get("action");
						String group = (String) jObj.get("group");
						String keyword = (String) jObj.get("keyword");
						String categoryId = (String) jObj.get("categoryId");
						
						out.print("\""+merchantId +"\",");
						out.print("\""+condition +"\",");
						out.print("\""+value +"\",");
						out.print("\""+action +"\",");
						out.print("\""+group +"\",");
						out.print("\""+keyword +"\",");
						out.println("\""+categoryId+"\"");
						recNum++;
					}
				}
			    LogUtil.info(request, PAGE, "exportPattern-->" + recNum +" record(s)|MerchantId:"+ merchantId);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			response.setContentType("application/csv");
	        response.addHeader("Content-Disposition", "attachment; filename = " + file); 
	        response.addHeader("X-Frame-Options", "ALLOWALL");
	        response.flushBuffer();
	        
			out.flush();  
		    out.close();
		}
	}
	
	
	@RequestMapping(value = "/admin/runpattern")
	public void runPattern(HttpServletRequest request, HttpServletResponse res, @RequestParam("obj") String data, @RequestParam("mId") String mId) throws Exception {
	
		HashMap<String, Object> rtn = new HashMap<String, Object>();
		JSONArray jsonDataArray = (JSONArray) new JSONParser().parse(data);
		int merchantId = Integer.valueOf(mId);
		Properties merchantConfig = loadMerchantConfig(merchantId);
		
		//------------ find start url ----------//
		List<UrlPatternBean> startUrlBeanList = new ArrayList<UrlPatternBean>();
		List<UrlPatternBean> otherBeanList = new ArrayList<UrlPatternBean>();
		addURLPattern(startUrlBeanList, otherBeanList, jsonDataArray);
		
		//------------ start crawler ----------//		
		URLCrawlerUtil r = new URLCrawlerUtil();
		r.initialize(merchantId, startUrlBeanList, otherBeanList, merchantConfig);
		r.setCheckDropAnalyzer (false);
		r.setCheckOtherUrlAnalyzer (false);
		r.process();

		//------------get result ----------//
		JSONArray jsonArr = r.getJsonArr();
		if(jsonArr.toString().equals("[]")) {
			rtn.put("header", "error");
		}else {
			rtn.put("header", "success");
			rtn.put("result", jsonArr);
		}
		LogUtil.info(request, PAGE, "runTest|MerchantId:"+ merchantId);
		JsonWriter.getInstance().write(res, rtn);
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/rungenerateurl")
	public void runGenerateUrl(HttpServletRequest request, HttpServletResponse res, @RequestParam("obj") String data, @RequestParam("mId") String mId) throws Exception {
		HashMap<String, Object> rtn = new HashMap<String, Object>();
		JSONArray jsonDataArray = (JSONArray) new JSONParser().parse(data);
		int merchantId = Integer.valueOf(mId);

		List<Map<String, String>> linkParamList = new ArrayList<>();
		List<String> allTxtList = new ArrayList<>();
		for (Object obj : jsonDataArray) {
			JSONObject jObj = (JSONObject)obj;
			String url = (String)jObj.get("value"); 
			if(StringUtils.isNotBlank(url)) {
				allTxtList.add(url);
				Map<String, String> param = getParamFromLink(url);
				linkParamList.add(param);
			}
		}
		
		// haveQuery
		List<String> mainKeyList = new ArrayList<>();
		if(linkParamList != null && linkParamList.size() > 0) {
			Map<String, String> mainMap = linkParamList.get(0);
			if(mainMap != null) {
				for (Entry<String, String> mainEntry : mainMap.entrySet()) {
					String mainKey = mainEntry.getKey();
					boolean isKeyFromAllLink = true;
					for (Map<String, String> map : linkParamList) {
						if(!map.containsKey(mainKey)) isKeyFromAllLink = false;
					}
					if(isKeyFromAllLink) {
						mainKeyList.add(mainKey);
					}
				}
				
			}
		}

		List<String> haveQueryList = new ArrayList<>();
		if(mainKeyList != null && mainKeyList.size() > 0) {
			for (String mainKey : mainKeyList) {
				List<String> valueList = new ArrayList<>();
				boolean isUnique = true;
				for (Map<String, String> map : linkParamList) {
					String value = map.get(mainKey);
					if(valueList.contains(value)) {
						isUnique = false;
					}else {
						valueList.add(value);
					}
				}
				if(isUnique && StringUtils.isNotBlank(mainKey)) {
					haveQueryList.add(mainKey);
				}
			}
		}
		
		// endWith  
		List<String> endWithList = new ArrayList<>();
		String[] allTxt = allTxtList.toArray(new String[allTxtList.size()]);
		if(allTxt !=null && allTxt.length > 0) {
			int size = allTxt.length;
			String[] reverseTxt = new String[size];
			for (int i=0; i<size; i++) {
				reverseTxt[i] = new StringBuilder(allTxt[i]).reverse().toString();
			}
			
			String mainTxt = reverseTxt[0];
			String result = "";
			OuterLoop :
			for(int i=0; i<mainTxt.length(); i++) {
				char mainChar = mainTxt.charAt(i);
				for(int j=0; j<size; j++) {
					if(reverseTxt[j].charAt(i) != mainChar) {
						break OuterLoop;
					}
				}
				result = mainChar + result;
			}
			if(StringUtils.isNotBlank(result)) {
				endWithList.add(result);				
			}
		}
		// haveString
		List<String> haveStringList = new ArrayList<>();
		if(allTxt !=null && allTxt.length > 0) {
			
			if(allTxtList != null && allTxtList.size()>0) {
				String mainLink = allTxtList.get(0);

				mainLink = Util.getStringAfter(mainLink, "//", mainLink);
				mainLink = Util.getStringAfter(mainLink, "/", mainLink);

				String[] result = mainLink.split("[&?.,!=+\\-*/\\^ ]+");

				for (String str : result) {
					boolean isAllLink = true;
					for (String link : allTxtList) {
						if(!link.contains(str)) isAllLink = false;
					}
					if(isAllLink && !haveStringList.contains(str)) {
						haveStringList.add(str);
					}
				}
			}
		}
		
		JSONArray haveQueryArr = new JSONArray();
		JSONArray endwithArr = new JSONArray();
		JSONArray haveStringArr = new JSONArray();
		for(String urlStr : haveQueryList) {
			JSONObject tmpJson = new JSONObject();
			tmpJson.put("value", urlStr);
			tmpJson.put("condition", "haveQuery");
			haveQueryArr.add(tmpJson);
		}
		for(String urlStr : endWithList){
			JSONObject tmpJson = new JSONObject();
			tmpJson.put("value", urlStr);
			tmpJson.put("condition", "endsWith");
			endwithArr.add(tmpJson);
		}
		for(String urlStr : haveStringList){
			JSONObject tmpJson = new JSONObject();
			tmpJson.put("value", urlStr);
			tmpJson.put("condition", "haveString");
			haveStringArr.add(tmpJson);
		}
		JSONObject rtnJson = new JSONObject();
		JSONArray jsonArr = new JSONArray();
		rtnJson.put("haveQueryArr", haveQueryArr);
		rtnJson.put("endwithArr", endwithArr);
		rtnJson.put("haveStringArr", haveStringArr);
		jsonArr.add(rtnJson);
		
		if(jsonArr.toString().equals("{}")) {
			rtn.put("header", "error");
		}else {
			rtn.put("header", "success");
			rtn.put("result", jsonArr);
		}
		LogUtil.info(request, PAGE, "rungenerateurl|MerchantId:"+ merchantId);
		JsonWriter.getInstance().write(res, rtn);
	}
	
	
	@RequestMapping(value = "/admin/runurlAnalysis")
	public void runUrlAnalysis(HttpServletRequest request, HttpServletResponse res, @RequestParam("obj") String data, @RequestParam("mId") String mId) throws Exception {
		
		HashMap<String, Object> rtn = new HashMap<String, Object>();
		JSONArray jsonDataArray = (JSONArray) new JSONParser().parse(data);
		int merchantId = Integer.valueOf(mId);
		Properties merchantConfig = loadMerchantConfig(merchantId);
		
		//------------ find start url ----------//
		List<UrlPatternBean> startUrlBeanList = new ArrayList<UrlPatternBean>();
		List<UrlPatternBean> otherBeanList = new ArrayList<UrlPatternBean>();
		addURLPattern(startUrlBeanList, otherBeanList, jsonDataArray);
		
		//------------ start crawler ----------//		
		URLCrawlerUtil r = new URLCrawlerUtil();
		r.initialize(merchantId, startUrlBeanList, otherBeanList, merchantConfig);
		r.setCheckDropAnalyzer (true);
		r.setCheckOtherUrlAnalyzer (true);
		r.process();
		
		//------------get result ----------//
		JSONArray jsonArr = r.getJsonArr();
		if(jsonArr.toString().equals("[]")) {
			rtn.put("header", "error");
		}else {
			rtn.put("header", "success");
			rtn.put("result", jsonArr);
		}
		LogUtil.info(request, PAGE, "runurlAnalysis|MerchantId:"+ merchantId);
		JsonWriter.getInstance().write(res, rtn);
	}
	
	
	public void addURLPattern(List<UrlPatternBean> startUrlBeanList, List<UrlPatternBean> otherBeanList, JSONArray jsonDataArray){
		
		for(Object obj : jsonDataArray) {
			JSONObject jsonData = (JSONObject) obj;
			String action = (String) jsonData.get("action");
			String group = (String) jsonData.get("group");
			String value = (String) jsonData.get("value");
			String condition = (String) jsonData.get("condition");
			
			UrlPatternBean urlPatternBean = new UrlPatternBean();
			urlPatternBean.setAction(action);
			urlPatternBean.setGroup(Integer.valueOf(group));
			urlPatternBean.setValue(value);
			urlPatternBean.setCondition(condition);
			if(action.equalsIgnoreCase("STARTURL")) {
				startUrlBeanList.add(urlPatternBean);
			} else {
				otherBeanList.add(urlPatternBean);
			}
		}
	}
	
	
	public void run(List<String> urlList, Set<String> contSet, Set<String> matchSet, List<UrlPatternBean> otherBeanList){
		UrlPatternManager urlPatternManager = new UrlPatternManager(otherBeanList);
		for(String urlStr : urlList) {
			boolean isMatch = urlPatternManager.isPatternMatch(urlStr);
			boolean isContinue = urlPatternManager.isPatternContinue(urlStr);
			if(urlPatternManager.isPatternDrop(urlStr) || (!isMatch && !isContinue)) {
				continue;
			}
			urlStr = urlPatternManager.processRemoveQuery(urlStr);
			if(isMatch) {
				matchSet.add(urlStr);
			}
			if(isContinue) {
				contSet.add(urlStr);
			}
			
		}
	}
	
	
	private Properties loadMerchantConfig(int merchantId) throws SQLException {
		Properties rtn = new Properties();
		MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		List<MerchantConfigBean> mcList = mcDB.findMerchantConfig(merchantId);
		
		for (MerchantConfigBean mcBean : mcList) {
			rtn.setProperty(mcBean.getField(), mcBean.getValue());
		}		
		return rtn;
	}
	
	
	public List<String> parseUrl(String html, String startUrl) throws Exception {
		
		HTMLParser parse = new HTMLParser();
        parse.source = new StringBuffer(html);
        Set<String> rtnSet = new HashSet<>();
        
        //find all the links
        while (!parse.eof()) {
            char ch = parse.get();
            if (ch == 0) {
                HTMLTag tag = parse.getTag();
                Attribute link = tag.get("HREF");
                if (link == null)
                    link = tag.get("SRC");

                if (link == null)
                    continue;         
                
                URL u = null;     
                try {
                    if (link.getValue().startsWith("?")) {
                        u = URLUtility.addNewQuery(new URL(startUrl), link.getValue());
                    } else {
                        u = new URL(new URL(startUrl), link.getValue());
                    }
                } catch (MalformedURLException e) {
                    continue;
                }

                u = URLUtility.stripAnhcor(u);
                u = URLUtility.formatToValidURL(u);          	                              
                               
                if (u.getHost().equalsIgnoreCase( new URL(startUrl).getHost())) {
                	String url = u.toString();
                	rtnSet.add(url);
                }
            }
        }
		
		return new ArrayList<>(rtnSet);
	}
	
	
	@SuppressWarnings("unchecked")
	public JSONArray sortJsonArray(JSONArray inJson) {
		JSONArray rtnJsonArray = new JSONArray();
		List<JSONObject> jsonList = new ArrayList<>();
		for(int i = 0 ; i < inJson.size() ; i++) {
			jsonList.add((JSONObject) inJson.get(i));
		}
		
		Collections.sort(jsonList, new Comparator<JSONObject>() {
			private final String KEY_NAME = "url";
			
			public int compare(JSONObject a, JSONObject b) {
				String valA = new String();
				String valB = new String();
				
				valA = (String) a.get(KEY_NAME);
				valB = (String) b.get(KEY_NAME);			
				
				return valA.compareTo(valB);
			}
		});
		
		for(int i = 0 ; i < jsonList.size() ; i++) {
			rtnJsonArray.add(jsonList.get(i));
		}
		
		return rtnJsonArray;
	}

	
	@SuppressWarnings("unchecked")
	private void insertHistory(List<UrlPatternBean> newPatternList, List<UrlPatternBean> oldPatternList, int merchantId, UserBean userBean){
		try{
			
			List<UrlPatternHistoryBean> deleteHistoryList = new ArrayList<>();
			List<UrlPatternHistoryBean> changeHistoryList = new ArrayList<>();
			List<UrlPatternHistoryBean> addHistoryList = new ArrayList<>();
			
			Map<String, UrlPatternBean> tempMap = new HashMap<>();
			if(oldPatternList != null && oldPatternList.size() > 0){
				for (UrlPatternBean patternBean : oldPatternList) {
					tempMap.put(patternBean.getValue(), patternBean);
				}
			}
			
			if(newPatternList != null && newPatternList.size() > 0){
				for (UrlPatternBean patternBean : newPatternList) {
					if(tempMap.containsKey(patternBean.getValue())){
						UrlPatternBean oldPatternBean = tempMap.get(patternBean.getValue());
						JSONObject jObj = new JSONObject();
						boolean change = false;
	
						if(patternBean.getGroup() != oldPatternBean.getGroup()) 				{change=true; jObj.put("oldGroup", oldPatternBean.getGroup()); }
						if(patternBean.getCategoryId() != oldPatternBean.getCategoryId()) 		{change=true; jObj.put("oldCategoryId", oldPatternBean.getCategoryId()); }
						
						if(!StringUtils.defaultString(patternBean.getCondition()).equals(StringUtils.defaultString(oldPatternBean.getCondition()))) 	{change=true; jObj.put("oldCondition", oldPatternBean.getCondition()); }
						if(!StringUtils.defaultString(patternBean.getKeyword()).equals(StringUtils.defaultString(oldPatternBean.getKeyword()))) 		{change=true; jObj.put("oldKeyword", oldPatternBean.getKeyword()); }
						if(!StringUtils.defaultString(patternBean.getAction()).equals(StringUtils.defaultString(oldPatternBean.getAction()))) 			{change=true; jObj.put("oldAction", oldPatternBean.getAction()); }
						
						if(change){
							UrlPatternHistoryBean historyBean = UrlPatternHistoryBean.createUrlPatternHistoryBean(patternBean, userBean.getUserId(), ACTION.CHANGE, jObj.toString());
							changeHistoryList.add(historyBean);
						}
						tempMap.remove(patternBean.getValue());
					}else{
						UrlPatternHistoryBean historyBean = UrlPatternHistoryBean.createUrlPatternHistoryBean(patternBean, userBean.getUserId(), ACTION.ADD, null);
						addHistoryList.add(historyBean);
					}
				}
			}
			
			if(tempMap != null && tempMap.size() > 0){
				for (Entry<String, UrlPatternBean> entry : tempMap.entrySet()) {
					UrlPatternHistoryBean historyBean = UrlPatternHistoryBean.createUrlPatternHistoryBean(entry.getValue(), userBean.getUserId(), ACTION.DELETE, null);
					deleteHistoryList.add(historyBean);
				}
			}

			UrlPatternHistoryDB urlPatternHistoryDB = DatabaseManager.getDatabaseManager().getUrlPatternHistoryDB();
			if(deleteHistoryList != null && deleteHistoryList.size() > 0) 	urlPatternHistoryDB.insertHistoryBatch(deleteHistoryList, merchantId);
			if(changeHistoryList != null && changeHistoryList.size() > 0) 	urlPatternHistoryDB.insertHistoryBatch(changeHistoryList, merchantId);
			if(addHistoryList != null && addHistoryList.size() > 0) 		urlPatternHistoryDB.insertHistoryBatch(addHistoryList, merchantId);
			
			tempMap.clear();
		}catch(Exception e){
			e.printStackTrace();
		}
	}	 
	
	
	private static Map<String, String> getParamFromLink(String link){
		Map<String, String> resultMap = new HashMap<>();
		if(StringUtils.isNotBlank(link)) {
			String txt = Util.getStringAfter(link,"?","");
			if(StringUtils.isBlank(txt)) return null;
	
			List<String> keyList = Util.getAllStringBetween("&"+txt, "&", "=");
			if(keyList == null || keyList.size() == 0) return null;
	
			List<String> valueList = Util.getAllStringBetween(txt+"&", "=", "&");
			if(valueList == null || valueList.size() == 0) return null;
	
			if(keyList.size() != valueList.size()) return null;
			for (int i=0; i<keyList.size(); i++) {
				resultMap.put(keyList.get(i), valueList.get(i));
			}
		}
		return resultMap;
	}
	

 

}
