package botbackend.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import botbackend.bean.BotQualityReportBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.UserBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.BotQualityReportDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.db.web.MerchantMappingDB;
import botbackend.system.BaseConfig;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;
import botbackend.utils.ViewUtil;
import utils.BotUtil;
import utils.DateTimeUtil;


@Controller
public class MerchantManagerController {
	
	final static String PAGE = "New Merchant";
	final static String permissionName = "merchant.merchantmanager";
	private static final int numRecord = 20;
	private static final int maxNodeTest = 1;
	private static final int USER_STATUS = 1;
	private static final String[] BOT_5_COUNTRY = new String[] {"th"};
	private static String BOT_SERVER_CURREBT_COUNTRY = BaseConfig.BOT_SERVER_CURRENT_COUNTRY;
	
	@RequestMapping(value = {"/admin/merchantmanager"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		return getMerchantData(1, null, "-1", "", "", "", "", "", request, response);
	}
	
	@RequestMapping(value = {"/admin/getmerchantdata"})
	public ModelAndView getMerchantData(
			@RequestParam(required=false, defaultValue="1", value="page") int page,
			@RequestParam(required=false, defaultValue="",  value="searchParam") String searchParam,
			@RequestParam(required=false, defaultValue="",  value="activeType") String activeType,
			@RequestParam(required=false, defaultValue="",  value="serverType") String serverType,
			@RequestParam(required=false, defaultValue="",  value="dataserverType") String dataserverType,
			@RequestParam(required=false, defaultValue="",  value="userFilter") String userFilter,
			@RequestParam(required=false, defaultValue="",  value="packageFilter") String packageFilter,
			@RequestParam(required=false, defaultValue="",  value="userUpdate") String userUpdate,
			HttpServletRequest request, HttpServletResponse response) {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = null;
		try {
			
			if(StringUtils.isNotBlank(searchParam)) {
				searchParam = searchParam.trim();
			}
			
			UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
			List<UserBean> userList = userDB.getUserList(USER_STATUS);
			
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean> merchantList = new ArrayList<MerchantBean>();
			
			PagingBean pagingBean = null;
			
			int totalmerchantList = merchantDB.countSearchMerchant(searchParam, activeType, serverType, dataserverType, userFilter, packageFilter, userUpdate);
			if(totalmerchantList > 0){
				int start = (page - 1) * numRecord;
				if(start >= totalmerchantList) {
					start = 0;
				}
				
				int totalPage = (int)(Math.ceil((double)totalmerchantList/(double)numRecord));
				
				if(page > totalPage) {
					page = 1;
				}
				
				if(merchantList.isEmpty()){
					merchantList = merchantDB.getSearchMerchant(searchParam, activeType, serverType, dataserverType, userFilter, packageFilter, userUpdate, start, numRecord);
				}
		
				pagingBean = PagingBean.createPagingBean(page, totalmerchantList, numRecord);
				
			}
			
			model = new ModelAndView("merchantmanager", "newPage", true);
			
			if(merchantList != null && merchantList.size() > 0){
				model.addObject("merchantList", merchantList);
				model.addObject("pagingBean", pagingBean);
			}

			model.addObject("userList", userList);
			model.addObject("totalmerchantList", totalmerchantList);
			model.addObject("searchParam", searchParam);
			model.addObject("activeType", activeType);
			model.addObject("serverType", serverType);
			model.addObject("dataserverType", dataserverType);
			model.addObject("userFilter", userFilter);
			model.addObject("userUpdate", userUpdate);
			model.addObject("packageFilter", packageFilter);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return model;
	}
	
	@RequestMapping(value = {"/admin/addnewmerchant"})
	public ModelAndView addNewMerchant(
			@RequestParam(required=false, defaultValue="",  value="merchantName") String merchantName,
			@RequestParam(required=false, defaultValue="",  value="packageType") String packageTypeInput, // recheck it's number
			@RequestParam(required=false, defaultValue="",  value="saleName") String saleName,
			@RequestParam(required=false, defaultValue="",  value="siteUrl") String siteUrl,
			@RequestParam(required=false, defaultValue="",  value="merchantType") String merchantType,
			@RequestParam(required=false, defaultValue="",  value="note") String note,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("merchantmanager", "newPage", true);
		
		try {
			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			String warning = "";
			
			UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
			List<UserBean> userList = userDB.getUserList(USER_STATUS);
			
			List<MerchantBean> merchantList = new ArrayList<MerchantBean>();
			if(StringUtils.isNotBlank(merchantName)){
				int packageType = 0;
				if(StringUtils.isNotBlank(packageTypeInput) && NumberUtils.isCreatable(packageTypeInput)) {
					packageType = Integer.parseInt(packageTypeInput);
				}

				MerchantMappingDB merchantMapping  = DatabaseManager.getDatabaseManager().getMerchantMappingDB();
				MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
				
				int nodeTest = getRandomNodeTest();
				int merchantId = merchantMapping.getMaxMerchantIdByRange(Util.getMerchantIdStartRange(), Util.getMerchantIdEndRange());
				int merchantIdBackEnd = merchantDB.getMaxMerchantIdByRange(Util.getMerchantIdStartRange(), Util.getMerchantIdEndRange());
				if(merchantId <= Util.getMerchantIdStartRange() && merchantIdBackEnd <= Util.getMerchantIdStartRange()){
					merchantId = Util.getMerchantIdStartRange();
				} else if (merchantId!=merchantIdBackEnd) {
					merchantId = Math.max(merchantId, merchantIdBackEnd);
				}
				
				MerchantBean merchantBean = new MerchantBean();
				merchantBean.setName(merchantName);
				merchantBean.setActive(0);
				merchantBean.setPackageType(packageType);
				merchantBean.setMerchant_package_runtime(packageType);
				merchantBean.setSale_name(saleName);
				merchantBean.setDueErrorCount(0);
				merchantBean.setWceErrorCount(0);
				merchantBean.setServer("");
				merchantBean.setDataServer("");
				merchantBean.setNodeTest(nodeTest);
				merchantBean.setSlug(Util.formatSlugName(merchantName)); 
				merchantBean.setOwnerId(userBean.getUserId());
				merchantBean.setUserUpdate(userBean.getUserName());
				merchantBean.setLastUpdate(new Date());
				merchantBean.setSite_url(siteUrl);
				merchantBean.setNote(note);
				if(StringUtils.isNotBlank(note)) {
					merchantBean.setNoteDate(new Date());
				}
				
				if(merchantType.equals("customer")) {
					merchantBean.setRedirect_type_runtime(2);
					merchantBean.setRedirect_type(2);
					merchantBean.setMobile_redirect_type(1);
					merchantBean.setEnable_report(1);
				} else if(merchantType.equals("test")) {
					merchantBean.setRedirect_type_runtime(6);
					merchantBean.setRedirect_type(6);
					merchantBean.setMobile_redirect_type(0);
					merchantBean.setEnable_report(0);
				}
				
				if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
					merchantBean.setState("BOT_FINISH");
					merchantBean.setStatus("FINISH");
					merchantBean.setDataUpdateState("BOT_FINISH");
					merchantBean.setDataUpdateStatus("FINISH");
					merchantBean.setWceNextStart(new Date());
					merchantBean.setDueNextStart(new Date());
				} else {
					merchantBean.setState("CRAWLER_START");
					merchantBean.setStatus("WAITING");
					merchantBean.setDataUpdateState("DATA_UPDATE_START");
					merchantBean.setDataUpdateStatus("WAITING");
					merchantBean.setWceNextStart(null);
					merchantBean.setDueNextStart(null);
				}
				
				merchantBean.setOwnerName(userBean.getUserName());
				merchantBean.setId(merchantId);
				
				if (merchantId <= Util.getMerchantIdEndRange()) {
					List<MerchantBean> mList = merchantMapping.getSlug(Util.formatSlugName(merchantName));
					if(mList != null && mList.size()> 0 ){
						for (MerchantBean mBean : mList) {
							warning += "Duplicate-"+ mBean.getMerchant_id() + ",";
						}
						warning = warning.substring(0,warning.length()-1);
						LogUtil.info(request, PAGE, "Duplicate MerchantName ;" + merchantId );
					} else{
						merchantMapping.insertNewMerchantMapping(merchantBean);
						merchantDB.insertNewMerchant(merchantBean);
						merchantList.add(merchantBean);
						LogUtil.info(request, PAGE, "addNewMerchant|" + merchantId + ", " + merchantName);
					}
				} else {
					warning = "MerchantID is out of range (" + merchantId + ">" + Util.getMerchantIdEndRange() + ")";
					LogUtil.info(request, PAGE, "MerchantID is out of range (" + merchantId + ">" + Util.getMerchantIdEndRange() + ")");
				}
			} else {
				warning = "Not found merchantName";
				LogUtil.info(request, PAGE, "Not found merchantName");
			}
			
			if(merchantList != null && merchantList.size() > 0) {
				model = new ModelAndView("merchantmanager", "newPage", true);
				model.addObject("merchantList", merchantList);
				model.addObject("pagingBean", null);
				model.addObject("totalmerchantList", 1);
			}else {
				model.addObject("warning", warning);
			}
			
			model.addObject("userList", userList);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return model;
		
	}
	
	@RequestMapping(value = {"/admin/editmerchant"})
	public ModelAndView editMerchant(
			@RequestParam(required=false, defaultValue="",  value="merchantName") String merchantName,
			@RequestParam(required=false, defaultValue="0", value="packageType") String packageTypeInput,
			@RequestParam(required=false, defaultValue="0", value="active") int active,
			@RequestParam(required=false, defaultValue="0", value="merchantID") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="servername") String servername,
			@RequestParam(required=false, defaultValue="",  value="dataservername") String dataservername,
			@RequestParam(required=false, defaultValue="",  value="currentserver") String currentserver,
			@RequestParam(required=false, defaultValue="",  value="currentdataserver") String currentdataserver,
			@RequestParam(required=false, defaultValue="",  value="ownerName") String ownerName,
			@RequestParam(required=false, defaultValue="",  value="note") String note,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("merchantmanager", "newPage", true);
		try {
			
			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			
			UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
			List<UserBean> userList = userDB.getUserList(USER_STATUS);
			
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			MerchantBean merchantBean  = merchantDB.getMerchantByIdAndServerWithOwner(merchantId, currentserver);

			int ownerId = 0;
			UserBean ownerUserBean = userDB.getUserIDbyUserName(ownerName);
			if(ownerUserBean != null){
				ownerId = ownerUserBean.getUserId();
			}
			
			String warning = "";
			List<MerchantBean> merchantList = new ArrayList<MerchantBean>();
			
			int packageType = 0;
			if(StringUtils.isNotBlank(packageTypeInput) && NumberUtils.isCreatable(packageTypeInput)) {
				packageType = Integer.parseInt(packageTypeInput);
			}
			
			if(StringUtils.isBlank(dataservername) && StringUtils.isNotBlank(currentdataserver)) {
				dataservername = currentdataserver;
			}
			
			if(merchantBean == null) {
				warning = "Not found merchant in database";
				LogUtil.info(request, PAGE, "Not found merchant in database");
			}else if(StringUtils.isBlank(merchantName)) {
				warning = "Cannot edit, Require merchantName";
				LogUtil.info(request, PAGE, "Cannot edit, Require merchantName");
			} else if(StringUtils.isBlank(dataservername) && active == 1) {
				warning = "Cannot edit, Require dataserver for active merchant";
				LogUtil.info(request, PAGE, "Cannot edit, Require dataserver for active merchant");
			} else {
				merchantBean.setCurrentserver(merchantBean.getServer());
				merchantBean.setName(merchantName);
				merchantBean.setPackageType(packageType);
				merchantBean.setServer(servername);
				merchantBean.setDataServer(dataservername);
				merchantBean.setActive(active);
				merchantBean.setOwnerId(ownerId);
				if(!BotUtil.checkStringEqual(merchantBean.getNote(), note)) {
					merchantBean.setNote(note);
					merchantBean.setNoteDate(new Date());
				}
				
				int success = merchantDB.updateNewMerchant(merchantBean);
				if (success > 0) {
					LogUtil.info(request, PAGE, "editNewMerchant|MerchantId:" + merchantId );					merchantBean.setUserUpdate(userBean.getUserName());
					merchantBean.setLastUpdate(new Date());
					merchantList.add(merchantBean);
				} else {
					warning = "Cannot edit merchantId " + merchantBean.getId();
					LogUtil.info(request, PAGE, "Cannot edit merchantId " + merchantBean.getId());
				}
			}
			
			if(merchantList != null && merchantList.size() > 0) {
				model = new ModelAndView("merchantmanager", "newPage", true);
				model.addObject("merchantList", merchantList);
				model.addObject("pagingBean", null);
				model.addObject("totalmerchantList", 1);
			}else {
				model.addObject("warning", warning);
			}
			
			model.addObject("userList", userList);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return model;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/getmerchantdatabyid"})
	public @ResponseBody void getMerchantDataById(
			@RequestParam(required=false, defaultValue="0", value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="currentserver") String currentserver,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		try{
			JSONObject json = new JSONObject();
			Map<Object, Object> header = new HashMap<Object, Object>();	
			if(merchantId > 0) {
				try {
					MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
					MerchantBean merchantBean = merchantDB.getMerchantByIdAndServerWithOwner(merchantId, currentserver);
					
					if(merchantBean != null ){
						header.put("name", merchantBean.getName());
						header.put("packageType", merchantBean.getPackageType());
						header.put("note", merchantBean.getNote());
						header.put("server", merchantBean.getServer());
						header.put("dataserver", merchantBean.getDataServer());
						header.put("ownerName", merchantBean.getOwnerName());
						header.put("active", merchantBean.getActive());
						header.put("merchantId", merchantBean.getId());
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			json.put("header", header);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/submitmerchant"})
	public @ResponseBody void submitMerchant(
			@RequestParam(required=false, defaultValue="0", value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="currentserver") String currentServer,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		try{
			JSONObject json = new JSONObject();
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			MerchantBean merchantBean  = merchantDB.getMerchantByIdAndServerWithOwner(merchantId, currentServer);

			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			
			String warning = "";
			if(merchantBean != null) {
				try {
					
					UrlPatternDB urlpatternDB  = DatabaseManager.getDatabaseManager().getUrlPatternDB();
					MerchantConfigDB merchantconfigDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
					ParserConfigDB parserConfigDB =  DatabaseManager.getDatabaseManager().getParserConfigDB();
					
					boolean allowSubmit = false;
					int success = 0;
					int nodeTest = getRandomNodeTest();
					
					if(merchantBean.getWceNextStart() != null || merchantBean.getDueNextStart() != null) {
						
						MerchantConfigBean parserClassBean = merchantconfigDB.findMerchantConfigByIdAndField(merchantId, "parserClass");
						if(parserClassBean == null) {
							MerchantConfigBean feedCrawlerClassBean = merchantconfigDB.findMerchantConfigByIdAndField(merchantId, "feedCrawlerClass");
							if(feedCrawlerClassBean != null) {
								allowSubmit = true;
							} else {
								warning = "Not allow, parserClass or feedCrawlerClass";
								LogUtil.info(request, PAGE, "submitNewMerchant|MerchantId:" + merchantId + "|Not allow, parserClass or feedCrawlerClass");
							}
						} else {
							allowSubmit = true;
						}
						if(allowSubmit) {
							success = merchantDB.updateMerchantNode(nodeTest, 3, merchantId, "BOT_FINISH", "BOT_FINISH", "FINISH", currentServer );
						}
						
					} else {
						
					    int countActionStartUrl = urlpatternDB.countActionByMerchantId(merchantId, "STARTURL");
					    if(countActionStartUrl > 0) {
					    	MerchantConfigBean parserClassBean = merchantconfigDB.findMerchantConfigByIdAndField(merchantId, "parserClass");
					    	if(parserClassBean == null) {
					    		int countParserName = parserConfigDB.countByFieldByMerchantId(merchantId, "name");
					    		int countParserPrice = parserConfigDB.countByFieldByMerchantId(merchantId, "price");
					    		if(countParserName > 0 && countParserPrice > 0) {
					    			allowSubmit = true;
					    		} else {
					    			warning = "Not allow, need parserConfig for name and price or parserClass";
					    			LogUtil.info(request, PAGE, "submitNewMerchant|MerchantId:" + merchantId + "|Not allow, need parserConfig for name and price or parserClass");
					    		}
					    	} else {
					    		allowSubmit = true;
					    	}
					    } else {
					    	warning = "Not allow, need starturl";
					    	LogUtil.info(request, PAGE, "submitNewMerchant|MerchantId:" + merchantId + "|Not allow, need starturl");
					    }
					    if(allowSubmit) {
					    	success = merchantDB.updateMerchantNode(nodeTest, 3, merchantId, "CRAWLER_START", "DATA_UPDATE_START", "WAITING", currentServer );
						}
					    
					}
					
					if (success > 0) {
						merchantDB.setMerchantRecentUpdate(merchantBean.getId(), merchantBean.getServer(), userBean.getUserName());
						LogUtil.info(request, PAGE, "submitNewMerchant|MerchantId:" + merchantId );
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			json.put("warning", warning);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/stopmerchant"})
	public @ResponseBody void stopMerchant(
			@RequestParam(required=false, defaultValue="0", value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="currentserver") String currentServer,
			@RequestParam(required=false, defaultValue="",  value="errorMessage") String errorMessage,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		String header = "error";	
		JSONObject json = new JSONObject();
		try{	
			if(errorMessage.length() > 0 &&  StringUtils.isNotBlank(errorMessage)){
				MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
				MerchantBean merchantBean  = merchantDB.getMerchantByMerchantIdAndServer(merchantId,currentServer);
				if(merchantBean != null) {
					try {
						merchantBean.setId(merchantId);
						merchantBean.setActive(13);
						merchantBean.setCurrentserver(currentServer);
						merchantBean.setErrorMessage(errorMessage);
						
						if(merchantBean.getWceNextStart() != null || merchantBean.getDueNextStart() != null) {
							merchantBean.setState("BOT_FINISH");
							merchantBean.setStatus("FINISH");
							merchantBean.setDataUpdateState("BOT_FINISH");
							merchantBean.setDataUpdateStatus("FINISH");
						} else {
							merchantBean.setState("CRAWLER_START");
							merchantBean.setStatus("WAITING");
							merchantBean.setDataUpdateState("DATA_UPDATE_START");
							merchantBean.setDataUpdateStatus("WAITING");
						}
						
						int success  = merchantDB.updateNewMerchant(merchantBean);
						if (success > 0) {
							header = "success";
							LogUtil.info(request, PAGE, "stopNewMerchant|MerchantId:" + merchantId );
						}
		
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			json.put("header", header);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/commandmerchant"})
	public @ResponseBody void commandMerchant(
			@RequestParam(required=false, defaultValue="0", value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="command") String command,
			@RequestParam(required=false, defaultValue="",  value="action") String action,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		String header = "error";	
		JSONObject json = new JSONObject();

		try{	
			
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			MerchantBean merchantBean  = merchantDB.getMerchantByMerchantId(merchantId);
				
			int success = 0;
			String[] commandString = command.split(",");
			if(commandString!=null&&commandString.length >0) {
				for(String s:commandString) {
					if(merchantBean.getCommand() == null) {
						if(action.toLowerCase().equals("add")) 
						success = merchantDB.updateCommand(merchantId, s+",");
					}else if(merchantBean.getCommand().contains(s+",")) { 
						if(action.toLowerCase().equals("remove")) 
						success = merchantDB.removeCommand(merchantId, s+",");
					}else {
						if(action.toLowerCase().equals("add")) 
						success = merchantDB.updateCommand(merchantId, merchantBean.getCommand() +s+",");
					}
				}
			}else {
				if(merchantBean.getCommand() == null) {
					if(action.toLowerCase().equals("add")) 
					success = merchantDB.updateCommand(merchantId, command+",");
				}else if(merchantBean.getCommand().contains(command+",")) { 
					if(action.toLowerCase().equals("remove")) 
					success = merchantDB.removeCommand(merchantId, command+",");
				}else {
					if(action.toLowerCase().equals("add")) 
					success = merchantDB.updateCommand(merchantId, merchantBean.getCommand() +command+",");
				}
			}
			
			if(success > 0) { 
				header = "success";
			}

			json.put("header", header);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/favoritemerchant"})
	public @ResponseBody void favoriteMerchant(
			@RequestParam(required=false, defaultValue="0", value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="currentServer") String currentServer,
			@RequestParam(required=false, defaultValue="",  value="statusFavorite") String statusFavorite,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		String header = "error";	
		JSONObject json = new JSONObject();
		HttpSession session = request.getSession(false);
		UserBean userBean = (UserBean)session.getAttribute("userData");
		try{	
				MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
				MerchantBean merchantBean  = merchantDB.getMerchantByMerchantIdAndServer(merchantId,currentServer);
					
				if(merchantBean != null) {
					try {

						merchantBean.setCurrentserver(currentServer);
						if(statusFavorite.equals("true")){
							if(merchantBean.getCoreUserConfig()==null){
								merchantBean.setCoreUserConfig(userBean.getUserId()+"|");
							}else{
								merchantBean.setCoreUserConfig(merchantBean.getCoreUserConfig()+userBean.getUserId()+"|");									
							}
						}else{
							if(("|"+merchantBean.getCoreUserConfig()).equals("|"+userBean.getUserId()+"|")){
								merchantBean.setCoreUserConfig(null);
							}else if(("|"+merchantBean.getCoreUserConfig()).contains("|"+userBean.getUserId()+"|")){
								merchantBean.setCoreUserConfig(merchantBean.getCoreUserConfig().replace(userBean.getUserId()+"|",""));	
							}							
						}
						
						int success  = merchantDB.updateNewMerchant(merchantBean);
						if (success > 0) {
							header = "success";
							LogUtil.info(request, PAGE, "addfavoriteMerchant|MerchantId:" + merchantId );
						}
						
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			
			json.put("header", header);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = {"/admin/savenotemerchant"})
	public @ResponseBody void saveNoteMerchant(
			@RequestParam(required=false, defaultValue="",  value="pk") String merchantInfo,
			@RequestParam(required=false, defaultValue="",  value="value") String noteText,
			HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, SQLException, JsonGenerationException, JsonMappingException, IOException {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		
		String merchantId = "";
		String currentServer = "";
		
		if(StringUtils.isBlank(merchantInfo)){
			return;
		}else{
			String[] info = merchantInfo.split(",");
			merchantId = (info.length >= 1 && StringUtils.isNotBlank(info[0]))? info[0] : "";
			currentServer = (info.length >= 2 && StringUtils.isNotBlank(info[1]))? info[1] : "";
		}
		
		if(StringUtils.isBlank(noteText)) {
			noteText = null;
		} else {
			noteText = Util.limitString(noteText, 200);
		}
		
		MerchantDB  merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		int result = merchantDB.editNote(merchantId, noteText, currentServer);
		HashMap<String, Object> rtn = new HashMap<String, Object>();
		if(result > 0) {
			rtn.put("success", true);
			LogUtil.info(request, PAGE, "saveNote|MerchantId:" + merchantId);
		} else {
			rtn.put("success", false);
		}
		
		JsonWriter.getInstance().write(response, rtn);
	}
	
	public int getRandomNodeTest() {
		int node = 1;
		try{
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean> countNode = merchantDB.countNodeTestByActive(3);
			if(countNode == null || countNode.size() <= 0){
				return 1;
			}
			List<Integer> listNode = new ArrayList<Integer>();
			boolean min = true;
			for(MerchantBean n : countNode){
				int cNode = n.getNodeTest();
				if(min && cNode  <= maxNodeTest && cNode > 0){
					node = cNode;
					min = false;
				}
				listNode.add(cNode);
			}
			
			for(int i=1;i<= maxNodeTest;i++){
				if(!listNode.contains(i)){
					node = i;
					break;
				}
			}
		}catch(Exception e){
			return 1;
		}
		
		return node;
	}
	
	@RequestMapping(value = {"/admin/checkmerchantbeforedelete"})
	public @ResponseBody void checkMerchantBeforeDelete(
			@RequestParam(required=false, defaultValue="0",  value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="dataserver") String dataserver,
			HttpServletRequest request, HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {
		HashMap<String, Object> rtn = new HashMap<String, Object>();
		try{
			if(merchantId==0||StringUtils.isBlank(dataserver)) {
				if(merchantId==0) {
					rtn.put("message","ลบไม่ได้เนื่องจากสินค้าร้านนี้ไม่ได้อยู่ในระบบครับ");
				}
				else if(StringUtils.isBlank(dataserver)) {
					rtn.put("message","ลบไม่ได้เนื่องจากไม่พบ dataserver ครับ");
				}
				return;
			}
			BotQualityReportDB botQualityDB = DatabaseManager.getDatabaseManager().getBotqualityreportDB();
			BotQualityReportBean qBean = botQualityDB.getDataByMerchantHaveLimit(merchantId);
			long count = 0;
			if(qBean!=null) {
				count = qBean.getAllCount();
			}
			if(count > 50000) {
				rtn.put("message","ลบไม่ได้เนื่องจากสินค้าจำนวนมากกว่า 50000 ชิ้นครับ");
				rtn.put("canDelete", false);
			}else if(count <= 0){
				rtn.put("message","ไม่พบสินค้าในระบบครับ ให้ลองใหม่พรุ่งนี้แทนนะครับ");
				rtn.put("canDelete", false);
			}else {
				rtn.put("canDelete", true);
			}
			rtn.put("success", true);
		}catch (Exception e) {
			rtn.put("success", false);
			e.printStackTrace();
		}
		JsonWriter.getInstance().write(response, rtn);
	}
	
	@RequestMapping(value = {"/admin/getDataAllMerchantnAjax"})
	public @ResponseBody void exportAllMerchant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String file = "";
		try{
			int merchantId = 1;
			String dateFile = DateTimeUtil.generateStringDate(new Date(), "yyyyMMdd-HHmmss");
			file = "AllMerchant-" + merchantId + "-" + dateFile;
			
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			BotQualityReportDB botQualityReportDB = DatabaseManager.getDatabaseManager().getBotqualityreportDB();
			List<MerchantBean> merchantListBean = merchantDB.getAllMerchantToFile();
			List<BotQualityReportBean> botQualityListBeans = botQualityReportDB.getAllData();
			Map<Integer, Integer> botQualityMap = botQualityListBeans.stream().collect(Collectors.toMap(BotQualityReportBean::getMerchantId,BotQualityReportBean::getAllCount,(x1,x2)->x2));
			
			for (MerchantBean merchanBean : merchantListBean) {
				merchanBean.setAllCount(botQualityMap.getOrDefault(merchanBean.getId() , 0));
			}
			
			if(merchantListBean != null && merchantListBean.size() > 0) {
				out.println("Merchant Id,Merchant Name,Status,Package,Note Dev,Note user,last update,user lastupdate,owner name,Allcount");
				int recNum = 0;
				for (MerchantBean merchantBean : merchantListBean) {
					out.print("\""+merchantBean.getId() +"\",");
					out.print("\""+merchantBean.getName() +"\",");
					out.print("\""+ViewUtil.generateShowActive(merchantBean.getActive()) +"\",");
					out.print("\""+merchantBean.getPackageType() +"\",");
					out.print("\""+(StringUtils.isBlank(merchantBean.getErrorMessage())?"":merchantBean.getErrorMessage()) +"\",");
					out.print("\""+(StringUtils.isBlank(merchantBean.getNote())?"":merchantBean.getNote()) +"\",");
					out.print("\""+(merchantBean.getLastUpdate() != null ? DateTimeUtil.generateStringDisplayDate(merchantBean.getLastUpdate()) : "")+"\",");
					out.print("\""+(StringUtils.isBlank(merchantBean.getUserUpdate())?"":merchantBean.getUserUpdate()) +"\",");
					out.print("\""+(StringUtils.isBlank(merchantBean.getOwnerName())?"":merchantBean.getOwnerName()) +"\",");
					out.println("\""+merchantBean.getAllCount() +"\",");
					recNum++;
				}
				LogUtil.info(request, PAGE, "exportPattern-->" + recNum +" record(s)|");
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			response.setContentType("application/csv");
	        response.addHeader("Content-Disposition", "attachment; filename = " + file); 
	        response.addHeader("X-Frame-Options", "ALLOWALL");
	        response.flushBuffer();
	        
			out.flush();  
		    out.close();
		}
	}
	 
}
