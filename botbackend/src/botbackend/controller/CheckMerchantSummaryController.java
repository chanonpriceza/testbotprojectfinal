package botbackend.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bean.ProductDataBean;
import utils.DateTimeUtil;

import botbackend.bean.BotQualityReportBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.bean.ProductUrlBean;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.WorkLoadBean;
import botbackend.db.BotQualityReportDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.ProductDataDB;
import botbackend.db.ProductUrlDB;
import botbackend.db.SendDataDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.WorkLoadDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;

@Controller
public class CheckMerchantSummaryController {
	
	final static String PAGE = "Check merchant summary";
	final static String permissionName = "merchant.merchantmanager.checksummary";
	final static int LIMIT = 100;
	final static int LIMIT_SYNC = 1000;

	
	@RequestMapping(value = {"/admin/checkmerchant"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");

		int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0); 
		return getMerchantData(merchantId, request, response);
	}
	
	private ModelAndView getMerchantData(@PathVariable("merchantId") int merchantId,HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("checkmerchant", "newPage", true);
		MerchantRuntimeReportDB merchantreportDB = DatabaseManager.getDatabaseManager().getMerchantruntimeReportDB();
		MerchantDB merchantdb = DatabaseManager.getDatabaseManager().getMerchantDB();
		
		MerchantRuntimeReportBean reportWceMerchantBean = merchantreportDB.getReportListByMerchant(merchantId,"WCE");
		MerchantRuntimeReportBean reportDueMerchantBean = merchantreportDB.getReportListByMerchant(merchantId,"DUE");
		MerchantBean merchantData = merchantdb.getMerchantBeanByIdWithOwner(merchantId);
		if(merchantData == null) {
			return model;
		}
		model.addObject("reportWceMerchantBean", reportWceMerchantBean);
		model.addObject("reportDueMerchantBean", reportDueMerchantBean);			
		model.addObject("merchantData", merchantData);			
		
		return model;
	}
	
	@RequestMapping(value = "/admin/checkmerchantconfigajax")
	public @ResponseBody void getMerchantConfigAjax(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantConfigDB configDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		List<MerchantConfigBean> configData = configDB.findMerchantConfig(merchantId);
		
		m.put("configData", configData);
		m.put("header", "success");
		
		LogUtil.info(request, PAGE, "getMerchantConfigAjax|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/checkmerchantpatternajax")
	public @ResponseBody void getMerchantPatternAjax(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		UrlPatternDB urlPatternDB = DatabaseManager.getDatabaseManager().getUrlPatternDB();
		List<UrlPatternBean> urlPatternBeanList = urlPatternDB.getUrlPatternByMerchantId(merchantId);
		
		m.put("patternData", urlPatternBeanList);
		m.put("header", "success");
		
		LogUtil.info(request, PAGE, "getMerchantPatternAjax|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/checkworkloadajax")
	public @ResponseBody void getMerchantWorkloadAjax(@RequestParam("merchantId") int merchantId,int start, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		if(mBean == null){
			m.put("header", "fail");
			JsonWriter.getInstance().write(response, m);
			return;
		}
		String getDataServer = mBean.getDataServer();
		if(StringUtils.isNotBlank(getDataServer)){
			try{
				WorkLoadDB workLoadDB = DatabaseManager.getDatabaseManager().getWorkLoadDB(getDataServer);
				if (workLoadDB != null) {
					List<WorkLoadBean> workloadData = workLoadDB.selectWorkLoadByIdHaveStartAndLimit(merchantId,start,LIMIT);		
					start = start + LIMIT;
					m.put("header", "success");
					m.put("start", start);
					m.put("listData", workloadData);
				} else {
					LogUtil.info(request, PAGE, "getMerchantWorkloadAjax --> No Server Name : "+ getDataServer );
				}
			} catch(Exception e){
				e.printStackTrace();		
			}
		}else{
			LogUtil.info(request, PAGE, "getMerchantWorkloadAjax --> No Server Name for this merchant|MerchantId:"+ merchantId );
		}			
		
		LogUtil.info(request, PAGE, "getMerchantWorkloadAjax|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/checkproducturlajax")
	public @ResponseBody void getMerchantProductUrlAjax(@RequestParam("merchantId") int merchantId,int start, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		if(mBean == null){
			m.put("header", "fail");
			JsonWriter.getInstance().write(response, m);
			return;
		}
		String getDataServer = mBean.getDataServer();
		if(StringUtils.isNotBlank(getDataServer)){
			try{
				ProductUrlDB productUrlDB = DatabaseManager.getDatabaseManager().getProductUrlDB(getDataServer);
				if (productUrlDB != null) {
					List<ProductUrlBean> productUrlData = productUrlDB.getProductUrlByMerchantIdAndStartLimit(merchantId, start,LIMIT);
					start = start + LIMIT;
					m.put("header", "success");
					m.put("start", start);
					m.put("listData", productUrlData);
				} else {
					LogUtil.info(request, PAGE, "getMerchantProductUrlAjax --> No Server Name : "+ getDataServer );
				}
			} catch(Exception e){
				e.printStackTrace();		
			}
		}else{
			LogUtil.info(request, PAGE, "getMerchantProductUrlAjax --> No Server Name for this merchant|MerchantId:"+ merchantId );
		}			
		
		LogUtil.info(request, PAGE, "getMerchantProductUrlAjax|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/checkproductdataajax")
	public @ResponseBody void getMerchantProductDataAjax(@RequestParam("merchantId") int merchantId,int start, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		if(mBean == null){
			m.put("header", "fail");
			JsonWriter.getInstance().write(response, m);
			return;
		}
		String getDataServer = mBean.getDataServer();
		if(StringUtils.isNotBlank(getDataServer)){
			try{
				ProductDataDB productDataDB = DatabaseManager.getDatabaseManager().getProductDataDB(getDataServer);
				if (productDataDB != null) {
					List<ProductDataBean> productData = productDataDB.getMerchantDataByAndStartLimit(merchantId, start,LIMIT);
					start = start + LIMIT;
					m.put("header", "success");
					m.put("start", start);
					m.put("listData", productData);
				} else {
					LogUtil.info(request, PAGE, "getMerchantProductUrlAjax --> No Server Name : "+ getDataServer );
				}
			} catch(Exception e){
				e.printStackTrace();		
			}
		}else{
			LogUtil.info(request, PAGE, "getMerchantProductUrlAjax --> No Server Name for this merchant|MerchantId:"+ merchantId );
		}			
		
		LogUtil.info(request, PAGE, "getMerchantProductUrlAjax|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/countproductonbot")
	public @ResponseBody void countProductOnBot(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> m = new HashMap<String, Object>();
		BotQualityReportDB botqualityreportDB = DatabaseManager.getDatabaseManager().getBotqualityreportDB();
		if(botqualityreportDB != null) {
			BotQualityReportBean botqualityreportBean = botqualityreportDB.getDataByMerchantHaveLimit(merchantId);
			if(botqualityreportBean != null ) {
				int count = botqualityreportBean.getAllCount();
				String reportdate = (botqualityreportBean.getReportDate() != null ? DateTimeUtil.generateStringDisplayDate(botqualityreportBean.getReportDate()) : ""); 
				m.put("header", "success");
				m.put("count", count);										
				m.put("reportdate", reportdate);										
			}
			else {
				m.put("header", "error");
			}
		}
		LogUtil.info(request, PAGE, "countProductOnBot|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/countwaitingtdelete")
	public @ResponseBody void countProductWaitingDelete(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		try{
			if(mBean != null){
				String getDataServer = mBean.getDataServer();
				if(StringUtils.isNotBlank(getDataServer)){
					try{
						ProductDataDB productDataDB = DatabaseManager.getDatabaseManager().getProductDataDB(getDataServer);
						if (productDataDB != null) {
							int count = productDataDB.countProductByStatusHaveLimit(merchantId,"E",LIMIT_SYNC);
							if(count > 0) {
								m.put("header", "success");
								m.put("count", count);
							}else {								
								m.put("header", "error");
							}
						} else {
							LogUtil.info(request, PAGE, "countProductWaiting --> No Server Name : "+ getDataServer );
						}
					} catch(Exception e){
						e.printStackTrace();		
					}
				}else{
					LogUtil.info(request, PAGE, "countProductWaiting --> No Server Name for this merchant|MerchantId:"+ merchantId );
				}		
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/countwaitingtsync")
	public @ResponseBody void countProductSync(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		
		try{
			if(mBean != null){
				String getDataServer = mBean.getDataServer();
				if(StringUtils.isNotBlank(getDataServer)){
					try{
						SendDataDB senddataDB = DatabaseManager.getDatabaseManager().getSendDataDB(getDataServer); 
						if (senddataDB!=null) {
							int count =  senddataDB.checkCountWaitingSyncHaveLimit(merchantId,"W");
							if(count > 0) {
								m.put("header", "success");
								m.put("count", count);
							}else {
								m.put("header", "error");
							}
							
						} else {
							LogUtil.info(request, PAGE, "countProductWaiting --> No Server Name : "+ getDataServer );
						}
					} catch(Exception e){
						e.printStackTrace();		
					}
				}else{
					LogUtil.info(request, PAGE, "countProductWaiting --> No Server Name for this merchant|MerchantId:"+ merchantId );
				}		
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/countproducturl")
	public @ResponseBody void getCountProductUrl(@RequestParam("merchantId") int merchantId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);
		try{
			if(mBean != null){
				String dataServer = mBean.getDataServer();
				String STATE_SUCCESS = "S";
				String STATE_DUPLICATE = "D";
				String STATE_FAIL = "F";
				String STATE_NEW = "N";
				String STATE_LANGUAGEERROR = "L";
				String STATE_EXPIRE = "E";
				if(StringUtils.isNotBlank(dataServer)){
					try{
						ProductUrlDB productUrlDB = DatabaseManager.getDatabaseManager().getProductUrlDB(dataServer);
						if (productUrlDB != null) {
							int countAll = productUrlDB.countAllProductUrl(merchantId);
							if(countAll <= 100000) {
								int countStateNew = productUrlDB.countByMerchantAndState(merchantId, STATE_NEW);
								int countStateLanguageError = productUrlDB.countByMerchantAndState(merchantId, STATE_LANGUAGEERROR);
								int countStateSuccess = productUrlDB.countByMerchantAndState(merchantId, STATE_SUCCESS);
								int countStateDuplicate = productUrlDB.countByMerchantAndState(merchantId, STATE_DUPLICATE);
								int countStateFailure = productUrlDB.countByMerchantAndState(merchantId, STATE_FAIL);
								int countStateExpire = productUrlDB.countByMerchantAndState(merchantId, STATE_EXPIRE);
								m.put("countStateNew", countStateNew);	
								m.put("countStateLanguageError", countStateLanguageError);	
								m.put("countStateSuccess", countStateSuccess);	
								m.put("countStateDuplicate", countStateDuplicate);	
								m.put("countStateFailure", countStateFailure);	
								m.put("countStateExpire", countStateExpire);	
								m.put("header", "success");
								
							}else {
								m.put("header", "error limit");
							}
						} else {
							m.put("header", "error");
							LogUtil.info(request, PAGE, "getCountProductUrl --> No Server Name : "+ dataServer );
						}
					} catch(Exception e){
						e.printStackTrace();		
					}
				}else{
					LogUtil.info(request, PAGE, "getCountProductUrl --> No Server Name for this merchant|MerchantId:"+ merchantId );
				}		
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		
		LogUtil.info(request, PAGE, "getCountProductUrl|merchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
}
