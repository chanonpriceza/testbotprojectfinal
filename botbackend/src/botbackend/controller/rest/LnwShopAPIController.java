package botbackend.controller.rest;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.TagUtils;

import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.bean.api.LnwShopProductDataBean.STATUS;
import botbackend.bean.api.LnwshopProductDataRequestBean.ACTION;
import botbackend.db.api.LnwShopProductDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.Util;

@RestController
public class LnwShopAPIController {
	static Logger trackLogger = Logger.getLogger("trackLogger");
	static Logger catLogger = Logger.getLogger("catLogger");
	private static LnwShopProductDataDB productDataDB = DatabaseManager.getDatabaseManager().getLnwshopProductDataDB();
	private static final String PRIVATE_KEY = "h8Nh_KLcZb6AQRbfYF459rEc8fQxYBr6";
	private String action = "";
	private String reqHash = "";
	
	@RequestMapping(value ="/lnwshop", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> lnwshop(HttpServletRequest request) {
		
		Map<String, String> requestMap = new HashMap<>();
		Map<String, String> rtn = new HashMap<>();
		
		try {
			Map<String, String[]> rawMap = request.getParameterMap();
			for(Entry<String, String[]> ent : rawMap.entrySet()) {
				if(ent.getValue().length > 0) {
					requestMap.put(ent.getKey(), ent.getValue()[0]);
					if(ent.getKey().equals("hash")){
						reqHash = ent.getValue()[0];
					}
				}
			}

			if(!validateData(requestMap)) {
				rtn.put("success", "false");
				rtn.put("hash", getHash(rtn, PRIVATE_KEY));
				trackLogger.info(requestMap.toString() + rtn.toString() + " ["+HttpStatus.UNSUPPORTED_MEDIA_TYPE+"]");
				return new ResponseEntity<Map<String, String>>(rtn, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
			}
			
			int referenceId = 0;
			referenceId = processData(requestMap);

			if(referenceId == 0 ) {
				rtn.put("success", "false");
				rtn.put("hash", getHash(rtn, PRIVATE_KEY));
				trackLogger.info(requestMap.toString() + rtn.toString() + " ["+HttpStatus.INTERNAL_SERVER_ERROR+"]");
				return new ResponseEntity<Map<String, String>>(rtn, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			rtn.put("success", "true");
			rtn.put("domain", requestMap.get("domain"));
			rtn.put("product_id", requestMap.get("product_id"));
			rtn.put("reference_id", String.valueOf(referenceId));
			rtn.put("hash", getHash(rtn, PRIVATE_KEY));
		} catch(Exception e) {
			e.printStackTrace();
			rtn.put("success", "false");
			rtn.put("hash", getHash(rtn, PRIVATE_KEY));
			trackLogger.info(requestMap.toString() + rtn.toString() + " ["+HttpStatus.INTERNAL_SERVER_ERROR+"]");
			trackLogger.error("Error on method: " + action);
			trackLogger.error(e);
			return new ResponseEntity<Map<String, String>>(rtn, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		trackLogger.info(requestMap.toString() + rtn.toString() + " ["+HttpStatus.OK+"]");
		return new ResponseEntity<Map<String, String>>(rtn, HttpStatus.OK);
	}
	
	private boolean validateData(Map<String, String> requestMap) throws Exception {
		// check important value
		if(!requestMap.containsKey("product_id") 
				|| !requestMap.containsKey("domain") 
				|| !requestMap.containsKey("product_name")
//				|| !requestMap.containsKey("product_price")
				|| !requestMap.containsKey("product_url")
				|| !requestMap.containsKey("action")
				|| !requestMap.containsKey("hash")) {
			return false;
		}
		
		if(Integer.valueOf(requestMap.get("product_id")) < 0) {
			return false;
		}
		if(StringUtils.isBlank(requestMap.get("domain"))) {
			return false;
		}
		if(StringUtils.isBlank(requestMap.get("product_name"))) {
			return false;
		}
//		if(Double.valueOf(requestMap.get("product_price")) < 0.00) {
//			return false;
//		}
		if(StringUtils.isBlank(requestMap.get("product_url"))) {
			return false;
		}
		if(StringUtils.isBlank(requestMap.get("hash"))) {
			return false;
		}
		if(StringUtils.isBlank(requestMap.get("action"))) {
			return false;
		} else {
			String action = (requestMap.get("action"));
			if(!action.equalsIgnoreCase(ACTION.ADD.toString()) 
					&& !action.equalsIgnoreCase(ACTION.UPDATE.toString()) 
					&& !action.equalsIgnoreCase(ACTION.HIDE.toString())) {
				return false;
			}
			if(action.equalsIgnoreCase(ACTION.UPDATE.toString())
					|| action.equalsIgnoreCase(ACTION.HIDE.toString())) {
				if(StringUtils.isBlank(requestMap.get("reference_id"))) {
					return false;
				}
			}
		}
		
		// check hash value
		String hash = requestMap.get("hash");
		requestMap.remove("hash");
		String myHash = getHash(requestMap, PRIVATE_KEY);
//		System.out.println("Product name: " + requestMap.get("product_name"));
//		System.out.println("Input Hash: " + hash);
//		System.out.println("output Hash: " + myHash);
		if(!hash.equals(myHash)) {
//			System.out.println(requestMap);
			return false;
		}
		requestMap.put("hash", reqHash);
		if(!requestMap.containsKey("product_price") || Double.valueOf(requestMap.get("product_price")) < 0.00) {
			requestMap.put("product_price", "0.00");
		}
		
		return true;
	}
	
	private String getHash(Map<String, String> valueMap, String secret) {
		String rtn = "get_hash_error";
		try {
			TreeMap<String, String> hashMap = new TreeMap<>(valueMap);
			String message = "";
			for(Entry<String, String> map : hashMap.entrySet()) {
				message += map.getValue();
			}
			
			byte[] r = message.getBytes("UTF-8");
			byte[] k = secret.getBytes("UTF-8");
			
			HMac hmac = new HMac(new RIPEMD160Digest());
			hmac.init(new KeyParameter(k));
			hmac.update(r, 0, r.length);
			
			byte[] out = new byte[hmac.getMacSize()];
			hmac.doFinal(out, 0);
			
			rtn =  new String(Hex.encode(out), "US-ASCII");
		} catch(Exception e) {
			trackLogger.error(e);
		}
		
		return rtn;
	}
	
	private int processData(Map<String, String> requestMap) throws SQLException {
		int referenceId = 0;
		LnwShopProductDataBean productData;
		String productName = requestMap.get("product_name");
		productName = Util.removeNonChar(productName);
		productName = Util.removeLongSpace(productName);
		productData = new LnwShopProductDataBean(
			Integer.valueOf(requestMap.get("product_id")),
			requestMap.get("domain"),
			productName,
			Double.valueOf(requestMap.get("product_price")),
			requestMap.get("product_url")
		);

		if(requestMap.containsKey("product_picture_url") && StringUtils.isNotBlank(requestMap.get("product_picture_url"))) {
			String allPicUrl = requestMap.get("product_picture_url");
			List<String> picUrlList = Arrays.asList(allPicUrl.split(","));
			productData.setPicture_url(picUrlList.get(0));
		}
		
		if(requestMap.containsKey("reference_id") && StringUtils.isNotBlank(requestMap.get("reference_id"))) {
			productData.setId(Integer.valueOf(requestMap.get("reference_id")));
		}
		
		if(requestMap.containsKey("product_detail") && StringUtils.isNotBlank(requestMap.get("product_detail"))) {
			String productDesc = requestMap.get("product_detail");
			productDesc = productDesc.replaceAll("&#x.{0,5};", "");
			productDesc = StringEscapeUtils.unescapeHtml4(productDesc);
			productDesc = Util.toPlainTextString(productDesc);
			productDesc = Util.removeNonChar(productDesc);
			productDesc = Util.removeLongSpace(productDesc);
			productData.setDescription(productDesc);
		}
		
		String action = requestMap.get("action");
		
		String tag = requestMap.get("tags");
		String category_name = requestMap.get("category_name");
		String parent_product_name = requestMap.get("parent_product_name");
		String min_price = requestMap.get("min_price");
		String max_price = requestMap.get("max_price");
		
		if(StringUtils.isNotBlank(min_price)) min_price = Util.removeCharNotPrice(min_price);
		if(StringUtils.isNotBlank(max_price)) max_price = Util.removeCharNotPrice(max_price);
		
		productData.setTag(Util.limitString(tag, 890));
		productData.setCategory(category_name);
		productData.setParent_product_name(parent_product_name);
		productData.setPriceMin(Double.valueOf(StringUtils.defaultIfBlank(min_price, "0")));
		productData.setPriceMax(Double.valueOf(StringUtils.defaultIfBlank(max_price, "0")));
		LnwShopProductDataBean getData = productDataDB.getProductData(productData);
		if(getData == null) {
			referenceId = productDataDB.insertProductData(productData, STATUS.ADD);
		} else {
			String oldName = getData.getOld_name();
			String curName = getData.getName();
			String newName = productData.getName();
			String status = getData.getStatus();
			if(oldName == null) {
				if(!newName.equals(curName)) {
					productData.setOld_name(curName);					
				}
			} else {
				productData.setOld_name(oldName);
			}
			if(action.equalsIgnoreCase(ACTION.ADD.toString())) {
				if(status.equalsIgnoreCase(STATUS.ADD.toString())) {
					productData.setOld_name(null);
					referenceId = productDataDB.updateProductData(productData, STATUS.ADD);
				} else if( status.equalsIgnoreCase(STATUS.UPDATE.toString())
						|| status.equalsIgnoreCase(STATUS.DELETE.toString())
						|| status.equalsIgnoreCase(STATUS.DONE.toString())) {
					referenceId = productDataDB.updateProductData(productData, STATUS.UPDATE);
				}
			} else if(action.equalsIgnoreCase(ACTION.UPDATE.toString())) {
				if(status.equalsIgnoreCase(STATUS.ADD.toString())) {
					productData.setOld_name(null);
					referenceId = productDataDB.updateProductData(productData, STATUS.ADD);
				} else if(status.equalsIgnoreCase(STATUS.UPDATE.toString())
						||status.equalsIgnoreCase(STATUS.DELETE.toString())
						||status.equalsIgnoreCase(STATUS.DONE.toString())) {
					referenceId = productDataDB.updateProductData(productData, STATUS.UPDATE);
				}
			} else if(action.equalsIgnoreCase(ACTION.HIDE.toString())) {
				if(status.equalsIgnoreCase(STATUS.ADD.toString())) {
					productDataDB.delete(productData);
					return productData.getId();
				} else if(status.equalsIgnoreCase(STATUS.UPDATE.toString())
						||status.equalsIgnoreCase(STATUS.DELETE.toString())
						||status.equalsIgnoreCase(STATUS.DONE.toString())) {
					referenceId = productDataDB.updateProductData(productData, STATUS.DELETE);
				}
			}
		}
		if(referenceId != 0) {
			referenceId = productDataDB.getProductData(productData).getId();				
		}
		
		catLogger.info(productData.getLnw_merchant_id() + " : " + productData.getCategory());
		
		return referenceId;
	}

}
