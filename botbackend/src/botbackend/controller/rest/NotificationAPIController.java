package botbackend.controller.rest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.NotificationApiBean;
import botbackend.bean.NotificationApiBean.COUNTRYLIST;
import botbackend.bean.NotificationApiBean.DESTYPE;
import botbackend.bean.NotificationApiBean.NOTITYPE;
import botbackend.bean.UserBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.NotificationApiDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import utils.BotUtil;

@RestController
public class NotificationAPIController {
	
	private static final String PAGE = "Notification page";
	private static final String permissionName = "service.pspn.crawl";
	private static JSONParser parser = new JSONParser();
	private static final int LIMIT = 8;
	private static final int numRecord = 8;
	
	@RequestMapping(value = "/admin/service/putnotification", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> putNotification(HttpServletRequest request){
		
		Map<String, Object> requestMap = new HashMap<>();
		Map<String, Object> rtn = new HashMap<>();
		
		
		if(!UserUtil.checkLogin(request)){
			rtn.put("success", "false");
			rtn.put("message", "Unauthorized");
			LogUtil.info(request, PAGE, "Unauthorized - require login token");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.UNAUTHORIZED); //401
		}
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName})) {
			rtn.put("success", "false");
			rtn.put("message", "Access denied – insufficient permission");
			LogUtil.info(request, PAGE, "Access denied - permission not match");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.FORBIDDEN); //403
		}
		
		Map<String, String[]> rawMap = request.getParameterMap();
		for(Entry<String, String[]> ent : rawMap.entrySet()) {
			if(ent.getValue().length > 0) {
				requestMap.put(ent.getKey(), ent.getValue()[0]);
			}
		}
		
		String beanData = (String) requestMap.get("notiData");
		
		if(StringUtils.isBlank(beanData)) {
			rtn.put("success", "true");
			rtn.put("message", "Not found url");
			LogUtil.info(request, PAGE, "Not found beanData");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
		try {
			
			JSONObject obj = (JSONObject) parser.parse(beanData);
			NotificationApiBean notiBean = new NotificationApiBean(obj);
			NotificationApiDB mcDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
			if(notiBean.getDestinationType().equals(DESTYPE.GROUP.toString())) {
				UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
				List<UserBean> us = userDB.getUserListRole(notiBean.getDestination());
				if(us!=null&&us.size()>0) {
					for(UserBean u:us) {
						notiBean.setDestination(u.getUserName());
						notiBean.setDestinationType(DESTYPE.INDIVIDUAL.toString());
						mcDB.insertNotification(notiBean);
					}
				}
			}
			else{
				if(notiBean!=null) {
					mcDB.insertNotification(notiBean);
				}
			}
			rtn.put("success", "true");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
			
		}catch (Exception e) {
			rtn.put("success", "true");
			rtn.put("message", "Error");
			LogUtil.info(request, PAGE, "Not found beanData");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
	}
	
	@RequestMapping(value = "/admin/service/markRead", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> markRead(HttpServletRequest request){
		
		Map<String, Object> requestMap = new HashMap<>();
		Map<String, Object> rtn = new HashMap<>();
		
		if(!UserUtil.checkLogin(request)){
			rtn.put("success", "false");
			rtn.put("message", "Unauthorized");
			LogUtil.info(request, PAGE, "Unauthorized - require login token");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.UNAUTHORIZED); //401
		}
		
		Map<String, String[]> rawMap = request.getParameterMap();
		for(Entry<String, String[]> ent : rawMap.entrySet()) {
			if(ent.getValue().length > 0) {
				requestMap.put(ent.getKey(), ent.getValue()[0]);
			}
		}
		
		String idListString = (String) requestMap.get("idList");	
		
		if(StringUtils.isBlank(idListString)) {
			rtn.put("success", "false");
			rtn.put("message", "NNot found beanData");
			LogUtil.info(request, PAGE, "Not found beanData");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
		String[] idList = idListString.split(",");
		if((idList==null||idList.length==0)) {
			rtn.put("success", "false");
			rtn.put("message", "parse String error");
			LogUtil.info(request, PAGE, "parse String error");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
		NotificationApiDB mcDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
		try {
			int count = mcDB.markNotiList(Arrays.asList(idList),true);
			if(count > 0) {
				rtn.put("success", "true");
				rtn.put("message", "ok ");
				LogUtil.info(request, PAGE, "parse String error");
				return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
			}
		} catch (Exception e) {
			rtn.put("success", "false");
			rtn.put("message", "Error "+e);
			LogUtil.info(request, PAGE, "Error "+e);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		rtn.put("success", "false");
		rtn.put("message", "count is zero ");
		LogUtil.info(request, PAGE,"count is zero ");
		return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
	}
	
	@RequestMapping(value = "/admin/service/readNotiAll", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> readNotiAll(HttpServletRequest request){
		
		Map<String, Object> rtn = new HashMap<>();
		
		if(!UserUtil.checkLogin(request)){
			rtn.put("success", "false");
			rtn.put("message", "Unauthorized");
			LogUtil.info(request, PAGE, "Unauthorized - require login token");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.UNAUTHORIZED); //401
		}
		
		return null;
	}
	
	@RequestMapping(value = {"/admin/readAllNotification"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {

		return getNotiAll(request, response);
	}
	
	public ModelAndView  getNotiAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView("notificationpage", "newPage", true);
		NotificationApiDB notiDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
		UserBean u = (UserBean)request.getSession().getAttribute("userData");
		Map<String, Object> requestMap = new HashMap<>();
		int page = ServletUtil.getIntParameter(request, "page", 0);
		Map<String, String[]> rawMap = request.getParameterMap();
		for(Entry<String, String[]> ent : rawMap.entrySet()) {
			if(ent.getValue().length > 0) {
				requestMap.put(ent.getKey(), ent.getValue()[0]);
			}
		}
		List<NotificationApiBean> beans = notiDB.selectDetail(u.getUserName(),"TH",0,0, LIMIT);
		int totalNoti =  DatabaseManager.getDatabaseManager().getNotiApiDB().countDetail(u.getUserName(),"TH",0);
		model.addObject("notificationList", beans);
		model.addObject("notiType",0);
		model.addObject("country", COUNTRYLIST.TH.toString());
		model.addObject("pagingBean", createPaginationBean(page,totalNoti));
		return model;
	}
	
	private PagingBean createPaginationBean(int page,int total) throws Exception {
		
		if(page < 1){ page = 1; }
			if(total > 0){
				int start = (page - 1) * numRecord;
				if(start >= total) {
					start = 0;
				}
				
				int totalPage = (int)(Math.ceil((double)total/(double)numRecord));
				
				if(page > totalPage) {
					page = 1;
				}
				PagingBean pagingBean = PagingBean.createPagingBean(page, total, numRecord);
				return pagingBean;
				
		}
		return null;
	}
	
	@RequestMapping(value = "/admin/service/getNotificationAjax", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getNotificationAjax(HttpServletRequest request) throws Exception{
		
		
		Map<String, Object> requestMap = new HashMap<>();
		Map<String, Object> rtn = new HashMap<>();
		
		
		if(!UserUtil.checkLogin(request)){
			rtn.put("success", "false");
			rtn.put("message", "Unauthorized");
			LogUtil.info(request, PAGE, "Unauthorized - require login token");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.UNAUTHORIZED); //401
		}
		
		
		Map<String, String[]> rawMap = request.getParameterMap();
		for(Entry<String, String[]> ent : rawMap.entrySet()) {
			if(ent.getValue().length > 0) {
				requestMap.put(ent.getKey(), ent.getValue()[0]);
			}
		}
		
		
		String country = (String) requestMap.get("country");
		Integer type =   BotUtil.stringToInt(String.valueOf(requestMap.get("notiType")),0);
		Integer page =   BotUtil.stringToInt(String.valueOf(requestMap.get("page")),0);
		
		if(StringUtils.isBlank(country)) 
			country = "TH";
		
		if(page==null||page==0)
			page = 1;
		
		int start = (page - 1) * numRecord;
		UserBean u = (UserBean)request.getSession().getAttribute("userData");
		NotificationApiDB notiDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
		List<NotificationApiBean> notiBeans = notiDB.selectDetail(u.getUserName(), country, type,start,LIMIT);
		if (notiBeans != null && notiBeans.size() > 0) {
			rtn.put("notiBeans",notiBeans);
		}
		Map<Integer,String> notiType = NOTITYPE.getNotiTypeMap();
		rtn.put("notiType",notiType);
		int totalNoti =  DatabaseManager.getDatabaseManager().getNotiApiDB().countDetail(u.getUserName(),country, type);
		rtn.put("pagingBean",createPaginationBean(page, totalNoti));
		rtn.put("success", "true");
		return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
	}
}
