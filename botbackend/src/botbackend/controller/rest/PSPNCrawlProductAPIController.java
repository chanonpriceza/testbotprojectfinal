package botbackend.controller.rest;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bean.ProductDataBean;
import bean.SendDataBean;
import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.MerchantConfigBean.FIELD;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.ProductDataDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.ParserUtil;
import botbackend.utils.UserUtil;
import product.processor.ParserUpdate;
import utils.BotUtil;
import web.parser.HTMLParser;

@RestController
public class PSPNCrawlProductAPIController {

	private static final int DEFAULT_IMAGE_WIDTH = 200;
	private static final int DEFAULT_IMAGE_HEIGHT = 200;
	private static final String permissionName = "service.pspn.crawl";
	private static final String PAGE = "Service PSPN-Crawl";
	
	@RequestMapping(value = "/partner/service_pspn", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> pspnService(HttpServletRequest request){
		
		Map<String, Object> requestMap = new HashMap<>();
		Map<String, Object> rtn = new HashMap<>();
		String url = "";
		
		if(!UserUtil.checkLogin(request)){
			rtn.put("success", "false");
			rtn.put("message", "Unauthorized");
			LogUtil.info(request, PAGE, "Unauthorized - require login token");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.UNAUTHORIZED); //401
		}
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName})) {
			rtn.put("success", "false");
			rtn.put("message", "Access denied – insufficient permission");
			LogUtil.info(request, PAGE, "Access denied - permission not match");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.FORBIDDEN); //403
		}
		
		try {
			Map<String, String[]> rawMap = request.getParameterMap();
			for(Entry<String, String[]> ent : rawMap.entrySet()) {
				if(ent.getValue().length > 0) {
					requestMap.put(ent.getKey(), ent.getValue()[0]);
				}
			}
			
			url = (String) requestMap.get("url");
			if(StringUtils.isBlank(url)) {
				rtn.put("success", "true");
				rtn.put("message", "Not found url");
				LogUtil.info(request, PAGE, "Not found url : " + url );
				return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
			}
			
			int targetMerchantId = 0;
			int realMerchantId = 0;
			if(url.contains("lazada")) 			{ targetMerchantId = 440001; realMerchantId = 314; }
			else if(url.contains("shopee")) 	{ targetMerchantId = 440002; realMerchantId = 7077; }
			else if(url.contains("jd.co.th"))	{ targetMerchantId = 440003; realMerchantId = 300127; }
			else if(url.contains("central"))	{ targetMerchantId = 440004; realMerchantId = 3171; }
			
			if(targetMerchantId != 0) {
				ProductDataBean pdBean = crawl(request, targetMerchantId, url);
				if(pdBean != null) {
					
					rtn.put("merchantId"      , realMerchantId);
					rtn.put("name"            , pdBean.getName());
					rtn.put("price"           , pdBean.getPrice());
					rtn.put("basePrice"       , pdBean.getBasePrice());
					rtn.put("description"     , pdBean.getDescription());
					rtn.put("pictureUrl"      , pdBean.getPictureUrl());
					rtn.put("url"             , pdBean.getUrl());
					rtn.put("isExpire"        , pdBean.getExpire());
					rtn.put("realProductId"   , pdBean.getRealProductId());
					rtn.put("upc"             , pdBean.getUpc());
					rtn.put("dynamicField"    , pdBean.getDynamicField());
					
					rtn.put("success", "true");
					rtn.put("message", "Service work successfully");
					LogUtil.info(request, PAGE, "Service work successfully. for url : " + url );
					return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
				} else {
					rtn.put("success", "false");
					rtn.put("message", "Cannot crawl product from this url.");
					LogUtil.info(request, PAGE, "Cannot crawl product from url : " + url );
					return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
				}
			} else {
				rtn.put("success", "false");
				rtn.put("message", "No content for this url, maybe this domain not match any site we support.");
				LogUtil.info(request, PAGE, "Domain not support. for url : " + url );
				return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
			}
			
		} catch(Exception e) {
			rtn.put("success", "false");
			rtn.put("message", "Nothing happen, please contact our stuff.");
			LogUtil.info(request, PAGE, "Error on PSPNCrawlProductAPIController : " + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.INTERNAL_SERVER_ERROR); //500
		}
		
//		rtn.put("success", "true");
//		rtn.put("message", "Nothing happen, please contact our stuff.");
//		LogUtil.info(request, PAGE, "Nothing catch in flow. for url : " + url );
//		return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
		
	}
	
	private ProductDataBean crawl(HttpServletRequest request, int targetMerchantId, String url) throws SQLException{
		
//		String parserClass = "";
//		if(targetMerchantId == 440001) parserClass = "web.parser.filter.TemplateLazadaJSIncludeIdHTMLParser";
//		if(targetMerchantId == 440002) parserClass = "web.parser.filter.TemplateShopeeConversionHTMLParser";
//		if(targetMerchantId == 440003) parserClass = "web.parser.filter.TemplateJDCentralHTMLParser"; 
//		if(targetMerchantId == 440004) parserClass = "web.parser.filter.TemplateCentralHTMLParser"; 
		
		MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		MerchantConfigBean mcBean = mcDB.findMerchantConfigByIdAndField(targetMerchantId, FIELD.parserClass.toString());
		
		try {
			HTMLParser parser = (HTMLParser)Class.forName(mcBean.getValue()).getDeclaredConstructor().newInstance();
			url = BotUtil.encodeURL(url);
			
			ProductDataBean[] productDataBean = parser.parse(url);
			if(productDataBean != null && productDataBean.length > 0) {
				
				if(StringUtils.isBlank(productDataBean[0].getUrl()))
					productDataBean[0].setUrl(url);
				
				if((productDataBean[0].getUpdateDate())==null)
					productDataBean[0].setUpdateDate(new Timestamp(System.currentTimeMillis()));
				
				if(ParserUpdate.baseValidate(productDataBean[0])) {

					boolean insertSuccess = botInsert(productDataBean[0], targetMerchantId);
					LogUtil.info(request, PAGE, "Insert bot database, result=" + insertSuccess);
					
					return productDataBean[0];
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private boolean botInsert(ProductDataBean pdBean, int targetMerchantId) throws SQLException {
		
		ConfigDB cDB = DatabaseManager.getDatabaseManager().getConfigDB();
		ConfigBean cBean = cDB.getByName("service.pspn.crawl.dataserver");
		String dataServer = cBean.getConfigValue();
		
		ProductDataDB pdDB = DatabaseManager.getDatabaseManager().getProductDataDB(dataServer);
		SendDataDB sdDB = DatabaseManager.getDatabaseManager().getSendDataDB(dataServer);
		
		pdBean.setMerchantId(targetMerchantId);
		
		byte[] imageByte = ParserUtil.parseImage(null, pdBean.getPictureUrl(), DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
		SendDataBean sdBean = SendDataBean.createSendDataBean(pdBean, imageByte, SendDataBean.ACTION.ADD);
		
		int insertProductCount  = pdDB.insertProductData(pdBean);
		if(insertProductCount > 0){
			sdDB.insertSendData(sdBean);
		}
		
		return false;
	}
	
}
