package botbackend.controller.rest;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bean.MerchantMonitorBean;
import botbackend.bean.NotificationApiBean;
import botbackend.bean.NotificationApiBean.COUNTRYLIST;
import botbackend.bean.NotificationApiBean.DESTYPE;
import botbackend.bean.NotificationApiBean.NOTITYPE;
import botbackend.component.NotificationAPIComponent;
import botbackend.db.MerchantMonitorDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;

@RestController
public class MonitoringControlller {
	
	private static final String PAGE = "Notification page";
	private static JSONParser parser = new JSONParser();
	
	@RequestMapping(value = "/botruntimemonitoring", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertMonitorData(HttpServletRequest request){
		Map<String, Object> requestMap = new HashMap<>();
		Map<String, Object> rtn = new HashMap<>();
		
		Map<String, String[]> rawMap = request.getParameterMap();
		for(Entry<String, String[]> ent : rawMap.entrySet()) {
			if(ent.getValue().length > 0) {
				requestMap.put(ent.getKey(), ent.getValue()[0]);
			}
		}
		
		String beanData = (String) requestMap.get("monitorBean");
		
		if(StringUtils.isBlank(beanData)) {
			rtn.put("success", "false");
			rtn.put("message", "Not found beanData");
			LogUtil.info(request, PAGE, "Not found beanData");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
		MerchantMonitorBean bean = null;
		boolean isSuccess = false;
		try {
			JSONObject obj = (JSONObject)parser.parse(beanData);
			bean = new MerchantMonitorBean(obj);
			if(isStable(bean)) {
				isSuccess = runStableProcess(bean);
			}
			else {
				isSuccess = runUnstableProcess(bean);
			}
		}catch (Exception e) {
			rtn.put("success", "false");
			rtn.put("message", "Error "+e);
			LogUtil.info(request, PAGE, "Error "+e);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		rtn.put("success",String.valueOf(isSuccess));
		return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK);
		
	}
	
	private boolean isStable(MerchantMonitorBean bean) {
		if(bean.getAnalyzeResult().equals("Stable")) {
			return true;
		}
		return false;
	}
	
//	private void sendNotification() {
//		
//	}
	
	private boolean runStableProcess(MerchantMonitorBean bean) throws Exception {
		try {
			MerchantMonitorDB merDB = DatabaseManager.getDatabaseManager().getMerchantMonitorDB();
			List<MerchantMonitorBean> selectDeleteList = merDB.selectOldDataNonTopic(bean);
			if(selectDeleteList!=null&&selectDeleteList.size()!=0) {
				merDB.insertMonitorHistoryList(selectDeleteList);
				merDB.deleteOldData(bean);
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean runUnstableProcess(MerchantMonitorBean bean) throws Exception {
		try {
			MerchantMonitorDB merDB = DatabaseManager.getDatabaseManager().getMerchantMonitorDB();
			List<MerchantMonitorBean> selectDeleteList = merDB.selectOldData(bean);
			if(bean.getTopic().equals("isCheckApprove")) {
				NotificationAPIComponent.putNotificationDirect(new NotificationApiBean(NotificationApiBean.generateMessageCheckApprove(bean.getMerchantId()),"SYSTEM","dev",DESTYPE.GROUP,NOTITYPE.MERCHANT_APPROVE,NotificationApiBean.generateLinkCheckApprove(bean.getMerchantId()),COUNTRYLIST.TH, bean.getMerchantId()));
				return true;
			}
			else if(selectDeleteList!=null&&selectDeleteList.size()>0) {
				MerchantMonitorBean b = selectDeleteList.get(0);
				int count = b.getCount();
				count++;
				if(count <= 3 && !b.getTopic().equals("isCheckApprove")) {
					b.setCount(count);
					merDB.updateMonitor(b);
				}else {
					//NotificationAPIComponent.putNotificationDirect(new NotificationApiBean(NotificationApiBean.generateMessageCheckApprove(b.getMerchantId()),"SYSTEM","dev",DESTYPE.GROUP,NOTITYPE.MERCHANT_APPROVE,NotificationApiBean.generateLinkCheckApprove(b.getMerchantId()),COUNTRYLIST.TH, b.getMerchantId()));
				}
			}else {
				if(bean.getFirstFoundDate()==null) {
					if(bean.getRecentFoundDate()!=null)
						bean.setFirstFoundDate(bean.getRecentFoundDate());
					else if(bean.getAnalyzeDate()!=null)
						bean.setFirstFoundDate(bean.getAnalyzeDate());
					else
						bean.setFirstFoundDate(new Timestamp(System.currentTimeMillis()));

				}
				merDB.insertMonitor(bean);
			}
			return true;
		}
		catch (Exception e) {
			return false;
		}

	}
	
}
