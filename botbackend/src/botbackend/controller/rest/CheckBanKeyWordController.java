package botbackend.controller.rest;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONObject;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.db.BanKeywordDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.manager.BanKeywordManager;
import botbackend.manager.BanKeywordRule;
import botbackend.utils.JsonWriter;
import botbackend.utils.ParserUtil;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;
@RestController
public class CheckBanKeyWordController {
	
	final static String permissionName = "botBanKeyword";
	
	@RequestMapping(value = "/botBanKeyword")
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
			
		int merchantId 	= ServletUtil.getIntParameter(request, "merchantId", 0);
		String url		= ServletUtil.getStringParam(request, "url", "");
		String word 	= ServletUtil.getStringParam(request, "word", "");
		int cat 		= ServletUtil.getIntParameter(request, "cat",-1);
		int price 		= ServletUtil.getIntParameter(request, "price", 0);
		boolean isUrl 	= StringUtils.isNotBlank(url);
		
		String input = "";
		
		if (isUrl) {
			input = url;
		} else {
			input = word;
		}
		getBanKeywordAPI(merchantId, input,cat,price,request,response, isUrl);
		
		return null;
	}
	
	private void getBanKeywordAPI(@PathVariable("merchantId") int merchantId,@PathVariable("url") String input,@PathVariable("cat") int cat,@PathVariable("price") int minPrice,HttpServletRequest request, HttpServletResponse response,boolean isURL) throws Exception {
		
		BanKeywordDB banDB = DatabaseManager.getDatabaseManager().getBanKeywordDB();
		MerchantConfigDB configDB  = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
		BanKeywordManager banManager = BanKeywordManager.getBanKeywordManager(banDB);
		JSONObject jsonObj = new JSONObject();
		
		String name = "";
		double price = minPrice ;
		
		if(isURL) {

			System.setProperty("sun.net.client.defaultReadTimeout", "15000");
			System.setProperty("sun.net.client.defaultConnectTimeout", "15000");
			
			String url = input;
			String parserClass = "";
			MerchantConfigBean configBean = configDB.findMerchantConfigByIdAndField(merchantId,"parserClass");
			
			if(configBean==null) {
				parserClass = Util.DEFAULT_PARSER_CLASS;
			}else {
				parserClass = configBean.getValue();
				try {
					@SuppressWarnings("unused")
					Class<?> c1 = Class.forName(parserClass);
				}catch (Exception e) {
					JsonWriter.getInstance().write(response,"ParserClass Not Found");
					return;
				}
				CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));  
				List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
				List<ProductDataBean> parseList = ParserUtil.parse(parserClass,  pList,new String[]{url});
				ProductDataBean[] productDataBean = parseList.toArray(new ProductDataBean[parseList.size()]);
				
				if(productDataBean==null) {
					JsonWriter.getInstance().write(response," ProductData can't parse ");
					return;
				}
				ProductDataBean pdb = productDataBean[0];
				name = pdb.getName();
				price = pdb.getPrice();
			}
					
			}else {
				name = input;
			}
    	
			BanKeywordRule result = banManager.isBanKeywordWithKeyword(name,merchantId,cat,price);

			jsonObj.put("result",result!=null);
			jsonObj.put("cat",cat==-1?"No CateforyId":cat+"");
			jsonObj.put("inputName",name);
			jsonObj.put("inputPrice",price);
        
			if(result!=null) {
				jsonObj.put("BanKeyword",result.getKeywordList());
				jsonObj.put("MerchantIds",result.getMerchantIds() );
				jsonObj.put("NonKeywordList", result.getNonKeywordList()==null?result.getNonKeywordList():"null");
				jsonObj.put("FullKeyword",result.getFullKeyword() );
				jsonObj.put("minPrice",result.getMinPrice());
			}
        
			JsonWriter.getInstance().write(response,jsonObj.toString());
			
	}
}
