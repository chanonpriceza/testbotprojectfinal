package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bean.ProductDataBean;
import bean.SendDataBean;

import botbackend.bean.ConfigBean;
import botbackend.bean.ManualProductBean;
import botbackend.bean.MerchantBean;
import botbackend.db.ConfigDB;
import botbackend.db.ManualProductDB;
import botbackend.db.MerchantDB;
import botbackend.db.ProductDataDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.AjaxUtil;
import botbackend.utils.UserUtil;

@Controller
public class ManualProductManagerController {
	
	final static String PAGE = "Manual Product Manager";
	final static String permissionName = "product.manualproductmanager";
	
	@RequestMapping(value = "/admin/manualproductmanager")
	public ModelAndView getModelAndView(HttpServletRequest request) throws IOException, ServletException, SQLException {		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("manualproductmanager", "newPage", true);
		try{
			ManualProductDB mpDB = DatabaseManager.getDatabaseManager().getManualProductDB();
			int countAll = mpDB.countAll();
			if(countAll > 0) {
				List<ManualProductBean> manualProductList = mpDB.getAll();
				model.addObject("manualProductList", manualProductList);
				model.addObject("manualProductSize", countAll);
			}
		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/manualproductmanager-deleteproduct")
	public @ResponseBody void manualDeleteAddedProduct(HttpServletRequest request, HttpServletResponse response, @RequestParam("deleteId") int deleteId) throws SQLException, IOException{
		JSONObject json = new JSONObject();
		try{
			if(deleteId > 0){
				ManualProductDB mpDB = DatabaseManager.getDatabaseManager().getManualProductDB();
				ManualProductBean mpBean = mpDB.getById(deleteId);
				if(mpBean != null && mpBean.getMerchantId() > 0 && StringUtils.isNotBlank(mpBean.getProductName())) {
					ConfigDB cDB = DatabaseManager.getDatabaseManager().getConfigDB();
					ConfigBean cBean = cDB.getByName("manual.product.manager.senddata.default.server");
					if(cBean != null) {
						MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
						List<MerchantBean> mBeanList = mDB.getMerchantsByMerchantId(mpBean.getMerchantId());
						if(mBeanList != null && mBeanList.size() > 0) {
							int target = 0, merchantCount = 0;
							for (MerchantBean mBean : mBeanList) {
								if(mBean.getActive() == 1) {
									target = merchantCount;
									break;
								}
								merchantCount++;
							}
							
							MerchantBean mBean = mBeanList.get(target);
							if(StringUtils.isNotBlank(mBean.getDataServer())) {
								ProductDataDB pdDB = DatabaseManager.getDatabaseManager().getProductDataDB(mBean.getDataServer());
								ProductDataBean pdBean = pdDB.getSpecificProduct(mpBean.getProductName(), mpBean.getMerchantId());
								if(pdBean != null) {
									SendDataBean sdBean = SendDataBean.createSendDataBean(pdBean, null, SendDataBean.ACTION.DELETE);
									SendDataDB sdDB = DatabaseManager.getDatabaseManager().getSendDataDB(cBean.getConfigValue());
									try {
										sdDB.insertSendData(sdBean);
										pdDB.deleteSpecificProduct(pdBean.getId());
										mpDB.deleteManual(mpBean.getId());
										LogUtil.info(request, PAGE, "deleteManual|MerchantId:" + mpBean.getMerchantId() + "|ProductName:" + mpBean.getProductName());
										json.put("header", "success");
									}catch(Exception e) {
										e.printStackTrace();
										json.put("header", "error-query-delete-get-exception");
									}
								}else {
									try {
										mpDB.deleteManual(mpBean.getId());
										LogUtil.info(request, PAGE, "deleteManual|MerchantId:" + mpBean.getMerchantId() + "|ProductName:" + mpBean.getProductName());
										json.put("header", "not-found-product-in-bot");
									}catch(Exception e) {
										e.printStackTrace();
									}
								}
							}else {
								json.put("header", "error-cannot-get-data-server-by-merchant-id");
							}
						}else {
							json.put("header", "error-cannot-get-merchant-data-by-merchant-id");
						}
					}else {
						json.put("header", "error-cannot-get-config-data");
					}
				}else {
					json.put("header", "error-cannot-get-manual-bean-by-id");
				}
			}else{
				json.put("header", "error-input-delete-target-id");
			}
			AjaxUtil.sendJSON(response, json);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
