package botbackend.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.TaskBean;
import botbackend.bean.TaskBean.STATUS;
import botbackend.bean.UserBean;
import botbackend.db.TaskDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import utils.DateTimeUtil;
import utils.FilterUtil;


@Controller
public class CrawlerCheckingController  {
	
	final static String PAGE = "Creawler Checking";
	final static String permissionName = "tools.crawlerchecking";
	
	@RequestMapping(value = {"/admin/crawlerchecking"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("crawlerchecking", "newPage", true);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes"})
	@RequestMapping(value = {"/admin/runcheck"})
	public void  getTaskf(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = ServletUtil.getStringParam(request, "url", "").trim().toLowerCase();
		String nextPageUrl = ServletUtil.getStringParam(request, "nextPageUrl", "").trim().toLowerCase();
		String productUrl = ServletUtil.getStringParam(request, "productUrl", "").trim().toLowerCase();
		String productName = ServletUtil.getStringParam(request, "productName", "").trim().toLowerCase();
		String productPrice = ServletUtil.getStringParam(request, "productPrice", "").trim().toLowerCase();
		String charset = ServletUtil.getStringParam(request, "charset", "UTF-8").trim().toLowerCase();
		
		JSONObject m = new JSONObject();
		url = URLDecoder.decode(url , "UTF-8");
		if(url.isEmpty() || !url.contains("http")){
			m.put("header", "error");
			m.put("result", "URL Invalid");
			JsonWriter.getInstance().write(response, m);	
			return;
		}


		URL netUrl = new URL(url);
		String host = netUrl.getProtocol()+"://"+netUrl.getHost();
		url = URLEncoder.encode(url , "UTF-8");
		url = url.replace("%3A", ":").replace("%2F", "/");
		
		String html = httpRequest(url, charset, 0, 0).toLowerCase();
		JSONObject result = new JSONObject();
		String pdHtml = null; 
		if(!productUrl.isEmpty()){
			productUrl = (productUrl.contains("http")?"":host+ (productUrl.startsWith("/")?"":"/")) +productUrl;
			pdHtml = httpRequest(productUrl, charset, 0, 0).toLowerCase();
		}
		
		if(StringUtils.isBlank(html)){
			result.put("passSSL", "no data");
			result.put("available", "false");
			result.put("nextpage", nextPageUrl.isEmpty()?"":"false");
			result.put("productlink", productUrl.isEmpty()?"":"false");
			
		}else if (html.equals("ssl")){	
			result.put("passSSL", "false");
			result.put("available", "false");
			result.put("nextpage", nextPageUrl.isEmpty()?"":"false");
			result.put("productlink", productUrl.isEmpty()?"":"false");
			
		}else{
			result.put("passSSL", "true");
			result.put("available", "true");
			if(nextPageUrl.isEmpty()){
				result.put("nextpage", "no data");
			}else{
				nextPageUrl = FilterUtil.getStringAfter(nextPageUrl, "://", nextPageUrl);
				String npUrl = nextPageUrl;
				while(StringUtils.isNotBlank(npUrl)){
					if(html.contains(npUrl)|| html.contains(URLDecoder.decode(npUrl,charset)) || html.contains(StringEscapeUtils.escapeHtml4(npUrl))){
						result.put("nextpage", "true");
						break;
					}else{
						npUrl = FilterUtil.getStringAfter(npUrl, "/", "");
					}
				}
				if(npUrl.isEmpty()){
					result.put("nextpage", "false");
				}
			}
			
			if(productUrl.isEmpty()){
				result.put("productlink", "no data");
			}else{
				productUrl = FilterUtil.getStringAfter(productUrl, "://", productUrl);
				String pdUrl = productUrl;
				while(StringUtils.isNotBlank(pdUrl)){
					
					if(html.contains(pdUrl)|| html.contains(URLDecoder.decode(pdUrl,charset)) || html.contains(StringEscapeUtils.escapeHtml4(pdUrl))){
						result.put("productlink", "true");
						break;
					}else{
						pdUrl = FilterUtil.getStringAfter(pdUrl, "/", "");
					}
				}
				if(pdUrl.isEmpty()){
					result.put("productlink", "false");
				}
			}
		}
		
		if(StringUtils.isBlank(html)|| html.equals("ssl") || StringUtils.isBlank(pdHtml) || pdHtml.equals("ssl")){
			result.put("nameMatch", productName.isEmpty()?"":"false");
			result.put("priceMatch", productPrice.isEmpty()?"":"false");

		}else{
			if(productName.isEmpty()){
				result.put("nameMatch", "no data");
			}else if(pdHtml.contains(productName) || pdHtml.contains(StringEscapeUtils.escapeHtml4(productName))){
				result.put("nameMatch", "true");
			}else {
				result.put("nameMatch", "false");
			}
			
			if(productPrice.isEmpty()){
				result.put("priceMatch", "no data");
			}else{
				result.put("priceMatch", String.valueOf(pdHtml.contains(productPrice)));
			}
					
		}
		String status = "success";
		if(result.get("passSSL").equals("false")||result.get("available").equals("false")){
			status = "danger";
		}else {
			java.util.Iterator keys = result.keySet().iterator();
			while( keys.hasNext() ) {
			    String key = (String)keys.next();
			    if ( result.get(key).equals("false") ) {
			    	status = "warning";
			    }
			}
		}
		LogUtil.info(request, PAGE, "runcheck");
		
		m.put("header", "success");
		m.put("status", status);
		m.put("result", result);
		AjaxUtil.sendJSON(response, m);	

	}
	
	public String httpRequest(String url, String charset, int connectTimeout, int readTimeout) {
		
		final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
		
		int size= 600000;
		url = StringEscapeUtils.unescapeHtml4(url);
		URL u = null;
	 	HttpURLConnection conn = null;
	 	InputStreamReader isr = null;
    	try{
    		u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setDoOutput(true);
			conn.setRequestProperty("User-Agent", USER_AGENT);
			
			if(connectTimeout > 0){
				conn.setConnectTimeout(connectTimeout);
			}
			if(readTimeout > 0){
				conn.setReadTimeout(readTimeout);
			}

			conn.connect();
			if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
			StringBuilder rtn = new StringBuilder(size);
			isr = new InputStreamReader(conn.getInputStream(), charset);
			char[] buffer = new char[size];
			while (true) {
	  		      int rsz = isr.read(buffer, 0, buffer.length);
	  		      if (rsz <= 0 ) {
	  		        break;
	  		      }
	  		    rtn.append(new String(buffer, 0, rsz));
	  		    }  
			
	 		return rtn.toString();
	 		
    	}catch (SSLException e) {
    		return "ssl";
    	}catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	} finally {
			if(conn != null) {
				conn.disconnect();
			}
			if(isr != null) {
				try { isr.close(); } catch (IOException e) { }
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/addTaskForCheck"})
	public  @ResponseBody void  addTaskForCheck(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj  = new JSONObject();
		try {

			String actionType = "checkCrawlerWeb";
			String detail     = StringUtils.defaultString((String)request.getParameter("data"));
			String duedate    = DateTimeUtil.calculateWorkDayNoHoliday();
			TaskDB taskDB = DatabaseManager.getDatabaseManager().getTaskDB();
		    
		    HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			
		    TaskBean taskBean  = new TaskBean();
		    JSONObject json = new JSONObject();	 
			
			if(StringUtils.isNotBlank(duedate)){
				json.put("duedate",duedate);
			}
			if(StringUtils.isNotBlank(detail)){
				json.put("detail",detail);
			}
			
			json.put("merchantId",0);
			taskBean.setData(json.toString());
			taskBean.setIssue("Check crawler web");
			taskBean.setProcess(actionType);
			taskBean.setMerchantId(0);
			taskBean.setOwner(userBean.getUserName());
			taskBean.setStatus(STATUS.WAITING.toString());
			taskBean.setIssueType("test");
			
			int success = taskDB.insertTaskReturnID(taskBean);
			if(success > 0){
				obj.put("success", true);
				obj.put("taskID", success);
				LogUtil.info(request, PAGE, "addNewtask from crawler check --> "+ actionType);
			}else {
				obj.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			obj.put("success", false);
		}
		JsonWriter.getInstance().write(response,obj);
	}
	
}
