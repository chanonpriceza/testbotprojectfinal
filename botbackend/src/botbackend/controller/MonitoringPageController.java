package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.ConfigBean;
import botbackend.bean.MonitoringBean;
import botbackend.bean.MonitoringDetailBean;
import botbackend.bean.UserBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.ConfigDB;
import botbackend.db.MonitoringDB;
import botbackend.db.MonitoringDetailDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.CentralizeUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;

@Controller
public class MonitoringPageController {
	final static String PAGE = "Merchant Monitoring";
	final static String permissionName = "merchant.merchantmonitoring";
	private static final int numRecord = 20;
	private static final int USER_STATUS = 1;
	final static String SERVICE_URL = "page.merchantmonitoring.service.url.";
	final static String[] COUNTRY_LIST = new String[] {"TH","VN","ID","SG","MY","PH"};


	@RequestMapping(value = {"/admin/merchantmonitoring"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");

		return getMonitoringkData(request, response);
	}
	
	public ModelAndView  getMonitoringkData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("merchantmonitoring", "newPage", true);
		MonitoringDB monitoringDB = DatabaseManager.getDatabaseManager().getMonitoringDB();
		
		int page = ServletUtil.getIntParameter(request, "page", 0);
		String searchParam = "";
		String processType = "";
		String topicType = "";
		int searchType = 0;
		UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
		List<UserBean> userList = userDB.getUserList(USER_STATUS);
		
		
		searchParam = StringUtils.defaultString((String)request.getParameter("searchParam")).trim();
		topicType = StringUtils.defaultString((String)request.getParameter("topicType")).trim();
		processType = StringUtils.defaultString((String)request.getParameter("processType")).trim();
		searchType = ServletUtil.getIntParameter(request, "searchType", 0);
		
		if(page < 1){ page = 1; }
		int total = monitoringDB.countMonitorData(searchParam, topicType, processType,searchType);
		if(total > 0){
			int start = (page - 1) * numRecord;
			if(start >= total) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)total/(double)numRecord));
			
			if(page > totalPage) {
				page = 1;
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, total, numRecord);

			List<MonitoringBean> monitorList = monitoringDB.getData(searchParam, topicType, processType,searchType, start, numRecord);
			if (monitorList != null && monitorList.size() > 0) {
				model.addObject("monitorList", monitorList);
				model.addObject("pagingBean", pagingBean);
			}
		}
		
		model.addObject("userList", userList);
		model.addObject("searchType", searchType);
		model.addObject("processType", processType);
		model.addObject("topicType", topicType);
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		model.addObject("searchParam", searchParam);
		return model;
	
	}
	
	@RequestMapping(value = {"/admin/merchantMonitoringDataAjax"})
	public void  getDataAjax(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView("merchantmonitoring", "newPage", true);
		MonitoringDB monitoringDB = DatabaseManager.getDatabaseManager().getMonitoringDB();
		ConfigDB configDB = DatabaseManager.getDatabaseManager().getConfigDB();
		int page = ServletUtil.getIntParameter(request, "page", 0);
		String searchParam = "";
		String processType = "";
		String topicType = "";
		int searchType = 0;
		UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
		List<UserBean> userList = userDB.getUserList(USER_STATUS);
		
		searchParam = StringUtils.defaultString((String)request.getParameter("searchParam")).trim();
		topicType = StringUtils.defaultString((String)request.getParameter("topicType")).trim();
		processType = StringUtils.defaultString((String)request.getParameter("processType")).trim();
		searchType = ServletUtil.getIntParameter(request, "searchType", 0);

		if(page < 1){ page = 1; }
		int total = monitoringDB.countMonitorData(searchParam, topicType, processType,searchType);
		if(total > 0){
			int start = (page - 1) * numRecord;
			if(start >= total) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)total/(double)numRecord));
			
			if(page > totalPage) {
				page = 1;
			}
			Map<String,String> serviceUrls = new HashMap<String,String>();
			
			for(String s :COUNTRY_LIST) {
				ConfigBean bean = configDB.getByName(SERVICE_URL+s.toLowerCase());
				String url = null;
				if(bean!=null) {
					url = bean.getConfigValue();
				}
				if(url!=null) {
					serviceUrls.put(s.toLowerCase(),url);
				}
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, total, numRecord);
			List<MonitoringBean> monitorList = monitoringDB.getData(searchParam, topicType, processType,searchType, start, numRecord);
			
			
			if (monitorList != null && monitorList.size() > 0) {
				model.addObject("monitorList", monitorList);
				model.addObject("pagingBean", pagingBean);
				model.addObject("serviceUrls",serviceUrls);
			}
		}
		model.addObject("userList", userList);
		model.addObject("searchType", searchType);
		model.addObject("processType", processType);
		model.addObject("topicType", topicType);
		model.addObject("searchParam", searchParam);
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		JsonWriter.getInstance().write(response,model);
	
	}
	
	@RequestMapping(value = {"/admin/monitoringmanagerAjax"})
	private @ResponseBody void  monitoringmanagerAjax(@RequestParam("monitoringId") int monitoringId,HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			
			if(cmd.equals("getMonitorById")){
				getMonitorById(monitoringId,request, response);
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void getMonitorById(int monitoringId, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		JSONObject json = new JSONObject();
		try {
			MonitoringDB monitoringDB = DatabaseManager.getDatabaseManager().getMonitoringDB(); 
			MonitoringBean data = monitoringDB.selectById(monitoringId);

			json.put("data", data);
			json.put("header", "success");
			json.put("monitoringId", monitoringId);
			System.out.println(data);
			
		}catch(Exception e) {
			json.put("header", "error");
			e.printStackTrace();
		}
		String origin = request.getHeader("Origin");
		CentralizeUtil.checkAllowOrigin(origin, response);
		JsonWriter.getInstance().write(response, json);
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/infoMonitoringManager"})
	public  @ResponseBody void  infoMonitoringManager(@RequestParam("monitoringId") int monitoringId,@RequestParam("merchantId") int merchantId,
			@RequestParam("type") String type ,HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{

			JSONObject json = new JSONObject();
			
			MonitoringDB monitoringDB = DatabaseManager.getDatabaseManager().getMonitoringDB();
			MonitoringDetailDB monitorDetailDB = DatabaseManager.getDatabaseManager().getMonitoringDetailDB();

			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
	
			MonitoringBean monitorData = monitoringDB.getDataByIdWithType(monitoringId,type);
			List<MonitoringDetailBean> dataDetailList =  monitorDetailDB.getDataById(merchantId,type);
			
			json.put("header", "success");
			json.put("dataDetailList", dataDetailList);
			json.put("monitorData", monitorData);
			json.put("monitoringId", monitoringId);
			json.put("currentUser", userBean.getUserName());
			json.put("currentRole",userBean.getUserPermission());
			String origin = request.getHeader("Origin");
			CentralizeUtil.checkAllowOrigin(origin, response);
			JsonWriter.getInstance().write(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
