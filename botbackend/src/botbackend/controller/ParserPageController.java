package botbackend.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import bean.ParserConfigBean;
import botbackend.bean.ProductParserBean;
import botbackend.bean.MerchantConfigBean.FIELD;
import botbackend.component.MerchantConfigComponent;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.ProductDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.parser.automate.ScrapeProduct;
import botbackend.system.BaseConfig;
import botbackend.utils.JsonWriter;
import botbackend.utils.ParserUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;
import bean.ProductDataBean;
import web.parser.image.DefaultImageParser;
import web.parser.image.ImageParserInterface;

@Controller
public class ParserPageController {
	
	final static String permissionName = "crawlerconfig.parser";
	
	final static String PAGE = "ParserPage";
	final static String DEFAULT_CHARSET = "UTF-8";
	private static final String[] BOT_5_COUNTRY = new String[] {"th"};
	private static final String BOT_5_PARSER_CLASS = "web.parser.DefaultUserHTMLParser";
	private static final String BOT_4_PARSER_CLASS = "com.ha.bot.parser.DefaultUserHTMLParser";
	private static String BOT_SERVER_CURREBT_COUNTRY = BaseConfig.BOT_SERVER_CURRENT_COUNTRY;
	
	@RequestMapping(value = "/admin/parser")
	public ModelAndView getModelAndView(HttpServletRequest request) throws IOException, ServletException, SQLException {	
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("parser", "", true);
	}
	
	@RequestMapping(value = "/admin/runparser")
	public void runParser(HttpServletRequest request, HttpServletResponse response,  @RequestParam("data")  String data, @RequestParam("url")  String[] urls, @RequestParam("merchantId") int merchantId, @RequestParam("parserClass")  String parserClass) throws SQLException, ParseException, JsonGenerationException, JsonMappingException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();
		try {

			List<bean.ParserConfigBean> psConfig = getParserConfigParserList(data, merchantId);
			Map<String, List<bean.ProductDataBean>> result = ParserUtil.parseGetAllSequence(parserClass, psConfig, urls);
			m.put("list", result);
			m.put("header", "success");
			
		} catch (Exception e) {
			e.printStackTrace();
			m.put("header", "error");
		}

		LogUtil.info(request, PAGE, "runparser|MerchantId:" + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/saveparser")
	public void saveParser(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("data")  String data) throws SQLException, ParseException, JsonGenerationException, JsonMappingException, IOException{
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		List<Object[]> list = new ArrayList<Object[]>();
		MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
		
		int active = 0;
		if(mList != null && mList.size() > 0){
			for(MerchantBean merchant : mList){
				if(merchant.getActive() == 1){ active = 1; break;}
			}
		}

		try{
			JSONObject json = (JSONObject)new JSONParser().parse(data);
			if(active != 0 && active != 2){
				json.remove("name");
				json.remove("concatId");
				json.remove("concatWord");
			}
			for(Object key : json.keySet()){
				String field = (String)key;
				JSONArray jArr = (JSONArray)json.get(key);
				for(Object jData : jArr){
					JSONObject rec = (JSONObject)jData;
					String filter = (String)rec.get("filter");
					String value = (String)rec.get("value");
					String[] split_value = value.split(",");
					for(String val : split_value){
						val = val.replace("|", "");
						if(!val.startsWith("'") && !val.endsWith("'")){
							continue;
						}
						String tmp = val.substring(1, val.length()-1);
						if(tmp.trim().isEmpty()){
							value = value.replace(val, tmp);
						}
						
					
					}

					Object[] oArr = new Object[]{ merchantId, field, filter, value};
					list.add(oArr);
				}
			}
		}catch(Exception e){
			m.put("header", "error");
		}
		
		LinkedHashMap<String, String> mValidate = ParserUtil.getConfigValidate(ParserUtil.getParserConfig(data));
		if(!mValidate.isEmpty()){
			m.put("header", "errorValidate");
			m.put("mValidate", mValidate);
		}else{
			if(!list.isEmpty()){
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
				int size = list.size();
				Object[][] obj = new Object[size][4];
				for(int i = 0; i < size; i++){
					obj[i] = list.get(i);
				}
				try { 
					mcDB.deleteConfigByConfigList(merchantId, Arrays.asList(FIELD.parserClass.toString(), FIELD.wceParserClass.toString(), FIELD.dueParserClass.toString()));
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
						MerchantConfigBean parserClassConfig = new MerchantConfigBean();
						parserClassConfig.setMerchantId(merchantId);
						parserClassConfig.setField(FIELD.parserClass.toString());
						parserClassConfig.setValue(BOT_5_PARSER_CLASS);
						mcDB.insertMerchantConfig(parserClassConfig);
					}
					
					if(active != 0 && active != 2){
						pDB.deleteByMerchantIdExcept(merchantId);
					}else{
						pDB.deleteByMerchantId(merchantId);
					}
					pDB.insert(obj);
					
					m.put("header", "success");
					LogUtil.info(request, PAGE, "saveParser|MerchantId:" + merchantId);
				} catch (Exception e) { 
					e.printStackTrace();
					m.put("header", "error");
				}
			}else{
				m.put("header", "notfound");
			}
		}
		
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/savefield")
	public void saveField (HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("data")  String data) throws SQLException, ParseException, JsonGenerationException, JsonMappingException, IOException{

		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> fieldList = new ArrayList<>();
		List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
		
		int active = 0;
		if(mList != null && mList.size() > 0){
			for(MerchantBean merchant : mList){
				if(merchant.getActive() == 1){ active = 1; break;}
			}
		}
		
		try{
			JSONObject json = (JSONObject)new JSONParser().parse(data);
			if(active != 0 && active != 2){
				json.remove("name");
				json.remove("concatId");
				json.remove("concatWord");
			}
			for(Object key : json.keySet()){
				String field = (String)key;
				JSONArray jArr = (JSONArray)json.get(key);
				for(Object jData : jArr){
					JSONObject rec = (JSONObject)jData;
					String filter = (String)rec.get("filter");
					String value = (String)rec.get("value");
					String[] split_value = value.split(",");
					for(String val : split_value){
						val = val.replace("|", "");
						if(!val.startsWith("'") && !val.endsWith("'")){
							continue;
						}
						String tmp = val.substring(1, val.length()-1);
						if(tmp.trim().isEmpty()){
							value = value.replace(val, tmp);
						}
					}
					
					if(!fieldList.contains(field)){
						fieldList.add(field);
					}
					
					Object[] oArr = new Object[]{ merchantId, field, filter, value};
					list.add(oArr);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			m.put("header", "error");
		}
		
		LinkedHashMap<String, String> mValidate = ParserUtil.getConfigValidate(ParserUtil.getParserConfig(data));
		if(!mValidate.isEmpty()){
			m.put("header", "errorValidate");
			m.put("mValidate", mValidate);
		}else{
			if(!list.isEmpty()){
				ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				int size = list.size();
				Object[][] obj = new Object[size][4];
				for(int i = 0; i < size; i++){
					obj[i] = list.get(i);
				}
				try { 
					mcDB.deleteConfigByConfigList(merchantId, Arrays.asList(FIELD.parserClass.toString(), FIELD.wceParserClass.toString(), FIELD.dueParserClass.toString()));
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
						MerchantConfigBean parserClassConfig = new MerchantConfigBean();
						parserClassConfig.setMerchantId(merchantId);
						parserClassConfig.setField(FIELD.parserClass.toString());
						parserClassConfig.setValue(BOT_5_PARSER_CLASS);
						mcDB.insertMerchantConfig(parserClassConfig);
					}
					
					pDB.deleteByMerchantIdAndField(merchantId, fieldList);
					pDB.insert(obj);
					m.put("header", "success");

					LogUtil.info(request, PAGE, "saveParser|MerchantId:" + merchantId);
				} catch (Exception e) {
					e.printStackTrace();
					m.put("header", "error");
				}
			}else{
				m.put("header", "notfound");
				LogUtil.info(request, PAGE, "saveParser --> notfound|MerchantId:" + merchantId);
			}
		}
		
		JsonWriter.getInstance().write(response, m);
	}
	
	private List<bean.ProductDataBean> mergeProductDataBean(List<bean.ProductDataBean> oldPdb,List<bean.ProductDataBean> newPdb) {
		List<bean.ProductDataBean> mergeData = new ArrayList<bean.ProductDataBean>();
		for(bean.ProductDataBean old:oldPdb) {
			boolean isAdd = false;
			for(bean.ProductDataBean current:newPdb) {
				if(old.getName().equals(current.getName())||current.isExpire()) {
					mergeData.add(current);
					isAdd = true;
					break;
				}
			}
			if(!isAdd) {
				mergeData.add(new bean.ProductDataBean());
			}
		}
		return mergeData;
	}
	
	private String getPictureData(String pictureUrl, String currentUrl, Properties merchantConfig){
		ImageParserInterface imageParser = null;
		
		imageParser = new DefaultImageParser();
		
		if(StringUtils.isNotBlank(pictureUrl)){
			if(!pictureUrl.startsWith("http")){
				try {
					URL url = new URL(new URL(currentUrl), pictureUrl);
					pictureUrl = url.toString();
		    	} catch (MalformedURLException e) {}
			}
			
			if(pictureUrl.startsWith("http")){
				byte[] bPic = imageParser.parse(pictureUrl, 200, 200);
				if(bPic != null){
					bPic = Base64.getEncoder().encode(bPic);
					return new String(bPic);
				}
			}
		}
		return null;
		
	}
	
	private List<bean.ParserConfigBean> getParserConfigParserList(String data,int merchantId) throws ParseException{
		
		List<bean.ParserConfigBean> psConfigBean = new ArrayList<bean.ParserConfigBean>();
		JSONObject json = (JSONObject)new JSONParser().parse(data);
		for(Object key : json.keySet()){
			String field = (String)key;
			JSONArray jArr = (JSONArray)json.get(key);
			for(Object jData : jArr){
				JSONObject rec = (JSONObject)jData;
				String filter = (String)rec.get("filter");
				String value = (String)rec.get("value");
				String[] split_value = value.split(",");
				for(String val : split_value){
					val = val.replace("|", "");
					if(!val.startsWith("'") && !val.endsWith("'")){
						continue;
					}
					String _tmp = val.substring(1, val.length()-1);
					if(_tmp.trim().isEmpty()){
						value = value.replace(val, _tmp);
					}

				}
				bean.ParserConfigBean ps = new bean.ParserConfigBean();
				ps.setFieldType(field);
				ps.setFilterType(filter);
				ps.setValue(value);
				ps.setMerchantId(merchantId);
				psConfigBean.add(ps);
			}
		}
		
		return psConfigBean;
	}

	@RequestMapping(value = "/admin/getproductdata")
	public @ResponseBody void getProductData(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId) throws SQLException, IOException{
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		HashMap<String, Object> m = new HashMap<String, Object>();
		MerchantBean mBean = merchantDB.getMerchantByMerchantId(merchantId);

		try{
			if(mBean != null){
				String getDataServer = mBean.getDataServer();
				if(StringUtils.isNotBlank(getDataServer)){
					try{
						ProductDataDB productDB = DatabaseManager.getDatabaseManager().getProductDataDB(getDataServer);
						if (productDB!=null) {
							// Get only 100 url samples 
							List<ProductDataBean> plist = productDB.getMerchantDataById(merchantId, 100);
							m.put("header", "success");
							m.put("plist", plist);
						} else {
							LogUtil.info(request, PAGE, "getproductdata --> No Server Name : "+ getDataServer );
						}
					} catch(Exception e){
						e.printStackTrace();		
					}
				}else{
					LogUtil.info(request, PAGE, "getproductdata --> No Server Name for this merchant|MerchantId:"+ merchantId );
				}		
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/compareproductdata")
	public @ResponseBody void compareproductdata(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId , @RequestParam("config")  String config, @RequestParam("data")  String data) throws SQLException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		List<bean.ProductDataBean> oldList = new ArrayList<bean.ProductDataBean>();
		List<bean.ProductDataBean> newList = new ArrayList<bean.ProductDataBean>();
		
		try{
			oldList = mapper.readValue(data, new TypeReference<List<bean.ProductDataBean>>() {});
			
			if(oldList != null && oldList.size() > 0){
				
				Properties mConfig = MerchantConfigComponent.loadMerchantConfig(merchantId);
				List<bean.ParserConfigBean> psConfig = getParserConfigParserList(config, merchantId);
				String parserClass = mConfig.getProperty(MerchantConfigBean.FIELD.parserClass.toString());
				
				if(StringUtils.isBlank(parserClass)) {
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
						parserClass = BOT_5_PARSER_CLASS;
					} else {
						parserClass = BOT_4_PARSER_CLASS;
					}
				}
				
				List<String> urls = new ArrayList<String>();
				for(bean.ProductDataBean pBean : oldList){
					String dataPic =getPictureData(pBean.getPictureUrl(),pBean.getUrl(),mConfig);
					urls.add(pBean.getUrl());
					pBean.setPictureData(dataPic);
				}	
				newList = ParserUtil.parse(parserClass, psConfig, urls.toArray(new String[urls.size()]));
				
				if(newList.size()!=oldList.size()) {
					newList = mergeProductDataBean(oldList, newList);
				}
				
				if(newList != null && newList.size() > 0){
					m.put("oldList", oldList);
					m.put("newList", newList);
					m.put("header", "success");
				}
			}					
		} catch(Exception e){
			e.printStackTrace();		
		}
		LogUtil.info(request, PAGE, "compareproductdata|oldListCount:"+oldList.size()+"|newListCount:"+newList.size()+"|MerchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/searchparser")
	public @ResponseBody void getMerchant(HttpServletResponse response, @RequestParam("merchantId") int mId) throws SQLException, IOException{		
		HashMap<String, Object> m = new HashMap<String, Object>();
		try {
			MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean> mList = mDB.getMerchantsByMerchantId(mId);
			
			MerchantBean mBean = null;
			if(mList != null && mList.size() > 0){
				for(MerchantBean merchant : mList){
					if(merchant.getActive() == 1){ mBean = merchant; break;}
					if(mBean == null){ mBean = merchant; }
				}
			}
			
			if(mBean == null){
				m.put("header", "notFoundMerchant");
			}else{
				m.put("header", "success");
				m.put("merchant", mBean);
				
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				MerchantConfigBean mcBean = mcDB.findMerchantConfigByIdAndField(mId, FIELD.parserClass.toString());
				if(mcBean == null) {
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
						m.put("parserClass", BOT_5_PARSER_CLASS);
					} else {
						m.put("parserClass", BOT_4_PARSER_CLASS);
					}
				} else if(!mcBean.getValue().equals(BOT_5_PARSER_CLASS) && !mcBean.getValue().equals(BOT_4_PARSER_CLASS)) { 
					m.put("parserClass", mcBean.getValue());
				} else {
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURREBT_COUNTRY)) {
						m.put("parserClass", BOT_5_PARSER_CLASS);
					} else {
						m.put("parserClass", BOT_4_PARSER_CLASS);
					}
					
					ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
					List<ParserConfigBean> pList = pDB.getByMerchantId(mId);
					if(pList != null && !pList.isEmpty()){
						m.put("plist", pList);
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		JsonWriter.getInstance().write(response, m);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/tool0GetParser")
	public @ResponseBody void tool0GetParser(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("jsonData") String jsonData) throws SQLException, IOException{		
 
		HashMap<String, Object> m = new HashMap<String, Object>();
		try{
			if(merchantId > 0 && StringUtils.isNotBlank(jsonData)){
				List<ProductParserBean> resultList = new ArrayList<ProductParserBean>();
				JSONArray jArr = (JSONArray) new JSONParser().parse(jsonData);
				if(jArr == null || jArr.size() == 0){
					m.put("header", "errorAfterParse");
				}else{
					List<JSONObject> urlListJson = new ArrayList<>();
					for (Object jObj : jArr) {
						JSONObject obj = (JSONObject) jObj;
						urlListJson.add(obj);
					}

					if(urlListJson != null && urlListJson.size() > 0){
						try{
							List<String> urlList = new ArrayList<>();
							for(int i=0; i<urlListJson.size(); i++){
								JSONObject obj = urlListJson.get(i);
								String tool0Url = (String) obj.get("tool0Url");
								if(StringUtils.isNotBlank(tool0Url)){
									urlList.add(tool0Url);
								}else {
									LogUtil.info(request, PAGE, "tool0GetParser -->  Url is Blank|MerchantId:"+ merchantId );
									m.put("header", "error");
									continue;
								}
							}
							resultList = ScrapeProduct.predict(urlList);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				
					if(resultList != null && resultList.size() > 0){
						
						JSONArray jArrayResult =  new JSONArray();
						for(ProductParserBean bbean : resultList){
							JSONObject jsonBean = new JSONObject();
							jsonBean.put("name", bbean.getName());
							jsonBean.put("price", bbean.getPrice());
							jsonBean.put("desc", bbean.getDescription());
							jsonBean.put("baseprice", bbean.getBasePrice());
							jsonBean.put("picurl", bbean.getPictureUrl());
							jsonBean.put("url", bbean.getUrl());
							jsonBean.put("realproductid ", bbean.getRealProductId());
							jArrayResult.add(jsonBean);
						}
						m.put("header","success");
						m.put("result", jArrayResult);
						
						LogUtil.info(request, PAGE, "tool0GetParser|ResultCount:"+jArrayResult.size()+"|MerchantId " + merchantId);
					}
				}
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		JsonWriter.getInstance().write(response, m);
	}
	
	@RequestMapping(value = "/admin/tool1GetParser")
	public @ResponseBody void tool1GetParser(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("jsonData") String jsonData) throws SQLException, IOException{		
		HashMap<String, Object> m = new HashMap<String, Object>();
		Map<String, List<String>> summaryMap = new HashMap<>();
		try{
			if(merchantId > 0 && StringUtils.isNotBlank(jsonData)){
				JSONArray jArr = (JSONArray) new JSONParser().parse(jsonData);
				if(jArr == null || jArr.size() == 0){
					m.put("header", "errorAfterParse");
				}else{
					summaryMap = ParserUtil.awakeTool1(merchantId , jArr);
					m.put("header","success");
					m.put("result", summaryMap);
				}
				
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		LogUtil.info(request, PAGE, "tool1GetParser|summaryMap:"+ summaryMap.size() +"|MerchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}

	@RequestMapping(value = "/admin/tool2GetParser")
	public @ResponseBody void tool2GetParser(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("jsonData") String jsonData) throws SQLException, IOException{		
		HashMap<String, Object> m = new HashMap<String, Object>();
		Map<String, List<String>> resultMap = new HashMap<>();
		try{
			if(merchantId > 0 && StringUtils.isNotBlank(jsonData)){				
				JSONArray jArr = (JSONArray) new JSONParser().parse(jsonData);
				if(jArr == null || jArr.size() == 0){
					m.put("header", "errorAfterParse");
				}else{
					resultMap = ParserUtil.awakeTool2(merchantId, jArr);
					m.put("header","success");
					m.put("result", resultMap);
				}
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		
		LogUtil.info(request, PAGE, "tool1GetParser|summaryMap:"+ resultMap.size() +"|MerchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	} 

	
}
