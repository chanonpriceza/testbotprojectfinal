package botbackend.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import botbackend.db.BanKeywordDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.manager.BanKeywordManager;
import botbackend.manager.BanKeywordRule;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import utils.DateTimeUtil;

@Controller
public class CheckBanKeywordPageConttroller {

	final static String PAGE = "Check Approval";
	final static String permissionName = "tools.checkbankeyword";
	final static String dataUpdateState = "DATA_UPDATE_START";
	final static String startState = "CRAWLER_START";
	final static String startStatus = "WAITING";
	final static int LIMIT = 100;
	
	@RequestMapping(value = {"/admin/checkBanKeywordPage"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("checkBankeywordPage", "newPage", true);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/checkbankeywordwithword"})
	public void checkBanKeywordWithWord(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return;
		
		JSONObject resultObj = new JSONObject();
		JSONObject jsonObj = new JSONObject();
		
		try {
			BanKeywordDB banDB = DatabaseManager.getDatabaseManager().getBanKeywordDB();
			
			String word = ServletUtil.getStringParam(request,"word","");
			int price = ServletUtil.getIntParameter(request, "price", 0); 
			int merchantId = ServletUtil.getIntParameter(request, "merchantId", 0); 
			int categoryId = ServletUtil.getIntParameter(request, "categoryId", 0); 
			
			BanKeywordManager banManager = new BanKeywordManager(banDB);
			BanKeywordRule result = banManager.isBanKeywordWithKeyword(word,merchantId,categoryId,Double.valueOf(price));
			
			jsonObj.put("result",result!=null);
			jsonObj.put("cat",categoryId==-1?"No CateforyId":categoryId+"");
			jsonObj.put("inputName",word);
			jsonObj.put("inputPrice",price);
        
			if(result!=null) {
				jsonObj.put("BanKeyword",result.getKeywordList());
				jsonObj.put("MerchantIds",result.getMerchantIds() );
				jsonObj.put("NonKeywordList", result.getNonKeywordList()==null?result.getNonKeywordList():"null");
				jsonObj.put("FullKeyword",result.getFullKeyword() );
				jsonObj.put("minPrice",result.getMinPrice());
			}
			resultObj.put("result",jsonObj);
			resultObj.put("header","success");
			JsonWriter.getInstance().write(response,resultObj);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			resultObj.put("header","error");
			resultObj.put("message",e);
			JsonWriter.getInstance().write(response,resultObj);
		}
		
	}
	
	@RequestMapping(value = {"/admin/checkbankeywordwithcsv"})
	public void checkBanKeywordWithCSV(HttpServletRequest request, HttpServletResponse response) throws Exception {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(isMultipart) {
			try {
				importBanKeyWordCSV(request, response);
			} catch (Exception e) {
				e.printStackTrace();		
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private @ResponseBody void importBanKeyWordCSV(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		try{
			
			JSONObject json = new JSONObject();
			
			JSONArray resultArray = new JSONArray();
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			fileItemFactory.setSizeThreshold(50*1024);
			fileUpload.setSizeMax(10485760);
			List<FileItem> multiParts = fileUpload.parseRequest(request);
			String errorMsg = "";
			boolean error = false;
			if(multiParts != null && multiParts.size() > 0){
				for(FileItem item : multiParts) {
					if (item.getFieldName().equals("inputPatternFile")) {
						if (StringUtils.isNotBlank(item.getName())) {
							
							InputStreamReader isr = new InputStreamReader(item.getInputStream(), "UTF-8");
							BufferedReader br = new BufferedReader(isr);
													
							errorMsg = parseCSV(br,resultArray);
							if(!errorMsg.isEmpty()){
								error = true;
							}
							item.delete();
						}
					}
				}
			}else{
				errorMsg = "Cannot detect upload file.";
				error = true; 
			}
			multiParts.clear();
			
			if(!error) {
				json.put("header", "success");
				json.put("result", resultArray);	
			}else{
				json.put("header", "fail");
				json.put("errorMsg", errorMsg);
			}
			
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public String parseCSV(BufferedReader br,JSONArray jsonArray) throws IOException, SQLException{
		String errorMsg = "";
		String line = null;
		boolean first = true;
		
		BanKeywordDB banDB = DatabaseManager.getDatabaseManager().getBanKeywordDB();
		BanKeywordManager banKeyWordManager = new BanKeywordManager(banDB);
		
		while ((line = br.readLine()) != null) {
			try{
				StringReader lineReader = new StringReader(line);
				CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
				for (CSVRecord csvRecord : record) {
					if(first){
						first = false;
						
						String colum0Name = csvRecord.get(0); if(!colum0Name.contains("word"))		{ errorMsg = "Header error." ;break; } // utf8 - first colum have something before first char.
						String colum1Name = csvRecord.get(1); if(!colum1Name.equals("merchantId"))	{ errorMsg = "Header error." ;break; }
						String colum2Name = csvRecord.get(2); if(!colum2Name.equals("price"))		{ errorMsg = "Header error." ;break; }
						String colum3Name = csvRecord.get(3); if(!colum3Name.equals("categoryId"))	{ errorMsg = "Header error." ;break; }
						String colum4Name = csvRecord.get(4); if(!colum4Name.equals("result"))		{ errorMsg = "Header error." ;break; }
						
						continue;
					}
					try{
						String word = csvRecord.get(0);
						String merchantId = csvRecord.size() > 1 ?csvRecord.get(1):"";
						String price = csvRecord.size() > 2 ? csvRecord.get(2):"";
						String categoryId = csvRecord.size() > 3 ? csvRecord.get(3):"";
						String result = csvRecord.size() > 4 ? csvRecord.get(4):"";
						
						if(csvRecord.size() <= 4) {
							continue;
						}
						
						if(StringUtils.isBlank(word) && StringUtils.isBlank(price) && StringUtils.isBlank(categoryId) && StringUtils.isBlank(result)
								&& ( StringUtils.isBlank(merchantId)) ) {
							continue;
						}
						
						word = StringUtils.isNotBlank(word)?word:"";
						merchantId = StringUtils.isNumeric(merchantId)?merchantId:"0";
						categoryId = StringUtils.isNumeric(categoryId)?categoryId:"0";
						price = StringUtils.isNumeric(price)?price:"0";
						
						BanKeywordRule banKeywordResult = banKeyWordManager.isBanKeywordWithKeyword(word,Integer.valueOf(merchantId),Integer.valueOf(categoryId),Double.valueOf(price));
						
						JSONObject jObj = new JSONObject();
						jObj.put("word", word);
						jObj.put("merchantId", merchantId);
						jObj.put("price", price);
						jObj.put("categoryId", categoryId);
						jObj.put("result", banKeywordResult!=null);
						if(banKeywordResult!=null) {
							jObj.put("keyWordList",banKeywordResult.getKeywordList());
							jObj.put("merchantIdList",banKeywordResult.getMerchantIds());
							jObj.put("minPrice",banKeywordResult.getMinPrice());
							
						}
						jsonArray.add(jObj);
						
					}catch(Exception e){
						e.printStackTrace();
						errorMsg = "Parse data error.";
						break;
					}
				}
				record.close();
			}catch(Exception e){
				e.printStackTrace();
				errorMsg = "Parse data error.";
				break;
			}
		}
		return errorMsg;
	}
	
	@RequestMapping(value = {"/admin/getBanKeywordTemplateCSV"})
	private @ResponseBody void exportBanKeyWordTemplate(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String file = "";
		try{
			
			String dateFile = DateTimeUtil.generateStringDate(new Date(), "yyyyMMdd-HHmmss");
			file = "Template-"+dateFile;

			out.println("word,merchantId,price,categoryId,result");	
			out.print("\""+"กระเป๋า (มือหนึ่ง) (350524061)" +"\",");
			out.print("\""+"30045" +"\",");
			out.print("\""+"2000" +"\",");
			out.println("\""+"110203" +"\",");	
			//--------------------------------------------------------
			out.print("\""+"Louis Vuitton เปิดบันทึก กลยุทธ์สร้างแบรนด์ หนังสือ" +"\",");
			out.print("\""+"30045" +"\",");
			out.print("\""+"450" +"\",");
			out.println("\""+"231899" +"\",");	
			//--------------------------------------------------------
			out.print("\""+"จักรยานเสือภูเขา RICHTER AK47 ล้อ27.5 เฟรมอลู เกียร์ 27 speed โช๊คปรับล็ค," +"\",");
			out.print("\""+"30045" +"\",");
			out.print("\""+"4900" +"\",");
			out.println("\""+"400108" +"\",");
			//--------------------------------------------------------
			out.print("\""+"ลูกสำรอง" +"\",");
			out.print("\""+"30045" +"\",");
			out.print("\""+"500" +"\",");
			out.println("\""+"0" +"\",");
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			response.setContentType("application/csv");
	        response.addHeader("Content-Disposition", "attachment; filename = " + file+".csv"); 
	        response.addHeader("X-Frame-Options", "ALLOWALL");
	        response.flushBuffer();
	        
			out.flush();  
		    out.close();
		}
	}
	
	
}
