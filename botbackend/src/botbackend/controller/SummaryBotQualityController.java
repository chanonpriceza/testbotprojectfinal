package botbackend.controller;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import botbackend.bean.BotQualityReportBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.BotQualityReportDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.UserUtil;


@Controller
public class SummaryBotQualityController {

	private static final int numRecord =20;
	final static String PAGE = "New summary";
	final static String permissionName = "report.summary.botquality";
	
	@RequestMapping(value = {"/admin/summarybotquality"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		return getSummaryData("",1, "","","",true,"", request, response);
	}
	
	@RequestMapping(value = {"/admin/searchsummarybotquality"})
	public ModelAndView getSummaryData(
			@RequestParam(required=false, defaultValue="",  value="inputProductCountRange") String inputProductCountRange,
			@RequestParam(required=false, defaultValue="1", value="page") int page,
    		@RequestParam(required=false, defaultValue="",  value="searchParam") String searchParam,
      		@RequestParam(required=false, defaultValue="",  value="inputTimeRange") String inputTimeRange,
      		@RequestParam(required=false, defaultValue="",  value="activeType") String activeType,
      		@RequestParam(required=false, defaultValue="false",  value="showSummary") boolean showSummary,
      		@RequestParam(required=false, defaultValue="",  value="sortBy") String sortBy,
			HttpServletRequest request, HttpServletResponse response) {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		ModelAndView model = new ModelAndView("summarybotquality", "newPage", true);
		try{
			 List<BotQualityReportBean> getReportMerchantByFilterList = new ArrayList<BotQualityReportBean> ();
          	 BotQualityReportDB botQualityReportDB =  DatabaseManager.getDatabaseManager().getBotqualityreportDB();
          	 List<BotQualityReportBean>reportSummaryList =  botQualityReportDB.getReportSummary();
             int countDataByFilter = botQualityReportDB.countDataByFilter(searchParam,inputProductCountRange,inputTimeRange,activeType);
         	 if(countDataByFilter > 0){
				if (page < 1) {
					page = 1;
				}
             int start = (page -1)*numRecord;
             if (start >= countDataByFilter) {
					start = 0;
				}
             
				int totalPage = (int) (Math.ceil((double) countDataByFilter / (double) numRecord));
				if (page > totalPage) {
					page = 1;
				}
		        getReportMerchantByFilterList = botQualityReportDB.getReportMerchantByFilter(start,numRecord,searchParam,inputProductCountRange,inputTimeRange,activeType,sortBy);
         	 }
		  	 PagingBean pagingBean = PagingBean.createPagingBean(page, countDataByFilter, numRecord);
		     model.addObject("countDataByFilter",countDataByFilter);
		     model.addObject("getReportMerchantByFilterList",getReportMerchantByFilterList);
		     model.addObject("pagingBean",pagingBean);
		     model.addObject("reportSummaryList",reportSummaryList);
		     model.addObject("searchParam",searchParam);
		     model.addObject("inputProductCountRange",inputProductCountRange);
		     model.addObject("inputTimeRange",inputTimeRange);
		     model.addObject("activeType",activeType);
		     model.addObject("showSummary",showSummary);
		     model.addObject("sortBy",sortBy);
		     
		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;
	}
}
