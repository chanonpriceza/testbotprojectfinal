package botbackend.controller;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import utils.DateTimeUtil;

import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.bean.ServerRuntimeReportBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.ServerRuntimeReportDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.AjaxUtil;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;


@Controller
public class ServerRuntimeReportController {
	
	private static final String permissionName = "report.servers";
	
	private static final int numRecord = 20;
	private static final String DEFAULT_REPORT_TYPE = "DUE";
	private static final String DEFAULT_REPORT_SERVER = "";
	private static final int DEFAULT_REPORT_TIME = 0;
	private static final int DEFAULT_REPORT_RECORD = 0;
	
	@RequestMapping(value = {"/admin/serversruntimereport"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return getMonitorData(0,DEFAULT_REPORT_TYPE, null,DEFAULT_REPORT_SERVER,DEFAULT_REPORT_TIME,DEFAULT_REPORT_RECORD, request, response);
	}
	
	@RequestMapping(value = {"/admin/serversruntimereport?page={page}?reportType={reportType}?reportStatus={reportStatus}?reportServer={reportServer}?reportTime={reportTime}?reportRecord={reportRecord}"})
	public ModelAndView  getMonitorData(@PathVariable("page") int page,@PathVariable("reportType") String reportType,@PathVariable("reportStatus") String reportStatus,@PathVariable("reportServer") String reportServer,@PathVariable("reportTime") int reportTime,@PathVariable("reportRecord") int reportRecord,HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("serversruntimereport", "newPage", true);
		try{
			List<ServerRuntimeReportBean> reportList =  new ArrayList<ServerRuntimeReportBean>();
			ServerRuntimeReportDB serverruntimeReportDB = DatabaseManager.getDatabaseManager().getServerruntimeReportDB();
			String cmd = StringUtils.defaultString((String)request.getParameter("cmd"));
			
			if(cmd.equals("searchServerReport")){
			reportType =  StringUtils.defaultString(request.getParameter("reportType"), "DUE");  
			reportStatus =  StringUtils.defaultString(request.getParameter("reportStatus")); 
			reportServer = StringUtils.defaultString(request.getParameter("reportServer"));
			reportTime = ServletUtil.getIntParameter(request, "reportTime", 0);
			reportRecord = ServletUtil.getIntParameter(request, "reportRecord", 0);
			}
			
			int totalReportList = 0;
			totalReportList = serverruntimeReportDB.countServerReportBySearch(reportType, reportStatus,reportServer,reportTime,reportRecord);
			
			if(totalReportList > 0){
				if (page < 1) {
					page = 1;
				}

				int start = (page - 1) * numRecord;
				if (start >= totalReportList) {
					start = 0;
				}

				int totalPage = (int) (Math.ceil((double) totalReportList / (double) numRecord));

				if (page > totalPage) {
					page = 1;
				}
				
				reportList = serverruntimeReportDB.getServerReportListBySearch(reportType,reportStatus,reportServer,reportTime,reportRecord, start, numRecord);	
			
			}
			PagingBean pagingBean = PagingBean.createPagingBean(page, totalReportList, numRecord);

			model.addObject("reportType", reportType);
			model.addObject("reportStatus", reportStatus);
			model.addObject("reportServer", reportServer);
			model.addObject("totalReportList", totalReportList);
			model.addObject("reportList", reportList);
			model.addObject("pagingBean", pagingBean);
			model.addObject("reportTime", reportTime);
			model.addObject("reportRecord", reportRecord);


		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;

	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/serverReportAjax"})
	private @ResponseBody void getDataById(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") String id, @RequestParam("server") String server, @RequestParam("type") String type) throws IOException, ServletException, SQLException {
		try{
			JSONObject json = new JSONObject();
				try {
					MerchantRuntimeReportDB merchantruntimeReportDB = DatabaseManager.getDatabaseManager().getMerchantruntimeReportDB();
					int toltalReportList = 0;
					int searchType = 0;
					JSONArray jsonNodeList = null;
					JSONObject jsonObj = null;
					toltalReportList = merchantruntimeReportDB.countReportBySearch("", searchType, "", "","",id);
					if(toltalReportList > 0){ 
						List<MerchantRuntimeReportBean> reportList = merchantruntimeReportDB.getReportListBySearch("", searchType, "", "","",id, 0, toltalReportList);
						if(reportList != null && reportList.size() > 0){
							jsonNodeList = new JSONArray();	
							for (MerchantRuntimeReportBean rpBean : reportList) {
								jsonObj = new JSONObject();	
								jsonObj.put("merchantId", rpBean.getMerchantId());
								jsonObj.put("merchantName", rpBean.getName());
								jsonObj.put("Type", rpBean.getType());
								jsonObj.put("Package", rpBean.getPackageType());
								jsonObj.put("CountOld", rpBean.getCountOld());
								jsonObj.put("CountNew", rpBean.getCountNew());
								jsonObj.put("SuccessCount", rpBean.getSuccessCount());
								jsonObj.put("ErrorCount", rpBean.getErrorCount());
								jsonObj.put("Add", rpBean.getAdd());
								jsonObj.put("Update", rpBean.getUpdate());
								jsonObj.put("UpdatePicture", rpBean.getUpdatePic());
								jsonObj.put("Delete", rpBean.getDelete());
								jsonObj.put("Server", rpBean.getServer());
								jsonObj.put("StartDate", DateTimeUtil.generateStringDisplayDateTime(rpBean.getStartDate()));
								
								Date dateStart = rpBean.getStartDate();
								Date dateEnd = rpBean.getEndDate();
								
								if (dateEnd!=null) {
									jsonObj.put("EndDate", DateTimeUtil.generateStringDisplayDateTime(dateEnd));
									jsonObj.put("RunTime", DateTimeUtil.calcurateHourDiffFullFormat(dateStart, dateEnd));
								} else {
									jsonObj.put("EndDate", "-");
									jsonObj.put("RunTime", "-");
								}
								
								jsonObj.put("Status", rpBean.getStatus());
								jsonNodeList.add(jsonObj);
							}
						   }
					}
					
						json.put("totalList", toltalReportList);
						json.put("header", "success");
						json.put("result", jsonNodeList);
						json.put("showServer", server);
						json.put("showType", type);
						
				}catch(Exception e) {
					json.put("header", "error");
					e.printStackTrace();
				}
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}
