package botbackend.controller;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONObject;

import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;
import web.parser.image.DefaultImageParser;

@Controller
public class CheckImageController {
	
	final static String permissionName = "tool.checkimage";
	private static DefaultImageParser  imageParser =  new DefaultImageParser();
	
	@RequestMapping(value = {"/admin/checkImage"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		String url		= ServletUtil.getStringParam(request, "url", "");
		if(StringUtils.isBlank(url))
			return new ModelAndView("checkImagePage", "newPage", true);
		
		getImageData(url,request,response);
		
		
		return null;
	}
	
	private void getImageData(String url,HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject jsonObj = new JSONObject();
		try {
			byte[] imageData = imageParser.processImage(url, 200, 200);
			String bPic = new String(Base64.getEncoder().encode(imageData));
			jsonObj.put("data", bPic);
			jsonObj.put("header", "success");
		}catch (Exception e) {
			jsonObj.put("message", e+"");
			jsonObj.put("header", "false");
		}
		JsonWriter.getInstance().write(response,jsonObj.toString());
	
	}
}
