package botbackend.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;
import bean.ProductDataBean;
import utils.DateTimeUtil;

import botbackend.bean.BotConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.bean.ProductUrlBean;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.UserBean;
import botbackend.bean.WorkLoadBean;
import botbackend.db.BotConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.ProductDataDB;
import botbackend.db.ProductUrlDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.WorkLoadDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.system.BaseConfig;
import botbackend.utils.JsonWriter;
import botbackend.utils.ServletUtil;
import botbackend.utils.UserUtil;

@Controller
public class CheckApprovalController {

	final static String PAGE = "Check Approval";
	final static String permissionName = "merchant.merchantmanager.checkapproval";
	final static String dataUpdateState = "DATA_UPDATE_START";
	final static String startState = "CRAWLER_START";
	final static String startStatus = "WAITING";
	final static int LIMIT = 100;
	private static String BOT_SERVER_CURRENT_COUNTRY = BaseConfig.BOT_SERVER_CURRENT_COUNTRY;

	private static final String[] BOT_5_COUNTRY = new String[] {"th"};
	
	@RequestMapping(value = {"/admin/checkapproval"})
	private ModelAndView getMerchantData(
			@RequestParam(required=false, defaultValue="0",  value="merchantId") int merchantId,
			HttpServletRequest request, HttpServletResponse response) {
		
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("checkapproval", "newPage", true);
		try {
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean>  merchantData = merchantDB.getMerchantListByIdWithOwner(merchantId);
			if(merchantData == null){
				return null;
			}
			
			MerchantBean mBean = null;
			if(merchantData != null && merchantData.size() > 0){
				for(MerchantBean merchant : merchantData){
					if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURRENT_COUNTRY)) {
						// 5.0 no shard
						mBean = merchant; 
					}else {
						if(merchant.getActive() == 3 && (merchant.getState().equals("PARSER_FINISH") || merchant.getDataUpdateState().equals("DATA_UPDATE_FINISH"))){ 
							mBean = merchant; 
							break;
						}
					}
					if(mBean == null){ 
						mBean = merchant; 
					}
				}
			}
			
			String country = BOT_SERVER_CURRENT_COUNTRY.toLowerCase();
			if(country.equals("th")) country = "";
			
			String serverName = "Bot-verify"+country+"1";
			if(mBean.getNodeTest() != 0) {
				serverName = "Bot-verify"+ country + mBean.getNodeTest();
			}
			BotConfigDB botConfigDB = DatabaseManager.getDatabaseManager().getBotConfigDB();
			BotConfigBean  botConfigDBean = botConfigDB.findBotConfig(serverName);
			
			List<WorkLoadBean> workloadData = null;
			List<ProductUrlBean> productUrlData = null;
			List<ProductDataBean> productData = null;
			List<ProductUrlBean> totalProductUrl = null;
			MerchantRuntimeReportBean wceReport = null;
			MerchantRuntimeReportBean dueReport = null;
			
			int totalWorkload = 0;
			int totalProductData = 0;
			
			if(botConfigDBean == null || botConfigDBean.getUrl() == null){
				LogUtil.info(request, PAGE, "getMerchantData --> Do not have configuration in this server : "+ serverName);
			}else{
				WorkLoadDB workLoadDB = DatabaseManager.getDatabaseManager().getWorkLoadDB(serverName);
				workloadData = workLoadDB.selectWorkLoadById(merchantId, LIMIT);
				totalWorkload = workLoadDB.countWorkload(merchantId);
				
				ProductUrlDB productUrlDB = DatabaseManager.getDatabaseManager().getProductUrlDB(serverName);
				productUrlData = productUrlDB.getProductUrlByMerchantId(merchantId, LIMIT);
				totalProductUrl = productUrlDB.countProductUrlByState(merchantId);
				
				ProductDataDB productDataDB = DatabaseManager.getDatabaseManager().getProductDataDB(serverName);
				productData = productDataDB.getMerchantDataById(merchantId, LIMIT);
				totalProductData = productDataDB.countProduct(merchantId);
				
				MerchantRuntimeReportDB reportDB = DatabaseManager.getDatabaseManager().getMerchantruntimeReportDB();
				
				wceReport = reportDB.findReportByMerchantId(merchantId, "WCE_TEST", 1);
				dueReport = reportDB.findReportByMerchantId(merchantId, "DUE_TEST", 1);
			}
			
			model.addObject("merchantData", merchantData);
			model.addObject("workloadData", workloadData);
			model.addObject("workloadCount", totalWorkload);
			model.addObject("productUrlData", productUrlData);
			model.addObject("productUrlCountList", totalProductUrl);
			model.addObject("productData", productData);
			model.addObject("productCount", totalProductData);
			model.addObject("wceReport", wceReport);
			model.addObject("dueReport", dueReport);
			
			LogUtil.info(request, PAGE, "getMerchantData|merchantId:"+ merchantId);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value = "/admin/checkapprovalajax")
	public @ResponseBody void getMerchantAjax(
			@RequestParam(required=false, defaultValue="0",  value="merchantId") int merchantId,
			HttpServletRequest request, HttpServletResponse response) {
		
		try {
			HashMap<String, Object> m = new HashMap<String, Object>();
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean>  merchantData = merchantDB.getMerchantsByMerchantId(merchantId);
			if(merchantData == null){
				m.put("header", "fail");
				JsonWriter.getInstance().write(response, m);
				return;
			}
			
			UrlPatternDB urlPatternDB = DatabaseManager.getDatabaseManager().getUrlPatternDB();
			List<UrlPatternBean> urlPatternBeanList = urlPatternDB.getUrlPatternByMerchantId(merchantId);
			
			m.put("merchantData", merchantData);
			m.put("patternData", urlPatternBeanList);
			m.put("header", "success");
			
			LogUtil.info(request, PAGE, "getMerchantAjax|merchantId " + merchantId);
			JsonWriter.getInstance().write(response, m);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/admin/updatemerchant")
	public @ResponseBody void updateMerchant(
			@RequestParam(required=false, defaultValue="0",  value="merchantId") int merchantId,
			@RequestParam(required=false, defaultValue="",  value="data") String _data,
			HttpServletRequest request, HttpServletResponse response) {
			
		try {
			HashMap<String, Object> m = new HashMap<String, Object>();
			MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean>  mList = merchantDB.getMerchantsByMerchantId(merchantId);
			List<MerchantBean> 	updatedList = new ArrayList<MerchantBean>();
			Map<Integer,MerchantBean> merchants = new HashMap<Integer,MerchantBean>();
			
			HttpSession session = request.getSession(false);
			UserBean userBean = (UserBean)session.getAttribute("userData");
			
			for(MerchantBean merchant :mList){
				merchants.put(merchant.getShardNo(), merchant);
			}

			JSONArray data = new JSONArray(_data);
			for (int i=0; i < data.length(); i++) {
				
				JSONObject detail = data.getJSONObject(i);
				int shardNo = Integer.parseInt((String) detail.get("shardNo"));
				int active = Integer.parseInt((String) detail.get("active"));
				String server = (String) detail.get("server");
				String dataServer = (String) detail.get("dataServer");
				String errorMessage = (String) detail.get("errorMessage");
				
				MerchantBean  merchantData = merchants.get(shardNo);
				if(merchantData == null){
					continue;
				}
				merchantData.setCurrentserver(merchantData.getServer());
				merchantData.setActive(active);
				merchantData.setServer(server);
				merchantData.setDataServer(dataServer);
				
				if(Arrays.asList(BOT_5_COUNTRY).contains(BOT_SERVER_CURRENT_COUNTRY)) {
					merchantData.setState("BOT_FINISH");
					merchantData.setStatus("FINISH");
					merchantData.setDataUpdateState("BOT_FINISH");
					merchantData.setDataUpdateStatus("FINISH");
					merchantData.setWceNextStart(new Date());
					merchantData.setDueNextStart(new Date());
				} else {
					merchantData.setState("CRAWLER_START");
					merchantData.setStatus("WAITING");
					merchantData.setDataUpdateState("DATA_UPDATE_START");
					merchantData.setDataUpdateStatus("WAITING");
					merchantData.setWceNextStart(null);
					merchantData.setDueNextStart(null);
				}
				
				if(active == 1){
					merchantData.setErrorMessage(null);
				}else{
					merchantData.setErrorMessage(errorMessage);
				}
				updatedList.add(merchantData);
			}

			if(updatedList.size() > 0){
				Object[][] param = getObjectParamList(updatedList, userBean);
				merchantDB.updateNewMerchants(param);
			}

			m.put("header", "success");
			m.put("count", updatedList.size());
			
			LogUtil.info(request, PAGE, "updatemerchant|count:"+updatedList.size()+"|merchantId " + merchantId);
			JsonWriter.getInstance().write(response, m);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private Object[][] getObjectParamList(List<MerchantBean> updatedList,UserBean userBean){
		if(updatedList == null || updatedList.size() <= 0){
			return null;
		}
		Object[][] paramList = new Object[updatedList.size()][14];
		int count = 0;
		for (MerchantBean merchantBean : updatedList) {
			Object[] param = new Object[] {
					merchantBean.getName(),
					merchantBean.getActive(),
					merchantBean.getPackageType(),
					(StringUtils.isBlank(merchantBean.getServer()))?"":merchantBean.getServer(),
					merchantBean.getDataServer(),
					merchantBean.getState(),
					merchantBean.getStatus(),
					merchantBean.getDataUpdateState(),
					merchantBean.getDataUpdateStatus(),
					merchantBean.getErrorMessage(),
					userBean.getUserName(),
					DateTimeUtil.generateStringDateTime(new Date()),
					merchantBean.getId(),
					(StringUtils.isBlank(merchantBean.getCurrentserver()))?"":merchantBean.getCurrentserver()
			};
			paramList[count] = param;
			count++;
		}
		return paramList;
	}
}
