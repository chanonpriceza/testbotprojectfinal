package botbackend.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bean.NotificationBean;
import db.NotificationDB;
import utils.BotUtil;

import botbackend.bean.UserBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.AjaxUtil;

@Controller
public class NotificationController {
	private static final int numRecordPage = 40;
	private static final int numRecord = 8;
	private static final int start = 0;
	
	@RequestMapping(value = {"/admin/notificationpage"})
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return getNotificationData(1,request, response);
	}
	
	@RequestMapping(value = {"/admin/notificationpagesearch"})
	public ModelAndView getNotificationData(@RequestParam("page") int page, HttpServletRequest request, HttpServletResponse response) throws SQLException {
		ModelAndView model = new ModelAndView("notificationpage", "newPage", true);
		HttpSession session = request.getSession(true);
		UserBean userLoginBean = (UserBean) session.getAttribute("userData");
		NotificationDB notificationDB = DatabaseManager.getDatabaseManager().getNotificationDB();
		List<NotificationBean> notificationList = new ArrayList<NotificationBean>();
		int totalnotification = 0;
		PagingBean pagingBean = null;
		
		if(page < 1) page = 1;
		if(notificationList != null && notificationList.size() > 0){
			totalnotification = notificationList.size();
		}else{
			 totalnotification  = notificationDB.countAllNotification(userLoginBean.getUserId(),start,numRecordPage);
		}
		
		if(totalnotification > 0){
			int start = (page - 1) * numRecordPage;
			if(start >= totalnotification) {
				start = 0;
			}
			
			int totalPage = (int)(Math.ceil((double)totalnotification/(double)numRecordPage));
			
			if(page > totalPage) {
				page = 1;
			}
			
			if(notificationList.isEmpty()){
				notificationList = notificationDB.getAllNotificationByUserID(userLoginBean.getUserId(), start, numRecordPage);
			}
	
			pagingBean = PagingBean.createPagingBean(page, totalnotification, numRecordPage);

			model.addObject("notificationList", notificationList);
			model.addObject("pagingBean", pagingBean);
		}
		model.addObject("totalnotification", totalnotification);
		
		return model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/admin/changeStatusNotification"})
	public @ResponseBody void changeStatusNotification(HttpServletRequest request, HttpServletResponse response, @RequestParam("notiID") String notiID, @RequestParam("action") String action) throws IOException, ServletException, SQLException {
		String header = "error";	
		JSONObject json = new JSONObject();
		final String cookieName = "checkCookieNoti";
		final int maxAge = 60*5;
		String path = "/botbackend";
		String cookieValue = "";
		int countUnread = 0;
		try{
			HttpSession session = request.getSession(true);
			UserBean userLoginBean = (UserBean) session.getAttribute("userData");
			NotificationDB notificationDB = DatabaseManager.getDatabaseManager().getNotificationDB();
			int success=0;
			if(action.equals("READ")){
				success = notificationDB.updateStatusNotification(notiID,"READ");				
			}else if(action.equals("READALL")){
				success = notificationDB.updateStatusAllNotification(userLoginBean.getUserId());
			}
			
			if(success > 0){
				header = "success";	
				List<NotificationBean> notificationBean = null;
			 	try {
			 		if(!BotUtil.LOCALE.toLowerCase().equals("th")){
						path += BotUtil.LOCALE.toLowerCase();
					}
			 		NotificationDB notiDB = DatabaseManager.getDatabaseManager().getNotificationDB();
					notificationBean = notiDB.getAllNotificationByUserID(userLoginBean.getUserId(),start,numRecord);
					if(notificationBean != null && notificationBean.size() > 0){
						countUnread = 0;
						for (NotificationBean notiBean : notificationBean) {
							cookieValue += notiBean.getId()+"_"+notiBean.getMessage()+"_"+notiBean.getStatus()+"_"+notiBean.getCreateDate()+"|";
							if(notiBean.getStatus().equals("UNREAD")){
								countUnread++;
							}
						}
						cookieValue = URLEncoder.encode(cookieValue,"UTF-8");
					}
					cookieValue = "&" + countUnread +"|"+ cookieValue;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 Cookie ck = new Cookie(cookieName, cookieValue);
				 ck.setMaxAge(maxAge);
				 ck.setPath(path);
				 response.addCookie(ck);
			}
			json.put("countUnread", countUnread);
			json.put("header", header);
			AjaxUtil.sendJSON(response, json);
		}catch(Exception e){
			e.printStackTrace();
		}
	} 
}
