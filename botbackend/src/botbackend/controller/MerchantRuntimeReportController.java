package botbackend.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.UserUtil;


@Controller
public class MerchantRuntimeReportController {
	
	private static final String permissionName = "report.merchant";
	
	private static final int numRecord = 20;
	private static final String DEFAULT_REPORT_REFERENCE = "";
	
	@RequestMapping(value = {"/admin/merchantruntimereport"})
	public ModelAndView  getMonitorData(
			@RequestParam(required=false, defaultValue="1", value="page") int page,
			@RequestParam(required=false, defaultValue="0", value="searchType") int searchType,
			@RequestParam(required=false, defaultValue="",  value="searchParam") String searchParam,
			@RequestParam(required=false, defaultValue="",  value="reportStatus") String reportStatus,
			@RequestParam(required=false, defaultValue="ALL",  value="reportType") String reportType,
			@RequestParam(required=false, defaultValue="",  value="reportTime") String reportTime,
			@RequestParam(required=false, defaultValue="",  value="cmd") String cmd,
			HttpServletRequest request, HttpServletResponse response) {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("merchantruntimereport", "newPage", true);
		try{
			List<MerchantRuntimeReportBean> reportList =  new ArrayList<MerchantRuntimeReportBean>();
			MerchantRuntimeReportDB merchantruntimereportDB = DatabaseManager.getDatabaseManager().getMerchantruntimeReportDB();

			int totalRepotList = merchantruntimereportDB.countReportBySearch(reportType, searchType, searchParam, reportStatus,reportTime,DEFAULT_REPORT_REFERENCE);
			
			if(totalRepotList > 0){
				if (page < 1) {
					page = 1;
				}

				int start = (page - 1) * numRecord;
				if (start >= totalRepotList) {
					start = 0;
				}

				int totalPage = (int) (Math.ceil((double) totalRepotList / (double) numRecord));

				if (page > totalPage) {
					page = 1;
				}

				reportList = merchantruntimereportDB.getReportListBySearch(reportType, searchType, searchParam, reportStatus,"",reportTime, start, numRecord);
			}
	
			PagingBean pagingBean = PagingBean.createPagingBean(page, totalRepotList, numRecord);
			
			model.addObject("searchParam", searchParam);
			model.addObject("searchType", searchType);
			model.addObject("reportType", reportType);
			model.addObject("reportStatus", reportStatus);
			model.addObject("totalRepotList", totalRepotList);
			model.addObject("reportTime", reportTime);
			model.addObject("reportList", reportList);
			model.addObject("pagingBean", pagingBean);


		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;

	}

}
