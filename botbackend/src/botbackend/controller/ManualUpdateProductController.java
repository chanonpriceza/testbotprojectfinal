package botbackend.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import bean.SendDataBean;
import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.MerchantConfigBean.FIELD;
import botbackend.component.MerchantConfigComponent;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.ProductDataDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.AjaxUtil;
import botbackend.utils.JsonWriter;
import botbackend.utils.ParserUtil;
import botbackend.utils.UserUtil;
import botbackend.utils.Util;

@Controller
public class ManualUpdateProductController {

	final static String PAGE = "Manual Update Product";
	final static String permissionName = "product.manualupdateproduct";
	final static String DEFAULT_CHARSET = "UTF-8";
	final static int DEFAULT_IMAGE_WIDTH = 200;
	final static int DEFAULT_IMAGE_HEIGHT = 200;
	
	@RequestMapping(value = "/admin/manualupdateproduct")
	public ModelAndView getModelAndView(HttpServletRequest request) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return new ModelAndView("manualupdateproduct", "", true);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/updateproduct-searchmerchant")
	public @ResponseBody void searchMerchant(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId){
		merchantId = convertMerchant(merchantId);
		try {
			JSONObject json = new JSONObject();
			
			MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
			
			MerchantBean mBean = null;
			if(mList != null && mList.size() > 0){
				for(MerchantBean merchant : mList){
					if(merchant.getActive() == 1){ mBean = merchant; break;}
					if(mBean == null){ mBean = merchant; }
				}
			}
			
			if(mBean == null){
				json.put("header", "error-not-found-merchant");
			}else{
				ObjectMapper mapper = new ObjectMapper();
				JsonNode mNode = mapper.valueToTree(mBean);
				json.put("merchant", mNode);
				
				MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
				MerchantConfigBean mcBean = mcDB.findMerchantConfigByIdAndField(merchantId, FIELD.parserClass.toString());
				if(mcBean == null){
					ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
					List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
					if(pList.size() <= 0) {
						json.put("header", "error-not-found-merchant-parser");
					} else {
						json.put("parserClass", "User Config");
						json.put("header", "success");
					}
				}else{
					if(mcBean.getValue().equals("com.ha.bot.parser.DefaultFeedHTMLParser")){
						ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
						List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
						if(pList.size() <= 0) {
							json.put("header", "error-feed-without-parser-config");
						} else {
							json.put("parserClass", "Parser Config");
							json.put("header", "success");
						}
					}else{
						json.put("parserClass", mcBean.getValue());
						json.put("header", "success");
					}
				}
				
			}
			
			AjaxUtil.sendJSON(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LogUtil.info(request, PAGE, "searchMerchant|MerchantId:" + merchantId);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/updateproduct-confirm-result")
	public @ResponseBody void confirmUpdateProduct(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, @RequestParam("data")  String data)throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();
		merchantId = convertMerchant(merchantId);
		try{

			JSONObject dynamicNote = new JSONObject();
			dynamicNote.put("method", "manual-update-botbackend");
			
			MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();
			ConfigDB cDB = DatabaseManager.getDatabaseManager().getConfigDB();

			List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
			ConfigBean cBean = cDB.getByName("manual.updateproduct.senddata.default.server");
			
			Properties mConfig = MerchantConfigComponent.loadMerchantConfig(merchantId);
			
			MerchantBean mBean = null;
			if(mList != null && mList.size() > 0){
				for(MerchantBean merchant : mList){
					if(merchant.getActive() == 1){ mBean = merchant; break;}
					if(mBean == null){ mBean = merchant; }
				}
			}
			
			String dataServer = mBean.getDataServer();
			String sendServer = cBean.getConfigValue();
			
			ProductDataDB pdDB = DatabaseManager.getDatabaseManager().getProductDataDB(dataServer);
			SendDataDB sdDB = DatabaseManager.getDatabaseManager().getSendDataDB(sendServer);
			
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(data);
			List<ProductDataBean> updateList  = new ArrayList<ProductDataBean>();
			String picData = "";
			boolean isUpdatePic = false;
			for(Object newData : jsonArray){
				JSONObject jData = (JSONObject) newData;
				String productId = (String) jData.get("id");
				List<ProductDataBean> tmpList  = new ArrayList<ProductDataBean>();
				tmpList = pdDB.getProductDataById(merchantId, productId); 
				if(tmpList == null || tmpList.size() == 0){
					continue;
				}
				ProductDataBean tmpData = tmpList.get(0);
				if(!tmpData.getName().trim().equals(((String) jData.get("name")).trim())){
					continue;
				}
				for (Object key : jData.keySet()){
					String keyStr = (String)key;
					String value = (String) jData.get(keyStr);
					if(keyStr.equals("price")){
						try{
							double p = Double.parseDouble(value);
							if(p == 0.0) continue;
							tmpData.setPrice(p);
						}catch(Exception e){}
					}else if(keyStr.equals("basePrice")){
						try{
							double p = Double.parseDouble(value);
							tmpData.setBasePrice(p);
						}catch(Exception e){}
						
					}else if(keyStr.equals("description")){
						tmpData.setDescription(value);
					}else if(keyStr.equals("pictureUrl")){
						picData = ParserUtil.getPictureData(value, tmpData.getUrl(), mConfig);
						if(StringUtils.isNotBlank(picData)){
							tmpData.setPictureUrl(value);
							tmpData.setPictureData(picData);
							isUpdatePic = true;
						}
					}
					
				}
				if(tmpData != null){
					updateList.add(tmpData);
				}
			}
			
			int insertSendDataCount = 0;
			if(updateList != null && updateList.size() > 0){
				for (ProductDataBean pdBean : updateList) {
					try{

						byte[] imageByte = ParserUtil.parseImage(null, pdBean.getPictureUrl(), DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
						
						pdBean.setDynamicField(dynamicNote.toJSONString());
						SendDataBean sdBean = null;
						if(isUpdatePic){
							sdBean = SendDataBean.createSendDataBean(pdBean, imageByte, SendDataBean.ACTION.UPDATE_PIC);
						}else{
							sdBean = SendDataBean.createSendDataBean(pdBean, imageByte, SendDataBean.ACTION.UPDATE);
						}
						
						if (StringUtils.isBlank(pdBean.getName()) || (pdBean.getPrice() <= 0)) {
							continue;
						}
						
						int insertProductCount  = pdDB.updateProductData(pdBean);
						if(insertProductCount > 0){
							insertSendDataCount += sdDB.insertSendData(sdBean);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			if(updateList.size() != insertSendDataCount){
				m.put("header", "error-update-statement");
				m.put("errorDetail", (updateList.size() - insertSendDataCount) + " products was not update.");
			}else{
				m.put("header","success");
				m.put("resultcount",updateList.size());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			m.put("header", "error");
			m.put("errorDetail", e.getMessage());
		}
		
		LogUtil.info(request, PAGE, "updateProduct|MerchantId:" + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/admin/updateproduct-get-parse-result")
	public @ResponseBody void compareProduct(HttpServletRequest request, HttpServletResponse response, @RequestParam("merchantId") int merchantId, String[] urlList, @RequestParam("parserClass")  String parserClass) throws SQLException, IOException{
		HashMap<String, Object> m = new HashMap<String, Object>();
		List<ProductDataBean> oldList = new ArrayList<ProductDataBean>();
		List<ProductDataBean> _oldList = new ArrayList<ProductDataBean>();
		List<ProductDataBean> newList = new ArrayList<ProductDataBean>();
		
		ParserConfigDB pDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
		merchantId = convertMerchant(merchantId);
		MerchantDB mDB = DatabaseManager.getDatabaseManager().getMerchantDB();

		List<MerchantBean> mList = mDB.getMerchantsByMerchantId(merchantId);
		
		MerchantBean mBean = null;
		if(mList != null && mList.size() > 0){
			for(MerchantBean merchant : mList){
				if(merchant.getActive() == 1){ mBean = merchant; break;}
				if(mBean == null){ mBean = merchant; }
			}
		}
		
		String dataServer = mBean.getDataServer();
		
		if(StringUtils.isNotBlank(dataServer)){
			ProductDataDB pdDB = DatabaseManager.getDatabaseManager().getProductDataDB(dataServer);
			for(String url : urlList){
				List<ProductDataBean> tmpList  = new ArrayList<ProductDataBean>();	
				url = url.trim();
				String[] urls = new String[]{url,URLDecoder.decode(url)};
				tmpList = pdDB.getProductDataByURL(merchantId, urls);
				if(tmpList != null && tmpList.size() > 0){
					_oldList.addAll(tmpList);
				}
			}
			String picData = "";
	
			try{
				for(ProductDataBean pBean : _oldList ){
					String[] urls = new String[] {pBean.getUrl()};
					List<ProductDataBean> tmpList  = new ArrayList<ProductDataBean>();
					Properties mConfig = MerchantConfigComponent.loadMerchantConfig(merchantId);
					
						
					String dynamic = pBean.getDynamicField();
					if( StringUtils.isBlank(pBean.getDynamicField()) || !(dynamic.contains("manual-add-botbackend") || dynamic.contains("manual-update-botbackend"))){
						m.put("errorDetail", "This product can't update by manual");
						continue;
					}

						
					List<ParserConfigBean> pList = pDB.getByMerchantId(merchantId);
					if("User Config".contentEquals(parserClass)) {
						parserClass = Util.DEFAULT_PARSER_CLASS;
					}
					tmpList = ParserUtil.parse(parserClass, pList,urls);
					picData = ParserUtil.getPictureData(pBean.getPictureUrl(), pBean.getUrl(), mConfig);
					
					pBean.setPictureData(picData);
					if(tmpList != null && tmpList.size() > 0){
						newList.addAll(tmpList);
						oldList.add(pBean);
					}
				}
				if(newList != null && newList.size() > 0){
					m.put("oldList", oldList);
					m.put("newList", newList);
					m.put("header", "success");
				}else if(m.get("errorDetail") != null){
					m.put("header", "error");
				}else{
					m.put("header", "error");
					m.put("errorDetail", "Can't find product by this url, please check the url and try again");
				}
			}catch(Exception e){
				e.printStackTrace();
				m.put("header", "error");
				m.put("errorDetail", "Error : " + e.getMessage());
			}
		
		}else{
			m.put("header", "error");
			m.put("errorDetail", "This merchant doesn't have dataServer");
		}
		LogUtil.info(request, PAGE, "compareProduct|MerchantId " + merchantId);
		JsonWriter.getInstance().write(response, m);
	}
	
	private int convertMerchant(int merchantId){
		ConfigDB cDB = DatabaseManager.getDatabaseManager().getConfigDB();
		try {
			ConfigBean cBean = cDB.getByName("manual.updateproduct.convert.merchant");
			if(cBean == null){
				return merchantId;
			}
			String value = cBean.getConfigValue();
			if(StringUtils.isBlank(value)){
				return merchantId;
			}
			String[] merchants = value.split("\\|");
			for(String merchant : merchants){
				String[] mc  = merchant.split(",");
				if(Integer.parseInt(mc[0]) == merchantId){
					merchantId = Integer.parseInt(mc[1]);
					break;
				}
			}
		} catch (SQLException e) {
			
		}
		return merchantId;
	}
}
