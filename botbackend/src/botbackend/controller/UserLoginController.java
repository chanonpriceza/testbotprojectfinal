package botbackend.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

import botbackend.bean.UserBean;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.LogUtil;
import botbackend.utils.DataEncription;
import botbackend.utils.UserUtil;

@Controller
public class UserLoginController {
	final static String PAGE = "Login";
	private static final String SECRET = "PRICEZABOT";
	private static int USER_SESSION_TIME = 0;
	private static int SERVICE_SESSTION_TIME = 1*60*60*1000;
	
	@RequestMapping(value = "/userlogin")
	public String userLogin(HttpServletRequest request, HttpServletResponse response, UserBean userBean) throws IOException, ServletException {
		
		String cmd = userBean.getCmd();
		if(cmd != null && cmd.equals("getLogout")){
			HttpSession session = request.getSession(false);
			if(session != null){
				session.invalidate();
			}
			return "redirect:/userlogin";
		}
		
		if(UserUtil.checkUserSession(request, response)){
			return "redirect:/admin/dashboard";
		}else if (cmd != null  && cmd.equals("getLogin")){
			String userName = userBean.getUserName();
			String userPass = userBean.getUserPass();
			
			UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
			UserBean user = null;
			
			try {
				user = (UserBean)userDB.getUserLogin(userName, DataEncription.toMD5(userPass));
			} catch (Exception e) {
				LogUtil.info(request, PAGE,e.getMessage());
				request.setAttribute("errormessage","system exception");
				return "forward:/WEB-INF/jsp/loginpage.jsp";
			}
			
			if (user == null || user.getUserStatus() != 1){
				request.setAttribute("errormessage","invalid username or password");
				return "forward:/WEB-INF/jsp/loginpage.jsp";
			}
			
			if(user.getUserPermission() != null) {
				String[] permission = user.getUserPermission().split("\\|");
				List<String> permits = new ArrayList<String>();
				try {
					permits = userDB.getRolePermission(permission);
				} catch (SQLException e) {
					LogUtil.info(request, PAGE,e.getMessage());
					request.setAttribute("errormessage","system exception");
					return "forward:/WEB-INF/jsp/loginpage.jsp";
				}
				
				HashMap<String, String> merchantPermission = new HashMap<String, String>();	
				for (String per : permits) {
					if(per.trim().length() != 0) {
						if(!merchantPermission.containsKey(per)) {
							merchantPermission.put(per,"");
						}
					}
				}
				
				if(request.getSession(false)==null) {
					request.getSession(true).setAttribute("Permission", merchantPermission);	
				}else {
					request.getSession(false).setAttribute("Permission", merchantPermission);
				}
			}
			createTokenSession(request,response,userName,userPass,USER_SESSION_TIME);
			if(request.getSession(false)==null) {
				request.getSession(true).setAttribute("userData", user);
			}else {
				request.getSession(false).setAttribute("userData", user);
			}
			LogUtil.info(request, PAGE, "Login success");
			return "redirect:/admin/dashboard";
		}else{
			return "forward:/WEB-INF/jsp/loginpage.jsp";
		}
	}
	
	@RequestMapping(value = "/service_login", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> serviceLogin(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		Map<String, Object> rtn = new HashMap<>();
		
		if(UserUtil.checkUserSession(request, response)){
			rtn.put("success", "true");
			rtn.put("message", "Already Login.");
			LogUtil.info(request, PAGE, "Already Login : ");
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
		}
		
		try {

			Map<String, Object> requestMap = new HashMap<>();
			Map<String, String[]> rawMap = request.getParameterMap();
			for(Entry<String, String[]> ent : rawMap.entrySet()) {
				if(ent.getValue().length > 0) {
					requestMap.put(ent.getKey(), ent.getValue()[0]);
				}
			}
			
			String userName = (String) requestMap.get("userName");
			String userPass = (String) requestMap.get("userPass");
			
			UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
			UserBean user = (UserBean)userDB.getUserLogin(userName, DataEncription.toMD5(userPass));
			
			if (user == null || user.getUserStatus() != 1) {
				rtn.put("success", "false");
				rtn.put("message", "User not found");
				LogUtil.info(request, PAGE, "User not found : " + userName);
				return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
			}
		
			if(user.getUserPermission() != null) {
				String[] permission = user.getUserPermission().split("\\|");
				List<String> permits = userDB.getRolePermission(permission);
				
				HashMap<String, String> userPermission = new HashMap<String, String>();	
				for (String per : permits) {
					if(per.trim().length() != 0) {
						if(!userPermission.containsKey(per)) {
							userPermission.put(per,"");
						}
					}
				}
				
				if(request.getSession(false)==null) {
					request.getSession(true).setAttribute("Permission", userPermission);	
				}else {
					request.getSession(false).setAttribute("Permission", userPermission);
				}
			}
			
			createTokenSession(request,response,userName,userPass,SERVICE_SESSTION_TIME);
			if(request.getSession(false)==null) {
				request.getSession(true).setAttribute("userData", user);
			}else {
				request.getSession(false).setAttribute("userData", user);
			}
			
			LogUtil.info(request, PAGE, "Login success");
			rtn.put("success", "true");
			rtn.put("message", "Generated jwt in response cookie.");
			LogUtil.info(request, PAGE, "Service login : " + userName);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK); //200
			
		} catch(Exception e) {
			rtn.put("success", "false");
			rtn.put("message", "User not found");
			LogUtil.info(request, PAGE, "Service login failed : " + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.BAD_REQUEST); //400
		}
		
	}
	
	private void createTokenSession(HttpServletRequest request,HttpServletResponse response,String username,String pass,int lifeTime) throws IllegalArgumentException, JWTCreationException, UnsupportedEncodingException {
		
		Builder jwt = JWT.create();
		jwt.withIssuer("auth0");
		jwt.withClaim("username",username);
		jwt.withClaim("password",DataEncription.toMD5(pass));
		if(lifeTime > 0) jwt.withExpiresAt(new Date(System.currentTimeMillis() + lifeTime));
		
		String token = jwt.sign(Algorithm.HMAC384(SECRET));
		Cookie bot_cookie = new Cookie("bot_token",token);
		if(lifeTime > 0) bot_cookie.setMaxAge(lifeTime);
		
		response.addCookie(bot_cookie);
	}

}
