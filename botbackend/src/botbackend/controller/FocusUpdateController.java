package botbackend.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import bean.ProductDataBean;

import botbackend.bean.FocusUpdateBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.UserBean;
import botbackend.db.FocusUpdateDB;
import botbackend.db.MerchantDB;
import botbackend.db.ProductDataDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.web.WebProductDataDB;
import botbackend.utils.AjaxUtil;

@Controller
public class FocusUpdateController {
	
	private static final int LIMIT_COUNT = 50;
	
	@RequestMapping(value = {"/admin/focusupdate"})
	public ModelAndView getAllFocusUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView("focusupdate", "newPage", true);
		try{
			FocusUpdateDB fuDB = DatabaseManager.getDatabaseManager().getFocusUpdateDB();
			List<FocusUpdateBean> fuBeanList = fuDB.getCurrentList(1);
			
			model.addObject("focusUpdateList", fuBeanList);
			model.addObject("focusUpdateSize", fuBeanList.size());
			model.addObject("limitCount", LIMIT_COUNT);
		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/focusupdate-saveitem")
	@ResponseBody
	public void addItem(@RequestBody Map<String, Object> json, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			JSONObject resJson = new JSONObject();
			String inputWebProductId = (String) json.get("webProductId");
			if(StringUtils.isNotBlank(inputWebProductId) && StringUtils.isNumeric(inputWebProductId)) {
				try {
					int webProductId = Integer.parseInt(inputWebProductId);
					
					FocusUpdateDB fuDB = DatabaseManager.getDatabaseManager().getFocusUpdateDB();
					FocusUpdateBean checkBean = fuDB.getByWebProductId(webProductId, 1);
					if(checkBean != null) {
						resJson.put("header", "duplicate-active-web-product-id");
					}else {
						WebProductDataDB webProductDataDB = DatabaseManager.getDatabaseManager().getWebProductDataDB();
						botbackend.bean.web.ProductDataBean pWebBean = webProductDataDB.getById(webProductId);
						if(pWebBean != null) {
							String targetServer = null;
							ProductDataBean pBotBean = null;
							MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
							List<MerchantBean> merchantList =  merchantDB.getMerchantsByMerchantId(pWebBean.getMerchant_id());
							if(merchantList != null && merchantList.size() > 0) {
								for (MerchantBean mBean : merchantList) {
									if(StringUtils.isBlank(mBean.getDataServer())) continue;
									ProductDataDB pDB = DatabaseManager.getDatabaseManager().getProductDataDB(mBean.getDataServer());
									pBotBean = pDB.getSpecificProduct(pWebBean.getName(), pWebBean.getMerchant_id());
									if(pBotBean != null) {
										targetServer = mBean.getDataServer();
										break;
									}
								}
							}else {
								resJson.put("header", "not-found-bot-merchant");
							}
							
							if(pBotBean != null && StringUtils.isNotBlank(targetServer)) {
								
								HttpSession session = request.getSession(false);
								UserBean userBean = (UserBean)session.getAttribute("userData");
								
								FocusUpdateBean fuBean = new FocusUpdateBean();
								fuBean.setWebProductId(pWebBean.getId());
								fuBean.setWebProductName(pWebBean.getName());
								fuBean.setMerchantId(pWebBean.getMerchant_id());
								fuBean.setBotProductId(pBotBean.getId());
								fuBean.setBotServerName(targetServer);
								fuBean.setAddBy(userBean.getUserName());
								fuBean.setAddDate(new Date());
								fuBean.setActive(1);
								
								int successCount = fuDB.saveNewItem(fuBean);
								if(successCount > 0) {
									ObjectMapper mapper = new ObjectMapper();
									JsonNode fuNode = mapper.valueToTree(fuBean);
									resJson.put("header", "success");
									resJson.put("result", fuNode);
								}
							}else {
								resJson.put("header", "not-found-bot-product-all-server");
							}
						}else {
							resJson.put("header", "not-found-web-product");
						}
					}
				}catch(NumberFormatException e) {
					e.printStackTrace();
					resJson.put("header", "error-productid-out-of-range");
				}
			}else {
				resJson.put("header", "error-invalid-input");
			}
			
			AjaxUtil.sendJSON(response, resJson);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/focusupdate-deleteitem")
	@ResponseBody
	public void deleteItem(@RequestBody Map<String, Object> json, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			JSONObject resJson = new JSONObject();
			String inputWebProductId = (String) json.get("webProductId");
			String inputBotProductId = (String) json.get("botProductId");
			
			if(StringUtils.isNotBlank(inputWebProductId) && StringUtils.isNumeric(inputWebProductId)
			&& StringUtils.isNotBlank(inputBotProductId) && StringUtils.isNumeric(inputBotProductId)) {
				
				int webProductId = Integer.parseInt(inputWebProductId);
				int botProductId = Integer.parseInt(inputBotProductId);
				
				FocusUpdateDB fuDB = DatabaseManager.getDatabaseManager().getFocusUpdateDB();
				int deleteCount = fuDB.deleteItem(webProductId, botProductId);
				
				if(deleteCount > 0) {
					resJson.put("header", "success");
				}else {
					resJson.put("header", "delete-error");
				}
			}
			AjaxUtil.sendJSON(response, resJson);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
