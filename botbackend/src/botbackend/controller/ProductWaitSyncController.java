package botbackend.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import botbackend.bean.ProductWaitSyncBean;
import botbackend.bean.common.PagingBean;
import botbackend.db.ProductWaitSyncDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.utils.UserUtil;

@Controller
public class ProductWaitSyncController {
	
	final static String permissionName = "report.productwaitsync";
	
	@RequestMapping(value = "/admin/productwaitsync")
	public ModelAndView getModalAndView(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		return getProductWaitSyncData(request, 1, "all");
	}

	@RequestMapping(value = {"/admin/productwaitsyncsearch"})
	public ModelAndView  getProductWaitSyncData(HttpServletRequest request, @RequestParam("page") int page, @RequestParam("viewBy") String viewBy) {
		if(!UserUtil.checkPermission(request, new String[] {permissionName}))
			return new ModelAndView("redirect:/admin/dashboard");
		
		ModelAndView model = new ModelAndView("productwaitsync", "newPage", true);
		try{
			ProductWaitSyncDB pwsDB = DatabaseManager.getDatabaseManager().getProductWaitSyncDB();
			Map<String, ProductWaitSyncBean> m = new HashMap<String, ProductWaitSyncBean>();
			List<ProductWaitSyncBean> productWaitSyncList = new ArrayList<ProductWaitSyncBean>();
			
			int numRecord = 20;
			int totalRecord = 0;
			
			if (viewBy.equals("date")) {
				totalRecord = pwsDB.countIdGroupByDate();
			} else {
				totalRecord = pwsDB.countAllDate();
			}
			
			if(totalRecord > 0){
				if (page < 1) {
					page = 1;
				}

				int start = (page - 1) * numRecord;
				if (start >= totalRecord) {
					start = 0;
				}

				int totalPage = (int) (Math.ceil((double) totalRecord / (double) numRecord));

				if (page > totalPage) {
					page = 1;
				}
				
				if (viewBy!=null && viewBy.equals("date")) {
					productWaitSyncList = pwsDB.getDateGroupByDate(start, numRecord);
					Date firstDate = productWaitSyncList.get(productWaitSyncList.size()-1).getAddDate();
					Date lastDate = productWaitSyncList.get(0).getAddDate();
					
					productWaitSyncList = pwsDB.getDataGroupByDate(firstDate, lastDate);
					
					for (ProductWaitSyncBean bean: productWaitSyncList) {
						if (m.get(bean.getAddDate()+bean.getDataServer()) == null) {
							m.put(bean.getAddDate().toString()+" "+bean.getDataServer(), bean);
						} else {
							ProductWaitSyncBean pb = m.get(bean.getAddDate().toString()+bean.getDataServer());
							if (pb.getAllCount()<bean.getAllCount()) {
								pb.setAllCount(bean.getAllCount());
							}
							pb.setDone(pb.getDone()+bean.getDone());
							m.replace(bean.getAddDate().toString()+bean.getDataServer(), pb);
							m.put(bean.getAddDate().toString()+bean.getDataServer(), pb);
						}
					}
					
					Object[] keys = new TreeSet(m.keySet()).toArray();
					Map<String, ProductWaitSyncBean> mSorted = new LinkedHashMap<String, ProductWaitSyncBean>();
					
					
					for (int count=keys.length-1; count>=0; count--) {
						mSorted.put(keys[count].toString(), m.get(keys[count]));
					}

					model.addObject("selected", "date");
					model.addObject("productWaitSyncList", new ArrayList<ProductWaitSyncBean>(mSorted.values()));
				} else {
					productWaitSyncList = pwsDB.getReportListBySearch(start, numRecord);
					
					model.addObject("selected", "all");
					model.addObject("productWaitSyncList", productWaitSyncList);
				}
			}
			
			PagingBean pagingBean = PagingBean.createPagingBean(page, totalRecord, numRecord);
			model.addObject("pagingBean", pagingBean);
			
		}catch (Exception e){
			e.printStackTrace();	
		}
		return model;

	}

}
