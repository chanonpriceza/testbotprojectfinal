package botbackend.service.aws;

import java.io.UnsupportedEncodingException;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;

import botbackend.utils.Util;
import botbackend.utils.LogUtil;

import com.amazonaws.services.simpleemail.AWSJavaMailTransport;

public class SimpleEmail {

	private String awsSESAccessKey = null;
	private String awsSESSecretKey = null;
	
	public SimpleEmail(String awsSESAccessKey, String awsSESSecretKey) {
		this.awsSESAccessKey = awsSESAccessKey;
		this.awsSESSecretKey = awsSESSecretKey;
	}	

	public void sendMail(String subject, String message, String sendFrom, String sendTo, String module, String ref)throws MessagingException{
		
		if(StringUtils.isNotBlank(sendTo)){
			sendTo = sendTo.trim();
		}
		
		if(!Util.validateEmail(sendTo, true)){
			LogUtil.logEmailSendData("AWS", module, ref, sendTo, false, "invalid Email");
			return;
		}
		//mail properties
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "aws");
		props.setProperty("mail.aws.user", awsSESAccessKey);
		props.setProperty("mail.aws.password", awsSESSecretKey);		  
		Session awsSession = Session.getInstance(props);
		Transport transport = new AWSJavaMailTransport(awsSession, null);
		transport.connect();
		// Create new message
		try{
			Session session = Session.getInstance(new Properties()); 
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(sendFrom, "Priceza.com"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
			msg.setSubject(subject, "UTF-8");			
			msg.setContent(message,"text/plain;charset=UTF-8");
			msg.saveChanges();
			transport.sendMessage(msg, null);
			LogUtil.logEmailSendData("AWS", module, ref, sendTo, true, "");
		} catch (MessagingException e){
			LogUtil.logEmailSendData("AWS", module, ref, sendTo, false, e.toString());
		} catch (UnsupportedEncodingException e) {
			LogUtil.logEmailSendData("AWS", module, ref, sendTo, false, e.toString());
		}
	}
}
