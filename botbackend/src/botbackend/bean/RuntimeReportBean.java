package botbackend.bean;

import java.util.Date;

public class RuntimeReportBean {
	private int id;
	private String botServerName;
	private String processType;
	private int status;
	private Date updateDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBotServerName() {
		return botServerName;
	}
	public void setBotServerName(String botServerName) {
		this.botServerName = botServerName;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
