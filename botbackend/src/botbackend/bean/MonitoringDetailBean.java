package botbackend.bean;

import java.util.Date;

public class MonitoringDetailBean {
	
	private int id;
	private int merchantId;
	private int count;
	private String topic;
	private String type;
	private String detail;
	private String solveDetail;
	private String solveBy;
	private Date firstFoundDate;
	private Date recentFoundDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Date getFirstFoundDate() {
		return firstFoundDate;
	}
	public void setFirstFoundDate(Date firstFoundDate) {
		this.firstFoundDate = firstFoundDate;
	}
	public Date getRecentFoundDate() {
		return recentFoundDate;
	}
	public void setRecentFoundDate(Date recentFoundDate) {
		this.recentFoundDate = recentFoundDate;
	}
	public String getSolveDetail() {
		return solveDetail;
	}
	public void setSolveDetail(String solveDetail) {
		this.solveDetail = solveDetail;
	}
	public String getSolveBy() {
		return solveBy;
	}
	public void setSolveBy(String solveBy) {
		this.solveBy = solveBy;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	

}
