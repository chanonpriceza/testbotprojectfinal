package botbackend.bean;

import java.util.Date;

public class ProductWaitSyncQualityBean {
	
	private int id;
	private String dataServer;
	private Date date;
	private int allCount;
	private int remain;
	private int done;
	private double score;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDataServer() {
		return dataServer;
	}
	public void setDataServer(String dataServer) {
		this.dataServer = dataServer;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getAllCount() {
		return allCount;
	}
	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}
	public int getRemain() {
		return remain;
	}
	public void setRemain(int remain) {
		this.remain = remain;
	}
	public int getDone() {
		return done;
	}
	public void setDone(int done) {
		this.done = done;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
}
