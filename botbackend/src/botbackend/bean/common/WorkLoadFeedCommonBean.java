package botbackend.bean.common;

public class WorkLoadFeedCommonBean {
	
	private String type;
	private int merchantId;
	private int categoryId;
	private String name;
	private double price;
	private String url;
	private String urlForUpdate;
	private String description;
	private String pictureUrl;
	private String pictureData;
	private String realProductId;
	private String keyword;
	private double basePrice;
	private String upc;
	
	// tbl_product_data
	private int	 old_merchantId;
	private String old_name;
	private double old_price;
	private String old_url;
	private String old_description;
	private String old_pictureUrl;
	private String old_pictureData;
	private String old_urlForUpdate;
	private String old_realProductId;
	private double old_basePrice;
	private String old_upc;

	
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public String getPictureData() {
		return pictureData;
	}
	public void setPictureData(String pictureData) {
		this.pictureData = pictureData;
	}
	public String getRealProductId() {
		return realProductId;
	}
	public void setRealProductId(String realProductId) {
		this.realProductId = realProductId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrlForUpdate() {
		return urlForUpdate;
	}
	public void setUrlForUpdate(String urlForUpdate) {
		this.urlForUpdate = urlForUpdate;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}

	public int getOld_merchantId() {
		return old_merchantId;
	}
	public void setOld_merchantId(int old_merchantId) {
		this.old_merchantId = old_merchantId;
	}
	public String getOld_name() {
		return old_name;
	}
	public void setOld_name(String old_name) {
		this.old_name = old_name;
	}
	public double getOld_price() {
		return old_price;
	}
	public void setOld_price(double old_price) {
		this.old_price = old_price;
	}
	public String getOld_url() {
		return old_url;
	}
	public void setOld_url(String old_url) {
		this.old_url = old_url;
	}
	public String getOld_description() {
		return old_description;
	}
	public void setOld_description(String old_description) {
		this.old_description = old_description;
	}
	public String getOld_pictureUrl() {
		return old_pictureUrl;
	}
	public void setOld_pictureUrl(String old_pictureUrl) {
		this.old_pictureUrl = old_pictureUrl;
	}
	public String getOld_pictureData() {
		return old_pictureData;
	}
	public void setOld_pictureData(String old_pictureData) {
		this.old_pictureData = old_pictureData;
	}
	public String getOld_urlForUpdate() {
		return old_urlForUpdate;
	}
	public void setOld_urlForUpdate(String old_urlForUpdate) {
		this.old_urlForUpdate = old_urlForUpdate;
	}
	public String getOld_realProductId() {
		return old_realProductId;
	}
	public void setOld_realProductId(String old_realProductId) {
		this.old_realProductId = old_realProductId;
	}
	public double getOld_basePrice() {
		return old_basePrice;
	}
	public void setOld_basePrice(double old_basePrice) {
		this.old_basePrice = old_basePrice;
	}
	public String getOld_upc() {
		return old_upc;
	}
	public void setOld_upc(String old_upc) {
		this.old_upc = old_upc;
	}
	

}