package botbackend.bean.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PagingBean {

	private int currentPage;
	private int totalRecord;
	private int totalPage;
	private List<String> pagingList;

	public static PagingBean createPagingBean(int currentPage, int totalRecord, int recordPerPage) {
		
		int totalPage = (int)(Math.ceil((double)totalRecord/(double)recordPerPage));
   		List<String> pagingList = genPaging(currentPage, totalPage);
   		
   		PagingBean pBean = new PagingBean();
   		pBean.setCurrentPage(currentPage);
   		pBean.setTotalRecord(totalRecord);
   		pBean.setTotalPage(totalPage);
   		pBean.setPagingList(pagingList);
   		
   		return pBean;		
	}	
	
	public static List<String> genPaging(int currentPage, int totalPage) {
		List<String> pagingList = new ArrayList<String>();        
		if(totalPage == 0) {
			return pagingList;
		}		
		int startMid = currentPage - 2;
		int stopMid = currentPage + 2;
		if(startMid > totalPage - 4) {
			startMid = totalPage - 4;
		}
		if(stopMid < 5) {
			stopMid = 5;
		}

		List<Integer> midList = new ArrayList<Integer>();
    	for (int i = startMid; i <= stopMid; i++) {
    			if(i <= totalPage && i > 0) {
    				midList.add(i);
    			}		      		
    	}
    	Set<Integer> check = new HashSet<Integer>();    	
	  	pagingList.add("1");
	  	check.add(1);
	  	int lastAdd = 1;
	  	for (Integer page : midList) {			
	  		if(!check.contains(page)) {	  			
	  			if(page > lastAdd + 1) {
	  				pagingList.add("...");	  				
	  			}	  			
	  			pagingList.add(String.valueOf(page));
	  			check.add(page);
	  			lastAdd = page;
	  		}
		}	  	
	  	if(!check.contains(totalPage)) {	  			
  			if(totalPage > lastAdd + 1) {
  				pagingList.add("...");	  				
  			}	  			
  			pagingList.add(String.valueOf(totalPage));
  		}    			
		return pagingList;
	}
	
	
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public List<String> getPagingList() {
		return pagingList;
	}
	public void setPagingList(List<String> pagingList) {
		this.pagingList = pagingList;
	}
}
