package botbackend.bean;

import java.util.List;

import org.jsoup.nodes.Element;

public class ProductParserBean {
	private String name;
	private String price;
	private String description;
	private String pictureUrl;
	private String realProductId;
	private String upc;
	private String basePrice;
	private String expire;
	private String url;
	private String concatId;
	private String concatWord;
	private String pictureData;
	private String category;
	private String keyword;
	private boolean isPrice;
	private boolean isBasePrice;
	
	List<String> nameStepResult;
	List<String> priceStepResult;
	List<String> descriptionStepResult;
	List<String> pictureUrlStepResult;
	List<String> realProductIdStepResult;
	List<String> urlStepResult;
	List<String> basePriceStepResult;
	List<String> upcStepResult;
	List<String> concatIdStepResult;
	List<String> concatWordStepResult;

	private Element elementName;
	private Element elementPrice;
	private Element elementBasePrice;
	private Element elementPictureUrl;
	private Element elementRealProductId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public String getRealProductId() {
		return realProductId;
	}
	public void setRealProductId(String realProductId) {
		this.realProductId = realProductId;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(String basePrice) {
		this.basePrice = basePrice;
	}
	public String getExpire() {
		return expire;
	}
	public void setExpire(String expire) {
		this.expire = expire;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getConcatId() {
		return concatId;
	}
	public void setConcatId(String concatId) {
		this.concatId = concatId;
	}
	public String getPictureData() {
		return pictureData;
	}
	public void setPictureData(String pictureData) {
		this.pictureData = pictureData;
	}
	public boolean getIsPrice() {
		return isPrice;
	}
	public void setIsPrice(boolean isPrice) {
		this.isPrice = isPrice;
	}
	public boolean getIsBasePrice() {
		return isBasePrice;
	}
	public void setIsBasePrice(boolean isBasePrice) {
		this.isBasePrice = isBasePrice;
	}
	public String getConcatWord() {
		return concatWord;
	}
	public void setConcatWord(String concatWord) {
		this.concatWord = concatWord;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Element getElementName() {
		return elementName;
	}
	public void setElementName(Element elementName) {
		this.elementName = elementName;
	}
	public Element getElementPrice() {
		return elementPrice;
	}
	public void setElementPrice(Element elementPrice) {
		this.elementPrice = elementPrice;
	}
	public Element getElementBasePrice() {
		return elementBasePrice;
	}
	public void setElementBasePrice(Element elementBasePrice) {
		this.elementBasePrice = elementBasePrice;
	}
	public Element getElementPictureUrl() {
		return elementPictureUrl;
	}
	public void setElementPictureUrl(Element elementPictureUrl) {
		this.elementPictureUrl = elementPictureUrl;
	}
	public Element getElementRealProductId() {
		return elementRealProductId;
	}
	public void setElementRealProductId(Element elementRealProductId) {
		this.elementRealProductId = elementRealProductId;
	}
	public void setEmptyField(){
		this.name = "";
		this.price = "";
		this.description = "";
		this.pictureUrl = "";
		this.realProductId = "";
		this.upc = "";
		this.basePrice = "";
		this.expire = "";
		this.url = "";
		this.concatId = "";
		this.concatWord = "";
		this.pictureData = "";
		this.category = "";
		this.keyword = "";
	}
	public List<String> getNameStepResult() {
		return nameStepResult;
	}
	public void setNameStepResult(List<String> nameStepResult) {
		this.nameStepResult = nameStepResult;
	}
	public List<String> getPriceStepResult() {
		return priceStepResult;
	}
	public void setPriceStepResult(List<String> priceStepResult) {
		this.priceStepResult = priceStepResult;
	}
	public List<String> getDescriptionStepResult() {
		return descriptionStepResult;
	}
	public void setDescriptionStepResult(List<String> descriptionStepResult) {
		this.descriptionStepResult = descriptionStepResult;
	}
	public List<String> getPictureUrlStepResult() {
		return pictureUrlStepResult;
	}
	public void setPictureUrlStepResult(List<String> pictureUrlStepResult) {
		this.pictureUrlStepResult = pictureUrlStepResult;
	}
	public List<String> getRealProductIdStepResult() {
		return realProductIdStepResult;
	}
	public void setRealProductIdStepResult(List<String> realProductIdStepResult) {
		this.realProductIdStepResult = realProductIdStepResult;
	}
	public List<String> getUrlStepResult() {
		return urlStepResult;
	}
	public void setUrlStepResult(List<String> urlStepResult) {
		this.urlStepResult = urlStepResult;
	}
	public List<String> getBasePriceStepResult() {
		return basePriceStepResult;
	}
	public void setBasePriceStepResult(List<String> basePriceStepResult) {
		this.basePriceStepResult = basePriceStepResult;
	}
	public List<String> getUpcStepResult() {
		return upcStepResult;
	}
	public void setUpcStepResult(List<String> upcStepResult) {
		this.upcStepResult = upcStepResult;
	}
	public List<String> getConcatIdStepResult() {
		return concatIdStepResult;
	}
	public void setConcatIdStepResult(List<String> concatIdStepResult) {
		this.concatIdStepResult = concatIdStepResult;
	}
	public List<String> getConcatWordStepResult() {
		return concatWordStepResult;
	}
	public void setConcatWordStepResult(List<String> concatWordStepResult) {
		this.concatWordStepResult = concatWordStepResult;
	}
	
	
	
}
