package botbackend.bean;

import java.util.Date;

public class MerchantBean {
	
	private int id;
	private String name;
	private int active;
	private String state;
	private String status;
	private String dataServer;
	private String dataUpdateState;
	private String dataUpdateStatus;
	private int packageType;
	private int dueErrorCount;
	private int wceErrorCount;
	private String errorMessage;
	private String server;
	private int nodeTest;
	private int ownerId;
	private String ownerName;
	private String note;
	private Date noteDate;
	private String currentserver;
	private int shardNo;
	private String userUpdate;
	private Date lastUpdate;
	private String coreUserConfig;
	private int countWebProduct;
	private Date wceNextStart;
	private Date dueNextStart;
	private String command;
	//merchant_mapping 
	
	private int merchant_package_runtime;
	private int redirect_type_runtime;
	private String sale_name;
	private String slug;
	private String store;
	private int merchant_id;
	private String site_url;
	private int redirect_type;
	private int mobile_redirect_type;
	private int enable_report;
	private String idServername;
	private int allCount;
	
	public int getAllCount() {
		return allCount;
	}
	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDataUpdateState() {
		return dataUpdateState;
	}
	public void setDataUpdateState(String dataUpdateState) {
		this.dataUpdateState = dataUpdateState;
	}
	public String getDataUpdateStatus() {
		return dataUpdateStatus;
	}
	public void setDataUpdateStatus(String dataUpdateStatus) {
		this.dataUpdateStatus = dataUpdateStatus;
	}
	public int getPackageType() {
		return packageType;
	}
	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}
	public int getDueErrorCount() {
		return dueErrorCount;
	}
	public void setDueErrorCount(int dueErrorCount) {
		this.dueErrorCount = dueErrorCount;
	}
	public int getWceErrorCount() {
		return wceErrorCount;
	}
	public void setWceErrorCount(int wceErrorCount) {
		this.wceErrorCount = wceErrorCount;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	
	public int getNodeTest() {
		return nodeTest;
	}
	public void setNodeTest(int nodeTest) {
		this.nodeTest = nodeTest;
	}
	public int getMerchant_package_runtime() {
		return merchant_package_runtime;
	}
	public void setMerchant_package_runtime(int merchant_package_runtime) {
		this.merchant_package_runtime = merchant_package_runtime;
	}
	public int getRedirect_type_runtime() {
		return redirect_type_runtime;
	}
	public void setRedirect_type_runtime(int redirect_type_runtime) {
		this.redirect_type_runtime = redirect_type_runtime;
	}
	public String getSale_name() {
		return sale_name;
	}
	public void setSale_name(String sale_name) {
		this.sale_name = sale_name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getNoteDate() {
		return noteDate;
	}
	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public int getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getSite_url() {
		return site_url;
	}
	public void setSite_url(String site_url) {
		this.site_url = site_url;
	}
	public int getRedirect_type() {
		return redirect_type;
	}
	public void setRedirect_type(int redirect_type) {
		this.redirect_type = redirect_type;
	}
	public int getMobile_redirect_type() {
		return mobile_redirect_type;
	}
	public void setMobile_redirect_type(int mobile_redirect_type) {
		this.mobile_redirect_type = mobile_redirect_type;
	}
	public int getEnable_report() {
		return enable_report;
	}
	public void setEnable_report(int enable_report) {
		this.enable_report = enable_report;
	}
	public String getDataServer() {
		return dataServer;
	}
	public void setDataServer(String dataServer) {
		this.dataServer = dataServer;
	}
	public String getCurrentserver() {
		return currentserver;
	}
	public void setCurrentserver(String currentserver) {
		this.currentserver = currentserver;
	}
	public int getShardNo() {
		return shardNo;
	}
	public void setShardNo(int shardNo) {
		this.shardNo = shardNo;
	}
	public String getUserUpdate() {
		return userUpdate;
	}
	public void setUserUpdate(String userUpdate) {
		this.userUpdate = userUpdate;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getCoreUserConfig() {
		return coreUserConfig;
	}
	public void setCoreUserConfig(String coreUserConfig) {
		this.coreUserConfig = coreUserConfig;
	}
	public int getCountWebProduct() {
		return countWebProduct;
	}
	public void setCountWebProduct(int countWebProduct) {
		this.countWebProduct = countWebProduct;
	}
	public String getIdServername() {
		return idServername;
	}
	public void setIdServername(String idServername) {
		this.idServername = idServername;
	}
	public Date getWceNextStart() {
		return wceNextStart;
	}
	public void setWceNextStart(Date wceNextStart) {
		this.wceNextStart = wceNextStart;
	}
	public Date getDueNextStart() {
		return dueNextStart;
	}
	public void setDueNextStart(Date dueNextStart) {
		this.dueNextStart = dueNextStart;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
}
