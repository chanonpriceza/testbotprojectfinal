package botbackend.bean;

import java.util.Date;

import utils.DateTimeUtil;

public class ServerRuntimeReportBean {
	private int id;
	private String name;
	private String type;
	private int packageType;
	private int merchantId;
	private int countOld;
	private int countNew;
	private int successCount;
	private int errorCount;
	private int add;
	private int update;
	private int updatePic;
	private int delete;
	private Date startDate;
	private Date endDate;
	private String status;
	private String msgData;
	private String server;
	private String diffDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPackageType() {
		return packageType;
	}
	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getCountOld() {
		return countOld;
	}
	public void setCountOld(int countOld) {
		this.countOld = countOld;
	}
	public int getCountNew() {
		return countNew;
	}
	public void setCountNew(int countNew) {
		this.countNew = countNew;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public int getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}
	public int getAdd() {
		return add;
	}
	public void setAdd(int add) {
		this.add = add;
	}
	public int getUpdate() {
		return update;
	}
	public void setUpdate(int update) {
		this.update = update;
	}
	public int getUpdatePic() {
		return updatePic;
	}
	public void setUpdatePic(int updatePic) {
		this.updatePic = updatePic;
	}
	public int getDelete() {
		return delete;
	}
	public void setDelete(int delete) {
		this.delete = delete;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsgData() {
		return msgData;
	}
	public void setMsgData(String msgData) {
		this.msgData = msgData;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getDiffDate() {
		return diffDate;
	}
	public void setDiffDate() {
		this.diffDate = DateTimeUtil.calcurateHourDiff(this.getStartDate(), this.getEndDate());
	}


}
