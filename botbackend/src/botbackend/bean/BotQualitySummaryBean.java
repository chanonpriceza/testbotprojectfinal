package botbackend.bean;

public class BotQualitySummaryBean {
	
	private int merchantAll;
	private int merchantNotUpdate;
	private int merchantWaitFix;
	private int merchantUpdateIn1Day;
	private int merchantUpdateIn3Day;
	private int merchantUpdateMore3Day;
	private int productAll;
	private int productNotUpdate;
	private int productWaitToFix;
	private int productUpdateIn1Day;  
	private int productUpdateIn3Day;  
	private int productUpdateMore3Day;
	
	public int getMerchantAll() {
		return merchantAll;
	}

	public int getMerchantNotUpdate() {
		return merchantNotUpdate;
	}

	public int getMerchantWaitFix() {
		return merchantWaitFix;
	}

	public int getMerchantUpdateIn1Day() {
		return merchantUpdateIn1Day;
	}

	public int getMerchantUpdateIn3Day() {
		return merchantUpdateIn3Day;
	}

	public int getMerchantUpdateMore3Day() {
		return merchantUpdateMore3Day;
	}

	public int getProductAll() {
		return productAll;
	}

	public int getProductNotUpdate() {
		return productNotUpdate;
	}

	public int getProductWaitToFix() {
		return productWaitToFix;
	}

	public int getProductUpdateIn1Day() {
		return productUpdateIn1Day;
	}

	public int getProductUpdateIn3Day() {
		return productUpdateIn3Day;
	}

	public int getProductUpdateMore3Day() {
		return productUpdateMore3Day;
	}
	
	public void increaseMerchantUpdateIn1Day() {
		this.merchantUpdateIn1Day++;
	}
	
	public void increaseMerchantUpdateIn3Day() {
		this.merchantUpdateIn3Day++;
	}
	
	public void increaseMerchantUpdateMore3Day() {
		this.merchantUpdateMore3Day++;
	}
	
	public void increaseNotUpdate() {
		this.merchantNotUpdate++;
	}
	
	public void increaseMerchantWaitingFix() {
		this.merchantWaitFix++;
	}
	
	public void increaseMerchantAll() {
		this.merchantAll++;
	}
	
	public void increasMerchantNotUpdate() {
		this.merchantNotUpdate++;
	}
	
	
//	private int productAll;
//	private int productNotUpdate;
//	private int productWaitToFix;
//	private int productUpdateIn1Day;  
//	private int productUpdateIn3Day;  
//	private int productUpdateMore3Day;
	
	public void addProductAll(int sum) {
		this.productAll += sum;
	}
	public void addProductNotUpdate(int sum) {
		this.productNotUpdate += sum;
	}
	public void addProductWaitToFix(int sum) {
		this.productWaitToFix += sum;
	}
	public void addProductUpdateIn1Day(int sum) {
		this.productUpdateIn1Day += sum;
	}
	public void addProductUpdateIn3Day(int sum) {
		this.productUpdateIn3Day += sum;
	}
	public void addProductUpdateMore3Day(int sum) {
		this.productUpdateMore3Day += sum;
	}
	
}
