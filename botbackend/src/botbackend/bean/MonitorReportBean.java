package botbackend.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import botbackend.schedule.MonitorAnalyzeSchedule.STATUS;

public class MonitorReportBean {
	
	private String key;
	private String result;
	private Map<String,String> exampleData;
	private List<String> testPhase;
	private String jsonResult;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public void setResult(STATUS result) {
		this.result = result.toString();
	}
	public Map<String, String> getExampleData() {
		return exampleData;
	}
	public void setExampleData(Map<String, String> exampleData) {
		this.exampleData = exampleData;
	}
	public void setExampleData(String key,String value) {
		if(exampleData==null) {
			exampleData = new HashMap<String, String>();
		}
		exampleData.put(key, value);
	}
	
	public List<String> getTestPhase() {
		return testPhase;
	}
	public void putTestPhase(String testPhase) {
		if(this.testPhase==null)this.testPhase=new ArrayList<String>();
		this.testPhase.add(testPhase);
	}
	public String getJsonResult() {
		return jsonResult;
	}
	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}
	
	public void mergeData() {
		try {
		//Object result = new Object();
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("exampleData", exampleData);
		result.put("testPhase", testPhase);
		JSONObject obj = new JSONObject();
		obj.putAll(result);
		jsonResult = obj.toJSONString();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
