package botbackend.bean.api;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import utils.BotUtil;
import utils.FilterUtil;

public class LnwShopSendDataBean {
	    
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	
	public enum ACTION {
		ADD, UPDATE ,DELETE, UPDATE_PIC
	}
	
	private int id;
	private String action;
	private int merchantId;
	private int categoryId;
		
	private String name;
	private double price;
	private String url;
	private String description;
	private byte[] pictureData;	
	private String pictureUrl;
	private String urlForUpdate;
	private Timestamp merchantUpdateDate;
	private Timestamp updateDate;
	private String realProductId;
	private String keyword;
	private double basePrice;
	private String upc;
	private String dynamicField;
	
	
	@SuppressWarnings("unchecked")
	public static LnwShopSendDataBean createSendDataBean(LnwShopProductDataBean productDataBean, LnwShopMerchantBean merchantBean, byte[] pictureData, Map<String, Object[]> catMap, ACTION action) {
		
		String productName = productDataBean.getName();
		Double productPrice = productDataBean.getPrice();
		
		if(merchantBean.getAddParentName() == 1 && StringUtils.isNotBlank(productDataBean.getParent_product_name())){
			StringBuilder mixProductName = new StringBuilder();
			mixProductName.append(productDataBean.getParent_product_name());
			mixProductName.append(" ");
			mixProductName.append(FilterUtil.getStringAfter(productName, productDataBean.getParent_product_name(), productName));
			productName = mixProductName.toString();
		}
		
		if(merchantBean.getAddProductId() == 1) {
			StringBuilder mixProductName = new StringBuilder();
			mixProductName.append(productName);
			mixProductName.append(" (");
			mixProductName.append(productDataBean.getLnw_product_id());
			mixProductName.append(")");
			productName = mixProductName.toString();
		}
		
		if(productPrice == 0  && productDataBean.getPriceMin() != 0){
			productPrice = productDataBean.getPriceMin();
		}
		
		if(productPrice == 0  && productDataBean.getPriceMax() != 0){
			productPrice = productDataBean.getPriceMax();
		}
		
		if(merchantBean.getForceContactPrice() == 1){
			productPrice = (double) BotUtil.CONTACT_PRICE;
		}
		
		if(merchantBean.getAddContactPrice() == 1 && productPrice == 0){
			productPrice = (double) BotUtil.CONTACT_PRICE;
		}
		
		if(merchantBean.getPriceMultiply() > 0 && productPrice != BotUtil.CONTACT_PRICE){
			productPrice = productPrice * merchantBean.getPriceMultiply();
		}
		
		if(productPrice == 0){
			return null;
		}
		
		productPrice = FilterUtil.convertPriceStr(formatPrice.format(productPrice));
		
		LnwShopSendDataBean sendDataBean = new LnwShopSendDataBean();
		sendDataBean.setAction(action.toString());
		sendDataBean.setMerchantId(merchantBean.getId());
		sendDataBean.setName(productName);
		sendDataBean.setPrice(productPrice);
		sendDataBean.setUrl(productDataBean.getProduct_url());
		sendDataBean.setDescription(productDataBean.getDescription());
		if(pictureData != null){
			sendDataBean.setPictureData(pictureData);
			sendDataBean.setPictureUrl(productDataBean.getPicture_url());				
		}
		if(catMap != null && catMap.size() > 0 && catMap.get(productDataBean.getCategory()) != null) {
			Object[] catObj = catMap.get(productDataBean.getCategory());
			sendDataBean.setCategoryId((int) catObj[0]);
			sendDataBean.setKeyword((String) catObj[1]);
			JSONObject dynamicField = new JSONObject();
			dynamicField.put("oldCategory", Integer.toString(0));
			dynamicField.put("forceUpdateCategory", "true");
			sendDataBean.setDynamicField(dynamicField.toJSONString());
		}
		sendDataBean.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		
		return sendDataBean;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrlForUpdate() {
		return urlForUpdate;
	}
	public void setUrlForUpdate(String urlForUpdate) {
		this.urlForUpdate = urlForUpdate;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Timestamp getMerchantUpdateDate() {
		return merchantUpdateDate;
	}
	public void setMerchantUpdateDate(Timestamp merchantUpdateDate) {
		this.merchantUpdateDate = merchantUpdateDate;
	}
	public byte[] getPictureData() {
		return pictureData;
	}
	public void setPictureData(byte[] pictureData) {
		this.pictureData = pictureData;
	}

	public String getRealProductId() {
		return realProductId;
	}

	public void setRealProductId(String realProductId) {
		this.realProductId = realProductId;
	}
    public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getDynamicField() {
		return dynamicField;
	}
	public void setDynamicField(String dynamicField) {
		this.dynamicField = dynamicField;
	}
	
}