package botbackend.bean.api;

import java.sql.Timestamp;

public class LnwShopProductDataBean {
	    
	public enum STATUS {
		ADD, UPDATE, DELETE, DONE
	}
	
	private int id;
	private String name;
	private double price;
	private String description;
	private String picture_url;
	private String product_url;
	private String status;
	private String lnw_merchant_id;
	private int lnw_product_id;
	private String old_name;
	private String parent_product_name;
	private String tag;
	private String category;
	private double priceMin;
	private double priceMax;
	private Timestamp updateDate;

	public LnwShopProductDataBean() {}
	public LnwShopProductDataBean(int lnw_product_id, String lnw_merchant_id, String name, double price, String product_url) {
		this.lnw_product_id = lnw_product_id;
		this.lnw_merchant_id = lnw_merchant_id;
		this.name = name;
		this.price = price;
		this.product_url = product_url;		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPicture_url() {
		return picture_url;
	}
	public void setPicture_url(String picture_url) {
		this.picture_url = picture_url;
	}
	public String getProduct_url() {
		return product_url;
	}
	public void setProduct_url(String product_url) {
		this.product_url = product_url;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLnw_merchant_id() {
		return lnw_merchant_id;
	}
	public void setLnw_merchant_id(String lnw_merchant_id) {
		this.lnw_merchant_id = lnw_merchant_id;
	}
	public int getLnw_product_id() {
		return lnw_product_id;
	}
	public void setLnw_product_id(int lnw_product_id) {
		this.lnw_product_id = lnw_product_id;
	}
	public String getOld_name() {
		return old_name;
	}
	public void setOld_name(String old_name) {
		this.old_name = old_name;
	}
	public String getParent_product_name() {
		return parent_product_name;
	}
	public void setParent_product_name(String parent_product_name) {
		this.parent_product_name = parent_product_name;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getPriceMin() {
		return priceMin;
	}
	public void setPriceMin(double priceMin) {
		this.priceMin = priceMin;
	}
	public double getPriceMax() {
		return priceMax;
	}
	public void setPriceMax(double priceMax) {
		this.priceMax = priceMax;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	
}