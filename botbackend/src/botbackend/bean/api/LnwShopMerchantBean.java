package botbackend.bean.api;

public class LnwShopMerchantBean {
	    
	private int id;
	private String lnw_id;
	private String name;
	private int active;
	private int addParentName;
	private int addProductId;
	private int addContactPrice;
	private int forceContactPrice;
	private int forceUpdateCat;
	private Double priceMultiply;
	private String note;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLnw_id() {
		return lnw_id;
	}
	public void setLnw_id(String lnw_id) {
		this.lnw_id = lnw_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public int getAddParentName() {
		return addParentName;
	}
	public void setAddParentName(int addParentName) {
		this.addParentName = addParentName;
	}
	public int getAddProductId() {
		return addProductId;
	}
	public void setAddProductId(int addProductId) {
		this.addProductId = addProductId;
	}
	public int getAddContactPrice() {
		return addContactPrice;
	}
	public void setAddContactPrice(int addContactPrice) {
		this.addContactPrice = addContactPrice;
	}
	public int getForceContactPrice() {
		return forceContactPrice;
	}
	public void setForceContactPrice(int forceContactPrice) {
		this.forceContactPrice = forceContactPrice;
	}
	public int getForceUpdateCat() {
		return forceUpdateCat;
	}
	public void setForceUpdateCat(int forceUpdateCat) {
		this.forceUpdateCat = forceUpdateCat;
	}
	public Double getPriceMultiply() {
		return priceMultiply;
	}
	public void setPriceMultiply(Double priceMultiply) {
		this.priceMultiply = priceMultiply;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}