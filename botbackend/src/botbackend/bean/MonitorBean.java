package botbackend.bean;

import java.util.Date;

public class MonitorBean {
	
	private int id;
	private int merchantId;
	private String analyzeResult;
	private Date updateDate;
	private int status;

	//tbl_merchant
	private String name;
	private String server;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getAnalyzeResult() {
		return analyzeResult;
	}
	public void setAnalyzeResult(String analyzeResult) {
		this.analyzeResult = analyzeResult;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	

}
