package botbackend.bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;

import utils.BotUtil;

public class NotificationApiBean  {
	
	private int id;
	private String message;
	private String destination;
	private String destinationType; // ชนิดของปลายทาง
	private int type; //ประเภทของ Noti
	private boolean isRead; 
	private Timestamp createDate;
	private String country;
	private int merchantId;
	private String source;
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	private String link;
	
	private static Map<String,String> formatMessage () {
		Map<String,String> map = new HashMap<String, String>();
		map.put(NOTITYPE.MERCHANT_WARNING.toString(),"");
		map.put(NOTITYPE.MERCHANT_APPROVE.toString(),"Merchant : {merchantId} is Waiting Approve.");
		map.put(NOTITYPE.PROCESS_TRACKING.toString(),"");
		map.put(NOTITYPE.TASK_COMMENT.toString(),"{userId} has comment on Task id: {taskId}");
		map.put(NOTITYPE.EVENT_BOARDCAST.toString(),"");
		map.put(NOTITYPE.MERCHANT_WARNING.toString(),"");
		return map;
	};
	
	private static Map<String,String> formatLink () {
		Map<String,String> map = new HashMap<String, String>();
		map.put(NOTITYPE.MERCHANT_WARNING.toString(),"");
		map.put(NOTITYPE.MERCHANT_APPROVE.toString(),"/admin/checkapproval?merchantId={merchantId}");
		map.put(NOTITYPE.PROCESS_TRACKING.toString(),"");
		map.put(NOTITYPE.TASK_COMMENT.toString(),"/admin/taskmanager?id={taskId}");
		map.put(NOTITYPE.EVENT_BOARDCAST.toString(),"");
		map.put(NOTITYPE.MERCHANT_WARNING.toString(),"");
		return map;
	};
	
	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public NotificationApiBean(String message,String source,String destination,DESTYPE destinationType,NOTITYPE type,String link,COUNTRYLIST country,int merchantId) {
		this.message = message;
		this.source = source;
		this.destination = destination;
		this.destinationType = destinationType.toString();
		this.type = type.getNumVal();
		this.isRead = false;
		this.createDate = new Timestamp(System.currentTimeMillis());
		this.link = link;
		this.country =  country.toString();
		this.merchantId = merchantId;
	}
	
	public NotificationApiBean(JSONObject obj) {
		this.message = String.valueOf(obj.get("message"));
		this.source = String.valueOf(obj.get("source"));
		this.destination = String.valueOf(obj.get("destination"));
		this.destinationType = String.valueOf(obj.get("destinationType"));
		this.type = BotUtil.stringToInt(String.valueOf(obj.get("type")),0);
		this.isRead = Boolean.valueOf(String.valueOf(obj.get("read")));
		this.createDate = convertTimeStamp(String.valueOf(obj.get("createDate")));
		this.link = (String.valueOf(obj.get("link")));
		this.country =  String.valueOf(obj.get("country"));
		this.merchantId =  BotUtil.stringToInt(String.valueOf(obj.get("merchantId")),0);
		
	}
	
	public NotificationApiBean() {
		
	}
	
	
	public enum NOTITYPE {
		TASK_COMMENT(2),MERCHANT_WARNING(1),TASK_DOING(3),MERCHANT_APPROVE(4)
		,PROCESS_TRACKING(5),EVENT_BOARDCAST(6);
		private int numVal;

		NOTITYPE(int numVal) {
	        this.numVal = numVal;
	    }

	    public int getNumVal() {
	    	 return numVal;
	    }   
	    
	    public static Map<Integer,String> getNotiTypeMap() {
	    	return	Arrays.asList(NOTITYPE.values()).stream().collect(Collectors.toMap(NOTITYPE::getNumVal,x->x.toString()));
	    }
	}
	
	public enum COUNTRYLIST{
		TH,ID,SG,MY,VN,PH
	}
	
	public enum DESTYPE{
		GROUP,INDIVIDUAL
	}
	
	public enum USER{
		SYSTEM,
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationType() {
		return destinationType;
	}
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public boolean getIsRead() {
		return isRead;
	}
	public void setIsRead(boolean read) {
		this.isRead = read;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		
		JSONObject obj = new JSONObject();
		obj.put("id",this.id);
		obj.put("message",this.message);
		obj.put("destination",this.destination);
		obj.put("source",this.source);
		obj.put("destinationType",this.destinationType);
		obj.put("type",this.type);
		obj.put("message",this.message);
		obj.put("createDate",this.createDate.toString());
		obj.put("read",this.isRead);
		obj.put("link",this.link);
		obj.put("country",this.country);
		obj.put("merchantId",this.merchantId);
		
		return obj.toJSONString();

	}
	
	public static String getFormantMessage(NOTITYPE type) {
		return formatMessage().get(type.toString());

	}
	
	public static String getFormantLink(NOTITYPE type) {
		return formatLink().get(type.toString());

	}
	
	public Timestamp convertTimeStamp(String timeStamp) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(timeStamp);
			Timestamp timestamp = new Timestamp(parsedDate.getTime());
			return timestamp;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String generateMessageAddNewComment(Object userId,Object taskId) {
		String userString = String.valueOf(userId);
		String taskIdString = String.valueOf(taskId);
		return getFormantMessage(NOTITYPE.TASK_COMMENT).replace("{userId}", userString).replace("{taskId}", taskIdString);
	}
	
	public static String generateMessageCheckApprove(Object merchantId){
		String merchantIdString = String.valueOf(merchantId);
		return getFormantMessage(NOTITYPE.MERCHANT_APPROVE).replace("{merchantId}", merchantIdString);
	}
	
	public static String generateLinkCheckApprove(Object merchantId) {
		String merchantIdString = String.valueOf(merchantId);
		return getFormantLink(NOTITYPE.MERCHANT_APPROVE).replace("{merchantId}", merchantIdString);
		
	}
	
	public static String generateLinkAddNewComment(Object taskId) {
		String taskIdString = String.valueOf(taskId);
		return getFormantLink(NOTITYPE.TASK_COMMENT).replace("{taskId}", taskIdString);
		
	}
	
}
