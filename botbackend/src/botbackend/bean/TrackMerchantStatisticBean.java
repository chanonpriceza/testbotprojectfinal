package botbackend.bean;

import java.util.Date;

public class TrackMerchantStatisticBean {
	
	private int id;
	private int merchant_id;
	private Date update_date;
	private int solr_product;
	private int merchant_package;
	private int merchant_package_runtime;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public int getSolr_product() {
		return solr_product;
	}
	public void setSolr_product(int solr_product) {
		this.solr_product = solr_product;
	}
	public int getMerchant_package() {
		return merchant_package;
	}
	public void setMerchant_package(int merchant_package) {
		this.merchant_package = merchant_package;
	}
	public int getMerchant_package_runtime() {
		return merchant_package_runtime;
	}
	public void setMerchant_package_runtime(int merchant_package_runtime) {
		this.merchant_package_runtime = merchant_package_runtime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
