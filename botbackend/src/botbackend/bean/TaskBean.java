package botbackend.bean;

import java.util.Date;

public class TaskBean {
	
	public enum PROCESS{
		feedCatList, feedCountProduct, feedForTestParser, 
		checkMissingProduct, checkExceedProduct, checkMissingPic, checkPriceNotMatch, 
		updateDesc, updateRealProductId, 
		reCrawling, botQualityDownload, 
		merchantFeedParser, merchantParser, 
		newMerchantShopee, newMerchantLnwshop, others,getSupportPerformanceReport
	}
	public enum STATUS{
		WAITING, DOING, REOPEN, HOLD, AUTOREPLY, DONE,RESOLVE
	}
	
	private int id;
	private String merchantName;
	private String process;
	private String issue;
	private int merchantId;
	private String data;
	private String status;
	private String result;
	private String merchantType;
	private int packageType;
	private Date createDate;
	private Date dueDate;
	private String owner;
	private String updatedBy;
	private String filePath;
	private long totalTime;
	private boolean isCustomer;
	private String issueType;
	private int worktime;
	private int active;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public int getPackageType() {
		return packageType;
	}
	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}
	public boolean isCustomer() {
		return isCustomer;
	}
	public void setIsCustomer(boolean isCustomer) {
		this.isCustomer = isCustomer;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public int getWorktime() {
		return worktime;
	}
	public void setWorktime(int worktime) {
		this.worktime = worktime;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
	

}
