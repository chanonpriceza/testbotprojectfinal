package botbackend.bean;

import java.util.Date;

public class BotQualityReportBean {
	
	private int id;
	private String type;
	private int merchantId;
	private String name;
	private String status;
	private String server;
	private String time;
	private int allCount;
	private Date reportDate;
	
	public BotQualityReportBean() {
		
	}
	
	public BotQualityReportBean(String type, int merchantId, String name, String status, String server, String time, int allCount, Date reportDate) {
		this.type = type;
		this.merchantId = merchantId;
		this.name = name;
		this.status = status;
		this.server = server;
		this.time = time;
		this.allCount = allCount;
		this.reportDate = reportDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getAllCount() {
		return allCount;
	}
	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

}
