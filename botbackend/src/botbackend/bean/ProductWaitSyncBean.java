package botbackend.bean;

import java.util.Date;

public class ProductWaitSyncBean {
	
	private int id;
	private String dataServer;
	private Date addDate;
	private Date syncDate;
	private int firstProductId;
	private int lastProductId;
	private int allCount;
	private int done;
	private String status;
	
	public enum STATUS {
		//Complete, Waiting
		C, W
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDataServer() {
		return dataServer;
	}
	public void setDataServer(String dataServer) {
		this.dataServer = dataServer;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public Date getSyncDate() {
		return syncDate;
	}
	public void setSyncDate(Date syncDate) {
		this.syncDate = syncDate;
	}
	public int getFirstProductId() {
		return firstProductId;
	}
	public void setFirstProductId(int firstProductId) {
		this.firstProductId = firstProductId;
	}
	public int getLastProductId() {
		return lastProductId;
	}
	public void setLastProductId(int lastProductId) {
		this.lastProductId = lastProductId;
	}
	public int getAllCount() {
		return allCount;
	}
	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}
	public int getDone() {
		return done;
	}
	public void setDone(int done) {
		this.done = done;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(STATUS status) {
		this.status = status.toString();
	}
	
	public void setStatus(String status) {
		try {
			this.status = STATUS.valueOf(status).toString();
		} catch (Exception e) {
		}
	}
	
}
