package botbackend.bean;

public class MerchantConfigBean {
	
	public enum FIELD {		
		maxDepth, parserClass, initCrawlerClass, urlCrawlerClass, feedCrawlerClass, realtimeParserClass, 
		productUpdateLimit, urlCrawlerCharset, wceParserThreadNum, 
		dueParserThreadNum, enableCookies, ignoreHttpError, skipEncodeURL, forceDelete,
		disableUpdatePic, dueSleepTime, wceSleepTime, userOldProductUrl, imageParserClass,
		enableLargeImage, forceUpdateImage, enableUpdateDesc, enableSmallImage,
		updatePricePercent, enableVeryLargeImage, deleteLimit, forceUpdateKeyword, forceUpdateCategory,
		disableParserCookies, disableDUEContinue, parserCharset, enableUpdateUrl, feedCrawlerParam,
		wceParserClass, dueParserClass, enableUpdateBasePrice,
		feedType, feedCheckGz, feedUrl, feedUsername, feedPassword,
		merchantName, currencyRate, parserRealtimeClass, parserDetailClass
	}
	 
	private int id;
	private int merchantId;
	private String field;
	private String value;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
