package botbackend.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class UrlPatternHistoryBean {
	
	private int id;
	private int merchantId;
	private String condition;
	private String value;
	private String action;
	private int group;
	private int categoryId;
	private String keyword;
	
	private int userId;
	private String userAction;
	private String userActionDetail;
	private Date updateDate;
	
	public enum ACTION{
		ADD, DELETE, CHANGE
	}
	
	public static UrlPatternHistoryBean createUrlPatternHistoryBean(UrlPatternBean urlPatternBean, int userId, ACTION userAction, String userActionDetail){
		UrlPatternHistoryBean uhBean = new UrlPatternHistoryBean();
		uhBean.setMerchantId(urlPatternBean.getMerchantId());
		uhBean.setCondition(urlPatternBean.getCondition());
		uhBean.setValue(urlPatternBean.getValue());
		uhBean.setAction(urlPatternBean.getAction());
		uhBean.setGroup(urlPatternBean.getGroup());
		uhBean.setCategoryId(urlPatternBean.getCategoryId());
		uhBean.setKeyword(urlPatternBean.getKeyword());
		uhBean.setUserId(userId);
		uhBean.setUserAction(userAction.toString());
		uhBean.setUserActionDetail(StringUtils.defaultString(userActionDetail));
		return uhBean;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public int getGroup() {
		return group;
	}
	public void setGroup(int group) {
		this.group = group;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserAction() {
		return userAction;
	}
	public void setUserAction(String userAction) {
		this.userAction = userAction;
	}
	public String getUserActionDetail() {
		return userActionDetail;
	}
	public void setUserActionDetail(String userActionDetail) {
		this.userActionDetail = userActionDetail;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
