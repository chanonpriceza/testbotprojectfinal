package botbackend.bean;

public class PluginConfigBean {
	private int userId;
	private int id;
    private int pluginType;
    private String pluginPosition;
    private String pluginConfig;

    // for web
    private Object showSetting;
    
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPluginType() {
		return pluginType;
	}
	public void setPluginType(int pluginType) {
		this.pluginType = pluginType;
	}
	public String getPluginPosition() {
		return pluginPosition;
	}
	public void setPluginPosition(String pluginPosition) {
		this.pluginPosition = pluginPosition;
	}
	public String getPluginConfig() {
		return pluginConfig;
	}
	public void setPluginConfig(String pluginConfig) {
		this.pluginConfig = pluginConfig;
	}
	public Object getShowSetting() {
		return showSetting;
	}
	public void setShowSetting(Object showSetting) {
		this.showSetting = showSetting;
	}
	
}
