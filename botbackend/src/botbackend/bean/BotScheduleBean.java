package botbackend.bean;

import java.util.Date;

public class BotScheduleBean {
	private String name;
	private Date processTime;
	private Date currentTime;
	private int nextTime;
	private int continueTime;
	private int maxRuntime;
	private String processDay;
	private String state;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getProcessTime() {
		return processTime;
	}
	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}
	public int getNextTime() {
		return nextTime;
	}
	public void setNextTime(int nextTime) {
		this.nextTime = nextTime;
	}
	public int getContinueTime() {
		return continueTime;
	}
	public void setContinueTime(int continueTime) {
		this.continueTime = continueTime;
	}
	public String getProcessDay() {
		return processDay;
	}
	public void setProcessDay(String processDay) {
		this.processDay = processDay;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}
	public int getMaxRuntime() {
		return maxRuntime;
	}
	public void setMaxRuntime(int maxRuntime) {
		this.maxRuntime = maxRuntime;
	}
}
