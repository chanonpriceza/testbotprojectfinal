package botbackend.bean;

import java.util.Date;

public class MonitoringBean {
	
	public enum TOPIC{
		Stable,ExceedDeleteLimit,NoUrlPattern,WorkloadFailedAll,CannotMatch,CannotParse,NotFoundProduct
	}
	public enum TYPE{
		WCE, DUE
	}
	
	private int id;
	private int merchantId;
	private int count;
	private String type;
	private String topic;
	private String detail;
	private int packageType;
	private Date firstFoundDate;
	private Date recentFoundDate;
	private int active;
	private String merchantName;
	
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPackageType() {
		return packageType;
	}
	public void setPackageType(int packageType) {
		this.packageType = packageType;
	}
	public Date getFirstFoundDate() {
		return firstFoundDate;
	}
	public void setFirstFoundDate(Date firstFoundDate) {
		this.firstFoundDate = firstFoundDate;
	}
	public Date getRecentFoundDate() {
		return recentFoundDate;
	}
	public void setRecentFoundDate(Date recentFoundDate) {
		this.recentFoundDate = recentFoundDate;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	
}
