package botbackend.bean;

import java.util.Date;

public class FocusUpdateBean {
	
	private int id;
	private int merchantId;
	private int webProductId;
	private int botProductId;
	private String webProductName;
	private String botServerName;
	private String addBy;
	private Date addDate;
	private Date lastUpdateDate;
	private String note;
	private int active;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public int getWebProductId() {
		return webProductId;
	}
	public void setWebProductId(int webProductId) {
		this.webProductId = webProductId;
	}
	public int getBotProductId() {
		return botProductId;
	}
	public void setBotProductId(int botProductId) {
		this.botProductId = botProductId;
	}
	public String getWebProductName() {
		return webProductName;
	}
	public void setWebProductName(String webProductName) {
		this.webProductName = webProductName;
	}
	public String getBotServerName() {
		return botServerName;
	}
	public void setBotServerName(String botServerName) {
		this.botServerName = botServerName;
	}
	public String getAddBy() {
		return addBy;
	}
	public void setAddBy(String addBy) {
		this.addBy = addBy;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
}
