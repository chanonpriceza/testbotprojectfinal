package botbackend.bean;

import java.util.Date;

public class TrackBotStateBean {
	
	private int id;
	private String server;
	private String detail;
	private Date signalDate;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Date getSignalDate() {
		return signalDate;
	}
	public void setSignalDate(Date signalDate) {
		this.signalDate = signalDate;
	}
	
}
