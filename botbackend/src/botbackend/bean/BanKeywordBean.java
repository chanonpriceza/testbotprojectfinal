package botbackend.bean;

public class BanKeywordBean {
	
	private int id;
	private String merchantIdList;
	private String keyword;
	private String nonkeyword;
	private String requiredPhrase;
	private String nonCategory;
	private Integer minPrice;
	
	public String getMerchantIdList() {
		return merchantIdList;
	}
	public void setMerchantIdList(String merchantIdList) {
		this.merchantIdList = merchantIdList;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getNonkeyword() {
		return nonkeyword;
	}
	public void setNonkeyword(String nonkeyword) {
		this.nonkeyword = nonkeyword;
	}
	public String getRequiredPhrase() {
		return requiredPhrase;
	}
	public void setRequiredPhrase(String requiredPharse) {
		this.requiredPhrase = requiredPharse;
	}
	public String getNonCategory() {
		return nonCategory;
	}
	public void setNonCategory(String nonCategory) {
		this.nonCategory = nonCategory;
	}
	public int getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
