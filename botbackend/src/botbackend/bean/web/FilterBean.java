package botbackend.bean.web;

import java.util.List;

public class FilterBean {
	private int id;
	private String name;
	private String brand;
	private String subBrand;      
	private int category;
	private int subcategory; 
	private int merchant;
	private int merchantFallback;
	private int priceMin; 
	private int priceMax;	
	private List<Integer> catAttrId;
	private int[] status;
	private List<Integer> excludeId;
	private List<Integer> includeId;
	private String url;
	private String catAttrIdMannaul;
	private List<Integer> merchantList;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSubBrand() {
		return subBrand;
	}
	public void setSubBrand(String subBrand) {
		this.subBrand = subBrand;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getMerchant() {
		return merchant > 0? merchant : merchantFallback;
	}
	public void setMerchant(int merchant) {
		this.merchant = merchant;
	}
	public int getMerchantFallback() {
		return merchantFallback;
	}
	public void setMerchantFallback(int merchantFallback) {
		this.merchantFallback = merchantFallback;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(int subcategory) {
		this.subcategory = subcategory;
	}
	public int getPriceMax() {
		return priceMax;
	}
	public int getPriceMin() {
		return priceMin;
	}
	public void setPriceMin(int priceMin) {
		this.priceMin = priceMin;
	}
	public void setPriceMax(int priceMax) {
		this.priceMax = priceMax;
	}
	public List<Integer> getCatAttrId() {
		return catAttrId;
	}
	public void setCatAttrId(List<Integer> catAttrId) {
		this.catAttrId = catAttrId;
	}
	public int[] getStatus() {
		return status;
	}
	public void setStatus(int[] status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Integer> getExcludeId() {
		return excludeId;
	}
	public void setExcludeId(List<Integer> excludeId) {
		this.excludeId = excludeId;
	}
	public List<Integer> getIncludeId() {
		return includeId;
	}
	public void setIncludeId(List<Integer> includeId) {
		this.includeId = includeId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCatAttrIdMannaul() {
		return catAttrIdMannaul;
	}
	public void setCatAttrIdMannaul(String catAttrIdMannaul) {
		this.catAttrIdMannaul = catAttrIdMannaul;
	}
	public List<Integer> getMerchantList() {
		return merchantList;
	}
	public void setMerchantList(List<Integer> merchantList) {
		this.merchantList = merchantList;
	}
	
}
