package botbackend.bean.web;

public class WebMerchantBean  {
	
	private int merchant_id;
	private String store;
	private int merchant_package;
	private int merchant_package_runtime;
	
	public int getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public int getMerchant_package() {
		return merchant_package;
	}
	public void setMerchant_package(int merchant_package) {
		this.merchant_package = merchant_package;
	}
	public int getMerchant_package_runtime() {
		return merchant_package_runtime;
	}
	public void setMerchant_package_runtime(int merchant_package_runtime) {
		this.merchant_package_runtime = merchant_package_runtime;
	}

	
	
}