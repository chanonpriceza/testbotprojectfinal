package botbackend.bean.web;

import java.util.Date;

public class ScheduleBean {

	private String name;
	private Date runTime;
	private int nextTime;
	private String processList;
	private String status;
	private Date fromDate;
	private Date toDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getRunTime() {
		return runTime;
	}
	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}
	public int getNextTime() {
		return nextTime;
	}
	public void setNextTime(int nextTime) {
		this.nextTime = nextTime;
	}
	public String getProcessList() {
		return processList;
	}
	public void setProcessList(String processList) {
		this.processList = processList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
}
