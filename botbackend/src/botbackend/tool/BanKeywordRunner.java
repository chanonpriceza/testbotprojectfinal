package botbackend.tool;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

import javax.sql.DataSource;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import botbackend.utils.ACCPropertyParser;
import botbackend.utils.ParserUtil;
import botbackend.utils.Util;
import utils.BotUtil;

import botbackend.db.BanKeywordDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.db.utils.DatabaseUtil;
import botbackend.manager.BanKeywordManager;
import botbackend.manager.BanKeywordRule;

public class BanKeywordRunner {
	

	public static void main(String[] args) {
		try {
			if(args.length == 0) {
				System.out.println("no <base-config>");
				return;
			}
			// intitail
			ACCPropertyParser propConfig = new ACCPropertyParser(args[0]);
			DataSource ds = createDataSource(propConfig);
			BanKeywordDB banDB = new BanKeywordDB(ds);
			ParserConfigDB pConfigDB = new ParserConfigDB(ds);
			
			// create BanManager
			BanKeywordManager banManager = new BanKeywordManager(banDB);
			
			System.setProperty("sun.net.client.defaultReadTimeout", "15000");
			System.setProperty("sun.net.client.defaultConnectTimeout", "15000");
			BotUtil.LOCALE = "TH";
			String url = "https://www.klook.com/th/activity/19-big-bus-tours-hong-kong/?krt=r22&krid=0ce60d52-7d02-47ee-5863-a84b094481e8";
			String parserClass = Util.DEFAULT_PARSER_CLASS;

			CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));  
			
			List<ParserConfigBean> pConfig = pConfigDB.getByMerchantId(300318);
	        List<ProductDataBean> pdbList = ParserUtil.parse(parserClass, pConfig,new String[] {url});
	        
	        
	        ProductDataBean pdb =  pdbList.get(0);
	        
	        String  name = pdb.getName();
			int merchantId = 0;
			System.out.println("name: "+name);
	        BanKeywordRule result = banManager.isBanKeywordWithKeyword(name,merchantId,pdb.getCategoryId(),pdb.getPrice());
	        if(result!=null) {
	        	System.out.println("BanKeyword: "+result.getKeywordList());
	        	System.out.println("MerchantIds: "+result.getMerchantIds());
	        	System.out.println("NonKeywordList: "+result.getNonKeywordList());
	        	System.out.println("FullKeyword: "+result.getFullKeyword());
	        }
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static DataSource createDataSource(ACCPropertyParser accProp) {
		DataSource ds = null;
		
			String webDBDriverName = accProp.getString("web.db.driver");
			String webDBUrl = accProp.getString("web.db.url");
			String webDBUserName = accProp.getString("web.db.username");
			String webDBPassword = accProp.getString("web.db.password");
			ds =  DatabaseUtil.createDataSource(webDBUrl, webDBDriverName, webDBUserName, webDBPassword, 3);
		
		return ds;
	}
}
