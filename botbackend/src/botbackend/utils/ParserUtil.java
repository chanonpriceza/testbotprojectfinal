package botbackend.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bean.ParserConfigBean;
import bean.ProductDataBean;
import web.parser.image.ImageParserInterface;
import web.parser.HTMLParser;
import web.parser.image.DefaultImageParser;
import utils.FilterUtil;

import botbackend.bean.MerchantConfigBean;
//import botbackend.ParserConfigBean;
import botbackend.component.MerchantConfigComponent;

public class ParserUtil {
	
	public static List<ProductDataBean> parse(String parserClass, List<ParserConfigBean> pConfig, String[] urls) {
		List<ProductDataBean> result = new ArrayList<ProductDataBean>();
		try {
			
			String className = Util.DEFAULT_PARSER_CLASS;
			
			if(StringUtils.isBlank(parserClass)) {
				parserClass = className;
			}
			
			Class<?> c1 = Class.forName(parserClass);
			HTMLParser htmlParserClass = (HTMLParser) c1.getDeclaredConstructor().newInstance();
			
			if(Util.DEFAULT_PARSER_CLASS.equals(parserClass) && pConfig!=null || "User Config".equals(parserClass) && pConfig!=null) {
				htmlParserClass.setParserConfig(pConfig);
			}
			
			for (String url : urls) {
				
				bean.ProductDataBean[] pdbList = htmlParserClass.parse(url);
				if(pdbList==null) {
					continue;
				}
				
				for (bean.ProductDataBean pdb : pdbList) {
					String pictureUrl = pdb.getPictureUrl();
					String pictureData = getPictureData(pictureUrl, url,null);
					pdb.setPictureData(pictureData);
					pdb.setUrl(url);
					result.add(pdb);
				}
	
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
		return result;
	}	
	
	public static Map<String, List<ProductDataBean>> parseGetAllSequence(String parserClass, List<ParserConfigBean> pConfig, String[] urls) {
		Map<String, List<ProductDataBean>> result = new LinkedHashMap<>();
		try {
			
			String className = Util.DEFAULT_PARSER_CLASS;
			
			if(StringUtils.isBlank(parserClass)) {
				parserClass = className;
			}
			
			Class<?> c1 = Class.forName(parserClass);
			HTMLParser htmlParserClass = (HTMLParser) c1.getDeclaredConstructor().newInstance();
			
			if(Util.DEFAULT_PARSER_CLASS.equals(parserClass) && pConfig!=null || "User Config".equals(parserClass) && pConfig!=null) {
				htmlParserClass.setParserConfig(pConfig);
			}
			
			int count=0;
			for (String url : urls) {

				count++;
				
				if(StringUtils.isBlank(url) || !url.startsWith("http")) {
					result.put(count+" "+url,  null);
					continue;
				}
				
				bean.ProductDataBean[] pdbList = htmlParserClass.parse(url);
				if(pdbList==null) {
					result.put(count+" "+url, null);
					continue;
				}
				
				List<ProductDataBean> resultList = new ArrayList<>();
				for (bean.ProductDataBean pdb : pdbList) {
					String pictureUrl = pdb.getPictureUrl();
					String pictureData = getPictureData(pictureUrl, url,null);
					pdb.setPictureData(pictureData);
					pdb.setUrl(url);
					resultList.add(pdb);
				}
				result.put(count+" "+url,  resultList);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> getParserConfig(String data) throws ParseException{
		LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> pConfig = new LinkedHashMap<ParserConfigBean.FIELD, List<String[]>>();
		JSONObject json = (JSONObject)new JSONParser().parse(data);
		for(Object key : json.keySet()){
			String field = (String)key;
			
			List<String[]> list = new ArrayList<String[]>();
			JSONArray jArr = (JSONArray)json.get(key);
			for(Object jData : jArr){
				JSONObject rec = (JSONObject)jData;
				String filter = (String)rec.get("filter");
				String value = (String)rec.get("value");
				String[] split_value = value.split(",");
				for(String val : split_value){
					val = val.replace("|", "");
					if(!val.startsWith("'") && !val.endsWith("'")){
						continue;
					}
					String _tmp = val.substring(1, val.length()-1);
					if(_tmp.trim().isEmpty()){
						value = value.replace(val, _tmp);
					}
				}
				String[] tmp = new String[]{ filter, value};
				list.add(tmp);
			}
			if(!list.isEmpty()){
				pConfig.put(ParserConfigBean.FIELD.valueOf(field), list);
			}
		}
		
		return pConfig;
	}
	
	public static LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> getParserConfig(List<ParserConfigBean> pList){
		if(pList != null && pList.size() > 0){
			LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> pConfig = new LinkedHashMap<ParserConfigBean.FIELD, List<String[]>>();
			for(ParserConfigBean pBean : pList){
				String filter = pBean.getFilterType();
				String field = pBean.getFieldType(); 
				String value = pBean.getValue();
				String[] split_value = value.split(",");
				for(String val : split_value){
					val = val.replace("|", "");
					if(!val.startsWith("'") && !val.endsWith("'")){
						continue;
					}
					String _tmp = val.substring(1, val.length()-1);
					if(_tmp.trim().isEmpty()){
						value = value.replace(val, _tmp);
					}
				}
				
				List<String[]> list = pConfig.getOrDefault(ParserConfigBean.FIELD.valueOf(field), new ArrayList<>());
				String[] tmp = new String[]{ filter, value};
				list.add(tmp);
				pConfig.put(ParserConfigBean.FIELD.valueOf(field), list);
				
			}
			return pConfig;
		}else{
			return null;
		}
	}
	
	public static LinkedHashMap<String, String> getConfigValidate(Map<ParserConfigBean.FIELD, List<String[]>> pConfig){
		LinkedHashMap<String, String> mValidate = new LinkedHashMap<String, String>();
		for(Map.Entry<ParserConfigBean.FIELD, List<String[]>> mNode : pConfig.entrySet()){
			List<String[]> filterType = mNode.getValue();
			int i = 0;
			int allStep = filterType.size();
			
			for(String[] conf : filterType){
				i++;
				ParserConfigBean.TYPE type = ParserConfigBean.TYPE.valueOf(conf[0]);
				String value = conf[1];
				
				boolean isFail = false;
				if(ParserConfigBean.TYPE.textStep == type){
					try{
						int step = Integer.parseInt(value);
						if(step < 0 || step > i){
							isFail = true;
						}
					}catch(Exception e){
						isFail = true;
					}
				}else if(ParserConfigBean.TYPE.ifEqual == type || ParserConfigBean.TYPE.ifNotEqual == type
						|| ParserConfigBean.TYPE.ifContain == type || ParserConfigBean.TYPE.ifNotContain == type){
					
					String[] arr = value.split("\\|,\\|");
					try{
						int jumpStep = Integer.parseInt(arr[1]);
						if(jumpStep > allStep || jumpStep <= i){
							isFail = true;
						}
					}catch(Exception e){
						isFail = true;
					}
				}
				
				if(isFail){
					String nodeType = mNode.getKey().toString();
					mValidate.put(nodeType, type.toString());
					break;
				}
			}
		}
		return mValidate;
	}
	
	public static String getPictureData(String pictureUrl, String currentUrl, Properties merchantConfig){
		
		ImageParserInterface imageParser = new DefaultImageParser();

		if(StringUtils.isNotBlank(pictureUrl)){
			if(!pictureUrl.startsWith("http")){
				try {
					URL url = new URL(new URL(currentUrl), pictureUrl);
					pictureUrl = url.toString();
		    	} catch (MalformedURLException e) {}
			}
			
			if(pictureUrl.startsWith("http")){
				byte[] bPic = imageParser.parse(pictureUrl, 200, 200);
				if(bPic != null){
					bPic = Base64.getEncoder().encode(bPic);
					return new String(bPic);
				}
			}
		}
		return null;
		
	}
	
	public static byte[] parseImage(String imageParserClass, String pictureUrl, int imageWidth, int imageHeight){
		ImageParserInterface imageParser = null;
		byte[] imageByte = null;
		if(imageParserClass == null) {
			imageParser = new DefaultImageParser();
			imageByte = imageParser.parse(pictureUrl, imageWidth, imageHeight);
    	} else {    		
    		try {
				Class<?> c1 = Class.forName(imageParserClass);
				imageParser = (ImageParserInterface) c1.getDeclaredConstructor().newInstance();
				imageByte = imageParser.parse(pictureUrl, imageWidth, imageHeight);
			} catch (Exception e) {
				e.printStackTrace();			
			} 		
    	}
		return imageByte;
	}

	public static Map<String, List<String>> awakeTool1(int merchantId, JSONArray jArr) throws SQLException{
		List<JSONObject> generalList = new ArrayList<>();
		List<JSONObject> basePriceList = new ArrayList<>();
		List<JSONObject> expireList = new ArrayList<>();
		Map<Integer, Map<String, List<String>>> resultMap = new HashMap<>();
		Map<String, List<String>> summaryMap = new HashMap<>();
		
		Properties mConfig = MerchantConfigComponent.loadMerchantConfig(merchantId);
		String parserCharset = mConfig.getProperty(MerchantConfigBean.FIELD.parserCharset.toString());
		if(StringUtils.isBlank(parserCharset)){
			parserCharset = "UTF-8";
		}
		
		for (Object jObj : jArr) {
			JSONObject obj = (JSONObject) jObj;
			String tool1Type = (String) obj.get("tool1Type");
			if(tool1Type.equals("0")){
				generalList.add(obj);
			}else if(tool1Type.equals("1")){
				basePriceList.add(obj);
			}else if(tool1Type.equals("2")){
				expireList.add(obj);
			}
		}
		
		if(expireList != null && expireList.size() > 0){
			for(int i=0; i<expireList.size(); i++){
				try{
					JSONObject obj = expireList.get(i);
					
					String tool1Url 	= (String) obj.get("tool1Url");
					String tool1Expire	= (String) obj.get("tool1Expire");
					
					if(StringUtils.isBlank(tool1Url) || !tool1Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(tool1Url, parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					Document doc = Jsoup.parse(rtnHtml);
					Elements elements = doc.select("*");
					for (Element element : elements) {
						if(StringUtils.isNotBlank(tool1Expire) && element.ownText().equals(tool1Expire)) tool1CreateCheckList(i, resultMap, "tool1Expire", element.cssSelector(), true);
						for(Attribute attribute : element.attributes()){
							if(StringUtils.isNotBlank(tool1Expire) && attribute.getValue().equalsIgnoreCase(tool1Expire))	tool1CreateCheckList(i, resultMap, "tool1Expire", attribute.getKey() + "|,|" + element.cssSelector(), true);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
		if(basePriceList != null && basePriceList.size() > 0){
			for(int i=0; i<basePriceList.size(); i++){
				try{
					JSONObject obj = basePriceList.get(i);
					
					String tool1Url 		= (String) obj.get("tool1Url");
					String tool1Price		= (String) obj.get("tool1Price");
					String tool1BasePrice 	= (String) obj.get("tool1BasePrice");
					
					if(StringUtils.isBlank(tool1Url) || !tool1Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(tool1Url, parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					
					Document doc = Jsoup.parse(rtnHtml);
					Elements elements = doc.select("*");
					for (Element element : elements) {
						if(StringUtils.isNotBlank(tool1Price) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool1Price)))			tool1CreateCheckList(i, resultMap, "tool1PriceB", element.cssSelector(), false);
						if(StringUtils.isNotBlank(tool1BasePrice) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool1BasePrice)))	tool1CreateCheckList(i, resultMap, "tool1BasePrice", element.cssSelector(), false);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		if(generalList != null && generalList.size() > 0){
			try{
				for(int i=0; i<generalList.size(); i++){
					JSONObject obj = generalList.get(i);
					
					String tool1Url 			= (String) obj.get("tool1Url");
					String tool1Name 			= (String) obj.get("tool1Name");
					String tool1Desc 			= (String) obj.get("tool1Desc");
					String tool1Price 			= (String) obj.get("tool1Price");
					String tool1UPC 			= (String) obj.get("tool1UPC");
					String tool1RealProductId 	= (String) obj.get("tool1RealProductId");
					String tool1ConcatProductId = (String) obj.get("tool1ConcatProductId");
					String tool1ConcatWord 		= (String) obj.get("tool1ConcatWord");
					String tool1PictureUrl 		= (String) obj.get("tool1PictureUrl");
					
					if(StringUtils.isBlank(tool1Url) || !tool1Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(Util.encodeURL(tool1Url), parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					
					if(StringUtils.isNotBlank(rtnHtml)){
						Document doc = Jsoup.parse(rtnHtml);
						Elements elements = doc.select("*");
						for (Element element : elements) {
							try{
								if(StringUtils.isNotBlank(tool1Name) && element.ownText().contains(tool1Name)) 														tool1CreateCheckList(i, resultMap, "tool1Name", element.cssSelector(), false);
								if(StringUtils.isNotBlank(tool1Price) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool1Price)))	tool1CreateCheckList(i, resultMap, "tool1PriceG", element.cssSelector(), false);
								if(StringUtils.isNotBlank(tool1Desc)){
									String targetDesc = tool1Desc.replace("&nbsp;", " ").replaceAll("\\s+", " ").trim();
									if(targetDesc.length() > 50) targetDesc = targetDesc.substring(0,50);
									if(FilterUtil.toPlainTextString(element.outerHtml()).replace("&nbsp;", " ").replaceAll("\\s+", " ").trim().contains(targetDesc)) tool1CreateCheckList(i, resultMap, "tool1Desc", element.cssSelector(),true);
								}
								if(StringUtils.isNotBlank(tool1UPC) && element.ownText().contains(tool1UPC)) 														tool1CreateCheckList(i, resultMap, "tool1UPC", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool1RealProductId) && element.ownText().contains(tool1RealProductId)) 									tool1CreateCheckList(i, resultMap, "tool1RealProductId", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool1ConcatProductId) && element.ownText().contains(tool1ConcatProductId)) 								tool1CreateCheckList(i, resultMap, "tool1ConcatProductId", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool1ConcatWord) && element.ownText().contains(tool1ConcatWord)) 											tool1CreateCheckList(i, resultMap, "tool1ConcatWord", element.cssSelector(), true);
								for(Attribute attribute : element.attributes()){ 
									if(StringUtils.isNotBlank(tool1PictureUrl) && ( 
											((attribute.getValue().startsWith("/") && attribute.getValue().contains(FilterUtil.getStringAfterLastIndex(tool1PictureUrl, "/", tool1PictureUrl)))
											||attribute.getValue().equalsIgnoreCase(tool1PictureUrl))))
										tool1CreateCheckList(i, resultMap, "tool1PictureUrl", attribute.getKey() + "|,|" + element.cssSelector(), false);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		summaryMap = tool1Intersection(resultMap);
		summaryMap = tool1MergePrice(summaryMap);
		return summaryMap;
	}
	
	public static Map<String, List<String>> awakeTool2(int merchantId, JSONArray jArr) throws SQLException{
		List<JSONObject> generalList = new ArrayList<>();
		List<JSONObject> basePriceList = new ArrayList<>();
		List<JSONObject> expireList = new ArrayList<>();
		Map<String, List<String>> resultMap = new HashMap<>();
		
		Properties mConfig = MerchantConfigComponent.loadMerchantConfig(merchantId);
		String parserCharset = mConfig.getProperty(MerchantConfigBean.FIELD.parserCharset.toString());
		if(StringUtils.isBlank(parserCharset)){
			parserCharset = "UTF-8";
		}
		
		for (Object jObj : jArr) {
			JSONObject obj = (JSONObject) jObj;
			String tool2Type = (String) obj.get("tool2Type");
			if(tool2Type.equals("0")){
				generalList.add(obj);
			}else if(tool2Type.equals("1")){
				basePriceList.add(obj);
			}else if(tool2Type.equals("2")){
				expireList.add(obj);
			}
		}
		
		if(expireList != null && expireList.size() > 0){
			for(int i=0; i<expireList.size(); i++){
				try{
					JSONObject obj = expireList.get(i);
					
					String tool2Url 	= (String) obj.get("tool2Url");
					String tool2Expire	= (String) obj.get("tool2Expire");
					
					if(StringUtils.isBlank(tool2Url) || !tool2Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(tool2Url, parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					Document doc = Jsoup.parse(rtnHtml);
					Elements elements = doc.select("*");
					for (Element element : elements) {
						if(StringUtils.isNotBlank(tool2Expire) && element.ownText().equals(tool2Expire)) tool2CreateCheckList(resultMap, "tool2Expire", element.cssSelector(), true);
						for(Attribute attribute : element.attributes()){
							if(StringUtils.isNotBlank(tool2Expire) && attribute.getValue().equalsIgnoreCase(tool2Expire))	tool2CreateCheckList(resultMap, "tool2Expire", attribute.getKey() + "|,|" + element.cssSelector(), true);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
		if(basePriceList != null && basePriceList.size() > 0){
			for(int i=0; i<basePriceList.size(); i++){
				try{
					JSONObject obj = basePriceList.get(i);
					
					String tool2Url 		= (String) obj.get("tool2Url");
					String tool2Price		= (String) obj.get("tool2Price");
					String tool2BasePrice 	= (String) obj.get("tool2BasePrice");
					
					if(StringUtils.isBlank(tool2Url) || !tool2Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(tool2Url, parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					
					Document doc = Jsoup.parse(rtnHtml);
					Elements elements = doc.select("*");
					for (Element element : elements) {
						if(StringUtils.isNotBlank(tool2Price) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool2Price)))			tool2CreateCheckList(resultMap, "tool2PriceB", element.cssSelector(), false);
						if(StringUtils.isNotBlank(tool2BasePrice) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool2BasePrice)))	tool2CreateCheckList(resultMap, "tool2BasePrice", element.cssSelector(), false);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		if(generalList != null && generalList.size() > 0){
			try{
				for(int i=0; i<generalList.size(); i++){
					JSONObject obj = generalList.get(i);
					
					String tool2Url 			= (String) obj.get("tool2Url");
					String tool2Name 			= (String) obj.get("tool2Name");
					String tool2Desc 			= (String) obj.get("tool2Desc");
					String tool2Price 			= (String) obj.get("tool2Price");
					String tool2UPC 			= (String) obj.get("tool2UPC");
					String tool2RealProductId 	= (String) obj.get("tool2RealProductId");
					String tool2ConcatProductId = (String) obj.get("tool2ConcatProductId");
					String tool2ConcatWord 		= (String) obj.get("tool2ConcatWord");
					String tool2PictureUrl 		= (String) obj.get("tool2PictureUrl");
					
					if(StringUtils.isBlank(tool2Url) || !tool2Url.startsWith("http")) continue;
					String rtnHtml = Util.httpRequest(Util.encodeURL(tool2Url), parserCharset, true);
					if(StringUtils.isBlank(rtnHtml)) continue;
					
					if(StringUtils.isNotBlank(rtnHtml)){
						Document doc = Jsoup.parse(rtnHtml);
						Elements elements = doc.select("*");
						for (Element element : elements) {
							try{
								if(StringUtils.isNotBlank(tool2Name) && element.ownText().contains(tool2Name)) 														tool2CreateCheckList(resultMap, "tool2Name", element.cssSelector(), false);
								if(StringUtils.isNotBlank(tool2Price) && Util.removeCharNotPrice(element.ownText()).contains(Util.removeCharNotPrice(tool2Price)))	tool2CreateCheckList(resultMap, "tool2PriceG", element.cssSelector(), false);
								if(StringUtils.isNotBlank(tool2Desc)){
									String targetDesc = tool2Desc.replace("&nbsp;", " ").replaceAll("\\s+", " ").trim();
									if(targetDesc.length() > 50) targetDesc = targetDesc.substring(0,50);
									if(FilterUtil.toPlainTextString(element.outerHtml()).replace("&nbsp;", " ").replaceAll("\\s+", " ").trim().contains(targetDesc)) tool2CreateCheckList(resultMap, "tool2Desc", element.cssSelector(),true);
								}
								if(StringUtils.isNotBlank(tool2UPC) && element.ownText().contains(tool2UPC)) 														tool2CreateCheckList(resultMap, "tool2UPC", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool2RealProductId) && element.ownText().contains(tool2RealProductId)) 									tool2CreateCheckList(resultMap, "tool2RealProductId", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool2ConcatProductId) && element.ownText().contains(tool2ConcatProductId)) 								tool2CreateCheckList(resultMap, "tool2ConcatProductId", element.cssSelector(), true);
								if(StringUtils.isNotBlank(tool2ConcatWord) && element.ownText().contains(tool2ConcatWord)) 											tool2CreateCheckList(resultMap, "tool2ConcatWord", element.cssSelector(), true);
								for(Attribute attribute : element.attributes()){ 
									if(StringUtils.isNotBlank(tool2PictureUrl) && ( 
											((attribute.getValue().startsWith("/") && attribute.getValue().contains(FilterUtil.getStringAfterLastIndex(tool2PictureUrl, "/", tool2PictureUrl)))
											||attribute.getValue().equalsIgnoreCase(tool2PictureUrl))))
										tool2CreateCheckList(resultMap, "tool2PictureUrl", attribute.getKey() + "|,|" + element.cssSelector(), false);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		resultMap = tool2MergePrice(resultMap);
		return resultMap;
	}
	
	public static Map<String, List<String>> tool1Intersection(Map<Integer, Map<String, List<String>>> dataMap){
		Map<String, List<String>> rtnMap = new HashMap<>();
		if(dataMap != null && dataMap.size() > 0){
			for (Entry<Integer, Map<String, List<String>>> entry : dataMap.entrySet()) {
				if(entry.getKey() == 0){
					for (Entry<String, List<String>> data : entry.getValue().entrySet()) {
						rtnMap.put(data.getKey(), data.getValue());
					}
				}else{
					for (Entry<String, List<String>> data : entry.getValue().entrySet()) {
						List<String> baseList = rtnMap.getOrDefault(data.getKey(), new ArrayList<String>());
						List<String> compareList = data.getValue();
						baseList.retainAll(compareList);
						rtnMap.put(data.getKey(), baseList);
					}
				}
			}
		}
		
		
		
		return rtnMap;
	}
	
	public static Map<String, List<String>> tool1MergePrice(Map<String, List<String>> dataMap){
		Map<String, List<String>> rtnMap = new HashMap<>(dataMap);
		if(dataMap != null && dataMap.size() > 0){
			List<String> mergeList = new ArrayList<>();
			List<String> basePriceList = dataMap.get("tool1PriceB");
			List<String> priceList = dataMap.get("tool1PriceG");
			if(basePriceList != null && basePriceList.size() > 0){
				mergeList.addAll(basePriceList);
			}
			if(priceList != null && priceList.size() > 0) {
				for (String s : priceList) {
					if(!mergeList.contains(s)){
						mergeList.add(s);
					}
				}
			}
			if(mergeList != null && mergeList.size() > 0){
				rtnMap.put("tool1Price", mergeList);
			}
		}
		return rtnMap;
	}
	
	public static Map<String, List<String>> tool2MergePrice(Map<String, List<String>> dataMap){
		Map<String, List<String>> rtnMap = new HashMap<>(dataMap);
		if(dataMap != null && dataMap.size() > 0){
			List<String> mergeList = new ArrayList<>();
			List<String> basePriceList = dataMap.get("tool2PriceB");
			List<String> priceList = dataMap.get("tool2PriceG");
			if(basePriceList != null && basePriceList.size() > 0){
				mergeList.addAll(basePriceList);
			}
			if(priceList != null && priceList.size() > 0) {
				for (String s : priceList) {
					if(!mergeList.contains(s)){
						mergeList.add(s);
					}
				}
			}
			if(mergeList != null && mergeList.size() > 0){
				rtnMap.put("tool2Price", mergeList);
			}
		}
		return rtnMap;
	}
	
	public static void tool1CreateCheckList(int index, Map<Integer, Map<String, List<String>>> resultMap, String name, String value, boolean depthFirst){
		if(StringUtils.isNotBlank(value)){
			Map<String, List<String>> indexMap = resultMap.getOrDefault(index, new HashMap<String, List<String>>());
			if(indexMap != null){
				List<String> selectorList = indexMap.getOrDefault(name, new ArrayList<>());
				if(!selectorList.contains(value)){
					boolean isAdd = false;
					int countValueSelector = StringUtils.countMatches(value, ">");
					if(selectorList != null && selectorList.size() > 0){
						for (int c=0; c<selectorList.size(); c++) {
							int countSelector = StringUtils.countMatches(selectorList.get(c), ">");
							if(depthFirst){
								if(countValueSelector >= countSelector){
									selectorList.add(c, value);
									isAdd = true;
								}
							}else{
								if(countValueSelector <= countSelector){
									selectorList.add(c, value);
									isAdd = true;
								}
							}
							if(isAdd) break;
						}
					}
					if(!isAdd) selectorList.add(value); 
					indexMap.put(name, selectorList);
				}
				resultMap.put(index, indexMap);
			}
		}
	}
	
	public static void tool2CreateCheckList(Map<String, List<String>> resultMap, String name, String value, boolean depthFirst){
		if(StringUtils.isNotBlank(value)){
			List<String> selectorList = resultMap.getOrDefault(name, new ArrayList<>());
			if(!selectorList.contains(value)){
				boolean isAdd = false;
				int countValueSelector = StringUtils.countMatches(value, ">");
				if(selectorList != null && selectorList.size() > 0){
					for (int c=0; c<selectorList.size(); c++) {
						int countSelector = StringUtils.countMatches(selectorList.get(c), ">");
						if(depthFirst){
							if(countValueSelector >= countSelector){
								selectorList.add(c, value);
								isAdd = true;
							}
						}else{
							if(countValueSelector <= countSelector){
								selectorList.add(c, value);
								isAdd = true;
							}
						}
						if(isAdd) break;
					}
				}
				if(!isAdd) selectorList.add(value); 
				resultMap.put(name, selectorList);
			}
		}
	}

}