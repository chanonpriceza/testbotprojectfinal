package botbackend.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DataEncription {
		
	public static synchronized String encode(String s) {
		
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(s.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);		
		if(hashtext.length() > 16) {
			hashtext = hashtext.substring(10, 16);
		} else if(hashtext.length() > 6) {
			hashtext = hashtext.substring(0, 6);
		}
		
		return hashtext;
	}

	public static synchronized String toMD5(String s) {
		
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(s.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);		
		return hashtext;
	}
	
	public static synchronized boolean validateId(String id) {		
		if(id == null || id.length() < 7) {
			return false;
		}		
		String realId = id.substring(0, id.length()-6);
		String encodedId = id.substring(id.length()-6);		
		if(encodedId.equals(encode(realId))) {
			return true;
		} else {
			return false;
		}		
	}
	
	public static synchronized String generateId(String id) {
		return id + encode(id);
	}
	
	public static String getId(String id) {		
		return id.substring(0, id.length()-6);
	}
	
	public static void main(String[] args) {
		
		System.out.println(toMD5("MATAmata@PZ"));
	}
	
}