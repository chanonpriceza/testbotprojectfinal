package botbackend.utils;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bean.MerchantConfigBean.FIELD;
import bean.WorkloadBean;
import bean.WorkloadBean.STATUS;
import botbackend.bean.UrlPatternBean;
import botbackend.db.WorkLoadFeedDB;
import botbackend.manager.UrlPatternManager;
import db.ProductUrlDB;
import utils.BotUtil;
import web.crawler.bot.CrawlerInterface;
import web.crawler.bot.HeatonURLCrawler;
import web.crawler.bot.URLCrawlerClient;
import web.crawler.bot.URLCrawlerInterface;
import web.crawler.bot.URLUtility;

public class URLCrawlerUtil implements URLCrawlerClient{

	private Logger logger = Logger.getLogger("trackLogger");
	
    protected int count;
    private int merchantId;
    protected ProductUrlDB productUrlDB;    
    protected WorkLoadFeedDB workLoadFeedDB;    
    private WorkloadBean target;
    private UrlPatternManager urlPatternManager;
    private String urlCrawlerCharset;
    private String initCrawlerClass; 
    private String urlCrawlerClass;
    private String parserClass;
    private String enableCookies;
    private boolean ignoreHttpError;
    private boolean skipEncodeURL;
    private URLCrawlerInterface urlCrawler;
    private List<UrlPatternBean> startUrlBeanList;
    private Set<String> contSet;
	private Set<String> matchSet;
	private Set<String> dropSet;
	private Set<String> otherSet;
	private boolean checkDropUrlAnalyzer;
	private boolean checkOtherUrlAnalyzer;
    private JSONArray jsonArr;
	private long startTime;
    private long finishTime; 
    public void initialize (int merchantId, List<UrlPatternBean> startUrlBeanList, List<UrlPatternBean> otherBeanList, Properties merchantConfig) throws Exception {
		
		this.merchantId = merchantId;
		this.startUrlBeanList = startUrlBeanList;
		this.urlPatternManager = new UrlPatternManager(otherBeanList);
		this.jsonArr = new JSONArray();

		urlCrawlerCharset = merchantConfig.getProperty(FIELD.urlCrawlerCharset.toString());
		if(StringUtils.isBlank(urlCrawlerCharset)){
			urlCrawlerCharset = "UTF-8";
		}
		initCrawlerClass = merchantConfig.getProperty(FIELD.initCrawlerClass.toString()); 
		urlCrawlerClass = merchantConfig.getProperty(FIELD.urlCrawlerClass.toString()); 
		parserClass = merchantConfig.getProperty(FIELD.parserClass.toString()); 
		enableCookies = merchantConfig.getProperty(FIELD.enableCookies.toString());
		engine.BaseConfig.MERCHANT_ID = merchantId;
		init();
	}
    
    public void setCheckDropAnalyzer(boolean checkDrop) {
    	checkDropUrlAnalyzer = checkDrop;
    }
    
    public void setCheckOtherUrlAnalyzer(boolean checkOther) {
    	checkOtherUrlAnalyzer = checkOther;
    }
	
    public JSONArray getJsonArr() {
		return jsonArr;
	}

	private void init() throws Exception {
    	
    	Map<String, String> config = new HashMap<String, String>();
    	config.put("enableCookies", enableCookies);
    	config.put("ignoreHttpError", String.valueOf(ignoreHttpError));
    	config.put("startUrl", startUrlBeanList.get(0).getValue());
    	config.put("urlCrawlerCharset", urlCrawlerCharset);
		
        try {
        	if (parserClass!=null) {
        		if (parserClass.contains("TemplateLazadaHaHTMLParser")) {
        			urlCrawlerClass = "com.ha.bot.crawler.impl.LazadaHaURLCrawler";
        		}
        	}
        	
        	if(urlCrawlerClass == null) {
        		urlCrawler = new HeatonURLCrawler(false);
        	} else {
        		
        		try {
    				Class<?> c1 = Class.forName(urlCrawlerClass);
    				urlCrawler = (URLCrawlerInterface) c1.newInstance();
    			} catch (Exception e) {
    				logger.error("Error ", e);	
    			} 
        		
        	}
        	
        	urlCrawler.init(this);
           
        } catch(Exception e){
        	e.printStackTrace();
            logger.error("Error ", e);
        }
    }

    public void process() throws SQLException {
    	
    	if(initCrawlerClass != null) {
    		CrawlerInterface crawler = null;
			try {
				Class<?> c1 = Class.forName(initCrawlerClass);
				crawler = (CrawlerInterface) c1.newInstance();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
				logger.error("Error ", e);	
			}
       	}
    	for(UrlPatternBean starturlBean : startUrlBeanList) {
    		target = new WorkloadBean();
			target.setUrl(starturlBean.getValue());
			target.setDepth(starturlBean.getGroup());
			target.setMerchantId(merchantId);
			target.setCategoryId(starturlBean.getCategoryId());
			target.setKeyword(starturlBean.getKeyword());
			target.setRequestUrl(starturlBean.getValue());
			
			contSet = new HashSet<>();
			matchSet = new HashSet<>();
			dropSet = new HashSet<>();
			otherSet = new HashSet<>();
	        processTarget();
    	}
    }
    
	@SuppressWarnings("unchecked")
	private void processTarget() throws SQLException {
		startTime = 0L;
		finishTime = 0L;
    	String targetUrl = target.getUrl();
        JSONObject rtnJson = new JSONObject();
        rtnJson.put("starturl", targetUrl);
        try {
            
            String encodedtargetUrl = null;
            //Encode url query string            
            if(!skipEncodeURL) {
	            try {
	            	URL normalURL = new URL(targetUrl);			
	            	URI encodedURI = new URI(
	            			normalURL.getProtocol(), 
	            			normalURL.getHost(), 
	            			normalURL.getPath(),
	            			normalURL.getQuery(),
	            			null);
	            	encodedtargetUrl = encodedURI.toString();
	
	            } catch (MalformedURLException e) {
	            	
				} catch (URISyntaxException e) {
					
				}
            }
                           
            if(encodedtargetUrl != null && encodedtargetUrl.trim().length() != 0) {		
				target.setRequestUrl(BotUtil.encodeURL(encodedtargetUrl));				
			} else {
				target.setRequestUrl(BotUtil.encodeURL(targetUrl));
			}
            DecimalFormat df = new DecimalFormat("####0.000"); 
            startTime = System.nanoTime();
			urlCrawler.process(target);	
//		------------------------ Finish timer -----------------------------------------
			if(finishTime != 0L){
				double runtime = (finishTime - startTime) / 1000000000.0;
				rtnJson.put("runtime", df.format(runtime));
			}else{
				rtnJson.put("runtime", df.format(0.0));
			}

			JSONArray contArr = new JSONArray();
			JSONArray matchArr = new JSONArray();
			JSONArray dropArr = new JSONArray();
			JSONArray otherhArr = new JSONArray();
			for(String urlStr : matchSet) {
				JSONObject tmpJson = new JSONObject();
				tmpJson.put("url", urlStr);
				tmpJson.put("action", "Match");
				matchArr.add(tmpJson);
			}
			for(String urlStr : contSet){
				JSONObject tmpJson = new JSONObject();
				tmpJson.put("url", urlStr);
				tmpJson.put("action", "Continue");
				contArr.add(tmpJson);
			}
			if(checkDropUrlAnalyzer) {
				for(String urlStr : dropSet){
					JSONObject tmpJson = new JSONObject();
					tmpJson.put("url", urlStr);
					tmpJson.put("action", "Drop");
					dropArr.add(tmpJson);
				}
			}
			if(checkOtherUrlAnalyzer) {
				for(String urlStr : otherSet){
					JSONObject tmpJson = new JSONObject();
					tmpJson.put("url", urlStr);
					tmpJson.put("action", "Other");
					otherhArr.add(tmpJson);
				}
			}
			
			rtnJson.put("continueUrl", sortJsonArray(contArr));
			rtnJson.put("matchUrl", sortJsonArray(matchArr));

			if(checkDropUrlAnalyzer) {
				rtnJson.put("dropUrl", sortJsonArray(dropArr));
			}
			if(checkOtherUrlAnalyzer) {
				rtnJson.put("otherUrl", sortJsonArray(otherhArr));				
			}

			jsonArr.add(rtnJson);

        } catch (Exception e) {
            e.printStackTrace();
           	logger.error("Error ", e);	
        } finally {

        }
    }
	
	@SuppressWarnings("unchecked")
	public JSONArray sortJsonArray(JSONArray inJson) {
		JSONArray rtnJsonArray = new JSONArray();
		List<JSONObject> jsonList = new ArrayList<>();
		for(int i = 0 ; i < inJson.size() ; i++) {
			jsonList.add((JSONObject) inJson.get(i));
		}
		
		Collections.sort(jsonList, new Comparator<JSONObject>() {
			private final String KEY_NAME = "url";
			
			public int compare(JSONObject a, JSONObject b) {
				String valA = new String();
				String valB = new String();
				
				valA = (String) a.get(KEY_NAME);
				valB = (String) b.get(KEY_NAME);			
				
				return valA.compareTo(valB);
			}
		});
		
		for(int i = 0 ; i < jsonList.size() ; i++) {
			rtnJsonArray.add(jsonList.get(i));
		}
		
		return rtnJsonArray;
	}
	
	@Override
	public void completePage(int merchantId, String url, STATUS status) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void foundInternalLink(int merchantId, String url, int depth, int categoryId, String keyword)
			throws SQLException {
		if((url.toUpperCase().endsWith(".JPG")) ||
                (url.toUpperCase().endsWith(".GIF")) ||
                (url.toUpperCase().endsWith(".PNG")) ||
                (url.toUpperCase().endsWith(".JS")) ||
                (url.toUpperCase().endsWith(".CSS"))) {
                 return;
             }
        	
             url = url.replaceAll("&amp;","&");
             
             url = URLUtility.removeJSessionID(url);
             
            boolean isMatch = urlPatternManager.isPatternMatch(url);
 			boolean isContinue = urlPatternManager.isPatternContinue(url);
 			boolean isDrop = urlPatternManager.isPatternDrop(url);
 			
 			finishTime = System.nanoTime();
            url = urlPatternManager.processRemoveQuery(url);
		if(checkDropUrlAnalyzer && checkOtherUrlAnalyzer) {
			if(isDrop) {
				dropSet.add(url);
			}else {
				if(isMatch) {
					matchSet.add(url);
				}
				if(isContinue) {
					contSet.add(url);
				}                                           	 
			}
			
			if(!isContinue && !isMatch) {
				otherSet.add(url);
			} 
		}else {
			if(isMatch) {
				matchSet.add(url);
			}
			if(isContinue) {
				contSet.add(url);
			}      
		}
        
	}


}
