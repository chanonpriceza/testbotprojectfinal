package botbackend.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

public class AjaxUtil {
	
	public static List genPaging(int pageNum, int numberOfPage) {
		List pagingList = new ArrayList();        
		if(numberOfPage == 0) {
			return pagingList;
		}
		List p = null;
		
    	if(pageNum != 1) {
    		p = new ArrayList();
    		p.add(pageNum - 1);
    		p.add("&lt; Back");
    		pagingList.add(p);
    	}         	
    	if(pageNum > 3) {
    		for (int i = 1; i <= 3; i++) {
    			if(i < numberOfPage) {
    				p = new ArrayList();
    	    		p.add(i);
    	    		p.add(i);	
            		pagingList.add(p);
    			}
			}      		
    	}
    	int start = pageNum - 2;
    	int stop = pageNum + 2;
    	if(start <= 1) {
    		start = 1;
    	} else if(start <= 4) {
    		start = 4;        		
    	}        	
    	if(stop >= numberOfPage) {
    		stop = numberOfPage -1;
    	}        	
    	for (int i = start; i <= stop; i++) {
    		if(i == start && i > 4) {
    			p = new ArrayList();
	    		p.add("...");
	    		p.add("...");       		
        		pagingList.add(p);            		
    		}
    		p = new ArrayList();
    		p.add(i);
    		p.add(i);       		
    		pagingList.add(p);    		
    		if(i == stop && i < numberOfPage -1) {
    			p = new ArrayList();
	    		p.add("...");
	    		p.add("...");        		
        		pagingList.add(p);            		
    		}
		}        	 
    	if(pageNum <= numberOfPage) {
    		p = new ArrayList();
    		p.add(numberOfPage);
    		p.add(numberOfPage);         		
    		pagingList.add(p);  
    	}        	        	
    	if(pageNum != numberOfPage) {
    		p = new ArrayList();
    		p.add(pageNum + 1);  
    		p.add("Next &gt;");     		
    		pagingList.add(p);
    	} 		
		return pagingList;
	}
	
	public static String bootstrapPagging(int currentPage, int totalPages, String url) {
		if(totalPages == 0) {
			return null;
		}
		
		StringBuilder pageList = new StringBuilder();
		pageList.append("<ul class='pagination'>");
		//Previous
		if(currentPage > 3 && currentPage < (totalPages - 2) && (totalPages - 2 > 0)) {
			pageList.append("<li><a href='"+ url +"page="+ (currentPage - 3) +"'>«</a></li>");
		} else if(currentPage > 3 && currentPage > (totalPages - 2) && (totalPages - 5 > 0)) {
			pageList.append("<li><a href='"+ url +"page="+ (totalPages - 5) +"'>«</a></li>");
		} else {
			pageList.append("<li class='disabled'><a href='#'>«</a></li>");
			
			if(currentPage != 3 || totalPages < 6) {
				for(int i = 1; i <= totalPages; i++) {
					
					if(i <= 5) {
						if(currentPage == i) {
							pageList.append("<li class='active'><a href='"+ url +"page="+ i +"'>"+ i +"</a></li>");
						} else {
							pageList.append("<li><a href='"+ url +"page="+ i +"'>"+ i +"</a></li>");
						}
					}
				}
			}
		}
		
		//Number Center
		if(currentPage >= 3 && currentPage <= (totalPages - 3) && totalPages > 5) {
			int start = currentPage - 2;
			for(int i = 0; i < 5; i++) {
				
				if(i == 2) {
					pageList.append("<li class='active'><a href='"+ url +"page="+ currentPage +"'>"+ currentPage +"</a></li>");
					start = currentPage;
				} else {
					pageList.append("<li><a href='"+ url +"page="+ start +"'>"+ start +"</a></li>");
				}
				start++;
			}
		}
		
		//Next
		if(currentPage >= 3 && currentPage < (totalPages - 2)) {
			pageList.append("<li><a href='"+ url +"page="+ (currentPage + 3) +"'>»</a></li>");
		} else if(currentPage < 3 && totalPages > 5) {
			pageList.append("<li><a href='"+ url +"page="+ (currentPage + (6 - currentPage)) +"'>»</a></li>");
		} else {
			
			if(totalPages > 5) {
				int start = currentPage - (currentPage - 6);
				for(int i = 0; i < 5; i++) {
					
					if(start == currentPage) {
						pageList.append("<li class='active'><a href='"+ url +"page="+ currentPage +"'>"+ currentPage +"</a></li>");
						start = currentPage;
					} else {
						pageList.append("<li><a href='"+ url +"page="+ start +"'>"+ start +"</a></li>");
					}
					start++;
				}
			}
			pageList.append("<li class='disabled'><a href='#'>»</a></li>");
		}
		pageList.append("</ul>");
		
		return pageList.toString();
	}
				
	public static void sendJSON(HttpServletResponse response, JSONObject json) throws IOException {
		
		//System.out.println(json.toJSONString());
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println(json);
		out.close();		
	}
	
	public static void sendJSONRedirect(HttpServletResponse response, String url) throws IOException {
		JSONObject json = new JSONObject();		
		Map<Object, Object> header = new HashMap<Object, Object>();		
		header.put("type", "redirect");		
		header.put("url", url);
		json.put("header", header);			
		sendJSON(response, json);		
	}
	
	public static void sendJSONError(HttpServletResponse response, String message) throws IOException {		
		JSONObject json = new JSONObject();		
		Map<Object, Object> header = new HashMap<Object, Object>();		
		header.put("type", "error");		
		header.put("errorTopic", "Request Exception");		
		header.put("errorDetail", message);
		json.put("header", header);			
		sendJSON(response, json);	
	}
	
	public static void sendJSONError(HttpServletResponse response, String topic, String message) throws IOException {		
		JSONObject json = new JSONObject();		
		Map<Object, Object> header = new HashMap<Object, Object>();		
		header.put("type", "error");		
		header.put("errorTopic", topic);		
		header.put("errorDetail", message);
		json.put("header", header);			
		sendJSON(response, json);	
	}
		
	
}
