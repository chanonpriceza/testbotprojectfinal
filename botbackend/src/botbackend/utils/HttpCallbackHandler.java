package botbackend.utils;

public interface HttpCallbackHandler{	
	
	public void processLine(String line);
	
}