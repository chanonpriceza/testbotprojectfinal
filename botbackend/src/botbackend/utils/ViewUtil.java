package botbackend.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ViewUtil {

	private final static int MAX_ACTIVE_TYPE = 15;
	public static int getMaxActiveType() {
		return MAX_ACTIVE_TYPE;
	}
	
	public static String generateShowTaskProcess(String process) {
		String type = "";
		if(process == null){
			return "";
		}
		if(process.equals("feedCatList")){
			type = "List Category";
		}else if(process.equals("feedCountProduct")){
			type = "Count product";
		}else if(process.equals("feedForTestParser")){
			type = "Test Feed Parser";
		}else if(process.equals("botQualityDownload")){
			type = "Get Quality Report";
		}else if(process.equals("checkMissingProduct")){
			type = "Check Missing Product";
		}else if(process.equals("checkExceedProduct")){
			type = "Check Exceed Product";
		}else if(process.equals("checkMissingPic")){
			type = "Check Missing Picture";
		}else if(process.equals("checkPriceNotMatch")){
			type = "Check Price";
		}else if(process.equals("updateDesc")){
			type = "Update Description";
		}else if(process.equals("updateRealProductId")){
			type = "Get RealProductId";
		}else if(process.equals("reCrawling")){
			type = "Add & Remove Product";
		}else if(process.equals("merchantFeedParser")){
			type = "Feed Parser";
		}else if(process.equals("merchantParser")){
			type = "Parser";
		}else if(process.equals("newMerchantShopee")){
			type = "Shopee";
		}else if(process.equals("newMerchantLnwshop")){
			type = "LnwShop API";
		}else if(process.equals("getSupportPerformanceReport")){
			type= "Issue Performance Report";
		}
		else if(process.equals("others")){
			type = "others";
		}
		return type;
	}
	
	public static String generateShowActive(int  active) {
		String strActive ="";
		switch (active) {
	        case 1:  strActive = "Active";
	                 break;
	        case 2:  strActive = "Stop";
	                 break;
	        case 3:  strActive = "Waiting Test";
	                 break;
	        case 4:  strActive = "Waiting Approval";
	                 break;
	        case 5:  strActive = "No change data";
	                 break;
	        case 6:  strActive = "Contact Content";
	                 break;
	        case 7:  strActive = "Contact Dev";
	                 break;
	        case 8:  strActive = "Contact Customer";
	                 break;
	        case 9:  strActive = "Pause Update";
	                 break;
	        case 10: strActive = "Cancel Promote";
	                 break;
	        case 11: strActive = "Lnwshop API";
	                 break;
	        case 12: strActive = "Add manual only";
	        		break;
	        case 13: strActive = "Force Stop";
	        		break;
	        case 14: strActive = "Analyze done";
	        		break;
	        case 15: strActive = "Analyzed Waiting Fix";
    				break;
	        default: strActive = "Inactive";
	        		break;
		}
		return strActive;
	}	
	
	public static String generateDomain(String country) {
		String path = "";
		if(country == null){
			return "";
		}
		if(country.toLowerCase().equals("th")){
			path = "https://www.priceza.com/";
		}else if(country.toLowerCase().equals("id")){
			path = "https://www.priceza.co.id/";
		}else if(country.toLowerCase().equals("my")){
			path = "https://www.priceza.com.my/";
		}else if(country.toLowerCase().equals("ph")){
			path = "https://www.priceza.com.ph/";
		}else if(country.toLowerCase().equals("sg")){
			path = "https://www.priceza.com.sg/";
		}else if(country.toLowerCase().equals("vn")){
			path = "https://www.priceza.com.vn/";
		}
		return path;
	}
	
	public static Date calcurateTargetDate(String assignDate) throws Exception{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(formatter.parse(assignDate));
		int count = 0;
		for(int i=0; i<30; i++) {
			if (c.get(Calendar.DAY_OF_WEEK) != 7 && c.get(Calendar.DAY_OF_WEEK) != 1) {
				count++;
			}
			c.add(Calendar.DAY_OF_WEEK, 1);
			if(count==5) break;
		}
		return c.getTime();
	}
	
	public static int getWorktimeDay(Date targetDate) throws Exception {
		
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTime(new Date());
		end.setTime(targetDate);

		long startLong = start.getTimeInMillis();
		long endLong = end.getTimeInMillis();

		if(startLong == endLong) return 1;
		
		int differentDay = 0;
		while (startLong < endLong) {
			if (start.get(Calendar.DAY_OF_WEEK) != 7 && start.get(Calendar.DAY_OF_WEEK) != 1) {
				differentDay++;
			}
			start.add(Calendar.DATE, 1);
			startLong = start.getTimeInMillis();
		}
		
		return differentDay;
	}
			
}
