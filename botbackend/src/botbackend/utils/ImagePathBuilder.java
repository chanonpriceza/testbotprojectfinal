package botbackend.utils;

import org.apache.commons.lang3.StringUtils;

import botbackend.system.BaseConfig;

public class ImagePathBuilder {
	
	public static String getPath (String fileName) {
		String domain = BaseConfig.SYSTEM_DOMAIN_IMAGE + "/img/product/";		
		return createPath(domain, "", BaseConfig.SYSTEM_DOMAIN_IMAGE_UPLOAD, fileName);
	}

	public static String getPath (String context, String fileName) {
		String domain = BaseConfig.SYSTEM_DOMAIN_IMAGE + context + "/img/product/";
		return createPath(domain, context, BaseConfig.SYSTEM_DOMAIN_IMAGE_UPLOAD, fileName);
	}

	public static String getPath (String domain, String context, String fileName) {
		domain += context + "/img/product/";
		return createPath(domain, context, "", fileName);
	}
	public static String getPath (String domain, String context,String context2, String fileName) {
		domain += context + "/img/product/";
		return createPath(domain, context, context2, fileName);
	}
	private static String createPath(String domain, String context,String context2, String fileName) {
		
		if (StringUtils.isBlank(fileName)) {
			return domain + "default.jpg";
		}
		
		String imagePath = "";
		String[] nameBuff = fileName.split("-");
		String name = nameBuff[0].trim();
		if (name.length() > 0 && StringUtils.isNumeric(name)) {
			imagePath = domain + name + "/" + fileName;
		}
		
		if (imagePath.trim().length() == 0) {
			
			if(StringUtils.isNotBlank(context2) && fileName.contains("/productgroup/")){
				imagePath = context2 + "/img/product/" + fileName;
			}else{
				imagePath = context + "/img/product/" + fileName;
			}
			
		}
		
		return imagePath;
	}
	
}
