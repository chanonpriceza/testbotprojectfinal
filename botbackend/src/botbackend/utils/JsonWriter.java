package botbackend.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonWriter {

	private static JsonWriter instance = new JsonWriter();
	public static JsonWriter getInstance() { return instance; }
	
	private ObjectMapper mapper;

	private JsonWriter() {
		mapper = new ObjectMapper();
	}
	
	public void write(HttpServletResponse res, Object o) throws JsonGenerationException, JsonMappingException, IOException {
		String rawJson = mapper.writeValueAsString(o);

		res.setCharacterEncoding("UTF-8");
		res.setContentType("application/json");
		PrintWriter writer = res.getWriter();
		writer.write(rawJson);
	}
	public void write(HttpServletResponse res, Object o,String nameJson) throws JsonGenerationException, JsonMappingException, IOException {

		String rawJson = "{\""+nameJson+"\":"+mapper.writeValueAsString(o)+"}";
		res.setCharacterEncoding("UTF-8");
		res.setContentType("application/json");
		PrintWriter writer = res.getWriter();
		writer.write(rawJson);
	}
}
