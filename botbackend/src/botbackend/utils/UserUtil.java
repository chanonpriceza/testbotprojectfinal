package botbackend.utils;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import botbackend.bean.UserBean;

public class UserUtil {

	public static boolean checkLogin(HttpServletRequest request) {		
		HttpSession session = request.getSession(false);		
		if(session == null) {
			return false;
		}	
		return true;
	}
	
	public static UserBean getUserSession(HttpServletRequest request) {		
		HttpSession session = request.getSession(false);		
		if(session == null) {
			return null;
		}		
		UserBean userBean = (UserBean)session.getAttribute("userData");
		return userBean;
	}	
	
	public static boolean checkUserSession(HttpServletRequest request, HttpServletResponse response) {
    	HttpSession session = request.getSession(false);		
		if(session == null) {
			return false;
		}		
		UserBean user = (UserBean)session.getAttribute("userData");
		if(user == null) {
	    	return false;
		} 
		
    	return true;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean checkPermission(HttpServletRequest request, String[] permissionList) {
		HttpSession session = request.getSession(false);	
		if(session == null || permissionList == null || permissionList.length == 0) {
			return false;
		}	
		
		HashMap<String, String> permissionmap = (HashMap<String, String>)session.getAttribute("Permission");
		if(permissionmap != null ) {
			for(String permission : permissionList){
				if(permissionmap.containsKey(permission)&&!permissionmap.containsKey("-"+permission)){
					return permissionmap.containsKey(permission);
				}else if(permissionmap.containsKey("all")&&!permissionmap.containsKey("-"+permission)){
					return permissionmap.containsKey("all");
				}else{
					return false;
				}
			}
		} 
		
		return false;
	}
	
}
