package botbackend.utils;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

public class CentralizeUtil {
	
	private static String[] allowOrigin = new String[] {"http://27.254.65.119:8080,http://localhost:8080,http://localhost:8090"};
	
	public static void checkAllowOrigin(String origin,HttpServletResponse response) {
		if(StringUtils.isBlank(origin)) {
			return;
		}
		boolean isAllow = false;
		for(String allow:allowOrigin) {
			if(allow.contains(origin))
				isAllow = true;
			if(isAllow) {  
				response.setHeader("Access-Control-Allow-Origin", origin );
				response.setHeader("Vary", "Origin");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			}
		}
	}
}
