package botbackend.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class ACCPropertyParser {
	private Configuration conf;
	private static Logger logger;
	static {
		logger = Logger.getLogger("priceza.utils");
	}

	public ACCPropertyParser(String propFilePath) throws ConfigurationException {
		conf = new PropertiesConfiguration(propFilePath);					
	}
	
	public ACCPropertyParser(Configuration conf) {
		this.conf = conf;
	}
	
	public boolean getBoolean(String key) {
		return conf.getBoolean(key);		
	}	

	public boolean getBoolean(String key, boolean defaultValue) {
		try {
			return conf.getBoolean(key, defaultValue);
		} catch (Throwable e) {
			logger.warn("Warning: value of boolean "+key+" invalid using "+defaultValue+" as default");
			return defaultValue;
		}
	}

	public int getInt(String key) {
		return conf.getInt(key);		
	}

	public int getInt(String key, int defaultValue) {
		try {
			return conf.getInt(key, defaultValue);
		} catch (Throwable e) {
			logger.warn("Warning: value of int "+key+" invalid using "+defaultValue+" as default");
			return defaultValue;
		}
	}

	public List<String> getList(String key) {
		List<?> list = conf.getList(key);
			if (list == null) {
				return null;
			} else if (list.size() == 0) {
				return new ArrayList<String>();
			} else if (list == null
					|| (list.size() == 1 && list.get(0).equals(""))) {
				return new ArrayList<String>();
			} else {
				List<String> output = new ArrayList<String>();
				for (Object en : list) {
					output.add((String) en);
				}
				return output;
			}		
	}

	public long getLong(String key) {
		return conf.getLong(key);		
	}

	public long getLong(String key, long defaultValue) {
		try {
			return conf.getLong(key, defaultValue);
		} catch (Throwable e) {
			logger.warn("Warning: value of long " + key + " is invalid using "
					+ defaultValue + " as default");
			return defaultValue;
		}
	}

	public String getString(String key) {
		return conf.getString(key);		
	}

	public String getString(String key, String defaultValue) {
		try {
			return conf.getString(key, defaultValue);
		} catch (Throwable e) {
			logger.warn("Warning: value of string "+key+" is invalid using "+defaultValue+" as default");
			return defaultValue;
		}
	}

	public ACCPropertyParser getSubProperties(String prefix) {
		return new ACCPropertyParser(conf.subset(prefix));
	}
	
	/*get private data*/
	
	public Configuration getConfiguration(){
		return conf;
	}

}
