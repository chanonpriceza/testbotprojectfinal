package botbackend.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.NotificationBean;
import utils.BotUtil;
import botbackend.bean.ConfigBean;
import botbackend.bean.NotificationApiBean;
import botbackend.bean.UserBean;
import botbackend.db.ConfigDB;
import botbackend.db.NotificationApiDB;
import botbackend.db.utils.DatabaseManager;
import db.NotificationDB;


public class NotificationFilter implements Filter {
	
	private static final int numRecord = 9;
	private static final int start = 0;
	final static String[] COUNTRY_LIST = new String[] {"TH","VN","ID","SG","MY","PH"};
	final static String SERVICE_URL = "task.service.url.";
	
	@Override
	public void init(FilterConfig config) throws ServletException {
	}
	
	@Override
	public void destroy() {
	}

	 @Override
	 public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
	
		 HttpServletRequest request = (HttpServletRequest) req;
		 HttpServletResponse response = (HttpServletResponse) res;
	
		 HttpSession session = request.getSession(true);
		 UserBean userLoginBean = (UserBean) session.getAttribute("userData");
		 List<NotificationApiBean> notificationBeanList = null;
			 	try {
			 		NotificationApiDB notiDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
					notificationBeanList = notiDB.selectNotiAll(userLoginBean.getUserName(), numRecord);
					int countRead = notiDB.countRead(userLoginBean.getUserName(),0);
					session.setAttribute("notiList",notificationBeanList);
					session.setAttribute("notRead",countRead);
				} catch (Exception e) {
					e.printStackTrace();
				}
		 if(session==null||session.getAttribute("serviceUrl")==null){	 	
			ConfigDB configDB = DatabaseManager.getDatabaseManager().getConfigDB();
			 Map<String,String> services  = new HashMap<String,String>();
				for(String s :COUNTRY_LIST) {
					try {
						ConfigBean bean = configDB.getByName(SERVICE_URL+s.toLowerCase());
						String url = null;
						if(bean!=null) {
							url = bean.getConfigValue();
						}
						if(url!=null) {
							services.put(s.toLowerCase(),url);
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				session.setAttribute("serviceUrl",services);
		 }
		             
	 filterChain.doFilter(request, response);
}
	
}
