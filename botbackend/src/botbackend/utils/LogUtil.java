package botbackend.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class LogUtil {

	
	static Logger textMessageLogger = Logger.getLogger("textMessageLogger");
			
	public static void logEmailSendData(String type,String module,String ref,String sendTo,Boolean status,String message) {
		StringBuilder logStr = new StringBuilder();
		logStr.append("pz-bn|Email:"+ type +"|Module:"+ module +"|Ref:"+ ref +"|sendTo:" + sendTo + "|status:"+status);
		if(StringUtils.isNotBlank(message)){
			logStr.append("|Message:"+message);
		}
		textMessageLogger.info(logStr.toString());
	}

	public static void logNotification(String type,String module,String ref,int userId,Boolean status,String message) {
		StringBuilder logStr = new StringBuilder();
		logStr.append("pz-bn|Notification:"+ type +"|Module:"+ module +"|Ref:"+ ref +"|userId:" + userId + "|status:"+status);
		if(StringUtils.isNotBlank(message)){
			logStr.append("|Message:"+message);
		}
		textMessageLogger.info(logStr.toString());
	}
	
}
