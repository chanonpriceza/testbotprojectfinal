package botbackend.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import botbackend.bean.UserBean;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;

public class AuthorizeFilter implements Filter {

	private UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
	private static final String SECRET = "PRICEZABOT";
	
	@Override
	public void init(FilterConfig config) throws ServletException {
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String userName = "";
		String userPass = "";
		HttpSession session = request.getSession(true);

		UserBean userLoginBean = (UserBean) session.getAttribute("userData");
		
		if(userLoginBean==null) {
			String[] tokenData = getTokenUserNameAndPassword(request, response);
	
			if (tokenData != null) {
				userName = tokenData[0];
				userPass = tokenData[1];
			}

			if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(userPass)) {
				regisUserDataSession(request, userName, userPass,session);
			}
		}
		
		String url = ((HttpServletRequest)request).getRequestURL().toString();
		if(url.contains("/admin/")) {
			userLoginBean = (UserBean) session.getAttribute("userData");
			if (userLoginBean != null) {
				filterChain.doFilter(req, res);
				return;
			}
			response.getWriter().write("Session expired, please login");
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/userlogin"));
		} else if(url.contains("/partner/")) {
			filterChain.doFilter(req, res);
		}
		
	}
	
	private String[] getTokenUserNameAndPassword(HttpServletRequest request,HttpServletResponse response) throws IllegalArgumentException, UnsupportedEncodingException {
		String token = StringUtils.defaultString((String)request.getParameter("bot_token")).trim();
		String[] result = null;
	
		if(StringUtils.isBlank(token))
			return null;
		
		try {
			Algorithm algorithm = Algorithm.HMAC384(SECRET);
		    JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
			DecodedJWT decoder = verifier.verify(token);
			algorithm.verify(decoder);
			String userName =	decoder.getClaim("username").asString();
			String userPass = decoder.getClaim("password").asString();
			result = new String[]{userName,userPass};
			
		}catch (SignatureVerificationException e) {
			System.out.print("Signature JWT doesn't match");
			return null;
		}
		return result;
	}
	
	private void regisUserDataSession(HttpServletRequest request,String userName,String userPass,HttpSession session) {
		try {
			UserBean userLoginBean = (UserBean) userDB.getUserLogin(userName,userPass);
			if (userLoginBean!=null&&userLoginBean.getUserPermission() != null) {
				String[] permission = userLoginBean.getUserPermission().split("\\|");
				List<String> permits = new ArrayList<String>();
				permits = userDB.getRolePermission(permission);

				HashMap<String, String> merchantPermission = new HashMap<String, String>();
				for (String per : permits) {
					if (per.trim().length() != 0) {
						if (!merchantPermission.containsKey(per)) {
							merchantPermission.put(per, "");
						}
					}
				}
				session.setAttribute("Permission", merchantPermission);
			}
			
			session.setAttribute("userData", userLoginBean);
		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute("errormessage", "system exception");
		}
	}
}
