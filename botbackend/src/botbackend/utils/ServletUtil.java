package botbackend.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class ServletUtil {

	public static int stringToInt(String s, int defaultValue) {
		if(s == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;			
		}
	}
	public static int stringToMinMaxInt(String s, int defaultValue,int min,int max) {
		if(s == null) {
			return defaultValue;
		}
		try {
			int num = Integer.parseInt(s.trim());
			if(num < min || num > max){
				return defaultValue;
			}
			return num;
		} catch (NumberFormatException e) {
			return defaultValue;			
		}
	}
	public static double stringToDouble(String s, double defaultValue) {
		if(s == null) {
			return defaultValue;
		}
		try {
			return Double.parseDouble(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;			
		}
	}
	
	public static double convertToDoubleFormat(String s, double defaultValue) {
		if(s == null) {
			return defaultValue;
		}
		try {
			s = s.replaceAll(",", "");
			return Double.parseDouble(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;			
		}
	}
	
	public static Cookie getCookie(HttpServletRequest request, String name) {		
		Cookie[] cookies = request.getCookies();
		if(cookies != null) {
        	for(int loopIndex = 0; loopIndex < cookies.length; loopIndex++) { 
        		Cookie ck = cookies[loopIndex];
	            if (ck.getName().equals(name)) { 
	            	return ck;
	            }
	        }  
        }        
        return null;
	}
	
	public static int getIntParameter(HttpServletRequest request, String name, int defaultValue){
        String p = request.getParameter(name);
        if (p != null) {
            try {
                return Integer.parseInt(p);
            } catch (Exception e) {
            }
        }
        return defaultValue;        
    }
	public static int getIntMinMaxParameter(HttpServletRequest request, String name, int defaultValue,int min,int max){
        String p = request.getParameter(name);
        if (p != null) {
            try {
            	int num = Integer.parseInt(p.trim());
    			if(num < min || num > max){
    				return defaultValue;
    			}
    			return num;
            } catch (Exception e) {
            }
        }
        return defaultValue;        
    }
	public static double getDoubleParameter(HttpServletRequest request, String name, Double defaultValue){
        String p = request.getParameter(name);
        if (p != null) {
            try {
                return Double.parseDouble(p);
            } catch (Exception e) {
            }
        }
        return defaultValue;        
    }
	
	public static String getStringParam(HttpServletRequest req, String name, String defaultValue){
        String p = req.getParameter(name);
        if(p == null) {
            p = defaultValue;
        }
        return p;
    }
	
}
