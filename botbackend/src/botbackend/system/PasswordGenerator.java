package botbackend.system;

import java.util.Random;

import botbackend.utils.RandomString;

public class PasswordGenerator {
	
	public static RandomString randomString = new RandomString();
	private static Random random = new Random();
		
	public static synchronized String generatePassword() {				
		int length = 10 + random.nextInt(3);
		return randomString.get(length);		
	}	
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(PasswordGenerator.generatePassword());
		}		
	}	
}
