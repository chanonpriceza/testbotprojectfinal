package botbackend.system;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;

import botbackend.db.utils.DatabaseFactory;

public class Context {
    private Connection con;

    static {    	
    	try {			
			Class.forName("com.mysql.jdbc.Driver").newInstance();			 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    }
    
    
    public Context(){

    }
    
    public Context(Connection con){
    	this.con = con;
    }

    public void open() throws SQLException{


            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
//            con = DriverManager.getConnection(BaseConfig.PRICEZA_URL, BaseConfig.PRICEZA_USER, BaseConfig.PRICEZA_PASSWORD);
            con = DatabaseFactory.getDataSourceInstance().getConnection();
            
    }

    public Connection getConnection(){
        return this.con;
    }

    public void setAutoCommit(boolean flag) throws SQLException{
        con.setAutoCommit(flag);
    }

    public void commit() throws SQLException{
        con.commit();
    }

    public void rollback() throws SQLException{
        con.rollback();
    }

    public PreparedStatement getPreparedStatement(String str) throws SQLException {
        return con.prepareStatement(str);
    }


    public void close()
    {
        try {
            if (con != null) { con.close(); }
            con = null;
        } catch (Exception e) {

        }
    }

    public Statement getStatement() throws SQLException{
            return con.createStatement();
    }


}
