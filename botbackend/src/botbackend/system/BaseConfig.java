package botbackend.system;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import botbackend.utils.ACCPropertyParser;
import utils.BotUtil;

import botbackend.component.BotServerListComponent;


public class BaseConfig {
	
	public static final String COOKIE_DOMAIN_NAME = ".priceza.com";
	public static final int LOGIN_COOKIE_AGE = 60*60*24*30;
	
	public static String DB_CONFIG_DRIVER ="";
	public static String DB_CONFIG_URL ="";
	public static String DB_CONFIG_USER = "";
	public static String DB_CONFIG_PASSWORD = "";	
	
	public static String WEB_PROD_CONFIG_DRIVER ="";
	public static String WEB_PROD_CONFIG_URL ="";
	public static String WEB_PROD_CONFIG_USER = "";
	public static String WEB_PROD_CONFIG_PASSWORD = "";	
	public static String DOWNLOAD_PATH = "";
	
	public static String EMAIL_SENDER = "";	
	public static String EMAIL_HOST ="";
	public static String EMAIL_USERNAME = "";
	public static String EMAIL_PASSWORD = "";	
	
	public static String AWS_SES_ACCESS_KEY = "";
	public static String AWS_SES_SECRET_KEY = "";
	public static String MANDRILL_ACCESS_KEY = "";
	
	public static String SYSTEM_DOMAIN_IMAGE = "";
    public static String SYSTEM_DOMAIN_SCRIPT = "";
    public static String SYSTEM_DOMAIN_IMAGE_UPLOAD = "";
    
    public static String FEED_DESTINATION_PATH = "";
    
    public static String BOT_SERVER_LIST_FILE_PATH="";
    
    public static List<String> BOT_ACTIVE_SERVER_LIST = null;
    public static List<String> BOT_ACTIVE_DATA_SERVER_LIST = null;
    public static List<String> BOT_SERVER_LIST_COUNTRY = null;
    public static String BOT_SERVER_CURRENT_COUNTRY = "";
    
	public static int DB_CONFIG_INIT_SIZE = 8;
	public static int DB_CONFIG_MAX_ACTIVE = 20;
	public static int DB_CONFIG_MAX_SIZE = 8;
	public static int DB_CONFIG_MIN_SIZE = 0;
	
	public static int WEB_PROD_CONFIG_INIT_SIZE = 2;
	public static int WEB_PROD_CONFIG_MAX_ACTIVE = 5;
	public static int WEB_PROD_CONFIG_MAX_SIZE = 2;
	public static int WEB_PROD_CONFIG_MIN_SIZE = 0;

	public static String UPLOAD_FILE_PATH = "";
		
    static {
    	
    	ConfigLoader config = ConfigLoader.getInstance();
    	ACCPropertyParser accCon = config.getPropConfig();
    	
    	System.out.println("############# Load config ##################");
    	
	    String file = accCon.getString("log4j-config-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	      System.out.println("BaseConfig: init log4j");	    	
	    } else {
	      System.out.println("BaseConfig: error no log4j-config-file");	    		
	    }
    	    	
	    DB_CONFIG_DRIVER = accCon.getString("web.db.driver");
	    DB_CONFIG_URL = accCon.getString("web.db.url");
	    DB_CONFIG_USER = accCon.getString("web.db.username");
	    DB_CONFIG_PASSWORD = accCon.getString("web.db.password");
    	  
	    DB_CONFIG_INIT_SIZE = accCon.getInt("web.db.init-size", DB_CONFIG_INIT_SIZE);
	    DB_CONFIG_MAX_ACTIVE = accCon.getInt("web.db.max-active", DB_CONFIG_MAX_ACTIVE);
	    DB_CONFIG_MAX_SIZE = accCon.getInt("web.db.max-idle", DB_CONFIG_MAX_SIZE);
	    DB_CONFIG_MIN_SIZE = accCon.getInt("web.db.min-idle", DB_CONFIG_MIN_SIZE);
	    
	    WEB_PROD_CONFIG_DRIVER = accCon.getString("web.prod.db.driver");
	    WEB_PROD_CONFIG_URL = accCon.getString("web.prod.db.url");
	    WEB_PROD_CONFIG_USER = accCon.getString("web.prod.db.username");
	    WEB_PROD_CONFIG_PASSWORD = accCon.getString("web.prod.db.password");
    	  
	    WEB_PROD_CONFIG_INIT_SIZE = accCon.getInt("web.prod.db.init-size", WEB_PROD_CONFIG_INIT_SIZE);
	    WEB_PROD_CONFIG_MAX_ACTIVE = accCon.getInt("web.prod.db.max-active", WEB_PROD_CONFIG_MAX_ACTIVE);
	    WEB_PROD_CONFIG_MAX_SIZE = accCon.getInt("web.prod.db.max-idle", WEB_PROD_CONFIG_MAX_SIZE);
	    WEB_PROD_CONFIG_MIN_SIZE = accCon.getInt("web.prod.db.min-idle", WEB_PROD_CONFIG_MIN_SIZE);
    	    	
//    	EMAIL_HOST = accCon.getString("mail.host");
//    	EMAIL_USERNAME = accCon.getString("mail.username");
//    	EMAIL_PASSWORD = accCon.getString("mail.password");
    	    	    	
//    	EMAIL_SENDER = accCon.getString("mail.sender");
//    	MANDRILL_ACCESS_KEY = accCon.getString("mandrill.accesskey");
//    	AWS_SES_ACCESS_KEY = accCon.getString("awsses.accesskey");
//    	AWS_SES_SECRET_KEY = accCon.getString("awsses.secretkey");
    	
    	SYSTEM_DOMAIN_IMAGE = accCon.getString("system.domain.image");
    	SYSTEM_DOMAIN_SCRIPT = accCon.getString("system.domain.script", "");
    	SYSTEM_DOMAIN_IMAGE_UPLOAD = accCon.getString("system.domain.image-upload", "");
    	
    	FEED_DESTINATION_PATH = accCon.getString("feed.destination.path", "");
    	
    	BOT_SERVER_LIST_COUNTRY = accCon.getList("bot.server-list-country");
    	BOT_SERVER_CURRENT_COUNTRY = accCon.getString("bot.server-current-country");
    	BOT_SERVER_LIST_FILE_PATH = accCon.getString("bot.server.list.file.path", "");
    	
    	UPLOAD_FILE_PATH = accCon.getString("upload-file-path", "");
    	
    	BotUtil.LOCALE = BOT_SERVER_CURRENT_COUNTRY;
    	
    	if (BOT_ACTIVE_SERVER_LIST==null) {
    		try {
    			BOT_ACTIVE_SERVER_LIST = BotServerListComponent.getActiveServerList();
    		} catch (SQLException e) {
    			System.out.println("Cant' get Server List");
    		}
    	}
    	
    	if (BOT_ACTIVE_DATA_SERVER_LIST == null) {
    		try {
    			BOT_ACTIVE_DATA_SERVER_LIST = BotServerListComponent.getActiveDataServerList();
    		} catch (SQLException e) {
    			System.out.println("Cant' get Data Server List");
    		}
    	}
    }
    
}
