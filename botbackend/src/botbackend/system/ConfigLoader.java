package botbackend.system;

import java.net.URL;
import org.apache.commons.configuration.ConfigurationException;

import botbackend.utils.ACCPropertyParser;
import utils.FilterUtil;


public class ConfigLoader {
	
	
	private static ConfigLoader conLoader;
	private static final String DEFAULT_PROP_FILENAME = "priceza-bot-backend.properties";
	private static final String DEFAULT_PROP_PATH = "D:/WebConfig/";
	
	private ACCPropertyParser propConfig;
	
	public static synchronized ConfigLoader getInstance() {
		if (conLoader == null) {
			conLoader = new ConfigLoader();
		}
		return conLoader;
	}
	
	public ConfigLoader() {
		String file = DEFAULT_PROP_FILENAME;
		String path = getClass().getResource("/").getPath();
		path = FilterUtil.getStringBetween(path, "botbackend", "/WEB-INF");
		if(!path.isEmpty()) {
			path = "-" + path + ".";
			file = file.replace(".", path);
		}
		
		try {
			propConfig = new ACCPropertyParser(DEFAULT_PROP_PATH + file);
			System.out.println("Finish loading config file from " + DEFAULT_PROP_PATH + file);
		} catch (ConfigurationException e) {
			System.out.println("Error loading config file from " + DEFAULT_PROP_PATH + file);
			System.out.println(e.getMessage());
		}
		
		if(propConfig == null) {
			URL url = Thread.currentThread().getContextClassLoader().getResource(file);	
			try {
				propConfig = new ACCPropertyParser(url.getPath());
				System.out.println("Finish loading config file from classpath");
			} catch (ConfigurationException e) {
				System.out.println("Error loading config file from classpath");
				e.printStackTrace();
			}		
		}		
	}		

	public ACCPropertyParser getPropConfig() {
		return propConfig;
	}
}