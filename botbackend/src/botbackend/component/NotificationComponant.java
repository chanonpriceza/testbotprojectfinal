package botbackend.component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import bean.NotificationBean;
import db.NotificationDB;

import botbackend.bean.MerchantBean;
import botbackend.bean.TaskBean;
import botbackend.bean.TaskDetailBean;
import botbackend.bean.UserBean;
import botbackend.db.MerchantDB;
import botbackend.db.TaskDetailDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;

@Component
public class NotificationComponant {
	
	public static void insertNotiWithServer(HttpSession session,int merchantId,String server,String action) throws SQLException{
		UserBean userBean = (UserBean)session.getAttribute("userData");
		if(userBean == null) return;
		
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		MerchantBean merchantBean = merchantDB.getMerchantByIdAndServerWithOwner(merchantId, server); 
		
		if(!userBean.getUserName().equals(merchantBean.getOwnerName())) {
			
			List<NotificationBean> notiList = new ArrayList<NotificationBean>();
			
			StringBuilder message = new StringBuilder();
			message.append(StringUtils.capitalize(userBean.getUserName()));
			message.append(" ");
			message.append(action);
			message.append(" - ");
			message.append("Merchant id");
			message.append(" ");
			message.append(merchantBean.getId());
			message.append(" : ");
			message.append(merchantBean.getName());
			
			if(merchantBean.getOwnerId() != 0){
				NotificationBean mainBean = new NotificationBean();
				mainBean.setMessage(message.toString());
				mainBean.setUser(String.valueOf(merchantBean.getOwnerId()));
				mainBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
				notiList.add(mainBean);
			}
			
			if(StringUtils.isNotBlank(merchantBean.getCoreUserConfig())){
				String[] userIdArray = merchantBean.getCoreUserConfig().split("\\|");
				if(userIdArray != null && userIdArray.length > 0){
					for (String userIdStr : userIdArray) {
						if(!String.valueOf(merchantBean.getOwnerId()).equals(userIdStr)) {
							NotificationBean notiBean = new NotificationBean();
							notiBean.setUser(String.valueOf(userIdStr));
							notiBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
							notiBean.setMessage(message.toString());	
							notiList.add(notiBean);							
						}
					}
				}
			}

			insertBatch(notiList);
		}
	}
	public static void insertNotiWithoutServer(HttpSession session,int merchantId,String action) throws SQLException{
		UserBean userBean = (UserBean)session.getAttribute("userData");
		if(userBean == null) return;
		
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		List<MerchantBean> merchantData = merchantDB.getMerchantListByIdWithOwner(merchantId);
		MerchantBean merchantBean = null;
		for(MerchantBean merchant : merchantData){
    		if(merchantBean == null){ merchantBean = merchant; }
    	}
		if(!userBean.getUserName().equals(merchantBean.getOwnerName())) {
			
			List<NotificationBean> notiList = new ArrayList<NotificationBean>();
			
			StringBuilder message = new StringBuilder();
			message.append(StringUtils.capitalize(userBean.getUserName()));
			message.append(" ");
			message.append(action);
			message.append(" - ");
			message.append("Merchant id");
			message.append(" ");
			message.append(merchantBean.getId());
			message.append(" : ");
			message.append(merchantBean.getName());
			
			if(merchantBean.getOwnerId() != 0){
				NotificationBean mainBean = new NotificationBean();
				mainBean.setMessage(message.toString());
				mainBean.setUser(String.valueOf(merchantBean.getOwnerId()));
				mainBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
				notiList.add(mainBean);
			}
			if(StringUtils.isNotBlank(merchantBean.getCoreUserConfig())){
				String[] userIdArray = merchantBean.getCoreUserConfig().split("\\|");
				if(userIdArray != null && userIdArray.length > 0){
					for (String userIdStr : userIdArray) {
						if(!String.valueOf(merchantBean.getOwnerId()).equals(userIdStr)) {
							NotificationBean notiBean = new NotificationBean();
							notiBean.setUser(String.valueOf(userIdStr));
							notiBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
							notiBean.setMessage(message.toString());	
							notiList.add(notiBean);							
						}
					}
				}
			}

			insertBatch(notiList);
		}
	}

	public static void insertNotiInTaskManager(HttpSession session,TaskBean taskData ,String action) throws SQLException{
			UserBean userBean = (UserBean)session.getAttribute("userData");
			if(userBean == null) return;
			
			if(!userBean.getUserName().equals(taskData.getOwner())) {
				
				List<NotificationBean> notiList = new ArrayList<NotificationBean>();
				
				StringBuilder message = new StringBuilder();
				message.append(StringUtils.capitalize(userBean.getUserName()));
				message.append(" ");
				message.append(action);
				message.append(" - ");
				message.append("Task id");
				message.append(" ");
				message.append(taskData.getId());
				message.append(" : ");
				message.append(taskData.getIssue());
				
				UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
				UserBean ownerUserBean = userDB.getUserIDbyUserName(taskData.getOwner());
				if(ownerUserBean != null){
					NotificationBean mainBean = new NotificationBean();
					mainBean.setMessage(message.toString());
					mainBean.setUser(String.valueOf(ownerUserBean.getUserId()));
					mainBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
					notiList.add(mainBean);					
				}
				
				TaskDetailDB taskDetailDB = DatabaseManager.getDatabaseManager().getTaskDetailDB();
				List<TaskDetailBean> taskDetailBean = taskDetailDB.getOwnerByTaskId(taskData.getId());
				if(taskDetailBean != null && taskDetailBean.size() > 0) {
					for (TaskDetailBean listTaskDetailBean : taskDetailBean) {
						if(!listTaskDetailBean.getOwner().equals(userBean.getUserName()) && !listTaskDetailBean.getOwner().equals(ownerUserBean.getUserName())) {
							NotificationBean notiBean = new NotificationBean();
							notiBean.setUser(String.valueOf(listTaskDetailBean.getOwnerId()));
							notiBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
							notiBean.setMessage(message.toString());	
							notiList.add(notiBean);							
						}
					}
				}
				
				
				insertBatch(notiList);
			}
		}
	
	public static void insertNotiWithSchedule(int ownerId, TaskBean taskData,String action, NotificationDB notiDB) throws SQLException{
		if(taskData == null) return;	
		
		List<NotificationBean> notiList = new ArrayList<NotificationBean>();
		StringBuilder message = new StringBuilder();
		message.append("SYSTEM");
		message.append(" ");
		message.append(action);
		message.append(" - ");
		message.append("Task id");
		message.append(" ");
		message.append(taskData.getId());
		message.append(" : ");
		message.append(taskData.getIssue());
			
		
		NotificationBean mainBean = new NotificationBean();
		mainBean.setMessage(message.toString());
		mainBean.setUser(String.valueOf(ownerId));
		mainBean.setStatus(NotificationBean.STATUS.UNREAD.toString());
		notiList.add(mainBean);

		insertBatch(notiList, notiDB);
	}
	
	public static void insertBatch(List<NotificationBean> notiList)throws SQLException{
		insertBatch(notiList, null);
	}
	
	public static void insertBatch(List<NotificationBean> notiList, NotificationDB notiDB)throws SQLException{
		int i = 0;
		Object[][] obj = new Object[notiList.size()][];
		for (NotificationBean notiDataBean : notiList) {
			obj[i] = new Object[]{ 
					notiDataBean.getUser(),
					notiDataBean.getStatus(),
					notiDataBean.getMessage()
			};
			i++;
		}
		
		if(notiDB == null) {
			notiDB = DatabaseManager.getDatabaseManager().getNotificationDB();
		}
		
		notiDB.insertNotiDataBatch(obj);
	}
	
	public static void userUpdateMerchantAndNotification(HttpSession session,int merchantId,String action) throws SQLException{
		UserBean userBean = (UserBean)session.getAttribute("userData");
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		merchantDB.setRecentUpdate(merchantId, null, userBean.getUserName());
		NotificationComponant.insertNotiWithoutServer(session, merchantId ,action);
	} 
	
	public static void userUpdateMerchantAndNotification(HttpSession session,int merchantId,String server,String action) throws SQLException{
		UserBean userBean = (UserBean)session.getAttribute("userData");
		MerchantDB merchantDB = DatabaseManager.getDatabaseManager().getMerchantDB();
		merchantDB.setMerchantRecentUpdate(merchantId, server, userBean.getUserName());
		NotificationComponant.insertNotiWithServer(session, merchantId,server ,action);
	}
	
	
}
