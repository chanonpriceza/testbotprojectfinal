package botbackend.component;

import java.sql.SQLException;
import java.util.List;

import botbackend.db.BotConfigDB;
import botbackend.db.utils.DatabaseFactory;

public class BotServerListComponent {
	
	public static List<String> getActiveServerList() throws SQLException  {
		BotConfigDB botConfigDB = new BotConfigDB(DatabaseFactory.getDataSourceInstance());
		List<String> list = botConfigDB.getActiveServerNameList();
		return list;
	}
	
	public static List<String> getActiveDataServerList() throws SQLException  {
		BotConfigDB botConfigDB = new BotConfigDB(DatabaseFactory.getDataSourceInstance());
		List<String> list = botConfigDB.getActiveDataServerNameList();
		return list;
	}
}
