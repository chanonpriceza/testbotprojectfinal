package botbackend.component;

import java.sql.SQLException;

import botbackend.db.MonitorDB;
import botbackend.db.utils.DatabaseManager;

public class MonitorConfigComponent {
	private static final int status = 1;
	
	public static int countMonitor() throws SQLException  {
		MonitorDB monitordb = DatabaseManager.getDatabaseManager().getMonitorDB();
		int count = monitordb.countIssuesReport(status);
		return count;
	}
}
