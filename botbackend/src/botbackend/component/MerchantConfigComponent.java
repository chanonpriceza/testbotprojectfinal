package botbackend.component;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Component;

import botbackend.bean.MerchantConfigBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.utils.DatabaseManager;

@Component
public class MerchantConfigComponent {

	public static Properties loadMerchantConfig(int merchantId) throws SQLException {
		Properties rtn = new Properties();
		MerchantConfigDB mcDB = DatabaseManager.getDatabaseManager().getMerchantConfigDB();
		List<MerchantConfigBean> mcList = mcDB.findMerchantConfig(merchantId);
		
		for (MerchantConfigBean mcBean : mcList) {
			rtn.setProperty(mcBean.getField(), mcBean.getValue());
		}		
		return rtn;
	}
	
}
