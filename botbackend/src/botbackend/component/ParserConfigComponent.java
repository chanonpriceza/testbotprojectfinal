package botbackend.component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import bean.ParserConfigBean;
import botbackend.db.ParserConfigDB;
import botbackend.db.utils.DatabaseManager;

@Component
public class ParserConfigComponent {
	public  static LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> getParserConfig(int merchantId) throws  SQLException{
		LinkedHashMap<ParserConfigBean.FIELD, List<String[]>> pConfig = new LinkedHashMap<ParserConfigBean.FIELD, List<String[]>>();
		ParserConfigDB parserConfigDB = DatabaseManager.getDatabaseManager().getParserConfigDB();
		List<ParserConfigBean> pConfigList  =  parserConfigDB.getByMerchantId(merchantId);
		if(pConfigList != null && pConfigList.size() > 0){			
			for(ParserConfigBean pBean : pConfigList){
				List<String[]> list = new ArrayList<String[]>();
				String[] tmp = new String[]{ pBean.getFilterType(), pBean.getValue()};
				if(pConfig.get(ParserConfigBean.FIELD.valueOf(pBean.getFieldType())) != null){
					list =  pConfig.get(ParserConfigBean.FIELD.valueOf(pBean.getFieldType()));
				}
				list.add(tmp);
				
				if(!list.isEmpty()){
					pConfig.put(ParserConfigBean.FIELD.valueOf(pBean.getFieldType()), list);
				}
			}
		}	
		return pConfig;
	}
	
}
