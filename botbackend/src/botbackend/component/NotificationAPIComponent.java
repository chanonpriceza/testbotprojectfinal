package botbackend.component;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;


import botbackend.bean.ConfigBean;
import botbackend.bean.NotificationApiBean;
import botbackend.bean.NotificationApiBean.DESTYPE;
import botbackend.bean.UserBean;
import botbackend.db.ConfigDB;
import botbackend.db.NotificationApiDB;
import botbackend.db.UserDB;
import botbackend.db.utils.DatabaseManager;
import botbackend.system.BaseConfig;

public class NotificationAPIComponent {

	public enum SERVICES{
		PUTNOTIFICATION,READNOTIFICATION
	}
	
	protected String country;
	protected static boolean isActivate = false;
	public	static boolean isLocal = false;
	protected String serviceUrl;
	protected String token;
	protected String serviceUsername ;
	protected String servicePassword ;
	protected static String SECRET = "PRICEZABOT";
	protected static int JWT_TIMEOUT = 1000*5*60;
	private static final Logger logger = LogManager.getRootLogger();
	
	
	private static final NotificationAPIComponent notificationComponent = new NotificationAPIComponent();

	public static NotificationAPIComponent getInstance() {
		return notificationComponent;
	}
	
	private NotificationAPIComponent() {
		initialize();
	}
	
	public void initialize() {
		try {
			ConfigDB mcDB = DatabaseManager.getDatabaseManager().getConfigDB();
			ConfigBean url = mcDB.getByName("service.notification.url");
			ConfigBean userName = mcDB.getByName("service.notification.username");
			ConfigBean userPassword = mcDB.getByName("service.notification.password");
			if(StringUtils.isNotBlank(BaseConfig.BOT_SERVER_CURRENT_COUNTRY)) {
				this.country = BaseConfig.BOT_SERVER_CURRENT_COUNTRY.toUpperCase();
			}else {
				this.country = "TH";
			}
			
			if(this.country.equals("TH")) {
				isLocal = true;
			}
			
			if(url!=null&&userName!=null&&userPassword!=null) {	
					String urlString =  url.getConfigValue();
					String userNameString  = userName.getConfigValue();
					String userPasswordString = userPassword.getConfigValue();
				if(StringUtils.isNotBlank(urlString)&&StringUtils.isNotBlank(userNameString)&&(StringUtils.isNotBlank(userPasswordString))) {
					this.serviceUrl = urlString;
					this.serviceUsername = userNameString;
					this.servicePassword = userPasswordString;
					System.out.println(("Notification System is Activated !!!"));
					isActivate = true;
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void generateTokken() {
		Builder jwt = JWT.create();
		jwt.withIssuer("auth0");
		jwt.withClaim("username",serviceUsername);
		jwt.withClaim("password",toMD5(servicePassword));
		jwt.withExpiresAt(new Date(System.currentTimeMillis() + JWT_TIMEOUT));
		String token = jwt.sign(Algorithm.HMAC384(SECRET));
		this.token = token;

	}
	
	public  boolean putNoti(NotificationApiBean noti) {
		generateTokken();
		HttpURLConnection conn = null;
		boolean success = false;
		noti.setCountry(this.country);
		String charset = "UTF-8";
		String putNotificationURL = this.serviceUrl+SERVICES.PUTNOTIFICATION.toString().toLowerCase();
		try {
			URI bulder = new URIBuilder(putNotificationURL).addParameter("notiData",noti.toJSONString()).addParameter("bot_token",this.token).build();
			URL u = new URL(bulder.toString());
			conn = (HttpURLConnection)  u.openConnection();
			conn.setInstanceFollowRedirects(true);	
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write(noti.toJSONString().getBytes());
			
			int status = conn.getResponseCode();
			success = status < HttpURLConnection.HTTP_BAD_REQUEST;
			if(!success)
				return false;
			 
			boolean gzip = false;
			String encode = conn.getContentEncoding();
			if(encode != null)
				gzip = encode.equals("gzip");
			
			try (InputStream is = conn.getInputStream();
				InputStreamReader isr = charset == null ? 
						new InputStreamReader(gzip ? new GZIPInputStream(is) : is) : 
							new InputStreamReader(gzip ? new GZIPInputStream(is) : is, charset);
				BufferedReader brd = new BufferedReader(isr);) {
				if(status < HttpURLConnection.HTTP_BAD_REQUEST )
					return true;
			}
		}catch (Exception e) {
			e.printStackTrace();
			success = false;
		}finally {
			if(!success)
				consumeInputStreamQuietly(conn.getErrorStream());
			if(conn != null)
				conn.disconnect();
		}
		return false;
	}
	

	public static synchronized String toMD5(String s) {
		
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(s.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);		
		return hashtext;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	

	public static boolean putNotificationDirect(NotificationApiBean notiBean){
		
		if(notiBean==null) {
			return false;
		}
		
		try {
			NotificationApiDB mcDB = DatabaseManager.getDatabaseManager().getNotiApiDB();
			if(notiBean.getDestinationType().equals(DESTYPE.GROUP.toString())) {
				UserDB userDB = DatabaseManager.getDatabaseManager().getUserDB();
				List<UserBean> us = userDB.getUserListRole(notiBean.getDestination());
				if(us!=null&&us.size()>0) {
					for(UserBean u:us) {
						notiBean.setDestination(u.getUserName());
						notiBean.setDestinationType(DESTYPE.INDIVIDUAL.toString());
						mcDB.insertNotification(notiBean);
					}
				}
			}
			else{
				if(notiBean!=null) {
					mcDB.insertNotification(notiBean);
				}
			}	
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	
	
}
