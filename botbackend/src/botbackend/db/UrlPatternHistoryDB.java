package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;

import botbackend.bean.UrlPatternHistoryBean;
import botbackend.db.utils.DatabaseUtil;

public class UrlPatternHistoryDB extends UrlPatternHistoryBean {
	QueryRunner queryRunner;
	
	public UrlPatternHistoryDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
	}
	
	private static final String INSERT_HISTORY_BATCH = "INSERT INTO `tbl_url_pattern_history` ("
			+ "`merchantId`, `condition`, `group`, `value`, `action`, `categoryId`, `keyword`, `userId`, `userAction`, `userActionDetail`, `updateDate`"
			+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
	public int[] insertHistoryBatch(List<UrlPatternHistoryBean> historyList, int merchantId) throws SQLException {
		
		Object[][] objList = new Object[historyList.size()][10];
		for(int i = 0; i<historyList.size(); i++){
			objList[i][0] = merchantId;
			objList[i][1] = historyList.get(i).getCondition();
			objList[i][2] = historyList.get(i).getGroup();
			objList[i][3] = historyList.get(i).getValue();
			objList[i][4] = historyList.get(i).getAction();
			objList[i][5] = historyList.get(i).getCategoryId();
			objList[i][6] = historyList.get(i).getKeyword();
			objList[i][7] = historyList.get(i).getUserId();
			objList[i][8] = historyList.get(i).getUserAction();
			objList[i][9] = historyList.get(i).getUserActionDetail();
		}
		
		return queryRunner.batch(INSERT_HISTORY_BATCH, objList);
	}	
	
}
