package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.MerchantConfigBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;
public class MerchantConfigDB extends MerchantConfigBean {
	private ResultSetHandler<List<MerchantConfigBean>> beanListHandler;
	private ResultSetHandler<MerchantConfigBean> beanHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	
	public MerchantConfigDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<MerchantConfigBean>(MerchantConfigBean.class);
		beanHandler = new BeanHandler<MerchantConfigBean>(MerchantConfigBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	
	private final String SELECT_BY_MERCHANTID = "SELECT * FROM tbl_merchant_config WHERE merchantId = ?";
	public List<MerchantConfigBean> findMerchantConfig(int merchantId) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID, beanListHandler, new Object[] {merchantId});
	}

	private final String SELECT_BY_FIELD = "SELECT * FROM tbl_merchant_config WHERE field = ?";
	public List<MerchantConfigBean> findMerchantConfigByField(String field) throws SQLException{
		return queryRunner.query(SELECT_BY_FIELD, beanListHandler, field);
	}
	
	private final String SELECT_BY_MERCHANTID_FIELD = "SELECT * FROM tbl_merchant_config WHERE merchantId = ? AND field = ?";
	public MerchantConfigBean findMerchantConfigByIdAndField(int merchantId, String field) throws SQLException{
		return queryRunner.query(SELECT_BY_MERCHANTID_FIELD, beanHandler, new Object[]{merchantId, field});
	}
	
	private static final String COUNT_FIELD_BY_MERCHANTID = "SELECT COUNT(id) FROM tbl_merchant_config WHERE merchantId = ?  AND field = ?  "; 
	public int countByFieldByMerchantId(int merchnatId, String field) throws SQLException {
		return queryRunner.query(COUNT_FIELD_BY_MERCHANTID, numRecordHandler, merchnatId , field);
	}
	
	private static final String DELETE_MERCHANT_CONFIG_BY_MERCHANTID = "DELETE FROM tbl_merchant_config WHERE merchantId = ?";
	public int deleteMerchantConfigByMerchantId(int merchantId) throws SQLException {
		return queryRunner.update(DELETE_MERCHANT_CONFIG_BY_MERCHANTID, merchantId);
	}
	
	private static final String DELETE_MERCHANT_CONFIG_BY_MERCHANTID_SPECIFIC_FIELD = "DELETE FROM tbl_merchant_config WHERE merchantId = ? AND field IN (---filter---)";
	public int deleteMerchantConfigByMerchantIdSpecificField(int merchantId, List<String> configList) throws SQLException {
		if(configList == null || configList.size() <= 0){
			return 0;
		}
		StringBuilder fieldList = new StringBuilder();
		for (String config : configList) {
			fieldList.append(",\"");
			fieldList.append(config);
			fieldList.append("\"");
		}
		String finalQuery = DatabaseUtil.replaceSQL(DELETE_MERCHANT_CONFIG_BY_MERCHANTID_SPECIFIC_FIELD, fieldList.toString().substring(1));
		return queryRunner.update(finalQuery, merchantId);
	}
	
	private static final String DELETE_MERCHANT_CONFIG_BY_MERCHANTID_EXCEPT_FIELD = "DELETE FROM tbl_merchant_config WHERE merchantId = ? AND field NOT IN (---filter---)";
	public int deleteMerchantConfigByMerchantIdExceptField(int merchantId, List<String> configList) throws SQLException {
		if(configList == null || configList.size() <= 0){
			return 0;
		}
		StringBuilder fieldList = new StringBuilder();
		for (String config : configList) {
			fieldList.append(",\"");
			fieldList.append(config);
			fieldList.append("\"");
		}
		String finalQuery = DatabaseUtil.replaceSQL(DELETE_MERCHANT_CONFIG_BY_MERCHANTID_EXCEPT_FIELD, fieldList.toString().substring(1));
		return queryRunner.update(finalQuery, merchantId);
	}
	
	private static final String DELETE_MERCHANT_CONFIG_BY_CONFIG_LIST = "DELETE FROM tbl_merchant_config WHERE merchantId = ? AND field IN (---filter---)";
	public int deleteConfigByConfigList(int merchantId, List<String> configList) throws SQLException {
		StringBuilder configBuilder = new StringBuilder();
		if(configList != null && configList.size() > 0){
			for (String config : configList) {
				configBuilder.append(",");
				configBuilder.append("\"" + config + "\"");
			}
		}
		String finalSql = DatabaseUtil.replaceSQL(DELETE_MERCHANT_CONFIG_BY_CONFIG_LIST, configBuilder.substring(1).toString());
		return queryRunner.update(finalSql, merchantId);
	}
	
	private static final String INSERT_MERCHANT_CONFIG = "INSERT INTO `tbl_merchant_config` (`merchantId`, `field`, `value`) VALUES (?, ?, ?)";
	public int insertMerchantConfig(MerchantConfigBean mcBean) throws SQLException {
		return queryRunner.update(INSERT_MERCHANT_CONFIG, mcBean.getMerchantId(), mcBean.getField(), mcBean.getValue());
	}	
	public int[] insertMerchantConfigBatch(String merchantId, List<String[]> dataList) throws SQLException {
		Object[][] obj = new Object[dataList.size()][3];
		for (int count=0; count<dataList.size(); count++) {
			obj[count][0] = (Object) merchantId;
			obj[count][1] = (Object) dataList.get(count)[0];
			obj[count][2] = (Object) dataList.get(count)[1];
		}
		return queryRunner.batch(INSERT_MERCHANT_CONFIG, obj);
	}	
	
	
	private static final String INSERT_MERCHANT_CONFIG_LIST = "INSERT INTO `tbl_merchant_config` (`merchantId`, `field`, `value`) VALUES ---filter---";
	public int insertMerchantConfigList(List<MerchantConfigBean> merchantConfigList) throws SQLException {
		StringBuilder countObject = new StringBuilder();	
		List<Object> params = new ArrayList<>();
		
		for(MerchantConfigBean mcfBean : merchantConfigList){
			countObject.append(",(?, ?, ?)");
			params.add(mcfBean.getMerchantId());
			params.add(mcfBean.getField());
			params.add(mcfBean.getValue());
		}
		
		String filter = countObject.substring(1).toString();
		String finalSql = DatabaseUtil.replaceSQL(INSERT_MERCHANT_CONFIG_LIST, filter);
		return queryRunner.update(finalSql, params.toArray());
	}	
	
}
