package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import botbackend.bean.MerchantBean;
import botbackend.bean.MonitorBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;

public class MonitorDB  {
	private ResultSetHandler<List<MonitorBean>> beanListHandler;
	private DataSource dataSource;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	public MonitorDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<MonitorBean>(MonitorBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_MONITOR_LIST = "SELECT * FROM  tbl_monitor ORDER BY updateDate DESC LIMIT ?, ? "; 
	public List<MonitorBean> getReporList(int start, int offset) throws SQLException {
		return queryRunner.query(GET_MONITOR_LIST, beanListHandler, start, offset);
	}
	
	private static final String COUNT_ISSUES_MONITOR = "SELECT COUNT(id) FROM tbl_monitor WHERE status = ? AND analyzeResult != 'Unknown'"; 
	public int countIssuesReport(int status) throws SQLException {
		return queryRunner.query(COUNT_ISSUES_MONITOR, numRecordHandler,status);
	}
	
	private static final String COUNT_ALL_MONITOR = "SELECT COUNT(id) FROM tbl_monitor"; 
	public int countAllReport() throws SQLException {
		return queryRunner.query(COUNT_ALL_MONITOR, numRecordHandler);
	}
	
	private static final String GET_MONITOR_LIST_BY_SEARCH = "SELECT mn.*,mc.`name` FROM  tbl_monitor mn INNER JOIN tbl_merchant mc  ON mn.merchantId = mc.id  ---filter---  ORDER BY status DESC, updateDate DESC LIMIT ?, ? "; 
	public List<MonitorBean> getMonitorListBySearch(int merchantId, String analyzeResult, int checkHistory, int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		filters.append(" WHERE analyzeResult != 'Unknown' "); 
		if(StringUtils.isNotBlank(analyzeResult)){ 
			filters.append(" AND analyzeResult = ? "); params.add(analyzeResult); 
		}
		
		if(merchantId > 0){
			filters.append(" AND mn.merchantId = ? "); params.add(merchantId);
		}
		
		if(checkHistory == 0){
			checkHistory = 1;
			filters.append(" AND mn.status = ? "); params.add(checkHistory); 
		}
		
		params.add(start);
		params.add(offset);
		
		return (List<MonitorBean>)queryRunner.query(DatabaseUtil.replaceSQL(GET_MONITOR_LIST_BY_SEARCH, filters.toString()), beanListHandler, params.toArray());
	}
	
	private static final String COUNT_MONITOR_LIST_BY_SEARCH = "SELECT COUNT(id) FROM  tbl_monitor ---filter--- "; 
	public int countMonitorBySearch(int merchantId, String analyzeResult, int checkHistory) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		filters.append(" WHERE analyzeResult != 'Unknown' "); 
		if(StringUtils.isNotBlank(analyzeResult)){ 
			filters.append(" AND analyzeResult = ? "); params.add(analyzeResult); 
		}
		
		if(merchantId > 0){
			filters.append(" AND merchantId = ? "); params.add(merchantId);
		}
		
		if(checkHistory == 0){
			checkHistory = 1;
			filters.append(" AND status = ? "); params.add(checkHistory); 
		}
		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_MONITOR_LIST_BY_SEARCH, filters.toString()), numRecordHandler, params.toArray());
	}
	
	private static final String GET_IDS_BY_STATUS = "SELECT id, merchantId, server FROM tbl_monitor WHERE status = ?"; 
	public Map<Integer, List<String>> getIdsByStatus(int status) throws SQLException {
		Map<Integer, List<String>> idsMap = new HashMap<Integer, List<String>>();
		List<MonitorBean> monitorList  =  queryRunner.query(GET_IDS_BY_STATUS, beanListHandler, status);
		if(monitorList != null && monitorList.size() > 0){
			for(MonitorBean monitorBean  : monitorList){
				List<String> data = new ArrayList<String>();
				if(idsMap.get(monitorBean.getMerchantId())!= null){
					data = idsMap.get(monitorBean.getMerchantId());
				}
				data.add(monitorBean.getServer());
				idsMap.put(monitorBean.getMerchantId(), data);
			}
		}
		return idsMap;
	}
	
	private final String INSERT_MONITOR_DATA = "INSERT INTO tbl_monitor (merchantId,analyzeResult, updateDate,server,detail) VALUES (?, ?, NOW(), ?,?) ";
	public int updateMonitorData(int merchantId,String analyzeResult, String server,String detail) throws Exception {
		return queryRunner.update(INSERT_MONITOR_DATA, new Object[] {merchantId ,analyzeResult, server,detail});
	}	
	
	private final String UPDATE_MONITOR_DAT_BY_MERCHANT_IDS = "UPDATE tbl_monitor SET status = ? ---filter--- ";
	public int updateMonitorDataByMerchants(List<MerchantBean> merchants, int status) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		params.add(status);
		if(merchants != null && merchants.size() > 0){
			String merchantStr = "";
			filters.append("  WHERE  (merchantId,server) NOT IN ( ");
			
			for(MerchantBean merchant : merchants){
				merchantStr += ",( ?, ?)";
				params.add(merchant.getId());
				params.add(merchant.getServer());
			}
			merchantStr  = merchantStr.substring(1, merchantStr.length());
			filters.append(merchantStr + " ) ");

		}
		return queryRunner.update(DatabaseUtil.replaceSQL(UPDATE_MONITOR_DAT_BY_MERCHANT_IDS, filters.toString()), params.toArray());
	}	
	private static final String UPDATE_MONITOR_STATUS_BY_ID = "UPDATE tbl_monitor SET status = ? WHERE merchantId = ? AND server = ?";
	public int updateMonitorStatusById(int status , int merchantId, String server) throws SQLException {
		return queryRunner.update(UPDATE_MONITOR_STATUS_BY_ID, new Object[] {status, merchantId, server });
	}	
	
	private static final String DELETE_MONITOR_DATA = "DELETE FROM tbl_monitor WHERE status = ? ";
	public int deleteMonitorData(int checkStatus) throws SQLException {
		return queryRunner.update(DELETE_MONITOR_DATA, new Object[] {checkStatus });
	}	
	
	private static final String DELETE_MONITOR_DATA_BY_ID = "DELETE FROM tbl_monitor WHERE id = ? ";
	public int deleteMonitorDataById(int id) throws SQLException {
		return queryRunner.update(DELETE_MONITOR_DATA_BY_ID, new Object[] {id });
	}
	
}
