package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.TrackMerchantStatisticBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class TrackMerchantStatisticDB extends TrackMerchantStatisticBean {
	private ResultSetHandler<List<TrackMerchantStatisticBean>> beanListHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	
	public TrackMerchantStatisticDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<TrackMerchantStatisticBean>(TrackMerchantStatisticBean.class);
	}
	
	private static final String GET_LATEST_RECORD = "SELECT * FROM `tbl_track_merchant_statistic` WHERE id >= (SELECT MAX(id) FROM `tbl_track_merchant_statistic` WHERE merchant_id = 0)"; 
	public List<TrackMerchantStatisticBean> getLatestRecord() throws SQLException {
		return queryRunner.query(GET_LATEST_RECORD, beanListHandler);
	}
	
}
