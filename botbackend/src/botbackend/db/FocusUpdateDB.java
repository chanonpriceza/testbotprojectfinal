package botbackend.db;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import botbackend.bean.FocusUpdateBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class FocusUpdateDB  {
	private ResultSetHandler<FocusUpdateBean> beanHandler;
	private ResultSetHandler<List<FocusUpdateBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	@SuppressWarnings("unchecked")
	public FocusUpdateDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<FocusUpdateBean>(FocusUpdateBean.class);
		beanListHandler = new BeanListHandler<FocusUpdateBean>(FocusUpdateBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_BY_WEB_PRODUCT_ID = "SELECT * FROM tbl_focus_update WHERE webProductId = ? and active = ?";
	public FocusUpdateBean getByWebProductId(int webProductId, int active) throws SQLException{
		return queryRunner.query(GET_BY_WEB_PRODUCT_ID, beanHandler, webProductId, active);
	}
	
	private static final String COUNT_CURRENT_LIST = "SELECT COUNT(id) FROM tbl_focus_update WHERE active = ?"; 
	public int countCurrentList(int active) throws SQLException {
		return queryRunner.query(COUNT_CURRENT_LIST, numRecordHandler, active);
	}
	
	private static final String GET_CURRENT_LIST = "SELECT * FROM tbl_focus_update WHERE active = ?";
	public List<FocusUpdateBean> getCurrentList(int active) throws SQLException {
		return queryRunner.query(GET_CURRENT_LIST, beanListHandler, active);
	}
	
	private static final String SAVE_NEW_ITEM = ""
			+ "INSERT INTO tbl_focus_update (merchantId, webProductId, webProductName, botProductId, botServerName, addBy, addDate, lastUpdateDate, note, active) "
			+ "VALUE (?, ?, ?, ?, ?, ?, now(), null, null, ?)";
	public int saveNewItem(FocusUpdateBean fuBean) throws SQLException{
		return queryRunner.update(SAVE_NEW_ITEM, 
			fuBean.getMerchantId(),
			fuBean.getWebProductId(),
			fuBean.getWebProductName(),
			fuBean.getBotProductId(),
			fuBean.getBotServerName(),
			fuBean.getAddBy(),
			fuBean.getActive()
		);
	}

	private static final String SAVE_UPDATE_ITEM = "UPDATE tbl_focus_update SET lastUpdateDate = now(), note = ? WHERE id = ?";
	public int saveUpdateItem(String note, int id) throws SQLException{
		return queryRunner.update(SAVE_UPDATE_ITEM, note, id);
	}
	
	private static final String DELETE_ITEM = "UPDATE tbl_focus_update SET active = 0 WHERE webProductId = ? and botProductId = ?";
	public int deleteItem(int webProductId, int botProductId) throws SQLException{
		return queryRunner.update(DELETE_ITEM, webProductId, botProductId);
	}
	
}
