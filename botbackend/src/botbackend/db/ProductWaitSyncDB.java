package botbackend.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.ProductWaitSyncBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class ProductWaitSyncDB {

	private ResultSetHandler<List<ProductWaitSyncBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;

	QueryRunner queryRunner; 		
	
	public ProductWaitSyncDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<ProductWaitSyncBean>(ProductWaitSyncBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	public static final String INSERT = "INSERT INTO tbl_product_wait_sync" +
			" (`dataServer`, `addDate`, `syncDate`, `firstProductId`, `lastProductId`, `allCount`, `done`, `status`) " +
			"VALUES (?,?,?,?,?,?,?,?)";
	public int insertProductWaitSync(ProductWaitSyncBean productWaitSyncBean) throws SQLException {
		return queryRunner.update(INSERT, 
				productWaitSyncBean.getDataServer(),
				productWaitSyncBean.getAddDate(),
				productWaitSyncBean.getSyncDate(),
				productWaitSyncBean.getFirstProductId(),
				productWaitSyncBean.getLastProductId(),
				productWaitSyncBean.getAllCount(),
				productWaitSyncBean.getDone(),
				productWaitSyncBean.getStatus());
	}

	private static final String SELECT_BY_STATUS = "SELECT * FROM tbl_product_wait_sync WHERE status=?";
	public List<ProductWaitSyncBean> getDataByStatus(String status) throws SQLException {
		return queryRunner.query(SELECT_BY_STATUS, beanListHandler, status);
	}
	
	private static final String UPDATE = "UPDATE tbl_product_wait_sync "
			+ "SET status=?, done=?, syncDate=? WHERE id=?";
	public void update(String status, int done, Date syncDate, int id) throws SQLException {
		queryRunner.update(UPDATE, status, done, syncDate, id);
	}
	
	private static final String SELECT_BY_RANGE_DATE = "SELECT * FROM tbl_product_wait_sync WHERE (addDate BETWEEN ? AND ?) AND dataServer=?";
	public List<ProductWaitSyncBean> getDataByRangeDateAndServer(Date startDate, Date endDate,String server) throws SQLException {
		return queryRunner.query(SELECT_BY_RANGE_DATE,beanListHandler, startDate, endDate, server);
	}
	
	private static final String SELECT_ALL = "SELECT * FROM tbl_product_wait_sync";
	public List<ProductWaitSyncBean> getAllData() throws SQLException {
		return queryRunner.query(SELECT_ALL,beanListHandler);
	}
	
	private static final String COUNT_ALL = "SELECT COUNT(id) FROM tbl_product_wait_sync";
	public int countAllDate() throws SQLException {
		return queryRunner.query(COUNT_ALL,numRecordHandler);
	}
	
	private static final String GET_REPORT_LIST_BY_SEARCH = "SELECT * FROM tbl_product_wait_sync ORDER BY id DESC LIMIT ?, ?";
	public List<ProductWaitSyncBean> getReportListBySearch(int start, int numRecord) throws SQLException {
		return queryRunner.query(GET_REPORT_LIST_BY_SEARCH,beanListHandler, start, start+numRecord);
	}
	
	private static final String COUNT_GROUP_BY_DATE = "SELECT COUNT(t.id) FROM (SELECT * FROM tbl_product_wait_sync GROUP BY addDate) as t";
	public int countIdGroupByDate() throws SQLException {
		return queryRunner.query(COUNT_GROUP_BY_DATE,numRecordHandler);
	}
	
	private static final String GET_DATE_GROUP_BY_DATE = "SELECT addDate FROM tbl_product_wait_sync GROUP BY addDate ORDER BY id DESC LIMIT ?, ?";
	public List<ProductWaitSyncBean> getDateGroupByDate(int start, int numRecord) throws SQLException {
		return queryRunner.query(GET_DATE_GROUP_BY_DATE,beanListHandler, start, start+numRecord);
	}
	
	private static final String GET_DATA_GROUP_BY_DATE = "SELECT * FROM tbl_product_wait_sync WHERE addDate>=? AND addDate<=? ORDER BY id DESC";
	public List<ProductWaitSyncBean> getDataGroupByDate(Date firstDate, Date lastDate) throws SQLException {
		return queryRunner.query(GET_DATA_GROUP_BY_DATE,beanListHandler, firstDate, lastDate);
	}

}
