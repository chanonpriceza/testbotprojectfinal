package botbackend.db.api;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

import botbackend.bean.api.LnwShopSendDataBean;
import botbackend.utils.Util;

public class LnwShopSendDataDB {		
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	
	private DataSource dataSource;	
	private ResultSetHandler<LnwShopSendDataBean> beanHandler;
	
	QueryRunner queryRunner; 		
	
	public LnwShopSendDataDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<LnwShopSendDataBean>(LnwShopSendDataBean.class);
	}
	
	public static final String INSERT = "INSERT INTO tbl_send_data" +
			"(action, merchantId, categoryId, name, price, url, description, pictureData, pictureUrl, merchantUpdateDate, updateDate, realProductId, keyword, basePrice, upc) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public int insertSendData(LnwShopSendDataBean sendDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				sendDataBean.getAction(),
				sendDataBean.getMerchantId(),
				sendDataBean.getCategoryId(), 
				Util.limitString(sendDataBean.getName(), NAME_LENGTH),
				sendDataBean.getPrice(),
				sendDataBean.getUrl(),
				Util.limitString(sendDataBean.getDescription(), DESCRIPTION_LENGTH), 
				sendDataBean.getPictureData(), 
				sendDataBean.getPictureUrl(),				
				sendDataBean.getMerchantUpdateDate(),
				sendDataBean.getUpdateDate(),
				sendDataBean.getRealProductId(),
				sendDataBean.getKeyword(),
				sendDataBean.getBasePrice(),
				sendDataBean.getUpc()
		}); 		
	}
	
	public static final String INSERT_BATCH = "INSERT INTO tbl_send_data" +
			"(action, merchantId, categoryId, name, price, url, description, pictureData, pictureUrl, merchantUpdateDate, updateDate, realProductId, keyword, basePrice, upc) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public int[] insertSendDataBatch(List<LnwShopSendDataBean> sendDataBeanList) throws SQLException{
		int i = 0;
		Object[][] obj = new Object[sendDataBeanList.size()][];
		for (LnwShopSendDataBean sendDataBean : sendDataBeanList) {
			obj[i] = new Object[]{ 
					sendDataBean.getAction(),
					sendDataBean.getMerchantId(),
					sendDataBean.getCategoryId(), 
					Util.limitString(sendDataBean.getName(), NAME_LENGTH),
					sendDataBean.getPrice(),
					sendDataBean.getUrl(),
					Util.limitString(sendDataBean.getDescription(), DESCRIPTION_LENGTH), 
					sendDataBean.getPictureData(), 
					sendDataBean.getPictureUrl(),				
					sendDataBean.getMerchantUpdateDate(),
					sendDataBean.getUpdateDate(),
					sendDataBean.getRealProductId(),
					sendDataBean.getKeyword(),
					sendDataBean.getBasePrice(),
					sendDataBean.getUpc()
				};
			i++;
		}
		return queryRunner.batch(INSERT_BATCH, obj);
	}
				
}
