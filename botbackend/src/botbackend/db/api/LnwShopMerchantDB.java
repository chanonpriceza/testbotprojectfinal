package botbackend.db.api;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.api.LnwShopMerchantBean;

public class LnwShopMerchantDB {
		
	private ResultSetHandler<LnwShopMerchantBean> beanHandler;
	private ResultSetHandler<List<LnwShopMerchantBean>> beanListHandler;
	
	public static final String SELECT_ALL = "SELECT * FROM tbl_merchant";
	
	QueryRunner queryRunner; 		
	
	public LnwShopMerchantDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<LnwShopMerchantBean>(LnwShopMerchantBean.class);
		beanListHandler = new BeanListHandler<LnwShopMerchantBean>(LnwShopMerchantBean.class);
	}

	private static final String SELECT_BY_ID = "SELECT * FROM tbl_merchant WHERE id = ?";
	public LnwShopMerchantBean findMerchant(int id) throws SQLException{	
		return queryRunner.query(SELECT_BY_ID, beanHandler, id); 
	}	
	private static final String SELECT_BY_DOMAIN = "SELECT * FROM tbl_merchant WHERE  lnw_id =  ? ";
	public LnwShopMerchantBean findMerchantByDomain(String domain) throws SQLException{	
		return queryRunner.query(SELECT_BY_DOMAIN, beanHandler, domain); 
	}	
	private static final String SELECT_ALL_ID_AND_ACTIVE = "SELECT * FROM tbl_merchant WHERE id > 0 AND active = 1";
	public List<LnwShopMerchantBean> findAllIdAndActive() throws SQLException{
		return queryRunner.query(SELECT_ALL_ID_AND_ACTIVE, beanListHandler);
	}
	private static final  String ACTIVE_MERCHANT_AND_UPDATE_ID = "UPDATE tbl_merchant SET active = ?,id = ?,addParentName = ? where lnw_id = ? ";
	public int activeByDomain(LnwShopMerchantBean id,String domainName) throws SQLException{
		return queryRunner.update(ACTIVE_MERCHANT_AND_UPDATE_ID,id.getActive(),id.getId(),id.getAddParentName(),domainName);
	}
	
	public static final String INSERT_BATCH_WITH_LNW_ID = "INSERT IGNORE INTO tbl_merchant ( `lnw_id`, `name`, `active` ) VALUES (?,?,?)";
	public int[] insertBatchWithLnwId(List<LnwShopMerchantBean> merchantList) throws SQLException{
		int i = 0;
		Object[][] obj = new Object[merchantList.size()][];
		for (LnwShopMerchantBean merchantBean : merchantList) {
			obj[i] = new Object[]{
					merchantBean.getLnw_id(),
					merchantBean.getName(),
					0
			};
			i++;
		}
		return queryRunner.batch(INSERT_BATCH_WITH_LNW_ID, obj);
	}
	
	public static final String UPDATE_FORCE_UPDATE_CAT = "UPDATE tbl_merchant SET forceUpdateCat = ? WHERE id = ?";
	public int updateForceUpdateCat(int id, int updateTo) throws SQLException{
		return queryRunner.update(UPDATE_FORCE_UPDATE_CAT, updateTo, id);
	}
	
	public static final String UPDATE_ACTIVE_NOTE = "UPDATE tbl_merchant SET active = ?, NOTE = ? WHERE id = ?";
	public int updateActiveNote(int merchantId, int active, String note) throws SQLException{
		return queryRunner.update(UPDATE_ACTIVE_NOTE, active, note, merchantId);
	}
	
}


