package botbackend.db.api;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.bean.api.LnwShopProductDataBean.STATUS;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;

public class LnwShopProductDataDB {		

	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	
	private DataSource dataSource;	
	private ResultSetHandler mapListHandler;
	private ResultSetHandler<LnwShopProductDataBean> beanHandler;
	private ResultSetHandler<List<LnwShopProductDataBean>> beanListHandler;
	private ResultSetHandler<List<Integer>> integerListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	
	QueryRunner queryRunner; 		
	
	public LnwShopProductDataDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		mapListHandler = new MapListHandler();
		numRecordHandler = new NumRecordResultSetHandler();
		beanHandler = new BeanHandler<LnwShopProductDataBean>(LnwShopProductDataBean.class);
		beanListHandler = new BeanListHandler<LnwShopProductDataBean>(LnwShopProductDataBean.class);
	}
	
	public static final String INSERT = "INSERT INTO tbl_product_data"
			+ "(id, name, price, description, picture_url, product_url, lnw_merchant_id, lnw_product_id, status, "
			+ "parent_product_name, tag, category, priceMin, priceMax, updateDate) "
			+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";
	public int insertProductData(LnwShopProductDataBean lnwShopProductDataBean, STATUS status) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				(lnwShopProductDataBean.getId() == 0 ? null : lnwShopProductDataBean.getId()),
				Util.limitString(lnwShopProductDataBean.getName(), NAME_LENGTH), 
				lnwShopProductDataBean.getPrice(),
				Util.limitString(lnwShopProductDataBean.getDescription(), DESCRIPTION_LENGTH), 
				lnwShopProductDataBean.getPicture_url(),
				lnwShopProductDataBean.getProduct_url(),
				lnwShopProductDataBean.getLnw_merchant_id(),
				lnwShopProductDataBean.getLnw_product_id(),
				status.toString(),
				lnwShopProductDataBean.getParent_product_name(),
				lnwShopProductDataBean.getTag(),
				lnwShopProductDataBean.getCategory(),
				lnwShopProductDataBean.getPriceMin(),
				lnwShopProductDataBean.getPriceMax()
		}); 		
	}
	
	public static final String UPDATE = "UPDATE tbl_product_data SET "
//			+ "id = ?, name = ?, price = ?, description = ?, picture_url = ?, product_url = ?, status = ?, old_name = ? " + 
			+ "name = ?, price = ?, description = ?, picture_url = ?, product_url = ?, status = ?, old_name = ?, "
			+ "parent_product_name = ?, tag = ?, category = ?, priceMin = ?, priceMax = ?, updateDate = NOW() "
			+ "WHERE lnw_merchant_id = ? AND lnw_product_id = ?";
	public int updateProductData(LnwShopProductDataBean lnwShopProductDataBean, STATUS status) throws SQLException {
		return queryRunner.update(UPDATE, new Object[] {
//				(lnwShopProductDataBean.getId() == 0 ? null : lnwShopProductDataBean.getId()),
				Util.limitString(lnwShopProductDataBean.getName(), NAME_LENGTH), 
				lnwShopProductDataBean.getPrice(),
				Util.limitString(lnwShopProductDataBean.getDescription(), DESCRIPTION_LENGTH), 
				lnwShopProductDataBean.getPicture_url(),
				lnwShopProductDataBean.getProduct_url(),
				status.toString(),
				lnwShopProductDataBean.getOld_name(),

				lnwShopProductDataBean.getParent_product_name(),
				lnwShopProductDataBean.getTag(),
				lnwShopProductDataBean.getCategory(),
				lnwShopProductDataBean.getPriceMin(),
				lnwShopProductDataBean.getPriceMax(),

				lnwShopProductDataBean.getLnw_merchant_id(),
				lnwShopProductDataBean.getLnw_product_id()
		}); 		
	}
	
	public static final String DELETE = "DELETE FROM tbl_product_data WHERE lnw_merchant_id = ? AND lnw_product_id = ?";
	public int delete(LnwShopProductDataBean bean) throws SQLException {
		return queryRunner.update(DELETE, new Object[] {bean.getLnw_merchant_id(), bean.getLnw_product_id()}); 
	}
	
	public static final String GET_ALL_MERCHANT_AND_SAMPLE_URL = "SELECT lnw_merchant_id , product_url FROM tbl_product_data GROUP BY lnw_merchant_id";
	public List<LnwShopProductDataBean> getAllMerchantAndSampleUrl() throws SQLException {
		return queryRunner.query(GET_ALL_MERCHANT_AND_SAMPLE_URL, beanListHandler);
	}
	
	public static final String COUNT_PRODUCT_BY_LNW_MERCHANT_AND_STATUS = "SELECT count(id) from tbl_product_data WHERE lnw_merchant_id = ? AND status != 'DONE'";
	public int countProductByLnwMerchantAndStatus(String lnwMerchantId) throws SQLException{
		return queryRunner.query(COUNT_PRODUCT_BY_LNW_MERCHANT_AND_STATUS, numRecordHandler, lnwMerchantId);
	}
	
	public static final String GET_PRODUCT_BY_LNW_MERCHANT_AND_STATUS = "SELECT * FROM tbl_product_data WHERE lnw_merchant_id = ? AND status IN ('ADD','UPDATE','UPDATE_NAME','DELETE') LIMIT ?,?";
	public List<LnwShopProductDataBean> getProductByLnwMerchantAndStatus(String lnwMerchantId, int start, int offset) throws SQLException{
		return queryRunner.query(GET_PRODUCT_BY_LNW_MERCHANT_AND_STATUS, beanListHandler, new Object[]{lnwMerchantId, start, offset});
	}
	
	public static final String UPDATE_STATUS_BATCH = "UPDATE tbl_product_data SET status = ? WHERE lnw_merchant_id = ? AND id IN (---filter---)";
	public int updateStatusBatch(List<Integer> idList, String lnwMerchantId) throws SQLException {
		if(idList != null && idList.size() > 0){
			StringBuilder idStr = new StringBuilder(); 
			for(Integer id : idList){
				idStr.append(",");
				idStr.append(id);
			}
			return queryRunner.update(DatabaseUtil.replaceSQL(UPDATE_STATUS_BATCH, idStr.toString().substring(1)), STATUS.DONE.toString(), lnwMerchantId);
		}
		return 0;
	}
	
	public static final String UPDATE_OLD_DATA_BATCH = "UPDATE tbl_product_data SET old_name = NULL, status = ? WHERE lnw_merchant_id = ? AND id IN (---filter---)";
	public int clearOldDataBatch(List<Integer> idList, String lnwMerchantId) throws SQLException {
		if(idList != null && idList.size() > 0){
			StringBuilder idStr = new StringBuilder(); 
			for(Integer id : idList){
				idStr.append(",");
				idStr.append(id);
			}
			return queryRunner.update(DatabaseUtil.replaceSQL(UPDATE_OLD_DATA_BATCH, idStr.toString().substring(1)), STATUS.DONE.toString(), lnwMerchantId);
		}
		return 0;
	}
	
	public static final String DELETE_PRODUCT_BATCH = "DELETE FROM tbl_product_data WHERE lnw_merchant_id = ? AND id IN (---filter---)";
	public int deleteProductBatch(List<Integer> idList, String lnwMerchantId) throws SQLException {
		if(idList != null && idList.size() > 0){
			StringBuilder idStr = new StringBuilder(); 
			for(Integer id : idList){
				idStr.append(",");
				idStr.append(id);
			}
			return queryRunner.update(DatabaseUtil.replaceSQL(DELETE_PRODUCT_BATCH, idStr.toString().substring(1)), lnwMerchantId);
		}
		return 0;
	}
	
	public static final String UPDATE_ALL_STATUS_BY_STATS = "UPDATE tbl_product_data SET status = ? WHERE lnw_merchant_id = ? AND status = ?";
	public int updateAllProductStatusByStatus(String lnwMerchantId, String updateFrom, String updateTo) throws SQLException {
		return queryRunner.update(UPDATE_ALL_STATUS_BY_STATS, updateTo, lnwMerchantId, updateFrom);
	}
	
	public static final String GET_PRODUCT_DATA	= "SELECT * FROM tbl_product_data WHERE lnw_merchant_id = ? AND lnw_product_id = ?";
	public LnwShopProductDataBean getProductData(LnwShopProductDataBean request) throws SQLException {
		return queryRunner.query(GET_PRODUCT_DATA, beanHandler, new Object[]{request.getLnw_merchant_id(), request.getLnw_product_id()});
	}
	
}
