package botbackend.db.api;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.UrlPatternBean;
import botbackend.db.utils.StringListValueResultSetHandler;

public class LnwShopUrlPatternDB {		

	private StringListValueResultSetHandler stringListHandler;
	private ResultSetHandler<List<UrlPatternBean>> beanListHandler;
	
	QueryRunner queryRunner; 		
	
	public LnwShopUrlPatternDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		stringListHandler = new StringListValueResultSetHandler();
		beanListHandler = new BeanListHandler<UrlPatternBean>(UrlPatternBean.class);
	}
	
	public static final String SELECT_VALUE_BY_MERCHANTID_ACTION = "SELECT value FROM tbl_url_pattern WHERE merchantId = ? and action = ?";
	public List<String> getValueByMerchantIdAction(int merchantId, String action) throws SQLException {		
		return queryRunner.query(SELECT_VALUE_BY_MERCHANTID_ACTION, stringListHandler, merchantId, action); 				
	}
	
	public static final String SELECT_BY_MERCHANTID_ACTION = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? and action = ?";
	public List<UrlPatternBean> getByMerchantIdAction(int merchantId, String action) throws SQLException{
		return queryRunner.query(SELECT_BY_MERCHANTID_ACTION, beanListHandler, merchantId, action);
	}
	
}
