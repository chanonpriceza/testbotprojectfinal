package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.TrackBotStateBean;
import botbackend.bean.TrackMerchantStatisticBean;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.db.utils.StringListValueResultSetHandler;

public class TrackBotStateDB extends TrackBotStateBean {
	QueryRunner queryRunner;
	
	private ResultSetHandler<List<TrackBotStateBean>> beanListHandler;
	private ResultSetHandler<TrackBotStateBean> beanHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private StringListValueResultSetHandler stringListHandler;
	
	public TrackBotStateDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<TrackBotStateBean>(TrackBotStateBean.class);
		beanListHandler = new BeanListHandler<TrackBotStateBean>(TrackBotStateBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
		stringListHandler = new StringListValueResultSetHandler();
	}
	
	private static final String INSERT_TRACK_DATA = "INSERT INTO `tbl_track_bot_state_history` (`server`, `detail`, `signalDate`) VALUES (?, ?, ?)"; 
	public int insertTrackDataHistory(String server, String detail, String signalDate) throws SQLException {
		return queryRunner.update(INSERT_TRACK_DATA, server, detail, signalDate);
	}
	
	private static final String DELETE_HISTORY_INTERVAL = "DELETE FROM tbl_track_bot_state_history WHERE signalDate <  date_sub(CURRENT_DATE(), INTERVAL ? month)";
	public int deleteOldHistoryInterval(int deleteMonthInterval) throws SQLException{
		return queryRunner.update(DELETE_HISTORY_INTERVAL);
	}
	
	private static final String GET_ALL_SERVER_FROM_HISTORY = "SELECT DISTINCT(server) FROM `tbl_track_bot_state_history`";
	public List<String> getAllServerFromHistory() throws SQLException{
		return queryRunner.query(GET_ALL_SERVER_FROM_HISTORY, stringListHandler);
	}
	
	private static final String GET_BY_SERVER_LIMIT = "SELECT * FROM `tbl_track_bot_state_history` WHERE server = ? ORDER BY id DESC LIMIT ?";
	public List<TrackBotStateBean> getByServerLimit(String server, int limit) throws SQLException{
		return queryRunner.query(GET_BY_SERVER_LIMIT, beanListHandler, server, limit);
	}
	
	private static final String GET_ALL_LAST_ROUND = "SELECT * FROM `tbl_track_bot_state`";
	public List<TrackBotStateBean> getAllLastRound() throws SQLException{
		return queryRunner.query(GET_ALL_LAST_ROUND, beanListHandler);
	}
	
	
	
	
}
