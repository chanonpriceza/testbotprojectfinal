package botbackend.db;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import botbackend.bean.ManualProductBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class ManualProductDB  {
	private ResultSetHandler<ManualProductBean> beanHandler;
	private ResultSetHandler<List<ManualProductBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	@SuppressWarnings("unchecked")
	public ManualProductDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ManualProductBean>(ManualProductBean.class);
		beanListHandler = new BeanListHandler<ManualProductBean>(ManualProductBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String COUNT_ALL = "SELECT count(id) FROM tbl_manual_product";
	public int countAll() throws SQLException {
		return queryRunner.query(COUNT_ALL, numRecordHandler);
	}
	
	private static final String GET_ALL = "SELECT * FROM tbl_manual_product";
	public List<ManualProductBean> getAll() throws SQLException {
		return queryRunner.query(GET_ALL, beanListHandler);
	}
	
	private static final String GET_BY_ID = "SELECT * FROM tbl_manual_product WHERE id = ?";
	public ManualProductBean getById(int id) throws SQLException {
		return queryRunner.query(GET_BY_ID, beanHandler, id);
	}
	
	private static final String INSERT_MANUAL = "INSERT INTO tbl_manual_product (merchantId, productName, addBy, addDate) VALUES (?, ?, ?, NOW())";
	public int insertManual(ManualProductBean mpBean) throws SQLException{
		return queryRunner.update(INSERT_MANUAL, 
				mpBean.getMerchantId(),
				mpBean.getProductName(),
				mpBean.getAddBy()
			);
	}
	
	private static final String DELETE_MANUAL = "DELETE FROM tbl_manual_product WHERE id = ?";
	public int deleteManual(int id) throws SQLException{
		return queryRunner.update(DELETE_MANUAL, id);
	}
	
}
