package botbackend.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BotQualityReportBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class BotQualityReportHistoryDB {
	
	private ResultSetHandler<Integer> numRecordHandler;
	private ResultSetHandler<List<BotQualityReportBean>> beanListHandler;
	
	QueryRunner queryRunner; 		
	
	public BotQualityReportHistoryDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
		numRecordHandler = new NumRecordResultSetHandler();
		beanListHandler = new BeanListHandler<BotQualityReportBean>(BotQualityReportBean.class);
	}
	
	public static final String INSERT = "INSERT INTO tbl_bot_quality_report_history (`type`, `merchantId`, `name`, `status`, `server`, `time`, `allCount`, `reportDate`) "
			+ "SELECT `type`, `merchantId`, `name`, `status`, `server`, `time`, `allCount`, `reportDate` "
			+ "FROM tbl_bot_quality_report";
	public int insertReport() throws SQLException {
		return queryRunner.insert(INSERT, numRecordHandler);
	}
	
	public static final String SELECT_REPORT_BY_DATE = "SELECT * FROM  tbl_bot_quality_report_history WHERE date(reportDate) = ? ORDER BY type, merchantId";
	public List<BotQualityReportBean> getReportByDate(Date date) throws SQLException {
		return queryRunner.query(SELECT_REPORT_BY_DATE, beanListHandler, new Object[] {date}); 	
	}
}
