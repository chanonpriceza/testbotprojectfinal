package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import botbackend.bean.ServerRuntimeReportBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;
import botbackend.db.utils.NumRecordResultSetHandler;


public class ServerRuntimeReportDB {
	private ResultSetHandler<ServerRuntimeReportBean> beanHandler;
	private ResultSetHandler<List<ServerRuntimeReportBean>> beanListHandler;
	private DataSource dataSource;
	private ResultSetHandler<Integer> numRecordHandler;
	private ResultSetHandler<List<Integer>> integerListHandler;
	QueryRunner queryRunner;
	
	public ServerRuntimeReportDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ServerRuntimeReportBean>(ServerRuntimeReportBean.class);
		beanListHandler = new BeanListHandler<ServerRuntimeReportBean>(ServerRuntimeReportBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
		integerListHandler = new IntegerListValueResultSetHandler();
	}
	
	private static final String COUNT_SERVER_REPORT_LIST_BY_SEARCH = "SELECT COUNT(fullRecord.id) FROM tbl_server_report as fullRecord ---filter--- ORDER BY id DESC "; 
	public int countServerReportBySearch(String type, String status,String server ,int time ,int record) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(record == 0){
			filters.append(" WHERE ");
		}else{
			filters.append(" INNER JOIN ( SELECT max(id) AS sid FROM tbl_server_report WHERE ");
		}
	
		if(StringUtils.isNotBlank(type)){ 
			filters.append(" type = ?"); 
			params.add(type); }
		
		if(StringUtils.isNotBlank(status)){
			filters.append(" AND status = ?"); 
			params.add(status);
			
		}
		
		if(time > 0){
			if(time == 1){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) <= 24"); }
			if(time == 2){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) BETWEEN 24 AND 72");}
			if(time == 3){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) >= 72"); }
		}
	
		if(StringUtils.isNotBlank(server)){
			filters.append(" AND server = ?"); 
			params.add(server);
		}
		
		if(record != 0){
			filters.append(" GROUP BY `server` ) AS eachLastRecord ON fullRecord.id = eachLastRecord.sid ");
		}

		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_SERVER_REPORT_LIST_BY_SEARCH, filters.toString()), numRecordHandler, params.toArray());
	}
	
	private static final String GET_SERVER_REPORT_LIST_BY_SEARCH = "SELECT fullRecord.* FROM tbl_server_report as fullRecord  ---filter---  ORDER BY id DESC LIMIT ?, ? ";
	public List<ServerRuntimeReportBean> getServerReportListBySearch(String type,String status,String server,int time,int record,int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(record == 0){
			filters.append(" WHERE ");
		}else{
			filters.append(" INNER JOIN ( SELECT max(id) AS sid FROM tbl_server_report WHERE ");
		}
		
		if(StringUtils.isNotBlank(type)){ 
			filters.append(" type = ?"); 
			params.add(type); 
		}
		
		if(StringUtils.isNotBlank(status)){
			filters.append(" AND status = ?"); 
			params.add(status);	
		}
		
		if(time > 0){
			if(time == 1){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) <= 24"); }
			if(time == 2){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) BETWEEN 24 AND 72");}
			if(time == 3){ filters.append(" AND TIMESTAMPDIFF(HOUR, startDate, endDate) >= 72"); }
		}
		
		if(StringUtils.isNotBlank(server)){
			filters.append(" AND server = ?"); 
			params.add(server);
		}
		
		if(record != 0){
			filters.append(" GROUP BY `server` ) AS eachLastRecord ON fullRecord.id = eachLastRecord.sid ");
		}
		params.add(start);
		params.add(offset);
		
		return (List<ServerRuntimeReportBean>)queryRunner.query(DatabaseUtil.replaceSQL(GET_SERVER_REPORT_LIST_BY_SEARCH, filters.toString()), beanListHandler, params.toArray());
	}
	
	private static final String GET_LATEST_SERVER_RUNTIME_REPORT_LIST = "SELECT fullRecord.* FROM tbl_server_report as fullRecord INNER JOIN ( SELECT max(id) AS sid FROM tbl_server_report WHERE `status` = ? AND type = ? GROUP BY `server` ) AS eachLastRecord ON fullRecord.id = eachLastRecord.sid ORDER BY fullRecord.`server` ASC";
	public List<ServerRuntimeReportBean> getLastestRuntimeAllServer(String status, String appType) throws SQLException{
		return queryRunner.query(GET_LATEST_SERVER_RUNTIME_REPORT_LIST, beanListHandler, new Object[]{status, appType});
	}
	
	
}
