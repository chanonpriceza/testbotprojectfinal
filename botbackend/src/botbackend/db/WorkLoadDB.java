package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.WorkLoadBean;
import botbackend.db.utils.NumRecordResultSetHandler;


public class WorkLoadDB {

	private DataSource dataSource;	
	private ResultSetHandler<List<WorkLoadBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	
	public static final String SELECT_BY_ID_LIMIT = "SELECT * FROM tbl_workload WHERE merchantId = ? ORDER BY depth DESC LIMIT ?";
	public static final String SELECT_BY_ID_START_AND_LIMIT = "SELECT * FROM tbl_workload INNER JOIN (SELECT merchantId, url FROM tbl_workload WHERE merchantId = ? LIMIT ?,?) AS p ON tbl_workload.merchantId = p.merchantId AND tbl_workload.url = p.url ";
	public static final String COUNT_WORKLOAD = "SELECT COUNT(url) FROM tbl_workload WHERE merchantId = ?";
	QueryRunner queryRunner; 		
	
	public WorkLoadDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		numRecordHandler = new NumRecordResultSetHandler();
		beanListHandler = new BeanListHandler<WorkLoadBean>(WorkLoadBean.class);
	}
	
	public List<WorkLoadBean> selectWorkLoadById(int merchantId, int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_ID_LIMIT, beanListHandler, new Object[] {merchantId, limit});
	}
	
	public List<WorkLoadBean> selectWorkLoadByIdHaveStartAndLimit(int merchantId, int start,int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_ID_START_AND_LIMIT, beanListHandler, new Object[] {merchantId,start,limit});
	}
	
	public int countWorkload(int merchantId) throws SQLException {
		return queryRunner.query(COUNT_WORKLOAD, numRecordHandler, new Object[] {merchantId});
	}
}
