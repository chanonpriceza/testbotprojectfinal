package botbackend.db;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import botbackend.bean.BotQualityReportBean;
import botbackend.bean.MerchantBean;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;
import botbackend.utils.ViewUtil;
import db.manager.DatabaseUtil;

public class BotQualityReportDB {
	private ResultSetHandler<Integer> numRecordHandler;
	private ResultSetHandler<BotQualityReportBean> beanHandler;
	BeanListHandler<BotQualityReportBean> beanListHandler;
	QueryRunner queryRunner; 		
	
	public BotQualityReportDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<BotQualityReportBean>(BotQualityReportBean.class);
		beanListHandler = new BeanListHandler<BotQualityReportBean>(BotQualityReportBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	public static final String INSERT_BATCH = "INSERT INTO tbl_bot_quality_report" +
			" (`type`, `merchantId`, `name`, `status`, `server`, `time`, `allCount`, `reportDate`) " +
			"VALUES (?,?,?,?,?,?,?,?)";
	public int[] insertReport(List<BotQualityReportBean> botQualityReportBeanList) throws SQLException {
		int i = 0;
		Object[][] obj = new Object[botQualityReportBeanList.size()][];
		
		for (BotQualityReportBean botQualityReportBean : botQualityReportBeanList) {
			obj[i] = new Object[]{ 
					botQualityReportBean.getType(),
					botQualityReportBean.getMerchantId(),
					botQualityReportBean.getName(),
					botQualityReportBean.getStatus(),
					botQualityReportBean.getServer(),
					botQualityReportBean.getTime(),
					botQualityReportBean.getAllCount(),
					botQualityReportBean.getReportDate()
				};
			i++;
		}
		return queryRunner.batch(INSERT_BATCH, obj);
	}
	public static final String  GET_REPORT_SUMMARY = "SELECT name,allCount from tbl_bot_quality_report where type = 'summary'";
	public List<BotQualityReportBean> getReportSummary() throws SQLException{
		return queryRunner.query(GET_REPORT_SUMMARY, beanListHandler);
	}
	
	public static final String COUNT_DATA_BY_FILTER = "SELECT COUNT(id) FROM tbl_bot_quality_report WHERE type = 'merchant' ---filter---";
	public int countDataByFilter (String searchParam,String inputProductCountRange,String inputTimeRange,String activeType) throws SQLException {
		StringBuilder filter = new StringBuilder();
		List<Object> param = new  ArrayList<>();
		if(StringUtils.isNotBlank(searchParam)) {
			if(StringUtils.isNumeric(searchParam)) {
				filter.append(" AND merchantId = ?"); param.add(searchParam);
			}else{
				filter.append(" AND name LIKE ?");param.add("%"+searchParam+"%");
			}
		}
		if(StringUtils.isNotBlank(inputTimeRange)) {
			if(StringUtils.isNumeric(inputTimeRange)) {
				if("1".equals(inputTimeRange)) {
					filter.append(" AND time = '<1day'");
				}else if ("2".equals(inputTimeRange)) {
					filter.append(" AND time = '<3day'");
				}else if ("3".equals(inputTimeRange)) {
					filter.append(" AND time = '>3day'");
				}else if ("4".equals(inputTimeRange)) {
					filter.append(" AND time = 'other'");
				}
			}
		}
		if(StringUtils.isNotBlank(inputProductCountRange)) {
			if(StringUtils.isNumeric(inputProductCountRange)) {
				if("0".equals(inputProductCountRange)) {
					filter.append(" AND allCount = 0");
				}else if("1".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 1 and 1000");
				}else if ("2".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 10001 and 50000 ");
				}else if ("3".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 50001 and 100000 ");
				}else if ("4".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 100001 and 300000 ");
				}else if ("5".equals(inputProductCountRange)) {
					filter.append("AND allCount >300000");
				}
			}	
		}
		if(NumberUtils.isCreatable(activeType)) {
			if(StringUtils.isNotBlank(activeType)) {
		    	int compare = Integer.parseInt(activeType);
		    	String str = ViewUtil.generateShowActive(compare);
		    	if(compare >= 0) {
		    		filter.append(" AND status = ?"); param.add(str);
		    	}
			}
		}
			return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_DATA_BY_FILTER, filter.toString()),numRecordHandler,param.toArray());
	}
	public static final String GET_REPORT_ALL_MERCHANT_BY_FILTER = "SELECT  merchantId,name,status,time,allCount from tbl_bot_quality_report where type = 'merchant' ---filter--- LIMIT ?,?";
	public List<BotQualityReportBean> getReportMerchantByFilter(int start , int offset , String searchParam,String inputProductCountRange,String inputTimeRange,String activeType,String sortBy) throws SQLException{
		StringBuilder filter = new StringBuilder();
		List<Object> param = new ArrayList<Object>();
		if(StringUtils.isNotBlank(searchParam)) {
			if(StringUtils.isNumeric(searchParam)) {
				filter.append(" AND merchantId = ?"); param.add(searchParam);
			}else {
				filter.append(" AND name LIKE ?");param.add("%"+searchParam+"%");
			}
		}
		if(StringUtils.isNotBlank(inputTimeRange)) {
			if(StringUtils.isNumeric(inputTimeRange)) {
				if("1".equals(inputTimeRange)) {
					filter.append(" AND time = '<1day'");
				}else if ("2".equals(inputTimeRange)) {
					filter.append(" AND time = '<3day'");
				}else if ("3".equals(inputTimeRange)) {
					filter.append(" AND time = '>3day'");
				}else if ("4".equals(inputTimeRange)) {
					filter.append(" AND time = 'other'");
				}
			}
		}
		if(StringUtils.isNotBlank(inputProductCountRange)) {
			if(StringUtils.isNumeric(inputProductCountRange)) {
				if("0".equals(inputProductCountRange)) {
					filter.append(" AND allCount = 0");
				}else if("1".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 1 and 1000");
				}else if ("2".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 10001 and 50000 ");
				}else if ("3".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 50001 and 100000 ");
				}else if ("4".equals(inputProductCountRange)) {
					filter.append(" AND allCount between 100001 and 300000 ");
				}else if ("5".equals(inputProductCountRange)) {
					filter.append("AND allCount >300000");
				}
			}	
		}
		if(NumberUtils.isCreatable(activeType)) {
			if(StringUtils.isNotBlank(activeType)) {
		    	int compare = Integer.parseInt(activeType);
		    	String str = ViewUtil.generateShowActive(compare);
		    	if(compare >= 0) {
		    		filter.append(" AND status = ?"); param.add(str);
		    	}
			}
		}
		if(StringUtils.isNotBlank(sortBy)) {
		   if(StringUtils.isNumeric(sortBy)) {
				if("1".equals(sortBy)) {
					filter.append(" ORDER BY merchantId ASC ");
				}else if ("2".equals(sortBy)) {
					filter.append(" ORDER BY merchantId DESC");
				}else if ("3".equals(sortBy)) {
					filter.append(" ORDER BY allCount ASC");
				}else if ("4".equals(sortBy)) {
					filter.append(" ORDER BY allCount DESC");
				}
	        }   
		}
		param.add(start);
		param.add(offset);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_REPORT_ALL_MERCHANT_BY_FILTER, filter.toString()),beanListHandler,param.toArray());
	}
	public static final String GET_ALL_DATA = "SELECT * FROM tbl_bot_quality_report";
	public List<BotQualityReportBean> getAllData() throws SQLException {
		return queryRunner.query(GET_ALL_DATA, beanListHandler);
	}
	
	public static final String COUNT_DATA_BY_FILTER_BY_MERCHANT_HAVE_LIMIT = "SELECT * FROM tbl_bot_quality_report WHERE merchantId = ? LIMIT 1";
	public BotQualityReportBean getDataByMerchantHaveLimit(int merchantId) throws SQLException {
		return queryRunner.query(COUNT_DATA_BY_FILTER_BY_MERCHANT_HAVE_LIMIT, beanHandler,merchantId);
	}
	
	public static final String DELETE_ALL_DATA = "DELETE FROM tbl_bot_quality_report";
	public int clearAllData() throws SQLException {
		return queryRunner.update(DELETE_ALL_DATA);
	}

}
