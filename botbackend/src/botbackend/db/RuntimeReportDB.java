package botbackend.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.RuntimeReportBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class RuntimeReportDB {
	private DataSource dateSource;
	private QueryRunner queryRunner;
	private NumRecordResultSetHandler numRecordHandler;
	private ResultSetHandler<List<RuntimeReportBean>> beanListHandler;	
	
	public RuntimeReportDB(DataSource dataSource) {
		this.dateSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		numRecordHandler = new NumRecordResultSetHandler();
		beanListHandler = new BeanListHandler<RuntimeReportBean>(RuntimeReportBean.class);
	}
	
	private String INSERT_RUNTIME = "INSERT INTO tbl_runtime_report(BotServerName, ProcessType, Status, UpdateDate) VALUES(?, ?, ?, ?)";
	public int insertRuntime(RuntimeReportBean bean) throws SQLException {
		return queryRunner.update(INSERT_RUNTIME, new Object[] {
				bean.getBotServerName(),
				bean.getProcessType(),
				bean.getStatus(),
				bean.getUpdateDate()
		});
	}
	
	private String UPDATE_RUNTIME = "UPDATE tbl_runtime_report SET Status = ?, UpdateDate = ? WHERE BotServerName = ? AND ProcessType = ? AND Status = 0";
	public int updateRuntime(RuntimeReportBean bean) throws SQLException {
		return queryRunner.update(UPDATE_RUNTIME, new Object[] {
				bean.getStatus(),
				bean.getUpdateDate(),
				bean.getBotServerName(),
				bean.getProcessType()
		});
	}
	
	private String GET_REPORT_SEND_EMAIL = "SELECT * FROM tbl_runtime_report WHERE updateDate = ? ORDER BY status Desc, botServerName, processType";
	public List<RuntimeReportBean> getReportSendEmail(Date date) throws SQLException {
		return queryRunner.query(GET_REPORT_SEND_EMAIL, beanListHandler, new Object[] {date});
	}
}
