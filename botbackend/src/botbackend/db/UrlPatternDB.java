package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.UrlPatternBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;
import botbackend.db.utils.NumRecordResultSetHandler;

public class UrlPatternDB extends UrlPatternBean {
	private ResultSetHandler<UrlPatternBean> beanHandler;
	private ResultSetHandler<List<UrlPatternBean>> beanListHandler;
	private IntegerListValueResultSetHandler integerListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	
	public UrlPatternDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<UrlPatternBean>(UrlPatternBean.class);
		beanListHandler = new BeanListHandler<UrlPatternBean>(UrlPatternBean.class);
		integerListHandler = new IntegerListValueResultSetHandler();
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String getUrlPatternByMerchantId = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? ORDER BY field (action, 'CONTINUE', 'MATCH', 'DROP', 'REMOVE', 'STARTURL', 'FEED'), id"; 
	public List<UrlPatternBean> getUrlPatternByMerchantId(int id) throws SQLException {
		return queryRunner.query(getUrlPatternByMerchantId, beanListHandler, id);
	}
	
	private static final String deleteUrlPatternByMerchantId = "DELETE FROM tbl_url_pattern WHERE merchantId = ?";
//	private static final String deleteUrlPatternByMerchantId = "DELETE FROM tbl_url_pattern WHERE merchantId = ? AND action != 'MATCH'";
	public int deleteUrlPatternByMerchantId(int id) throws SQLException {
		return queryRunner.update(deleteUrlPatternByMerchantId, id);
	}
	
	private static final String insertUrlPatternBatch = "INSERT INTO `tbl_url_pattern` (`merchantId`, `condition`, `group`, `value`, `action`, `categoryId`, `keyword`) VALUES ---filter---";
	public int insertUrlPatternBarch(List<UrlPatternBean> urlPatternList, int merchantId) throws SQLException {
		StringBuilder countObject = new StringBuilder();	
		List<Object> params = new ArrayList();
		
		for(UrlPatternBean upBean : urlPatternList){
			countObject.append(",(?, ?, ?, ?, ?, ?, ?)");
			params.add(merchantId);
			params.add(upBean.getCondition());
			params.add(upBean.getGroup());
			params.add(upBean.getValue());
			params.add(upBean.getAction());
			params.add(upBean.getCategoryId());
			params.add(upBean.getKeyword());
		}
		
		String filter = countObject.substring(1).toString();
		String finalSql = DatabaseUtil.replaceSQL(insertUrlPatternBatch, filter);
		return queryRunner.update(finalSql, params.toArray());
	}	
	
	private static final String COUNT_ACTION_BY_MERCHANTID = "SELECT COUNT(id) FROM tbl_url_pattern WHERE merchantId = ?  AND action = ?"; 
	public int countActionByMerchantId(int merchantId, String action) throws SQLException {
		return queryRunner.query(COUNT_ACTION_BY_MERCHANTID, numRecordHandler, merchantId , action);
	}
	
	public static final String SELECT_BY_MERCHANTID_ACTION = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? and action = ?";
	public List<UrlPatternBean> findUrlPattern(int merchantId, String action) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_ACTION, beanListHandler, new Object[] {merchantId, action});
	} 
	
	public static final String SELECT_BY_MERCHANTID_ACTION_WITH_LIMIT = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? and action = ? limit ?";
	public List<UrlPatternBean> findUrlPatternWithLimit(int merchantId, String action,int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_ACTION_WITH_LIMIT, beanListHandler, new Object[] {merchantId, action,limit});
	} 
	
}
