package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.MerchantMonitorBean;

public class MerchantMonitorDB {
	
	private ResultSetHandler<MerchantMonitorBean> beanHandler;
	QueryRunner queryRunner;
	BeanListHandler<MerchantMonitorBean> beanListHandler;

	public MerchantMonitorDB(DataSource dataSource) {
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MerchantMonitorBean>(MerchantMonitorBean.class);
		beanListHandler = new BeanListHandler<MerchantMonitorBean>(MerchantMonitorBean.class);
	}
	
	private static final String SELECT_DATA_BY_ANALYZE_MERCHANT_TYPE = "select id,type,merchantId,topic,detail,count,firstFoundDate,recentFoundDate from tbl_merchant_monitoring where type = ? and merchantId = ? and topic = ?";
	public List<MerchantMonitorBean> selectOldData(MerchantMonitorBean bean) throws SQLException {
		return queryRunner.query(SELECT_DATA_BY_ANALYZE_MERCHANT_TYPE, beanListHandler, bean.getType(),bean.getMerchantId(),bean.getTopic());
	}
	
	private static final String SELECT_DATA_OLD = "select id,type,merchantId,topic,detail,count,firstFoundDate,recentFoundDate from tbl_merchant_monitoring where type = ? and merchantId = ?";
	public List<MerchantMonitorBean> selectOldDataNonTopic(MerchantMonitorBean bean) throws SQLException {
		return queryRunner.query(SELECT_DATA_OLD, beanListHandler, bean.getType(),bean.getMerchantId());
	}
	
	private static final String INSERT_MONITOR_ANALYZE_LIST = "INSERT INTO tbl_merchant_monitoring_history(type,merchantId,topic,detail,count,firstFoundDate, recentFoundDate,solveDetail,solveBy) VALUES (?,?,?,?,?,?,?,?,?)";
	public int[] insertMonitorHistoryList(List<MerchantMonitorBean> bean) throws SQLException {
		int i = 0;
		Object[][] obj = new Object[bean.size()][];
		for (MerchantMonitorBean merchantMonitorBean : bean) {
			obj[i] = new Object[]{ 
					merchantMonitorBean.getType(),
					merchantMonitorBean.getMerchantId(),
					merchantMonitorBean.getTopic(),
					merchantMonitorBean.getDetail(),
					merchantMonitorBean.getCount(),
					merchantMonitorBean.getFirstFoundDate(),
					merchantMonitorBean.getRecentFoundDate(),
					"SYSTEM",
					"SYSTEM"
				};
			i++;
		}
		return queryRunner.batch(INSERT_MONITOR_ANALYZE_LIST, obj);
	}
	
	private static String DELETE_OLD_DATA  = "delete from tbl_merchant_monitoring where type = ?  and merchantId = ?";
	public int deleteOldData(MerchantMonitorBean bean) throws SQLException {
		return queryRunner.update(DELETE_OLD_DATA,bean.getType(),bean.getMerchantId());
	}
	
	private static String INSERT_MONITOR_DATA = "INSERT INTO tbl_merchant_monitoring(type,merchantId,topic,detail,count,firstFoundDate, recentFoundDate) VALUES (?,?,?,?,?,?,?)";
	public int insertMonitor(MerchantMonitorBean bean) throws SQLException {
		return queryRunner.update(INSERT_MONITOR_DATA,new Object[] {					
				bean.getType(),
				bean.getMerchantId(),
				bean.getTopic(),
				bean.getDetail(),
				bean.getCount(),
				bean.getFirstFoundDate(),
				bean.getRecentFoundDate(),
				});
	}
	
	private static String UPDATE_COUNT = "update  tbl_merchant_monitoring set count = ? , recentFoundDate = now() where id = ?";
	public int updateMonitor(MerchantMonitorBean bean) throws SQLException {		
		return queryRunner.update(UPDATE_COUNT,new Object[] {		
				bean.getCount(),
				bean.getId()
		});
	}
}

