package botbackend.db.utils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import botbackend.bean.BotConfigBean;
import botbackend.bean.ConfigBean;
import botbackend.db.BotConfigDB;
import botbackend.db.ConfigDB;
import botbackend.system.BaseConfig;

public class DatabaseFactory {

	private static DataSource dataSource;
	private static DataSource webDataSource;
	private static DataSource lnwShopSource;
	private static Map<String, DataSource> productDataSource;
	private static int initSize = 2;
	private static int maxActive = 6;
	private static int maxIdle = 3;
	private static int minIdle = 1;
	private static String dbDriver = "com.mysql.jdbc.Driver";
		
	public static synchronized DataSource getDataSourceInstance() {		
		if(dataSource == null) {
			dataSource = DatabaseUtil.createDataSource(BaseConfig.DB_CONFIG_URL, BaseConfig.DB_CONFIG_DRIVER, BaseConfig.DB_CONFIG_USER, BaseConfig.DB_CONFIG_PASSWORD,
					BaseConfig.DB_CONFIG_INIT_SIZE, BaseConfig.DB_CONFIG_MAX_ACTIVE, BaseConfig.DB_CONFIG_MAX_SIZE, BaseConfig.DB_CONFIG_MIN_SIZE);
			
			System.out.println("###### set up datasource ######");
			System.out.println("DB_INIT_SIZE = "+ initSize);
			System.out.println("DB_MAX_ACTIVE = "+ maxActive);
			System.out.println("DB_MAX_IDLE = "+ maxIdle);
			System.out.println("DB_MIN_IDLE = "+ minIdle);
		}		
		return dataSource;	
	}	
	
	public static synchronized void registerDataSource(String connectURI, String driverClassName, String username, String password, int initialSize, int maxActive, int maxIdle, int minIdle) throws Exception {		
		if(dataSource == null) {
			dataSource = DatabaseUtil.createDataSource(connectURI, driverClassName, username, password, initialSize, maxActive, maxIdle, minIdle);
		} else {
			throw new Exception("datasource already create");
		}
	}	
	
	public static synchronized DataSource getWebDataSourceInstance() {		
		if(webDataSource == null) {
			webDataSource = DatabaseUtil.createDataSource(BaseConfig.WEB_PROD_CONFIG_URL, BaseConfig.WEB_PROD_CONFIG_DRIVER, BaseConfig.WEB_PROD_CONFIG_USER, BaseConfig.WEB_PROD_CONFIG_PASSWORD,BaseConfig.WEB_PROD_CONFIG_INIT_SIZE, BaseConfig.WEB_PROD_CONFIG_MAX_ACTIVE, BaseConfig.WEB_PROD_CONFIG_MAX_SIZE, BaseConfig.WEB_PROD_CONFIG_MIN_SIZE);
			System.out.println("###### set up web datasource ######");	
			return webDataSource;	
		}
		return webDataSource; 
	}
	
	public static synchronized DataSource getlnwShopDataSourceInstance(ConfigDB configDB ,BotConfigDB botConfigDB) {	
		try {
			if(lnwShopSource == null) {
				ConfigBean configBean;
				BotConfigBean botConfigBean;
				configBean = configDB.getByName("lnwshop.manager.server");
				if(configBean == null) return null;
				
				botConfigBean = botConfigDB.findBotConfig(configBean.getConfigValue());
				if(botConfigBean != null) {
					lnwShopSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), dbDriver, botConfigBean.getUsername(), botConfigBean.getPassword(), initSize);
					System.out.println("###### set up lnwshop datasource ######");	
					return lnwShopSource;						
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lnwShopSource; 
	}
	
	public static synchronized void initializeBotDataSourceInstance() {	
		try {
			if(db.manager.DatabaseFactory.getConfigDSInstance() == null) {
				db.manager.DatabaseFactory.registerConfigDataSource(BaseConfig.DB_CONFIG_URL, BaseConfig.DB_CONFIG_DRIVER,BaseConfig.DB_CONFIG_USER,BaseConfig.DB_CONFIG_PASSWORD,BaseConfig.DB_CONFIG_INIT_SIZE);
				System.out.println("###### set up Bot ConfigDataSource datasource ######");	
			}
			if(db.manager.DatabaseFactory.getBotDSInstance()==null ) {
				db.manager.DatabaseFactory.registerDataSource(BaseConfig.DB_CONFIG_URL, BaseConfig.DB_CONFIG_DRIVER,BaseConfig.DB_CONFIG_USER,BaseConfig.DB_CONFIG_PASSWORD,BaseConfig.DB_CONFIG_INIT_SIZE);
				System.out.println("###### set up Bot DataSource datasource ######");	
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized Map<String, DataSource> getProductDataSourceInstance(BotConfigDB botConfigDB) {		
		if(productDataSource == null) {
			productDataSource = new HashMap<String, DataSource>();
			
			List<BotConfigBean> botConfigList;	
			
			try {
				botConfigList = botConfigDB.getBotConfigList();
				
				for (BotConfigBean botConfigBean: botConfigList) {
					if(botConfigBean.getUrl() != null){
						productDataSource.put(botConfigBean.getServer(), 
								DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), dbDriver, botConfigBean.getUsername(),
								botConfigBean.getPassword(), 2));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}	
			System.out.println("###### set up product datasource ######");	
		}
		return productDataSource; 
	}
}
