package botbackend.db.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;

public class StringListValueResultSetHandler implements ResultSetHandler<List<String>>{
	
	public List<String> handle(ResultSet rs) throws SQLException {
		List<String> list = new ArrayList<String>();
		if(rs != null){
			while (rs.next()) {
				list.add(rs.getString(1));
			}
		}
		return list;
	}
	
}