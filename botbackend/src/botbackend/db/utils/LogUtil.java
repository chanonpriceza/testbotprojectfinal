package botbackend.db.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import botbackend.bean.UserBean;

public class LogUtil {
	final static Logger trackLogger = Logger.getLogger("trackLogger");
	
	public static void info(HttpServletRequest request, String page, String action){
		try{
			HttpSession session = request.getSession(false);
			UserBean b = (UserBean)session.getAttribute("userData");
			String user = b.getUserName();
			trackLogger.info("Page:"+ page +"|Username:"+ user +"|" + action);
		}catch(Exception e){
			cannotLog(e, page, action);
		}
	}
	
	public static void cannotLog(Exception e, String page, String action){
		trackLogger.error("Page:"+ page +"|" + action, e);
	}
	
}
