package botbackend.db.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import botbackend.db.BanKeywordDB;
import botbackend.db.BotConfigDB;
import botbackend.db.BotQualityReportDB;
import botbackend.db.ConfigDB;
import botbackend.db.DashboardDB;
import botbackend.db.FocusUpdateDB;
import botbackend.db.LnwShopMerchantDB;
import botbackend.db.LnwShopPatternDB;
import botbackend.db.ManualProductDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.MerchantMonitorDB;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.MonitorDB;
import botbackend.db.NotificationApiDB;
import botbackend.db.MonitoringDB;
import botbackend.db.MonitoringDetailDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.PluginDB;
import botbackend.db.ProductDataDB;
import botbackend.db.ProductUrlDB;
import botbackend.db.ProductWaitSyncDB;
import botbackend.db.SendDataDB;
import botbackend.db.ServerRuntimeReportDB;
import botbackend.db.TaskDB;
import botbackend.db.TaskDetailDB;
import botbackend.db.TrackBotStateDB;
import botbackend.db.TrackMerchantStatisticDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.UrlPatternHistoryDB;
import botbackend.db.UserDB;
import botbackend.db.WorkLoadDB;
import botbackend.db.api.LnwShopProductDataDB;
import botbackend.db.web.MerchantMappingDB;
import botbackend.db.web.WebProductDataDB;
import db.NotificationDB;


public class DatabaseManager {
	
	private static final DatabaseManager databaseManager = new DatabaseManager();

	public static DatabaseManager getDatabaseManager() {
		return databaseManager;
	}
	
	private BanKeywordDB banKeywordDB;
	private DashboardDB dashboardDB;
	private BotConfigDB botConfigDB;
	private ConfigDB configDB;
	private UserDB userDB;
	private MerchantDB merchantDB;
	private MerchantConfigDB merchantConfigDB;
	private UrlPatternDB urlPatternDB;
	private UrlPatternHistoryDB urlPatternHistoryDB;
	private TaskDB taskDB;
	private ParserConfigDB parserConfigDB;
	private MerchantRuntimeReportDB merchantruntimeReportDB;
	private ServerRuntimeReportDB serverruntimeReportDB;
	private LnwShopProductDataDB lnwshopProductDataDB;
	private LnwShopMerchantDB lnwshopmerchantDB;
	private LnwShopPatternDB lnwshoppatternDB;
	private MonitorDB monitorDB;
	private PluginDB pluginDB;
	private WorkLoadDB workLoadDB;
	private ProductWaitSyncDB productWaitSyncDB;
	private FocusUpdateDB focusUpdateDB;
	private ManualProductDB manualProductDB;
	private TaskDetailDB taskDetailDB;
	private MerchantMappingDB merchantMappingDB;
	private TrackBotStateDB trackBotStateDB;
	private TrackMerchantStatisticDB trackMerchantStatisticDB;
	private WebProductDataDB webProductDataDB;
	private NotificationDB notificationDB;
	private BotQualityReportDB botqualityreportDB;
	private MerchantMonitorDB merchantMonitorDB;
	private NotificationApiDB notiApi;
	private MonitoringDB monitoringDB;
	private MonitoringDetailDB monitoringDetailDB;
	private Map<String, ProductDataDB> productDBMap;
	private Map<String, ProductUrlDB> productUrlDBMap;
	private Map<String, WorkLoadDB> workLoadDBMap;
	private Map<String, SendDataDB> sendDataDBMap;
	
	private DatabaseManager() {
		initialize();
	}
	
	private void initialize(){
		
		botConfigDB = new BotConfigDB(DatabaseFactory.getDataSourceInstance());

		configDB = new ConfigDB(DatabaseFactory.getDataSourceInstance());
		userDB = new UserDB(DatabaseFactory.getDataSourceInstance());
		merchantDB = new MerchantDB(DatabaseFactory.getDataSourceInstance());
		merchantConfigDB = new MerchantConfigDB(DatabaseFactory.getDataSourceInstance());
		urlPatternDB = new UrlPatternDB(DatabaseFactory.getDataSourceInstance());
		urlPatternHistoryDB = new UrlPatternHistoryDB(DatabaseFactory.getDataSourceInstance());
		taskDB = new TaskDB(DatabaseFactory.getDataSourceInstance());
		taskDetailDB = new TaskDetailDB(DatabaseFactory.getDataSourceInstance());
		parserConfigDB = new ParserConfigDB(DatabaseFactory.getDataSourceInstance());
		merchantruntimeReportDB = new MerchantRuntimeReportDB(DatabaseFactory.getDataSourceInstance());
		serverruntimeReportDB = new ServerRuntimeReportDB(DatabaseFactory.getDataSourceInstance());
		monitorDB =  new MonitorDB(DatabaseFactory.getDataSourceInstance());
		dashboardDB = new DashboardDB(DatabaseFactory.getDataSourceInstance());
		pluginDB = new PluginDB(DatabaseFactory.getDataSourceInstance());
		workLoadDB = new WorkLoadDB(DatabaseFactory.getDataSourceInstance());
		productWaitSyncDB = new ProductWaitSyncDB(DatabaseFactory.getDataSourceInstance());
		focusUpdateDB = new FocusUpdateDB(DatabaseFactory.getDataSourceInstance());
		notificationDB = new NotificationDB(DatabaseFactory.getDataSourceInstance());
		manualProductDB = new ManualProductDB(DatabaseFactory.getDataSourceInstance());
		trackBotStateDB = new TrackBotStateDB(DatabaseFactory.getDataSourceInstance());
		botqualityreportDB = new BotQualityReportDB(DatabaseFactory.getDataSourceInstance());
		merchantMappingDB = new MerchantMappingDB(DatabaseFactory.getWebDataSourceInstance());
		trackMerchantStatisticDB = new TrackMerchantStatisticDB(DatabaseFactory.getWebDataSourceInstance());
		webProductDataDB = new WebProductDataDB(DatabaseFactory.getWebDataSourceInstance());
		monitoringDB = new MonitoringDB(DatabaseFactory.getDataSourceInstance());
		monitoringDetailDB = new MonitoringDetailDB(DatabaseFactory.getDataSourceInstance());
		
		banKeywordDB = new BanKeywordDB(DatabaseFactory.getDataSourceInstance());
		merchantMonitorDB = new MerchantMonitorDB(DatabaseFactory.getDataSourceInstance());
		notiApi = new NotificationApiDB(DatabaseFactory.getDataSourceInstance());
		DatabaseFactory.initializeBotDataSourceInstance(); 
		createProductDataBean();
		createProductUrlBean();
		createWorkLoadBean();
		createSendDataBean();
		
		/* lnwShop API */
		lnwshopProductDataDB = new LnwShopProductDataDB(DatabaseFactory.getDataSourceInstance());
		
		/* lnwShop BACKEND */
		if(DatabaseFactory.getlnwShopDataSourceInstance(configDB,botConfigDB)!=null) {
			lnwshopmerchantDB = new LnwShopMerchantDB(DatabaseFactory.getlnwShopDataSourceInstance(configDB,botConfigDB));
			lnwshoppatternDB = new LnwShopPatternDB(DatabaseFactory.getlnwShopDataSourceInstance(configDB, botConfigDB));
		}
	}
	

	private void createProductDataBean() {
		productDBMap = new HashMap<String, ProductDataDB>();
		for (Entry<String, DataSource> e: DatabaseFactory.getProductDataSourceInstance(botConfigDB).entrySet()) {
			productDBMap.put(e.getKey(), new ProductDataDB(e.getValue()));
		}
	}

	private void createProductUrlBean() {
		productUrlDBMap = new HashMap<String, ProductUrlDB>();
		for (Entry<String, DataSource> e: DatabaseFactory.getProductDataSourceInstance(botConfigDB).entrySet()) {
			productUrlDBMap.put(e.getKey(), new ProductUrlDB(e.getValue()));
		}
	}

	private void createWorkLoadBean() {
		workLoadDBMap = new HashMap<String, WorkLoadDB>();
		for (Entry<String, DataSource> e: DatabaseFactory.getProductDataSourceInstance(botConfigDB).entrySet()) {
			workLoadDBMap.put(e.getKey(), new WorkLoadDB(e.getValue()));
		}
	}

	public MonitoringDB getMonitoringDB() {
		return monitoringDB;
	}

	public void setMonitoringDB(MonitoringDB monitoringDB) {
		this.monitoringDB = monitoringDB;
	}

	public MonitoringDetailDB getMonitoringDetailDB() {
		return monitoringDetailDB;
	}

	public void setMonitoringDetailDB(MonitoringDetailDB monitoringDetailDB) {
		this.monitoringDetailDB = monitoringDetailDB;
	}

	private void createSendDataBean(){
		sendDataDBMap = new HashMap<String, SendDataDB>();
		for (Entry<String, DataSource> e: DatabaseFactory.getProductDataSourceInstance(botConfigDB).entrySet()) {
			sendDataDBMap.put(e.getKey(), new SendDataDB(e.getValue()));
		}
	}
	
	public LnwShopProductDataDB getLnwshopProductDataDB() {
		return lnwshopProductDataDB;
	}

	public UserDB getUserDB(){
		return userDB;
	}

	public MerchantDB getMerchantDB() {
		return merchantDB;
	}

	public MerchantConfigDB getMerchantConfigDB() {
		return merchantConfigDB;
	}

	public UrlPatternDB getUrlPatternDB() {
		return urlPatternDB;
	}
	
	public UrlPatternHistoryDB getUrlPatternHistoryDB() {
		return urlPatternHistoryDB;
	}
	
	public TaskDB getTaskDB() {
		return taskDB;
	}

	public TaskDetailDB getTaskDetailDB(){
		return taskDetailDB;
	}
	
	public ParserConfigDB getParserConfigDB() {
		return parserConfigDB;
	}
	
	public MerchantMappingDB getMerchantMappingDB(){
		return merchantMappingDB;
	}
	
	public TrackBotStateDB getTrackBotStateDB() {
		return trackBotStateDB;
	}
	
	public TrackMerchantStatisticDB getTrackMerchantStatisticDB() {
		return trackMerchantStatisticDB;
	}

	public WebProductDataDB getWebProductDataDB() {
		return webProductDataDB;
	}
	
	public MonitorDB getMonitorDB(){
		return monitorDB;
	}
	
	public PluginDB getPluginDB() {
		return pluginDB;
	}
	
	public MerchantRuntimeReportDB getMerchantruntimeReportDB() {
		return merchantruntimeReportDB;
	}

	public ServerRuntimeReportDB getServerruntimeReportDB() {
		return serverruntimeReportDB;
	}

	public ProductDataDB getProductDataDB(String botServer) {
		return productDBMap.get(botServer);
	}
	
	public ProductUrlDB getProductUrlDB(String botServer) {
		return productUrlDBMap.get(botServer);
	}
	
	public WorkLoadDB getWorkLoadDB(String botServer) {
		return workLoadDBMap.get(botServer);
	}
	
	public SendDataDB getSendDataDB(String botServer) {
		return sendDataDBMap.get(botServer);
	}
	
	public BotConfigDB getBotConfigDB() {
		return botConfigDB;
	}
	
	public ConfigDB getConfigDB() {
		return configDB;
	}
	
	public WorkLoadDB getWorkLoadDB() {
		return workLoadDB;
	}
	
	public ProductWaitSyncDB getProductWaitSyncDB() {
		return productWaitSyncDB;
	}

	public FocusUpdateDB getFocusUpdateDB() {
		return focusUpdateDB;
	}
	
	public ManualProductDB getManualProductDB() {
		return manualProductDB;
	}

	public DashboardDB getDashboardDB() {
		return dashboardDB;
	}

	public NotificationDB getNotificationDB() {
		return notificationDB;
	}

	public BanKeywordDB getBanKeywordDB() {
		return banKeywordDB;
	}

	public BotQualityReportDB getBotqualityreportDB() {
		return botqualityreportDB;
	}

	public LnwShopMerchantDB getLnwshopmerchantDB() {
		return lnwshopmerchantDB;
	}

	public LnwShopPatternDB getLnwshopPatternDB() {
		return lnwshoppatternDB;
	}
	
	public MerchantMonitorDB getMerchantMonitorDB() {
		return merchantMonitorDB;
	}
	
	public NotificationApiDB getNotiApiDB() {
		return notiApi; 
	}
	
}
