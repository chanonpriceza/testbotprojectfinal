package botbackend.db.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.ResultSetHandler;

public class NumRecordResultSetHandler implements ResultSetHandler{
	
	public Integer handle(ResultSet rs) throws SQLException {
		if (rs.next()) {
			return rs.getInt(1);
		}
		return -1;
	}
	
}