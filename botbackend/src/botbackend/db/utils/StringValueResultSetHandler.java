package botbackend.db.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.ResultSetHandler;

public class StringValueResultSetHandler implements ResultSetHandler<String>{
	
	public String handle(ResultSet rs) throws SQLException {
		if (rs.next()) {
			return rs.getString(1);
		}
		return null;
	}
	
}