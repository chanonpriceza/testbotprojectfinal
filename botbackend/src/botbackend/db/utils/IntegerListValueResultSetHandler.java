package botbackend.db.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;

public class IntegerListValueResultSetHandler implements ResultSetHandler<List<Integer>>{
	
	public List<Integer> handle(ResultSet rs) throws SQLException {
		List<Integer> list = new ArrayList<Integer>();
		if(rs != null){
			while (rs.next()) {
				list.add(rs.getInt(1));
			}
		}
		return list;
	}
	
}