package botbackend.db.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import botbackend.utils.ACCPropertyParser;

public class DatabaseUtil {

	static {		
		try {			
			Class.forName("com.mysql.jdbc.Driver").newInstance();			 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}		
	}	
	
	// use to validate connection instead of server status monitor
	private static final String SQL_TEST = "select 1 from DUAL";

    public static DataSource createDataSource(String connectURI, String driverClassName, String username, String password, int initialSize) {
        BasicDataSource ds = new BasicDataSource();
        // TODO
        ds.setDriverClassName(driverClassName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(connectURI);
        ds.setInitialSize(initialSize);
//        ds.setMinIdle(5);
//        ds.setMaxWait(5000);
//        ds.setTimeBetweenEvictionRunsMillis(1000);
//        ds.setNumTestsPerEvictionRun(3);
        ds.setTestOnBorrow(true);
        ds.setValidationQuery(SQL_TEST);
        return ds;
    }
    
    
    /**
     *  initialSize 0 	The initial number of connections that are created when the pool is started.
     *  maxActive 	8 	The maximum number of active connections that can be allocated from this pool at the same time, or negative for no limit.
     *  maxIdle 	8 	The maximum number of connections that can remain idle in the pool, without extra ones being released, or negative for no limit.
     *  minIdle 	0 	The minimum number of connections that can remain idle in the pool, without extra ones being created, or zero to create none.
     *  maxWait 	indefinitely 	The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception, or -1 to wait indefinitely. 
     */
    public static DataSource createDataSource(String connectURI, String driverClassName, String username, String password, 
    		int initialSize, int maxActive, int maxIdle, int minIdle) {
        BasicDataSource ds = new BasicDataSource();
        // TODO
        ds.setDriverClassName(driverClassName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(connectURI);
        ds.setInitialSize(initialSize);
        
        ds.setMaxActive(maxActive);
        ds.setMaxIdle(maxIdle);
        ds.setMinIdle(minIdle);        
        
        ds.setTestOnBorrow(true);
        ds.setValidationQuery(SQL_TEST);
        return ds;        
        //return net.bull.javamelody.JdbcWrapper.SINGLETON.createDataSourceProxy(ds);
    }


    public static void closeDataSource(DataSource ds) throws SQLException {
        BasicDataSource bds = (BasicDataSource) ds;
        bds.close();
    }

    public static void closeResultSet(ResultSet rs) {
        if(rs != null) {
        	try {
				rs.close();
			} catch (SQLException e) {				
			}
        }
    }
    
    public static void closeStatement(Statement stmt) {
        if(stmt != null) {
        	try {
        		stmt.close();
			} catch (SQLException e) {				
			}
        }
    }
    
    public static void closePreparedStatement(PreparedStatement prep) {
        if(prep != null) {
        	try {
        		prep.close();
			} catch (SQLException e) {				
			}
        }
    }
    
    public static void closeConnection(Connection conn) {
        if(conn != null) {
        	try {
        		conn.close();
			} catch (SQLException e) {				
			}
        }
    }
    
    public static Connection createConnection(ACCPropertyParser conProp) throws SQLException{         	
    	return DriverManager.getConnection(conProp.getString("url"), conProp.getString("username"), conProp.getString("password"));    	
    }
    
    public static String replaceSQL(String base, String filter) {		
		base = base.replaceFirst("---filter---", filter);
		return base;
	}
    public static String replaceSQL(String base, String[] filter) {
    	
    	if(filter != null && filter.length>0 ){
    		for(int index=1;index<=filter.length ; index++){
    			base = base.replace("---filter"+index+"---", filter[index-1]);
    		}
    	}
    	
		return base;
	}
    public static String replaceAllSQL(String base, String filter) {
		base = base.replace("---filter---", filter);
		return base;
	}
        
    public static String replaceSQLOrder(String base, String order) {		
		base = base.replaceFirst("---order---", order);
		//System.out.println(base);
		return base;
	}
        
    public static String replaceSQL(String base, String filter, String order) {		
		base = base.replaceFirst("---filter---", filter);
		base = base.replaceFirst("---order---", order);
		//System.out.println(base);
		return base;
	}
        
}
