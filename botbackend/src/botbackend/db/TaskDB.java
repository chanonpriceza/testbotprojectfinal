package botbackend.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import botbackend.bean.TaskBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;

public class TaskDB {
	private ResultSetHandler<TaskBean> beanHandler;
	private ResultSetHandler<List<TaskBean>> beanListHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	private ResultSetHandler<Integer> numRecordHandler;
	
	public TaskDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<TaskBean>(TaskBean.class);
		beanListHandler = new BeanListHandler<TaskBean>(TaskBean.class);
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_TASK_DATA_BY_PROCESS = "SELECT * FROM tbl_backend_task WHERE process IN (---filter---) ORDER BY createDate DESC LIMIT ?,? "; 
	public List<TaskBean> getTaskDataByProcess(List<String> processList, int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(processList != null && processList.size() > 0){
			for(String  process : processList){
				filters.append(",?");
				params.add(process);

			}
			params.add(start);
			params.add(offset);
			String filter = filters.substring(1).toString();
			return queryRunner.query(DatabaseUtil.replaceSQL(GET_TASK_DATA_BY_PROCESS, filter), beanListHandler, params.toArray());
			
		}
		return null;
	
	}
	
	private static final String COUNT_ALL_TASK_DATA_BY_PROCESS = "SELECT COUNT(id) FROM tbl_backend_task WHERE process IN (---filter---) "; 
	public int countTaskDataByProcess(List<String> processList) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(processList != null && processList.size() > 0){
			for(String  process : processList){
				filters.append(",?");
				params.add(process);

			}
			String filter = filters.substring(1).toString();
			return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_ALL_TASK_DATA_BY_PROCESS, filter), numRecordHandler, params.toArray());
			
		}
		return 0;
	}
	private static final String GET_TASK_BETWEEN_DATE = "SELECT t.* FROM `tbl_backend_task` t  join tbl_backend_task_history h on t.id = h.taskId where  t.createDate BETWEEN ? and ? and h.STATUS = ? and t.status = ?;";
	public List<TaskBean> getTaskDataBetweenDate(String startDate,String endDate,String status) throws SQLException{
		return queryRunner.query(GET_TASK_BETWEEN_DATE, beanListHandler,startDate,endDate,status,status);
	}
	
	private static final String GET_TASK_DATA = "SELECT a.*,b.owner as updatedBy,b.message as result , c.active as active FROM tbl_backend_task a left join `tbl_backend_task_history` b on a.id = b.taskId and b.id  in (select max(id) from tbl_backend_task_history  group by taskid) left join tbl_merchant c  on a.merchantId = c.id ---filter--- ORDER BY createDate DESC LIMIT ?,? "; 
	public List<TaskBean> getTaskData(String searchParam,String processType,String statusType,String ownerType,int searchType , int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		StringBuilder subFilters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(searchParam)){
			if(searchType == -1) {
				filters.append("where ");
			}else if(searchType == 0){
				if(StringUtils.isNumeric(searchParam)){
					filters.append("where a.id in (select task.id from tbl_backend_task task where task.merchantId = ? )");
					params.add(searchParam);
				}else{
					filters.append("where a.id in (-1)");
				}					
			}else if(searchType == 1){
				filters.append("where a.id in (select task.id from tbl_backend_task task left join tbl_merchant m on task.merchantId = m.id where m.name like ? )"); 
				params.add("%"+searchParam+"%");					
			}else if(searchType == 2){
				filters.append("where a.id in (select task.id from tbl_backend_task task where task.issue like ? )");
				params.add("%"+searchParam+"%");					
			}else if(searchType == 3){
				filters.append("where a.id in (select th.id from tbl_backend_task_history th where th.message like ? )");
				params.add("%"+searchParam+"%");					
			}else if(searchType == 4){
				filters.append("where a.id in (select task.id from tbl_backend_task task where task.id = ? )");
				params.add(searchParam);					
			}
		}
		if(filters.toString().contains("where a.id in")){
			if(Util.isContainBracket(ownerType)){
				filters.append(" AND a.owner IN "+ownerType);  	
			}else if(StringUtils.isNotBlank(ownerType)) {
				filters.append(" AND a.owner = ?");  		params.add(ownerType);
			}
			
			if(Util.isContainBracket(statusType)){
				filters.append(" AND a.status IN "+statusType);  	
			}else if(StringUtils.isNotBlank(statusType)) {
				filters.append(" AND a.status = ?");  		params.add(statusType);
			}
			
			if(Util.isContainBracket(processType)){
				filters.append(" AND a.process IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				filters.append(" AND a.process = ?");  		params.add(processType);
			}
		}else{
			if(Util.isContainBracket(ownerType)){
				subFilters.append(" AND a.owner IN "+ownerType);  	
			}else if(StringUtils.isNotBlank(ownerType)) {
				subFilters.append(" AND a.owner = ?");  		params.add(ownerType);
			}
			
			if(Util.isContainBracket(statusType)){
				subFilters.append(" AND a.status IN "+statusType);  	
			}else if(StringUtils.isNotBlank(statusType)) {
				subFilters.append(" AND a.status = ?");  		params.add(statusType);
			}
			
			if(Util.isContainBracket(processType)){
				subFilters.append(" AND a.process IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				subFilters.append(" AND a.process = ?");  		params.add(processType);
			}
			
			if(subFilters.length() != 0){
				subFilters = subFilters.delete(0, 4);
				subFilters = subFilters.insert(0, "WHERE ");
				filters.append(subFilters.toString());
			}
		}
		
		params.add(start);
		params.add(offset);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_TASK_DATA, filters.toString()), beanListHandler, params.toArray());
	
	}
	private static final String GET_TASK_DATA_BY_ID = "SELECT * FROM tbl_backend_task WHERE id = ?";
	public TaskBean getTaskById(int taskId) throws SQLException {
		return queryRunner.query(GET_TASK_DATA_BY_ID, beanHandler, taskId);
	}
	
	private static final String COUNT_ALL_TASK_DATA_SPEED = " SELECT COUNT(id) FROM tbl_backend_task";
	private static final String COUNT_ALL_TASK_DATA = "SELECT COUNT(a.id) FROM tbl_backend_task a left join `tbl_backend_task_history` b on a.id = b.taskId and b.id in (select max(id) from tbl_backend_task_history group by taskid) ---filter--- "; 
	public int countTaskData(String searchParam,String processType,String statusType,String ownerType,int searchType) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		StringBuilder subFilters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(searchParam)){
			if(searchType == 0){
				if(StringUtils.isNumeric(searchParam)){
					filters.append("where a.id in (select task.id from tbl_backend_task task where task.merchantId = ? )");
					params.add(searchParam);
				}else{
					filters.append("where a.id in (-1)");
				}					
			}else if(searchType == 1){
				filters.append("where a.id in (select task.id from tbl_backend_task task left join tbl_merchant m on task.merchantId = m.id where m.name like ? )"); 
				params.add("%"+searchParam+"%");					
			}else if(searchType == 2){
				filters.append("where a.id in (select task.id from tbl_backend_task task where task.issue like ? )");
				params.add("%"+searchParam+"%");					
			}else if(searchType == 3){
				filters.append("where a.id in (select th.id from tbl_backend_task_history th where th.message like ? )");
				params.add("%"+searchParam+"%");					
			}else if(searchType == 4){
				filters.append("where a.id in (select task.id from tbl_backend_task task where task.id = ? )");
				params.add(searchParam);					
			}
		}
		if(filters.toString().contains("where a.id in")){
			if(Util.isContainBracket(ownerType)){
				filters.append(" AND a.owner IN "+ownerType);  	
			}else if(StringUtils.isNotBlank(ownerType)) {
				filters.append(" AND a.owner = ?");  		params.add(ownerType);
			}
			
			if(Util.isContainBracket(statusType)){
				filters.append(" AND a.status IN "+statusType);  	
			}else if(StringUtils.isNotBlank(statusType)) {
				filters.append(" AND a.status = ?");  		params.add(statusType);
			}
			
			if(Util.isContainBracket(processType)){
				filters.append(" AND a.process IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				filters.append(" AND a.process = ?");  		params.add(processType);
			}
		}else{
			if(Util.isContainBracket(ownerType)){
				subFilters.append(" AND a.owner IN "+ownerType);  	
			}else if(StringUtils.isNotBlank(ownerType)) {
				subFilters.append(" AND a.owner = ?");  		params.add(ownerType);
			}
			
			if(Util.isContainBracket(statusType)){
				subFilters.append(" AND a.status IN "+statusType);  	
			}else if(StringUtils.isNotBlank(statusType)) {
				subFilters.append(" AND a.status = ?");  		params.add(statusType);
			}
			
			if(Util.isContainBracket(processType)){
				subFilters.append(" AND a.process IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				subFilters.append(" AND a.process = ?");  		params.add(processType);
			}
			
			if(subFilters.length() != 0){
				subFilters = subFilters.delete(0, 4);
				subFilters = subFilters.insert(0, "WHERE ");
				filters.append(subFilters.toString());
			}
		}
		
		if(filters.length() != 0){
			return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_ALL_TASK_DATA, filters.toString()), numRecordHandler, params.toArray());
		}
		
		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_ALL_TASK_DATA_SPEED,""), numRecordHandler);
	}
	
	private static final String DELETE_TASK_BY_ID = "DELETE  FROM tbl_backend_task WHERE id =  ? "; 
	public int deleteTaskById(int taskId) throws SQLException {
		return queryRunner.update(DELETE_TASK_BY_ID, taskId);
	}
	
	private static final String SELECT_TASK_BY_ID = "SELECT a.*,b.owner as updatedBy, b.message as result FROM tbl_backend_task a left join `tbl_backend_task_history` b on a.id = b.taskId and b.id in (select max(id) from tbl_backend_task_history group by taskid) WHERE a.id =  ? "; 
	public TaskBean selectTaskById(int taskId) throws SQLException {
		return queryRunner.query(SELECT_TASK_BY_ID, beanHandler, taskId);
	}
	
	private static final String SELECT_MERCHANTNAME_BY_ID = "SELECT name as merchantName FROM tbl_merchant WHERE id = ?;"; 
	public List<TaskBean> selectMerchantNameById(int merchantID) throws SQLException {
		return queryRunner.query(SELECT_MERCHANTNAME_BY_ID, beanListHandler, merchantID);
	}

	
	private static final String INSERT_TASK = "INSERT INTO tbl_backend_task (process, issue, merchantId, data, status, owner, createDate,issueType) VALUES (?, ?, ?, ?, ?, ?, NOW(),?)";
	public int insertTask(TaskBean taskBeans) throws SQLException {
		return queryRunner.update(INSERT_TASK, new Object[] {
				taskBeans.getProcess(),
				taskBeans.getIssue(),
				taskBeans.getMerchantId(),
				taskBeans.getData(),
				taskBeans.getStatus(),				
				taskBeans.getOwner(),
				taskBeans.getIssueType()
				
		});
	}	
	
	private final String INSERT_TASK_RETURN = "INSERT INTO tbl_backend_task (process, issue, merchantId, data, status, owner, createDate,issueType) VALUES (?, ?, ?, ?, ?, ?, NOW(),?)";
	public int insertTaskReturnID(TaskBean taskBeans) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(INSERT_TASK_RETURN, new String[]{"id"});
			queryRunner.fillStatement(pstmt, new Object[] {	
					taskBeans.getProcess(),
					taskBeans.getIssue(),
					taskBeans.getMerchantId(),
					taskBeans.getData(),
					taskBeans.getStatus(),
					taskBeans.getOwner(),
					taskBeans.getIssueType()
			}); 		
			
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			if(rs.next()) {
				return rs.getInt(1);
			}
	
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DatabaseUtil.closeStatement(pstmt);				
			DatabaseUtil.closeConnection(conn);			
		}		
		return -1;		
	}
	
	private static final String UPDATE_TASK_DATA = "UPDATE tbl_backend_task SET data = ?, status = ?  WHERE id = ?";
	public int updateTaskData(String data, String status, int id) throws SQLException {
		return queryRunner.update(UPDATE_TASK_DATA, data , status, id);
	}	
	
	private static final String UPDATE_TASK_DATA_BATCH = "UPDATE tbl_backend_task SET data = ?, status = ?  WHERE id = ?";
	public int[] updateTaskDataBatch(List<TaskBean> taskList) throws SQLException {
		int i = 0;
		Object[][] obj = new Object[taskList.size()][3];
		
		for (TaskBean task : taskList) {
			obj[i][0] = task.getData();
			obj[i][1] =	task.getStatus();
			obj[i][2] =	task.getId();
			i++;
		}
		return queryRunner.batch(UPDATE_TASK_DATA_BATCH, obj );
	}	
	
	private static final String UPDATE_TASK_STATUS = "UPDATE tbl_backend_task SET status = ?  WHERE id = ?";
	public int updateTaskStatus(String status, int id) throws SQLException {
		return queryRunner.update(UPDATE_TASK_STATUS, new Object[] { status, id});
	}	
	
	private static final String GET_TASK_BY_INTERVAL_WITH_PROCESS = "SELECT * FROM tbl_backend_task WHERE createDate <  date_sub(now(), INTERVAL ? day) ---filter--- AND  process = ?";
	public List<TaskBean> getTaskByInterval(int interval , List<String> keepStatus,  String process) throws SQLException{
		String filter = "";
		if(keepStatus != null && keepStatus.size() > 0){
			StringBuilder filterStatus = new StringBuilder();
			for (String status : keepStatus) {
				filterStatus.append(",");
				filterStatus.append("'" + status + "'");
			}
			filter = "AND status NOT IN (" + filterStatus.toString().substring(1) + ")";
		}
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_TASK_BY_INTERVAL_WITH_PROCESS, filter),beanListHandler, interval , process);

	}
	
	private static final String GET_TASK_BY_STATUS_WITH_PROCESS = "SELECT * FROM tbl_backend_task WHERE  status = ? AND  process = ? ORDER BY createDate DESC ";
	public List<TaskBean> getTaskByStatusWithProcess(String status,  String process) throws SQLException{
		return queryRunner.query(GET_TASK_BY_STATUS_WITH_PROCESS,beanListHandler, status, process);
	}
	

	
	private static final String UPDATE_TASK_WORKTIME = "UPDATE tbl_backend_task SET worktime = ? where id = ?";
	public int updateTaskWorktime(int worktime,int id) throws SQLException{
		return queryRunner.update(UPDATE_TASK_WORKTIME, new Object[] { worktime, id});
	}
	
	private static final String GET_TASK_PROCESS_WORKDAY = "SELECT * from tbl_backend_task where status in ---filter---  and worktime = ? and process <> 'getSupportPerformanceReport' ;";
	public List<TaskBean> getTaskToProcessWorkDay(String[] status,int worktime) throws SQLException{
		String filter = "";
		if(status != null && status.length > 0){
			StringBuilder filterStatus = new StringBuilder();
			for (String s : status) {
				filterStatus.append(",");
				filterStatus.append("\"" + s + "\"");
			}
			filter = "("+filterStatus.toString().substring(1)+")";
		}
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_TASK_PROCESS_WORKDAY,filter), beanListHandler,worktime);
	}
	public List<TaskBean> OnlyGu(String sql) throws SQLException{
		return queryRunner.query(sql, beanListHandler);
	}
	
}
