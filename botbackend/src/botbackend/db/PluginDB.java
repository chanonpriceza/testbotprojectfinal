package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;


import botbackend.bean.PluginConfigBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;

public class PluginDB {
	
	private ResultSetHandler<List<PluginConfigBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	
	QueryRunner queryRunner;

	public PluginDB(DataSource dataSource) {
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<PluginConfigBean>(PluginConfigBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}	
	
	private static final String GET_CONFIG_PLUGIN = "SELECT bus.*  FROM tbl_backend_user_dashboard bus LEFT JOIN tbl_backend_user usr on bus.userId = usr.userId WHERE usr.userId = ?"; 
	public List<PluginConfigBean> getConfigPluginByUserId(int userId) throws SQLException {
		return queryRunner.query(GET_CONFIG_PLUGIN, beanListHandler, userId);
	}	
	
	private final String INSERT_CONFIG_PLUGIN = "INSERT INTO tbl_backend_user_dashboard (userId,pluginType) VALUES (?,?)";
	public int insertConfigPlugin(int userid,String strArray ) throws SQLException {
		return queryRunner.insert(INSERT_CONFIG_PLUGIN, numRecordHandler, new Object[] {userid ,strArray});
	}
	
	private static final String UPDATE_CONFIG_PLUGIN = "UPDATE tbl_backend_user_dashboard SET pluginPosition = ? WHERE id = ? ";
	public int[] updateConfigPlugin(Object[][] param) throws SQLException {
		return queryRunner.batch(UPDATE_CONFIG_PLUGIN, param );
	}
	
	private static final String DELETE_PLUGIN_CONFIG_BY_ID = "DELETE FROM tbl_backend_user_dashboard WHERE userId = ? AND id = ?";
	public int deletePluginById(int userid ,int idPosition) throws SQLException {
		return queryRunner.update(DELETE_PLUGIN_CONFIG_BY_ID, userid,idPosition);
	}
}
