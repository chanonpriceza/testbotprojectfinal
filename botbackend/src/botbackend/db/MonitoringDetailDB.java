package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.MonitoringDetailBean;
import botbackend.bean.TaskBean;
import botbackend.bean.TaskDetailBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class MonitoringDetailDB {
	private ResultSetHandler<MonitoringDetailBean> beanHandler;
	private ResultSetHandler<List<MonitoringDetailBean>> beanListHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	private ResultSetHandler<Integer> numRecordHandler;
	
	public MonitoringDetailDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MonitoringDetailBean>(MonitoringDetailBean.class);
		beanListHandler = new BeanListHandler<MonitoringDetailBean>(MonitoringDetailBean.class);
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_DATA_BY_ID = "SELECT * FROM tbl_merchant_monitoring_history WHERE  merchantId = ? AND type = ? ORDER BY id DESC ";
	public List<MonitoringDetailBean> getDataById(int merchantId, String type) throws SQLException{
		return queryRunner.query(GET_DATA_BY_ID, beanListHandler, merchantId,type);
	}
}
