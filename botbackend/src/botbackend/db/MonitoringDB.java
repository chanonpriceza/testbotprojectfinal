package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import botbackend.bean.MonitoringBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;

public class MonitoringDB {
	private ResultSetHandler<MonitoringBean> beanHandler;
	private ResultSetHandler<List<MonitoringBean>> beanListHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	private ResultSetHandler<Integer> numRecordHandler;
	
	public MonitoringDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MonitoringBean>(MonitoringBean.class);
		beanListHandler = new BeanListHandler<MonitoringBean>(MonitoringBean.class);
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String COUNT_ALL_DATA_SPEED = " SELECT COUNT(id) FROM tbl_merchant_monitoring";
	private static final String COUNT_ALL_DATA = "SELECT COUNT(a.id) FROM tbl_merchant_monitoring a left join `tbl_merchant_monitoring_history` b on a.id = b.id and b.id in (select max(id) from tbl_merchant_monitoring_history group by id) ---filter--- "; 
	public int countMonitorData(String searchParam,String topicType,String processType,int searchType) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		StringBuilder subFilters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(searchParam)){
			if(searchType == 0){
				if(StringUtils.isNumeric(searchParam)){
					filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring where monitoring.merchantId = ? )");
					params.add(searchParam);
				}else{
					filters.append("where a.id in (-1)");
				}					
			}else if(searchType == 1){
				filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring left join tbl_merchant m on monitoring.merchantId = m.id where m.name like ? )"); 
				params.add("%"+searchParam+"%");					
			}else if(searchType == 2){
				filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring where monitoring.id = ? )");
				params.add(searchParam);					
			}
		}
		if(filters.toString().contains("where a.id in")){
			
			if(Util.isContainBracket(processType)){
				filters.append(" AND a.type IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				filters.append(" AND a.type = ?");  		params.add(processType);
			}
			
			if(Util.isContainBracket(topicType)){
				filters.append(" AND a.topic IN "+topicType);  	
			}else if(StringUtils.isNotBlank(topicType)) {
				filters.append(" AND a.topic = ?");  		params.add(topicType);
			}
		}else{
			
			if(Util.isContainBracket(processType)){
				subFilters.append(" AND a.type IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				subFilters.append(" AND a.type = ?");  		params.add(processType);
			}
			
			if(Util.isContainBracket(topicType)){
				subFilters.append(" AND a.topic IN "+topicType);  	
			}else if(StringUtils.isNotBlank(topicType)) {
				subFilters.append(" AND a.topic = ?");  		params.add(topicType);
			}
			
			if(subFilters.length() != 0){
				subFilters = subFilters.delete(0, 4);
				subFilters = subFilters.insert(0, "WHERE ");
				filters.append(subFilters.toString());
			}
		}
		
		if(filters.length() != 0){
			return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_ALL_DATA, filters.toString()), numRecordHandler, params.toArray());
		}

		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_ALL_DATA_SPEED,""), numRecordHandler);
	}
	
	private static final String GET_DATA = "SELECT a.*,b.solveBy as solveBy,b.solveDetail as solveDetail , c.active as active ,c.name as merchantName  FROM tbl_merchant_monitoring a left join `tbl_merchant_monitoring_history` b on a.id = b.id and b.id in (select max(id) from tbl_merchant_monitoring_history  group by id) left join tbl_merchant c  on a.merchantId = c.id ---filter---  ORDER BY firstFoundDate DESC LIMIT ?,?"; 
	public List<MonitoringBean> getData(String searchParam,String topicType,String processType,int searchType , int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		StringBuilder subFilters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(searchParam)){
			if(searchType == -1) {
				filters.append("where ");
			}else if(searchType == 0){
				if(StringUtils.isNumeric(searchParam)){
					filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring where monitoring.merchantId = ? )");
					params.add(searchParam);
				}else{
					filters.append("where a.id in (-1)");
				}					
			}else if(searchType == 1){
				filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring left join tbl_merchant m on monitoring.merchantId = m.id where m.name like ? )"); 
				params.add("%"+searchParam+"%");					
			}else if(searchType == 2){
				filters.append("where a.id in (select monitoring.id from tbl_merchant_monitoring monitoring where monitoring.id = ? )");
				params.add(searchParam);					
			}
		}
		if(filters.toString().contains("where a.id in")){
			
			if(Util.isContainBracket(processType)){
				filters.append(" AND a.type IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				filters.append(" AND a.type = ?");  		params.add(processType);
			}
			
			if(Util.isContainBracket(topicType)){
				filters.append(" AND a.topic IN "+topicType);  	
			}else if(StringUtils.isNotBlank(topicType)) {
				filters.append(" AND a.topic = ?");  		params.add(topicType);
			}
		}else{
			
			if(Util.isContainBracket(processType)){
				subFilters.append(" AND a.type IN "+processType);  	
			}else if(StringUtils.isNotBlank(processType)) {
				subFilters.append(" AND a.type = ?");  		params.add(processType);
			}
			
			if(Util.isContainBracket(topicType)){
				subFilters.append(" AND a.topic IN "+topicType);  	
			}else if(StringUtils.isNotBlank(topicType)) {
				subFilters.append(" AND a.topic = ?");  		params.add(topicType);
			}
			
			if(subFilters.length() != 0){
				subFilters = subFilters.delete(0, 4);
				subFilters = subFilters.insert(0, "WHERE ");
				filters.append(subFilters.toString());
			}
		}
		
		params.add(start);
		params.add(offset);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_DATA, filters.toString()), beanListHandler, params.toArray());
	
	}
	
	private static final String GET_DATA_BY_ID = "SELECT * FROM tbl_merchant_monitoring WHERE id = ?";
	public MonitoringBean getDataById(int monitoringId , String type) throws SQLException {
		return queryRunner.query(GET_DATA_BY_ID, beanHandler, monitoringId , type);
	}
	
	private static final String GET_DATA_WITH_NAME_BY_ID = "SELECT a.*,m.name as merchantName FROM tbl_merchant_monitoring a left join tbl_merchant m  on a.merchantId = m.id WHERE a.id = ? AND type = ? ";
	public MonitoringBean getDataByIdWithType(int monitoringId , String type) throws SQLException {
		return queryRunner.query(GET_DATA_WITH_NAME_BY_ID, beanHandler, monitoringId , type);
	}
	
	private static final String SELECT_DATA_BY_ID = "SELECT a.*,b.solveBy as solveBy, b.solveDetail as solveDetail FROM tbl_merchant_monitoring a left join `tbl_merchant_monitoring_history` b on a.id = b.id and b.id in (select max(id) from tbl_merchant_monitoring_history group by id) WHERE a.id =  ?"; 
	public MonitoringBean selectById(int monitoringId) throws SQLException {
		return queryRunner.query(SELECT_DATA_BY_ID, beanHandler, monitoringId);
	}
}
