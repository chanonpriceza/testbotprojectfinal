package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import botbackend.bean.MerchantBean;
import botbackend.bean.api.LnwShopMerchantBean;
import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;
import utils.FilterUtil;

public class LnwShopMerchantDB {
	private ResultSetHandler<LnwShopMerchantBean> beanHandler;
	private ResultSetHandler<List<LnwShopMerchantBean>> beanListHandler;
	private ResultSetHandler<List<LnwShopProductDataBean>> beanDataListHandler;
	private IntegerListValueResultSetHandler integerListHandler;
	private DataSource dataSource;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	public LnwShopMerchantDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<LnwShopMerchantBean>(LnwShopMerchantBean.class);
		beanListHandler = new BeanListHandler<LnwShopMerchantBean>(LnwShopMerchantBean.class);
		beanDataListHandler = new BeanListHandler<LnwShopProductDataBean>(LnwShopProductDataBean.class);
		integerListHandler = new IntegerListValueResultSetHandler();
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	public static final String SELECT_BY_LNWID_NAME = "SELECT * FROM tbl_product_data WHERE lnw_merchant_id = ? LIMIT ?,? ";
	public List<LnwShopProductDataBean> findProductData(String lnw_merchant_id, int start,int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_LNWID_NAME, beanDataListHandler, new Object[] {lnw_merchant_id,start,limit}); 				
	}
	
	private static final String UPDATE_LNW_MERCHANTS = "UPDATE tbl_merchant SET  id = ?, active = ?, addParentName = ?, addProductId = ? , addContactPrice = ? , forceContactPrice = ? , forceUpdateCat = ? , priceMultiply = ?  WHERE lnw_id = ? ";
	public int updateLnwMerchant(LnwShopMerchantBean lnwMerchantBean) throws SQLException {
		return queryRunner.update(UPDATE_LNW_MERCHANTS, new Object[] {
				lnwMerchantBean.getId(),
				lnwMerchantBean.getActive(),
				lnwMerchantBean.getAddParentName(),
				lnwMerchantBean.getAddProductId(),
				lnwMerchantBean.getAddContactPrice(),
				lnwMerchantBean.getForceContactPrice(),
				lnwMerchantBean.getForceUpdateCat(),
				lnwMerchantBean.getPriceMultiply(),
				lnwMerchantBean.getLnw_id()
		});
	}	
	
	private static final String GET_MERCHANT_BY_LNWSHOP_ID = "SELECT * FROM tbl_merchant WHERE lnw_id = ? ";
	public LnwShopMerchantBean getMerchantBeanByLnwShopID(String lnwMerchantId) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_LNWSHOP_ID, beanHandler, lnwMerchantId);
	}
	
	private static final String CHECK_COUNT_PRODUCTDA_LNWSHOP = "SELECT count(lnw_merchant_id) FROM tbl_product_data WHERE lnw_merchant_id = ? ";
	public int checkCountProductDataLnwShop(String lnwMerchantId) throws SQLException {		
		return queryRunner.query(CHECK_COUNT_PRODUCTDA_LNWSHOP,numRecordHandler,lnwMerchantId);
	}
	
	private static final String CHECK_COUNT_PRODUCTDA_LNWSHOP_HAVE_LIMIT = "SELECT count(lnw_merchant_id) FROM tbl_product_data WHERE lnw_merchant_id = ? LIMIT 1";
	public int checkCountProductDataLnwShopHaveLimit(String lnwMerchantId) throws SQLException {		
		return queryRunner.query(CHECK_COUNT_PRODUCTDA_LNWSHOP_HAVE_LIMIT,numRecordHandler,lnwMerchantId);
	}
	
	private static final String COUNT_SEARCH_MERCHANT = "SELECT COUNT(id) FROM tbl_merchant WHERE ---filter--- ";
	public int countSearchMerchant(String searchParam, String activeType) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		if(activeType.equals("-1")){	
			filters.append(" active > -1 "); 
		}else if(StringUtils.isNotBlank(activeType)){
				filters.append(" active =  ? "); 
				params.add(activeType);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				filters.append(" AND id = ?"); 
				params.add(searchParam);					
			}else{
				filters.append(" AND lnw_id LIKE ?"); 
				params.add("%"+searchParam+"%");					
			}
		}
		
		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_SEARCH_MERCHANT, filters.toString()), numRecordHandler, params.toArray());
	}
	
	private static final String GET_SEARCH_MERCHANT = "SELECT * FROM tbl_merchant WHERE ---filter--- ORDER BY `lnw_id` LIMIT ?, ?"; 
	public List<LnwShopMerchantBean> getSearchMerchant(String searchParam, String activeType, int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();	
		
		if(activeType.equals("-1")){	
			filters.append(" active > -1 ");
		}else if(Util.isContainBracket(activeType)){	
			filters.append(" active IN "+activeType); 
		}else if(StringUtils.isNotBlank(activeType)){
				filters.append(" active =  ? "); 
				params.add(activeType);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				filters.append(" AND id = ?"); 
				params.add(searchParam);					
			}else{
				filters.append(" AND lnw_id LIKE ?"); 
				params.add("%"+searchParam+"%");					
			}
		}
		
	
		params.add(start);
		params.add(offset);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_SEARCH_MERCHANT, filters.toString()), beanListHandler, params.toArray());
	}
}
