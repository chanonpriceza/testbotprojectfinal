package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BanKeywordBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class BanKeywordDB extends BanKeywordBean{
	
	private ResultSetHandler<Integer> numRecordHandler;
	private BeanListHandler<BanKeywordBean> beanListHandler;
	QueryRunner queryRunner;
	
	
	public BanKeywordDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		this.numRecordHandler = new NumRecordResultSetHandler();
		this.beanListHandler = new BeanListHandler<BanKeywordBean>(BanKeywordBean.class);
	}
	
	private static final String COUNT_BAN_KEYWORD = "SELECT count(id) FROM tbl_ban_keyword";
	public int countBanKeyword() throws SQLException{
		return queryRunner.query(COUNT_BAN_KEYWORD, numRecordHandler);
	}
	
	private static final String GET_MIN_ID = "SELECT MAX(id) FROM tbl_ban_keyword";
	public int getMaxId() throws SQLException{
		return queryRunner.query(GET_MIN_ID, numRecordHandler);
	}
	
	private static final String DELETE_ALL = "DELETE FROM tbl_ban_keyword WHERE id <= ?";
	public int deleteAllById(int id) throws SQLException{
		return queryRunner.update(DELETE_ALL, id);
	}
	
	private static final String INSERT_BAN_KEYWORD_BATCH = "INSERT INTO tbl_ban_keyword(merchantIdList, keyword, nonKeyword,"
			+ " requiredPhrase, nonCategory, minPrice) VALUES (?,?,?,?,?,?)";
	public int[] insertBatch(List<BanKeywordBean> bkwBeanList) throws SQLException {
		if(bkwBeanList == null || bkwBeanList.size() == 0) return null;
		
		Object[][] obj = new Object[bkwBeanList.size()][6];
		int count = 0;
		for(BanKeywordBean bkwBean : bkwBeanList) {
			obj[count][0] = bkwBean.getMerchantIdList();
			obj[count][1] = bkwBean.getKeyword();
			obj[count][2] = bkwBean.getNonkeyword();
			obj[count][3] = bkwBean.getRequiredPhrase();
			obj[count][4] = bkwBean.getNonCategory();
			obj[count][5] = bkwBean.getMinPrice();
			count++;
		}
		
		return queryRunner.batch(INSERT_BAN_KEYWORD_BATCH, obj);
	}
	private static final String QUERY_ALL = "select * from tbl_ban_keyword";
	public List<BanKeywordBean> load() throws SQLException {
		return queryRunner.query(QUERY_ALL, beanListHandler);
	}
}
