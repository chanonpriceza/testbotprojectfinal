package botbackend.db.web;

import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import botbackend.bean.web.ProductDataBean;

public class WebProductDataDB {
	private ResultSetHandler<ProductDataBean> beanHandler;
	QueryRunner queryRunner;
	
	public WebProductDataDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ProductDataBean>(ProductDataBean.class);
	}
	
	private static final String GET_BY_ID = "SELECT * FROM tbl_product_data WHERE id = ?"; 
	public ProductDataBean getById(int id) throws SQLException {
		return queryRunner.query(GET_BY_ID, beanHandler, id);
	}
	
}
