package botbackend.db.web;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BanKeywordBean;
import botbackend.db.utils.NumRecordResultSetHandler;


public class WebBanKeywordDB extends BanKeywordBean {
	private ResultSetHandler<List<BanKeywordBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	
	public WebBanKeywordDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		this.beanListHandler = new BeanListHandler<BanKeywordBean>(BanKeywordBean.class);
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String COUNT_BAN_KEYWORD = "SELECT count(id) FROM tbl_ban_keyword";
	public int countBanKeyword() throws SQLException{
		return queryRunner.query(COUNT_BAN_KEYWORD, numRecordHandler);
	}
	
	private static final String FIND_BAN_KEYWORD_BEAN = "SELECT * FROM tbl_ban_keyword LIMIT ?, ?";
	public List<BanKeywordBean> getBanKeywordLimit(int start,int offset) throws SQLException {
		return queryRunner.query(FIND_BAN_KEYWORD_BEAN,beanListHandler,start,offset);			
	}
	
}
