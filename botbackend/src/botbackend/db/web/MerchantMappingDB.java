package botbackend.db.web;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.MerchantBean;
import botbackend.bean.web.WebMerchantBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class MerchantMappingDB extends MerchantBean {
	private ResultSetHandler<WebMerchantBean> beanWebHandler;
	private ResultSetHandler<MerchantBean> beanHandler;
	private ResultSetHandler<List<WebMerchantBean>> beanWebListHandler;
	private ResultSetHandler<List<MerchantBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	
	public MerchantMappingDB(DataSource dataSource){
		this.dataSource = dataSource;
		this.queryRunner = new QueryRunner(dataSource);
		this.beanHandler = new BeanHandler<MerchantBean>(MerchantBean.class);
		this.beanWebHandler = new BeanHandler<WebMerchantBean>(WebMerchantBean.class);
		this.numRecordHandler = new NumRecordResultSetHandler();
		this.beanListHandler = new BeanListHandler<MerchantBean>(MerchantBean.class);
		this.beanWebListHandler = new BeanListHandler<WebMerchantBean>(WebMerchantBean.class);
	}
	
	private static final String GET_MAX_MERCHANT_ID = "SELECT MAX(merchant_id)+1  FROM merchant_mapping WHERE merchant_id >= ? AND merchant_id < ?"; 
	public int getMaxMerchantIdByRange(int startRange, int endRange) throws SQLException {
		return queryRunner.query(GET_MAX_MERCHANT_ID, numRecordHandler, startRange, endRange);
	}
	
	private static final String INSERT_NEW_MERCHANT_MAPPING = "INSERT INTO merchant_mapping( merchant_id, store, merchant_package, merchant_package_runtime"
			+ ",redirect_type_runtime,sale_name, slug, site_url, redirect_type, mobile_redirect_type, enable_report) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
	public int insertNewMerchantMapping(MerchantBean mBean) throws SQLException {
		return queryRunner.update(INSERT_NEW_MERCHANT_MAPPING, new Object[] {
				mBean.getId(),
				mBean.getName(),
				mBean.getPackageType(),
				mBean.getMerchant_package_runtime(),
				mBean.getRedirect_type_runtime(),
				mBean.getSale_name(),
				mBean.getSlug(),
				mBean.getSite_url(),
				mBean.getRedirect_type(),
				mBean.getMobile_redirect_type(),
				mBean.getEnable_report()
		});
	}
	
	private static final String FIND_MERCHANT_MONITOR = "SELECT * FROM merchant_mapping WHERE monitor = ?";
	public List<MerchantBean> getMerchantByMonitor(int monitor) throws SQLException {		
		return queryRunner.query(FIND_MERCHANT_MONITOR, beanListHandler, new Object[] { monitor });
	}
	
	private static final String GET_SLUG = "SELECT * FROM `merchant_mapping` WHERE slug = ?";
	public List<MerchantBean> getSlug(String name) throws SQLException {
		return queryRunner.query(GET_SLUG,beanListHandler, new Object[]{name});			
	}

	private static final String GET_MERCHANT_BY_OFFSET = "SELECT * FROM merchant_mapping LIMIT ?,?";
	public List<WebMerchantBean> getMerchantByLimit(int offset, int limit) throws SQLException{
		return queryRunner.query(GET_MERCHANT_BY_OFFSET, beanWebListHandler, offset, limit);
	}
	
}
