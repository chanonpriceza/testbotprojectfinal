package botbackend.db.web;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import botbackend.db.utils.NumRecordResultSetHandler;


public class ProductDataDB {		

	private ResultSetHandler<Integer> numRecordsHandler;
	private ResultSetHandler<List<Integer>> integerListHandler;

	private DataSource dataSource;	
	private QueryRunner queryRunner; 		
	
	public ProductDataDB(DataSource dataSource) {
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);	
		numRecordsHandler = new NumRecordResultSetHandler();
	}
	
	private final String COUNT_ACTIVE_PRODUCT_BY_MERCHANT = "SELECT count(*) from tbl_product_data where merchant_id = ? and status in (0, 2) ";
	public int countActiveProductByMerchantId(int merchantId) throws SQLException {		
		return (Integer)queryRunner.query(COUNT_ACTIVE_PRODUCT_BY_MERCHANT, numRecordsHandler, new Object[] { merchantId });
	}
	
	private final String COUNT_PRODUCT_BY_STATUS = "SELECT count(*) from tbl_product_data where status = ?";
	public int countProductByStatus(int status) throws SQLException {		
		return (Integer)queryRunner.query(COUNT_PRODUCT_BY_STATUS, numRecordsHandler, new Object[] { status });
	}

}
