package botbackend.db.web;

import java.sql.SQLException;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

import botbackend.bean.web.ScheduleBean;
public class ScheduleDB {

	private DataSource dataSource;
	private ResultSetHandler<ScheduleBean> beanHandler;
	QueryRunner queryRunner; 		
	
	public static final String QUERY_BY_NAME = "select * from tbl_schedule where name = ?";
	    
	public static final String UPDATE = "update tbl_schedule set " +
		" runTime = ?, fromDate = ?, toDate = ?, status = ? " +
		" where name = ?";
	
	public static final String UPDATE_STATUS = "update tbl_schedule set status = ? where name = ?";
	
	public ScheduleDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		this.beanHandler = new BeanHandler(ScheduleBean.class);	
	}

	public ScheduleBean query(String name) throws SQLException {
		return queryRunner.query(QUERY_BY_NAME, beanHandler, new Object[] {name});
	}
	
}
