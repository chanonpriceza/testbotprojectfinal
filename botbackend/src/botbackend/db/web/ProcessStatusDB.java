package botbackend.db.web;

import java.sql.*;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;

import botbackend.db.utils.NumRecordResultSetHandler;


public class ProcessStatusDB {
	private NumRecordResultSetHandler numRecordsHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
		
	public ProcessStatusDB(DataSource dataSource) {
		this.dataSource = dataSource;
		this.queryRunner = new QueryRunner(dataSource);
		this.numRecordsHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_STATUS = "SELECT status FROM tbl_process_status";    
    	
	public int getStatus() throws SQLException {
		return (int) queryRunner.query(GET_STATUS, numRecordsHandler);
	}
	
}
