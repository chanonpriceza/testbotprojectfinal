package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.UrlPatternBean;
import botbackend.db.utils.DatabaseUtil;

public class LnwShopPatternDB extends UrlPatternBean {
	private ResultSetHandler<List<UrlPatternBean>> beanListHandler;
	QueryRunner queryRunner;
	
	public LnwShopPatternDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<UrlPatternBean>(UrlPatternBean.class);
	}
	
	private static final String getLnwshopPatternByMerchantId = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? ORDER BY field (action, 'ALLOW', 'BLOCK', 'LNWSHOP'), id"; 
	public List<UrlPatternBean> getLnwshopPatternByMerchantId(int id) throws SQLException {
		return queryRunner.query(getLnwshopPatternByMerchantId, beanListHandler, id);
	}
	
	private static final String deleteLnwshopPatternByMerchantId = "DELETE FROM tbl_url_pattern WHERE merchantId = ?";
	public int deleteLnwshopPatternByMerchantId(int id) throws SQLException {
		return queryRunner.update(deleteLnwshopPatternByMerchantId, id);
	}
	
	private static final String insertLnwshopPatternBatch = "INSERT INTO `tbl_url_pattern` (`merchantId`, `condition`, `group`, `value`, `action`, `categoryId`, `keyword`) VALUES ---filter---";
	public int insertLnwshopPatternBarch(List<UrlPatternBean> urlPatternList, int merchantId) throws SQLException {
		StringBuilder countObject = new StringBuilder();	
		List<Object> params = new ArrayList<>();
		
		for(UrlPatternBean upBean : urlPatternList){
			countObject.append(",(?, ?, ?, ?, ?, ?, ?)");
			params.add(merchantId);
			params.add(upBean.getCondition());
			params.add(upBean.getGroup());
			params.add(upBean.getValue());
			params.add(upBean.getAction());
			params.add(upBean.getCategoryId());
			params.add(upBean.getKeyword());
		}
		
		String filter = countObject.substring(1).toString();
		String finalSql = DatabaseUtil.replaceSQL(insertLnwshopPatternBatch, filter);
		return queryRunner.update(finalSql, params.toArray());
	}	
	
}
