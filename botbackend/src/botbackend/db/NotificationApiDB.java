package botbackend.db;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.MerchantBean;
import botbackend.bean.NotificationApiBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;

public class NotificationApiDB {
	
	private ResultSetHandler<Integer> numRecordHandler;
	private BeanListHandler<NotificationApiBean> beanListHandler;
	QueryRunner queryRunner;
	
	public NotificationApiDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		this.numRecordHandler = new NumRecordResultSetHandler();
		this.beanListHandler = new BeanListHandler<NotificationApiBean>(NotificationApiBean.class);
	}
	
	private final String INSERT_NOTIFICATION = "INSERT INTO tbl_notification_api (merchantId,message,source,destination,destination_type,type,isRead,link,country,createDate) VALUES (?,?,?,?,?,?,?,?,?,?) ";
	public int insertNotification(NotificationApiBean noti) throws Exception {
		return queryRunner.update(INSERT_NOTIFICATION, new Object[] {
					noti.getMerchantId() ,
					noti.getMessage(), 
					noti.getSource(),
					noti.getDestination(),
					noti.getDestinationType(),
					noti.getType(),
					noti.getIsRead(),
					noti.getLink(),
					noti.getCountry(),
					noti.getCreateDate(),
				});
	}
	
	private final String SELECT_NOTI_OFFSET = "select * from tbl_notification_api where destination = ? and isRead = ? limit ?";
	public List<NotificationApiBean>  selectNoti(String des,int read,int limit ) throws Exception {
		return queryRunner.query(SELECT_NOTI_OFFSET, beanListHandler,des,read,limit);
	}
	
	private final String COUNT_READ = "select count(id) from tbl_notification_api where destination = ? and isRead = ?";
	public int  countRead(String des,int read) throws Exception {
		return queryRunner.query(COUNT_READ, numRecordHandler,des,read);
	}
	
	private final String COUNT_DETAIL_WITH_TYPE = "select count(id) from tbl_notification_api where destination = ? and country = ? and type = ?";
	private final String COUNT_DETAIL = "select count(id) from tbl_notification_api where destination = ? and country = ?";
	public int  countDetail(String des,String country,int type) throws Exception {
		if(type==0) {
			return queryRunner.query(COUNT_DETAIL, numRecordHandler,des,country);
		}
		return queryRunner.query(COUNT_DETAIL_WITH_TYPE, numRecordHandler,des,country,type);
	}
	
	private final String SELECT_DETAIL_WITH_TYPE = "select * from tbl_notification_api where destination = ? and country = ? and type = ?   order by createDate desc limit ?,?";
	private final String SELECT_DETAIL			 = "select * from tbl_notification_api where destination = ? and country = ? order by createDate desc limit ?,?";
	public List<NotificationApiBean>  selectDetail(String des,String country,int type,int start,int limit) throws Exception {
		if(type==0) {
			return queryRunner.query(SELECT_DETAIL, beanListHandler,des,country,start,limit);
		}
		return queryRunner.query(SELECT_DETAIL_WITH_TYPE, beanListHandler,des,country,type,start,limit);
	}
	
	private final String SELECT_NOTI_ALL_LIMIT = "select * from tbl_notification_api where destination = ? order by isRead asc,createDate desc  limit ? ";
	public List<NotificationApiBean>  selectNotiAll(String des,int limit ) throws Exception {
		return queryRunner.query(SELECT_NOTI_ALL_LIMIT, beanListHandler,des,limit);
	}
	
	private final String SELECT_NOTI_ALL_OFFSET = "select * from tbl_notification_api where destination = ? order by isRead asc,createDate desc  limit ?,? ";
	public List<NotificationApiBean>  selectNotiAllOffset(String des,int offset,int limit) throws Exception {
		return queryRunner.query(SELECT_NOTI_ALL_OFFSET, beanListHandler,des,offset,limit);
	}
	
	private final String MARK_NOTI_READ = "update tbl_notification_api set isRead = ? where id in ---filter---";
	public int  markNotiList(List<String> idList,boolean isRead) throws Exception {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		params.add(isRead);
		if(idList != null && idList.size() > 0){
			String merchantStr = "";
			filters.append(" ( ");
			
			for(String id : idList){
				merchantStr += ",?";
				params.add(id);
			}
			merchantStr  = merchantStr.substring(1, merchantStr.length());
			filters.append(merchantStr + " ) ");

		}
		return queryRunner.update(DatabaseUtil.replaceSQL(MARK_NOTI_READ, filters.toString()), params.toArray());
	}
}
