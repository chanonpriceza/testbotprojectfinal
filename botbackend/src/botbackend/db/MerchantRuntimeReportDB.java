package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;


import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;
import botbackend.db.utils.NumRecordResultSetHandler;

public class MerchantRuntimeReportDB {
	private ResultSetHandler<MerchantRuntimeReportBean> beanHandler;
	private ResultSetHandler<List<MerchantRuntimeReportBean>> beanListHandler;
	private ResultSetHandler<List<MerchantBean>> merchantBeanListHandler;
	private DataSource dataSource;
	private ResultSetHandler<Integer> numRecordHandler;
	private ResultSetHandler<List<Integer>> integerListHandler;
	QueryRunner queryRunner;
	
	public MerchantRuntimeReportDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MerchantRuntimeReportBean>(MerchantRuntimeReportBean.class);
		beanListHandler = new BeanListHandler<MerchantRuntimeReportBean>(MerchantRuntimeReportBean.class);
		merchantBeanListHandler = new BeanListHandler<MerchantBean>(MerchantBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
		integerListHandler = new IntegerListValueResultSetHandler();
	}
	
	private static final String GET_REPORT_LIST = "SELECT * FROM  tbl_report ORDER BY endDate DESC  LIMIT ?, ? "; 
	public List<MerchantRuntimeReportBean> getReporList(int start, int offset) throws SQLException {
		return queryRunner.query(GET_REPORT_LIST, beanListHandler, start, offset);
	}
	private static final String GET_REPORT_LIST_BY_STATUS = "SELECT * FROM  tbl_report WHERE status = ? ORDER BY endDate DESC "; 
	public List<MerchantRuntimeReportBean> getReporListByStatus(String status) throws SQLException {
		return queryRunner.query(GET_REPORT_LIST_BY_STATUS, beanListHandler, status);
	}
	private static final String GET_REPORT_LIST_BY_STATUS_GROUP_BY_MERCHANTID = "SELECT fullRecord.* FROM tbl_report as fullRecord INNER JOIN ( SELECT max(id) AS sid FROM tbl_report WHERE status IN(?) AND type = ? GROUP BY merchantId ) AS eachLastRecord ON fullRecord.id = eachLastRecord.sid"; 
	public List<MerchantRuntimeReportBean> getReporListByStatusGroupByMerchantId(String status[],String type) throws SQLException {
		return queryRunner.query(GET_REPORT_LIST_BY_STATUS_GROUP_BY_MERCHANTID, beanListHandler,status[0],type);
	}
	
	private static final String GET_REPORT_BY_STATUS_AND_TYPE = "SELECT * FROM tbl_report where id = (SELECT max(id) from tbl_report where type = ? and status NOT IN (?,?) and merchantId = ?)";
	public MerchantRuntimeReportBean getReportByStatusAndType(String status[],String type,int merchantId) throws SQLException {
		return queryRunner.query(GET_REPORT_BY_STATUS_AND_TYPE,beanHandler,type,status[0],status[1],merchantId);
	}
	
	private static final String COUNT_ALL_REPORT = "SELECT COUNT(id) FROM tbl_report"; 
	public int countAllReport() throws SQLException {
		return queryRunner.query(COUNT_ALL_REPORT, numRecordHandler);
	}
	
	private static final String GET_REPORT_LIST_BY_SEARCH = "SELECT r.*,r.package as packageType,m.server FROM tbl_report r LEFT JOIN tbl_server_report m ON r.serverReportRef = m.id WHERE ---filter---  ORDER BY id DESC LIMIT ?, ? "; 
	public List<MerchantRuntimeReportBean> getReportListBySearch(String type, int searchType, String searchParam, String status,String searchMerchantRef,String time,int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isBlank(type)) {
			type = "ALL";
		}
		
		if(!("ALL").equals(type)){ 
			
			filters.append(" r.type = ?"); params.add(type); 
		}
		
		if(("ALL").equals(type)) {
			type = "%";
			filters.append("r.type Like ?"); params.add(type);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				filters.append(" AND r.merchantId = ?"); params.add(searchParam);				
			}else{
				filters.append(" AND r.name LIKE ?"); params.add("%"+searchParam+"%");				
			}
		}
		
		if(StringUtils.isNotBlank(status)){
			filters.append(" AND r.status = ?"); 
			params.add(status);
			
		}
		
		
		if(StringUtils.isNotBlank(searchMerchantRef)){
			filters.append("r.serverReportRef = ?"); 
			params.add(searchMerchantRef);
		}
		String timeFilter = "";
		if(StringUtils.isNotBlank(time)) {
			if("1".equals(time)) {
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) <= 24";
			}else if("2".equals(time)) {
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) BETWEEN 24 AND 72 ";
			}else if("3".equals(time)){
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) >= 72";
			}
		}
		
		if(StringUtils.isNotBlank(timeFilter)) {
			filters.append(timeFilter); 
		}
		params.add(start);
		params.add(offset);
		
		return (List<MerchantRuntimeReportBean>)queryRunner.query(DatabaseUtil.replaceSQL(GET_REPORT_LIST_BY_SEARCH, filters.toString()), beanListHandler, params.toArray());
	}
	
	private static final String COUNT_REPORT_LIST_BY_SEARCH = "SELECT COUNT(r.id) FROM  tbl_report r LEFT JOIN tbl_server_report m ON r.serverReportRef = m.id WHERE ---filter---   "; 
	public int countReportBySearch(String type, int searchType, String searchParam, String status,String time,String searchMerchantRef) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isBlank(type)) {
			type = "ALL";
		}
		
		if(!("ALL").equals(type)){ 
			
			filters.append(" r.type = ?"); params.add(type); 
		}
		
		if(("ALL").equals(type)) {
			type = "%";
			filters.append("r.type Like ?"); params.add(type);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				filters.append(" AND r.merchantId = ?"); params.add(searchParam);					
			}else{
				filters.append(" AND r.name LIKE ?"); params.add("%"+searchParam+"%");				
			}
		}
		
		if(StringUtils.isNotBlank(status)){
			filters.append(" AND r.status = ?"); 
			params.add(status);
			
		}
		
		if(StringUtils.isNotBlank(searchMerchantRef)){
			filters.append(" r.serverReportRef = ?"); 
			params.add(searchMerchantRef);
		}
		
		String timeFilter = "";
		if(StringUtils.isNotBlank(time)) {
			if("1".equals(time)) {
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) <= 24";
			}else if("2".equals(time)) {
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) BETWEEN 24 AND 72 ";
			}else if("3".equals(time)){
				timeFilter = " AND TIMESTAMPDIFF(HOUR,r.startDate,r.endDate) >= 72";
			}
		}
		
		if(StringUtils.isNotBlank(timeFilter)) {
			filters.append(timeFilter); 
		}

		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_REPORT_LIST_BY_SEARCH, filters.toString()), numRecordHandler, params.toArray());
	}
	
	private static final String GET_MERCHANT_LIST_BY_STATUS_AND_INTERVAL = "SELECT r.merchantId as id , s.server FROM tbl_report r LEFT JOIN tbl_server_report s ON r.serverReportRef = s.id WHERE r.type = ('DUE' OR 'DUE_TEST') AND r.status = ? AND r.startDate  >  date_sub(CURRENT_DATE(), INTERVAL ? month) ---filter--- GROUP BY r.merchantId"; 
	public List<MerchantBean> getMerchantListByStatusAndInterval(List<MerchantBean> merchants,String status, int interval) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		params.add(status);
		params.add(interval);
		if(merchants != null && merchants.size() > 0){
			String merchantStr = "";
			filters.append(" AND (merchantId, server) NOT IN ( ");
			
			for(MerchantBean merchant : merchants){
				merchantStr += ",(?, ?)";
				params.add(merchant.getId());
				params.add(merchant.getServer());
			}
			merchantStr  = merchantStr.substring(1, merchantStr.length());
			filters.append(merchantStr + " ) ");

		}
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_MERCHANT_LIST_BY_STATUS_AND_INTERVAL, filters.toString()), merchantBeanListHandler, params.toArray());
	}
	
	private static final String GET_REPORT_BY_INTERVAL = "SELECT r.*,s.server FROM tbl_report r LEFT JOIN tbl_server_report s ON r.serverReportRef = s.id WHERE r.type = ('DUE' OR 'DUE_TEST')  AND r.startDate  >  date_sub(CURRENT_DATE(), INTERVAL ? month) AND r.merchantId IN (---filter---) ORDER BY  r.startDate DESC"; 
	public  List<MerchantRuntimeReportBean> getReportByInterval(List<MerchantBean> merchants, int interval) throws SQLException {
		if(merchants != null && merchants.size() > 0){
			StringBuilder filters = new StringBuilder();	
			List<Object> params = new ArrayList<Object>();
			params.add(interval);
			for(MerchantBean merchant : merchants){
				filters.append(",?");
				params.add(merchant.getId());
			}
			filters = filters.deleteCharAt(0);
			return (List<MerchantRuntimeReportBean>)queryRunner.query(DatabaseUtil.replaceSQL(GET_REPORT_BY_INTERVAL, filters.toString()), beanListHandler, params.toArray());
		}
		return null;
		
	}
	
	public static final String FIND_REPORT_BY_MERCHANTID = "SELECT * from tbl_report WHERE merchantId = ? AND type = ? ORDER BY id DESC LIMIT ?";
	public MerchantRuntimeReportBean findReportByMerchantId(int merchantId, String processType,int limit) throws SQLException {
		return queryRunner.query(FIND_REPORT_BY_MERCHANTID, beanHandler, merchantId, processType, limit);
	}
	
	private static final String GET_REPORT_LIST_BY_MERCHANT_MAXDUE_AND_WCE = "SELECT r.*,r.package as packageType,m.server FROM tbl_report r LEFT JOIN tbl_server_report m ON r.serverReportRef = m.id  WHERE r.type = ? AND r.merchantId = ? ORDER BY id DESC LIMIT 1"; 
	public MerchantRuntimeReportBean getReportListByMerchant(int merchantId ,String type) throws SQLException {
		return queryRunner.query(GET_REPORT_LIST_BY_MERCHANT_MAXDUE_AND_WCE, beanHandler, type,merchantId);
	}
}

