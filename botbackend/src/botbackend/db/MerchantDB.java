
package botbackend.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;

import utils.DateTimeUtil;
import utils.FilterUtil;

import botbackend.bean.MerchantBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.utils.Util;

public class MerchantDB extends MerchantBean {
	private ResultSetHandler<MerchantBean> beanHandler;
	private ResultSetHandler<List<MerchantBean>> beanListHandler;
	private IntegerListValueResultSetHandler integerListHandler;
	private DataSource dataSource;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	public MerchantDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<MerchantBean>(MerchantBean.class);
		beanListHandler = new BeanListHandler<MerchantBean>(MerchantBean.class);
		integerListHandler = new IntegerListValueResultSetHandler();
		this.numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private static final String GET_ALL_MERCHANT = "SELECT * FROM tbl_merchant ORDER BY id";
	public List<MerchantBean> getAllMerchant() throws SQLException {
		return queryRunner.query(GET_ALL_MERCHANT, beanListHandler);
	}
	
	private static final String GET_ALL_UNIQUE_MERCHANT = "SELECT * FROM tbl_merchant ORDER BY id";
	public List<MerchantBean> getAllUniqueMerchant() throws SQLException {
		return queryRunner.query(GET_ALL_UNIQUE_MERCHANT, beanListHandler);
	}
	
	private static final String GET_ALL_MERCHANT_EXCEPT_RANGE = "SELECT * FROM tbl_merchant WHERE id < ? OR id > ? ORDER BY id";
	public List<MerchantBean> getAllMerchantExceptRange(int startRange, int endRange) throws SQLException {
		return queryRunner.query(GET_ALL_MERCHANT_EXCEPT_RANGE, beanListHandler, new Object[]{startRange, endRange});
	}
	
	private static final String GET_MERCHANT_LIST = "SELECT id, name FROM tbl_merchant"; 
	public List<MerchantBean> getMerchantList() throws SQLException {
		return queryRunner.query(GET_MERCHANT_LIST, beanListHandler);
	}
	
	private static final String GET_MERCHANT_IDS_BY_STATUS = "SELECT id FROM tbl_merchant WHERE active= ? "; 
	public List<Integer> getMerchantIdsByStatus(int status) throws SQLException {
		return queryRunner.query(GET_MERCHANT_IDS_BY_STATUS, integerListHandler, status);
	}
	private static final String GET_MERCHANT_IDS_AND_SEVER_BY_STATUS = "SELECT id , server  FROM tbl_merchant WHERE active= ? "; 
	public List<MerchantBean> getMerchantIdsAndServerByStatus(int status) throws SQLException {
		return queryRunner.query(GET_MERCHANT_IDS_AND_SEVER_BY_STATUS, beanListHandler, status);
	}
	
	private static final String GET_SEARCH_MERCHANT = "SELECT mch.*, usr.userName as ownerName , bqr.allCount as countWebProduct FROM tbl_merchant mch LEFT JOIN tbl_backend_user usr on mch.ownerId = usr.userId LEFT JOIN tbl_bot_quality_report bqr on mch.id = bqr.merchantId AND mch.server = bqr.server ---filter--- LIMIT ?, ?"; 
	public List<MerchantBean> getSearchMerchant(String searchParam, String activeType, String inputServer, String inputDataServer, String userType, String packageType, String userUpdate, int start, int offset) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		String firstPackage = null;
		String lastPackage = null;
		
		if(packageType.contains(",")){
			List<String> parts = FilterUtil.getAllStringBetween(packageType, "'", "'");
			firstPackage = parts.get(0);
			lastPackage = parts.get(1);		
		}
		
		if(activeType.equals("-1")){	
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active > -1 ");
		}else if(Util.isContainBracket(activeType)){
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active IN "+activeType); 
		}else if(StringUtils.isNotBlank(activeType)){
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active =  ? "); 
			params.add(activeType);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("mch.id = ?"); 
				params.add(searchParam);					
			}else{
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("mch.name LIKE ?"); 
				params.add("%"+searchParam+"%");					
			}
		}
		
		if(Util.isContainBracket(userType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userName IN "+ userType); 
		}else if(StringUtils.isNotBlank(userType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userName = ?");  		
			params.add(userType);
		}
	
		if(Util.isContainBracket(packageType)){
			if(StringUtils.isNotBlank(firstPackage) && StringUtils.isNumeric(firstPackage) && StringUtils.isNotBlank(lastPackage) && StringUtils.isNumeric(lastPackage)) {
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("packageType BETWEEN "+ firstPackage +" AND "+lastPackage);
			}			
		}else if(StringUtils.isNotBlank(packageType) && StringUtils.isNumeric(packageType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("packageType = ?"); 	params.add(packageType);
		}
		
		if(Util.isContainBracket(inputServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("mch.server IN "+ inputServer); 
		}else if(StringUtils.isNotBlank(inputServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("mch.server = ?"); 			
			params.add(inputServer);
		}	
		
		if(Util.isContainBracket(inputDataServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("dataServer IN "+ inputDataServer); 
		}else if(StringUtils.isNotBlank(inputDataServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("dataServer = ?"); 		
			params.add(inputDataServer);
		}
		
		if(Util.isContainBracket(userUpdate)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userUpdate IN "+ userUpdate); 
		}else if(StringUtils.isNotBlank(userUpdate)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userUpdate = ?"); 		
			params.add(userUpdate);
		}

		if(filters.length() > 0) filters.insert(0, "WHERE ");
		
		
		params.add(start);
		params.add(offset);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_SEARCH_MERCHANT, filters.toString()), beanListHandler, params.toArray());
	}
	
//	slow query
//	private static final String COUNT_SEARCH_MERCHANT = "SELECT COUNT(mch.id) FROM tbl_merchant mch LEFT JOIN tbl_backend_user usr on mch.ownerId = usr.userId LEFT JOIN tbl_bot_quality_report bqr on mch.id = bqr.merchantId AND mch.server = bqr.server WHERE ---filter--- ";
	private static final String COUNT_SEARCH_MERCHANT = "SELECT COUNT(mch.id) FROM tbl_merchant mch LEFT JOIN tbl_backend_user usr on mch.ownerId = usr.userId ---filter--- ";
	public int countSearchMerchant(String searchParam, String activeType, String inputServer, String inputDataServer, String userType, String packageType, String userUpdate) throws SQLException {
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		String firstPackage = null;
		String lastPackage = null;
		
		if(packageType.contains(",")){
			List<String> parts = FilterUtil.getAllStringBetween(packageType, "'", "'");
			firstPackage = parts.get(0);
			lastPackage = parts.get(1);		
		}
		
		if(activeType.equals("-1")){	
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active > -1 ");
		}else if(Util.isContainBracket(activeType)){
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active IN "+activeType); 
		}else if(StringUtils.isNotBlank(activeType)){
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("active =  ? "); 
			params.add(activeType);
		}
		
		if(StringUtils.isNotBlank(searchParam)){
			if(StringUtils.isNumeric(searchParam)){
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("mch.id = ?"); 
				params.add(searchParam);					
			}else{
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("mch.name LIKE ?"); 
				params.add("%"+searchParam+"%");					
			}
		}
		
		if(Util.isContainBracket(userType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userName IN "+ userType); 
		}else if(StringUtils.isNotBlank(userType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userName = ?");  		
			params.add(userType);
		}
	
		if(Util.isContainBracket(packageType)){
			if(StringUtils.isNotBlank(firstPackage) && StringUtils.isNumeric(firstPackage) && StringUtils.isNotBlank(lastPackage) && StringUtils.isNumeric(lastPackage)) {
				if(filters.length() > 0) filters.append(" AND ");
				filters.append("packageType BETWEEN "+ firstPackage +" AND "+lastPackage);
			}			
		}else if(StringUtils.isNotBlank(packageType) && StringUtils.isNumeric(packageType)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("packageType = ?"); 	params.add(packageType);
		}
		
		if(Util.isContainBracket(inputServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("mch.server IN "+ inputServer); 
		}else if(StringUtils.isNotBlank(inputServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("mch.server = ?"); 			
			params.add(inputServer);
		}	
		
		if(Util.isContainBracket(inputDataServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("dataServer IN "+ inputDataServer); 
		}else if(StringUtils.isNotBlank(inputDataServer)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("dataServer = ?"); 		
			params.add(inputDataServer);
		}
		
		if(Util.isContainBracket(userUpdate)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userUpdate IN "+ userUpdate); 
		}else if(StringUtils.isNotBlank(userUpdate)) {
			if(filters.length() > 0) filters.append(" AND ");
			filters.append("userUpdate = ?"); 		
			params.add(userUpdate);
		}
		
		if(filters.length() > 0) {
			filters.insert(0, "WHERE ");
		}

		return queryRunner.query(DatabaseUtil.replaceSQL(COUNT_SEARCH_MERCHANT, filters.toString()), numRecordHandler, params.toArray());
	}
	
	private static final String GET_MERCHANT_BY_ID_WITH_OWNERNAME = "SELECT mch.*, usr.userName as ownerName FROM tbl_merchant mch LEFT JOIN tbl_backend_user usr on mch.ownerId = usr.userId WHERE mch.id = ?";  
	public List<MerchantBean> getMerchantListByIdWithOwner(int merchantId) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_ID_WITH_OWNERNAME, beanListHandler, merchantId);
	}
	public MerchantBean getMerchantBeanByIdWithOwner(int merchantId) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_ID_WITH_OWNERNAME, beanHandler, merchantId);
	}
	
	private static final String GET_MERCHANT_BY_ID_AND_SERVER_WITH_OWNERNAME = "SELECT mch.*, usr.userName as ownerName FROM tbl_merchant mch LEFT JOIN tbl_backend_user usr on mch.ownerId = usr.userId WHERE mch.id = ? AND mch.server = ?";  
	public MerchantBean getMerchantByIdAndServerWithOwner(int merchantId, String server) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_ID_AND_SERVER_WITH_OWNERNAME, beanHandler, merchantId, server);
	}
	
	private static final String GET_MERCHANT_BY_MERCHANTID = "SELECT * FROM tbl_merchant WHERE id = ?"; 
	public MerchantBean getMerchantByMerchantId(int merchantId) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_MERCHANTID, beanHandler, merchantId);
	}
	
	private static final String GET_MERCHANTs_BY_MERCHANTID = "SELECT * FROM tbl_merchant WHERE id = ?"; 
	public List<MerchantBean> getMerchantsByMerchantId(int merchantId) throws SQLException {
		return queryRunner.query(GET_MERCHANTs_BY_MERCHANTID, beanListHandler, merchantId);
	}
	
	private static final String GET_MERCHANT_BY_MERCHANTID_AND_SERVER = "SELECT * FROM tbl_merchant WHERE id = ? AND server = ?"; 
	public MerchantBean getMerchantByMerchantIdAndServer(int merchantId,String server) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_MERCHANTID_AND_SERVER, beanHandler, merchantId,server);
	}
	
	private static final String GET_ALL_MERCHANT_LIST = "SELECT * FROM tbl_merchant  LIMIT ?, ?"; 
	public List<MerchantBean> getAllMerchantList(int start, int offset) throws SQLException {
		return queryRunner.query(GET_ALL_MERCHANT_LIST, beanListHandler, start, offset);
	}
	
	private static final String COUNT_ALL_MERCHANT = "SELECT COUNT(id) FROM tbl_merchant"; 
	public int countAllMerchant() throws SQLException {
		return queryRunner.query(COUNT_ALL_MERCHANT, numRecordHandler);
	}
	
	private static final String INSERT_NEW_MERCHANT = "INSERT INTO tbl_merchant (id, name, active, packageType, state, status, dataUpdateState, dataUpdateStatus, dueErrorCount, wceErrorCount, wceNextStart, dueNextStart, server, nodeTest, ownerId, note, noteDate) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public int insertNewMerchant(MerchantBean merchantBean) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(INSERT_NEW_MERCHANT, new String[]{"id"});
			queryRunner.fillStatement(pstmt, new Object[] {	
					merchantBean.getId(),
					merchantBean.getName(),
					merchantBean.getActive(),
					merchantBean.getPackageType(),
					merchantBean.getState(),
					merchantBean.getStatus(),
					merchantBean.getDataUpdateState(),
					merchantBean.getDataUpdateStatus(),
					merchantBean.getDueErrorCount(),
					merchantBean.getWceErrorCount(),
					(merchantBean.getWceNextStart() != null)? DateTimeUtil.generateStringDateTime(merchantBean.getWceNextStart()):null,
					(merchantBean.getWceNextStart() != null)? DateTimeUtil.generateStringDateTime(merchantBean.getDueNextStart()):null,
					merchantBean.getServer(),
					merchantBean.getNodeTest(),
					merchantBean.getOwnerId(),
					merchantBean.getNote(),
					merchantBean.getNoteDate()
			}); 		
			
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			if(rs.next()) {
				return rs.getInt(1);
			}
	
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DatabaseUtil.closeStatement(pstmt);				
			DatabaseUtil.closeConnection(conn);			
		}		
		return -1;		
	}	
	
	private static final String UPDATE_NEW_MERCHANT = "UPDATE tbl_merchant SET name=?, active=?, packageType=?, server=?, dataServer=?, state=?, status=?, dataUpdateState=?, dataUpdateStatus=?, errorMessage=?, ownerId=?, coreUserConfig=?, note=?, noteDate=? WHERE id = ? AND server = ?";
	public int updateNewMerchant(MerchantBean merchantBean) throws SQLException {
		return queryRunner.update(UPDATE_NEW_MERCHANT, new Object[] {
				merchantBean.getName(),
				merchantBean.getActive(),
				merchantBean.getPackageType(),
				(StringUtils.isBlank(merchantBean.getServer()))?"":merchantBean.getServer(),
				merchantBean.getDataServer(),
				merchantBean.getState(),
				merchantBean.getStatus(),
				merchantBean.getDataUpdateState(),
				merchantBean.getDataUpdateStatus(),
				merchantBean.getErrorMessage(),
				merchantBean.getOwnerId(),
				merchantBean.getCoreUserConfig(),
				merchantBean.getNote(),
				merchantBean.getNoteDate(),
				merchantBean.getId(),
				(StringUtils.isBlank(merchantBean.getCurrentserver()))?"":merchantBean.getCurrentserver()
		});
	}	

	private static final String UPDATE_NEW_MERCHANTS = "UPDATE tbl_merchant SET  name=?, active = ?, packageType = ?,server=?,dataServer=?, state= ?, status=?, dataUpdateState= ?, dataUpdateStatus= ?, errorMessage= ?,  userUpdate = ?, lastUpdate = ? WHERE id = ? AND server = ?";
	public int[] updateNewMerchants(Object[][] param) throws SQLException {
		return queryRunner.batch(UPDATE_NEW_MERCHANTS, param );
	}
	
	private static final String UPDATE_MERCHANT_NODE = "UPDATE tbl_merchant SET  nodeTest= ? , active= ?,state= ?, status=?, dataUpdateState = ?, dataUpdateStatus= ? WHERE id = ? AND server = ?";
	public int updateMerchantNode(int node, int active, int merchantId,String state, String dataUpdateState, String status,String server) throws SQLException {
		return queryRunner.update(UPDATE_MERCHANT_NODE, new Object[] {node ,active,state, status, dataUpdateState, status, merchantId,server });
	}	
	
	private static final String UPDATE_MERCHANT_NEXTSTART_WCE = "UPDATE tbl_merchant SET  active = ?, wceNextStart= ?,state= ?, status= ? WHERE id = ? ";
	public int updateMerchantNextStartWCE( int merchantId,int active, String state, String status) throws SQLException {
		return queryRunner.update(UPDATE_MERCHANT_NEXTSTART_WCE, active,DateTimeUtil.generateStringDateTime(new Date()),state,status,merchantId );
	}	
	
	private static final String UPDATE_MERCHANT_NEXTSTART_DUE = "UPDATE tbl_merchant SET  active = ?, dueNextStart= ?,dataUpdateState = ?, dataUpdateStatus= ? WHERE id = ? ";
	public int updateMerchantNextStartDUE( int merchantId,int active, String dataUpdateState, String dataUpdateStatus) throws SQLException {
		return queryRunner.update(UPDATE_MERCHANT_NEXTSTART_DUE, active,DateTimeUtil.generateStringDateTime(new Date()),dataUpdateState,dataUpdateStatus, merchantId );
	}	
	
	private static final String UPDATE_MERCHANT_BY_RECOVER = "UPDATE tbl_merchant SET  nodeTest= ? , active= ?,state= ?, status=?, dataUpdateState = ?, dataUpdateStatus= ?, errorMessage= CONCAT(errorMessage,' ', ?)  WHERE id = ? AND server = ?";
	public int updateMerchantByRecover(int node, int active, int merchantId,String state, String dataUpdateState, String status,String server, String errorMessage) throws SQLException {
		return queryRunner.update(UPDATE_MERCHANT_BY_RECOVER, new Object[] {node ,active,state, status, dataUpdateState, status, errorMessage,  merchantId,server });
	}	
	
	private static final String GET_MERCHANT_LIST_BY_IDS = "SELECT * FROM tbl_merchant ---filter---"; 
	public  List<MerchantBean> getMerchantListByIds(List<MerchantBean> merchantIds) throws SQLException {
		if(merchantIds != null && merchantIds.size() > 0){
			StringBuilder filters = new StringBuilder();	
			List<Object> params = new ArrayList<Object>();
			if(merchantIds != null && merchantIds.size() > 0){
				String merchantStr = "";
				filters.append("  WHERE  (id,server) IN ( ");
				for(MerchantBean merchant : merchantIds){
					merchantStr += ",( ?, ?)";
					params.add(merchant.getId());
					params.add(merchant.getServer());
				}
				merchantStr  = merchantStr.substring(1, merchantStr.length());
				filters.append(merchantStr + " ) ");

			}
			filters = filters.deleteCharAt(0);
			return (List<MerchantBean>)queryRunner.query(DatabaseUtil.replaceSQL(GET_MERCHANT_LIST_BY_IDS, filters.toString()), beanListHandler, params.toArray());
		}
		return null;
		
	}
	
	private static final String GET_MAX_MERCHANT_ID = "SELECT MAX(id)+1  FROM tbl_merchant WHERE id >= ? AND id < ?"; 
	public int getMaxMerchantIdByRange(int startRange, int endRange) throws SQLException {
		return queryRunner.query(GET_MAX_MERCHANT_ID, numRecordHandler, startRange, endRange);
	}
	private static final String EDIT_NOTE = "UPDATE tbl_merchant SET note = ?, noteDate = NOW() WHERE id = ? AND server = ?";
	public int editNote(String id, String value,String server) throws SQLException {
		return queryRunner.update(EDIT_NOTE, new Object[] {value, id,server});
		
	}
	
	private static final String GET_MERCHANT_BY_ACTIVE_STATUS = "SELECT * FROM tbl_merchant WHERE active = ? and server = ?";
	public List<MerchantBean> getMerchantByActiveAndServer(int active, String server) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_ACTIVE_STATUS, beanListHandler, active, server);
	}
	
	private static final String GET_MERCHANT_BY_ACTIVE = "SELECT * from tbl_merchant WHERE active = ?";
	public List<MerchantBean> getMerchantByActive(int active) throws SQLException {
		return queryRunner.query(GET_MERCHANT_BY_ACTIVE, beanListHandler, new Object[]{active});			
	}
	
	private static final String SET_MERCHANT_RECENT_UPDATE = "UPDATE tbl_merchant SET userUpdate = ? , lastUpdate = ? WHERE id = ? AND server = ?";
	public int setMerchantRecentUpdate(int merchantId, String server, String userName) throws SQLException {
		return queryRunner.update(SET_MERCHANT_RECENT_UPDATE, userName, DateTimeUtil.generateStringDateTime(new Date()), merchantId, server);
	}
	
	private static final String SET_RECENT_UPDATE = "UPDATE tbl_merchant SET userUpdate = ? , lastUpdate = ? WHERE id = ?";
	public int setRecentUpdate(int merchantId, String server, String userName) throws SQLException {
		if (StringUtils.isNotBlank(server)) {
			return queryRunner.update(SET_RECENT_UPDATE+" AND server = ?", userName, DateTimeUtil.generateStringDateTime(new Date()), merchantId, server);
		} else {
			return queryRunner.update(SET_RECENT_UPDATE, userName, DateTimeUtil.generateStringDateTime(new Date()), merchantId);
		}
	}
	
	private static final String GET_RANDOM_NODE_TEST = "SELECT nodeTest, count(nodeTest) AS count FROM tbl_merchant WHERE active = ? GROUP BY nodeTest ORDER BY count, nodeTest ASC";
	public List<MerchantBean> countNodeTestByActive(int active) throws SQLException {
		return queryRunner.query(GET_RANDOM_NODE_TEST, beanListHandler, new Object[]{active});			
	}

	private static final String INSERT_MERCHANT_BATCH = "INSERT INTO tbl_merchant (id, name, active, packageType, state, status, dataUpdateState, dataUpdateStatus, dueErrorCount, wceErrorCount, server, nodeTest, ownerId) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public int[] insertNewMerchantBatch(List<MerchantBean> merchantList) throws SQLException{
		if(merchantList == null || merchantList.size() == 0) return null;
		int count = 0;
		Object[][] obj = new Object[merchantList.size()][13];
		for (MerchantBean merchantBean : merchantList) {
			obj[count][0]  = merchantBean.getId();
			obj[count][1]  = merchantBean.getName();
			obj[count][2]  = merchantBean.getActive();
			obj[count][3]  = merchantBean.getPackageType();
			obj[count][4]  = merchantBean.getState();
			obj[count][5]  = merchantBean.getStatus();
			obj[count][6]  = merchantBean.getDataUpdateState();
			obj[count][7]  = merchantBean.getDataUpdateStatus();
			obj[count][8]  = merchantBean.getDueErrorCount();
			obj[count][9]  = merchantBean.getWceErrorCount();
			obj[count][10] = merchantBean.getServer();
			obj[count][11] = merchantBean.getNodeTest();
			obj[count][12] = merchantBean.getOwnerId();
			count++;
		}
		return queryRunner.batch(INSERT_MERCHANT_BATCH, obj);
	}
	
	private static final String UPDATE_PACKAGE_BATCH = "UPDATE tbl_merchant SET packageType = ? WHERE id = ?";
	public int[] updatePackageBatch(List<MerchantBean> merchantList) throws SQLException{
		if(merchantList == null || merchantList.size() == 0) return null;
		int count = 0;
		Object[][] obj = new Object[merchantList.size()][2];
		for (MerchantBean merchantBean : merchantList) {
			obj[count][0] = merchantBean.getPackageType();
			obj[count][1] = merchantBean.getId();
			count++;
		}
		return queryRunner.batch(UPDATE_PACKAGE_BATCH, obj);
	}
	
	private static final String UPDATE_COMMAND = "UPDATE tbl_merchant SET command = ? WHERE id = ?";
	public int updateCommand(int merchantId , String command ) throws SQLException{
		return queryRunner.update(UPDATE_COMMAND, command,merchantId);
	}
	
	private static final String REMOVE_COMMAND = "UPDATE tbl_merchant set command = REPLACE(command, ?, \"\") WHERE id = ? ";
	public int removeCommand(int merchantId , String command ) throws SQLException{
		return queryRunner.update(REMOVE_COMMAND, command,merchantId);
	}
	
	private static final String GET_ALLMERCHANT_TO_FILE = "SELECT m.id,m.name,m.active,m.packageType,m.errorMessage,m.note,m.lastUpdate,m.userUpdate,bu.userName AS ownerName FROM tbl_merchant m  LEFT JOIN tbl_backend_user bu ON m.ownerId = bu.userId "; 
	public List<MerchantBean> getAllMerchantToFile() throws SQLException {
		return queryRunner.query(GET_ALLMERCHANT_TO_FILE, beanListHandler);
	}
}
