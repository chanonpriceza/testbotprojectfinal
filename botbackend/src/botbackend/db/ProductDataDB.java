package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.ProductDataBean;
import bean.ProductDataBean.STATUS;
import utils.FilterUtil;

import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.db.utils.StringListValueResultSetHandler;

public class ProductDataDB {		

	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	public static final int KEYWORD_LENGTH = 500;
	
	private ResultSetHandler<List<ProductDataBean>> beanListHandler;
	private ResultSetHandler<ProductDataBean> beanHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private StringListValueResultSetHandler stringListHandler;

	QueryRunner queryRunner; 		
	
	public ProductDataDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ProductDataBean>(ProductDataBean.class);
		beanListHandler = new BeanListHandler<ProductDataBean>(ProductDataBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
		stringListHandler = new StringListValueResultSetHandler();
	}
	
	public static final String GET_SPECIFIC_PRODUCT = "SELECT * FROM tbl_product_data WHERE name = ? AND merchantId = ?";
	public ProductDataBean getSpecificProduct(String name, int merchantId) throws SQLException{
		return queryRunner.query(GET_SPECIFIC_PRODUCT, beanHandler, name, merchantId);
	}
	
	public static final String SELECT_BY_MERCHANTID_AND_LIMIT = "SELECT url FROM tbl_product_data WHERE merchantId = ? LIMIT ?";
	public List<String> findProductData(int merchantId, int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_AND_LIMIT, stringListHandler, new Object[] {merchantId, limit}); 				
	}
	
	public static final String GET_MERCHANT_DATA_BY_ID = "SELECT * FROM tbl_product_data WHERE merchantId = ? ORDER BY RAND() LIMIT ?";
	public List<ProductDataBean> getMerchantDataById(int merchantId, int limit) throws SQLException {		
		return queryRunner.query(GET_MERCHANT_DATA_BY_ID, beanListHandler, new Object[] {merchantId, limit}); 				
	}
	
	public static final String GET_MERCHANT_DATA_BY_AND_START_LIMIT = "SELECT * FROM tbl_product_data INNER JOIN (SELECT id FROM tbl_product_data WHERE merchantId = ? LIMIT ?,?) AS p ON p.id = tbl_product_data.id";
	public List<ProductDataBean> getMerchantDataByAndStartLimit(int merchantId, int start,int limit) throws SQLException {		
		return queryRunner.query(GET_MERCHANT_DATA_BY_AND_START_LIMIT, beanListHandler, new Object[] {merchantId,start,limit}); 				
	}
	
	private static final String COUNT_ALL_MONITOR = "SELECT COUNT(id) FROM tbl_product_data WHERE merchantId = ? AND status = ? "; 
	public int countProductByStatus(int merchantId, String status) throws SQLException {
		return queryRunner.query(COUNT_ALL_MONITOR, numRecordHandler, new Object[] {merchantId, status});
	}
	
	private static final String COUNT_SYNC_DATA_HAVE_LIMIT = "SELECT COUNT(id) FROM tbl_product_data WHERE merchantId = ? AND status = ? LIMIT ?"; 
	public int countProductByStatusHaveLimit(int merchantId, String status ,int Limit) throws SQLException {
		return queryRunner.query(COUNT_SYNC_DATA_HAVE_LIMIT, numRecordHandler, merchantId,status,Limit);
	}
	
	private static final String COUNT_PRODUCT = "SELECT COUNT(id) FROM tbl_product_data WHERE merchantId = ?"; 
	public int countProduct(int merchantId) throws SQLException {
		return queryRunner.query(COUNT_PRODUCT, numRecordHandler, new Object[] {merchantId});
	}
	
	public static final String INSERT = "INSERT IGNORE INTO tbl_product_data" +
			"(merchantId, name, price, url, urlForUpdate, description, pictureUrl, " +
			" status, updateDate, errorUpdateCount, categoryId, keyword, merchantUpdateDate, realProductId, basePrice, upc, dynamicField) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public int insertProductData(ProductDataBean productDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				productDataBean.getMerchantId(),
				FilterUtil.limitString(productDataBean.getName(), NAME_LENGTH),
				productDataBean.getPrice(),
				productDataBean.getUrl(),
				productDataBean.getUrlForUpdate(),
				FilterUtil.limitString(productDataBean.getDescription(), DESCRIPTION_LENGTH), 
				productDataBean.getPictureUrl(),
				STATUS.C.toString(),
				productDataBean.getUpdateDate(),
				productDataBean.getErrorUpdateCount(),
				productDataBean.getCategoryId(),
				productDataBean.getKeyword(),
				productDataBean.getMerchantUpdateDate(),
				productDataBean.getRealProductId(),
				productDataBean.getBasePrice(),
				productDataBean.getUpc(),
				productDataBean.getDynamicField()
		}); 		
	}
	
	public static final String GET_PRODUCT_DATA_BY_URL = "SELECT * FROM tbl_product_data WHERE merchantId = ? AND urlForUpdate IN (---filter---)";
	public List<ProductDataBean> getProductDataByURL(int merchantId, String[] urls) throws SQLException {	
		String filterStr = "";
		List<Object> params = new ArrayList<Object>();
		params.add(merchantId);
		for(String url : urls){
			filterStr += ",?";
			params.add(url);
		}
		filterStr = filterStr.substring(1);
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_PRODUCT_DATA_BY_URL, filterStr), beanListHandler, params.toArray()); 				
	}
	public static final String GET_PRODUCT_DATA_BY_ID = "SELECT * FROM tbl_product_data WHERE merchantId = ? AND id = ?";
	public List<ProductDataBean> getProductDataById(int merchantId, String id) throws SQLException {	
		return queryRunner.query(GET_PRODUCT_DATA_BY_ID, beanListHandler, new Object[] {merchantId, id}); 				
	}
	
	public static final String UPDATE = "UPDATE IGNORE tbl_product_data SET price = ?, basePrice = ?, description = ?, " +
			"pictureUrl = ?, status = ?, updateDate = NOW(), errorUpdateCount = ? WHERE id = ? AND merchantId = ?";
	public int updateProductData(ProductDataBean productDataBean) throws SQLException {
		productDataBean.setErrorUpdateCount(0);
		return queryRunner.update(UPDATE, new Object[] {
				productDataBean.getPrice(),
				productDataBean.getBasePrice(),
				FilterUtil.limitString(productDataBean.getDescription(), DESCRIPTION_LENGTH), 
				productDataBean.getPictureUrl(),
				STATUS.C.toString(),
				productDataBean.getErrorUpdateCount(),
				productDataBean.getId(),
				productDataBean.getMerchantId(),
		}); 		
	}
	
	public static final String DELETE = "DELETE FROM tbl_product_data WHERE id = ?";
	public int deleteSpecificProduct(int id) throws SQLException{
		return queryRunner.update(DELETE, id); 
	}
	
	
	public static final String GET_MERCHANT_DATA_BY_ID_LIMIT = "SELECT * FROM tbl_product_data WHERE merchantId = ? LIMIT ?";
	public List<ProductDataBean> getMerchantDataByIdLimit(int merchantId, int limit) throws SQLException {		
		return queryRunner.query(GET_MERCHANT_DATA_BY_ID_LIMIT, beanListHandler, new Object[] {merchantId, limit}); 				
	}
	
	public static final String GET_MERCHANT_DATA_BY_ID_WITH_STATUS = "SELECT * FROM tbl_product_data WHERE merchantId = ? and status = ? LIMIT ?";
	public ProductDataBean getMerchantDataByIdLimitWithStatus(int merchantId,String status,int limit) throws SQLException {		
		return queryRunner.query(GET_MERCHANT_DATA_BY_ID_WITH_STATUS, beanHandler, new Object[] {merchantId,status,limit}); 				
	}
}
