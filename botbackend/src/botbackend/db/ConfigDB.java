package botbackend.db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import org.apache.commons.dbutils.handlers.BeanHandler;


import botbackend.bean.ConfigBean;


public class ConfigDB {
	
	private ResultSetHandler<ConfigBean> beanHandler;
	QueryRunner queryRunner;

	public ConfigDB(DataSource dataSource) {
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<ConfigBean>(ConfigBean.class);
	}
	
	private static final String GET_VALUE_BY_NAME = "SELECT * FROM tbl_config WHERE configName = ?";
	public ConfigBean getByName(String configName) throws SQLException {
		return queryRunner.query(GET_VALUE_BY_NAME, beanHandler, configName);
	}
	
}
