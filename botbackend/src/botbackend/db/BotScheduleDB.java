package botbackend.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BotScheduleBean;
import botbackend.db.utils.NumRecordResultSetHandler;

public class BotScheduleDB {
	private ResultSetHandler<BotScheduleBean> beanHandler;
	private ResultSetHandler<List<BotScheduleBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	private DataSource dataSource;
	private QueryRunner queryRunner;
	
	public BotScheduleDB(DataSource dataSource) {
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<BotScheduleBean>(BotScheduleBean.class);
		beanListHandler = new BeanListHandler<BotScheduleBean>(BotScheduleBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private String GET_ALL_PROCESS = "SELECT * FROM tbl_bot_schedule";	
	public List<BotScheduleBean> getAllProcess() throws SQLException {
		return queryRunner.query(GET_ALL_PROCESS, beanListHandler);
	}
	
	private String GET_CURRENT_TIME = "SELECT NOW() as currentTime from tbl_bot_schedule LIMIT 1;";
	public Date getCurrentTime() throws SQLException {
		return queryRunner.query(GET_CURRENT_TIME, beanHandler).getCurrentTime();
	}
}
