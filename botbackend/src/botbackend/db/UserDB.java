package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.UserBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.IntegerListValueResultSetHandler;

public class UserDB extends UserBean {
	private ResultSetHandler<UserBean> beanHandler;
	private ResultSetHandler<List<UserBean>> beanListHandler;
	private IntegerListValueResultSetHandler integerListHandler;
	private DataSource dataSource;
	QueryRunner queryRunner;
	
	public UserDB(DataSource dataSource){
		this.dataSource = dataSource;
		queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<UserBean>(UserBean.class);
		beanListHandler = new BeanListHandler<UserBean>(UserBean.class);
		integerListHandler = new IntegerListValueResultSetHandler();
	}
	
	private static final String GET_USER = "SELECT * FROM tbl_backend_user WHERE UserName = ? AND UserPass = ?";
	public UserBean getUserLogin(String username, String password) throws SQLException{
		return queryRunner.query(GET_USER, beanHandler, new Object[] {username, password});
	}
	
	private static final String GET_USER_LIST = "SELECT userId, userName, userPermission FROM tbl_backend_user WHERE userStatus = ? ORDER BY Username";
	public List<UserBean> getUserList(int userStatus) throws SQLException { 
		return queryRunner.query(GET_USER_LIST, beanListHandler, userStatus);
	}
	
	private static final String GET_USER_LIST_ROLE = "SELECT userId, userName, userPermission FROM tbl_backend_user WHERE userPermission = ? and userStatus = 1 ORDER BY Username";
	public List<UserBean> getUserListRole(String role) throws SQLException { 
		return queryRunner.query(GET_USER_LIST_ROLE, beanListHandler, role);
	}
	
	private static final String GET_USER_ID = "SELECT userId, userName, userPermission FROM tbl_backend_user WHERE userName = ?";
	public UserBean getUserIDbyUserName(String userName) throws SQLException {
		return queryRunner.query(GET_USER_ID, beanHandler, userName);
	}
	
	private static final String GET_ROLE_PERMISSION ="SELECT rolePermission FROM tbl_backend_user_role WHERE roleName IN ( ---filter--- )";
	public List<String> getRolePermission(String [] permissionList) throws SQLException {
		List<String> permission = null;
		if(permissionList != null && permissionList.length > 0){
			StringBuilder filter = new StringBuilder();	
			List<String> params = new ArrayList<String>();
			permission = new ArrayList<String>();
			for(String permit : permissionList){
				filter.append("?,");
				params.add(permit);
				permission.add(permit);
			}
			filter.deleteCharAt(filter.length()-1);
			List<UserBean> permitList =  queryRunner.query(DatabaseUtil.replaceSQL(GET_ROLE_PERMISSION, filter.toString()), beanListHandler, params.toArray());
			if(permitList != null && permitList.size() > 0){
				for(UserBean s : permitList){
					if(s.getRolePermission() != null){
						String[] permitArr = s.getRolePermission().split("\\|");
						permission.addAll(Arrays.asList(permitArr));
					}	
				}	
			}
		}
		return permission;
		
	}
	
}
