package botbackend.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.SendDataBean;

import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.MapFieldStringResultSetHandler;
import utils.FilterUtil;

public class SendDataDB {		

	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;

	private ResultSetHandler<Map<String, String>> mapFieldStringResultSetHandler;
	private ResultSetHandler<Map<String, String>> mapFieldMerchantIdResultSetHandler;
	private ScalarHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	public SendDataDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		mapFieldStringResultSetHandler = new MapFieldStringResultSetHandler("action", "count");
		mapFieldMerchantIdResultSetHandler = new MapFieldStringResultSetHandler("merchantId", "count");
		numRecordHandler = new ScalarHandler<>();
	}
	
	private static final String REPORT_DATA_BY_STATUS = "SELECT action,COUNT(id) as count FROM tbl_send_data WHERE STATUS=? GROUP BY action";
	public Map<String,String> reportDataByStatus(String status) throws SQLException {		
		return queryRunner.query(REPORT_DATA_BY_STATUS,mapFieldStringResultSetHandler,status);
	}
	
	private static final String CHECK_COUNT_WAITING_SYNC = "SELECT merchantId,COUNT(id) as count FROM tbl_send_data WHERE STATUS=? GROUP BY merchantId";
	public Map<String,String> checkCountWaitingSync(String status) throws SQLException {		
		return queryRunner.query(CHECK_COUNT_WAITING_SYNC,mapFieldMerchantIdResultSetHandler,status);
	}
	
	private static final String CHECK_COUNT_WAITING_SYNC_HAVE_LIMIT = "SELECT id FROM tbl_send_data WHERE merchantId = ? AND STATUS = ? LIMIT 1";
	public int checkCountWaitingSyncHaveLimit(int merchantId,String status) throws SQLException {		
		return queryRunner.query(CHECK_COUNT_WAITING_SYNC_HAVE_LIMIT,numRecordHandler,merchantId,status);
	}

	public static final String INSERT = "INSERT INTO tbl_send_data" +
			"(action, merchantId, categoryId, name, price, url, description, pictureData, pictureUrl, " +
			" merchantUpdateDate, updateDate, realProductId, keyword, basePrice, upc, dynamicField) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public int insertSendData(SendDataBean sendDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				sendDataBean.getAction(),
				sendDataBean.getMerchantId(),
				sendDataBean.getCategoryId(), 
				FilterUtil.limitString(sendDataBean.getName(), NAME_LENGTH),
				sendDataBean.getPrice(),
				sendDataBean.getUrl(),
				FilterUtil.limitString(sendDataBean.getDescription(), DESCRIPTION_LENGTH), 
				sendDataBean.getPictureData(), 
				sendDataBean.getPictureUrl(),				
				sendDataBean.getMerchantUpdateDate(),
				sendDataBean.getUpdateDate(),
				sendDataBean.getRealProductId(),
				sendDataBean.getKeyword(),
				sendDataBean.getBasePrice(),
				sendDataBean.getUpc(),
				sendDataBean.getDynamicField()
		}); 		
	}
	
	private static final String SELECT_BY_STATUS_AND_RANGE_ID = "SELECT id FROM tbl_send_data WHERE status=? AND (id BETWEEN ? AND ?)";
	public int getDataByStatusAndRangeId(String status, int firstProductId, int lastProdictId) throws SQLException {		
		return queryRunner.query(SELECT_BY_STATUS_AND_RANGE_ID,numRecordHandler, status, firstProductId, lastProdictId);
	}
	
	private static final String SELECT_BY_DATE_AND_STATUS = "SELECT id FROM tbl_send_data WHERE updateDate>=? AND updateDate<=? ORDER BY id ---order--- LIMIT 1";
	public int getIdByDate(Date start, Date end, String order) throws SQLException {
		return queryRunner.query(DatabaseUtil.replaceSQLOrder(SELECT_BY_DATE_AND_STATUS, order),numRecordHandler, start, end);
	}
	
}
