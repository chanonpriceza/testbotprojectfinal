package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BotConfigBean;
import botbackend.db.utils.StringListValueResultSetHandler;

public class BotConfigDB {
		
	private DataSource dataSource;	
	private ResultSetHandler<BotConfigBean> beanHandler;
	private ResultSetHandler<List<BotConfigBean>> beanListHandler;
	private StringListValueResultSetHandler stringListHandler;
	
	QueryRunner queryRunner; 		
	
	public BotConfigDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<BotConfigBean>(BotConfigBean.class);	
		beanListHandler = new BeanListHandler<BotConfigBean>(BotConfigBean.class);
		stringListHandler = new StringListValueResultSetHandler();
	}
	
	public static final String SELECT_BY_SERVER_NAME = "SELECT * FROM tbl_botdb_config WHERE server =  ?";
	public BotConfigBean findBotConfig(String serverName) throws SQLException{	
		return queryRunner.query(SELECT_BY_SERVER_NAME, beanHandler, new Object[] {serverName});
	}	
	
	public static final String SELECT_BY_SERVER_LIST = "SELECT * FROM tbl_botdb_config ";
	public List<BotConfigBean> getBotConfigList() throws SQLException{	
		return queryRunner.query(SELECT_BY_SERVER_LIST, beanListHandler);
	}
	
	public static final String SELECT_BY_ACTIVE_SERVER_LIST = "SELECT * FROM tbl_botdb_config WHERE available = 1";
	public List<BotConfigBean> getActiveBotConfigList() throws SQLException{	
		return queryRunner.query(SELECT_BY_ACTIVE_SERVER_LIST, beanListHandler);
	}
	
	public static final String SELECT_SERVER_LIST = "SELECT server FROM tbl_botdb_config where available = 1";
	public List<String> getActiveServerNameList() throws SQLException{	
		return queryRunner.query(SELECT_SERVER_LIST, stringListHandler);
	}
	
	public static final String SELECT_DATA_SERVER_LIST = "SELECT server FROM tbl_botdb_config where dataAvailable = 1";
	public List<String> getActiveDataServerNameList() throws SQLException{	
		return queryRunner.query(SELECT_DATA_SERVER_LIST, stringListHandler);
	}	
}
