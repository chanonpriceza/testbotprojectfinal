package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.ProductDataBean;

import botbackend.bean.common.WorkLoadFeedCommonBean;
import botbackend.db.utils.DatabaseUtil;

public class WorkLoadFeedDB {		

	private ResultSetHandler<WorkLoadFeedCommonBean> beanHandler;
	private ResultSetHandler<List<WorkLoadFeedCommonBean>> beanListHandler;
	QueryRunner queryRunner;
	
	public WorkLoadFeedDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanHandler = new BeanHandler<WorkLoadFeedCommonBean>(WorkLoadFeedCommonBean.class);
		beanListHandler = new BeanListHandler<WorkLoadFeedCommonBean>(WorkLoadFeedCommonBean.class);
	}
		
	public static final String GET_COMPARE_PRODUCT_DATA = "SELECT wf.*,pd.merchantId AS old_merchantId, pd.name AS old_name, pd.price AS old_price, pd.url AS old_url, pd.urlForUpdate AS old_urlForUpdate, pd.description AS old_description, pd.pictureUrl AS old_pictureUrl, pd.realProductId AS old_realProductId, pd.basePrice AS old_basePrice, pd.upc AS old_upc from tbl_workload_feed wf LEFT JOIN tbl_product_data pd ON wf.urlForUpdate = pd.urlForUpdate WHERE wf.merchantId = ? AND wf.type = ? LIMIT ?";
	public List<WorkLoadFeedCommonBean> getCompareProductData(String type, int merchantId, int offset) throws SQLException {		
		return queryRunner.query(GET_COMPARE_PRODUCT_DATA, beanListHandler, new Object[] {merchantId, type, offset});
	}
	
	public static final String GET_COMPARE_PRODUCT_DATA_BY_IDS = "SELECT * FROM tbl_workload_feed WHERE  type = ? AND  merchantId = ? AND urlForUpdate IN (---filter---)";
	public List<WorkLoadFeedCommonBean> getRandomProductDataByIds(String type, int merchantId, List<ProductDataBean> pdList) throws SQLException {
		
		StringBuilder filters = new StringBuilder();	
		List<Object> params = new ArrayList<Object>();
		
		params.add(type);
		params.add(merchantId);
		if(pdList != null && pdList.size() > 0){
			String urlStr = "";
			for(ProductDataBean pdBean : pdList){
				urlStr += ",?";
				params.add(pdBean.getUrlForUpdate());
			}
			urlStr  = urlStr.substring(1, urlStr.length());
			filters.append(urlStr);

		}
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_COMPARE_PRODUCT_DATA_BY_IDS, filters.toString()), beanListHandler, params.toArray());
	}
	public static final String GET_SAMPLE_DATA = "SELECT * FROM tbl_workload_feed WHERE  type = ? AND  merchantId = ? LIMIT 1";
	public WorkLoadFeedCommonBean getSampleDta(String type, int merchantId) throws SQLException {
		return  queryRunner.query(GET_SAMPLE_DATA, beanHandler, new Object[] {type,merchantId});
	}
	
}
