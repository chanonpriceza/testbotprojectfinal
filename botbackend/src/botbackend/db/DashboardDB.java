package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.BotQualityReportBean;
import botbackend.bean.ProductWaitSyncBean;

public class DashboardDB {
	private ArrayListHandler arrayListHandler;
	QueryRunner queryRunner;
	ResultSetHandler<List<ProductWaitSyncBean>> beanListHandler;
	ResultSetHandler<List<BotQualityReportBean>> beanListHandlerBotQuality;

	public DashboardDB(DataSource dataSource) {
		queryRunner = new QueryRunner(dataSource);
		arrayListHandler = new ArrayListHandler();
		beanListHandler = new BeanListHandler<ProductWaitSyncBean>(ProductWaitSyncBean.class);
		beanListHandlerBotQuality = new BeanListHandler<BotQualityReportBean>(BotQualityReportBean.class);
	}
	
	private static final String GET_PLUGIN_TYPE_1_DATA_LIST = ""
			+ "SELECT "
			+ "bu.username owner, "
			+ "count(bu.userId)  total, "
			+ "count(CASE WHEN(mo.status = 1 AND mo.analyzeResult != 'Unknown') THEN 1 END) issue, "
			+ "ROUND(count(CASE WHEN(mo.status = 1 AND mo.analyzeResult != 'Unknown') THEN 1 END) / count(bu.userId) * 100 ) percentage "
			+ "FROM tbl_merchant mc "
			+ "JOIN tbl_backend_user bu ON mc.ownerId = bu.userId "
			+ "LEFT JOIN tbl_monitor mo ON mc.id = mo.merchantId AND mo.status = 1 "
			+ "GROUP BY bu.userId "
			+ "ORDER BY percentage DESC";
	
	public List<Object[]> getPluginType1DataList() throws SQLException {
		List<Object[]> resultSet = queryRunner.query(GET_PLUGIN_TYPE_1_DATA_LIST, arrayListHandler);
		return resultSet;
	}
	
	private static final String GET_PLUGIN_TYPE_2_DATA_LIST = "SELECT * FROM tbl_product_wait_sync WHERE addDate>=?";
	public List<ProductWaitSyncBean> getPluginType2DataList(Date date) throws SQLException {
		List<ProductWaitSyncBean> listTask = queryRunner.query(GET_PLUGIN_TYPE_2_DATA_LIST, beanListHandler, date);
		return listTask;
	}
	
	private static final String GET_PLUGIN_TYPE_3_MERCHANTLIST = "SELECT id,name,active,server,userUpdate,errorMessage FROM tbl_merchant WHERE CONCAT('|',coreUserConfig ,'|') like ?;";
	public List<Object[]> getUserCoreMerchant(int userId) throws SQLException {
		List<Object[]> resultSet1 = queryRunner.query(GET_PLUGIN_TYPE_3_MERCHANTLIST, arrayListHandler,"%|"+userId+"|%");
		return resultSet1;	
	}
	
	private static final String GET_PLUGIN_TYPE_3_RUNTIMELIST = "UNION (SELECT merchantId, type, startDate, endDate FROM `tbl_report` WHERE merchantId = ? and type = ? ORDER BY id DESC limit ?) ";
	public List<Object[]> getPluginType3DataList(List<String> merchantIdList) throws SQLException {
		String allQuery = "";
		List<Object> allParam = new ArrayList<>();
		for (String merchantId : merchantIdList) {
			allQuery += GET_PLUGIN_TYPE_3_RUNTIMELIST + GET_PLUGIN_TYPE_3_RUNTIMELIST;
			allParam.addAll(Arrays.asList(new Object[] {merchantId, "WCE", 3, merchantId, "DUE", 3}));
		}
		List<Object[]> resultSet1 = queryRunner.query(allQuery.substring(6), arrayListHandler, allParam.toArray());
		return resultSet1;	
	}
	
	private static final String GET_PLUGIN_TYPE_4_DATA_LIST = "(SELECT * FROM tbl_bot_quality_report q WHERE q.`type` = 'Summary' AND q.name LIKE 'Product%' ) UNION (SELECT * FROM tbl_bot_quality_report_history qh WHERE qh.`type` = 'Summary' AND qh.`name` LIKE 'Product%' order by id desc limit 42)";
	public List<BotQualityReportBean> getPluginType4DataList() throws SQLException {
		List<BotQualityReportBean> listTask = queryRunner.query(GET_PLUGIN_TYPE_4_DATA_LIST, beanListHandlerBotQuality);
		return listTask;
	}
}