package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.ProductWaitSyncBean;
import botbackend.bean.ProductWaitSyncQualityBean;

public class ProductWaitSyncQualityDB {
	
	private DataSource dataSource;	
	private ResultSetHandler<List<ProductWaitSyncQualityBean>> beanListHandler;

	QueryRunner queryRunner; 		
	
	public ProductWaitSyncQualityDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<ProductWaitSyncQualityBean>(ProductWaitSyncQualityBean.class);
	}
	
	public static final String INSERT = "INSERT INTO tbl_product_wait_sync_quality" +
			" (`dataServer`, `date`, `allCount`, `remain`, `done`, `score`) " +
			"VALUES (?,?,?,?,?,?)";
	public int insertProductWaitSyncQualityBean(ProductWaitSyncQualityBean productWaitSyncQualityBean) throws SQLException {
		return queryRunner.update(INSERT, 
				productWaitSyncQualityBean.getDataServer(),
				productWaitSyncQualityBean.getDate(),
				productWaitSyncQualityBean.getAllCount(),
				productWaitSyncQualityBean.getRemain(),
				productWaitSyncQualityBean.getDone(),
				productWaitSyncQualityBean.getScore());
	}

}
