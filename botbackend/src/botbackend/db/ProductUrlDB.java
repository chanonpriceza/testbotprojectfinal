package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.ProductUrlBean;
import botbackend.db.utils.NumRecordResultSetHandler;
import botbackend.db.utils.StringListValueResultSetHandler;

public class ProductUrlDB {		

	private DataSource dataSource;	
	private StringListValueResultSetHandler stringListHandler;
	private ResultSetHandler<List<ProductUrlBean>>  beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	
	QueryRunner queryRunner; 		
	
	public ProductUrlDB(DataSource dataSource) {
		this.dataSource = dataSource;		
		this.queryRunner = new QueryRunner(dataSource);
		stringListHandler = new StringListValueResultSetHandler();
		numRecordHandler = new NumRecordResultSetHandler();
		beanListHandler = new BeanListHandler<ProductUrlBean>(ProductUrlBean.class);
	}
	
	public static final String SELECT_BY_MERCHANTID_AND_STATUS = "SELECT url FROM tbl_product_url WHERE merchantId = ? AND state = ? AND status = ? LIMIT ?";
	public List<String> findProductUrl(int merchantId,String state, String status, int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_AND_STATUS, stringListHandler, new Object[] {merchantId, state, status, limit}); 				
	}
	
	public static final String SELECT_BY_MERCHANTID_LIMIT = "SELECT * FROM tbl_product_url WHERE merchantId = ? LIMIT ?";
	public List<ProductUrlBean> getProductUrlByMerchantId(int merchantId,int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_LIMIT, beanListHandler, new Object[] {merchantId, limit}); 				
	}
	
	public static final String COUNT_PRODUCTURL_GROUP_BY_STATE = "SELECT count(url) as totoalCount, state FROM tbl_product_url WHERE merchantId = ? GROUP BY state"; 
	public List<ProductUrlBean> countProductUrlByState(int merchantId) throws SQLException {
		return queryRunner.query(COUNT_PRODUCTURL_GROUP_BY_STATE, beanListHandler , new Object[] {merchantId});
	}
	
	public static final String SELECT_BY_MERCHANTID_AND_START_LIMIT = "SELECT * FROM tbl_product_url INNER JOIN (SELECT merchantId, url FROM tbl_product_url WHERE merchantId = ? LIMIT ?,?) AS p ON tbl_product_url.merchantId = p.merchantId AND tbl_product_url.url = p.url ";
	public List<ProductUrlBean> getProductUrlByMerchantIdAndStartLimit(int merchantId,int start,int limit) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_AND_START_LIMIT, beanListHandler, new Object[] {merchantId, start ,limit}); 				
	}
	
	public static final String COUNT_ALL_PRODUCTURL = "SELECT count(url) as totoalCount FROM tbl_product_url WHERE merchantId = ?"; 
	public Integer countAllProductUrl(int merchantId) throws SQLException {
		return queryRunner.query(COUNT_ALL_PRODUCTURL, numRecordHandler , merchantId);
	}
	
	public static final String COUNT_BY_MERCHANTID_AND_STATE = "SELECT count(url) FROM tbl_product_url WHERE merchantId = ? AND state = ?";
	public int countByMerchantAndState(int merchantId,String state) throws SQLException {		
		return queryRunner.query(COUNT_BY_MERCHANTID_AND_STATE, numRecordHandler,merchantId, state); 				
	}


}
