package botbackend.db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import botbackend.bean.TaskDetailBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;

public class TaskDetailDB {

	private ResultSetHandler<List<TaskDetailBean>> beanListHandler;
	private ResultSetHandler<TaskDetailBean> beanHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	QueryRunner queryRunner;
	
	public TaskDetailDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		numRecordHandler = new NumRecordResultSetHandler();
		beanListHandler = new BeanListHandler<TaskDetailBean>(TaskDetailBean.class);
		beanHandler = new BeanHandler<TaskDetailBean>(TaskDetailBean.class);
	}
	
	private static final String GET_DATA_BY_TASK_ID = "SELECT * FROM tbl_backend_task_history WHERE  taskId = ? ORDER BY id DESC ";
	public List<TaskDetailBean> getDataByTaskId(int id) throws SQLException{
		return queryRunner.query(GET_DATA_BY_TASK_ID, beanListHandler, id);
	}
	
	private static final String GET_DATA_BY_TASK_ID_STATUS = "SELECT * FROM tbl_backend_task_history WHERE  taskId = ? and status in ---filter--- ORDER BY id DESC ";
	public List<TaskDetailBean> getDataByTaskIdAndStatus(int id,String[] status) throws SQLException{
		String filter = "";
		if(status != null && status.length > 0){
			StringBuilder filterStatus = new StringBuilder();
			for (String s : status) {
				filterStatus.append(",");
				filterStatus.append("\"" + s + "\"");
			}
			filter = "("+filterStatus.toString().substring(1)+")";
		}
		return queryRunner.query(DatabaseUtil.replaceSQL(GET_DATA_BY_TASK_ID_STATUS,filter), beanListHandler,id);
	}
	
	private static final String GET_DATA_BY_TASK_ID_LIMIT = "SELECT * FROM tbl_backend_task_history WHERE  taskId = ? ORDER BY id DESC LIMIT ?";
	public TaskDetailBean getDataByTaskId(int id, int limit) throws SQLException{
		return queryRunner.query(GET_DATA_BY_TASK_ID_LIMIT, beanHandler, id, limit);
	}
	
	private static final String GET_OWNER_BY_TASK_ID = "SELECT DISTINCT usr.userId as ownerId, usr.userName as owner FROM tbl_backend_task_history tkh LEFT JOIN tbl_backend_user usr on tkh.`owner` = usr.userName WHERE taskId = ?";
	public List<TaskDetailBean> getOwnerByTaskId(int id) throws SQLException{
		return queryRunner.query(GET_OWNER_BY_TASK_ID, beanListHandler, id);
	}
	
	private static final String INSERT_NEW_COMMENT = "INSERT INTO tbl_backend_task_history(taskId, message, status, owner, createDate) VALUES (?, ?, ?, ?, NOW()) ";
	public int insertComment(int taskId, String message, String status, String owner) throws SQLException{
		return queryRunner.insert(INSERT_NEW_COMMENT, numRecordHandler, taskId, message, status, owner);
	}
	
	private static final String DELETE_COMMENT = "DELETE FROM tbl_backend_task_history WHERE id = ? AND taskId = ? AND owner = ?";
	public int deleteComment(int commentId, int taskId, String owner) throws SQLException{
		return queryRunner.update(DELETE_COMMENT, commentId, taskId, owner);
	}
	
	private static final String INSERT_NEW_COMMENT_BATCH = "INSERT INTO tbl_backend_task_history(taskId, message, status, owner, createDate) VALUES (?, ?, ?, ?, NOW()) ";
	public int[] insertCommentBatch(List<TaskDetailBean> commentList) throws SQLException {
		int i = 0;
		Object[][] obj = new Object[commentList.size()][4];
		
		for (TaskDetailBean comment : commentList) {
			obj[i][0] = comment.getTaskId();
			obj[i][1] =	comment.getMessage();
			obj[i][2] =	comment.getStatus();
			obj[i][3] =	comment.getOwner();
			
			i++;
		}
		return queryRunner.batch(INSERT_NEW_COMMENT_BATCH, obj );
	}	
}
