package botbackend.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.ParserConfigBean;
import botbackend.db.utils.DatabaseUtil;
import botbackend.db.utils.NumRecordResultSetHandler;

public class ParserConfigDB {
	private QueryRunner queryRunner;
	private ResultSetHandler<List<ParserConfigBean>> beanListHandler;
	private ResultSetHandler<Integer> numRecordHandler;
	
	public ParserConfigDB(DataSource dataSource){
		queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<ParserConfigBean>(ParserConfigBean.class);
		numRecordHandler = new NumRecordResultSetHandler();
	}
	
	private final String INSERT = "INSERT INTO tbl_parser_config(merchantId, fieldType, filterType, value) VALUES(?, ?, ?, ?)";
	public int[] insert(Object[][] obj) throws SQLException{
		return queryRunner.batch(INSERT, obj);
	}
	
	private final String DELETE = "DELETE FROM tbl_parser_config WHERE merchantId = ?";
	public int deleteByMerchantId(int merchantId) throws SQLException{
		return queryRunner.update(DELETE, merchantId);
	}

	private final String DELETE_EXCEPT = "DELETE FROM tbl_parser_config WHERE merchantId = ? AND fieldType NOT IN ('name', 'concatId', 'concatWord') ";
	public int deleteByMerchantIdExcept(int merchantId) throws SQLException{
		return queryRunner.update(DELETE_EXCEPT, merchantId);
	}
	
	private final String DELETE_FIELD = "DELETE FROM tbl_parser_config WHERE merchantId = ? AND fieldType IN (---filter---)";
	public int deleteByMerchantIdAndField(int merchantId, List<String> fieldList) throws SQLException {
		StringBuilder filters = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if(fieldList != null && fieldList.size() > 0){
			params.add(merchantId);
			for(String  field : fieldList){
				filters.append(",?");
				params.add(field);
			}
			String filter = filters.substring(1).toString();
			return queryRunner.update(DatabaseUtil.replaceSQL(DELETE_FIELD, filter), params.toArray());
		}
		return 0;
	}
	
	private final String GET_BY_MERCHANT_ID = "SELECT * FROM tbl_parser_config WHERE merchantId = ? ORDER BY fieldType, id";
	public List<ParserConfigBean> getByMerchantId(int merchantId) throws SQLException{
		return queryRunner.query(GET_BY_MERCHANT_ID, beanListHandler, merchantId);
	}
	
	private static final String COUNT_FIELD_TYPE_BY_MERCHANTID = "SELECT COUNT(id) FROM tbl_parser_config WHERE merchantId = ?  AND fieldType = ? "; 
	public int countByFieldByMerchantId(int merchantId, String fieldType) throws SQLException {
		return queryRunner.query(COUNT_FIELD_TYPE_BY_MERCHANTID, numRecordHandler, merchantId , fieldType);
	}
}
