package botbackend.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import web.crawler.bot.URLUtility;

import botbackend.bean.UrlPatternBean;

public class UrlPatternManager {
	
	List<List<UrlPatternBean>> allContinueBeanList;
	List<List<UrlPatternBean>> allMatchBeanList;
	List<List<UrlPatternBean>> allDropBeanList;
	List<List<UrlPatternBean>> allRemoveBeanList;
	List<List<UrlPatternBean>> allFeedmappingBeanList;
	
	public UrlPatternManager(List<UrlPatternBean> urlPatternBeanList) {
		allContinueBeanList 	= new ArrayList<List<UrlPatternBean>>();
		allMatchBeanList 		= new ArrayList<List<UrlPatternBean>>();
		allDropBeanList 		= new ArrayList<List<UrlPatternBean>>();
		allRemoveBeanList 		= new ArrayList<List<UrlPatternBean>>();
		allFeedmappingBeanList 	= new ArrayList<List<UrlPatternBean>>();
		
		Map<Integer, List<UrlPatternBean>> continueMap 		= new HashMap<Integer, List<UrlPatternBean>>();
		Map<Integer, List<UrlPatternBean>> matchMap 		= new HashMap<Integer, List<UrlPatternBean>>();
		Map<Integer, List<UrlPatternBean>> dropMap 			= new HashMap<Integer, List<UrlPatternBean>>();
		Map<Integer, List<UrlPatternBean>> removeMap		= new HashMap<Integer, List<UrlPatternBean>>();
		Map<Integer, List<UrlPatternBean>> feedMappingMap 	= new HashMap<Integer, List<UrlPatternBean>>();
		
		for(UrlPatternBean urlPatternBean : urlPatternBeanList) {
			Map<Integer, List<UrlPatternBean>> currentMap = null;
			List<List<UrlPatternBean>> currentList = null;
			
			if(urlPatternBean.getAction().equalsIgnoreCase("CONTINUE")) {
				currentMap = continueMap;
				currentList = allContinueBeanList;
			} else if(urlPatternBean.getAction().equalsIgnoreCase("MATCH")) {
				currentMap = matchMap;
				currentList = allMatchBeanList;
			} else if(urlPatternBean.getAction().equalsIgnoreCase("DROP")) {
				currentMap = dropMap;
				currentList = allDropBeanList;
			} else if(urlPatternBean.getAction().equalsIgnoreCase("REMOVE")) {
				currentMap = removeMap;
				currentList = allRemoveBeanList;
			} else if(urlPatternBean.getAction().equalsIgnoreCase("FEEDMAPPING")) {
				currentMap = feedMappingMap;
				currentList = allFeedmappingBeanList;
			}
			int group = urlPatternBean.getGroup();
			
			if(group == 0) {
				List<UrlPatternBean> tmpList = new ArrayList<>();
				tmpList.add(urlPatternBean);
				currentList.add(tmpList);
			} else {				
				if(currentMap.get(group) == null) {
					currentMap.put(group, new ArrayList<UrlPatternBean>());
				}
				currentMap.get(group).add(urlPatternBean);
			}
		}
		
		if(continueMap.size() > 0) {
			for(Entry<Integer, List<UrlPatternBean>> tmpEntry : continueMap.entrySet()) {
				allContinueBeanList.add(tmpEntry.getValue());
			}
		}
		
		if(matchMap.size() > 0) {
			for(Entry<Integer, List<UrlPatternBean>> tmpEntry : matchMap.entrySet()) {
				allMatchBeanList.add(tmpEntry.getValue());
			}
		}
		
		if(dropMap.size() > 0) {
			for(Entry<Integer, List<UrlPatternBean>> tmpEntry : dropMap.entrySet()) {
				allDropBeanList.add(tmpEntry.getValue());
			}
		}
		
		if(removeMap.size() > 0) {
			for(Entry<Integer, List<UrlPatternBean>> tmpEntry : removeMap.entrySet()) {
				allRemoveBeanList.add(tmpEntry.getValue());
			}
		}
		
		if(feedMappingMap.size() > 0) {
			for(Entry<Integer, List<UrlPatternBean>> tmpEntry : feedMappingMap.entrySet()) {
				allFeedmappingBeanList.add(tmpEntry.getValue());
			}
		}
		
	}

	public boolean isPatternMatch(String url) {
		return checkURLPatternMatch(url, allMatchBeanList);
	}

	public boolean isPatternContinue(String url) {
		return checkURLPatternMatch(url, allContinueBeanList);
	}

	public boolean isPatternDrop(String url) {
		return checkURLPatternMatch(url, allDropBeanList);
	}
	
	public String processRemoveQuery(String url) {
		if(allRemoveBeanList.size() == 0) { 
			return url;
		}
		for(List<UrlPatternBean> removeBeanList : allRemoveBeanList) {
			for(UrlPatternBean patternBean : removeBeanList) {
				if(patternBean.getCondition().equalsIgnoreCase("haveQuery")
						&& checkURLhaveQuery(url, patternBean.getValue())) {
					url = URLUtility.removeQueryString(url, patternBean.getValue());
					//System.out.println("RemoveQuery "+patternBean.getValue()+" from "+url);
				}
			}			
		}
		return url;
	}

	public boolean checkURLPatternMatch(String url, List<List<UrlPatternBean>> allPatternList) {
		
		if (allPatternList.size() == 0) {
			return false;
		}
		
		boolean rtn = true;
		for(List<UrlPatternBean> patternList : allPatternList) {
			rtn = true;
			for(UrlPatternBean patternBean : patternList) {
				if (patternBean.getCondition().equalsIgnoreCase("haveString")) {
					if (!checkURLhavaString(url, patternBean.getValue())) {								
						rtn = false;
						break;
					}
				} else if (patternBean.getCondition().equalsIgnoreCase("haveQuery")) {
					if (!checkURLhaveQuery(url, patternBean.getValue())) {
						rtn = false;
						break;
					}
				} else if (patternBean.getCondition().equalsIgnoreCase("endsWith")) {
					if (!checkURLEndsWith(url, patternBean.getValue())) {
						rtn = false;
						break;
					}
				}
			}
			if(rtn == true) {
				return rtn;
			}
		}
		return rtn;
	}

	public boolean checkURLhavaString(String url, String test) {
		return (url.toUpperCase().indexOf(test.toUpperCase()) == -1) ? false
				: true;
	}

	public boolean checkURLEndsWith(String url, String test) {
		return (url.toUpperCase().endsWith(test.toUpperCase())) ? true : false;
	}

	public boolean checkURLhaveQuery(String url, String test) {
		int eqSign = test.indexOf("=");
		if (eqSign == -1) {
			return URLUtility.haveQueryString(url, test, "");
		} else {
			return URLUtility.haveQueryString(url, test.substring(0, eqSign),
					test.substring(eqSign + 1, test.length()));
		}
	}

}