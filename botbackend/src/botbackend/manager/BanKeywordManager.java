package botbackend.manager;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.lowerCase;
import static org.apache.commons.lang3.StringUtils.trim;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Pattern;

import botbackend.db.BanKeywordDB;

public class BanKeywordManager {
	
	private static  BanKeywordManager banKeywordManager ;
	
	private static final Pattern NORMALIZE_PATTERN = Pattern.compile("[,+\\-*/\\[\\]_().'\"\\\\#:;@<>~]");
	
	private List<BanKeywordRule> checkList;
	
	public static BanKeywordManager getBanKeywordManager(BanKeywordDB banDB) {
		try {			
			if(banKeywordManager==null) {
			   banKeywordManager = new BanKeywordManager(banDB);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return banKeywordManager;
	}
	
	public BanKeywordManager(BanKeywordDB banKeywordDB) throws SQLException {
		this(banKeywordDB.load().stream().map(BanKeywordRule::fromBanKeyword).filter(x -> x != null).collect(toList()));
	}
	
	public BanKeywordManager(List<BanKeywordRule> banList) {
		this.checkList = banList;
	}
	
	public static String normalizeForMatching(String text) {
		if (text == null)
			return null;
		
		return lowerCase(NORMALIZE_PATTERN.matcher(trim(text)).replaceAll(" "));
	}
	
	public boolean isBanKeyword(String productInfo, int merchantId, int category, Double price) {
		if (isBlank(productInfo)) {
			return false;
		}
		
		productInfo = " " + normalizeForMatching(productInfo) + " ";
		for (BanKeywordRule rule : checkList) {
			boolean ruleMatch = rule.isRuleMatch(productInfo, merchantId, category, price);
			if (ruleMatch) {
				return true;
			}
		}
		return false;
	}

	public BanKeywordRule isBanKeywordWithKeyword(String productInfo, int merchantId, int category, Double price) {
		if (isBlank(productInfo)) {
			return null;
		}
		
		productInfo = " " + normalizeForMatching(productInfo) + " ";
		for (BanKeywordRule rule : checkList) {
			boolean ruleMatch = rule.isRuleMatch(productInfo, merchantId, category, price);
			if (ruleMatch) {
				return  rule;
			}
		}
		return null;
	}
	
	public List<BanKeywordRule> getCheckList() {
		return checkList;
	}

	public void setCheckList(List<BanKeywordRule> checkList) {
		this.checkList = checkList;
	}	
}
