package botbackend.manager;

import static java.util.Arrays.*;
import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.math.NumberUtils.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import utils.FilterUtil;

import botbackend.bean.BanKeywordBean;

public class BanKeywordRule {
	private final Set<Integer> merchantIds;
	private final List<String> keywordList;
	private final List<String> nonKeywordList;
	private final String fullKeyword;
	private final Integer minPrice;
	private final int[] nonCategory;
	
	public BanKeywordRule(String merchantIdListStr, String keywordListStr, String nonKeywordListStr, String fullKeyword, Integer minPrice, String nonCategoryList) {
		this.merchantIds = stream(split(defaultString(merchantIdListStr), "|"))
				.map(s -> toInt(trim(s), 0))
				.filter(id -> id > 0)
				.collect(toSet());
		
		String normalizeKeyword = BanKeywordManager.normalizeForMatching(keywordListStr);
		this.keywordList = stream(split(normalizeKeyword, " "))
									.filter(s -> isNotBlank(s))
									.map(s -> " " + s + " ")
									.collect(toList());
		
		this.nonKeywordList = stream(split(defaultString(nonKeywordListStr), "|"))
				.map(BanKeywordManager::normalizeForMatching)
				.filter(s -> isNotBlank(s))
				.collect(toList());
		
		this.fullKeyword = BanKeywordManager.normalizeForMatching(fullKeyword);
		
		this.minPrice = minPrice;
		
		Optional<String> nonCategoryOpt = Optional.ofNullable(nonCategoryList);
		this.nonCategory = nonCategoryOpt.map(s -> split(s, "|"))
									.map(arr -> stream(arr)
											.mapToInt(s -> toInt(s))
											.toArray()
									)
									.orElse(new int[0]);
	}
	
	public static BanKeywordRule fromBanKeyword(BanKeywordBean banKeyword) {
		String keyword = banKeyword.getKeyword();
		if (isBlank(keyword))
			return null;
									
		return new BanKeywordRule(banKeyword.getMerchantIdList(), banKeyword.getKeyword(), banKeyword.getNonkeyword(), banKeyword.getRequiredPhrase(), banKeyword.getMinPrice(), banKeyword.getNonCategory());
	} 

	public boolean isRuleMatch(String normalizedProductInfo, int merchantId, int category, Double productPrice) {

		if (Arrays.stream(this.nonCategory).anyMatch(c -> c == category))
			return false;
		if (merchantIds != null && !merchantIds.isEmpty()) {
			if (!merchantIds.contains(merchantId)) {
				return false;
			}
		}
		
		if (nonKeywordList != null) {
			for (String s : nonKeywordList) {
				if (normalizedProductInfo.contains(s)) {
					return false;
				}
			}
		}
		
		if (isNotBlank(fullKeyword)) {
			if (!normalizedProductInfo.contains(fullKeyword)) {
				return false;
			}
		} else if (!keywordList.isEmpty()){
			for (String s : keywordList) {
				s = s.trim();
				s = FilterUtil.toPlainTextString(s);
				if (!normalizedProductInfo.contains(s)) { // all keyword must matches
					return false;
				}
			}
		} else {
			return false;
		}

		if (minPrice != 0) {
			return productPrice != null && productPrice < minPrice.doubleValue();
		}
		return true;
	}

	public List<String> getKeywordList() {
		return keywordList;
	}
	public String getFullKeyword() {
		return fullKeyword;
	}
	public Set<Integer> getMerchantIds() {
		return merchantIds;
	}
	public List<String> getNonKeywordList() {
		return nonKeywordList;
	}
	public Integer getMinPrice() {
		return minPrice;
	}
	public int[] getNonCategory() {
		return nonCategory;
	}
}