package botbackend.schedule;

import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;

import botbackend.utils.ACCPropertyParser;

import botbackend.db.utils.DatabaseUtil;
import botbackend.service.aws.SimpleEmail;

public abstract class Schedule {
	
	private SimpleEmail sendEmail;
	private String PROCESS_NAME;
	private String EMAIL_SENDER;
	private String EMAIL_TO;
	private String MODULE;
	
	protected DataSource getWebDataSource(ACCPropertyParser accProp,String country) {
		
		DataSource ds = null;
		
		if (StringUtils.isBlank(country)) {
			String webDBDriverName = accProp.getString("web.db.driver");
			String webDBUrl = accProp.getString("web.db.url");
			String webDBUserName = accProp.getString("web.db.username");
			String webDBPassword = accProp.getString("web.db.password");
			ds =  DatabaseUtil.createDataSource(webDBUrl, webDBDriverName, webDBUserName, webDBPassword, 3);
		} else {
			String webDBDriverName = accProp.getString("web."+country+".db.driver");
			String webDBUrl = accProp.getString("web."+country+".db.url");
			String webDBUserName = accProp.getString("web."+country+".db.username");
			String webDBPassword = accProp.getString("web."+country+".db.password");
			ds =  DatabaseUtil.createDataSource(webDBUrl, webDBDriverName, webDBUserName, webDBPassword, 3);
		}
		
		return ds;
	}
	
	protected DataSource getProdDataSource(ACCPropertyParser accProp) {
		String prodDBDriverName = accProp.getString("web.prod.db.driver");
		String prodDBURL 		= accProp.getString("web.prod.db.url");
		String prodDBUserName 	= accProp.getString("web.prod.db.username");
		String prodDBPassword 	= accProp.getString("web.prod.db.password");
		
		return DatabaseUtil.createDataSource(prodDBURL, prodDBDriverName, prodDBUserName, prodDBPassword, 3);
	}
	
	protected DataSource getBotBackendDataSource(ACCPropertyParser propConfig, String country) {
	    String bbeDBDriverName = propConfig.getString("bot."+country+".backend.db.driver-name");
	  	String bbeDBURL 	   = propConfig.getString("bot."+country+".backend.db.url");
	  	String bbeDBUserName   = propConfig.getString("bot."+country+".backend.db.username");
	  	String bbeDBPassword   = propConfig.getString("bot."+country+".backend.db.password");
	  	
	  	return DatabaseUtil.createDataSource(bbeDBURL, bbeDBDriverName, bbeDBUserName, bbeDBPassword, 3);
	}
	
	protected void setUpEmail(ACCPropertyParser accProp, String module, String processName) {
		
		String accessKey = accProp.getString("awsses.accesskey");
		String secretKey = accProp.getString("awsses.secretkey");					
		sendEmail = new SimpleEmail(accessKey, secretKey);
		
		EMAIL_SENDER = 	accProp.getString("mail.sender");
		if (StringUtils.isBlank(EMAIL_SENDER)) {
			EMAIL_SENDER = BaseSchedule.EMAIL_FROM;
		}
		EMAIL_TO = accProp.getString("email.admin");
		if (StringUtils.isBlank(EMAIL_TO)) {
			EMAIL_TO = BaseSchedule.EMAIL_TO;
		}
		
		PROCESS_NAME = processName;
		MODULE = module;

	}
	
	protected void sendEmail(String subject, String message, String ref) {
		try {
			sendEmail.sendMail(subject, message, EMAIL_SENDER, EMAIL_TO, MODULE, ref);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendEmail(String subject, String message, String sendFrom, String sendTo, String module, String ref) {
		try {
			sendEmail.sendMail(subject, message, sendFrom, sendTo, module, ref);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendError(String message, String ref) {
		try {
			sendEmail.sendMail(BaseSchedule.ERROR_EMAIL_SUBJECT+" : "+PROCESS_NAME+" : "+BaseSchedule.now(), PROCESS_NAME+" "+message, EMAIL_SENDER, EMAIL_TO, MODULE, ref);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendError(String message, String ref, String country) {
		try {
			sendEmail.sendMail(BaseSchedule.ERROR_EMAIL_SUBJECT+" : "+PROCESS_NAME+" ["+ country +"] : " + BaseSchedule.now(), PROCESS_NAME+" "+message, EMAIL_SENDER, EMAIL_TO, MODULE, ref);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected void cleanUp(DataSource dataSources) {
		if(dataSources != null) {
			try {
				DatabaseUtil.closeDataSource(dataSources);
			} catch (SQLException e) {}
		}	
	}
	
	protected void cleanUp(DataSource[] dataSources) {
		for (DataSource ds: dataSources) {
			if(dataSources != null) {
				try {
					DatabaseUtil.closeDataSource(ds);
				} catch (SQLException e) {}
			}	
		}
	}

	public abstract void initialize();
	public abstract void start();
}
