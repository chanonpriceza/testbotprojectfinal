package botbackend.schedule;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.sql.DataSource;
import javax.swing.ImageIcon;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.byteSources.ByteSource;
import org.apache.sanselan.common.byteSources.ByteSourceArray;
import org.apache.sanselan.formats.jpeg.JpegImageParser;
import org.apache.sanselan.formats.jpeg.segments.UnknownSegment;
import org.json.simple.parser.ParseException;

import web.parser.image.DefaultImageParser;
import botbackend.utils.ACCPropertyParser;
import utils.BotUtil;
import utils.FilterUtil;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.api.LnwShopMerchantBean;
import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.bean.api.LnwShopProductDataBean.STATUS;
import botbackend.bean.api.LnwShopSendDataBean;
import botbackend.bean.api.LnwShopSendDataBean.ACTION;
import botbackend.db.api.LnwShopMerchantDB;
import botbackend.db.api.LnwShopProductDataDB;
import botbackend.db.api.LnwShopSendDataDB;
import botbackend.db.api.LnwShopUrlPatternDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.service.aws.SimpleEmail;

public class APILnwShopConvertToSendDataSchedule {
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser propConfig;
	private DataSource webDataSource;
	
	private String EMAIL_TO;
	private String PROCESS_NAME = "LnwShop_API_FetchSendData";
	private LnwShopMerchantDB merchantDB;
	private LnwShopProductDataDB productDB;
	private LnwShopSendDataDB sendDataDB;
	private LnwShopUrlPatternDB urlPatternDB;
	
	public static final int DEFAULT_START = 0;
	public static final int DEFAULT_OFFSET = 200;
	public static final int DEFAULT_WIDTH = 200;
	public static final int DEFAULT_HEIGHT = 200;
	public static final int DEFAULT_TIMEOUT = 10000;
	
	public static void main(String args []){
		APILnwShopConvertToSendDataSchedule process = null;
		try {			
			if(args.length == 0) {
				System.out.println("no configuration file argument");
				return;
			}
			process = new APILnwShopConvertToSendDataSchedule(args[0]);
			process.initailize();
			process.run();
		} catch (Exception e) {
			logger.error("APILnwShopConvertToSendDataSchedule Error", e);			
			e.printStackTrace();
		} finally {
			if(process != null) {
				process.cleanUp();	
			}					
		}       
	}

	public APILnwShopConvertToSendDataSchedule(String configFile) throws ConfigurationException{
			
		if(configFile == null || configFile.trim().length() == 0) {
			throw new ConfigurationException("no config file");
		}		
		propConfig = new ACCPropertyParser(configFile);		
	}
	
	public void cleanUp(){
		if(webDataSource != null) {
			try {
				DatabaseUtil.closeDataSource(webDataSource);
			} catch (SQLException e) {				
			}
		}	
		
	}
	
	public void initailize(){
		String file = propConfig.getString("log4j-config-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    String webDBDriverName = propConfig.getString("web.db.driver-name");
		String webDBURL = propConfig.getString("web.db.url");
		String webDBUserName = propConfig.getString("web.db.username");
		String webDBPassword = propConfig.getString("web.db.password");
		EMAIL_TO = propConfig.getString("email.admin");
	    webDataSource = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
	    productDB = new LnwShopProductDataDB(webDataSource);
	    merchantDB = new LnwShopMerchantDB(webDataSource);
	    sendDataDB = new LnwShopSendDataDB(webDataSource);
	    urlPatternDB = new LnwShopUrlPatternDB(webDataSource);
	    BotUtil.LOCALE = "TH";
	}
	
	public void run() throws SQLException, ParseException, IOException{
      try{
    	  	
    	  	logger.info("Start API LnwShop Convert To SendData process ...");	
    	  	getData();
    	  	logger.info("End API LnwShop Convert To SendData process.");	
		
      }catch (Exception e) {
    	 sendErrorEmail(e);
      }
	}
	
	public void getData() throws SQLException {
		try{
			List<LnwShopMerchantBean> merchantActiveList = merchantDB.findAllIdAndActive();
			
			MerchantLoop:
			for (LnwShopMerchantBean lnwShopMerchantBean : merchantActiveList) {
				if(lnwShopMerchantBean.getForceUpdateCat() == 1) {
					int updateDoneCount = productDB.updateAllProductStatusByStatus(lnwShopMerchantBean.getLnw_id(), "DONE", "UPDATE");
					logger.info(lnwShopMerchantBean.getLnw_id() + " set " + updateDoneCount + " products for Re-Sync");
					merchantDB.updateForceUpdateCat(lnwShopMerchantBean.getId(), 0);
					logger.info(lnwShopMerchantBean.getLnw_id() + " disable Re-Sync");
				}
				List<String> allowCategoryList = urlPatternDB.getValueByMerchantIdAction(lnwShopMerchantBean.getId(), "ALLOW");
				List<String> blockCategoryList = urlPatternDB.getValueByMerchantIdAction(lnwShopMerchantBean.getId(), "BLOCK");
				
				Map<String, Object[]> catMap = new HashMap<>();
				List<UrlPatternBean> lnwshopCatList = urlPatternDB.getByMerchantIdAction(lnwShopMerchantBean.getId(), "LNWSHOP");
				if(lnwshopCatList != null && lnwshopCatList.size() > 0) {
					for (UrlPatternBean catBean : lnwshopCatList) {
						catMap.put(catBean.getValue(), new Object[] {catBean.getCategoryId(), catBean.getKeyword()});
					}
				}
				
				List<String> nameList = new ArrayList<>();
				while(true){
					int countProductByMerchant = productDB.countProductByLnwMerchantAndStatus(lnwShopMerchantBean.getLnw_id());
					if(countProductByMerchant == 0) break;
					
					List<LnwShopProductDataBean> productList = productDB.getProductByLnwMerchantAndStatus(lnwShopMerchantBean.getLnw_id(), DEFAULT_START, DEFAULT_OFFSET);

					if(productList != null && productList.size() > 0){
						
						List<LnwShopSendDataBean> sendDataList = new ArrayList<LnwShopSendDataBean>();
						List<Integer> insertIdList = new ArrayList<Integer>();
						List<Integer> updateIdList = new ArrayList<Integer>();
						List<Integer> deleteIdList = new ArrayList<Integer>();
						List<Integer> rejectIdList = new ArrayList<Integer>();
						List<Integer> updateNameIdList = new ArrayList<Integer>();
						
						for (LnwShopProductDataBean productBean : productList) {
							
							if(allowCategoryList != null && allowCategoryList.size() > 0) {
								if(!allowCategoryList.contains(productBean.getCategory())) {
									rejectIdList.add(productBean.getId());
									logger.info(lnwShopMerchantBean.getId() + " Not Allow product id " + productBean.getId() + " from category " + productBean.getCategory());
									continue;
								}
							}
							
							if(blockCategoryList != null && blockCategoryList.size() > 0) {
								if(blockCategoryList.contains(productBean.getCategory())) {
									rejectIdList.add(productBean.getId());
									logger.info(lnwShopMerchantBean.getId() + " Block product id " + productBean.getId() + " from category " + productBean.getCategory());
									continue;
								}
							}
							
							byte[] imageByte = null;
							if(StringUtils.isNotBlank(productBean.getPicture_url())) {
				        		imageByte = parseImage(productBean.getPicture_url(), DEFAULT_WIDTH, DEFAULT_HEIGHT);
				        		if(imageByte == null) {
				        			System.out.println("cannot parse product image " + productBean.getPicture_url());
				        		}        	
				        	}
							
							LnwShopSendDataBean sdBean = null;
							if(productBean.getStatus().equals(STATUS.ADD.toString())){
								sdBean = LnwShopSendDataBean.createSendDataBean(productBean, lnwShopMerchantBean, imageByte, catMap, ACTION.ADD);
								if(sdBean != null){
									insertIdList.add(productBean.getId());
									sendDataList.add(sdBean); 
								}
							}else if(productBean.getStatus().equals(STATUS.UPDATE.toString())){
								if(StringUtils.isBlank(productBean.getOld_name())){
									sdBean = LnwShopSendDataBean.createSendDataBean(productBean, lnwShopMerchantBean, imageByte, catMap, ACTION.UPDATE); 
									if(sdBean != null){
										updateIdList.add(productBean.getId());
										sendDataList.add(sdBean); 
									}
								}else{
									updateNameIdList.add(productBean.getId());
									String oldName = productBean.getOld_name();
									String newName = productBean.getName();
									
									productBean.setName(oldName);
									LnwShopSendDataBean sdOldBean = LnwShopSendDataBean.createSendDataBean(productBean, lnwShopMerchantBean, imageByte, catMap, ACTION.DELETE);
									if(sdOldBean != null){
										sendDataList.add(sdOldBean);
									}
									
									productBean.setName(newName);
									sdBean = LnwShopSendDataBean.createSendDataBean(productBean, lnwShopMerchantBean, imageByte, catMap, ACTION.ADD);
									if(sdBean != null){
										sendDataList.add(sdBean);
									}
								}
							}else if(productBean.getStatus().equals(STATUS.DELETE.toString())){
								if(StringUtils.isNotBlank(productBean.getOld_name())){
									String oldName = productBean.getOld_name();
									productBean.setName(oldName);
								}
								sdBean = LnwShopSendDataBean.createSendDataBean(productBean, lnwShopMerchantBean, imageByte, catMap, ACTION.DELETE);
								if(sdBean != null){
									deleteIdList.add(productBean.getId());
									sendDataList.add(sdBean); 
								}
							}
							
							if(sdBean == null){
								rejectIdList.add(productBean.getId());
							}
							
							
							String checkName = productBean.getName();
							if(lnwShopMerchantBean.getAddParentName() == 1 && StringUtils.isNotBlank(productBean.getParent_product_name())){
								StringBuilder mixProductName = new StringBuilder();
								mixProductName.append(productBean.getParent_product_name());
								mixProductName.append(" ");
								mixProductName.append(FilterUtil.getStringAfter(checkName, productBean.getParent_product_name(), checkName));
								checkName = mixProductName.toString();
							}
							if(lnwShopMerchantBean.getAddProductId() == 1) {
								StringBuilder mixProductName = new StringBuilder();
								mixProductName.append(checkName);
								mixProductName.append(" (");
								mixProductName.append(productBean.getLnw_product_id());
								mixProductName.append(")");
								checkName = mixProductName.toString();
							}
							if(nameList.contains(checkName.toLowerCase())) {
								merchantDB.updateActiveNote(lnwShopMerchantBean.getId(), 7, "Duplicate product name, stop sync.");
								logger.info(lnwShopMerchantBean.getLnw_id() + " Duplicate product name, stop sync.");
								continue MerchantLoop;
							} else {
								nameList.add(checkName.toLowerCase());
							}
								
						}
						
						if(sendDataList != null && sendDataList.size() > 0) sendDataDB.insertSendDataBatch(sendDataList);
						if(insertIdList != null && insertIdList.size() > 0) productDB.updateStatusBatch(insertIdList, lnwShopMerchantBean.getLnw_id());
						if(updateIdList != null && updateIdList.size() > 0) productDB.updateStatusBatch(updateIdList, lnwShopMerchantBean.getLnw_id());
						if(deleteIdList != null && deleteIdList.size() > 0) productDB.deleteProductBatch(deleteIdList, lnwShopMerchantBean.getLnw_id());
						if(rejectIdList != null && rejectIdList.size() > 0) productDB.updateStatusBatch(rejectIdList, lnwShopMerchantBean.getLnw_id());
						if(updateNameIdList != null && updateNameIdList.size() > 0) productDB.clearOldDataBatch(updateNameIdList, lnwShopMerchantBean.getLnw_id());
						if(updateNameIdList != null && updateNameIdList.size() > 0) productDB.updateStatusBatch(updateNameIdList, lnwShopMerchantBean.getLnw_id());
						
					}else{
						break;
					}
				}
			}
			
		}catch(Exception e){
			logger.error("APILnwShopConvertToSendDataSchedule Error", e);
		}
	}
	
	public void sendErrorEmail(Exception e){
		 logger.error( PROCESS_NAME + " process Fail ", e);
			
			String accessKey = propConfig.getString("awsses.accesskey");
			String secretKey = propConfig.getString("awsses.secretkey");					
			SimpleEmail sEmail = new SimpleEmail(accessKey, secretKey);
			try {
				sEmail.sendMail(BaseSchedule.ERROR_EMAIL_SUBJECT + " : " + PROCESS_NAME + " : " + BaseSchedule.now(), 
						PROCESS_NAME + " " + e.toString(), 
						BaseSchedule.EMAIL_FROM, 
						EMAIL_TO, "APILnwShopConvertToSendDataSchedule", "sendErrorEmail");
			} catch (Exception e1) { 
				
			}  
		
	}

	// --------------------IMAGE UTILITY----------------------------
	
	private byte[] parseImage(String imageUrl, int width, int height) {
		URL url = null;
		HttpURLConnection conn = null;
		try {
			url = new URL(imageUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5");
			conn.setConnectTimeout(DEFAULT_TIMEOUT);
			conn.setReadTimeout(DEFAULT_TIMEOUT);
			String contentLength = conn.getHeaderField("Content-Length");
			int length = BotUtil.stringToInt(contentLength,0);

			if(length > 1000000)
				return null;

			try (InputStream inputStream = conn.getInputStream();
				ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
				
				int read = 0;
				byte[] bytes = new byte[20480];
				while ((read = inputStream.read(bytes)) != -1)
					byteOutputStream.write(bytes, 0, read);

				try (ImageInputStream imageStream = ImageIO.createImageInputStream(new ByteArrayInputStream(byteOutputStream.toByteArray()))) {
					Iterator<ImageReader> iter = ImageIO.getImageReaders(imageStream);
					BufferedImage image = null;
					String imageFormat = null;
					
					if (iter.hasNext()) {
						ImageReader reader = null;
						try {
							reader = iter.next();
							reader.setInput(imageStream);
							image = reader.read(0);
							imageFormat = reader.getFormatName();
						} catch (IIOException e) {
							try {
								WritableRaster raster = (WritableRaster) reader.readRaster(0, null);
								ICC_Profile profile = Sanselan.getICCProfile(byteOutputStream.toByteArray());

								checkAdobeMarker(byteOutputStream.toByteArray(), raster);

								image = convertCmykToRgb(raster, profile);
							} catch (ImageReadException e1) {
								System.out.print("Can't get ICC Profile : ");
							}
						} finally {
							if (reader!=null) {
								reader.dispose();
							}
						}
					}
					
					byte[] output;
					if (image != null) {
						byteOutputStream.reset();
						try {
							ImageIO.write(image, "jpg", byteOutputStream);
						} catch (IIOException e) {
							ImageIO.write(image, "png", byteOutputStream);
						}
					}
					output = parseImage(byteOutputStream.toByteArray(), width, height, imageFormat);
						
					return output;
				}
			} catch(FileNotFoundException e) {
				System.out.print("FileNotFoundException : ");
			} 
		} catch (Exception e) {
			BotUtil.consumeInputStreamQuietly(conn.getErrorStream());
			e.printStackTrace();
		} finally {
			if(conn != null)
	 			conn.disconnect();
		}

		return null;
	}

	private byte[] parseImage(byte[] imageByte, int width, int height, String imageFormat) {
		
		try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {

			ImageIcon ii = new ImageIcon(imageByte);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if (iWidth == -1 && iHeight == -1) {
				return null;
			}

			double xPos = (width - 1 * iWidth) / 2;
			double yPos = (height - 1 * iHeight) / 2;
			Image temp = null;

			if (iHeight > height || iWidth > width) {

				if (iWidth > iHeight) {
					resizedImage = i.getScaledInstance(width, (height * iHeight) / iWidth, Image.SCALE_SMOOTH);
				} else {
					resizedImage = i.getScaledInstance((width * iWidth) / iHeight, height, Image.SCALE_SMOOTH);
				}

				double xScale = (double) width / iWidth;
				double yScale = (double) height / iHeight;
				double scale = Math.min(xScale, yScale);

				// Calculate the center position of the panel -- with scale
				xPos = (width - scale * iWidth) / 2;
				yPos = (height - scale * iHeight) / 2;
				// This code ensures that all the pixels in the image are
				// loaded.
				temp = new ImageIcon(resizedImage).getImage();

			} else {
				temp = i;
			}

			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics g = bufferedImage.createGraphics();

			// Clear background and paint the image.
			g.setColor(Color.white);
			g.fillRect(0, 0, width, height);
			g.drawImage(temp, (int) xPos, (int) yPos, null);
			g.dispose();

			switch(imageFormat.toLowerCase()) {
				case "jpeg"	: ImageIO.write(bufferedImage, "jpg", outStream); break;
				case "jpg"	: ImageIO.write(bufferedImage, "jpg", outStream); break;
				case "png"	: ImageIO.write(bufferedImage, "png", outStream); break;
				default		: ImageIO.write(bufferedImage, "jpg", outStream); break;
			}
			
			outStream.flush();
			byte[] imageInByte = outStream.toByteArray();
			outStream.close();
			
			return imageInByte;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void checkAdobeMarker(byte[] imageByte, WritableRaster raster) {
		JpegImageParser imageParser = new JpegImageParser();
		ByteSource byteSource = new ByteSourceArray(imageByte);
		
		@SuppressWarnings("rawtypes")
		ArrayList segments = null;
		try {
			segments = imageParser.readSegments(byteSource, new int[] { 0xffee }, true);
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		}
		if (segments != null && segments.size() >= 1) {
			UnknownSegment app14Segment = (UnknownSegment) segments.get(0);
			byte[] segmentByte = app14Segment.bytes;
			if (segmentByte.length >= 12 && segmentByte[0] == 'A' && segmentByte[1] == 'd' && segmentByte[2] == 'o' && segmentByte[3] == 'b' && segmentByte[4] == 'e') {
				if ((app14Segment.bytes[11] & 0xff) == 2) {
					convertYcckToCmyk(raster);
				}
				convertInvertedColors(raster);
			}
		}
	}

	private void convertYcckToCmyk(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);

			for (int x = 0; x < stride; x += 4) {
				int y = pixelRow[x];
				int cb = pixelRow[x + 1];
				int cr = pixelRow[x + 2];

				int c = (int) (y + 1.402 * cr - 178.956);
				int m = (int) (y - 0.34414 * cb - 0.71414 * cr + 135.95984);
				y = (int) (y + 1.772 * cb - 226.316);

				if (c < 0) {
					c = 0;
				} else if (c > 255) {
					c = 255;
				}
					
				if (m < 0) {
					m = 0;
				} else if (m > 255) {
					m = 255;
				}
					
				if (y < 0) {
					y = 0;
				} else if (y > 255) {
					y = 255;
				}

				pixelRow[x] = 255 - c;
				pixelRow[x + 1] = 255 - m;
				pixelRow[x + 2] = 255 - y;
			}

			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private void convertInvertedColors(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);
			for (int x = 0; x < stride; x++) {
				pixelRow[x] = 255 - pixelRow[x];
			}
			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private BufferedImage convertCmykToRgb(Raster srcRaster, ICC_Profile profile) throws IOException {
		if (profile == null) {
			profile = ICC_Profile.getInstance(DefaultImageParser.class.getResourceAsStream("/ISOcoated_v2_300_eci.icc"));
		}

		if (profile.getProfileClass() != ICC_Profile.CLASS_DISPLAY) {
			byte[] profileData = profile.getData();

			if (profileData[ICC_Profile.icHdrRenderingIntent] == ICC_Profile.icPerceptual) {
				intToBigEndian(ICC_Profile.icSigDisplayClass, profileData, ICC_Profile.icHdrDeviceClass);
				
				profile = ICC_Profile.getInstance(profileData);
			}
		}

		BufferedImage resultImage = new BufferedImage(srcRaster.getWidth(), srcRaster.getHeight(), BufferedImage.TYPE_INT_RGB);
		WritableRaster resultRaster =  resultImage.getRaster();

		ICC_ColorSpace cmykCS = new ICC_ColorSpace(profile);
		ColorSpace rgbCS = resultImage.getColorModel().getColorSpace();

		ColorConvertOp cmykToRgb = new ColorConvertOp(cmykCS, rgbCS, null);
		cmykToRgb.filter(srcRaster, resultRaster);

		return resultImage;
	}

	private void intToBigEndian(int value, byte[] profileData, int index) {
		profileData[index] = (byte) (value >> 24);
		profileData[index + 1] = (byte) (value >> 16);
		profileData[index + 2] = (byte) (value >> 8);
		profileData[index + 3] = (byte) (value);
	}

	
}
