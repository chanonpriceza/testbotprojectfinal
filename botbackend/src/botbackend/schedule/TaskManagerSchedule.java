package botbackend.schedule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;
import botbackend.bean.BotConfigBean;
import botbackend.bean.BotQualityReportBean;
import botbackend.bean.ConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.TaskBean;
import botbackend.bean.TaskBean.PROCESS;
import botbackend.bean.TaskBean.STATUS;
import botbackend.bean.UserBean;
import botbackend.bean.api.LnwShopMerchantBean;
import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.db.BotConfigDB;
import botbackend.db.BotQualityReportHistoryDB;
import botbackend.db.ConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.TaskDB;
import botbackend.db.TaskDetailDB;
import botbackend.db.UserDB;
import botbackend.db.api.LnwShopMerchantDB;
import botbackend.db.api.LnwShopProductDataDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ACCPropertyParser;
import botbackend.utils.HttpCallbackHandler;
import botbackend.utils.Util;
import db.NotificationDB;

public class TaskManagerSchedule extends Schedule implements HttpCallbackHandler {
	Set<String> HolidayList;
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd");
	private ACCPropertyParser baseConfig;
	
	private static final int LIMIT_TASK_TO_PROCESS = 100;
	private String DEFAULT_CHARSET = "UTF-8";
	
	private final int defaultConnectTimeout = 1800000;
	private final int defaultReadTimeout = 1800000;
	private final int defaultReadBuffer = 10000;
	private final static String lnwshopServer = "Bot12-bot_lnwshop";
	private final String lineBreak = System.getProperty("line.separator");
	private final String OWNER = "SYSTEM";
	private BufferedWriter writer;
	private FileOutputStream os = null;
	private OutputStreamWriter osw = null;
	private Map<String, String> columnMap = null;
	private TaskDB taskDB = null;
	private TaskDetailDB taskDetailDB = null;
	private BotQualityReportHistoryDB qualityReportHisDB = null;
	private DataSource ds = null; 
	private Map<String,String> catList;
	private String FEED_DESTINATION_PATH;
	private int totalCol;
	private DataSource lnwShopDatasource = null;
	private LnwShopMerchantDB lnwShopMerchantDB = null;
	private LnwShopProductDataDB lnwshopProductDB = null;
	private MerchantConfigDB merchantConfig = null;
	private MerchantDB merchantDB = null;
	private BotConfigDB botConfigDB;
	private ConfigDB configDB;
	private UserDB userDB;
	private NotificationDB notiDB;
	
	private Map<String, Integer> userMap = null;
	
	public static void main(String[] args) {
		Schedule process = null;
		try {
			if(args.length != 1) {
			    System.out.println("Wrong configuration file argument,<base-config>");
			    return;
			} 			
			process = new TaskManagerSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {			
			logger.error("TaskManagerSchedule Error", e);
			e.printStackTrace();
		}
	}
	
	public TaskManagerSchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
	
		baseConfig = new ACCPropertyParser(configFile[0]);
	}
	
	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-task-manager-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    setUpEmail(baseConfig, "TaskManagerSchedule", "TASK_MANAGER");
	    FEED_DESTINATION_PATH = baseConfig.getString("feed.destination.path");
	}

	@Override
	public void start() {
		List<String> countryList = baseConfig.getList("task-manager-country-list");
		HolidayList = new HashSet<>(baseConfig.getList("priceza-holiday-list-2018"));		
		for (String country: countryList) {
			
			ds = getBotBackendDataSource(baseConfig, country.toLowerCase());
			botConfigDB = new BotConfigDB(ds);
			taskDB = new TaskDB(ds);
			taskDetailDB = new TaskDetailDB(ds);
			qualityReportHisDB = new BotQualityReportHistoryDB(ds);
			merchantConfig = new MerchantConfigDB(ds);
			merchantDB = new MerchantDB(ds);
			configDB = new ConfigDB(ds);
			userDB = new UserDB(ds);
			notiDB = new NotificationDB(ds);
			
			if(country.toLowerCase().equals("th")) {
				lnwShopDatasource = createLnwShopDataSource();
				lnwShopMerchantDB = new LnwShopMerchantDB(lnwShopDatasource);
				lnwshopProductDB = new LnwShopProductDataDB(lnwShopDatasource);
			}
			
			userMap = userNameIdMapping();
			
			List<TaskBean> taskBeanList;
			try {
				taskBeanList = taskDB.getTaskData(null, null, STATUS.WAITING.toString(), null, -1, 0, LIMIT_TASK_TO_PROCESS);
				if(taskBeanList != null && taskBeanList.size() > 0) {
					for (TaskBean taskBean : taskBeanList) {
						try {
//							taskBean.setStatus(STATUS.DOING.toString());

							if(taskBean.getProcess().equals(PROCESS.feedCatList.toString())) {
//								processFeedCatList(country.toLowerCase(), taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.feedCountProduct.toString())) {
//								processFeedCountProduct(country.toLowerCase(), taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.feedForTestParser.toString())) {
//								processFeedForTestParser(country.toLowerCase(), taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.checkMissingProduct.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.checkExceedProduct.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.checkMissingPic.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.checkPriceNotMatch.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.updateDesc.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.updateRealProductId.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.reCrawling.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.botQualityDownload.toString())) {
								processBotQualityDownload(country.toLowerCase(), taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.merchantFeedParser.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.merchantParser.toString())) {
								// no schedule for now
							}else if(taskBean.getProcess().equals(PROCESS.newMerchantShopee.toString())) {
//								processDistributedShopee(taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.newMerchantLnwshop.toString())) {
//								processAddNewLnwShopMerchant(taskBean);
							}else if(taskBean.getProcess().equals(PROCESS.getSupportPerformanceReport.toString())){
//								processPerformanceReport(taskBean);
							}
							else if(taskBean.getProcess().equals(PROCESS.others.toString())) {
								// no schedule for now
							}
					
						} catch(Exception e) {
							taskDB.updateTaskStatus(STATUS.AUTOREPLY.toString(), taskBean.getId());
							logger.error("Task Manager Fail: " + country +" at task id " + taskBean.getId(), e);
							sendError(e.toString(), "Task Manager Fail");
							
							e.printStackTrace();
						} 
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				cleanUp(ds);
				cleanUp(lnwShopDatasource);
			}
		}
	}

//	###############################################################
//	###### Main Process Method ####################################
//	###############################################################
	
	@SuppressWarnings("unchecked")
	private void processFeedCatList(String country, TaskBean taskBean) throws Exception{ // change to use add comment method
		
		int taskId = taskBean.getId();

		if(taskDB == null || taskDetailDB == null){
			return;
		}
				
		columnMap = new HashMap<String, String>();
		int countProduct = 0;
		int limit = 0;
		int pageoffset = 0;
		boolean doPage = false;
		int ownerId = userMap.get(taskBean.getOwner());
		
		JSONObject json = (JSONObject)new JSONParser().parse(taskBean.getData().toString());
		String urlLink = (String) json.get("urlLink");
		String fileType = (String) json.get("feedType");
		String pageParam = (String) json.get("pageParam");
		String limitPage = (String) json.get("limitPage");
		String pageOffset = (String) json.get("pageOffset");					
		String productTag = (String) json.get("productTag");
		String tags = (String) json.get("tag");
		String actionType =  taskBean.getProcess();
		String feedFile = actionType + "_" +taskId   + ".csv";
		
		if(StringUtils.isNotBlank(limitPage)&& StringUtils.isNumeric(limitPage)){
			limit =  Integer.parseInt(limitPage);
			if(StringUtils.isNotBlank(pageOffset)&& StringUtils.isNumeric(pageOffset)){
				pageoffset = Integer.parseInt(pageOffset);
			}

			if(limit > 0){
				doPage = true;
			}
		}

		if(StringUtils.isNotBlank(fileType) && StringUtils.isNotBlank(urlLink)){
				String inputFile = "inputFeedFile_" + taskId + "." + fileType;
				writeFeedInputFile(inputFile, fileType, urlLink, doPage, pageParam, limit, pageoffset);
				
				File file = new File(FEED_DESTINATION_PATH + inputFile);
				if(file.exists()){
					if(StringUtils.isNotBlank(tags)){
						String[] tagList = tags.split("\\|");
						if(tagList.length == 0) { // remove after patch
							tagList = tags.split(",");
						}
						if (tagList != null && tagList.length > 0) {
							for (String tag : tagList) {
								if("xml".equals(fileType)){
									String colName = "";
									colName = Util.getStringBetween(tag, "<", ">");
									columnMap.put(colName, tag);
								
								}else{
									columnMap.put(tag, tag);
								}
							}
						}
					}
		
				JSONObject comment = new JSONObject();
				if(columnMap != null && columnMap.size() > 0 ){
					taskDB.updateTaskStatus(STATUS.DOING.toString(), taskId);
					
					file = new File(FEED_DESTINATION_PATH + feedFile);
					FileWriterWithEncoding fw = new FileWriterWithEncoding(file, "UTF-8");
					writer = new BufferedWriter(fw);
					
					totalCol  =  columnMap.size();
					String headerLine  = "\"";
					int countHeaderCol  = 0;
					
					for(Entry<String, String> colName : columnMap.entrySet()){
						countHeaderCol++;
						headerLine  = headerLine +  colName.getKey();
						if(countHeaderCol != totalCol){
							headerLine = headerLine +  "\",\"";
						}
					}
					headerLine = headerLine  +  "\"" + lineBreak;
					writer.write(headerLine);
		
			
					listCatFromFile(FEED_DESTINATION_PATH + inputFile, DEFAULT_CHARSET, productTag, fileType);
			
					json.put("fileName", feedFile);
					json.put("inputFile", inputFile);
				
					int update  =  taskDB.updateTaskData(json.toJSONString(), STATUS.AUTOREPLY.toString(), taskBean.getId());
					if(update > 0){
						comment.put("fileName", feedFile);
						comment.put("message", "process completed");
						taskDetailDB.insertComment(taskId, comment.toString(), STATUS.AUTOREPLY.toString(), OWNER);
						logger.info("Task Manager --> ProcessFeedCatList successfully generated : " + urlLink + " |Action : "+ actionType + "|Count: " + countProduct);
					}else{
						logger.info("Task Manager --> ProcessFeedCatList error generated : " + urlLink + " |Action : "+ actionType + "|Count: " + countProduct);
					}
					
					if(writer != null){ writer.close();}
					if(fw != null) {fw.close();}
				}
			}else{
				logger.error("Task Manager -- > ProcessFeedCatList Fail : cannot found  " + inputFile + " " + country +" at task id " + taskBean.getId());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processFeedCountProduct(String country, TaskBean taskBean) throws Exception{ 

		if(taskDB == null || taskDetailDB == null){
			return;
		}
		
		int taskId = taskBean.getId();
		int countProduct = 0;
		int limit = 0;
		int pageoffset = 0;
		boolean doPage = false;
		int ownerId = userMap.get(taskBean.getOwner());
		
		JSONObject json = (JSONObject)new JSONParser().parse(taskBean.getData().toString());
		String urlLink = (String) json.get("urlLink");
		String fileType = (String) json.get("feedType");
		String pageParam = (String) json.get("pageParam");
		String limitPage = (String) json.get("limitPage");
		String pageOffset = (String) json.get("pageOffset");					
		String productTag = (String) json.get("productTag");
		String actionType = taskBean.getProcess();
		
		if(StringUtils.isNotBlank(limitPage)&& StringUtils.isNumeric(limitPage)){
			limit =  Integer.parseInt(limitPage);
			if(StringUtils.isNotBlank(pageOffset)&& StringUtils.isNumeric(pageOffset)){
				pageoffset = Integer.parseInt(pageOffset);
			}

			if(limit > 0){
				doPage = true;
			}
		}

		if(StringUtils.isNotBlank(fileType) && StringUtils.isNotBlank(urlLink)){
			String inputFile = "inputFeedFile_" + taskId + "." + fileType;
			writeFeedInputFile(inputFile, fileType, urlLink, doPage, pageParam, limit, pageoffset);
			
			JSONObject comment = new JSONObject();
			File file = new File(FEED_DESTINATION_PATH + inputFile);
			if(file.exists()){
				taskDB.updateTaskStatus(STATUS.DOING.toString(), taskId);
				countProduct = countProductFromFile(FEED_DESTINATION_PATH + inputFile, DEFAULT_CHARSET, productTag, fileType);

				json.put("inputFile", inputFile);
				
				int update  =  taskDB.updateTaskData(json.toJSONString(), STATUS.AUTOREPLY.toString(), taskId);
				if(update > 0){
					comment.put("message", "product count : " +countProduct);
					taskDetailDB.insertComment(taskId, comment.toString(), STATUS.AUTOREPLY.toString(), OWNER);
					logger.info("Task Manager -- > processFeedCountProduct successfully generated : " + urlLink + " |Action : "+ actionType + "|Count: " + countProduct);
				}else{
					logger.info("Task Manager -- > processFeedCountProduct error generated : " + urlLink + " |Action : "+ actionType + "|Count: " + countProduct);
				}
				
			}else{
				logger.error("Task Manager -- > processFeedCountProduct  : cannot found  " + inputFile + " " + country +" at task id " + taskBean.getId());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processFeedForTestParser(String country, TaskBean taskBean) throws Exception{
		
		if(taskDB == null || taskDetailDB == null){
			return;
		}
		int taskId = taskBean.getId();
		int ownerId = userMap.get(taskBean.getOwner());
		String actionType =  taskBean.getProcess();
		
		taskDB.updateTaskStatus(STATUS.DOING.toString(), taskId);
		JSONObject json = (JSONObject)new JSONParser().parse(taskBean.getData().toString());
		String urlLink = (String) json.get("urlLink");
		String feedUsername = (String) json.get("feedUsername");
		String feedPassword = (String) json.get("feedPassword");
		String chkGz = (String) json.get("chkGz");
		String fileType = (String) json.get("feedType");
		
		if(StringUtils.isNotBlank(fileType) && StringUtils.isNotBlank(urlLink)){
			
			String fileName = "inputFeedFile_" + taskId + "." + fileType;
			
			File file = new File(FEED_DESTINATION_PATH + fileName);
			FileWriterWithEncoding fw = new FileWriterWithEncoding(file, "UTF-8");
			writer = new BufferedWriter(fw);
			
			boolean hasError = false;
			boolean isGz = false;
			
			if(StringUtils.isNotBlank(chkGz)&& StringUtils.isNumeric(chkGz)){
				if(Integer.parseInt(chkGz) == 1){
					isGz = true;
				}
			}
			JSONObject comment = new JSONObject();
			try{
				if(isGz){
					Util.httpRequestContentGzip(urlLink, feedUsername, feedPassword, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout);
				}else{
					Util.httpRequest(urlLink, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout, defaultReadBuffer);
				}
			}catch(Exception e){
				hasError = true;
				e.printStackTrace();
			}
			
			int update = 0;
			if(!hasError){
				json.put("fileName", fileName);
				comment.put("message", "generate file successfully");
				update = taskDB.updateTaskData(json.toJSONString(), STATUS.AUTOREPLY.toString(), taskId);
			}else{
				comment.put("message", "generate file fail");
			}
			
			taskDetailDB.insertComment(taskId, comment.toString(), STATUS.AUTOREPLY.toString(), OWNER);
			if(update > 0){
				logger.info("Task Manager -- > processFeedForTestParser successfully generated : " + urlLink + " |Action : "+ actionType + "|MerchantId : " + taskBean.getMerchantId());
			}else{
				logger.info("Task Manager -- > processFeedForTestParser error generated : " + urlLink + " |Action : "+ actionType + "|MerchantId : " + taskBean.getMerchantId());
			}
			
			if(writer != null){ writer.close();}
			if(fw != null) {fw.close();}
			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processBotQualityDownload(String country, TaskBean taskBean) throws Exception{
		
		if(taskDB == null || taskDetailDB == null){
			return;
		}
			
		if(qualityReportHisDB == null){
			qualityReportHisDB  = new BotQualityReportHistoryDB(ds);
		}
		
		int taskId = taskBean.getId();
		int ownerId = userMap.get(taskBean.getOwner());
		taskDB.updateTaskStatus(STATUS.DOING.toString(), taskId);
		
		JSONObject json = (JSONObject)new JSONParser().parse(taskBean.getData().toString());
		JSONObject comment = new JSONObject();	    
		
		String dateString = (String) json.get("date");
		if(StringUtils.isBlank(dateString)){
			comment.put("message", "Cannot find date param");
		}else{
			
			Date date = formatter.parse(dateString);  
			List<BotQualityReportBean> result = qualityReportHisDB.getReportByDate(date);
			if(result == null || result.size() == 0){
				comment.put("message", "don't have report at "+ date);
			}
			
			if(comment.get("message") == null){
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				String fileName = "qulityReport-" + cal.get(Calendar.YEAR) +"-" +cal.get(Calendar.MONTH)+"-" +cal.get(Calendar.DATE)+ "("+ taskBean.getId()+").csv";
				boolean success = writeQualityReportCsvFile(fileName , result);
				
				if(success){
					comment.put("fileName", fileName);
					comment.put("message", "Process completed");
				}else{
					comment.put("message", "Cannot Create File");
				}
			}
		}
		
		taskDB.updateTaskStatus(STATUS.AUTOREPLY.toString(), taskId);
		taskDetailDB.insertComment(taskId, comment.toString(), STATUS.AUTOREPLY.toString(), OWNER);
		logger.info("Task Manager -- > processBotQualityDownload : "+ comment.get("message"));
	}
	
	private void processDistributedShopee(TaskBean task) throws Exception {
		JSONObject data = (JSONObject) new JSONParser().parse(task.getData().toString());
		String shopeeIdString = (String) data.get("shopeeID");
		String merchantIdString = String.valueOf(data.get("merchantId"));
		
		int ownerId = userMap.get(task.getOwner());
		
		task.setOwner(OWNER);
		task.setStatus(TaskBean.STATUS.AUTOREPLY.toString());
		String comment = "";
		if(StringUtils.isBlank(shopeeIdString)||StringUtils.isBlank(merchantIdString)){
			logger.info("not found merchant: " + shopeeIdString);
			comment = "กรอก Id ไม่ครบครับกรุณาสร้าง task ใหม่";
			addComment(ownerId, task, comment);
			return;
		}
		int merchantId = Util.convertStringToInt(merchantIdString,0);
		int shopeeId = Util.convertStringToInt(shopeeIdString,0);
		
		MerchantBean merchantBean = merchantDB.getMerchantByMerchantId(merchantId);
		if(merchantBean==null){
			comment = "ไม่พบร้านค้าใน DB";
			logger.info("not found merchant: " + merchantId);
			addComment(ownerId, task, comment);
			return;
		}
		List<MerchantConfigBean> merchantConfigList = new ArrayList<>();
		merchantConfig.deleteMerchantConfigByMerchantId(merchantId);
		for (int i = 0; i < 3; i++) {
			MerchantConfigBean dummy = new MerchantConfigBean();
			dummy.setMerchantId(merchantId);
			switch (i) {
			case 0:
				dummy.setField("feedCrawlerClass");
				dummy.setValue("com.ha.bot.crawler.feed.impl.TemplateShopeeDistributedFeedCrawler");
				break;
			case 1:
				dummy.setField("parserClass");
				dummy.setValue("com.ha.bot.parser.DefaultFeedHTMLParser");
				break;
			case 2:
				dummy.setField("feedCrawlerParam");
				dummy.setValue(String.valueOf(shopeeId));
				break;

			default:
				break;
			}
			merchantConfigList.add(dummy);
		}
		ConfigBean shoppeeServerBean = configDB.getByName("taskmanager.shopee.default.server");
		if(shoppeeServerBean==null||StringUtils.isBlank(shoppeeServerBean.getConfigValue())){
			logger.info("Cant find Config Shopee Server");
			return;
		}
		String shopeeSeverName = shoppeeServerBean.getConfigValue();
		String shopeeDataServerName = "";
		String[] shopeeServerList = shopeeSeverName.split("|");
		if(shopeeServerList.length > 1){
			shopeeSeverName = shopeeServerList[0];
			shopeeDataServerName = shopeeServerList[1];
		}else{
			logger.info("Shopee Server config not complete !");
			return;
		}
		merchantBean.setServer(shopeeSeverName);
		merchantBean.setDataServer(shopeeDataServerName);
		merchantBean.setActive(1);
		merchantDB.updateNewMerchant(merchantBean);
		merchantDB.setCurrentserver(shopeeSeverName);
		merchantConfig.insertMerchantConfigList(merchantConfigList);
		comment = "จะเป็นปรับข้อมูลแล้ว รอรันตามรอบ";
		addComment(ownerId, task, comment);
	}
	
	private void processAddNewLnwShopMerchant(TaskBean task) throws Exception {
		int count = 0;
		int merchantId = task.getMerchantId();
		int ownerId = userMap.get(task.getOwner());
		
		task.setOwner(OWNER);
		taskDB.updateTaskStatus(STATUS.DOING.toString(), task.getId());
		JSONObject json = (JSONObject) new JSONParser().parse(task.getData().toString());
		String domain = getPlainMerchantName((String) json.get("DomainName"));
		LnwShopMerchantBean lnwBean = lnwShopMerchantDB.findMerchantByDomain(domain);
		String comment = "";
		if (lnwBean == null) {
			logger.info("not found merchant: " + merchantId);
			comment = "ไม่พบร้าน";
			addComment(ownerId, task, comment);
			return;
		}
		if (lnwBean.getActive() == 1 || lnwBean.getId() != 0) {
			logger.info("This merchant id: " + merchantId + " alredy active !! ");
			comment = "ร้านนี้ Active ไปแล้ว";
			addComment(ownerId, task, comment);
			return;
		}
		String inw_id = lnwBean.getLnw_id();
		int start = 0;
		boolean hasProduct = false;
		boolean addParrent = false;
		while (hasProduct == false || addParrent == false) {
			List<LnwShopProductDataBean> productdata = lnwshopProductDB.getProductByLnwMerchantAndStatus(inw_id, start, LIMIT_TASK_TO_PROCESS); //list LnwShopProductData
			if (productdata.size() > 0 && hasProduct == false) {
				hasProduct = true;
			}
			if (productdata == null || productdata.size() == 0) {
				break;
			}
			for (LnwShopProductDataBean data : productdata) { // for update product
				String parentName = data.getParent_product_name();
				String name = data.getName();
				if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(parentName) && !name.contains(parentName) && addParrent == false) { //check add parrent
					addParrent = true;
					break;

				}
			}
			start += LIMIT_TASK_TO_PROCESS;
		}
		if (hasProduct) {
			count = lnwshopProductDB.countProductByLnwMerchantAndStatus(inw_id);
			comment = "อัพเดทได้จำนวน " + count + " ชิ้น";
			lnwBean.setActive(1);
			lnwBean.setId(merchantId);
			if (addParrent) {
				lnwBean.setAddParentName(1);
			}
			lnwShopMerchantDB.activeByDomain(lnwBean, domain);
		} else {
			comment = "ไม่พบ Product";
		}
		addComment(ownerId, task, comment);
		
	}

	@SuppressWarnings("unchecked")
	private void processPerformanceReport(TaskBean task) {
		try {
			String jsonString = task.getData();

			if (StringUtils.isBlank(jsonString)) {
				logger.info("Cant Find Data from task ");
				return;
			}

			JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonString);
			String year = (String) jsonObject.get("yearReport");
			String quarter = (String) jsonObject.get("quarter");

			if (StringUtils.isBlank(year) || StringUtils.isBlank(quarter)) {
				logger.info("Cant Parse Data from json ");
				return;
			}
			String[] dueDate = genMerchantReportDueDate(year, quarter);
			if (dueDate.length != 2) {
				logger.info("dueDate is Not Macth !!");
				return;
			}
			List<TaskBean> taskDoneBeanList;
			String inputStartDate = dueDate[0];
			String inputEndDate = dueDate[1];
			String FileName = "task-report-quarter"+quarter+"-year"+year+".txt";
			String filePath = baseConfig.getString("upload-file-path");
			File file = new File(filePath+FileName);
			FileWriterWithEncoding fw = new FileWriterWithEncoding(file, "UTF-8");
			writer = new BufferedWriter(fw);
			taskDoneBeanList = taskDB.getTaskDataBetweenDate(inputStartDate, inputEndDate, STATUS.DONE.toString());
			writeLine(fw, "Quater: " + quarter + " StartDate: " + inputStartDate + " endDate: " + inputEndDate);
			List<TaskBean> onDate = new ArrayList<>();
			List<TaskBean> outDate = new ArrayList<>();
			List<TaskBean> outDateCustomer = new ArrayList<>();
			int countOutDateCustomer = 0;
			
			for (TaskBean taskBean : taskDoneBeanList) {
				try {
					
					long diffDate = taskBean.getWorktime();
					boolean isCustomer = "customer".equals(taskBean.getIssueType());
					if (isCustomer) {
						taskBean.setIsCustomer(isCustomer);
						countOutDateCustomer++;
					}

					taskBean.setTotalTime(diffDate);
					if (diffDate > 5) {
						outDate.add(taskBean);
						if (taskBean.isCustomer()) {
							outDateCustomer.add(taskBean);
						}
					} else {
						onDate.add(taskBean);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			writeLine(fw, "All issue : " + (onDate.size() + outDate.size()));
			writeLine(fw, "All Done in 5 day : " + onDate.size());
			writeLine(fw, "");
			writeLine(fw, "Issue which worktime more than 5 day");
			for (TaskBean t : outDate) {
				writeLine(fw, "-TOTAL-DATE: " + t.getTotalTime() + " | ID: " + t.getId() + " MERCHANTID: " + t.getMerchantId() + " NAME: " + t.getIssue());
			}
			writeLine(fw, "----------------------------------");
			writeLine(fw, "Customer issue	: " + countOutDateCustomer);
			writeLine(fw, "Customer done in 5 day	" + (countOutDateCustomer - outDateCustomer.size()));
			if (outDateCustomer.size() > 0) {
				writeLine(fw, "Issue which worktime more than 5 day");

				for (TaskBean t : outDateCustomer) {
					writeLine(fw, "-TOTAL-DATE: " + t.getTotalTime() + " | ID: " + t.getId() + " MERCHANTID: " + t.getMerchantId() + " NAME: " + t.getIssue());
				}
				writeLine(fw, "------------------------------------------------------------------------------------------------------------------------------------------------------");
			} else {
				writeLine(fw, "none");
			}
			fw.close();
			jsonObject.put("fileName", FileName);
			JSONObject commentObject = new JSONObject();
			commentObject.put("fileName", FileName);
			commentObject.put("message", "Download Here");
			taskDetailDB.insertComment(task.getId(), commentObject.toJSONString(), STATUS.AUTOREPLY.toString(), OWNER);
			taskDB.updateTaskStatus(STATUS.DONE.toString(), task.getId());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	###############################################################
//	###### Other Facility Method ##################################
//	###############################################################
	
	public void writeFeedInputFile(String inputFile, String fileType,  String urlLink, boolean doPage, String  pageParam,int  limit, int pageoffset) throws FileNotFoundException, UnsupportedEncodingException{
		os = new FileOutputStream(FEED_DESTINATION_PATH  +inputFile, false);
		osw = new OutputStreamWriter(os, "UTF-8");
		writer = new BufferedWriter(osw);
		try {

			if("xml".equals(fileType)){
				  if(doPage){
					  int pageNum = 1;
					  while(pageNum <= limit){
						  String link = urlLink + pageParam + pageNum;
						  Util.httpRequest(link, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout, defaultReadBuffer);
						  logger.info("Crawl Url Link : " + link );	
						  
						  if(pageoffset != 0){
							  pageNum = pageNum + pageoffset;
						  }else{
							  pageNum++;
						  }
					  }
				   }else{
					   	  Util.httpRequest(urlLink, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout, defaultReadBuffer);
				    	  logger.info("Crawl Url Link : " + urlLink );		
				   }
		
			}else if("json".equals(fileType)){
				if(doPage){
					  int pageNum = 1;
					  while(pageNum <= limit){
						  String link = urlLink + pageParam + pageNum;
						  writer.write("<page>"+lineBreak);
						  Util.httpRequest(link, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout, defaultReadBuffer);  
						  writer.write("</page>"+lineBreak);
						  logger.info("Crawl Url Link : " + link );	
						  
						  if(pageoffset != 0){
							  pageNum = pageNum + pageoffset;
						  }else{
							  pageNum++;
						  }	  
					  }
				         
				   }else{
					   	  writer.write("<page>"+lineBreak);
						  Util.httpRequest(urlLink, DEFAULT_CHARSET, true, this, defaultConnectTimeout, defaultReadTimeout, defaultReadBuffer);  
						  writer.write("</page>"+lineBreak);
				    	  logger.info("Crawl Url Link : " + urlLink );	
				   }
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(writer != null) { try { writer.close(); } catch (IOException e) {} }
			if(osw != null) { try { osw.close(); } catch (IOException e) {} }
			if(os != null) { try { os.close(); } catch (IOException e) {} }
		}
	}
	
	public int countProductFromFile(String fileName, String charsetName, String productTag, String fileType) {
		
		FileInputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		int countProduct = 0;
		try {			
			is = new FileInputStream(fileName);
			isr = new InputStreamReader(is, charsetName);
			br = new BufferedReader(isr);
			String line = null;
			String tmp = "";
			while ((line = br.readLine()) != null) {	
				tmp = tmp + line;
				if("xml".equals(fileType)){
					List<String>  productdataList = Util.getAllStringBetween(tmp, productTag, productTag.replace("<", "</"));
					if(productdataList != null && productdataList.size() > 0) {			
						tmp = Util.getStringAfterLastIndex(tmp, productTag.replace("<", "</"), "");
						countProduct += productdataList.size();
					}
				}else if("json".equals(fileType)){
					String  productdata= Util.getStringBetween(tmp, "<page>", "</page>");
					if(StringUtils.isNotBlank(productdata)) {			
						tmp = Util.getStringAfterLastIndex(tmp, "</page>", "");
						JSONObject jData = null;
						JSONArray jarr = null;
						if(StringUtils.isNotBlank(productTag)){
							jData = (JSONObject)new JSONParser().parse(productdata);
							jarr = (JSONArray)jData.get(productTag);
						}else{
							jarr = (JSONArray) new JSONParser().parse(productdata);
						}
						
						countProduct += jarr.size();
						generateJSONFeed(productdata, productTag);
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			if(br != null) { try { br.close(); } catch (IOException e) {} }
			if(isr != null) { try { isr.close(); } catch (IOException e) {} }
			if(is != null) { try { is.close(); } catch (IOException e) {} }
		}
		
		return countProduct;
	}
	
	public void  listCatFromFile(String fileName, String charsetName, String productTag, String fileType) {
		
		FileInputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		String tmp = "";
		try {			
			is = new FileInputStream(fileName);
			isr = new InputStreamReader(is, charsetName);
			br = new BufferedReader(isr);
			String line = null;
			catList = new HashMap<>();
			while ((line = br.readLine()) != null) {	
				tmp = tmp + line;
				if("xml".equals(fileType)){
					List<String>  productdataList = Util.getAllStringBetween(tmp, productTag, productTag.replace("<", "</"));
					if(productdataList != null && productdataList.size() > 0) {			
						tmp = Util.getStringAfterLastIndex(tmp, productTag.replace("<", "</"), "");
						for (String data : productdataList) {
							generateXMLFeed(data);
						}
					}
				}else if("json".equals(fileType)){
					String  productdata= Util.getStringBetween(tmp, "<page>", "</page>");
					if(StringUtils.isNotBlank(productdata)) {			
						tmp = Util.getStringAfterLastIndex(tmp, "</page>", "");
						generateJSONFeed(productdata, productTag);
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) { try { br.close(); } catch (IOException e) {} }
			if(isr != null) { try { isr.close(); } catch (IOException e) {} }
			if(is != null) { try { is.close(); } catch (IOException e) {} }
		}		
	}
	
	public void generateXMLFeed(String data){
		try {
			if (StringUtils.isNotBlank(data)) {
				String line = "\"";
				int countColumn = 0;
				for (Entry<String, String> col : columnMap.entrySet()) {
					countColumn++;
					String starTag = col.getValue();
					String endTag = col.getValue().replace("<", "</");
					String value  = ((StringUtils.isNotBlank(removeCDATATag(Util.getStringBetween(data, starTag, endTag))) ? removeCDATATag(Util.getStringBetween(data, starTag, endTag)) : ""));
					line = line + value;
					if (countColumn != totalCol) {
						line = line + "\",\"";
					}
				}
				line = line + "\"" + lineBreak;
				if(catList.get(line) == null){
					catList.put(line, "");
					writer.write(line);
				}
			}
		} catch (IOException e) {
			logger.error("CountAndListFeedSchedule Error", e);	
			e.printStackTrace();
		}
	}
	
	public void generateJSONFeed(String data, String productTag){
		try {
			JSONObject jData = null;
			JSONArray jarr = null;
			if(StringUtils.isNotBlank(productTag)){
				jData = (JSONObject)new JSONParser().parse(data);
				jarr = (JSONArray)jData.get(productTag);
			}else{
				jarr = (JSONArray) new JSONParser().parse(data);
			}
			if (jarr == null || jarr.size() == 0) {
				return;
			}

//			countProduct += jarr.size();
			Map<String,String> catList = new HashMap<>();
			for (Object obj : jarr) {
				JSONObject jObj = (JSONObject) obj;

				String line = "\"";
				int countColumn = 0;
				for (Entry<String, String> col : columnMap.entrySet()) {
					countColumn++;
					String field = col.getValue();
					line = line + ((jObj.get(field) != null ) ? jObj.get(field) : "");
	
					if (countColumn != totalCol) {
						line = line + "\",\"";
					}
				}
				if(catList.get(line) == null){
					catList.put(line, "");
					line = line + "\"" + lineBreak;
					writer.write(line);
				}
			}
			
		} catch (IOException e) {
			logger.error("CountAndListFeedSchedule Error", e);	
			e.printStackTrace();
		} catch (ParseException e1) {
			logger.error("CountAndListFeedSchedule Error", e1);	
			e1.printStackTrace();
		}
	}
	
	private boolean writeQualityReportCsvFile(String fileName, List<BotQualityReportBean> results){
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";
		final String FILE_HEADER = "id,type,merchantId,name,status,server,time,allCount";

		FileWriter fileWriter = null;

		String filePath = FEED_DESTINATION_PATH + fileName;
		
		boolean success = false;
		try{
			fileWriter = new FileWriter(filePath);
			
			fileWriter.append(FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);

			int i = 1;
			for(BotQualityReportBean result : results){
				fileWriter.append(String.valueOf(i));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(result.getType());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(result.getMerchantId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(result.getName());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(result.getStatus());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(result.getServer());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(result.getTime());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(result.getAllCount()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(NEW_LINE_SEPARATOR);
				i++;
			}
			fileWriter.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fileWriter != null) {
				try {
					fileWriter.close();
					success = true;
				} catch (IOException e) {}
			}
		}
		
		return success;
	}
	
	public String removeCDATATag(String value){
		if(StringUtils.isNotBlank(value)){
			value = value.replace("<![CDATA[", "").replace("]]>", "");
		}
		return value;
	}
	
	@Override
	public void processLine(String line) {		
		try {
			writer.write(line);
			writer.flush();      
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	@SuppressWarnings("unchecked")
	public void addComment(int ownerId, TaskBean task,String comment){
		try {
			JSONObject jsonComment = new JSONObject();
			jsonComment.put("message",comment);
			if(taskDetailDB!=null)
			taskDetailDB.insertComment(task.getId(), jsonComment.toJSONString(), task.getStatus(), task.getOwner());
			taskDB.updateTaskStatus(STATUS.DOING.toString(), task.getId());
		} catch (SQLException e) {	
			e.printStackTrace();
		}
	}
	
	private void writeLine(FileWriterWithEncoding fw,String text) throws IOException{
		System.out.println(text);
		fw.write(text+lineBreak);
	}
	
	private String getPlainMerchantName(String name) {
		String result = name;
		if (name.indexOf("https://") > -1) {
			result = result.replaceAll("https://", "");
		}
		if (name.indexOf("http://") > -1) {
			result = result.replaceAll("http://", "");
		}
		result = FilterUtil.toPlainTextString(result);
		return result;

	}
	
	private DataSource createLnwShopDataSource(){
		DataSource dsLnwShop = null;
		try {
			BotConfigBean botConfigBean = botConfigDB.findBotConfig(lnwshopServer);
			dsLnwShop = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
			return dsLnwShop;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return dsLnwShop;
	}
	
	private String[] genMerchantReportDueDate(String year, String quarter) {
		String[] result = new String[2];
		switch (quarter) {
		case "1":
			result[0] = year+"-01-01";
			result[1] = year+"-03-31";
			break;
		case "2":
			result[0] = year+"-04-01";
			result[1] = year+"-06-31";
			break;
		case "3":
			result[0] = year+"-07-01";
			result[1] = year+"-09-31";
			break;
		case "4":
			result[0] = year+"-09-01";
			result[1] = year+"-12-31";
			break;
		default:
			break;
		}

		return result;
	}
	
	private Map<String, Integer> userNameIdMapping(){
		Map<String, Integer> userMap = new HashMap<>();
		try{
			if(userDB != null){
				List<UserBean> userList = userDB.getUserList(1);
				if(userList != null && userList.size() > 0){
					for (UserBean userBean : userList) {
						userMap.put(userBean.getUserName(), userBean.getUserId());
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return userMap;
	}
}
