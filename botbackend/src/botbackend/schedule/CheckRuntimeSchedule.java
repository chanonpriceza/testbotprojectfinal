package botbackend.schedule;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.utils.ACCPropertyParser;

import botbackend.bean.BotScheduleBean;
import botbackend.bean.RuntimeReportBean;
import botbackend.db.BotScheduleDB;
import botbackend.db.RuntimeReportDB;
import botbackend.db.utils.DatabaseUtil;

public class CheckRuntimeSchedule extends Schedule {
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser runtimeReportConfig;
	private ACCPropertyParser botServerConfig;
	private DataSource webDataSource;
	private RuntimeReportDB runtimeReportDB;
	private List<String> countryList;
	private Map<String, List<String>> serverNameMap;
	private Map<String, String> dbDriverMap;
	private Map<String, String> dbURLMap;
	private Map<String, String> dbUsernameMap;
	private Map<String, String> dbPasswordMap;
	
	public static void main(String[] args) {
		Schedule process = null;
		try {
			if(args.length != 2) {
				System.out.println("options: <runtime-report-configuration> <bot-server-configuration>");
				return;
			}
			process = new CheckRuntimeSchedule(args);
			process.initialize();
			process.start();
		} catch(Exception e) {
			logger.error("CheckRuntimeSchedule error");
			e.printStackTrace();
		} finally {
			if(process != null) {
				((CheckRuntimeSchedule) process).cleanUp();
			}
		}
	}
	
	private void cleanUp() {
		cleanUp(webDataSource);
	}
	
	public CheckRuntimeSchedule(String[] args) throws ConfigurationException {
		if(args[0] == null || args[0].trim().isEmpty()) {
			throw new ConfigurationException("no runtime-report config file");
		}
		if(args[1] == null || args[1].trim().isEmpty()) {
			throw new ConfigurationException("no bot-server config file");
		}
		
		runtimeReportConfig = new ACCPropertyParser(args[0]);
		botServerConfig = new ACCPropertyParser(args[1]);
	}
	
	public void initialize() {
		String file = runtimeReportConfig.getString("log4j-config-file");
		if(file != null) {
			PropertyConfigurator.configure(file);
		}
		
		setUpEmail(runtimeReportConfig, "CheckRuntimeSchedule", "CHECK_RUNTIME");

		webDataSource = getWebDataSource(runtimeReportConfig, null);
		
		runtimeReportDB = new RuntimeReportDB(webDataSource);
		countryList = new ArrayList<String>();
		serverNameMap = new HashMap<String, List<String>>();
		
		countryList = botServerConfig.getList("bot.server-country");
		
		for(String country: countryList) {
			serverNameMap.put(country, new ArrayList<String>());
			serverNameMap.get(country).addAll(botServerConfig.getList("bot.server-list-" + country));
			serverNameMap.get(country).removeAll(runtimeReportConfig.getList("ignore.server-list-" + country));
		}
		dbDriverMap = new HashMap<String, String>();
		dbURLMap = new HashMap<String, String>();
		dbUsernameMap = new HashMap<String, String>();
		dbPasswordMap = new HashMap<String, String>();
		
		for(String country : countryList) {
			List<String> serverNameList = serverNameMap.get(country);
			for(String serverName : serverNameList) {
				String driverName = "bot." + serverName + ".driver-name";
				String url = "bot." + serverName + ".url";
				String username = "bot." + serverName + ".username";
				String password = "bot." + serverName + ".password";
				
				dbDriverMap.put(serverName 		, botServerConfig.getString(driverName));
				dbURLMap.put(serverName 		, botServerConfig.getString(url));
				dbUsernameMap.put(serverName 	, botServerConfig.getString(username));
				dbPasswordMap.put(serverName 	, botServerConfig.getString(password));
			}
		}
	}
	
	public void start() {
		try {
			logger.info("Start CheckRuntimeSchedule Process");
			
			checkRuntime();
			
			logger.info("Finish CheckRuntimeSchedule Process");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkRuntime() throws SQLException {
		Date checkTime = new Date();
		
		for(String country : countryList) {
			logger.info("Checking on " + country.toUpperCase());
			List<String> serverNameList = serverNameMap.get(country);
			for(String serverName : serverNameList) {
				String dbDriver = dbDriverMap.get(serverName);
				String dbURL = dbURLMap.get(serverName);
				String dbUsername = dbUsernameMap.get(serverName);
				String dbPassword = dbPasswordMap.get(serverName);
				DataSource botDataSource = null;
				botDataSource = DatabaseUtil.createDataSource(dbURL, dbDriver, dbUsername, dbPassword, 3);
				BotScheduleDB botScheduleDB = new BotScheduleDB(botDataSource);
				List<BotScheduleBean> botScheduleBeanList = botScheduleDB.getAllProcess();
				Date serverTime = botScheduleDB.getCurrentTime();
				
				for(BotScheduleBean bean : botScheduleBeanList) {
					Date processTime = bean.getProcessTime();
					Calendar c = Calendar.getInstance();
					c.setTime(processTime);
					c.add(Calendar.HOUR, bean.getMaxRuntime());
					Date nextProcessTime = c.getTime();
					
					RuntimeReportBean rsBean = new RuntimeReportBean();
					rsBean.setBotServerName(serverName);
					rsBean.setProcessType(bean.getName());
					rsBean.setUpdateDate(checkTime);
					if(serverTime.after(nextProcessTime)) {
						rsBean.setStatus(1);
					} else {
						rsBean.setStatus(0);
					}
					int update = runtimeReportDB.updateRuntime(rsBean);
					if(update == 0) {
						runtimeReportDB.insertRuntime(rsBean);
					}
				}
				
				cleanUp(botDataSource);
			}
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = sdf.format(checkTime);
		List<RuntimeReportBean> reportBeanList = runtimeReportDB.getReportSendEmail(checkTime);
		int countOvertime = 0;
		for(RuntimeReportBean reportBean : reportBeanList) {
			if(reportBean.getStatus() == 1) {
				countOvertime++;
			}
		}
		String subject = "Priceza Bot Runtime Checking Report " + dateStr;
		String reportString;
		reportString = "Priceza Bot Runtime Report\n";
		reportString += "-----------------------------------------------------------------------------------------------------\n";
		reportString += "Overtime/All : " + countOvertime + "/" + reportBeanList.size() + "\n";
		reportString += "Update Date : " + dateStr + "\n";
		reportString += "-----------------------------------------------------------------------------------------------------\n";
		reportString += "-----------------------------------------------------------------------------------------------------\n";
		reportString += "Bot Server Name    |Process    |Status\n";
		reportString += "-----------------------------------------------------------------------------------------------------\n";
		for(RuntimeReportBean reportBean : reportBeanList) {
			reportString += reportBean.getBotServerName() + "    |";
			reportString += reportBean.getProcessType() + "    |";
			if(reportBean.getStatus() == 1) {
				reportString += "Overtime\n";
			} else {
				reportString += "Active\n";
			}
		}
		sendEmail(subject, reportString, "Runtime report");
	}
}
