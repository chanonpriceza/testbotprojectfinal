package botbackend.schedule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.parser.ParseException;

import botbackend.bean.TaskBean;
import botbackend.bean.TaskBean.STATUS;
import botbackend.bean.TaskDetailBean;
import botbackend.db.TaskDB;
import botbackend.db.TaskDetailDB;
import botbackend.utils.ACCPropertyParser;

public class CalculateTaskWorkDaySchedule extends Schedule {
	Set<String> HolidayList;
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser baseConfig;
	private DataSource ds = null;
	private TaskDB taskDB = null;
	private TaskDetailDB taskDetailDB = null;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public CalculateTaskWorkDaySchedule(String[] configFile) throws ConfigurationException {
		if (configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}

		baseConfig = new ACCPropertyParser(configFile[0]);
	}

	public static void main(String[] args) {
		Schedule process = null;
		try {
			if (args.length != 1) {
				System.out.println("Wrong configuration file argument,<base-config>");
				return;
			}
			process = new CalculateTaskWorkDaySchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {
			logger.error("CalculateTaskWorkDaySchedule Error", e);
			e.printStackTrace();
		}
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-calculate-workday-file");
		if (file != null) {
			PropertyConfigurator.configure(file);
		}
		setUpEmail(baseConfig, "CalculateTaskWorkDaySchedule", "CalculateTaskWorkDaySchedule");

	}

	@Override
	public void start() {
		HolidayList = new HashSet<>(baseConfig.getList("priceza-holiday-list-2018"));
		List<String> countryList = baseConfig.getList("task-manager-country-list");
		for(String country:countryList){
			ds = getBotBackendDataSource(baseConfig,country);
			taskDB = new TaskDB(ds);
			taskDetailDB = new TaskDetailDB(ds);
			processDifferentDayOfTask();
		}
	}

	private void processDifferentDayOfTask() {
		try {

			List<TaskBean> taskList = new ArrayList<TaskBean>();
			taskList = taskDB.getTaskToProcessWorkDay(new String[] {STATUS.DONE.toString(),STATUS.RESOLVE.toString()}, 0);

			for (TaskBean task : taskList) {
				List<TaskDetailBean> taskBeans = taskDetailDB.getDataByTaskIdAndStatus(task.getId(),new String[] {STATUS.RESOLVE.toString(),STATUS.DONE.toString()});
				String endDate = "";
				for (TaskDetailBean t : taskBeans) {
					endDate = formatter.format(t.getCreateDate());
				}
				String startDate = formatter.format(task.getCreateDate());
				if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
					logger.info("Cant find endDate or StartDate TaskDetail ! Task Id : "+task.getId());
					continue;
				}
				taskDB.updateTaskWorktime(calculateDifferentDays(startDate, endDate), task.getId());
			}

		} catch (Exception e) {
			logger.info(e);
		}

	}
	private int calculateDifferentDays(String startDateString, String endDateString) throws ParseException, java.text.ParseException {

		Date startDate = formatter.parse(startDateString);
		Date endDate = formatter.parse(endDateString);

		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTime(startDate);
		end.setTime(endDate);

		long startLong = start.getTimeInMillis();
		long endLong = end.getTimeInMillis();

		if(startLong == endLong) return 1;
		
		int differentDay = 0;
		while (startLong < endLong) {
			String startString = formatter.format(new Date(start.getTimeInMillis()));
			if (start.get(Calendar.DAY_OF_WEEK) != 7 && start.get(Calendar.DAY_OF_WEEK) != 1 && !HolidayList.contains(startString)) {
				differentDay++;
			}
			start.add(Calendar.DATE, 1);
			startLong = start.getTimeInMillis();
		}
		
		return differentDay;
	}

}
