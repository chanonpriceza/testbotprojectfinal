package botbackend.schedule;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;

import com.amazonaws.util.json.JSONObject;
import botbackend.utils.ACCPropertyParser;
import utils.FilterUtil;

public class APILnwShopCheckingSchedule extends Schedule {
	
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser baseConfig;
	private static String PRIVATE_KEY = "h8Nh_KLcZb6AQRbfYF459rEc8fQxYBr6";
	private static final String APIDomain = "https://www.priceza.com/lnwshop";

	public static void main(String[] args) {
		Schedule process = null;
		try {
			if(args.length!=1) {
			    System.out.println("Wrong configuration file argument,<base-config>");
			    return;
			} 			
			process = new APILnwShopCheckingSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {			
			logger.error("APILnwShopCheckingSchedule Error", e);
			e.printStackTrace();
		}
	}
	
	public APILnwShopCheckingSchedule(String[] configFile) throws ConfigurationException {
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
		
		baseConfig = new ACCPropertyParser(configFile[0]);
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-api-lnwshop-checking-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    setUpEmail(baseConfig, "APILnwShopCheckingSchedule", "API_LNWSHOP_CHECKING");
	}

	@Override
	public void start() {
		try {
			Map<String, String> m = new HashMap<String, String>();
			m.put("category_name", "สินค้าแบรนด์อื่น ๆ");
			m.put("product_url", "http://www.testRequest.com/p/893");
			m.put("reference_id", "");
			m.put("product_detail", "");
			m.put("domain", "www.testRequest.com");
			m.put("product_id", "893");
			m.put("product_price", "1490.00");
			m.put("product_picture_url",
					"https://cv.lnwfile.com/whezlg.jpg,https://cv.lnwfile.com/o3f1ok.jpg,https://cv.lnwfile.com/nkyxaf.jpg,https://cv.lnwfile.com/33d9ko.jpg,https://cv.lnwfile.com/vkjd39.jpg,https://cv.lnwfile.com/to4jyt.jpg,https://cv.lnwfile.com/m5udc4.jpg,https://cv.lnwfile.com/hnzpnq.jpg");
			m.put("product_name", "New รองเท้าบู้ท Topshops");
			m.put("tags", "");
			m.put("action", "add");
			m.put("hash", getHash(m, PRIVATE_KEY));

			logger.info("------Start Checking lnwshop process------");

			add(m);
			update(m);
			delete(m);

			logger.info("------End Checking lnwshop process--------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private JSONObject add(Map<String, String> m) throws Exception {
		String result = sendPost(m);
		String status = FilterUtil.getStringBefore(result, "||", "");
		JSONObject jsonObj = new JSONObject(FilterUtil.getStringAfter(result, "||", ""));

		if (jsonObj.get("success").equals("true")) {
			m.put("reference_id", (String) jsonObj.get("reference_id"));
			logger.info("ADD : SUCCESS");
		} else {
			logger.info("ADD : FAIL ["+status+"]");
		}

		return jsonObj;
	}

	private void update(Map<String, String> m) throws Exception {
		m.put("action", "update");	
		m.remove("hash");
	
		m.put("hash", getHash(m, PRIVATE_KEY));
		
		String result = sendPost(m);
		String status = FilterUtil.getStringBefore(result, "||", "");
		JSONObject jsonObj = new JSONObject(FilterUtil.getStringAfter(result, "||", ""));
		
		if (jsonObj.get("success").equals("true")) {
			logger.info("UPDATE : SUCCESS");
		} else {
			logger.info("UPDATE : FAIL ["+status+"]");
		}
	}
	
	private void delete(Map<String, String> m) throws Exception {
		m.put("action", "hide");	
		m.remove("hash");
	
		m.put("hash", getHash(m, PRIVATE_KEY));
		
		String result = sendPost(m);
		String status = FilterUtil.getStringBefore(result, "||", "");
		JSONObject jsonObj = new JSONObject(FilterUtil.getStringAfter(result, "||", ""));
		
		if (jsonObj.get("success").equals("true")) {
			logger.info("DELETE : SUCCESS");
		} else {
			logger.info("DELETE : FAIL ["+status+"]");
		}
	}
	
	private String getHash(Map<String, String> valueMap, String secret) {
		String rtn = "get_hash_error";
		try {
			TreeMap<String, String> hashMap = new TreeMap<>(valueMap);
			String message = "";
			for(Entry<String, String> map : hashMap.entrySet()) {
				message += map.getValue();
			}
			
			byte[] r = message.getBytes("UTF-8");
			byte[] k = secret.getBytes("UTF-8");
			
			HMac hmac = new HMac(new RIPEMD160Digest());
			hmac.init(new KeyParameter(k));
			hmac.update(r, 0, r.length);
			
			byte[] out = new byte[hmac.getMacSize()];
			hmac.doFinal(out, 0);
			
			rtn =  new String(Hex.encode(out), "US-ASCII");
		} catch(Exception e) {
			logger.error(e);
		}
		
		return rtn;
	}
	
	private String sendPost(Map<String, String> m) throws Exception {
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(APIDomain);
		
		post.setHeader("Content-Type","application/x-www-form-urlencoded; ");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		
		urlParameters.add(new BasicNameValuePair("category_name", m.get("category_name")));
		urlParameters.add(new BasicNameValuePair("product_url", m.get("product_url")));
		urlParameters.add(new BasicNameValuePair("reference_id", m.get("reference_id")));
		urlParameters.add(new BasicNameValuePair("product_detail", m.get("product_detail")));
		urlParameters.add(new BasicNameValuePair("domain", m.get("domain")));
		urlParameters.add(new BasicNameValuePair("product_id", m.get("product_id")));
		urlParameters.add(new BasicNameValuePair("action", m.get("action")));
		urlParameters.add(new BasicNameValuePair("product_price", m.get("product_price")));
		urlParameters.add(new BasicNameValuePair("product_picture_url", m.get("product_picture_url")));
		urlParameters.add(new BasicNameValuePair("product_name", m.get("product_name")));
		urlParameters.add(new BasicNameValuePair("hash", m.get("hash")));
		urlParameters.add(new BasicNameValuePair("tags", m.get("tags")));
		
		post.setEntity(new UrlEncodedFormEntity(urlParameters,"UTF-8"));
		
		HttpResponse response = client.execute(post);
		
		BufferedReader rd = new BufferedReader( new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		result.append(response.getStatusLine().getStatusCode()+"||");
		String line = "";
		
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();
	}
}
