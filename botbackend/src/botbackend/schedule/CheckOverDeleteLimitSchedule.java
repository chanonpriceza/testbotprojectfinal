package botbackend.schedule;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.utils.ACCPropertyParser;
import utils.DateTimeUtil;

import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.TrackMerchantStatisticBean;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.TrackMerchantStatisticDB;

public class CheckOverDeleteLimitSchedule extends Schedule {
	private static Logger logger = Logger.getLogger("priceza.schedule");
	
	private ACCPropertyParser propConfig;
	
	private DataSource prodDataSource;
	private DataSource webDataSource;
	
	private MerchantDB botMerchantDB;
	private MerchantConfigDB botMerchantConfigDB;
	
	private TrackMerchantStatisticDB trackMerchantStatisticDB;
	
	private String TARGET_FIELD = "deleteLimit";
    private String FILE_DESTINATION_PATH = "";
   
	public static void main(String args []){
		Schedule process = null;
		try {	
			if(args.length != 1) {
				logger.error("Must have : <bot-backend-configuration> ");
				return;
			}
			process = new CheckOverDeleteLimitSchedule(args);
			process.initialize();
			process.start();
		} catch (Exception e) {
			logger.error("CheckOverDeleteLimitSchedule Error", e);			
			e.printStackTrace();
		} finally {
			if(process != null) {
				((CheckOverDeleteLimitSchedule) process).cleanUp();	
			}	
		}
	}
	
	private void cleanUp() {
		cleanUp(new DataSource[]{prodDataSource, webDataSource});
	}

	public CheckOverDeleteLimitSchedule(String[] args) throws ConfigurationException{
		if(args[0] == null || args[0].trim().length() == 0) {
			throw new ConfigurationException("<bot-backend-configuration>");
		}	
		
		propConfig = new ACCPropertyParser(args[0]);	
	}
	
	public void initialize() {
		String file = propConfig.getString("log4j-check-overlimit-file");
	    if(file != null) {
	    	PropertyConfigurator.configure(file);
	    }
	    
	    setUpEmail(propConfig, "CheckOverDeleteLimitSchedule", "CHECK_OVER_DELETE_LIMIT");
	    
	    webDataSource = getWebDataSource(propConfig, null);
	    prodDataSource = getProdDataSource(propConfig);
	    
	    botMerchantDB = new MerchantDB(webDataSource);
		botMerchantConfigDB = new MerchantConfigDB(webDataSource);
		trackMerchantStatisticDB = new TrackMerchantStatisticDB(prodDataSource);
		
		FILE_DESTINATION_PATH = propConfig.getString("report.check-deletelimit.destination.path");
	}
	
	public void start() {
		try {
			logger.info("Start check over deleteLimit.");	
    	  	
    	  	checkOverDeleteLimit();
    	  	
    	  	logger.info("End check over deleteLimit.");	
      	} catch (Exception e) {
      		logger.error("CheckOverDeleteLimitSchedule process Fail ", e);
      		sendError(e.toString(), "sendErrorEmail");
      	}
	}
	
	public void checkOverDeleteLimit() {
		try {
			// GET COUNT CURRENT PRODUCT OF ALL MERCHANT FROM PRODUCTION DB
			Map<Integer, Integer> countProductMapping = generateCountProductMapping();
			if(countProductMapping == null || countProductMapping.size() == 0){
				logger.info("checkOverDeleteLimit : no data from countProductMapping");
				return;
			}
			
			Map<Integer, Integer> deleteLimitMapping = generateDeleteLimitMapping();
			if(deleteLimitMapping == null || deleteLimitMapping.size() == 0){
				logger.info("checkOverDeleteLimit : no data from deleteLimitMapping");
				return;
			}
			
			List<MerchantBean> allMerchantBean = botMerchantDB.getAllMerchant();
			if(allMerchantBean == null || allMerchantBean.size() == 0){
				logger.info("checkOverDeleteLimit : no data from allMerchantBean");
				return;
			}
			
			// WRITE CSV REPORT
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(FILE_DESTINATION_PATH + "Check Over DeleteLimit " + DateTimeUtil.generateStringDate(new Date(), "yyyy-MM") + ".txt"),  false));){
				
				String summary = String.format("%s%n%tD%n", "Check Over DeleteLimit", Calendar.getInstance());
				String header = String.format(""
						+ "%n===========|================================|==============|==============|==============|=============|============="
						+ "%n%-10s | %-30s | %-12s | %-12s | %-12s | %-12s | %-12s "
						+ "%n===========|================================|==============|==============|==============|=============|============="
						, "MerchantId", "MerchantName", "Product", "DeleteLimit", "QueryInsert", "QueryUpdate", "QueryDelete");
				
				pwr.println(summary);
				pwr.println(header);
				
				for (MerchantBean merchantBean : allMerchantBean) {

					int countProduct = countProductMapping.getOrDefault(merchantBean.getId(), 0);
					int deleteLimit = deleteLimitMapping.getOrDefault(merchantBean.getId(), 0);
					String queryInsert = "";
					String queryUpdate = "";
					String queryDelete = "";
					
					if(deleteLimit == 0){ // DEFAULT 200
						if(countProduct > 0){
							queryInsert = "INSERT INTO `tbl_merchant_config` (`merchantId`, `field`, `value`) VALUES ('"+merchantBean.getId()+"', 'deleteLimit', '"+countProduct/5+"');";
						}
					}else if(deleteLimit > countProduct){ // over delete limit
						if(countProduct > 0){
							queryUpdate = "UPDATE `tbl_merchant_config` SET `value`='"+countProduct/5+"' WHERE (`merchantId`='"+merchantBean.getId()+"') AND (`field`='deleteLimit');";
						}else{
							queryDelete = "DELETE FROM `tbl_merchant_config` WHERE (`merchantId`='"+merchantBean.getId()+"') AND (`field`='deleteLimit');";
						}
					}
					
					String merchantPrintDataLine = String.format("%-10s | %-30s | %-12s | %-12s | %-12s | %-12s | %-12s "
							, merchantBean.getId()
							, merchantBean.getName()
							, countProduct
							, deleteLimit
							, queryInsert
							, queryUpdate
							, queryDelete);
					pwr.println(merchantPrintDataLine);
					
				}
				
			}catch(Exception e){
				logger.error("checkOverDeleteLimit Error : checkOverDeleteLimit() : write CSV", e);
				e.printStackTrace();
			}
		}catch(Exception e){
			logger.error("checkOverDeleteLimit Error : checkOverDeleteLimit()", e);	
			e.printStackTrace();
		}
		
	}
	
	public Map<Integer, Integer> generateCountProductMapping(){
		Map<Integer, Integer> merchantCountProductMap = new HashMap<>();
		try{
			List<TrackMerchantStatisticBean> latestMerchantStatisticList = trackMerchantStatisticDB.getLatestRecord();
			if(latestMerchantStatisticList == null || latestMerchantStatisticList.size() == 0) return null;
			
			for (TrackMerchantStatisticBean trackMerchantStatisticBean : latestMerchantStatisticList) {
				merchantCountProductMap.put(trackMerchantStatisticBean.getMerchant_id(), trackMerchantStatisticBean.getSolr_product());
			}
			
		}catch(Exception e){
			logger.error("checkOverDeleteLimit Error : generateCountProductMapping()", e);
			e.printStackTrace();
		}
		return merchantCountProductMap;
	}
	
	public Map<Integer, Integer> generateDeleteLimitMapping() {
		Map<Integer, Integer> deleteLimitMap = new HashMap<>();
		try{
			List<MerchantConfigBean> merchantConfigList = botMerchantConfigDB.findMerchantConfigByField(TARGET_FIELD);
			if(merchantConfigList == null || merchantConfigList.size() == 0) return null;
			
			for (MerchantConfigBean merchantConfigBean : merchantConfigList) {
				deleteLimitMap.put(merchantConfigBean.getMerchantId(), Integer.parseInt(merchantConfigBean.getValue()));
			}
			
		}catch(Exception e){
			logger.error("checkOverDeleteLimit Error : generateServerRuntimeMapping()", e);
		}
		return deleteLimitMap;
	}
}
