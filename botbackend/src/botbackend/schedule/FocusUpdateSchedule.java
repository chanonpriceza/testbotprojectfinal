package botbackend.schedule;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.MerchantConfigBean.FIELD;
import bean.ParserConfigBean;
import bean.ProductDataBean;
import bean.SendDataBean;
import botbackend.bean.BotConfigBean;
import botbackend.bean.ConfigBean;
import botbackend.bean.FocusUpdateBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.db.BotConfigDB;
import botbackend.db.ConfigDB;
import botbackend.db.FocusUpdateDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ACCPropertyParser;
import botbackend.utils.ParserUtil;
import db.ProductDataDB;
import db.SendDataDB;
import utils.DateTimeUtil;

public class FocusUpdateSchedule extends Schedule {
	
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private static final String PROCESS = "FocusUpdate";
	private static final String PROCESS_NAME = "FocusUpdateSchedule";
	private static final String CONFIG_SEND_DATA_DEFAULT_SERVER = "focus.update.senddata.default.server";
	
    private Map<String, DataSource> dataSourceMap = new HashMap<>();
	private Map<String, ProductDataDB> productDataMap = new HashMap<>();
	private Map<String, SendDataDB> sendDataMap = new HashMap<>();
	
	private Map<String, String> parserClassMap = new HashMap<>();
	private Map<String, List<ParserConfigBean>> parserConfigMap = new HashMap<>();
	private Map<String, BotConfigBean> botConfigMap = new HashMap<>();
	
	private Map<String, Properties> merchantConfigPropertiesMap = new HashMap<>();
    
	private ACCPropertyParser baseConfig;
	
	public static void main(String[] args) {
		Schedule process;
		try {
			process = new FocusUpdateSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public FocusUpdateSchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
		
		baseConfig = new ACCPropertyParser(configFile[0]);
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-focus-update-config-file");
	    if(file != null) {
	    	PropertyConfigurator.configure(file);
	    }
		setUpEmail(baseConfig, PROCESS, PROCESS_NAME);
	}

	@Override
	public void start() {
		
		List<String> contryList = baseConfig.getList("focus-update-country-list");
		
		// run all country
		for (String country: contryList) {
			logger.info("Start Focus Update process : "+country);
			
			DataSource dsBotBackend = getBotBackendDataSource(baseConfig, country.toLowerCase());
			
			// config db
			FocusUpdateDB focusUpdateDB = new FocusUpdateDB(dsBotBackend);
			BotConfigDB botConfigDB = new BotConfigDB(dsBotBackend);
			ConfigDB configDB = new ConfigDB(dsBotBackend);
			MerchantConfigDB merchantConfigDB = new MerchantConfigDB(dsBotBackend);
			ParserConfigDB parserConfigDB = new ParserConfigDB(dsBotBackend);
			
			try {
				
				ConfigBean configBean = configDB.getByName(CONFIG_SEND_DATA_DEFAULT_SERVER);
				if(configBean == null) {
					logger.info("Can't find config for send data in country : " + country);
					continue; // go next country
				}
				String targetSendDataServer = configBean.getConfigValue();
				
				List<FocusUpdateBean> focusUpdateBeanList = focusUpdateDB.getCurrentList(1);
				if(focusUpdateBeanList != null && focusUpdateBeanList.size() > 0) {
					for (FocusUpdateBean focusUpdateBean : focusUpdateBeanList) {
						int merchantId = focusUpdateBean.getMerchantId();
						
						String resultNote = "";
						MerchantConfigBean merchantConfigBean = merchantConfigDB.findMerchantConfigByIdAndField(merchantId, FIELD.parserClass.toString());
						if(merchantConfigBean != null) {
							
							String parserClass = parserClassMap.get(country+merchantId);
							if(StringUtils.isBlank(parserClass)) {
								parserClass = merchantConfigBean.getValue();
								parserClassMap.put(country+merchantId, parserClass);
							}
							List<ParserConfigBean> parserConfigList = parserConfigMap.get(country+merchantId);
							if(parserConfigList==null) {
								parserConfigList = parserConfigDB.getByMerchantId(merchantId);
								parserConfigMap.put(country+merchantId,parserConfigList);
							}
							
						} 
						
						// find product data
						if(StringUtils.isNotBlank(focusUpdateBean.getBotServerName()) && focusUpdateBean.getBotProductId() > 0) {
							BotConfigBean botConfigBean = botConfigMap.get(country+focusUpdateBean.getBotServerName());
							if(botConfigBean == null) {
								botConfigBean = botConfigDB.findBotConfig(focusUpdateBean.getBotServerName());
								botConfigMap.put(country+focusUpdateBean.getBotServerName(), botConfigBean);
							}
							
							if(botConfigBean != null) {
								
								ProductDataDB productDataDB = getBotProductDataDB(country+focusUpdateBean.getBotServerName(), botConfigBean);
								SendDataDB sendDataDB = getBotSendDataDB(country+targetSendDataServer, botConfigBean);
								
								ProductDataBean productDataBean = productDataDB.getProduct(focusUpdateBean.getBotProductId());
								
								// update process
								if(productDataBean != null) {
									
										try {
												List<ParserConfigBean> parserConfigList = parserConfigMap.get(country+merchantId);
												String parserClass = parserClassMap.get(country+merchantId);
												Properties mConfig = merchantConfigPropertiesMap.get(country+merchantId);
												if(mConfig == null) {
													mConfig = new Properties();
													List<MerchantConfigBean> merchantConfigList = merchantConfigDB.findMerchantConfig(merchantId);
													for (MerchantConfigBean merchantConfigDataBean : merchantConfigList) {
														mConfig.setProperty(merchantConfigDataBean.getField(), merchantConfigDataBean.getValue());
													}		
													merchantConfigPropertiesMap.put(country+merchantId, mConfig);
												}
												
												List<ProductDataBean> updateProductDataBeanList = ParserUtil.parse(parserClass,parserConfigList, new String[] {productDataBean.getUrlForUpdate()} );
												if(updateProductDataBeanList != null && updateProductDataBeanList.size() > 0) {
													for(ProductDataBean updateProductDataBean : updateProductDataBeanList) {
														if(StringUtils.isBlank(updateProductDataBean.getName()) || updateProductDataBean.getPrice()==0) {
															resultNote = "Error : Fix Parser";
															continue;
														}
														ProductDataBean updatedProductDataBean = update(productDataBean, updateProductDataBean);
														if(updatedProductDataBean != null) {
															SendDataBean sendDataBean = SendDataBean.createSendDataBean(updatedProductDataBean, null, SendDataBean.ACTION.UPDATE);
															int updateResult = productDataDB.updateProductData(focusUpdateBean.getBotProductId(), updatedProductDataBean);
															long sendDataResult = sendDataDB.insertSendData(sendDataBean);
															if(updateResult > 0 && sendDataResult > 0) {
																resultNote = "Data Change " + DateTimeUtil.generateStringDisplayDateTime(new Date());
																break;
															}
														}else {
															if(StringUtils.isNotBlank(focusUpdateBean.getNote()) && focusUpdateBean.getNote().contains("Error")) {
																resultNote = "No Change";
															}else if(StringUtils.isNotBlank(focusUpdateBean.getNote())) {
																resultNote = focusUpdateBean.getNote();
															}else {
																resultNote = "No Change";
															}
														}
													}
												}else {
													resultNote = "Error : Can't Parse";
												}
											
										}catch(Exception e) {
											resultNote = "Error : Parse Exception";
										}
										
								}else {
									resultNote = "Error : Product deleted from bot";
								}
							}
						}
						focusUpdateDB.saveUpdateItem(resultNote, focusUpdateBean.getId());
					}
				}
				
			} catch (SQLException e1) {
				e1.printStackTrace();
				logger.error("There is a problem occuring in process : "+country);
			} finally {
				for (Entry<String, DataSource> e: dataSourceMap.entrySet()) {
					cleanUp(e.getValue());
				}
				cleanUp(dsBotBackend);
			}
			logger.info("End Focus Update process : "+country);
				
		}
	}
	
	private ProductDataBean update(ProductDataBean productDataBean, ProductDataBean productParserBean) {
		
		boolean isUpdated = false;
		if(productDataBean != null && productParserBean != null) {
				
//			not check same name
//			if(!productDataBean.getName().equals(productParserBean.getName())) {
//				return false;
//			}
			
//			not check same url
//			if(!productDataBean.getUrlForUpdate().equals(productParserBean.getUrl())) {
//				return false;
//			}
			
			if(productParserBean.getPrice()!=0) {
				return null;
			}
			
			if(productParserBean.getPrice()!=0) {
				return null;
			}
			
			if(productParserBean.getPrice()!=0  && productDataBean.getPrice() != productParserBean.getPrice()) {
				productDataBean.setPrice(productParserBean.getPrice());
				isUpdated = true;
			}
			
			if(productParserBean.getPrice()!=0  && productDataBean.getBasePrice() != productParserBean.getBasePrice()) {
				productDataBean.setBasePrice(productParserBean.getBasePrice());
				isUpdated = true;
			}
				
		}
		
		if(isUpdated) {
			return productDataBean;
		}else {
			return null;
		}
	}
	
	private ProductDataDB getBotProductDataDB(String countryAndServer, BotConfigBean botConfigBean) {
		ProductDataDB productDataDB = productDataMap.get(countryAndServer);
		if(productDataDB == null) {
			DataSource dataSource = dataSourceMap.get(countryAndServer);
			if(dataSource == null) {
				dataSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
				dataSourceMap.put(countryAndServer, dataSource);
			}
			productDataDB = new ProductDataDB(dataSource);
			productDataMap.put(countryAndServer, productDataDB);
		}
		return productDataDB;
	}
	
	private SendDataDB getBotSendDataDB(String countryAndServer, BotConfigBean botConfigBean) {
		SendDataDB sendDataDB = sendDataMap.get(countryAndServer);
		if(sendDataDB == null) {
			DataSource dataSource = dataSourceMap.get(countryAndServer);
			if(dataSource == null) {
				dataSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
				dataSourceMap.put(countryAndServer, dataSource);
			}
			sendDataDB = new SendDataDB(dataSource);
			sendDataMap.put(countryAndServer, sendDataDB);
		}
		return sendDataDB;
	}
	
	public static String decodeUrl(String url) {	
		if(url == null) {
			return url;
		}
		try {
			return URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return url;
		}		
	}

}
