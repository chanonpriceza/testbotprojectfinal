package botbackend.schedule;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import botbackend.bean.BotConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.ProductUrlBean;
import botbackend.bean.WorkLoadBean;
import botbackend.db.BotConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.ProductDataDB;
import botbackend.db.ProductUrlDB;
import botbackend.db.WorkLoadDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ACCPropertyParser;

public class AutoApproveSchedule extends Schedule {

		private static Logger logger = Logger.getLogger("priceza.schedule");
		private ACCPropertyParser baseConfig;
		
		private DataSource botDataSource;
		private DataSource verifyDataSource;
		private MerchantDB merchantDB;

		public AutoApproveSchedule(String[] configFile) throws ConfigurationException {
			if (configFile[0] == null || configFile[0].trim().isEmpty()) {
				throw new ConfigurationException("no <base-config>");
			}

			baseConfig = new ACCPropertyParser(configFile[0]);
		}

		public static void main(String[] args) {
			Schedule process = null;
			try {
				if (args.length != 1) {
					System.out.println("Wrong configuration file argument,<base-config>");
					return;
				}
				process = new AutoApproveSchedule(args);
				process.initialize();
				process.start();
			} catch (ConfigurationException e) {
				logger.error("AutoApproveSchedule Error", e);
				e.printStackTrace();
			}
		}

		@Override
		public void initialize() {
//			String file = baseConfig.getString("log4j-calculate-workday-file");
//			if (file != null) {
//				PropertyConfigurator.configure(file);
//			}
			setUpEmail(baseConfig, "AutoApproveSchedule", "AutoApproveSchedule");
		}

		@Override
		public void start() {
			try {
				// select active 4
				// assign alternative Datasource
				List<String> countryList = baseConfig.getList("auto-approve-db-country-list");
				for (String country: countryList) {
					country = country.toLowerCase();
					botDataSource = getBotBackendDataSource(baseConfig, country.toLowerCase());
					verifyDataSource = assignVerifyDataSource(baseConfig,country);
					merchantDB = new MerchantDB(botDataSource);
					List<MerchantBean> merchantBean = merchantDB.getMerchantByActive(4);
					for(MerchantBean m:merchantBean) {
						logger.info("Start Analyze MerchantID: "+m.getId()+" COUNTRY: "+country.toUpperCase());
						WorkLoadDB workLoadDB = new WorkLoadDB(verifyDataSource);
						ProductUrlDB productUrl = new ProductUrlDB(verifyDataSource);
						
						List<WorkLoadBean> workLoadBean = workLoadDB.selectWorkLoadById(m.getId(),100);
						long workLoadBeanCount = workLoadBean.stream().filter(x->x.getStatus().equals("C")).map(x->x).count();
						if(!calculateStandardAllow(workLoadBean.size(),workLoadBeanCount)) {
							logger.info("Analyze not Allow Workload has success less than 50%"+ "MerchantID: "+m.getId()+" COUNTRY: "+country.toUpperCase());
							break;
						}
						List<ProductUrlBean> productUrls = productUrl.getProductUrlByMerchantId(m.getId(), 100);
						long productUrlCount = productUrls.stream().filter(x->x.getStatus().equals("C")).map(x->x).count();
						if(!calculateStandardAllow(productUrls.size(),productUrlCount)) {
							logger.info("Analyze not Allow ProductUrl has success less than 50%"+ "MerchantID: "+m.getId()+" COUNTRY: "+country.toUpperCase());
							break;
						}
						
						ProductDataDB pdDB = new ProductDataDB(verifyDataSource);
						int countPDB = pdDB.countProduct(m.getId());
						if(countPDB==0) {
							logger.info("Analyze not Allow no ProductData " + "MerchantID: "+m.getId()+" COUNTRY: "+country.toUpperCase());
						}
						
						logger.info("Analyze success !!!!!!! "+ "MerchantID: "+m.getId()+" COUNTRY: "+country.toUpperCase());
						m.setActive(1);
						m.setDataServer(autoAssignDataServer(baseConfig, country));
						if(!country.equalsIgnoreCase("th"))
							m.setServer(autoAssignServer(baseConfig, country));
						merchantDB.updateNewMerchant(m);
						
						//Select data and check Data
						//conclusion result
						
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
				
		}
		
		private DataSource assignVerifyDataSource(ACCPropertyParser accProp,String country) throws SQLException {
			DataSource ds = null;
			String verifyName = accProp.getString("verify."+country+".db.name");
			BotConfigDB config = new BotConfigDB(botDataSource);
			BotConfigBean ver = config.findBotConfig(verifyName);
			String webDBDriverName = "com.mysql.jdbc.Driver";
			String webDBUrl = "jdbc:mysql://"+ver.getUrl();
			String webDBUserName = ver.getUsername();
			String webDBPassword = ver.getPassword();
			ds =  DatabaseUtil.createDataSource(webDBUrl, webDBDriverName, webDBUserName, webDBPassword, 3);
			return ds;
		}
		
		private String autoAssignServer(ACCPropertyParser accProp,String country) {
			String serverName = accProp.getString("autoapprove."+country+".server");
			if(StringUtils.isBlank(serverName))
				return null;
			return serverName;
		}
		
		private String autoAssignDataServer(ACCPropertyParser accProp,String country) {
			String serverName = accProp.getString("autoapprove."+country+".dataserver");
			if(StringUtils.isBlank(serverName))
				return null;
			return serverName;
		}
		
		private boolean calculateStandardAllow(double total,double filter) {
			return filter/total>0.5;
			
		}

}
