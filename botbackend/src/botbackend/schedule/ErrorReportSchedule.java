package botbackend.schedule;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.utils.ACCPropertyParser;

import botbackend.bean.BotConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.db.BotConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.Util;


public class ErrorReportSchedule extends Schedule {
	private static Logger logger = Logger.getLogger("priceza.schedule");
	
	private ACCPropertyParser propConfig;
	private DataSource botBackendDataSource;
	private static final String DEFAULT_DB_DRIVER = "com.mysql.jdbc.Driver";
    
	public static void main(String[] args) {
		
		Schedule process = null;
		try {			
			if(args.length != 1) {
			    System.out.println("Wrong configuration file argument,<base-config> ");
			    return;
			} 			
			process = new ErrorReportSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {			
			logger.error("ErrorReportSchedule Error", e);
			e.printStackTrace();
		} finally {
			if(process != null) {
				((ErrorReportSchedule) process).cleanUp();		
			}					
		}
	}
	
	private void cleanUp() {
		cleanUp(botBackendDataSource);
	}
	
	public ErrorReportSchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <error-report-config>");
		}
	
		propConfig = new ACCPropertyParser(configFile[0]);
	}
	
	public void initialize() {
		String file = propConfig.getString("log4j-error-report-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    setUpEmail(propConfig, "ERROR_REPORT", "ErrorReportSchedule");
	}
	
	public void start(){
		logger.info("Start check error report.");	
		  	
		checkMerchantError();
		  	
		logger.info("End check error report.");	
	}
	
	private void checkMerchantError(){
	
		List<String> emailCountryList = propConfig.getList("email-country-list");
		if(emailCountryList != null && emailCountryList.size() > 0){
			List<String> excludeBotBackendDBList = propConfig.getList("bot.server-list-all-exclude-botbackend");
			for(String country : emailCountryList){
			    try{
			    	logger.info("Start error report country: "+country);
				    
				    String bbeDBDriverName = propConfig.getString("bot."+country+".backend.db.driver");
				  	String bbeDBURL 	   = propConfig.getString("bot."+country+".backend.db.url");
				  	String bbeDBUserName   = propConfig.getString("bot."+country+".backend.db.username");
				  	String bbeDBPassword   = propConfig.getString("bot."+country+".backend.db.password");		
				  	botBackendDataSource = DatabaseUtil.createDataSource(bbeDBURL, bbeDBDriverName, bbeDBUserName, bbeDBPassword, 3);
			    	
		        	StringBuilder errorMessage = new StringBuilder();
		        	StringBuilder errorMessage2 = new StringBuilder();
		        	StringBuilder errorText = new StringBuilder();
					
					MerchantDB mBotDB = new MerchantDB(botBackendDataSource);
					BotConfigDB botConfigDB = new BotConfigDB(botBackendDataSource);
					 	
					//CHECK BOT SERVERLIST

					List<BotConfigBean> botConfigList = botConfigDB.getBotConfigList();
					
					if(botConfigList != null && botConfigList.size() > 0){
						for (BotConfigBean bConfig : botConfigList) {			
							DataSource botDataSource = null;	
							String serverName = bConfig.getServer();
							try {
								botDataSource = DatabaseUtil.createDataSource("jdbc:mysql://"+bConfig.getUrl(), DEFAULT_DB_DRIVER, bConfig.getUsername(), bConfig.getPassword(), 2);	
								// check bot merchant active = 2
								try {
									List<MerchantBean> errorMerchant = null;
									if(excludeBotBackendDBList != null && excludeBotBackendDBList.contains(serverName)){
										MerchantDB botMerchantDB = new MerchantDB(botDataSource);
										errorMerchant = botMerchantDB.getMerchantByActive(2);
									}else{
										errorMerchant = mBotDB.getMerchantByActiveAndServer(2, serverName);
									}
								
									for (MerchantBean botMerchantBean : errorMerchant) {
										errorMessage.append(serverName + " | " + botMerchantBean.getId() + " | " + botMerchantBean.getName() + " | errorMessage = " + botMerchantBean.getErrorMessage()+"\n");
									}
								} catch (Exception ex) {
									errorMessage.append(serverName+" | "+"error checking status on bot serverName = "+serverName+", "+ex.getMessage()+"\n");
									logger.error("Error checking status on bot serverName = "+serverName, ex);
								}
								
								// Count Data Wait For Sync
								try {
									SendDataDB sendDataDB = new SendDataDB(botDataSource);
									Map<String, String> reportDataWaitSyncMap = sendDataDB.reportDataByStatus("W");
									int CountDataWaitSync=0;
									StringBuilder reportText = new StringBuilder();
									for(Map.Entry<String, String> entry : reportDataWaitSyncMap.entrySet()){
										int countData = Integer.parseInt(entry.getValue());
										CountDataWaitSync += countData;
										
										reportText.append(" | "+entry.getKey()+":"+Util.formatNumber(countData));
									}
									
									if( reportText.length()>0 ){ reportText.delete(0,3).insert(0,"[ ").append(" ]"); }
									errorMessage2.append(serverName+" | " +reportText.toString()+" | "+"Data = "+Util.formatNumber(CountDataWaitSync)+" \n");
								
								} catch (Exception ex) {
									errorMessage2.append(serverName+" | "+"error count data wait to sync on bot serverName = "+serverName+", "+ex.getMessage()+"\n");
									logger.error("Error count data wait to sync on bot serverName = "+serverName, ex);
								}
								
							} catch (Exception ex) {
								errorMessage.append("error check bot serverlist serverName = "+serverName+", "+ex.getMessage()+"\n");
								logger.error("Error check bot serverlist on bot serverName = "+serverName, ex);
							} finally {			
								cleanUp(botDataSource);			
							}				
						}	
					
					}
					
					if(errorMessage.length() > 0) {
		        		errorText.append("####### Found Bot Merchant Active = 2 (exceed delete limit) #######\n");
		        		errorText.append(errorMessage.toString());
		        		errorText.append("\n");
		        	}
					
					if(errorMessage2.length() > 0) {
		        		errorText.append("####### Count Data Wait For Sync #######\n");
		        		errorText.append(errorMessage2.toString());
		        		errorText.append("\n");
		        	}
					
					errorMessage.setLength(0);		
					errorMessage2.setLength(0);
					
		        	if(errorText.length() > 0) {
		        		logger.error("!!!!! Found error !!!!!");
		        		logger.error(errorText.toString());
		    			sendEmail("!!!!! Bot Information Report ["+ country +"] !!!!! : Priceza Monitoring :" + BaseSchedule.now(), errorText.toString(), "Priceza Error Report");
		        	}
					
					logger.info("Finish error report: "+ country);				
				} catch (Exception e) {
					logger.error("Error report Fail: "+country, e);
					sendError(e.toString(), "Error report Fail");
				}
			}
		}
	}
}