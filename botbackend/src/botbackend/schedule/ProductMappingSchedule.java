package botbackend.schedule;

import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bean.BotConfigBean;
import bean.DataObjectBean;
import bean.MerchantBean;
import bean.ProductMappingBean;
import botbackend.utils.ACCPropertyParser;
import db.BotConfigDB;
import db.MerchantDB;
import db.ProductDataDB;
import db.ProductMappingDB;
import db.manager.DatabaseUtil;

public class ProductMappingSchedule extends Schedule{
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser propConfig;
	private List<DataSource> ds = new ArrayList<>();
	private DataSource botDS;
	private Map<String, DataObjectBean> DATA_OBJ = new HashMap<>();
	private KafkaConsumer<String, String> CONSUMER = null;
	private boolean KAFKA_START;
	private ProductMappingDB mapDB;
	private MerchantDB mDB;

	public static void main(String[] args) {
		ProductMappingSchedule process = null;
		try {
			process = new ProductMappingSchedule(args);
			logger.info("ProductMappingSchedule Start");
			process.initialize();
			process.start();
			logger.info("ProductMappingSchedule Finish");
		} catch (ConfigurationException e) {
			logger.error("ProductMappingSchedule Error", e);
			e.printStackTrace();
		} finally {
			process.cleanup();
		}
	}
	
	public ProductMappingSchedule(String[] args) throws ConfigurationException {
		if(StringUtils.isNotBlank(args[0]))
			propConfig = new ACCPropertyParser(args[0]);
		
		String file = propConfig.getString("log4j-product-mapping-config-file");
	    if(file != null)
	    	PropertyConfigurator.configure(file);
	}
	
	public void cleanup(){		
		if(CONSUMER != null)
			CONSUMER.close();
		
		if(ds != null)
			ds.forEach(d -> {
				try {
					((BasicDataSource)d).close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		
		if(botDS != null) {
			try {
				((BasicDataSource)botDS).close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void initialize() {
		botDS = DatabaseUtil.createDataSource(propConfig.getString("web.db.url"), propConfig.getString("web.db.driver"),
				propConfig.getString("web.db.username"), propConfig.getString("web.db.password"), 1);
		
		String jaasFormat = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
		String jaasCfg = String.format(jaasFormat, propConfig.getString("kafka.username"), propConfig.getString("kafka.password"));
		
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, propConfig.getString("kafka.server"));
		props.put(ConsumerConfig.GROUP_ID_CONFIG, propConfig.getString("kafka.group"));
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("sasl.jaas.config", jaasCfg);
		props.put("security.protocol", "SASL_PLAINTEXT");
		props.put("sasl.mechanism", "PLAIN");
		CONSUMER = new KafkaConsumer<String, String>(props);
		CONSUMER.subscribe(Arrays.asList(propConfig.getString("kafka.topic")));
		KAFKA_START = propConfig.getBoolean("kafka.start", false);
		
		mapDB = new ProductMappingDB(botDS);
		mDB = new MerchantDB(botDS);
	}

	@Override
	public void start() {
		try {
			loadBotConnection();
			while(true) {
				List<JSONObject> data = getRequest();
				if(data.isEmpty())
					break;
				
				for(JSONObject obj : data) {
					int prodId = -1;
					int merchantId = -1;
					String name = null;
					
					logger.info(obj.toJSONString());
					try {
						prodId = Math.toIntExact((Long)obj.get("id"));
						merchantId = Math.toIntExact((Long)obj.get("merchantId"));
						name = (String) obj.get("name");
						
						if(prodId < 1 || merchantId < 1 || StringUtils.isEmpty(name)) {
							logger.error("data is invalid");
							continue;
						}
						
						MerchantBean merchantB = mDB.getMerchantById(merchantId);
						String dataServer = merchantB.getDataServer();
						if(StringUtils.isBlank(dataServer))
							throw new Exception("dataserver is blank");
						
						updateProductMapping(prodId, merchantId, name, dataServer);
					}catch(Exception e) {
						logger.error(e);
					}
				}
			}
		}catch(Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		
	}
	
	private void loadBotConnection() throws SQLException {
		BotConfigDB bDB = new BotConfigDB(botDS);
		List<BotConfigBean> config = bDB.getByDataAvailable(1);
		for(BotConfigBean db : config) {
			String botUrl = "jdbc:mysql://"+ db.getUrl();
			DataSource ds = DatabaseUtil.createDataSource(botUrl, "com.mysql.jdbc.Driver", db.getUsername(), db.getPassword(), 1);
			this.ds.add(ds);
			
			DataObjectBean b = new DataObjectBean();
			ProductDataDB productDB = new ProductDataDB(ds);
			b.setProductDataDB(productDB);
			DATA_OBJ.put(db.getServer(), b);
		}
	}
	
	private List<JSONObject> getRequest() throws Exception  {
		List<JSONObject> data = new ArrayList<>();
		int empty = 0;
		int limit = 5;
		while(empty < limit) {
			ConsumerRecords<String, String> records = null;
			if(KAFKA_START)
				CONSUMER.seekToBeginning(CONSUMER.assignment());
			
			records = CONSUMER.poll(Duration.ofMillis(1000));
			if(records.isEmpty()) {
				empty++;
				continue;
			}
			
			if(KAFKA_START)
				KAFKA_START = false;
			
			for(ConsumerRecord<String, String> record : records) {
				String value = record.value();
				try {
					JSONObject obj = (JSONObject)new JSONParser().parse(value);
					data.add(obj);
				} catch (ParseException e) {
					logger.error("Kafka Parse : " + value, e);
				}
			}
			break;
		}
		return data;
	}
	
	private void updateProductMapping(int prodId, int merchantId, String name, String dataServer) throws SQLException {
		DataObjectBean dObj = DATA_OBJ.get(dataServer);
		int botId = dObj.getProductDataDB().getIdByMerchantIdName(merchantId, name);
		ProductMappingBean mapB = mapDB.getByProdId(prodId);
		
		if(botId < 1) {
			logger.warn("not found product : " + prodId);
			if(mapB != null) {
				logger.info("Delete : "+ prodId);
				mapDB.deleteByProdId(prodId);
			}
		}else {
			if(mapB == null) {
				logger.info("Add : "+ prodId);
				mapB = new ProductMappingBean();
				mapB.setBotId(botId);
				mapB.setMerchantId(merchantId);
				mapB.setProdId(prodId);
				mapDB.insert(mapB);
			}else {
				if(botId != mapB.getBotId()){
					logger.info("Update : "+ prodId);
					mapDB.update(botId, prodId);
				}else {
					logger.info("Duplicate : " + prodId);
				}
			}
		}
	}
	
}
