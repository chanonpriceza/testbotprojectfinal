package botbackend.schedule;

import java.io.IOException;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.parser.ParseException;

import botbackend.utils.ACCPropertyParser;

import botbackend.bean.api.LnwShopMerchantBean;
import botbackend.bean.api.LnwShopProductDataBean;
import botbackend.db.api.LnwShopMerchantDB;
import botbackend.db.api.LnwShopProductDataDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.service.aws.SimpleEmail;
import botbackend.utils.Util;



public class APILnwShopFetchLnwMerchantIdSchedule {
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser propConfig;
	private DataSource webDataSource;
	
	private String EMAIL_TO;
	private String PROCESS_NAME = "LnwShop_API_FetchMerchantId";
	private LnwShopMerchantDB merchantDB;
	private LnwShopProductDataDB productDB;
	
	public static void main(String args []){
		APILnwShopFetchLnwMerchantIdSchedule process = null;
		try {			
			if(args.length == 0) {
				System.out.println("no configuration file argument");
				return;
			}
			process = new APILnwShopFetchLnwMerchantIdSchedule(args[0]);
			process.initailize();
			process.run();
		} catch (Exception e) {
			logger.error("APILnwShopFetchLnwMerchantIdSchedule Error", e);			
			e.printStackTrace();
		} finally {
			if(process != null) {
				process.cleanUp();	
			}					
		}       
	}

	public APILnwShopFetchLnwMerchantIdSchedule(String configFile) throws ConfigurationException{
			
		if(configFile == null || configFile.trim().length() == 0) {
			throw new ConfigurationException("no config file");
		}		
		propConfig = new ACCPropertyParser(configFile);		
	}
	
	public void cleanUp(){
		if(webDataSource != null) {
			try {
				DatabaseUtil.closeDataSource(webDataSource);
			} catch (SQLException e) {				
			}
		}	
		
	}
	public void initailize(){
		String file = propConfig.getString("log4j-config-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    String webDBDriverName = propConfig.getString("web.db.driver-name");
		String webDBURL = propConfig.getString("web.db.url");
		String webDBUserName = propConfig.getString("web.db.username");
		String webDBPassword = propConfig.getString("web.db.password");
		EMAIL_TO = propConfig.getString("email.admin");
	    webDataSource = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
	    productDB = new LnwShopProductDataDB(webDataSource);
	    merchantDB = new LnwShopMerchantDB(webDataSource);
	    
	}
	
	public void run() throws SQLException, ParseException, IOException{
      try{
    	  	
    	  	logger.info("Start API LnwShop Fetch lnwMerchantId process ...");	
    	  	getData();
    	  	logger.info("End API LnwShop Fetch lnwMerchantId process.");	
		
      }catch (Exception e) {
    	 sendErrorEmail(e);
      }
	}
	
	public void getData() throws SQLException {
		try{
			List<LnwShopProductDataBean> productList = productDB.getAllMerchantAndSampleUrl();
			if(productList != null && productList.size() > 0){
				List<LnwShopMerchantBean> merchantList = new ArrayList<LnwShopMerchantBean>();
				for (LnwShopProductDataBean productBean : productList) {
					
					String lnwMerchantId = productBean.getLnw_merchant_id();
					String merchantName = productBean.getProduct_url();
					merchantName = Util.getStringAfter(merchantName, "http", merchantName);
					merchantName = Util.getStringAfter(merchantName, "//", merchantName);
					merchantName = Util.getStringAfter(merchantName, "www.", merchantName);
					merchantName = Util.getStringBefore(merchantName, "/", merchantName);
					merchantName = Util.getStringBefore(merchantName, ".com", merchantName);
					merchantName = Util.getStringBefore(merchantName, ".co.", merchantName);
					merchantName = Util.getStringBefore(merchantName, ".net", merchantName);
					merchantName = Util.getStringBefore(merchantName, ".lnwshop", merchantName);
					merchantName = Util.getStringBefore(merchantName, ".", merchantName);
					
					LnwShopMerchantBean merchantBean = new LnwShopMerchantBean();
					merchantBean.setLnw_id(lnwMerchantId);
					merchantBean.setName(merchantName);
					
					merchantList.add(merchantBean);
				}
				if(merchantList != null && merchantList.size() > 0) merchantDB.insertBatchWithLnwId(merchantList); 
			}
		}catch(Exception e){
			logger.error("APILnwShopFetchLnwMerchantIdSchedule Error", e);
		}
	}
	
	public void sendErrorEmail(Exception e){
		 logger.error( PROCESS_NAME + " process Fail ", e);
			
			String accessKey = propConfig.getString("awsses.accesskey");
			String secretKey = propConfig.getString("awsses.secretkey");					
			SimpleEmail sEmail = new SimpleEmail(accessKey, secretKey);
			try {
				sEmail.sendMail(BaseSchedule.ERROR_EMAIL_SUBJECT + " : " + PROCESS_NAME + " : " + BaseSchedule.now(), 
						PROCESS_NAME + " " + e.toString(), 
						BaseSchedule.EMAIL_FROM, 
						EMAIL_TO, "APILnwShopFetchLnwMerchantIdSchedule", "sendErrorEmail");
			} catch (Exception e1) { 
				
			}  
		
	}
			
}
