package botbackend.schedule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.bean.BotQualityReportBean;
import botbackend.bean.BotQualitySummaryBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantRuntimeReportBean;
import botbackend.bean.ServerRuntimeReportBean;
import botbackend.bean.TrackMerchantStatisticBean;
import botbackend.db.BotQualityReportDB;
import botbackend.db.BotQualityReportHistoryDB;
import botbackend.db.MerchantDB;
import botbackend.db.MerchantRuntimeReportDB;
import botbackend.db.ServerRuntimeReportDB;
import botbackend.db.TrackMerchantStatisticDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ACCPropertyParser;
import botbackend.utils.Util;
import botbackend.utils.ViewUtil;
import utils.BotUtil;
import utils.DateTimeUtil;



public class BotQualityReportSchedule extends Schedule {

	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser propConfig;
	
	private DataSource prodDataSource;
	private DataSource botDataSource;
	
	private TrackMerchantStatisticDB trackMerchantStatisticDB;
	private MerchantDB botMerchantDB;
	private ServerRuntimeReportDB serverReport;
	private MerchantRuntimeReportDB  reportDB;
	private BotQualityReportDB botQualityReportDB;
	private BotQualityReportHistoryDB botQualityReportHistoryDB;

	
	private List<String> COUNTRY_LIST ;
	
	private static List<Integer> notUpdateListFamily; //0,10,13
	private static List<Integer> waitFixFamily; //2,3,4,6,7,8,9,14,15
	private static List<Integer> lessThanOneDayFamily; //5,11,12
	
	private static List<String> allowCalculateStatus; //"PASSED","WARNING"
	
	private static final String REPORT_TYPE = "DUE";
	private static final String[] NOT_SELECT_TYPE = new String[] {"RUNNING","KILLED"};
	
	private static final String MERCHANT = "Merchant";
	private static final String SERVER = "Server";
	private static final  String SUMMARY = "Summary";
	
	private List<BotQualityReportBean> insertList;
	private Map<String, ServerRuntimeReportBean> serverList;
	
	
	public static void main(String[] args) {
		Schedule process = null;
		try {	
			
			if(args.length != 1) {
				logger.error("Must have : <bot-backend-configuration> ");
				return;
			}

			process = new BotQualityReportSchedule(args);
			process.initialize();
			process.start();
		} catch (Exception e) {
			logger.error("BotQualityReportSchedule Error", e);			
			e.printStackTrace();
		} finally {
			if(process != null) {
				((BotQualityReportSchedule) process).cleanUp();	
			}					
		}       
	}
	
	@Override
	public void initialize() {
		
		String file = propConfig.getString("log4j-bot-quality-config-file");
	    if(file != null) {
	    	PropertyConfigurator.configure(file);
	    }
	    setUpEmail(propConfig, "BotQualityReportSchedule", "GENERATE_BOT_QUALITY_REPORT");
	    
		
	    COUNTRY_LIST = propConfig.getList("report-botquality-country-list");

	    notUpdateListFamily = Arrays.asList(new Integer[] {0,10,13});
	    waitFixFamily = Arrays.asList(new Integer[] {2,3,4,6,7,8,9,14,15});
	    lessThanOneDayFamily = Arrays.asList(new Integer[] {5,11,12});
	    insertList = new ArrayList<BotQualityReportBean>();
	    allowCalculateStatus = Arrays.asList(new String[]{"PASSED","WARNING"});
		
	}
	
	private BotQualityReportSchedule(String[] args) throws ConfigurationException {
		
		if(args[0] == null || args[0].trim().length() == 0) {
			throw new ConfigurationException("<bot-backend-configuration>");
		}	
		
		propConfig = new ACCPropertyParser(args[0]);	
	}
	
	private void initializeConnection(String country) {
		
		String bbeDBDriverName = propConfig.getString("bot."+country+".backend.db.driver-name");
	  	String bbeDBURL 	   = propConfig.getString("bot."+country+".backend.db.url");
	  	String bbeDBUserName   = propConfig.getString("bot."+country+".backend.db.username");
	  	String bbeDBPassword   = propConfig.getString("bot."+country+".backend.db.password");
		
		botDataSource = DatabaseUtil.createDataSource(bbeDBURL, bbeDBDriverName, bbeDBUserName, bbeDBPassword, 3);
		botMerchantDB = new MerchantDB(botDataSource);
		serverReport = new ServerRuntimeReportDB(botDataSource);
		
		botQualityReportDB = new BotQualityReportDB(botDataSource);
		botQualityReportHistoryDB = new BotQualityReportHistoryDB(botDataSource);
		reportDB = new MerchantRuntimeReportDB(botDataSource);

		prodDataSource = getWebDataSource(propConfig, country);
		trackMerchantStatisticDB = new TrackMerchantStatisticDB(prodDataSource);
	}

	@Override
	public void start() {
		try {
			for(String country:COUNTRY_LIST) {
				initializeConnection(country);
				generateReport();
			}
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
	
	private void generateReport() throws Exception {
		
		BotQualitySummaryBean botQualitySummary = new BotQualitySummaryBean();
		List<MerchantBean> merchantBeans = getAllMerchant();
		serverList = generateServerRuntimeMapping();
		botQualityReportDB.clearAllData();
		for(MerchantBean merchant:merchantBeans) {

			if(hasNextTime(merchant)) {
				calculateMerchantWithReport(merchant,botQualitySummary);
			}else {
				calculateMerchantWithServerReport(merchant,botQualitySummary);
			}
		}
		
		writeSummaryReport(botQualitySummary);
		writeSummaryServerReport();
		writeSummaryMerchantReport();
		botQualityReportHistoryDB.insertReport();
	}
	
	public List<MerchantBean> getAllMerchant(){
		List<MerchantBean> allMerchant = new ArrayList<>();
		Map<Integer, Integer>  countProductMapping = generateCountProductMapping();
		try{
			for(Entry<Integer, Integer> m:countProductMapping.entrySet()) {
				MerchantBean bean = botMerchantDB.getMerchantByMerchantId(m.getKey());
				if(bean==null) {
					continue;
				}
				bean.setCountWebProduct(m.getValue());
				allMerchant.add(bean);
			}
			countProductMapping.clear();
		}catch(Exception e){
			e.printStackTrace();
		}
		return allMerchant;
	}
	
	private static boolean hasNextTime(MerchantBean merchant) {
		boolean result = true;
		
		if((merchant.getWceNextStart()==null)||(merchant.getDueNextStart()==null)) {
			result = false;
		}
		
		return result;
	}
	
	private void calculateMerchantWithReport(MerchantBean bean,BotQualitySummaryBean summary) throws Exception {
		
		int active = bean.getActive();
		int merchantId  = bean.getId();
		int hourInt = 0;
		int productCount = bean.getCountWebProduct();

		String timeSign = "Other";
		summary.increaseMerchantAll();
		summary.addProductAll(productCount);
		if(active==1) {
			
			MerchantRuntimeReportBean report = reportDB.getReportByStatusAndType(NOT_SELECT_TYPE,REPORT_TYPE,merchantId);
			
			if(checkReport(report)) {
				
				Date startDate = report.getDefineDate();
				Date endDate = report.getEndDate();
				
				if(startDate == null)
					startDate = report.getStartDate();
				String runTimeStr = (startDate!=null&&endDate!=null)? DateTimeUtil.calcurateHourDiff(startDate,endDate):"0";
				
				String hourStr = Util.getStringBefore(runTimeStr, "h", "0");
				hourInt = BotUtil.stringToInt(hourStr,0);
				
				if(hourInt < 24){                                                                                                            
					  summary.addProductUpdateIn1Day(productCount);   
					  summary.increaseMerchantUpdateIn1Day();
					  timeSign = "<1Day";
				}else if(hourInt < 72){                                                                                                            
					summary.addProductUpdateIn3Day(productCount);      
					summary.increaseMerchantUpdateIn3Day();
					timeSign = "<3Day";
				}else{
					summary.addProductUpdateMore3Day(productCount);  
					summary.increaseMerchantUpdateMore3Day();
					timeSign = ">3Day";
				}
			}else {
				logger.error("Merchant Id: "+bean.getId());
			}
			
		}else if(lessThanOneDayFamily.contains(active)) {
			summary.increaseMerchantUpdateIn1Day();
			summary.addProductUpdateIn1Day(productCount);
			timeSign = "<1Day";
		}else if(waitFixFamily.contains(active)) {
			summary.increaseMerchantWaitingFix();
			summary.addProductWaitToFix(productCount);
		}
		else if(notUpdateListFamily.contains(active)) {
			summary.increaseNotUpdate();
			summary.addProductNotUpdate(productCount);
		}
		else { 
			summary.increaseNotUpdate();
			summary.addProductNotUpdate(productCount);
		}
		BotQualityReportBean insert = new BotQualityReportBean(MERCHANT, bean.getId(), bean.getName(), (ViewUtil.generateShowActive(bean.getActive())), StringUtils.defaultString(bean.getServer()) ,timeSign,productCount , new Date());
		insertList.add(insert);
	}
	
	private void calculateMerchantWithServerReport(MerchantBean bean,BotQualitySummaryBean summary) throws Exception {

		int active = bean.getActive();
		String timeSign = "Other";
		String server = bean.getServer();
		ServerRuntimeReportBean serverReport = serverList.getOrDefault(server,null);
		String hourStr = serverReport!=null?Util.getStringBefore(serverReport.getDiffDate(), "h", "0"):"0";
		int hourInt = Integer.parseInt(hourStr);
		int productCount = bean.getCountWebProduct();
		summary.increaseMerchantAll();
		
		if(serverList!=null&&serverReport!=null) {
			serverReport.setSuccessCount(serverReport.getSuccessCount()+productCount);
			serverList.put(serverReport.getServer(), serverReport);
			summary.addProductAll(productCount);
			if(active==1) {
					if(hourInt < 24){                                                                                                            
						  summary.addProductUpdateIn1Day(productCount);   
						  summary.increaseMerchantUpdateIn1Day();
						  timeSign = "<1Day";
					}else if(hourInt < 72){                                                                                                            
						summary.addProductUpdateIn3Day(productCount);      
						summary.increaseMerchantUpdateIn3Day();
						timeSign = "<3Day";
					}else{
						summary.addProductUpdateMore3Day(productCount);  
						summary.increaseMerchantUpdateMore3Day();
						timeSign = ">3Day";
					}
				
			}else if(lessThanOneDayFamily.contains(active)) {
				summary.increaseMerchantUpdateIn1Day();
				summary.addProductUpdateIn1Day(productCount);
				timeSign = "<1Day";
			}else if(waitFixFamily.contains(active)) {
				summary.increaseMerchantWaitingFix();
				summary.addProductWaitToFix(productCount);
			}else if(notUpdateListFamily.contains(active)) {
				summary.increaseNotUpdate();
				summary.addProductNotUpdate(productCount);
			}else {
				summary.increaseNotUpdate();
				summary.addProductNotUpdate(productCount);
			}
		}else {
			logger.error("Merchant Id: "+bean.getId());
		}
		
		BotQualityReportBean insert = new BotQualityReportBean(MERCHANT, bean.getId(), bean.getName(), (ViewUtil.generateShowActive(bean.getActive())), StringUtils.defaultString(bean.getServer()) ,timeSign,productCount , new Date());
		insertList.add(insert);
	}
	
	private void writeSummaryReport(BotQualitySummaryBean summary) throws Exception{
		
		List<BotQualityReportBean> listSummary = new  ArrayList<BotQualityReportBean>();
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant all",null, null,null,summary.getMerchantAll(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant not update",null, null,null,summary.getMerchantNotUpdate(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant wait to fix",null, null,null,summary.getMerchantWaitFix(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant update within 1Day",null, null,null,summary.getMerchantUpdateIn1Day(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant update within 3Day",null, null,null,summary.getMerchantUpdateIn3Day(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Merchant update more than 3Day",null, null,null,summary.getMerchantUpdateMore3Day(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product all",null, null,null,summary.getProductAll(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product not update",null, null,null,summary.getProductNotUpdate(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product wait to fix",null, null,null,summary.getProductWaitToFix(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product update within 1Day",null, null,null,summary.getProductUpdateIn1Day(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product update within 3Day",null, null,null,summary.getProductUpdateIn3Day(),new Date()));
		listSummary.add(new BotQualityReportBean(SUMMARY,0,"Product update more than 3Day",null, null,null,summary.getProductUpdateMore3Day(),new Date()));
		botQualityReportDB.insertReport(listSummary);
		listSummary.clear();

	}
	
	private void writeSummaryServerReport() throws Exception{
		List<BotQualityReportBean> insertServerList = new  ArrayList<BotQualityReportBean>();
		if(serverList==null)
			return;
		for(Entry<String,ServerRuntimeReportBean> server:serverList.entrySet()) {
			insertServerList.add(new BotQualityReportBean(SERVER, 0, null, null, server.getKey().toString(),server.getValue().getDiffDate(),server.getValue().getSuccessCount(), new Date()));
		}
		botQualityReportDB.insertReport(insertServerList);
		insertServerList.clear();
	}
	
	public Map<Integer, Integer> generateCountProductMapping(){
		Map<Integer, Integer> merchantCountProductMap = new LinkedHashMap<>();
		try{
			List<TrackMerchantStatisticBean> latestMerchantStatisticList = trackMerchantStatisticDB.getLatestRecord();
			if(latestMerchantStatisticList == null || latestMerchantStatisticList.size() == 0) return null;
			for (TrackMerchantStatisticBean trackMerchantStatisticBean : latestMerchantStatisticList) {
				merchantCountProductMap.put(trackMerchantStatisticBean.getMerchant_id(), trackMerchantStatisticBean.getSolr_product());
			}
			
		}catch(Exception e){
			logger.error("generateBotQualityReport Error : generateCountProductMapping()", e);
			e.printStackTrace();
		}
		return merchantCountProductMap;
	}
	
	private void writeSummaryMerchantReport() throws Exception {
		botQualityReportDB.insertReport(insertList);
		insertList.clear();
	}
	
	public Map<String,ServerRuntimeReportBean> generateServerRuntimeMapping() {
		Map<String, ServerRuntimeReportBean> serverRuntimeMap = new LinkedHashMap<>();
		try{
			List<ServerRuntimeReportBean> latestServerReportList = serverReport.getLastestRuntimeAllServer("DONE", REPORT_TYPE);
			if(latestServerReportList == null || latestServerReportList.size() == 0) return null;
			for(ServerRuntimeReportBean serv:latestServerReportList) {
				serv.setDiffDate();
				serverRuntimeMap.put(serv.getServer(),serv);
			}
			
		}catch(Exception e){
			logger.error("generateBotQualityReport Error : generateServerRuntimeMapping()", e);
			e.printStackTrace();
		}
		return serverRuntimeMap;
	}
	
	public void cleanUp(){
		cleanUp(new DataSource[]{botDataSource, prodDataSource});
	}
	
	private boolean checkReport(MerchantRuntimeReportBean report) {
		
		boolean result = true;
		
		if(report==null) 
			return false;
		
		if(!allowCalculateStatus.contains(report.getStatus())) {
			return  false;
		};
		
		Date startDate = report.getDefineDate();
		Date endDate = report.getEndDate();
		
		if(startDate == null)
			startDate = report.getStartDate();
		
		if(startDate==null||endDate==null)
			return false;
		
		return result ;
	}
	


			
}
