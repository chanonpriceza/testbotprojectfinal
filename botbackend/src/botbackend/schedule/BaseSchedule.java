package botbackend.schedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BaseSchedule {
    private static String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
    private static String DATE_FORMAT_NAME = "yyyyMMddHHmmss";
    public static String LOG_FILENAME ="BotDataSync.log";
    
	public static String now() {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	    return sdf.format(cal.getTime());
	}
	public static String time_for_name_extension() {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NAME);
	    return sdf.format(cal.getTime());
	}
	
	public static final String ERROR_EMAIL_SUBJECT ="!!!!! Priceza Schedule Error !!!!!";
	public static final String EMAIL_FROM ="swirod@priceza.com";
	public static final String EMAIL_TO ="backend-report-th@priceza.com";
	
	//for migration to new server
	public static final String SECOND_PIC_PATH = "/home/image/product_pic_update/";
    
}
