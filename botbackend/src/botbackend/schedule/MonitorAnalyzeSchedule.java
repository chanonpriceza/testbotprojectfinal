package botbackend.schedule;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.parser.ParseException;

import bean.MerchantConfigBean.FIELD;
import bean.ParserConfigBean;
import bean.PatternResultBean;
import bean.ProductDataBean;
import botbackend.bean.BotConfigBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.MerchantConfigBean;
import botbackend.bean.MonitorReportBean;
import botbackend.bean.UrlPatternBean;
import botbackend.bean.common.WorkLoadFeedCommonBean;
import botbackend.db.BotConfigDB;
import botbackend.db.MerchantConfigDB;
import botbackend.db.MerchantDB;
import botbackend.db.MonitorDB;
import botbackend.db.ParserConfigDB;
import botbackend.db.ProductDataDB;
import botbackend.db.UrlPatternDB;
import botbackend.db.WorkLoadFeedDB;
import botbackend.db.utils.DatabaseUtil;
import botbackend.utils.ACCPropertyParser;
import botbackend.utils.ParserUtil;
import botbackend.utils.Util;
import manager.UrlPatternManager;
import utils.BotUtil;
import utils.FilterUtil;
import utils.HTTPUtil;

public class MonitorAnalyzeSchedule extends Schedule implements Runnable {
	
	private static Logger logger = Logger.getLogger("priceza.schedule");
	private String PROCESS = "Monitor";
	private String PROCESS_NAME = "MonitorSchedule";

	private Map<String, ProductDataDB> mProductDataDB = new HashMap<String, ProductDataDB>();
	private Map<String, WorkLoadFeedDB> mWorkLoadFeedDB = new HashMap<String, WorkLoadFeedDB>();
	private final int THREAD_TIME_OUT = 15*60;
	private final int CHECK_URL_MAX_TIME_REQUEST = 5*60*1000;
	private final int MAXTIME_PARSER_URL_REQUEST = 5*60;
	private final int MAX_EXECUTOR_WAITING_TIME = 23;
	private final int THREAD_NUM = 5;
	private MonitorReportBean monitorReport;
	
	static private Map<String, DataSource> listDsBotBackend;
	static private MerchantDB merchantDB; 
	static private UrlPatternDB urlDB; 
	static private BotConfigDB botConfigDB; 
	static private MerchantConfigDB mcDB; 
	static private ParserConfigDB pcDB; 
	static private MonitorDB mtDB; 
	static private db.UrlPatternDB urlPatternDB;
	private static Map<String, DataSource> mDataSource = new HashMap<String, DataSource>();
	
	private MerchantBean merchantBean;
	private ACCPropertyParser baseConfig;
	
	
	public enum STATUS {
		
		OK, NAMEERROR, PRICEERROR, ERRORPARSE, NAMECHANGE, URLCHANGE, 
		
		SUCCESS,EXCEPTION,SSL,SOCKETTIMEOUT,NOPRODUCTDATA,CANTFINDDUEWORKLOAD,NOPARSERCLASS,
		
		CANTFINDWCEWORKLOAD,CANTFINDWCEANDDUEWORKLOAD,RUNTIMEERROR,CANTFINDSTARTURL,STARTURLCANTNOTUSE,ISFACEBOOKPARSER;



	}
	
	
	public static void main(String[] args) {
		Schedule process;
		try {
			process = new MonitorAnalyzeSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public MonitorAnalyzeSchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
		
		baseConfig = new ACCPropertyParser(configFile[0]);
	}
	
	public MonitorAnalyzeSchedule(MerchantBean merchantBean) throws ConfigurationException{
		this.merchantBean = merchantBean;
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-analyzer-config-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
		setUpEmail(baseConfig, PROCESS, PROCESS_NAME);
	}

	@Override
	public void start() {
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUM);
		try {
			List<String> contryList = baseConfig.getList("bot.server-list-country");
			
			for (String country : contryList) {
				country = country.toLowerCase();
		    	if(listDsBotBackend==null){
		    		listDsBotBackend = new HashMap<>();
		    	}
		    	DataSource tmp = getBotBackendDataSource(baseConfig, country.toLowerCase());
		    	listDsBotBackend.put(country,tmp);
				BotUtil.LOCALE = country.toLowerCase();
				DataSource dsBotBackend = listDsBotBackend.get(country);
				merchantDB = new MerchantDB(dsBotBackend);
				urlDB = new UrlPatternDB(dsBotBackend);
				botConfigDB = new BotConfigDB(dsBotBackend);
				mcDB = new MerchantConfigDB(dsBotBackend);
				pcDB = new ParserConfigDB(dsBotBackend);
				mtDB = new MonitorDB(dsBotBackend);
				urlPatternDB = new db.UrlPatternDB(dsBotBackend);
				
				List<MerchantBean> merchantAnalyList = merchantDB.getMerchantByActive(14);
				for(int i = 0;i<merchantAnalyList.size();i++){
					MerchantBean merchant = merchantAnalyList.get(i);
					merchant.setActive(1);
					merchantDB.updateNewMerchant(merchant);
				}
				
				List<MerchantBean> merchantErrorList = merchantDB.getMerchantByActive(2);
				Map<String,Future<?>> futures = new HashMap<String,Future<?>>();
				Map<String,MerchantBean> merchantDataList = new HashMap<String,MerchantBean>();
				for(int i = 0;i<merchantErrorList.size();i++){
					MerchantBean merchant = merchantErrorList.get(i);
					merchant.setIdServername(merchant.getId()+"-"+merchant.getServer());
					Future<?>  f = executor.submit(new MonitorAnalyzeSchedule(merchant));
					futures.put(merchant.getIdServername(),f);
					merchantDataList.put(merchant.getIdServername(),merchant);
					
				}
				for(String key : futures.keySet()){
						Future<?> future = futures.get(key);
						MerchantBean merchantData =  merchantDataList.get(key);
						
					try{
						future.get(THREAD_TIME_OUT,TimeUnit.SECONDS);
					}catch (TimeoutException e) {
						try{
							future.cancel(true);
							monitorReport.setResult(STATUS.RUNTIMEERROR);
							updateMonitorDB(merchantData);
						}catch (CancellationException  e2) {
							e2.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			for (Entry<String, DataSource> e: mDataSource.entrySet()) {
				cleanUp(e.getValue());
			}
			for (Entry<String, DataSource> e: listDsBotBackend.entrySet()) {
				cleanUp(e.getValue());
			}
			mProductDataDB.clear();
			mWorkLoadFeedDB.clear();
			mDataSource.clear();
			listDsBotBackend.clear();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(MAX_EXECUTOR_WAITING_TIME,TimeUnit.HOURS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
		if(!executor.isShutdown()) {
			executor.shutdown();
		}
		logger.info("End Monitor Analyze process : ");
	}

	private boolean testCheckRequest(MerchantBean merchantBean, UrlPatternDB urlDB, BotConfigBean botConfigBean,String crawlerClass) {
		try {
			if (StringUtils.isBlank(crawlerClass)) {
				List<UrlPatternBean> startUrlList = urlDB.findUrlPatternWithLimit(merchantBean.getId(), "STARTURL", 1);
				if (startUrlList == null || startUrlList.size() < 1) {
					monitorReport.setResult(STATUS.CANTFINDSTARTURL);
					monitorReport.setExampleData("Detail","STARTURL NOT FOUND");
					monitorReport.putTestPhase("FAIL - check StartUrl at testCheckRequest");
					return true;
				}
				monitorReport.putTestPhase("PASS - check StartUrl available at testCheckRequest");
				
				String startUrl = null;
				String[] httpStatus = null;
				if (startUrlList != null && startUrlList.size() > 0) {
					startUrl = startUrlList.get(0).getValue();
					monitorReport.setExampleData("testCheckRequestStartUrl",startUrl);
					startUrl = genMainUrl(startUrl);
					httpStatus = Util.httpRequestWithStatusOnly(startUrl, "UTF-8", true, CHECK_URL_MAX_TIME_REQUEST, CHECK_URL_MAX_TIME_REQUEST);
				}
				if (httpStatus == null) {
					monitorReport.setResult(STATUS.STARTURLCANTNOTUSE);
					monitorReport.putTestPhase("FAIL - test request startUrl response return null at testCheckRequest, url: "+startUrl);
					return true;
				}
				
				monitorReport.putTestPhase("PASS - test request startUrl response at testCheckRequest, return httpStatus:"+httpStatus[1]);
				
				for (int i = 0; i < 5; i++) {
					if (httpStatus.length == 2 && httpStatus[1].startsWith("3")) {
						
						httpStatus = Util.getRedirectLink(startUrl);
						startUrl = httpStatus[0];
						
						httpStatus = Util.httpRequestWithStatusOnly(startUrl, "UTF-8", true,CHECK_URL_MAX_TIME_REQUEST, CHECK_URL_MAX_TIME_REQUEST);
						
						monitorReport.putTestPhase("Go follow redirect link "+startUrl+" at testCheckRequest ");

					}
					if("200".equals(httpStatus[1])) {
						break;
					}
				}

				if (httpStatus.length == 2 && (!httpStatus[1].equals("200"))) {
					monitorReport.setExampleData("url",startUrl);
					monitorReport.setExampleData("httpStatus",httpStatus[1]);

					if (STATUS.SSL.toString().equals(httpStatus[1])) {
						monitorReport.setResult(STATUS.SSL);
					} else if (STATUS.SOCKETTIMEOUT.toString().equals(httpStatus[1])) {
						monitorReport.setResult(STATUS.SOCKETTIMEOUT);
					} else {
						monitorReport.setResult(STATUS.EXCEPTION);
					}
					
					monitorReport.putTestPhase("FAIL - check http Status at testCheckRequest, status:..., url:"+startUrl);
					
					return true;
				}
				
				
			} else {
				
				monitorReport.putTestPhase("SKIP - testCheckRequest because this merchant use urlCrawlerClass");
				
				return false;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		monitorReport.putTestPhase("FINISH - testCheckRequest");
		return false;
		
	}
	
	private String genMainUrl(String url){
		String result = "";
		try {
			URL aURL = new URL(url);
			result = aURL.getProtocol()+"://"+aURL.getHost();
		} catch (MalformedURLException e) {
			return url;
		}
		return result;
		
	}
	
	private boolean checkUrl(MerchantBean merchantBean, Properties mConfig, List<ProductDataBean> prdBeanList, String disableParserCookies) throws Exception {
		String urlCrawlerClass = mConfig.getProperty("urlCrawlerClass");
		if (StringUtils.isBlank(urlCrawlerClass)) {
			List<UrlPatternBean> startUrlList = urlDB.findUrlPatternWithLimit(merchantBean.getId(), "STARTURL",10);
			List<String> macthList = new ArrayList<String>();
			List<String> continueList = new ArrayList<String>();
			UrlPatternManager urlPatternManager = new UrlPatternManager(urlPatternDB, merchantBean.getId());
			if (startUrlList.size() == 0) {
				
				monitorReport.setResult(STATUS.CANTFINDSTARTURL);
				monitorReport.putTestPhase("FAIL - check StartUrl at checkUrl");
				
				return true;
			}
			
			monitorReport.putTestPhase("PASS - check StartUrl at checkUrl");

			List<String> startUrlListString = new ArrayList<String>();
			
			for (UrlPatternBean url : startUrlList) {
				String request = BotUtil.encodeURL(url.getValue());
				String[] response = HTTPUtil.httpRequestWithStatus(request, "UTF-8", true);
				if(response==null){
					monitorReport.setResult(STATUS.STARTURLCANTNOTUSE);
					monitorReport.putTestPhase("FAIL - test request startUrl response return null at testCheckRequest, url:"+request);
					return true;
				}
				
				monitorReport.putTestPhase("PASS - test request startUrl response return null at testCheckRequest, url:"+request);
				
				String body = response[0];
				String status = response[1];
				if(!status.equals("200")){
					monitorReport.setResult(STATUS.STARTURLCANTNOTUSE);
					monitorReport.putTestPhase("FAIL - request StartUrl response staus = "+status+"at checkUrl  url :"+Util.decodeUrl(request));
					return true;
				}
				
				monitorReport.putTestPhase("PASS - request StartUrl at checkUrl");
				
				List<String> innerLink = FilterUtil.getAllStringBetween(body, "href=\"", "\"");
				innerLink.addAll(FilterUtil.getAllStringBetween(body, "href=\'", "\'"));
				if (innerLink == null||innerLink.size()==0) {
					continue;
				}
				for (String link : innerLink) {
					PatternResultBean resultMacth = urlPatternManager.isPatternMatch(link);
					PatternResultBean resultContinue = urlPatternManager.isPatternContinue(link);
					if (resultMacth.isPass()) {
						macthList.add(link);
					}
					if (resultContinue.isPass()) {
						continueList.add(link);
					}
					if (continueList.size() > 0 || macthList.size() > 0) {
						monitorReport.putTestPhase("PASS - Map Continue link or Macth link at checkUrl");
						return false;
					}
					
				}
				startUrlListString.add(request);
			}
			monitorReport.setExampleData("checkUrlListRequestStartUrl",startUrlListString.toString());
			startUrlListString.clear();
			monitorReport.setResult(STATUS.STARTURLCANTNOTUSE);
		}else{
			monitorReport.putTestPhase("SKIP - testCheckRequest because this merchant use urlCrawlerClass");
			return false;
		}
		monitorReport.putTestPhase("FAIL - Map Continue link or Macth link at checkUrl");
		return true;
	}

	private boolean checkParser(BotConfigBean botConfigBean, MerchantBean merchantBean, Properties mConfig, List<ParserConfigBean> pList, List<ProductDataBean> prdBeanList) throws Exception {
		int countNameError = 0;
		int countPriceError = 0;
		int countErrorParse = 0;
		int countNameChange = 0;
		int countUrlChange = 0;
		
		int countSuccess = 0;
		int countExpire = 0;
		int languageError = 0;
		boolean checkData = false;
		String parserClass = mConfig.getProperty("parserClass");
		
		if(parserClass!=null) {
			try{
				@SuppressWarnings("unused")
				Class<?> c1 = Class.forName(parserClass);
			}catch (ClassNotFoundException e) {
				monitorReport.setResult(STATUS.EXCEPTION);
				monitorReport.putTestPhase("FAIL - ClassNotFound Exception parserClass "+parserClass+" check at CheckParser");
		
			return true;
			}
		}
		
		if (pList!=null && pList.size()>0) {
			long startFirstParse = 0 ; 
			for (int i = 0;i<prdBeanList.size();i++) {
				
				if(Thread.currentThread().isInterrupted()){
					monitorReport.putTestPhase("Thread is Interrupted at UserConfig checkParser");
					return false;
				}
				if(i==0){
					startFirstParse = System.currentTimeMillis();
				}
				
				ProductDataBean prdBean = prdBeanList.get(i);
				List<ProductDataBean> pBeanList = ParserUtil.parse(parserClass, pList, new String[]{prdBean.getUrlForUpdate()});
				boolean isError = true;
				if (pBeanList.size()>0) {
					
					String name = pBeanList.get(0).getName();
					String price = String.valueOf(pBeanList.get(0).getPrice());
					if (StringUtils.isBlank(name) && StringUtils.isBlank(price)) {
						monitorReport.setExampleData("countErrorParseUrl",Util.decodeUrl(prdBean.getUrlForUpdate()));
						countErrorParse++;
					}else  if (StringUtils.isBlank(name)) {
						monitorReport.setExampleData("countNameErrorUrl",Util.decodeUrl(prdBean.getUrlForUpdate()));
						countNameError++;
					}else  if (!prdBean.getName().equals(name)) {
						monitorReport.setExampleData("NameChangeOld",prdBean.getName());
						monitorReport.setExampleData("NameChangeNew",name);
						monitorReport.setExampleData("countNameChangeUrl",Util.decodeUrl(prdBean.getUrlForUpdate()));
						countNameChange++;
					}else  if (StringUtils.isBlank(price)) {
						monitorReport.setExampleData("countPriceErrorUrl",Util.decodeUrl(prdBean.getUrlForUpdate()));
						countPriceError++;
					}else if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(price)) {
						countSuccess++;
						isError = false;
					}else if(pBeanList.get(0).isExpire()){
						monitorReport.setExampleData("countExpireurl",Util.decodeUrl(prdBean.getUrlForUpdate()));
						countExpire++;
					} if(!FilterUtil.checkAllowCharacter(pBeanList.get(0).getName(),BotUtil.LOCALE.toLowerCase())){
						monitorReport.setExampleData("languageErrorUrl",Util.decodeUrl(prdBean.getUrlForUpdate()));
					}
					if(isError){
						monitorReport.setExampleData("parserClass","userConfig");
					}
					long diffTime = (System.currentTimeMillis()-startFirstParse)/1000;
					if(diffTime > MAXTIME_PARSER_URL_REQUEST){
						checkData=checkCurrentState(i+1,countNameChange,countPriceError,countSuccess,merchantBean,countUrlChange,countNameError,countErrorParse,countExpire,languageError,75);
						monitorReport.putTestPhase("FINISH - Timeout Request Url at UserConfig checkPraser");
						return checkData;
					}
				}
			}
		} 
		
		int total = prdBeanList.size();
		checkData = checkCurrentState(total,countNameChange,countPriceError,countSuccess,merchantBean,countUrlChange,countNameError,countErrorParse,countExpire,languageError,50);
		monitorReport.putTestPhase("FINISH - at checkParser");
		return checkData;
	}
	
	private boolean checkCurrentState(int total, int countNameChange, int countPriceError, int countSuccess, MerchantBean merchantBean, int countUrlChange, int countNameError, int countErrorParse, int expire, int language, int raito) throws ParseException {

		if (total == 0) {
			monitorReport.setResult(STATUS.NOPRODUCTDATA);
			monitorReport.putTestPhase("FAIL - check productdata available at checkParser");
			return true;
		}
		;
		monitorReport.setExampleData("total",String.valueOf(total));
		monitorReport.setExampleData("success",String.valueOf(countSuccess));

		if ((double) countNameError / total * 100 > raito) {
			monitorReport.setResult(STATUS.NAMEERROR);
			return true;
		} else if ((double) countNameChange / total * 100 > raito) {
			monitorReport.setResult(STATUS.NAMECHANGE);
			return true;
		} else if ((double) countPriceError / total * 100 > raito) {
			monitorReport.setResult(STATUS.PRICEERROR);
			return true;
		} else if ((double) countUrlChange / total * 100 > raito) {
			monitorReport.setResult(STATUS.URLCHANGE);
			return true;
		} else if ((double) countErrorParse / total * 100 > raito) {
			monitorReport.setResult(STATUS.ERRORPARSE);
			return true;
		} else {
			return false;
		}

	}
	
	private Properties loadMerchantConfig(int merchantId, MerchantConfigDB mcDB) throws SQLException {
		Properties rtn = new Properties();
		List<MerchantConfigBean> mcList = mcDB.findMerchantConfig(merchantId);

		for (MerchantConfigBean mcBean : mcList) {
			rtn.setProperty(mcBean.getField(), mcBean.getValue());
		}
		return rtn;
	}
	
	private WorkLoadFeedDB getWorkLoadFeedDB(String serverName, BotConfigBean botConfigBean) {
		DataSource dSource = mDataSource.get(serverName);
		if(dSource == null) {
			dSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
			mDataSource.put(serverName, dSource);
		}
		if (mWorkLoadFeedDB.get(serverName)==null) {
			mWorkLoadFeedDB.put(serverName, new WorkLoadFeedDB(dSource));
		}
		return mWorkLoadFeedDB.get(serverName);
	}
	
	private ProductDataDB getProductDataDB(String serverName, BotConfigBean botConfigBean) {
		DataSource dSource = mDataSource.get(serverName);
		if(dSource == null) {
			dSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
			mDataSource.put(serverName, dSource);
		}
		if (mProductDataDB.get(serverName)==null) {
			mProductDataDB.put(serverName, new ProductDataDB(dSource));
		}
		return mProductDataDB.get(serverName);
	}
	
	private List<WorkLoadFeedCommonBean> createSampleData(int merchantId, List<ProductDataBean> pdList, List<WorkLoadFeedCommonBean> workLoadfeedList) throws SQLException {
		
		List<WorkLoadFeedCommonBean> productDataList = new ArrayList<WorkLoadFeedCommonBean>();
		Map<String, WorkLoadFeedCommonBean> mapWlfBean = workLoadfeedList.stream().collect(Collectors.toMap(WorkLoadFeedCommonBean::getUrlForUpdate, x -> x));
		
		for (ProductDataBean pdbBean : pdList) {
			WorkLoadFeedCommonBean wfBean = new WorkLoadFeedCommonBean();
			if (mapWlfBean.get(pdbBean.getUrlForUpdate()) != null) {
				wfBean = mapWlfBean.get(pdbBean.getUrlForUpdate());
				wfBean.setOld_name(pdbBean.getName());
				wfBean.setOld_url(pdbBean.getUrl());
				wfBean.setOld_urlForUpdate(pdbBean.getUrlForUpdate());

			}else{
				wfBean.setName(pdbBean.getName()); 
				wfBean.setUrl(pdbBean.getUrl());
			}
			productDataList.add(wfBean);

		}
		return productDataList;
	}
	
	@Override
	public void run() {		
			try {
					try {

						merchantBean.setActive(15);
						merchantBean.setCurrentserver(merchantBean.getServer());
						int updateMerhcant = merchantDB.updateNewMerchant(merchantBean);
						if(updateMerhcant==0) {
							return;
						}
						monitorReport = new MonitorReportBean();
						monitorReport.setKey(merchantBean.getIdServername());
						BotConfigBean botConfigBean = botConfigDB.findBotConfig(merchantBean.getDataServer());
						Properties mConfig = loadMerchantConfig(merchantBean.getId(), mcDB);
						List<ParserConfigBean> pConfigList = pcDB.getByMerchantId(merchantBean.getId());
						ProductDataDB prodDB = getProductDataDB(merchantBean.getDataServer(), botConfigBean);
						List<ProductDataBean> prdBeanList = prodDB.getMerchantDataByIdLimit(merchantBean.getId(), 100);
						String parserClass = mConfig.getProperty("parserClass")==null?"":mConfig.getProperty("parserClass");
						String urlCrawlerClass = mConfig.getProperty("urlCrawlerClass")==null?"":mConfig.getProperty("urlCrawlerClass");
						
						if("com.ha.bot.parser.FacebookParser".equals(parserClass)){
							monitorReport.setResult(STATUS.ISFACEBOOKPARSER);
							monitorReport.setExampleData("Detail","This merchant is FacebookParser");
							updateMonitorDB();
							return;
						}
						monitorReport.putTestPhase("FINISH - Test isFacebookParser Phase");
						if (Thread.currentThread().isInterrupted()||(!"com.ha.bot.parser.DefaultFeedHTMLParser".equals(parserClass)&&testCheckRequest(merchantBean, urlDB, botConfigBean,urlCrawlerClass))) {
							if(Thread.currentThread().isInterrupted()){
								return;
							}
							updateMonitorDB();
							return;
						} else if (Thread.currentThread().isInterrupted()||(!"com.ha.bot.parser.DefaultFeedHTMLParser".equals(parserClass)&&checkUrl(merchantBean, mConfig, prdBeanList, mConfig.getProperty(FIELD.disableParserCookies.toString())))) {
							if(Thread.currentThread().isInterrupted()){
								return;
							}
							updateMonitorDB();
							return;
						} else if(Thread.currentThread().isInterrupted()||("com.ha.bot.parser.DefaultFeedHTMLParser".equals(parserClass)&&checkFeedCrawler(botConfigBean, merchantBean, mConfig, pConfigList, prdBeanList))){
							if(Thread.currentThread().isInterrupted()){
								return;
							}
							updateMonitorDB();
							return;
						}else if (Thread.currentThread().isInterrupted()||(!"com.ha.bot.parser.DefaultFeedHTMLParser".equals(parserClass)&&checkParser(botConfigBean, merchantBean, mConfig, pConfigList, prdBeanList))) {
							if(Thread.currentThread().isInterrupted()){
								return;
							}
							updateMonitorDB();
							return;
						}
						if(Thread.currentThread().isInterrupted()){
							return;
						}
						checkResult(botConfigBean, mConfig, pConfigList, prdBeanList);
						monitorReport.putTestPhase("FINISH - checkResult");
						monitorReport.setResult(STATUS.OK);
						updateMonitorDB();
						return;
					}catch(InterruptedException ie){
						ie.printStackTrace();
						return;
					}catch (Exception e) {
						e.printStackTrace();
						monitorReport.setResult(STATUS.EXCEPTION);
						monitorReport.setExampleData("detail",e.toString());
						updateMonitorDB();
						logger.error(merchantBean.getId()+" : cannot process this merchant.");
					}
			
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("There is a problem occuring in process : "+BotUtil.LOCALE);
			} 
			logger.info("End Monitor Analyze process : "+BotUtil.LOCALE);
		}
	
	private void updateMonitorDB(){
	
		monitorReport.mergeData();
		String jsonResult = StringUtils.isNotBlank(monitorReport.getJsonResult())?monitorReport.getJsonResult():"";

		
		try {
			mtDB.updateMonitorData(merchantBean.getId(),monitorReport.getResult(), merchantBean.getServer(),jsonResult);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		
	}
	private void updateMonitorDB(MerchantBean merchant){
		monitorReport.mergeData();
		String jsonResult = monitorReport.getJsonResult();
		try {
			mtDB.updateMonitorData(merchant.getId(),monitorReport.getResult(), merchant.getServer(),jsonResult);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	
}
	
	private boolean checkFeedCrawler(BotConfigBean botConfigBean, MerchantBean merchantBean, Properties mConfig, List<ParserConfigBean> pConfigList, List<ProductDataBean> prdBeanList) throws Exception{
			
		
			int countNameError = 0;
			int countPriceError = 0;
			int countErrorParse = 0;
			int countNameChange = 0;
			int countUrlChange = 0;
			int countSuccess = 0;
				
			int countExpire = 0;
			int languageError = 0;
			
			
			WorkLoadFeedDB wlfDB = getWorkLoadFeedDB(merchantBean.getDataServer(), botConfigBean);
			
			WorkLoadFeedCommonBean wceSample = wlfDB.getSampleDta("WCE", merchantBean.getId());
			WorkLoadFeedCommonBean dueSample = wlfDB.getSampleDta("DUE", merchantBean.getId());
			if(wceSample==null){
				monitorReport.setResult(STATUS.CANTFINDWCEWORKLOAD);
				monitorReport.putTestPhase("FAIL - wce wceSample is null check at checkFeedCrawler");
				return true;
			}else if(dueSample==null){
				monitorReport.setResult(STATUS.CANTFINDDUEWORKLOAD);
				monitorReport.putTestPhase("FAIL - due check dueSample is null at checkFeedCrawler");
				return true;
			}
			
			monitorReport.putTestPhase("PASS - WCE and DUE check available check at checkFeedCrawler");
			
			if(prdBeanList==null||prdBeanList.size()==0){
				monitorReport.setResult(STATUS.NOPRODUCTDATA);
				monitorReport.putTestPhase("FAIL - ProductData is null at checkFeedCrawler ");
				return true;
			}
			
			monitorReport.putTestPhase("PASS - ProductData check available check at checkFeedCrawler ");
			
			List<WorkLoadFeedCommonBean> workLoadfeedList = wlfDB.getRandomProductDataByIds("DUE", merchantBean.getId(), prdBeanList);
			workLoadfeedList = createSampleData(merchantBean.getId(), prdBeanList, workLoadfeedList);
			if(workLoadfeedList.size()==0){
				monitorReport.setResult(STATUS.CANTFINDDUEWORKLOAD);
				monitorReport.putTestPhase("FAIL - workLoadfeedList not found at checkFeedCrawler ");
				return true;
			}
			
			monitorReport.putTestPhase("PASS - workLoadfeedList available check at checkFeedCrawler ");
			
			for (WorkLoadFeedCommonBean wData : workLoadfeedList) {
				if(Thread.currentThread().isInterrupted()){
					monitorReport.putTestPhase("Thread is Interrupted at checkFeedCrawler ");
					return false;
				}
				if(wData.getOld_name()==null || wData.getOld_url()==null && wData.getName()!=null && wData.getUrl()!=null){
					monitorReport.setExampleData("countErrorParseUrl",Util.decodeUrl(wData.getUrl()));
					countErrorParse++;
				}
				else if (!wData.getOld_url().equals(wData.getUrl())) {
					monitorReport.setExampleData("countUrlChangeUrl",Util.decodeUrl(wData.getUrl()));
					countUrlChange++;
				} else if (!wData.getOld_name().equals(wData.getName())) {
					monitorReport.setExampleData("countNameChangeUrl",Util.decodeUrl(wData.getUrl()));
					countNameChange++;
				}else {
					countSuccess++;
				}
			}

			int total = prdBeanList.size();
			boolean result = checkCurrentState(total,countNameChange,countPriceError,countSuccess,merchantBean,countUrlChange,countNameError,countErrorParse,countExpire,languageError,50);
			monitorReport.putTestPhase("FINSIH - check FeedCrawler");
			return result;
		
	}
	
	private void checkResult(BotConfigBean botConfigBean, Properties mConfig, List<ParserConfigBean> pConfigList, List<ProductDataBean> prdBeanList) {
		try {

			ProductDataDB proDB = getProductDataDB(merchantBean.getDataServer(), botConfigBean);
			ProductDataBean pdb = proDB.getMerchantDataByIdLimitWithStatus(merchantBean.getId(), "C", 100);
			if (pdb == null) {
				monitorReport.putTestPhase("FAIL - check complete product available at checkResult");
				return;
			}
			merchantBean.setActive(14);
			merchantBean.setCurrentserver(merchantBean.getServer());
			merchantDB.updateNewMerchant(merchantBean);
			monitorReport.putTestPhase("PASS - check complete product available at checkResult");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
