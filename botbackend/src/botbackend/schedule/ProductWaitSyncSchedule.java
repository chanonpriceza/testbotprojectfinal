package botbackend.schedule;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.utils.ACCPropertyParser;
import utils.DateTimeUtil;

import botbackend.bean.BotConfigBean;
import botbackend.bean.ProductWaitSyncBean.STATUS;
import botbackend.bean.ProductWaitSyncBean;
import botbackend.db.BotConfigDB;
import botbackend.db.ProductWaitSyncDB;
import botbackend.db.SendDataDB;
import botbackend.db.utils.DatabaseUtil;

public class ProductWaitSyncSchedule extends Schedule {
	
	private static Logger logger = Logger.getLogger("priceza.schedule");
	
	private ACCPropertyParser baseConfig;
	private Map<String, DataSource> mDataSource;
	private Map<String, SendDataDB> mSendData;
	
	public static void main(String[] args) {
		Schedule process = null;
		try {
			if(args.length != 1) {
			    System.out.println("Wrong configuration file argument,<base-config>");
			    return;
			} 			
			process = new ProductWaitSyncSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {			
			logger.error("ProductWaitSyncSchedule Error", e);
			e.printStackTrace();
		}
	}
	
	public ProductWaitSyncSchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
	
		baseConfig = new ACCPropertyParser(configFile[0]);
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-product-wait-sync-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    mDataSource = new HashMap<String, DataSource>();
	    mSendData = new HashMap<String, SendDataDB>();
	    setUpEmail(baseConfig, "ProductWaitSyncSchedule", "PRODUCT_WAIT_SYNC");
	}

	@Override
	public void start() {
		List<String> contryList = baseConfig.getList("bot.sync.wait");
		
		for (String country: contryList) {
			DataSource ds = getBotBackendDataSource(baseConfig, country.toLowerCase());
			BotConfigDB botConfigDB = new BotConfigDB(ds);
			ProductWaitSyncDB pwsDB = new ProductWaitSyncDB(ds);
			
			try {
				generate(botConfigDB, pwsDB);
				update(botConfigDB, pwsDB);
			} catch (Exception e) {
				logger.error("ProductWaitSync Fail: "+country, e);
				sendError(e.toString(), "ProductWaitSync Fail");
			} finally {
				cleanUp(ds);
//				clear mDataSource and mSendData
				for(Entry<String, DataSource> ms : mDataSource.entrySet()) {
					cleanUp(ms.getValue());
				}
				mDataSource.clear();
				mSendData.clear();
			}
		}
	}
	
	private void update(BotConfigDB botConfigDB, ProductWaitSyncDB productWaitSyncDB) throws SQLException {
		
		List<ProductWaitSyncBean> pwsBeanList = productWaitSyncDB.getDataByStatus("W");
		int firstProductId = 0;
		int lastProductId = 0;
		int id = 0;
		int done = 0;
		for (ProductWaitSyncBean pwsBean : pwsBeanList) {
			BotConfigBean bcBean = botConfigDB.findBotConfig(pwsBean.getDataServer());
			SendDataDB sdtDB = getSendDataDB(pwsBean.getDataServer(), bcBean);
			
			Date today = new Date();
			Date yesterday = DateTimeUtil.generateDateDayOffset(today, -1);
			
			firstProductId = pwsBean.getFirstProductId();
			lastProductId = pwsBean.getLastProductId();
			id = sdtDB.getDataByStatusAndRangeId("W", firstProductId, lastProductId);
			done = id - firstProductId;
			
			if (id >= firstProductId && id <= lastProductId) {
				productWaitSyncDB.update("C", done, yesterday, pwsBean.getId());

				ProductWaitSyncBean bean = new ProductWaitSyncBean();
				bean.setDataServer(pwsBean.getDataServer());
				bean.setAddDate(pwsBean.getAddDate());
				bean.setFirstProductId(id);
				bean.setLastProductId(pwsBean.getLastProductId());
				bean.setAllCount(pwsBean.getAllCount() - done);
				bean.setDone(0);
				bean.setStatus(STATUS.W);
				
				productWaitSyncDB.insertProductWaitSync(bean);
			} else if (id > lastProductId || id == -1) {
				productWaitSyncDB.update("C", pwsBean.getAllCount(), yesterday, pwsBean.getId());
			}
		}
	}
	
	private void generate(BotConfigDB botConfigDB, ProductWaitSyncDB productWaitSyncDB) throws SQLException {
		List<String> serverList = botConfigDB.getActiveServerNameList();
		for (String serverName : serverList) {
			BotConfigBean bcBean = botConfigDB.findBotConfig(serverName);
			SendDataDB sDB = getSendDataDB(serverName, bcBean);
			
			Date today = new Date();
			Date yesterday = DateTimeUtil.generateDateDayOffset(today, -1);
			
			int firstProductId = sDB.getIdByDate(DateTimeUtil.clearTime(yesterday), DateTimeUtil.clearTime(today), "ASC");
			int lastProductId = sDB.getIdByDate(DateTimeUtil.clearTime(yesterday), DateTimeUtil.clearTime(today), "DESC");
			
			ProductWaitSyncBean bean = new ProductWaitSyncBean();
			bean.setDataServer(serverName);
			bean.setAddDate(yesterday);
			bean.setDone(0);
			
			if (firstProductId==-1 && lastProductId==-1) {
				bean.setFirstProductId(0);
				bean.setSyncDate(yesterday);
				bean.setLastProductId(0);
				bean.setAllCount(0);
				bean.setStatus(STATUS.C);
			} else {
				bean.setFirstProductId(firstProductId);
				bean.setLastProductId(lastProductId);
				bean.setAllCount(lastProductId - firstProductId + 1);
				bean.setStatus(STATUS.W);
			}
			
			productWaitSyncDB.insertProductWaitSync(bean);
		}
		
	}
	
	private SendDataDB getSendDataDB(String serverName, BotConfigBean botConfigBean) {
		DataSource dSource = mDataSource.get(serverName);
		if(dSource == null) {
			dSource = DatabaseUtil.createDataSource("jdbc:mysql://"+botConfigBean.getUrl(), "com.mysql.jdbc.Driver", botConfigBean.getUsername(), botConfigBean.getPassword(), 3);
			mDataSource.put(serverName, dSource);
		}
		if (mSendData.get(serverName)==null) {
			mSendData.put(serverName, new SendDataDB(dSource));
		}
		return mSendData.get(serverName);
	}
}
