package botbackend.schedule;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.ProductDataBean.STATUS;
import botbackend.utils.ACCPropertyParser;
import utils.DateTimeUtil;
import utils.FilterUtil;

import botbackend.bean.ProductWaitSyncBean;
import botbackend.bean.ProductWaitSyncQualityBean;
import botbackend.db.BotConfigDB;
import botbackend.db.ProductWaitSyncDB;
import botbackend.db.ProductWaitSyncQualityDB;

public class ProductWaitSyncQualitySchedule extends Schedule {
	
	private static Logger logger = Logger.getLogger("priceza.schedule");
	
	private ACCPropertyParser baseConfig;
	private int month;
	private int year;
	
	public static void main(String[] args) {
		Schedule process = null;
		try {
			if(args.length==0 || args.length>2) {
			    System.out.println("Wrong configuration file argument,<base-config> or wrong parameters");
			    return;
			} 			
			process = new ProductWaitSyncQualitySchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {			
			logger.error("ProductWaitSyncQualitySchedule Error", e);
			e.printStackTrace();
		}
	}
	
	public ProductWaitSyncQualitySchedule(String[] configFile) throws ConfigurationException{
		if(configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}
	
		baseConfig = new ACCPropertyParser(configFile[0]);
		
		if (configFile.length>1) {
			String[] date = configFile[1].split("/");
			if (date.length==2) {
				month = Integer.parseInt(date[0]);
				year = Integer.parseInt(date[1]);
			} else {
				month = LocalDate.now().minusMonths(1).getMonth().getValue();
				year = LocalDate.now().getYear();
			}
		} else {
			month = LocalDate.now().minusMonths(1).getMonth().getValue();
			year = LocalDate.now().getYear();
		}
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-product-wait-sync-quality-file");
	    if(file != null) {
	      PropertyConfigurator.configure(file);
	    }
	    
	    setUpEmail(baseConfig, "ProductWaitSyncQualitySchedule", "PRODUCT_WAIT_SYNC_QUALITY");
	}

	@Override
	public void start() {
		List<String> contryList = baseConfig.getList("bot.sync.wait.quality");

		for (String country : contryList) {
			DataSource ds = null;
			try {
				int totalAllCountServer = 0;
				int totalDoneServer = 0;
				ds = getBotBackendDataSource(baseConfig, country.toLowerCase());
				
				ProductWaitSyncDB pwsDB = new ProductWaitSyncDB(ds);
				ProductWaitSyncQualityDB pwsqDB = new ProductWaitSyncQualityDB(ds);
			
				BotConfigDB botConfigDB = new BotConfigDB(ds);
				List<String> serverList = botConfigDB.getActiveServerNameList();
				for (String serverName : serverList) {
					int totalAllCount = 0;
					int totalDone = 0;

					List<ProductWaitSyncBean> pwsBeanList = pwsDB.getDataByRangeDateAndServer(DateTimeUtil.getFirstDayOfMonth(month, year), DateTimeUtil.getLastDayOfMonth(month, year), serverName);

					for (ProductWaitSyncBean bean : pwsBeanList) {
						if (bean.getSyncDate() != null) {
							int diffDate = DateTimeUtil.calcurateDayDiff(bean.getAddDate(), bean.getSyncDate());
							if (bean.getStatus().equals(STATUS.C.toString()) && diffDate < 2) {
								totalDone += bean.getDone();
								totalAllCount += bean.getDone();
								
								totalDoneServer += bean.getDone();
								totalAllCountServer += bean.getDone();
							} else {
								totalAllCount += bean.getAllCount();
								totalAllCountServer += bean.getAllCount();
							}
						} else {
							totalAllCount += bean.getAllCount();
							totalAllCountServer += bean.getAllCount();
						}
					}

					double score = calculateScore(totalAllCount, totalDone);

					ProductWaitSyncQualityBean bean = new ProductWaitSyncQualityBean();
					bean.setDataServer(serverName);
					bean.setDate(new Date());
					bean.setAllCount(totalAllCount);
					bean.setRemain(totalAllCount - totalDone);
					bean.setDone(totalDone);
					bean.setScore(score);
					pwsqDB.insertProductWaitSyncQualityBean(bean);
				}
				
				double scoreServer = calculateScore(totalAllCountServer, totalDoneServer);
				
				ProductWaitSyncQualityBean bean = new ProductWaitSyncQualityBean();
				bean.setDataServer("allServer");
				bean.setDate(new Date());
				bean.setAllCount(totalAllCountServer);
				bean.setRemain(totalAllCountServer - totalDoneServer);
				bean.setDone(totalDoneServer);
				bean.setScore(scoreServer);
				pwsqDB.insertProductWaitSyncQualityBean(bean);
				
			} catch (SQLException e) {
				logger.error("ProductWaitSyncQuality report Fail: " + country, e);
				sendError(e.toString(), "Error report Fail");
			} finally {
				cleanUp(ds);
			}
		}
	}
	
	private double calculateScore(int total, int done) {
		if (total != 0) {
			DecimalFormat df = new DecimalFormat("##.##");
			return Double.valueOf(df.format((done * 100.00) / total));
		}
		
		return 0;
	}
}
