package botbackend.schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import botbackend.utils.ACCPropertyParser;

import botbackend.bean.BanKeywordBean;
import botbackend.bean.MerchantBean;
import botbackend.bean.web.WebMerchantBean;
import botbackend.db.BanKeywordDB;
import botbackend.db.MerchantDB;
import botbackend.db.web.MerchantMappingDB;
import botbackend.db.web.WebBanKeywordDB;

public class DumpProductionDBSchedule extends Schedule {

	private static Logger logger = Logger.getLogger("priceza.schedule");
	private ACCPropertyParser baseConfig;
	
	private DataSource webDataSource;
	private DataSource botDataSource;
	
	private MerchantMappingDB webMerchantDB;
	private MerchantDB botMerchantDB;
	
	private WebBanKeywordDB webBanKeywordDB;
	private BanKeywordDB botBanKeywordDB;
	
	private final static int offset = 100;

	public DumpProductionDBSchedule(String[] configFile) throws ConfigurationException {
		if (configFile[0] == null || configFile[0].trim().isEmpty()) {
			throw new ConfigurationException("no <base-config>");
		}

		baseConfig = new ACCPropertyParser(configFile[0]);
	}

	public static void main(String[] args) {
		Schedule process = null;
		try {
			if (args.length != 1) {
				System.out.println("Wrong configuration file argument,<base-config>");
				return;
			}
			process = new DumpProductionDBSchedule(args);
			process.initialize();
			process.start();
		} catch (ConfigurationException e) {
			logger.error("DumpProductionDBSchedule Error", e);
			e.printStackTrace();
		}
	}

	@Override
	public void initialize() {
		String file = baseConfig.getString("log4j-calculate-workday-file");
		if (file != null) {
			PropertyConfigurator.configure(file);
		}
		setUpEmail(baseConfig, "DumpProductionDBSchedule", "DumpProductionDBSchedule");
	}

	@Override
	public void start() {
			
		List<String> contryList = baseConfig.getList("dumb-production-db-country-list");
		
		for (String country: contryList) {
			
			try {
				webDataSource = getWebDataSource(baseConfig, country.toLowerCase());
				botDataSource = getBotBackendDataSource(baseConfig, country.toLowerCase());
				
				webMerchantDB = new MerchantMappingDB(webDataSource);
				botMerchantDB = new MerchantDB(botDataSource);
				
				webBanKeywordDB = new WebBanKeywordDB(webDataSource);
				botBanKeywordDB = new BanKeywordDB(botDataSource);
				
				//###################################################
				//################ MERCHANT #########################
				//###################################################
				
				logger.info("Start dumb merchant : " + country);
				
				List<MerchantBean> botMerchantList = botMerchantDB.getAllMerchant();
				if(botMerchantList == null || botMerchantList.size() == 0) {
					logger.error("Cannot get merchant list from bot database.");
					continue;
				}else {
					
					List<MerchantBean> dumbMerchantInsertList = new ArrayList<>();
					List<MerchantBean> dumbMerchantUpdateList = new ArrayList<>();
					
					Map<Integer, List<MerchantBean>> botMerchantMap = new HashMap<>();
					for (MerchantBean merchantBean : botMerchantList) {
						List<MerchantBean> merchantList = botMerchantMap.getOrDefault(merchantBean.getId(), new ArrayList<MerchantBean>());
						merchantList.add(merchantBean);
						botMerchantMap.put(merchantBean.getId(), merchantList);
					}
					
					int start = 0;
					while (true) {
						List<WebMerchantBean> webMerchantList = webMerchantDB.getMerchantByLimit(start, offset);
						if (webMerchantList == null || webMerchantList.size() == 0)
							break;
						
						for (WebMerchantBean webMerchantBean : webMerchantList) {
							
							if(webMerchantBean.getMerchant_id() == 0) continue;
							
							List<MerchantBean> botMerchantListFromMap = botMerchantMap.getOrDefault(webMerchantBean.getMerchant_id(), new ArrayList<>());
							if(botMerchantListFromMap == null || botMerchantListFromMap.size() == 0) {
								
								MerchantBean mBean = new MerchantBean();
								mBean.setId(webMerchantBean.getMerchant_id());
								mBean.setName(webMerchantBean.getStore());
								mBean.setActive(0);
								mBean.setPackageType(webMerchantBean.getMerchant_package());
								mBean.setState("CRAWLER_START");
								mBean.setStatus("WAITING");
								mBean.setDataUpdateState("DATA_UPDATE_START");
								mBean.setDataUpdateStatus("WAITING");
								mBean.setDueErrorCount(0);
								mBean.setWceErrorCount(0);
								mBean.setServer("");
								mBean.setDataServer("");
								mBean.setNodeTest(1);
								mBean.setOwnerId(0);
								
								dumbMerchantInsertList.add(mBean);
								
								System.out.println(mBean.getName());
								
								botMerchantListFromMap.add(mBean);
								botMerchantMap.put(mBean.getId(), botMerchantListFromMap);
							}else {
								boolean updated = false;
								for (MerchantBean merchant : botMerchantListFromMap) {
									if(merchant.getPackageType() != webMerchantBean.getMerchant_package()) {
										if(!updated) {
											updated = true;
											merchant.setPackageType(webMerchantBean.getMerchant_package());
											dumbMerchantUpdateList.add(merchant);
										}
									}
								}
							}
						}
						
						start = start+offset;
					}
					
				if(dumbMerchantInsertList != null && dumbMerchantInsertList.size() > 0) botMerchantDB.insertNewMerchantBatch(dumbMerchantInsertList);
				if(dumbMerchantUpdateList != null && dumbMerchantUpdateList.size() > 0) botMerchantDB.updatePackageBatch(dumbMerchantUpdateList);
					
				logger.info("Finish dumb merchant : " + country);
				
				}
				
				//###################################################
				//################ BAN KEYWORD ######################
				//###################################################
				
				logger.info("Start dumb ban keyword : " + country);
				
				int botCountBanKeyword = botBanKeywordDB.countBanKeyword();
				int webCountBanKeyword = webBanKeywordDB.countBanKeyword();
				
				if(webCountBanKeyword == 0) {
					logger.error("Cannot get ban-keyword data from web database.");
					continue ;
				}
				
				if(botCountBanKeyword == webCountBanKeyword) {
					continue ;
				}else {
					int maxId = botBanKeywordDB.getMaxId();
					int start = 0;
					boolean isInsert = false;
					while(true){
						List<BanKeywordBean> webBanKeywordList = webBanKeywordDB.getBanKeywordLimit(start, offset);
						if(webBanKeywordList==null||webBanKeywordList.size()==0){
							break;
						}
						int[] result = botBanKeywordDB.insertBatch(webBanKeywordList);
						for(int i=0; i<result.length; i++) {
							if(result[i] > 0) {
								isInsert = true;
							}
						}
						start = start+offset;
					}
					
					if(isInsert) {
						botBanKeywordDB.deleteAllById(maxId);
					}
				}
				
				logger.info("Finish dumb ban keyword : " + country);
				
				logger.info("Finish DumpProductionDBSchedule");
			
			} catch (Exception e) {
				logger.error("Error DumpProductionDBSchedule " , e);
			} finally {
				cleanUp(webDataSource);
				cleanUp(botDataSource);
			}
		}
	}

}
