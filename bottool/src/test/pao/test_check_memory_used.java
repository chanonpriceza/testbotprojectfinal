package test.pao;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

public class test_check_memory_used {
	
	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		
		HashSet<String> resultSet = new HashSet<>();
		
		String file = "D:\\test_file\\lazada_backup_sku_list.txt";
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))){
			String line = null;
			while ((line = br.readLine()) != null) {
				if(isBlank(line)) {
					continue;
				} else {
					resultSet.add(line);
				}
				if(resultSet.size() % 1000000 == 0) {
					System.out.println("Done chk " + resultSet.size());
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done All " + resultSet.size());
		
		long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		long actualMemUsed=afterUsedMem-beforeUsedMem;
		System.out.println((beforeUsedMem / (double)(1024 * 1024)) + " → " + (afterUsedMem / (double)(1024 * 1024)) + " \nsummary use memory " + (actualMemUsed / (double)(1024 * 1024)));
		System.out.println(new DecimalFormat("####0.000").format((System.nanoTime() - startTime) / 1000000000.0) + " sec");
		
		check(resultSet, "213263571");
		check(resultSet, "840682980");
		check(resultSet, "3a21ads56");
		
	}	
	
	private static void check(Set<String> checkList, String input) {
		long startTime = System.nanoTime();
		if(checkList.contains(input)) {
			System.out.print("contains ... ");
		} else {
			System.out.print("not exist .. ");
		}
		System.out.println(new DecimalFormat("####0.000").format((System.nanoTime() - startTime) / 1000000000.0) + " sec");
		
	}
	
}
