package test.pao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class test_convert_first_infex_value_of_map_to_list {
	
	public static void main(String[] args) {
		
		Map<String, String[]> source = new HashMap<>();
		source.put("a", new String[] {"a1","a2","a3","a4","a5","a6"});
		source.put("b", new String[] {"b1","b2","b3","b4","b5","b6"});
		source.put("c", new String[] {"c1","c2","c3","c4","c5","c6"});
		source.put("d", new String[] {"d1","d2","d3","d4","d5","d6"});
		source.put("e", new String[] {"e1","e2","e3","e4","e5","e6"});
		source.put("f", new String[] {"f1","f2","f3","f4","f5","f6"});
		
		List<String> allFirstIndex = new ArrayList<>();
		
		Collection<String[]> values = source.values();
		
		allFirstIndex = values.stream().filter(x -> x.length >= 1).map(x -> x[0]).collect(Collectors.toList());
		
		if(allFirstIndex != null && allFirstIndex.size() > 0) {
			allFirstIndex.forEach(System.out::println);
		}
		
		
	}
}
