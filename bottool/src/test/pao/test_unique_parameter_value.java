package test.pao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import Util.Util;

public class test_unique_parameter_value {
	
	public static void main(String[] args) {
		
		List<String> allLink = new ArrayList<>();
		
		allLink.add("http://www.test.com/search?category=AAAAA&page=1&search=BBBBB");
		allLink.add("http://www.test.com/search?category=AAAAA&page=2&search=CCCCC");
		allLink.add("http://www.test.com/search?category=AAAAA&page=3&search=DDDDD");
		allLink.add("http://www.test.com/search?category=AAAAA&page=4&search=EEEEE");
		allLink.add("http://www.test.com/search?category=AAAAA&page=5&search=FFFFF");
		allLink.add("http://www.test.com/search?category=AAAAA&page=6");
		
		List<Map<String, String>> linkParamList = new ArrayList<>();
		for (String link : allLink) {
			Map<String, String> param = getParamFromLink(link);
			linkParamList.add(param);
		}

		List<String> mainKeyList = new ArrayList<>();
		
		// get param appear on all link
		if(linkParamList != null && linkParamList.size() > 0) {
			Map<String, String> mainMap = linkParamList.get(0);
			for (Entry<String, String> mainEntry : mainMap.entrySet()) {
				String mainKey = mainEntry.getKey();
				boolean isKeyFromAllLink = true;
				for (Map<String, String> map : linkParamList) {
					if(!map.containsKey(mainKey)) isKeyFromAllLink = false;
				}
				if(isKeyFromAllLink) {
					mainKeyList.add(mainKey);
				}
			}
		}
		
		// remove param have duplicate value
		List<String> uniqueKeyList = new ArrayList<>();
		if(mainKeyList != null && mainKeyList.size() > 0) {
			for (String mainKey : mainKeyList) {
				List<String> valueList = new ArrayList<>();
				boolean isUnique = true;
				for (Map<String, String> map : linkParamList) {
					String value = map.get(mainKey);
					if(valueList.contains(value)) {
						isUnique = false;
					}else {
						valueList.add(value);
					}
				}
				if(isUnique) {
					uniqueKeyList.add(mainKey);
				}
			}
		}
		
		System.out.println(uniqueKeyList.toString());
    }
	
	private static Map<String, String> getParamFromLink(String link){
		Map<String, String> resultMap = new HashMap<>();
		if(StringUtils.isNotBlank(link)) {
			String txt = Util.getStringAfter(link,"?","");
			if(StringUtils.isBlank(txt)) return null;
			
			List<String> keyList = Util.getAllStringBetween("&"+txt, "&", "=");
			if(keyList == null || keyList.size() == 0) return null;
			
			List<String> valueList = Util.getAllStringBetween(txt+"&", "=", "&");
			if(valueList == null || valueList.size() == 0) return null;
			
			if(keyList.size() != valueList.size()) return null;
			for (int i=0; i<keyList.size(); i++) {
				resultMap.put(keyList.get(i), valueList.get(i));
			}
		}
		
		return resultMap;
	}
	
}
