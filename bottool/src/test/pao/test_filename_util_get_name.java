package test.pao;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FilenameUtils;

public class test_filename_util_get_name {
	public static void main(String[] args) {
		try {
			
			String url = "http://27.254.85.9/7107_20180403.csv";
			
			String fileName = FilenameUtils.getName(new URL(url).getPath());
			
			System.out.println(fileName);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    }
}
