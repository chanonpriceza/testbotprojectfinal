package test.pao;

public class test_kill_another_thread extends Thread {

//	private volatile static boolean threadLive;

	public static void main(String[] args) {
		
		test_kill_another_thread t1 = new test_kill_another_thread();
		try {
			t1.start();
	
			checkRuntime();
		}catch(RuntimeException e) {
			if(t1 != null) {
				t1.interrupt();
			}
			e.printStackTrace();
		}

	}
	
	public void run() {
		for(int i=0; i<1000; i++) {
			try {
				Thread.sleep(300);
				if(i%50==0) System.out.print("\nCount : "); System.out.print(i%10+1);
			} catch (InterruptedException e) {
				System.out.println("UPDATE after stop here");
				e.printStackTrace();
				return;
			}
		}
	}

	private static void checkRuntime() throws RuntimeException{
		int i = 1;
		while(true) {
			try {
				Thread.sleep(1000);
				System.out.print("\n----- CHECK !!!\nCount : ");
				
				if(i > 5) {
					throw new RuntimeException("---------------- Stop !!!");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
	
	
}
