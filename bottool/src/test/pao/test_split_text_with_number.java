package test.pao;

import org.apache.commons.lang.StringUtils;

import Util.Util;

public class test_split_text_with_number {
	
	public static void main(String[] args) {
		
		String[] testText = new String[] {
				"มือถือ samsung|min-450",
				"มือถือ apple",
				"",
		};
		
		for (String test : testText) {
			if(StringUtils.isNotBlank(test) && test.contains("|")) {
				String[] keywordArgument = test.split("\\|");
				if(keywordArgument != null && keywordArgument.length > 0) {
					for (String ka : keywordArgument) {
						try {
							if(ka.contains("max-")) {
								long filterPrice = Long.parseLong(Util.getStringAfter(ka, "max-", "999999999999999"));
								System.out.println("filter max  = " + filterPrice);
							}else if(ka.contains("min-")) {	
								long filterPrice = Long.parseLong(Util.getStringAfter(ka, "min-", "0"));
								System.out.println("filter min  = " + filterPrice);
							}else {
								System.out.println("set keyword = " + ka);
							}
						}catch(NumberFormatException e) {
							System.out.println("set exception = " + test);
						}
					}
				}
			}else {
				System.out.println("set keyword = " + test);
			}
		}
		
		
    }
}
