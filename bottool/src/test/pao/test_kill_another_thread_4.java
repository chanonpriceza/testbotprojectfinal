package test.pao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class test_kill_another_thread_4 {

	public static void main(String[] args) {

		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		try {
			
			Future<?> mainProcess = executor.submit(new Runnable() {
				public void run() {
					System.out.println("Process Run.");
					try {
						for (int i = 0; i < 1000; i++) {
							try {
								Thread.sleep(300);
								if (i % 50 == 0) System.out.print("\n");
								
								System.out.print(i % 10 + 1);
							} catch (Exception e) {
								e.printStackTrace();
								if (e instanceof InterruptedException) throw e;
							}
						}
						
					}catch(Exception e) {
						e.printStackTrace();
//						if (e instanceof InterruptedException) throw e;
					}
				}
			});

			executor.submit(new Runnable() {
				public void run() {
					System.out.println("Check Run.");
					int i = 1;
					while (true) {
						try {
							Thread.sleep(1000);
							System.out.println("\n----- CHECK !!! ");
							
							if (i > 3) {
								if(!mainProcess.isDone()) {
									mainProcess.cancel(true);
									System.out.println("KILL");
								}
							}
							
							if(mainProcess.isDone()) {
								System.out.println("DONE");
							}
							
							if(mainProcess.isCancelled()) {
								System.out.println("CANCELED");
							}
							
//							if(mainProcess.isDone() && mainProcess.isCancelled()) {
//								System.out.println("\nCONFIRM clear all and exit");
//								return;
//							}
							
//							System.out.println(mainProcess.get());
							
						} catch (Exception e) {
							e.printStackTrace();
							return;
						}
						i++;
					}
				}
			});
			
			executor.shutdown();
			if(executor.awaitTermination(10, TimeUnit.SECONDS)){
				System.out.println("Done All.");
			}else{
				System.out.println("Time out.");
				executor.shutdownNow();
			}
			
			System.out.println("NEXT STEP");
			
		} catch (Exception e) {
			System.out.println("MAIN EXCEPTION");
			e.printStackTrace();
		}

	}


}
