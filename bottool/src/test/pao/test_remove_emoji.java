package test.pao;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Util.Util;

public class test_remove_emoji {
	
	private static final Map<String, Map<Integer, Integer>> languageCharacterMap = createLanguageCharacterMap();
	private static Map<String, Map<Integer, Integer>> createLanguageCharacterMap(){
		System.out.println("CREATE LANGUAGE CHARACTER MAP !!");
    	Map<String, Map<Integer, Integer>> maps = new HashMap<>();
    	maps.put("th", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0E00, 0x0E5B);
			}
		});
    	maps.put("eng", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0041, 0x005A);
				put(0x0061, 0x007A);
				put(0x0030, 0x0039);
			}
		});
		maps.put("viet", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0xc0, 0xc3);
				put(0xc8, 0xca);
				put(0xcc, 0xcd);
				put(0xd2, 0xd5);
				put(0xd9, 0xda);
				put(0xdd, 0xdd);
				put(0xe0, 0xe3);
				put(0xe8, 0xea);
				put(0xec, 0xed);
				put(0xf2, 0xf5);
				put(0xf9, 0xfa);
				put(0xfd, 0xfd);
				put(0x102, 0x103);
				put(0x110, 0x111);
				put(0x128, 0x129);
				put(0x168, 0x169);
				put(0x1a0, 0x1a1);
				put(0x1af, 0x1b0);
				put(0x1ea0, 0x1ef9);
			}
		});
		maps.put("chinese", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x4E00, 0x9FD5);
			}
		});
    	return maps;
    }
	
	public static void main(String[] args) {
		
		String[] allowList = {"th", "eng"};
		String inputString = ".เงื่อนไข..📍ไม่รับเปลี่ยนสินค้า";
		
		System.out.println(inputString);
		
		for (char splitChar : inputString.toCharArray()) {
			System.out.print(splitChar);
			if(Character.isLetter(splitChar)) {
				boolean isInAllowList = false;
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							isInAllowList = true;
						}
					}
				}
				if(!isInAllowList) {
					System.out.println(" - NOT ALLOW");
				}
			}else {
				System.out.println(" - ALLOW");
				continue;
			}
		}
		
		System.out.println("done");
    }
	
	public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
		if ((i >= minValueInclusive && i <= maxValueInclusive))
			return true;
		else
			return false;
	}
	
}
