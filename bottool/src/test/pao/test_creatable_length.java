package test.pao;

import java.text.SimpleDateFormat;

import org.apache.commons.lang3.math.NumberUtils;

public class test_creatable_length {
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	public static void main(String[] args) {
		
		String num1 = "3199000000";
		
		if(NumberUtils.isCreatable(num1)) {
//			int priceFull_1 = Integer.parseInt(num1);
//			int priceReal_1 = priceFull_1 / 100000;
//			if(priceReal_1 > 0) System.out.println(String.valueOf(priceReal_1));
			
			long priceFull_2 = Long.parseLong(num1);
			long priceReal_2 = priceFull_2 / 100000;
			if(priceReal_2 > 0) System.out.println(String.valueOf(priceReal_2));
		}
		
	}
	
}