package test.pao;

import org.apache.commons.lang3.StringUtils;

public class test_out_of_range {
	
	public static void main(String[] args) {
		
		String[] str = {"abc", "def", "ghi"};
		System.out.println(str.length);
		method1(str[0]);
		method1(str[1]);
		method1(str[2]);
		method1(str[99]);
		
    }
	
	private static void method1(String a) {
		if(StringUtils.isNotBlank(a)) {
			System.out.println(a);
		}
	}
	
}
