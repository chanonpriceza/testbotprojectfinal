package test.pao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class test_batch_sql_object {
	
	public static void main(String[] args) {
		
		int id = 123;
		List<String> urls = new ArrayList<>();
		urls.add("url0");
		urls.add("url1");
		urls.add("url2");
		urls.add("url3");
		urls.add("url4");
		urls.add("url5");
		urls.add("url6");
		urls.add("url7");
		urls.add("url8");
		urls.add("url9");
		
		List<String> names = new ArrayList<>();
		names.add("name10");
		names.add("name11");
		names.add("name12");
		names.add("name13");
		names.add("name14");
		names.add("name15");
		names.add("name16");
		names.add("name17");
		names.add("name18");
		names.add("name19");
		
		List<Object> fixObjList = Arrays.asList(id);
		List<Object> urlObjList = new ArrayList<>(urls);
		List<Object> nameObjList = new ArrayList<>(names);
		
		Object[][] test = genSQLBatchParam(fixObjList, urlObjList, nameObjList);
		if(test != null && test.length > 0) {
			for(int i=0; i<test.length; i++) {
				for(int j=0; j<test[i].length; j++) {
					System.out.print(test[i][j]);
					System.out.print(", ");
				}
				System.out.println();
			}
		}
		
    }


	@SafeVarargs
	public static Object[][] genSQLBatchParam(List<Object> fixList, List<Object>... objListSet){
		Object[][] resultObj = null;
		int dimention1Size = 0, dimention2Size = 0;
		int fixListSize = 0, eachListSize = 0;
		
		if(fixList != null && fixList.size() > 0) {
			dimention1Size += fixList.size();
			fixListSize = fixList.size();
		}
		
		if(objListSet != null && objListSet.length > 0) {
			dimention1Size += objListSet.length;
			for(List<Object> objList : objListSet) {
				if(eachListSize == 0) {
					eachListSize = objList.size();
					dimention2Size += objList.size();
					resultObj = new Object[dimention2Size][dimention1Size];
				}else {
					if(eachListSize != objList.size()) return null; 
				}
			}
		}
		for(int i=0; i<dimention2Size; i++) {
			for(int j=0; j<dimention1Size; j++) {
				if(j<fixListSize)
					resultObj[i][j] = fixList.get(j);
				else
					resultObj[i][j] = objListSet[j-fixListSize].get(i);
			}
		}
		return resultObj;
	}
	
	
	
	
	
	
	
	
	
	
	
}
