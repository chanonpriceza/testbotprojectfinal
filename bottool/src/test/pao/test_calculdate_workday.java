package test.pao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class test_calculdate_workday {
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	public static void main(String[] args) {
		try {
			String[] dateList = new String[] {
					"2019-09-30 15:53:30",
					"2019-09-27 16:57:07",
					"2019-09-27 16:31:49",
					"2019-09-27 14:43:52",
					"2019-09-27 14:24:21",
					"2019-09-27 11:35:57",
					"2019-09-27 10:58:45",
					"2019-09-26 17:46:51",
					"2019-09-26 17:46:12",
					"2019-09-26 17:42:46",
					"2019-09-26 10:43:28",
					"2019-09-26 10:33:28",
					"2019-09-25 19:19:48",
					"2019-09-25 17:57:18",
					"2019-09-25 17:25:32",
					"2019-09-25 17:04:58",
					"2019-09-25 16:52:35",
					"2019-09-24 19:02:55",
					"2019-09-24 15:16:56",
					"2019-09-24 13:55:57",
					"2019-09-24 13:40:41",
					"2019-09-24 13:35:35",
					"2019-09-24 13:31:00",
					"2019-09-24 13:27:18",
					"2019-09-24 11:51:56",
					"2019-09-24 11:50:48",
					"2019-09-24 11:39:18",
					"2019-09-23 14:09:29",
					"2019-09-19 18:19:27",
					"2019-09-19 18:16:07",
					"2019-09-19 17:45:22",
					"2019-09-19 16:20:19",
					"2019-09-19 16:15:44",
					"2019-09-19 16:12:50",
					"2019-09-19 16:08:03",
					"2019-09-19 16:07:25",
					"2019-09-19 16:06:54",
					"2019-09-19 16:06:24",
					"2019-09-19 10:43:34",
					"2019-09-18 18:37:20",
			};
			for(String date : dateList) {
				// this is for case DEV ( 5 workday )
				System.out.println(date + " -> " + getDiffDay(findTargetDate(date)));
				
				// this is for case CONTENT ( have due date )
				// System.out.println(date + " -> " + getDiffDay(dueDate));
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Date findTargetDate(String assignDate) throws Exception{
		Calendar c = Calendar.getInstance();
		c.setTime(formatter.parse(assignDate));
		int count = 0;
		for(int i=0; i<30; i++) {
//			String dateString = formatter.format(new Date(c.getTimeInMillis()));
//			if (start.get(Calendar.DAY_OF_WEEK) != 7 && start.get(Calendar.DAY_OF_WEEK) != 1 && !HolidayList.contains(dateString)) {
			if (c.get(Calendar.DAY_OF_WEEK) != 7 && c.get(Calendar.DAY_OF_WEEK) != 1) {
				count++;
			}
			c.add(Calendar.DAY_OF_WEEK, 1);
			if(count==5) break;
		}
		return c.getTime();
	}
	
	private static int getDiffDay(Date targetDate) throws Exception {
		
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTime(new Date());
		end.setTime(targetDate);

		long startLong = start.getTimeInMillis();
		long endLong = end.getTimeInMillis();

		if(startLong == endLong) return 1;
		
		int differentDay = 0;
		while (startLong < endLong) {
//			String startString = formatter.format(new Date(start.getTimeInMillis()));
//			if (start.get(Calendar.DAY_OF_WEEK) != 7 && start.get(Calendar.DAY_OF_WEEK) != 1 && !HolidayList.contains(startString)) {
			if (start.get(Calendar.DAY_OF_WEEK) != 7 && start.get(Calendar.DAY_OF_WEEK) != 1) {
				differentDay++;
			}
			start.add(Calendar.DATE, 1);
			startLong = start.getTimeInMillis();
		}
		
		return differentDay;
	}
	
}
