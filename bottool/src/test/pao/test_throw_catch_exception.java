package test.pao;

public class test_throw_catch_exception {
	
	public static void main(String[] args) throws Exception {
		
		try {
			
			// catch and finally
//			int zeroException = 150/0;
//			System.out.println(zeroException);
			
			throw new InterruptedException("THIS IS THROW NEW EXCEPTION");
			
			
			
		}catch(InterruptedException e) {
			throw e;
		}catch(Exception e) {
			System.out.println("------------------------- catch");
		}finally {
			System.out.println("------------------------- finally");
		}
		
		
	}
}
