package test.pao;

import java.util.List;
import Util.Util;

public class test_get_all_string_between {
	
	public static void main(String[] args) {
		
		String text = "[{\"make\":\"Audi\",\"model\":\"A4\",\"cc\":\"1800cc\",\"year\":\"2005\",\"class\":\"2+\"},{\"make\":\"Audi\",\"model\":\"A4\",\"cc\":\"1800cc\",\"year\":\"2006\",\"class\":\"2+\"},{\"make\":\"Audi\",\"model\":\"A4\",\"cc\":\"1800cc\",\"year\":\"2007\",\"class\":\"2+\"}]";
		
		List<String> objList = Util.getAllStringBetween(text, "{", "}");
		if(objList != null) {
			for (String spec : objList) {
				System.out.println(spec);
			}
		}
		System.out.println("done");
    }
}
