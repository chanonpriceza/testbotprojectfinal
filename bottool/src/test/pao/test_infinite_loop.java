package test.pao;

public class test_infinite_loop {
	
	public static void main(String[] args) {
		try {
			
			int count = 0;
			while(count<86400) {
				
				System.out.println("test_infinite_loop : " + count);
				Thread.sleep(1000);
				count++;
				
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}

