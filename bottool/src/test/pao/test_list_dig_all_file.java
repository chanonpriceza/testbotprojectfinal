package test.pao;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class test_list_dig_all_file {
	
	public static void main(String[] args) {
		
		Map<String, List<String>> resultMap = new LinkedHashMap<>();
		
//		File f1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
//		getAllFiles(f1, resultMap);
//		
//		File f2 = new File(Environment.getDataDirectory().getAbsolutePath);
//		getAllFiles(f2, resultMap);
		
		showAllResultMap(resultMap);
    }
	
	private static void getAllFiles(File directory, Map<String, List<String>> resultMap) {
	    
		// System.out.println("check directory : " + directory.getAbsolutePath());
	    final File[] files = directory.listFiles();

	    if(files != null){
	        for(File file : files){
	            if(file != null){
	                if(file.isDirectory()){  
	                    getAllFiles(file, resultMap);
	                } else {  
	                	if(file.getName().endsWith(".mp3") || file.getName().endsWith(".wav")) {
	                		List<String> resultList = resultMap.getOrDefault(file.getName(), new ArrayList<>());
	                		resultList.add(directory.getAbsolutePath() + "\\" + file.getName());
	                		resultMap.put(directory.getAbsolutePath(), resultList);
	                	}
	                }
	            }
	        }
	    }
	}

	private static void showAllResultMap(Map<String, List<String>> resultMap) {
		if(resultMap != null) {
			for (Entry<String, List<String>> entry : resultMap.entrySet()) {
				System.out.println(entry.getKey());
			}
		}
	}
}




