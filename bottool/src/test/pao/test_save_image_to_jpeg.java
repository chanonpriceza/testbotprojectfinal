package test.pao;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.byteSources.ByteSource;
import org.apache.sanselan.common.byteSources.ByteSourceArray;
import org.apache.sanselan.formats.jpeg.JpegImageParser;
import org.apache.sanselan.formats.jpeg.segments.UnknownSegment;

//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGEncodeParam;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;

import Util.Util;

public class test_save_image_to_jpeg {
	public static void main(String[] args) {
		
		String imageUrl = "http://www.bangnashop.com/components/com_virtuemart/shop_image/product/____________iPho_53d34a464055b.jpg";
		test_save_image_to_jpeg c = new test_save_image_to_jpeg();
		byte[] result = c.parse(imageUrl, 200, 200);
		System.out.println(result.length);
		
    }
	
	public byte[] parse(String imageUrl, int width, int height) {

		URL url = null;
		HttpURLConnection conn = null;
		try {
			url = new URL(imageUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5");
			conn.setReadTimeout(60000);
			conn.setConnectTimeout(60000);

			try (InputStream inputStream = conn.getInputStream();
				ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
				
				int read = 0;
				byte[] bytes = new byte[20480];
				while ((read = inputStream.read(bytes)) != -1)
					byteOutputStream.write(bytes, 0, read);

				try (ImageInputStream imageStream = ImageIO.createImageInputStream(new ByteArrayInputStream(byteOutputStream.toByteArray()))) {
					Iterator<ImageReader> iter = ImageIO.getImageReaders(imageStream);
					BufferedImage image = null;
					
					if (iter.hasNext()) {
						ImageReader reader = null;
						try {
							reader = iter.next();
							reader.setInput(imageStream);
							image = reader.read(0);
						} catch (IIOException e) {
							try {
								WritableRaster raster = (WritableRaster) reader.readRaster(0, null);
								ICC_Profile profile = Sanselan.getICCProfile(byteOutputStream.toByteArray());

								checkAdobeMarker(byteOutputStream.toByteArray(), raster);

								image = convertCmykToRgb(raster, profile);
							} catch (ImageReadException e1) {
								System.out.print("Can't get ICC Profile : ");
							}
						} finally {
							if (reader!=null) {
								reader.dispose();
							}
						}
					}
					
					byte[] output;
					if (image != null) {
						byteOutputStream.reset();
						try {
							ImageIO.write(image, "jpg", byteOutputStream);
						} catch (IIOException e) {
							ImageIO.write(image, "png", byteOutputStream);
						}
					}
					output = parse(byteOutputStream.toByteArray(), width, height);
						
					return output;
				}
			} catch(FileNotFoundException e) {
				System.out.print("FileNotFoundException : ");
			} 
		} catch (Exception e) {
			Util.consumeInputStreamQuietly(conn.getErrorStream());
			e.printStackTrace();
		} finally {
			if(conn != null)
	 			conn.disconnect();
		}

		return null;
	}

	public byte[] parse(byte[] imageByte, int width, int height) {

		
		try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {

			ImageIcon ii = new ImageIcon(imageByte);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if (iWidth == -1 && iHeight == -1) {
				return null;
			}

			double xPos = (width - 1 * iWidth) / 2;
			double yPos = (height - 1 * iHeight) / 2;
			Image temp = null;

			if (iHeight > height || iWidth > width) {

				if (iWidth > iHeight) {
					resizedImage = i.getScaledInstance(width, (height * iHeight) / iWidth, Image.SCALE_SMOOTH);
				} else {
					resizedImage = i.getScaledInstance((width * iWidth) / iHeight, height, Image.SCALE_SMOOTH);
				}

				double xScale = (double) width / iWidth;
				double yScale = (double) height / iHeight;
				double scale = Math.min(xScale, yScale);

				// Calculate the center position of the panel -- with scale
				xPos = (width - scale * iWidth) / 2;
				yPos = (height - scale * iHeight) / 2;
				// This code ensures that all the pixels in the image are
				// loaded.
				temp = new ImageIcon(resizedImage).getImage();

			} else {
				temp = i;
			}

			// Create the buffered image.
			// BufferedImage bufferedImage = new
			// BufferedImage(temp.getWidth(null), temp.getHeight(null),
			// BufferedImage.TYPE_INT_RGB);
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			// Copy image to buffered image.
			Graphics g = bufferedImage.createGraphics();

			// Clear background and paint the image.
			g.setColor(Color.white);
			g.fillRect(0, 0, width, height);
			// g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
			g.drawImage(temp, (int) xPos, (int) yPos, null);
			g.dispose();

			// Soften.
			// float softenFactor = 0.05f;
			// float[] softenArray = { 0, softenFactor, 0, softenFactor,
			// 1 - (softenFactor * 4), softenFactor, 0, softenFactor, 0 };
			// Kernel kernel = new Kernel(3, 3, softenArray);
			// ConvolveOp cOp = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP,
			// null);
			// bufferedImage = cOp.filter(bufferedImage, null);

			// Write the jpeg to output stream.
			

			// Encodes image as a JPEG data stream
//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outStream);
//			JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bufferedImage);
//			param.setQuality(1f, true);
//			encoder.setJPEGEncodeParam(param);
//			encoder.encode(bufferedImage);

			return outStream.toByteArray();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void checkAdobeMarker(byte[] imageByte, WritableRaster raster) {
		JpegImageParser imageParser = new JpegImageParser();
		ByteSource byteSource = new ByteSourceArray(imageByte);
		
		@SuppressWarnings("rawtypes")
		ArrayList segments = null;
		try {
			segments = imageParser.readSegments(byteSource, new int[] { 0xffee }, true);
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		}
		if (segments != null && segments.size() >= 1) {
			UnknownSegment app14Segment = (UnknownSegment) segments.get(0);
			byte[] segmentByte = app14Segment.bytes;
			if (segmentByte.length >= 12 && segmentByte[0] == 'A' && segmentByte[1] == 'd' && segmentByte[2] == 'o' && segmentByte[3] == 'b' && segmentByte[4] == 'e') {
				if ((app14Segment.bytes[11] & 0xff) == 2) {
					convertYcckToCmyk(raster);
				}
				convertInvertedColors(raster);
			}
		}
	}

	private void convertYcckToCmyk(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);

			for (int x = 0; x < stride; x += 4) {
				int y = pixelRow[x];
				int cb = pixelRow[x + 1];
				int cr = pixelRow[x + 2];

				int c = (int) (y + 1.402 * cr - 178.956);
				int m = (int) (y - 0.34414 * cb - 0.71414 * cr + 135.95984);
				y = (int) (y + 1.772 * cb - 226.316);

				if (c < 0) {
					c = 0;
				} else if (c > 255) {
					c = 255;
				}
					
				if (m < 0) {
					m = 0;
				} else if (m > 255) {
					m = 255;
				}
					
				if (y < 0) {
					y = 0;
				} else if (y > 255) {
					y = 255;
				}

				pixelRow[x] = 255 - c;
				pixelRow[x + 1] = 255 - m;
				pixelRow[x + 2] = 255 - y;
			}

			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private void convertInvertedColors(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);
			for (int x = 0; x < stride; x++) {
				pixelRow[x] = 255 - pixelRow[x];
			}
			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private BufferedImage convertCmykToRgb(Raster srcRaster, ICC_Profile profile) throws IOException {
		if (profile == null) {
			profile = ICC_Profile.getInstance(test_save_image_to_jpeg.class.getResourceAsStream("/ISOcoated_v2_300_eci.icc"));
		}

		if (profile.getProfileClass() != ICC_Profile.CLASS_DISPLAY) {
			byte[] profileData = profile.getData();

			if (profileData[ICC_Profile.icHdrRenderingIntent] == ICC_Profile.icPerceptual) {
				intToBigEndian(ICC_Profile.icSigDisplayClass, profileData, ICC_Profile.icHdrDeviceClass);
				
				profile = ICC_Profile.getInstance(profileData);
			}
		}

		BufferedImage resultImage = new BufferedImage(srcRaster.getWidth(), srcRaster.getHeight(), BufferedImage.TYPE_INT_RGB);
		WritableRaster resultRaster =  resultImage.getRaster();

		ICC_ColorSpace cmykCS = new ICC_ColorSpace(profile);
		ColorSpace rgbCS = resultImage.getColorModel().getColorSpace();

		ColorConvertOp cmykToRgb = new ColorConvertOp(cmykCS, rgbCS, null);
		cmykToRgb.filter(srcRaster, resultRaster);

		return resultImage;
	}

	private void intToBigEndian(int value, byte[] profileData, int index) {
		profileData[index] = (byte) (value >> 24);
		profileData[index + 1] = (byte) (value >> 16);
		profileData[index + 2] = (byte) (value >> 8);
		profileData[index + 3] = (byte) (value);
	}

	
	
}
