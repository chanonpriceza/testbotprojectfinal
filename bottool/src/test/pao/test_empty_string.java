package test.pao;

import org.apache.commons.lang.StringUtils;

public class test_empty_string {
	
	public static void main(String[] args) {
		
		String a1 = "";
		String a2 = null;
		String a3 = "asdsads";
		
		System.out.println(StringUtils.defaultIfBlank(a1, null));
		System.out.println(StringUtils.defaultIfBlank(a2, null));
		System.out.println(StringUtils.defaultIfBlank(a3, null));
		
		
    }
}
