package test.pao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import Util.Util;

public class test_post_request {
	
	public static void main(String[] args) {

//		String url = "http://www.highshopping.com/category/categoryView.do?categoryCode=10000067&orderType=2&rowsPerPage=20&viewType=0&keywordSearch=";
//		String data = "currentPage=5";
		
//		String url = "https://thebeautrium.com/ssr/api/v2/materials";
//		String data = "{\"filter\":{\"cat1_id\":12},\"page\":1,\"limit\":20,\"sort\":{\"label\":\"What's new?\",\"value\":\"new_arrivals\"},\"options\":{}}";
		
		String url = "https://api.bhinneka.com/krill-dev/v1/clients/login";
		String data = "{\"appId\":\"b90e1679-0550-456a-8\",\"appSecret\":\"e70e1227-9546-52f5-bbc0-7a7e0b5adef3\"}";
		
//		String url = "https://api.bhinneka.com/krill-dev/v1/clients/login";
//		String data = "{\"appId\": \"3c5b5d05-0f55-42c2-8\",\"appSecret\": \"0564ae25-f8b1-5af2-aff1-21094862dac1\"}";
		
		String result = postRequest(url, data);
		System.out.println(result);
		
	}
	
	public static String postRequest(String url, String data) {
		URL u = null;
		HttpURLConnection conn = null;
		try {
			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Accept", "*/*");				
			
			conn.setRequestProperty("Content-Length", "" + data.length());
			conn.setInstanceFollowRedirects(true);
			
			try(DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());){
				outStream.writeBytes(data);
				outStream.flush();
				try (
					InputStream is = conn.getInputStream();
					InputStreamReader isr = new InputStreamReader(is, "UTF-8");
	    			BufferedReader brd = new BufferedReader(isr);) {
					
		    		StringBuilder rtn = new StringBuilder(1000);
		    		String line = "";
					while ((line = brd.readLine()) != null) {
						rtn.append(line);
					}
					return rtn.toString();
		    	}catch(IOException e){
		    		e.printStackTrace();
		    		Util.consumeInputStreamQuietly(conn.getErrorStream());
		    	}
			}catch(IOException e){
				Util.consumeInputStreamQuietly(conn.getErrorStream());
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}
}
