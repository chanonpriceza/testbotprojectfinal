package test.pao;

import java.io.StringReader;
import java.util.Calendar;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class test_edit_csv_data {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		String HEADER = "product_name,simple_sku,old_sku_id,deeplink,brand,sale_price,discounted_price,discount_percentage,availability,cross_border,picture_url,image_url_2,image_url_3,image_url_4,image_url_5,level_1,level_2,level_3,level_4,level_5,is_slash_it,is_hot_deals,is_crazy_flash_sale";
		String line = "PHIL T-Shirt How will Cactus evole during the next Ice Age? - Green,5060,PH230FAAD0MBANTH-147055,https://c.lazada.co.th/t/c.PVm?url=https%3A%2F%2Fwww.lazada.co.th%2Fproducts%2Fphil-t-shirt-how-will-cactus-evole-during-the-next-ice-age-green-i3055-s5060.html,PHIL,790.0,790.0,0.0,1,0,https://th-live.slatic.net/p/PHIL-PHIL-T-Shirt-How-will-Cactus-evole-during-the-next-Ice-Age?---Green-8333-177041-1-catalog.jpg,,https://th-live.slatic.net/p/PHIL-PHIL-T-Shirt-How-will-Cactus-evole-during-the-next-Ice-Age?---Green-8338-177041-2-catalog.jpg,https://th-live.slatic.net/p/PHIL-PHIL-T-Shirt-How-will-Cactus-evole-during-the-next-Ice-Age?---Green-8342-177041-3-catalog.jpg,https://th-live.slatic.net/p/140771-4472-177041-4-catalog.jpg,Fashion,Men,Clothing,Casual Tops,T-Shirts,0,0,0";
		
		try {
			StringReader lineReader = new StringReader(line);
			CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL.withHeader(HEADER.split(",")));
			for (CSVRecord csvRecord : record) {
				String name = String.valueOf(csvRecord.get("product_name"));
				System.out.println(name);
				System.out.println(csvRecord.toString());
			}
			
			
			
			line = line.replaceFirst(",", " LazMall,");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(line);
		
    }
}
