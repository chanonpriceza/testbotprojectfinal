package test.pao;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.json.simple.JSONArray;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class test_parse_object_mapper {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public static void main(String[] args) throws Exception {
		
		String[] text = new String[] {
				"{\"oldCategory\":20101}",
				"{\"productDetail\":{\"isOfficialShop\":false,\"promotions\":[{\"name\":\"ซื้อขั้นต่ำ ฿3,600.00 รับส่วนลดสูงสุด ฿1,800.00 \"},{\"name\":\"ฟรีของขวัญ\"}],\"shopScore\":\"78%\",\"shopName\":\"ENDLESS INTERTRADE\",\"isFreeShipping\":false}}",
				"{\"oldCategory\":20101,\"productDetail\":{\"isOfficialShop\":false,\"promotions\":[{\"name\":\"ซื้อขั้นต่ำ ฿3,600.00 รับส่วนลดสูงสุด ฿1,800.00 \"},{\"name\":\"ฟรีของขวัญ\"}],\"shopScore\":\"78%\",\"shopName\":\"ENDLESS INTERTRADE\",\"isFreeShipping\":false}}"
		};

		for (String txt : text) {
			try {
				
				DynamicData dynamicData = null;
				dynamicData = mapper.readValue(txt, DynamicData.class);

				if(dynamicData.getOldCategory() != 0) {
					System.out.println("oldCategory     : " + dynamicData.getOldCategory());
				}
				
				if(dynamicData.getProductDetail() != null) {
					System.out.println("shopName        : " + dynamicData.getProductDetail_shopName());
					System.out.println("shopScore       : " + dynamicData.getProductDetail_shopScore());
					System.out.println("soldCount       : " + dynamicData.getProductDetail_soldCount());
					System.out.println("isOfficialShop  : " + dynamicData.getProductDetail_isOfficialShop());
					System.out.println("isFreeShipping  : " + dynamicData.getProductDetail_isFreeShipping());
					System.out.println("promotions      : " + dynamicData.getProductDetail_promotions());
				}
				
				System.out.println("------------------------------");
				
				
			} catch (Exception ex) {
				System.out.println("UPDATE : Fail to parse dynamic field");
				throw ex;
			}
		}
	
	}	
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	private static class DynamicData {
		
		private int oldCategory;
		private JsonNode productDetail;
		
		public int getOldCategory() {
			return oldCategory;
		}
		
		public JsonNode getProductDetail() {
			return productDetail;
		}
		
		public String getProductDetail_shopName() {
			return (productDetail==null) ? null : productDetail.get("shopName").toString();
		}
		
		public String getProductDetail_shopScore() {
			return (productDetail==null) ? null : productDetail.get("shopScore").toString();
		}
		
		public String getProductDetail_soldCount() {
			return (productDetail==null) ? null : productDetail.get("soldCount").toString();
		}
		
		public Boolean getProductDetail_isOfficialShop() {
			return (productDetail==null) ? false : productDetail.get("isOfficialShop").asBoolean();
		}
		
		public Boolean getProductDetail_isFreeShipping() {
			return (productDetail==null) ? false : productDetail.get("isFreeShipping").asBoolean();
		}
		
		public List<String> getProductDetail_promotions(){
			return (productDetail==null) ? null : StreamSupport.stream(productDetail.get("promotions").spliterator(), false).map(x -> x.get("name").toString()).collect(Collectors.toList());
		}

	}
	
}
