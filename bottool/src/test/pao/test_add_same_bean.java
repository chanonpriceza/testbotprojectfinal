package test.pao;

import java.util.ArrayList;
import java.util.List;

public class test_add_same_bean {
	public static void main(String[] args) {
		
		List<somethingBean> aList = new ArrayList<>();
		
		somethingBean a = new somethingBean();
		a.setId(1);
		
		a.setName("Test a0"); a.setTest(false); aList.add(a);
		a.setName("Test a1"); a.setTest(false); aList.add(a);
		a.setName("Test a2"); a.setTest(false); aList.add(a);
		a.setName("Test a3"); a.setTest(true ); aList.add(a);
		a.setName("Test a4"); a.setTest(false); aList.add(a);
		a.setName("Test a5"); a.setTest(false); aList.add(a);
		a.setName("Test a6"); a.setTest(false); aList.add(a);
		a.setName("Test a7"); a.setTest(true ); aList.add(a);
		a.setName("Test a8"); a.setTest(false); aList.add(a);
		
		for (somethingBean bean : aList) {
			System.out.println(bean.getId() + "   " + bean.getName() + "   " + bean.isTest());
		}
		
		//////////////////////////////////////////////////////////////////
		System.out.println("\n\n\n");
		//////////////////////////////////////////////////////////////////
		
		List<somethingBean> bList = new ArrayList<>();
		
		somethingBean b = new somethingBean();
		
		b = new somethingBean(); b.setId(1); b.setName("Test b0"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b1"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b2"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b3"); b.setTest(true ); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b4"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b5"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b6"); b.setTest(false); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b7"); b.setTest(true ); bList.add(b);
		b = new somethingBean(); b.setId(1); b.setName("Test b8"); b.setTest(false); bList.add(b);
		
		for (somethingBean bean : bList) {
			System.out.println(bean.getId() + "   " + bean.getName() + "   " + bean.isTest());
		}
		
		
		
	}	
}

class somethingBean{
	
	int id;
	String name;
	boolean isTest;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isTest() {
		return isTest;
	}
	public void setTest(boolean isTest) {
		this.isTest = isTest;
	}
	
}

