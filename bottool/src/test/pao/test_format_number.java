package test.pao;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import Util.Util;

public class test_format_number {
	public static void main(String[] args) {
		
		DecimalFormat formatPrice = new DecimalFormat("###");
		String productPrice = "18732144";
		productPrice = formatPrice.format(Double.parseDouble(Util.removeCharNotPrice(productPrice)));
		System.out.println(productPrice);
		System.out.println(Util.convertPriceStr(productPrice));
		System.out.println(Double.parseDouble(productPrice));
		System.out.println(String.format("%.0f", Double.parseDouble(productPrice)));
		
		System.out.println("---------------------");
		
		String[] inputs = new String[] {
				"1000",
				"1200",
				"86451.1",
				"",
				"1as32dsa",
				"15432.15361",
				"-8645",
				"-3.477",
				"5.0000",
				"aaaaaa",
				"64132",
		};
		for (String input : inputs) {
			System.out.print(input);
			System.out.print("________");
			if(StringUtils.isNotBlank(input) && NumberUtils.isCreatable(input)) {
				System.out.println(String.valueOf((new DecimalFormat("#######.0").format(Double.parseDouble(input)))));
			} else {
				System.out.println("NO PASS CONDITION");
			}
		}
		
		System.out.println("-----------------");
		
		System.out.println(String.format("%08d", 50000) + "-" + String.format("%08d", 100000));
    }
}
