package test.pao;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

public class test_intersection_array {
	
	public static void main(String[] args) {
		
		Map<String, String[]> color = new HashMap<>();
		color.put("blue", new String[] {"123456"});
		color.put("green", new String[] {"789456123"});

		Map<String, String[]> size = new HashMap<>();
		size.put("s",  new String[] {"123456", "789456123"});
		size.put("m",  new String[] {"qqqqqq", "789456123"});
		size.put("l",  new String[] {"123456", "wwwwwwwww"});
		size.put("xl", new String[] {"asdsad", "czxxzczxc"});

		for(Entry<String, String[]> entryColor : color.entrySet()){
			String finalcolor = entryColor.getKey();
			String[] colorIdList = entryColor.getValue();
			
			for(Entry<String, String[]> entrySize : size.entrySet()){
				String finalsize = entrySize.getKey();
				String[] sizeIdList = entrySize.getValue();
				
				System.out.print(finalcolor + "  "); Stream.of(colorIdList).forEach(x -> {System.out.print(x); System.out.print(",");});
				System.out.println();
				
				System.out.print(finalsize + "  "); Stream.of(sizeIdList).forEach(x -> {System.out.print(x); System.out.print(",");});
				System.out.println();
				
				boolean checkValueInBoth = Collections.disjoint(Arrays.asList(colorIdList), Arrays.asList(sizeIdList));
				if(!checkValueInBoth){
					System.out.println("------------------------- MATCH → " + finalcolor + " + " + finalsize);
				}
				
				System.out.println();
			}

		
		
        }
    }
}
