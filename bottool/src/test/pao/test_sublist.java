package test.pao;

import java.util.Arrays;
import java.util.List;

public class test_sublist {
	public static void main(String[] args) {
		
		List<Integer> intList = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
		int size = intList.size();
		intList = intList.subList(7, size);
		
		for (Integer value : intList) {
			System.out.println(value);
		}
    }
}
