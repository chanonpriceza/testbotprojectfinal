package test.pao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class test_compare_date {
	
	public static void main(String[] args) throws ParseException {
		
		Date date1, date2, date3;
		
		date1 = generateDateHourOffset(new Date(), 24);
		date2 = new Date();
		
		System.out.println(date1.toString());
		System.out.println(date2.toString());
		
		// 1 is date1 is later than date2
		System.out.println(date1.compareTo(date2)); 
		
		///////////////////////////////////////////////////////////
		
		date1 = new Date();
		date2 = new Date();
		
		System.out.println(date1.toString());
		System.out.println(date2.toString());
		
		// 0 is date1 is equals date2
		System.out.println(date1.compareTo(date2)); 
		
		///////////////////////////////////////////////////////////
		
		date1 = new Date();
		date2 = generateDateHourOffset(new Date(), 24);
		
		System.out.println(date1.toString());
		System.out.println(date2.toString());
		
		// -1 is date1 is earlier than date2
		System.out.println(date1.compareTo(date2)); 
		
		
		///////////////////////////////////////////////////////////
		
		String dateStr1 = "2019-10-28 14:00:00";
		String dateStr3 = "2039-10-28 18:59:59";
		
		date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr1);
		date2 = new Date();
		date3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr3);
		
		if((date1.compareTo(date2) == -1) && (date2.compareTo(date3) == -1)) {
			System.out.println("BETWEEN    : [" + date1.toString() + "] - [" + date2.toString() + "] - [" + date3.toString() + "]");
		}else {
			System.out.println("NO BETWEEN : [" + date1.toString() + "] - [" + date2.toString() + "] - [" + date3.toString() + "]");
		}
		
		
		
	}
	
	
	
	private static Date generateDateHourOffset(Date date, int hourOffset) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, hourOffset);
		return c.getTime();
	}
}
