package test.pao;

import org.apache.commons.lang3.StringEscapeUtils;

public class test_unescapehtml4 {
	
	public static void main(String[] args) {
		
		String test = "Electronics &amp; Gadgets &gt; Mobile Accessories &gt; iPhone  deconcentrator";
		System.out.println(StringEscapeUtils.unescapeHtml4(test));
		
	}
	
}
