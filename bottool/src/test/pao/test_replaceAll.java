package test.pao;

public class test_replaceAll {
	
	public static void main(String[] args) {
		String input = "https://nocnoc.com/p/โทรทัศน์/Sharp AQOUS LED TV รุ่น LC-40SA5300X ขนาด 40 นิ้ว FULL TV\r\n" + 
				"FacebookTwitterEmailLineShare/10080904";
		
		String result = input.replaceAll("\r\n", " ").replaceAll("\\s+", " ");
		System.out.println(result);
		
		String productName = "Trylagina Serum 10x ไตรลาจีน่า เซรั่ม (ซื้อ 1 แถมฟรี 1+กระปุกเล็ก ขนาด 5 กรัม)(1352732313)";
		String replaceName = productName.toLowerCase()
				.replaceAll("trylagina", "Official trylagina")
				.replaceAll("ไตรลาจีน่า", "Official ไตรลาจีน่า")
				.replaceAll("ไตรลาจิน่า",  "Official ไตรลาจิน่า");
		System.out.println(replaceName);
		
	}
	
}
