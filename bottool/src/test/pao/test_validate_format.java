package test.pao;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class test_validate_format {
	public static void main(String[] args) {
		
		Pattern pattern = null;
		Matcher matcher = null;
		String[] inputs = null;
		String test = null;
		boolean matchFound = false;
		
		test = "\\d+(?:\\.\\d+)?%";
		inputs = new String[]{
				"20",  "0",  "",  "-1",  "20.0",  "91.2143545",  "0.1455145",  "-82.21",  "abcdef",  "LLL",  "11,22.33,44",  "", 
				"20%", "0%", "%", "-1%", "20.0%", "91.2413545%", "0.1454545%", "-82.21%", "abcdef%", "LLL%", "11,22,33,14"};
		
		pattern = Pattern.compile(test);
		for (CharSequence input : inputs) {
			matcher = pattern.matcher(input);
			matchFound = matcher.matches();
			System.out.println(String.format("matchFound %5s", matchFound) + " + input : " + input);
		
		}
		
    }
}
