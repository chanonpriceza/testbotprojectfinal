package test.pao;

import java.util.ArrayList;
import java.util.List;
import Util.Util;

public class test_check_word_in_all_list {
	
	public static void main(String[] args) {
		
		List<String> allLink = new ArrayList<>();
		List<String> resultList = new ArrayList<>();
		
		allLink.add("http://www.test.com/search?category=AAAAA&page=1&query=BBBBB");
		allLink.add("http://www.test.com/search?category=AAAAA&page=2&query=CCCCC");
		allLink.add("http://www.test.com/search?category=AAAAA&page=3&query=DDDDD");
		allLink.add("http://www.test.com/search?category=AAAAA&page=4&query=EEEEE");
		allLink.add("http://www.test.com/search?category=AAAAA&page=5&query-index=FFFFF");
		allLink.add("http://www.test.com/search?category=AAAAA&page=6");
		
		if(allLink != null && allLink.size()>0) {
			String mainLink = allLink.get(0);
			
			mainLink = Util.getStringAfter(mainLink, "//", mainLink);
			mainLink = Util.getStringAfter(mainLink, "/", mainLink);
			
			String[] result = mainLink.split("[&?.,!=+\\-*/\\^ ]+");
			
			for (String str : result) {
				boolean isAllLink = true;
				for (String link : allLink) {
					if(!link.contains(str)) isAllLink = false;
				}
				if(isAllLink && !resultList.contains(str)) {
					resultList.add(str);
				}
			}
		}
		System.out.println(resultList.toString());
		//[search, category, AAAAA, page]
		
	}
}
