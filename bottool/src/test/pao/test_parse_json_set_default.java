package test.pao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Util.Util;

public class test_parse_json_set_default {
	public static void main(String[] args) throws ParseException {
		
		String json = "{\"productId\" : 123124123,"
				+ "\"booleanInput\" : true,"
				+ "\"stringInput\" : \"DETAIL\""
				+ "}";

		JSONObject obj = (JSONObject) new JSONParser().parse(json);
		
		boolean booleanInput = Boolean.parseBoolean(StringUtils.defaultIfBlank(String.valueOf((Object) obj.get("booleanInput")), "false"));
		System.out.println(booleanInput);
		
		String stringInput1 = StringUtils.defaultIfBlank((String) obj.get("stringInput"), "DEFAULT");
		System.out.println(stringInput1);
		
		String stringInput2 = (String) obj.get("stringInput");
		System.out.println(stringInput2);
		
		String stringInput3 = (StringUtils.isNotBlank(stringInput2))? stringInput2 : "DEFAULT";
		System.out.println(stringInput3);
		
		
		json = "[{\"id\":\"49049\",\"name\":\"Diana Heels 3.2 in Copper pink\",\"price\":3290,\"base_price\":3290,\"description\":\"รองเท้าหนังแกะแท้ส้นสูง 3.2 นิ้ว ทรงหัวแหลมช่วยเก็บหน้าให้ได้รูปสวย แนะนำสำหรับสาวๆที่ชอบใส่ส้นสูง รุ่นนี้ทำจากหนังแกะแท้ รับประกันความนุ่มสบายใส่ได้ทั้งวัน ตอบโจทย์ที่สุด\",\"subcategory_id\":\"20000031\",\"url\":\"https://shop.line.me/@oandb/product/318601541\",\"image_url\":\"https://d.line-scdn.net/lcp-prod-photo/20191013_90/1570906810892PhM1C_JPEG/KORIMI2GND33GPQGJIGFOTEAJ1QS8O.jpg\",\"shop_id\":\"394\"},{\"id\":\"49050\",\"name\":\"Diana Heels 3.2 in Copper pink\",\"price\":3290,\"base_price\":3290,\"description\":\"รองเท้าหนังแกะแท้ส้นสูง 3.2 นิ้ว ทรงหัวแหลมช่วยเก็บหน้าให้ได้รูปสวย แนะนำสำหรับสาวๆที่ชอบใส่ส้นสูง รุ่นนี้ทำจากหนังแกะแท้ รับประกันความนุ่มสบายใส่ได้ทั้งวัน ตอบโจทย์ที่สุด\",\"subcategory_id\":\"20000031\",\"url\":\"https://shop.line.me/@oandb/product/318601541\",\"image_url\":\"https://d.line-scdn.net/lcp-prod-photo/20191013_90/1570906810892PhM1C_JPEG/KORIMI2GND33GPQGJIGFOTEAJ1QS8O.jpg\",\"shop_id\":\"394\"},{\"id\":\"49051\",\"name\":\"Diana Heels 3.2 in Copper pink\",\"price\":3290,\"base_price\":3290,\"description\":\"รองเท้าหนังแกะแท้ส้นสูง 3.2 นิ้ว ทรงหัวแหลมช่วยเก็บหน้าให้ได้รูปสวย แนะนำสำหรับสาวๆที่ชอบใส่ส้นสูง รุ่นนี้ทำจากหนังแกะแท้ รับประกันความนุ่มสบายใส่ได้ทั้งวัน ตอบโจทย์ที่สุด\",\"subcategory_id\":\"20000031\",\"url\":\"https://shop.line.me/@oandb/product/318601541\",\"image_url\":\"https://d.line-scdn.net/lcp-prod-photo/20191013_90/1570906810892PhM1C_JPEG/KORIMI2GND33GPQGJIGFOTEAJ1QS8O.jpg\",\"shop_id\":\"394\"},{\"id\":\"49052\",\"name\":";
		List<String> jsonList = Util.getAllStringBetween(json, "{", "}");
		if(jsonList != null && jsonList.size() > 0) {
			for (String jsonTxt : jsonList) {
				System.out.println(jsonTxt);
			}
		}
		
		
		
    }
}
