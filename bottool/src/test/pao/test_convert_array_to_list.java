package test.pao;

import java.util.Arrays;
import java.util.List;

public class test_convert_array_to_list {
	
	public static void main(String[] args) {
		
		String[] allLinkStr = {
			"http://www.test.com/-----1",
			"http://www.test.com/-----2",
			"http://www.test.com/-----3",
			"http://www.test.com/-----4",
			"http://www.test.com/-----5",
			"http://www.test.com/-----6"
		};
		
		System.out.println(Arrays.toString(allLinkStr));
		
		List<String> allLink = Arrays.asList(allLinkStr);
		for (String link : allLink) {
			System.out.println(link);
		}
		
	}
}
