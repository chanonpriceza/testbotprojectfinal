package test.pao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class test_gearbest_sign {
	
	private static final String mainAPI = "https://affiliate.gearbest.com/api/promotions/list-product-creative?api_key=[APIKEY]&time=[TIME]&type=[TYPE]&page=[PAGE]&sign=[SIGN]&lkid=[LKID]";
	private static final String _oldAPI = "https://affiliate.gearbest.com/api/products/list-promotion-products?api_key=[APIKEY]&currency=[CURRENCY]&page=[PAGE]&time=[TIME]&sign=[SIGN]";
	private static final String mainSign = "[APISECRET][PARAM][APISECRET]";
	private static final String _oldSign = "[APISECRET]api_key[APIKEY]currency[CURRENCY]page[PAGE]time[TIME][APISECRET]";
	private static final String apiKey = "1366135";
	private static final String apiSecret = "Ue5pzHlHPDQ";
	private static final String lkid = "78496144";
	
	public static void main(String[] args) {
				
		for(int i = 1; i< 10; i++) {
			
			System.out.println("Page " + i);
			
			Calendar d = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
			String dateInString = "31-12-2021 10:20:56";
			try {
				Date date = sdf.parse(dateInString);
				d.setTime(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
				
			System.out.println(d.getTime());
			
			String page = String.valueOf(i);
			String time = String.valueOf(d.getTimeInMillis());
			String sign = "";
			
			
			String mainParam = "api_key"+apiKey+"lkid"+lkid+"page"+page+"time"+time+"type1";
			sign = mainSign
					.replace("[APISECRET]", apiSecret)
					.replace("[PARAM]", mainParam)
					;
			System.out.println(sign);
			sign = toMD5(sign);
			sign = sign.toUpperCase();
			System.out.println(sign);
			String link = mainAPI
					.replace("[APIKEY]", apiKey)
					.replace("[TIME]", time)
					.replace("[TYPE]", "1")
					.replace("[PAGE]", page)
					.replace("[SIGN]", sign)
					.replace("[LKID]", lkid)
					;
			System.out.println(link);
			
			sign = _oldSign
					.replace("[APISECRET]", apiSecret)
					.replace("[APIKEY]", apiKey)
					.replace("[CURRENCY]", "USD")
					.replace("[PAGE]", page)
					.replace("[TIME]", time)
					.replace("[APISECRET]", apiSecret)
					;
			sign = toMD5(sign);
			sign = sign.toUpperCase();
			System.out.println(sign);
			String _oldLink = _oldAPI
					.replace("[APIKEY]", apiKey)
					.replace("[TIME]", time)
					.replace("[PAGE]", page)
					.replace("[SIGN]", sign)
					.replace("[CURRENCY]", "USD")
					;
			System.out.println(_oldLink);
			
			System.out.println();
		}
	}
	
	private static String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
}
