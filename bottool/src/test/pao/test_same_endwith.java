package test.pao;

public class test_same_endwith {
	public static void main(String[] args) {
		
		String[] allTxt = {
				"https://www.dropbox.com/s/pdjdyaphia0urd4/EclipseNeon.zip?dl=0",
				"https://www.dropbox.com/s/plcnlnc08bwxsx2/eclipse-foundation-201809.zip?dl=0",
				"https://www.dropbox.com/s/p8w4e0jdp5fb94p/jdk-8u102-windows-x64.zip?dl=0",
		};
		
		int size = allTxt.length;
		String[] reverseTxt = new String[size];
		
		for (int i=0; i<size; i++) {
			reverseTxt[i] = new StringBuilder(allTxt[i]).reverse().toString();
		}
		
		String mainTxt = reverseTxt[0];
		String result = "";

		OuterLoop :
		for(int i=0; i<mainTxt.length(); i++) {
			char mainChar = mainTxt.charAt(i);
			for(int j=0; j<size; j++) {
				if(reverseTxt[j].charAt(i) != mainChar) {
					break OuterLoop;
				}
			}
			result = mainChar + result;
		}
		
		System.out.println(result);
		
    }
}
