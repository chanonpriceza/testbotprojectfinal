package test.pao;

import java.util.Arrays;

public class test_check_contains_inline {
	
	public static void main(String[] args) {
		
		String a = "newText03";
		
		if(Arrays.asList(new String[] {"newText01", "newText02"}).contains(a)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	
	}	
	
	
}
