package test.pao;

public class test_kill_another_thread_2 {

	public static void main(String[] args) {

		Thread t1 = null, t2 = null;
		
		Thread.UncaughtExceptionHandler h = null;
		
		try {
			h = new Thread.UncaughtExceptionHandler() {
			    public void uncaughtException(Thread th, Throwable ex) {
			        System.out.println("Uncaught exception: " + ex);
			        throw new RuntimeException("Exception from thread " + ex);
			    }
			};
			
			t1 = new Thread("Process Thread") {
				public void run() {
					System.out.println("run by: " + getName());
					for (int i = 0; i < 1000; i++) {
						try {
							Thread.sleep(300);
							if (i % 50 == 0)
								System.out.print("\nCount : ");
							System.out.print(i % 10 + 1);
						} catch (InterruptedException e) {
							System.out.println("UPDATE after stop here");
							e.printStackTrace();
							return;
						}
					}
				}
			};

			t2 = new Thread("Check Thread") {
				public void run() {
					System.out.println("run by: " + getName());
					int i = 0;
					while (true) {
						try {
							Thread.sleep(1000);
							System.out.print("\n----- CHECK !!!\nCount : ");

							if (i > 5) {
								throw new RuntimeException("---------------- Stop !!!");
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (RuntimeException e) {
							System.out.println("CATCH INSIDE");
							throw e;
						}
						i++;
					}
				}
			};
			
			t1.setUncaughtExceptionHandler(h);
			t2.setUncaughtExceptionHandler(h);
			t1.start();
			t2.start();
		} catch (Exception e) {
			System.out.println("MAIN EXCEPTION");
			e.printStackTrace();
		}

	}


}
