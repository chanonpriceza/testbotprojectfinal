package test.pao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class test_parse_date {
	
	public static final String DATE_FORMAT = "yyyy.MM.dd";
	public static final String DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm a";
	
	public static void main(String[] args) throws ParseException {
		
		String a = "";
		
		a = "1890673200"; // 30.11.2029
		Calendar cal1 = Calendar.getInstance();
		cal1.setTimeInMillis(Long.parseLong(a)* 1000);
		System.out.println(new SimpleDateFormat(DATE_FORMAT).format(cal1.getTime()));
		System.out.println("--------------------------------------");
		
		
		a = "1575342000"; // 30.11.2029
		Calendar cal2 = Calendar.getInstance();
		cal2.setTimeInMillis(Long.parseLong(a)* 1000);
		System.out.println(cal2.getTime());
		System.out.println(new SimpleDateFormat(DATE_FORMAT).format(cal2.getTime()));
		System.out.println("--------------------------------------");

		a = "18/12/2019 4:29 pm";
		System.out.println(new SimpleDateFormat(DATE_TIME_FORMAT).parse(a));
		System.out.println("--------------------------------------");
		
		
	}
	
}
