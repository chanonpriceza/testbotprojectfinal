package test.pao;

import java.util.Calendar;
import java.util.Date;

public class test_date_vs_calendar {
	public static void main(String[] args) {
		
		Calendar cal = Calendar.getInstance();
		Date date1 = cal.getTime();
		
		Date date2 = new Date();
		long date3 = System.currentTimeMillis();
		Date date4 = new Date(date3);
		
		System.out.println("Date1 : " + date1); // Date1 : Tue Dec 04 12:58:51 ICT 2018
		System.out.println("Date2 : " + date2); // Date2 : Tue Dec 04 12:58:51 ICT 2018
		System.out.println("Date3 : " + date3); // Date3 : 1543903131564
		System.out.println("Date4 : " + date4); // Date4 : Tue Dec 04 12:58:51 ICT 2018
		
    }
}
