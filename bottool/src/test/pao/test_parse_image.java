package test.pao;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.lang3.StringUtils;


public class test_parse_image {
	
	public static final int IMAGE_WIDTH = 200;
	public static final int IMAGE_HEIGHT = 200;
	public static final String IMAGE_OUTPUT = "D:\\test_file\\test_parse_image_output.jpg";
	
	public static void main(String[] args) throws ParseException {
		
		String imageUrl = "https://cf.shopee.co.th/file/fa5302c946b568797ac0b135540d4564";
		byte[] imageByte = parseImage(imageUrl);
		
		System.out.println("imageByte.length = " + imageByte.length);
		if(imageByte != null && imageByte.length > 0) {
			try {
				BufferedImage imgBuffer = ImageIO.read(new ByteArrayInputStream(imageByte));
				ImageIO.write(imgBuffer, "jpg", new File(IMAGE_OUTPUT));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private static byte[] parseImage(String image) {
		try {
			if(StringUtils.isBlank(image)) return null;
			
			URL url = new URL(image);			
			ImageIcon ii = new ImageIcon(url);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if(iWidth == -1 && iHeight == -1)
				return null;
			
			double xPos = (IMAGE_WIDTH - 1 * iWidth) / 2;
			double yPos = (IMAGE_HEIGHT - 1 * iHeight) / 2;
			Image temp = null;
			
			if (iHeight > IMAGE_HEIGHT || iWidth > IMAGE_WIDTH) {
				if (iWidth > iHeight)
					resizedImage = i.getScaledInstance(IMAGE_WIDTH, (IMAGE_HEIGHT * iHeight) / iWidth, Image.SCALE_SMOOTH);
				else
					resizedImage = i.getScaledInstance((IMAGE_WIDTH * iWidth) / iHeight, IMAGE_HEIGHT, Image.SCALE_SMOOTH);

				double xScale = (double) IMAGE_WIDTH / iWidth;
				double yScale = (double) IMAGE_HEIGHT / iHeight;
				double scale = Math.min(xScale, yScale);

				xPos = (IMAGE_WIDTH - scale * iWidth) / 2;
				yPos = (IMAGE_HEIGHT - scale * iHeight) / 2;
				temp = new ImageIcon(resizedImage).getImage();
				
			} else {
				temp = i;
			}
					
			BufferedImage bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
			
			Graphics g = bufferedImage.createGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
			g.drawImage(temp, (int)xPos, (int)yPos, null);
			g.dispose();

			try(ByteArrayOutputStream outStream = new ByteArrayOutputStream()){
				
				ImageIO.write(bufferedImage, "jpg", outStream);
				outStream.flush();
				byte[] imageInByte = outStream.toByteArray();
				outStream.close();
				
				return imageInByte;
			}
		} catch (Exception e) {
			System.out.println(new Date() + " : Exception when parseImage " + e.toString());
		}
		return null;
	}
	
}
