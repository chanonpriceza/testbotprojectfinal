package test.pao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class test_read_file_with_buffer_size {
	
	public static void main(String[] args) {
		
		String fileName = "D:\\Eclipse\\workspace\\default\\__Bot_5.0\\data\\product-feed.xml";
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF-8"), 10);
			 PrintWriter pwr = new PrintWriter(new FileOutputStream(new File("D:\\Eclipse\\workspace\\default\\__Bot_5.0\\data\\product-feed-2.xml"),  false))){
			
			String text = "";
			while((text = br.readLine()) != null) {
				text = text.replaceAll("\r\n", "");
				pwr.print(text.toString().replaceAll("</item>","</item>\n"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
}
