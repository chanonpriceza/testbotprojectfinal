package test.pao;

import java.io.IOException;
import java.util.List;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

public class test_sftp_files_listing_sshclient {

	public static void main(String[] args) throws IOException {
		String SFTPHOST = "27.254.85.9";
	    int SFTPPORT = 98;
	    String SFTPUSER = "sale";
	    String SFTPPASS = "Sale@pr1ceza#";
	    String SFTPWORKINGDIR = "/home/sale";
	
		SSHClient ssh = new SSHClient();
		SFTPClient sftp  = null;
		
		try {
			ssh.addHostKeyVerifier(new PromiscuousVerifier());
			ssh.connect(SFTPHOST, SFTPPORT);
			ssh.authPassword(SFTPUSER, SFTPPASS);
			sftp = ssh.newSFTPClient();
			List<RemoteResourceInfo> files = sftp.ls(SFTPWORKINGDIR);
			if(files != null && files.size() > 0) {
				for (RemoteResourceInfo remoteResourceInfo : files) {
					System.out.println(remoteResourceInfo.getName());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();;
		} finally {
			ssh.close();
			sftp.close();
			ssh.disconnect();
		}
	    
	}
	
}