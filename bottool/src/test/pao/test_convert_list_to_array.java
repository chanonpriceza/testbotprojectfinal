package test.pao;

import java.util.ArrayList;
import java.util.List;

public class test_convert_list_to_array {
	
	public static void main(String[] args) {
		
		String[] result = null;
		
		List<String> a = new ArrayList<>();
		a.add("1");
		a.add("2");
		a.add("3");
		a.add("4");
		a.add("5");
		a.add("6");
		a.add("7");
		
		result = new String[a.size()];
		result = a.toArray(result);
		
		for(String b : result) {
			System.out.print(b);
		}
		
	}
}
