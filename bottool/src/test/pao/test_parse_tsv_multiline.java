package test.pao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import Util.DateTimeUtil;

public class test_parse_tsv_multiline {
	
//	private static final String file1 = "D:\\central-maincat-" + DateTimeUtil.generateStringDate(new Date(), "yyyy-MM-DD") + ".txt";
//	private static final String file2 = "D:\\central-subcat-" + DateTimeUtil.generateStringDate(new Date(), "yyyy-MM-DD") + ".txt";
	
	private static List<String> mainCatList, subCatList;
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		mainCatList = new ArrayList<>();
		subCatList = new ArrayList<>();
		
//		try(PrintWriter pwr1 = new PrintWriter(new FileOutputStream(new File(file1),  true));
//			PrintWriter pwr2 = new PrintWriter(new FileOutputStream(new File(file2),  true));){
//			pwr1.println("mainCat|productName");
//			pwr2.println("subCat|productName");
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		
		String fileName = "D:\\2.txt";
		int count = 0;
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF-8"))){			
			String line = null;
			String tmp = "";
			while ((line = br.readLine()) != null) {		
				try{
					tmp += line;
					StringReader lineReader = new StringReader(tmp);
					String[]   HEADERS     = {"id", "google_product_category", "product_type", "condition", "availability", "title", "description", "image_link", "brand", "price", "sale_price", "size", "size_id", "color", "color_id", "pr_id", "item_group_id", "link"};
					CSVParser record = new CSVParser(lineReader, CSVFormat.TDF.withHeader(HEADERS));
					for (CSVRecord csvRecord : record) {
						System.out.println("success : " + (++count));
						parseCSV(csvRecord);
						tmp = "";
					}
				}catch(Exception e){
					// no need log
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	private static void parseCSV(CSVRecord data) {
		String id                       = data.get(0);
		String google_product_category  = data.get(1);
		String product_type             = data.get(2);
		String condition                = data.get(3);
		String availability             = data.get(4);
		String title                    = data.get(5);
		String description              = data.get(6);
		String image_link               = data.get(7);
		String brand                    = data.get(8);
		String price                    = data.get(9);
		String sale_price               = data.get(10);
		String size                     = data.get(11);
		String size_id                  = data.get(12);
		String color                    = data.get(13);
		String color_id                 = data.get(14);
		String pr_id                    = data.get(15);
		String item_group_id            = data.get(16);
		String link                     = data.get(17);
		
//		if(!mainCatList.contains(google_product_category)) {
////			mainCatList.add(google_product_category);
//			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(file1),  true));){
//				pwr.println(google_product_category + "|" + title);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
		
//		if(!subCatList.contains(product_type)) {
////			subCatList.add(product_type);
//			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(file2),  true));){
//				pwr.println(product_type + "|" + title);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
		
		System.out.println(id);
		System.out.println(google_product_category);
		System.out.println(product_type);
		System.out.println(condition);
		System.out.println(availability);
		System.out.println(title);
		System.out.println(description);
		System.out.println(image_link);
		System.out.println(brand);
		System.out.println(price);
		System.out.println(sale_price);
		System.out.println(size);
		System.out.println(size_id);
		System.out.println(color);
		System.out.println(color_id);
		System.out.println(pr_id);
		System.out.println(item_group_id);
		System.out.println(link);
		System.out.println("------------------------------------------------");
		
		
		
	}
	
}
