package test.pao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class test_kill_another_thread_3 {

	public static void main(String[] args) {

		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		try {
			
			Future<?> mainProcess = executor.submit(new Runnable() {
				public void run() {
					System.out.println("Process Run.");
					for (int i = 0; i < 1000; i++) {
						try {
							Thread.sleep(300);
							if (i % 50 == 0) System.out.print("\n");
							
							System.out.print(i % 10 + 1);
						} catch (InterruptedException e) {
							System.out.println("UPDATE after stop here");
							return;
						}
					}
				}
			});

			executor.submit(new Runnable() {
				public void run() {
					System.out.println("Check Run.");
					int i = 0;
					while (true) {
						try {
							Thread.sleep(1000);
							System.out.println("\n----- CHECK !!! ");
							if(mainProcess.isDone() || mainProcess.isCancelled()) {
								System.out.println("No main process anymore.");
								return;
							}
							if (i > 5) {
								if(!mainProcess.isDone()) {
									mainProcess.cancel(true);
									return;
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							return;
						}
						i++;
					}
				}
			});
			
			executor.shutdown();
			if(executor.awaitTermination(24, TimeUnit.HOURS)){
				System.out.println("Done All.");
			}else{
				System.out.println("Time out.");
				executor.shutdownNow();
			}
			
			System.out.println("NEXT STEP");
			
		} catch (Exception e) {
			System.out.println("MAIN EXCEPTION");
			e.printStackTrace();
		}

	}


}
