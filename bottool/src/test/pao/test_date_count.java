package test.pao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class test_date_count {
	public static void main(String[] args) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = "2018-12-9";
		
		try {
			
			Date target = formatter.parse(dateStr);
			Date start = new Date();

			long diff = start.getTime() - target.getTime();
			
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
    }
}
