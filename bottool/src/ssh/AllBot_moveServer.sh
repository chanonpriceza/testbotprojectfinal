moveLazadaTH(){
        inputPath=$1
        inputFile=$2
        sourcePath="${inputPath}new_${inputFile}"
        sourceFile="new_${inputFile}"
        targetFile="temp_${inputFile}"

        if [ -f $sourcePath ] ; then
                checkProcess=$(lsof $sourcePath)
                if [ "$checkProcess" = "" ] ; then
                        echo `date` : "Start  transfer $sourcePath"

                        scp -P98 $sourcePath sale@27.254.82.243:/home/sale/314-lazada/
                        echo `date` : "Finish transfer ${sourcePath}"

                        ssh sale@27.254.82.243 -p 98 mv "/home/sale/314-lazada/${sourceFile}" "/home/sale/314-lazada/${targetFile}"
                        echo `date` : "Rename file to /home/sale/314-lazada/${targetFile}"

                        rm -f $sourcePath
                        echo `date` : "Delete source file ${sourcePath}"
                        echo `date` : "-------------"
                fi
        fi
}

moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_skuFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_catFilterPrice3000File.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_catFilterPrice5000File.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_catLocalFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_catSpecificFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_lazMallFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/" "crawl_skuFile.csv.tar.gz"
