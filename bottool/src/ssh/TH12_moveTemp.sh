moveFile(){
	fileSource=$1
	fileTarget=$2
	checkSize=$3

	if [ "$checkSize" = "" ] ; then
			checkSize=0
	fi

	if [ -f $fileSource ] && [ -f $fileTarget ] ; then
		fileSize=$(wc -c $fileSource | awk '{print $1}')	   
		if [ $fileSize -gt $checkSize ] ; then
			processSource=$(lsof $fileSource)
			processTarget=$(lsof $fileTarget)
			if [ "$processSource" = "" ] && [ "$processTarget" = "" ] ; then
				mv -f $fileSource $fileTarget
				echo [`date`] : Moved $fileSource to $fileTarget
			else
				echo [`date`] : File $fileSource is using by another process, skip.
			fi
		fi
	fi
}

moveFile "/home/sale/5003001_temp.xml" "/home/sale/5003001.xml" 5000000
moveFile "/home/sale/4003001_temp.xml" "/home/sale/4003001.xml" 5000000
moveFile "/home/sale/314-lazada/temp_crawl_catFilterPrice3000File.csv.tar.gz" "/home/sale/314-lazada/crawl_catFilterPrice3000File.csv.tar.gz"
moveFile "/home/sale/314-lazada/temp_crawl_catFilterPrice5000File.csv.tar.gz" "/home/sale/314-lazada/crawl_catFilterPrice5000File.csv.tar.gz"
moveFile "/home/sale/314-lazada/temp_crawl_catLocalFile.csv.tar.gz" "/home/sale/314-lazada/crawl_catLocalFile.csv.tar.gz"
moveFile "/home/sale/314-lazada/temp_crawl_catSpecificFile.csv.tar.gz" "/home/sale/314-lazada/crawl_catSpecificFile.csv.tar.gz"
moveFile "/home/sale/314-lazada/temp_crawl_lazMallFile.csv.tar.gz" "/home/sale/314-lazada/crawl_lazMallFile.csv.tar.gz"
moveFile "/home/sale/314-lazada/temp_crawl_skuFile.csv.tar.gz" "/home/sale/314-lazada/crawl_skuFile.csv.tar.gz"


                    
                                                   