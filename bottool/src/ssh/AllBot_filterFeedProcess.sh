
echo `date` : start filterFeedProcess.sh

compressTarGz(){
	sourceFile=$1
	targetFile=$2

	if [ -f $sourceFile ] ; then
		checkProcess=$(lsof $sourceFile)
		if [ "$checkProcess" = "" ] ; then
			rm -f $targetFile
			echo `date` : delete old file $targetFile

			echo `date` : compressing $sourceFile
			tar cvzf $targetFile $sourceFile
			echo `date` : complete $sourceFile

			rm -f $sourceFile
			echo `date` : delete source file $sourceFile
		fi
	fi
}

#-----------------LAZADA-TH------------------

echo `date` : start filter-lazada-th

if [ -f /var/lib/mysql/lazada/th/priceza_main_th_marketing_feed.csv.gz ] ; then
	processFilterLazadaTH=$(lsof /var/lib/mysql/lazada/th/priceza_main_th_marketing_feed.csv.gz)
	if [ "$processFilterLazadaTH" = "" ] ; then
		/usr/local/java/jdk1.8.0_112/bin/java -cp /home/priceza/feed/tool/tool_runner.jar:/home/priceza/feed/tool/lib/* tool.tool_filter_lazada_th ;
	fi
fi

compressTarGz "/var/lib/mysql/lazada/th/result/crawl_catFilterPrice3000File.csv" "/var/lib/mysql/lazada/th/result/new_crawl_catFilterPrice3000File.csv.tar.gz"
compressTarGz "/var/lib/mysql/lazada/th/result/crawl_catFilterPrice5000File.csv" "/var/lib/mysql/lazada/th/result/new_crawl_catFilterPrice5000File.csv.tar.gz"
compressTarGz "/var/lib/mysql/lazada/th/result/crawl_catLocalFile.csv" "/var/lib/mysql/lazada/th/result/new_crawl_catLocalFile.csv.tar.gz"
compressTarGz "/var/lib/mysql/lazada/th/result/crawl_catSpecificFile.csv" "/var/lib/mysql/lazada/th/result/new_crawl_catSpecificFile.csv.tar.gz"
compressTarGz "/var/lib/mysql/lazada/th/result/crawl_lazMallFile.csv" "/var/lib/mysql/lazada/th/result/new_crawl_lazMallFile.csv.tar.gz"
compressTarGz "/var/lib/mysql/lazada/th/result/crawl_skuFile.csv" "/var/lib/mysql/lazada/th/result/new_crawl_skuFile.csv.tar.gz"

echo `date` : finish filter-lazada-th

#-----------------LAZADA-ID------------------

#-----------------END------------------------

echo `date` : finish filterFeedProcess.sh

