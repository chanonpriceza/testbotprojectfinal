
echo `date` : start  test_toon.sh

    exceedAlert(){
        alertCode=$1
        alertCount=$2
        alertDate=$3

        if [ "$alertCount" = "" ] ; then
            alertCount=0
        fi
        echo `date` : alertType $alertCode $alertDate = $alertCount
        if [ $alertCount -gt 10 ] ; then
            #insert script to send email here
            echo `date` : send alert email.
        fi
    }

    checkLog(){
        targetLogPath=$1
        targetYear=$2
        targetMonth=$3
        targetDate=$4
        targetHour=$5
        targetMin=$6

        if [ -f $targetLogPath ] ; then
            if [ $targetMin -lt 0 ] ; then
                targetMin=$((targetMin+60))
                targetHour=$((targetHour-1))
                if [ $targetMin -lt 10 ] ; then 
                    targetMin='0'$targetMin
                fi
            fi
            if [ $targetHour -lt 0 ] ; then
                targetHour=$((targetHour+24))
                targetDate=$((targetDate-1))
            fi

            countGrep=$(grep -c -E '$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin.*HTTP/1.1" 503|$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin.*HTTP/2.0" 503' $targetLogPath);
            exceedAlert "503" "$countGrep" "$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin"
            
            #add another error code for check like this 
            #countGrep=$(grep -c -E '$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin.*HTTP/1.1" 404|$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin.*HTTP/2.0" 404' $targetLogPath);
            #exceedAlert "404" "$countGrep" "$targetDate/$targetMonth/$targetYear:$targetHour:$targetMin"
            

        fi
    }

    #-----------------PROGRESS------------------

    varYear=$(date +"%Y");
    varMonth=$(date +"%b");
    varDate=$(date +"%d");
    varHour=$(date +"%H");
    varMin=$(date +"%M");

    for (( varCount=0; varCount<60; varCount++))
    do
        checkLog "/home/pao/testlog.log" $varYear $varMonth $varDate $varHour $((varMin-varCount))
    done

echo `date` : finish test_toon.sh
