
moveLazadaTH(){
	sourceFile=$1
	if [ -f $sourceFile ] ; then
		checkProcess=$(lsof $sourceFile)
		if [ "$checkProcess" = "" ] ; then
			echo `date` : Start  transfer $sourceFile 
			scp -P98 $sourceFile sale@27.254.82.243:/home/sale/314-lazada/ 
			rm -f $sourceFile
			echo `date` : Finish transfer $sourceFile
		fi
	fi
}

moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_skuFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_catFilterPrice3000File.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_catFilterPrice5000File.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_catLocalFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_catSpecificFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_lazMallFile.csv.tar.gz"
moveLazadaTH "/var/lib/mysql/lazada/th/result/new_crawl_skuFile.csv.tar.gz"
                                                                            
