package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.BotUrlPatternBean;

public class BotUrlPatternDB {
	
	private ResultSetHandler<List<BotUrlPatternBean>> beanListHandler;
	
	private static final String SELECT_BY_MERCHANTID = "SELECT * FROM tbl_url_pattern WHERE merchantId = ?";
	private static final String SELECT_BY_MERCHANTID_ACTION = "SELECT * FROM tbl_url_pattern WHERE merchantId = ? AND action = ?";
	private QueryRunner queryRunner;
	
	public BotUrlPatternDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		beanListHandler = new BeanListHandler<BotUrlPatternBean>(BotUrlPatternBean.class);	
	}
	
	public List<BotUrlPatternBean> findUrlPattern(int merchantId) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID, beanListHandler, merchantId);
	}
	
	public List<BotUrlPatternBean> findUrlPattern(int merchantId, String action) throws SQLException {		
		return queryRunner.query(SELECT_BY_MERCHANTID_ACTION, beanListHandler, merchantId, action);
	} 
}
