package db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import Util.Util;
import bean.BotSendDataBean;

public class BotSendDataDB {		
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
		
	private QueryRunner queryRunner;
	private static final String INSERT = "INSERT INTO tbl_send_data" +
			"(action, merchantId, categoryId, name, price, url, description, pictureData, pictureUrl, " +
			" merchantUpdateDate, updateDate, realProductId, keyword, basePrice, upc, dynamicField) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public BotSendDataDB(DataSource dataSource) {	
		this.queryRunner = new QueryRunner(dataSource);
	}
	
	public long insertSendData(BotSendDataBean sendDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				sendDataBean.getAction(),
				sendDataBean.getMerchantId(),
				sendDataBean.getCategoryId(),
				Util.limitString(sendDataBean.getName(), NAME_LENGTH),
				sendDataBean.getPrice(),
				sendDataBean.getUrl(),
				Util.limitString(sendDataBean.getDescription(), DESCRIPTION_LENGTH), 
				sendDataBean.getPictureData(), 
				sendDataBean.getPictureUrl(),				
				sendDataBean.getMerchantUpdateDate(),
				sendDataBean.getUpdateDate(),
				sendDataBean.getRealProductId(),
				sendDataBean.getKeyword(),
				sendDataBean.getBasePrice(),
				sendDataBean.getUpc(),
				sendDataBean.getDynamicField()
		}); 		
	}
		
}
