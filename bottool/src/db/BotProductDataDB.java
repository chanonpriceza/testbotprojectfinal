package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import Util.Util;
import bean.BotProductDataBean;
import bean.BotProductDataBean.STATUS;

public class BotProductDataDB {
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	public static final int KEYWORD_LENGTH = 500;
	
	private ArrayListHandler arrayListHandler;
	private ResultSetHandler<List<BotProductDataBean>> arrayBeanListHandler;
	private QueryRunner queryRunner;
	
	public BotProductDataDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		arrayListHandler = new ArrayListHandler();
		arrayBeanListHandler = new BeanListHandler<BotProductDataBean>(BotProductDataBean.class);
	}
	
	private static final String INSERT = "INSERT IGNORE INTO tbl_product_data" +
			"(merchantId, name, price, url, urlForUpdate, description, pictureUrl, " +
			" status, updateDate, errorUpdateCount, categoryId, keyword, merchantUpdateDate, realProductId, basePrice, upc) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public int insertProductData(BotProductDataBean productDataBean) throws SQLException {
		return queryRunner.update(INSERT, new Object[] {
				productDataBean.getMerchantId(),
				Util.limitString(productDataBean.getName(), NAME_LENGTH),
				productDataBean.getPrice(),
				productDataBean.getUrl(),
				productDataBean.getUrlForUpdate(),
				Util.limitString(productDataBean.getDescription(), DESCRIPTION_LENGTH), 
				productDataBean.getPictureUrl(),
				STATUS.C.toString(),
				productDataBean.getUpdateDate(),
				productDataBean.getErrorUpdateCount(),
				productDataBean.getCategoryId(),
				productDataBean.getKeyword(),
				productDataBean.getMerchantUpdateDate(),
				productDataBean.getRealProductId(),
				productDataBean.getBasePrice(),
				productDataBean.getUpc()

		}); 		
	}
	
	private static final String SELECT_REALPRODUCTID = "SELECT tbl_product_data.REAL_PRODUCT_ID, tbl_product_data.Id FROM tbl_product_data INNER JOIN "
			+ " (SELECT Id FROM tbl_product_data WHERE MERCHANT_ID = ? AND STATUS != 4 LIMIT ?, ?) AS p ON tbl_product_data.Id = p.Id ";
	public List<Object[]> getRealProductId(int merchantId, int start, int offset) throws SQLException {
		List<Object[]> resultSet = queryRunner.query(SELECT_REALPRODUCTID, arrayListHandler, merchantId, start, offset);
		return resultSet;
	}
	
	private static final String SELECT_FIELD_FOR_BOT_BY_MERCHANT_ID = "SELECT tbl_product_data.REAL_PRODUCT_ID, tbl_product_data.Id FROM tbl_product_data INNER JOIN "
			+ " (SELECT Id FROM tbl_product_data WHERE MERCHANT_ID = ? AND STATUS != 4 LIMIT ?, ?) AS p ON tbl_product_data.Id = p.Id ";
	public List<BotProductDataBean> getFieldForBotByMerchantId(int merchantId, int start, int offset) throws SQLException {
		List<BotProductDataBean> resultSet = queryRunner.query(SELECT_FIELD_FOR_BOT_BY_MERCHANT_ID, arrayBeanListHandler, merchantId, start, offset);
		return resultSet;
	}
}
