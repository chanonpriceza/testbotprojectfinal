package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import bean.BotMerchantConfigBean;
import bean.BotMerchantConfigBean.FIELD;

public class BotMerchantConfigDB {
		
	private ResultSetHandler<List<BotMerchantConfigBean>> beanListHandler;
	private ScalarHandler<String> stringValueHandler;
	private QueryRunner queryRunner;
	
	private static final String SELECT_BY_MERCHANTID_FIELD = "SELECT value FROM tbl_merchant_config WHERE merchantId = ? and field = ?";
	
	public BotMerchantConfigDB(DataSource ds) {
		this.queryRunner = new QueryRunner(ds);
		stringValueHandler = new ScalarHandler<>();
		beanListHandler = new BeanListHandler<BotMerchantConfigBean>(BotMerchantConfigBean.class);		
	}
	
	public String findMerchantConfigValue(int merchantId, String field) throws SQLException {
		return queryRunner.query(SELECT_BY_MERCHANTID_FIELD, stringValueHandler, merchantId, field);
	}
		
}
