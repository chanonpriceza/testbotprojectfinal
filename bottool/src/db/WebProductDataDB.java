package db;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import bean.WebProductDataBean;

public class WebProductDataDB {
	public static final int NAME_LENGTH = 200;
	public static final int DESCRIPTION_LENGTH = 500;
	public static final int KEYWORD_LENGTH = 500;
	
	private ResultSetHandler<List<WebProductDataBean>> arrayBeanListHandler;
	private QueryRunner queryRunner;
	
	public WebProductDataDB(DataSource dataSource) {
		this.queryRunner = new QueryRunner(dataSource);
		arrayBeanListHandler = new BeanListHandler<WebProductDataBean>(WebProductDataBean.class);
	}
	
	private static final String SELECT_FIELD_FOR_BOT_BY_MERCHANT_ID = "SELECT "
			+ "  tbl_product_data.MERCHANT_ID"
			+ ", tbl_product_data.NAME"
			+ ", tbl_product_data.DESCRIPTION"
			+ ", tbl_product_data.PRICE"
			+ ", tbl_product_data.BASE_PRICE"
			+ ", tbl_product_data.URL"
			+ ", tbl_product_data.PICTURE_URL"
			+ ", tbl_product_data.SUBCATEGORY"
			+ ", tbl_product_data.PRODUCT_KEYWORD"
			+ ", tbl_product_data.UPC"
			+ ", tbl_product_data.REAL_PRODUCT_ID"
			+ " FROM tbl_product_data INNER JOIN "
			+ " (SELECT Id FROM tbl_product_data WHERE MERCHANT_ID = ? AND STATUS != 4 LIMIT ?, ?) AS p ON tbl_product_data.Id = p.Id ";
	public List<WebProductDataBean> getFieldForBotByMerchantId(int merchantId, int start, int offset) throws SQLException {
		List<WebProductDataBean> resultSet = queryRunner.query(SELECT_FIELD_FOR_BOT_BY_MERCHANT_ID, arrayBeanListHandler, merchantId, start, offset);
		return resultSet;
	}
}
