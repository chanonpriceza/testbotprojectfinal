package tool.prod_id_4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;

import Util.DatabaseUtil;
import bean.WebProductDataBean;
import db.WebProductDataDB;

public class tool_dumb_csv_from_productdata_by_merchant_id {

	private static final int OFFSET = 10000;
	private static final String OUTPUT_PATH = "E:\\";
//	private static final String OUTPUT_PATH = "/home/pao/";
	private static final int[] merchantIdList = new int[] {
			37,			// Lazada
			103011,		// Tees
			2003010,	// Tokopedia Marketplace
			2130044		// iLotte
	};
	
	private DataSource backupWebIndoDS;
	private WebProductDataDB webProductDataDB;
	
	public static void main(String[] args) {
		System.out.println(new Date() + " " +new Date() + " : Start tool_filter_lazada");
		try {
			tool_dumb_csv_from_productdata_by_merchant_id runner = new tool_dumb_csv_from_productdata_by_merchant_id();
			runner.setDatabase();
			runner.process();
		} catch(RuntimeException e) {
			System.out.println(new Date() + " " +new Date() + " : " + e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " " +new Date() + " : Finish tool_filter_lazada");
    }
	
	private void setDatabase() {
		String webDBDriverName 	= "com.mysql.jdbc.Driver";
		String webDBURL 		= "jdbc:mysql://119.235.248.232:3306/priceza";
		String webDBUserName 	= "pao";
		String webDBPassword 	= "PorIn98@";		
		
		backupWebIndoDS = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
		webProductDataDB = new WebProductDataDB(backupWebIndoDS);
	}
	
	private void process() {
		
		for (int merchantId : merchantIdList) {
			System.out.println(new Date() + " -----------------------------------------------------------------");
			System.out.println(new Date() + " Start " + merchantId);
			int start = 0;
			boolean haveResult = false;
			do {
				haveResult = false;
				try {
					int resultCount = 0;
					String countRange = String.format("%08d", start) + "_" + String.format("%08d", start+OFFSET);
					List<WebProductDataBean> pdBeanList = webProductDataDB.getFieldForBotByMerchantId(merchantId, start, OFFSET);
					if(pdBeanList != null && pdBeanList.size() > 0) {
						haveResult = true;
						resultCount = pdBeanList.size();
					}
					
					System.out.println(new Date() + " From MerchantId " + merchantId + " Range" + countRange + " have " + resultCount + " results");
					if(haveResult) {
						try (PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(OUTPUT_PATH + countRange + ".csv"),  false));){
							pwr.println("merchantId\t"
									+ "productName\t"
									+ "productDesc\t"
									+ "productPrice\t"
									+ "productBasePrice\t"
									+ "productUrl\t"
									+ "productUrlForUpdate\t"
									+ "productPictureUrl\t"
									+ "productCatId\t"
									+ "productKeyword\t"
									+ "productUpc\t"
									+ "productRealProductId"
									);
							for (WebProductDataBean WebProductDataBean : pdBeanList) {
								String productUrlForUpdate = WebProductDataBean.getUrl();
								if(merchantId == 37) productUrlForUpdate = WebProductDataBean.getReal_product_id();
								if(merchantId == 103011) productUrlForUpdate = WebProductDataBean.getReal_product_id();
								if(merchantId == 2003010) productUrlForUpdate = WebProductDataBean.getReal_product_id();
								
								pwr.println(WebProductDataBean.getMerchant_id() + "\t"
										+ WebProductDataBean.getName() + "\t" 
										+ StringUtils.defaultIfBlank(WebProductDataBean.getDescription(), "").replaceAll("\r\n", "").replaceAll("\\s+", "") + "\t" 
										+ WebProductDataBean.getPrice() + "\t" 
										+ WebProductDataBean.getBase_price() + "\t" 
										+ WebProductDataBean.getUrl() + "\t" 
										+ productUrlForUpdate + "\t" 
										+ WebProductDataBean.getPicture_url() + "\t" 
										+ WebProductDataBean.getSubcategory() + "\t" 
										+ WebProductDataBean.getProduct_keyword() + "\t" 
										+ WebProductDataBean.getUpc() + "\t" 
										+ WebProductDataBean.getReal_product_id()  
										);
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				start += OFFSET;
				
			}while(haveResult);
			
			System.out.println(new Date() + " Finish " + merchantId);
			
		}
		
	}
	
	
	
	
	
}
