package tool.bot_all;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import Util.DatabaseUtil;
import Util.FTPUtil;
import bean.BotUrlPatternBean;
import db.BotMerchantConfigDB;
import db.BotUrlPatternDB;

public class tool_filter_lazada_th {

//	ALL BOT
	private static final String FEED_SOURCE_PATH = "/var/lib/mysql/lazada/th/";
	private static final String FEED_STORE_PATH = "/var/lib/mysql/lazada/th/result/";
	
//	MY-IMAGE
//	private static final String FEED_SOURCE_PATH = "/home/priceza/lazada/";
//	private static final String FEED_STORE_PATH = "/home/priceza/lazada/result/";
	
//	LOCAL
//	private static final String FEED_SOURCE_PATH = "D:\\test_file\\";
//	private static final String FEED_STORE_PATH = "D:\\test_file\\lazada_result\\";
	
	private static final String[] fileList = new String[] {
		"priceza_main_th_marketing_feed.csv.gz",
		"priceza_lazmall_th_marketing_feed.csv.gz"
	};

	private Set<String> skuListSet = null;
	private Set<String> catLocalSet = null;
	private Set<String> catSpecificSet = null;
	private Set<String> catFilterPrice3000Set = null;
	private Set<String> catFilterPrice5000Set = null;
	private Set<String> nonCategoryMappingSet = null;
	private Set<String> nonCrawlCategorySet = null;
	private Set<String> blockSku = null;
	private Set<String> blockSeller = null;
	private Set<String> detailFileList = null;
	private Map<String, Boolean> blockCat = null;
	private Map<String, Object[]> catCountMap = null;
	private Map<String, String[]> categoryMap = null;
	
	private String[] HEADER;
	private String HEADER_LINE;
	private int HEADER_SIZE;

	private long CRAWL_COUNT;
	private long CRAWL_LIMIT;
	private long CRAWL_SKU, CRAWL_LOCAL, CRAWL_LAZMALL, CRAWL_SPECIFIC, CRAWL_FILTER_PRICE_3000, CRAWL_FILTER_PRICE_5000;
	private long countLocal, countCrossBorder;
	private long countRecordContentError=0;
	private long countRecordNotCrawl=0;
	
	private DataSource configDS;
	private BotMerchantConfigDB merchantConfigDB;
	private BotUrlPatternDB urlPatternDB;
	
	private final String skuFile = FEED_STORE_PATH + "crawl_skuFile.csv";
	private final String lazMallFile = FEED_STORE_PATH + "crawl_lazMallFile.csv";
	private final String catLocalFile = FEED_STORE_PATH + "crawl_catLocalFile.csv";
	private final String catSpecificFile = FEED_STORE_PATH + "crawl_catSpecificFile.csv";
	private final String catFilterPrice3000File = FEED_STORE_PATH + "crawl_catFilterPrice3000File.csv";
	private final String catFilterPrice5000File = FEED_STORE_PATH + "crawl_catFilterPrice5000File.csv";
	private final String nonCategoryFile = FEED_STORE_PATH + "list_nonCategoryFile.csv";
	private final String nonCrawlCategoryFile = FEED_STORE_PATH + "list_nonCrawlCategoryFile.csv";
	private final String mapCategoryDetailFile = FEED_STORE_PATH + "list_mapCategoryDetailFile.csv";
	
	private boolean processSuccess = false;
	
	public static void main(String[] args) {
		System.out.println(new Date() + " : Start tool_filter_lazada");
		try {
			tool_filter_lazada_th runner = new tool_filter_lazada_th();
			runner.setInitVariable();
			runner.checkFile();
			runner.clearFile();
			runner.setDatabase();
			runner.setCategory();
			runner.setConfig();
			runner.setConfigByFile();
			runner.setBlockCategory();
			runner.setBlockSeller();
			runner.process();
			runner.deleteSource();
		} catch(RuntimeException e) {
			System.out.println(new Date() + " : " + e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " : Finish tool_filter_lazada");
    }
	
	private void setInitVariable() {
		categoryMap = new HashMap<>();
		catCountMap = new HashMap<>();

		nonCategoryMappingSet = new HashSet<>();
		nonCrawlCategorySet = new HashSet<>();
		
		blockSeller = new HashSet<>();
		blockCat = new HashMap<String, Boolean>();
		
		detailFileList = new LinkedHashSet<>();
		detailFileList.addAll(Arrays.asList(new String[] {skuFile ,lazMallFile ,catLocalFile ,catSpecificFile ,catFilterPrice3000File ,catFilterPrice5000File ,nonCategoryFile ,nonCrawlCategoryFile ,mapCategoryDetailFile}));
	}
	
	private void checkFile() throws RuntimeException {
		for (String fileName : fileList) {
			String fileSource = FEED_SOURCE_PATH + fileName;
			File file = new File(fileSource);
			if(!file.exists()) {
				throw new RuntimeException(fileName + " not exist, process require both file.");
			}
		}
	}
	
	private void clearFile() {
		if(detailFileList != null && detailFileList.size() > 0) {
			for (String detailFile : detailFileList) {
				File file = new File(detailFile);
				if(file.exists()) {
					file.delete();
					System.out.println(new Date() + " : Delete detail file " + detailFile);
				}
			}
		}
	}
	
	private void setDatabase() {
		String webDBDriverName 	= "com.mysql.jdbc.Driver";
		String webDBURL 		= "jdbc:mysql://27.254.86.48:3306/pzbotbackend_th";
		String webDBUserName 	= "botupdate";
		String webDBPassword 	= "Process88";		
	    configDS = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
	    
	    merchantConfigDB = new BotMerchantConfigDB(configDS);
	    urlPatternDB = new BotUrlPatternDB(configDS);
	}
	
	private void setCategory() throws SQLException {
    	List<BotUrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(314, "FEED");
    	if(categoryMapping != null && categoryMapping.size() > 0) {
    		for(BotUrlPatternBean cat : categoryMapping){
    			String merchantCat = cat.getValue();
    			if(isBlank(merchantCat))
    				continue;
    			
    			String pzCatId = Integer.toString(cat.getCategoryId());
    			String pzKeyword = cat.getKeyword();
    			String[] mapValue = new String[]{ pzCatId, pzKeyword};
    			categoryMap.put(merchantCat, mapValue);
    		}
    	}
	}
	
	private void setConfig() throws SQLException {
		CRAWL_LIMIT = 30000000;
		String feedCrawlerParam = merchantConfigDB.findMerchantConfigValue(314, "findMerchantConfigValue");
		if(StringUtils.isNotBlank(feedCrawlerParam) && NumberUtils.isCreatable(feedCrawlerParam))
			CRAWL_LIMIT = NumberUtils.toLong(feedCrawlerParam);
	}
	
	private void setConfigByFile() {
		String FEED_URL = "";
		String FILE_NAME = "";
		String FEED_USER = "bot";
		String FEED_PASS = "pzad4r7u";
		
		//sku file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_sku.csv";
		FILE_NAME = FEED_STORE_PATH + "config_skuFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		skuListSet = getDataFromOfflineFile(FILE_NAME, null, null, true);
		
		//cat local file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_nocb.csv";
		FILE_NAME = FEED_STORE_PATH + "config_catLocalFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catLocalSet = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2}, false);
		
		//cat specific file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_specific_category.csv";
		FILE_NAME = FEED_STORE_PATH + "config_catSpecificFile.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catSpecificSet = getDataFromOfflineFile(FILE_NAME, "\\|", new Integer[] {0,1,2,3,4}, false);

		//cat filter price 3000 file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_filter_price_3000.csv";
		FILE_NAME = FEED_STORE_PATH + "config_catFilterPrice3000.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catFilterPrice3000Set = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2}, false);

		//cat filter price 5000 file
		FEED_URL = "http://27.254.82.243:8081/314_crawl_filter_price_5000.csv";
		FILE_NAME = FEED_STORE_PATH + "config_catFilterPrice5000.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		catFilterPrice5000Set = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0,1,2}, false);
		
		//block sku
		FEED_URL = "http://27.254.82.243:8081/314_block_sku.csv";
		FILE_NAME = FEED_STORE_PATH + "config_block_sku.csv";
		FTPUtil.downloadFileWithLogin(FEED_URL, FILE_NAME, FEED_USER, FEED_PASS);
		blockSku = getDataFromOfflineFile(FILE_NAME, ",", new Integer[] {0}, false);
		
	}
	
	private void setBlockCategory() {
		blockCat.put("Health & Beauty|Sexual Wellness|Sensual Toys", false);
		blockCat.put("Health & Beauty|Sexual Wellness|Condoms", false);
		blockCat.put("Health & Beauty|Sexual Wellness|Lubricants", false);
		blockCat.put("Mobiles & Tablets|Mobile Accessories|Phone Cases", true);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test normal Product", false);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test sample", false);
		blockCat.put("Test Product （wont be seen in seller pls dont inactive）|Test service", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test normal Product", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test sample", false);
		blockCat.put("Test Product (wont be seen in seller pls dont inactive)|Test service", false);
	}
	
	private void setBlockSeller() {
		blockSeller.add("MusicMove");
	}
	
	private void process() {
		for (String fileName : fileList) {
			try {
				String fileSource = FEED_SOURCE_PATH + fileName;
				File file = new File(fileSource);
				long fileSize = file.length();
				
				if(fileSource.contains("priceza_main_th_marketing_feed") && fileSize < (14 * 1004857600)) { //1004857600 bytes = 1 GB.
					System.out.println(new Date() + " : File smaller than expect. Only " + fileSize + " byte, stop program.");
					throw new RuntimeException("File smaller than expect. Only " + fileSize + " byte, stop program.");
				}
				
				if(!checkHeader(file)) {
					System.out.println(new Date() + " : Header Error, stop program.");
					throw new RuntimeException("Header Error, stop program.");
				}
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		try (
				PrintWriter pwr_sku = new PrintWriter(new FileOutputStream(new File(skuFile),  true));
				PrintWriter pwr_local = new PrintWriter(new FileOutputStream(new File(catLocalFile),  true));
				PrintWriter pwr_lazMall = new PrintWriter(new FileOutputStream(new File(lazMallFile),  true));
				PrintWriter pwr_specific = new PrintWriter(new FileOutputStream(new File(catSpecificFile),  true));
				PrintWriter pwr_filterPrice3000 = new PrintWriter(new FileOutputStream(new File(catFilterPrice3000File),  true));
				PrintWriter pwr_filterPrice5000 = new PrintWriter(new FileOutputStream(new File(catFilterPrice5000File),  true));
				PrintWriter pwr_noncat = new PrintWriter(new FileOutputStream(new File(nonCategoryFile),  true));
				PrintWriter pwr_catNotCrawl = new PrintWriter(new FileOutputStream(new File(nonCrawlCategoryFile),  true));
				PrintWriter pwr_catCount = new PrintWriter(new FileOutputStream(new File(mapCategoryDetailFile),  true));
			){
			
			pwr_sku.println(HEADER_LINE);
			pwr_local.println(HEADER_LINE);
			pwr_lazMall.println(HEADER_LINE);
			pwr_specific.println(HEADER_LINE);
			pwr_filterPrice3000.println(HEADER_LINE);
			pwr_filterPrice5000.println(HEADER_LINE);
			pwr_catCount.println("LazadaCatLv1|LazadaCatLv2|LazadaCatLv3|LazadaCatLv4|LazadaCatLv5^countProduct^countPzCrawl^countLocal^countLocalCrawl^countLazMall^countLazMallCrawl^catPzMapping^isSpecificMap^isLocalMap^isFilterPrice3000Map^isFilterPrice5000Map");
			
			Map<String, PrintWriter> writerMap = new HashMap<>();
			writerMap.put("sku", pwr_sku);
			writerMap.put("local", pwr_local);
			writerMap.put("lazMall", pwr_lazMall);
			writerMap.put("specific", pwr_specific);
			writerMap.put("filterPrice3000", pwr_filterPrice3000);
			writerMap.put("filterPrice5000", pwr_filterPrice5000);
			
			CRAWL_COUNT = 0;
			CRAWL_SKU = 0; 
			CRAWL_LOCAL = 0; 
			CRAWL_LAZMALL = 0; 
			CRAWL_SPECIFIC = 0; 
			CRAWL_FILTER_PRICE_3000 = 0; 
			CRAWL_FILTER_PRICE_5000 = 0;
			
			long lineCount = 0;
			boolean firstLine = true;
			for (String fileName : fileList) {
				String fileSource = FEED_SOURCE_PATH + fileName;
				try(BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(fileSource)), "UTF-8"))){
					String line = null;
					while ((line = br.readLine()) != null) { 
						lineCount++;
						if(lineCount % 10000000 == 0)
							System.out.println(new Date() + " : verifyByFilter " + lineCount + ", error " + countRecordContentError + ", skip " + countRecordNotCrawl);
						if(firstLine) {
							firstLine = false;
							continue;
						}
						verifyByFilter(line, writerMap, fileSource);
						
						if(Thread.currentThread().isInterrupted())
							throw new InterruptedException();
					}
				}catch(InterruptedException e) {
					throw e;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			System.out.println(new Date() + " : ---------------------------------------------------------");
			System.out.println(new Date() + " : all lineCount           = " + lineCount);
			System.out.println(new Date() + " : all crawlCount          = " + CRAWL_COUNT);
			System.out.println(new Date() + " : crawlSKU                = " + CRAWL_SKU);
			System.out.println(new Date() + " : crawlLocal              = " + CRAWL_LOCAL);
			System.out.println(new Date() + " : crawlLazMall            = " + CRAWL_LAZMALL);
			System.out.println(new Date() + " : crawlSpecific           = " + CRAWL_SPECIFIC);
			System.out.println(new Date() + " : crawlFilterPrice3000    = " + CRAWL_FILTER_PRICE_3000);
			System.out.println(new Date() + " : crawlFilterPrice5000    = " + CRAWL_FILTER_PRICE_5000);
			System.out.println(new Date() + " : -----------------------");
			System.out.println(new Date() + " : countLocal              = " + countLocal);
			System.out.println(new Date() + " : countCrossBorder        = " + countCrossBorder);
			System.out.println(new Date() + " : -----------------------");
			System.out.println(new Date() + " : countRecordContentError = " + countRecordContentError);
			System.out.println(new Date() + " : countRecordNotCrawl     = " + countRecordNotCrawl);
			System.out.println(new Date() + " : ---------------------------------------------------------");
			
			if(CRAWL_COUNT < (CRAWL_LIMIT*0.5)) {
				throw new RuntimeException("CRAWL_COUNT less than half of CRAWL_LIMIT (" + CRAWL_COUNT + " < " + (CRAWL_LIMIT*0.5) + ")");
			}
			
			if(nonCategoryMappingSet != null && nonCategoryMappingSet.size() > 0)
				for (String cat : nonCategoryMappingSet) 
					pwr_noncat.println(cat);
			
			if(nonCrawlCategorySet != null && nonCrawlCategorySet.size() > 0)
				for (String cat : nonCrawlCategorySet) 
					pwr_catNotCrawl.println(cat);
			
			if(catCountMap != null && catCountMap.size() > 0) {
				for(Entry<String, Object[]> catMapDetail : catCountMap.entrySet()) {
					pwr_catCount.println(catMapDetail.getKey() 
						+ "^" + catMapDetail.getValue()[0] 
						+ "^" + catMapDetail.getValue()[1] 
						+ "^" + catMapDetail.getValue()[2] 
						+ "^" + catMapDetail.getValue()[3] 
						+ "^" + catMapDetail.getValue()[4] 
						+ "^" + catMapDetail.getValue()[5] 
						+ "^" + catMapDetail.getValue()[6] 
						+ "^" + catMapDetail.getValue()[7] 
						+ "^" + catMapDetail.getValue()[8] 
						+ "^" + catMapDetail.getValue()[9] 
						+ "^" + catMapDetail.getValue()[10]);
				}
			}
			
			if(skuListSet != null && skuListSet.size() > 0)
				try(PrintWriter pwr_remain_sku = new PrintWriter(new FileOutputStream(new File(FEED_STORE_PATH + "lazada-remain-sku.csv"),  true));){
					for (String cat : skuListSet) 
						pwr_remain_sku.println(cat);
				}
		
			List<String> finalCrawlList = new ArrayList<>();
			if(CRAWL_SKU > 0) finalCrawlList.add(skuFile);
			if(CRAWL_LOCAL > 0) finalCrawlList.add(catLocalFile);
			if(CRAWL_LAZMALL > 0) finalCrawlList.add(lazMallFile);
			if(CRAWL_SPECIFIC > 0) finalCrawlList.add(catSpecificFile);
			if(CRAWL_FILTER_PRICE_3000 > 0) finalCrawlList.add(catFilterPrice3000File);
			if(CRAWL_FILTER_PRICE_5000 > 0) finalCrawlList.add(catFilterPrice5000File);
			String[] genFile = (String[]) finalCrawlList.toArray(new String[finalCrawlList.size()]);
			Arrays.asList(genFile).stream().forEach(s -> System.out.println(new Date() + " : Summary File to crawl : " + s));
			processSuccess = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void deleteSource() {
		if(processSuccess) {
			for (String fileName : fileList) {
				String fileSource = FEED_SOURCE_PATH + fileName;
				File file = new File(fileSource);
				file.delete();
				System.out.println(new Date() + " : Delete source file " + fileSource);
			}
		}
	}
	
	//#############################################################################
	//####################### UTILITY METHOD ######################################
	//#############################################################################
	
	private void verifyByFilter(String line, Map<String, PrintWriter> writerMap, String feedName) {
		try {
			StringReader lineReader = new StringReader(line);
			
			@SuppressWarnings("resource")
			CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL.withHeader(HEADER));
			
			for (CSVRecord csvRecord : record) {
				
				if (csvRecord == null || csvRecord.size() != HEADER_SIZE) {
					countRecordContentError++;
					continue;
				}
					
				String id = csvRecord.get("simple_sku");
				String cat1 = csvRecord.get("level_1");
				String cat2 = csvRecord.get("level_2");
				String cat3 = csvRecord.get("level_3");
				String cat4 = csvRecord.get("level_4");
				String cat5 = csvRecord.get("level_5");
				String oldSku = StringUtils.defaultIfBlank(csvRecord.get("old_sku_id"), "");
				String shippingType = StringUtils.defaultIfBlank(csvRecord.get("cross_border"), "");
				String discountPrice = StringUtils.defaultIfBlank(csvRecord.get("discounted_price"), "0");
				double price = (NumberUtils.isCreatable(discountPrice))? Double.parseDouble(discountPrice) : 0;
				boolean isCrawl = false;
				boolean isLocal = false;
				boolean isLazMall = false;
				
				String allCat = cat1;
				if(StringUtils.isNotBlank(cat2)) allCat += "|" + cat2;
				if(StringUtils.isNotBlank(cat3)) allCat += "|" + cat3;
				if(StringUtils.isNotBlank(cat4)) allCat += "|" + cat4;
				if(StringUtils.isNotBlank(cat5)) allCat += "|" + cat5;
				allCat = allCat.replace("\"", "");
				
				String catLevel3 = cat1;
				if(StringUtils.isNotBlank(cat2)) catLevel3 += "|" + cat2;
				if(StringUtils.isNotBlank(cat3)) catLevel3 += "|" + cat3;
				
				if(shippingType.equals("0")) {
					isLocal = true;
					countLocal++;
				} else {
					countCrossBorder++;
				}
				
				if(getCategory(allCat) == null)
					if(!nonCategoryMappingSet.contains(allCat))
						nonCategoryMappingSet.add(allCat);
				
				Boolean typeBlock = blockCat.get(allCat);
				if (allCat.contains("SellerCenter") || (typeBlock != null && (!typeBlock || (typeBlock && shippingType.equals("1"))))) {
					countRecordNotCrawl++;
					return;
				}
				
				if(blockSku.contains(oldSku)) {
					countRecordNotCrawl++;
					return;
				}
				
				String crawlType = "";
				if(skuListSet.contains(id)) {
					skuListSet.remove(id);
					crawlType = "sku";
					CRAWL_SKU++;
				} else if(feedName.contains("priceza_lazmall_th")) {
					isLazMall = true;
					crawlType = "lazMall";
					CRAWL_LAZMALL++;
				} else if((catFilterPrice5000Set.contains(allCat) || catFilterPrice5000Set.contains(catLevel3)) && price >= 5000) {
					crawlType = "filterPrice5000";
					CRAWL_FILTER_PRICE_5000++;
				} else if((catFilterPrice3000Set.contains(allCat) || catFilterPrice3000Set.contains(catLevel3)) && price >= 3000) {
					crawlType = "filterPrice3000";
					CRAWL_FILTER_PRICE_3000++;
				} else if((catLocalSet.contains(allCat) || catLocalSet.contains(catLevel3)) && shippingType.equals("0")) {
					crawlType = "local";
					CRAWL_LOCAL++;
				} else if(catSpecificSet.contains(allCat) && (CRAWL_COUNT < CRAWL_LIMIT)) {
					crawlType = "specific";
					CRAWL_SPECIFIC++;
				} else {
					if(!nonCrawlCategorySet.contains(allCat))
						nonCrawlCategorySet.add(allCat);
				}
				
				if(StringUtils.isNotBlank(crawlType)) {
					writeLineToFile(line, crawlType, writerMap);
					CRAWL_COUNT++;
					isCrawl = true;
				} else {
					countRecordNotCrawl++;
				}
				
				verifyCat(allCat, catLevel3, isCrawl, isLocal, isLazMall);
			}
		}catch(IllegalStateException | IllegalArgumentException e) {
			countRecordContentError++;
		}catch(Exception e) {
			System.out.println(new Date() + " : " + e + "\n" + line);
		}
	}
	
	private void verifyCat(String allCat, String catLevel3 ,boolean isCrawl, boolean isLocal, boolean isLazMall) {
		//countProduct^countPzCrawl^countLocal^countLocalCrawl^countLazMall^countLazMallCrawl^catPzMapping^isSpecificMap^isLocalMap^isFilterPrice3000Map^isFilterPrice5000Map
		Object[] catMapDetail = catCountMap.getOrDefault(allCat, new Object[] {(long) 0, (long) 0,(long) 0, (long) 0, (long) 0, (long) 0, (long) 0, false, false, false, false});
		long countProduct = (long) catMapDetail[0];
		long countCrawl = (long) catMapDetail[1];
		long countLocal = (long) catMapDetail[2];
		long countLocalCrawl = (long) catMapDetail[3];
		long countLazMall = (long) catMapDetail[4];
		long countLazMallCrawl = (long) catMapDetail[5];
		long catPatternMap = (long) catMapDetail[6];
		boolean isSpecificMap = (boolean) catMapDetail[7];
		boolean isLocalMap = (boolean) catMapDetail[8];
		boolean isFilterPrice3000Map = (boolean) catMapDetail[9];
		boolean isFilterPrice5000Map = (boolean) catMapDetail[10];
		
		//count all product and count crawl product
		countProduct++;
		if(isCrawl)
			countCrawl++;
		
		//check local
		if(isLocal) {
			countLocal++;
			if(isCrawl)
				countLocalCrawl++;
		}
		
		//check lazMall
		if(isLazMall) {
			countLazMall++;
			if(isCrawl)
				countLazMallCrawl++;
		}
		
		//check isPatternMap all level
		String[] catMap = getCategory(allCat);
		if (catMap != null) 
			catPatternMap = Long.parseLong(catMap[0]);
		
		//check isSpecificMap all level
		if (catSpecificSet.contains(allCat)) 
			isSpecificMap = true;
		
		//check isLocalMap only level 3
		if (catLocalSet.contains(catLevel3)) 
			isLocalMap = true;
		
		//check isFilterPrice3000Map only level 3
		if (catFilterPrice3000Set.contains(catLevel3)) 
			isFilterPrice3000Map = true;
		
		//check isFilterPrice5000Map only level 3
		if (catFilterPrice5000Set.contains(catLevel3)) 
			isFilterPrice5000Map = true;
		
		catMapDetail = new Object[] {countProduct, countCrawl, countLocal, countLocalCrawl, countLazMall, countLazMallCrawl, catPatternMap, isSpecificMap, isLocalMap, isFilterPrice3000Map, isFilterPrice5000Map};
		catCountMap.put(allCat, catMapDetail);
		
	}
	
	private void writeLineToFile(String line, String crawlType, Map<String, PrintWriter> writerMap) {
		PrintWriter pw = writerMap.get(crawlType);
		if(pw != null) {
			line = editDataLine(crawlType, line);
			pw.println(line);
		}
	}
	
	private String editDataLine(String crawlType, String line) {
		if(crawlType.equals("lazMall")) {
			line = line.replaceFirst("\"\"\",", " LazMall\"\"\",");
			if(!line.contains("LazMall")) line = line.replaceFirst("\"\",", " LazMall\"\",");
			if(!line.contains("LazMall")) line = line.replaceFirst("\",", " LazMall\",");
			if(!line.contains("LazMall")) line = line.replaceFirst(",", " LazMall,");
		}
		return line;
	}
	
	private boolean checkHeader(File file) throws Exception {
		try(FileInputStream fis = new FileInputStream(file);
				GZIPInputStream gzis = new GZIPInputStream(fis);
				InputStreamReader isr = new InputStreamReader(gzis, "UTF-8");
				BufferedReader br = new BufferedReader(isr)){
			
			String firstLine = br.readLine();
			if(firstLine != null) {
				HEADER = firstLine.split(",");
				HEADER_LINE = firstLine;
				HEADER_SIZE = HEADER.length;
				return true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private Set<String> getDataFromOfflineFile(String file, String delimiter, Integer[] index, boolean enableLog){
		if(enableLog) System.out.println(new Date() + " : Start read " + file);
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))){
			HashSet<String> resultSet = new HashSet<>();
			String line = null;
			int lineCount = 0;
			int duplicateCount = 0;
			while ((line = br.readLine()) != null) {
				if(isBlank(line)) {
					continue;
				} else if(isBlank(delimiter)) {
					resultSet.add(line);
				} else {
					String[] data = line.split(delimiter);
					String result = "";
					for (Integer i : index) {
						if(i >= data.length) 
							continue;
						
						result += "|";
						result += data[i];
					}
					if(StringUtils.isNotBlank(result)) {
						result = result.substring(1);
						if(resultSet.contains(result)) {
							duplicateCount++;
						}else {
							resultSet.add(result);
						}
					}
				}
				if(Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				
				lineCount++;
				if(enableLog) {
					if(lineCount % 1000000 == 0) {
						System.out.println(new Date() + " : " + file + " check read " + lineCount + " line.");
					}
				}
			}
			System.out.println(new Date() + " : " + file + " done, " + resultSet.size() + " records, " + duplicateCount + " duplicated.");
			return resultSet;
		}catch(Exception e) {
			System.out.println(new Date() + " : Error from getDataFromOfflineFile : " + file + " -> " + e);
		}
		return new HashSet<>();
	}

	private String[] getCategory(String merchantCat) {
		if(merchantCat == null)
			return null;
		return categoryMap.get(merchantCat);
	}
	
}
