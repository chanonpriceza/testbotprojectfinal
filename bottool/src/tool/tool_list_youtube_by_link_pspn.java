package tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import Util.Util;

public class tool_list_youtube_by_link_pspn {
	
//	private static final String outputFile = "C:\\Users\\Nuthe\\Desktop\\result_youtube_productGroup_20101.csv";
//	private static final String inputFile = "C:\\Users\\Nuthe\\Desktop\\crawl_youtube_productGroup_20101.csv";
	
	private static final String inputFile = "C:\\Users\\Nuthe\\Desktop\\target_youtube_10000-20000.csv";
	private static final String outputFileAll = "C:\\Users\\Nuthe\\Desktop\\result_all_youtube_10000.csv";
	private static final String outputFileTh = "C:\\Users\\Nuthe\\Desktop\\result_th_youtube_10000.csv";
	
	private static int start = 0;
	private static int lineCount = -1;
	private static Map<String, String> resultMap;
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		resultMap = new HashMap<>();
		
		try(PrintWriter pwr_all = new PrintWriter(new FileOutputStream(new File(outputFileAll),  start!=0));
			PrintWriter pwr_th  = new PrintWriter(new FileOutputStream(new File(outputFileTh),  start!=0));){
			
			if(start == 0) {
				pwr_all.println("groupId|productId|productName|countProduct|yt_link|yt_email|yt_tel|yt_fb|yt_channel_name|yt_channel_link|yt_channel_sub|yt_view_count|yt_title|yt_description");
				pwr_th.println("groupId|productId|productName|countProduct|yt_link|yt_email|yt_tel|yt_fb|yt_channel_name|yt_channel_link|yt_channel_sub|yt_view_count|yt_title|yt_description");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		lineCount = lineCount + start;
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputFile)), "UTF-8"));
			String line = "";
			
			while((line = br.readLine()) != null) {
				try{
					
					lineCount++;
					if(lineCount==0) continue;
					
					CSVParser record = new CSVParser(new StringReader(line), CSVFormat.EXCEL);
					for (CSVRecord csvRecord : record) {
						try{
							parseCSV(csvRecord);
							if(lineCount > 0 && lineCount % 100 == 0) {
								System.out.println("Done : " + lineCount);
							}
						}catch(Exception e){
							continue;
						}
					}
				}catch(Exception e){
					System.err.println("Can't parse line data : " + line);
					e.printStackTrace();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void parseCSV(CSVRecord data) {
		// 0 "group_id"
		// 1 "product_id"
		// 2 "title"
		// 3 "count_product"
		// 4 "youtubeLink"
		
		String groupId = (String) data.get(0); 
		String productId = (String) data.get(1);
		String productName = (String) data.get(2);
		String countProduct = (String) data.get(3);
		String youtubeLink = (String) data.get(4);
		
		String youtubeLink1 = youtubeLink;
		String youtubeLink2 = "";
		
		if(youtubeLink.contains("|")) {
			String[] youtubeLinks = youtubeLink.split("\\|");
			youtubeLink1 = youtubeLinks[0];
			youtubeLink2 = youtubeLinks[1];
		}
		youtubeLink1 = Util.getStringBefore(youtubeLink1, ",", youtubeLink1);
		youtubeLink2 = Util.getStringBefore(youtubeLink2, ",", youtubeLink2);
		
		try {
			if(StringUtils.isNotBlank(youtubeLink1)) {
				parseYoutube(youtubeLink1, groupId, productId, productName, countProduct);
			}
			
			if(StringUtils.isNotBlank(youtubeLink2)) {
				parseYoutube(youtubeLink2, groupId, productId, productName, countProduct);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void parseYoutube(String videoName, String groupId, String productId, String productName, String countProduct) throws InterruptedException, UnsupportedEncodingException {

		boolean sleep = true;
		
		String link = "https://www.youtube.com/watch?v=" + videoName ;
		String email = "";
		String tel = "";
		String fb = "";
		String channelName = "";
		String channelLink = "";
		String channelSub = "";
		String videoTitle = "";
		String videoDesc = "";
		String viewCount = "";
		
		String finalDetail = resultMap.getOrDefault(videoName, "");
		if(StringUtils.isNotBlank(finalDetail)){
			sleep = false;
		} else {
			String[] response = Util.httpRequestWithStatus(link, "UTF-8", false);
			if(response != null && response.length == 2) {
				
				Document doc = Jsoup.parse(response[0]);
				Elements subscriber = doc.select("span.yt-subscriber-count");
				Elements channel = doc.select("span[itemprop=author]");
				
				String playerScript = Util.getStringBetween(response[0], "<script >var ytplayer = ytplayer || {};ytplayer.config = ", ";ytplayer.load");
				try {
					
					JSONObject scriptObj = (JSONObject) new JSONParser().parse(playerScript);
					JSONObject argsObj = (JSONObject) scriptObj.get("args");
					String playerResponse = (String) argsObj.get("player_response");
					JSONObject playerResponseObj = (JSONObject) new JSONParser().parse(playerResponse);
					JSONObject videoDetailsObj = (JSONObject) playerResponseObj.get("videoDetails");
					
					String description = (String) videoDetailsObj.get("shortDescription");
					description = description.replaceAll("\r\n", " ");
					description = description.replaceAll("\\s+", " ");
					
					email = find_email(description);          
					tel = find_tel(description);     
					fb = find_facebook(description);         
					channelName = (String) videoDetailsObj.get("author");
					channelLink = channel.select("link[itemprop=url]").attr("href");    
					channelSub = subscriber.text();
					viewCount = (String) videoDetailsObj.get("viewCount");
					videoTitle = (String) videoDetailsObj.get("title");
					videoDesc = description;      
							
					tel = check_tel(tel);

					finalDetail = ""
						+ ""  + StringUtils.defaultIfBlank(displayPrintText(groupId), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(productId), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(productName), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(countProduct), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(link), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(email), " ")    
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(tel), " ")   
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(fb), " ")   
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(channelName), " ")   
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(channelLink), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(channelSub), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(viewCount), " ")
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(videoTitle), " ")   
						+ "|" + StringUtils.defaultIfBlank(displayPrintText(videoDesc), " ")   
						;
					
					resultMap.put(videoName, finalDetail);
					
				} catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
		try(PrintWriter pwr_all = new PrintWriter(new FileOutputStream(new File(outputFileAll),  true));
			PrintWriter pwr_th  = new PrintWriter(new FileOutputStream(new File(outputFileTh),  true));){
			
			if(start == 0) {
				pwr_all.println(finalDetail);
				if(Util.checkContainsCharacter(videoDesc,"th")) {
					pwr_th.println(finalDetail);
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if(sleep) {
			int sleepTime = RandomUtils.nextInt(15000,60000);
			System.out.println("Line : " + lineCount + " → SLEEP : " + (double) sleepTime/1000 + "sec after parse " + link);
			Thread.sleep(sleepTime);
		} else {
			System.out.println("Line : " + lineCount + " → no sleep, found duplicate link : " + link);
		}

		return;
	}
	
	private static String find_email(String text) {
		Matcher m = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])").matcher(text);
		List<String> resultList = new ArrayList<>();
	    while (m.find()) {
	    	if(!resultList.contains(m.group())) {
				resultList.add(m.group());
			}
	    }
	    if(resultList!=null && resultList.size()>0) {
	    	return resultList.toString().substring(1, resultList.toString().length()-1);
	    }
	    return "";
	}
	
	private static String find_tel(String text) {
		Matcher m = Pattern.compile("(?:(?:\\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?").matcher(text);
		List<String> resultList = new ArrayList<>();
	    while (m.find()) {
	    	if(!resultList.contains(m.group())) {
				resultList.add(m.group());
			}
	    }
	    if(resultList!=null && resultList.size()>0) {
	    	return resultList.toString().substring(1, resultList.toString().length()-1);
	    }
		return "";
	}
	
	private static String find_facebook(String text) {
		Matcher m = Pattern.compile("(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))").matcher(text);
		List<String> resultList = new ArrayList<>();
		while (m.find()) {
			if(m.group().contains("fb.com") || m.group().contains("facebook.com")) {
				String link = m.group();
				link = Util.getStringBefore(link, "&", link);
				link = Util.getStringBefore(link, "?", link);
				if(!resultList.contains(link)) {
					resultList.add(link);
				}
			}
		}
		if(resultList!=null && resultList.size()>0) {
			return resultList.toString().substring(1, resultList.toString().length()-1);
		}
		return "";
	}
	
	private static String check_tel(String tel) {
		if(StringUtils.isBlank(tel)) {
			return "";
		}
		List<String> result = new ArrayList<>();;
		String[] results = null;
		if(tel.contains(",")) {
			results = tel.split(",");
		}else {
			results = new String[] {tel};
		}
		
		if(results != null && results.length > 0) {
			for (String res : results) {
				res = Util.removeCharNotPrice(res).trim();
				if(res.length() == 9) {
					if(res.startsWith("02")){
						result.add(res);
					} else {
						result.add("0" + res);
					}
				}
				if(res.length() == 10 && res.charAt(0) == '0') {
					result.add(res);
				}
			}
			return result.toString().substring(1, result.toString().length()-1);
		}
		
		return "";
	}
	
	private static String displayPrintText(String text) {
		if(StringUtils.isBlank(text)) {
			return "";
		}
		String result = text;
		result = result.replaceAll("\\s+", " ");
		result = result.replaceAll("\\|", " ");
		result = result.trim();
		result = "\"" + result + "\"";
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
