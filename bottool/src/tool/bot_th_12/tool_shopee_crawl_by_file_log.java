package tool.bot_th_12;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.sql.DataSource;
import javax.swing.ImageIcon;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Util.DatabaseUtil;
import Util.Util;
import bean.BotProductDataBean;
import bean.BotSendDataBean;
import bean.BotUrlPatternBean;
import db.BotProductDataDB;
import db.BotSendDataDB;
import db.BotUrlPatternDB;

public class tool_shopee_crawl_by_file_log {
	
	// bot12
	private static final String INPUT_PATH = "/home/sale/300642-shopee/";			 
	
	// local
//	private static final String INPUT_PATH = "D:\\test_file\\";
	
	private static final String GET_PRODUCT_LINK = "https://shopee.co.th/api/v2/item/get?itemid={itemid}&shopid={shopId}";
	private static final String FILENAME_KEY = "shopee-purchase-";
	
	private static JSONParser parser = new JSONParser();

	private static final int MERCHANT_ID = 300642;
	private static final int IMAGE_WIDTH = 200;
	private static final int IMAGE_HEIGHT = 200;
	
	private Map<String, String[]> categoryMap = null;
	
	private DataSource configDS, dataDS;
	private BotUrlPatternDB urlPatternDB;
	private BotProductDataDB productDataDB;
	private BotSendDataDB sendDataDB;
	
	private static int lineCount=0, addProductCount=0, dupProductCount=0, errorProductCount=0;

	// ------------------------------------------------------------------
	
	public static void main(String[] args) {

		System.out.println(new Date() + " : Start tool_shopee_list_cat_by_conversion");
		try {
			tool_shopee_crawl_by_file_log t = new tool_shopee_crawl_by_file_log();
			t.setInitVariable();
			t.setDatabase();
			t.setCategory();
			t.run();
		} catch(RuntimeException e) {
			System.out.println(new Date() + " : : " + e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " : Finish tool_shopee_list_cat_by_conversion");
		
	}
	
	private void run() {
		
		File folder = new File(INPUT_PATH);
		File[] listOfFiles = folder.listFiles();
		
		int fileCount = 0;
		if(listOfFiles != null && listOfFiles.length > 0) {
			for (File INPUT_FILE : listOfFiles) {
				
				if(INPUT_FILE.getName().toLowerCase().contains(FILENAME_KEY)) {
					
					fileCount++;
					System.out.println(new Date() + " : Start read file : " + INPUT_FILE.getName());
					
					try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(INPUT_FILE), "UTF-8"));){
						
						lineCount = 0;
						String line = null;
						while ((line = br.readLine()) != null) { 
							lineCount++;
							
							//real_product_id,shop_id
							
							String[] data = line.split(",");
							if(data != null && data.length==2) {
								String shopId = data[1];
								String itemId = data[0];
								
								if(NumberUtils.isCreatable(shopId) && NumberUtils.isCreatable(itemId)) {
									run(shopId, itemId);
								}
							}
							if(lineCount%1000 == 0) {
								System.out.println(new Date() + " : Done " + lineCount + " line.");
							}
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
					
					System.out.println(new Date() + " : Finish read file : " + INPUT_FILE.getName());
					replaceDoneFile(INPUT_FILE);
					
					String summaryText = ""
							+ "lineCount = " 		+ lineCount
							+ ", addProduct = " 	+ addProductCount
							+ ", dupProduct = " 	+ dupProductCount
							+ ", errorRecord = " 	+ errorProductCount;
					
					System.out.println(new Date() + " : Summary records : " + summaryText);
					
				}
				
			}
		}
		
		System.out.println(new Date() + " : Finish file count " + fileCount);
	}
	
	private void run(String shopId,String productId) {
		try {
			String req = GET_PRODUCT_LINK.replace("{itemid}", productId).replace("{shopId}", shopId);
			String[] response = Util.httpRequestWithStatus(req, "UTF-8", true);
			if(response==null||response.length==0) {
				errorProductCount++;
				return;
			}
			
			if(response[0]==null) {
				errorProductCount++;
				return;
			}
			
			JSONObject obj = (JSONObject) parser.parse(response[0]);
			obj = (JSONObject) obj.get("item");
			if(obj==null) {
				errorProductCount++;
				return;
			}

			String productName = String.valueOf(obj.get("name"));
			String productDesc = String.valueOf(obj.get("description"));
			String catId = String.valueOf(obj.get("catid"));
			String price = String.valueOf(obj.get("price"));
			String basePrice = StringUtils.defaultString(String.valueOf(obj.get("price_before_discount")), "0");
			String[] subCat = genSubcat(obj.get("categories"), catId);
			String subCatId = subCat[0];
			String image = null;
			
			JSONArray imageArr = (JSONArray) obj.get("images");
			if(imageArr != null && imageArr.size() > 0) {
				for (Object img : imageArr) {
					image = "https://cf.shopee.co.th/file/" + String.valueOf(img);
					break;
				}
			}
			
			productName = StringEscapeUtils.unescapeHtml4(productName).trim();
    		productName = Util.removeEmoji(productName);
    		productName = Util.removeNonUnicodeBMP(productName);
    		productName = productName.replaceAll("\\s+", " ");
			String productNameUrl = productName.replaceAll("%", "");
			String linkName = productNameUrl + "-i." + shopId + "." + productId;
			String productUrl = "https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed||https://shopee.co.th/universal-link/"+linkName+"?smtt=9&deep_and_deferred=1&pid=priceza_int&c=datafeed&af_click_lookback=7d&is_retargeting=true&af_reengagement_window=7d&af_installpostback=false&clickid=(xxxxx)&utm_source=priceza&utm_medium=affiliates&utm_campaign=datafeed";
			
			productDesc = StringUtils.defaultString(Util.removeEmoji(Util.toPlainTextString(productDesc)), "").replaceAll("/r/n", " ").replaceAll("//s+", " ");
			
			double priceDouble = Util.convertPriceStr(price) / 100000;
			double basePriceDouble = Util.convertPriceStr(basePrice) / 100000;
			
			BotProductDataBean pdb = new BotProductDataBean();
			pdb.setMerchantId(MERCHANT_ID);
			pdb.setName(productName.trim() + " (" + productId.trim() + ")");
			pdb.setDescription(productDesc);
			pdb.setRealProductId(productId.trim());
			pdb.setPrice(priceDouble);
			pdb.setBasePrice(basePriceDouble);
			pdb.setPictureUrl(image);
			pdb.setUrl(productUrl);
			pdb.setUrlForUpdate(productId.trim());
			pdb.setUpdateDate(new Timestamp(System.currentTimeMillis()));
			
			String[] pzCatMap = categoryMap.get(subCatId);
			if(pzCatMap != null && pzCatMap.length == 2) {
				pdb.setCategoryId(Util.stringToInt(pzCatMap[0], 0));	
				pdb.setKeyword(pzCatMap[1]);
			}
			
			int insertProductDataDBResult = productDataDB.insertProductData(pdb);
			if(insertProductDataDBResult > 0) {
				byte[] imageByte = parseImage(pdb.getPictureUrl());
				BotSendDataBean sendDataBean = BotSendDataBean.createSendDataBean(pdb, imageByte, BotSendDataBean.ACTION.ADD);
				sendDataDB.insertSendData(sendDataBean);
				
				try {
					if(imageByte != null && imageByte.length > 0) {
						String filePath = INPUT_PATH + "picture-" + addProductCount + ".jpg";
						BufferedImage imgBuffer = ImageIO.read(new ByteArrayInputStream(imageByte));
						ImageIO.write(imgBuffer, "jpg", new File(filePath));
					}
				} catch (IOException e) {
					System.err.println("Exception when uploadImage " + e);
				}
				
				addProductCount++;
			} else {
				dupProductCount++;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// ------------------------------------------------------------------

	private void setCategory() throws SQLException {
		List<BotUrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(7077, "FEED");
		if(categoryMapping != null && categoryMapping.size() > 0) {
			for(BotUrlPatternBean cat : categoryMapping){
				String merchantCat = cat.getValue();
				if(isBlank(merchantCat))
					continue;
				
				String pzCatId = Integer.toString(cat.getCategoryId());
				String pzKeyword = cat.getKeyword();
				String[] mapValue = new String[]{ pzCatId, pzKeyword};
				categoryMap.put(merchantCat, mapValue);
			}
		}
	}
	
	private void setInitVariable() {
		categoryMap = new HashMap<>();
	}
	
	private void setDatabase() {
		
		// bot14
		String webDBDriverName 	= "com.mysql.jdbc.Driver";
		String webDBURL 		= "jdbc:mysql://27.254.86.48:3306/pzbotbackend_th";
		String webDBUserName 	= "botupdate";
		String webDBPassword 	= "Process88";		
		configDS = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
		
//		// local
//		String dataDBDriverName = "com.mysql.jdbc.Driver";
//		String dataDBURL 		= "jdbc:mysql://localhost:3306/test";
//		String dataDBUserName 	= "root";
//		String dataDBPassword 	= "root";		
//		dataDS = DatabaseUtil.createDataSource(dataDBURL, dataDBDriverName, dataDBUserName, dataDBPassword, 3);
		
		// center2
		String dataDBDriverName = "com.mysql.jdbc.Driver";
		String dataDBURL 		= "jdbc:mysql://27.254.86.48:3308/bot";
		String dataDBUserName 	= "botupdate";
		String dataDBPassword 	= "Process88";		
		dataDS = DatabaseUtil.createDataSource(dataDBURL, dataDBDriverName, dataDBUserName, dataDBPassword, 3);
		
		urlPatternDB = new BotUrlPatternDB(configDS);
		productDataDB = new BotProductDataDB(dataDS);
		sendDataDB = new BotSendDataDB(dataDS);
	}

	private static byte[] parseImage(String image) {
		try {
			if(StringUtils.isBlank(image)) return null;
			
			URL url = new URL(image);			
			ImageIcon ii = new ImageIcon(url);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if(iWidth == -1 && iHeight == -1)
				return null;
			
			double xPos = (IMAGE_WIDTH - 1 * iWidth) / 2;
			double yPos = (IMAGE_HEIGHT - 1 * iHeight) / 2;
			Image temp = null;
			
			if (iHeight > IMAGE_HEIGHT || iWidth > IMAGE_WIDTH) {
				if (iWidth > iHeight)
					resizedImage = i.getScaledInstance(IMAGE_WIDTH, (IMAGE_HEIGHT * iHeight) / iWidth, Image.SCALE_SMOOTH);
				else
					resizedImage = i.getScaledInstance((IMAGE_WIDTH * iWidth) / iHeight, IMAGE_HEIGHT, Image.SCALE_SMOOTH);

				double xScale = (double) IMAGE_WIDTH / iWidth;
				double yScale = (double) IMAGE_HEIGHT / iHeight;
				double scale = Math.min(xScale, yScale);

				xPos = (IMAGE_WIDTH - scale * iWidth) / 2;
				yPos = (IMAGE_HEIGHT - scale * iHeight) / 2;
				temp = new ImageIcon(resizedImage).getImage();
				
			} else {
				temp = i;
			}
					
			BufferedImage bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
			
			Graphics g = bufferedImage.createGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
			g.drawImage(temp, (int)xPos, (int)yPos, null);
			g.dispose();

			try(ByteArrayOutputStream outStream = new ByteArrayOutputStream()){
				
//				ImageIO.write
//				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outStream);
//				JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bufferedImage);
//				param.setQuality(1f, true);
//				encoder.setJPEGEncodeParam(param);
//				encoder.encode(bufferedImage);
				outStream.flush();
				byte[] imageInByte = outStream.toByteArray();
				outStream.close();
				
				return imageInByte;
			}
		} catch (Exception e) {
			System.out.println(new Date() + " : Exception when parseImage " + e.toString());
		}
		return null;
	}
	
	private String[] genSubcat(Object bread,String cat) {
		String[] result = new String[2];
		
		if(bread instanceof JSONArray) {
			String subCatId = "";
			String subCatName = "";
			
			JSONArray array = (JSONArray) bread;
			for(Object o:array) {
				JSONObject obj = (JSONObject) o;
				String id = String.valueOf(obj.get("catid"));
				String name = String.valueOf(obj.get("display_name"));
				subCatId += id+"|";
				subCatName += name+"|";
			}
			
			if(StringUtils.isNotBlank(subCatId)) subCatId = subCatId.substring(0, subCatId.length()-1);
			if(StringUtils.isNotBlank(subCatName)) subCatName = subCatName.substring(0, subCatName.length()-1);
			
			result[0] = subCatId;
			result[1] = subCatName;
		}
		
		return result;
	}

	private void replaceDoneFile(File input) {
		String newName = input.getName().replace(FILENAME_KEY, "SHOPEE_DONE_");
		input.renameTo(new File(INPUT_PATH+newName));
		System.out.println(new Date() + " : Rename target to : " + newName);
	}
	
}
