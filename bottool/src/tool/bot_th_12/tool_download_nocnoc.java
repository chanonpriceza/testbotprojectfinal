package tool.bot_th_12;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class tool_download_nocnoc {
	
	private static int MERCHANT_ID = 300617;
	private static String FILE_PATH = "/home/sale/300617-nocnoc/";
	private static String FILE_BACKUP_PATH = "/home/sale/300617-nocnoc/backup/";
	private static JSONParser parser = new JSONParser();
	
	public static void main(String[] args) {
		System.out.println(new Date() + " Start running NocnocFileDownloader");
		try {
			
			moveFile(FILE_PATH,FILE_BACKUP_PATH);
			
			String[] fileNames = getFileNames();
			
			boolean check = downloadFile(fileNames);
			
			if(!check) {
				System.out.println(new Date() + " Download process is incomplete");
				deleteOldFile(FILE_PATH);
				moveFile(FILE_BACKUP_PATH,FILE_PATH);
				return;
			}
			
			check = checkFile();
			
			if(!check) {
				System.out.println(new Date() + " Some file are expected is incomplete File");
				deleteOldFile(FILE_PATH);
				moveFile(FILE_BACKUP_PATH,FILE_PATH);
				return;
			}
			
			deleteOldFile(FILE_BACKUP_PATH);
			
			for(int i =0;i<fileNames.length;i++) {
				String data = "{\"filename\":\"{fileName}\",\"action\":\"process\"}";
				String dataDummy = data.replace("{fileName}", fileNames[i]);
				sendPost(dataDummy);
				System.out.println(new Date() + " Achieve customer file " + fileNames[i]);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " Finish running NocnocFileDownloader");
	}
	
	private static String[] getFileNames() throws Exception {
		String response = sendPost("{\"action\":\"list\"}");
		JSONObject obj = (JSONObject)parser.parse(response);
		JSONArray json = (JSONArray)obj.get("output");
		List<String> jsonList = new ArrayList<String>();
		for(Object j:json) {
			JSONObject js = (JSONObject) j;
			jsonList.add(String.valueOf(js.get("Key")));
		}
		return jsonList.stream().toArray(String[]::new);
	}
	
	private  static boolean downloadFile(String[] fileNames)  {
		boolean result = true;
		try {
			for (int i = 0; i < fileNames.length; i++) {
				String url = "https://pzay6sw4e6.execute-api.ap-southeast-1.amazonaws.com/api/{fileName}";
				URL u = new URL(url.replace("{fileName}", fileNames[i]));
				HttpURLConnection conn = (HttpURLConnection) u.openConnection();
				conn.addRequestProperty("x-api-key", "LdmotsobXy8wsM67ACOtO8KU5aGi8Mw24PF3KtUF");
				conn.setRequestProperty("User-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
				Files.copy(conn.getInputStream(), new File(FILE_PATH +MERCHANT_ID+"_"+ i + ".csv").toPath(),StandardCopyOption.REPLACE_EXISTING);
				System.out.println(new Date() + " Downloaded " + fileNames[i] + ", convert to file name " + FILE_PATH +MERCHANT_ID+"_"+ i + ".csv");
			}
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}
	
	private static boolean checkFile() {
		boolean result = true;
		try {
			File folder = new File(FILE_PATH);
			File[] listOfFiles = folder.listFiles();
			if(listOfFiles != null && listOfFiles.length > 0) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						if(listOfFiles[i].getName().contains("300617")) {
							long size = listOfFiles[i].length();
							if(size <= 100000)
								return false;
						}
					}
				}
			}else {
				result = false;
				System.out.println(new Date() + " File not found");
			}
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}
	
	private static void moveFile(String path,String destination) {
		try {
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			if(listOfFiles != null && listOfFiles.length > 0) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						if(listOfFiles[i].getName().contains("300617")) {
							listOfFiles[i].renameTo(new File(destination+listOfFiles[i].getName()));
							listOfFiles[i].delete();
						}
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void deleteOldFile(String filePath) {
		try {
			File folder = new File(filePath);
			File[] listOfFiles = folder.listFiles();
			if(listOfFiles != null && listOfFiles.length > 0) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						if(listOfFiles[i].getName().contains("300617")) 
							listOfFiles[i].delete();
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static String sendPost(String data) throws Exception {
		String url = "https://pzay6sw4e6.execute-api.ap-southeast-1.amazonaws.com/api/";

		URL u = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setDoOutput(true);
		conn.addRequestProperty("x-api-key", "LdmotsobXy8wsM67ACOtO8KU5aGi8Mw24PF3KtUF");
		conn.setRequestProperty("User-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
		conn.setRequestMethod("POST");
		DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
		outStream.writeBytes(data);
		outStream.flush();
		try (InputStream is = conn.getInputStream();
				InputStreamReader isr = new InputStreamReader(is, "UTF-8");
				BufferedReader brd = new BufferedReader(isr);) {

			StringBuilder rtn = new StringBuilder(1000);
			String line = "";
			while ((line = brd.readLine()) != null) {
				rtn.append(line);
			}
			return rtn.toString();
		} catch (IOException e) {
			consumeInputStreamQuietly(conn.getErrorStream());
		}
		return null;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null)
				try { in.close(); } catch (IOException e) {}
		}
	}
	
}
