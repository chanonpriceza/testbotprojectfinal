package tool.bot_th_12;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Util.DatabaseUtil;
import Util.DefaultImageParser;
import Util.Util;
import bean.BotProductDataBean;
import bean.BotSendDataBean;
import bean.BotUrlPatternBean;
import db.BotProductDataDB;
import db.BotSendDataDB;
import db.BotUrlPatternDB;


public class tool_lazada_crawl_by_file_log  {
	
	
	//private static final String INPUT_PATH = "D:/lazada/";	
	private static final String INPUT_PATH = "/home/sale/300667-lazada/";
	private static final String FILENAME_KEY = "lazada-purchase-";
	

	private static final int MERCHANT_ID = 300667;
	private static final int IMAGE_WIDTH = 200;
	private static final int IMAGE_HEIGHT = 200;
	
	private Map<String, String[]> categoryMap = null;
	
	private DataSource configDS, dataDS;
	private BotUrlPatternDB urlPatternDB;
	private BotProductDataDB productDataDB;
	private BotSendDataDB sendDataDB;
	
	private static int lineCount=0, addProductCount=0, dupProductCount=0, errorProductCount=0,addProductFileCount=0,
	dupProductFileCount=0,errorProductFileCount=0,error404,errorFile404,errorTimeOut,errorFileTimeOut,lineCountTotal,expireCount,expireFileCount;
	
	public static void main(String[] args) {

		System.out.println(new Date() + " : Start tool_lazada_list_cat_by_conversion");
		try {
			tool_lazada_crawl_by_file_log t = new tool_lazada_crawl_by_file_log();
			t.setInitVariable();
			t.setDatabase();
			t.setCategory();
			t.run();
		} catch(RuntimeException e) {
			System.out.println(new Date() + " : : " + e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " : Finish tool_lazada_list_cat_by_conversion");
		
	}
	
	private void setInitVariable() {
		categoryMap = new HashMap<>();
	}
	
	private void setDatabase() {
		
		// bot14
		String webDBDriverName 	= "com.mysql.jdbc.Driver";
//		String webDBURL 		= "jdbc:mysql://localhost:3306/testdb4.0";
//		String webDBUserName 	= "root";
//		String webDBPassword 	= "root";		
		String webDBURL 		= "jdbc:mysql://27.254.86.48:3306/pzbotbackend_th";
		String webDBUserName 	= "botupdate";
		String webDBPassword 	= "Process88";		
		configDS = DatabaseUtil.createDataSource(webDBURL, webDBDriverName, webDBUserName, webDBPassword, 3);
		
//		// local
//		String dataDBDriverName = "com.mysql.jdbc.Driver";
//		String dataDBURL 		= "jdbc:mysql://localhost:3306/test";
//		String dataDBUserName 	= "root";
//		String dataDBPassword 	= "root";		
//		dataDS = DatabaseUtil.createDataSource(dataDBURL, dataDBDriverName, dataDBUserName, dataDBPassword, 3);
		
		// center2
		String dataDBDriverName = "com.mysql.jdbc.Driver";
//		String dataDBURL 		= "jdbc:mysql://localhost:3306/testdb4.0";
//		String dataDBUserName 	= "root";
//		String dataDBPassword 	= "root";		
		String dataDBURL 		= "jdbc:mysql://27.254.86.48:3308/bot";
		String dataDBUserName 	= "botupdate";
		String dataDBPassword 	= "Process88";		
		dataDS = DatabaseUtil.createDataSource(dataDBURL, dataDBDriverName, dataDBUserName, dataDBPassword, 3);
		
		urlPatternDB = new BotUrlPatternDB(configDS);
		productDataDB = new BotProductDataDB(dataDS);
		sendDataDB = new BotSendDataDB(dataDS);
	}

	private void setCategory() throws SQLException {
		List<BotUrlPatternBean> categoryMapping = urlPatternDB.findUrlPattern(MERCHANT_ID, "FEED");
		if(categoryMapping != null && categoryMapping.size() > 0) {
			for(BotUrlPatternBean cat : categoryMapping){
				String merchantCat = cat.getValue();
				if(isBlank(merchantCat))
					continue;
				
				String pzCatId = Integer.toString(cat.getCategoryId());
				String pzKeyword = cat.getKeyword();
				String[] mapValue = new String[]{ pzCatId, pzKeyword};
				categoryMap.put(merchantCat, mapValue);
			}
		}
	}
	
	
	private void run() {
		Util.enableUseProxy();
		File folder = new File(INPUT_PATH);
		File[] listOfFiles = folder.listFiles();
		
		int fileCount = 0;
		if(listOfFiles != null && listOfFiles.length > 0) {
			for (File INPUT_FILE : listOfFiles) {
				
				if(INPUT_FILE.getName().toLowerCase().contains(FILENAME_KEY)) {
					
					fileCount++;
					System.out.println(new Date() + " : Start read file : " + INPUT_FILE.getName());
					
					try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(INPUT_FILE), "UTF-8"));){
						addProductFileCount=0;
						dupProductFileCount=0;
						errorFileTimeOut=0;
						errorFile404=0;
						errorProductFileCount=0;
						lineCount = 0;
						expireFileCount=0;
						String line = null;
						while ((line = br.readLine()) != null) { 
							lineCount++;
							lineCountTotal++;
							
							String url = line;
							
							if(StringUtils.isNotBlank(url)) {
								String[] split = url.split(",");
								if(split!=null&&split.length==2) {
									String shopId = split[0];
									String itemId = split[1];
									if(NumberUtils.isCreatable(shopId) && NumberUtils.isCreatable(itemId)) {
										String genLink =	createLink(shopId,itemId);
										run(genLink);
									}
									
								}
							}
							
							if(lineCount%1000 == 0) {
								String summaryText = ""
										+ "lineCount = " 		+ lineCount
										+ ", addProduct = " 	+ addProductCount
										+ ", dupProduct = " 	+ dupProductCount
										+ ", errorRecord = " 	+ errorProductCount
										+ ", error404 = " 	+ error404
										+ ", timeout = " 	+ errorTimeOut
										+ ", expire = " 	+ expireCount;
								System.out.println(new Date() + " : Summary records : " + summaryText);
							}
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
					
					System.out.println(new Date() + " : Finish read file : " + INPUT_FILE.getName());
					replaceDoneFile(INPUT_FILE);
					
					String summaryText = ""
							+ "lineCount = " 		+ lineCount
							+ ", addProduct = " 	+ addProductFileCount
							+ ", dupProduct = " 	+ dupProductFileCount
							+ ", errorRecord = " 	+ errorProductFileCount
							+ ", error404 = " 	+ errorFile404
							+ ", timeout = " 	+ errorFileTimeOut
							+ ", expire = " 	+ expireFileCount;
					System.out.println(new Date() + " : Summary Each File records : "+INPUT_FILE.getName()+" → " + summaryText);
					
				}
				
			}
			
			String summaryText = ""
					+ "lineCount = " 		+ lineCountTotal
					+ ", addProduct = " 	+ addProductCount
					+ ", dupProduct = " 	+ dupProductCount
					+ ", errorRecord = " 	+ errorProductCount
					+ ", error404 = " 	+ error404
					+ ", timeout = " 	+ errorTimeOut
					+ ", expire = " 	+ expireCount;
			System.out.println(new Date() + " : Summary Total : "+summaryText);

		}
		
		System.out.println(new Date() + " : Finish file count " + fileCount);
	}
		
	
	private String createLink(String shopId,String itemId) {
		String lazadaLink = "https://www.lazada.co.th/products/-i"+itemId+"-s"+shopId+".html";
		return lazadaLink;
	}
	
	private void replaceDoneFile(File input) {
		String newName = input.getName().replace(FILENAME_KEY, "lazada_DONE_");
		input.renameTo(new File(INPUT_PATH+newName));
		System.out.println(new Date() + " : Rename target to : " + newName);
	}

	private void run(String url) {
		try {
			String[] data = Util.httpRequestWithProxy(url, "UTF-8", true);
			if(data==null||data.length!=2||!data[1].equals("200")) {
				return;
			}
			
			if(data[0].contains("non-existent products")) {
				error404++;
				errorFile404++;
			}
			
			if(data[0].contains("captcha | lazadacom")) {
				errorFileTimeOut++;
				errorTimeOut++;
			}
			
			BotProductDataBean pdb = parseJSON(data[0],url);
			if(pdb==null) {
				System.err.println(url);
				errorProductFileCount++;
				errorProductCount++;
			}else	
				insertPdb(pdb);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static DefaultImageParser image = new DefaultImageParser();

	
	private void insertPdb(BotProductDataBean pdb) throws SQLException {
		if(pdb.getUpdateDate()==null)
			pdb.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		
		pdb.setMerchantId(MERCHANT_ID);
		
		int insertProductDataDBResult = productDataDB.insertProductData(pdb);
		if(insertProductDataDBResult > 0) {
			byte[] imageByte = image.parse(pdb.getPictureUrl(),IMAGE_WIDTH, IMAGE_HEIGHT);
			BotSendDataBean sendDataBean = BotSendDataBean.createSendDataBean(pdb, imageByte, BotSendDataBean.ACTION.ADD);
			sendDataDB.insertSendData(sendDataBean);
			addProductCount++;
			addProductFileCount++;
		}else {
			dupProductCount++;
			dupProductFileCount++;
		}
	}
	
	private BotProductDataBean parseJSON(String html,String currentUrl) {
		if(StringUtils.isBlank(html)) return null;
    	
    	String allScriptTxt = Util.getStringBetween(html, "try{", "} catch(e) {");
    	String allObjectTxt = Util.getStringBetween(allScriptTxt, "app.run(", ");");
    	
    	if(StringUtils.isBlank(allObjectTxt)) return null;
    	try {
			JSONObject appObj = (JSONObject) new JSONParser().parse(allObjectTxt);
			if(appObj == null) return null;
			
			JSONObject dataObj = (JSONObject) appObj.get("data");
			if(dataObj == null) return null;
			
			JSONObject rootObj = (JSONObject) dataObj.get("root");
			if(rootObj == null) return null;
			
			JSONObject fieldObj = (JSONObject) rootObj.get("fields");
			if(fieldObj == null) return null;
			
			JSONObject skuInfoObj = (JSONObject) fieldObj.get("skuInfos");
			if(skuInfoObj == null) return null;
			
			JSONObject productObj = (JSONObject) fieldObj.get("product");
			if(productObj == null) return null;
			
			JSONObject sku0Obj = (JSONObject) skuInfoObj.get("0");
			if(sku0Obj == null) return null;
			
			JSONObject dataLayerObj = (JSONObject) sku0Obj.get("dataLayer");
			if(dataLayerObj == null) return null;
			
			JSONObject priceObj = (JSONObject) sku0Obj.get("price");
			if(priceObj == null) return null;
			
			String sellerName = String.valueOf((Object) dataLayerObj.get("seller_name"));
			
			String itemId = String.valueOf((Object) sku0Obj.get("itemId"));
			String skuId = String.valueOf((Object) sku0Obj.get("skuId"));
			
			String productName = String.valueOf((Object) productObj.get("title"));
			if(StringUtils.isNotBlank(productName)) {
				productName = Util.toPlainTextString(productName);
				if(productName.length() > 200) {
					productName = productName.substring(0, 180);				
				}
			}
			
			String productDesc = String.valueOf((Object) productObj.get("highlights"));
			if(StringUtils.isNotBlank(productDesc)) productDesc = Util.toPlainTextString(productDesc);
			
			if("null".equals("null"))
				productDesc = null;
			
			String productPrice = String.valueOf((Object) dataLayerObj.get("pdt_price"));
			String productBasePrice = "0";
			JSONObject originalPriceObj = (JSONObject) priceObj.get("originalPrice");
			JSONObject salePriceObj = (JSONObject) priceObj.get("salePrice");
			if(originalPriceObj != null && salePriceObj != null) {
				String originalPrice = String.valueOf((Object) originalPriceObj.get("text"));
				String salePrice = String.valueOf((Object) salePriceObj.get("text"));
				originalPrice = Util.removeCharNotPrice(originalPrice);
				salePrice = Util.removeCharNotPrice(salePrice);
				if(!originalPrice.equals(salePrice)) {
					productPrice = salePrice;
					productBasePrice = originalPrice;
				}
			}
			JSONObject q = (JSONObject) sku0Obj.get("quantity");
			if(q!=null) {
				String ex = String.valueOf(q.get("text"));
				if(ex.contains("สินค้าหมด")) {
					expireCount++;
					expireFileCount++;
					return null;
				}
				
			}
			String productImage = String.valueOf((Object) sku0Obj.get("image"));
			if(!productImage.startsWith("http")) {
				URL url = new URL(new URL(currentUrl), productImage);
				productImage = url.toString();
				productImage = productImage.replace(".jpg", ".jpg_400x400q80.jpg");
			}
			
			BotProductDataBean result = getResult(sellerName, productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
			if(result != null) {
				return result;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return null;
	}
	
	protected BotProductDataBean getResult(String sellerName, String productName, String productPrice, String productBasePrice, 
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {

		return gatherData(productName, productPrice, productBasePrice, currentUrl, productDesc, productImage, itemId, skuId);
	}
	
	protected BotProductDataBean gatherData(String productName, String productPrice, String productBasePrice,
			String currentUrl, String productDesc, String productImage, String itemId, String skuId) {
		
		BotProductDataBean rtn = new BotProductDataBean();
		rtn.setName(productName + " (" + skuId + ")");
		rtn.setPrice(Util.convertPriceStr(Util.removeCharNotPrice(productPrice)));
		rtn.setBasePrice(Util.convertPriceStr(Util.removeCharNotPrice(productBasePrice)));
		rtn.setUrl("https://c.lazada.co.th/t/c.PVm?url="+ URLEncoder.encode(currentUrl));
		rtn.setUrlForUpdate(currentUrl);
		rtn.setDescription(productDesc);
		rtn.setPictureUrl(productImage);
		rtn.setRealProductId(skuId);
		
		
		return rtn;
	}
	
	
	
	
	

}
