package tool.bot_th_12;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Util.Util;
import bean.BotProductDataBean;

public class tool_download_tqm {
	
//	private static String FILE_PATH = "D://____WORK//";
	private static String FILE_PATH = "/home/sale/7112-tqm/";
	
	private static JSONParser parser = new JSONParser();
	private static final DecimalFormat formatPrice = new DecimalFormat("###");

	private final static int REQUEST_TIMEOUT = 10000;
	private final static String[] YEAR_INSURE_OFFSET = new String[] {"1","2","20","3","30"};
	private final static String TQM_CAR_REQUEST_URL = "https://www.tqm.co.th/service/insure_service/service_car.php?wsdl";
	private final static String TQM_INSURE_REQUEST_URL = "https://www.tqm.co.th/service/insure_service/service_insure.php?wsdl";
	public enum TYPE {
		GetCarBrand,GetCarModel,GetCarSubModel,GetInsure	
	}
	
	private static JSONArray allBrandArray;
	private static List<String> finishBrandList;
	
	public static void main(String[] args) {
		System.out.println(new Date() + " Start running tool download TQM");
		try {
			
			checkAllBrand();
			checkFinishBrand();
			if(checkRuntimeCondition()) {
				ExecutorService ex = Executors.newFixedThreadPool(1);
				Future<?> mainProcess = ex.submit(new Runnable() {
					public void run() {
						for (Object brandObject : allBrandArray) {
							try {
								runBrand((JSONObject) brandObject);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						System.out.println(new Date() + " " + Thread.currentThread().getName() + " Finish All");
						runMove();
					}
				});
				
				ex.shutdown();
				if(ex.awaitTermination(6, TimeUnit.HOURS)){
					System.out.println(new Date() + " " + Thread.currentThread().getName() + " Done All.");
				}else{
					System.out.println(new Date() + " " + Thread.currentThread().getName() + " Time out.");
					mainProcess.cancel(true);
					ex.shutdownNow();
				}
					
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " Finish running tool download TQM");
		System.exit(0);
	}
	
	private static void checkAllBrand() throws ParseException {
		String param = genJSONObject("Username,priceza","Password,Pi21_Z4@x");
		String body =  genParam(TYPE.GetCarBrand.toString(),param);
		String response = feedRequest(body, TQM_CAR_REQUEST_URL);
		response = StringEscapeUtils.unescapeXml(response);
		JSONObject jObj = (JSONObject) parser.parse(getResult(response));
		allBrandArray = (JSONArray)jObj.get("Result");
	}
	
	private static void checkFinishBrand(){
		
		String finishFileName = FILE_PATH + "tqm-config-finish-brand.txt";
		File finishFile = new File(finishFileName);
		finishBrandList = new ArrayList<>();
		
		if(finishFile.exists()) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(finishFileName),"UTF-8"))) {
				String text = "";
				while((text = br.readLine()) != null) {
					if(StringUtils.isNotBlank(text)) {
						finishBrandList.add(text);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			clearFinishBrandConfig(finishFileName);
		}
	}

	private static void addFinishBrand(String finishBrand) {
		String finishFileName = FILE_PATH + "tqm-config-finish-brand.txt";
		try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(finishFileName),  true));){
			pwr.println(finishBrand);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " Finish brand " + finishBrand);
	}
	
	private static void clearFinishBrandConfig(String finishFileName) {
		File finishFile = new File(finishFileName);
		finishFile.delete();
	}
	
	private static boolean checkRuntimeCondition() {
		
		System.out.println(new Date() + " " + Thread.currentThread().getName() + "allBrand = " + allBrandArray.size());
		System.out.println(new Date() + " " + Thread.currentThread().getName() + "finishBrand = " + finishBrandList.size());
		
		if(finishBrandList.size() == allBrandArray.size()) {
			String finishFileName = FILE_PATH + "tqm-config-finish-brand.txt";
			clearFinishBrandConfig(finishFileName);
			runMove();
			return false;
		}
		
		return true;

	}
	
	private static void runMove() {
		// move file here
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " ---------------------------------------------------------");
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " Start Move Files");
		try {
			
			String sourcePath = FILE_PATH + "tqm-loading/";
			String targetPath = FILE_PATH + "tqm-complete/";
			
			File folder = new File(sourcePath);
			File[] listOfFiles = folder.listFiles();
			if(listOfFiles != null && listOfFiles.length > 0) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						listOfFiles[i].renameTo(new File(targetPath + listOfFiles[i].getName()));
						System.out.println(new Date() + " " + Thread.currentThread().getName() + " Move " + listOfFiles[i].getName());
						
						listOfFiles[i].delete();
						System.out.println(new Date() + " " + Thread.currentThread().getName() + " Delete " + listOfFiles[i].getName());
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " Finish Move Files");
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " ---------------------------------------------------------");
	}
	
	private static void runBrand(JSONObject brandJSON) throws ParseException, InterruptedException {
		
		String makeCode = String.valueOf(brandJSON.get("MakeCode"));
		String descriptionGetBrand = String.valueOf(brandJSON.get("Description"));
		
		if(finishBrandList.contains(descriptionGetBrand)) {
			return;
		}
		
		String param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", "MakeCode," + makeCode);
		String body = genParam(TYPE.GetCarModel.toString(), param);
		
		String response = StringEscapeUtils.unescapeXml(feedRequest(body,TQM_CAR_REQUEST_URL));
		JSONObject jObj = (JSONObject) parser.parse(getResult(response));
		JSONArray modelArray = (JSONArray) jObj.get("Result");
		
		System.out.println(new Date() + " " + Thread.currentThread().getName() + " Request GetCarModel " + makeCode + ", result " + modelArray.size());
		
//		ExecutorService ex = Executors.newFixedThreadPool(7);
//		CountDownLatch latch = new CountDownLatch(7);
		for (Object model : modelArray) {
			run(makeCode, descriptionGetBrand, (JSONObject) model);
		}
//		ex.shutdown();
//		latch.await();
		
		addFinishBrand(descriptionGetBrand);
		
	}
	
	@SuppressWarnings("unchecked")
	private static void run(String makeCode, String descriptionGetBrand, JSONObject modelJSON) {
		try {
			String param, body, response;
			
			String descriptionGetModel = String.valueOf(modelJSON.get("Description"));
			String LatestYear = String.valueOf(modelJSON.get("LatestYear"));
			String StartYear = String.valueOf(modelJSON.get("StartYear"));
			String familyCode = String.valueOf(modelJSON.get("FamilyCode"));
			String carUsage = String.valueOf(modelJSON.get("CarUsage"));
			
			int lastYear = Util.stringToInt(LatestYear, 2019);
			int startYear = Util.stringToInt(StartYear, 2003);
	
			for (int y = startYear; y <= lastYear; y++) {
				
				param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", "MakeCode," + makeCode, "FamilyCode," + familyCode, "Year," + y);
				body = genParam(TYPE.GetCarSubModel.toString(), param);
				
				response = feedRequest(body, TQM_CAR_REQUEST_URL);
				response = StringEscapeUtils.unescapeXml(response);

				JSONObject jObj = null;
				
				try {
					jObj = (JSONObject) parser.parse(getResult(response));
				} catch (ParseException e) {
					e.printStackTrace();
					continue;
				}
				if ("No Result".equals(jObj.get("Result"))) {
					System.out.println(new Date() + " " + Thread.currentThread().getName() + " Request Brand " + makeCode + ", GetCarSubModel " + familyCode + ", year " + y + ", result 0");
					continue;
				}
	
				JSONArray cars = (JSONArray) jObj.get("Result");
	
				System.out.println(new Date() + " " + Thread.currentThread().getName() + " Request Brand " + makeCode + ", GetCarSubModel " + familyCode + ", year " + y + ", result " + cars.size());
				
				for (Object c : cars) {
	
					JSONObject car = (JSONObject) c;
	
					String descriptionGetSubModel = String.valueOf(car.get("Description"));
					String cc = String.valueOf(car.get("Cc"));
					String year = String.valueOf(car.get("Year"));
	
					String vehicleKey = String.valueOf(car.get("VehicleKey"));
					for (String insureLevel : YEAR_INSURE_OFFSET) {
						
						param = genJSONObject("Username,priceza", "Password,Pi21_Z4@x", 
								"MakeCode,"     + makeCode,
								"FamilyCode,"   + familyCode, 
								"Year,"         + year, 
								"InsureLevel,"  + insureLevel,
								"Cc,"           + cc, 
								"VehicleKey,"   + vehicleKey, 
								"CarUsage,"     + carUsage, 
								"CoverMin,"     + 0,
								"CoverMax,"     + 9999999);
						
						body = genParam(TYPE.GetInsure.toString(), param, "GetInsureDetail");
						response = StringEscapeUtils.unescapeXml(feedRequest(body, TQM_INSURE_REQUEST_URL));
						
						try {
							jObj = (JSONObject) parser.parse(getResult(response));
						} catch (ParseException e) {
							e.printStackTrace();
							continue;
						}
						if (jObj == null || "No Result".equals(jObj.get("Result"))) {
							System.out.println(new Date() + " " + Thread.currentThread().getName() + " Brand " + makeCode + ", Request insure " + descriptionGetSubModel + ", year " + year + ", level " + insureLevel + ", result 0");
							continue;
						}
						if (jObj.get("Result") == null || jObj.get("Result").equals("No Result")) {
							System.out.println(new Date() + " " + Thread.currentThread().getName() + " Brand " + makeCode + ", Request insure " + descriptionGetSubModel + ", year " + year + ", level " + insureLevel + ", result 0");
							continue;
						}
	
						JSONArray jsonArray = (JSONArray) jObj.get("Result");
						
						System.out.println(new Date() + " " + Thread.currentThread().getName() + " Brand " + makeCode + ", Request insure " + descriptionGetSubModel + ", year " + year + ", level " + insureLevel + ", result " + jsonArray.size());
						
						for (Object isure : jsonArray) {
							JSONObject insureJSON = (JSONObject) isure;
							if (insureJSON.get("Level") == null || String.valueOf(insureJSON.get("Level")).equals("9"))
								continue;
							insureJSON.put("MakeCode", makeCode);
							insureJSON.put("FamilyCode", familyCode);
							insureJSON.put("Year", year);
							insureJSON.put("Cc", cc);
							insureJSON.put("VehicleKey", vehicleKey);
							insureJSON.put("CarUsage", carUsage);
							insureJSON.put("descriptionGetBrand", descriptionGetBrand);
							insureJSON.put("descriptionGetModel", descriptionGetModel);
							insureJSON.put("descriptionGetSubModel", descriptionGetSubModel);
							mockJSON(insureJSON, makeCode);
						}
					}
				}
			}
		} catch(Exception e) {
			System.err.println(e);
		}
	}
	
		
	private static void mockJSON(JSONObject obj, String makeCode) {
			
//		{
//			"Cc": "2.2",
//			"descriptionGetModel": "Brera",
//			"Vat": "168.84",
//			"CoverAcc": "50000",
//			"CoverLegal": "50000",
//			"ProductId": "14859",
//			"CoverFlooding": "N",
//			"Cover3RD": "300000",
//			"CompanyName": "ประกันภัยไทยวิวัฒน์",
//			"Duty": "10.00",
//			"VehicleKey": "ALFA13AB",
//			"NetAmount": "2580.84",
//			"CoverMed": "50000",
//			"CoverDeduct": "-",
//			"CarUsage": "110",
//			"descriptionGetSubModel": "Manual \/ 2.2 \/ (CBU) \/ 2dr \/ ราคา 4,690,000",
//			"CoverAccNum": "5",
//			"CoverAccPass": "50000",
//			"descriptionGetBrand": "Alfa Romeo",
//			"CarCheck": "N",
//			"GarageTotal": "1710",
//			"Cover3RDTime": "10000000",
//			"PromotionCodeSupplier": "TVI3110",
//			"Terrorism": "N",
//			"PromotionCode": "TVI 3110",
//			"Amount": "2402.00",
//			"MakeCode": "ALFA",
//			"ShowDeductNoPaid": "ND",
//			"CoverLostFire": "0",
//			"Cover": "0",
//			"Garage": "ซ่อมอู่",
//			"Year": "2006",
//			"CoverAccPassNum": "5",
//			"FamilyCode": "BRERA",
//			"Level": "3",
//			"CompleteOnline": "N",
//			"CompanyCode": "TVI",
//			"Cover3RDAsset": "1000000"
//		}

		String id 			= String.valueOf(obj.get("ProductId"));                                  			
		String insurer 		= String.valueOf(obj.get("CompanyName"));                                        	
		String type			= String.valueOf(obj.get("Level"));                                           		
		String title		= insurer+type;      
		String brand		= String.valueOf(obj.get("descriptionGetBrand"));                                   
		String model		= String.valueOf(obj.get("descriptionGetModel"));                                   
		
		String subModel		= String.valueOf(obj.get("descriptionGetSubModel"));                                
		subModel 			= Util.getStringBefore(subModel,"ราคา", model);
		subModel 			= subModel.replaceAll("/","");
		String year			= String.valueOf(obj.get("Year"));                                            		
		String repair		= String.valueOf(obj.get("Garage"));                                   				

		String fire			= String.valueOf((Object)obj.get("CoverLostFire"));                       			
		String theft		= String.valueOf((Object)obj.get("CoverLostFire"));                             	
		String flood		= String.valueOf((Object)obj.get("CoverFlooding"));                     			
		String bi			= String.valueOf((Object)obj.get("Cover3RD"));           							
		String bt			= String.valueOf((Object)obj.get("Cover3RDTime"));  								
		String tp			= String.valueOf((Object)obj.get("Cover3RDAsset"));         						
		String pa			= String.valueOf((Object)obj.get("CoverAcc"));        								
		String bb			= String.valueOf((Object)obj.get("CoverLegal"));                   					
		String me			= String.valueOf((Object)obj.get("CoverAccPass"));                 					
		String cc			= String.valueOf((Object)obj.get("Cc"));                 						 	
		
		String premium		= String.valueOf((Object)obj.get("NetAmount"));                       				
		String od			= String.valueOf((Object)obj.get("Cover"));                      					
		String oddd			= String.valueOf((Object)obj.get("CoverDeduct"));

		if(type.contains("0")) {
			type = type.replace("0","+");
		}
		
		cc = cc+"cc";
		
		String uniqueText = "";
		uniqueText += id;
		uniqueText += type;
		uniqueText += insurer;
		uniqueText += brand;
		uniqueText += model;
		uniqueText += subModel;
		uniqueText += year;
		uniqueText += title;
		uniqueText += repair;
		uniqueText += od;
		uniqueText = toMD5(uniqueText);
		
		String productName = "";
		productName += "ประกันชั้น " + type;
		productName += " - " + insurer.replace("-", " ");
		productName += " - " + brand.replace("-", " ");
		productName += " - " + model.replace("-", " ")+" "+cc;
		productName += " - " + ((StringUtils.isNotBlank(subModel))? subModel.replace("-", " ") : "ทุกรุ่น");
		productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
		productName += " (" + uniqueText.substring(0,10) + ")";
		productName = productName.replace(",", "");
		productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
		productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
		productName = productName.replaceAll("\\s+", " ");                                                  
		productName = productName.replace("( ", "(");                                                       
		productName = productName.replace(" )", ")");                                                       
		productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
		productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");   		

		String productDesc = "";
		productDesc += insurer+" ประกันชั้น " + type;;
		productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(repair))? repair : " ไม่ระบุอู่/ห้าง");
		productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
		productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : (oddd.equals("-")) ? "ไม่ระบุ":oddd ));
		productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
		productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
		productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0") && !flood.equals("N"))? flood : "ไม่ระบุ" ); 
		productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
		productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
		productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
		productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
		productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );         
		productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
		productDesc = productDesc.replaceAll("\\s+", " ");                                               
		productDesc = productDesc.replace("( ", "(");                                                    
		productDesc = productDesc.replace(" )", ")");                                                    
		productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
		productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
		productDesc = productDesc.replace("ซ่อมซ่อม", "ซ่อม");                         
		productDesc = productDesc.replace(",", "");
		
		String productPrice =  formatPrice.format(Double.parseDouble(Util.removeCharNotPrice(premium)));
		
		String productUrl = "https://www.tqm.co.th/";
		String productUrlForUpdate = uniqueText;
		String productImage = getImage(insurer);
		
		BotProductDataBean pdb = new BotProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(Util.convertPriceStr(productPrice));
		pdb.setPictureUrl(productImage);
		pdb.setDescription(productDesc);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrlForUpdate);
		pdb.setCategoryId(250101);
		pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;
		
		String mockData = mockResult(pdb);
		if(StringUtils.isNotBlank(mockData)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(FILE_PATH + "tqm-loading/mockFeed-" + makeCode + ".xml"),  true));){
				pwr.println(mockData);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
		
	private static String getImage(String insurer) {
		
		String productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-55651.jpg";
		if(insurer.equalsIgnoreCase("สินทรัพย์ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5567.jpg"; 
		if(insurer.equalsIgnoreCase("เอเชียประกันภัย 1950"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5570.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ ซี.พี. ประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5561.jpg"; 
		if(insurer.equalsIgnoreCase("กรุงเทพประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5558.jpg"; 
		if(insurer.equalsIgnoreCase("ทิพยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5565.jpg"; 
		if(insurer.equalsIgnoreCase("กรุงไทยพานิชประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5571.jpg"; 
		if(insurer.equalsIgnoreCase("เคเอสเคประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5559.jpg"; 
		if(insurer.equalsIgnoreCase("แอลเอ็มจีประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5572.jpg"; 
		if(insurer.equalsIgnoreCase("เมืองไทยประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5551.jpg"; 
		if(insurer.equalsIgnoreCase("นวกิจประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5569.jpg"; 
		if(insurer.equalsIgnoreCase("อาคเนย์ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5552.jpg"; 
		if(insurer.equalsIgnoreCase("สินมั่นคงประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5562.jpg"; 
		if(insurer.equalsIgnoreCase("ประกันคุ้มภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5554.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5575.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยไพบูลย์ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9559.jpg"; 
		if(insurer.equalsIgnoreCase("ประกันภัยไทยวิวัฒน์"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5574.jpg"; 
		if(insurer.equalsIgnoreCase("วิริยะประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2018/02/insure-company-5555.jpg"; 
		if(insurer.equalsIgnoreCase("อลิอันซ์ โกลบอล แอสซิสแทนซ์"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9605.jpg"; 
		if(insurer.equalsIgnoreCase("ไอโออิ กรุงเทพ ประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5568.jpg"; 
		if(insurer.equalsIgnoreCase("แอกซ่าประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5557.jpg"; 
		if(insurer.equalsIgnoreCase("บูพา"))					productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9606.jpg"; 
		if(insurer.equalsIgnoreCase("จรัญประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9558.jpg"; 
		if(insurer.equalsIgnoreCase("ซิกน่าประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9604.jpg"; 
		if(insurer.equalsIgnoreCase("เจ้าพระยาประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5560.jpg"; 
		if(insurer.equalsIgnoreCase("เทเวศประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5563.jpg"; 
		if(insurer.equalsIgnoreCase("เอราวัณประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9569.jpg"; 
		if(insurer.equalsIgnoreCase("อินทรประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9572.jpg"; 
		if(insurer.equalsIgnoreCase("กมลประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9576.jpg"; 
		if(insurer.equalsIgnoreCase("เอ็มเอสไอจีประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5564.jpg"; 
		if(insurer.equalsIgnoreCase("นำสินประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5573.jpg"; 
		if(insurer.equalsIgnoreCase("เอไอจี ประกันภัย"))				productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9435.jpg"; 
		if(insurer.equalsIgnoreCase("ฟีนิกซ์ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9573.jpg"; 
		if(insurer.equalsIgnoreCase("รู้ใจ รับประกันโดย KPI กรุงไทยพานิช"))	productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9419.jpg"; 
		if(insurer.equalsIgnoreCase("ศรีอยุธยา เจนเนอรัล ประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9565.jpg"; 
		if(insurer.equalsIgnoreCase("สามัคคีประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5553.jpg"; 
		if(insurer.equalsIgnoreCase("ธนชาตประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5556.jpg"; 
		if(insurer.equalsIgnoreCase("โตเกียวมารีนศรีเมืองประกันภัย"))		productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9601.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยเศรษฐกิจประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-5566.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยพัฒนาประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9556.jpg"; 
		if(insurer.equalsIgnoreCase("ไทยศรีประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9561.jpg"; 
		if(insurer.equalsIgnoreCase("มิตรแท้ประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9566.jpg"; 
		if(insurer.equalsIgnoreCase("พุทธธรรมประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9564.jpg"; 
		if(insurer.equalsIgnoreCase("บางกอกสหประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9568.jpg"; 
		if(insurer.equalsIgnoreCase("สหนิรภัยประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9580.jpg"; 
		if(insurer.equalsIgnoreCase("สหมงคลประกันภัย"))			productImage = "https://halobe.files.wordpress.com/2017/08/insure-company-9567.jpg"; 
		
		return productImage;
	}
		
	private static String mockResult(BotProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	// ##############################################################################
	// ################### UTILITY METHOD ###########################################
	// ##############################################################################
	
	private static String genParam(String genType,String param,String genTypeUrl) {
		
		String  mainString =  "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + 
			"    <Body>\r\n" + 
			"        <"+genType+" xmlns=\"http://www.tqm.co.th/soap/"+genTypeUrl+"\">\r\n" + 
			"           <parameter>"+param+"</parameter>\r\n" + 
			"        </"+genType+">\r\n" + 
			"    </Body>\r\n" + 
			"</Envelope>";
		return mainString;
		
	}
	
	private static String genParam(String genType,String param) {
		return genParam(genType,param,"GetCarDetail");
	}
	
	@SuppressWarnings("unchecked")
	private static String genJSONObject(String... params) {
		JSONObject jsonObj = new JSONObject();
		for(String p:params) {
			String[] param = p.split(",");
			if(param==null||param.length!=2)
				continue;
			String key = param[0];
			String value=  param[1];
			jsonObj.put(key,value);
		}
		return jsonObj.toJSONString();
	}
	
	private static String getResult(String result) {
		String parseResult = Util.getStringBetween(result,"<return xsi:type=\"xsd:string\">","</return>");
		return parseResult;
	}
	
	private static String feedRequest(String body,String url) {
	   	URL u = null;
	 	HttpURLConnection conn = null;
	 	BufferedReader brd = null;
	 	InputStreamReader isr = null;
	 	InputStream is = null;
	 	StringBuilder rtn = null;
	 	
	 	try {

	 		u = new URL(url);
	 		conn = (HttpURLConnection) u.openConnection();
	 		conn.setInstanceFollowRedirects(true);
	 		conn.setRequestMethod("POST");
	 		conn.setRequestProperty("Content-Type","text/xml");
	 		conn.setDoOutput(true);
			conn.setConnectTimeout(REQUEST_TIMEOUT);
			conn.setReadTimeout(REQUEST_TIMEOUT);
	 		DataOutputStream  wr = new DataOutputStream(conn.getOutputStream());
	 		wr.write(body.getBytes("UTF-8"));
	 		wr.flush();
	 		wr.close();
	 		
	 		conn.connect();
	 		if(conn.getResponseCode() != 200) {
	 			return null;
	 		}
	 		
	 		is = conn.getInputStream();
	 		isr = new InputStreamReader(new GZIPInputStream(is));
	 		brd = new BufferedReader(isr);
	 		rtn = new StringBuilder(5000);
	 		String line = "";
	 		while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
	 			rtn.append(line);
	 		}
	 	} catch (ConnectException e) {
			String message = "Connection timed out : " + url + " | " + body;
			System.out.print(message);
		} catch (SocketTimeoutException e) {
			String message = "Socket timed out : " + url + " | " + body;
			System.out.print(message);
	 	} catch (MalformedURLException e) {
	 	} catch (IOException e) {
	 		if(is != null) {
	 			try {
					isr = new InputStreamReader(is, "UTF-8");
					brd = new BufferedReader(isr);
					rtn = new StringBuilder(5000);
					String line = "";
					while (!Thread.currentThread().isInterrupted() && (line = brd.readLine()) != null) {
						rtn.append(line);
					}
				} catch (Exception e1) {
					e.printStackTrace();
				}
	 		}
	 	} finally {	
	 		if(brd != null) {
	 			try { brd.close(); } catch (IOException e) {	}
	 		}
	 		if(isr != null) {
	 			try { isr.close(); } catch (IOException e) {	}
	 		}
	 		if(conn != null) {
	 			conn.disconnect();
	 		}
	 	}
	 	
	 	if(rtn != null && StringUtils.isNotBlank(String.valueOf(rtn))) {
	 		return rtn.toString();
	 	}
		return null;
	 	
	}

	private static String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
}
