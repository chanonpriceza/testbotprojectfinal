package tool.bot_th_12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Util.Util;
import bean.BotProductDataBean;

public class tool_download_gettgo {
	
//	private static String FILE_PATH = "D:/";
	private static String FILE_PATH = "/home/sale/300673-gettgo/";
	private static String cookie = "";
	private static JSONParser parser = new JSONParser();
	private static final DecimalFormat formatPrice = new DecimalFormat("###");
	private static List<String[]> allCombination;
	private static List<String> finishCombinationList;
	
	public static void main(String[] args) {
		System.out.println(new Date() + " Start running tool download Gettgo");
		try {
			
			ExecutorService ex = Executors.newFixedThreadPool(1);
			Future<?> mainProcess = ex.submit(new Runnable() {
				public void run() {
					try {
						genCookie();
						listAllCombination();
						listCompleteCombination();
						if(checkRuntimeCondition()) {
							runAllCombination();
							moveAllFeed();
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			ex.shutdown();
			if(ex.awaitTermination(8, TimeUnit.HOURS)){
				System.out.println(new Date() + " Done All.");
			}else{
				System.out.println(new Date() + " Time out.");
				mainProcess.cancel(true);
				ex.shutdownNow();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date() + " Finish running tool download Gettgo");
		System.exit(0);
	}
	
	private static void genCookie() {
		URL u = null;
		HttpURLConnection conn = null;
		try {
			u = new URL(Util.encodeURL("https://gettgo.com/motor/search?manufacturer=Volkswagen&model=Sharan+(1900cc)&sub_model=ไม่แน่ใจ&year=2002&plate_province=กรุงเทพมหานคร&vehicle_id=70216&sort_by=price-asc&tier1=on&tier2plus=on&tier3plus=on&tier3=on&no_excess=on&dealer=on&garage=on&insurers[]=15&insurers[]=6&insurers[]=18"));
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent", "PostmanRuntime/7.26.3");
			conn.getResponseCode();
	//		System.out.println(status);
	//		System.out.println(conn.getHeaderFields());
			cookie = conn.getHeaderField("set-cookie");
			cookie = Util.getStringBetween(cookie,"_gettgo_web_session=",";");
			cookie = "_gettgo_web_session="+cookie+";";
	//		System.err.println(cookie);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void listCompleteCombination() throws Exception {
		String finishFileName = FILE_PATH + "gettgo-config-finish-combination.txt";
		File finishFile = new File(finishFileName);
		finishCombinationList = new ArrayList<>();
		
		if(finishFile.exists()) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(finishFileName),"UTF-8"))) {
				String text = "";
				while((text = br.readLine()) != null) {
					if(StringUtils.isNotBlank(text)) {
						finishCombinationList.add(text);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static boolean checkRuntimeCondition() throws Exception {
		
		System.out.println(new Date() + " " + Thread.currentThread().getName() + "allCombination = " + allCombination.size());
		System.out.println(new Date() + " " + Thread.currentThread().getName() + "finishCombination = " + finishCombinationList.size());
		
		if(finishCombinationList.size() == allCombination.size()) {
			moveAllFeed();
			return false;
		}
		return true;

	}
	
	@SuppressWarnings("unchecked")
	private static void listAllCombination() throws Exception {
		allCombination = new ArrayList<>();
		String url = "https://gettgo.com/motor";
		String[] response = Util.httpRequestWithStatus(url, "UTF-8", false);
		if(StringUtils.isNotBlank(response[0])) {
			String vehicleMap = Util.getStringBetween(response[0], "var vehicleMap = ", ";");
			if(StringUtils.isNotBlank(vehicleMap)) {
				JSONObject allObj = (JSONObject) parser.parse(vehicleMap);
				Set<String> brands = allObj.keySet();
				for (String brand : brands) {
					JSONObject brandObj = (JSONObject) allObj.get(brand);
					Set<String> models = brandObj.keySet();
					for (String model : models) {
						JSONObject modelObj = (JSONObject) brandObj.get(model);
						Set<String> years = modelObj.keySet();
						for (String year : years) {
							JSONObject yearObj = (JSONObject) modelObj.get(year);
							String vehicleId = String.valueOf(yearObj.getOrDefault(("ไม่แน่ใจ"), ""));
							if(StringUtils.isNotBlank(vehicleId)) {
								allCombination.add(new String[] {brand, model, year, vehicleId });
							}
						}
					}
				}
				
			}
		}
	}
	
	private static String[] tierList = new String[] {"&tier1=on","&tier2plus=on","&tier3plus=on","&tier3=on"};
	
	private static void runAllCombination() throws Exception {
		if(allCombination != null && allCombination.size() > 0) {
			for (String[] combination : allCombination) {
				int resultCount = 0;
				if(finishCombinationList != null && finishCombinationList.size() > 0) {
					if(finishCombinationList.contains(combination[3])) {
						continue;
					}
				}
				for(String t:tierList) {
					
					String url = ""
							+ "https://gettgo.com/motor/search"
							+ "?manufacturer=" + combination[0].replace(" ", "+")
							+ "&model=" + combination[1].replace(" ", "+")
							+ "&sub_model=ไม่แน่ใจ"
							+ "&year=" + combination[2]
							+ "&plate_province=กรุงเทพมหานคร"
							+ "&vehicle_id=" + combination[3]
							+ "&sort_by=price-asc"
							+"{tier}"
							+ "&no_excess=on"
							+ "&dealer=on"
							+ "&garage=on"
							+ "&insurers[]=15"
							+ "&insurers[]=6"
							+ "&insurers[]=18"
							+ "";
					
					String[] response = httpRequestWithStatus(url.replace("{tier}", t), "UTF-8", false,true);
					if(StringUtils.isNotBlank(response[0])) {
						List<String> cardBoxList = Util.getAllStringBetween(response[0], "<div class=\"card-box\">", "<i class=\"far fa-file-alt\"></i>");
						for (String cardBox : cardBoxList) {
							resultCount++;
							String price = Util.getStringBetween(cardBox, "<p class=\"text-price\">", "฿");
							String dataId = Util.getStringBetween(cardBox, "data-id=\"", "\"");
							String policyType = Util.getStringBetween(cardBox, "data-policy-type=\"", "\"");
							getPlanDetail(dataId, policyType, price, combination[0], combination[1], combination[2]);
						}
					}
				}
				System.out.println(new Date() + " combination : " + combination[0] + " , "  + combination[1] + " , "  + combination[2] + " , "  + combination[3] + " ------ result " + resultCount);
				try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(FILE_PATH + "gettgo-config-finish-combination.txt"),  true));){
					pwr.println(combination[3]);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable, boolean printTrace) {
		URL u = null;
		HttpURLConnection conn = null;
		BufferedReader brd = null;
		InputStreamReader isr = null;
		try {
			u = new URL(Util.encodeURL(url));
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("Cookie",cookie);
				
			int status = conn.getResponseCode();
			if (conn.getResponseCode() == 200) {
				if ("gzip".equals(conn.getContentEncoding())) {
					isr = new InputStreamReader(new GZIPInputStream(
							conn.getInputStream()), charset);
				} else {
					isr = new InputStreamReader(conn.getInputStream(), charset);
				}
			} else if (conn.getResponseCode() == 500) {

				isr = new InputStreamReader(conn.getErrorStream(), charset);
			} else {
				return new String[] { null, String.valueOf(status) };
			}

			StringBuilder rtn = new StringBuilder(50000);
			char[] buffer = new char[1000];
			while(true){
				int rsz = isr.read(buffer, 0, buffer.length);
				if (rsz <= 0) {
					break;
				}
				rtn.append(new String(buffer, 0, rsz));
		    }
			
			
			return new String[] { rtn.toString(), String.valueOf(status) };
		} catch (MalformedURLException e) {
			if(printTrace){
				e.printStackTrace();
			}
		} catch (IOException e2) {
			if(printTrace){
				e2.printStackTrace();
			}
		} finally {
			if (brd != null) {
				try {
					brd.close();
				} catch (IOException e) {
				}
			}
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}
	
	
	private static void moveAllFeed() throws Exception {
		// move file here
		System.out.println(new Date() + " ---------------------------------------------------------");
		System.out.println(new Date() + " Start Move Files");
		try {
			
			String sourcePath = FILE_PATH + "gettgo-loading/";
			String targetPath = FILE_PATH + "gettgo-complete/";
			
			File folder = new File(sourcePath);
			File[] listOfFiles = folder.listFiles();
			if(listOfFiles != null && listOfFiles.length > 0) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						listOfFiles[i].renameTo(new File(targetPath + listOfFiles[i].getName()));
						System.out.println(new Date() + " Move " + listOfFiles[i].getName());
						
						listOfFiles[i].delete();
						System.out.println(new Date() + " Delete " + listOfFiles[i].getName());
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		clearFinishCombinationConfig();
		
		System.out.println(new Date() + " Finish Move Files");
		System.out.println(new Date() + " ---------------------------------------------------------");
		
	}

	private static void clearFinishCombinationConfig() {
		String finishFileName = FILE_PATH + "gettgo-config-finish-combination.txt";
		File finishFile = new File(finishFileName);
		if(finishFile.exists()) {
			finishFile.delete();
			System.out.println(new Date() + " Clear complete combination config file.");
		}
	}
	
	private static void getPlanDetail(String dataId, String policyType, String price, String brand, String model, String year) {
		if(StringUtils.isNotBlank(dataId) && StringUtils.isNotBlank(policyType)){
			String url = ""
					+ "https://gettgo.com/motor/plan_details"
					+ "?id=" + dataId
					+ "&policy_type=" + policyType
					+ "&sale_line_only=false"
					+ "";

			String[] response = httpRequestWithStatus(url, "UTF-8", true,true);
			
			if(!response[1].equals("200")) return;
			
			String insurer      = StringUtils.defaultIfBlank(Util.toPlainTextString(Util.getStringBetween(response[0], "<h2 class=\"gettgo-popup--header-title\">", "</h2>")), "");
			String od           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ทุนประกัน", "</li>"))), "0");
			String oddd         = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ค่าเสียหายส่วนแรก", "</li>"))), "0");
			String repair       = StringUtils.defaultIfBlank(Util.toPlainTextString(Util.getStringBetween(response[0], "ประเภทการซ่อม", "</li>")), "");
			String theft        = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "รถยนต์สูญหาย", "</li>"))), "0");
			String fire         = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ไฟไหม้", "</li>"))), "0");
			String flood        = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "น้ำท่วม", "</li>"))), "0");
			String bi           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ความบาดเจ็บทางร่างกาย หรือเสียชีวิตต่อคน", "</li>"))), "0");
			String bt           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ความบาดเจ็บทางร่างกาย หรือเสียชีวิตต่อครั้ง", "</li>"))), "0");
			String tp           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ทรัพย์สินบุคคลภายนอก ต่อครั้ง", "</li>"))), "0");
			String pa           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "อุบัติเหตุส่วนบุคคล", "</li>"))), "0");
			String me           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ค่ารักษาพยาบาล", "</li>"))), "0");
			String bb           = StringUtils.defaultIfBlank(Util.removeCharNotPrice(Util.toPlainTextString(Util.getStringBetween(response[0], "ประกันตัวผู้ขับขี่", "</li>"))), "0");
			
			mockData(price, insurer, od, oddd, policyType, brand, model, year, repair, fire, theft, flood, bi, bt, tp, pa, bb, me);
			
		}
	}
	
	private static void mockData(String premium, String insurer, String od, String oddd, String type, String brand, String model, String year, String repair, String fire, String theft, String flood, String bi, String bt, String tp, String pa, String bb, String me) {
			
		if(type.contains("0")) {
			type = type.replace("0","+");
		}
		
		String uniqueText = "";
		uniqueText += type;
		uniqueText += insurer;
		uniqueText += brand;
		uniqueText += model;
		uniqueText += year;
		uniqueText += repair;
		uniqueText += od;
		uniqueText += oddd;
		uniqueText = toMD5(uniqueText);
		
		String productName = "";
		productName += "ประกันชั้น " + type;
		productName += " - " + insurer.replace("-", " ");
		productName += " - " + brand.replace("-", " ");
		productName += " - " + model.replace("-", " ");
		productName += " - " + "ไม่ระบุรุ่นย่อย";
		productName += " - ปี " + ((StringUtils.isNotBlank(year))? year : "ไม่ระบุ" );
		productName += " (" + uniqueText.substring(0,10) + ")";
		productName = productName.replace(",", "");
		productName = productName.replace("2 5 ปี", "2 ถึง 5 ปี");
		productName = productName.replace("Used Car SpecialG 3   4", "Used Car SpecialG 3 และ 4");
		productName = productName.replaceAll("\\s+", " ");                                                  
		productName = productName.replace("( ", "(");                                                       
		productName = productName.replace(" )", ")");                                                       
		productName = productName.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");        
		productName = productName.replace("( ถึง ", "(").replace(" ถึง )", ")");   		

		String productDesc = "";
		productDesc += insurer+" ประกันชั้น " + type;;
		productDesc += " - ซ่อม" + ((StringUtils.isNotBlank(repair))? repair : " ไม่ระบุอู่/ห้าง");
		productDesc += " - ทุนประกัน " + ((StringUtils.isBlank(od))? "ไม่ระบุ" : ((od.equals("0"))? "ไม่ระบุ" : od ));
		productDesc += " - ค่าเสียหายส่วนแรก " + ((StringUtils.isBlank(oddd))? "ไม่ระบุ" : ((oddd.equals("0"))? "ไม่ระบุ" : (oddd.equals("-")) ? "ไม่ระบุ":oddd ));
		productDesc += " - คุ้มครองไฟไหม้ " + ((StringUtils.isNotBlank(fire) && !fire.equals("0"))? fire : "ไม่ระบุ" );
		productDesc += " - คุ้มครองโจรกรรม " + ((StringUtils.isNotBlank(theft) && !theft.equals("0"))? theft : "ไม่ระบุ" );
		productDesc += " - คุ้มครองน้ำท่วม " + ((StringUtils.isNotBlank(flood) && !flood.equals("0") && !flood.equals("N"))? flood : "ไม่ระบุ" ); 
		productDesc += " - ชีวิตบุคคลภายนอกต่อคน " + ((StringUtils.isNotBlank(bi) && !bi.equals("0"))? bi : "ไม่ระบุ" );   
		productDesc += " - ชีวิตบุคคลภายนอกต่อครั้ง " + ((StringUtils.isNotBlank(bt) && !bt.equals("0"))? bt : "ไม่ระบุ" );   
		productDesc += " - ทรัพย์สินบุคคลภายนอก " + ((StringUtils.isNotBlank(tp) && !tp.equals("0"))? tp : "ไม่ระบุ" );    
		productDesc += " - อุบัติเหตุส่วนบุคคล " + ((StringUtils.isNotBlank(pa) && !pa.equals("0"))? pa : "ไม่ระบุ" );       
		productDesc += " - ประกันตัวผู้ขับขี่ " + ((StringUtils.isNotBlank(bb) && !bb.equals("0"))? bb : "ไม่ระบุ" );         
		productDesc += " - ค่ารักษาพยาบาล " + ((StringUtils.isNotBlank(me) && !me.equals("0"))? me : "ไม่ระบุ" ); 
		productDesc = productDesc.replaceAll("\\s+", " ");                                               
		productDesc = productDesc.replace("( ", "(");                                                    
		productDesc = productDesc.replace(" )", ")");                                                    
		productDesc = productDesc.replaceAll("\\((\\d{4})*\\s*(-*)\\s*(\\d{4})*\\)", "($1 ถึง $3)");     
		productDesc = productDesc.replace("( ถึง ", "(").replace(" ถึง )", ")");                         
		productDesc = productDesc.replace("ซ่อมซ่อม", "ซ่อม");                         
		productDesc = productDesc.replace(",", "");
		
		String productPrice =  formatPrice.format(Double.parseDouble(Util.removeCharNotPrice(premium)));
		
		String productUrl = "https://gettgo.com/";
		String productUrlForUpdate = uniqueText;
		
		BotProductDataBean pdb = new BotProductDataBean();
		pdb.setName(productName);
		pdb.setPrice(Util.convertPriceStr(productPrice));
		pdb.setDescription(productDesc);
		pdb.setUrl(productUrl);
		pdb.setUrlForUpdate(productUrlForUpdate);
		pdb.setCategoryId(250101);
		pdb.setKeyword("ประกันรถยนต์ ประกันภัยรถยนต์");;
		
		String mockData = mockResult(pdb);
		if(StringUtils.isNotBlank(mockData)) {
			try(PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(FILE_PATH + "gettgo-loading/mockFeed-" + brand + ".xml"),  true));){
				pwr.println(mockData);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
		
	private static String mockResult(BotProductDataBean pdb) {
		StringBuilder result = new StringBuilder();
		
		result.append("<product>");
		if(StringUtils.isNotBlank(pdb.getName())) 			result.append("<name>"+pdb.getName()+"</name>");
		if(StringUtils.isNotBlank(pdb.getDescription())) 	result.append("<desc>"+pdb.getDescription()+"</desc>");
		if(StringUtils.isNotBlank(pdb.getPictureUrl())) 	result.append("<pictureUrl>"+pdb.getPictureUrl()+"</pictureUrl>");
		if(StringUtils.isNotBlank(pdb.getUrl())) 			result.append("<url>"+pdb.getUrl()+"</url>");
		if(StringUtils.isNotBlank(pdb.getUrlForUpdate())) 	result.append("<urlForUpdate>"+pdb.getUrlForUpdate()+"</urlForUpdate>");
		if(pdb.getPrice() != 0) 							result.append("<price>"+pdb.getPrice()+"</price>");
		if(pdb.getBasePrice() != 0) 						result.append("<basePrice>"+pdb.getBasePrice()+"</basePrice>");
		if(pdb.getCategoryId() != 0) 						result.append("<categoryId>"+pdb.getCategoryId()+"</categoryId>");
		if(StringUtils.isNotBlank(pdb.getKeyword())) 		result.append("<keyword>"+pdb.getKeyword()+"</keyword>");
		if(StringUtils.isNotBlank(pdb.getRealProductId()))	result.append("<realProductId>"+pdb.getRealProductId()+"</realProductId>");
		if(StringUtils.isNotBlank(pdb.getUpc()))			result.append("<upc>"+pdb.getUpc()+"</upc>");
		result.append("</product>");
		
		return result.toString();
	}
	
	private static String toMD5(String text){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
}
