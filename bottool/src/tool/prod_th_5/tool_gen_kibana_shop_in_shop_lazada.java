package tool.prod_th_5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Date;
import java.util.zip.GZIPInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

public class tool_gen_kibana_shop_in_shop_lazada {
	
	private static final String OUTPUT_FILE = "/ssd/feed/gen_kibana_lazada.csv";                                
	private static final String[] INPUT_FILE =  new String[] {
			"/ssd/feed/lazada/priceza_lazmall_th_marketing_feed.csv.gz",
			"/ssd/feed/lazada/priceza_main_th_marketing_feed.csv.gz",
		};              
	
//	private static final String INPUT_FILE = "D:\\____WORK\\LazadaFeed\\priceza_main_th_marketing_feed.csv.gz";
//	private static final String OUTPUT_FILE = "D:\\____WORK\\LazadaFeed\\gen_kibana_lazada.csv";
	
	private static int count=0, failCount = 0;
	
	public static void main(String[] args) {
//		download();
		parse();
	}
	
	@SuppressWarnings("resource")
	public static void parse() {

		String tmp = "";
		System.out.println(new Date() + " List lazada : Start");
		
		try (PrintWriter pwr = new PrintWriter(new FileOutputStream(new File(OUTPUT_FILE),  true));){
			for (String inputFile : INPUT_FILE) {
				File file = new File(inputFile);
				InputStream ins = new FileInputStream(file);
				InputStreamReader isr = new InputStreamReader(new GZIPInputStream(ins), "UTF-8");				
				BufferedReader br = new BufferedReader(isr);
				String line = "";
				boolean first = true;
				while((line = br.readLine()) != null) {
					tmp += line;
					boolean success = false;
					try {
						StringReader lineReader = new StringReader(tmp);
						CSVParser record = new CSVParser(lineReader, CSVFormat.EXCEL);
						for (CSVRecord csvRecord : record) {
							if(first){
								first = false;
								success = true;
								continue;
							}
							try{
								success = parseCSV(csvRecord, pwr);
								failCount = 0;
							}catch(Exception e){
								failCount++;
								continue;
							}
							if(failCount>50) {
								failCount = 0;
								tmp = "";
							}
						}
					}catch(Exception e) {
						//append new line before parse;
					}
					
					if(success) tmp = "";
					if(success && count > 0 && count % 100000 == 0) System.out.println(new Date() + " Done lazada : " + count);
				}
			}
			System.out.println(new Date() + " Done lazada : All");
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private static boolean parseCSV(CSVRecord data, PrintWriter pwr) {
		
		String product_name             = (String) StringUtils.defaultIfBlank(data.get(0), "");
		String simple_sku               = (String) StringUtils.defaultIfBlank(data.get(1), "");
		String old_sku_id               = (String) StringUtils.defaultIfBlank(data.get(2), "");
		String deeplink                 = (String) StringUtils.defaultIfBlank(data.get(3), "");
		String brand                    = (String) StringUtils.defaultIfBlank(data.get(4), "");
		String sale_price               = (String) StringUtils.defaultIfBlank(data.get(5), "");
		String discounted_price         = (String) StringUtils.defaultIfBlank(data.get(6), "");
		String discount_percentage      = (String) StringUtils.defaultIfBlank(data.get(7), "");
		String availability             = (String) StringUtils.defaultIfBlank(data.get(8), "");
		String cross_border             = (String) StringUtils.defaultIfBlank(data.get(9), "");
		String picture_url              = (String) StringUtils.defaultIfBlank(data.get(10), "");
		String image_url_2              = (String) StringUtils.defaultIfBlank(data.get(11), "");
		String image_url_3              = (String) StringUtils.defaultIfBlank(data.get(12), "");
		String image_url_4              = (String) StringUtils.defaultIfBlank(data.get(13), "");
		String image_url_5              = (String) StringUtils.defaultIfBlank(data.get(14), "");
		String level_1                  = (String) StringUtils.defaultIfBlank(data.get(15), "");
		String level_2                  = (String) StringUtils.defaultIfBlank(data.get(16), "");
		String level_3                  = (String) StringUtils.defaultIfBlank(data.get(17), "");
		String level_4                  = (String) StringUtils.defaultIfBlank(data.get(18), "");
		String level_5                  = (String) StringUtils.defaultIfBlank(data.get(19), "");
		String is_slash_it              = (String) StringUtils.defaultIfBlank(data.get(20), "");
		String is_hot_deals             = (String) StringUtils.defaultIfBlank(data.get(21), "");
		String is_crazy_flash_sale      = (String) StringUtils.defaultIfBlank(data.get(22), "");
		String seller_name              = (String) StringUtils.defaultIfBlank(data.get(23), "");
		
		String mockCSV = ""
				+ ""  + cutGreaterThan(product_name)
				+ ">" + cutGreaterThan(simple_sku)
				+ ">" + cutGreaterThan(old_sku_id)
				+ ">" + cutGreaterThan(deeplink)
				+ ">" + cutGreaterThan(brand)
				+ ">" + cutGreaterThan(sale_price)
				+ ">" + cutGreaterThan(discounted_price)
				+ ">" + cutGreaterThan(discount_percentage)
				+ ">" + cutGreaterThan(availability)
				+ ">" + cutGreaterThan(cross_border)
				+ ">" + cutGreaterThan(picture_url)
				+ ">" + cutGreaterThan(image_url_2)
				+ ">" + cutGreaterThan(image_url_3)
				+ ">" + cutGreaterThan(image_url_4)
				+ ">" + cutGreaterThan(image_url_5)
				+ ">" + cutGreaterThan(level_1)
				+ ">" + cutGreaterThan(level_2)
				+ ">" + cutGreaterThan(level_3)
				+ ">" + cutGreaterThan(level_4)
				+ ">" + cutGreaterThan(level_5)
				+ ">" + cutGreaterThan(is_slash_it)
				+ ">" + cutGreaterThan(is_hot_deals)
				+ ">" + cutGreaterThan(is_crazy_flash_sale)
				+ ">" + cutGreaterThan(seller_name)
				+ "";
		
		pwr.println(mockCSV);
		count++;
		
		return true;
	}
	
	private static String cutGreaterThan(String input) {
		return input.replace(">", "&gt;").replaceAll("\r\n", "").replaceAll("\\s+", " ");
	}

}
