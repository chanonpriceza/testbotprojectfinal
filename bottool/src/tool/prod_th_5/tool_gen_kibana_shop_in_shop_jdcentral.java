package tool.prod_th_5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import Util.Util;

public class tool_gen_kibana_shop_in_shop_jdcentral {
	
	private static List<String> catList;
	private static final String fileFolder = "/ssd/feed/jdcentral/";
	private static final String fileOutputFull = "/ssd/feed/gen_kibana_jdcentral.csv";
	private static final String fileOutputCat = "/ssd/feed/gen_category_jdcentral.csv";
	private static final String[] feedLinks = new String[] {
			"http://tdc.jd.co.th/getFeedXml.do",
		};
	
	private static int count=0, noCatCount=0;
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		// loadFeed
		for (String feedLink : feedLinks) {
			String fileName = Util.getFileNameURL(feedLink);

			Path path = new File(fileFolder + fileName).toPath();
			try (InputStream in = new URL(feedLink).openStream()) {
				System.out.println("Start Download " + feedLink);
				Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Done Download  " + feedLink);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		catList = new ArrayList<>();
		String tmp = "";
		count = 0;
		
		try(PrintWriter pwrf = new PrintWriter(new FileOutputStream(new File(fileOutputFull),  false));
			PrintWriter pwrc = new PrintWriter(new FileOutputStream(new File(fileOutputCat),  false));){
			
			pwrf.println("extra_deeplink_url_ios>gtin>image_link>color>gender>link>description>extra_deeplink_url_andriod>availability>title>sale_price>condition>product_type1>product_type2>product_type3>size>price>vendor_id>code_of_vendor>mobile_link>id>unit_pricing_measure>adult>brand>google_product_category");
			pwrc.println("g:product_type|g:title|g:link");
			
			for (String feedLink : feedLinks) {
				System.out.println(new Date() + " Start read  " + feedLink);
				
				String fileName = Util.getFileNameURL(feedLink);
				File file = new File(fileFolder + fileName);
				InputStream ins = new FileInputStream(file);
				InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
				BufferedReader br = new BufferedReader(isr);
				String line = "";
				while((line = br.readLine()) != null) {
					tmp += line;
					List<String> products = Util.getAllStringBetween(tmp, "<item>", "</item>");
					if(products != null && products.size() > 0) {
						for (String product : products) {
							parseXML(product, pwrf, pwrc);
							if(count > 0 && count % 100000 == 0)
								System.out.println(new Date() + " DONE " + count);
						}
						tmp = Util.getStringAfterLastIndex(tmp, "</item>", tmp);
					}
				}
				
				System.out.println(new Date() + " count = " + count);
				System.out.println(new Date() + " noCatCount = " + noCatCount);
				
				System.out.println(new Date() + " Finish read " + feedLink);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		// readFeed
	}
	
	// parseFeed
	private static void parseXML(String data, PrintWriter pwrf, PrintWriter pwrc) {
		
//		<item>
//			<g:extra_deeplink_url_ios>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/3146800</g:extra_deeplink_url_ios>
//			<g:gtin/>
//			<g:image_link>https://img.jd.co.th/n0/s340x340_jfs/t16/292/633088575/54840/97e6d914/5cc6a3a9N06ccee81.jpg</g:image_link>
//			<g:color>สีเบจ</g:color>
//			<g:gender>unisex</g:gender>
//			<g:link>https://www.jd.co.th/product/morgan-bags-crossbody-exotic-skin-corona-03_3146800.html</g:link>
//			<g:description>Morgan Bags กระเป๋าสะพายข้าง กระเป๋า Crossbody ดีไซน์ลาย Exotic Skin แมตช์กับผ้าสักหลาดแบบหูรูดขนมจีบ รุ่น Corona 03 สีเบจ</g:description>
//			<g:extra_deeplink_url_andriod>openjdthai://deeplink/category/jump/M_sourceFrom/sx_thai/des/productDetail/skuId/3146800</g:extra_deeplink_url_andriod>
//			<g:availability>in stock</g:availability>
//			<g:title>Morgan Bags กระเป๋าสะพายข้าง กระเป๋า Crossbody ดีไซน์ลาย Exotic Skin แมตช์กับผ้าสักหลาดแบบหูรูดขนมจีบ รุ่น Corona 03</g:title>
//			<g:sale_price>2090 THB</g:sale_price>
//			<g:condition>new</g:condition>
//			<g:product_type>แฟชั่นผู้หญิง &amp;gt; กระเป๋าผู้หญิง &amp;gt; กระเป๋าแฟชั่น</g:product_type>
//			<g:size/>
//			<g:price>2990 THB</g:price>
//			<g:vendor_id>11719</g:vendor_id>
//			<g:code_of_vendor/>
//			<g:mobile_link>https://m.jd.co.th/product/3146800.html</g:mobile_link>
//			<g:id>3146800</g:id>
//			<g:unit_pricing_measure>0.5 kg</g:unit_pricing_measure>
//			<g:adult>no</g:adult>
//			<g:brand>MORGAN</g:brand>
//			<g:google_product_category>166</g:google_product_category>
//		</item>
		
		String extra_deeplink_url_ios 				 = Util.getStringBetween(data, "<g:extra_deeplink_url_ios>", "</g:extra_deeplink_url_ios>").trim();		       
		String gtin 				                 = Util.getStringBetween(data, "<g:gtin>", "</g:gtin>").trim();		       
		String image_link 				             = Util.getStringBetween(data, "<g:image_link>", "</g:image_link>").trim();		       
		String color 				                 = Util.getStringBetween(data, "<g:color>", "</g:color>").trim();		       
		String gender 				                 = Util.getStringBetween(data, "<g:gender>", "</g:gender>").trim();		       
		String link 				                 = Util.getStringBetween(data, "<g:link>", "</g:link>").trim();		       
		String description 				             = Util.getStringBetween(data, "<g:description>", "</g:description>").trim();		       
		String extra_deeplink_url_andriod 			 = Util.getStringBetween(data, "<g:extra_deeplink_url_andriod>", "</g:extra_deeplink_url_andriod>").trim();		       
		String availability 				         = Util.getStringBetween(data, "<g:availability>", "</g:availability>").trim();		       
		String title 				                 = Util.getStringBetween(data, "<g:title>", "</g:title>").trim();		       
		String sale_price 				             = Util.getStringBetween(data, "<g:sale_price>", "</g:sale_price>").trim();		       
		String condition 				             = Util.getStringBetween(data, "<g:condition>", "</g:condition>").trim();		       
		String product_type 				         = Util.getStringBetween(data, "<g:product_type>", "</g:product_type>").trim();		       
		String size 				                 = Util.getStringBetween(data, "<g:size>", "</g:size>").trim();		       
		String price 				                 = Util.getStringBetween(data, "<g:price>", "</g:price>").trim();		       
		String vendor_id 				             = Util.getStringBetween(data, "<g:vendor_id>", "</g:vendor_id>").trim();		       
		String code_of_vendor 				         = Util.getStringBetween(data, "<g:code_of_vendor>", "</g:code_of_vendor>").trim();		       
		String mobile_link 				             = Util.getStringBetween(data, "<g:mobile_link>", "</g:mobile_link>").trim();		       
		String id 				                     = Util.getStringBetween(data, "<g:id>", "</g:id>").trim();		       
		String unit_pricing_measure 				 = Util.getStringBetween(data, "<g:unit_pricing_measure>", "</g:unit_pricing_measure>").trim();		       
		String adult 				                 = Util.getStringBetween(data, "<g:adult>", "</g:adult>").trim();		       
		String brand 				                 = Util.getStringBetween(data, "<g:brand>", "</g:brand>").trim();		       
		String google_product_category 				 = Util.getStringBetween(data, "<g:google_product_category>", "</g:google_product_category>").trim();		       
		
		String product_type1 = "";	                    
		String product_type2 = "";	                
		String product_type3 = "";	
		
		if(product_type.contains("&amp;gt;")) {
			product_type1         = (product_type.split("&amp;gt;").length > 0) ? product_type.split("&amp;gt;")[0] : product_type ;           
			product_type2         = (product_type.split("&amp;gt;").length > 1) ? product_type.split("&amp;gt;")[1] : "" ;                     
			product_type3         = (product_type.split("&amp;gt;").length > 2) ? product_type.split("&amp;gt;")[2] : "" ;                     
		}else if(product_type.contains("&gt;")) {
			product_type1         = (product_type.split("&gt;").length > 0) ? product_type.split("&gt;")[0] : product_type ;           
			product_type2         = (product_type.split("&gt;").length > 1) ? product_type.split("&gt;")[1] : "" ;                     
			product_type3         = (product_type.split("&gt;").length > 2) ? product_type.split("&gt;")[2] : "" ;                     
		}
		
		String mockCSV = ""
				+ ""  + cutGreaterThan(extra_deeplink_url_ios)
				+ ">"  + cutGreaterThan(gtin)
				+ ">"  + cutGreaterThan(image_link)
				+ ">"  + cutGreaterThan(color)
				+ ">"  + cutGreaterThan(gender)
				+ ">"  + cutGreaterThan(link)
				+ ">"  + cutGreaterThan(description)
				+ ">"  + cutGreaterThan(extra_deeplink_url_andriod)
				+ ">"  + cutGreaterThan(availability)
				+ ">"  + cutGreaterThan(title)
				+ ">"  + cutGreaterThan(sale_price)
				+ ">"  + cutGreaterThan(condition)
				+ ">"  + cutGreaterThan(product_type1)
				+ ">"  + cutGreaterThan(product_type2)
				+ ">"  + cutGreaterThan(product_type3)
				+ ">"  + cutGreaterThan(size)
				+ ">"  + cutGreaterThan(price)
				+ ">"  + cutGreaterThan(vendor_id)
				+ ">"  + cutGreaterThan(code_of_vendor)
				+ ">"  + cutGreaterThan(mobile_link)
				+ ">"  + cutGreaterThan(id)
				+ ">"  + cutGreaterThan(unit_pricing_measure)
				+ ">"  + cutGreaterThan(adult)
				+ ">"  + cutGreaterThan(brand)
				+ ">"  + cutGreaterThan(google_product_category)
				+ "";                 
		
		pwrf.println(mockCSV);
		
		if(StringUtils.isBlank(product_type)) {
			noCatCount++;
		}
		
		if(!catList.contains(product_type)) {
			String mockCatCSV = ""
					+ ""  + convertGreaterThan(product_type)
					+ "|" + title
					+ "|" + link
					+ "";
			pwrc.println(mockCatCSV);
			catList.add(product_type);
		}
		
		count++;
	}
	
	private static String cutGreaterThan(String input) {
		return input.replace(">", "&gt;");
	}
	
	private static String convertGreaterThan(String input) {
		input = input.replace("&amp;gt;", ">");
		input = input.replace("&gt;", ">");
		return input;
	}
	
}
