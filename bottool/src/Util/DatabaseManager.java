package Util;

import db.BotMerchantConfigDB;
import db.BotUrlPatternDB;

public class DatabaseManager {
	
	private BotMerchantConfigDB merchantConfigDB;
	private BotUrlPatternDB urlPatternDB;
	
	private static class SingletonHelper {
        private static final DatabaseManager INSTANCE = new DatabaseManager();
    }
	
	public static DatabaseManager getInstance(){
		return SingletonHelper.INSTANCE;
	}
	
	private DatabaseManager() {
		merchantConfigDB = new BotMerchantConfigDB(DatabaseFactory.getConfigDSInstance());
		urlPatternDB = new BotUrlPatternDB(DatabaseFactory.getConfigDSInstance());
	}
	
	public BotMerchantConfigDB getMerchantConfigDB() {
		return merchantConfigDB;
	}

	public BotUrlPatternDB getUrlPatternDB() {
		return urlPatternDB;
	}

}
