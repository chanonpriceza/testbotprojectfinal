package Util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;




public class Util {
	
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
	private static final String regexEmail = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
	private static final Pattern EMOJI_AND_SYMBOL_REGEX_PATTERN = Pattern.compile("([^\\p{L}\\p{N}\\p{IsThai}]|[๛๏])+");
	
	private static final Map<String, Map<Integer, Integer>> languageCharacterMap = createLanguageCharacterMap();
    private static Map<String, Map<Integer, Integer>> createLanguageCharacterMap(){
    	Map<String, Map<Integer, Integer>> maps = new HashMap<>();
    	maps.put("th", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0E00, 0x0E5B);
			}
		});
    	maps.put("eng", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x0041, 0x005A);
				put(0x0061, 0x007A);
				put(0x0030, 0x0039);
			}
		});
		maps.put("viet", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0xc0, 0xc3);
				put(0xc8, 0xca);
				put(0xcc, 0xcd);
				put(0xd2, 0xd5);
				put(0xd9, 0xda);
				put(0xdd, 0xdd);
				put(0xe0, 0xe3);
				put(0xe8, 0xea);
				put(0xec, 0xed);
				put(0xf2, 0xf5);
				put(0xf9, 0xfa);
				put(0xfd, 0xfd);
				put(0x102, 0x103);
				put(0x110, 0x111);
				put(0x128, 0x129);
				put(0x168, 0x169);
				put(0x1a0, 0x1a1);
				put(0x1af, 0x1b0);
				put(0x1ea0, 0x1ef9);
			}
		});
		maps.put("chinese", new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(0x4E00, 0x9FD5);
			}
		});
    	return maps;
    }

    public static boolean checkContainsCharacter(String inputString, String language) {
    	for (char splitChar : inputString.toCharArray()) {
			if(Character.isLetter(splitChar)) {
				Map<Integer, Integer> rangeMap = languageCharacterMap.get(language);
				for (Integer min : rangeMap.keySet()) {
					int max = rangeMap.get(min);
					if (between((int) splitChar, min, max)) {
						return true;
					}
				}
			}
    	}
    	return false;
    }
    
	public static boolean checkContainOnlyCharacter(String inputString , String[] allowList) {
		for (char splitChar : inputString.toCharArray()) {
			if(Character.isLetter(splitChar)) {
				boolean isInAllowList = false;
				for (String allowLanguage : allowList) {
					if(allowLanguage.equals("eng")){
						String normalrizeChar = Normalizer.normalize(String.valueOf(splitChar), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
						splitChar =  normalrizeChar.length()==0?splitChar:normalrizeChar.charAt(0);
					}
					Map<Integer, Integer> rangeMap = languageCharacterMap.get(allowLanguage);
					for (Integer min : rangeMap.keySet()) {
						int max = rangeMap.get(min);
						if (between((int) splitChar, min, max)) {
							isInAllowList = true;
						}
					}
				}
				if(!isInAllowList) {
					return false;
				}
			}else {
				continue;
			}
		}
		return true;
	}
	
	public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
		if ((i >= minValueInclusive && i <= maxValueInclusive))
			return true;
		else
			return false;
	}
	
	public static String displayPrice(Double price){
		String result ="";
		if(price !=null){
			if(price==9999999.00){
				result ="ติดต่อร้านค้า";				
			}else if(price==9999998.00){
				result ="ยังไม่ระบุราคา";				
			}else if(price==9999997.00){
				result ="ประมูลราคาพิเศษ";				
			}else if (price >8000000 && price <9000000){
				String installment = String.valueOf((int)(price-8000000));
				int startComma = installment.length()%3;
				int numberOfComma = Math.round((installment.length()-1)/3);	
				result = "ผ่อน ";
				for(int i=0; i<installment.length(); i++){
					result+=installment.charAt(i); 					 
					 if(numberOfComma >0 && (i-startComma+1)%3==0){
						 result+=",";
						 numberOfComma--;					 
					 }
				}
				String period = String.valueOf((int)Math.round(((price-8000000-((int)(price-8000000)))*100)));
				result += "บาท x"+ period+ "เดือน";
			}else{
				long numberOfX = Math.round(((price*100)-Math.round(price*100))*100);			
				String priceString = String.valueOf(Math.round(price));
				int startComma = priceString.length()%3;
				int numberOfComma = Math.round((priceString.length()-1)/3);				
				if(priceString.length()-1<numberOfX) numberOfX = priceString.length()-1;
				for(int i=0; i<priceString.length(); i++){
					 if(i >= priceString.length()-numberOfX){
						 result+="X";					 
					 }else{
						result+=priceString.charAt(i); 
					 }
					 if(numberOfComma >0 && (i-startComma+1)%3==0){
						 result+=",";
						 numberOfComma--;					 
					 }
				}
				result="฿"+result;
			}
		}		
		return result;
	}

	public static String convert2display(String text) {
		if (text == null) {
			return "";
		}
		text = text.replaceAll("'", "&#39;");
		text = text.replaceAll("\"", "&quot;");
		text = text.replaceAll("<.*>", "");
		text = text.replaceAll("&nbsp;", " ");
		text = text.replaceAll("&nbsp", " ");
		return text;
	}
	
	public static String convert2display2(String text) {
		if (text == null) {
			return "";
		}
		text = text.replaceAll("'", "&#39;");
		text = text.replaceAll("\"", "&quot;");
		text = text.replaceAll("&nbsp;", " ");
		text = text.replaceAll("&nbsp", " ");
		return text;
	}

	public static String removeSpecialCharacter(String input) {

		if (input == null)
			return null;
		String[] removeString = { "'", "%", "\\?" };
		String output = input;
		for (int i = 0; i < removeString.length; i++) {
			output = output.replaceAll(removeString[i], "");
		}
		String[] replaceForSpace = { "\\-", "\\+", "\\/"};
		for (int i = 0; i < replaceForSpace.length; i++) {
			output = output.replaceAll(replaceForSpace[i], " ");
		}
		
		return output;
	}
		
	public static String changeSpaceToHyphen(String input){
		if (input == null)
			return null;	
		String output = removeSpecialCharacter(input);
		return output.replaceAll(" ", "-"); 
	}
	
	public static String formatMerchantLink(String input){
		if (input == null)
			return null;
		return input.replaceAll(" ", "-").toLowerCase(); 
	}

	public static String convertFromDB(String isoString) {
		if (isoString == null) {
			return null;
		}
		try {
			return new String(isoString.getBytes("ISO-8859-1"), "TIS-620");
		} catch (Exception e) {
			char[] charArray = isoString.toCharArray();
			StringBuffer convertedBufferString = new StringBuffer();

			for (int i = 0; i < charArray.length; i++) {
				int eachCharAscii = (int) charArray[i];

				if (161 <= eachCharAscii && eachCharAscii <= 251) {
					eachCharAscii += 3424;
				}

				convertedBufferString.append((char) eachCharAscii);
			}

			return convertedBufferString.toString();
		}
	}

	public static String convertISOtoUTF8(String isoString) {
		
		//TODO skip because set tomcat to use UTF8 instead
		//return isoString;
		
		
		if (isoString == null) {
			return null;
		}
		try {
			return new String(isoString.getBytes("ISO-8859-1"), "UTF-8");
		} catch (Exception e) {
			char[] charArray = isoString.toCharArray();
			StringBuffer convertedBufferString = new StringBuffer();

			for (int i = 0; i < charArray.length; i++) {
				int eachCharAscii = (int) charArray[i];

				if (161 <= eachCharAscii && eachCharAscii <= 251) {
					eachCharAscii += 3424;
				}

				convertedBufferString.append((char) eachCharAscii);
			}

			return convertedBufferString.toString();
		}
		
	}

	public static String convertToUTF8(String anyString) {
		if (anyString == null) {
			return null;
		}
		char[] charArray = anyString.toCharArray();
		StringBuffer convertedBufferString = new StringBuffer();

		for (int i = 0; i < charArray.length; i++) {
			int eachCharAscii = (int) charArray[i];

			if (161 <= eachCharAscii && eachCharAscii <= 251) {
				eachCharAscii += 3424;
			}

			convertedBufferString.append((char) eachCharAscii);
		}

		return convertedBufferString.toString();
	}

	public static boolean containStringWithSpace(String s1, String test) {

		//fixed bug name have two char
		if (test.length() > 2) {
			s1 = s1.replaceAll(" ", "");
		}

		if (s1.toUpperCase().indexOf(test.toUpperCase()) != -1)
			return true;
		else
			return false;
	}

	public static String removeVongLep(String s) {

		if (s == null)
			return null;
		if (s.length() <= 1)
			return s;

		if ((s.charAt(0) == '[' && s.charAt(s.length() - 1) == ']')
				|| (s.charAt(0) == '(' && s.charAt(s.length() - 1) == ')') || //en
				(s.charAt(0) == '(' && s.charAt(s.length() - 1) == ')') || //th
				(s.charAt(0) == '{' && s.charAt(s.length() - 1) == '}')) {
			s = s.substring(1, s.length() - 1);
			s = removeVongLep(s);
		}

		return s;
	}

	public static boolean testPattern(Vector<?> patternName, String testName) {

		for (int i = 0; i < patternName.size(); i++) {
			Pattern p = Pattern.compile((String) patternName.get(i));
			Matcher m = p.matcher(testName.toUpperCase());
			if (m.matches())
				return true;
		}
		return false;
	}

	public static boolean isPrice(String price) {

		if (price == null || price.length() == 0)
			return false;

		if (haveCharNotPrice(price) == true)
			return false;

		return checkPriceFormat(price);
	}

	private static final String PRICE_CHAR = "0123456789.";

	public static boolean haveCharNotPrice(String price) {

		char charArray[] = price.toCharArray();

		for (int i = 0; i < charArray.length; i++) {
			if (PRICE_CHAR.indexOf((int) charArray[i]) == -1) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkPriceFormat(String price) {

		String first = "";
		String last = "";

		if (price.indexOf(".") != -1) {
			first = price.substring(0, price.indexOf("."));
			last = price.substring(price.indexOf(".") + 1, price.length());
		} else
			first = price;

		if (last != null && last.length() != 0
				&& (last.indexOf(".") != -1 || last.indexOf(",") != -1))
			return false;

		if (first == null || first.length() == 0)
			return false;

		if ((first.indexOf(",") + 1) == first.length())
			return false;
		String[] sp = first.split(",");

		if (sp.length == 1)
			return true;

		for (int i = 0; i < sp.length; i++) {
			if (i == 0) {
				if (sp[i].length() == 0)
					return false;
			} else {
				if (sp[i].length() != 3)
					return false;
			}
		}

		return true;
	}

	public static String formatPrice(String price) {

		return str2Digits(price.replaceAll(",", ""));
	}

	public static String str2Digits(String str) {
		double d;
		try {
			d = Double.parseDouble(str);
		} catch (Exception e) {
			return "";
		}
		DecimalFormat df = new DecimalFormat("###,##0.00");
		String result = df.format(d);
		return result;

	}
	
	public static String formatNumber(int number) {		
		DecimalFormat df = new DecimalFormat("###,##0");
		String result = df.format(number);
		return result;
	}
		
	public static String formatNumber(double number) {		
		DecimalFormat df = new DecimalFormat("###,##0.00");
		String result = df.format(number);
		return result;
	}
	
	public static String formatNumberWithNopoint(double number) {		
		DecimalFormat df = new DecimalFormat("###");
		String result = "";
		if(number % 1.0 == 0){
			result = df.format(number);
		}else{
			result = String.valueOf(number);
		}
		return result;
	}
	
    public static String convertUTF8ToISO(String s){

    	if (s == null)
        {
            return null;
        }
    	
    	try {
    		return new String(s.getBytes("UTF-8"), "ISO-8859-1");
    	}catch(Exception e){
    		 char[] charArray = s.toCharArray();
             StringBuffer convertedBufferString = new StringBuffer();

             for (int i = 0; i < charArray.length; i++)
             {
                 int eachCharAscii = (int) charArray[i];
                 
                 if (3585 <= eachCharAscii && eachCharAscii <= 3675)
                 {
                     eachCharAscii -= 3424;
                 }

                 convertedBufferString.append((char) eachCharAscii);
             }

             return convertedBufferString.toString();
    	}      
    }
	
    //TODO fix encode query value
	public static String encodeRedirectUrl(String s) {		
		URL url;
		try {
			url = new URL(s);
			String query = url.getQuery();
			String path = url.getPath();
			System.out.println("query = "+path);	
			System.out.println("path = "+path);	
			path = URLEncoder.encode(path, "UTF-8");
			path = path.replaceAll("%2F", "/");
			
			if(query != null) {
				query = URLEncoder.encode(query, "UTF-8");
				path = path + "?" + query;
			}		
			System.out.println("path = "+path);	
			
			return new URL(url.getProtocol(), url.getHost(), url.getPort(), path).toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return s;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return s;
		}
	}
	
	public static void printChar(String s){
		System.out.println(s);
		char[] charArray = s.toCharArray();
        for (int i = 0; i < charArray.length; i++){
            System.out.print("|"+(int) charArray[i]);
        }
        System.out.println();
    }
	
	public static void writeImageToFile(String path, byte[] imageData) throws IOException {
		
		File file = null;
		FileOutputStream fos = null;
		try {
			file = new File(path);
			
			if(file.exists()) {
				throw new IOException("file already exist");
			}
			
			fos = new FileOutputStream(file);
			fos.write(imageData);
		} finally {
			if(fos != null) { try {	fos.close(); } catch (IOException e) {	} }		
		}
	}	
	
	public static String upperCase(String name) {
		String result ="";
		if(name!= null && name.length()>=1){
			result = (""+name.charAt(0)).toUpperCase();
			if(name.length()>=2) result += name.substring(1);
		}
		return result;
		
	}	

	public static String checkUrlReplaceNoFollow(String str){
		
		if(StringUtils.isBlank(str)){
			return str;
		}
		
		StringBuilder itemText = new StringBuilder();
		str = str.replace("rel='nofollow'", "");
		str = str.replace("rel=\"nofollow\"", "");
		String [] parts = str.split("href=\"");
		
		for( String item : parts ){
			if(itemText.length() == 0){
				itemText.append(item);
			}else{
				if (item.trim().startsWith("http") && (!item.trim().startsWith("http://www.priceza.com") && !item.trim().startsWith("https://www.priceza.com"))) {
					itemText.append("rel=\"nofollow\" href=\""+item);
				}else{
					itemText.append(" href=\""+item);
				}
			}
		}

		return itemText.toString();
	}
	
	public static String removeDuplicateKeyword(String s) {
		
		if(s == null) {
			return s;
		}
		
		HashMap<String, String> wordMap = new HashMap<String, String>();
		StringBuilder rtn = new StringBuilder();
		
		String[] wordArr = s.split(" ");
		for (String word : wordArr) {
			if(word.length() != 0) {
				if(!wordMap.containsKey(word)) {
					wordMap.put(word, "");
					rtn.append(word);
					rtn.append(" ");
				}
			}			
		}
		
		return rtn.toString().trim();
	}
	
	public static String getExceptionList (Exception e) {
		try {
			
			if (e == null) {
				return null;
			}
			
			String msg = e.fillInStackTrace() + "<br>";
			StackTraceElement[] stackTraceList = e.getStackTrace();
			for (StackTraceElement stackTrace : stackTraceList) {
				msg += stackTrace.toString() +"<br>";
			}
			
			return msg;
			
		} catch (Exception exception) {}
		
		return null;
	}
	
	public static int calculateStartPage (int page, int total, int offsetRow) {
		
		if (page < 1 || total < 1 || offsetRow < 1) {
			return 0;
		}
		
		int start = (page - 1) * offsetRow;
		if(start >= total) {
			start = 0;
		}
		
		return start;
	}
	
	public static String addDotDotDot (String str, int offset) {
		String result = str;
		if (StringUtils.isNotBlank(str) && str.length() > offset) {
			result = str.substring(0, offset) +"...";
		}
		return result;
	}
	
	public static boolean isAfterExpireDate (Date expire) {
		if (expire == null) {
			return false;
		}
		
		Date today = new Date();
		boolean result = false;
		if (today.after(expire)) {
			result = true;
		}
		
		return result;
	}
	
	public static int mathRoundToTheHundreds (int number) {
		if (number < 0) {
			return -1;
		}

		int result = -1;
		String numberStr = String.valueOf(number);
		char[] numberBuff = numberStr.toCharArray();
		
		if (numberBuff.length > 0) {
			
			char unitChr = numberBuff[numberBuff.length - 1];
			char tensChr = '0';
			if (numberBuff.length >= 2) {
				tensChr = numberBuff[numberBuff.length - 2];
			}
			
			int tens = Character.getNumericValue(tensChr) * 10;
			int unit = Character.getNumericValue(unitChr);
			int cal = 100 - (tens + unit);
			result = number + cal;
		}
		
		return result;
	}
	
	public static int calPercentDiscount(Double p1, Double p2) {		
		if(p1 == null || p2 == null) {
			return 0;
		}
		if(p1 <= p2 || p1 > 8000000) {
			return 0;
		}
		return (int)Math.ceil( (p1 - p2)* 100 / p1 );
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable) {
		return httpRequestWithStatus(url, charset, redirectEnable, true);
	}
	
	public static String[] httpRequestWithStatus(String url, String charset, boolean redirectEnable, boolean printTrace) {

		URL u = null;
		HttpURLConnection conn = null;
		BufferedReader brd = null;
		InputStreamReader isr = null;
		try {
			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setInstanceFollowRedirects(redirectEnable);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			conn.connect();
			int status = conn.getResponseCode();
			if (conn.getResponseCode() == 200) {
				if ("gzip".equals(conn.getContentEncoding())) {
					isr = new InputStreamReader(new GZIPInputStream(
							conn.getInputStream()), charset);
				} else {
					isr = new InputStreamReader(conn.getInputStream(), charset);
				}
			} else if (conn.getResponseCode() == 500) {

				isr = new InputStreamReader(conn.getErrorStream(), charset);
			} else {
				return new String[] { null, String.valueOf(status) };
			}

			StringBuilder rtn = new StringBuilder(50000);
			char[] buffer = new char[1000];
			while(true){
				int rsz = isr.read(buffer, 0, buffer.length);
				if (rsz <= 0) {
					break;
				}
				rtn.append(new String(buffer, 0, rsz));
		    }
			
			
			return new String[] { rtn.toString(), String.valueOf(status) };
		} catch (MalformedURLException e) {
			if(printTrace){
				e.printStackTrace();
			}
		} catch (IOException e2) {
			if(printTrace){
				e2.printStackTrace();
			}
		} finally {
			if (brd != null) {
				try {
					brd.close();
				} catch (IOException e) {
				}
			}
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}
	
	public static String[] httpRequestWithStatusIgnoreCookies(String url, String charset, boolean redirectEnable) {
		return httpRequestWithStatusIgnoreCookies(url, charset, redirectEnable, true);
	}
	
	public static String[] httpRequestWithStatusIgnoreCookies(String url, String charset, boolean redirectEnable, boolean printTrace) {
    	
    	BufferedReader brd = null;
    	InputStreamReader isr = null;
    	CloseableHttpResponse response1 = null;
    	HttpEntity entity1 = null;
    	try {
			
			CloseableHttpClient httpclient = HttpClients.createDefault();	    	
	      	HttpGet httpGet = new HttpGet(url);
			  
			RequestConfig localConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
			httpGet.setConfig(localConfig);	  	
			//httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
			httpGet.addHeader("User-Agent", USER_AGENT);
				
			response1 = httpclient.execute(httpGet);						
			entity1 = response1.getEntity();
		    		   
		    int status = response1.getStatusLine().getStatusCode();
			if(status == 200) {
				isr = new InputStreamReader(entity1.getContent(), charset);
			} else {
				return new String[]{null, String.valueOf(status)};	
			}
			
			brd = new BufferedReader(isr);
			StringBuilder rtn = new StringBuilder(5000);
			String line = "";
			while ((line = brd.readLine()) != null) {
				rtn.append(line);
			}
			
			return new String[]{rtn.toString(), String.valueOf(status)};		    
		} catch (Exception e) {
			if(printTrace){
				e.printStackTrace();
			}
		} finally {		
			if(entity1 != null) {
				try {
					EntityUtils.consume(entity1);
				} catch (IOException e) {
					if(printTrace){
						e.printStackTrace();
					}
				}
			}
			if(response1 != null) {
				try {
					response1.close();
				} catch (IOException e) {
					if(printTrace){
						e.printStackTrace();
					}
				}
			}
			if(brd != null) {
				try { brd.close(); } catch (IOException e) { }
			}
			if(isr != null) {
				try { isr.close(); } catch (IOException e) { }
			}
		}  
    	return null;   
    }
	
    public static String httpRequest(String url, String charset, boolean redirectEnable) {
    	
    	  URL u = null;
    	  HttpURLConnection conn = null;
    	  BufferedReader brd = null;
    	  InputStreamReader isr = null;
  		try {			
  			u = new URL(url);
  			conn = (HttpURLConnection) u.openConnection();
  			conn.setInstanceFollowRedirects(redirectEnable);			
  			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
  			
  			conn.connect();
  			if(conn.getResponseCode() == 200) {
  				if("gzip".equals(conn.getContentEncoding())) {
    					isr = new InputStreamReader(new GZIPInputStream(conn.getInputStream()), charset);
    				} else {
    					isr = new InputStreamReader(conn.getInputStream(), charset);
    				} 
  			} else if(conn.getResponseCode() == 500) {	
  				
  				isr = new InputStreamReader(conn.getErrorStream(), charset);
  			} else {
  				return null;			
  			}
  				
  			
  			brd = new BufferedReader(isr);
  			StringBuilder rtn = new StringBuilder(5000);
  			String line = "";
  			while ((line = brd.readLine()) != null) {
  				rtn.append(line);
  			}
  			return rtn.toString();
  		} catch (MalformedURLException e) {
  			e.printStackTrace();
  		} catch (IOException e2) {
  			e2.printStackTrace();
  		} finally {			
  			if(brd != null) {
  				try { brd.close(); } catch (IOException e) {	}
  			}
  			if(isr != null) {
  				try { isr.close(); } catch (IOException e) {	}
  			}
  			if(conn != null) {
  				conn.disconnect();
  			}
  		}
  		return null;    	
      }
	
	public static String getStringBetween(String target, String begin, String end) {

		if (target == null) {
			return "";
		}

		int index = target.indexOf(begin);
		if (index == -1) {
			return "";
		}
		target = target.substring(index + begin.length());

		index = target.indexOf(end);
		if (index == -1) {
			return "";
		}
		target = target.substring(0, index);

		return target;
	}

	public static List<String> getAllStringBetween(String target, String begin, String end) {
		if ((target == null) || (begin.length() == 0) || (end.length() == 0)) {
			return null;
		}
		List<String> rtn = new ArrayList<String>();
		do {
			int index = target.indexOf(begin);
			if (index == -1) {
				return rtn;
			}
			target = target.substring(index + begin.length());
			index = target.indexOf(end);
			if (index == -1) {
				return rtn;
			}
			String found = target.substring(0, index);
			rtn.add(found);

			target = target.substring(index + end.length());
		} while (target.trim().length() != 0);
		return rtn;
	}

	public static String getStringBefore(String target, String begin, String defaultValue){
	    if (target == null) {
	    	return "";
	    }
	    int index = target.indexOf(begin);
	    if (index == -1) {
	    	return defaultValue;
	    }
	    target = target.substring(0, index);
	    return target;
	}
	
	public static String getStringAfter(String target, String begin, String defaultValue){
		if (target == null) {
			return "";
		}
		int index = target.indexOf(begin);
		if (index == -1) {
			return defaultValue;
		}
		target = target.substring(index + begin.length());
		return target;
	}
	
	public static String getStringAfterLastIndex(String target, String begin, String defaultValue){
        if(target == null) {
        	return "";
        }
        
        int index = target.lastIndexOf(begin);
        if(index == -1) {
        	return defaultValue;        
        }
        target = target.substring(index + begin.length());
                       
        return target;
    }
	
	public static String toPlainTextString(String html) {
		return html.replaceAll("\\<.*?>", "");
	}
	
	public static String convertSpecAttributeName(String attrName){
		String convertAttrName="";
		convertAttrName = attrName.replaceAll("[ &%|._-]","");
		if("Features".equals(attrName)){ 
			convertAttrName = "HDR";
	    }else if("Misc".equals(attrName)){ 
	    	convertAttrName = "Fast-Charging";
	    }
		return convertAttrName;
		
	}
	
	public static String convertToDisplayPictureSpec(String attrPicName){
		String convertAttrPicName = "";
		if(attrPicName.contains("-etc")){
			convertAttrPicName = "Etc";
		}else if(attrPicName.contains(".jpg")){
			convertAttrPicName = attrPicName.replace(".jpg", "");	
		}else if(attrPicName.contains(".png")){
			convertAttrPicName = attrPicName.replace(".png", "");
		}
		convertAttrPicName = WordUtils.capitalize(convertAttrPicName);
		return convertAttrPicName;
	}
	
	public static String getSourceName (int source) {
		String name = "Unknown";
		
		if (source == 0) {
			name = "UnKnown/App";
		} else if (source == 1) {
			name = "Desktop";
		} else if (source == 2) {
			name = "Mobile";
		} else if (source == 3) {
			name = "App";
		}
		
		return name;
	}
	
	public static boolean invalidEmail(String email) {
		if(StringUtils.isBlank(email)){  
			return false;
		}
		return email.matches(regexEmail);
	}
	
	public static String convertSpecText (String data) {
		if (data.contains("-")) {
			data = data.replace("-", " -");
		}
		if (data.contains("\"")) {
			data = data.replace("\"", "'");
		}
		if (data.contains("\n")|| data.contains("\r\n")) {
			 if(data.contains("\r\n")){
				data = data.replace("\r\n", "/n");
			 }else if(data.contains("\n")){
				data = data.replace("\n", "/n");
			 }
		}
		return data;
	}
	
	public static Map<String, String> splitQuery(URI url) throws UnsupportedEncodingException {
	    Map<String, String> query_pairs = new LinkedHashMap<String, String>();
	    String query = url.getQuery();
		    if(query !=null ){
			    String[] pairs = query.split("&");
			    for (String pair : pairs) {
			    	 if(pair.contains("=")){
			    		int idx = pair.indexOf("=");
			        	query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
			        }
			    }
	    }
	    return query_pairs;
	}
	
	public static List<String> splitStringToList(String str,String regex,Boolean trim,Boolean notBlank){

		List<String> list = new ArrayList<String>();
		if(StringUtils.isBlank(str)){
			return list;
		}
		
    	int pos = 0;
    	int end  = 0;
    	int regexLength = regex.length();
    	try{
		  while ((end = str.indexOf(regex, pos)) >= 0) {
			  String strTmp = str.substring(pos, end);
			  if(trim){
				  strTmp = strTmp.trim();
			  }
			  if(notBlank && StringUtils.isNotBlank(strTmp)){
				  list.add(strTmp);
			  }
		        pos = end + regexLength;
		  }
		  if(pos != str.length()){
			  String strTmp = str.substring(pos, str.length());
			  if(trim){
				  strTmp = strTmp.trim();
			  }
			  if(notBlank && StringUtils.isNotBlank(strTmp)){
				  list.add(strTmp);
			  }
		  }
    	}catch (Exception e) {
    		e.printStackTrace();
	    	list.clear();   		
		}finally{
			pos  = 0;
	    	end  = 0;	    	
		}
		return list;
    }
	
	@SuppressWarnings("unchecked")
	public static String loadFile(String filePath) throws IOException {
		FileInputStream templateFile = new FileInputStream(filePath);
		try {
			List<String> templates = IOUtils.readLines(templateFile, "UTF-8");
			String message = StringUtils.join(templates, System.getProperty("line.separator"));
			
			return message;
		} finally {
			IOUtils.closeQuietly(templateFile);
		}
	}
	
	public static boolean validateEmail(String email){
		return validateEmail(email, false);
	}
	
	public static boolean validateEmail(String email, boolean isLog){
		if(email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$")){
			return true;
		}
		if(isLog){
			System.out.println("Cannot Send Email To : " + email);
		}
		return false;
	}
	
	public static int stringToInt(String s, int defaultValue) {
		if(s == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(s.trim());
		} catch (NumberFormatException e) {
			return defaultValue;			
		}
	}
	
	public static String limitString(String s, int length) {
		
		if(s == null) {
			return null;
		}
		
		if(s.length() > length) {
			return s.substring(0, length);
		}
		
		return s;		
	}
	
	public static String limitLength(String s, int length, String suffix) {
		
		if(s == null || length < 0) {
			return "";
		}
		
		if(s.length() > length) {
			return s.substring(0, length) + suffix;
		} else {
			return s;
		}
	}
	
	public static String removeCharNotPrice(String price){

        if(price == null || price.equals("")) {
        	return price;
        }

        StringBuilder rtn = new StringBuilder();
        char charArray[] = price.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            if(PRICE_CHAR.indexOf((int)charArray[i]) != -1){
                rtn.append(charArray[i]);
            }
        }
        return removeSuffixCharNotPrice(rtn.toString());
    }
	
	public static String removeSuffixCharNotPrice(String price){
	    
    	if(price == null || price.equals("")) return price;
    	
    	if(( price.charAt(price.length()-1) == ',' ) || 
    		( price.charAt(price.length()-1) == '.' )){
    		return removeSuffixCharNotPrice(price.substring(0, price.length()-1));
    	}
    	return price;    
    }
	
	public static String removeSpace(String s){

        if(s == null) return null;
        s = s.replaceAll("\\s+", " ").trim();
        return s;
    }
	
	public static double convertPriceStr(String price) {    	    	
    	try {
			double rtn = Double.parseDouble(price);
			return rtn;
		} catch (Exception e) {
			return 0;
		}    	
    }
	
	public static String convertISOToUTF(String isoString){
        if (isoString == null){
            return null;
        }
        char[] charArray = isoString.toCharArray();
        StringBuilder convertedBufferString = new StringBuilder();

		for (int i = 0; i < charArray.length; i++) {
			int eachCharAscii = (int) charArray[i];

			if (161 <= eachCharAscii && eachCharAscii <= 251) {
				eachCharAscii += 3424;
			}

			convertedBufferString.append((char) eachCharAscii);
		}

		return convertedBufferString.toString();
    }
	
	public static String encodeURL(String url) {
    	if(url == null) {
    		return null;
    	}
    	    	
		StringBuilder tmp = new StringBuilder();		
		char cc[] = url.toCharArray();
	    for (int i = 0; i < cc.length; i++) {
	        int eachCharAscii = (int) cc[i];
	        if (3585 <= eachCharAscii && eachCharAscii <= 3675) {	        	
	        	try {
					tmp.append(URLEncoder.encode(String.valueOf(cc[i]), "UTF-8"));
				} catch (UnsupportedEncodingException e) {					
				}	        	
            } else if(eachCharAscii == 32) { // space
            	tmp.append("%20");  	
            } else {
            	tmp.append(cc[i]);            	
            }
	    }	    
		return tmp.toString();    	
    }
	
	public static String removeNonChar(String s){
		if (s == null || s.isEmpty()){
			return s;
		}
		char[] charArray = s.toCharArray();
		StringBuilder convertedBufferString = new StringBuilder();
		for (int i = 0; i < charArray.length; i++){
			int eachCharAscii = (int) charArray[i];
			if(eachCharAscii < 32 || eachCharAscii == 127 || charArray[i] == ' ') {
				convertedBufferString.append(" ");
				continue;
			}
			convertedBufferString.append((char) eachCharAscii);
		}
		return convertedBufferString.toString().trim();
	}
	
	public static String removeLongSpace(String s){
        if(s == null) {
            return null;
        } else {
            return s.replaceAll("\\s+", " ").trim();
        }
    }
	
	public static String formatSlugName(String input){
		if (input == null){
			return null;
		} 
		input = input.replaceAll("[.*&,]+", " ").trim();
		input = removeLongSpace(input);
		return input.replace(' ', '-').toLowerCase();
	}
	
	public static boolean isContainBracket(String str) {
		if (StringUtils.isBlank(str)) return false;
		
		if(str.contains("(") && str.contains(")")) return true;
		return false;
	}
	
	public static String removeBracketAndQuote(String str) {
		if (StringUtils.isBlank(str)) return null;
		
		String res = str.replace("(", "").replace(")", "").replace("'", ""); 
		return res;
	}
	
	public static void consumeInputStreamQuietly(InputStream in) {
		if (in == null) return;
		
		byte[] buffer = new byte[1024];
		try {
			while (in.read(buffer) != -1) {}
		} catch (IOException ex) {
		} finally {
			if(in != null){
				try { in.close(); } catch (IOException e) {}
			}
		}
	}

	public static void optimizedImage(String fileName) {

		String getFileType = FilenameUtils.getExtension(fileName);
		if (!("jpg").equals(getFileType)) {
			return;
		}
		ImageOutputStream ios = null;
		OutputStream os = null;
		ImageWriter writer = null;
		try {

			File input = new File(fileName);
			BufferedImage image = ImageIO.read(input);

			File compressedImageFile = new File(fileName);
			os = new FileOutputStream(compressedImageFile);

			Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
			writer = (ImageWriter) writers.next();

			ios = ImageIO.createImageOutputStream(os);
			writer.setOutput(ios);

			ImageWriteParam param = writer.getDefaultWriteParam();

			if (param.canWriteCompressed()) {
				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				param.setCompressionQuality(0.85f);
				writer.write(null, new IIOImage(image, null, null), param);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (ios != null) {
					ios.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (writer != null) {
				writer.dispose();
			}
		}
	}

	public static String getFileNameURL(String url) {
		try {
			return FilenameUtils.getName(new URL(url).getPath());
		} catch (MalformedURLException e) {
			return "";
		}
	}

	public static String removeNonUnicodeBMP(String str) {
		int length = str.length();
		int offset = 0;
		StringBuffer bmpString = new StringBuffer();

		while (!Thread.currentThread().isInterrupted() && offset < length) {
			int codepoint = str.codePointAt(offset);

			if (codepoint < 65536) {
				bmpString.append(str.charAt(offset));
			}

			offset += Character.charCount(codepoint);
		}
		return bmpString.toString();
	}
	
	public static String removeEmoji(String text) {
    	if (text == null)
    		return null;
    	return EMOJI_AND_SYMBOL_REGEX_PATTERN.matcher(text).replaceAll(" ");
    }
	
	private final static CredentialsProvider CREDENTIAL_PROXY = initCredentialProxy();
	
	public static void enableUseProxy() {
		USE_PROXY = true;
		RequestConfig httpRequestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).build();
		CLIENT_BUILDER = HttpClients.custom().setDefaultRequestConfig(httpRequestConfig).setProxy(PROXY_HOST).setDefaultCredentialsProvider(CREDENTIAL_PROXY);
	}
	
	public static String LOCALE;
	public static boolean USE_PROXY;
	private final static String PROXY_DOMAIN = "148.251.73.161";
	private final static int PROXY_PORT = 60000;
	private final static String PROXY_USER = "priceza";
	private final static String PROXY_PASSWORD = "49sO7t2jgQ";
	private final static HttpHost PROXY_HOST = new HttpHost(PROXY_DOMAIN, PROXY_PORT);
	private static HttpClientBuilder CLIENT_BUILDER = HttpClients.custom();
	private static RequestConfig HTTP_LOCAL_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000).setSocketTimeout(10000).setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
	
	
	
	public static String[] httpRequestWithProxy(String url, String charset, boolean redirectEnable) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(HTTP_LOCAL_CONFIG);
		httpGet.addHeader("User-Agent", USER_AGENT);
		HttpEntity entity = null;
    	try(CloseableHttpClient httpclient = CLIENT_BUILDER.build();
    		CloseableHttpResponse response = httpclient.execute(httpGet)){
    		
    		int status = response.getStatusLine().getStatusCode();
    		entity = response.getEntity();
    		if(status == HttpURLConnection.HTTP_OK) {
    			try(InputStream is = entity.getContent();
    				InputStreamReader isr = new InputStreamReader(is, charset);
	    			BufferedReader brd = new BufferedReader(isr)) {
    				StringBuilder rtn = new StringBuilder();
    				String line = null;
    				while((line = brd.readLine()) != null)
    					rtn.append(line);
    				return new String[]{rtn.toString(), String.valueOf(status)};
    			}
    		}else{
				return new String[]{null, String.valueOf(status)};
			}
    		
    	}catch (Exception e) {
			e.printStackTrace();
    	} finally {
    		if(httpGet != null)
				httpGet.releaseConnection();
    		EntityUtils.consumeQuietly(entity);
    	}
    	return null;
    }
	
	private static CredentialsProvider initCredentialProxy() {
		CredentialsProvider credentailProxy = new BasicCredentialsProvider();
		credentailProxy.setCredentials(new AuthScope(PROXY_DOMAIN, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
		return credentailProxy;
	}
	
}
