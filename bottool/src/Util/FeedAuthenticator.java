package Util;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class FeedAuthenticator extends Authenticator {
	
	private String username, password;

	public FeedAuthenticator(String user, String pass) {
		username = user;
		password = pass;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password.toCharArray());
	}
}
