package Util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

import org.apache.commons.lang3.StringUtils;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.byteSources.ByteSource;
import org.apache.sanselan.common.byteSources.ByteSourceArray;
import org.apache.sanselan.formats.jpeg.JpegImageParser;
import org.apache.sanselan.formats.jpeg.segments.UnknownSegment;

public class DefaultImageParser {
	public static final int DEFAULT_SMALL_WIDTH = 130;
	public static final int DEFAULT_SMALL_HEIGHT = 130;
	public static final int DEFAULT_LARGE_WIDTH = 200;
	public static final int DEFAULT_LARGE_HEIGHT = 200;
	public static final int DEFAULT_VERY_LARGE_WIDTH = 400;
	public static final int DEFAULT_VERY_LARGE_HEIGHT = 400;
	private final static int DEFAULT_TIMEOUT = 30000;

	public static void main(String[] args) {

		byte[] result = null;

		DefaultImageParser d = new DefaultImageParser();
		String[] links = new String[] {
				"https://d.line-scdn.net/lcp-prod-photo/20200205_11/1580889601952Ud14z_PNG/9NIQT8VG84JC8T80MSO6QO97T58QUS.PNG",
				"https://d.lnwfile.com/lig8bl.jpg",
				"https://d.line-scdn.net/lcp-prod-photo/20191013_237/1570907073353OnRIV_JPEG/5KUZEKC2GV5MQU0W4H5CIK9APOA5Z5.jpg",
				"https://nocnoc.com/assets-static/assets/portal-assets/804/product/images/MGS-MODERN-DG-BRICK/mgs-modern-dg-brick_001.png",
				"https://cf.shopee.co.th/file/fa5302c946b568797ac0b135540d4564",
				""
		};

		for (String link : links) {
			if(StringUtils.isBlank(link)) continue;
			result = d.parse(link, DEFAULT_LARGE_WIDTH, DEFAULT_LARGE_HEIGHT); System.out.println((result==null)?"no result":result.length);
		}
	}
	
	public byte[] parse(String imageUrl, int width, int height) {
		return parse(imageUrl, width, height, 0);
	}
	
	public byte[] parse(String imageUrl, int width, int height, long limitLength) {
		URL url = null;
		HttpURLConnection conn = null;
		
		try {
			url = new URL(imageUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5");
			conn.setConnectTimeout(DEFAULT_TIMEOUT);
			conn.setReadTimeout(DEFAULT_TIMEOUT);

			String contentLength = conn.getHeaderField("Content-Length");
			int length = Util.stringToInt(contentLength,0);
			if(limitLength > 0 && length > limitLength)
				return null;
			
			try (InputStream inputStream = conn.getInputStream();
				ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
				
				int read = 0;
				byte[] bytes = new byte[20480];
				while ((read = inputStream.read(bytes)) != -1)
					byteOutputStream.write(bytes, 0, read);

				try (ImageInputStream imageStream = ImageIO.createImageInputStream(new ByteArrayInputStream(byteOutputStream.toByteArray()))) {
					Iterator<ImageReader> iter = ImageIO.getImageReaders(imageStream);
					BufferedImage image = null;
					String imageFormat = null;
					
					if (iter.hasNext()) {
						ImageReader reader = null;
						try {
							reader = iter.next();
							reader.setInput(imageStream);
							image = reader.read(0);
							imageFormat = reader.getFormatName();
						} catch (IIOException e) {
							try {
								WritableRaster raster = (WritableRaster) reader.readRaster(0, null);
								ICC_Profile profile = Sanselan.getICCProfile(byteOutputStream.toByteArray());

								checkAdobeMarker(byteOutputStream.toByteArray(), raster);

								image = convertCmykToRgb(raster, profile);
							} catch (ImageReadException e1) {
								System.out.print("Can't get ICC Profile : ");
							}
						} finally {
							if (reader!=null) {
								reader.dispose();
							}
						}
					}
					
					byte[] output;
					if (image != null) {
						byteOutputStream.reset();
						try {
							switch(imageFormat.toLowerCase()) {
								case "jpeg"	: ImageIO.write(image, "jpg", byteOutputStream); break;
								case "jpg"	: ImageIO.write(image, "jpg", byteOutputStream); break;
								case "png"	: ImageIO.write(image, "png", byteOutputStream); break;
								default		: ImageIO.write(image, "jpg", byteOutputStream); break;
							}
						} catch(Exception e) {
							// nothing
						}
					}
					output = parse(byteOutputStream.toByteArray(), width, height, imageFormat);
						
					return output;
				}
			} catch(FileNotFoundException e) {
				System.out.print("FileNotFoundException : ");
			} 
		} catch (Exception e) {
			Util.consumeInputStreamQuietly(conn.getErrorStream());
			e.printStackTrace();
		} finally {
			if(conn != null)
	 			conn.disconnect();
		}

		return null;
	}

	public byte[] parse(byte[] imageByte, int width, int height, String imageFormat) {
		
		try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {

			ImageIcon ii = new ImageIcon(imageByte);
			Image i = ii.getImage();
			Image resizedImage = null;

			int iWidth = i.getWidth(null);
			int iHeight = i.getHeight(null);

			if (iWidth == -1 && iHeight == -1) {
				return null;
			}

			double xPos = (width - 1 * iWidth) / 2;
			double yPos = (height - 1 * iHeight) / 2;
			Image temp = null;

			if (iHeight > height || iWidth > width) {

				if (iWidth > iHeight) {
					resizedImage = i.getScaledInstance(width, (height * iHeight) / iWidth, Image.SCALE_SMOOTH);
				} else {
					resizedImage = i.getScaledInstance((width * iWidth) / iHeight, height, Image.SCALE_SMOOTH);
				}

				double xScale = (double) width / iWidth;
				double yScale = (double) height / iHeight;
				double scale = Math.min(xScale, yScale);

				// Calculate the center position of the panel -- with scale
				xPos = (width - scale * iWidth) / 2;
				yPos = (height - scale * iHeight) / 2;
				
				// This code ensures that all the pixels in the image are loaded.
				temp = new ImageIcon(resizedImage).getImage();

			} else {
				temp = i;
			}

			// Create the buffered image.
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

			// Copy image to buffered image.
			Graphics g = bufferedImage.createGraphics();

			// Clear background and paint the image.
			g.setColor(Color.white);
			g.fillRect(0, 0, width, height);
			g.drawImage(temp, (int) xPos, (int) yPos, null);
			g.dispose();

			try {
				switch(imageFormat.toLowerCase()) {
					case "jpeg"	: ImageIO.write(bufferedImage, "jpg", outStream); break;
					case "jpg"	: ImageIO.write(bufferedImage, "jpg", outStream); break;
					case "png"	: ImageIO.write(bufferedImage, "png", outStream); break;
					default		: ImageIO.write(bufferedImage, "jpg", outStream); break;
				}
			} catch(Exception e) {
				// nothing
			}
			
			outStream.flush();
			byte[] imageInByte = outStream.toByteArray();
			outStream.close();
			
			return imageInByte;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void checkAdobeMarker(byte[] imageByte, WritableRaster raster) {
		JpegImageParser imageParser = new JpegImageParser();
		ByteSource byteSource = new ByteSourceArray(imageByte);
		
		@SuppressWarnings("rawtypes")
		ArrayList segments = null;
		try {
			segments = imageParser.readSegments(byteSource, new int[] { 0xffee }, true);
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		}
		if (segments != null && segments.size() >= 1) {
			UnknownSegment app14Segment = (UnknownSegment) segments.get(0);
			byte[] segmentByte = app14Segment.bytes;
			if (segmentByte.length >= 12 && segmentByte[0] == 'A' && segmentByte[1] == 'd' && segmentByte[2] == 'o' && segmentByte[3] == 'b' && segmentByte[4] == 'e') {
				if ((app14Segment.bytes[11] & 0xff) == 2) {
					convertYcckToCmyk(raster);
				}
				convertInvertedColors(raster);
			}
		}
	}

	private void convertYcckToCmyk(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);

			for (int x = 0; x < stride; x += 4) {
				int y = pixelRow[x];
				int cb = pixelRow[x + 1];
				int cr = pixelRow[x + 2];

				int c = (int) (y + 1.402 * cr - 178.956);
				int m = (int) (y - 0.34414 * cb - 0.71414 * cr + 135.95984);
				y = (int) (y + 1.772 * cb - 226.316);

				if (c < 0) {
					c = 0;
				} else if (c > 255) {
					c = 255;
				}
					
				if (m < 0) {
					m = 0;
				} else if (m > 255) {
					m = 255;
				}
					
				if (y < 0) {
					y = 0;
				} else if (y > 255) {
					y = 255;
				}

				pixelRow[x] = 255 - c;
				pixelRow[x + 1] = 255 - m;
				pixelRow[x + 2] = 255 - y;
			}

			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private void convertInvertedColors(WritableRaster raster) {
		int height = raster.getHeight();
		int width = raster.getWidth();
		int stride = width * 4;
		int[] pixelRow = new int[stride];
		for (int h = 0; h < height; h++) {
			raster.getPixels(0, h, width, 1, pixelRow);
			for (int x = 0; x < stride; x++) {
				pixelRow[x] = 255 - pixelRow[x];
			}
			raster.setPixels(0, h, width, 1, pixelRow);
		}
	}

	private BufferedImage convertCmykToRgb(Raster srcRaster, ICC_Profile profile) throws IOException {
		if (profile == null) {
			profile = ICC_Profile.getInstance(DefaultImageParser.class.getResourceAsStream("/ISOcoated_v2_300_eci.icc"));
		}

		if (profile.getProfileClass() != ICC_Profile.CLASS_DISPLAY) {
			byte[] profileData = profile.getData();

			if (profileData[ICC_Profile.icHdrRenderingIntent] == ICC_Profile.icPerceptual) {
				intToBigEndian(ICC_Profile.icSigDisplayClass, profileData, ICC_Profile.icHdrDeviceClass);
				
				profile = ICC_Profile.getInstance(profileData);
			}
		}

		BufferedImage resultImage = new BufferedImage(srcRaster.getWidth(), srcRaster.getHeight(), BufferedImage.TYPE_INT_RGB);
		WritableRaster resultRaster =  resultImage.getRaster();

		ICC_ColorSpace cmykCS = new ICC_ColorSpace(profile);
		ColorSpace rgbCS = resultImage.getColorModel().getColorSpace();

		ColorConvertOp cmykToRgb = new ColorConvertOp(cmykCS, rgbCS, null);
		cmykToRgb.filter(srcRaster, resultRaster);

		return resultImage;
	}

	private void intToBigEndian(int value, byte[] profileData, int index) {
		profileData[index] = (byte) (value >> 24);
		profileData[index + 1] = (byte) (value >> 16);
		profileData[index + 2] = (byte) (value >> 8);
		profileData[index + 3] = (byte) (value);
	}
}
