package bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class WebProductDataBean  {
	
	public static final int STATUS_DELETE = 4;
	
	//DB fields
	private int id;
    private String name;
    private int merchant_id;
    @JsonIgnore private Date merchant_date; 
    private String description;
    private Double price;
    private Double previous_price;
    private String image;
    private String picture_url;
    private String url;
    private int category;
    private int subcategory; 
    @JsonIgnore private Date update_date;
    @JsonIgnore private Date create_date; 
    @JsonIgnore private String search;
    private int status;
    private int parent_product_id;
    private int total_child;
    @JsonIgnore private int random;
    @JsonIgnore private String group_merchant_id;
    private String brand;
    private String sub_brand; 
    @JsonIgnore private int related;  
    @JsonIgnore private String real_product_id;
    @JsonIgnore private int like_this_week;
    @JsonIgnore private int like_all_time;
    @JsonIgnore private int view_all_time;
    @JsonIgnore private int last_week_rank_like;
    @JsonIgnore private int last_week_rank_all_time_like;
    @JsonIgnore private int last_week_rank_all_time_view;
    @JsonIgnore private String product_keyword;
    @JsonIgnore private int popularity;
    @JsonIgnore private int sort_score;
    @JsonIgnore private String top_merchant;
    @JsonIgnore private String coupon_id_list;
    @JsonIgnore private int update_status;
    @JsonIgnore private int total;
    
    @JsonIgnore private String categoryName;
    @JsonIgnore private String subCategoryName; 
    @JsonIgnore private int group_done;
    @JsonIgnore private int score;
	
    // for package field
    private int mPackage;
    
    //other fields
    @JsonIgnore private int totalRecordCount;
    
    //category attr
    @JsonIgnore private String force_attribute;
    @JsonIgnore private String cal_attribute;
    
    //brand subbrand 
    @JsonIgnore private String brandKeyword;
    @JsonIgnore private String subBrandKeyword;
    
    //tbl_product_group
    @JsonIgnore private String pictureNameList;
    
    private int redirectTypeRuntime;
    
    private String store;
    private String logo;
    private String slug;
    private int ShippingType;
    private int awardList;
    
    private int mCountReview;
    private double mSumRate;
    private int pdCountReview;
    private double pdSumRate;
    
    private int mobileRedirectType;
    @JsonInclude(Include.NON_EMPTY) private Integer flag;
    @JsonInclude(Include.NON_EMPTY) private Integer outOfStock;
//    @JsonInclude(Include.NON_EMPTY) @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtil.DATETIME_FORMAT,timezone = DateTimeUtil.GMT_TIMEZONE_TH )
    private Date startDate;
    private double base_price;
    private String youtubeLink;
    
    @JsonIgnore private String upc;
    private int enableAppInstall;
	private String appLogo;
	private String appDescription;
	private String appIosUrl;
	private String appAndroidUrl;
	private String schemeIOS;
	private String schemeAndroid;
	
	private int verifyFlag;
	private int enableToApp;

    private int totalComment;
    private int totalGroupComment;
    
	// product rating data
    private double productRatingScore;
    private int productRatingCount;
    private int merchantRatingCount;
    private double merchantRatingScore;
	
    // fix descrption product group
    private Double product_child_max_price;
    private String total_product_child;
    
    // ranking
    private int ranktype;
    private int current_rank;
	private int avg_click;
	private int brand_current_rank;
	private int brand_avg_click;
	
    public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}
	public Date getMerchant_date() {
		return merchant_date;
	}
	public void setMerchant_date(Date merchant_date) {
		this.merchant_date = merchant_date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getPrevious_price() {
		return previous_price;
	}
	public void setPrevious_price(Double previous_price) {
		this.previous_price = previous_price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getPicture_url() {
		return picture_url;
	}
	public void setPicture_url(String picture_url) {
		this.picture_url = picture_url;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(int subcategory) {
		this.subcategory = subcategory;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getParent_product_id() {
		return parent_product_id;
	}
	public void setParent_product_id(int parent_product_id) {
		this.parent_product_id = parent_product_id;
	}
	public int getTotal_child() {
		return total_child;
	}
	public void setTotal_child(int total_child) {
		this.total_child = total_child;
	}
	public int getRandom() {
		return random;
	}
	public void setRandom(int random) {
		this.random = random;
	}
	public String getGroup_merchant_id() {
		return group_merchant_id;
	}
	public void setGroup_merchant_id(String group_merchant_id) {
		this.group_merchant_id = group_merchant_id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSub_brand() {
		return sub_brand;
	}
	public void setSub_brand(String sub_brand) {
		this.sub_brand = sub_brand;
	}
	public int getRelated() {
		return related;
	}
	public void setRelated(int related) {
		this.related = related;
	}
	public String getReal_product_id() {
		return real_product_id;
	}
	public void setReal_product_id(String real_product_id) {
		this.real_product_id = real_product_id;
	}
	public int getLike_this_week() {
		return like_this_week;
	}
	public void setLike_this_week(int like_this_week) {
		this.like_this_week = like_this_week;
	}
	public int getLike_all_time() {
		return like_all_time;
	}
	public void setLike_all_time(int like_all_time) {
		this.like_all_time = like_all_time;
	}
	public int getView_all_time() {
		return view_all_time;
	}
	public void setView_all_time(int view_all_time) {
		this.view_all_time = view_all_time;
	}
	public int getLast_week_rank_like() {
		return last_week_rank_like;
	}
	public void setLast_week_rank_like(int last_week_rank_like) {
		this.last_week_rank_like = last_week_rank_like;
	}
	public int getLast_week_rank_all_time_like() {
		return last_week_rank_all_time_like;
	}
	public void setLast_week_rank_all_time_like(int last_week_rank_all_time_like) {
		this.last_week_rank_all_time_like = last_week_rank_all_time_like;
	}
	public int getLast_week_rank_all_time_view() {
		return last_week_rank_all_time_view;
	}
	public void setLast_week_rank_all_time_view(int last_week_rank_all_time_view) {
		this.last_week_rank_all_time_view = last_week_rank_all_time_view;
	}
	public int getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	public String getProduct_keyword() {
		return product_keyword;
	}
	public void setProduct_keyword(String product_keyword) {
		this.product_keyword = product_keyword;
	}
	public int getPopularity() {
		return popularity;
	}
	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}
	public int getSort_score() {
		return sort_score;
	}
	public void setSort_score(int sort_score) {
		this.sort_score = sort_score;
	}
	public String getTop_merchant() {
		return top_merchant;
	}
	public void setTop_merchant(String top_merchant) {
		this.top_merchant = top_merchant;
	}
	public int getMPackage() {
		return mPackage;
	}
	public void setMPackage(int mPackage) {
		this.mPackage = mPackage;
	}
	public String getForce_attribute() {
		return force_attribute;
	}
	public void setForce_attribute(String force_attribute) {
		this.force_attribute = force_attribute;
	}
	public String getCal_attribute() {
		return cal_attribute;
	}
	public void setCal_attribute(String cal_attribute) {
		this.cal_attribute = cal_attribute;
	}
	@JsonIgnore public List<Integer> getAllCategoryAttribute() {
		List<Integer> allAttr = new ArrayList<Integer>();
		
		String calAttr = getCal_attribute();
		String forceAttr = getForce_attribute();
		
		if (StringUtils.isNotBlank(calAttr)) {
			for (String attrId : StringUtils.split(calAttr, ',')) {
				allAttr.add(NumberUtils.toInt(attrId));
			}
		}
		
		if (StringUtils.isNotBlank(forceAttr)) {
			for (String attrId : StringUtils.split(forceAttr, ',')) {
				allAttr.add(NumberUtils.toInt(attrId));
			}
		}
		
		return allAttr;
	}
	public String getBrandKeyword() {
		return brandKeyword;
	}
	public void setBrandKeyword(String brandKeyword) {
		this.brandKeyword = brandKeyword;
	}
	public String getSubBrandKeyword() {
		return subBrandKeyword;
	}
	public void setSubBrandKeyword(String subBrandKeyword) {
		this.subBrandKeyword = subBrandKeyword;
	}
	public String getPictureNameList() {
		return pictureNameList;
	}
	public void setPictureNameList(String pictureNameList) {
		this.pictureNameList = pictureNameList;
	}
	public String getCoupon_id_list() {
		return coupon_id_list;
	}
	public void setCoupon_id_list(String coupon_id_list) {
		this.coupon_id_list = coupon_id_list;
	}
	public int getmPackage() {
		return mPackage;
	}
	public void setmPackage(int mPackage) {
		this.mPackage = mPackage;
	}
	public int getRedirectTypeRuntime() {
		return redirectTypeRuntime;
	}
	public void setRedirectTypeRuntime(int redirectTypeRuntime) {
		this.redirectTypeRuntime = redirectTypeRuntime;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public int getmCountReview() {
		return mCountReview;
	}
	public void setmCountReview(int mCountReview) {
		this.mCountReview = mCountReview;
	}
	public double getmSumRate() {
		return mSumRate;
	}
	public void setmSumRate(double mSumRate) {
		this.mSumRate = mSumRate;
	}
	public int getPdCountReview() {
		return pdCountReview;
	}
	public void setPdCountReview(int pdCountReview) {
		this.pdCountReview = pdCountReview;
	}
	public double getPdSumRate() {
		return pdSumRate;
	}
	public void setPdSumRate(double pdSumRate) {
		this.pdSumRate = pdSumRate;
	}
	public int getMobileRedirectType() {
		return mobileRedirectType;
	}
	public void setMobileRedirectType(int mobileRedirectType) {
		this.mobileRedirectType = mobileRedirectType;
	}
	public int getUpdate_status() {
		return update_status;
	}
	public void setUpdate_status(int update_status) {
		this.update_status = update_status;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public double getBase_price() {
		return base_price;
	}
	public void setBase_price(double base_price) {
		this.base_price = base_price;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
    public Date getStartDate() {
		return startDate;
	}
	public Integer getOutOfStock() {
		return outOfStock;
	}
	public void setOutOfStock(Integer outOfStock) {
		this.outOfStock = outOfStock;
	}
	public int getEnableAppInstall() {
		return enableAppInstall;
	}
	public void setEnableAppInstall(int enableAppInstall) {
		this.enableAppInstall = enableAppInstall;
	}
	public String getAppLogo() {
		return appLogo;
	}
	public void setAppLogo(String appLogo) {
		this.appLogo = appLogo;
	}
	public String getAppDescription() {
		return appDescription;
	}
	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}
	public String getAppIosUrl() {
		return appIosUrl;
	}
	public void setAppIosUrl(String appIosUrl) {
		this.appIosUrl = appIosUrl;
	}
	public String getAppAndroidUrl() {
		return appAndroidUrl;
	}
	public void setAppAndroidUrl(String appAndroidUrl) {
		this.appAndroidUrl = appAndroidUrl;
	}
	public String getYoutubeLink() {
		return youtubeLink;
	}
	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}
	public String getSchemeIOS() {
		return schemeIOS;
	}
	public void setSchemeIOS(String schemeIOS) {
		this.schemeIOS = schemeIOS;
	}
	public String getSchemeAndroid() {
		return schemeAndroid;
	}
	public void setSchemeAndroid(String schemeAndroid) {
		this.schemeAndroid = schemeAndroid;
	}
	public void setVerifyFlag(int verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	public int getVerifyFlag() {
		return verifyFlag;
	}
	public double getProductRatingScore() {
		return productRatingScore;
	}
	public void setProductRatingScore(double productRatingScore) {
		this.productRatingScore = productRatingScore;
	}
	public int getProductRatingCount() {
		return productRatingCount;
	}
	public void setProductRatingCount(int productRatingCount) {
		this.productRatingCount = productRatingCount;
	}
	public int getMerchantRatingCount() {
		return merchantRatingCount;
	}
	public void setMerchantRatingCount(int merchantRatingCount) {
		this.merchantRatingCount = merchantRatingCount;
	}
	public double getMerchantRatingScore() {
		return merchantRatingScore;
	}
	public void setMerchantRatingScore(double merchantRatingScore) {
		this.merchantRatingScore = merchantRatingScore;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getGroup_done() {
		return group_done;
	}
	public void setGroup_done(int group_done) {
		this.group_done = group_done;
	}
	public int getEnableToApp() {
		return enableToApp;
	}
	public void setEnableToApp(int enableToApp) {
		this.enableToApp = enableToApp;
	}
	public void setTotalComment(int totalComment) {
		this.totalComment = totalComment;
	}
	public int getTotalComment() {
		return totalComment;
	}
	public void setTotalGroupComment(int totalGroupComment) {
		this.totalGroupComment = totalGroupComment;
	}
	public int getTotalGroupComment() {
		return totalGroupComment;
	}
	public Double getProduct_child_max_price() {
		return product_child_max_price;
	}
	public void setProduct_child_max_price(Double product_child_max_price) {
		this.product_child_max_price = product_child_max_price;
	}
	public String getTotal_product_child() {
		return total_product_child;
	}
	public void setTotal_product_child(String total_product_child) {
		this.total_product_child = total_product_child;
	}
	public int getCurrent_rank() {
		return current_rank;
	}
	public void setCurrent_rank(int current_rank) {
		this.current_rank = current_rank;
	}
	public int getAvg_click() {
		return avg_click;
	}
	public void setAvg_click(int avg_click) {
		this.avg_click = avg_click;
	}
	public int getBrand_current_rank() {
		return brand_current_rank;
	}
	public void setBrand_current_rank(int brand_current_rank) {
		this.brand_current_rank = brand_current_rank;
	}
	public int getBrand_avg_click() {
		return brand_avg_click;
	}
	public void setBrand_avg_click(int brand_avg_click) {
		this.brand_avg_click = brand_avg_click;
	}
	public int getRanktype() {
		return ranktype;
	}
	public void setRanktype(int ranktype) {
		this.ranktype = ranktype;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public int getShippingType() {
		return ShippingType;
	}
	public void setShippingType(int shippingType) {
		ShippingType = shippingType;
	}
	public int getAwardList() {
		return awardList;
	}
	public void setAwardList(int awardList) {
		this.awardList = awardList;
	}
	@JsonIgnore

	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	
}