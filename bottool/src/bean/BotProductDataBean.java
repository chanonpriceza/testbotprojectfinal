package bean;

import java.sql.Timestamp;

public class BotProductDataBean {
	    
	public enum STATUS {
		W, R ,C ,E
		//Waiting, Running, Complete, Error
	}
				
	private int id;
	private int merchantId;
	private int categoryId;
		
	private String name;
	private double price;
	private String url;
	private String description;
	
	private String pictureUrl;
	private String pictureData;
	private String urlForUpdate;
	private String realProductId;
	private String keyword;
	
	private Timestamp updateDate;
	private int errorUpdateCount;

	private Timestamp merchantUpdateDate;
	private String status;
	
	private double basePrice;
	private String upc;
	private String dynamicField;
	
	private boolean isExpire;
	private String httpStatus;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getPictureData() {
		return pictureData;
	}

	public void setPictureData(String pictureData) {
		this.pictureData = pictureData;
	}

	public String getUrlForUpdate() {
		return urlForUpdate;
	}

	public void setUrlForUpdate(String urlForUpdate) {
		this.urlForUpdate = urlForUpdate;
	}

	public String getRealProductId() {
		return realProductId;
	}

	public void setRealProductId(String realProductId) {
		this.realProductId = realProductId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public int getErrorUpdateCount() {
		return errorUpdateCount;
	}

	public void setErrorUpdateCount(int errorUpdateCount) {
		this.errorUpdateCount = errorUpdateCount;
	}

	public Timestamp getMerchantUpdateDate() {
		return merchantUpdateDate;
	}

	public void setMerchantUpdateDate(Timestamp merchantUpdateDate) {
		this.merchantUpdateDate = merchantUpdateDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDynamicField() {
		return dynamicField;
	}

	public void setDynamicField(String dynamicField) {
		this.dynamicField = dynamicField;
	}

	public boolean isExpire() {
		return isExpire;
	}

	public void setExpire(boolean isExpire) {
		this.isExpire = isExpire;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}
	
		
}