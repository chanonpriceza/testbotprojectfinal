package bean;

import java.sql.Timestamp;

public class BotSendDataBean {
	    
	public enum ACTION {
		ADD, UPDATE ,DELETE, UPDATE_PIC
	}
	public enum STATUS {
		W, C, E 
	}
	
	private int id;
	private String action;
	private int merchantId;
	private int categoryId;
		
	private String name;
	private double price;
	private String url;
	private String description;
	private byte[] pictureData;	
	private String pictureUrl;
	private String urlForUpdate;
	private Timestamp merchantUpdateDate;
	private Timestamp updateDate;
	private String realProductId;
	private String keyword;
	private double basePrice;
	private String upc;
	private String dynamicField;
	

	public static BotSendDataBean createSendDataBean(BotProductDataBean productDataBean, byte[] pictureData, ACTION action) {
		BotSendDataBean sendDataBean = new BotSendDataBean();
				
		sendDataBean.setAction(action.toString());
		sendDataBean.setMerchantId(productDataBean.getMerchantId());
		sendDataBean.setCategoryId(productDataBean.getCategoryId()); 
		sendDataBean.setName(productDataBean.getName());
		sendDataBean.setPrice(productDataBean.getPrice());
		sendDataBean.setUrl(productDataBean.getUrl());
		sendDataBean.setDescription(productDataBean.getDescription());
		sendDataBean.setPictureData(pictureData);
		sendDataBean.setPictureUrl(productDataBean.getPictureUrl());				
		sendDataBean.setMerchantUpdateDate(productDataBean.getMerchantUpdateDate());
		sendDataBean.setUpdateDate(productDataBean.getUpdateDate());
		sendDataBean.setRealProductId(productDataBean.getRealProductId());
		sendDataBean.setKeyword(productDataBean.getKeyword());
		sendDataBean.setBasePrice(productDataBean.getBasePrice());
		sendDataBean.setUpc(productDataBean.getUpc());
		sendDataBean.setDynamicField(productDataBean.getDynamicField());
		
		return sendDataBean;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrlForUpdate() {
		return urlForUpdate;
	}
	public void setUrlForUpdate(String urlForUpdate) {
		this.urlForUpdate = urlForUpdate;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Timestamp getMerchantUpdateDate() {
		return merchantUpdateDate;
	}
	public void setMerchantUpdateDate(Timestamp merchantUpdateDate) {
		this.merchantUpdateDate = merchantUpdateDate;
	}
	public byte[] getPictureData() {
		return pictureData;
	}
	public void setPictureData(byte[] pictureData) {
		this.pictureData = pictureData;
	}

	public String getRealProductId() {
		return realProductId;
	}

	public void setRealProductId(String realProductId) {
		this.realProductId = realProductId;
	}
    public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}	
	public String getDynamicField() {
		return dynamicField;
	}

	public void setDynamicField(String dynamicField) {
		this.dynamicField = dynamicField;
	}

}